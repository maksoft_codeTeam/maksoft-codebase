<?php
$googleVerify = array_filter(array_map(
    function($setting){
         if($setting['conf_key'] == 'GOOGLE_SITE_VERIF'){
             return $setting['conf_value'];
         }
    },
    $o_page->get_sConfigurations($o_page->_site['SitesID'])
), function($var){ 
        return !is_null($var); 
    }
);

$code = "";
$key = isset($googleVerify[key($googleVerify)]) ? $googleVerify[key($googleVerify)] : null ;

if($key){
    $code = <<<HEREDOC
<meta name="google-site-verification" content="$key">
HEREDOC;
}
return $code;

