<?php
$googleAnalytics = array_filter(array_map(
    function($setting){
         if($setting['conf_key'] == 'GOOGLE_TRACKING_CODE'){
             return $setting['conf_value'];
         }
    },
    $o_page->get_sConfigurations($o_page->_site['SitesID'])
), function($var){ 
        return !is_null($var); 
    }
);

$code = "";
$key = isset($googleAnalytics[key($googleAnalytics)]) ? $googleAnalytics[key($googleAnalytics)] : null ;

if($key){
    $code = <<<HEREDOC

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '$key', 'auto');
  ga('send', 'pageview');

</script>
HEREDOC;
}
return $code;

