<style>
#sticky-bar {
  /*position: fixed;*/
  /*display: none;*/
  width: 100%;
  top: 0;
  padding: 5px;
  color: #5e3939;
  background-color: rgba(255, 237, 237, 0.98);
	text-align:center;
}

}

#sticky-bar > .wrap {
 /* max-width: 900px;*/
  margin: 0 auto;
  text-align:center;
}
#sticky-bar.active {
    /* Animation */
    -webkit-animation-duration: 0.8s;
    animation-duration: 0.8s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
    /* Start animation to go down */
    -webkit-animation-name: goDown;
    animation-name: goDown;
}
#sticky-bar.deactive {
    /* Animation */
    -webkit-animation-duration: 0.8s;
    animation-duration: 0.8s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
    /* Start animation to go up */
    -webkit-animation-name: goUp;
    animation-name: goUp;
}
/* ANIMATION for go up */
@-webkit-keyframes goUp {
    0% {
        -webkit-transform: none;
        transform: none;
    }
    100% {
        -webkit-transform: translate3d(0, -100px, 0);
        transform: translate3d(0, -100px, 0);
    }
}
@keyframes goUp {
    0% {
        -webkit-transform: none;
        transform: none;
    }
    100% {
        -webkit-transform: translate3d(0, -100px, 0);
        transform: translate3d(0, -100px, 0);
    }
}
/* ANIMATION for go down */
@-webkit-keyframes goDown {
    0% {
        -webkit-transform: translate3d(0, -100%, 0);
        transform: translate3d(0, -100%, 0);
    }
    100% {
        -webkit-transform: none;
        transform: none;
    }
}
@keyframes goDown {
    0% {
        -webkit-transform: translate3d(0, -100%, 0);
        transform: translate3d(0, -100%, 0);
    }
    100% {
        -webkit-transform: none;
        transform: none;
    }
}
</style>

<div id="sticky-bar">
  <div class="wrap">
    <?=(defined("SITE_MAINTENANCE")) ? SITE_MAINTENANCE:"����� � � ����������."?>
    <!--<button type="button">X</button>-->
  </div>
</div>
