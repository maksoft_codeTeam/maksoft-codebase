<?php
$shareKit = array_filter(array_map(
    function($setting){
         if($setting['conf_key'] == 'SHARE_KIT'){
             return $setting['conf_value'];
         }
    },
    $o_page->get_sConfigurations($o_page->_site['SitesID'])
), function($var){ 
        return !is_null($var); 
    }
);

$ssk_count = "";
$ssk_admin = "";
$p_url  = $o_page->get_pLink($o_page->n, $o_page->SiteID, false);
if($o_page->_user['AccessLevel'] > 0) {
	$ssk_count = "ssk-count";
	$ssk_admin = "
	<div class='ssk-sticky ssk-left ssk-left-down ssk-center'>
	<a href='$p_url' class='ssk ssk-copy ssk-silver' title='������� ������� ���� �� ���� ��������'><i class='fa fa-link' aria-hidden='true'></i></a>
	<a href='page.php?n=111&amp;ParentPage=$o_page->n&amp;SiteID=$o_page->SiteID' class='ssk ssk-emerald' title='���� ��������'><span class='hiddentitle'>������</span> <i class='fa fa-file-text-o' aria-hidden='true'></i></a>
	<a href='page.php?n=132&amp;no=$o_page->n&amp;SiteID=$o_page->SiteID' class='ssk ssk-carrot' title='���������� ���� ��������'><span class='hiddentitle'>����������</span> <i class='fa fa-pencil' aria-hidden='true'></i></a>
	<a href='page.php?n=11&amp;SiteID=$o_page->SiteID' class='ssk ssk-amethyst' title='�������������'><span class='hiddentitle'>�������������</span> <i class='fa fa-cog' aria-hidden='true'></i></a>
    </div>
	<style>.ssk-left-down { top: 87% !important; } </style>
	";
}
$c_url = "".$_SERVER['SERVER_NAME']."";
$share_kit_url  = $o_page->get_pLink($o_page->n, $o_page->SiteID, "", true);
$code = "";
$key = isset($shareKit[key($shareKit)]) ? $shareKit[key($shareKit)] : null ;

if(($key) or ( $o_page->_user['AccessLevel'] > 2) ) {
    $code = <<<HEREDOC
<div class="ssk-sticky ssk-left $ssk_count ssk-center">
    <a href="" class="ssk ssk-facebook"></a>
    <a href="" class="ssk ssk-twitter"></a>
    <a href="" class="ssk ssk-google-plus"></a>
    <a href="" class="ssk ssk-pinterest"></a>
	<a href="" class="ssk ssk-linkedin"></a>
</div>

$ssk_admin

<!-- Share Kit CSS -->
<link rel="stylesheet" href="/web/assets/share-kit/css/share-kit.css" type="text/css">
<script type="text/javascript" src="/web/assets/share-kit/js/share-kit.min.js"></script>
<script type="text/javascript">
    // Init Social Share Kit
    SocialShareKit.init({
        url: '$share_kit_url',
        onBeforeOpen: function(targetElement, network, paramsObj){
            console.log(arguments);
        },
        onOpen: function(targetElement, network, url, popupWindow){
            console.log(arguments);
        },
        onClose: function(targetElement, network, url, popupWindow){
            console.log(arguments);
        }
    });


</script>

HEREDOC;
}
return $code;

