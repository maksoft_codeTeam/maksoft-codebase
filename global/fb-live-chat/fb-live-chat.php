<?php
$fbLiveChat= array_filter(array_map(
    function($setting){
         if($setting['conf_key'] == 'FB_CHAT_PAGE_NAME'){
             return $setting['conf_value'];
         }
    },
    $o_page->get_sConfigurations($o_page->_site['SitesID'])
), function($var){ 
        return !is_null($var); 
    }
);

$code = "";
$key = $fbLiveChat[key($fbLiveChat)];
if($key){
    $code = <<<HEREDOC

<script type="text/javascript" src="/web/assets/fb-live-chat/fb-live-chat.min.js"></script>
<script type="text/javascript">
	fbLiveChat.init({
		sdk_locale: 'bg_BG',
		facebook_page: '$key', // required
		position: 'right',
		header_text: '����� �������?',
		header_background_color: '#3b5998',
		show_close_btn: true,
		animation_speed: 400,
		auto_show_delay: 15000
	});
</script>
<style>
span#fb-chat-header-text img {
    margin-left: 7px;
}
span.fb-chat-text-quest{
    margin-left: 10px;
    margin-top: 2px;
	color:#fff !important; 
}
</style>
HEREDOC;
}
return $code;

