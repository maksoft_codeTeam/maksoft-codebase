<?php

if(!isset($o_page)) {
    return;
}

?>


<script src="/global/prism/prism.js" data-manual></script>
<link rel="stylesheet" href="/global/prism/prism.css">
<style>
pre { white-space: pre; }

textarea {
  height: 240px !important;
}

p {
  font-weight: bold;
}
</style>




<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <h3>Textarea Output Pre Code</h3>
<hr>
 
<div class="form-group">  
<textarea id="code" class="form-control"></textarea>
</div>
  
<div class="form-group">  
<button type="submit" id="submit-code" class="btn btn-success">Submit Your Code</button>
</div>

  
<p>Front End View</p>  
<pre class="language-css"><code class="language-css">p { color: red }</code></pre>




<script>
jQuery(window).load(function(){  
    jQuery("#submit-code").keydown(function() {
        jQuery("pre code").text(jQuery("textarea").val());
    });
    jQuery('#code').keypress(function() {
        var dInput = this.value;
        var html = Prism.highlight(dInput, Prism.languages.javascript);
        jQuery("pre code").text(dInput);
    });
});  

</script>
