<?php
require_once __DIR__.'/../../modules/vendor/autoload.php';
require_once __DIR__.'/config.php';
if(user::$uLevel < 3) {
    return ;
}

if(!isset($container) or !$container instanceof \Pimple\Container){
    require_once __DIR__.'/../../Templates/ra/container.php';
    $container = new \Pimple\Container();
    $container->register(new RaMaksoftLoader());
}

if(!isset($gate)){
    $gate = $container['gate'];
}

define('DEFAULT_ORDER_STATUS', 0);
define('PENDING', DEFAULT_ORDER_STATUS);
define('COMPLETED', 1);
define('REJECTED', 2);

$order_has_status = array(
    PENDING => '<i class="fa fa-circle-o-notch fa-spin fa-fw order-waiting" aria-hidden="true" title="'.CART_CONFIG::$status[PENDING].'"></i>', 
    COMPLETED => '<i class="fa fa-check order-complete" aria-hidden="true" title="'.CART_CONFIG::$status[COMPLETED].'"></i>', 
    REJECTED => '<i class="fa fa-ban order-cancel" aria-hidden="true" title="'.CART_CONFIG::$status[REJECTED].'"></i> ',
);
echo '<link href="../../global/cart/assets/css/orders.css" rel="stylesheet" type="text/css" />';

function print_orders($gate, $orders, $templ1='single_order.php', $templ2='empty_orders.php'){
    global $order_has_status;

    $i = 0;

    foreach($orders as $order) { 
        $cart_items = $gate->cart()->get_cart_items($order->id);
        if(count($cart_items) == 0){
            continue;
        }
        include __DIR__.'/templates/'.$templ1;
        $i++;
    } 

    if (count($orders) == 0) {
        include __DIR__ .'/templates/'.$templ2;
    }
}
