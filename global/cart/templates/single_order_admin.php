<?php
include_once __DIR__.'/../forms/ChangeStatus.php';
$status_form = new ChangeStatus($_POST, $order, $gate);
if($_SERVER['REQUEST_METHOD'] === 'POST') {
    try{
        $status_form->is_valid();
        $status_form->save();
        session_write_close();
    } catch (\Exception $e){
        echo $e->getMessage();
        foreach($status_form as $field){
            foreach($field->get_errors() as $err){
                echo $err;
            }

        }
    }
}
parse_str($_SERVER['QUERY_STRING'], $url);
$url['order_id'] = $order->id;
$url['templ'] = 303;
$url['print'] = 1;
$url = http_build_query($url);
?>
<div class="toggle toggle-transparent-body orders-page">
    <div class="toggle">
        <label>
        <?php 
            if(array_key_exists($order->status, $order_has_status)) {
                echo $order_has_status[$order->status];
            } else {
                echo $order_has_status[DEFAULT_ORDER_STATUS];
            }
        ?>
        <span class="order-text">������� �:</span> <?=$order->id?><span class="order-text"> �� ����: </span> <span class="order-date"><?=date('d-m-Y H:i', strtotime($order->checkout_date));?></span>
        <span class="order-text" style="font-size:12px;">/ <?=$order->Name?>, <?=$order->Phone?> - <a href="mailto:<?=$order->EMail?>"><?=$order->EMail?></a> /</span>
        </label>
        <div class="toggle-content">
			<div class="print-order">
				<a href="/page.php?<?=$url?>" target="_blank" class="btn btn-default btn-bordered">
					<i class="fa fa-print" aria-hidden="true"></i>
					<span>���������</span>
				</a>
			</div>
           
            <div class="cartContent">
                <div class="item head clearfix">
                    <span class="sku">N</span>
                    <span class="sku">���</span>
                    <span class="product_cart">�������</span>
                    <span class="remove_item"></span>
                    <span class="total_price">����</span>
                    <span class="qty">���</span>
                </div>
                <?php
                    $i = 1;
                    foreach ($cart_items as $item) {
                        $it = json_decode($item->data_json);
                        include __DIR__.'/single_item.php';
                        $i++;
                    }
                ?>
                <div class="total-sum">
					����: <strong><?php echo $gate->cart()->get_cart_sum($order->id);?></strong> ��.
				</div>
            </div>
            <div class='cartContent change-status'>
            <?php
                echo $status_form->start();
                echo $status_form->status;
                echo $status_form->order_id;
                echo $status_form->action;
                echo $status_form->submit;
                echo $status_form->end();
            ?>
            </div>
        </div>
    </div>
</div>
