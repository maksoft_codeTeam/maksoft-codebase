<div class="item">
    <div class="sku"><b><?=$i?>.</b></div>
    <div class="sku"><?=$item->sku?></div>
    <div class="cart_img product_img width-100">
    <?php 
        if ( $it->attributes->image ){
            echo '<img src="'.$it->attributes->image.'" alt="" width="80">';
        }
    ?>
    </div>
    <a href="<?=$it->attributes->link;?>" class="product_name">
    <span><?=iconv('utf8', 'cp1251', $it->name);?></span>
    </a>

    <div class="total_price"><span><?=$it->total;?> ��.</span>
    </div>
    <div class="qty"><?=$item->qty;?> x <?=$item->price;?></div>

    <div class="clearfix"></div>
</div>
