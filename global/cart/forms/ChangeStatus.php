<?php
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Field;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\Integerish;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\BaseForm;
use Jokuf\Form\Bootstrap;
use Jokuf\Form\DivForm;


class ChangeStatus extends Bootstrap
{
    protected $statuses = array(
        0 => "������������",
        1 => "������",
        2 => "��������",
    );

    protected $gate;

    public function __construct($post_data=null, $order, $gate)
    {
        $this->gate = $gate;

        $this->action = Hidden::init()->add('value', 'status_update');

        $this->order_id = Hidden::init()
            ->add_validator(Integerish::init()->err_msg("������ order_id ������ �� ���� ���� �����!"))
            ->add('value', $order->id);

        $this->status = Select::init()
            ->add_validator(Integerish::init()->err_msg("������ ������ ������ �� ���� ���� �����!"))
            ->add('value', $order->status)
            ->add('data', $this->statuses);
        $this->submit = Submit::init()
            ->add('class', 'btn btn-3d btn-green')
            ->add('value', '������');
        parent::__construct($post_data);
    }

    public function validate_order_id($field)
    {
        return (int) $field->value > 0;
    }

    public function save()
    {
        $this->gate->cart()->update_cart_status($this->order_id->value, $this->status->value);
    }
}
