<?php 
require_once __DIR__."/../../modules/vendor/autoload.php";
?>
<section class="check-out">
       <div class="container">
        <?php
        if(!class_exists('Reset') or !class_exists('Cart') or !class_exists('Remove') or !class_exists('Purchase')){
                define("cart_error", true);
                echo '<strong>����� ������� �� � ���������. ���� �������� �� � ��� 02/8464646</strong><br>';
        }
        ?>
        <?php if(!isset($_cart) or $_cart->count() < 1 or defined("cart_error")){ ?>
        <!-- EMPTY CART -->
        <div class="panel panel-default">
            <div class="panel-body">
                <strong>��������� � ������</strong><br>
                ������ �������� �������� ��� ������ �������.<br>
                ��������� <a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>">���</a>, �� �� ���������� �� ����������. <br>
            </div>
        </div>
        <?php return; ?>
        <!-- /EMPTY CART -->
        <?php } ?>
        <!-- CART -->
        <div class="row">
            <!-- LEFT -->
            <div class="col-lg-9 col-sm-8">
                <!-- CART -->
                <div class="cartContent clearfix" method="post" action="#">
                    <!-- cart content -->
                    <div id="cartContent">
                        <!-- cart header -->
                        <div class="item head clearfix">
                            <div class="sku">���</div>
                            <div class="product_cart">�������</div>
                            <div class="x"></div>
                            <div class="total_price">����</div>
                            <div class="qty-price">��.����</div>
                            <div class="qty-kvo">���</div>
                        </div>
                        <!-- /cart header -->
                    <?php
                    $remove = array();
                    $cart_string = <<<HEREDOC
                        <table>
                        <tr>
                            <th># </th>
                            <th>��� </th>
                            <th>������� </th>
                            <th>�-�� </th>
                            <th>��.���� </th>
                            <th>���� </th>
                            <th>���� </th>
                        </tr>
HEREDOC;
                    $i = 1;
                    foreach ($_cart->get_objects() as $item){

                    $cart_string .= "
                            <tr>
                                <td> $i </td>
                                <td>". $item->getSku()  ."</td>
                                <td>". $item->getName() ." - $item->color </td>
                                <td>". $item->getQty() ." </td>
                                <td>". $item->getPrice()." </td>
                                <td>". $item->total()." </td>
                            </tr> ";
                    ?>
                        <!-- cart item -->
                        <div class="item">
                            <div class="sku">
                                <?=$item->getSku();?>
                            </div>
                            <div class="product_cart">
                            <div class="cart_img product_img width-100"><img src="<?=$item->image;?>" alt="" width="80"></div>
                            <div class="product-title">
                            <a href="<?=$item->link?>">
                                <span><?=$item->getName();?></span>
                                </a>
                                <?php
                                    if($item->color){
                                        ?> 
                                        <br><small>����: <?=$item->color;?></small>
                                        <?php
                                    }
                                ?>

                            </a>
                            </div>
                            </div>
                            <?php 
                                $remove[$item->getSku()] = new Remove($_cart, $item->getSku(), $_POST);
                                $remove_form = $remove[$item->getSku()];
                                echo $remove_form->start();
                                echo $remove_form->action;
                                echo $remove_form->sku;
                                echo $o
                                ?>
                                <button type="submit" class="remove_item"><i class="fa fa-times"></i></button>
                                <?php
                                echo $remove_form->end();
                            ?>

                            <div class="qty"><span><?=$item->total();?></span></div>
                            <div class="qty">
                                <!-- <input type="number" value="" name="qty" maxlength="3" max="999" min="1"> -->
                                <?=$item->getPrice();?>
                             </div>
                            <div class="qty">
                                <!-- <input type="number" value="" name="qty" maxlength="3" max="999" min="1"> -->
                                <?=$item->getQty()?> <?$item->getUnit();?>
                             </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- /cart item -->
                    <?php 
                        $i++;
                    }
                    ?>
                        <!-- update cart -->
                <?php
                    $reset = new Reset($_cart);
                    $reset->setId("empty_cart");
                ?>
                    <form method="POST" onsubmit="return confirm('������� �� ���, �� ������ �� �������� ������ �������?');">
                    <?php
                    echo $reset->action;
                    echo $reset->submit;
                    echo $reset->end();
                    ?>
                        <div class="clearfix"></div>
                    </div>
                    <!-- /cart content -->
                </div>
                <!-- /CART -->
            </div>
            <!-- RIGHT -->

                                <?php
                                $cart_string .= "
                                <tr> <td colspan=\"7\"></td></tr>
                                <tr> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><strong>���.������:</strong> </td>
                                    <td>{$_cart->sum()} </td>
                                </tr>
                                </table> ";
                                ?>
            <div class="col-lg-3 col-sm-4">
                <div class="toggle-transparent toggle-bordered-full clearfix">
                    <div class="toggle active">
                        <label>��������� ��� ���������</label>
                        <div class="toggle-content" style="display: none;">
                            <?php 
                                echo $purchase->start();
                                echo "<label>{$purchase->name->label}</label>";
                                echo $purchase->name;
                                echo "<label>{$purchase->mail->label}</label>";
                                echo $purchase->mail;
                                echo "<label>{$purchase->phone->label}</label>";
                                echo $purchase->phone;
                                echo "<label>{$purchase->address->label}</label>";
                                echo $purchase->address;
                                echo "<label>{$purchase->notes->label}</label>";
                                echo $purchase->notes;
                                echo "<label>{$purchase->delivery->label}</label>";
                                echo $purchase->delivery;
                                echo $purchase->action;
                            ?>

                                <!--
                                <span class="clearfix">
                                    <span class="pull-right"><?php //echo $_cart->sum();?> ��.</span>
                                    <span class="pull-left">������� ������:</span>
                                </span>
                                <span class="clearfix">
                                    <span class="pull-right"><?php //echo $_cart->vat();?> ��.</span>
                                    <span class="pull-left">���:</span>
                                </span>
                                -->
                                <hr>
                                <span class="clearfix">
                                    <!-- <span class="pull-right size-20"><?php //echo $_cart->sum_with_vat();?> ��.</span> -->
                                    <span class="pull-right size-20"><?=$_cart->sum();?> ��.</span>
                                    <strong class="pull-left">����:</strong>
                                </span>
                                <span class="clearfix">
                                </span>
                                <input type="hidden" name="order" value="<?=htmlentities($cart_string, ENT_QUOTES, 'cp1251');?>">
                                <button type="submit" class="btn btn-primary btn-lg btn-block size-15"><i class="fa fa-mail-forward"></i> ���������</button>
                                <?=$purchase->end();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /CART -->
	</div>
</section>


<script>
var $ = jQuery;
// $j is now an alias to the jQuery function; creating the new alias is optional.
$(document).ready(function(){
    .confirm({
            buttons: {
            hey: function(){
            location.href = this.$target.attr('href');
            }
        }
    });
    //$("#delete_cart").confirm({
    //    theme: 'black',
    //    title: '��������',
    //    confirmButton: '��',
    //    cancelButton: '��',
    //    content: '������� �� ���, �� ������ �� �������� ������������?',
    //    confirm: function(){
    //        var form = document.getElementById("delete")
    //        form.submit();
    //    },
    //    cancel: function(){
    //    }
    //});

});
</script>
