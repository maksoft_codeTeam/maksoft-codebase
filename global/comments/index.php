<?php
require_once "modules/vendor/autoload.php";
require_once "lib/Database.class.php";
require_once "global/comments/forms/NewComment.php";
require_once "global/recaptcha/init.php";
$db = new Database();
$db->exec("SET NAMES UTF8");
$gate = new Maksoft\Gateway\Gateway($db);

$post_comment = new NewComment($gate->page(), $o_page->_page['n'], $_POST);

if($_SERVER['REQUEST_METHOD'] === "POST" and isset($_POST['action'])){
    if($_POST['action'] === $post_comment->action->value){
        $post_comment->is_valid();
        $post_comment->save();
    }
}
    
