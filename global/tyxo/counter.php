<?php
$tyxoCounter = array_filter(array_map(
    function($setting){
         if($setting['conf_key'] == 'tyxo_counter'){
             return $setting['conf_value'];
         }
    },
    $o_page->get_sConfigurations($o_page->_site['SitesID'])
), function($var){ 
        return !is_null($var); 
    }
);

$code = "";
$key = isset($tyxoCounter[key($tyxoCounter)]) ? $tyxoCounter[key($tyxoCounter)] : null ;
if($key){
    $code = <<<HEREDOC
<script>
(function(i,s,o,g,r,a,m){i['TyxoObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//s.tyxo.com/c.js','tx');
tx('create', '$key');
tx('pageview');
</script>
HEREDOC;
}
return $code;

