<?php
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\MinLength;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class CreateGroupForm extends Bootstrap
{
    private $services, $group;
    public $title = '������ ��������';

    public function __construct($services, $group,  $post_data)
    {
        $this->services = $services;

        $class = 'form-control';

        $this->group = $group ? (object) $group: null;
        
        $this->name = Text::init()
            ->add("label", "��� �� �������")
            ->add_validator(NotEmpty::init(true))
            ->add('class', 'form-control');

        $this->action = Hidden::init()
            ->add('value', 'createGroupForm');

        $this->submit = Submit::init()
            ->add('class', 'btn btn-default btn-lg btn-block')
            ->add('value', '������');

        parent::__construct($post_data);
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function save()
    {
        $clean = $this->clean_data();
        $parent_id = $this->group ? $this->group->group_id : 0;
        $SiteID = $this->services['o_page']->_site['SitesID'];
        $id = $this->services['gate']->site()->insertGroup($parent_id, strip_tags($clean['name']), $SiteID);
        $this->services['gate']->site()->setGroupSortOrder($id, $id);
        return true;
    }
}
