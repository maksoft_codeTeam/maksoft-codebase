<?php
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class EditTaskForm extends Bootstrap
{
    private $gate, $n, $SiteID;

    public function __construct($services, $task, $post_data) {
        $class = 'form-control';
        $this->services = $services;
        $this->task = $task;

        $this->priority = Select::init()
            ->add('class', $class)
            ->add('label', '���������')
            ->add('data', array(1 => '�����', 2 => '������', 3 => '�����'))
            ->add('value', $task->priority);
        $usersTmp = $services['gate']->user()->getUsersByCompanyId($services['o_page']->_user['FirmID']);
        $users = array();
        foreach($usersTmp as $user) {
            $users[$user->ID] = $user->Name;
        }

        $this->fromUser = Select::init()
            ->add('data', $users)
            ->add('label', '�� ��������')
            ->add('value', $task->from_user)
            ->add('class', $class);
        $this->startDate = Text::init()
            ->add('label', '&nbsp;')
            ->add('value', date('Y-m-d H:i', strtotime($task->start_date)))
            ->add('data-format', 'dd-mm-yyyy')
            ->add('class', 'hasDatepicker form-control');
        $this->period = Integer::init()
            ->add('label', '����')
            ->add('class', $class .' stepper')
            ->add('min', 0)
            ->add('max', 366)
            ->add('step', 1)
            ->add('value', $task->period);
        $tempTypes = $services['gate']->task()->getTaskTypes();
        $types = array();
        foreach($tempTypes as $type) {
            $types[$type->task_type_ID] = $type->description;
        }
        $this->type = Select::init()
            ->add('data', $types)
            ->add('value', $task->task_type)
            ->add('label', '���')
            ->add('class', $class);
        $this->status = Select::init()
            ->add('value', $task->completed)
            ->add('label', '&nbsp;')
            ->add('class', $class)
            ->add('data', array('0'=>'�����������', '1' => '���������'));

        $this->repeat_in = Select::init()
            ->add('class', $class)
            ->add('label', '&nbsp;')
            ->add('value', $task->repeat_in)
            ->add('data', array(
                  0 => '�� �� �������',
                  7 => '���� �������',
                  14 => '���� 2 �������',
                  30 => '�� 1 ����� ',
                  60 => '�� 2 ������',
                  90 => '�� 3 ������',
                  182 => '�� 6 ������',
                  365 => '���� 1 ������',
            ));
        $this->timeToFinish = Integer::init()
            ->add('label', '�����')
            ->add('class', $class .' stepper')
            ->add('value', $task->time_to_finish)
            ->add('min', 0)
            ->add('max', 24)
            ->add('step', 1)
            ->add('value', 1);
        $tmpCompanies = $services['gate']->fak()->getCompaniesList();
        $companies = array();
        foreach($tmpCompanies as $company) {
            $companies[$company->ID] = $company->Name;
        }


        $this->company = Select::init()
            ->add('label', '�����')
            ->add('value', $task->ClientID)
            ->add('data', $companies)
            ->add('class', $class);

        if($services['o_page']->_user['FirmID'] != 1097) {
            $this->company = Hidden::init()
                ->add('label', '&nbsp;')
                ->add('value', '0');
        }

        $this->subject = Text::init()
            ->add('label', '�������')
            ->add('value', $task->subject)
            ->add('class', $class);

        $this->content = Textarea::init()
            ->add('class', 'summernote form-control')
            ->add('value', $task->notes)
            ->add('data-height', 200)
            ->add('data-lang', 'en-US');

        $this->action = Hidden::init()->add('value', 'edit_task');
        parent::__construct($post_data);
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                return false;
            }
        }
        return false;
    }

    public function save()
    {
        $cleaned = $this->clean_data();
        return $this->services['gate']->task()->updateTask(
            $this->task->taskID,
            $cleaned['subject'],
            $cleaned['content'],
            $cleaned['startDate'],
            $this->services['o_page']->_user['ID'],
            $cleaned['toUser'],
            $cleaned['timeToFinish'],
            $cleaned['type'],
            $cleaned['priority'],
            $cleaned['period'],
            $cleaned['company'],
            $cleaned['completed'],
            $cleaned['repeat_in']
        );
    }
}

