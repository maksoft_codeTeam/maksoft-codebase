<?php
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\MinLength;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class SetImageAsSiteLogoForm extends Bootstrap
{
    private $services, $image;
    public $title = '��������� ���� ����������� ���� ���� �� �����?';

    public function __construct($services, $image,  $post_data) {
        $this->services = $services;
        $this->image = $image;
        $already_logo = $services['o_page']->_site['Logo'] == $image->image_src ? 1 : 0;
        if($already_logo) {
            $this->title = '������������� ���� � �������� ���� ���� �� �����';
        }
        $class = 'form-control';
        $this->services = $services;
        $this->image = $image;
        $this->logo = Select::init()
            ->add('class', $class)
            ->add('data', array(0=>'��', 1=>'��'))
            ->add('value', $already_logo);
        $this->action = Hidden::init()->add('value', 'setImageAsSiteLogo');
        $this->submit = Submit::init()->add('class', 'btn btn-success btn-block')->add('value', '������');
        parent::__construct($post_data);
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function save()
    {
        $imgSrc = boolval($this->logo->value) ? $this->image->image_src : '';
        $this->services['gate']->site()->setImageAsLogo($imgSrc, $this->services['o_page']->_site['SitesID']);
    }
}
