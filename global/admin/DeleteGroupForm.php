<?php
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\MinLength;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class DeleteGroupForm extends Bootstrap
{
    private $services, $group;
    public $title = '������ �������� ��� �������?';

    public function __construct($services, $group,  $post_data)
    {
        $this->services = $services;

        $this->group = (object) $group; 

        $class = 'form-control';
        $this->services = $services;

        $this->action = Hidden::init()
            ->add('value', 'deleteGroup');

        $this->submit = Submit::init()
            ->add('class', 'btn btn-default btn-lg btn-block')
            ->add('value', '������');

        parent::__construct($post_data);
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function save()
    {
        $SiteID = $this->services['o_page']->_site['SitesID'];
        $id = $this->group->group_id;

        if($this->group->SiteID != $SiteID) {
            return false;
        }

        $this->services['gate']->site()->deleteGroup($id);
        $this->services['gate']->site()->deletePagesToGroupsByGroupId($id);
        return true;
    }
}
