<?php


/*
    For Western churches, use EASTER_WESTERN after the transition to Gregorian calendar
        (which occurred between 1582 and 1752 in most European countries) and
        EASTER_JULIAN before the transition. 
        
        For Orthodox churches (Greece, Russia, Serbia, Georgia, etc.), use EASTER_ORTHODOX after the transition and 
        EASTER_JULIAN before the transition. 
        For example, Denmark changed to the Gregorian calendar in 1700, so you should use EASTER_JULIAN before 1700 and
         EASTER_WESTERN since 1700.
*/

class EASTER_WESTERN{
}

class EASTER_ORTHODOX{
}

class EASTER_JULIAN{
}

class HolidayCalendar
{
    public static $year = 2015;
    public static $format = 'd-m-Y';
    public static function easter($year, $type)
    {
        $century = $year / 100;   
        $gregorian_shift = $century - $century / 4 -2;
        $x = 15; $y = 6;
        if($type instanceof EASTER_WESTERN){
            $coef = -1;
        } else {
            $coef = 0;
        }
        $x += (2 - (13+8 * $century) / 25 + $gregorian_shift) & ($type instanceof EASTER_WESTERN ? -1 : 0);
        $y += $gregorian_shift & ($type instanceof EASTER_WESTERN ? -1 : 0);

        $g = $year % 19;
        $d = ($g * 19 + $x) % 30;

        $e = (2 * ($year % 4) + 4 * $year - $d + $y) % 7;

        $day = $d + $e;
        $day -= ($e == 6) & (($d==29) || (($d==28) && ($g > 10))) ? 7: 0;
        // Convert Orthodox Easter to Grigorian calendar
        //  day += gregorian_shift & (type == EASTER_ORTHODOX ? -1 : 0);
        $day += $gregorian_shift & ($type instanceof EASTER_ORTHODOX ? -1 : 0);

        $is_may = ($day >= 40);
        $is_not_march = ($day >= 10);

        $month = 3 + $is_not_march + $is_may;
        $day += 22 - ($is_not_march ? 31: 0) - ($is_may ? 30 : 0);

        return new DateTime(sprintf("%s-%s-%s", $day, $month, $year));
    }

    public static function zagovezni()
    {
        return self::sub("P49D");
    }

    public static function lazarov_den()
    {
        return self::sub("P8D");
    }
    public static function spasovden()
    {
        return self::sub("P40D");
    }
    public static function petdesetnica()
    {
        return self::add("P50D");
    }

    protected static function sub($interval)
    {
        $date = self::easter(self::$year, new EASTER_ORTHODOX);
        $date->sub(new DateInterval($interval));
        return $date->format(self::$format);
    }

    protected static function add($interval)
    {
        $date = self::easter(self::$year, new EASTER_ORTHODOX);
        $date->add(new DateInterval($interval));
        return $date->format(self::$format);
    }

    public static function cvetnica()
    {
        $date = new DateTime(self::lazarov_den());
        $date->add(new DateInterval("P1D"));
        return $date->format(self::$format);
    }
}




$table = <<<HERE
<style>
td 
{
    height: 5%; 
    width:10%;
    text-align:center; 
}
</style>
<table border="1">
    <thead>
        <th> ��������� </th>
        <th> ���������� </th>
        <th> ��������</th>
        <th> ����������� �������� </th>
        <th> ����������� �������� </th>
        <th> ��������� </th>
        <th> ������������ </th>
    </thead>

HERE;
$year = date("Y", time());

$url = parse_str($_SERVER['QUERY_STRING'], $url_arr);

if (isset($_GET['year'])) {
    $year = $_GET['year'];
    $url_arr['year'] = $year;
}
$url_arr['n'] = $o_page->_page['n'];

$url =http_build_query($url_arr);

?>
<style>
.ui-datepicker-calendar {
   display: none;
}
</style>

<form method="GET" action="/page.php?<?=$url?>">
    <input type="hidden" name="n" value="<?=$o_page->_page['n'];?>" />
    <input type="text" id="datepicker" value="<?=$year?>" name="year">
    <button type="submit">�������</button>
</form>

<script>
/* Bulgarian initialisation for the jQuery UI date picker plugin. */
/* Written by Stoyan Kyosev (http://svest.org). */
jQuery(function($){
    $.datepicker.regional['bg'] = {
        closeText: '�������',
        prevText: '&#x3c;�����',
        nextText: '������&#x3e;',
        nextBigText: '&#x3e;&#x3e;',
        currentText: '����',
        monthNames: ['������','��������','����','�����','���','���',
        '���','������','���������','��������','�������','��������'],
        monthNamesShort: ['���','���','���','���','���','���',
        '���','���','���','���','���','���'],
        dayNames: ['������','����������','�������','�����','���������','�����','������'],
        dayNamesShort: ['���','���','���','���','���','���','���'],
        dayNamesMin: ['��','��','��','��','��','��','��'],
        weekHeader: 'Wk',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['bg']);
});
jQuery(function() {
    var start = new Date();
    start.setFullYear(start.getFullYear());
    var end = new Date();
    end.setFullYear(end.getFullYear()+20);
    jQuery('#datepicker').datepicker( {
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy',
        yearRange: start.getFullYear() + ':' + end.getFullYear(),
        onClose: function(dateText, inst) { 
            var year = jQuery("#ui-datepicker-div .ui-datepicker-year :selected").val();
            jQuery(this).datepicker('setDate', new Date(year, 1));
        }
    });
});
</script>

<?php

HolidayCalendar::$year = $year;
$table .= '<tr>';
$table .= '<td>'.HolidayCalendar::zagovezni().'</td>';
$table .= '<td>'.HolidayCalendar::lazarov_den().'</td>';
$table .= '<td>'.HolidayCalendar::cvetnica().'</td>';
$table .= '<td>'.HolidayCalendar::easter(HolidayCalendar::$year, new EASTER_WESTERN)->format(HolidayCalendar::$format).'</td>';
$table .= '<td>'.HolidayCalendar::easter(HolidayCalendar::$year, new EASTER_ORTHODOX)->format(HolidayCalendar::$format).'</td>';
$table .= '<td>'.HolidayCalendar::spasovden().'</td>';
$table .= '<td>'.HolidayCalendar::petdesetnica().'</td>';
$table .= '</tr>';
$table .= '</table>';
echo $table;

