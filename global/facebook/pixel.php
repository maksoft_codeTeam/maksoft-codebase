<?php
$facebookPixelCode = array_filter(array_map(
    function($setting){
         if($setting['conf_key'] == 'FB_CONVERSION_PIXEL_CODE'){
             return $setting['conf_value'];
         }
    },
    $o_page->get_sConfigurations($o_page->_site['SitesID'])
), function($var){ 
        return !is_null($var); 
    }
);

$code = "";
$key = isset($facebookPixelCode[key($facebookPixelCode)]) ? $facebookPixelCode[key($facebookPixelCode)] : null ;
if($key){
    $code = <<<HEREDOC
<!-- Facebook Pixel Code -->

<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');
// Insert Your Facebook Pixel ID below. 
fbq('init', '$key');
fbq('track', 'PageView');
</script>
<!-- Insert Your Facebook Pixel ID below. --> 
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=$key&amp;ev=PageView&amp;noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
HEREDOC;
}
return $code;

