<?php
$FACEBOOK_APP_ID = array_filter(array_map(
    function($setting){
         if($setting['conf_key'] == 'FB_COMMENTS_APP_ID'){
             return $setting['conf_value'];
         }
    },
    $o_page->get_sConfigurations($o_page->_site['SitesID'])
), function($var){ 
        return !is_null($var); 
    }
);


if( $key = $FACEBOOK_APP_ID[key($FACEBOOK_APP_ID)]){
    $script = <<<HEREDOC
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    if (response.status === 'connected') {
      testAPI();
    }
  }

    function httpGet(theUrl)
    {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
        xmlHttp.send( null );
        response = xmlHttp.responseText;
        return response;
    }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }
    
  function getProfilePicture(user_id){
    FB.api(
        "/" + user_id + "/picture",
        function (response) {
          if (response && !response.error) {
            /* handle the result */
          }
        }
    );
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '$key',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.8' // use graph api version 2.8
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    FB.api('/me', function(response) {
      getProfilePicture(response.id);
      $.notify("���������� ��, �� �� �������� " + response.name, "success");
      changeStatus(true);
    });
  }
</script>
HEREDOC;

    $button = '<div class="fb-login-button" style="margin: 0 auto;" data-max-rows="1" data-size="large" data-show-faces="true"></div>';
    return array($key, $script, $button);
}
return false;

