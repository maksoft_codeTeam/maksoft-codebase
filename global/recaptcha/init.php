<?php
require_once __DIR__."/../..//modules/vendor/autoload.php";
$withRecaptcha = False;

$reCaptchaKey = array_filter(array_map(
    function($setting){
        // if($setting['conf_key'] == 'reCAPTCHA_Secret_key'){
        //     return $setting['conf_value'];
        // }
         if($setting['conf_key'] == 'reCAPTCHA_Site_key'){
             return $setting['conf_value'];
         }
    },
    $o_page->get_sConfigurations($o_page->_site['SitesID'])
), function($var){ 
        return !is_null($var); 
    }
);
$reCaptchaSecretKey = array_filter(array_map(
    function($setting){
         if($setting['conf_key'] == 'reCAPTCHA_Secret_key'){
             return $setting['conf_value'];
         }
    },
    $o_page->get_sConfigurations($o_page->_site['SitesID'])
), function($var){ 
        return !is_null($var); 
    }
);
$secretKey = $reCaptchaSecretKey[key($reCaptchaSecretKey)];
$secret = $reCaptchaKey[key($reCaptchaKey)];
if(!empty($secretKey) and !empty($secret)){
    $withRecaptcha = true;
}
$lang = "bg";
$passedSpamCheck = False;
