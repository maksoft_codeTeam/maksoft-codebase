<?php
error_reporting(E_ALL);
ini_set("display_errors", true); 
use Symfony\Component\HttpFoundation\RedirectResponse;

require_once __DIR__ .'/../../loader.php';

require_once __DIR__.'/forms/SearchCourseForm.php';
require_once __DIR__.'/forms/MakeChoiceForm.php';

eval($o_page->get_sInfo('initPHP', 1054));

$action = $request->request->get('action'); 

$response = new RedirectResponse($o_page->get_pLink($o_page->_site['StartPage']));


$forms = array(
    SearchCourseForm::$_action => function() {
        return new SearchCourseForm($_POST);
    },
    MakeChoiceForm::$_action => function() {
        return new MakeChoiceForm($_POST);
    }
);

if(!is_numeric($request->request->get('n')) or !array_key_exists($action, $forms)) {
    $response->send();   
    return;
}

$form = $forms[$action](); 

if($form->is_valid()) {
    $form->save();
    $success_url = $o_page->get_pLink($request->request->get('n'), $request->request->get('SiteID'));
    $response->setTargetUrl($success_url);
}

$response->send();   
