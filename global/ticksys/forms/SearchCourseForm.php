<?php
use Jokuf\Form\Bootstrap;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Datetime;


class SearchCourseForm extends Bootstrap
{
    public static $_action = 'search_course';
    public function __construct($post_data) {
        $this->for_date = Datetime::init();
        $this->from_city = Integer::init();
        $this->to_city = Integer::init();
        $this->action = Hidden::init()->add('value', self::$_action);
        return parent::__construct($post_data);
    }

    public function validate_for_date($field) {
        $time = strtotime($field->value) > time() - 60 ? strtotime($field->value) : time();
        $this->for_date->value = new \DateTime(date('d-m-Y', $time));
    }

    public function validate_from_city($field) {
        return is_numeric($field->value) and $field->value > 0;
    }

    public function validate_to_city($field) {
        return is_numeric($field->value) and $field->value > 0;
    }

    public function is_valid() {
        try {
            parent::is_valid();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function save() {
        $_SESSION['ticksys'] = array();
        $_SESSION['ticksys'][0] = $this->clean_data();
    }
}
