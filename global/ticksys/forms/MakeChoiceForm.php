<?php
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Integer;

use Jokuf\Form\Bootstrap;


class MakeChoiceForm extends Bootstrap 
{
    public static $_action = 'choice_schedules';

    public function __construct($post_data) {
        $this->n = Hidden::init();
        $this->SiteID = Hidden::init();
        $this->course_id1 = Hidden::init();
        #$this->course_id2 = Hidden::init();
        $this->action = Hidden::init()
            ->add('value', self::$_action);

        $this->submit = Submit::init()
            ->add('class', 'btn large black')
            ->add('value', '���� �����');

        parent::__construct($post_data);
    }

    public function validate_course_id1($field) {
        return is_numeric($field->value) and $field->value > 0;
    }

    public function is_valid() {
        try {
            parent::is_valid();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
    public function validate_submit()
    {
        return isset($_SESSION['ticksys'][0]);
    }

    public function save() {
        $_SESSION['ticksys'][1] = $this->clean_data();
    }
}
