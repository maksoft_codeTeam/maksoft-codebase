 <link rel='stylesheet' href='/Templates/grouptravel/assets/css/jquery.seat-charts.css' type='text/css' media='all' />

<?php

$format_time = function($minutes) {
    $hours = floor($minutes / 60); // Get the number of whole hours
    $minutes = $minutes % 60;
    return sprintf("%s %s %s", $hours, $hours < 2 ? " ���": " ����", $minutes > 0 ? " � $minutes ������" : '');
};


define("COURSE_ID", $_SESSION['ticksys'][1]['course_id1']);
$schedule = $_SESSION['ticksys']['schedules'][COURSE_ID];

$busPlacesRequest = jokuf\ticksys\Api::L('get_bus_places', $schedule->BUS_TYPE);
$bus_places = $busPlacesRequest->fetch();

$freePlacesRequest = jokuf\ticksys\Api::L(
    'get_free_places',
    $schedule->FIRST_CITY_ID,
    $schedule->LAST_CITY_ID,
    $schedule->KURS_ID
);

$free_places = $freePlacesRequest->fetch();

$dirRequest = jokuf\ticksys\Api::L('get_dir_line', $schedule->DIR_ID);
$dirInfo = $dirRequest->fetch();

$seat_matrix = array();


foreach( range(1,$bus_places->ROW_COUNT) as $row ) {
    $seat_matrix[$row] = array();
    foreach(range(1, $bus_places->POS_COUNT) as $col){
        $seat_matrix[$row][$col] = false;
        $i++;
    }
}

foreach($bus_places->BUS_PLACE as $place) {
    $place->FREE = false;
    if(!isset($seat_matrix[$place->ROW])) {
        $seat_matrix[$place->ROW] = array();
    }
    $seat_matrix[$place->ROW][$place->POS] = $place; 
}

foreach($free_places->BUS_PLACE as $place) {
    $place->FREE = $place->TAKEN == 0 ? true  : false;
    $seat_matrix[$place->ROW][$place->POS] = $place; 
}

$o_page->debug_cli($schedule);
?>
<div class="entry-content">
    <div class="row">
        <div class='col-md-12'>
<div class="destination-info">
    <div class="row">
        <div class="col-md-6 first-col no-padmob">
            <div class="col-md-3 famob">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
            </div>
            <div class="col-md-9 no-padmob">
                �������� ��:
                <br>
                <span>
                    <?=iconv('utf8', 'cp1251', $cities[$schedule->FIRST_CITY_ID]->NAME)?>
                 </span>
                <br> ���������� �:
                <br>
                <span>
                    <?=iconv('utf8', 'cp1251', $cities[$schedule->LAST_CITY_ID]->NAME)?>
                </span>
                <br> ����������:
                <br>
                <span>
                478 ��.</span>
            </div>
        </div>
        <div class="col-md-6 no-padmob">
            <div class="col-md-3 famob">
                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
            </div>
            <div class="col-md-9 no-padmob">
                T�������:
                <br>
                <span> <?=$schedule->SCHEDULE_DATETIME?> </span>
                <br> ��������:
                <br>
                <span>
                    <?php
                        $schedule_date = date('d.m.Y', strtotime($schedule->SCHEDULE_DATETIME));
                        $arrives_format = 'H:i';
                        if($schedule->START_TIME > $schedule->END_TIME) {
                            $time = new DateTime($schedule_date);
                            $time->modify('+1 day');
                            $schedule_date = $time->format('d.m.Y');
                            $arrives_format = 'd.m.Y H:i';
                        }

                        echo date($arrives_format, strtotime($schedule_date .' '.$schedule->END_TIME));   
                    ?>
                </span>
                <br> �����������:
                <br>
                <span> 
                    <?php
                        $from_time = strtotime($schedule->SCHEDULE_DATETIME);
                        $to_time = strtotime($schedule_date .' '.$schedule->END_TIME);
                        echo $format_time(round(abs($to_time - $from_time) / 60,2));
                    ?> 
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <?php foreach($schedule->PRICES as $price): ?> 
        <div class="col-md-12 text-center">
            ���� �� <strong><?=iconv('utf8', 'cp1251', $price->PRICE_NAME);?></strong>:
            <br>
            <span class="price"> <?=$price->PRICE?> ��. </span>
        </div>
        <?php endforeach;?>
    </div>
</div>

        </div>
        <div class="col-md-6">
            <div class="seatCharts-container">
                <div class="front-indicator">������� </div>
                    <?php foreach($seat_matrix as $row) :?>
                        <?php 
                        $css_class =$place->FREE ? 'available' : 'unavailable';
                        ?>
                        <div class="seatCharts-row">
                            <?php foreach($row as $seat): ?>
                                <?php $number = iconv('utf8', 'cp1251', $seat->NUMBER); ?>
                                <?php if(!$seat): ?>
                                    <div class="seatCharts-cell seatCharts-space"></div>
                                    <?php continue;?>
                                <?endif;?>
                            <div data-row="<?php echo $seat->ROW;?>"
                                 data-pos="<?php echo $seat->POS;?>"
                                 data-number="<?=$number;?>"
                                 role="checkbox" aria-checked="false" 
                                 id="<?=$number;?>"
                                 focusable="true" class="seat seatCharts-seat seatCharts-cell economy-class <?=$css_class?>"><?=$number;?>
                            </div>
                            <?php endforeach;?>
                        </div>
                    <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
