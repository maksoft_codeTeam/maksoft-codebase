<?php
use Symfony\Component\HttpFoundation\RedirectResponse;

require_once __DIR__ . '/index.php';

$form = new MakeChoiceForm($_POST);
$form->add_attr('action', '/global/ticksys/callback.php');

$form->n->value = $o_page->_page['n'];
$form->SiteID->value = $o_page->_page['SiteID'];

if(!isset($_SESSION['ticksys']) or !isset($_SESSION['ticksys'][0])) {
    $response->send();
    return false;
}


$for_date = $_SESSION['ticksys'][0]['for_date'];
$from_city = $_SESSION['ticksys'][0]['from_city'];
$to_city = $_SESSION['ticksys'][0]['to_city'];


if(!$for_date or !$from_city or !$to_city): ?>
    <div class="alert alert-danger margin-bottom-30"><!-- DANGER -->
        <strong>Oh snap!</strong> Change a few things up and try submitting again.
    </div>
    <?php return; ?>
<?php endif; ?>

<?php 
$first_date = $for_date->format('d.m.Y');
$second_date = date('d.m.Y', strtotime($first_date . ' +1 days')); 

try {

    $citiesRequest = jokuf\ticksys\Api::L('get_cities');
    $tmp_cities = $citiesRequest->fetch();
    $cities = array();
    foreach($tmp_cities->CITY as $city) {
        $cities[$city->ID] = $city;
    }
    if(isset($_SESSION['ticksys'][1])) {
        require_once __DIR__ .'/reserve_ticket.php';
        return;
    }

    $requestPromise = jokuf\ticksys\Api::L('get_schedules', $first_date, $from_city, $to_city);
    $schedules = $requestPromise->fetch();

    if(empty($schedules->SCHEDULE)) {
        $requestPromise = jokuf\ticksys\Api::L('get_schedules', $second_date, $from_city, $to_city);
        $schedules = $requestPromise->fetch();
    }

    $_SESSION['ticksys']['schedules'] = array();

} catch (jokuf\ticksys\Exception\InvalidResponse $e) { ?>
    <div class="alert alert-danger margin-bottom-30"><!-- DANGER -->
        <strong><?=$e->getMessage()?></strong>
    </div>
<?php } ?>

<h4 id='schedule-headline'></h4>
<div class="row process-wizard process-wizard-primary">

    <div class="col-xs-3 process-wizard-step complete">
        <div class="text-center process-wizard-stepnum">Step 1</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="process-wizard-dot"></a>
        <div class="process-wizard-info text-center">Register User via control panel.</div>
    </div>

    <div class="col-xs-3 process-wizard-step complete"><!-- complete -->
        <div class="text-center process-wizard-stepnum">Step 2</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="process-wizard-dot"></a>
        <div class="process-wizard-info text-center">Process Payment and fill out all required fields.</div>
    </div>

    <div class="col-xs-3 process-wizard-step active"><!-- complete -->
        <div class="text-center process-wizard-stepnum">Step 3</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="process-wizard-dot"></a>
        <div class="process-wizard-info text-center">Confirm Data entered in step 2.</div>
    </div>

    <div class="col-xs-3 process-wizard-step disabled"><!-- active -->
        <div class="text-center process-wizard-stepnum">Step 4</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="process-wizard-dot"></a>
        <div class="process-wizard-info text-center">Download product after receiving confirmation email.</div>
    </div>

</div>


<table class='row'>
        <th>����</th>
        <th>�����������</th>
        <th>T������� �� �����</th>
        <th>������</th>
        <th>������</th>
    <?php foreach($schedules['SCHEDULE'] as $schedule): ?>
    <tr>
        <?php $_SESSION['ticksys']['schedules'][$schedule->KURS_ID] = $schedule; ?>
        <?php $form->course_id1->value = $schedule->KURS_ID; ?>
        <td><?=date('d.m.Y', strtotime($schedule->SCHEDULE_DATETIME))?></td>
        <td><?=helpers\__($schedule->NAME)?></td>
        <td><?=$schedule->START_TIME?></td>
        <td> <?=$form?> </td>
        <td><?=$schedule->SEKTOR?></td>
    </tr>
    <?php endforeach; ?>
</table>

<script>

</script>
