<?php
use Symfony\Component\HttpFoundation\RedirectResponse;

$response = new RedirectResponse($o_page->get_pLink($o_page->_site['StartPage']));

require_once __DIR__ .'/../../loader.php';
require_once __DIR__.'/forms/MakeChoiceForm.php';
require_once __DIR__.'/helpers.php';
