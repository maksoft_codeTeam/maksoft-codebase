<?php
require_once __DIR__.'/base.php';
$vehicles = vehicles_list($em, COMPANY_ID);
$load_form('AddSchedule');
$load_form('DeleteSchedule');
$load_form('AddVehicleToSchedule');
$load_form('RemoveVehicleFromSchedule');
require_once __DIR__.'/templates/show_directions.php';
$add_vehicle = new AddVehicleToSchedule($em, $_POST);
$remove_vehicle = new RemoveVehicleFromSchedule($em, $_POST);
if($add_vehicle->is_valid()){ 
    try{
        $add_vehicle->save();
        echo '<script> toastr.success(\'������� ��������� �������� ��������\');</scirpt>';       
    } catch(\Exception $e) {
    }
}
if($remove_vehicle->is_valid()){ 
    try{
        $remove_vehicle->save();
        echo '<script> toastr.success(\'������� ��������� �������� ��������\');</scirpt>';       
    } catch(\Exception $e) {
    }
}
?>

<div class="row">
    <?php if(!isset($_GET['direction_id']) or !in_array($_GET['direction_id'], $direction_ids)): ?>
        <div class="col-md-12 text-center">
            <h2> �� ��� ������� ����������� </h2>
        </div>
        <?php return;?>
    <?php endif;?>
    <?php 
    $add_schedule= new AddSchedule($em , $_POST);
    $add_schedule->direction_id->value=$_GET['direction_id'];
    $forms = array();
    $delete_schedule = new DeleteSchedule($em, $_POST);
    if($delete_schedule->is_valid()) {
        $delete_schedule->save();
    }
    if($_SERVER['REQUEST_METHOD'] === 'POST' and isset($_POST[$add_schedule->action->name]) and $_POST[$add_schedule->action->name] == $add_schedule->action->value) {
        try{
            $add_schedule->is_valid();
            $add_schedule->save();
        } catch(\Exception $e) {
            echo '<script> toastr.error(\''.addslashes($e->getMessage()).'\')</scirpt>';       
        }
    }

    ?>
    <div class="col-md-12">
        <div class='row'>
            <div class='col-md-3'>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addSchedule">������ ����</button>
            </div>
            <div class='col-md-3'>
            </div>
            <div class='col-md-3'>
            </div>
            <div class='col-md-3'>
            </div>
        </div>
    </div>
    <!-- LIST COURSES -->
    <div class="col-md-12">
        <h3> ������ ������� �� �����������:</h3> 
        <table class='table table-responsive'>
            <thead>
                <th>N</th> 
                <th>����</th> 
                <th>�������� ��������</th> 
                <th>������</th> 
                <th>�������</th>
                <th>��������</th>
            </thead>
    <?php $direction = $em->getRepository('Bus\models\Direction')->findOneById($_GET['direction_id']); ?>
            <tbody>
            <?php foreach($direction->getSchedules() as $schedule): ?>
                <tr>
                    <td> <?=$schedule->getId();?> </td>
                    <td> <?=$schedule->getStartDate()->format('d-m-Y H:i');?> </td>
                    <td>
                        <ul>
                        <?php foreach($schedule->getVehicles() as $vehicle): ?>
                        <li>
                             <?=$vehicle->getMake()?> - <?=$vehicle->getModel()?> [<?=$vehicle->getLicensePlate()?>] [<?=count($vehicle->getSeats());?> ������]
                            <form method='POST'>
<?php
    $remove_vehicle->vehicle_id->value = $vehicle->getId();
    echo $remove_vehicle->vehicle_id;
    $remove_vehicle->schedule_id->value = $schedule->getId();
    echo $remove_vehicle->schedule_id;
    echo $remove_vehicle->action;
?>
    <button type="submit"><i class="fa fa-trash" aria-hidden="true"></i> </button>
                            </form>
                        </li>      
                        <?php endforeach;?>
                        </ul>
                    </td>
                    <td> 
                <?php $tickets = $schedule->getReservations()->filter(function($ticket) {
                    return $ticket->getSold() == true;
                }); ?>
                <?php if(count($tickets)):?>
                    <?=count($tickets)?> <?php echo count($tickets) < 2 ? '�����': '������';?> 
                <?php else: ?>
                    ���� ��������� ������
                    </td>
                <?php endif;?>
                    <td>
                    <?php
                    $now = new DateTime();   
                    echo  $schedule->getStartDate() >= $now ? '�������' : '���������';
                    ?>
                    </td>
                    <td>
                    <!-- DELETE SCHEDULE -->
                    <?php 
                        $delete_schedule->schedule_id->value = $schedule->getId();        
                        echo $delete_schedule;
                        $add_vehicle->schedule_id->value = $schedule->getId();
                    ?>
                        <button onclick="show_hide_el(this)" class="add_vehicle_link"> ������ �������� ��������</button>
                        <div class="hidden">
                        <?=$add_vehicle?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- END LIST COURSES -->
</div>

<!-- MODALS -->
<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="addSchedule" tabindex="-1" role="dialog" aria-labelledby="addScheduleLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addScheduleLabel"><?=$add_schedule->title?></h4>
      </div>
      <div class="modal-body">
        <form method="POST">
        <div class="form-group">
            <label for="<?=$add_schedule->startDate->name?>"> <?=$add_schedule->startDate->label?></label>
            <?=$add_schedule->startDate?>
        </div>
        <?php
            echo $add_schedule->direction_id;
            echo $add_schedule->action;
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
        <button type="submit" class="btn btn-primary">������</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- END MODALS -->

<script>
            function show_hide_el(el) { 
                if(jQuery(el).next().is(':visible')) {
                    jQuery(el).next().removeClass('show').addClass('hidden');
                } else {
                    jQuery(el).next().removeClass('hidden').addClass('show');
                }
            }
jQuery(document).ready(function(){
    jQuery( "#checkInDate" ).datepicker({dateFormat:"mm/dd/yy", minDate: 0}).datepicker("setDate",new Date());
});
</script>
