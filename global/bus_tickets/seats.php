<?php
require_once __DIR__.'/base.php';
$load_form('AddSeat');
$load_form('AddVehicle');
$load_form('DeleteVehicle');
$add_vehicle = new AddVehicle($em, $_POST, COMPANY_ID);
$vehicle_ids = array();
$delete_vehicle = new DeleteVehicle($em, $_POST);
if($delete_vehicle->is_valid()) {
    $delete_vehicle->save();
}
$catch = function($callable) {
    try {
        return $callable();
    } catch (\Exception $e) {
    }
};
if($add_vehicle->is_valid()) {
    try { 
        $add_vehicle->save();
    } catch (\Exception $e) {
        echo '<script> toastr.ERROR(\''.$e->getMessage().'\');</script>';
    }
}

$vehicles = vehicles_list($em, COMPANY_ID);



$add_seat = new AddSeat($em);
$seat_matrix = array();
$i = 1;
foreach(range(1,15) as $row) {
    $seat_matrix[$row] = array();
    foreach(range(1,5) as $col){
        $seat_matrix[$row][$col] = array('available' => 'taken', 'number' => '', 'row'=>$row, 'col' => $col, 'blocked' => false);
        $i++;
    }
}
?>

<div class="row">
    <div class='col-md-12'>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addVehicle">
            ���� �������� ��������
        </button>
        <p></p>
    </div>
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <th>N:</th>
                <th>�����</th>
                <th>�����</th>
                <th>���. �����</th>
                <th>��������</th>
            </thead>
            <tbody>
        <?php foreach($vehicles as $vehicle): ?>
            <?php $vehicle_ids[] = $vehicle->getId();?>
        <tr>
        <td><?=$vehicle->getId(); ?></td>
        <td><?=$vehicle->getMake(); ?></td>
        <td><?=$vehicle->getModel(); ?></td>
        <td><?=$vehicle->getLicensePlate(); ?></td>
        <td>
        <div class='btn-group'>
            <?php
                $delete_vehicle->vehicle_id->value = $vehicle->getId();
                echo $delete_vehicle;
            ?>
        <form method="GET">
            <input type="hidden" name='n' value="<?=$o_page->_page['n']?>"> 
            <input type="hidden" name='SiteID' value="<?=$o_page->_site['SitesID']?>"> 
            <input type="hidden" name='vehicle_id' value="<?=$vehicle->getId()?>"> 
            <button type='submit'>�������</button>
        </form>
        </div>
        </td>
        <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<!-- Add vehicle Modal -->
<div class="modal fade" id="addVehicle" tabindex="-1" role="dialog" aria-labelledby="addVehicleLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addVehicleLabel"><?=$add_vehicle->title?> </h4>
      </div>
      <div class="modal-body">
        <form method="POST">
        <?php foreach($add_vehicle as $field): ?>
            <?php if($field instanceof Jokuf\Form\Field\Submit): continue; endif;?>
            <div class="form-group">
            <?php if($field->label):?>
            <label for="<?=$field->name?>" class=""><?=$field->label?></label>
            <?php endif;?>
                <?=$field?>
            </div>
        <?php endforeach;?>
      </div>
      <div class="modal-footer">
        <?=$add_vehicle->submit?>
        </form>
      </div>
    </div>
  </div>
</div>

<?php if(!isset($_GET['vehicle_id']) or !in_array($_GET['vehicle_id'], $vehicle_ids)):
    echo '<h2>�� ��� ������� �������� ��������</h2>';
    return;
endif; ?>
<?php
//TODO: move to reserved_seats
$is_free = function ($seat_id, $schedule_id, $vehicle_id) use($em){
    $res = $em->createQueryBuilder()->select('t')
        ->from('Bus\models\Ticket', 't')
        ->where('t.schedule = :schedule_id')
        ->andWhere('t.seat = :seat_id')
        ->andWhere('t.vehicle = :vehicle_id')
        ->setParameter('schedule_id', $schedule_id)
        ->setParameter('seat_id', $seat_id)
        ->setParameter('vehicle_id', $vehicle_id)
        ->getQuery()
        ->setMaxResults(1)
        ->getOneOrNullResult();
    return $res ? false : true;

};

$vehicle = $em->getRepository('Bus\models\Vehicle')->findOneById($_GET['vehicle_id']);

foreach($vehicle->getSeats() as $seat) {
    $row = $seat->getRow();
    $col = $seat->getCol();
    $number = $seat->getNumber();
    if(!array_key_exists($row, $seat_matrix)) {
        $seat_matrix[$row] = array();
    }

    $seat_matrix[$row][$col] = array(
        'id' => $seat->getId(),
        'number'=> $number,
        'available'=> 'true',
        'row' => $row,
        'col' => $col,
        'edit' => true,
        'blocked' => $seat->isBlocked(),
    );
}
?>

<?=$add_seat->add_attr('id', 'addSeat');?>
<h1><?=$add_seat->title;?> </h1>
<div class="row">
    <div class="col-md-6">
        <?=$add_seat?>
    </div>
    <div class="col-md-6">
        <h2>������ �� ������ ��������</h2>
        <?php $i=1;?>
        <?php foreach($seat_matrix as $row): ?>
            <div class="row" style='margin-bottom: 5%;margin-left:-1%;margin-right:-1%'>
            <?php foreach($row as $seat): ?>
                <div class="col-xs-2" style='width:20%'>
                    <div id="seat-<?=$seat['col']?><?=$seat['row']?>"class="seat <?php echo !$seat['blocked'] ? 'free' : 'taken';?> text-center"
                         style="width:50px;" 
                         data-seat="<?php echo $seat['id'];?>"
                         data-num="<?=$seat['number'] ? $seat['number'] : $i?>"
                         data-col="<?=$seat['col']?>"
                         data-row="<?=$seat['row']?>"
                    >
                    <strong><?=$seat['number']?></strong>
                    </div> 
                </div>
                <?php $i++;?>
            <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<script>
jQuery(".seat").on('click', function(e){
    var seat = jQuery(this);
    //seat.removeClass('free');
    //seat.addClass('selected');
    seat.attr('class', 'seat selected');
    jQuery("input[name=number]").val(seat.data('num'));
    jQuery("input[name=col]").val(seat.data('row'));
    jQuery("input[name=row]").val(seat.data('col'));
    jQuery("#blocked").val(seat.data('blocked'));
});
jQuery("#<?=$add_seat->get_id()?>").submit(function( event ) {
  event.preventDefault();
    var row = jQuery("input[name=col]").val();
    var col = jQuery("input[name=row]").val();
    var blocked = jQuery("select[name=blocked]").val();
    seat = jQuery('#seat-'+col + row);
    jQuery.ajax({
        method: "POST",
        url: "/global/bus_tickets/api.php?cmd=create_seat",
        data: {
            row: seat.data('row'),
            col: seat.data('col'),
            number: jQuery("input[name=number]").val(),
            vehicle: "<?=$_GET['vehicle_id']?>",
            action: 'change_seat',
            blocked: blocked
        },
        success: function(response) {
            seat.data('num', response.number);
            seat.html('<strong>'+response.number+'</strong>');
            toastr.success(response.number);
            cls = 'free';
            if (response.blocked) {
                cls = 'taken';
            }
            seat.attr('class', 'seat ' + cls);
        },
        error: function(response) {
            toastr.error('������')
        } 
    });
});
function save() 
{
    console.log('save');
}
</script>
