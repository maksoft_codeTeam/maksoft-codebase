<?php
require_once __DIR__.'/base.php';

if($o_page->_user['AccessLevel'] < 2) {
    return;
}

if($_SERVER['REQUEST_METHOD']==='POST' and isset($_POST['action']) && $_POST['action'] == 'delete_ticket' and is_numeric($_POST['ticket_id'])) {
    try {
        $ticket = $em->getRepository('Bus\models\Ticket')->findOneById($_POST['ticket_id']);
        $em->remove($ticket);
        $em->flush();
    } catch (\Exception $e) {
    }
}

$tickets = $em->getRepository('Bus\models\Ticket')->findAll();


?>

<div class="row">
    <div clas="col-md-12">
    <h3> ��������� ������:</h3> 
    <table class='table table-responsive'>
        <thead>
            <th>N</th> 
            <th>�����</th> 
            <th>O�</th> 
            <th>��</th> 
            <th>����</th>
            <th>�������</th>
            <th>�������</th>
            <th>��������</th>
        </thead>
        <tbody>
            <?php foreach($tickets as $ticket): ?>
<?php
$direction = $ticket->getSchedule()->getDirection();
$route = $ticket->getRoute();
$schedule = $ticket->getSchedule();
$course_info = $get_course_info($direction, $route);
$time_to_course = $course_info['time']['to_course'];
$travel_time = $course_info['time']['duration'];
$deparature = $schedule->getStartDate()->modify("+$time_to_course minutes");
$deparature_date = $deparature->format('������ � H:i ��  d-m-Y');
$arrival = clone $deparature;
$arrival->add(new DateInterval('PT' . $travel_time  . 'M'));
$arrival_date = $arrival->format('�������� � H:i �� d-m-Y');
?>
            <tr>
                <td> <?=$ticket->getId();?></td>
                <td> <?=$ticket->getSchedule()->getDirection()->getName();?></td>
                <td> <?=$ticket->getRoute()->getStartStation()->getName();?></td>
                <td> <?=$ticket->getRoute()->getEndStation()->getName();?></td>
                <td> <?=$deparature_date?> <?=$arrival_date?> </td>
                <td>
                     <?=$ticket->getPayer()->getFirstName();?>
                     <?=$ticket->getPayer()->getMiddleName();?>
                     <?=$ticket->getPayer()->getLastName();?>
                    <a href='tel:<?=$ticket->getPayer()->getPhone();?>'><?=$ticket->getPayer()->getPhone();?></a>
                    <a href='mailto:<?=$ticket->getpayer()->getemail();?>'> <?=$ticket->getPassenger()->getPhone();?> </a>
                </td>
                <td>
                     <?=$ticket->getPassenger()->getFirstName();?>
                     <?=$ticket->getPassenger()->getMiddleName();?>
                     <?=$ticket->getPassenger()->getLastName();?>
                    <a href='tel:<?=$ticket->getPayer()->getPhone();?>'><?=$ticket->getPayer()->getPhone();?></a>
                    <a href='mailto:<?=$ticket->getpayer()->getemail();?>'> <?=$ticket->getPassenger()->getPhone();?> </a>
                </td>
                <td>
                <form onSubmit="confirm('������� �� ��� �� ������ �� �������� ���� �����? ���������� � ����������.');" method="POST">
                    <input type="hidden" name="ticket_id" value="<?=$ticket->getId();?>">
                    <input type="hidden" name='action' value='delete_ticket'>
                    <button type="submit" value="SUBMIT" id="submit" onclick="return confirm('������� �� ���, �� ������ �� �������� ���� �����? ���������� � ����������.')" >������</button>
                </form>
                <a class='btn btn-info' target="_blank" href="/Templates/grouptravel/templates/busticket/printable_busticket.php?s=<?=$schedule->getId()?>&r=<?=$route->getId()?>&p=<?=$ticket->getPayer()->getId()?>">���������</a>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    </div>
</div>
