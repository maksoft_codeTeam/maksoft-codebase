<?php
require_once __DIR__.'/../../modules/bootstrap.php';
require_once __DIR__.'/repository.php';

use Symfony\Component\HttpFoundation\JsonResponse;

$mail = new \PHPMailer;
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'server.maksoft.net';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->CharSet = "UTF-8";
$mail->Username = 'cc@maksoft.bg';                 // SMTP username
$mail->Password = 'maksoft@cc';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to
$mail->isHTML(true);                                  // Set email format to HTML
/*
 *
$mail->setFrom($from_email, $subject);
$mail->addAddress($email, $this->to_utf8($name));     // Add a recipient
$mail->addBcc($from_email, $subject);
$mail->addBcc('cc@maksoft.bg', $subject);
$mail->Subject = $subject;
$mail->Body    = $subject."<br>".$content;
if(!$mail->send()) {
    throw new \Exception($this->mailer->ErrorInfo);
}

*/

$load_form = function($form) {
    $file = __DIR__.'/forms/'.$form.'.php';
    if(file_exists($file)) {
        include $file;
        return true;
    }
    trigger_error ("Файлът не е намерен");
};

$add_seat = function ($reqeust) use ($load_form, $em) {
    $load_form('AddSeat');
    $addForm =new AddSeat($em , $reqeust->request->all());
    $addForm->is_valid();
    return $addForm->save();
};


$seats_map = function($vehicle_id) use($em) {
    $vehicle = $em->getRepository('\Bus\models\Vehicle')->findOneById($vehicle_id);

    $seats = $vehicle->getSeats();
    $tmp_map = array();
    foreach(range(1,15) as $row) {
        $tmp_map[$row] = array();
        foreach(range(1,5) as $col){
            $tmp_map[$row][$col] = "_";
            $i++;
        }
    }

    $i =0;
    foreach($vehicle->getSeats() as $seat) {
        $row = $seat->getRow();
        $col = $seat->getCol();
        $number = $seat->getNumber();
        if(!array_key_exists($row, $tmp_map)) {
            $tmp_map[$row] = array();
        }

        $status = 'e';
        if($seat->isBlocked() or !is_free($seat)) {
            $status = 'f';
        }

        $tmp_map[$row][$col] = 'e';     
        $i = $row;
    }

    return array_map(function($row) {
        return implode('', $row);
    }, array_slice($tmp_map, 0, $i));
};

$seats_map_api = function($request) use ($seats_map) {
    $vehicle_id = $request->query->get('vehicle');
    return $seats_map($vehicle_id);
};

$buy_ticket = function($request) use ($load_form, $em) {
    $load_form('AddPassenger');
    $load_form('AddPayer');
    
    $payerForm = new AddPayer($em, $request->request->all());
    $payerForm->is_valid();
    $payer = $addForm->save();
    
    if(!$request->request->get('passenger') or !is_array($request->request->get('passenger'))) {
        throw new \Exception('Malformed request');
    }

    foreach($request->request->get('passenger') as $passenger) {
        $passengerForm = new AddPassenger($em, $passenger, $payer);
        $passengerForm->is_valid();
        $passengerForm->save();
    }
};


$render_form = function($request) use ($load_form, $em) {
    if(!$request->query->get('formName')) {
        throw new \Exception('Specify form first');
    }
    $formName = $request->query->get('formName');
    if(!$load_form($formName)) {
        throw new \Exception('unknown form');
    }
    $form = new $formName($em, $request->request->all());
    $form->is_valid();
    #$form->save();
    return (string) $form;
};
