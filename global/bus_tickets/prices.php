<?php
require_once __DIR__.'/base.php';

$load_form('AddPrice');
$load_form('DeletePrice');
$addPriceForm = new AddPrice($em, $_POST);
$deletePriceForm = new DeletePrice($em, $_POST);

if($_SERVER['REQUEST_METHOD'] === 'POST' and $_POST['action'] == $addPriceForm->action->value) {
#if($addPriceForm->is_valid()) {
    $addPriceForm->is_valid();
    $addPriceForm->save();
}

if($deletePriceForm->is_valid()) {
    $deletePriceForm->save();
}

$ticketTypes = $em->getRepository('\Bus\models\TicketType')->findAll();
$routes = $em->getRepository('\Bus\models\Route')->findAll();

?>

<div class='row'>
    <div class='col-md-6'>
            <div class='row'>
                <div class='col-md-1'>N</div>
                <div class='col-md-5'>���</div>
                <div class='col-md-6'>��������</div>
            </div>
        <?php foreach($ticketTypes as $ticketType): ?>
            <div class='row'>
                <div class='col-md-1'> <?=$ticketType->getId()?></div>
                <div class='col-md-5'> <?=$ticketType->getName()?></div>
                <div class='col-md-6'> <?=$ticketType->getDescription()?></div>
            </div>
        <?php endforeach;?>
    </div>
</div>
<div class='row'>
    <div class='col-md-12'>
        <table class='table'>
            <th> # </th>
            <th> �� - �� </th>
            <th> ���� </th>
            <th> <i class="fa fa-keyboard-o" aria-hidden="true"></i> </th>
        <?php $i=1; ?>
        <?php foreach($routes as $route): ?>
            <tr>
                <td><?=$i?></td>
                <td><?=$route->getStartStation()->getName();?> - <?=$route->getEndStation()->getName();?> </td>
                <td>
                    <ol>
                    <?php foreach($route->getPrices() as $routePrice):?>
                        <li>
                        <?=$routePrice->getType()->getName();?> [ <?=number_format($routePrice->getPrice(), 2);?> BGN ]
                            <?=$deletePriceForm->start()?>
                            <input type='hidden' name='routePriceId' value='<?=$routePrice->getId()?>'>
                            <?=$deletePriceForm->action?>
                            <button type='submit'><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                            <?=$deletePriceForm->end()?>
                        </li>
                    <?php endforeach;?>
                    </ol>
                </td>
                <td> <button type='submit' data-toggle="modal" data-target="#addPrice" data-routeId='<?=$route->getId()?>'> <i class="fa fa-plus" aria-hidden="true"></i></button> </td>
            </tr>
            <?php $i++;?>
        <?php endforeach;?>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addPrice" tabindex="-1" role="dialog" aria-labelledby="addPriceLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addPriceLabel">������ ����</h4>
      </div>
      <div class="modal-body">
        <?=$addPriceForm->start()?>
        <?php foreach($addPriceForm as $field): ?>
            <?php if($field instanceof Jokuf\Form\Field\Submit) { continue; } ?>
            <div class="form-group">
            <?php if($field->label): ?>
            <label for="<?=$field->name?>" class="">
            <?php endif;?>
            <?=$field->label?></label><?=$field?>
            <?php foreach($field->get_errors() as $error) {
                echo $error;
            }?>
          </div>
        <?php endforeach;?>
        <div id='extraField'> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        <?=$addPriceForm->end()?>
      </div>
    </div>
  </div>
</div>

<script>
jQuery('#addPrice').on('show.bs.modal', function (event) {
  var button = jQuery(event.relatedTarget) // Button that triggered the modal
  var routeId = button.data('routeid') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = jQuery(this)
  jQuery('#extraField').html('<input type="hidden" name="routeId" value="'+routeId+'">');
  modal.find('.modal-title').text('New message to ' + routeId)
})
</script>
