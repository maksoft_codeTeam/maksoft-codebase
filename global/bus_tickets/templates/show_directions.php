<?php
$directions = directions_list($em, COMPANY_ID);
$direction_ids = array();
if(!isset($dir_title)) {
    $dir_title = '������ �����������';
}

if(!isset($dir_actions)) {
    $dir_actions = true;
}
?>
    <h2> <?=$dir_title?> </h2>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-1">
                <b>N:</b>
            </div>  
            <div class="col-md-3">
                <b>���:</b>
            </div>  
            <div class="col-md-3">
                <b>������� ����</b>
            </div>  
            <div class="col-md-3">
                <b>������ ����</b>
            </div>  
        <?php if($dir_actions):?>
            <div class="col-md-2">
                <b>��������</b>
            </div>  
        <?php endif;?>
        </div>
    </div>
    <hr>
    <?php foreach($directions as $direction): ?>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-1">
                <?=$direction->getId();?>
                <?php $direction_ids[] = $direction->getId();?>
            </div>
            <div class="col-md-3">
                <?=$direction->getName();?>
            </div>
            <div class="col-md-3">
                <?=$direction->getStartStation()->getName();?>
            </div>
            <div class="col-md-3">
                <?=$direction->getEndStation()->getName();?>
            </div>
        <?php if($dir_actions):?>
            <div class="col-md-2">
                <form method='get'>
                <input type='hidden' value='<?=$o_page->_page['n']?>' name='n'>
                <input type='hidden' value='<?=$o_page->_site['SitesID']?>' name='SiteID'>
                <input type='hidden' value='<?=$direction->getId()?>' name='direction_id'>
                <input type='submit' value='������' class='btn btn-info'>
                </form>
            </div>
        <?php endif;?>
        </div>
    </div>
    <?php endforeach;?>
</div>
