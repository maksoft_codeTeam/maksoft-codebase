<!DOCTYPE html>
<html lang="en" style="font-family: sans-serif; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-size: 100%; box-sizing: border-box; color: #000 !important; box-shadow: none !important; text-shadow: none !important; height: 100%;">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>������� ����������</title>
    
  </head>
  <body style="font-family: 'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif; font-weight: 400; font-size: 100%; box-sizing: border-box; line-height: 1.5; color: #000 !important; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; box-shadow: none !important; text-shadow: none !important; height: 100%; background-color: #fefefe; margin: 0; padding: 0;" bgcolor="#fefefe">
<style type="text/css">
.button.dropdown::after { position: relative !important; }
a:active { outline: 0 !important; }
a:hover { outline: 0 !important; }
:after { box-sizing: inherit !important; }
:before { box-sizing: inherit !important; }
.row::after { content: ' ' !important; display: table !important; }
.row::before { content: ' ' !important; display: table !important; }
.row::after { clear: both !important; }
.breadcrumbs::after { clear: both !important; }
.button-group::after { clear: both !important; }
.clearfix::after { clear: both !important; }
.off-canvas-wrapper-inner::after { clear: both !important; }
.pagination::after { clear: both !important; }
.tabs::after { clear: both !important; }
.title-bar::after { clear: both !important; }
.top-bar::after { clear: both !important; }
a:focus { color: #1585cf !important; }
a:hover { color: #1585cf !important; }
cite:before { content: '\2014 \0020' !important; }
a:visited { text-decoration: underline !important; }
.ir a:after { content: '' !important; }
a[href^='javascript:']:after { content: '' !important; }
a[href^='#']:after { content: '' !important; }
abbr[title]:after { content: " (" attr(title) ")" !important; }
@page { margin: .5cm !important; }
.button:focus { background: #1583cc !important; color: #fff !important; }
.button:hover { background: #1583cc !important; color: #fff !important; }
.button.primary:focus { background: #147cc0 !important; color: #fff !important; }
.button.primary:hover { background: #147cc0 !important; color: #fff !important; }
.button.secondary:focus { background: #5f5f5f !important; color: #fff !important; }
.button.secondary:hover { background: #5f5f5f !important; color: #fff !important; }
.button.success:focus { background: #22bb5b !important; color: #fff !important; }
.button.success:hover { background: #22bb5b !important; color: #fff !important; }
.button.alert:focus { background: #da3116 !important; color: #fff !important; }
.button.alert:hover { background: #da3116 !important; color: #fff !important; }
.button.warning:focus { background: #cc8b00 !important; color: #fff !important; }
.button.warning:hover { background: #cc8b00 !important; color: #fff !important; }
.button.hollow:focus { background: 0 0 !important; }
.button.hollow:hover { background: 0 0 !important; }
.button.hollow:focus { border-color: #0c4d78 !important; color: #0c4d78 !important; }
.button.hollow:hover { border-color: #0c4d78 !important; color: #0c4d78 !important; }
.button.hollow.primary:focus { border-color: #0c4d78 !important; color: #0c4d78 !important; }
.button.hollow.primary:hover { border-color: #0c4d78 !important; color: #0c4d78 !important; }
.button.hollow.secondary:focus { border-color: #3c3c3c !important; color: #3c3c3c !important; }
.button.hollow.secondary:hover { border-color: #3c3c3c !important; color: #3c3c3c !important; }
.button.hollow.success:focus { border-color: #157539 !important; color: #157539 !important; }
.button.hollow.success:hover { border-color: #157539 !important; color: #157539 !important; }
.button.hollow.alert:focus { border-color: #881f0e !important; color: #881f0e !important; }
.button.hollow.alert:hover { border-color: #881f0e !important; color: #881f0e !important; }
.button.hollow.warning:focus { border-color: #805700 !important; color: #805700 !important; }
.button.hollow.warning:hover { border-color: #805700 !important; color: #805700 !important; }
.button.dropdown::after { content: '' !important; width: 0 !important; height: 0 !important; border: .4em inset !important; border-color: #fefefe transparent transparent !important; border-top-style: solid !important; top: .4em !important; float: right !important; margin-left: 1em !important; display: inline-block !important; }
.button.arrow-only::after { margin-left: 0 !important; float: none !important; top: .2em !important; }
[type=text]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=password]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=date]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=datetime]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=datetime-local]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=month]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=week]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=email]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=tel]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=time]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=url]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=color]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=number]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
[type=search]:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
textarea:focus { border: 1px solid #8a8a8a !important; background: #fefefe !important; outline: 0 !important; box-shadow: 0 0 5px #cacaca !important; transition: box-shadow .5s,border-color .25s ease-in-out !important; }
.is-invalid-input:not(:focus) { background-color: rgba(236,88,64,.1) !important; border-color: #ec5840 !important; }
.show-on-focus:active { position: static !important; height: auto !important; width: auto !important; overflow: visible !important; clip: auto !important; }
.show-on-focus:focus { position: static !important; height: auto !important; width: auto !important; overflow: visible !important; clip: auto !important; }
.clearfix::after { content: ' ' !important; display: table !important; }
.clearfix::before { content: ' ' !important; display: table !important; }
.accordion-title:focus { background-color: #e6e6e6 !important; }
.accordion-title:hover { background-color: #e6e6e6 !important; }
.accordion-title::before { content: '+' !important; position: absolute !important; right: 1rem !important; top: 50% !important; margin-top: -.5rem !important; }
.is-active>.accordion-title::before { content: '�' !important; }
.is-accordion-submenu-parent>a::after { content: '' !important; display: block !important; width: 0 !important; height: 0 !important; border: 6px inset !important; border-color: #2199e8 transparent transparent !important; border-top-style: solid !important; position: absolute !important; top: 50% !important; margin-top: -4px !important; right: 1rem !important; }
.is-accordion-submenu-parent[aria-expanded=true]>a::after { -webkit-transform-origin: 50% 50% !important; -ms-transform-origin: 50% 50% !important; transform-origin: 50% 50% !important; -webkit-transform: scaleY(-1) !important; -ms-transform: scaleY(-1) !important; transform: scaleY(-1) !important; }
.breadcrumbs::after { content: ' ' !important; display: table !important; }
.breadcrumbs::before { content: ' ' !important; display: table !important; }
.breadcrumbs li:not(:last-child)::after { color: #cacaca !important; content: "/" !important; margin: 0 .75rem !important; position: relative !important; top: 1px !important; opacity: 1 !important; }
.breadcrumbs a:hover { text-decoration: underline !important; }
.button-group::after { content: ' ' !important; display: table !important; }
.button-group::before { content: ' ' !important; display: table !important; }
.button-group.primary .button:focus { background: #147cc0 !important; color: #fff !important; }
.button-group.primary .button:hover { background: #147cc0 !important; color: #fff !important; }
.button-group.secondary .button:focus { background: #5f5f5f !important; color: #fff !important; }
.button-group.secondary .button:hover { background: #5f5f5f !important; color: #fff !important; }
.button-group.success .button:focus { background: #22bb5b !important; color: #fff !important; }
.button-group.success .button:hover { background: #22bb5b !important; color: #fff !important; }
.button-group.alert .button:focus { background: #da3116 !important; color: #fff !important; }
.button-group.alert .button:hover { background: #da3116 !important; color: #fff !important; }
.button-group.warning .button:focus { background: #cc8b00 !important; color: #fff !important; }
.button-group.warning .button:hover { background: #cc8b00 !important; color: #fff !important; }
.callout.primary a:hover { color: #0a4063 !important; }
.callout.success a:hover { color: #126330 !important; }
.callout.alert a:hover { color: #791b0c !important; }
.callout.warning a:hover { color: #664600 !important; }
.close-button:focus { color: #0a0a0a !important; }
.close-button:hover { color: #0a0a0a !important; }
.is-drilldown-submenu-parent>a::after { width: 0 !important; content: '' !important; display: block !important; height: 0 !important; }
.js-drilldown-back::before { width: 0 !important; content: '' !important; display: block !important; height: 0 !important; }
.is-drilldown-submenu-parent>a::after { border: 6px inset !important; border-color: transparent transparent transparent #2199e8 !important; border-left-style: solid !important; position: absolute !important; top: 50% !important; margin-top: -6px !important; right: 1rem !important; }
.js-drilldown-back::before { border: 6px inset !important; border-color: transparent #2199e8 transparent transparent !important; border-right-style: solid !important; float: left !important; margin-right: .75rem !important; margin-left: .6rem !important; margin-top: 14px !important; }
.dropdown.menu .has-submenu a::after { float: right !important; margin-top: 3px !important; margin-left: 10px !important; }
.dropdown.menu .has-submenu.is-down-arrow>a::after { content: '' !important; display: block !important; width: 0 !important; height: 0 !important; border: 5px inset !important; border-color: #2199e8 transparent transparent !important; border-top-style: solid !important; position: absolute !important; top: 12px !important; right: 5px !important; }
.dropdown.menu .has-submenu.is-left-arrow>a::after { content: '' !important; display: block !important; width: 0 !important; height: 0 !important; border: 5px inset !important; border-color: transparent #2199e8 transparent transparent !important; border-right-style: solid !important; float: left !important; margin-left: 0 !important; margin-right: 10px !important; }
.dropdown.menu .has-submenu.is-right-arrow>a::after { content: '' !important; display: block !important; width: 0 !important; height: 0 !important; border: 5px inset !important; border-color: transparent transparent transparent #2199e8 !important; border-left-style: solid !important; }
.dropdown.menu .submenu:not(.js-dropdown-nohover)>.has-submenu:hover>.dropdown.menu .submenu { display: block !important; }
.off-canvas-wrapper-inner::after { content: ' ' !important; display: table !important; }
.off-canvas-wrapper-inner::before { content: ' ' !important; display: table !important; }
.orbit-next:active { background-color: rgba(10,10,10,.5) !important; }
.orbit-next:focus { background-color: rgba(10,10,10,.5) !important; }
.orbit-next:hover { background-color: rgba(10,10,10,.5) !important; }
.orbit-previous:active { background-color: rgba(10,10,10,.5) !important; }
.orbit-previous:focus { background-color: rgba(10,10,10,.5) !important; }
.orbit-previous:hover { background-color: rgba(10,10,10,.5) !important; }
.orbit-bullets button:hover { background-color: #8a8a8a !important; }
.pagination::after { content: ' ' !important; display: table !important; }
.pagination::before { content: ' ' !important; display: table !important; }
.pagination a:hover { background: #e6e6e6 !important; }
.pagination button:hover { background: #e6e6e6 !important; }
.pagination [aria-label=previous]::before { content: '�' !important; display: inline-block !important; margin-right: .75rem !important; }
.pagination [aria-label=next]::after { content: '�' !important; display: inline-block !important; margin-left: .75rem !important; }
.pagination .disabled:hover { background: 0 0 !important; }
.pagination .ellipsis::after { content: '�' !important; padding: .1875rem .625rem !important; color: #0a0a0a !important; }
.slider-handle:hover { background-color: #1583cc !important; }
.switch-paddle::after { display: block !important; transition: all .25s ease-out !important; }
.switch-paddle::after { background: #fefefe !important; content: '' !important; position: absolute !important; height: 1.5rem !important; left: .25rem !important; top: .25rem !important; width: 1.5rem !important; -webkit-transform: translate3d(0,0,0) !important; transform: translate3d(0,0,0) !important; }
.tooltip.left::before { -webkit-transform: translateY(-50%) !important; -ms-transform: translateY(-50%) !important; }
input:checked~.switch-paddle::after { left: 2.25rem !important; }
[data-whatinput=mouse] input:focus~.switch-paddle { outline: 0 !important; }
.switch.tiny .switch-paddle::after { width: 1rem !important; height: 1rem !important; }
.switch.tiny input:checked~.switch-paddle:after { left: 1.75rem !important; }
.switch.small .switch-paddle::after { width: 1.25rem !important; height: 1.25rem !important; }
.switch.small input:checked~.switch-paddle:after { left: 2rem !important; }
.switch.large .switch-paddle::after { width: 2rem !important; height: 2rem !important; }
.switch.large input:checked~.switch-paddle:after { left: 2.75rem !important; }
table.hover tr:hover { background-color: #f9f9f9 !important; }
table.hover tr:nth-of-type(even):hover { background-color: #ececec !important; }
.tabs::after { content: ' ' !important; display: table !important; }
.tabs::before { content: ' ' !important; display: table !important; }
.tabs.simple>li>a:hover { background: 0 0 !important; }
.tabs.primary>li>a:focus { background: #1893e4 !important; }
.tabs.primary>li>a:hover { background: #1893e4 !important; }
.tabs-title>a:focus { background: #e6e6e6 !important; }
.tabs-title>a:hover { background: #e6e6e6 !important; }
.thumbnail:focus { box-shadow: 0 0 6px 1px rgba(33,153,232,.5) !important; }
.thumbnail:hover { box-shadow: 0 0 6px 1px rgba(33,153,232,.5) !important; }
.title-bar::after { content: ' ' !important; display: table !important; }
.title-bar::before { content: ' ' !important; display: table !important; }
.menu-icon.dark::after { content: '' !important; top: 0 !important; left: 0 !important; }
.menu-icon::after { content: '' !important; top: 0 !important; left: 0 !important; }
.menu-icon::after { position: absolute !important; display: block !important; width: 100% !important; height: 2px !important; background: #fff !important; box-shadow: 0 7px 0 #fff,0 14px 0 #fff !important; }
.menu-icon:hover::after { background: #cacaca !important; box-shadow: 0 7px 0 #cacaca,0 14px 0 #cacaca !important; }
.menu-icon.dark::after { position: absolute !important; display: block !important; width: 100% !important; height: 2px !important; background: #000 !important; box-shadow: 0 7px 0 #000,0 14px 0 #000 !important; }
.menu-icon.dark:hover::after { background: #666 !important; box-shadow: 0 7px 0 #666,0 14px 0 #666 !important; }
.tooltip.top::before { content: '' !important; display: block !important; width: 0 !important; height: 0 !important; }
.tooltip::before { content: '' !important; display: block !important; width: 0 !important; height: 0 !important; }
.has-tip:hover { cursor: help !important; }
.tooltip::before { border: .75rem inset !important; border-color: transparent transparent #0a0a0a !important; border-bottom-style: solid !important; bottom: 100% !important; position: absolute !important; left: 50% !important; -webkit-transform: translateX(-50%) !important; -ms-transform: translateX(-50%) !important; transform: translateX(-50%) !important; }
.tooltip.top::before { border: .75rem inset !important; border-color: #0a0a0a transparent transparent !important; border-top-style: solid !important; top: 100% !important; bottom: auto !important; }
.tooltip.left::before { content: '' !important; display: block !important; width: 0 !important; height: 0 !important; bottom: auto !important; top: 50% !important; }
.tooltip.right::before { content: '' !important; display: block !important; width: 0 !important; height: 0 !important; bottom: auto !important; top: 50% !important; }
.tooltip.left::before { border: .75rem inset !important; border-color: transparent transparent transparent #0a0a0a !important; border-left-style: solid !important; left: 100% !important; transform: translateY(-50%) !important; }
.tooltip.right::before { border: .75rem inset !important; border-color: transparent #0a0a0a transparent transparent !important; border-right-style: solid !important; left: auto !important; right: 100% !important; -webkit-transform: translateY(-50%) !important; -ms-transform: translateY(-50%) !important; transform: translateY(-50%) !important; }
.top-bar::after { content: ' ' !important; display: table !important; }
.top-bar::before { content: ' ' !important; display: table !important; }
></style>
    

    
    <div style="box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; background-color: #eee; margin: 0; padding: .5rem;">
    <img src="<?=$text['website']['logo']?>" style="vertical-align: middle; max-width: 100% !important; height: auto; -ms-interpolation-mode: bicubic; display: inline-block; box-sizing: inherit; page-break-inside: avoid; color: #000 !important; box-shadow: none !important; text-shadow: none !important;">
    </div>
    
    <br style="box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important;">
    
    <div style="box-sizing: inherit; max-width: 75rem; width: 100%; float: none; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0 auto; padding: 0 .9375rem;">
        <h2 style="text-rendering: optimizeLegibility; font-family: 'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif; font-weight: 400; box-sizing: inherit; font-style: normal; color: #000 !important; line-height: 1.4; font-size: 2.5rem; box-shadow: none !important; text-shadow: none !important; orphans: 3; widows: 3; page-break-after: avoid; margin: 0 0 .5rem; padding: 0;"> <?=$text['title']?> </h2>
      <nav aria-label="&lt;?<?=$text['website']['name']?>" role="navigation" style="display: block; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important;">
        <ul style="line-height: 1.6; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; list-style-type: none; list-style-position: outside; margin: 0 0 1rem; padding: 0;">
          <li style="box-sizing: inherit; font-size: .6875rem; color: #000 !important; box-shadow: none !important; text-shadow: none !important; float: left; cursor: default; text-transform: uppercase; margin: 0; padding: 0;"><a href="<?=$text['website']['link']?>" style="line-height: inherit; box-sizing: inherit; color: #000 !important; text-decoration: underline; cursor: pointer; box-shadow: none !important; text-shadow: none !important; background-color: transparent;">��� �����</a></li>
        </ul>
      </nav>
    </div>

    <div style="box-sizing: inherit; max-width: 75rem; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0 auto; padding: 0;">
      <div style="box-sizing: inherit; width: 50%; float: left; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0; padding: 0 .9375rem;">
            <div style="box-sizing: inherit; max-width: 75rem; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0 -.9375rem; padding: 0;">
                <div style="box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0; padding: 0;">
                    <img src="<?=$text['website']['banner']?>" style="vertical-align: middle; max-width: 100% !important; height: auto; -ms-interpolation-mode: bicubic; display: inline-block; box-sizing: inherit; page-break-inside: avoid; color: #000 !important; box-shadow: none !important; text-shadow: none !important; line-height: 0; transition: box-shadow .2s ease-out; border-radius: 0; margin-bottom: 1rem; border: 4px solid #fefefe;">
                </div>
                <div style="box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0; padding: 0;">
                    <ul style="line-height: 1.6; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; list-style-type: disc; list-style-position: outside; margin: 0 0 1rem; padding: 0;">
                        <li style="box-sizing: inherit; font-size: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0; padding: 0;"> <b style="font-weight: 700; line-height: inherit; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important;">���: </b><?=$text['buyer']['firstName']?></li>
                        <li style="box-sizing: inherit; font-size: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0; padding: 0;"> <b style="font-weight: 700; line-height: inherit; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important;">�������:</b><?=$text['buyer']['lastName']?></li>
                        <li style="box-sizing: inherit; font-size: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0; padding: 0;"> <b style="font-weight: 700; line-height: inherit; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important;">�������: </b><?=$text['buyer']['phone']?></li>
                        <li style="box-sizing: inherit; font-size: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0; padding: 0;"> <b style="font-weight: 700; line-height: inherit; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important;">���������� ����: </b><?=$text['buyer']['email']?></li>
                        <li style="box-sizing: inherit; font-size: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0; padding: 0;"> <b style="font-weight: 700; line-height: inherit; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important;">���� ���� �� ���������: </b> <?=$text['total_sum']?></li>
                    </ul>
                </div>
            </div>
      </div>
      <div style="box-sizing: inherit; width: 41.66667%; float: right; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0; padding: 0 .9375rem;">
        <h3 style="text-rendering: optimizeLegibility; font-family: 'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif; font-weight: 400; box-sizing: inherit; font-style: normal; color: #000 !important; line-height: 1.4; font-size: 1.9375rem; box-shadow: none !important; text-shadow: none !important; orphans: 3; widows: 3; page-break-after: avoid; margin: 0 0 .5rem; padding: 0;"><strong style="font-weight: 700; line-height: inherit; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important;"><?=$text['body']['title']?></strong></h3>
            <?=$text['body']['content']?>
        <a href="<?=$text['ticket']['link']?>" target="_blank" style="vertical-align: middle; line-height: 1; box-sizing: inherit; color: #000 !important; text-decoration: underline; cursor: pointer; box-shadow: none !important; text-shadow: none !important; display: block; text-align: center; -webkit-appearance: none; transition: all .25s ease-out; border-radius: 0; font-size: 1.25rem; width: 100%; background-color: #2199e8; margin: 0 0 1rem; padding: .85em 1em; border: 1px solid transparent;"><?=$text['ticket']['name']?></a>

        <div style="box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; font-size: .75rem; margin: 0 0 1rem; padding: 0;">
        <a href="<?=$text["facebook"]?>" style="line-height: inherit; box-sizing: inherit; color: #000 !important; text-decoration: underline; cursor: pointer; box-shadow: none !important; text-shadow: none !important; background-color: transparent;" target="_blank" class="button">Facebook</a>
          </div>
        </div>
    </div>

    <div style="box-sizing: inherit; max-width: 75rem; width: 100%; float: none; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0 auto; padding: 0 .9375rem;">
      <hr style="box-sizing: content-box; clear: both; max-width: 75rem; height: 0; border-top-width: 0; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #cacaca; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 1.25rem auto;">
      <ul data-tabs style="line-height: 1.6; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; background-color: #fefefe; list-style-type: none; list-style-position: outside; margin: 0; padding: 0; border: 1px solid #e6e6e6;">
        <li style="box-sizing: inherit; font-size: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; float: left; margin: 0; padding: 0;"><a href="#ed6f28272bd9177fbe24b2c969a33ef3" style="line-height: 1; box-sizing: inherit; color: #000 !important; text-decoration: underline; cursor: pointer; box-shadow: none !important; text-shadow: none !important; display: block; font-size: 12px; background-color: transparent; padding: 1.25rem 1.5rem;">�������� ������</a></li>
      </ul>
      <div data-tabs-content="example-tabs" style="box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; transition: all .5s ease; background-color: #fefefe; margin: 0; padding: 0; border-color: #e6e6e6; border-style: solid; border-width: 0 1px 1px;">
        <div id="ed6f28272bd9177fbe24b2c969a33ef3" style="box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; display: block; margin: 0; padding: 1rem;">
          <div style="box-sizing: inherit; max-width: 75rem; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0 -.9375rem; padding: 0;">
            <?php foreach($text['passengers'] as $passenger): ?>
                <div style="box-sizing: inherit; width: 20%; float: left; clear: both; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0; padding: 0 .9375rem;">
                  <img src="http://placehold.it/350x200" style="vertical-align: middle; max-width: 100% !important; height: auto; -ms-interpolation-mode: bicubic; display: inline-block; box-sizing: inherit; page-break-inside: avoid; color: #000 !important; box-shadow: none !important; text-shadow: none !important; line-height: 0; transition: box-shadow .2s ease-out; border-radius: 0; margin-bottom: 1rem; border: 4px solid #fefefe;">
                  <h5 style="text-rendering: optimizeLegibility; font-family: 'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif; font-weight: 400; box-sizing: inherit; font-style: normal; color: #000 !important; line-height: 1.4; font-size: 1.25rem; box-shadow: none !important; text-shadow: none !important; margin: 0 0 .5rem; padding: 0;"><?=$passenger['firstName']?> <?=$passenger['lastName']?> <small style="font-size: 80%; line-height: 0; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important;"><?=$passenger['ticket_price']?> BGN</small>
</h5>
                  <p style="text-rendering: optimizeLegibility; line-height: 1.6; box-sizing: inherit; font-size: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; orphans: 3; widows: 3; margin: 0 0 1rem; padding: 0;">����� �������: <?=$passenger['seat_number']?></p>
                  <a href="<?=$text['ticket']['link']?>" target="_blank" style="vertical-align: middle; line-height: 1; box-sizing: inherit; color: #000 !important; text-decoration: underline; cursor: pointer; box-shadow: none !important; text-shadow: none !important; display: block; text-align: center; -webkit-appearance: none; transition: all .25s ease-out; border-radius: 0; font-size: .6rem; width: 100%; background-color: #2199e8; margin: 0 0 1rem; padding: .85em 1em; border: 1px solid #2199e8;"><?=$text['ticket']['name']?></a>
                </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>

    <div style="box-sizing: inherit; max-width: 75rem; width: 100%; float: none; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 0 auto; padding: 0 .9375rem;">
      <hr style="box-sizing: content-box; clear: both; max-width: 75rem; height: 0; border-top-width: 0; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #cacaca; color: #000 !important; box-shadow: none !important; text-shadow: none !important; margin: 1.25rem auto;">
      <ul style="line-height: 1.6; box-sizing: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; list-style-type: none; list-style-position: outside; margin: 0; padding: 0;">
        <li style="box-sizing: inherit; font-size: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; vertical-align: middle; display: table-cell; margin: 0; padding: 0;"><a href="<?=$text['about']['link']?>" style="line-height: 1; box-sizing: inherit; color: #000 !important; text-decoration: underline; cursor: pointer; box-shadow: none !important; text-shadow: none !important; display: block; margin-bottom: 0; background-color: transparent; padding: .7rem 1rem;"><?=$text['about']['name']?></a></li>
        <li style="box-sizing: inherit; font-size: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; vertical-align: middle; display: table-cell; margin: 0; padding: 0;"><a href="<?=$text['contact']['link']?>" style="line-height: 1; box-sizing: inherit; color: #000 !important; text-decoration: underline; cursor: pointer; box-shadow: none !important; text-shadow: none !important; display: block; margin-bottom: 0; background-color: transparent; padding: .7rem 1rem;"><?=$text['contact']['name']?></a></li>
        <li style="box-sizing: inherit; font-size: inherit; color: #000 !important; box-shadow: none !important; text-shadow: none !important; float: right !important; vertical-align: middle; display: table-cell; margin: 0; padding: 0;">Copyright <?=date('Y', time());?></li>
      </ul>
    </div>
  </body>
</html>

