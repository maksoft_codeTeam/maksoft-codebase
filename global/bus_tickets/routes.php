<?php 
require_once __DIR__.'/base.php';
$load_form('AddRoute');
$load_form('AddStop');
$load_form('AddRouteToDirection');
$add_route = new AddRoute($em, $_POST);
$add_stop = new AddStop($em, $_POST);
$attach_route = new AddRouteToDirection($em, $_POST);
$add_route->add_class('div', 'col-md-6');
$add_route->add_class('form', 'row');
$add_stop->add_class('div', 'col-md-6');
$add_stop->add_class('form', 'row');
require_once __DIR__.'/templates/show_directions.php';
?>
<?php if(!isset($_GET['direction_id']) or !in_array($_GET['direction_id'], $direction_ids)): ?>
    <div class="col-md-12 text-center">
        <h2> �� ��� ������� ����� </h2>
    </div>
    <?php return;?>
<?php endif;?>

<?php
$direction = $em->getRepository('\Bus\models\Direction')->findOneById($_GET['direction_id']);

$routes = $direction->getRoutes();


if($add_route->is_valid()){
    $add_route->save();
}
if($add_stop->is_valid()){
    $add_stop->save();
}
if($attach_route->is_valid()){
    $attach_route->save();
}
?>

<div class="row">
    <div class='col-md-6'>
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addRoute"><?=$add_route->title?></button>
    </div>
    <div class='col-md-6'>
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#attachRoute"><?=$attach_route->title?></button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
<pre><code>
</code></pre>
        <h2>������ �� ������ ��������</h2>
        <table class='table table-responsive'>
            <th>ID</th>
            <th>OT</th>
            <th>��</th>
        <?php foreach($routes as $route) : ?>
            <?php $route = $route->getRoute();?>
            <tr>
            <td><?=$route->getId();?></td>
            <td><?=$route->getStartStation()->getName();?></td>
            <td><?=$route->getEndStation()->getName();?></td>
            </tr>
        <?php endforeach;?>
        </table>
    </div>
</div>


<!-- Modal -->
<div id="attachRoute" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?=$attach_route->title?></h4>
      </div>
      <div class="modal-body">
            <?=$attach_route?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="addRoute" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?=$add_route->title?></h4>
      </div>
      <div class="modal-body">
            <?=$add_route?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="addRouteStop" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?=$add_stop->title?></h4>
      </div>
      <div class="modal-body">
            <?=$add_stop?>
            <p>&nbsp;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
