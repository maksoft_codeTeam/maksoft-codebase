<?php
require_once __DIR__.'/../../lib/lib_functions.php';
require_once __DIR__ .'/../../modules/tasks.php';
if(!defined('MAX_RESERVED_TIME')) {
    define("MAX_RESERVED_TIME", 600);
}
if(!defined("MAX_RESERVED_SEATS")) {
    define("MAX_RESERVED_SEATS", 2);
}
use Doctrine\Common\Collections\Criteria;
require_once __DIR__.'/helpers.php';

function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
{
    $sets = array();
    if(strpos($available_sets, 'l') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if(strpos($available_sets, 'u') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if(strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    if(strpos($available_sets, 's') !== false)
        $sets[] = '!@#$%&*?';
    $all = '';
    $password = '';
    foreach($sets as $set)
    {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if(!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while(strlen($password) > $dash_len)
    {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}



function directions_list($em, $company_id) {
    return $em->createQueryBuilder()->select('d')
       ->from('Bus\models\Direction', 'd')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->setParameter('company_id', $company_id)
       ->getQuery()
       ->getResult();
}

function stations_list($em, $company_id) {
    return $em->createQueryBuilder()->select('s')
       ->from('Bus\models\Station', 's')
       ->Where('s.company = :company')
       ->setParameter('company', $company_id)
       ->getQuery()
       ->getResult();
};


function vehicles_list($em, $company_id) {
    return $em->createQueryBuilder()->select('s')
       ->from('Bus\models\Vehicle', 's')
       ->Where('s.company = :company')
       ->setParameter('company', $company_id)
       ->getQuery()
       ->getResult();
}

function get_vehicle($id) {
    return $em->createQueryBuilder()->select('v')
       ->from('Bus\models\Vehicle', 'v')
       ->where('v.id = :id')
       ->setParameter('id', $id)
       ->getQuery()
       ->setMaxResults(1)
       ->getOneOrNullResult();
}

function get_sold_tickets($em, $schedule_id) {
    return $em->getRepository('Bus\models\Ticket')->findAll(array('schedule'=>$schedule_id));
}


function add_buyer($em, $email, $first_name, $middle_name, $last_name, $phone) {
    $client = $em->getRepository('\Bus\models\Payer')->findOneBy(array('email' => $email));
    if($client) {
        return $client;
    }

    $client = new \Bus\models\Payer();
    $client->setFirstName($first_name);
    $client->setMiddleName($middle_name);
    $client->setLastName($last_name);
    $client->setPhone($phone);
    $client->setPassword(generateStrongPassword());
    $client->setEmail($email);
    $em->persist($client);
    $em->flush();
    return $client;
}

function add_passenger($em, $email, $first_name, $middle_name, $last_name, $phone) {
    $client = $em->getRepository('\Bus\models\Passenger')->findOneBy(array('email' => $email));
    if($client) {
        return $client;
    }

    $client = new \Bus\models\Passenger();
    $client->setFirstName($first_name);
    $client->setMiddleName($middle_name);
    $client->setLastName($last_name);
    $client->setPhone($phone);
    $client->setPassword(generateStrongPassword());
    $client->setEmail($email);
    $em->persist($client);
    $em->flush();
    return $client;
}

function add_ticket($em, $client, $vehicle, $seat, $price, $schedule, $route, $passenger) {
    $ticket = new \Bus\models\Ticket();
    $ticket->setPayer($client);
    $ticket->setVehicle($vehicle);
    $ticket->setSeat($seat);
    $ticket->setPrice($price);
    $ticket->setSchedule($schedule);
    $ticket->setPassenger($passenger);
    $ticket->setRoute($route);
    $em->persist($ticket);
    $em->flush();
    return $ticket;
}

class EmptyObject
{
    protected $data = array();
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }

        return null;
    }

    /**  As of PHP 5.1.0  */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /**  As of PHP 5.1.0  */
    public function __unset($name)
    {
        unset($this->data[$name]);
    }
    public function __call($name, $arguments)
    {
    }

    /**  As of PHP 5.3.0  */
    public static function __callStatic($name, $arguments)
    {
    }
}

$money = function($price) {
    $fmt = '%l ';
    return iconv('utf8', 'cp1251', (string) money_format($fmt, $price)) . "\n";
};

$format_time = function($minutes) {
    $hours = floor($minutes / 60); // Get the number of whole hours
    $minutes = $minutes % 60;
    return sprintf("%s %s %s", $hours, $hours < 2 ? " час": " часа", $minutes > 0 ? " и $minutes минути" : '');
};

function get_object($obj) {
    return $obj ? $obj : new EmptyObject; 
}


function my_json_encode($in) { 
  $_escape = function ($str) { 
    return addcslashes($str, "\v\t\n\r\f\"\\/"); 
  }; 
  $out = ""; 
  if (is_object($in)) { 
    $class_vars = get_object_vars(($in)); 
    $arr = array(); 
    foreach ($class_vars as $key => $val) { 
      $arr[$key] = "\"{$_escape($key)}\":\"{$val}\""; 
    } 
    $val = implode(',', $arr); 
    $out .= "{{$val}}"; 
  }elseif (is_array($in)) { 
    $obj = false; 
    $arr = array(); 
    foreach($in AS $key => $val) { 
      if(!is_numeric($key)) { 
        $obj = true; 
      } 
      $arr[$key] = my_json_encode($val); 
    } 
    if($obj) { 
      foreach($arr AS $key => $val) { 
        $arr[$key] = "\"{$_escape($key)}\":{$val}"; 
      } 
      $val = implode(',', $arr); 
      $out .= "{{$val}}"; 
    }else { 
      $val = implode(',', $arr); 
      $out .= "[{$val}]"; 
    } 
  }elseif (is_bool($in)) { 
    $out .= $in ? 'true' : 'false'; 
  }elseif (is_null($in)) { 
    $out .= 'null'; 
  }elseif (is_string($in)) { 
    $out .= "\"{$_escape($in)}\""; 
  }else { 
    $out .= $in; 
  } 
  return "{$out}"; 
} 

$week_days = array(
    'en' => array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'),
    'bg' => array('понеделник', 'вторник', 'сряда', 'четвъртък', 'петък', 'събота', 'неделя'),
);
$day_name = function($day, $local='bg') use ($week_days){
    if(!isset($week_days[$local])) {
        $local = 'bg';
    }

    return $week_days[$local][$day];
};

function getDayName($day, $local='bg') {
    return jddayofweek($day);
}

function getWeekday($date) {
    return date('w', strtotime($date)) -1 ;
}

$get_station = function($station_id) use ($em) {
    return $em->getRepository('Bus\models\Station')->findOneById($station_id);
};


$directions_from = function ($company_id) use ($em) {
    return $em->createQueryBuilder()->select('r')
       ->from('Bus\models\Route', 'r')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->setParameter('company_id', $company_id)
       ->groupBy('r.startStation')
       ->getQuery()
       ->getResult();
};

$directions_to = function($company_id, $city_id) use ($em, $get_schedule_relative) {
    $date = new DateTime();
    $schedule = $em->createQueryBuilder()->select('s')
        ->from('Bus\models\Schedule', 's')
        ->where("s.start_date >= :date")
        ->setParameter('date', $date)
        ->orderBy('s.start_date', 'ASC')
        ->getQuery()
        ->setMaxResults(1)
        ->getOneOrNullResult();

    return $em->createQueryBuilder()->select('r')
       ->from('Bus\models\Route', 'r')
       ->join('Bus\models\Schedule', 'sh', 'WITH', 'sh.id = :schedule_id')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->where('r.startStation = :start_station')
       ->setParameter('schedule_id', $schedule->getId())
       ->setParameter('start_station', $city_id)
       ->setParameter('company_id', $company_id)
       ->getQuery()
       ->getResult();
};

$get_course = function($company_id, $city_from, $city_to, $date) use ($em) {
    $date = date('Y-m-d', strtotime($date));
    return $em->createQueryBuilder()->select('r')
       ->from('Bus\models\Route', 'r')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->join('Bus\models\Schedule', 'sh', 'WITH', 'sh.start_date = :today')
       ->where('r.startStation = :start_station')
       ->andWhere('r.endStation = :end_station')
       ->setParameter('start_station', $city_from)
       ->setParameter('end_station', $city_to)
       ->setParameter('today', $date)
       ->setParameter('company_id', $company_id)
       ->getQuery()
       ->getOneOrNullResult();
};


$get_course_relative = function($company_id, $city_from, $city_to, $date) use ($em) {
    $date = date('Y-m-d', strtotime($date));
    return $em->createQueryBuilder()->select('r')
       ->from('Bus\models\Route', 'r')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->where('r.startStation = :start_station')
       ->andWhere('r.endStation = :end_station')
       ->setParameter('start_station', $city_from)
       ->setParameter('end_station', $city_to)
       ->setParameter('company_id', $company_id)
       ->getQuery()
       ->setMaxResults(1)
       ->getOneOrNullResult();
};

$get_route = function($from_city, $to_city, $direction_id) use ($em) {
    return $em->getRepository('Bus\models\Route')->findOneBy(array('startStation' => $from_city, 'endStation' => $to_city, 'direction' => $direction_id));
};

use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;

$get_schedule_relative = function ($date, $criteria, $route)  use ($em) {
    $date = date('Y-m-d', strtotime($date));
    return $em->createQueryBuilder()->select('s')
        ->from('Bus\models\Schedule', 's')
        ->leftJoin('Bus\models\DirectionRoute', 'r', 'WHERE', 'r.direction = s.direction' )
        ->where("s.start_date >= :date")
        ->andWhere("r.route = :route_id")
        ->setParameter('date', $date)
        ->setParameter('route_id', $route->getId())
        ->orderBy('s.start_date', 'ASC')
        ->getQuery()
        ->setMaxResults(1)
        ->getOneOrNullResult();
};

function get_time_to_course($em, $direction, $route) {
    $current_station = $em->getRepository('Bus\models\DirectionPlan')->findOneBy(array('direction'=> $direction, 'stop' => $route->getStartStation()));
    $routeStops =  $em->createQueryBuilder()->select('dp')
        ->from('Bus\models\DirectionPlan', 'dp')
        ->join('Bus\models\Direction', 'd', 'WITH', 'd.id = dp.direction')
        ->Where('d.id = :direction_id')
        ->AndWhere("dp.order <= :order")
        ->setParameter('direction_id', $direction->getId())
        ->setParameter('order', $current_station->getOrder())
        ->getQuery()
        ->getResult();
    $rest_time = 0;
    foreach($routeStops as $stop) {
        $rest_time = $rest_time + $stop->getDriveTime() + $stop->getRestTime();
    }

    return $rest_time;
}

$get_course_info = function ($direction, $route) use ($em) {
    $start_station = $em->getRepository('Bus\models\DirectionPlan')->findOneBy(array('direction'=> $direction, 'stop' => $route->getStartStation()));
    $end_station = $em->getRepository('Bus\models\DirectionPlan')->findOneBy(array('direction'=> $direction, 'stop' => $route->getEndStation()));
    $routeStops =  $em->createQueryBuilder()->select('dp')
        ->from('Bus\models\DirectionPlan', 'dp')
        ->join('Bus\models\Direction', 'd', 'WITH', 'd.id = dp.direction')
        ->Where('d.id = :direction_id')
        ->AndWhere("dp.order > :start_order")
        ->AndWhere("dp.order <= :end_order")
        ->setParameter('direction_id', $direction->getId())
        ->setParameter('start_order', $start_station->getOrder())
        ->setParameter('end_order', $end_station->getOrder())
        ->getQuery()
        ->getResult();
    $rest_time = 0;
    $distance = 0;
    foreach($routeStops as $stop) {
        $distance += $stop->getDistanceKm();
        $rest_time += $stop->getDriveTime() + $stop->getRestTime();
    }

    return array(
        'time' => array(
            'to_course' => get_time_to_course($em, $direction, $route),
            'duration' => $rest_time - $end_station->getRestTime()
        ), 'distance' => $distance
    );
};

$get_schedule = function($route_id, $direction_id, $date, $criteria) use ($em) {
    $date = date('Y-m-d', strtotime($date));
    return $em->createQueryBuilder()->select('s')
        ->from('Bus\models\Schedule', 's')
        ->join('Bus\models\Direction', 'd', 'WITH', 'd.id = s.direction')
        ->join('Bus\models\Route', 'r')
        ->Where('r.id = :route_id')
        ->AndWhere("s.start_date $criteria :date")
        ->setParameter('route_id', $route_id)
        ->setParameter('date', $date)
        ->getQuery()
        ->setMaxResults(1)
        ->getOneOrNullResult();
};
$get_vehicles = function($route_id) use ($em) {
    return $em->createQueryBuilder()->select(array('v', 's'))
        ->from('Bus\models\Vehicle', 'v')
        ->from('Bus\models\Schedule', 'v')
        ->join('Bus\models\Direction', 'd', 'WITH', 'd.id = s.direction')
        ->join('Bus\models\Route', 'r')
        ->Where('r.id = :route_id')
        ->setParameter('route_id', $route_id)
        ->getQuery()
        ->getSingleResult();
};


$clean_tickets = function() use ($em) {
    $ticketsRepository = $em->getRepository('Bus\models\ReservedSeats');
    $criteria = Criteria::create()
        ->where(Criteria::expr()->lte("created", new \Datetime(date('Y-m-d H:i:s', time()- MAX_RESERVED_TIME))))
        ->andWhere(Criteria::expr()->eq("sold", false))
        ->andWhere(Criteria::expr()->eq("auto_remove", true))
    ;
    $tickets = $ticketsRepository->matching($criteria);

    foreach($tickets as $ticket) {
        $em->remove($ticket);
    }

    $em->flush();
};

$get_reserved_tickets = function($kwargs) use ($em, $clean_tickets) {
    $clean_tickets();
    return  $em->getRepository('Bus\models\ReservedSeats')
                  ->findBy(array(
                      'schedule' => $kwargs['schedule'],
                      'identifier' => session_id(),
                      'sold' => false,
                    ));
    
};

$sticky_message = function ($msg, $class='success', $return=false) {
    $msg = " 
        <script>
            jQuery( document ).ready(function() {
                toastr.$class('{$msg}');
            });
        </script>
";
    if ($return):
        return $msg;
    endif;
    
    echo $msg;
};

function get_user_reservations($em, $schedule_id, $session_id) {
    return $em->getRepository('\Bus\models\ReservedSeats')
        ->findBy(array('schedule' => $schedule_id, 'identifier' => $session_id, 'sold' => false, 'auto_remove'=> true));
}

$reserve_ticket = function($kwargs) use($em, $clean_tickets){
    //First remove trashy reservations 
    $clean_tickets();


    //Try to get reserved seat
    $reserve = $em->getRepository('Bus\models\ReservedSeats')
                  ->findOneBy(array(
                      'seat' => $kwargs['seat'],
                      'schedule' => $kwargs['schedule'],
                      'identifier' => session_id(),
                    ));
    $seat = $em->getRepository('Bus\models\Seat')
                ->findOneById($kwargs['seat']);
    // If there is reservation we delete it
    if($seat->isBlocked() ) {
        throw new \Exception('Не може да изтриете тази седалка');
    }

    if(!empty($reserve)){
        if($reserve->getSold() or !$reserve->getAutoRemove()) {
            throw new \Exception('Не може да изтриете тази седалка');
        }

        $em->remove($reserve);
        $em->flush();
        return array('number' => $reserve->getSeat()->getNumber(), 'id' => $reserve->getId());
    }

    $user_reservations = get_user_reservations($em, $kwargs['schedule'], session_id());

    if(count($user_reservations) > MAX_RESERVED_SEATS ) {
        throw new \Exception('Максималният брой на резервирани седалки е достигнат.');
    }

    // Trying to insert new reservation if other user is already reserved a seat throws Exception
    $seat = $em->getRepository('Bus\models\Seat')->findOneById($kwargs['seat']);
    $schedule = $em->getRepository('Bus\models\Schedule')->findOneById($kwargs['schedule']);
    $reserve = new Bus\models\ReservedSeats();
    $reserve->setSeat($seat);
    $reserve->setSchedule($schedule);
    $reserve->setIdentifier(session_id());
    try {
    $em->persist($reserve);
    $em->flush();
    } catch (\Exception $e) {
        throw new Exception('Не може да резервирате тази седалка');
    }
    return array('reserved' => true, 'number' => $seat->getNumber(), 'id' => $reserve->getId());

    

};

function get_schedule_by_id ($schedule_id) {
    global $em;
    return $em->getRepository('Bus\models\Schedule')->findOneById($schedule_id);
}

function get_seat($seat_id) {
    global $em;
    return $em->getRepository('Bus\models\Seat')->findOneById($seat_id);
}

function get_client ($client_id) {
    global $em;
    return $em->getRepository('Bus\models\Client')->findOneById($client_id);
}

function get_route_by_id($route_id) {
    global $em;
    return $em->getRepository('Bus\models\Route')->findOneById($route_id);
}

$get_stops_between = function($from_city, $to_city, $company_id) use($em) {
    return $em->createQueryBuilder()->select('r')
       ->from('Bus\models\Route', 'r')
       ->join('Bus\models\Direction', 'd', 'WITH', 'd.id = r.parent')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->where('r.startStation = :start_station')
       ->andWhere('r.endStation = :end_station')
       ->setParameter('start_station', $from_city)
       ->setParameter('end_station', $to_city)
       ->setParameter('company_id', $company_id)
       ->getQuery()
       ->getResult();
};

function get_reserved_seats($request, $em) {
    $results =  $em->createQueryBuilder()->select('r')
       ->from('Bus\models\Route', 'r')
       ->join('Bus\models\Direction', 'd', 'WITH', 'd.id = r.parent')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->where('r.startStation = :start_station')
       ->andWhere('r.endStation = :end_station')
       ->setParameter('start_station', $from_city)
       ->setParameter('end_station', $to_city)
       ->setParameter('company_id', $company_id)
       ->getQuery()
       ->getResult();
    return array_map(function($i) {
        return $i->getId();
    }, $results);
}

function is_free ($seat, $schedule){
    $reservations = $seat->getReservations()->filter(
        function($reservation) use ($schedule) {
            return $reservation->getSchedule()->getId() == $schedule->getId();
        }
    );
    return count($reservations) == 0;
}

$get_stops_between_dev = function($from_city, $to_city, $company_id) use($em) {
    return $em->createQueryBuilder()->select('t')
       ->from('Bus\models\Stop', 't')
       ->join('Bus\models\Route', 'r', 'WITH', 'r.id = t.route')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->where('r.startStation = :start_station')
       ->andWhere('r.endStation <= :end_station')
       ->setParameter('start_station', $from_city)
       ->setParameter('end_station', $to_city)
       ->setParameter('company_id', $company_id)
       ->getQuery()
       ->getResult();
};
