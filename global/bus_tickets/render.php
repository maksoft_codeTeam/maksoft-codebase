<?php
error_reporting(1);
require_once __DIR__.'/helpers.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
$request = Request::createFromGlobals();
$response = new Response();

$commands = array(
    'create_seat' => $add_seat,
    'render_form' => $render_form,
);

if(!isset($_GET['cmd']) or !array_key_exists($_GET['cmd'], $commands)) {
    $response->setStatusCode(Response::HTTP_BAD_REQUEST);
    $response->prepare($request);
    return $response->send();
}

$cmd = $_GET['cmd'];

try {
    $data = $commands[$cmd]($request, $em);
    $response->setContent($data);
} catch (Exception $e) {
    $response->setStatusCode(Response::HTTP_BAD_REQUEST);
    $response->setContent($e->getMessage());
}

$response->prepare($request);
$response->send();
