<?php
require_once __DIR__.'/../repository.php';
require_once __DIR__.'/Base.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Date;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;


class AddSchedule extends Base
{
    public $title = '�������� �� �� ����:';

    public $default_error = '������� ������� ������';

    public function __construct($em, $post_data=array(), $files_data=array(), $initial=null)
    {
        $this->em = $em;
        $this->action = Hidden::init()->add('value', 'insert_schedule');
        $this->startDate = Text::init()
            ->add('class', 'hasDatepicker')
            ->add('id', 'startDate')
            ->add('label', '�� ����:');
        $this->direction_id = Hidden::init();
        $this->submit = Submit::init()->add('value', '������');
        parent::__construct($post_data, $files_data, $initial);
    }

    public function save()
    {
        $direction = $this->em->getRepository('\Bus\models\Direction')
                        ->findOneBy(array('id' => $this->direction_id->value));
        $date = date('d-m-Y H:i' , strtotime($this->startDate->value));
        $date = new DateTime($date);
        $schedule = new \Bus\models\Schedule();

        $schedule->setDirection($direction);
        $schedule->setStartDate($date);
        $this->em->persist($schedule);
        $this->em->flush();
        return $schedule;
    }
}
