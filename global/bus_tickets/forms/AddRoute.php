<?php
require_once __DIR__.'/../repository.php';
require_once __DIR__.'/Base.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Password;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;



class AddRoute extends Base
{
    public $title = '�������� �� ��� �������';

    protected $week = array('����������', '�������', '�����', '���������', '�����', '������', '������');

    public $default_error = '������� ������� ������';

    protected $stations = array();
    protected $em;
    public function __construct($em, $post_data=array(), $files_data=array(), $initial=null)
    {
        $this->em = $em;
        $stations_list = stations_list($em, COMPANY_ID);
        $stations = array();
        foreach($stations_list as $station) {
            $stations[$station->getId()] = $station->getName();
            $this->stations[$station->getId()] = $station;
        }
        $this->startStationId = Select::init()
            ->add('label', '������ ��:')
            ->add('data', $stations);
        $this->endStationId = Select::init()
            ->add('label', '��������::')
            ->add('data', $stations);
        $this->action = Hidden::init()->add('value', 'addRoute');
        $this->submit = Submit::init()->add('value', '������');
        parent::__construct($post_data, $files_data, $initial);
    }

    public function save()
    {
        $start_station = array_key_exists($this->startStationId->value, $this->stations) ? $this->stations[$this->startStationId->value] : null;
        $end_station = array_key_exists($this->endStationId->value, $this->stations) ? $this->stations[$this->endStationId->value] : null;
        $route = new \Bus\models\Route();
        $route
            ->setStartStationId($start_startion)
            ->setEndStationId($end_station)
        ;
        $this->em->persist($route);
        $this->em->flush();
        return $route;
    }
}
