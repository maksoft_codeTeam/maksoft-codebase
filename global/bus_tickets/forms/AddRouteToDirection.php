<?php
require_once __DIR__.'/../repository.php';
require_once __DIR__.'/Base.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Password;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;



class AddRouteToDirection extends Base
{
    public $title = '�������������� �� ������� ��� �����������';

    protected $type = array('�������� �����', '�������', '���������', '�����');

    public $default_error = '������� ������� ������';

    protected $stations = array();
    protected $em;
    public function __construct($em, $post_data=array(), $files_data=array(), $initial=null)
    {
        $this->em = $em;
        $direction = $this->em->getRepository('Bus\models\Direction')->findOneById($_GET['direction_id']);

        $this->routes_arr = $this->em->createQueryBuilder()->select('r')
            ->from('Bus\models\Route', 'r')
            ->innerJoin('r.directions', 'rd', 'WHERE', 'rd != :direction')
           ->setParameter('direction', $direction)
           ->getQuery()
           ->getResult();

        $routes_select = array();

        foreach($this->routes_arr as $route) 
        {
            $routes_select[$route->getId()] = $route->getStartStation()->getName() .' - '. $route->getEndStation()->getName();

        }

        $this->route_id = Select::init()
            ->add('name', 'route_id')
            ->add('label', '������ ������')
            ->add('data', $routes_select);

        $this->action = Hidden::init()->add('value', 'addRouteToDirection');
        $this->submit = Submit::init()->add('value', '������');
        parent::__construct($post_data, $files_data, $initial);
    }

    public function save()
    {
        $route = $this->em->getRepository('\Bus\models\Route')->findOneBy(array('id'=> $this->route_id->value));
        $direction = $this->em->getRepository('\Bus\models\Direction')->findOneById($_GET['direction_id']);
        $direction->addRoute($route);
        $this->em->persist($direction);
        $this->em->flush();
        return $route;
    }
}
