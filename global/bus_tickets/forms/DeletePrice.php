<?php
require_once __DIR__.'/../repository.php';
require_once __DIR__.'/Base.php';
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Bootstrap;



class DeletePrice extends Base
{
    public $default_error = '������� ������� ������';

    public function __construct($em, $post_data=array(), $files_data=array(), $initial=null)
    {
        $this->em = $em;
        $this->routePriceId = Hidden::init()->add_validator(NotEmpty::init(true));
        $this->action = Hidden::init()->add('value', 'delete_price');
        parent::__construct($post_data, $files_data, $initial);
    }

    public function save()
    {
        $routePrice = $this->em->getRepository('\Bus\models\RoutePrice')->findOneById($this->routePriceId->value);
        $this->em->remove($routePrice);
        $this->em->flush();
    }
}
