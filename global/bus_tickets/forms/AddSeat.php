<?php
require_once __DIR__.'/../repository.php';
require_once __DIR__.'/Base.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Password;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;


class AddSeat extends Base
{
    public $title = '�������� �� �������:';

    protected $week = array('����������', '�������', '�����', '���������', '�����', '������', '������');

    public $default_error = '������� ������� ������';

    public function __construct($em, $post_data=array(), $files_data=array(), $initial=null)
    {
        $this->em = $em;
        $this->vehicle = Hidden::init();
        $this->number = Integer::init()
            ->add('value', '1')
            ->add('label', '�����')
            ->add('min', 0)
            ->add('step', 1);
        $this->row = Hidden::init()
            ->add('value', '1')
            ->add('min', 0)
            ->add('step', 1);
        $this->col = Hidden::init()
            ->add('value', '1')
            ->add('min', 0)
            ->add('step', 1);
        $this->blocked = Select::init()
            ->add('class', "form-control")
            ->add("id", "blocked")
            ->add('data', array('������', '���������'));
        $this->seat_id = Hidden::init();
        $this->action = Hidden::init()->add('value', 'change_seat');
        $this->submit = Submit::init()->add('value', '������');
        parent::__construct($post_data, $files_data, $initial);
    }

    public function save()
    {
        $vehicle = $this->em->getRepository('\Bus\models\Vehicle')
                        ->findOneBy(array('id' => $this->vehicle->value));
        
        $seat = $this->em->createQueryBuilder()->select('s')
            ->from('Bus\models\Seat', 's')
            ->where('s.vehicle = :vehicle_id')
            ->andWhere('s.row = :row_id')
            ->andWhere('s.col = :col_id')
            ->setParameter('vehicle_id', $vehicle->getId())
            ->setParameter('row_id', $this->row->value)
            ->setParameter('col_id', $this->col->value)
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();

        if(!$seat) {
            $seat = new \Bus\models\Seat();
        }

        $seat->setVehicle($vehicle);
        $seat->setNumber($this->number->value);
        $seat->setRow($this->row->value);
        $seat->setCol($this->col->value);
        $seat->setBlocked($this->blocked->value);
        $this->em->persist($seat);
        $this->em->flush();
        return array(
            'id' => $seat->getId(),
            'row' => $seat->getRow(),
            'col' => $seat->getCol(),
            'number' => $seat->getNumber(),
            'blocked' => $seat->isBlocked()
        );
    }
}
