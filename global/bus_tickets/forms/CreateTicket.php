<?php
require_once __DIR__.'/../repository.php';
require_once __DIR__.'/Base.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Date;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;



class CreateTicket extends Base
{
    public $default_error = '������� ������� ������';

    public function __construct($em, $post_data=array(), $files_data=array(), $initial=null)
    {
        $format = function($parent, $child) {
            return sprintf("%s[%s]", $parent, $child);
        };
        $this->em = $em;
        $passanger_name = 'passengers';
        $payer_name = 'payer';
        $this->passenger_fname= Text::init()
            ->add('label' ,'���')
            ->add_validator(NotEmpty::init(true)->err_msg('������������ ����'))
            ->add('name', $format($passanger_name, "firstName"));
        $this->passenger_lname= Text::init()
            ->add('label' ,'�������')
            ->add('name', $format($passanger_name, "lastName"));
        $this->passenger_phone= Text::init()
            ->add('label' ,'������� �������')
            ->add('name', $format($passanger_name, "phone"));
        $this->passenger_phone= Email::init()
            ->add('label' ,'���������� ����:')
            ->add('name', $format($passanger_name, "email"));
        $this->ticket_price= Text::init()
            ->add('label' ,'���� �� ������')
            ->add_validator(NotEmpty::init(true)->err_msg('�� ���� �� �������� ����� � ������ ��������'))
            ->add('name', $format($passanger_name, "ticket_price"));
        $this->ticket_type= Hidden::init()
            ->add('label' ,'')
            ->add('name', $format($passanger_name, "ticket_type"));
        $this->ticket_seat_id= Hidden::init()
            ->add('label' ,'')
            ->add('name', $format($passanger_name, "seat_id"));
        $this->tariff= Hidden::init()
            ->add('label' ,'')
            ->add('name', $format($passanger_name, "tariff"));
        $this->payer_fname = Text::init()
            ->add('label' ,'���')
            ->add('name', $format($payer_name, 'firstName'));
        $this->payer_lname = Text::init()
            ->add('label' ,'�������')
            ->add('name', $format($payer_name, 'lastName'));
        $this->payer_phone = Text::init()
            ->add('label' ,'�������')
            ->add('name', $format($payer_name, 'phone'));
        $this->payer_email = Text::init()
            ->add('label' ,'���������� ����')
            ->add('name', $format($payer_name, 'email'));

        $this->deparature_date = Hidden::init();
        $this->total_sum = Hidden::init();
        $this->route_name = Hidden::init();

        $this->action = Hidden::init()->add('value', 'create_ticket');
        $this->submit = Submit::init()->add('value', '������')
            ->add('onclick', 'return confirm(\'���������� ���� �������� �������� ��� ���� ���� �� �������� � ������ ���������� � ���� ������. ������� �� ���, �� ������ �� ����������?\')');
        parent::__construct($post_data, $files_data, $initial);
    }

    public function save()
    {
        $vehicle = $this->em->getRepository('Bus\models\Vehicle')
                         ->findOneBy(array('id' => $this->vehicle_id->value));
        if($vehicle) {
            $this->em->remove($vehicle);
            $this->em->flush();
            return true;
        }
        return false;
    }
}
