<?php
require_once __DIR__.'/../repository.php';
require_once __DIR__.'/Base.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Password;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;



class AddStop extends Base
{
    public $title = '�������� �� ���� ������';

    protected $type = array('�������� �����', '�������', '���������', '�����');

    public $default_error = '������� ������� ������';

    protected $stations = array();
    protected $em;
    public function __construct($em, $post_data=array(), $files_data=array(), $initial=null)
    {
        $this->em = $em;
        $stations_list = stations_list($em, COMPANY_ID);
        $stations = array();

        $this->description = Text::init()
            ->add('placeholder', '���������� ������� 96')
            ->add('label', '������ ��������')
            ->add_validator(Jokuf\Form\Validators\MaxLength::init(95)->err_msg('���������� ����������� ������� �� ���� ���� � 96 �������'));

        $this->rest_time = Integer::init()
            ->add('value', '1')
            ->add('label', '����������')
            ->add('min', 0)
            ->add('step', 1);

        $this->type = Select::init()
            ->add('label', '���')
            ->add('data', $this->type);
        $this->route_id = Hidden::init();
        $this->submit = Submit::init()->add('value', '������');
        $this->action = Hidden::init()->add('value', 'addStop');
        parent::__construct($post_data, $files_data, $initial);
    }

    public function save()
    {
        $start_station = array_key_exists($this->startStationId->value, $this->stations) ? $this->stations[$this->startStationId->value] : null;
        $end_station = array_key_exists($this->endStationId->value, $this->stations) ? $this->stations[$this->endStationId->value] : null;
        $direction = $this->em->getRepository('Bus\models\Direction')->findOneById($_GET['direction_id']);
        $route = new \Bus\models\Route();
        $route
            ->setDurationMinutes($this->duration_minutes->value)
            ->setDistanceKm($this->distance_km->value)
            ->setStartStationId($start_startion)
            ->setEndStationId($end_station)
            ->addDirection($direction)
        ;
        return $route;
    }
}
