<?php
require_once __DIR__.'/../repository.php';
require_once __DIR__.'/Base.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;



class AddPrice extends Base
{
    public $default_error = '������� ������� ������';

    public function __construct($em, $post_data=array(), $files_data=array(), $initial=null)
    {
        $this->em = $em;
        $ticket_types = array();
        foreach($this->em->getRepository('\Bus\models\TicketType')->findAll() as $type) {
            $ticket_types[$type->getId()] = $type->getName();
        }
        $this->ticketType = Select::init()->add('data', $ticket_types)->add('label', '��� �� ������');
        $this->price = Integer::init()->add('step', '0.01')->add('label', '����:');
        $this->routeId = Hidden::init();
        $this->action = Hidden::init()->add('value', 'addPrice');
        $this->submit = Submit::init()->add('value', '������')->add('class', 'btn btn-default');
        parent::__construct($post_data, $files_data, $initial);
    }

    public function save()
    {
        $route = $this->em->getRepository('Bus\models\Route')->findOneById($this->routeId->value);
        $ticketType = $this->em->getRepository('Bus\models\TicketType')->findOneById($this->ticketType->value);

        var_Dump($this->price->value);
        $routePrice = new \Bus\models\RoutePrice();
        $routePrice->setRoute($route);
        $routePrice->setPrice($this->price->value);
        $routePrice->setType($ticketType);
        $this->em->persist($routePrice);
        $this->em->flush();
    }
}
