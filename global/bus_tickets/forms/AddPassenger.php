<?php
require_once __DIR__.'/Base.php';
require_once __DIR__.'/../repository.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Date;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;



class AddPassenger extends Base
{
    public $default_error = '������� ������� ������';

    protected $payer;

    public function __construct($em, $post_data=array(), $files_data=array(), $initial=null)
    {
        $format = function($parent, $child) {
            return sprintf("%s[%s]", $parent, $child);
        };

        $this->em = $em;
        $passanger_name = 'passengers';
        $payer_name = 'payer';
        $this->firstName= Text::init()
            ->add('label' ,'���')
            ->add_validator(NotEmpty::init(true)->err_msg('������������ ����'));
        $this->lastName= Text::init()
            ->add_validator(NotEmpty::init(true)->err_msg('�� ���� �� ���� ������'))
            ->add('label' ,'�������');
        $this->phone= Text::init()
            ->add('label' ,'������� �������')
            ->add_validator(NotEmpty::init(true)->err_msg('�� ���� �� ���� ������'));
        $this->email= Email::init()
            ->add('label' ,'���������� ����:')
            ->add_validator(NotEmpty::init(true)->err_msg('�� ���� �� ���� ������'));
        $this->ticket_price= Text::init()
            ->add('label' ,'���� �� ������')
            ->add_validator(NotEmpty::init(true)->err_msg('�� ���� �� �������� ����� � ������ ��������'));
        $this->ticket_type= Hidden::init()
            ->add('label' ,'')
            ->add_validator(NotEmpty::init(true)->err_msg('��������� ��������'));
        $this->seat_id= Hidden::init()
            ->add('label' ,'')
            ->add_validator(NotEmpty::init(true)->err_msg('�� ���� �� ���� ������'));
        $this->tariff= Hidden::init()
            ->add('label' ,'')
            ->add_validator(NotEmpty::init(true)->err_msg('�� ���� �� ���� ������'));

        $this->submit = Submit::init()->add('value', '������');

        $this->action = Hidden::init()->add('value', 'add_passenger');
        parent::__construct($post_data, $files_data, $initial);
    }

    public function save()
    {
        $vehicle = $this->em->getRepository('Bus\models\Vehicle')
                         ->findOneBy(array('id' => $this->vehicle_id->value));
        if($vehicle) {
            $this->em->remove($vehicle);
            $this->em->flush();
            return true;
        }
        return false;
    }
}
