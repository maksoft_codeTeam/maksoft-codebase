<?php
require_once __DIR__.'/../repository.php';
require_once __DIR__.'/Base.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;



class AddPayer extends Base
{
    public $default_error = '������� ������� ������';

    public function __construct($em, $post_data=array(), $files_data=array(), $initial=null)
    {
        $format = function($parent, $child) {
            return sprintf("%s[%s]", $parent, $child);
        };
        $this->em = $em;
        $passanger_name = 'passengers';
        $payer_name = 'payer';
        $this->firstName = Text::init()
            ->add('label' ,'���');
        $this->lastName = Text::init()
            ->add('label' ,'�������');
        $this->phone = Text::init()
            ->add('label' ,'�������');
        $this->email = Email::init()
            ->add('label' ,'���������� ����');

        $this->deparature_date = Hidden::init();
        $this->total_sum = Hidden::init();
        $this->route_name = Hidden::init();

        $this->action = Hidden::init()->add('value', 'create_ticket');
        $this->submit = Submit::init()->add('value', '������');
        parent::__construct($post_data, $files_data, $initial);
    }

    public function save()
    {
        $vehicle = $this->em->getRepository('Bus\models\Vehicle')
                         ->findOneBy(array('id' => $this->vehicle_id->value));
        if($vehicle) {
            $this->em->remove($vehicle);
            $this->em->flush();
            return true;
        }
        return false;
    }
}
