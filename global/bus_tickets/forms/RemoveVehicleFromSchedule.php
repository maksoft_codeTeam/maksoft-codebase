<?php
require_once __DIR__.'/../repository.php';
require_once __DIR__.'/Base.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Date;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;



class RemoveVehicleFromSchedule extends Base
{
    public $default_error = '������� ������� ������';

    public function __construct($em, $post_data=array(), $files_data=array(), $initial=null)
    {
        $this->em = $em;
        $vehicles = vehicles_list($em, COMPANY_ID);
        $this->action = Hidden::init()->add('value', 'remove_vehicle_from_schedule');
        $this->vehicle_id = Hidden::init();
        $this->schedule_id = Hidden::init();
        $this->submit = Submit::init()->add('value', '������');
        parent::__construct($post_data, $files_data, $initial);
    }

    public function save()
    {
        $schedule = $this->em
                         ->getRepository('Bus\models\Schedule')
                         ->findOneBy(array('id' => $this->schedule_id->value));
        $vehicle = $this->em
                         ->getRepository('Bus\models\Vehicle')
                         ->findOneBy(array('id' => $this->vehicle_id->value));
        if($vehicle and $schedule) {
            $schedule->removeVehicle($vehicle);
            $this->em->persist($schedule);
            $this->em->flush();
            return true;
        }
        return false;
    }
}
