<?php
require_once __DIR__.'/../repository.php';
require_once __DIR__.'/Base.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Date;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;



class AddVehicle extends Base
{
    public $default_error = '������� ������� ������';
    public $title = '�������� �� ���� �������� ��������';

    protected $company_id;

    public function __construct($em, $post_data=array(), $company_id)
    {
        $this->company_id = $company_id;
        $this->em = $em;
        $this->action = Hidden::init()->add('value', 'add_vehicle');
        $this->make = Text::init()
            ->add('label', '�����');
        $this->model = Text::init()
            ->add('label', '�����');
        $this->license_plate = Text::init()
            ->add('label', '���. �����');
        $this->fuel = Select::init()
            ->add('data', array('diesel' => '�����', 'gasoline' => "������"))
            ->add('value', 'diesel')
            ->add('label', '������');
        $this->submit = Submit::init()->add('value', '������');
        parent::__construct($post_data);
    }

    public function save()
    {
        $vehicle = new Bus\models\Vehicle();

        $company = $this->em->getRepository('Bus\models\Company')
                         ->findOneBy(array('id' => $this->company_id));
        
        $vehicle->setCompany($company);
        $vehicle->setModel($this->model->value);
        $vehicle->setMake($this->make->value);
        $vehicle->setLicensePlate($this->license_plate->value);
        $vehicle->setFuel($this->fuel->value);
        $this->em->persist($vehicle);
        $this->em->flush();
        return false;
    }
}
