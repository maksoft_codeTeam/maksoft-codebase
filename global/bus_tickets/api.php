<?php
error_reporting(0);
require_once __DIR__.'/helpers.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
$request = Request::createFromGlobals();
$response = new JsonResponse();
$response->headers->set('Access-Control-Allow-Origin', '*');
$response->headers->set('Access-Control-Allow-Methods', 'GET,POST,PUT');
$response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');

$commands = array(
    'create_seat' => $add_seat,
    'seats_map' => $seats_map_api,
    'reserved_seats' => get_reserved_seats,

);

if(!isset($_GET['cmd']) or !array_key_exists($_GET['cmd'], $commands)) {
    $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
    $response->prepare($request);
    return $response->send();
}

$cmd = $_GET['cmd'];
try {
    $data = json_encode($commands[$cmd]($request, $em));
    $response->setContent(iconv('cp1251', 'utf8', $data));
} catch (Exception $e) {
    $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
    $response->setContent(json_encode($e->getMessage()));
}
$response->prepare($request);
$response->send();
