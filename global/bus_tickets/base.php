<link async rel='stylesheet' href='//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css'>
<link async rel="stylesheet" href='/global/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'>
<link async rel="stylesheet" href='/global/bootstrap-datetimepicker/datetimepicker.css'>
<script type='text/javascript' src='/global/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'></script>
<script type='text/javascript' src='/global/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.bg.js'></script>
<script type='text/javascript' src='//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js'></script>
<?php
require_once __DIR__.'/helpers.php';


if(!defined('COMPANY_ID') or user::$uLevel < $o_page->_page['SecLevel']) {
    print('������ ����� �� ������ �� ���� �����');
    return;
}
?>


<script>
jQuery(function() {
    jQuery(".hasDatepicker").datetimepicker({
        format: 'yyyy-mm-dd hh:ii'
    });
    jQuery('.table').DataTable();
});
</script>
