<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/lib/lib_page.php';
require_once __DIR__."/data_write.php";
if(session_id() == '') {
        session_start();
}
if (!isset($o_page)) {
    $o_page = new page();
    $db = page::getInstance();
}
/* Forces all GET and POST globals to register and be magically quoted.
* This forced register_globals and magic_quotes_gpc both act as if
* they were turned ON even if turned off in your php.ini file.
*
* Reason behind forcing register_globals and magic_quotes is for legacy
* PHP scripts that need to run with PHP 5.4 and higher.  PHP 5.4+ no longer
* support register_globals and magic_quotes, which breaks legacy PHP code.
*
* This is used as a workaround, while you upgrade your PHP code, yet still
* allows you to run in a PHP 5.4+ environment.
*
* Licenced under the GPLv2. Matt Kukowski Sept. 2013
*/

if (! isset($PXM_REG_GLOB)) {

    $PXM_REG_GLOB = 1;

    if (! ini_get('register_globals')) {
        foreach (array_merge($_GET, $_POST) as $key => $val) {
            global $$key;
            $$key = (get_magic_quotes_gpc()) ? $val : addslashes($val);
        }
    }
    if (! get_magic_quotes_gpc()) {
        foreach ($_POST as $key => $val) $_POST[$key] = addslashes($val);
        foreach ($_GET as $key => $val)  $_GET[$key]  = addslashes($val);
    }
}
?>
