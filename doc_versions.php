<?php
session_start();
require_once __DIR__."/lib/Database.class.php";
require_once __DIR__."/lib/lib_page.php";

$o_page = new page();
if($o_page->_user['AccessLevel'] < 1 or !$o_page->_user['slID']){
    exit("you must be logged");
}

if(!isset($_GET['fID'], $_GET['daID'])){
    exit("insufficient number of parameters");
}

$db = new Database();
$stmt = $db->prepare("
    SELECT * FROM doc_versions WHERE doc_access_id = :daID
    ");
$stmt->bindValue(":daID", $_GET['daID']);
$stmt->execute();
$doc = $stmt->fetch(\PDO::FETCH_OBJ);

echo $doc->doc_text;
