<?php

// daily cron task
// generate Net Service analitycs written in mails_outbox BD table
require_once("../lib/lib_cron.php");
require_once("../lib/lib_user.php");
require_once("../lib/PHPMailer/PHPMailerAutoload.php");


//$o_cron = new cron();
$o_user = new user(); 

$mail_outbox_query = $o_user->db_query("SELECT * FROM mails_outbox WHERE mail_sent = '0000-00-00 00:00:00' AND mail_created<NOW() ORDER BY mail_priority ASC, mail_created ASC LIMIT 5 "); 
while($row_mail = mysqli_fetch_object($mail_outbox_query)) {
	  $o_user->mime_mail(); 
      $o_user->mail_from = $row_mail->mail_reply_to; 
	  $o_user->mail_subject = $row_mail->mail_subject;
	  $o_user->mail_ctype = $row_mail->mail_ctype; 
	  $o_user->mail_to = $row_mail->mail_to; 
	  $o_user->mail_body = iconv("CP1251","UTF-8",$row_mail->mail_text);		  


// phpMailer
// https://github.com/PHPMailer/PHPMailer

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

// https://github.com/PHPMailer/PHPMailer/blob/master/test/phpmailerTest.php#L1251

/*

$mail->DKIM_domain = 'example.com';
$mail->DKIM_private = '/path/to/my/private.key';
$mail->DKIM_selector = 'phpmailer';
$mail->DKIM_passphrase = '';
$mail->DKIM_identifier = $mail->From;



$mail->setLanguage('bg', '../lib/PHPMailer/language/directory/'); 
$mail->Mail->Priority = 3;
$mail->Mail->Encoding = '8bit';
*/
$mail->CharSet = 'UTF-8';

$mail->isSMTP();                                      // Set mailer to use SMTP
//$mail->Host = 'leo.cmailpro.net';  // Specify main and backup SMTP servers
$mail->Host = 'server.maksoft.net';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'cc@maksoft.bg';                 // SMTP username
$mail->Password = 'maksoft@cc';                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 25;                                    // TCP port to connect to = 587;   

$mail->From = 'cc@maksoft.bg';
$mail->FromName = 'Maksoft - Customer Care';

$emails = explode(",",$row_mail->mail_to, 10); 
$i=1; 
foreach($emails as $email) {
	if($i==1) {
		$mail->addAddress($email);     // Add a recipient
	} else {
		$mail->addCC($email);     // Add a recipient
	}
	$i++; 	
} 
//$mail->addAddress('ellen@example.com');               // Name is optional
$mail->addReplyTo('mk@maksoft.net');

/*
$mail->addCC('cc@example.com');
$mail->addBCC('bcc@example.com');

$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML
$mail->Body    = 'This is the HTML message body <b> курсив in bold!</b>';
*/

$mail->Subject = $row_mail->mail_subject;
$mail->Body    =  iconv("CP1251","UTF-8",$row_mail->mail_text);		
$mail->AltBody =  strip_tags($mail->Body);		

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    //echo 'Message has been sent';
	$o_user->db_query("UPDATE mails_outbox SET mail_sent = NOW()  WHERE mID='$row_mail->mID'  "); 
	sleep(3);
}

	 // $o_user->mail_send(); 
}

$o_user->_mysql_db();
unset($o_user);
unset($mail);

?>