<?php

// daily cron task
// generate Net Service analitycs written in mails_outbox BD table

require_once("../lib/lib_cron.php");
require_once("../lib/lib_site.php");

$o_cron = new cron();
$o_seo = new seo(); 

// all sites
//$sites_q = $o_cron->db_query("SELECT DISTINCT(SiteID) FROM keywords WHERE keywords.visits>5 ORDER BY enter_time DESC LIMIT 10   "); 

// Net Service Sites
$i = rand(1,2); 
if($i==1) $sites_q = $o_cron->db_query("SELECT DISTINCT(keywords.SiteID) FROM keywords left join hostCMS on hostCMS.SiteID=keywords.SiteID WHERE keywords.SiteID>0 AND keywords.visits>5 AND hostCMS.active>0  AND hostCMS.toDate>NOW() AND hostCMS.netservice>0  ORDER BY enter_time DESC LIMIT 10 "); 
if($i==2) $sites_q = $o_cron->db_query("SELECT *  FROM Sites left join hostCMS on hostCMS.SiteID=Sites.SitesID WHERE Sites.SitesID>0 AND  hostCMS.active>0  AND hostCMS.toDate>NOW() AND hostCMS.netservice>0  ORDER BY RAND() LIMIT 10 "); 

//$sites_q = $o_cron->db_query("SELECT *  FROM Sites WHERE SitesID='334'  "); 

while ($row_sites = mysqli_fetch_object($sites_q) ) {

	//echo($row_sites->SiteID);  echo("<br>"); 
	$SiteID = $row_sites->SiteID; 
	
	$site_q = $o_cron->db_query("SELECT * FROM Sites WHERE SitesID='$SiteID' "); 
	while($row_site = mysqli_fetch_object($site_q)) {
		$mail_to = $row_site->EMail; 
		$users_q = $o_cron->db_query("SELECT DISTINCT(users.EMail) FROM SiteAccess  left join users on SiteAccess.userID=users.ID WHERE SiteID='$SiteID'  AND users.EMail<>'$row_site->EMail' AND SiteAccess.WriteLevel>=2 "); 
		while($row_users = mysqli_fetch_object($users_q)) {
			if(strlen($row_users->EMail)>5)  $mail_to = "$mail_to , $row_users->EMail "; 
		}
		$mail_outbox_q = $o_cron->db_query("SELECT * FROM mails_outbox WHERE mail_to='$mail_to' AND mail_created > DATE_SUB(CURDATE(), INTERVAL 14 DAY) ") ;
		if(mysqli_num_rows($mail_outbox_q)==0)  { // niama  generiran v mail_outbox mail v poslednite 14 dni
				//$mail_to = "mk@maksoft.net, ad@maksoft.net"; 
				$mail_subject = "Net Service info ".$row_site->primary_url; 
				$mail_reply_to = "Maksoft Net <mk@maksoft.net>"; 
				
				$keywords_last_q = $o_cron->db_query("SELECT DISTINCT (keywords) FROM keywords WHERE SiteID = '$SiteID' AND enter_time>DATE_SUB(CURDATE(),INTERVAL 30 DAY)   ORDER BY visits DESC  LIMIT 30 "); 
				$top_pages_new = $o_cron->db_query("SELECT * FROM pages WHERE  SiteID = '$SiteID' AND preview>10 AND date_added >DATE_SUB(CURDATE(),INTERVAL 45 DAY)  ORDER BY preview  DESC LIMIT 10 "); 
				$top_pages_modified = $o_cron->db_query("SELECT * FROM pages WHERE  SiteID = '$SiteID' AND date_modified >DATE_SUB(CURDATE(),INTERVAL 45 DAY)  ORDER BY preview  DESC LIMIT 10 "); 
		
				$mail_text = ' 
				<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
				<html>
				<head>
				<title>Net Service info</title>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				</head>
				
				<body>
				<p>�������� �������, </p>
				<p>��������� ����, <br>
				  �� �����<strong> Net Service</strong> ������
				  <br>
				  ��� ������ ���� '.$row_site->primary_url .'  <br>� �������� ���-�����
				  �������� ���� ���������� ������� ��� ��: <br><br>
				  '; 
				  while ($row_k = mysqli_fetch_object($keywords_last_q))  {
				  if(strlen($row_k->keywords)<8)  $row_k->keywords = "<b>$row_k->keywords</b>"; 
					$mail_text = "$mail_text $row_k->keywords, "; 
				  }
				  $mail_text = $mail_text.'
				</p><p><b>�����!</b> ���� ������ �������� ������ �������� �� ���� ������������ � ����� �� ���������� � ����������� �� <b>��� ������</b> ������ �� ��������� �������� �����������. <br>
				��� ����� �� �������� ��� �������� �� ������� �� ������ ���� ������, <a href="http://seo.maksoft.net">���� �������� �� � ���</a>! </p>
				<p>������� ���������� � �������� �� ��� � ������ ������ �� ������� � ������� ��
				  ����� <strong>Net Service</strong> ����� �� ��������� �� ����� <br>http://'.$row_site->primary_url .'/admin<br>
				  ������������� / ���������� / ������� ����
				</p>
				'; 
		
				if(mysqli_num_rows($top_pages_new)>0) {
					 $mail_text = "$mail_text		<p><strong>���-������������ ���� �������� ��� ������ ���� ��: </strong></p>			 <table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"; 
					while($row = mysqli_fetch_object($top_pages_new) ) {
						 $mail_text = "$mail_text				  <tr><td><a href=\"http://$row_site->primary_url/page.php?n=$row->n&SiteID=$SiteID\">$row->Name</a> / $row->preview ���������&nbsp;</td></tr>" ; 
					}
					$mail_text = "$mail_text 	    </table> "; 
				} else {
					 $mail_text = "$mail_text		<p><strong>��������! ���� ���������� 30 ��� ��� �� ��� �������� ���� �������� ��� ������ ����</strong><br>
					��������� �������� �� ���� ���������� � �����  � ����� �� ������ ������ ������� � Google! </p>"; 
				}
				
				if(mysqli_num_rows($top_pages_modified)>0) {
					 $mail_text = "$mail_text		<p><strong>���-������������ ������������� �������� ��� ������ ���� ��: </strong></p>			 <table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> "; 
					while($row = mysqli_fetch_object($top_pages_modified) ) {
						 $mail_text = "$mail_text				  <tr><td><a href=\"http://$row_site->primary_url/page.php?n=$row->n&SiteID=$SiteID\">$row->Name</a> / $row->preview ���������&nbsp;</td></tr>" ; 
					}
					$mail_text = "$mail_text 	    </table> "; 
				} else {
					 $mail_text = "$mail_text		<p><strong>��������! ���� ���������� 30 ��� ��� �� ��� ������������� ���������� � ����� ��.</strong><br>
					�������������� �� ������������ � ����� �� � �� ������� �������� �� �������������! </p>"; 
				}
			
				  $mail_text = $mail_text.'
					<p></p><p>��� ����� ������� ����������� ����������, ������� �� ����������� ������������
				  � ����� �� ���� ��� �� ��������� ���� �� ���, ���� �� �� ���������
				  �� ��������� �� ��������: <br>
				  02 846 46 46 <br>
				  0886 846 006<br>
			  0896 846 006                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </p>
				<p><a href="www.maksoft.net">������� ���</a> �������� ������ �������� ������� �� 1999 ������.<br>
				  ����� �� � �� ������ �� �������� ������ �������, � �� �� ��������<br>
				  ������ ������� � ������� ������ � ������ ������ ������� ����������
				  ��<br>
					  ������ ������� ������.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          </p>		
				<hr>
				<p><em><font size="-1">��� ���������� ���� ���������� ���� ���� �� ������ Net Service ����� �� ������ ��������� �� �������� ����. ��� ������� �� �� ��������
					  �� ���� ������� ������ �� �� ��������� ��� subject: Unsubscribe '.$SiteID.' � �������
					  �� ���� ���� </font></em></p>
				</body>
				</html>
				'; 
				$o_site_query_text = "INSERT INTO mails_outbox  SET  mail_to = '$mail_to', mail_subject = '$mail_subject', mail_reply_to = '$mail_reply_to', mail_text = '$mail_text', mail_priority = 3 "; 
				//echo $o_site_query_text; 
				$o_cron->db_query("$o_site_query_text"); 
		} // end proverka za generiran naskoro mail kum sushtiat mail_to 
	}

}



?>