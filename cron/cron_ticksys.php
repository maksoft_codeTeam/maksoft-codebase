<?php
require_once $_SERVER['DOCUMENT_ROOT']."/lib/Database.class.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/prettytable.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/lib_functions.php";
$db = new Database();
$template = array(0 => "<!DOCTYPE html>
                        <html lang='en'>
                        <head>
                            <meta charset='utf-8'>
                            <title>%s</title>",

                  1 =>     "<p>Ako ne razchitate informaciata molia otvorete tozi link<br>
                            <a href='http://%s'>%s</a></p>
                            <p>&nbsp;</p>
                        </head>",

                  2 => "<body>%s</body></html>");

$title = "Informatsiya po poddrazhka na informatsionna sistema";
$message = array(
    " <p>�������� �������,</p><p>�� ���������� %s ��� ���� ������ ������������� ������� �� ������������ %s ����� �� �������� ",
    "� �.�. <b>%s</b> ������� ��������, �� ����� ��� ��������� ���������� ������ ����� ������: </p><br>");
$days = 7;
#$past_week = ;
$past_week = date('Y-m-d', time() - ($days * 24 * 60 * 60));
$now       = date('Y-m-d', time());
$select_company = "SELECT DISTINCT SiteID FROM ticksys_payments
                   WHERE created BETWEEN :created_date AND NOW()";
$stmt = $db->prepare($select_company);
$stmt->bindValue(":created_date", $past_week);
$stmt->execute();
$fields = array("���", "Email", "�������", "������", "�����", "����� �� ����", "����");
$table = new PrettyTable();
$table->set_field_names($fields);
while ($company = $stmt->fetch(PDO::FETCH_OBJ)) {
    $email = array();
    $select_query = "SELECT * FROM ticksys_payments
                     WHERE SiteID = :site_id
                     AND created BETWEEN :created_date AND :now
                     GROUP BY (ticksys_payment)
                     ORDER BY created DESC";
    $stmt = $db->prepare($select_query);

    $stmt->execute(array(":site_id"      => $company->SiteID,
                         ":created_date" => $past_week,
                         ":now"          => $now));
    $tickets = $stmt->fetchAll();
    $count = count($tickets);
    if ($count > 0):
        $captured = 0;
        foreach ($tickets as $ticket):
        if (!strpos($ticket["ticksys_payment"], "CAPTURED")):
                $ticket["ticksys_payment"] = "CANC�LED";
                $ticket["ticket"] = '&nbsp;';
            else:
                $ticket["ticksys_payment"] = "CAPTURED";
                $ticket["ticket"] = "<a href='http://www.trans5.bg/bileti-online.html?paymentid=".$ticket["payment_id"]."&templ=260'><button class='btn btn-success btn-lg butto    n_submit '> ��� ����� </button></a>";
            $captured++;
            endif;
        $row = array($ticket["Name"],
                     $ticket["EMail"],
                     $ticket["Phone"], 
                     $ticket["ticksys_payment"],
                     $ticket['ticket'],
                     $ticket["kurs_id"],
                     $ticket["created"]);
        $table->add_row($row);
        endforeach;
        $email[] = sprintf($message[0], $days,  $count);
        $email[] = sprintf($message[1], $captured);
        $email[] =(string) $table;
        $table->clear_rows();
    else:
        $email[] = '.</p>';
    endif;
    $mail_RC = randomkeys(3);
    $stmt = $db->prepare("SELECT EMail FROM Sites WHERE SitesID=:site_id");
    $stmt->execute(array(":site_id" => $company->SiteID));
    $c = $stmt->fetch(PDO::FETCH_OBJ);
    $id = to_outbox(implode('', $email), $c->EMail, $mail_RC);
    $mail_url = sprintf("www.maksoft.net/email/mymail.php?mail_RC=%s&mID=%s",
                        $mail_RC, $id); 
    $template[0] = sprintf($template[0], $title);
    $template[1] = sprintf($template[1], $mail_url, $mail_url);
    $template[2] = sprintf($template[2], implode('', $email));
    update(implode('', $template), $id);
}


function to_outbox($content, $recipient, $mail_RC)
{   
    global $db;
    global $title;
    $o_site_query_text = "INSERT INTO mails_outbox 
                          SET  
                          mail_to = :mail_to, 
                          mail_subject = :mail_subject,
                          mail_itGroup =  15,
                          mail_reply_to = 'mk@maksoft.bg, cc@maksoft.bg',
                          mail_text = :mail_text,
                          mail_RC = :mail_RC, mail_priority = 3";

    $stmt = $db->prepare($o_site_query_text);
    $stmt->execute(array(":mail_to"      => $recipient,
                         ":mail_subject" => $title,
                         ":mail_text"    => $content,
                         ":mail_RC"      => $mail_RC));
    return $db->lastInsertId();
}


function update($content, $id)
{
    global $db;
    $update_query = "UPDATE mails_outbox
                     SET mail_text = :mail_text 
                     WHERE mID = :id";
    $stmt = $db->prepare($update_query);
    $stmt->execute(array(":mail_text" => $content, ":id" => $id));
}
