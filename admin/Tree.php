<?php


class Tree extends BlueM\Tree
{
    /**
     * Constructor.
     *
     * @param array $data    The data for the tree (array of associative arrays)
     * @param array $options [optional] Currently, the only supported key is "rootId"
     *                       (ID of the root node)
     */
    public function __construct(array $data, array $options = array())
    {
        $this->options = array_change_key_case($options, CASE_LOWER);
        if (!isset($this->options['rootid'])) {
            $this->options['rootid'] = 0;
        }
        $this->build($data);
    }

    

    private function build(array $data)
    {
        $children = array();
        $this->nodes[$this->options['rootid']] = $this->createNode(
            array(
                'id'     => $this->options['rootid'],
                'parent' => null,
            )
        );
        foreach ($data as $row) {
            $this->nodes[$row['id']] = $this->createNode($row);
            if (empty($children[$row['parent']])) {
                $children[$row['parent']] = array($row['id']);
            } else {
                $children[$row['parent']][] = $row['id'];
            }
        }
        foreach ($children as $pid => $childids) {
            foreach ($childids as $id) {
                if ((string)$pid === (string)$id) {
                    continue;
                }
                if (isset($this->nodes[$pid])) {
                    $this->nodes[$pid]->addChild($this->nodes[$id]);
                } else {
                    continue;
                }
            }
        }
    }

}
