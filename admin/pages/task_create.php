<section id="middle">
    <div id="content" class="dashboard padding-20">
        <header id="page-header">
            <h1>���� ������</h1>
            <ol class="breadcrumb">
                <li><a href="<?=$this->route->generate('login')?>">�������������</a></li>
                <li><a href="<?=$this->route->generate('tasks')?>">������</a></li>
                <li class="active">���� ������</li>
            </ol>
        </header>
        <!-- /page title -->
        <div id="content" class="dashboard padding-20">
            <div class="panel-body">
                <div class='row'>
                    <?=$createTaskForm->start();?>
                    <div class='col-md-12'>
                        <div class='row'>
                            <div class='col-md-6'> 
                            <?=$createTaskForm->priority->label?>
                            <?=$createTaskForm->priority?>
                            </div>
                            <div class='col-md-6'> 
                            <?=$createTaskForm->toUser->label?>
                            <?=$createTaskForm->toUser?>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class='row'>
                            <div class='col-md-6'> 
                            <?=$createTaskForm->startDate->label?>
                            <?=$createTaskForm->startDate?>
                            </div>
                            <div class='col-md-6'> 
                            <?=$createTaskForm->period->label?>
                            <?=$createTaskForm->period?>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class='row'>
                            <div class='col-md-6'> 
                            <?=$createTaskForm->type->label?>
                            <?=$createTaskForm->type?>
                            </div>
                            <div class='col-md-6'> 
                            <?=$createTaskForm->repeat_in->label?>
                            <?=$createTaskForm->repeat_in?>
                            </div> 
                        </div>     
                    </div>
                    <div class='col-md-12'>
                        <div class='row'>
                            <div class='col-md-6'> 
                            <?=$createTaskForm->timeToFinish->label?>
                            <?=$createTaskForm->timeToFinish?>
                            </div>
                            <div class='col-md-6'> 
                            <?=$createTaskForm->company->label?>
                            <?=$createTaskForm->company?>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <?=$createTaskForm->subject->label?>
                        <?=$createTaskForm->subject?>
                        <hr>
                    </div>
                    <div class='col-md-12'>
                        <textarea name="<?=$createTaskForm->content->name?>" class="summernote form-control" data-height="200" data-lang="en-US">
                            <?=$createTaskForm->content->value?>
                        </textarea>
                    </div>
                    <div class='col-md-12'>
                        <hr>
                        <button type="submit" class="btn btn-success btn-block">������</button>
                    </div>
                </div>
                <?=$createTaskForm->action;?>
                <?=$createTaskForm->end();?>
             </div>
        </div>
    </div>
</section>
