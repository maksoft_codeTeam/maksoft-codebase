<!-- MIDDLE -->

<section id="middle">
    <header id="page-header">
        <h1><?=$groupDetail['group_title']?></h1>
        <ol class="breadcrumb">
            <li><a href="<?=$this->route->generate('login')?>">�������������</a></li>
            <li><a href="<?=$this->route->generate('groups')?>">��������� �� ��������</a></li>
            <li class="active"><?=$groupDetail['group_title']?></li>
        </ol>
    </header>
    <div id="content" class="padding-20">
        <div class='panel panel-default'>
            <div class="panel-body">
                <div class='row'>
                <?php
                    if(count($childGroups)):
                        require_once __DIR__.'/../templates/list_child_groups.php';
                    endif;
                ?>
                    <div class="col-md-<?=count($childGroups) ? 5 : 9?>">
                        <div id="content" class="padding-20">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                <h6 class="nomargin"> <?=count($groupPages);?> ��������</h6>
                                    <hr class="nomargin-bottom margin-top-10">
                                    <?php foreach($groupPages as $page):?>
                                    <div class="clearfix search-result">
                                        <!-- item -->
                                        <h4><a href="#"><?=$page->Name?></a></h4>
                                        <small class="text-success">Dashboard / About</small>
                                        <p><?=cut_text($page->textStr);?>
                                        <button type='submit'><i class="fa fa-edit"></i> ��������</button>
                                        <?=$deleteGroupFromListing->end();?>
                                        <br>
                                        <br>
                                        <div class="btn-group">
                                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">�������� <span class="caret"></span></button>
                                          <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <?=$deleteForm->start();?>
                                                <?=$deleteForm->action;?>
                                                <?php $deleteForm->id->value = $page->id; ?>
                                                <?=$deleteForm->id;?>
                                                <button type='submit'><i class="fa fa-edit"></i> ��������</button>
                                                <?=$deleteForm->end();?>
                                            </li>
                                            <li><a target='_blank' href="<?=$this->route->generate('editPage', array('id' => $page->n))?>"><i class="fa fa-question-circle"></i> ����������</a></li>
                                            <li><a target='_blank' href="/page.php?n=<?=$page->n?>&SiteID=<?=$page->SiteID?>"><i class="fa fa-print"></i> ������</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-cogs"></i> Separated link</a></li>
                                          </ul>
                                        </div>
                                    </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h4><strong> <?=$createGroup->title?> </strong></h4>
                        <?=$createGroup?>
                        <h4><strong> <?=$form->title?> </strong></h4>
                        <?=$form?>
                        <hr>
                        <?=$deleteGroupForm->start()?>
                        <?=$deleteGroupForm->action?>
                        <button  type='submit' class="btn btn-warning btn-featured btn-inverse">
                            <span>������ �������� �����</span>
                            <i class="et-megaphone"></i>
                        </button>
                        <?=$deleteGroupForm->end()?>
                        <br>
                        <br>
                        <h4><strong>������ ��</strong> � ��������</h4>
                        <?php if(!count($pages)): ?>
                        <p><em>������� ��� ��� �� �� �������� � ����� ��������!</em></p>
                        <?php endif;?>
                        <?php foreach($pages as $page): ?>
                        <p><em><a target="_blank" href="/page.php?n=<?=$page->n?>&SiteID=<?=$page->SiteID?>"><?=$page->Name?></a></em></p>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$javascript = <<<'EOD'
<script type='text/javascript' src='https://code.jquery.com/ui/1.12.1/jquery-ui.min.js'></script>
<script>
 $( function() {
    $( "#pages" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "/api/index.php",
          data: {
            n: 1,
            command: 'autosuggest_pages',
            name: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 2,
          select: function( event, ui ) {
            $( "#pages" ).val( ui.item.id );
            $( "#page_id" ).val( ui.item.id );
          }
    } );
  } );
</script>
EOD;
\add_js($javascript);
?>
