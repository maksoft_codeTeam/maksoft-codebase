<section id="middle">
    <div id="content" class="dashboard padding-20">
        <h4>��� ���������� �� ��� <?=count($tmp_pages);?> ������� ��������.</h4>
        <div class="margin-bottom-20">
            <button type="button" class="btn btn-default" data-action="expand-all">Expand All</button>
            <button type="button" class="btn btn-default" data-action="collapse-all">Collapse All</button>
        </div>
        <div class="dd" id="nestable_list_1">
            <ol class="dd-list">
                <?php
                $rootNodes = $tree->getRootNodes(); 
                foreach($rootNodes as $topPage): ?>
                    <li class="dd-item dd3-item" data-id="<?=$topPage->id?>" data-parent="<?=$topPage->parent?>" data-sort="<?=$topPage->sort?>">
                        <div class="dd-handle dd3-handle"> </div>
                        <div class="dd3-content">
                             <?=$topPage->title?> [<?$topPage->preview?>] | [<a target="_blank" href="<?=$this->route->generate('editPage', array('id' => $topPage->id))?>">���������� </a>] 
                        </div>
                        <?php 
                            if($topPage->hasChildren()):
                                echo printSiteMap($topPage->getChildren(), $this->route);
                            endif;
                        ?>
                    </li>
                <?php endforeach; ?>
            </ol>
        </div>
    </div>
</section>
<?php
$javascript = <<<EOD
<script type="text/javascript" src="/admin/assets/plugins/nestable/jquery.nestable.js"></script>
<script type="text/javascript">
            loadScript(plugin_path + "nestable/jquery.nestable.js", function(){

                if(jQuery().nestable) {

                    var updateOutput = function (e) {
                        var list = e.length ? e : $(e.target),
                            output = list.data('output');
                        if (window.JSON) {
                            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                        } else {
                            output.val('JSON browser support required for this demo.');
                        }
                    };


                    // Nestable list 1

                    jQuery('#nestable_list_1').nestable({
                        group: 1,
                        dropCallback: function(details) {
                            var sourceId = details.sourceId;
                            var destinationId = $($(details.sourceEl).next()).data('id');
                            var destRoot = $($(details.sourceEl).next()).data('parent');
                             jQuery.get('/api/?n=1&command=swap_pages&n1=' + sourceId + '&n2=' + destinationId + '&n3=' + destRoot, function(data) {
                             });
                        }
                    }).on('change', updateOutput);

                    // output initial serialised data
                    updateOutput(jQuery('#nestable_list_1').data('output', jQuery('#nestable_list_1_output')));

                    // Expand All
                    jQuery("button[data-action=expand-all]").bind("click", function() {
                        jQuery('.dd').nestable('expandAll');
                    });

                    // Collapse All
                    jQuery("button[data-action=collapse-all]").bind("click", function() {
                        jQuery('.dd').nestable('collapseAll');
                    });

                }

            });
        </script>
EOD;

\add_js($javascript);
?>
