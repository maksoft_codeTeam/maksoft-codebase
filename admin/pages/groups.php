<!-- MIDDLE -->

<section id="middle">
    <header id="page-header">
        <h1>�����</h1>
        <ol class="breadcrumb">
            <li><a href="<?=$this->route->generate('login')?>">�������������</a></li>
            <li class="active">��������� �� ��������</li>
        </ol>
    </header>
    <div id="content" class="dashboard padding-20 margin-right-50">
        <div class='col-md-8'>
            <div class="dd" id="nestable_list_1">
                <ol class="dd-list">
                    <?php
                    $groups = $tree->getRootNodes(); 
                    foreach($groups as $group): ?>
                        <li class="dd-item dd3-item" data-id="<?=$group->id?>">
                            <div class="dd-handle dd3-handle"> </div>
                            <div class="dd3-content">
                                 <?=$group->title?> [<?=$group->created_at?>] 
                            <a  href="<?=$this->route->generate('groupDetail', array('group_id' => $group->id))?>"> 
                                    <span class='label label-success'>������� </span>
                            </a>
                             &nbsp; <span class="label label-info"> <?=$group->pages_count ? $group->pages_count.' ��������' : '������� � ������'?> </span>
                            </div>
                            <?php 
                                if($group->hasChildren()):
                                    echo printSiteGroups($group->getChildren());
                                endif;
                            ?>
                        </li>
                    <?php endforeach; ?>
                </ol>
            </div>
        </div>
        <div class='col-md-4'>
            <h4> �������� �� ���� ����� </h4>
            <?=$form?>
        </div>
    </div>
</section>
<?php
$javascript = <<<EOD
<script type="text/javascript" src="/admin/assets/plugins/nestable/jquery.nestable.js"></script>
<script type="text/javascript">
            loadScript(plugin_path + "nestable/jquery.nestable.js", function(){

                if(jQuery().nestable) {

                    var updateOutput = function (e) {
                        var list = e.length ? e : $(e.target),
                            output = list.data('output');
                        if (window.JSON) {
                            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                        } else {
                            output.val('JSON browser support required for this demo.');
                        }
                    };


                    // Nestable list 1
                    jQuery('#nestable_list_1').nestable({
                        group: 1
                    }).on('change', updateOutput);

                    // output initial serialised data
                    updateOutput(jQuery('#nestable_list_1').data('output', jQuery('#nestable_list_1_output')));

                    // Expand All
                    jQuery("button[data-action=expand-all]").bind("click", function() {
                        jQuery('.dd').nestable('expandAll');
                    });

                    // Collapse All
                    jQuery("button[data-action=collapse-all]").bind("click", function() {
                        jQuery('.dd').nestable('collapseAll');
                    });

                }

            });
        </script>
EOD;

\add_js($javascript);
?>
