<section id="middle">
<!-- page title -->
<header id="page-header">
    <h1>������</h1>
    <ol class="breadcrumb">
        <li><a href="<?=$this->route->generate('login')?>">�������������</a></li>
        <li class="active">������</li>
    </ol>
</header>
<!-- /page title -->
    <div id="content" class="dashboard padding-20">

            <div class="panel-body">
                <div class='row'>
                    <div class="col-md-12">
                        <div id="panel-ui-tan-l4" class="panel panel-default">
                            <div class="panel-heading">
                                <!-- tabs nav -->
                                <ul class="nav nav-tabs pull-left">
                                    <li class="active">
                                    <a href="#ttab1l_nobg" data-toggle="tab" aria-expanded="true">������� <span class="label label-danger"> <?=count($activeTasks)?></span></a>
                                    </li>
                                    <li class="">
                                    <a href="#ttab2l_nobg" data-toggle="tab" aria-expanded="false">������� <span class="label label-danger"> <?=count($expiredTasks)?></span></a>
                                    </li>
                                    <li class="">
                                    <a href="#ttab3l_nobg" data-toggle="tab" aria-expanded="false"> ��������� <span class="label label-danger"> <?=count($repeatedTasks)?></span></a>
                                    </li>
                                    <li class="">
                                    <a href="#ttab4l_nobg" data-toggle="tab" aria-expanded="false"> ��������� ������ <span class="label label-danger"> <?=count($assignedTasks)?></span></a>
                                    </li>
                                    <li class="">
                                    <a href="#ttab5l_nobg" data-toggle="tab" aria-expanded="false"> ���������� ������ <span class="label label-danger"> <?=count($upcomingTasks);?></span></a>
                                    </li>
                                </ul>
                                <!-- /tabs nav -->

                            </div>

                            <!-- panel content -->
                            <div class="panel-body">

                                <!-- tabs content -->
                                <div class="tab-content transparent">
                                    <div id="ttab1l_nobg" class="tab-pane active">
                                        <?php
                                            $tasks = $activeTasks;
                                            include __DIR__.'/../templates/tasks_table.php';
                                        ?>
                                    </div>

                                    <div id="ttab2l_nobg" class="tab-pane">
                                        <?php
                                            $tasks = $expiredTasks;
                                            include __DIR__.'/../templates/tasks_table.php';
                                        ?>
                                    </div>
                                    <div id="ttab3l_nobg" class="tab-pane">
                                        <?php
                                            $tasks = $repeatedTasks;
                                            include __DIR__.'/../templates/tasks_table.php';
                                        ?>
                                    </div>
                                    <div id="ttab4l_nobg" class="tab-pane">
                                        <?php
                                            $tasks = $assignedTasks;
                                            include __DIR__.'/../templates/task_table_assigned.php';
                                        ?>
                                    </div>
                                    <div id="ttab5l_nobg" class="tab-pane">
                                        <?php
                                            $tasks = $upcomingTasks;
                                            include __DIR__.'/../templates/tasks_table.php';
                                        ?>
                                    </div>

                                </div>
                                <!-- /tabs content -->

                            </div>
                            <!-- /panel content -->
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <a href="<?=$this->route->generate('createTask')?>" class="btn btn-default btn-featured btn-inverse">
                            <span>���� ������</span>
                            <i class="et-pencil"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
