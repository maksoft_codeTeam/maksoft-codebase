<section id="middle">
    <div id="content" class="dashboard padding-20">

        <!-- 
            PANEL CLASSES:
                panel-default
                panel-danger
                panel-warning
                panel-info
                panel-success

            INFO:   panel collapse - stored on user localStorage (handled by app.js _panels() function).
                    All pannels should have an unique ID or the panel collapse status will not be stored!
        -->
        <div id="panel-1" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong> ��������� �� ��� ���� �������� ����� </strong>
                </span>

                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                </ul>
                <!-- /right options -->

            </div>

            <!-- panel content -->
            <div class="panel-body">
                <canvas id="myChart" width="100%" height="30%"></canvas>
            </div>
            <!-- /panel content -->

            <!-- panel footer -->
            <div class="panel-footer">

                <!-- 
                    .md-4 is used for a responsive purpose only on col-md-4 column.
                    remove .md-4 if you use on a larger column
                -->
                <ul class="easypiecharts list-unstyled">
                    <li class="clearfix">
                    <span class="stat-number"><?=$activePagesCount?></span>
                        <span class="stat-title">������� ��������</span>

                        <span class="easyPieChart" data-percent="86" data-easing="easeOutBounce" data-barColor="#F8CB00" data-trackColor="#dddddd" data-scaleColor="#dddddd" data-size="60" data-lineWidth="4">
                            <span class="percent"></span>
                        </span> 
                    </li>
                    <li class="clearfix">
                    <span class="stat-number"><?=$registedUsersCount?></span>
                        <span class="stat-title">������������ �����������</span>

                        <span class="easyPieChart" data-percent="59.83" data-easing="easeOutBounce" data-barColor="#F86C6B" data-trackColor="#dddddd" data-scaleColor="#dddddd" data-size="60" data-lineWidth="4">
                            <span class="percent"></span>
                        </span> 
                    </li>
                    <li class="clearfix">
                        <span class="stat-number">12%</span>
                        <span class="stat-title">Canceled Orders</span>

                        <span class="easyPieChart" data-percent="12" data-easing="easeOutBounce" data-barColor="#98AD4E" data-trackColor="#dddddd" data-scaleColor="#dddddd" data-size="60" data-lineWidth="4">
                            <span class="percent"></span>
                        </span> 
                    </li>
                    <li class="clearfix">
                        <span class="stat-number">97%</span>
                        <span class="stat-title">Positive Feedbacks</span>

                        <span class="easyPieChart" data-percent="97" data-easing="easeOutBounce" data-barColor="#0058AA" data-trackColor="#dddddd" data-scaleColor="#dddddd" data-size="60" data-lineWidth="4">
                            <span class="percent"></span>
                        </span> 
                    </li>
                </ul>

            </div>
            <!-- /panel footer -->

        </div>
        <!-- /PANEL -->



        <!-- BOXES -->
        <div class="row">

            <!-- Feedback Box -->
            <div class="col-md-3 col-sm-6">
                <div class="box danger"><!-- default, danger, warning, info, success -->
                <a href="<?=$this->route->generate('createTask')?>" class="btn btn-danger btn-featured">
                        <span>���� ������</span>
                        <i class="et-edit"></i>
                    </a>
                </div>

            </div>

            <!-- Profit Box -->
            <div class="col-md-3 col-sm-6">
            <div class="box <?=count($latestQueries) ? 'success' : 'warning' ?>"><!-- default, danger, warning, info, success -->
                    <a href="<?=$this->route->generate('siteRequests')?>" class="btn btn-<?=count($latestQueries) ? 'success' : 'warning' ?> btn-featured">
                    <?php if(count($latestQueries)): ?>
                    <span> ����� <?=count($latestQueries)?> <?=count($latestQueries) > 1 ? '���� ����������':'���� ���������' ?></span>
                        <i class="et-ribbon"></i>
                    </a>
                    <?php else: ?>
                    <span> ������ ���� ����������</span>
                        <i class="et-ribbon"></i>
                    </a>
                    <?php endif;?>
                </div>

            </div>

            <!-- Orders Box -->
            <div class="col-md-3 col-sm-6">

                <!-- BOX -->
                <div class="box default"><!-- default, danger, warning, info, success -->

                    <div class="box-title"><!-- add .noborder class if box-body is removed -->
                        <h4>58944 Orders</h4>
                        <small class="block">18 New Orders</small>
                        <i class="fa fa-shopping-cart"></i>
                    </div>

                    <div class="box-body text-center">
                        <span class="sparkline" data-plugin-options='{"type":"bar","barColor":"#ffffff","height":"35px","width":"100%","zeroAxis":"false","barSpacing":"2"}'>
                            331,265,456,411,367,319,402,312,300,312,283,384,372,269,402,319,416,355,416,371,423,259,361,312,269,402,327
                        </span>
                    </div>

                </div>
                <!-- /BOX -->

            </div>

            <!-- Online Box -->
            <div class="col-md-3 col-sm-6">

                <!-- BOX -->
                <div class="box success"><!-- default, danger, warning, info, success -->

                    <div class="box-title"><!-- add .noborder class if box-body is removed -->
                        <h4>3485 Online</h4>
                        <small class="block">78185 Unique visitors today</small>
                        <i class="fa fa-globe"></i>
                    </div>

                    <div class="box-body text-center">
                        <span class="sparkline" data-plugin-options='{"type":"bar","barColor":"#ffffff","height":"35px","width":"100%","zeroAxis":"false","barSpacing":"2"}'>
                            331,265,456,411,367,319,402,312,300,312,283,384,372,269,402,319,416,355,416,371,423,259,361,312,269,402,327
                        </span>
                    </div>

                </div>
                <!-- /BOX -->

            </div>

        </div>
        <!-- /BOXES -->



        <div class="row">
        <?php if(count($inquiries)): ?>
            <div class="col-md-6">
                <div id="panel-3" class="panel panel-success">
                    <div class="panel-heading">
                        <span class="title elipsis">
                            <strong>�������� 10 ����������</strong> <!-- panel title -->
                        </span>
                    </div>

                    <div class="panel-body">
                        <ul class="list-unstyled list-hover slimscroll height-300" data-slimscroll-visible="true">
                            <?php foreach($inquiries as $sReq): ?>
                            <div class="clearfix search-result"><!-- item -->
                            <h4>
                                <a  href="/admin/inquiries/"><?=$sReq->Name?> | <?=$title?> 
                                    <?php
                                    switch($sReq->ftype){ 
                                    case 0:
                                        echo '<span class="label label-danger">����</span>';
                                        break;
                                    case 1:
                                        echo '<span class="label label-success">���������</span>';
                                        break;
                                    case 2:
                                        echo '<span class="label label-warning">��������</span>';
                                        break;
                                    case 3:
                                        echo '<span class="label label-default">����������</span>';
                                        break;
                                    }
                                    ?>
                                </a></h4>
                                <small class="text-success">����: <?=$sReq->Data?></small>
                                <small class="text-success">�� ��������: <a target='_blank' href='/page.php?n=<?=$sReq->n?>&SiteID=<?=$sReq->SiteID?>'> <?=$sReq->Name?></a></small>
                                <p> <?=\cut_text(stripslashes($sReq->Zapitvane));?></p>
                            </div>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <a href="<?=$this->route->generate('siteRequests', array())?>"><i class="fa fa-arrow-right text-muted"></i> ������ ����������</a>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="col-md-6">
                <div id="panel-3" class="panel panel-success">
                    <div class="panel-heading">
                        <span class="title elipsis">
                            <strong>�������� ����������� ��������</strong> <!-- panel title -->
                        </span>
                    </div>

                    <div class="panel-body">
                        <ul class="list-unstyled list-hover slimscroll height-300" data-slimscroll-visible="true">
                            <?php foreach($lastEditedPages as $page): ?>
                            <li>
                                <span class="label label-info"><i class="glyphicon glyphicon-edit"></i></span>
                               <a target="_blank" href="/page.php?n=<?=$page->n?>&SiteID=<?=$page->SitesID?>"> <?=$page->Name?> </a> |
                               [<a target="_blank" href="<?=$this->route->generate('editPage', array('id' => $page->n))?>">���������� </a>] 
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <a href="<?=$this->route->generate('sitemap', array())?>"><i class="fa fa-arrow-right text-muted"></i> ����� �� �����</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
        <?php if($isMaksoftEmployee and $isMaksoftEmployee->perm > 0 ): ?>
            <div class="col-md-6">
                <div id="panel-3" class="panel panel-success">
                    <div class="panel-heading">
                        <span class="title elipsis">
                            <strong>����� ������</strong> <!-- panel title -->
                        </span>
                    </div>

                    <div class="panel-body">
                        <ul class="list-unstyled list-hover slimscroll height-300" data-slimscroll-visible="true">
                            <?php foreach($tasks as $task): ?>
                            <li>
                                <span class="label label-info"><i class="et et-edit size-15"></i></span>
                               <a target="_blank" href="<?=$this->route->generate('taskDetail', array('task_id' => $task->taskID))?>"> <?=$task->subject?> </a> |
                               <span class="label label-info"> <?=$task->Name?> </span>
                            <?php if($expired($task)): ?>
                             <span class="label label-warning"> ��������� </span>
                            <?php endif;?>
                            <?php if($countNewComments($task)): ?> 
                             <span class="label label-success"> ��� �������� </span>
                            <?php endif;?>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="panel-footer">
                    <a href="<?=$this->route->generate('tasks', array())?>"><i class="fa fa-arrow-right text-muted"></i> ������ ������</a>
                    </div>
                </div>
            </div>
            <?php endif;?>
        </div>

    </div>
</section>

<?php
$javascript = <<<'EOD'

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js" integrity="sha256-SiHXR50l06UwJvHhFY4e5vzwq75vEHH+8fFNpkXePr0=" crossorigin="anonymous"></script>

<script type="text/javascript">
function convert_date(date) {
    var today = new Date(date);
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    return dd + '.' + mm + '.' + yyyy;
}
$(function() {
    get('/api/?n=1&command=getSiteVisitsDaily')
        .then(JSON.parse)
        .then(function(pages) {
            var data = {labels: [], dataset: []};
            for(i=0; i<pages.length; i++) {
                data['labels'].push(convert_date(pages[i].viewTime));
                data['dataset'].push(pages[i].visits);
            }
            return data;
        })
        .then(function(data){
            var ctx = $("#myChart");
            var myLineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: data.labels,
                    fillColor: "rgba(220,220,220,0)",
                    strokeColor: "rgba(220,180,0,1)",
                    pointColor: "rgba(220,180,0,1)",
                    datasets: [{
                        label: '���� ���������',
                        data: data.dataset,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
            });
        });
});
</script>
EOD;
\add_js($javascript);
?>
