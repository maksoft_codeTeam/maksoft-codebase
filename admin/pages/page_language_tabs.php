<div id="<?=$lang['language_id']?>" class="tab-pane">
    <?php 
        $form->set_id('lang'.$lang['language_id']);;
        $form->set_action('/forms/global/save.php');
        echo $form->start();
        $row_no = $o_page->get_page($page_id, 'object', $lang['language_id']);
        $query = "
            SELECT * 
            FROM  `pages_text` 
            WHERE  `n` = $page_id
            AND lang_id = {$lang['language_id']}
            ";
        $row_no = $o_page->fetch('object', $o_page->db_query($query));
        #include __DIR__.'/page/page_texts_panel.php';
        $form->lang_id->value = $lang['language_id'];
        echo $form->action;
    ?>
    <div class='row'>
        <div class='col-md-6'>
            <label for='NamePage'> ��� �� ����������</label>
            <input name="NamePage" class='form-control' id="p_name" type="text" value="<?php echo htmlspecialchars(stripslashes($row_no->Name));?>">
        </div>
        <div class="col-md-6"> </div>
        <div class='col-md-6'>
          <label for='title'> ��������</label>
            <input name="title" class='form-control'  type="text" value="<?php echo htmlspecialchars(stripslashes($row_no->title));?>">
        </div>
        <div class="col-md-6">
          <label for='slug'> ������ ���</label>
          <input name="slug" class='form-control' type="text" value="<?php echo stripslashes($row_no->slug);?>">
        </div>
    </div>
    <div class='row'>  
        <div class="col-md-12"> 
            <label for='<?=$createPageForm->textStr->name?>'> ����� �� ����������</label>
            <textarea class="summernote form-control" data-height="200" data-lang="en-US" name="<?=$createPageForm->textStr->name?>">
                <?php echo stripslashes($row_no->textStr);?>
            </textarea>
        </div>
        <div class="col-md-12">
            <label for='tags'> ������� ����</label>
            <input name="tags" class='form-control' type="text" id="tags" value="<?=stripslashes($row_no->tags) ?>">
         </div>
         <div class='col-md-12'>
            <?php $createPageForm->submit->class='btn btn-default btn-lg btn-block';?>
            <?=$createPageForm->submit?>
         </div>
            <input type='hidden' name='lang_id' value='<?=$lang['language_id'];?>'>
            <input type="hidden" name="no" value="<?=$page_id?>">
            <input type="hidden" name="SiteID" id="SiteID" value="<?=$SiteID?>">
        </form>
    </div>
</div>
<!-- /TAB 1 CONTENT -->
