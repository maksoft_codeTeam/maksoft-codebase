<?php
$default_image="https://lh3.googleusercontent.com/-H7_QuNyFVMM/AAAAAAAAAAI/AAAAAAAAAAA/1095ekL4OEA/photo.jpg";
$gate = $this->services['gate'];
$avatar = $gate->user()->getAvatar($this->o_page->_user["ID"]);
if($avatar){
    $default_image=$avatar->picture;
}
?>
<section id="middle">
<div id="panel-1" class="panel panel-default">
    <div class="panel-heading"> <span class="title elipsis"> <strong>CLEAN TABLE</strong> <!-- panel title --> </div>

    <!-- panel content -->
    <div class="panel-body">
        <div class='row'>
            <div class="col-lg-9 col-md-9 col-sm-8 col-lg-push-3 col-md-push-3 col-sm-push-4 margin-bottom-80">
                <ul class="nav nav-tabs nav-top-border">
                    <li class="active"><a href="#info" data-toggle="tab">����� �����</a></li>
                    <li><a href="#avatar" data-toggle="tab">�����</a></li>
                    <li><a href="#password" data-toggle="tab">������</a></li>
                    <li><a href="#privacy" data-toggle="tab">������������ �� ���</a></li>
                    <li><a href="#my_sites" data-toggle="tab">����� �������</a></li>
                </ul>
                <div class="tab-content margin-top-20">
                    <!-- PERSONAL INFO TAB -->
                    <div class="tab-pane fade in active" id="info">
                        <p><h3> ������� �� ������������� �����: </h3></p>
                        <hr>
                        <?php
                        echo $profile;
                        ?>
                    </div>
                    <div class="tab-pane fade" id="avatar">
                     <?php 
                        echo $company;
                     ?>
                    </div>

                    <!-- PASSWORD TAB -->
                    <div class="tab-pane fade" id="password">
                        <p><h3> ������� �� ������: </h3></p>
                        <hr>
                        <ul>
                            <li> ���������� ��� ��������: 
                                <ul>
                                    <li>���������� ������� - 12 �������</li>
                                    <li>��������� ������� - 5 �������</li>
                                    <li>���� ���� ������ �����</li>
                                    <li>���� ���� �����</li>
                                </ul>
                            </li>
                        </ul>
                        <?php
                        echo $password;
                        ?>
                    </div>
                    <div class="tab-pane fade" id="privacy">
                    <?php
                        if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['username'])){
                            $new_user = new \Maksoft\Admin\Profile\NewUser($gate, $this->o_page, $_POST);
                            try{

                               $new_user->is_valid();
                               $new_user->save();
                            } catch (Exception $e) {
                               require_once __DIR__."/errors.php";
                            }

                        } else {
                            $new_user = new \Maksoft\Admin\Profile\NewUser($gate, $this->o_page);
                        }

                        $new_user->load();

                        $registered_users = $this->services['gate']->user()->getUsersByParentUserId($this->o_page->_user["ID"]);
                        ?>
                        <p><h3>��������� �� ��� ���������� � ��������� �� �������CMS </h3></p>
                        <hr>
                        <ul>
                            <li> ���������� ��� ��������: 
                                <ul>
                                    <li>���������� ������� - 12 �������</li>
                                    <li>��������� ������� - 5 �������</li>
                                    <li>���� ���� ������ �����</li>
                                    <li>���� ���� �����</li>
                                </ul>
                            </li>
                        </ul>
                        <hr>
                        <div class="table-responsive">
                            <table class="table nomargin">
                            <th>ID</th>
                            <th>���</th>
                            <th>������������� ���</th>
                            <th>Email</th>
                            <th>���� �� ������</th>
                            <th>������</th>
                            <th>������</th>
                        <?php
                        echo $new_user;
                        $modals = array();
                        $forms = array();
                        foreach($registered_users as $reg_user){
                            $permissions = $gate->site()->get_site_permissions($reg_user->ID, $this->o_page->_site["SitesID"]);
                            if($permissions){ continue; }
                            ?>
                            <tr>
                                <td><?=$reg_user->ID;?></td>
                                <td><?=$reg_user->Name;?></td>
                                <td><?=$reg_user->username;?></td>
                                <td><?=$reg_user->EMail;?></td>
                                <td><?=$reg_user->AccessLevel;?></td>
                                <td>  
                                    <?php
                                        if($reg_user->SiteID == $this->o_page->_site["SitesID"]){
                                            echo "��� ������";
                                        } else {
                                    ?>
                                    <button 
                                        type="button"
                                        class="btn btn-success btn-xs"
                                        data-toggle="modal"
                                        data-target="#modal-<?=$reg_user->ID;?>">
                                            ������ ������
                                      </button>
                                      <?php } ?>
                                  </td>

                                <td>������</tr>
                            </tr>
                
                    <?  $forms[$reg_user->ID] = new \Maksoft\Admin\Profile\Access($gate, $this->o_page, $reg_user->ID);
            $modals[] = <<<HEREDOC
<div class="modal fade" id="modal-{$reg_user->ID}" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Modal title</h4>
  </div>
  <div class="modal-body">
    <p>�������� �� ����� �� ���������� <b>{$reg_user->Name}</b> �� ����: {$this->o_page->_site['primary_url']}</p>
    {$forms[$reg_user->ID]}
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
HEREDOC;
                }
                ?>
                        </table>
                        </div>
                        <?php
                    echo implode(PHP_EOL, $modals);

                    if($_SERVER["REQUEST_METHOD"] === "POST" && array_key_exists($_POST['action'], $forms)){
                        try{

                                $f = new \Maksoft\Admin\Profile\Access($gate, $this->o_page, 99999);
                                $f->load($_POST);
                                $f->is_valid();
                                $f->save();
                            
                        } catch (\Exception $e){
                            require_once __DIR__."/errors.php";
                        }
                    }
                    ?>
                    
                    </div>
                    <div class="tab-pane fade" id="my_sites">
                    <?php
                    $sites= $gate->user()->get_uSites($this->o_page->_user["ID"]);
                    ?>
                    <div class="table-responsive">
                        <table class="table nomargin">
                     <tr>
                        <th>#</th>
                        <th>������</th>
                        <th>Email</th>
                        <th>������</th>
                        <th>�����</th>
                      </tr>
                    <?php foreach($sites as $site){ ?>
                        <tr>
                            <td><?=$site->SitesID;?></td>
                            <td><a href="<?=$site->primary_url;?>"><?=$site->primary_url;?></a></td>
                            <td><?=$site->EMail;?></td>
                            <td><?=$site->ReadLevel;?></td>
                            <td><?=$site->WriteLevel;?></td>
                         </tr>
                    <?php } ?>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
    <!-- LEFT -->
            <div class="col-lg-3 col-md-3 col-sm-4 col-lg-pull-9 col-md-pull-9 col-sm-pull-8">
                <div class="thumbnail text-center">
                    <img id="profile-picture" src="/<?=$default_image;?>" alt="" />
                    <h2 class="size-18 margin-top-10 margin-bottom-0"><?=$this->o_page->_user["Name"];?></h2>
                </div>
                <!-- completed -->
                <div class="margin-bottom-30">
                    <?php
                    $picture = new \Maksoft\Admin\Profile\Picture($gate, $this->o_page);
                    if($_SERVER["REQUEST_METHOD"]==="POST" && isset($_FILES["avatar"]))
                    {
                        try{
                            $picture->save($this->o_page->_user["ID"]);
                        } catch ( \Exception $e ) {
                        }
                    }
                    echo $picture;
                    ?>
                </div>
                <!-- /completed -->
                </div>
            </div>
        </div>
    </div>
</div>
</section>
