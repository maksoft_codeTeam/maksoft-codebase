<section id="middle">
    <div id="content" class="dashboard padding-20">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>����</th>
                            <th>��</th>
                            <th>������</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($translations as $translation): ?>
                        <tr> 
                            <td><?=$translation->id?></td>
                            <td><?=$translation->language_text?></td>
                            <td><?=$translation->base?></td>
                            <td><?=$translation->translation?></td>
                            <td>
                                <?php 
                                echo $deleteForm->start();
                                $deleteForm->translation_id->value = $translation->id;
                                echo $deleteForm->translation_id;
                                echo $deleteForm->action;
                                echo $deleteForm->submit;
                                $deleteForm->end();
                                ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal"> ���������� </button>
                            </td>
                        </tr>
                    <?php endforeach?>
                    </tbody>
                </table>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> ������ </button>
            </div>

        </div>
    </div>
</section>

<div id="editModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <?=$form?>
            </div>
        </div>
    </div>
</div>



<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <?=$form?>
            </div>
        </div>
    </div>
</div>
