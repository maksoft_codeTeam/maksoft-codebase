<section id="middle">
    <div id="content" class="dashboard padding-20">
        <div class='panel panel-default'>
            <div class="panel-body">
                <div class='row'>
                    <div class="col-md-9">
                        <h6 class="nomargin"> About <?=count($inquiries)?> results <small class="text-success">(0.45 seconds) </small> </h6>

                        <hr class="nomargin-bottom margin-top-10">

                        <?php foreach($inquiries as $sReq): ?>
                        <!-- SEARCH RESULTS -->
                        <div class="clearfix search-result">
                            <?php 
                            $izp_ot="";
                            if($sReq->userID>0 && $SiteID>=0) {
                                $izp_ot=$this->o_page->extract_uInfo($sReq->userID, "Name");
                            }
                            switch($sReq->ftype) {
                                case 0: {$title = "����"; break;}
                                case 1: {$title = "��������� �� $izp_ot "; break;}
                                case 2: {$title = "��������"; break;}
                                case 3: {$title = "����������"; break;}
                                default : {$title = "����"; break;}
                            }
                            ?>
                            <!-- item -->
                            <h4><a href="#"><?=$sReq->Name?> | <?=$title?> </a></h4>
                            <small class="text-success">����: <?=$sReq->Data?></small>
                            <small class="text-success">�� ��������: <a target='_blank' href='/page.php?n=<?=$sReq->n?>&SiteID=<?=$sReq->SiteID?>'> <?=$sReq->Name?></a></small>
                            <p> <?=stripslashes($sReq->Zapitvane);?></p>
                            <br>
                            <div class='row'>
                                <div class='col-md-2 offset-md-2'>
                                    <form method="post">
                                    <select class='form-control' name="order<?=$sReq->ID?>" onchange="this.form.submit();">
                                        <option value=0 <?php echo($sReq->ftype == 0 ? "selected":""); ?>>����</option>
                                        <option value=1 <?php echo($sReq->ftype == 1 ? "selected":""); ?> >���������</option>
                                        <option value=2 <?php echo($sReq->ftype == 2 ? "selected":""); ?>>��������</option>
                                        <option value=3 <?php echo($sReq->ftype == 3 ? "selected":""); ?>>����������</option>
                                        <option value=255 <?php echo($sReq->ftype == 255 ? "selected":""); ?>>������</option>
                                    </select>
                                   </form>
                                </div>
                                <div class='col-md-4 text-right'>
                                    <a href="mailto:<?=$sReq->EMail?>" class="btn btn-default btn-bordered"> <span>�������� ��: <small><?=$sReq->EMail?></small> </a>
                                </div>
                                <div class='col-md-4'>
                                    <!-- Large Modal >-->
                                    <button type="button" class="btn btn-default btn-bordered" data-toggle="modal" data-target="#modal-<?=$sReq->ID?>">������</button>
                                    <div id='modal-<?=$sReq->ID?>' class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <!-- header modal -->
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                                                </div>

                                                <!-- body modal -->
                                                <div class="modal-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered nomargin">
                                                            <thead>
                                                                <tr><td colspan="2">��������� # <?=$sReq->ID?> </td></tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>���:<td>
                                                                    <?=$sReq->Name?>
                                                                </tr>
                                                                <tr>
                                                                    <td>e-mail:<td>
                                                                    <?=$sReq->EMail?>
                                                                </tr>
                                                                <tr>
                                                                    <td>���������:<td>
                                                                    <?=stripslashes($sReq->Zapitvane)?>
                                                                </tr>
                                                                <tr>
                                                                    <td>�������� ���������:<td>
                                                                    <?=html_entity_decode($sReq->TextForm)?>    
                                                                </tr>
                                                                <tr>
                                                                    <td>����:<td>
                                                                    <?=$sReq->Data?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /item -->
                        <?php endforeach; ?>

                    </div>
                    <div class="col-md-3">
                        <h4>���������� � ����� ���� �������</h4>
                        <?php if(!count($otherSitesInquiries)): ?>
                        <p><em>������ ���� ����������</em></p>
                        <?php endif;?>
                        <?php foreach($otherSitesInquiries as $req): ?>
                            <p><em><a target="_blank" href="/page.php?n=<?=$page->n?>&SiteID=<?=$page->SiteID?>"><?=$req->Name?> - <?=$req->requests?></a></em></p>
                        <?php endforeach;?>
                        <p> <em>�������� �� ���� ���������, �� ����� ����� �������� ����� �� �������� ��� �������� ���� ���������</em></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
