<section id="middle">
    <header id="page-header">
        <h1>�����</h1>
        <ol class="breadcrumb">
            <li><a href="<?=$this->route->generate('dashboard')?>">�������������</a></li>
            <li><a href="<?=$this->route->generate('fileManager')?>">������ ��������</a></li>
            <li class="active">����</li>
        </ol>
    </header>
    <div id="content" class="dashboard padding-20">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class='col-md-3 col-md-offset-9'>
                     <form method="post" multipart=""  enctype="multipart/form-data">
                    <?=$form->action?>
                    <?=$form->files?>
                    <?=$form->submit?>
                    </form>
                    <label>������� �� ���</label>
                    <input id='filterByName' type='text' name='name' class='form-control' >
                    <a id='filterLink' href='#' class='btn btn-success'> ��������� </a>
                </div>
                <div class='row'>
                    <?php foreach($files as $file): ?>
                        <div class='col-md-3'>
                            <div class="thumbnail">
                                <img height='20%' width='20%' class="img-fluid grayscale-hover-color" src="/<?=$file->image_src?>" alt="" />
                                <a href='<?=$this->route->generate('imageDetail', array('dir_id'=>$id, 'image_id' => $file->imageID))?>'><i class='et-edit'></i></a>
                                <button class="shortcode" title="�������� �� shortcode" data-clipboard-text="[get_picture id='<?=$file->imageID?>']">
                                    <img src="/web/images/icons/copy.png" alt="">
                                </button>
                            </div>
                        </div>
                    <?php endforeach;?>
                    <div class="text-center">
                        <ul class="pagination pagination-sm">
                            <li class="disabled"><a href="#">Prev</a></li>
                            <?php for($i=1; $i < round($totalFilesCount/$imagesPerPage); $i++): ?>
                            <li 
                                <?php if($i==1 and $step==0) { ?>
                                    class="active"
                                <?php } elseif($step > 0 and $step == $i) { ?>
                                    class="active"
                                <?php } ?>
                            >
                            <?php $urlData['step'] = $i;?>
                                <a href="<?=$this->route->generate('imageDir', $urlData)?>"><?=$i?></a>
                            </li>
                            <?php endfor;?>
                            <li><a href="#">Next</a></li>
                        </ul>                                                   
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$javascript = <<<'EOD'
<script>
$( document ).ready(function() {
    const url = window.location.href;    

    $("#filterByName").on('keyup', function() {
        var filterByName = $(this).val();
        var tmp_url = url;
        tmp_url = tmp_url.split('name')[0];
        document.getElementById("filterLink").href= tmp_url + 'name=' + filterByName; 
    });
});
</script>
EOD;
\add_js($javascript);
?>
