<section id="middle">

    <!-- page title -->
    <header id="page-header">
        <h1>������</h1>
        <ol class="breadcrumb">
        <li><a href="<?$this->route->generate('login')?>">�������������</a></li>
        <li><a href="<?=$this->route->generate('tasks')?>">������</a></li>
            <li class="active"><?=ucfirst($task->subject)?></li>
        </ol>
    </header>
    <!-- /page title -->


            <div id="content" class="padding-20">
    <div class='row'>
        <div class='col-md-8'>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 text-left">
                            <h4><?=$task->subject?></h4>
                                <ul class="list-unstyled">
                                <li><strong>��:</strong> <?=$task->Name?></li>
                                    <li><strong>����:</strong> <?=$task->start_date?></li>
                                    <li><strong>����:</strong> <?=$task->period?> ��� /<?=$task->end_date?>/</li>
                                    <li><strong>���������:</strong> <?=str_repeat("<i class='fa fa-star-o'></i>", (integer) $task->priority);?></li>
                                    <li><strong>������:</strong> <?=$task->company_name?></li>
                                    <li>
                                    <strong>������:</strong>
                                    <?php if(strtotime($task->end_date) > time()): ?>
                                        ������� / ������� <?=$dayDifference?> ��� /
                                    <?php else: ?>
                                        ���������� / <?=$dayDifference;?> ��� / 
                                    <?php endif;?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class='row'>
                        <div class='col-md-12'><?=$task->notes?></div>
                        </div>
                        <hr class="nomargin-top">
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php if($task->from_user == $this->o_page->_user['ID'] or $task->to_user == $this->o_page->_user['ID'] ): ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">�������</button>
                        <?php endif; ?>
                        <?=$completeTaskForm->start();?>
                        <button type='submit' class="btn btn-success"><i class="fa fa-print"></i> �������</button>
                        <?=$completeTaskForm->action;?>
                        <?=$completeTaskForm->end();?>
                    </div>
                </div>
        </div>
        <div class='col-md-4'>
            <section class="panel panel-default">
                <header class="panel-heading">
                    <h2 class="panel-title elipsis">
                        <i class="glyphicon glyphicon-pushpin"></i> ���������
                    </h2>
                </header>

                <div class="panel-body noradius padding-10">
                    <br>
                    <ul class="comment list-unstyled">
                    <?php foreach($taskComments as $comment): ?>
                        <li class="comment">
                            <!-- avatar -->
                            <img class="avatar" src="/Templates/maksoft/v2/assets/img/logo-admin.png" width="50" height="50" alt="avatar">

                            <!-- comment body -->
                            <div class="comment-body"> 
                                <a href="#" class="comment-author">
<?=var_dump(strtotime($comment->created_at))?>
                                    <small class="text-muted pull-right"> <?=elpasedTime(strtotime($comment->created_at));?> </small>
                                    <span><?=$comment->Name?></span>
                                </a>
                                <p><?=$comment->comment?> </p>
                            </div><!-- /comment body -->
                        </li><!-- /options -->
                     <?php endforeach;?>
                        <li>
                                <?=$addCommentForm->start();?>
                            <div class="input-group">
                                <?=$addCommentForm->action;?>
                                <input id="btn-input" type="text" required='true' name='message' class="form-control" placeholder="Type your message...">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" id="btn-chat" type='submit'>
                                        <i class="fa fa-reply"></i> Reply
                                    </button> 
                                </span>
                            </div>
                                <?=$addCommentForm->end();?>
                        </li>
                    </ul>
                </div>
            </section>
        </div>
    </div>
            </div>
</section>

                        <!-- EDIT TASK MODAL -->
                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <!-- header modal -->
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myLargeModalLabel">������� ������ �<?=$task->taskID?></h4>
                                    </div>
                                    <!-- body modal -->
                                    <div class="modal-body">
                                        <?=$editTaskForm->start()?>
                                        <div class='row'>
                                            <div class='col-md-12'>
                                                <div class='row'>
                                                    <div class='col-md-6'> 
                                                    <?=$editTaskForm->priority->label?>
                                                    <?=$editTaskForm->priority?>
                                                    </div>
                                                    <div class='col-md-6'> 
                                                    <?=$editTaskForm->fromUser->label?>
                                                    <?=$editTaskForm->fromUser?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-md-12'>
                                                <div class='row'>
                                                    <div class='col-md-6'> 
                                                    <?=$editTaskForm->startDate->label?>
                                                    <?=$editTaskForm->startDate?>
                                                    </div>
                                                    <div class='col-md-6'> 
                                                    <?=$editTaskForm->period->label?>
                                                    <?=$editTaskForm->period?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-md-12'>
                                                <div class='row'>
                                                    <div class='col-md-6'> 
                                                    <?=$editTaskForm->type->label?>
                                                    <?=$editTaskForm->type?>
                                                    </div>
                                                    <div class='col-md-3'> 
                                                    <?=$editTaskForm->status->label?>
                                                    <?=$editTaskForm->status?>
                                                    </div>
                                                    <div class='col-md-3'> 
                                                    <?=$editTaskForm->repeat_in->label?>
                                                    <?=$editTaskForm->repeat_in?>
                                                    </div> 
                                                </div>     
                                            </div>
                                            <div class='col-md-12'>
                                                <div class='row'>
                                                    <div class='col-md-6'> 
                                                    <?=$editTaskForm->timeToFinish->label?>
                                                    <?=$editTaskForm->timeToFinish?>
                                                    </div>
                                                    <div class='col-md-6'> 
                                                    <?=$editTaskForm->company->label?>
                                                    <?=$editTaskForm->company?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-md-12'>
                                                <?=$editTaskForm->subject->label?>
                                                <?=$editTaskForm->subject?>
                                                <hr>
                                            </div>
                                            <div class='col-md-12'>
                                                <textarea name="<?=$editTaskForm->content->name?>" class="summernote form-control" data-height="200" data-lang="en-US">
                                                            <?=$editTaskForm->content->value?>
                                                </textarea>
                                            </div>
                                            <div class='col-md-12'>
                                                <hr>
                                                <button type="submit" class="btn btn-success btn-block">������</button>
                                            </div>
                                        <?=$editTaskForm->action?>
                                        <?=$editTaskForm->end()?>
                                    </div>
                                </div>
                            </div>
                        </div>
