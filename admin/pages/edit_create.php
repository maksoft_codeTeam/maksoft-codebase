<?php
use Maksoft\Administration\Forms\CMSTranslation;
?>
<section id="middle">
    <div id="content" class="dashboard padding-20">
        <div class='row'>
            <div class="col-md-12">
                <div id="panel-ui-tan-l4" class="panel panel-default">
                    <div class="panel-heading">
                        <!-- tabs nav -->
                        <ul class="nav nav-tabs pull-left">
                            <li class="active">
                                 <a href="#<?=$main_language['language_id'];?>" data-toggle="tab" aria-expanded='true'>
                                 <?=$main_language['language_text'];?></a> 
                            </li>
                            <?php if($page_id) { ?>
                                <?php foreach($site_versions as $version) { ?>
                                    <?php if ($version['language_id'] == $main_language['language_id']) { continue; }; ?>
                                    <li>
                                        <a href="#<?=$version['language_id'];?>" data-toggle="tab" aria-expanded='false'>
                                        <?=$version['language_text'];?></a> 
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                        <!-- /tabs nav -->
                    </div>
                    <!-- panel content -->
                    <div class="panel-body">

                        <!-- tabs content -->
                        <div class="tab-content transparent">
                            <?php 
                                if($page_id) {
                                    foreach($site_versions as $lang) {
                                        require_once __DIR__.'/page_language_tabs.php';
                                    }
                                }
                            ?>


                            <div id="<?=$main_language['language_id']?>" class="tab-pane active">
                                <div class="row">
                                <?=$createPageForm->start()?>
                                    <div class="col-md-6">
                                        <label for=" <?=$createPageForm->Name->name?>"><?=$createPageForm->Name->label?></label>
                                        <?=$createPageForm->Name?>
                                        <?=$createPageForm->Name->print_errors()?>
                                    </div>
                                    <div class="col-md-6">
                                        <label for=" <?=$createPageForm->ParentPage->name?>"><?=$createPageForm->ParentPage->label?></label>
                                        <div  id="ParentPage">
                                    <?php if(!$page_id && $SiteID == $p_parent->SiteID):?>
                                        <select class="form-control select2" tabindex="-1" name="<?=$createPageForm->ParentPage->name?>" style="display: none;">
                                        <?php
                                        $rootNodes = $tree->getRootNodes(); 
                                        foreach($rootNodes as $topPage): ?>
                                            <option value="<?=$topPage->id?>" > <?=$topPage->title?> </option>
                                                <?php 
                                                    if($topPage->hasChildren()):
                                                        echo printParentPageOptions($topPage->getChildren(), 2);
                                                    endif;
                                                ?>
                                        <?php endforeach; ?>
                                        </select>
                                    <?php elseif($page_id == $Site->StartPage): ?>
                                          <input class='<?=$createPageForm->ParentPage->class?>' type="text" value="- no parent page -" disabled>
                                          <input type="hidden" name="ParentPage" value="0">
                                    <?php else: ?>
                                           <select class='<?=$createPageForm->ParentPage->class?>'
                                                   name="<?=$createPageForm->ParentPage->name?>"
                                                   id="<?=$createPageForm->ParentPage->id?>"
                                                   onChange="<?=$createPageForm->ParentPage->onchange?>()">
                                               <option value="<?=$page->ParentPage?>"><?=$o_page->get_pName($page->ParentPage)?></option>
                                               <option value="<?=$p_parent->ParentPage?>"> - ������ ��������� ��������-</option>
                                           </select>
                                    <?php endif;?>
                                        </div>
                                        <?=$createPageForm->ParentPage->print_errors()?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for=" <?=$createPageForm->title->name?>"><?=$createPageForm->title->label?></label>
                                        <?=$createPageForm->title?>
                                        <?=$createPageForm->title->print_errors()?>
                                    </div>

                                    <div class="col-md-6">
                                        <label for=" <?=$createPageForm->slug->name?>"><?=$createPageForm->slug->label?></label>
                                        <?=$createPageForm->slug?>
                                        <?=$createPageForm->slug->print_errors()?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php $createPageForm->textStr->class .= ' '.$user->WriteLevel >=4 ? ' tinymce4-admin' : ' tinymce4-user'; ?>
                                        <textarea class="<?=$createPageForm->textStr->class?>" name="<?=$createPageForm->textStr->name?>"> <?=$createPageForm->textStr->value?> </textarea>
                                        <?=$createPageForm->textStr->print_errors()?>
                                    </div>
                                    <div class='col-md-12 '>
                                        <div class='checkbox'>
                                            <label for=" <?=$createPageForm->notags->name?>"><?=$createPageForm->notags->label?></label>
                                            <?=$createPageForm->notags?>
                                            <?=$createPageForm->notags->print_errors()?>
                                        </div>
                                    </div>
                                    <div class='col-md-12'>
                                        <label for=" <?=$createPageForm->tags->name?>"><?=$createPageForm->tags->label?></label>
                                        <?=$createPageForm->tags?>
                                        <?=$createPageForm->tags->print_errors()?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="#" class="thumbnail">
                                            <?php $base_image = $page->image_src ? '/'.$page->image_src : 'https://st3.depositphotos.com/1654249/12728/i/170/depositphotos_127280100-stock-photo-blank-hexagon-signboards-on-white.jpg'; ?>
                                           <img id='preview_image' src="<?=$base_image?>" alt="...">
                                        </a>
                                        <a href="/web/admin/includes/images_manager.php?n=<?=$page->n?>&amp;SiteID=<?=$page->SiteID?>"
                                           class="btn btn-info"
                                               title="������� �� ������������� �� �������� <?=$page->Name?>" id="change_image_link">�����
                                        </a>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for=" <?=$createPageForm->userFile->name?>"><?=$createPageForm->userFile->label?></label>
                                                <?=$createPageForm->userFile?>
                                                <?=$createPageForm->userFile->print_errors()?>
                                            </div>
                                            <div class="col-md-6">
                                                <label for=" <?=$createPageForm->imageName->name?>"><?=$createPageForm->imageName->label?></label>
                                                <?=$createPageForm->imageName?>
                                                <?=$createPageForm->imageName->print_errors()?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for=" <?=$createPageForm->img_dir->name?>"><?=$createPageForm->img_dir->label?></label>
                                                <select name='<?=$createPageForm->img_dir->name?>' class='<?=$createPageForm->img_dir->class?>'>
                                                <?=$createPageForm->print_select(array_filter($img_dirs, function($el){
                                                        return !empty($el);
                                                    }), $createPageForm->img_dir->value)?>
                                                </select>
                                                <?=$createPageForm->img_dir->print_errors()?>
                                            </div>
                                            <div class="col-md-4">
                                                <label for=" <?=$createPageForm->image_allign->name?>"><?=$createPageForm->image_allign->label?></label>
                                                <select name='<?=$createPageForm->image_allign->name?>' class='<?=$createPageForm->image_allign->class?>'>
                                                    <?=$createPageForm->print_select(CMSTranslation::$config['bg']['image_allign'], $createPageForm->image_allign->value)?>
                                                </select>
                                                <?=$createPageForm->image_allign->print_errors()?>
                                            </div>
                                            <div class="col-md-4">
                                                <label for=" <?=$createPageForm->imageWidth->name?>"><?=$createPageForm->imageWidth->label?></label>
                                                <?=$createPageForm->imageWidth?>
                                                <?=$createPageForm->imageWidth->print_errors()?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class='col-md-12'>
                                        <label for=" <?=$createPageForm->PHPcode->name?>"><?=$createPageForm->PHPcode->label?></label>
                                         <?=$createPageForm->PHPcode?>
                                         <?=$createPageForm->PHPcode->print_errors()?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class='col-md-12'>
                                        <label for=" <?=$createPageForm->PHPvars->name?>"><?=$createPageForm->PHPvars->label?></label>
                                         <?=$createPageForm->PHPvars?>
                                         <?=$createPageForm->PHPvars->print_errors()?>
                                    </div>
                                </div>

                                <div class='row'>
                                    <div class="bs-callout bs-callout-info">
                                        <h4> ��������� �� ������������ <h4>
                                    </div>
                                    <div class='col-md-6'>
                                        <label for="<?=$createPageForm->show_link->name?>"><?=$createPageForm->show_link->label?></label>
                                         <select name='<?=$createPageForm->image_allign->name?>' class='<?=$createPageForm->image_allign->class?>'>
                                            <?=$createPageForm->print_select(CMSTranslation::$config['bg']['show_link'])?>
                                         </select>
                                         <?=$createPageForm->show_link->print_errors()?>
                                    </div>

                                    <div class='col-md-6'>
                                         <label for=" <?=$createPageForm->show_link_order->name?>"><?=$createPageForm->show_link_order->label?></label>
                                         <select name='<?=$createPageForm->show_link_order->name?>' class='<?=$createPageForm->show_link_order->class?>'>
                                            <?=$createPageForm->print_select(CMSTranslation::$config['bg']['show_link_order'])?>
                                         </select>
                                         <?=$createPageForm->show_link_order->print_errors()?>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-6'>
                                        <label for="<?=$createPageForm->show_link_cols->name?>"><?=$createPageForm->show_link_cols->label?></label>
                                        <?=$createPageForm->show_link_cols?>
                                        <?=$createPageForm->show_link_cols->print_errors()?>
                                    </div>

                                    <div class='col-md-6'>
                                        <label for="<?=$createPageForm->make_links->name?>"><?=$createPageForm->make_links->label?></label>
                                        <?=$createPageForm->make_links?>
                                        <?=$createPageForm->make_links->print_errors()?>
                                    </div>
                                    <div class='col-md-6'>
                                        <label for="<?=$createPageForm->show_group_id->name?>"><?=$createPageForm->show_group_id->label?></label>
                                         <select name='<?=$createPageForm->show_group_id->name?>' class='<?=$createPageForm->show_group_id->class?>'>
                                            <option value="0"> - �������� - </option>
                                            <?=$createPageForm->print_select($groups, $createPageForm->show_group_id->value)?>
                                         </select>
                                         <?=$createPageForm->show_group_id->print_errors()?>
                                    </div>
                                    <div class='col-md-6'>
                                        <a href="page.php?n=63931&amp;SiteID=<?=$o_page->_site['SitesID']?>" target="_blank" class="add"> ������ �����</a>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <label for="<?=$createPageForm->page_template->name?>"><?=$createPageForm->page_template->label?></label>
                                         <select name='<?=$createPageForm->page_template->name?>' class='<?=$createPageForm->page_template->class?>'>
                                            <?=$createPageForm->print_select($templates, $createPageForm->page_template->value, $template_id)?>
                                         </select>
                                         <?=$createPageForm->page_template->print_errors()?>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <label for="<?=$createPageForm->PageURL->name?>"><?=$createPageForm->PageURL->label?></label>
                                         <select name='<?=$createPageForm->PageURL->name?>' class='<?=$createPageForm->PageURL->class?>'>
                                            <option value="">- �������� -</option>
                                            <?=$createPageForm->print_select($addons, $createPageForm->PageURL->value, $template_id)?>
                                         </select>
                                         <?=$createPageForm->PageURL->print_errors()?>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-md-4">
                                        <label for="<?=$createPageForm->toplink->name?>"><?=$createPageForm->toplink->label?></label>
                                        <?=$createPageForm->toplink?>
                                        <?=$createPageForm->toplink->print_errors()?>
                                    </div>
                                    <div class='col-md-8'>
                                        <label for="<?=$createPageForm->status->name?>"><?=$createPageForm->status->label?></label>
                                        <?=$createPageForm->status?>
                                        <?=$createPageForm->status->print_errors()?>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-md-4">
                                        <label for="<?=$createPageForm->ptg_group_id->name?>"><?=$createPageForm->ptg_group_id->label?></label>
                                         <select name='<?=$createPageForm->ptg_group_id->name?>' class='<?=$createPageForm->ptg_group_id->class?>'>
                                            <option value="0"> - �������� - </option>
                                            <?=$createPageForm->print_select($groups)?>
                                         </select>
                                         <?=$createPageForm->ptg_group_id->print_errors()?>
                                    </div>
                                    <div class='col-md-4'>
                                        <label for="<?=$createPageForm->ptg_label->name?>"><?=$createPageForm->ptg_label->label?></label>
                                        <?=$createPageForm->ptg_label?>
                                        <?=$createPageForm->ptg_label->print_errors()?>
                                    </div>
                                    <div class='col-md-4'>
                                        <label for="<?=$createPageForm->ptg_end_date->name?>"><?=$createPageForm->ptg_end_date->label?></label>
                                        <?=$createPageForm->ptg_end_date?>
                                        <?=$createPageForm->ptg_end_date->print_errors()?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-2">1<?=$priceForm->description->label?></div>
                                            <div class="col-md-2"><?=$priceForm->code->label?></div>
                                            <div class="col-md-2"><?=$priceForm->pcs->label?></div>
                                            <div class="col-md-2"><?=$priceForm->qty->label?></div>
                                            <div class="col-md-2"><?=$priceForm->price->label?></div>
                                            <div class="col-md-2"><?=$priceForm->currency->label?></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                    <?php foreach($pagePrices as $i => $price): ?>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input class="<?=$priceForm->description->class?>"
                                                       name="price[<?=$i?>][description]"
                                                       type="text" id="price[<?=$i?>][description]"
                                                       value="<?=$price->price_description?>">
                                            </div>
                                            <div class="col-md-2">
                                                <input class="<?=$priceForm->code->class?>"
                                                       name="price[<?=$i?>][code]"
                                                       type="text"
                                                       id="price[<?=$i?>][code]"
                                                       value="<?=$price->price_code?>">
                                            </div>
                                            <div class="col-md-2">
                                                <input class="<?=$priceForm->pcs->class?>"
                                                       name="price[<?=$i?>][pcs]"
                                                       type="text"
                                                       id="price[<?=$i?>][pcs]"
                                                       value="<?=$price->price_pcs?>">
                                            </div>
                                            <div class="col-md-2">
                                                <input class="<?=$priceForm->qty->class?>"
                                                       name="price[<?=$i?>][qty]"
                                                       type="text"
                                                       id="price[<?=$i?>][qty]"
                                                       value="<?=$price->price_qty?>">
                                            </div>
                                            <div class="col-md-2">
                                                <input class="<?=$priceForm->price->class?>"
                                                       name="price[<?=$i?>][price]"
                                                       type="text"
                                                       id="price[<?=$i?>][price]"
                                                       value="<?=$price->price_value?>">
                                            </div>
                                            <div class="col-md-2">
                                                 <select name='price[<?=$i?>][currency]' class='<?=$priceForm->currency->class?>'>
                                                    <option value="">- �������� -</option>
                                                    <?=$createPageForm->print_select($currencies, $price->price_currency)?>
                                                 </select>
                                                 <a href="javascript: void(0)" class="delete" onClick="remove_price(this)">
                                                     <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                 </a>
                                            </div>
                                        </div>
                                    <?php endforeach;?>
                                    </div>
                                    <a href="javascript: void(0)" id="add_page_price" class="add">������ ����</a>
                                </div>
                                <div class="row">
                                    <div class='col-md-6'>
                                        <label for="<?=$createPageForm->SecLevel->name?>"><?=$createPageForm->SecLevel->label?></label>
                                         <select name='<?=$createPageForm->SecLevel->name?>' class='<?=$createPageForm->SecLevel->class?>'>
                                            <?=$createPageForm->print_select(range($page->SecLevel, $user->AccessLevel))?>
                                         </select>
                                    </div>
                                    <div class='col-md-6'>
                                        <span>���� �� ��������:<br> <?=$page->date_added?></span>
                                        <br>
                                        <input type="hidden" name="date_added" value="2016-03-18 15:36:59">
                                        �����: <?=$o_page->extract_uInfo($o_page->get_pInfo('author', $page_id), 'Name')?>
                                        <?php
                                        if($user->AccessLevel >=3) {
                                            $page_edit_log_q = $o_page->db_query("SELECT DISTINCT(uID) FROM page_edit_log WHERE n='$page_id' ");
                                            if(mysqli_num_rows($page_edit_log_q)>0) {
                                                echo("<br>���������� � ������������� ��: ");
                                            }
                                            $users = array();
                                            while($row_log = mysqli_fetch_object($page_edit_log_q)) {
                                                $users[] = $o_page->extract_uInfo($row_log->uID, 'Name');
                                            }
                                            echo implode(', ', $users);
                                            echo("<br>");
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class='row'>
                                    <hr>
                                    <div class='col-md-12  text-center'>
                                        <?=$createPageForm->date_added?>
                                        <?=$createPageForm->action?>
                                        <?=$createPageForm->lang_id?>
                                        <?=$createPageForm->imageNo?>
                                        <?=$createPageForm->old_image_src?>
                                        <?=$createPageForm->old_imageNo?>
                                        <?=$createPageForm->no?>
                                        <?php $createPageForm->submit->class='btn btn-default btn-lg btn-block';?>
                                        <?=$createPageForm->submit?>
                                    </div>
                                    <div class='col-md-6  text-center'>
                                    <?php if( ($page->n > 0 ) && ($user->AccessLevel >=2 || $site->_site['netservice']>0) ) {
                                        include __DIR__ .'/web/stats/site_page_analitycs.php'; 
                                    } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                                    
                            </div>
                            <!-- /TAB 1 CONTENT -->

                        </div>
                        <!-- /tabs content -->

                    </div>
                    <!-- /panel content -->
                </div>
</section>

<?php
$createPageForm->end();
require_once __DIR__.'/../../global/tinymce/tinymce.php';
$javascript = <<<EOD

<script type="text/javascript">
function set_parent_page()
{
    //$no - n of the edited page
    //n - selected value from the menu
    var n = jQuery('#site_list_pages').val();

    jQuery('#ParentPage').html('<input value="Loading menu ..." disabled>');
    jQuery.get('web/admin/includes/site_list_pages.php?n='+n+'&SiteID=<?=$SiteID?>&no=<?=$page->n?>&class=form-control', function(data) {
        jQuery('#ParentPage').html(data);
    })
}

function set_image()
{

    var im=document.getElementById("preview_image");
    var iml=document.getElementById("preview_image_link");
    var x=document.getElementById("imageNo").selectedIndex;
    var y=document.getElementById("imageNo").options;

    im.src = y[x].getAttribute('rel');
    //alert(iml.getAttribute("href"));
    //iml.href = y[x].getAttribute('rel');
}

function remove_price(id) {
    jQuery(id).parent().parent().remove();
}

jQuery("#add_page_price").click(function(){
    jQuery("#page_prices tbody tr:last").clone().appendTo("#page_prices tbody");
});

</script>
EOD;

\add_js($javascript);
?>
