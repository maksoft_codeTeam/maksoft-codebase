<script language="javascript" type="text/javascript">
jQuery(document).ready(function(){
    
        jQuery('#selected_page').click(function(){
            if(jQuery('#selected_page').attr('checked'))
                {
                    jQuery(":checkbox").attr('disabled', true);
                    jQuery('#selected_page').attr('disabled', false);
                    jQuery(":checkbox").attr('checked', true);
                    jQuery('#selected_page').attr('checked', true);
                }
            else
                {
                    jQuery(":checkbox").attr('disabled', false);
                    jQuery('#selected_page').attr('disabled', false);
                    jQuery(":checkbox").attr('checked', true);
                    jQuery('#selected_page').attr('checked', false);
                }
        })
            
        jQuery('#delete_form').submit(function(){
            
            if(confirm("Внимание: всички избрани страници ще бъдат засегнати. \n\nСигурни ли сте ?"))
                return true;
            else
                return false
            });
})
</script>
<?php
$p_delete = new page($del_page_no); 
$num_pages = 0;
//if($p_delete->get_pStatus()!=0)
//{                         
for($i=0; $i<count($selected_page); $i++)
    {
        if($selected_page[$i]>199)
        switch($action)
            {
                case "delete": {$num_pages+= $p_delete->delete_page($selected_page[$i], 1); break;}
                case "trash": {$num_pages+= $p_delete->delete_page($selected_page[$i], -1); break;}
                case "empty": {$num_pages+= $p_delete->delete_page($selected_page[$i], 2); break;}
                default: {$p_delete->delete_page($selected_page[$i], 0); }
            }
        else { echo "Служебни или заключени страници не могат да бъдат манипулирани!"; break; }
        
    }
if($action == "delete") mk_output_message("warning", "Успешно изтрихте <b>".$num_pages ."</b> страници <br><a href=\"page.php?n=$Site->StartPage&SiteID=".$SiteID."\">Начало</a>!");
if($action == "trash") mk_output_message("normal", "Успешно изхвърлихте <b>".$num_pages ."</b> страници в <a href=\"page.php?n=144&SiteID=".$SiteID."\">кошчето</a> !");
if($action == "empty") mk_output_message("normal", "Успешно изпразнихте <b>".$num_pages ."</b> страници !");

//return number of subpages to delete
$subpages_delete = $p_delete->get_pSubpages();

if($num_pages == 0)
{
  
  $subapages_disabled = "disabled";
  
  if($p_delete->get_pStatus()==0)
    {
        $subapages_disabled = "";
        mk_output_message("error", "Избраната страница е заключена и не може да бъде изтрита ! Можете да изтриете само нейните подстраници !");
    }
  ?>
  <form name="delete_form" id="delete_form" method="post" action="page.php?n=143&SiteID=<?php echo("$SiteID"); ?>">
  <table class="border_table" width="550px" align="center" cellpadding="5" cellspacing="1">
  <thead><tr><th colspan="3">Изтриване на страници</th></tr></thead>
  <tbody>
  <tr><td width="50px">
  <?php
    if($p_delete->get_pStatus()==0)
        echo '&nbsp;<td width="480px"><a href="'.$p_delete->get_pLink().'" target="_blank"><h1>'.$p_delete->get_pName().'</h1></a><td width="20px">&nbsp;';
    else
        echo '<input id="selected_page" type="checkbox" checked name="selected_page[]" value="'.$p_delete->n.'"><td width="480px"><a href="'.$p_delete->get_pLink().'" target="_blank"><h1>'.$p_delete->get_pName().'</h1></a><td width="20px">&nbsp;'; 
    
    for($i=0; $i<count($subpages_delete); $i++)
        {
            $subpages = $o_page->get_pSubpagesCount($subpages_delete[$i]['n']);
            echo "<tr><td><input type=\"checkbox\" checked $subapages_disabled name=\"selected_page[]\" value=\"".$subpages_delete[$i]['n']."\"><td><li>".$subpages_delete[$i]['Name']."<td>&nbsp;";
            if($subpages>0) echo "<a href=\"#\" title=\"Заедно с тази страница ще бъдат изтрити и всички нейни подстраници: $subpages !\" style=\"display: block; width: 20px; height: 20px; color: #FFF; background: #F00; text-align: center; line-height: 20px; font-size: 14px;\">".$subpages."</a>";               
            //if($subpages_delete[$i]['status'] == 0) echo "<a href=\"#\" title=\"Тази страница е заключена!\" style=\"display: block; width: 20px; height: 20px; color: #FFF; background: #F00; text-align: center; line-height: 20px; font-size: 14px;\"> X </a>";
        }
  ?>
  </tbody>
  <tfoot>
  <tr>
  <td><img src="web/admin/images/arrow_ltr.png">
  <td align="left">
    <select name="action" style="float: left;">
        <option value="delete">изтрий</option>
        <option value="trash" selected>премести в кошчето</option>
        <option value="empty">изпразни</option>
    </select>
    <input name="button" type="submit" id="submit" value="Продължи" style="float: right; background: #E60000; color: #FFF; border: none; width:150px; height: 25px; line-height:25px; padding:0px;">
    <td>&nbsp;
  <tr><td colspan="3" align="left"><div style="margin: 0 50px 0 60px"><small><em><strong><u>изтриий</u></strong> - безвъзвратно изтриване / <strong><u>премести в кошчето</u></strong> - симулира изтриване, премества в кошчето / <strong><u>изпразни</u></strong> - премахва съдържанието на странците</em></small></div>
  </tfoot>
  </table>
  <input name="del_page_no" type="hidden" id="del_page_no" value="<?php echo("$del_page_no");?>">
  </form>
  <?php
  }
?>
