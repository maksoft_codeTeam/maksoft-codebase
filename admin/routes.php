<?php
use Maksoft\Administration\Views\LoginView;
use Maksoft\Administration\Views\CreateOrEditPageView;
use Maksoft\Administration\Views\SiteMapView;
use Maksoft\Administration\Views\TrashbinView;
use Maksoft\Administration\Views\TaskView;
use Maksoft\Administration\Views\TaskDetailView;
use Maksoft\Administration\Views\CreateTaskView;
use Maksoft\Administration\Views\GroupView;
use Maksoft\Administration\Views\GroupDetailView;
use Maksoft\Administration\Views\DashView;            
use Maksoft\Administration\Views\DeletePageView;      
use Maksoft\Administration\Views\InquiriesListView;   
use Maksoft\Administration\Views\SiteSettingsView;    
use Maksoft\Administration\Views\UserProfileView;    
use Maksoft\Administration\Views\FileManagerView;     
use Maksoft\Administration\Views\ImageDirView;     
use Maksoft\Administration\Views\ImageDetailView;     
use Maksoft\Administration\Views\BlankPageView;       
use Maksoft\Administration\Views\TranslationListView; 
use Maksoft\Administration\Views\PagesListView;       


$router->map( 'GET', '/admin/', LoginView, 'login');

if (page::$uLevel < 2) {
    return array();
}

/*
 *           METHOD      PATH                                VIEW_NAME             ROUTE_NAME 
 */

$router->map('GET',      '/admin/dash/',                                                         DashView,             'dashboard');
$router->map('GET|POST', '/admin/pages/[i:id]/edit/',                                            CreateOrEditPageView, 'editPage');
$router->map('GET|POST', '/admin/pages/[i:id]/delete/',                                          DeletePageView,       'deletePage');
$router->map('GET|POST', '/admin/pages/new/',                                                    CreateOrEditPageView, 'newPage');
$router->map('GET',      '/admin/sitemap/',                                                      SiteMapView,          'sitemap');
$router->map('GET',      '/admin/trashbin/',                                                     TrashbinView,         'trashbin') ; 
$router->map('GET',      '/admin/tasks/',                                                        TaskView,             'tasks'); 
$router->map('GET|POST', '/admin/tasks/[i:task_id]/',                                            TaskDetailView,       'taskDetail');
$router->map('GET|POST', '/admin/tasks/new/',                                                    CreateTaskView,       'createTask');
$router->map('GET|POST', '/admin/groups/',                                                       GroupView,            'groups');
$router->map('GET|POST', '/admin/groups/edit/[i:group_id]/',                                     GroupDetailView,      'groupDetail');
$router->map('GET|POST', '/admin/inquiries/',                                                    InquiriesListView,    'siteRequests');
$router->map('GET|POST', '/admin/site/settings/',                                                SiteSettingsView,     'siteSettings');
$router->map('GET|POST', '/admin/me/',                                                           UserProfileView,      'profile');
$router->map('GET',      '/admin/files/',                                                        FileManagerView,      'fileManager');
$router->map('GET|POST', '/admin/files/imdir/[i:id]/[asc|desc:order]/[i:step]?/name=[*:name]?/', ImageDirView,         'imageDir');
$router->map('GET|POST', '/admin/files/imdir/[i:dir_id]/image/[i:image_id]/',                    ImageDetailView,      'imageDetail');
$router->map('GET',      '/admin/modules/banners/',                                              BlankPageView,        'banner');
$router->map('GET',      '/admin/modules/cart/',                                                 BlankPageView,        'cart');
$router->map('GET',      '/admin/modules/polls/',                                                BlankPageView,        'poll');
$router->map('GET',      '/admin/modules/comments/',                                             BlankPageView,        'comments');
$router->map('GET',      '/admin/modules/ticksys/',                                              BlankPageView,        'ticksys');
$router->map('GET',      '/admin/help/',                                                         BlankPageView,        'help');
$router->map('GET|POST',      '/admin/translations/',                                            TranslationListView,  'translations');
$router->map('GET',      '/admin/pages/',                                                        PagesListView,        'pageslist');
$router->map('GET|POST', '/admin/custom/[i:id]/',                                                CMSPageView,          'customPage');
