<?php
error_reporting(E_ALL);
ini_set("display_errors", false); 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

$BASE_PATH = __DIR__.'/../';

require_once $BASE_PATH.'/lib/lib_functions.php';
require_once $BASE_PATH.'/lib/UrlFactory.php';
require_once $BASE_PATH.'/modules/vendor/autoload.php';
require_once $BASE_PATH.'/settings.php';;
require_once $BASE_PATH.'/modules/vendor/altorouter/altorouter/AltoRouter.php';

require_once __DIR__ .'/vendor/autoload.php';
require_once __DIR__ .'/Tree.php';
require_once __DIR__ .'/helpers.php';


$services = new Pimple\Container();
$makContainer = new Maksoft\Containers\GlobalContainer();
$response     = new Response();
$router       = new AltoRouter();
$request      = Request::createFromGlobals();

$services->register($makContainer);

$o_page       = $services['o_page'];

$custom_js = array();

require_once __DIR__.'/routes.php';

$match = $router->match();
var_dump($match);

$view_name = sprintf('Maksoft\Administration\Views\%s', $match['target']);

$custom_pages = array();
if($admin_group =$services['gate']->site()->getGroupByName('admin', $o_page->_site['SitesID'])) { 
    $custom_pages = $services['gate']->site()->getGroupPages($admin_group->group_id);
    #var_dump($custom_pages);
}
if($match and class_exists($view_name)) {
    ob_start();
    $cls = new $view_name($request, $services, $o_page, $user, $router);
        call_user_func_array(array($cls, 'as_view'), $match['params']);
        $content = ob_get_contents();
    ob_end_clean();
    require_once __DIR__.'/templates/base.php';
} else {
    header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
}

