<?php
include "../modules/vendor/autoload.php";
include "../modules/settings.php";
include "../lib/lib_page.php";
use Maksoft\Admin\Auth\Login;
use Maksoft\Admin\Auth\FailedAttempts;
session_start();

$o_page = new page();
$client_ip = \Maksoft\Admin\FormCMS\Save::client_ip();
$db = new \Maksoft\Core\MysqlDb($env);
$db->exec("SET NAMES cp1251");
$gate = new Maksoft\Gateway\Gateway($db);
$login = new Login($gate);
$login->attach( new FailedAttempts($gate));
$block = $gate->auth()->isBlocked($client_ip, get_class($login));
?>

<?php
$redirect = function($site){
    return header("Location: /page.php?n={$site["StartPage"]}&SiteID={$site["SitesID"]}");
};

?>
<!DOCTYPE html>
<html lang="bg">
  <head>
	<title>���� � ��������� �� Maksoft</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
    <link rel="icon" type="image/x-icon" href="http://maksoft.net/admin/assets/images/favicon.ico">
    <link href="http://maksoft.net/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="http://maksoft.net/admin/assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="http://maksoft.net/admin/assets/css/login.css">

  </head>
  <body>
	    <div class="site-container">

        <div id="content" class="mainContent">       
                                    <div class="logo-admin wrap"><img src="http://maksoft.net/admin/assets/images/logo-admin.png" width="150px" alt="Maksoft.net" class="mobileimg"></div>             
                <div id="emp-login" class="wrap">
    <?php
if($block){
$time = date("d-m-Y H:i", $block->block_until);
echo <<<HERE
<div class="alert alert-danger">
<strong>�������� ������!</strong> ���������� 5 ��������� ����� �� ������� � ������ �������. ��� ��� �������� �� {$time}.

</div>
HERE;
    exit();
}

if($_SERVER["REQUEST_METHOD"] === "POST"){
    try{
        $form = new \Maksoft\Admin\Auth\LoginForm($_POST);
        $form->is_valid();
        $login->init($form->_cleaned_data["username"], $form->_cleaned_data["password"], $client_ip);
        //TODO add login timeout function
        echo $redirect($o_page->_site);
    } catch (Exception $e){ ?>
             <div class="alert alert-danger" role="alert">
                   <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                   <span class="sr-only">Error:</span>
                   <?php echo $e->getMessage(); ?>
             </div>
    <?}
} elseif (isset($_SESSION["user"]) && is_object($_SESSION["user"]) && isset($_SESSION['user']->ID) && isset($_SESSION['user']->ParentUserID)){
    $redirect($o_page->_site);
}else {
    $form = new \Maksoft\Admin\Auth\LoginForm();
}
$form->class = "class=\"form-group\"";
echo $form;
?>
    <div id="emp-login" class="wrap">
        <script>
            // This is called with the results from from FB.getLoginStatus().
            function statusChangeCallback(response) {
                console.log('statusChangeCallback');
                console.log(response);
                // The response object is returned with a status field that lets the
                // app know the current login status of the person.
                // Full docs on the response object can be found in the documentation
                // for FB.getLoginStatus().
                if (response.status === 'connected') {
                    // Logged into your app and Facebook.
                    testAPI();
                } else if (response.status === 'not_authorized') {
                    // The person is logged into Facebook, but not your app.
                    document.getElementById('status').innerHTML = 'Please log ' +
                    'into this app.';
                } else {
                    // The person is not logged into Facebook, so we're not sure if
                    // they are logged into this app or not.
                    document.getElementById('status').innerHTML = 'Please log ' +
                    'into Facebook.';
                }
            }

            // This function is called when someone finishes with the Login
            // Button.  See the onlogin handler attached to it in the sample
            // code below.
            function checkLoginState() {
                FB.getLoginStatus(function(response) {
                    statusChangeCallback(response);
                });
            }

            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '115122745948',
                    cookie     : true,  // enable cookies to allow the server to access
                                        // the session
                    xfbml      : true,  // parse social plugins on this page
                    version    : 'v2.5' // use graph api version 2.5
                });

                // Now that we've initialized the JavaScript SDK, we call
                // FB.getLoginStatus().  This function gets the state of the
                // person visiting this page and can return one of three states to
                // the callback you provide.  They can be:
                //
                // 1. Logged into your app ('connected')
                // 2. Logged into Facebook, but not your app ('not_authorized')
                // 3. Not logged into Facebook and can't tell if they are logged into
                //    your app or not.
                //
                // These three cases are handled in the callback function.

                FB.getLoginStatus(function(response) {
                    statusChangeCallback(response);
                });

            };

            // Load the SDK asynchronously
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            // Here we run a very simple test of the Graph API after login is
            // successful.  See statusChangeCallback() for when this call is made.
            function testAPI() {
                console.log('Welcome!  Fetching your information.... ');
                FB.api('/me', function(response) {
                    console.log('Successful login for: ' + response.name);
                    document.getElementById('status').innerHTML =
                        'Thanks for logging in, ' + response.name + '!';
                });
            }
        </script>

        <!--
          Below we include the Login Button social plugin. This button uses
          the JavaScript SDK to present a graphical Login button that triggers
          the FB.login() function when clicked.
        -->

        <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
        </fb:login-button>

        <div id="status">
        </div>

    </div>
        </div>   
		
    </div>
	<footer>
		<div id="footer-widgets">
			<div class="wrap">

<div class="fb-like" data-href="https://facebook.com/Maksoft.Net/" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>

			</div>
		</div>


        <div id="footer">
            <div class="wrap">
				<div id="copyright">Copyright &copy; 2016 Maksoft.net.  All rights reserved.</div>
            </div>
        </div>
	</footer>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/bg_BG/sdk.js#xfbml=1&version=v2.5&appId=1650640101821444";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
		
    </body>
</html>
