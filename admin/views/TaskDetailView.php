<?php


class TaskDetailView extends TemplateView
{
    protected $template_name = 'task_detail.php';

    public function as_view($task_id)
    {
        $user_id = $this->o_page->_user['ID'];
        $task = $this->services['gate']->task()->getTask($task_id, $user_id);
        $comments_from_user = array_diff(array($task->from_user, $task->to_user), array($user_id));
        $this->services['gate']->task()->setCommentStateToReaded($task_id, implode($comments_from_user));
        $taskEndDate= new DateTime($task->end_date);
        $today = new DateTime();
        $dayDifference = $taskEndDate->diff($today)->format("%a");
        $editTaskForm = new EditTaskForm($this->services, $task, $_POST);
        if($editTaskForm->is_valid()) {
            $editTaskForm->save();
        }
        $addCommentForm = new CreateTaskCommentForm($this->services, $task, $_POST);
        if($addCommentForm->is_valid()) {
            $addCommentForm->save();
            $task = $this->services['gate']->task()->getTask($task_id, $this->o_page->_user['ID']);
        }
        $completeTaskForm = new TaskCompleteForm($this->services, $task);
        if($completeTaskForm->is_valid()) {
            $completeTaskForm->save();
            $response = new Symfony\Component\HttpFoundation\RedirectResponse($this->route->generate('tasks'));
            $response->send();
        }
        $taskComments = $this->services['gate']->task()->getTaskComments($task->taskID);
        require_once __DIR__.'/pages/'.$this->template_name;
    }
}
