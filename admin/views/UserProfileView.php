<?php


class UserProfileView extends TemplateView
{
    protected $template_name = 'profile.php';
    
    public function get_context_data()
    {
        $default_image="https://lh3.googleusercontent.com/-H7_QuNyFVMM/AAAAAAAAAAI/AAAAAAAAAAA/1095ekL4OEA/photo.jpg";
        $this->add('gate', $this->services['gate']);
        $avatar = $this->services['gate']->user()->getAvatar($this->o_page->_user["ID"]);
        if($avatar){
            $default_image=$avatar->picture;
        }
        $this->add('default_image', $default_image);

        if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["edit_profile"])){
            $profile = new \Maksoft\Admin\Profile\Edit($this->services['gate'], $_POST);
            $profile->setName("profile");
            try {
                $profile->is_valid();
                $profile->update($this->o_page);
                echo '<div class="alert alert-success" role="alert">Успешно променихте вашият профил</div>';
            } catch (Exception $e) {
            }
        } else {
            $profile = new \Maksoft\Admin\Profile\Edit($this->services['gate']);
            $profile->setName("profile");
            $profile->setID("profile");
            $profile->load($this->o_page);
        }
        if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['bulstat'])){

            try {
                $company = new \Maksoft\Admin\Profile\Company($this->services['gate'], $this->o_page, $_POST);
                $company->is_valid();
                $company->update();
                echo '<a href="#" class="btn btn-success toastr-notify" data-progressBar="true" data-position="top-right" data-notifyType="success" data-message="Успешно променихте данните на вашата фирма!">Show Success</a>
';
            } catch (Exception $e) {
            }
        } else {
            $company = new \Maksoft\Admin\Profile\Company($this->services['gate'], $this->o_page);
        }
        if($this->o_page->_user["FirmID"]){
            $company->load($this->o_page->_user["FirmID"]);
        }

        if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['new_password'])){
            $password = new \Maksoft\Admin\Profile\Password($this->services['gate'], $_POST);
            $password->setName("password");
            try {
                $password->is_valid();
                $password->update($this->o_page);
                echo '<a href="#" class="btn btn-success toastr-notify" data-progressBar="true" data-position="top-right" data-notifyType="success" data-message="Успешно променихте вашата парола!">Show Success</a>';
            } catch (Exception $e) {
            }
        } else {
            $password = new \Maksoft\Admin\Profile\Password($this->services['gate']);
            $password->setName("password");
            $password->load($this->o_page);
        }

        $this
            ->add('profile', $profile)
            ->add('password', $password)
            ->add('company', $company);
        return parent::get_context_data();
    }
}
