<?php


class CreateOrEditPageView extends TemplateView
{
    protected $template_name = 'edit_create.php';

    public function as_view($page_id=null)
    {
        return editOrCreatePage($this->request, $this->o_page, $this->services, $this->user, $page_id);
    }
}
