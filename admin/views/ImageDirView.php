<?php


class ImageDirView extends TemplateView
{
    public function as_view($id, $order, $step=0, $name='') 
    {
        $urlData = array('id' => $id);
        if(!$step) {
            $step = 1;
        }

        if($step > 0 ) {
            $urlData['step'] = $step;
        }


        $imagesPerPage = 100;
        $max = $step * $imagesPerPage - $imagesPerPage; 
        $max = $max > 0 ? $max : 0;
        $directory = $this->services['gate']->site()->getSiteImageDir($id, $order);
        $form = new UploadImageForm($this->services, $directory, $_POST, $_FILES);
        if($form->is_valid()) {
            $form->save();
        }

        $urlData['order'] = $order;
        if($name) {
            $name = iconv('utf8', 'cp1251', urldecode($name));
            $urlData['name'] = $name;
            $files = $this->services['gate']->site()->getSiteImagesFilteredByName($id, $name, $order);
            $totalFilesCount = count($files);
        } else {
            $files = $this->services['gate']->site()->getSiteImages($id, $max, $imagesPerPage);
            $totalFilesCount = $this->services['gate']->site()->countDirectoryImages($id);
        }

        $files = array_slice($files, $max, $imagesPerPage);
        return require_once __DIR__ .'/pages/directory_list.php';
    }
}
