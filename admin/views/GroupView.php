<?php


class GroupView extends TemplateView
{
    protected $template_name = 'groups.php';

    public function get_context_data() 
    {
        #$pages = $services['gate']->page()->getPagesByGroupId($group_id, $o_page->_site['SitesID']);
        $this
            ->add('tmpGroups', $this->services['gate']->site()->getSiteGroups($this->o_page->_site['SitesID']))
            ->add('tree', new Tree($this->get('tmpGroups')))  
            ->add('form', new CreateGroupForm($this->services, null, $_POST)); 

        if($this->get('form')->is_valid()) {
            return $this->get('form')->save();
        }

        return parent::get_context_data();
    }

}
