<?php


class SiteMapView extends TemplateView
{
    protected $template_name = 'sitemap.php';
    
    public function get_context_data()
    {
        $this
            ->add('tmp_pages', $this->services['gate']->page()->getSiteMapPages($this->o_page->_site['SitesID']))
            ->add('tree', new Tree($this->get('tmp_pages')));
        return $this->context;
    }
} 
