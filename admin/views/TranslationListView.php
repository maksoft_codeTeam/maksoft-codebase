<?php


class TranslationListView extends TemplateView
{
    protected $template_name = 'translations_list.php';
    public function get_context_data() 
    {
        $SiteID = $this->services['o_page']->_site['SitesID'];
        $form = new TranslationCreateForm($this->services, $this->request->request->all());
        $deleteForm = new DeleteTranslationForm($this->services, $this->request->request->all());
        $response = new Symfony\Component\HttpFoundation\RedirectResponse($this->route->generate('translations'));
        if($form->is_valid()) {
            $form->save();
            $response->send();
        }
        if($deleteForm->is_valid()) {
            $deleteForm->save();
            $response->send();
        }

        $translations = $this->services['gate']->lang()->getAllTranslations($SiteID);
        $this->add('translations', $translations);
        $this->add('form', $form);
        $this->add('deleteForm', $deleteForm);
        return parent::get_context_data();
    }
}
