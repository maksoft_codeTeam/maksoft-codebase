<?php
namespace Maksoft\Administration\Views;


class SiteSettingsView extends TemplateView
{
    protected $template_name = 'site_settings.php';
    public function get_context_data() {
        $SiteID = $this->o_page->_site['SitesID'];
        $tmpl_id = $this->o_page->_site['Template'];
        $template_query = $this->o_page->db_query("SELECT * FROM Templates t left join CSSs c on t.default_css = c.cssID WHERE t.ID = '$tmpl_id' ");
        $this
            ->add('Template', $this->o_page->fetch("object", $template_query))
            ->add('o_page', $this->o_page)
            ->add('SiteID', $SiteID)
            ->add('Site', (object) $this->o_page->_site);

        return parent::get_context_data();
    }
}

