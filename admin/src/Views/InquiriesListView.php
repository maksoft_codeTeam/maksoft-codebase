<?php
namespace Maksoft\Administration\Views;


class InquiriesListView extends TemplateView
{
    protected $template_name = 'site_inquiries.php';
    public function get_context_data() 
    {
        $this
            ->add('SiteID', $o_page->_site['SitesID'])
            ->add('inquiries', $this->services['gate']->site()->getSiteRequestsList($this->o_page->_site['SitesID']))
            ->add('uSites', array_map(function($site) {
                                    return (integer) $site['SiteID'];
                              }, $this->o_page->get_uSites($this->user->ID, "sa.notifications>0")))
            ->add('otherSitesInquiries', call_user_func_array(array($this->services['gate']->site(), 'getSiteRequestsListForMultipleSites'), $this->get('uSites')));
        return parent::get_context_data();
    }

    public function as_view()
    {
        set_orders(\page::$uID, $this->o_page->_site, $this->o_page->_user);
        return parent::as_view();
    }
}
