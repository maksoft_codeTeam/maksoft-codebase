<?php
namespace Maksoft\Administration\Views;

use Maksoft\Administration\Forms\EditImageForm;
use Maksoft\Administration\Forms\SetImageAsSiteLogoForm;


class ImageDetailView extends TemplateView 
{
    public function as_view($dirId, $imageId) 
    {
        $dir = $this->services['gate']->site()->getSiteImageDir($dirId);
        $image = $this->services['gate']->site()->getImage($imageId);
        $imageDirs = $this->services['gate']->site()->getSiteImageDirs($this->services['o_page']->_site['SitesID']);
        $form = new EditImageForm($this->services, $image, $imageDirs, $_POST);
        $setAsLogoForm = new SetImageAsSiteLogoForm($this->services, $image, $_POST);
        if($form->is_valid()) {
            $form->save();
            $image = $this->services['gate']->site()->getImage($imageId);
        }
        if($setAsLogoForm->is_valid()) {
            $setAsLogoForm->save();
        }

        $pages = $this->services['gate']->site()->getPagesByImage($imageId, $this->services['o_page']->_site['SitesID']);

        return require_once __DIR__ .'/../../pages/image_detail.php';
    }
}
