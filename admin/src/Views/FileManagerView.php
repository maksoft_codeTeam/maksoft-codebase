<?php
namespace Maksoft\Administration\Views;


class FileManagerView extends TemplateView  {
    public $template_name = 'filemanager.php';

    public function get_context_data()
    {
        $this->add('user', $this->user)
            ->add('o_page', $this->services['o_page']);
        $this->add('imageDirs', $this->services['gate']->site()->getSiteImageDirs($this->services['o_page']->_site['SitesID']));
        return parent::get_context_data();
    }
}
