<?php
namespace Maksoft\Administration\Views;


class TemplateView
{
    protected $context = array();
    protected $template_name;

    public function __construct($request, $services, $o_page, $user, $route) {
        $this->request = $request;
        $this->services = $services;
        $this->o_page = $o_page;
        $this->user = $user;
        $this->route = $route;
        $this->resolver = new \UrlFactory($this->request);
    }

    public function get_context_data()
    {
        return $this->context;
    }

    public function as_view() {
        if(!file_exists(__DIR__.'/../../pages/'.$this->template_name)) {
            throw new \Exception("{$this->template_name} doesnt found!");
        }
        $context = $this->get_context_data();
        extract($context, EXTR_OVERWRITE);
        require_once __DIR__.'/../../pages/'.$this->template_name;
    }

    public function add($name, $value) {
        $this->context[$name] = $value;
        return $this;
    }

    public function get($name) {
        return isset($this->context[$name]) ? $this->context[$name] : null;
    }
    
}

