<?php
namespace Maksoft\Administration\Views;
use Maksoft\Administration\Forms\CMSTranslation;
use Maksoft\Administration\Forms\PageTranslationForm;
use Maksoft\Administration\Forms\AddPriceForm;
use Maksoft\Administration\Forms\CreatePageForm;

use \Symfony\Component\HttpFoundation\RedirectResponse;


class CreateOrEditPageView extends TemplateView
{
    protected $template_name = 'edit_create.php';

    public function as_view($page_id=null)
    {
        $request = $this->request;
        $o_page = $this->o_page;
        $di_container = $this->services;
        $user = $this->user;
        $main_language = $o_page->get_sLanguage(null, true);
        $site_versions = $o_page->get_sVersions();

        $gate = $di_container['gate'];
        $form = new PageTranslationForm($gate, $page_id, $_POST, $o_page->_site['SitesID']);

        $page = (object) CMSTranslation::$emptyPage;
        $Site = $di_container['gate']->site()->getSiteDataByUrl();
        $SiteID = $Site->SitesID;
        $page->SiteID = $Site->SitesID;

        if($page_id) {
            $temp_page = $gate->page()->getPage($page_id);
            if($temp_page->SiteID == $page->SiteID) {
                $page = $temp_page;
            }
        }

        $priceForm = new AddPriceForm($gate, null, null, $_POST);

        $p_parent = $gate->page()->getPage(!empty($page->ParentPage) ? $page->ParentPage : $o_page->_site['StartPage']);
        /*
         *
         */ 
        $page->title = string_sanitize($page->title);
        $page->Name = string_sanitize($page->Name);
        $createPageForm = new CreatePageForm($di_container['gate'], $page, $o_page, $_POST, $_FILES);
        if($form->is_valid()) {
            $form->save();
            $response = new RedirectResponse($this->route->generate('editPage', array('id' => $page_id)));
            $response->send();
        }
        if($_SERVER['REQUEST_METHOD'] === 'POST' and isset($_POST['action']) and $_POST['action'] == $createPageForm->action->value){
            $createPageForm->is_valid();
            $n = $createPageForm->save($page);

            $prices = array_filter($request->request->get('price', array()), function($e) {
                return !empty($e['code']);
            });

            foreach($prices as $price) {
                $p = new AddPriceForm($gate, $n, $page->SiteID, $price);
                if($p->is_valid()) {
                    $p->save();
                }
            }
            $response = new RedirectResponse($this->route->generate('editPage', array('id' => $page_id)));
            $response->send();
        }

        /*
         * Данните за селекта
         */
        $img_dirs = array();
        array_map(function($dir) use (&$img_dirs) {
            $img_dirs[$dir["ID"]] = $dir['Name'];
        }, $o_page->get_sImageDirectories());

        $pageGroups = $gate->site()->getSiteGroups(empty($page->SiteID) ? $o_page->_site['SitesID'] : $page->SiteID);
        foreach ($pageGroups as $group) {
            $groups[$group->group_id] = $group->group_title;
        }

        $pTemplate = $o_page->get_pTemplate($no);
        $templates = array();
        $template_id = isset($pTemplate['pt_id']) ? $pTemplate['pt_id'] : null;
        foreach($o_page->get_pTemplates() as $pTemplate) {
            $templates[$pTemplate['pt_id']] = $pTemplate['pt_title'];
        }

        $siteAddons = $gate->site()->getSiteAddons($o_page->_site['SitesID']);
        $addons = array();
        foreach($siteAddons as $addon) {
            $addons[$addon->ID] = $addon->Name;
        }

        $currencies = array();
        foreach ($gate->page()->getCurrencyList() as $c) {
            $currencies[$c->currency_id] = $c->currency_key .', '.$c->currency_string;
        }
        $pagePrices = $gate->page()->getPagePrice($page->n);
        $pagePrices = array_merge($pagePrices ? $pagePrices : array(), array(0 => array()));
        $tmp_pages = $gate->page()->getSiteMapPages($o_page->_site['SitesID']);
        $tree = new \Tree($tmp_pages);
        require_once __DIR__.'/../../pages/edit_create.php';
    }
}
