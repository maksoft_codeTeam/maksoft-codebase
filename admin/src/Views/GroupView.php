<?php
namespace Maksoft\Administration\Views;

use Maksoft\Administration\Forms\CreateGroupForm;
use \Symfony\Component\HttpFoundation\RedirectResponse;


class GroupView extends TemplateView
{
    protected $template_name = 'groups.php';

    public function get_context_data() 
    {
        #$pages = $services['gate']->page()->getPagesByGroupId($group_id, $o_page->_site['SitesID']);
        $this
            ->add('tmpGroups', $this->services['gate']->site()->getSiteGroups($this->o_page->_site['SitesID']))
            ->add('tree', new \Tree($this->get('tmpGroups')))  
            ->add('form', new CreateGroupForm($this->services, null, $_POST)); 

        if($this->get('form')->is_valid()) {
            $this->get('form')->save();
            $response = new RedirectResponse($this->route->generate('groups'));
            $response->send();
        }

        return parent::get_context_data();
    }

}
