<?php
namespace Maksoft\Administration\Views;


class PagesListView extends TemplateView
{
	protected $template_name = 'pageslist.php';
    public function get_context_data() 
    {
        $this
            ->add('tmp_pages', $this->services['gate']->page()->getSiteMapPages($this->o_page->_site['SitesID']))
            ->add('tree', new \Tree($this->get('tmp_pages')));
        $this->add('allNodes', $this->get('tree')->getNodes());
        return $this->context;
    }
}
