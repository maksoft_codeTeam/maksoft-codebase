<?php

namespace Maksoft\Administration\Views;


class CMSPageView extends TemplateView
{
    public $template_name = 'single_page.php';
    public function get_context_data() 
    {
        $this
            ->add('o_page', $this->o_page)
            ->add('o_site', $this->services['o_site'])
            ->add('services', $this->services)
            ->add('di_container', $this->services)
            ->add('request', $this->request);
        return parent::get_context_data();
    }

    public function as_view($id) {
        $pTemplate = $this->o_page->get_pTemplate($id);
        $this->add('page', new \page($id));
        $this->add('pTemplate', $pTemplate);
        return parent::as_view($id);
    }
}
