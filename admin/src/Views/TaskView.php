<?php
namespace Maksoft\Administration\Views;


class TaskView extends TemplateView
{
    protected $template_name = 'task_list.php';
    
    public function get_context_data()
    {
        $filterOnlyActive = function($task) {
            return strtotime($task->end_date) >= time();
        };

        $filterElpasedTasks = function($task) {
            return strtotime($task->end_date) < time();
        };

        $tasks = $this->services['gate']->task()->getTasks($this->o_page->_user['ID']);
        $activeTasks = array_filter($tasks, $filterOnlyActive);
        $expiredTasks = array_filter($tasks, $filterElpasedTasks);

        $this
            ->add('o_page', $this->o_page)
            ->add('activeTasks' , $activeTasks)
            ->add('expiredTasks', $expiredTasks)
            ->add('repeatedTasks' , $this->services['gate']->task()->getUserRepeatedTasks($this->get('o_page')->_user['ID']))
            ->add('assignedTasks' , $this->services['gate']->task()->getAssignedTasks($this->get('o_page')->_user['ID']))
            ->add('upcomingTasks' , $this->services['gate']->task()->getUpcomingTasks($this->get('o_page')->_user['ID']));
        return $this->context;
    }
}
