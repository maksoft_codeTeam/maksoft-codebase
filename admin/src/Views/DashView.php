<?php
namespace Maksoft\Administration\Views;


class DashView extends TemplateView 
{
    public $template_name = 'dashboard.php';

    function get_context_data()
    {
        $stats = $this->services['gate']->stats();
        $tasks = $this->services['gate']->task()->getTasks($this->o_page->_user['ID']);
        $inquiries = $this->services['gate']->site()->getSiteRequestsList($this->o_page->_site['SitesID']);
        $taskGate = $this->services['gate']->task();
        $user_id = $this->o_page->_user['ID'];
        $this
            ->add('statsGate', $stats)
            ->add('SiteID', $this->o_page->_site['SitesID'])
            ->add('lastEditedPages', $stats->getLastEditedPages($this->get('SiteID')))
            ->add('activePagesCount', $stats->getCountActivePages($this->get('SiteID')))
            ->add('registeredUsersCount', $stats->getRegisteredUsersCount($this->get('SiteID')))
            ->add('latestQueries', call_user_func_array(array($this->services['gate']->site(), 'getSiteRequestsListForMultipleSites'), array($this->get('SiteID'))))
            ->add('inquiries', array_slice($inquiries, 0,10))
            ->add('countNewComments', function($task) use($taskGate, $user_id) {
                return $taskGate->countNewTaskComments($task->taskID, $user_id);
            })
            ->add('isMaksoftEmployee', $this->services['gate']->user()->getEmployeePermission($user_id))
            ->add('expired', function($task) {
                return strtotime($task->end_date) < time();
            })
            #->add('getMostVisitedPages', $stats->getMostVisitedPages($this->services['o_page']->_site['SitesID']))
            ->add('tasks', array_slice($tasks, 0, 10));
        return $this->context;
    }
}
