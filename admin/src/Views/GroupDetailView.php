<?php
namespace Maksoft\Administration\Views;

use Maksoft\Administration\Forms\AddPageToGroupForm;
use Maksoft\Administration\Forms\DeletePageToGroupPage;
use Maksoft\Administration\Forms\DeleteGroupForm;
use Maksoft\Administration\Forms\DeleteGroupFromListForm;
use Maksoft\Administration\Forms\CreateGroupForm;

use \Symfony\Component\HttpFoundation\RedirectResponse;


class GroupDetailView extends TemplateView 
{
    public function as_view($group_id) {
        $groupDetail = $this->services['gate']->page()->get_pGroupInfo($group_id);
        $form = new AddPageToGroupForm($this->services, $groupDetail, $_POST);
        $deleteForm = new DeletePageToGroupPage($this->services, $groupDetail, $_POST);
        $deleteGroupForm = new DeleteGroupForm($this->services, $groupDetail, $_POST);
        $deleteGroupFromListing = new DeleteGroupFromListForm($this->services, $_POST);

        if($form->is_valid()) { $form->save(); }
        if($deleteGroupFromListing->is_valid()) { $deleteGroupFromListing->save(); }
        if($deleteForm->is_valid()) { $deleteForm->save(); }
        if($deleteGroupForm->is_valid()) {
            $deleteGroupForm->save();
            $response = new RedirectResponse($this->route->generate('groups'));
            $response->send();
        }
        $createGroup = new CreateGroupForm($this->services, $groupDetail, $_POST);
        if($createGroup->is_valid()) {
            $createGroup->save();
        }
        $childGroups = $this->services['gate']->site()->getGroupsByParentGroupId($group_id);
        $groupPages  = $this->services['gate']->site()->getGroupPages($group_id);
        require_once __DIR__.'/../../pages/group_detail.php';
    }
}
