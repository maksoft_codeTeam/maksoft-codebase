<?php
namespace Maksoft\Administration\Views;

use Maksoft\Administration\CreateTaskForm;


class CreateTaskView extends TemplateView  
{ 
    public $template_name = 'task_create.php';

    public function get_context_data()
    {
        $createTaskForm = new CreateTaskForm($this->services, $_POST);
        $this->add('createTaskForm', $createTaskForm);
        if($createTaskForm->is_valid()) {
            $createTaskForm->save();
        }
        return parent::get_context_data();
    }
}
