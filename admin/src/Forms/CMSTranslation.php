<?php
namespace Maksoft\Administration\Forms;


class CMSTranslation
{
    public static $emptyPage = array(
        'n' => '',
        'an' => '',
        'ParentPage' => '',
        'Name' => '',
        'slug' => ' ',
        'title' => '',
        'tags' => '',
        'url_rules' => '',
        'PHPvars' => '',
        'textStr' => '',
        'PHPcode' => '',
        'toplink' => '',
        'show_link' => '',
        'show_group_id' => '',
        'show_link_order' => '',
        'show_link_cols' => '',
        'make_links' => '',
        'form' => '',
        'PageURL' => '',
        'imageNo' => '',
        'imageWidth' => '',
        'image_allign' => '',
        'ankID' => '',
        'PrintVer' => '',
        'SiteID' => '',
        'date_added' => '',
        'author' => '',
        'date_modified' => '',
        'preview' => '',
        'addtype' => '',
        'SecLevel' => '',
        'status' => '',
        'sort_n' => '',
        'imageID' => '',
        'imageName' => '',
        'image_src' => '',
        'image_dir' => '',
        'image_date_added' => '',
        'image_date_modified' => '',
    );

    public static $config = array(
    'bg' => array(
        'image_allign' => array(
            0 => '--- �� ������������ --- ',
            '���������' => array(
                1 => '�� ������������',
                21 => '�������� �����',
                22 => '���'
            ),
            '��� ������' => array(
                2 => '����',
                3 => '�����',
                9 => '������'
            ),
            '��� ������' => array(
                10 => '����',
                11 => '�����',
                12 => '������'
            )
        ),
        'show_link' => array(
            1 => '������ ������ ��� �������������',
            '������ ������������ �� �������������' => array(
                    2  => '������ ����������, ������ � �������� �� �������������',
                    5  => '������ ������ � �������� �� �������������',
                    6  => '������ ���� ������������ �� �������������',
                    10 => '���������� � ������������ �� �������������'

                ),
            '���������' => array(
                    3 => '������ ���������� � �������� �� �������������',
                    9 => '������ ���� �������� �� �������������',
                ),
            '����� ��� �������������' => array(
                    7  => '������ �������� � ������� ����� �� �������������',
                    8  => '������ ������� ����� �� �������������',
                    11 => '������ ���������� � ������ ����� �� �������������',
                ),
            '������������ ������' => array(
                    13 => '������ ��������, ���������� � �������������� ������',
                    14 => '������ �������� � �������������� ������',
                    15 => '������ ���������� � �������������� ������',
                ),
            '����� ������' => array(
                    0 => '�� ������������ ������ ��� �������������',
                )
        ),
        'show_link_cols' => array(
            1 => '� 1 ������',
            2 => '� 2 ������',
            3 => '� 3 ������',
            4 => '� 4 ������',
            6 => '� 6 ������',
        ),
        'show_link_order' => array(
            '�� ������������' => array(
                'ASC' => '�������� ��� 0-9',
                'DESC' => '�������� ��� 9-0'
            ),
            '�� ��� �� ����������' => array(
                'p.Name ASC' => '��� �������� ��� A-Z',
                'p.Name DESC' => '�������� ��� Z-A'
            ),
            '�� ����' => array(
                'p.date_added ASC' => '��� �������� ��� 0-9',
                'p.date_added DESC' => '� �������� ��� 9-0'
            ),
            '�� ������' => array(
                'p.toplink ASC' => '�������� ���',
                'p.toplink DESC' => '�������� ���'
            ),
            '�����' => array(
                'RAND()' => '����������'
            ),
        ),
        'make_links' => array(
            1 => '� ��������� ������',
            2 => '������ ���� �� ����������',
            3 => '������ ���� �� ����������',
            4 => '������ ���� �� ����������',
            0 => '��� ������'
        ),
        'toplink' => array(
            '-', '��� ������' , '������� ����', '���. ������' , 'vip ������',
            '����� ������'
        ),
        'status' => array(
             1  => '������',
             0  => '���������',
             -1 => '����������'
        )
    ),
    'en' => array(

    )
);

}
