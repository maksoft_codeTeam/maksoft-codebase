<?php
namespace Maksoft\Administration\Forms;


use \Jokuf\Form\Field\Text;
use \Jokuf\Form\Field\Textarea;
use \Jokuf\Form\Field\Select;
use \Jokuf\Form\Field\Radio;
use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Field\Integer;
use \Jokuf\Form\Field\Recaptcha;
use \Jokuf\Form\Validators\NotEmpty;
use \Jokuf\Form\Validators\MinLength;
use \Jokuf\Form\Exceptions\ValidationError;
use \Jokuf\Form\Bootstrap;


class CreateTaskCommentForm extends Bootstrap
{
    private $gate, $n, $SiteID;

    public function __construct($services, $task,  $post_data) {
        $class = 'form-control';
        $this->services = $services;
        $this->task = $task;
        
        $this->message = Hidden::init();

        $this->action = Hidden::init()->add('value', 'insert_comment');
        parent::__construct($post_data);
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function save()
    {
        $cleaned = $this->clean_data();
        return $this->services['gate']->task()->addCommentToTask(
            $this->task->taskID,
            $this->services['o_page']->_user['ID'],
            strip_tags($cleaned['message'])
        );
    }
}

