<?php
namespace Maksoft\Administration\Forms;


use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\MinLength;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class TranslationCreateForm extends Bootstrap
{
    private $services, $versions;
    public $title = '������ ������';

    public function __construct($services,  $post_data)
    {
        $this->services = $services;
        $this->versions = $services['o_site']->get_sVersions();

        $class = 'form-control';

        $tmp = array();
        foreach($this->versions as $version) {
            $tmp[$version['language_id']] = $version['language_text'];
        }

        $this->base = Text::init()
            ->add("label", "��")
            ->add_validator(NotEmpty::init(true))
            ->add('class', 'form-control');

        $this->language = Select::init()
            ->add("label", "����")
            ->add('data', $tmp)
            ->add('class', 'form-control');

        $this->text = Text::init()
            ->add("label", '������')
            ->add_validator(NotEmpty::init(true))
            ->add('class', 'form-control');

        $this->action = Hidden::init()
            ->add('value', 'insertTranslation');

        $this->submit = Submit::init()
            ->add('class', 'btn btn-default btn-lg btn-block')
            ->add('value', '������');

        parent::__construct($post_data);
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function save()
    {
        $clean = $this->clean_data();
        $SiteID = $this->services['o_page']->_site['SitesID'];
        $lang_id = $clean['language'];
        return $this->services['gate']->lang()->insertTranslation($lang_id, $SiteID, $clean['base'], $clean['text']);
    }
}
