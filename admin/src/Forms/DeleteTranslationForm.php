<?php
namespace Maksoft\Administration\Forms;


use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Validators\NotEmpty;
use \Jokuf\Form\Bootstrap;


class DeleteTranslationForm extends Bootstrap
{
    private $services, $versions;
    public $title = '������ ������';

    public function __construct($services,  $post_data)
    {
        $this->services = $services;

        $tmp = array();
        foreach($this->versions as $version) {
            $tmp[$version['language_id']] = $version['language_text'];
        }

        $this->translation_id  = Hidden::init()
            ->add_validator(NotEmpty::init(true));

        $this->action = Hidden::init()
            ->add('value', 'deleteTranslation');

        $this->submit = Submit::init()
            ->add('onsubmit', 'return confirm("Are you sure you want to delete?");')
            ->add('class', 'btn btn-default')
            ->add('value', '������');

        parent::__construct($post_data);
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function save()
    {
        $clean = $this->clean_data();
        $SiteID = $this->services['o_page']->_site['SitesID'];
        return $this->services['gate']->lang()->deleteTranslation($clean['translation_id'], $SiteID);
    }
}
