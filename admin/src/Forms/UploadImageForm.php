<?php
namespace Maksoft\Administration\Forms;


use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\MinLength;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class UploadImageForm extends Bootstrap
{
    private $services, $directory;

    public function __construct($services, $directory,  $post, $files) {
        $class = 'form-control';
        $this->services = $services;
        $this->directory = $directory;

        $this->files = Files::init()
            ->add('name', 'files[]')
            ->add('label', '������ �����������')
            ->add('multiple', true)
            ->add('class', 'form-control');
        $this->action = Hidden::init()->add('value', 'uploadImages');
        $this->submit = Submit::init()->add('class', 'btn btn-success btn-block')->add('value', '����');
        parent::__construct($post, $files);
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function save()
    {
        $cleaned = $this->clean_data();
        $images = $this->files->normalize_files();
        if(!isset($this->files->files['name'])) {
            return false;
        }

        if(!is_array($this->files->files['name'])) {
            $this->saveImage($this->files->files, $this->directory);
            return;
        }

        foreach($images as $image) {
            $this->saveImage($image, $this->directory);
        }
    }

    public function saveImage($image, $img_dir)
    {
        $DS = DIRECTORY_SEPARATOR;
        $image_name = urlencode($image['name']);
        $full_path = $_SERVER['DOCUMENT_ROOT'].$DS.$img_dir->image_dir.$DS.$image_name;
        $web_path = $img_dir->image_dir.$DS.$image_name;
        if(file_exists($full_path)) {
            unlink($full_path);
        }

        $status = move_uploaded_file($image['tmp_name'], $full_path);
        $web_path = str_replace($_SERVER['DOCUMENT_ROOT'], '', $full_path);

        if(!$status){
            return $status;
        }

        return $this->services['gate']->page()->insertImage($image_name, $web_path, $img_dir->ID);
    }

    public function reArrayFiles(&$file_post)
    {
        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }
        return $file_ary;
    }
}

