<?php
namespace Maksoft\Administration\Forms;

use \Jokuf\Form\Field\Text;
use \Jokuf\Form\Field\Files;
use \Jokuf\Form\Field\Select;
use \Jokuf\Form\Field\Radio;
use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Field\Integer;
use \Jokuf\Form\Field\Recaptcha;
use \Jokuf\Form\Validators\NotEmpty;
use \Jokuf\Form\Validators\MinLength;
use \Jokuf\Form\Exceptions\ValidationError;
use \Jokuf\Form\Bootstrap;


class AddPageToGroupForm extends Bootstrap
{
    private $services, $group;
    public $title = '������ ��������';

    public function __construct($services, $group,  $post_data)
    {
        $this->services = $services;

        $this->group = (object) $group; 

        $class = 'form-control';
        $this->services = $services;

        $this->page = Text::init()
            ->add('label', '��� �� ��������')
            ->add('id', 'pages')
            ->add_validator(NotEmpty::init(true))
            ->add('class', 'form-control');

        $this->n = Hidden::init()
            ->add('id', 'page_id');

        $this->label = Text::init()
            ->add('label', '��������')
            ->add('class', 'form-control');
        
        $this->start_date = Text::init()
            ->add('label', '������� ����')
            ->add('data-format', "dd.mm.yyyy")
            ->add('data-lang', "en")
            ->add('class', 'form-control datepicker');

        $this->end_date = Text::init()
            ->add('label', '������ ����')
            ->add('data-format', "dd.mm.yyyy")
            ->add('data-lang', "en")
            ->add('class', 'form-control datepicker');

        $this->action = Hidden::init()
            ->add('value', 'addPageToGroup');

        $this->submit = Submit::init()
            ->add('class', 'btn btn-default btn-lg btn-block')
            ->add('value', '������');

        parent::__construct($post_data);
    }


    public function validate_start_date($field) {
        $date_timestamp = strtotime($field->value);
        $this->start_date->value = $date_timestamp < time() - 60 ? '0000-00-00' : new \DateTime($field->value); 
        return true;
    }

    public function validate_end_date($field) {
        $date_timestamp = strtotime($field->value);
        $this->end_date->value = $date_timestamp < time() - 60 ? '0000-00-00' : new \DateTime($field->value); 
        return true;
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function save()
    {
        $clean = $this->clean_data();
        $page = $this->services['gate']->page()->getPage($clean['n']);
        if($page->SiteID != $this->services['o_page']->_site['SiteID']) {
            return false;
        }

        if($page->show_group_id == $this->goup->group_id) {
            return false;
        }
        $page_id = $page->n;
        $group_id = $this->group->group_id;
        $label = $clean['label'];
        $sort_order= 999;
        $start_date = $this->start_date->value;
        $end_date = $this->end_date->value;
        $id = $this->services['gate']->site()->addPageToGroup($page_id, $group_id, $label, $sort_order, $start_date, $end_date);
        $this->services['gate']->site()->setPageToGroupSortOrder($id, $id);
    }
}
