<?php
namespace Maksoft\Administration\Forms;


use \Jokuf\Form\Field\Text;
use \Jokuf\Form\Field\Integer;
use \Jokuf\Form\Field\Select;
use \Jokuf\Form\Field\Phone;
use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Field\Files;
use \Jokuf\Form\Field\Checkbox;
use \Jokuf\Form\Field\Textarea;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Field\Recaptcha;
use \Jokuf\Form\Validators\NotEmpty;
use \Jokuf\Form\Exceptions\ValidationError;

use \Maksoft\Gateway\Gateway;

require_once __DIR__.'/CMSTranslation.php';
require_once __DIR__.'/HelpersTrait.php';

class CreatePageForm extends HelpersTrait
{
    private $gate, $page, $o_page;

    public function __construct(Gateway $gate, $page, $o_page, $post=array(), $files=array())
    {
        $this->gate = $gate;
        $this->page = $page;
        $this->o_page = $o_page;
        $this->add_attr('enctype', 'multipart/form-data');

        $base_class = 'form-control';

        $this->Name           = Text::init()
                    ->add('class', $base_class)
                    ->add('label', '���')
                    ->add('value', $page->Name)
                    ->add_validator(NotEmpty::init(true)->err_msg('�� ��� ������ ��� �� ��������'))
        ;

        $this->ParentPage      = Select::init()
                    ->add('id', 'site_list_pages')
                    ->add('class', $base_class)
                    ->add('onchange', 'set_parent_page()')
                    ->add('label', '��������� ��������');

        $this->title           = Text::init()
                    ->add('class', $base_class)
                    ->add('label', '��������')
                    ->add('value', stripslashes($page->title))
        ;

        $this->slug            = Text::init()
                    ->add('class', $base_class)
                    ->add('label', '������ ���')
                    ->add('value', $page->slug)
        ;

        $this->textStr         = Textarea::init()
                    ->add('class', $base_class)
                    ->add('label', '����� �� ����������')
                    ->add('value', stripslashes($page->textStr))
        ;

        $this->tags            = Text::init()
                    ->add('class', $base_class)
                    ->add('label', '������� ����')
                    ->add('value', stripslashes($page->tags))
        ;

        $this->notags          = Checkbox::init()
                    ->add('data', array('selected' => '������ ��������� ������ �� �����'));
        /*
         *  ���������� �� ������
         */
        $this->userFile        = Files::init()
                    ->add('class', $base_class)
                    ->add('label', '���� ������');

        $this->imageName       = Text::init()
                    ->add('value', stripslashes($page->imageName))
                    ->add('class', $base_class)
                    ->add('label', '�������� �� ������');

        $this->img_dir         = Select::init()
                    ->add('value', $page->image_dir)
                    ->add('class', $base_class)
                    ->add('label', '������ � ����������');

        $this->image_allign    = Select::init()
                    ->add('label', '������������ �� ��������: ')
                    ->add('value', $page->image_allign)
                    ->add('class', $base_class);
        $this->imageWidth      = Integer::init()
                    ->add('label', '������ �� 600px')
                    ->add('class', $base_class)
                    ->add('value', $page->imageWidth ? $page->imageWidth : 0)
                    ->add('min', 0)
                    ->add('max', 600)
                    ->add('step', 1);

        $this->show_link       = Select::init()
                    ->add('class', $base_class);
        $this->show_link_order = Select::init()
                     ->add('class', $base_class);
        $this->show_link_cols  = Select::init()
                    ->add('data', CMSTranslation::$config['bg']['show_link_cols'])
                    ->add('class', $base_class);
        $this->make_links      = Select::init()
                    ->add('data', CMSTranslation::$config['bg']['make_links'])
                    ->add('class', $base_class);

        $this->PHPcode        = Textarea::init()
                    ->add('class', $base_class)
                    ->add('label', 'PHP content')
                    ->add('value', $page->PHPcode);

        $this->PHPvars        = Textarea::init()
                    ->add('class', $base_class)
                    ->add('label', 'PHPvars')
                    ->add('value', $page->PHPvars);

        // ��������� �� ������������

        $this->show_group_id   = Select::init()
                    ->add('class', $base_class)
                    ->add('label', '������ ������������ �� �����')
                    ->add('value', $page->show_group_id);
        $this->page_template   = Select::init()
                    ->add('class', $base_class)
                    ->add('label', '�������� �� ��������');
        $this->PageURL         = Select::init()
                    ->add('class', $base_class)
                    ->add('label', '����������')
                    ->add('value', $page->PageURL)
        ;
        // ��������� �����
        // ��� �� ����������
        $this->toplink        = Select::init()
                    ->add('data', CMSTranslation::$config['bg']['toplink'])
                    ->add('class', $base_class)
                    ->add('label', '��� �� ����������')
                    ->add('value', $page->toplink)
            ;

        $this->status         = Select::init()
                    ->add('data', CMSTranslation::$config['bg']['status'])
                    ->add('class', $base_class)
                    ->add('label', '������ �� ����������')
                    ->add('value', $page->status ? $page->status : 1);

        $this->ptg_group_id   = Select::init()
                    ->add('class', $base_class)
                    ->add('label', '������ ���������� � �����');
        $this->ptg_label      = Text::init()
                    ->add('class', $base_class)
                    ->add('label', '��������� ��� �������');
        $this->ptg_end_date   = Text::init()
                    ->add('class', $base_class. ' datepicker')
                    ->add('label', '���� �� ��������');
        $this->SecLevel       = Select::init()
                    ->add('class', $base_class)
                    ->add('label', '��������� �� ����������');
        // ������ �� ������ ����� � ���������
        // � �� ������� ���� AddPriceForm
        $this->date_added    = Hidden::init()->add('value', $page->date_added ? $page->date_added : date('Y-m-d H:i:s', time()));
        $this->action        = Hidden::init()->add('value', 'create_page');
        $this->lang_id       = Hidden::init();
        $this->imageNo       = Hidden::init()->add('value', $page->imageNo)->add('id', 'imageNo');
        $this->old_image_src = Hidden::init()->add('value', $page->image_src);
        $this->old_imageNo   = Hidden::init()->add('value', $page->imageNo);
        $this->no            = Hidden::init()->add('value', $page->n);
        $this->submit        = Submit::init()->add('class', 'btn btn-primary btn-lg')->add('value', '������');

        parent::__construct($post, $files);
    }

    public function validate_ptg_end_date($field) {
        $time = date('Y-m-d', strtotime($field->value));
        if($time + 60 > time()) {
            $this->ptg_end_date->value = $time;
        }

        return true;
    }

    public function validate_Name($field) {
        return !empty($field->value) and strlen($field->value) > 3;
    }

    protected function insertPage(){
        $escape = $this->getEscape($this->clean_data());
        return $this->gate->page()->insertPage(
            $escape('Name'), $escape('ParentPage'),
            $escape('title'), $escape('slug'),
            $escape('textStr'), $escape('show_link'),
            $escape('show_link_order'), $escape('show_link_cols'),
            $escape('make_links'), $escape('tags'),
            $escape('image_allign'), $escape('imageWidth'),
            $escape('PageURL'), $escape('toplink'),
            $escape('status'), $this->page->SiteID,
            $escape('SecLevel'), $this->o_page->get_uID(),
            $escape('show_group_id')
        );

    }

    protected function getEscape($clean) {
        return function ($key, $default='') use ($clean) {
                        return isset($clean[$key]) ? trim($clean[$key]) : trim($default);
                };
    }

    protected function updatePage()
    {
        $escape = $this->getEscape($this->clean_data());
        return $this->gate->page()->updatePage(
            $escape('Name'), $escape('ParentPage'),
            $escape('title'), $escape('slug'),
            $escape('textStr'), $escape('show_link'),
            $escape('show_link_order'), $escape('show_link_cols'),
            $escape('make_links'), $escape('tags'),
            $escape('image_allign'), $escape('imageWidth'),
            $escape('PageURL'), $escape('toplink'),
            $escape('status'), $escape('SecLevel'),
            $this->page->n, $escape('show_group_id')
        );
    }

    protected function insertImage()
    {

    }

    protected function insertIntoLogs()
    {

    }

    protected function insertIntoGroups($n)
    {
        $this->gate->page()->insertPageToGroup(
            $n,
            $this->ptg_group_id->value,
            $this->ptg_label->value,
            $this->ptg_end_date->value
        );
    }

    protected function insertIntoSlugs($n)
    {
        if(!array_key_exists('slug', $this->clean_data())) {
            return false;
        }

        return $this->gate->page()->insertSlug($this->slug->value, $n, $this->page->SiteID);
    }

    protected function uploadImage($image)
    {
        $clean = $this->clean_data();

        $img_dir = $this->gate->site()->getSiteImageDir($clean['img_dir']);
        if(!$img_dir) {
            $img_dir = $this->gate
                            ->site()
                            ->getSiteImageDir(
                                $this->o_page->_site['imagesdir']
                            );
        }

        return $this->saveImage($image, $img_dir);
    }

    public function validate_notags($field) {
        if($field->value == 'selected') {
            $unallowed_tags = "<hr><b><strong><br><p><ul><ol><li><i><table><tr><td><u><em><script>";
            $this->textStr->value = strip_tags($this->textStr->value, $unallowed_tags);
        }
        return true;
    }

    public function print_select($data, $initial=null)
    {
        $txt = '';
        foreach($data as $key=>$value) {
            $selected = '';

            if(is_array($value)){
                $txt .= sprintf("<optgroup label='%s'> %s </optiongroup>", $key, $this->print_select($value, $initial));
                continue;
            }
            if($key == $initial) {
                $selected = 'selected';
            }
            $txt .= sprintf("<option value='%s' %s>%s</option>", $key, $selected, $value);
        }
        return $txt;
    }

    public function validate_no($field)
    {
        return $this->page->n != $field->value;
    }

    public function save()
    {
        $clean = $this->clean_data();

        if ($this->page->n > 0) {
            $n = $this->updatePage();
        } else {
            $n = $this->insertPage();
        }

        if(!$n) {
            throw new \Exception("..");
        }

        $escape = $this->getEscape($this->clean_data());

        $this->gate->page()->insert_page_texts($n, $this->lang_id->value, $escape('Name'), $escape('title'), $escape('slug'), $escape('textStr'), $escape('tags'));

        $this->gate->page()->deletePrices($n, $this->page->SiteID);

        if(in_array('page_template', $clean) and is_integer($clean['page_template'])) {
            $this->gate->page()->insertPageTemplate($n, $clean['page_template']);
        }

        $this->insertIntoSlugs($n);

        if($this->ptg_group_id > 0) {
            $this->insertIntoGroups($n);
        }

        if (isset($clean['imageNo']) and $this->page->imageNo != $clean['imageNo']) {
            $imageID = $clean['imageNo'];
        }

        if($this->userFile->files['error'] === UPLOAD_ERR_OK) {
            $imageID = $this->uploadImage($this->userFile->files);
        }

        if(!empty($imageID)) {
            $this->gate->page()->setPageImage($n, $imageID);
        }

        $this->gate->page()->updatePageEditLog($n, $this->o_page->get_uID());

        return $n;
    }

    public function reArrayFiles(&$file_post)
    {
        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }
        return $file_ary;
    }

    public function saveImage($image, $img_dir)
    {
        $ds = DIRECTORY_SEPARATOR;
        $image_name = urlencode($image['name']);
        $full_path = $_SERVER['DOCUMENT_ROOT'].$ds.$img_dir->image_dir.$ds.$image_name;
        $web_path = $img_dir->image_dir.$ds.$image_name;
        if(file_exists($full_path)) {
            unlink($full_path);
        }

        $status = move_uploaded_file($image['tmp_name'], $full_path);
        $web_path = str_replace($_SERVER['DOCUMENT_ROOT'], '', $full_path);

        if(!$status){
            return $status;
        }

        return $this->gate->page()->insertImage($image_name, $web_path, $img_dir->ID);
    }
}
