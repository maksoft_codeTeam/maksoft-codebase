<?php
namespace Maksoft\Administration\Forms;


use \Jokuf\Form\Field\Text;
use \Jokuf\Form\Field\Select;
use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Field\Recaptcha;
use \Jokuf\Form\Validators\NotEmpty;
use \Jokuf\Form\Exceptions\ValidationError;
use \Jokuf\Form\Bootstrap;


class AddPriceForm extends Bootstrap
{
    private $gate, $n, $SiteID;

    public function __construct($gate, $n, $SiteID, $post=array())
    {
        $this->n           = $n;
        $this->gate        = $gate;
        $this->SiteID      = $SiteID;

        $base_class = 'form-control';

        $this->description = Text::init()
                                ->add('label', '��������')
                                ->add('class', $base_class)
                                ->add_validator(NotEmpty::init(true));

        $this->code        = Text::init()
                                ->add_validator(NotEmpty::init(true))
                                ->add('class', $base_class)
                                ->add('label', '���');

        $this->pcs         = Text::init()
                                ->add('class', $base_class)
                                ->add('label', '�������� ������');

        $this->qty         = Text::init()
                                ->add('class', $base_class)
                                ->add('label', '����������');

        $this->price       = Text::init()
                                ->add('class', $base_class)
                                ->add('label', '����');

        $this->currency    = Select::init()
                                ->add('class', $base_class)
                                ->add('label', '������');

        $this->action      = Hidden::init()
                                ->add('value', 'add_price');

        parent::__construct($post);
    }

    public function validate_currency($field) {
        $allowed_currencies = array(1,2,3,4);
        return in_array($field->value, $allowed_currencies);
    }

    public function validate_price($field) {
        if(!$field->value or $field->value < 0) {
            $this->price->value = 0;
        }

        return true;
    }

    public function is_valid()
    {
        try {
            parent::is_valid();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function save()
    {
        $this->gate->page()->insertPagePrice(
            $this->n,
            $this->SiteID,
            $this->description->value,
            $this->code->value,
            $this->pcs->value,
            $this->qty->value,
            $this->price->value,
            $this->currency->value
        );

    }
}
