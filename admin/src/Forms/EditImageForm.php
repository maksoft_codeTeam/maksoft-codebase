<?php
namespace Maksoft\Administration\Forms;


use \Jokuf\Form\Field\Text;
use \Jokuf\Form\Field\Files;
use \Jokuf\Form\Field\Select;
use \Jokuf\Form\Field\Radio;
use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Field\Integer;
use \Jokuf\Form\Field\Recaptcha;
use \Jokuf\Form\Validators\NotEmpty;
use \Jokuf\Form\Validators\MinLength;
use \Jokuf\Form\Exceptions\ValidationError;
use \Jokuf\Form\Bootstrap;


class EditImageForm extends Bootstrap
{
    private $gate, $n, $SiteID;

    public function __construct($services, $image, $dirs,  $post, $files) {
        $class = 'form-control';
        $this->services = $services;
        $this->image = $image;
        $this->dirs = $dirs;
        $tmp = array();
        foreach($dirs as $dir) {
            $tmp[$dir->ID] = $dir->Name;
        }
        $this->image_name = Text::init()
            ->add('label', '�������� �� ��������')
            ->add('value', $this->image->imageName)
            ->add_validator(NotEmpty::init(true))
            ->add('class', $class);

        $this->change = Files::init()
            ->add('class', $class)
            ->add('label', '�������� ��� ����');

        $this->image_dir = Select::init()
            ->add('label', '�������� � ����������')
            ->add('data', $tmp)
            ->add('value', $image->image_dir)
            ->add('class', $class);
        $this->action = Hidden::init()->add('value', 'edit_image');
        $this->submit = Submit::init()->add('class', 'btn btn-success btn-block')->add('value', '������');
        parent::__construct($post, $files);
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function validate_image_dir($field) {
        $dirs = array_map(function($dir) {
            return $dir->ID;
        }, $this->dirs);
        return in_array($field->value, $dirs);
    }

    public function validate_name($field) {
        $name = strip_tags($field->value);
        return !empty($name);
    }

    public function save()
    {
        $cleaned = $this->clean_data();
        return $this->services['gate']
                    ->site()
                    ->updateImageNameAndDir(
                        $this->image->imageID, 
                        strip_tags($cleaned['image_name']),
                        strip_tags($cleaned['image_dir'])
                    );
    }
}

