<?php
namespace Maksoft\Administration\Forms;

use \Jokuf\Form\Field\Text;
use \Jokuf\Form\Field\Textarea;
use \Jokuf\Form\Field\Select;
use \Jokuf\Form\Field\Radio;
use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Field\Integer;
use \Jokuf\Form\Field\Recaptcha;
use \Jokuf\Form\Validators\NotEmpty;
use \Jokuf\Form\Validators\MinLength;
use \Jokuf\Form\Exceptions\ValidationError;
use \Jokuf\Form\Bootstrap;


class CreateTaskForm extends Bootstrap
{
    private $gate, $n, $SiteID;

    public function __construct($services, $post_data) {
        $class = 'form-control';
        $this->services = $services;

        $this->priority = Select::init()
            ->add('class', $class)
            ->add('label', '���������')
            ->add('data', array(1 => '�����', 2 => '������', 3 => '�����'))
            ->add('value', 2);
        $usersTmp = $services['gate']->user()->getUsersForTasks();
        $users = array();
        foreach($usersTmp as $user) {
            $users[$user->userID] = $user->FullName;
        }

        $this->toUser = Select::init()
            ->add('data', $users)
            ->add('label', '�� ��������')
            ->add('class', $class);

        $this->startDate = Text::init()
            ->add('label', '&nbsp;')
            ->add('value', date('Y-m-d H:i', time()))
            ->add('data-format', 'dd-mm-yyyy')
            ->add('class', 'hasDatepicker form-control');

        $this->period = Integer::init()
            ->add('label', '����')
            ->add('class', $class .' stepper')
            ->add('min', 0)
            ->add('max', 366)
            ->add('step', 1)
            ->add('value', 1);
        $tempTypes = $services['gate']->task()->getTaskTypes();
        $types = array();
        foreach($tempTypes as $type) {
            $types[$type->task_type_ID] = $type->description;
        }
        $this->type = Select::init()
            ->add('data', $types)
            ->add('value', 1)
            ->add('label', '���')
            ->add('class', $class);

        $this->repeat_in = Select::init()
            ->add('class', $class)
            ->add('label', '&nbsp;')
            ->add('value', 0)
            ->add('data', array(
                  0 => '�� �� �������',
                  7 => '���� �������',
                  14 => '���� 2 �������',
                  30 => '�� 1 ����� ',
                  60 => '�� 2 ������',
                  90 => '�� 3 ������',
                  182 => '�� 6 ������',
                  365 => '���� 1 ������',
            ));

        $this->timeToFinish = Integer::init()
            ->add('label', '�����')
            ->add('class', $class .' stepper')
            ->add('value', 1)
            ->add('min', 0)
            ->add('max', 24)
            ->add('step', 1)
            ->add('value', 1);
        $tmpCompanies = $services['gate']->fak()->getCompaniesList();
        $companies = array();
        foreach($tmpCompanies as $company) {
            $companies[$company->ID] = $company->Name;
        }


        $this->company = Select::init()
            ->add('label', '�����')
            ->add('value', $task->ClientID)
            ->add('data', $companies)
            ->add('class', 'form-control select2');

        if($services['o_page']->_user['FirmID'] != 1097) {
            $this->company = Hidden::init()
                ->add('label', '&nbsp;')
                ->add('value', '0');
        }

        $this->subject = Text::init()
            ->add('label', '�������')
            ->add_validator(NotEmpty::init(true))
            ->add_validator(MinLength::init(5))
            ->add('class', $class);

        $this->content = Textarea::init()
            ->add('class', 'summernote form-control')
            ->add('data-height', 200)
            ->add('data-lang', 'en-US');

        $this->action = Hidden::init()->add('value', 'create_task');
        parent::__construct($post_data);
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                return false;
            }
        }
        return false;
    }

    public function save()
    {
        $cleaned = $this->clean_data();
        return $this->services['gate']->task()->createTask(
            $cleaned['subject'],
            $cleaned['content'],
            $cleaned['startDate'],
            $this->services['o_page']->_user['ID'],
            $cleaned['toUser'],
            $cleaned['timeToFinish'],
            $cleaned['type'],
            $cleaned['priority'],
            $cleaned['period'],
            $cleaned['company'],
            $completed=0,
            $cleaned['repeat_in']
        );
    }
}

