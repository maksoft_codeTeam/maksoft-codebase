<?php
require_once __DIR__.'/../global/admin/CreatePageForm.php';
require_once __DIR__.'/../global/admin/AddPriceForm.php';
require_once __DIR__.'/../global/admin/EditTaskForm.php';
require_once __DIR__.'/../global/admin/CreateTaskForm.php';
require_once __DIR__.'/../global/admin/CreateTaskCommentForm.php';
require_once __DIR__.'/../global/admin/TaskCompleteForm.php';
require_once __DIR__.'/../global/admin/EditImageForm.php';
require_once __DIR__.'/../global/admin/SetImageAsSiteLogoForm.php';
require_once __DIR__.'/../global/admin/UploadImageForm.php';
require_once __DIR__.'/../global/admin/AddPageToGroupForm.php';
require_once __DIR__.'/../global/admin/DeletePageToGroupPage.php';
require_once __DIR__.'/../global/admin/DeleteGroupForm.php';
require_once __DIR__.'/../global/admin/DeleteGroupFromListForm.php';
require_once __DIR__.'/../global/admin/CreateGroupForm.php';
require_once __DIR__.'/../global/admin/TranslationCreateForm.php';
require_once __DIR__.'/../global/admin/DeleteTranslationForm.php';


class BlankPageView extends TemplateView {
    protected $template_name = 'blank.php';
}


class LoginView extends TemplateView 
{
    protected $template_name = 'login.php';
}


class DashView extends TemplateView 
{
    public $template_name = 'dashboard.php';

    function get_context_data()
    {
        $stats = $this->services['gate']->stats();
        $tasks = $this->services['gate']->task()->getTasks($this->o_page->_user['ID']);
        $inquiries = $this->services['gate']->site()->getSiteRequestsList($this->o_page->_site['SitesID']);
        $taskGate = $this->services['gate']->task();
        $user_id = $this->o_page->_user['ID'];
        $this
            ->add('statsGate', $stats)
            ->add('SiteID', $this->o_page->_site['SitesID'])
            ->add('lastEditedPages', $stats->getLastEditedPages($this->get('SiteID')))
            ->add('activePagesCount', $stats->getCountActivePages($this->get('SiteID')))
            ->add('registeredUsersCount', $stats->getRegisteredUsersCount($this->get('SiteID')))
            ->add('latestQueries', call_user_func_array(array($this->services['gate']->site(), 'getSiteRequestsListForMultipleSites'), array($this->get('SiteID'))))
            ->add('inquiries', array_slice($inquiries, 0,10))
            ->add('countNewComments', function($task) use($taskGate, $user_id) {
                return $taskGate->countNewTaskComments($task->taskID, $user_id);
            })
            ->add('isMaksoftEmployee', $this->services['gate']->user()->getEmployeePermission($user_id))
            ->add('expired', function($task) {
                return strtotime($task->end_date) < time();
            })
            #->add('getMostVisitedPages', $stats->getMostVisitedPages($this->services['o_page']->_site['SitesID']))
            ->add('tasks', array_slice($tasks, 0, 10));
        return $this->context;
    }
}


class CreateOrEditPageView extends TemplateView
{
    protected $template_name = 'edit_create.php';

    public function as_view($page_id=null)
    {
        return editOrCreatePage($this->request, $this->o_page, $this->services, $this->user, $page_id);
    }
}


class SiteMapView extends TemplateView
{
    protected $template_name = 'sitemap.php';
    
    public function get_context_data()
    {
        $this
            ->add('tmp_pages', $this->services['gate']->page()->getSiteMapPages($this->o_page->_site['SitesID']))
            ->add('tree', new Tree($this->get('tmp_pages')));
        return $this->context;
    }
} 


class TaskView extends TemplateView
{
    protected $template_name = 'task_list.php';
    
    public function get_context_data()
    {
        $filterOnlyActive = function($task) {
            return strtotime($task->end_date) >= time();
        };

        $filterElpasedTasks = function($task) {
            return strtotime($task->end_date) < time();
        };

        $tasks = $this->services['gate']->task()->getTasks($this->o_page->_user['ID']);
        $activeTasks = array_filter($tasks, $filterOnlyActive);
        $expiredTasks = array_filter($tasks, $filterElpasedTasks);

        $this
            ->add('o_page', $this->o_page)
            ->add('activeTasks' , $activeTasks)
            ->add('expiredTasks', $expiredTasks)
            ->add('repeatedTasks' , $this->services['gate']->task()->getUserRepeatedTasks($this->get('o_page')->_user['ID']))
            ->add('assignedTasks' , $this->services['gate']->task()->getAssignedTasks($this->get('o_page')->_user['ID']))
            ->add('upcomingTasks' , $this->services['gate']->task()->getUpcomingTasks($this->get('o_page')->_user['ID']));
        return $this->context;
    }
}


class TaskDetailView extends TemplateView
{
    protected $template_name = 'task_detail.php';

    public function as_view($task_id)
    {
        $user_id = $this->o_page->_user['ID'];
        $task = $this->services['gate']->task()->getTask($task_id, $user_id);
        $comments_from_user = array_diff(array($task->from_user, $task->to_user), array($user_id));
        $this->services['gate']->task()->setCommentStateToReaded($task_id, implode($comments_from_user));
        $taskEndDate= new DateTime($task->end_date);
        $today = new DateTime();
        $dayDifference = $taskEndDate->diff($today)->format("%a");
        $editTaskForm = new EditTaskForm($this->services, $task, $_POST);
        if($editTaskForm->is_valid()) {
            $editTaskForm->save();
        }
        $addCommentForm = new CreateTaskCommentForm($this->services, $task, $_POST);
        if($addCommentForm->is_valid()) {
            $addCommentForm->save();
            $task = $this->services['gate']->task()->getTask($task_id, $this->o_page->_user['ID']);
        }
        $completeTaskForm = new TaskCompleteForm($this->services, $task);
        if($completeTaskForm->is_valid()) {
            $completeTaskForm->save();
            $response = new Symfony\Component\HttpFoundation\RedirectResponse($this->route->generate('tasks'));
            $response->send();
        }
        $taskComments = $this->services['gate']->task()->getTaskComments($task->taskID);
        require_once __DIR__.'/pages/'.$this->template_name;
    }
}


class GroupView extends TemplateView
{
    protected $template_name = 'groups.php';

    public function get_context_data() 
    {
        #$pages = $services['gate']->page()->getPagesByGroupId($group_id, $o_page->_site['SitesID']);
        $this
            ->add('tmpGroups', $this->services['gate']->site()->getSiteGroups($this->o_page->_site['SitesID']))
            ->add('tree', new Tree($this->get('tmpGroups')))  
            ->add('form', new CreateGroupForm($this->services, null, $_POST)); 

        if($this->get('form')->is_valid()) {
            return $this->get('form')->save();
        }

        return parent::get_context_data();
    }

}


class GroupDetailView extends TemplateView 
{
    public function as_view($group_id) {
        $groupDetail = $this->services['gate']->page()->get_pGroupInfo($group_id);
        $form = new AddPageToGroupForm($this->services, $groupDetail, $_POST);
        $deleteForm = new DeletePageToGroupPage($this->services, $groupDetail, $_POST);
        $deleteGroupForm = new DeleteGroupForm($this->services, $groupDetail, $_POST);
        $deleteGroupFromListing = new DeleteGroupFromListForm($this->services, $_POST);

        if($form->is_valid()) { $form->save(); }
        if($deleteGroupFromListing->is_valid()) { $deleteGroupFromListing->save(); }
        if($deleteForm->is_valid()) { $deleteForm->save(); }
        if($deleteGroupForm->is_valid()) {
            $deleteGroupForm->save();
            $response = new Symfony\Component\HttpFoundation\RedirectResponse($this->route->generate('groups'));
            $response->send();
        }
        $createGroup = new CreateGroupForm($this->services, $groupDetail, $_POST);
        if($createGroup->is_valid()) {
            $createGroup->save();
        }
        $childGroups = $this->services['gate']->site()->getGroupsByParentGroupId($group_id);
        $groupPages  = $this->services['gate']->site()->getGroupPages($group_id);
        require_once __DIR__.'/pages/group_detail.php';
    }
}


class InquiriesListView extends TemplateView
{
    protected $template_name = 'site_inquiries.php';
    public function get_context_data() 
    {
        $this
            ->add('SiteID', $o_page->_site['SitesID'])
            ->add('inquiries', $this->services['gate']->site()->getSiteRequestsList($this->o_page->_site['SitesID']))
            ->add('uSites', array_map(function($site) {
                                    return (integer) $site['SiteID'];
                              }, $this->o_page->get_uSites($this->user->ID, "sa.notifications>0")))
            ->add('otherSitesInquiries', call_user_func_array(array($this->services['gate']->site(), 'getSiteRequestsListForMultipleSites'), $this->get('uSites')));
        return parent::get_context_data();
    }

    public function as_view()
    {
        set_orders(page::$uID, $this->o_page->_site, $this->o_page->_user);
        return parent::as_view();
    }
}



class SiteSettingsView extends TemplateView
{
    protected $template_name = 'site_settings.php';
    public function get_context_data() {
        $SiteID = $this->o_page->_site['SitesID'];
        $tmpl_id = $this->o_page->_site['Template'];
        $template_query = $this->o_page->db_query("SELECT * FROM Templates t left join CSSs c on t.default_css = c.cssID WHERE t.ID = '$tmpl_id' ");
        $this
            ->add('Template', $this->o_page->fetch("object", $template_query))
            ->add('o_page', $this->o_page)
            ->add('SiteID', $SiteID)
            ->add('Site', (object) $this->o_page->_site);

        return parent::get_context_data();
    }
}


class TrashbinView extends BlankPageView 
{
}


class UserProfileView extends TemplateView
{
    protected $template_name = 'profile.php';
    
    public function get_context_data()
    {
        $default_image="https://lh3.googleusercontent.com/-H7_QuNyFVMM/AAAAAAAAAAI/AAAAAAAAAAA/1095ekL4OEA/photo.jpg";
        $this->add('gate', $this->services['gate']);
        $avatar = $this->services['gate']->user()->getAvatar($this->o_page->_user["ID"]);
        if($avatar){
            $default_image=$avatar->picture;
        }
        $this->add('default_image', $default_image);

        if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["edit_profile"])){
            $profile = new \Maksoft\Admin\Profile\Edit($this->services['gate'], $_POST);
            $profile->setName("profile");
            try {
                $profile->is_valid();
                $profile->update($this->o_page);
                echo '<div class="alert alert-success" role="alert">������� ���������� ������ ������</div>';
            } catch (Exception $e) {
            }
        } else {
            $profile = new \Maksoft\Admin\Profile\Edit($this->services['gate']);
            $profile->setName("profile");
            $profile->setID("profile");
            $profile->load($this->o_page);
        }
        if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['bulstat'])){

            try {
                $company = new \Maksoft\Admin\Profile\Company($this->services['gate'], $this->o_page, $_POST);
                $company->is_valid();
                $company->update();
                echo '<a href="#" class="btn btn-success toastr-notify" data-progressBar="true" data-position="top-right" data-notifyType="success" data-message="������� ���������� ������� �� ������ �����!">Show Success</a>
';
            } catch (Exception $e) {
            }
        } else {
            $company = new \Maksoft\Admin\Profile\Company($this->services['gate'], $this->o_page);
        }
        if($this->o_page->_user["FirmID"]){
            $company->load($this->o_page->_user["FirmID"]);
        }

        if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['new_password'])){
            $password = new \Maksoft\Admin\Profile\Password($this->services['gate'], $_POST);
            $password->setName("password");
            try {
                $password->is_valid();
                $password->update($this->o_page);
                echo '<a href="#" class="btn btn-success toastr-notify" data-progressBar="true" data-position="top-right" data-notifyType="success" data-message="������� ���������� ������ ������!">Show Success</a>';
            } catch (Exception $e) {
            }
        } else {
            $password = new \Maksoft\Admin\Profile\Password($this->services['gate']);
            $password->setName("password");
            $password->load($this->o_page);
        }

        $this
            ->add('profile', $profile)
            ->add('password', $password)
            ->add('company', $company);
        return parent::get_context_data();
    }
}


class FileManagerView extends TemplateView  {
    public $template_name = 'filemanager.php';

    public function get_context_data()
    {
        $this->add('user', $this->user)
            ->add('o_page', $this->services['o_page']);
        $this->add('imageDirs', $this->services['gate']->site()->getSiteImageDirs($this->services['o_page']->_site['SitesID']));
        return parent::get_context_data();
    }
}

class ImageDirView extends TemplateView
{
    public function as_view($id, $order, $step=0, $name='') 
    {
        $urlData = array('id' => $id);
        if(!$step) {
            $step = 1;
        }

        if($step > 0 ) {
            $urlData['step'] = $step;
        }


        $imagesPerPage = 100;
        $max = $step * $imagesPerPage - $imagesPerPage; 
        $max = $max > 0 ? $max : 0;
        $directory = $this->services['gate']->site()->getSiteImageDir($id, $order);
        $form = new UploadImageForm($this->services, $directory, $_POST, $_FILES);
        if($form->is_valid()) {
            $form->save();
        }

        $urlData['order'] = $order;
        if($name) {
            $name = iconv('utf8', 'cp1251', urldecode($name));
            $urlData['name'] = $name;
            $files = $this->services['gate']->site()->getSiteImagesFilteredByName($id, $name, $order);
            $totalFilesCount = count($files);
        } else {
            $files = $this->services['gate']->site()->getSiteImages($id, $max, $imagesPerPage);
            $totalFilesCount = $this->services['gate']->site()->countDirectoryImages($id);
        }

        $files = array_slice($files, $max, $imagesPerPage);
        return require_once __DIR__ .'/pages/directory_list.php';
    }
}

class DeletePageView extends BlankPageView  {
}

class CreateTaskView extends TemplateView  
{ 
    public $template_name = 'task_create.php';

    public function get_context_data()
    {
        $createTaskForm = new CreateTaskForm($this->services, $_POST);
        $this->add('createTaskForm', $createTaskForm);
        if($createTaskForm->is_valid()) {
            $createTaskForm->save();
        }
        return parent::get_context_data();
    }
}


class ImageDetailView extends TemplateView 
{
    public function as_view($dirId, $imageId) 
    {
        $dir = $this->services['gate']->site()->getSiteImageDir($dirId);
        $image = $this->services['gate']->site()->getImage($imageId);
        $imageDirs = $this->services['gate']->site()->getSiteImageDirs($this->services['o_page']->_site['SitesID']);
        $form = new EditImageForm($this->services, $image, $imageDirs, $_POST);
        $setAsLogoForm = new SetImageAsSiteLogoForm($this->services, $image, $_POST);
        if($form->is_valid()) {
            $form->save();
            $image = $this->services['gate']->site()->getImage($imageId);
        }
        if($setAsLogoForm->is_valid()) {
            $setAsLogoForm->save();
        }

        $pages = $this->services['gate']->site()->getPagesByImage($imageId, $this->services['o_page']->_site['SitesID']);

        return require_once __DIR__ .'/pages/image_detail.php';
    }
}


class TranslationListView extends TemplateView
{
    protected $template_name = 'translations_list.php';
    public function get_context_data() 
    {
        $SiteID = $this->services['o_page']->_site['SitesID'];
        $form = new TranslationCreateForm($this->services, $this->request->request->all());
        $deleteForm = new DeleteTranslationForm($this->services, $this->request->request->all());
        $response = new Symfony\Component\HttpFoundation\RedirectResponse($this->route->generate('translations'));
        if($form->is_valid()) {
            $form->save();
            $response->send();
        }
        if($deleteForm->is_valid()) {
            $deleteForm->save();
            $response->send();
        }

        $translations = $this->services['gate']->lang()->getAllTranslations($SiteID);
        $this->add('translations', $translations);
        $this->add('form', $form);
        $this->add('deleteForm', $deleteForm);
        return parent::get_context_data();
    }
}
class PagesListView extends TemplateView
{
	protected $template_name = 'pageslist.php';
    public function get_context_data() 
    {
        $this
            ->add('tmp_pages', $this->services['gate']->page()->getSiteMapPages($this->o_page->_site['SitesID']))
            ->add('tree', new Tree($this->get('tmp_pages')));
        $this->add('allNodes', $this->get('tree')->getNodes());
        return $this->context;
    }
}
