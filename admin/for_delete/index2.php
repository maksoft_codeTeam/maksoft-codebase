<?php
error_reporting(E_ALL);
ini_set("display_errors", 1); 
$settings = realpath(__DIR__."/../settings.php");
require_once __DIR__. '/../lib/lib_functions.php';
require_once __DIR__. '/../lib/UrlFactory.php';
require_once __DIR__.'/../modules/vendor/autoload.php';
require_once __DIR__.'/Tree.php';
require_once __DIR__.'/../modules/vendor/altorouter/altorouter/AltoRouter.php';

require_once __DIR__ .'/vendor/autoload.php';
require_once __DIR__ .'/helpers.php';
require_once($settings);

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

$services = new Pimple\Container();
$makContainer = new Maksoft\Containers\GlobalContainer();
$response     = new Response();
$router       = new AltoRouter();
$request      = Request::createFromGlobals();

$services->register($makContainer);

$o_page       = $services['o_page'];

$custom_js = array();

require_once __DIR__.'/routes2.php';

// Файлов менидиджър
// запитвания, карта, групиране и статистика, месец за месец кои страници са 
// най посещавани, най-малко посещавани
// match current request url
//
$match = $router->match();

// call closure or throw 404 status
$view_name = sprintf('Maksoft\Administration\Views\%s', $match['target']);
if($match and class_exists($view_name)) {
    ob_start();
    $cls = new $view_name($request, $services, $o_page, $user, $router);
        call_user_func_array(array($cls, 'as_view'), $match['params']);
        $content = ob_get_contents();
    ob_end_clean();
    require_once __DIR__.'/templates/base.php';
} else {
    header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
}
