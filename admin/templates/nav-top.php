<!-- HEADER -->
<header id="header">

    <!-- Mobile Button -->
    <button id="mobileMenuBtn"></button>

    <!-- Logo -->
    <span class="logo pull-left">
        <img src="/admin/assets/images/admin_maksoft_beta.png" alt="admin panel" height="35" />
    </span>

    <form method="get" action="page-search.html" class="search pull-left hidden-xs">
        <input type="text" class="form-control" name="k" placeholder="Search for something..." />
    </form>

    <nav>

        <!-- OPTIONS LIST -->
        <ul class="nav pull-right">

            <!-- USER OPTIONS -->
            <li class="dropdown pull-left">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img class="user-avatar" alt="" src="/<?=$o_page->_user['picture'] ? $o_page->_user['picture'] : 'admin/assets/images/noavatar.jpg'?>" height="34" /> 
                    <span class="user-name">
                    <span class="hidden-xs"> <?=$o_page->_user['Name']?> <i class="fa fa-angle-down"></i> </span>
                    </span>
                </a>
                <ul class="dropdown-menu hold-on-click">
                    <li><!-- settings -->
                    <a href="<?=$router->generate("profile")?>"><i class="fa fa-cogs"></i> ������</a>
                    </li>

                    <li class="divider"></li>
                    <li><!-- logout -->
                        <a href="/web/admin/logout.php"><i class="fa fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
            <!-- /USER OPTIONS -->

        </ul>
        <!-- /OPTIONS LIST -->

    </nav>

</header>
<!-- /HEADER -->

