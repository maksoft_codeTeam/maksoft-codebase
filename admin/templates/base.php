<!doctype html>
<html lang="bg-BG">
    <head>
        <meta charset="cp1251" />
        <meta http-equiv="Content-type" content="text/html; charset=cp1251" />
        <title>Admin panel</title>

        <!-- mobile settings -->
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

        <!-- WEB FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />

        <!-- CORE CSS -->
        <link href="/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        
        <!-- THEME CSS -->
        <link href="/admin/assets/css/essentials.css" rel="stylesheet" type="text/css" />
        <link href="/admin/assets/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="/admin/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
        <link href="/admin/assets/css/layout-nestable.css" rel="stylesheet" type="text/css">

        <link async rel='stylesheet' href='//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css'>
        <link async rel="stylesheet" href='/global/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'>
        <link async rel="stylesheet" href='/global/bootstrap-datetimepicker/datetimepicker.css'>
        <link async rel="stylesheet" href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>

        <!-- PAGE LEVEL STYLES -->
        <?=$custom_css ? $custom_css : ''?>

    </head>
    <!-- .boxed = boxed version -->
    <body>
        <div id="wrapper" class="clearfix">
            <?php if($match['name'] != 'login') {
                require_once __DIR__.'/../templates/left-sidebar.php';
                require_once __DIR__.'/../templates/nav-top.php';
            }?>

            <?=$content ? $content : ''?>
        </div>

        <!-- JAVASCRIPT FILES -->
        <script type="text/javascript">var plugin_path = '/admin/assets/plugins/';</script>
        <script type="text/javascript" src="/admin/assets/plugins/jquery/jquery-2.2.3.min.js"></script>
        <script type="text/javascript" src="/admin/assets/js/app.js"></script>
        <script type="text/javascript" src="/lib/jquery/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="/lib/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <script type='text/javascript' src='/global/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'></script>
        <script type='text/javascript' src='/global/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.bg.js'></script>
        <script type='text/javascript' src='//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js'></script>
<!--        <script type='text/javascript' src='//cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js'></script>-->
        <script>
        jQuery(function() {
            jQuery(".hasDatepicker").datetimepicker({
                format: 'yyyy-mm-dd hh:ii'
            });
            jQuery('.dataTable').DataTable( {
                "language": {
                    "sProcessing":   "��������� �� �����������...",
                    "sLengthMenu":   "��������� �� _MENU_ ���������",
                    "sZeroRecords":  "���� �������� ���������",
                    "sInfo":         "��������� �� ��������� �� _START_ �� _END_ �� ���� _TOTAL_",
                    "sInfoEmpty":    "��������� �� ��������� �� 0 �� 0 �� ���� 0",
                    "sInfoFiltered": "(���������� �� ���� _MAX_ ���������)",
                    "sInfoPostFix":  "",
                    "sSearch":       "������� ��� ������ ������:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "�����",
                        "sPrevious": "��������",
                        "sNext":     "��������",
                        "sLast":     "��������"
                }
            },
                stateSave: true
            });
        });
        </script>
        <?=$custom_js ? implode(PHP_EOL, $custom_js) : ''?>
    </body>
</html>
