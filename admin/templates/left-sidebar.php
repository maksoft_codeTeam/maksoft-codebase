<!-- 
    ASIDE 
    Keep it outside of #wrapper (responsive purpose)
-->
<aside id="aside">
    <!--
        Always open:
        <li class="active alays-open">

        LABELS:
            <span class="label label-danger pull-right">1</span>
            <span class="label label-default pull-right">1</span>
            <span class="label label-warning pull-right">1</span>
            <span class="label label-success pull-right">1</span>
            <span class="label label-info pull-right">1</span>
    -->
    <nav id="sideNav"><!-- MAIN MENU -->
        <ul class="nav nav-list">
            <li class="active"><!-- dashboard -->
                <a class="dashboard" href="/admin/dash/"><!-- warning - url used by default by ajax (if eneabled) -->
                    <i class="main-icon fa fa-dashboard"></i> <span>�����</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-files-o"></i> <span>��������</span>
                </a>
                <ul><!-- submenus -->
				<li><a href="<?=$router->generate("pageslist")?>">������</a></li>
                <li><a href="<?=$router->generate("newPage")?>">���� ��������</a></li>
                    <li><a href="<?=$router->generate("trashbin")?>">�����</a></li>
                </ul>
            </li>
            <li>
            <a href="<?=$router->generate('fileManager');?>">
                    <i class="main-icon fa fa fa-folder-open-o"></i> <span>�������</span>
                </a>
            </li>
            <li>
                <a href="<?=$router->generate("sitemap")?>">
                    <i class="main-icon fa fa-sitemap"></i> <span>����� �� �����</span>
                </a>
            </li>
            <li>
            <a href="<?=$router->generate("groups")?>">
                    <i class="main-icon fa fa-object-group"></i> <span>���������</span>
                </a>
            </li>
            <li>
                <a href="<?=$router->generate("siteRequests")?>">
                    <i class="main-icon fa fa-question-circle"></i> <span>����������</span>
                </a>
            </li>
            <li>
                <a href="<?=$router->generate('translations')?>">
                <i class="main-icon et-book-open"></i> <span>�������</span>
                </a>
            </li>
<!--            <li>
                <a href="<?=$router->generate('help')?>">
                    <i class="main-icon fa fa-info"></i> <span>�����</span>
                </a>
            </li>-->
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-cogs"></i> <span>������</span>
                </a>
                <ul><!-- submenus -->
                    <li><a href="<?=$router->generate("banner")?>">������</a></li>
                    <li><a href="<?=$router->generate("cart")?>">�������</a></li>
                    <li><a href="<?=$router->generate("poll")?>">������</a></li>
                    <li><a href="<?=$router->generate("comments")?>">���������</a></li>
                    <li><a href="<?=$router->generate("ticksys")?>">������</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-cogs"></i> <span>�������</span>
                </a>
                <ul><!-- submenus -->
                    <li><a href="#">�������� ����</a></li>
                    <li><a href="<?=$router->generate("siteSettings")?>">���������</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="main-icon fa fa-globe"></i> <span>�������</span>
                </a>
            </li>
        </ul>
        
<?php if($o_page->_user['perm']>  1): ?>
        <h3>Maksoft ��������������</h3>
        <ul class="nav nav-list">
            <li>
                <a href="<?=$router->generate('tasks')?>">
                <i class="main-icon fa fa-users"></i> <span>������</span>
                </a>
            </li>
        </ul>
<?php endif; ?>
<h3>���������� ����������</h3>
<ul class="nav nav-list">
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-cogs"></i> <span>������</span>
                </a>
        <?php if(count($custom_pages)): ?>
                <ul> 
            <?php foreach($custom_pages as $page) : ?>
                <li>
                    <a href="<?=$router->generate('customPage', array('id' => $page->n))?>">
                    <i class="main-icon fa fa-users"></i> <span><?=$page->Name?></span>
                    </a>
                </li>
            <?php endforeach; ?>

                </ul>
        <?php endif; ?>
            </li>
        </ul>

    </nav>

    <span id="asidebg"><!-- aside fixed background --></span>
</aside>
<!-- /ASIDE -->
