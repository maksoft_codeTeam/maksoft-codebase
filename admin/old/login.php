<?php
	include "../lib/configure.php";
	include "../web/admin/db/logOn.inc.php";
	
	$url = $_SERVER['SERVER_NAME']; 
	//echo $url;
	$site_url_q = mysqli_query("SELECT * FROM Sites WHERE `url` LIKE '%$url%' "); 
	if (mysqli_num_rows($site_url_q)>0)
		{
			if(mysqli_num_rows($site_url_q)>1)
				{
					$field_SiteID = "<select name=\"SiteID\">";

					while($Site = mysqli_fetch_array($site_url_q))
						{
							//$Site_array[] = $row;
							$field_SiteID.="<option value=\"".$Site['SitesID']."\">".$Site['Name']."</option>";
						
						}
					$field_SiteID.="</select>";
				}
			else
				{
					$Site = mysqli_fetch_array($site_url_q); 
					$SiteID = $Site['SitesID']; 
					$n=$Site['StartPage']; 
					$field_SiteID = "<input type=\"hidden\" name=\"SiteID\" value=\"".$SiteID."\">";
				}
		} 
?>
<!DOCTYPE html>
<!--[if IEMobile 7]><html class="no-js iem7 oldie linen"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie linen" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie linen" lang="en"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class="no-js ie9 linen" lang="en"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!--><html class="no-js linen" lang="en"><!--<![endif]-->
<head>
	<meta charset="windows-1251">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="user-scalable=0, initial-scale=1.0, target-densitydpi=115">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/colors.css">
	<link rel="stylesheet" href="assets/css/styles/form.css">
	<link rel="stylesheet" href="assets/css/styles/switches.css">
	<link rel="stylesheet" media="screen" href="assets/css/login.css">
	<link rel="stylesheet" type="text/css" href="assets/css/keyboard.css">
	<script src="assets/js/libs/modernizr.custom.js"></script>
	<script type="text/javascript" src="assets/js/keyboard.js" charset="UTF-8"></script>
	<link rel="shortcut icon" href="assets/img/fav.ico">
    
<!--    	<script src='../www.google.com/recaptcha/api3a7e.js?hl=bg'></script>-->
</head>
<body>
	<div id="container">
		<hgroup id="login-title" class="large-margin-bottom">
			<h1 class="login-title-image">Maksoft.net</h1>
		</hgroup>
		<form method="post" action="#" id="form-login">
			<ul class="inputs black-input large">
				<li><span class="icon-user mid-margin-right"></span><input type="text" name="login" id="login" value="" class="input-unstyled" placeholder="����������" autocomplete="off"></li>
				<li><span class="icon-key mid-margin-right"></span><input type="password" name="pass" id="pass" class="input-unstyled keyboardInput" placeholder="������" autocomplete="off" data-tooltip-options='{"position":"center"}'>
				<a class="button icon-keyboard keyboardInputInitiator" style="float:right;top:10px; left:6px;" id="keyboardInputInitiator" href="javascript:void(0)"></a></li>
				<li>
				<div class="g-recaptcha" data-theme="dark" data-sitekey="6LfB3BITAAAAAC4i8rUjWY2ogZ3xLKUQctfLtHdU" style="padding-top:35px;transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;"></div></li>
			</ul>
			<button type="submit" class="button glossy full-width huge">����</button>			
		</form>
			<?php
				if($url != "maksoft.net" && $url != "www.maksoft.net")
					echo $field_SiteID;
			?>



<?php

?>
<?php

?>
<?php

?> 
	</div>
	<script src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script src="assets/js/setup.js"></script>
	<script src="assets/js/input.js"></script>
	<script src="assets/js/message.js"></script>
	<script src="assets/js/notify.js"></script>
	<script src="assets/js/tooltip.js"></script>
	<script>

	/*
		else if (scode.length === 0)
				{
					displayError('���� �������� ��� �� ���������');
					return false;
				}
		*/

		$(document).ready(function()
		{
			var doc = $('html').addClass('js-login'),
				container = $('#container'),
				formLogin = $('#form-login'),
				centered;

			formLogin.submit(function(event)
			{
				var login = $.trim($('#login').val()),
					pass = $.trim($('#pass').val()),
					scode = $.trim($('#g-recaptcha-response').val());
				var url = 'index-2.html';

				if (login.length === 0)
				{
					displayError('���� �������� ������������� ���');
					return false;
				}
				else if (pass.length === 0)
				{
					formLogin.clearMessages('���� �������� ������������� ���');
					displayError('���� �������� ������');
					return false;
				}
				
				else
				{
					formLogin.clearMessages();
					displayLoading('��������...');

					event.preventDefault();

					  $.ajax(url, {
					  		data: {
								action: 'login',
					  			login:	login,
					  			pass:	pass,
								scode:	scode
					  		},
							dataType:'JSON', 
							type: 'POST',
					  		success: function(data)
					  		{
					  			if (data.logged)
					  			{
									formLogin.clearMessages();
									displayLoading('������������� ...');
					  				document.location.href = 'index-2.html';
					  			}
					  			else
					  			{
					  				formLogin.clearMessages();
					  				displayError(data.errormsg);
									document.getElementById('captcha').src = 'assets/securimage/securimage_showd41d.png?' + Math.random();
					  			}
					  		},
					  		error: function()
					  		{
					  			formLogin.clearMessages();
					  			displayError('������ ��� ����������� ��� �������');
					  		}
					  });
				}
			});


			function handleLoginResize()
			{
				centered = (container.css('position') === 'absolute');

				if (!centered)
				{
					container.css('margin-top', '');
				}
				else
				{
					if (parseInt(container.css('margin-top'), 10) === 0)
					{
						centerForm(false);
					}
				}
			};

			$(window).on('normalized-resize', handleLoginResize);
			handleLoginResize();

			function centerForm(animate, ignore)
			{
				if (centered)
				{
					var siblings = formLogin.siblings(),
						finalSize = formLogin.outerHeight();

					if (ignore)
					{
						siblings = siblings.not(ignore);
					}

					siblings.each(function(i)
					{
						finalSize += $(this).outerHeight(true);
					});

					container[animate ? 'animate' : 'css']({ marginTop: -Math.round(finalSize/2)+'px' });
				}
			};

			centerForm(false);

			function displayError(message)
			{
				var message = formLogin.message(message, {
					append: false,
					arrow: 'bottom',
					classes: ['red-gradient'],
					animate: false					
				});

				centerForm(true, 'fast');

				message.on('endfade', function(event)
				{
					centerForm(true, message.get(0));

				}).hide().slideDown('fast');
			}

			function displayLoading(message)
			{
				var message = formLogin.message('<strong>'+message+'</strong>', {
					append: false,
					arrow: 'bottom',
					classes: ['blue-gradient', 'align-center'],
					stripes: true,
					darkStripes: false,
					closable: false,
					animate: false					
				});
				centerForm(true, 'fast');
				message.on('endfade', function(event)
				{
					centerForm(true, message.get(0));
				}).hide().slideDown('fast');
			}
		});
		function html5_audio(){
				var a = document.createElement('audio');
				return !!(a.canPlayType && a.canPlayType('audio/wav;').replace(/no/, ''));
			}
			function isIE() { 
				return ((navigator.appName == 'Microsoft Internet Explorer') || ((navigator.appName == 'Netscape') && (new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent) != null))); 
			}
			 
			var play_html5_audio = false;
			if(html5_audio()) {play_html5_audio = true;}
			if(isIE()) {play_html5_audio = false;}
			if ($.template.android) {play_html5_audio = true;}

			function play_sound(url){
				if(play_html5_audio){
					var snd = new Audio(url);
					snd.load();
					snd.play();
				}else{
					$("#sound").remove();
					var sound = $("<embed id='sound' type='audio/wav' />");
					sound.attr('src', url);
					sound.attr('loop', false);
					sound.attr('hidden', true);
					sound.attr('autostart', true);
					$('body').append(sound);
				}
			}	
			function playCaptcha()
			{
				play_sound('admin/securimage/securimage_audio-3404a503f4a2becfbe8db40cf0a0dc37d41d.wav?' + Math.random());
			}
	</script>
</body>
</html>