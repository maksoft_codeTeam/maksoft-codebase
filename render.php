<?php
error_reporting(E_ALL | E_STRICT); 
ini_set('memory_limit', '512M'); 
ini_set('display_errors', 1); 
ini_set('max_execution_time', 900); //300 seconds = 5 minutes

//page object classes
require_once('lib/Database.class.php');
include_once('prod/settings.php');



function ob_file_callback($buffer)
{
global $ob_file;
fwrite($ob_file,$buffer);
}
$db = new Database();
$stmt = $db->prepare(PAGE_SELECT_QUERY);
$stmt->bindValue(":SiteID", 999);
$stmt->bindValue(":last_updated", '1980');
$stmt->execute();
#$o_site = new site();
#$o_page = new page(); 
#$SiteID = $o_site->SiteID;
$pages = array(19329617,19330216);
$site_pages = array();
while($_row = $stmt->fetch(PDO::FETCH_OBJ)){
    if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/prod/sites/999/'))
	mkdir($_SERVER['DOCUMENT_ROOT'].'/prod/sites/999/', 0755, true);
    $path = $_SERVER['DOCUMENT_ROOT'].'/prod/sites/999/'.$_row->n.'.html';
    $ob_file = fopen($path,'w');
    ob_start();
    include_once ('lib/lib_page.php');
    $_GET['n']=$_row->n;
    $a = global_include('page_render.php');
    $hmtl = ob_get_contents();
    ob_end_flush();
    echo $a;
}
// http://codeutopia.net/blog/2007/10/03/how-to-easily-redirect-php-output-to-a-file/
function global_include($script_path) {
    // check if the file to include exists:
    global $o_site, $o_page, $_GET;
    if (isset($script_path) && is_file($script_path)) {
        // extract variables from the global scope:
        extract($GLOBALS, EXTR_REFS);
        ob_start();
	include_once("lib/lib_page.php");
        return ob_get_clean();
    } else {
        ob_clean();
        trigger_error('The script to parse in the global scope was not found');
    }
}
