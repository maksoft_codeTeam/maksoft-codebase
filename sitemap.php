﻿<?php 
header("Content-type: text/xml");
//login functions
include_once("web/admin/db/logOn.inc.php");
//DB functions
include_once("web/admin/query_set.php");
//Input/Output functions
include_once("web/admin/io.php");
//page object classes
include_once("lib/lib_page.php");

class sitemap extends page {
    public function __construct()
    {
        parent::page();
    }
/*

	$url = $_SERVER['SERVER_NAME']; 
	//echo $url;
	$site_url_q = mysqli_query("SELECT * FROM Sites WHERE `url` LIKE '%$url%' "); 
			$Site = mysqli_fetch_object($site_url_q);
					//$SiteID = $Site['SitesID']; 
	$Site = $Site->SitesID;		
	
*/
		
function list_pages($Site, $url, $ppage, $priority) {
global $Site;
 if ($priority>0.2) {$priority=$priority-0.1; }
 $today = date("YYYY-MM-DD");
 $pages_q = mysqli_query("SELECT * FROM pages WHERE ParentPage='$ppage' AND SiteID='$Site->SitesID' AND SecLevel=0 AND LENGTH(Name) >3"); 
 while ($page = mysqli_fetch_object($pages_q)) {
 if ($page->ParentPage=="$Site->news") {$priority=0.9; }
 if ($page->toplink>0) {$priority=0.9; }
 if ($an>0) {$priority=$priority-0.1; }
	$str_link = iconv("CP1251", "UTF-8", htmlspecialchars($this->get_pLink($page->n, $page->SiteID)));   
 // PROBLEM s o_page->get_pLink
 //    	   <loc>http://$url/page.php?n=$page->n&amp;SiteID=$page->SiteID</loc>
 //$date = $page->date_modified; 
 $changefreq = "weekly"; 
 //$mem = round((memory_get_peak_usage()/1024)/1024,2); 
 echo("
<url>
	<loc>$str_link</loc>
	<lastmod>".date("Y-m-d",strtotime($page->date_modified))."</lastmod>
	<changefreq>$changefreq</changefreq>
	<priority>$priority</priority>
</url>");
 
// comments
$comments_list = $this->db_query("SELECT * FROM comments WHERE n='$page->$n' AND comment_subject<>'' ");
while($comm_row = mysqli_fetch_object()) {
}
 

// tags list
if($o_page->PageURL=='web/admin/pageURLs/other_links.php') {
 $tag_keys = split(",",iconv("CP1251","UTF-8",$page->tags)); 
 foreach($tag_keys as $tag_key) {
//$similar_pages_q = $this->db_query("SELECT * FROM pages WHERE SiteID='$Site->SitesID' AND Name LIKE '%$tag_key%' OR title LIKE '%$tag_key%' ");
if(strlen($tag_key)>12) {
$priority_tag = round($priority-0.1,1);
$str_link = $this->protocol.$this->_site['primary_url']."/tag/$tag_key"; 
echo("<url>
	<loc>$str_link</loc>
	<lastmod>".date("Y-m-d")."</lastmod>
	<changefreq>weekly</changefreq>
	<priority>$priority_tag</priority>
</url>"); 
} // there are similar pages, generates tag address
}// dopulnenie drugi vruzki

}

$this->list_pages($Site, $url, $page->n, $priority);
} // while
}

// tagovete sledva da se opisvat (tazi funkcia podleji na revisia i prepisvane
// zakomentirana na line 124
function list_tags() {
 $tag_keys = split(",",iconv("CP1251","UTF-8",$this->_site['Keywords'])); 
 foreach($tag_keys as $tag_key) {
$priority = "0.9"; 
if(strlen($tag_key)>10) $priority = "0.8"; 
//$tag_key = preg_replace('/^ ', '/^', $tag_key); 
$str_link = $this->scheme.$this->_site['primary_url']."/tag/$tag_key"; 
if(strlen($tag_key)>2) {
echo("<url>
	<loc>$str_link</loc>
	<lastmod>".date("Y-m-d")."</lastmod>
	<changefreq>weekly</changefreq>
	<priority>$priority</priority>
</url>"); 
} // tag_key is not null
}
}

} // end class sitemap

$o_sitemap = new sitemap();
//$o_page = new page(); 
$n = $o_sitemap->n; 
$SiteID = $o_sitemap->SiteID;

echo("<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">"); 
if ($SiteID>0) {
 $site_q = mysqli_query("SELECT * FROM Sites WHERE SitesID='$SiteID' "); 
 $Site = mysqli_fetch_object($site_q); 
 if(strlen($Site->primary_url)>3) {
 	$url=$Site->primary_url;  
 } else {
	 $urls = explode(" ", $Site->url); 
	 $url=$urls[0]; 
 }

// home page
echo("
<url>
	<loc>".$o_sitemap->scheme.$url."</loc>
	<lastmod>".date("Y-m-d")."</lastmod>
	<changefreq>weekly</changefreq>
	<priority>1</priority>
</url>"); 

 $priority = 0.9; 
 $o_sitemap->list_pages($Site, $url, $Site->StartPage, $priority); 
 //$o_sitemap->list_tags(); 
} // SiteID > 0
echo("</urlset> 
"); 
?>

