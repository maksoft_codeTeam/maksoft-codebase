<?php
function parseTextForEmail($text) {
	$email = array();
	$invalid_email = array();
 
	$text = ereg_replace("[^A-Za-z._0-9@ ]"," ",$text);
 
	$token = trim(strtok($text, " "));
 
	while($token !== "") {
 
		if(strpos($token, "@") !== false) {		
 
			$token = ereg_replace("[^A-Za-z._0-9@]","", $token);
 
			//checking to see if this is a valid email address
			if(is_valid_email($email) !== true) {
				$email[] = strtolower($token);
			}
			else {
				$invalid_email[] = strtolower($token);
			}
		}
 
		$token = trim(strtok(" "));
	}
 
	$email = array_unique($email);
	$invalid_email = array_unique($invalid_email);
 
	return array("valid_email"=>$email, "invalid_email" => $invalid_email);
 
}
 
function is_valid_email($email) {
	if (eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$",$email)) return true;
	else return false;
}
 
//search for emial input
$emails = parseTextForEmail($str); 
if(strlen($emails['valid_email'][0]) > 5)
	$ToMail =  $emails['valid_email'][0];
else $ToMail = $Site->EMail;

//on start page use $Site->EMail
if($o_page->n == $o_site->get_sStartPage())
	$ToMail = $Site->EMail;

define("MESSAGE_SEND_TO_FRIEND_SUBJECT", "����������� �� %s");
define("MESSAGE_SEND_TO_FRIEND", "�������. ����������� �� �� ������� ������� ����:  %s");
define("FORM_MESSAGE_VALID_EMAIL", "�� � ������� e-mail");
define("FORM_MESSAGE_ALL_REQUESTED_FIELD", "���� ��������� �������� ������������ ������:");
define("FORM_MESSAGE_REQUESTED_FIELD", " � ������������ ����");

function send_to_friend()
{
	global $email_from, $email_to, $comment, $view_url, $code, $codestr;
	$email_subject = sprintf(MESSAGE_SEND_TO_FRIEND_SUBJECT, $email_from);
	$email_message = "��: ".$email_from."\n\n\n";
	$email_message.= sprintf(MESSAGE_SEND_TO_FRIEND, $view_url) ."\n\n".$comment;
	
	//translate message to Latin output
	$email_message = CyrLat($email_message);
	
	$headers = 'From: '.$email_from."\n" .   'Reply-To: '.$email_from."\n" .  'X-Mailer: PHP/' . phpversion();
	
	if(check_code_str($code, $codestr))
		{
			mail($email_to, $email_subject, $email_message, $headers);
			echo "<div class=\"message_normal\"><a name=\"sent\">����������� � ��������� ������� !</a></div>";
		}
	else echo "<div class=\"message_error\"><a name=\"sent\">����������� �� � ���������! ���� �� ��������� �� ������� !</a></div>";
}

function send_request()
{
	global $EMail, $phone, $request, $name, $c_name, $Site, $ToMail, $code, $codestr;
	
	$email_subject = sprintf("��������� �� ����� ".$c_name." �� Toplotehnika.eu");
	$email_message= stripslashes(sprintf("��������� ".$c_name.", \n\n ����� ��������� �� ���������� �� Toplotehnika.eu"));
	$email_message.= "\n ���: ".$name;
	$email_message.= "\n e-mail: ".$EMail;
	$email_message.= "\n �������: ".$phone;
	$email_message.= "\n ���������: ".$request;
	$email_message.= "\n\n �� ����� �� www.toplotehnika.net";

	//translate message to Latin output
	$email_message = CyrLat($email_message);

	$headers = 'From: web@tolplotehnika.eu '."\n" .   'Reply-To: '.$EMail."\n" .  'X-Mailer: PHP/' . phpversion();

	if(check_code_str($code, $codestr))
		{
		  //send to company mail
		  mail($ToMail, $email_subject, $email_message, $headers);
		  //send to site owner email
		  
		  if($ToMail != $Site->EMail)
			  mail($Site->EMail, "����� �� ".$email_subject, "����� �� ��������� �� ����� � Toplotehnika.eu \n ===========================\n".$email_message, $headers);
		  
		  echo "<div class=\"message_normal\"><a name=\"request\">����������� � ��������� ������� !</a></div>";
		}
	else echo "<div class=\"message_error\"><a name=\"sent\">����������� �� � ���������! ���� �� ��������� �� ������� !</a></div>";
}

?>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.title; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' <?=FORM_MESSAGE_VALID_EMAIL?> \n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' <?=FORM_MESSAGE_MUST_CONTAIN_NUMBERS?>\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' <?=FORM_MESSAGE_MUST_CONTAIN_NUMBER_BETWEEN?> '+min+' - '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' <?=FORM_MESSAGE_REQUESTED_FIELD?>\n'; }
  } if (errors) alert('<?=FORM_MESSAGE_ALL_REQUESTED_FIELD?>\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>
<center>
<?php
 if($_POST['send_to_friend'] == 1) 
 	send_to_friend();
	
 if($_POST['send_request'] == 1) 
 	send_request();
?>
<table width="100%" class="border_table">
<tr><th>���� �� ������� �� ���� �����<th>������� ��������� �� ���� �����
<tr><td width="50%" align="center" valign="top">
    <form name="contact_form" method="post" action="/page.php?<?php echo("n=$n&SiteID=$SiteID"); ?>#sent">
    ����� e-mail: *<br />
    <input type="text" name="email_from" id="email_from" title="����� e-mail"><br />
    ������� �� e-mail: *<br />
    <input type="text" name="email_to" id="email_to" title="������� �� e-mail"><br />
    ��������:<br />
	<textarea name="comment" cols="15" rows="5" title="��������"></textarea><br />
 	<?php $code = rand(0, 65535); ?>
    <img src="http://www.maksoft.net/gen.php?code=<?=$code?>" width="140" align="middle" vspace=5 hspace=5><br />
	<input type="hidden" name="code" value="<?=$code?>">
	��� �� ���������: *<br>
	<input name="codestr" type="text" id="secure_code" title="��� �� ���������"> 
    
    <input name="view_url" type="hidden" value="http://www.toplotehnika.eu/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>">
	<input name="send_to_friend" type="hidden" value="1"><br /><br />
    <input type="button" onclick="MM_validateForm('email_from','','RisEmail', 'email_to','','R', 'secure_code','', 'R'); if(document.MM_returnValue) document.contact_form.submit()" value=" ������� ">
	</form>
<td width="50%" align="center" valign="top">
    <form name="request_form" method="post" action="/page.php?<?php echo("n=$n&SiteID=$SiteID"); ?>#request">
    ����� e-mail: *<br />
    <input type="text" name="EMail" id="EMail" title="����� e-mail"><br />
    ����� �������: *<br />
    <input type="text" name="phone" id="phone" title="����� �������"><br />
    ������ ���:<br />
	<input type="text" name="name" id="name" title="������ ���"><br />
    ���������:<br />
    <textarea name="request" cols="15" rows="5" title="���������"></textarea><br />
 	<?php $code = rand(0, 65535); ?>
    <img src="http://www.maksoft.net/gen.php?code=<?=$code?>" width="140" align="middle" vspace=5 hspace=5><br />
	<input type="hidden" name="code" value="<?=$code?>">
	��� �� ���������: *<br>
	<input name="codestr" type="text" id="secure_code2" title="��� �� ���������">     
    
	<input name="c_name" type="hidden" value="<?=$row->Name?>">
	<input name="send_request" type="hidden" value="1"><br /><br />
    <input type="button" onclick="MM_validateForm('EMail','','RisEmail', 'phone','','RNum', 'name','','R', 'secure_code2','', 'R'); if(document.MM_returnValue) document.request_form.submit()" value=" ������� ">
	</form>
<?php
if($user->ReadLevel >0)
	{
	?>
		<tr><td colspan="2">	
	<?
		mk_output_message("normal", "����������� �� ���� ������� �� �����: ".$ToMail);
	}
?>
</table>
<table width="100%" class="keywords">
<tr><th>������� ����
<tr><td>
	<?php $o_page->print_pTags("page.php?n=".$o_page->get_pParent()."&amp;SiteID=".$o_site->SiteID);?>
</table>
</center>