<div class="row">
      <div class="col-md-11">
		<?php
			if($sent == 1) mk_output_message('normal','
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>�����!</strong> ������ ������ � ��������� �������!
				</div>');
		?>
        <div class="well well-sm">
          <form role="form" data-toggle="validator" id="view_repair_form" class="form-horizontal" name="form1" method="post" action="/form-cms.php?<?php echo("n=$n&SiteID=$SiteID"); ?>">
          <fieldset>
            <legend class="text-center">����� �����</legend>
    
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-5 control-label" for="Name">������ ����� (��� �����)</label>
              <div class="col-md-7">
                
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-user"></i></div>
					<input id="Name" name="Name" type="text" placeholder="��� �����" class="form-control" value="<?=$user->Name;?>" data-error="�������� �����! ������ � ������." required>
					<div class="input-group-addon">*</div>
				</div>
					<div class="help-block with-errors"></div>
              </div>
            </div>
			
            <!-- phone input-->
            <div class="form-group">
              <label class="col-md-5 control-label" for="Phone">������ �������</label>
              <div class="col-md-7">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-phone"></i></div>
					<input id="Phone" name="Phone" type="text" placeholder="�������" class="form-control">
				</div>
              </div>
            </div>
    
            <!-- Email input-->
            <div class="form-group">
              <label class="col-md-5 control-label" for="EMail">E-mail</label>
              <div class="col-md-7">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-envelope"></i></div>
					<input id="EMail" name="EMail" type="email" placeholder="EMail" class="form-control" value="<?=$user->EMail;?>" data-error="�������� Email! ������ � ������." required>
					<div class="input-group-addon">*</div>
				</div>
					<div class="help-block with-errors"></div>
              </div>
            </div>
			
            <!-- city input-->
            <div class="form-group">
              <label class="col-md-5 control-label" for="city">����</label>
              <div class="col-md-7">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-building"></i></div>
					<input id="city" name="city" type="text" placeholder="����" class="form-control">
				</div>
			  </div>
            </div>
    
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-5 control-label" for="Zapitvane">�������� �� �������� �� ��� ������</label>
              <div class="col-md-7">
                <textarea class="form-control" id="Zapitvane" name="Zapitvane" placeholder="����, �������� ������ ��������� ���..." rows="5"></textarea>
              </div>
            </div>
			
			
            <!-- Message body -->
            <div class="form-group">
<!--              <label class="col-md-5 control-label" for="codestr">��� �� ���������</label>
              <div class="col-md-7">
                <?php
					$code = rand(0, 65535);   
					
					echo("
					<img src=\"http://www.maksoft.net/gen.php?code=$code\" align=\"middle\" vspace=5 hspace=5>
					<input type=\"hidden\" name=\"code\" value=\"$code\">
					"); 
				?>
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-hand-o-up"></i></div>
					<input name="codestr" type="text" id="codestr" size="10" class="form-control">
				</div>
              </div>-->
              <div class="col-md-7">
              <?php
 include("./web/forms/form_captcha.php");
?>
</div>
            </div>
			
			<input name="ConfText" type="hidden" id="ConfText" value="����������� �� � ����������!"> 
			<input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
			<input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
			<input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$Site->EMail"); ?>"> 
			<input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$Site->StartPage&SiteID=$SiteID&sent=1"); ?>">
			<input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
			<input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-7 text-right">
                <button type="submit" id="submitter" class="btn btn-default btn-lg">�������</button>
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
</div>