<?php
if(isset($_GET['first_city']) && isset($_GET['last_city'])){
    $first_city = $_GET['first_city'];
    $last_city = $_GET['last_city'];
}


$date 	  = (isset($date)     ? $date: date('d.m.Y', time()));
$days 	  = (isset($days)     ? $days: 1);
$prices   = (isset($prices)   ?	$prices: true);
$position = (isset($position) ? $position: 'top');
if(!isset($template)){
    $template = False;
}
echo $o_ticksys->print_to_destination($last_city, $date, $o_page->get_pLink());
echo $o_ticksys->print_two_way($first_city, $last_city, $date, $days, $template);
