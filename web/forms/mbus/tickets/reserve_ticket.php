<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script> -->
<?php
/********************************************************************
 *  @(#)UniversalPluginCheckoutDemo.php                             *
 *                                                                  *
 *  Copyright (c) 2000 - 2007 by ACI Worldwide Inc.                 *
 *  330 South 108th Avenue, Omaha, Nebraska, 68154, U.S.A.          *
 *  All rights reserved.                                            *
 *                                                                  *
 *  This software is the confidential and proprietary information   *
 *  of ACI Worldwide Inc ("Confidential Information").  You shall   *
 *  not disclose such Confidential Information and shall use it     *
 *  only in accordance with the terms of the license agreement      *
 *  you entered with ACI Worldwide Inc.                             *
 ********************************************************************/

 //
 // The purpose of the UniversalPluginCheckoutDemo.php is to
 // simulate a typical online shopping card page that the
 // merchant might use for allowing consumers to enter checkout and
 // subsequently enter the Commerce Gateway.
 //
	
	require_once "./pay/UBB/Universal/UniversalPlugin.php";
	require_once "./pay/UBB/Universal/UniversalPluginXMLFileParser.php";
	require_once "./pay/UBB/Universal/Framework.php";
	
	$destination = $_REQUEST['destination'];
	if (!strcmp($destination, "Merchant")) {
	    $kickoffURL = "http://www.mbus-bg.com/pay/UBB/Universal/UniversalPluginCheckoutMerchantPaymentPage.php";
	} else {
	    $kickoffURL = "http://www.mbus-bg.com/pay/UBB/Universal/UniversalPluginCheckoutPaymentInit.php";
	}
	
	/* $info = $o_ticksys->ticksys_url."GET_SCHEDULES&PASS=".$o_ticksys->ticksys_pass."&FOR_DATE=".$DATA_STR."&FIRST_CITY=".$FIRST_CITY."&LAST_CITY=".$LAST_CITY."&company=".$o_ticksys->company."&MIN_FREE_PLACES=0"; 
	echo $info."<br>"; */
	$info = $o_ticksys->ticksys_url."GET_KURS_INFO&PASS=".$o_ticksys->ticksys_pass."&COMPANY=".$o_ticksys->company."&FIRST_CITY=".$FIRST_CITY."&LAST_CITY=".$LAST_CITY."&KURS_ID=".$_GET['KURS_ID']; 
	//echo $info; 
	$infoxml = simplexml_load_file($info);
	
	
	
	
	
	
	
	$prices=array();
	foreach($infoxml->COMPANY->SCHEDULE->PRICES as $price){
		$pricename = $price->PRICE_NAME->asXML();
		$pricetype = $price->PRICE_TYPE->asXML();
		$actual_price = $price->PRICE->asXML();
		$pricename = utf8_to_cp1251($pricename);
		$prices[]=array(
			"pricename" => strip_tags($pricename),
			"pricetype" => strip_tags($pricetype),
			"actual_price" => strip_tags($actual_price)
		);
	}
	$datetime = $infoxml->COMPANY->SCHEDULE->SCHEDULE_DATETIME;
	
	// $regularPrice = $price
	function findWhere($array, $matching) {
		foreach ($array as $item) {
			$is_match = true;
			foreach ($matching as $key => $value) {

				if (is_object($item)) {
					if (! isset($item->$key)) {
						$is_match = false;
						break;
					}
				} else {
					if (! isset($item[$key])) {
						$is_match = false;
						break;
					}
				}

				if (is_object($item)) {
					if ($item->$key != $value) {
						$is_match = false;
						break;
					}
				} else {
					if ($item[$key] != $value) {
						$is_match = false;
						break;
					} 
				}
			}

			if ($is_match) {
				return $item;   
			}
		}

		return false;
	}
	$prices = array_map("unserialize", array_unique(array_map("serialize", $prices)));
	//$traat = array_search("�������", $prices);
	$regularprice = findWhere($prices, array(
		'pricename' => '�������'
	));
	$regularprice = $regularprice["actual_price"];
	
	echo "
	<div class='col-xs-12'>
		<div class='alert alert-info'>
		<h3><i class='fa fa-bus fa-2'></i> ���������� �� ����� �� ����� <strong>".$o_ticksys->cities[$FIRST_CITY]." - ".$o_ticksys->cities[$LAST_CITY]."</strong></h3>
		<h3><i class='fa fa-time fa-2'></i> ���� � ��� �� ��������: <strong>".$datetime."</strong></h3>
		<hr/>
		<h3>������ �� �������� <strong>�������� 5 ������.</strong></h3>
		<hr/>
		<h4><i class='fa fa-ticket fa-2'></i> ���� �� ������</h4>
	";
	echo "<table style='width:100%;'>";
	foreach($prices as $price){
		if(($price["pricetype"] == "1") && ($o_ticksys->online_discount>0)){
			$redovenPrice = floatval($price["actual_price"]);
			$redovenPriceMinusTenPercent = round($redovenPrice - ( $redovenPrice * ($o_ticksys->online_discount/100)), 2);
			echo "<tr><td>".$price["pricename"]."</td><td><strong style='text-decoration: line-through;'>".$price["actual_price"]." ��</strong></td></tr><tr><td>� ������ �������� ".$o_ticksys->online_discount."%</td><td><strong>".$redovenPriceMinusTenPercent." ��</strong></td></tr>";
		}else{
			echo "<tr><td>".$price["pricename"]."</td><td><strong>".$price["actual_price"]." ��</strong></td></tr>";
		}
	}
	echo "</table>";
	echo '
		<br/>
		<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#sitplaces">
		  ������ �����
		</button>';
	echo "<div class='clearfix'></div>";
	echo "</div>";
	echo "</div>";
	$url=$o_ticksys->ticksys_url."GET_FREE_PLACES&PASS=".$o_ticksys->ticksys_pass."&COMPANY=".$o_ticksys->company."&FIRST_CITY=".$FIRST_CITY."&LAST_CITY=".$LAST_CITY."&KURS_ID=".$KURS_ID;
	$xml = simplexml_load_file($url);
	
	?>
	<div class="modal fade" id="sitplaces" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">����� �� �����</h4>
		  </div>
		  <div class="modal-body">
	<?php
	
	$i=0;
	
	echo "<div class='col-xs-12'>";
		echo "<div class='row' style='margin-bottom:10px;'>";
		$busseats = array();
		foreach($xml->COMPANY->BUS_PLACE as $seat){
			$i++;
			$seatrow = preg_replace("/[^0-9]/","", $seat->ROW->asXML());
			$seatposition = preg_replace("/[^0-9]/","", $seat->POS->asXML());
			$seatnumber = preg_replace("/[^0-9]/","", $seat->NUMBER->asXML());
			$taken = preg_replace("/[^0-9]/","", $seat->TAKEN->asXML());
			
			$busseats[]=array(
				"seatrow" => $seatrow,
				"seatposition" => $seatposition,
				"seatnumber" => $seatnumber,
				"taken" => $taken,
			);
			
		}
		
		$rows=array();
		$cols=array();
		foreach($busseats as $seat){
			$seatrow = $seat["seatrow"];
			$seatposition = $seat["seatposition"];
			$rows[]=$seatrow;
			$cols[]=$seatposition;
		}
		$maxrows = max($rows);
		$maxcols = max($cols);
		
		for($i=1; $i<=$maxrows; $i++){
			echo "<div class='row'>";
			
			foreach($busseats as $seat){
				$seatrow = $seat["seatrow"];
				$seatnumber = $seat["seatnumber"];
				$taken = $seat["taken"];
				$seatposition = $seat["seatposition"];
				if($seatrow == $i){
					//echo "�����#".$seatnumber."&nbsp;";
					for($j=1; $j<=$maxcols; $j++){
						if($seatposition == $j){
							if($taken==1){
								$seatimage="taken";
							}else{
								$seatimage="free";
							}
							echo "
								<div class='col-xs-2' style='width:20%;'>
									<div class='seat ".$seatimage." text-center' style='width:50px;'>
											<strong>".$seatnumber."</strong>
											<input type='hidden' class='seat_red' value='".$seatrow."'/>
											<input type='hidden' class='seat_kol' value='".$seatposition."'/>
											<input type='hidden' class='seat_nom' value='".$seatnumber."'/>
									</div><br />
								</div>";
						}
							$k=$seatnumber;
							$l = $j+1;
							$m = $busseats[$k]["seatposition"];
							// if($seatposition == $j && $m!=$l){
								// echo "<div class='col-xs-2' style='width:20%;'></div>";
							// }
						
						// if($seatposition == $j && $j==2){
							// echo "<div class='col-xs-2' style='width:20%;'></div>";
						// }
					}
					
				}
			}
			echo "</div>";
		}
		echo "</div>";
	echo "</div>";
	
?>
<style> 
.taken {
background-color: #ff6e6e;
padding: 15px 0;
width: 50px;
height: 50px;
font-size: 18px !important;
}

.free {
background-color: #82dc78;
padding: 15px 0;
width: 50px;
height: 50px;
font-size: 18px !important;
}

.selected {
background-color: #00cbe7;
padding: 15px 0;
width: 50px;
height: 50px;
font-size: 18px !important;
}

</style>
			<div class="clearfix"></div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default btn-lg pull-left" data-dismiss="modal">������</button>
		  </div>
		</div>
	  </div>
	</div>
<div class="clearfix"></div>
<div class="col-xs-12 show-grid">
		<h3>������� ������</h3>
	<div class="alert alert-info" id="chosenHolder">
		<div class="clearfix"></div>
		<hr>
		<button class="btn btn-primary btn-small" id="renewPrice">������ ������</button>
	</div>
	<div class='col-xs-12'>
		<div class='alert alert-info'>
			<table>
				<tr>
					<td>����:</td>
					<td><strong><span id="priceInfo"></span> ��</strong></td>
				</tr>
			</table>
			<hr>
			<br>
			<script src="http://1000hz.github.io/bootstrap-validator/dist/validator.min.js"></script>
			<form id="payToBank" action="<?=$kickoffURL;?>" data-toggle="validator" role="form">
				<div class="form-group">
					<div class="input-group">
					  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
					  <input name="nameorder" id="nameorder" data-minlength="5" data-error="����, �������� ���" type="text" class="form-control" placeholder="���" aria-describedby="basic-addon1" required>
					</div>
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
					<div class="input-group">
					  <span class="input-group-addon" id="basic-addon2"><i class="fa fa-envelope"></i></span>
					  <input name="emailorder" id="emailorder" data-minlength="5" type="email" data-error="����, �������� E-Mail" class="form-control" placeholder="E-mail" aria-describedby="basic-addon2" required>
					</div>
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
					<div class="input-group">
					  <span class="input-group-addon" id="basic-addon3"><i class="fa fa-phone"></i></span>
					  <input name="telefonorder" id="telefonorder" data-minlength="5" data-error="����, �������� �������" type="text" class="form-control" placeholder="�������" aria-describedby="basic-addon3" required>
					</div>
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon4"><i class="fa fa-barcode fa-2"></i> ����� �� ����� �� ��������</span>
						<input type="text" name="cardnumberorder" id="cardnumberorder" class="form-control" placeholder="�������� �����..." aria-describedby="basic-addon4">
					</div>
					<div class="help-block with-errors"></div>
				</div>
                <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"  id="basic-addon5"><input type="checkbox" aria-label="���� ������� �� ��������!" data-error="���� ���������� �� ������ �������!" aria-describedby="basic-addon5" required><a href="http://www.mbus-bg.com/obshti-uslovia.html" target="_blank">�������� � ������� ������ ������� �� ��������!</a></span>
                    <div class="help-block with-errors"></div>
                    </div>
                </div>

				<input type="hidden" name="quantity" value="1"/>
				<input type="hidden" name="total" id="actualmoney" value=""/>
				<input type="hidden" name="trackid" id="trackid" value="0"/>
				<input type="hidden" name="FROM" id="FROM" value="<?=$o_ticksys->cities[$FIRST_CITY];?>"/>
				<input type="hidden" name="TO" id="TO" value="<?=$o_ticksys->cities[$LAST_CITY];?>"/>
				<input type="hidden" name="departure" id="departure" value="<?=$datetime;?>"/>
				<input type="hidden" name="kurs_id" id="kurs_id" value="<?=$KURS_ID;?>"/>
				<input type="hidden" name="for_date" id="for_date" value="<?=$_GET["DATA_STR"];?>"/>
				<input type="hidden" name="schedule_id" id="schedule_id" value="<?=$infoxml->COMPANY->SCHEDULE->ID;?>"/>
				<input type="hidden" name="price_id" id="price_id" value="<?=$infoxml->COMPANY->SCHEDULE->PRICES->PRICE_ID;?>"/>
				<input type="hidden" name="price_type_id" id="price_type_id" value="<?=$infoxml->COMPANY->SCHEDULE->PRICES->PRICE_TYPE;?>"/>
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">��������!</span>
                    �������� ���������� ������ ��� � �������, ���������� ���� � ������� ����� �� ���������� ��� �������!
                </div>
				<button type="submit" id="paymentBtn" class="btn btn-success btn-md disabled">
					�������
				</button>
			</form>
			<script>
			/* $j("#payToBank input.form-control").focusout(function(){
				
				
			}); */
			</script>
		</div>
	</div>
</div>
		<div class="clearfix"></div>
<style>
.seat.free img, .seat.selected img{
	cursor:pointer;
}
.fixedalert{
	position:fixed;
	top:0px;
	right:-300px;
}
#chosenHolder .col-xs-6{
	margin-bottom:20px;
}
.seat{
	font-size:12px;
}
.show-grid{
	display:none;
}

</style>
<script>
function recalculate(){
	var obshtak=0;
	var minustenpercent=0;
	var totaled=0;
	$j("#chosenHolder select").each(function(){
		var selected=parseFloat($j(this).find("option:selected").val());
		obshtak=selected+obshtak;
		$j("#priceInfo").html(obshtak);
		
		/* minustenpercent=obshtak*0.1;
		minustenpercent=minustenpercent.toFixed(2);
		$j("#minustenpercent").html(minustenpercent); */
		
		// totaled=obshtak-minustenpercent;
		// $j("#totaled").html(totaled);
		$j("#actualmoney").val(obshtak);
		$j("#paymentBtn .badge").html(obshtak+" ��");
	});
	
}
$j(document).ready(function(){
	$j('#payToBank').validator({
		disable: true
	});
	
	$j("#renewPrice").click(function(){
		recalculate();
	});
	$j(document.body).on("change", "#chosenHolder select", function(){
		recalculate();
	});
	
	var total=0;
	var chosen=0;
	var regularPrice = parseFloat(<?=$regularprice;?>);
	$j(document.body).on("click", ".seat.free", function(){
		$j("#payToBank").validator('validate');
		if(chosen==5){
			alert("�� ������ �� �������� ������ �����.");
		}else{
			total=regularPrice+total;
			$j(this).find("img").attr("src", "http://maksoft.net/web/forms/mbus/tickets/images/selected-seat.png");
			$j(this).removeClass("free").addClass("selected");
			chosen = chosen + 1;
			var seat_red = $j(this).find(".seat_red").val();
			var seat_kol = $j(this).find(".seat_kol").val();
			var seat_nom = $j(this).find(".seat_nom").val();
			$j("#chosenHolder").prepend("<div class='col-xs-6'>"+
			"<div class='' id='ticket"+seat_nom+"'>"+
				"<strong>����� "+seat_nom+"</strong> ���: "+seat_red+" ������: "+seat_kol+
			"</div>"+
			"<select class='form-control aboutTickets' id='"+seat_nom+"'>"+
				<?php 
				foreach($prices as $price){
					if($price["pricetype"] == "1"){
						$redovenPrice = floatval($price["actual_price"]);
						$redovenPriceMinusTenPercent = round($redovenPrice - ( $redovenPrice * ($o_ticksys->online_discount/100)), 2);
						
						?>
						"<option class='<?=$price["pricetype"];?>' typeofticket='<?=$price["pricetype"];?>' value='<?=$redovenPriceMinusTenPercent;?>'><?=$price["pricename"];?> - <?=$redovenPriceMinusTenPercent;?> ��.</option>"+
						<?php
					}else{
						?>
						"<option class='<?=$price["pricetype"];?>' typeofticket='<?=$price["pricetype"];?>' value='<?=$price["actual_price"];?>'><?=$price["pricename"];?> - <?=$price["actual_price"];?> ��.</option>"+
						<?php
					}
				}
				$without = $redovenPrice * 0.2;
				$PriceForCardHolder = $redovenPrice - $without;
				
				?>
				"<option class='99' typeofticket='99' value='<?=$PriceForCardHolder;?>'>� ����� - <?=$PriceForCardHolder;?> ��.</option>"+
			"</select>"+
			"</div>");
			$j(".fixedalert").remove();
			$j(".modal-footer").append('<div class="alert alert-success fixedalert alert-dismissible" role="alert">\
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
				<strong>�����!</strong> ��������� �����: ����� '+seat_nom+', ���: '+seat_red+' ������: '+seat_kol+'</a>.\
				</div>');
			//$j("#priceInfo").html(total);
			$j("#actualmoney").before('<input type="hidden" class="biletHidden" biletId="'+seat_nom+'" id="ticketa'+seat_nom+'" value="����� '+seat_nom+' ���: '+seat_red+' ������: '+seat_kol+'"/>');
			recalculate();
			$j(".show-grid").show();
		}
	});
	$j(document.body).on("click", ".alert button.close", function(){
		$j(this).parent().remove();
	});
	$j(document.body).on("click", ".seat.selected", function(){
		$j(this).find("img").attr("src", "http://maksoft.net/web/forms/mbus/tickets/images/free-seat.png");
		$j(this).removeClass("selected").addClass("free");
		var seat_nom = $j(this).find(".seat_nom").val();
		var seat_red = $j(this).find(".seat_red").val();
		var seat_kol = $j(this).find(".seat_kol").val();
		$j("#ticket"+seat_nom).parent().remove();
		chosen = chosen - 1;
		$j(".fixedalert").remove();
		$j("#ticketa"+seat_nom).remove();
		$j(".modal-footer").append('<div class="alert alert-info fixedalert alert-dismissible" role="alert">\
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
				<strong>�����!</strong> ����������� �����: ����� '+seat_nom+', ���: '+seat_red+' ������: '+seat_kol+'</a>.\
				</div>');
		total=regularPrice+total;
		//$j("#priceInfo").html(total);
		recalculate();
		if(chosen==0){
			$j(".show-grid").hide();
		}
	});
	
	$j("#payToBank #paymentBtn").on("click", function(event){
		if (!($j(this).hasClass('disabled'))) {
			event.preventDefault();
			var ticketsArray = [];
			var TicketsIds = [];
			$j(".biletHidden").each(function(){
				var thisval = $j(this).val();
				ticketsArray.push(thisval);
				var ticketId = $j(this).attr("biletId");
				TicketsIds.push(ticketId);
			});
			var kurs_id=$j("#kurs_id").val();
			
			<?php
			$userid=0;
			if(($user->ID)!='' && ($user->ID)!=null){
				$userid=$user->ID;
			}
			?>
			var userid=<?php echo $userid;?>;
			var name=$j("#nameorder").val();
			var EMail=$j("#emailorder").val();
			var Phone=$j("#telefonorder").val();
			var discountcard=$j("#cardnumberorder").val();
			var company=<?php echo $o_ticksys->company;?>;
			var SiteID=<?=$o_ticksys->SiteID;?>;
			var formSerialized = $j("#payToBank").serialize();
			var for_date=$j("#for_date").val();
			var schedule_id=$j("#schedule_id").val();
			var price_id=$j("#price_id").val();
			var price_type_id=$j("#price_type_id").val();
			var FROM=$j("#FROM").val();
			var TO=$j("#TO").val();
			var departure=$j("#departure").val();
			var ticketsPrices = [];
			$j(".aboutTickets").each(function(){
				var ticketAdditionalInfo = { }; // or "var valueToPush = new Object();" which is the same
				ticketAdditionalInfo["NomerID"] = $j(".aboutTickets").attr("id");
				ticketAdditionalInfo["Money"] = $j(".aboutTickets").val();
				ticketsPrices.push(ticketAdditionalInfo);
			});
			$j.ajax({
				url: "http:\/\/www.mbus-bg.com\/web\/forms\/mbus\/tickets\/ajaxhandler.php",
				data: {FROM: FROM, TO: TO, departure: departure, ticketsArray: ticketsArray, kurs_id: kurs_id, userid: userid, name: name, EMail: EMail, Phone: Phone, discountcard: discountcard, company: company, SiteID: SiteID, formSerialized: formSerialized, TicketsIds:TicketsIds, for_date:for_date, schedule_id:schedule_id, price_id:price_id, price_type_id:price_type_id, ticketsPrices:ticketsPrices},
				type: "POST",
				success: function(response){
					if(response!="error"){
						$j("#trackid").val(response);
						$j("#payToBank").submit();
					}else{
						alert("���� �� ������� � ���� ����������� �������������� ����� ��� �� �� ��������. ���������� �� �� ���������");
						location.reload();
					}
					
				}
			});
		
		}
		//alert(ticketsArray);
	});
	$j(document.body).on("change", ".aboutTickets", function(){
		var selected = $j(this).find("option:selected").attr("typeofticket");
		if(selected=="99"){
			$j("#cardnumberorder").attr("required", "");
			$j("#cardnumberorder").attr("data-error", "������� ��� ����� � �����, ���� ��������� ����� �� ����� �� ��������! ����� � �� ����������� ������� ��� ����������� � ��������!");
		}else if(selected!="99"){
			$j("#cardnumberorder").removeAttr("required");
			$j("#cardnumberorder").removeAttr("data-error");
		}
		/* else if(selected!="99"){
			$j("#cardnumberorder").removeAttr("required");
			$j("#cardnumberorder").removeAttr("data-error");
		} */
		$j("#payToBank").validator('validate');
		var ticketsWithCard = 0;
		$j(".aboutTickets").each(function(){
			var thisTicketType = $j(this).find("option:selected").attr("typeofticket");
			if(thisTicketType=="99"){
				$j(this).addClass("doNotRemove99");
				ticketsWithCard++;
			}
			/* else if(thisTicketType!="99"){
				$j(this).removeClass("doNotRemove99");
				ticketsWithCard--;
				//"<option class='99' typeofticket='99' value='<?=$PriceForCardHolder;?>'>� ����� - <?=$PriceForCardHolder;?> ��.</option>"
			} */
		});
		if(ticketsWithCard>0){
			$j(".aboutTickets option.99:not(.doNotRemove99 option.99)").remove();
		}
	})
});
</script>
