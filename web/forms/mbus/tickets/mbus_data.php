﻿<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
include_once("/hosting/maksoft/maksoft/lib/lib_ticksys.php"); 
$o_ticksys = new ticksys(); 
$o_ticksys->company = 4;
$for_date = $_GET['data'];
$cities = $o_ticksys->get_destinations($val, $for_date); 

$options_str = ""; 
foreach($cities as $city) {
	$options_str = $options_str."<option value=\"".$city['ID']."\">".$city['NAME']."</option>"; 
}
echo('
  <select name="dest_city" id="dest_city" onchange="">
    <option value="0">- изберете направление -</option>
	'.$options_str.'
  </select>
'); 	

?>
