<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script> -->
<?php
function get_data($url) {
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function dump($var, $debug=False){
    if($debug){
        print "<pre>";
        print_r($var);
        print "</pre>";
    }
}

function price_handler(object $schedule){
    $pr = array();
    foreach($schedule->PRICES as $price){
        $pricename = $price->PRICE_NAME;
        $pricetype = $price->PRICE_TYPE;
        $actual_price = $price->PRICE2;
        $pricename = utf8_to_cp1251($pricename);
        $pr[$price->PRICE_ID]=array(
            "pricename" => strip_tags($pricename),
            "pricetype" => strip_tags($pricetype),
            "actual_price" => strip_tags($actual_price),
            "price_id" => $price->PRICE_ID
        );
    }
    return $pr;
}

function findWhere($array, $matching) {
    foreach ($array as $item) {
        $is_match = true;
        foreach ($matching as $key => $value) {

            if (is_object($item)) {
                if (! isset($item->$key)) {
                    $is_match = false;
                    break;
                }
            } else {
                if (! isset($item[$key])) {
                    $is_match = false;
                    break;
                }
            }

            if (is_object($item)) {
                if ($item->$key != $value) {
                    $is_match = false;
                    break;
                }
            } else {
                if ($item[$key] != $value) {
                    $is_match = false;
                    break;
                } 
            }
        }

        if ($is_match) {
            return $item;   
        }
    }

    return false;
}

	require_once "./pay/UBB/Universal/UniversalPlugin.php";
	require_once "./pay/UBB/Universal/UniversalPluginXMLFileParser.php";
	require_once "./pay/UBB/Universal/Framework.php";
	
	$destination = $_REQUEST['destination'];
	if (!strcmp($destination, "Merchant")) {
	    $kickoffURL = "http://www.trans5.bg/pay/UBB/Universal/UniversalPluginCheckoutMerchantPaymentPage.php";
	} else {
	    $kickoffURL = "http://www.trans5.bg/pay/UBB/Universal/UniversalPluginCheckoutPaymentInit.php";
	}
	$firstTrip_KURS_ID = $_GET["firstTrip_KURS_ID"];
	$firstTrip_FIRST_CITY = $_GET["firstTrip_FIRST_CITY"];
	$firstTrip_LAST_CITY = $_GET["firstTrip_LAST_CITY"];
	$firstTrip_DATA_STR = $_GET["firstTrip_DATA_STR"];
	
	$firstTrip = $o_ticksys->ticksys_url."GET_KURS_INFO&PASS=".$o_ticksys->ticksys_pass."&COMPANY=".$o_ticksys->company."&FIRST_CITY=".$firstTrip_FIRST_CITY."&LAST_CITY=".$firstTrip_LAST_CITY."&KURS_ID=".$firstTrip_KURS_ID; 
	
    $json = json_decode(get_data($firstTrip."&JSON=T"));
    $schedule1 = reset($json->RESULT->COMPANY->SCHEDULE);
    $firstTripXml = $schedule1;
    dump($schedule1->ID);

	dump($schedule1);
	// $firstTripXml = simplexml_load_file($firstTrip);
	$firstTrip_datetime = $schedule1->SCHEDULE_DATETIME;
	
	
	$secondTrip_KURS_ID = $_GET["secondTrip_KURS_ID"];
	$secondTrip_FIRST_CITY = $_GET["secondTrip_FIRST_CITY"];
	$secondTrip_LAST_CITY = $_GET["secondTrip_LAST_CITY"];
	$secondTrip_DATA_STR = $_GET["secondTrip_DATA_STR"];
	
	$secondTrip = $o_ticksys->ticksys_url."GET_KURS_INFO&PASS=".$o_ticksys->ticksys_pass."&COMPANY=".$o_ticksys->company."&FIRST_CITY=".$secondTrip_FIRST_CITY."&LAST_CITY=".$secondTrip_LAST_CITY."&KURS_ID=".$secondTrip_KURS_ID; 

    $json = json_decode(get_data($secondTrip."&JSON=T"));
    $schedule2 = reset($json->RESULT->COMPANY->SCHEDULE);
    $secondTripXml = $schedule2;
	
	// print "<pre>";
	// print $secondTrip;
	// print "</pre>";
	
	// $secondTripXml = simplexml_load_file($secondTrip);
    $secondTrip_datetime = $schedule2->SCHEDULE_DATETIME;

    $prices = price_handler($schedule1);

	$prices = array_map("unserialize", array_unique(array_map("serialize", $prices)));
	//$traat = array_search("�������", $prices);
	$regularprice = findWhere($prices, array(
		'pricename' => '�������'
	));
	$regularprice = $regularprice["actual_price"];
	
	echo '
	<div class="col-xs-12">
		<div class="alert alert-info">
		<div class="col-xs-7">
			<h2><i class="fa fa-bus fa-2"></i> ������<br><strong>'.$o_ticksys->cities[$firstTrip_FIRST_CITY].' >> '.$o_ticksys->cities[$firstTrip_LAST_CITY].'</strong></h2>
			<h3><i class="fa fa-clock-o fa-2"></i> ���� � ��� �� ��������:<br /><br /> <strong>'.$firstTrip_datetime.'</strong></h3>
		</div>
		<div class="col-xs-5">
			<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#sitplacesfirstTrip">
			  ������ ����� �� �������
			</button>
		</div>
		<div class="clearfix"></div>
		<hr/>
		<div class="col-xs-7">
			<h2><i class="fa fa-bus fa-2"></i> ������<br><strong>'.$o_ticksys->cities[$secondTrip_FIRST_CITY].' >> '.$o_ticksys->cities[$secondTrip_LAST_CITY].'</strong></h2>
			<h3><i class="fa fa-clock-o fa-2"></i> ���� � ��� �� ��������:<br /><br /> <strong>'.$secondTrip_datetime.'</strong></h3>
		</div>
		<div class="col-xs-5">
			<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#sitplacessecondTrip">
			  ������ ����� �� �������
			</button>
		</div>
		<div class="clearfix"></div>
		<hr/>
		<h4><i class="fa fa-ticket fa-2"></i> ���� �� ������</h4>
	';
	echo "<table style='width:100%;'>";
	foreach($prices as $price){
		if($price["pricetype"] == "1"){
			$redovenPrice = floatval($price["actual_price"]);
			$redovenPriceMinusTenPercent = round($redovenPrice - ( $redovenPrice * (10/100)), 2);
			echo "<tr><td>".$price["pricename"]."</td><td><strong style='text-decoration: line-through;'>".$price["actual_price"]." ��</strong></td></tr><tr><td>� ��������</td><td><strong>".$redovenPriceMinusTenPercent." ��</strong></td></tr>";
		}else{
			echo "<tr><td>".$price["pricename"]."</td><td><strong>".$price["actual_price"]." ��</strong></td></tr>";
		}
	}
	echo "</table>";
	echo "* ������ �� �������� <strong>�������� 5 ������.</strong>";
	echo "<div class='clearfix'></div>";
	echo "</div>";
	echo "</div>";
	
	$firstTripFreePlaces=$o_ticksys->ticksys_url."GET_FREE_PLACES&PASS=".$o_ticksys->ticksys_pass."&COMPANY=".$o_ticksys->company."&FIRST_CITY=".$firstTrip_FIRST_CITY."&LAST_CITY=".$firstTrip_LAST_CITY."&KURS_ID=".$firstTrip_KURS_ID;
	$firstTripFreePlacesXml = simplexml_load_file($firstTripFreePlaces);
	//echo $firstTripFreePlaces;
	
	$secondTripFreePlaces=$o_ticksys->ticksys_url."GET_FREE_PLACES&PASS=".$o_ticksys->ticksys_pass."&COMPANY=".$o_ticksys->company."&FIRST_CITY=".$secondTrip_FIRST_CITY."&LAST_CITY=".$secondTrip_LAST_CITY."&KURS_ID=".$secondTrip_KURS_ID;
    if($o_page->_user['ID'] == 1424){
        echo "<br>".$secondTripFreePlaces."<br>";
    }
	$secondTripFreePlacesXml = simplexml_load_file($secondTripFreePlaces);
	//echo $secondTripFreePlaces;
	
?>
<div class='clearfix'></div>
<div class="modal fade" id="sitplacesfirstTrip" tabindex="-1" role="dialog" aria-labelledby="sitplacesfirstTripLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="sitplacesfirstTripLabel">����� �� ����� �� �������</h4>
		  </div>
		  <div class="modal-body">
			<?php
	
				$i=0;
				
				echo "<div class='col-xs-12'>";
					echo "<div class='row' style='margin-bottom:10px;'>";
					$busseats = array();
					foreach($firstTripFreePlacesXml->COMPANY->BUS_PLACE as $seat){
						$i++;
						$seatrow = preg_replace("/[^0-9]/","", $seat->ROW->asXML());
						$seatposition = preg_replace("/[^0-9]/","", $seat->POS->asXML());
						$seatnumber = preg_replace("/[^0-9]/","", $seat->NUMBER->asXML());
						$taken = preg_replace("/[^0-9]/","", $seat->TAKEN->asXML());
						
						$busseats[]=array(
							"seatrow" => $seatrow,
							"seatposition" => $seatposition,
							"seatnumber" => $seatnumber,
							"taken" => $taken,
						);
						
					}
					
					$rows=array();
					$cols=array();
					foreach($busseats as $seat){
						$seatrow = $seat["seatrow"];
						$seatposition = $seat["seatposition"];
						$rows[]=$seatrow;
						$cols[]=$seatposition;
					}
					$maxrows = max($rows);
					$maxcols = max($cols);
					
					for($i=1; $i<=$maxrows; $i++){
						echo "<div class='row'>";
						
						foreach($busseats as $seat){
							$seatrow = $seat["seatrow"];
							$seatnumber = $seat["seatnumber"];
							$taken = $seat["taken"];
							$seatposition = $seat["seatposition"];
							if($seatrow == $i){
								//echo "�����#".$seatnumber."&nbsp;";
								for($j=1; $j<=$maxcols; $j++){
									if($seatposition == $j){
										if($taken==1){
											$seatimage="taken";
										}else{
											$seatimage="free";
										}
										echo "
											<div class='col-xs-2' style='width:20%;'>
									<div class='seat ".$seatimage." text-center' style='width:50px;'>
											<strong>".$seatnumber."</strong>
											<input type='hidden' class='seat_red' value='".$seatrow."'/>
											<input type='hidden' class='seat_kol' value='".$seatposition."'/>
											<input type='hidden' class='seat_nom' value='".$seatnumber."'/>
									</div><br />
								</div>";
									}
										$k=$seatnumber;
										$l = $j+1;
										$m = $busseats[$k]["seatposition"];
										// if($seatposition == $j && $m!=$l){
											// echo "<div class='col-xs-2' style='width:20%;'></div>";
										// }
									
									// if($seatposition == $j && $j==2){
										// echo "<div class='col-xs-2' style='width:20%;'></div>";
									// }
								}
								
							}
						}
						echo "</div>";
					}
					echo "</div>";
				echo "</div>";
				
			?>
<style> 
.taken {
background-color: #ff6e6e;
padding: 15px 0;
width: 50px;
height: 50px;
font-size: 18px !important;
}

.free {
background-color: #82dc78;
padding: 15px 0;
width: 50px;
height: 50px;
font-size: 18px !important;
}

.selected {
background-color: #00cbe7;
padding: 15px 0;
width: 50px;
height: 50px;
font-size: 18px !important;
}

</style>
		  <div class="clearfix"></div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">������</button>
		  </div>
		</div>
	  </div>
</div>
<div class='clearfix'></div>
<div class="modal fade" id="sitplacessecondTrip" tabindex="-1" role="dialog" aria-labelledby="sitplacessecondTripLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="sitplacessecondTripLabel">����� �� ����� �� �������</h4>
		  </div>
		  <div class="modal-body">
			<?php

				$i=0;
				
				echo "<div class='col-xs-12'>";
					echo "<div class='row' style='margin-bottom:10px;'>";
					unset($busseats);
					$busseats = array();
					foreach($secondTripFreePlacesXml->COMPANY->BUS_PLACE as $seat){
						$i++;
						$seatrow = preg_replace("/[^0-9]/","", $seat->ROW->asXML());
						$seatposition = preg_replace("/[^0-9]/","", $seat->POS->asXML());
						$seatnumber = preg_replace("/[^0-9]/","", $seat->NUMBER->asXML());
						$taken = preg_replace("/[^0-9]/","", $seat->TAKEN->asXML());
						
						$busseats[]=array(
							"seatrow" => $seatrow,
							"seatposition" => $seatposition,
							"seatnumber" => $seatnumber,
							"taken" => $taken,
						);
						
					}
					
					$rows=array();
					$cols=array();
					foreach($busseats as $seat){
						$seatrow = $seat["seatrow"];
						$seatposition = $seat["seatposition"];
						$rows[]=$seatrow;
						$cols[]=$seatposition;
					}
					$maxrows = max($rows);
					$maxcols = max($cols);
					
					for($i=1; $i<=$maxrows; $i++){
						echo "<div class='row'>";
						
						foreach($busseats as $seat){
							$seatrow = $seat["seatrow"];
							$seatnumber = $seat["seatnumber"];
							$taken = $seat["taken"];
							$seatposition = $seat["seatposition"];
							if($seatrow == $i){
								//echo "�����#".$seatnumber."&nbsp;";
								for($j=1; $j<=$maxcols; $j++){
									if($seatposition == $j){
										if($taken==1){
											$seatimage="taken";
										}else{
											$seatimage="free";
										}
										echo "
											<div class='col-xs-2' style='width:20%;'>
									<div class='seat ".$seatimage." text-center' style='width:50px;'>
											<strong>".$seatnumber."</strong>
											<input type='hidden' class='seat_red' value='".$seatrow."'/>
											<input type='hidden' class='seat_kol' value='".$seatposition."'/>
											<input type='hidden' class='seat_nom' value='".$seatnumber."'/>
									</div><br />
								</div>";
									}
										$k=$seatnumber;
										$l = $j+1;
										$m = $busseats[$k]["seatposition"];
										// if($seatposition == $j && $m!=$l){
											// echo "<div class='col-xs-2' style='width:20%;'></div>";
										// }
									
									// if($seatposition == $j && $j==2){
										// echo "<div class='col-xs-2' style='width:20%;'></div>";
									// }
								}
								
							}
						}
						echo "</div>";
					}
					echo "</div>";
				echo "</div>";
				
			?>
		  <div class="clearfix"></div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">������</button>
		  </div>
		</div>
	  </div>
</div>
<div class="clearfix"></div>
<div class="col-xs-12 show-grid">
		<h3>������� ����� �� �������</h3>
	<div class="alert alert-info" id="chosenHolderGoing">
		<div class="clearfix"></div>
		<hr>
		<button class="btn btn-primary btn-small" id="renewPrice">������ ������</button>
	</div>
		<h3>������� ����� �� �������</h3>
	<div class="alert alert-info" id="chosenHolderReturn">
		<div class="clearfix"></div>
	</div>
	<div class='col-xs-12'>
		<div class='alert alert-info'>
			<table>
				<tr>
					<td>����:</td>
					<td><strong><span id="priceInfo"></span> ��</strong></td>
				</tr>
			</table>
			<hr>
			<small><strong>��� ����������� ������ �� ������������ ������� �� ��������.<br>
			��� ������������� �� ������� �� �������� ������� ���������.</strong></small>
			
			<br>
			<script src="http://1000hz.github.io/bootstrap-validator/dist/validator.min.js"></script>
			<form id="payToBank" action="<?=$kickoffURL;?>" data-toggle="validator" role="form">
				<div class="form-group">
					<div class="input-group">
					  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
					  <input name="nameorder" id="nameorder" data-minlength="5" data-error="����, �������� ���" type="text" class="form-control" placeholder="���" aria-describedby="basic-addon1" required>
					</div>
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
					<div class="input-group">
					  <span class="input-group-addon" id="basic-addon2"><i class="fa fa-envelope"></i></span>
					  <input name="emailorder" id="emailorder" data-minlength="5" type="email" data-error="����, �������� E-Mail" class="form-control" placeholder="E-mail" aria-describedby="basic-addon2" required>
					</div>
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
					<div class="input-group">
					  <span class="input-group-addon" id="basic-addon3"><i class="fa fa-phone"></i></span>
					  <input name="telefonorder" id="telefonorder" data-minlength="5" data-error="����, �������� �������" type="text" class="form-control" placeholder="�������" aria-describedby="basic-addon3" required>
					</div>
					<div class="help-block with-errors"></div>
				</div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"  id="basic-addon5"><input type="checkbox" aria-label="���� ������� �� ��������!" data-error="���� ���������� �� ������ �������!"  required><a href="http://www.trans5.bg/Trans-5/obshti-uslovia.html" target="_blank">�������� � ������� ������ ������� �� ��������!</a></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
				<!-- <div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon4"><i class="fa fa-barcode fa-2"></i> ����� �� ����� �� ��������</span>
						<input type="text" name="cardnumberorder" id="cardnumberorder" class="form-control" placeholder="�������� �����..." aria-describedby="basic-addon4">
					</div>
					<div class="help-block with-errors"></div>
				</div> -->
                <?php
                    $firstTripPrices = array_shift($firstTripXml->PRICES);
                    $secondTripPrices = array_shift($secondTripXml->PRICES);
                if($o_page->_user['ID'] == 1424){

                    echo "<pre><code>";
                    print_r($firstTripPrices);
                    #print_r($firstTripXml);
                    echo "</pre></code>";
                }
                ?>
				<input type="hidden" name="quantity" value="1"/>
				<input type="hidden" name="total" id="actualmoney" value=""/>
				<input type="hidden" name="trackid" id="trackid" value="0"/>
				<input type="hidden" name="FROM" id="FROM" value="<?=$o_ticksys->cities[$firstTrip_FIRST_CITY];?>"/>
				<input type="hidden" name="TO" id="TO" value="<?=$o_ticksys->cities[$firstTrip_LAST_CITY];?>"/>
				<input type="hidden" name="firstTrip_datetime" id="firstTrip_datetime" value="<?=$firstTrip_datetime;?>"/>
				<input type="hidden" name="secondTrip_datetime" id="secondTrip_datetime" value="<?=$secondTrip_datetime;?>"/>
				<input type="hidden" name="kurs_id_firstTrip" id="kurs_id_firstTrip" value="<?=$firstTrip_KURS_ID;?>"/>
				<input type="hidden" name="kurs_id_secondTrip" id="kurs_id_secondTrip" value="<?=$secondTrip_KURS_ID;?>"/>
				<input type="hidden" name="for_date_firstTrip" id="for_date_firstTrip" value="<?=$_GET["firstTrip_DATA_STR"];?>"/>
				<input type="hidden" name="for_date_secondTrip" id="for_date_secondTrip" value="<?=$_GET["secondTrip_DATA_STR"];?>"/>
				<input type="hidden" name="schedule_id_firstTrip" id="schedule_id_firstTrip" value="<?=$schedule1->ID;?>"/>
				<input type="hidden" name="schedule_id_secondTrip" id="schedule_id_secondTrip" value="<?=$schedule2->ID;?>"/>
				<input type="hidden" name="price_id_firstTrip" id="price_id_firstTrip" value="<?=$firstTripPrices->PRICE_ID;?>"/>
				<input type="hidden" name="price_id_secondTrip" id="price_id_secondTrip" value="<?=$secondTripPrices->PRICE_ID;?>"/>
				<input type="hidden" name="price_type_id_firstTrip" id="price_type_id_firstTrip" value="<?=$firstTripPrices->PRICE_TYPE;?>"/>
				<input type="hidden" name="price_type_id_secondTrip" id="price_type_id_secondTrip" value="<?=$secondTripPrices->PRICE_TYPE;?>"/>
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">��������!</span>
                    �������� ���������� ������ ��� � �������, ���������� ���� � ������� ����� �� ���������� ��� �������!
                </div>
				<button type="submit" id="paymentBtn" class="btn btn-success btn-md disabled">
					�������
				</button>
			</form>
			<script>
			/* $j("#payToBank input.form-control").focusout(function(){
				
				
			}); */
			</script>
		</div>
	</div>
</div>
		<div class="clearfix"></div>
<style>
.seat.free img, .seat.selected img{
	cursor:pointer;
}
.fixedalert{
	position:fixed;
	top:0px;
	right:-300px;
}
#chosenHolder .col-xs-6{
	margin-bottom:20px;
}
.seat{
	font-size:12px;
}
.show-grid{
	display:none;
}

</style>
<script>
function recalculate(){
	var obshtak=0;
	var minustenpercent=0;
	var totaled=0;
	$j("#chosenHolderGoing select").each(function(){
		var selected=parseFloat($j(this).find("option:selected").val());
		obshtak=selected+obshtak;
        console.log(obshtak);
		$j("#priceInfo").html(obshtak);
		
		/* minustenpercent=obshtak*0.1;
		minustenpercent=minustenpercent.toFixed(2);
		$j("#minustenpercent").html(minustenpercent); */
		
		// totaled=obshtak-minustenpercent;
		// $j("#totaled").html(totaled);
		$j("#actualmoney").val(obshtak);
		$j("#paymentBtn .badge").html(obshtak+" ��");
	});
	
}
$j(document).ready(function(){
	$j('#payToBank').validator({
		disable: true
	});
	
	$j("#renewPrice").click(function(){
		recalculate();
	});
	$j(document.body).on("change", "#chosenHolderGoing select", function(){
		recalculate();
	});
	
	var total=0;
	var chosen=0;
	var regularPrice = parseFloat(<?=$regularprice;?>);
	$j(document.body).on("click", "#sitplacesfirstTrip .seat.free", function(){
		$j("#payToBank").validator('validate');
		if(chosen==5){
			alert("�� ������ �� �������� ������ �����.");
		}else{
			total=regularPrice+total;
			$j(this).find("img").attr("src", "/web/forms/trans5/images/selected-seat.png");
			$j(this).removeClass("free").addClass("selected");
			chosen = chosen + 1;
			var seat_red = $j(this).find(".seat_red").val();
			var seat_kol = $j(this).find(".seat_kol").val();
			var seat_nom = $j(this).find(".seat_nom").val();
			$j("#chosenHolderGoing").prepend("<div class='col-xs-6'>"+
			"<div class='' id='ticket"+seat_nom+"'>"+
				"<strong>����� "+seat_nom+"</strong> ���: "+seat_red+" ������: "+seat_kol+
			"</div>"+
			"<select class='form-control aboutTickets' id='"+seat_nom+"'>"+
				<?php 
				foreach($prices as $price){
					if($price["pricetype"] == "1"){
						$redovenPrice = floatval($price["actual_price"]);
						$redovenPriceMinusTenPercent = round($redovenPrice - ( $redovenPrice * (10/100)), 2);
						
						?>
						"<option class='<?=$price["pricetype"];?>' typeofticket='<?=$price["pricetype"];?>' value='<?=$redovenPriceMinusTenPercent;?>'><?=$price["pricename"];?> - <?=$redovenPriceMinusTenPercent;?> ��.</option>"+
						<?php
					}else{
						?>
						"<option class='<?=$price["pricetype"];?>' typeofticket='<?=$price["pricetype"];?>' value='<?=$price["actual_price"];?>'><?=$price["pricename"];?> - <?=$price["actual_price"];?> ��.</option>"+
						<?php
					}
				}
				$without = $redovenPrice * 0.2;
				$PriceForCardHolder = $redovenPrice - $without;
				
				?>
				//"<option class='99' typeofticket='99' value='<?=$PriceForCardHolder;?>'>� ����� - <?=$PriceForCardHolder;?> ��.</option>"+
			"</select>"+
			"</div>");
			$j(".fixedalert").remove();
			$j(".modal-footer").append('<div class="alert alert-success fixedalert alert-dismissible" role="alert">\
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
				<strong>�����!</strong> ��������� �����: ����� '+seat_nom+', ���: '+seat_red+' ������: '+seat_kol+'</a>.\
				</div>');
			//$j("#priceInfo").html(total);
			$j("#actualmoney").before('<input type="hidden" class="biletHiddenfirstTrip" biletId="'+seat_nom+'" id="ticketa'+seat_nom+'" value="����� '+seat_nom+' ���: '+seat_red+' ������: '+seat_kol+'"/>');
			recalculate();
			$j(".show-grid").show();
		}
	});
	$j(document.body).on("click", ".alert button.close", function(){
		$j(this).parent().remove();
	});
	$j(document.body).on("click", "#sitplacesfirstTrip .seat.selected", function(){
		$j(this).find("img").attr("src", "/web/forms/trans5/images/free-seat.png");
		$j(this).removeClass("selected").addClass("free");
		var seat_nom = $j(this).find(".seat_nom").val();
		var seat_red = $j(this).find(".seat_red").val();
		var seat_kol = $j(this).find(".seat_kol").val();
		$j("#ticket"+seat_nom).parent().remove();
		chosen = chosen - 1;
		$j(".fixedalert").remove();
		$j("#ticketa"+seat_nom).remove();
		$j(".modal-footer").append('<div class="alert alert-info fixedalert alert-dismissible" role="alert">\
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
				<strong>�����!</strong> ����������� �����: ����� '+seat_nom+', ���: '+seat_red+' ������: '+seat_kol+'</a>.\
				</div>');
		total=regularPrice+total;
		//$j("#priceInfo").html(total);
		recalculate();
		if(chosen==0){
			$j(".show-grid").hide();
		}
	});
	var chosenreturn=0;
	$j(document.body).on("click", "#sitplacessecondTrip .seat.free", function(){
		if(chosen==0){
			alert("������� ����� �� ������� ����� �� �������� ����� �� �������!");
		}else if(chosenreturn>=chosen){
			alert("�� ������ �� �������� ������ ����� �� �������, ������ �� �� ������� ���� � ������� �� �������.");
		}else{
			chosenreturn++;
			$j(this).find("img").attr("src", "/web/forms/trans5/images/selected-seat.png");
			$j(this).removeClass("free").addClass("selected");
			var seat_red = $j(this).find(".seat_red").val();
			var seat_kol = $j(this).find(".seat_kol").val();
			var seat_nom = $j(this).find(".seat_nom").val();
			$j("#chosenHolderReturn").prepend('\
			<div class="col-xs-6" style="margin-bottom:20px;">\
			<span class="label label-success" id="ticket'+seat_nom+'">����� '+seat_nom+'</strong> ���: '+seat_red+' ������: '+seat_kol+'</span>\
			</div>\
			');
			$j(".fixedalert").remove();
			$j(".modal-footer").append('<div class="alert alert-success fixedalert alert-dismissible" role="alert">\
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
				<strong>�����!</strong> ��������� �����: ����� '+seat_nom+', ���: '+seat_red+' ������: '+seat_kol+'</a>.\
				</div>');
			$j("#actualmoney").before('<input type="hidden" class="biletHiddensecondTrip" biletId="'+seat_nom+'" id="ticketa'+seat_nom+'" value="����� '+seat_nom+' ���: '+seat_red+' ������: '+seat_kol+'"/>');
			recalculate();
			$j(".show-grid").show();
		}
	});
	
	$j(document.body).on("click", "#sitplacessecondTrip .seat.selected", function(){
		$j(this).find("img").attr("src", "/web/forms/trans5/images/free-seat.png");
		$j(this).removeClass("selected").addClass("free");
		var seat_nom = $j(this).find(".seat_nom").val();
		var seat_red = $j(this).find(".seat_red").val();
		var seat_kol = $j(this).find(".seat_kol").val();
		$j("#ticket"+seat_nom).parent().remove();
		chosenreturn = chosenreturn - 1;
		$j(".fixedalert").remove();
		$j("#ticketa"+seat_nom).remove();
		$j(".modal-footer").append('<div class="alert alert-info fixedalert alert-dismissible" role="alert">\
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
				<strong>�����!</strong> ����������� �����: ����� '+seat_nom+', ���: '+seat_red+' ������: '+seat_kol+'</a>.\
				</div>');
		total=regularPrice+total;
		//$j("#priceInfo").html(total);
		recalculate();
		if(chosen==0){
			$j(".show-grid").hide();
		}
	});
	$j("#payToBank #paymentBtn").on("click", function(event){
		if (!($j(this).hasClass('disabled'))) {
			if(chosenreturn==chosen){
				event.preventDefault();
				var ticketsArrayFirstTrip = [];
				var TicketsIdsFirstTrip = [];
				$j(".biletHiddenfirstTrip").each(function(){
					var thisval = $j(this).val();
					ticketsArrayFirstTrip.push(thisval);
					var ticketId = $j(this).attr("biletId");
					TicketsIdsFirstTrip.push(ticketId);
				});
				var ticketsArraySecondTrip = [];
				var TicketsIdsSecondTrip = [];
				$j(".biletHiddensecondTrip").each(function(){
					var thisval = $j(this).val();
					ticketsArraySecondTrip.push(thisval);
					var ticketId = $j(this).attr("biletId");
					TicketsIdsSecondTrip.push(ticketId);
				});
				var kurs_id_firstTrip=$j("#kurs_id_firstTrip").val();
				var kurs_id_secondTrip=$j("#kurs_id_secondTrip").val();
				
				<?php
				$userid=0;
				if(($user->ID)!='' && ($user->ID)!=null){
					$userid=$user->ID;
				}
				?>
				var userid=<?php echo $userid;?>;
				var name=$j("#nameorder").val();
				var EMail=$j("#emailorder").val();
				var Phone=$j("#telefonorder").val();
				
				var company=<?php echo $o_ticksys->company;?>;
				var SiteID=<?=$o_ticksys->SiteID;?>;
				var formSerialized = $j("#payToBank").serialize();
				var for_date_firstTrip=$j("#for_date_firstTrip").val();
				var for_date_secondTrip=$j("#for_date_secondTrip").val();
				var schedule_id_firstTrip=$j("#schedule_id_firstTrip").val();
				var schedule_id_secondTrip=$j("#schedule_id_secondTrip").val();
				var price_id_firstTrip=$j("#price_id_firstTrip").val();
				var price_id_secondTrip=$j("#price_id_secondTrip").val();
				var price_type_id_firstTrip=$j("#price_type_id_firstTrip").val();
				var price_type_id_secondTrip=$j("#price_type_id_secondTrip").val();
				var FROM=$j("#FROM").val();
				var TO=$j("#TO").val();
				var firstTrip_datetime=$j("#firstTrip_datetime").val();
				var secondTrip_datetime=$j("#secondTrip_datetime").val();
				var ticketsPrices = [];
				$j(".aboutTickets").each(function(){
					var ticketAdditionalInfo = { }; // or "var valueToPush = new Object();" which is the same
					ticketAdditionalInfo["NomerID"] = $j(".aboutTickets").attr("id");
					ticketAdditionalInfo["Money"] = $j(".aboutTickets").val();
					ticketsPrices.push(ticketAdditionalInfo);
				});
				$j.ajax({
					url: "/web/forms/trans5/ajaxhandler_twoway.php",
					data: {
							FROM: FROM,
							TO: TO,
							firstTrip_datetime: firstTrip_datetime,
							secondTrip_datetime: secondTrip_datetime,
							ticketsArrayFirstTrip: ticketsArrayFirstTrip,
							ticketsArraySecondTrip: ticketsArraySecondTrip,
							kurs_id_firstTrip: kurs_id_firstTrip, 
							kurs_id_secondTrip: kurs_id_secondTrip, 
							userid: userid, 
							name: name, 
							EMail: EMail, 
							Phone: Phone, 
							company: company, 
							SiteID: SiteID, 
							formSerialized: formSerialized, 
							TicketsIdsFirstTrip:TicketsIdsFirstTrip, 
							TicketsIdsSecondTrip:TicketsIdsSecondTrip, 
							for_date_firstTrip:for_date_firstTrip, 
							for_date_secondTrip:for_date_secondTrip, 
							schedule_id_firstTrip:schedule_id_firstTrip, 
							schedule_id_secondTrip:schedule_id_secondTrip, 
							price_id_firstTrip:price_id_firstTrip, 
							price_id_secondTrip:price_id_secondTrip, 
							price_type_id_firstTrip:price_type_id_firstTrip,
							price_type_id_secondTrip:price_type_id_secondTrip,
							ticketsPrices:ticketsPrices
						},
					type: "POST",
					success: function(response){
						//console.log(response);
						 if(response!="error"){
							$j("#trackid").val(response);
							$j("#payToBank").submit();
						}else{
							alert("�� ��� ������ ������ ����� (���, e-mail, �������) ��� ���� �� ������� ����� ��� ������� � ���� ����������� �������������� ����� ��� �� �� ��������. ���������� �� �� ���������! ���� �������� ����� �����!");
							//location.reload();
							console.log(response);
						} 
						
					}
				});
			}else{
				event.preventDefault();
				alert("�������� ����� ���� ����� �� ������� � �������");
			}
				
		
		}
		//alert(ticketsArray);
	});
	$j(document.body).on("change", ".aboutTickets", function(){
		var selected = $j(this).find("option:selected").attr("typeofticket");
		if(selected=="99"){
			$j("#cardnumberorder").attr("required", "");
			$j("#cardnumberorder").attr("data-error", "������� ��� ����� � �����, ���� ��������� ����� �� ����� �� ��������");
		}else if(selected!="99"){
			$j("#cardnumberorder").removeAttr("required");
			$j("#cardnumberorder").removeAttr("data-error");
		}
		/* else if(selected!="99"){
			$j("#cardnumberorder").removeAttr("required");
			$j("#cardnumberorder").removeAttr("data-error");
		} */
		$j("#payToBank").validator('validate');
		var ticketsWithCard = 0;
		$j(".aboutTickets").each(function(){
			var thisTicketType = $j(this).find("option:selected").attr("typeofticket");
			if(thisTicketType=="99"){
				$j(this).addClass("doNotRemove99");
				ticketsWithCard++;
			}
			/* else if(thisTicketType!="99"){
				$j(this).removeClass("doNotRemove99");
				ticketsWithCard--;
				//"<option class='99' typeofticket='99' value='<?=$PriceForCardHolder;?>'>� ����� - <?=$PriceForCardHolder;?> ��.</option>"
			} */
		});
		if(ticketsWithCard>0){
			$j(".aboutTickets option.99:not(.doNotRemove99 option.99)").remove();
		}
	})
});

    function updatePrices()
    {
        var result;
        var selected = $("select").val()
        $.each($("option"), function(i, option){
            if(selected == $(option).val()){
                result = $(option).attr('typeofticket')
            }
        });
        var prices = <?=json_encode($prices)?>;
        if(price = prices[result]){
            console.log(price);
            $("#price_id").val(price.priceId);
            $("#price_type_id").val(price.pricetype);
        }
        return result;
    }
</script>
