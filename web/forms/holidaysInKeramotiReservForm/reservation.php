<link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<style>
span.redstar{
	color:red;
}
.main-footer .small-post {
    border-bottom-color: rgb(210, 204, 200);
  border-bottom-style: solid;
  border-bottom-width: 1px;
  display: block;
  float: left;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 13px;
  height: 150px;
  margin-bottom: 0px;
  margin-left: 0px;
  margin-right: 0px;
  margin-top: 0px;
  min-height: 170px;
  width: 30%;
}
.main-footer .small-post h2{
	 border-bottom-color: rgb(28, 153, 176);
  border-bottom-style: solid;
  border-bottom-width: 1px;
  color: rgb(28, 153, 176);
  display: block;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 19.5px;
  font-weight: bold;
  height: 47px;
  margin-bottom: 10px;
  margin-left: 0px;
  margin-right: 0px;
  margin-top: 0px;
  padding-bottom: 10px;
  padding-left: 0px;
  padding-right: 0px;
  padding-top: 10px;
  width: 290px;
}
</style>
<script src="//code.jquery.com/jquery-2.1.3.js"></script>
<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="http://www.maksoft.net/web/forms/holidaysInKeramotiReservForm/js/validator.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(document).ready(function(){
		$('#reservation_keramoti_appartments').validator();
	
	/* $('#reservation_keramoti_appartments').validator().on('submit', function (e) {
	  if (e.isDefaultPrevented()) {
		// handle the invalid form...
		alert('wrong');
	  } else {
		// everything looks good!
		alert('right');
	  }
	}) */
})
</script>
<div class="row">
      <div class="col-md-12">
		<?php
			if($sent == 1) mk_output_message('normal','
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  '.LABEL_SUCCESS_RESERV_FORM.'
				</div>');
		?>
        <div class="well well-sm">
          <form role="form" data-toggle="validator" id="reservation_keramoti_appartments" class="form-horizontal" name="form1" method="post" action="/form-cms.php?<?php echo("n=$n&SiteID=$SiteID"); ?>">
          <fieldset>
            <legend class="text-center"><?=LABEL_RESERVATION;?></legend>
    
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="Name"><?=LABEL_NAME;?></label>
              <div class="col-md-9">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-user"></i></div>
					<input id="Name" name="Name" type="text" placeholder="<?=LABEL_YOURNAME;?>" class="form-control" value="" data-error="<?=LABEL_FILLNAME;?>" required>
					<div class="input-group-addon"><span class="redstar">*</span></div>
				</div>
					<div class="help-block with-errors"></div>
              </div>
            </div>
			
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="Middlename"><?=LABEL_MIDDLENAME;?></label>
              <div class="col-md-9">
                
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-user"></i></div>
					<input id="Middlename" name="Middlename" type="text" placeholder="<?=LABEL_YOUR_MIDDLENAME;?>" class="form-control" value="" data-error="<?=LABEL_FILLMIDDLENAME;?>" required>
					<div class="input-group-addon"><span class="redstar">*</span></div>
				</div>
					<div class="help-block with-errors"></div>
              </div>
            </div>
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="Family"><?=LABEL_FAMILY;?></label>
              <div class="col-md-9">
                
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-user"></i></div>
					<input id="Family" name="Family" type="text" placeholder="<?=LABEL_YOUR_FAMILY;?>" class="form-control" value="" data-error="<?=LABEL_FILLFAMILY;?>" required>
					<div class="input-group-addon"><span class="redstar">*</span></div>
				</div>
					<div class="help-block with-errors"></div>
              </div>
            </div>
			
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="EGN"><?=LABEL_EGN;?></label>
              <div class="col-md-9">
                
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-user"></i></div>
					<input id="EGN" name="EGN" type="text" placeholder="<?=LABEL_YOUREGN;?>" class="form-control" value="" data-error="<?=LABEL_FILLEGN;?>" required>
					<div class="input-group-addon"><span class="redstar">*</span></div>
				</div>
				<div class="help-block with-errors"></div>
              </div>
            </div>
			
            <!-- city input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="address"><?=LABEL_ADDRESS?></label>
              <div class="col-md-9">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-road"></i></div>
					<input id="address" name="address" type="text" placeholder="<?=LABEL_ADDRESS?>" class="form-control" data-error="<?=LABEL_FILLADDRESS?>" required>
					<div class="input-group-addon"><span class="redstar">*</span></div>
				</div>
				<div class="help-block with-errors"></div>
			  </div>
            </div>
			
            <!-- phone input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="Phone"><?=LABEL_YOURPHONE;?></label>
              <div class="col-md-9">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-phone"></i></div>
					<input id="Phone" name="Phone" type="text" placeholder="<?=LABEL_PHONE;?>" class="form-control" data-error="<?=LABEL_FILLPHONE;?>" required>
					<div class="input-group-addon"><span class="redstar">*</span></div>
				</div>
				<div class="help-block with-errors"></div>
              </div>
            </div>
    
            <!-- Email input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="EMail">E-mail</label>
              <div class="col-md-9">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-envelope"></i></div>
					<input id="EMail" name="EMail" type="email" placeholder="EMail" class="form-control" value="<?=$user->EMail;?>" data-error="<?=LABEL_FILLMAIL;?>" required>
					<div class="input-group-addon"><span class="redstar">*</span></div>
				</div>
					<div class="help-block with-errors"></div>
              </div>
            </div>
			
            <!-- city input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="arrival"><?=LABEL_ARRIVAL;?></label>
              <div class="col-md-9">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
					<input id="arrival" name="arrival" type="text" placeholder="<?=LABEL_ARRIVAL;?>" class="form-control" data-error="<?=LABEL_FILLARRIVAL;?>" required>
					<div class="input-group-addon"><span class="redstar">*</span></div>
				</div>
					<div class="help-block with-errors"></div>
			  </div>
            </div>
            <!-- city input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="leaving"><?=LABEL_DEPARTURE;?></label>
              <div class="col-md-9">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
					<input id="leaving" name="leaving" type="text" placeholder="����" class="form-control" data-error="<?=LABEL_FILLDEPARTURE;?>" required>
					<div class="input-group-addon"><span class="redstar">*</span></div>
				</div>
					<div class="help-block with-errors"></div>
			  </div>
            </div>
    
			  <script>
			  $(function() {
				$( "#arrival, #leaving" ).datepicker({
					showAnim: "slideDown",
					dateFormat: "dd-mm-yy",
					dayNamesMin: [ "��", "��", "��", "��", "��", "��", "��" ],
					firstDay: 1,
					monthNames: [ "������", "��������", "����", "�����", "���", "���", "���", "������", "���������", "��������", "�������", "��������" ]
					}
				);
			  });
			  </script>
    
            <!-- city input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="hoteltype"><?=LABEL_PICKVILLA;?></label>
              <div class="col-md-9">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-home"></i></div>
					<select id="hoteltype" name="hoteltype" class="form-control" data-error="<?=LABEL_FILLPICKVILLA;?>" required>
						<option></option>
						<option value='���� "�����"'><?=VILLA_BELLEVUE;?></option>
						<option value='���� "���������"'><?=VILLA_EXCELSIOR;?></option>
						<option value='������� ��� ����������� ����'><?=DUPLEX_ONEFAMILY;?></option>
					</select>
					<div class="input-group-addon"><span class="redstar">*</span></div>
				</div>
					<div class="help-block with-errors"></div>
			  </div>
            </div>
			
            <div class="form-group">
              <label class="col-md-3 control-label" for="guests"><?=LABEL_GUESTNUMBER;?></label>
              <div class="col-md-9">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-users"></i></div>
					<input id="guests" name="guests" type="number" placeholder="-" class="form-control" data-error="<?=LABEL_FILLGUESTNUMBER;?>" required>
					<div class="input-group-addon"><span class="redstar">*</span></div>
				</div>
					<div class="help-block with-errors"></div>
			  </div>
            </div>
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="Zapitvane"><?=LABEL_ADDITIONALINFO;?></label>
              <div class="col-md-9">
                <textarea class="form-control" id="Zapitvane" name="Zapitvane" placeholder="<?=LABEL_FILLADDITIONALINFO;?>" rows="5"></textarea>
              </div>
            </div>
			
			
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="codestr"></label>
              <div class="col-md-9">
          <?php
          include("../form_captcha.php");
          ?>
					<div class="help-block with-errors"></div>
              </div>
            </div>
			
			<input name="ConfText" type="hidden" id="ConfText" value="����������� �� � ����������!"> 
			<input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
			<input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
			<input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$Site->EMail"); ?>"> 
			<input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$Site->StartPage&SiteID=$SiteID&sent=1"); ?>">
			<input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
			<input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-9 text-right">
                <button type="submit" id="submitter" class="btn btn-default btn-lg"><?=LABEL_SUBMIT;?></button>
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
</div>