<script language="JavaScript">
$j(document).ready(function(){
		
		$j("#button_submit").click(function(){
			
			var product_model = $j("#product_model").val();
			var product_price = $j("#product_price").val();
				
			if(product_model == "") $j("#product_model").css("background", "#F00");
			else $j("#product_model").css("background", "#FFF");

			if(product_price == "") $j("#product_price").css("background", "#F00");
			else $j("#product_price").css("background", "#FFF");
									
			if(product_model != "" && product_price != "")
				$j("#request_form").submit();	
			else alert("�������� ���������� � * �� ������������!");
		})
		
		$j("#product_brand").change(function(){
			var brand = $j("#product_brand").val(); 
			$j("#product_type").load("web/forms/aircon/functions.php", "brand="+brand);
		})
		
		var brand = $j("#product_brand").val(); 
		$j("#product_type").load("web/forms/aircon/functions.php", "brand="+brand);
		
		$j("#new_brand").click(function(){
			alert("���� ���� �������� ���� ����� ��������� ��������� � �������� ���� �������� � F5 !");
			window.open('http://www.maksoft.net/page.php?n=111&ParentPage=129739&SiteID=678','mywindow','width=1000,height=600,toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes, resizable=yes');
		})
});

</script>
<?php
	if($user->AccessLevel >= $row->SecLevel)
	{
	require_once "lib/lib_page.php";
	
	$o_user = new user($user->username, $user->pass);
	$o_site = new site();
	$o_page = new page();
	
	###########################################
	function list_menu($name, $data, $params)
		{
			$content = "";
			$content.= "<select name=\"".$name."\" style=\"width: 300px\" $params>";
			$keys = array_keys($data);
			for($i=0; $i<count($keys); $i++)
				$content.= "<option value=\"".$keys[$i]."\">".$data[$keys[$i]]."</option>";
			$content.= "</select>";
			return $content;
		}	
	
	//product brand
	$products_category = 129739;
	$product_brand = array();
	$brands = $o_page->get_pSubpages($products_category);
	for($i=0; $i<count($brands); $i++)
		$product_brand[$brands[$i]['n']] = $brands[$i]['Name'];
	
	//product type
	$product_type = array();
				
	//product kind
	$product_kinds = array("0"=>"������ ��������", "1"=>"����� ��������", "2"=>"������� ��������", "3"=> "������-������� ��������", "4"=> "������� ��������", "5"=> "������� (duct) ��������");
	/*
	echo "<pre>";
	print_r($_POST);
	echo "</pre>";
	*/	
	$pParent = $_POST['product_type'];
	$pName = $product_kinds[$_POST["product_kind"]] ." ". str_replace("��������� -", "", $product_brand[$_POST['product_brand']])." - ����� ".$_POST['product_model'];
	$pImage = $_FILES['product_image'];
	$pImageWidth = 200;
	//default image align 
	$pImageAlign = 1; 
	$pParagraph = "<p>";
	$pParagraph.= "<strong>�����:</strong>".str_replace("��������� -", "", $product_brand[$_POST['product_brand']])."<br>";
	$pParagraph.= "<strong>���:</strong> ".$product_kinds[$_POST["product_kind"]]."<br>";
	$pParagraph.= "<strong>������:</strong> ".$_POST["product_opt4"]." kW<br>";
	$pParagraph.= "<strong>�����:</strong> ".$_POST["product_opt11"]." m3/h<br>";
	
	if($_POST['product_price_type'] == 0)
		$pParagraph.= "<strong>����:</strong> <span class=\"price\">".$_POST["product_price"]."</span>";
		
	if($_POST['product_price_type'] == 2)
		$pParagraph.= "<strong>����� ����:</strong> <span class=\"special_price\">".$_POST["product_price"]."</span>";
	
	$pParagraph.= "</p>";
	
	$pText = $pParagraph;
	$pText.= "<table border=\"0\" cellpadding=\"5\" cellspacing=\"2\" class=\"product_table\">";
	$pText.= "<thead><tr><th colspan=\"3\">".$product_kinds[$_POST["product_kind"]]." - ".$_POST["product_model"]."</tr></thead>";
	$pText.= '
    <tr>
      <td width="180" align="right" valign="top"><strong>BTU:</strong></td>
      <td width="30" align="center" valign="top">&nbsp;</td>
      <td width="290" valign="top">'.$_POST['product_opt1'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>��������. ���� m2 / ���� m3</strong></td>
      <td align="center" valign="top">&nbsp;</td>
      <td valign="top">'.$_POST['product_opt2'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>K�������� ���������</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top">'.$_POST['product_opt3'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>��.������ ���������</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top">'.$_POST['product_opt4'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>����������� / ���������</strong></td>
      <td align="center" valign="top">EER</td>
      <td valign="top">'.$_POST['product_opt5'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>��������� ���������</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top">'.$_POST['product_opt6'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>��.������ ���������</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top">'.$_POST['product_opt7'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>����������� / ���������</strong></td>
      <td align="center" valign="top">COP</td>
      <td valign="top">'.$_POST['product_opt8'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>���. ��������� (-10&deg;C)</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top">'.$_POST['product_opt9'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>�������� ������ (-10&deg;C)</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top">'.$_POST['product_opt10'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>����� ����������</strong></td>
      <td align="center" valign="top">m3/h</td>
      <td valign="top">'.$_POST['product_opt11'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>������ ��������</strong></td>
      <td align="center" valign="top">dB(A)</td>
      <td valign="top">'.$_POST['product_opt12'].'</td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>������� � <em>mm</em></strong></td>
      <td align="center" valign="top">�/�/�</td>
      <td valign="top">'.$_POST['product_opt13'].'</td>
    </tr>
	<tr>
		<th colspan="3">������ ������� - '.$_POST["out_opt1"].'
	</tr>
	<tr>
		<td align="right" valign="top"><strong>���������</strong></td>
		<td colspan="2" valign="top">'.$_POST['out_opt2'].'</td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>��. ����������</strong></td>
		<td colspan="2" valign="top">'.$_POST['out_opt3'].'</td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>������ ��������</strong></td>
		<td colspan="2" valign="top">'.$_POST['out_opt4'].'</td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>����. ������ ��� / �����������</strong></td>
		<td colspan="2" valign="top">'.$_POST['out_opt5'].'</td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>����� ����� ���� / ���</strong></td>
		<td colspan="2" valign="top">'.$_POST['out_opt6'].'</td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>t&deg; �� ���������� </strong></td>
		<td colspan="2" valign="top">'.$_POST['out_opt7'].'</td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>������ � <em>mm</em></strong></td>
		<td colspan="2" valign="top">'.$_POST['out_opt8'].'</td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>����� � <em>kg</em></strong></td>
		<td colspan="2" valign="top">'.$_POST['out_opt9'].'</td>
	</tr>
	
	';
	
	if($_POST['product_price_type'] == 1)
		$pText.= "<tfoot><tr><td colspan=\"3\"><h2>���� �� ��������� !</h2></tfoot>";	
	
	$pText.= "</table>";
	
	if($_POST['action'] == "add")
		{
			
			$inserted_id = $o_page->add($pParent, $pName);
			mk_output_message("normal", "������ ������ � �������� �������: <a href=\"page.php?n=".$inserted_id."&SiteID=".$SiteID."\" target=\"_blank\">".$pName."</a>");
			//mk_output_message("normal", "������ ������ �� � ��������, �� ������� ����:");
			echo $pText;
		}
	else
	{
?>
<form method="post" enctype="multipart/form-data" id="request_form">
  <table border="0" align="center" cellpadding="5" cellspacing="2" class="border_table">
	 <thead><tr>
	   <th colspan="3">��������</thead>
   <tbody>
   <tr>
    <td align="right" valign="middle"><b>* �����:</b></td>
    <td colspan="2"><?=list_menu("product_brand", $product_brand, "id=\"product_brand\"")?><br><a href="#" id="new_brand" class="bgcolor_link">+ �������� �����</a></td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>* ��� ��������:</b></td>
    <td colspan="2"><?=list_menu("product_type", $product_type, "id=\"product_type\"")?></td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>* ��� ��������:</b></td>
    <td colspan="2"><?=list_menu("product_kind", $product_kinds, "")?></td>
   </tr>
    <tr>
      <td align="right" valign="top">* <strong>�����:</strong></td>
      <td align="center" valign="top">&nbsp;</td>
      <td valign="top"><input name="product_model" type="text" id="product_model" size="25" value="<?=$_POST['product_model']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>BTU:</strong></td>
      <td width="30" align="center" valign="top">&nbsp;</td>
      <td width="290" valign="top"><input name="product_opt1" type="text" id="product_opt1" size="25" value="<?=$_POST['product_opt1']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>��������. ���� m2 / ���� m3</strong></td>
      <td align="center" valign="top">&nbsp;</td>
      <td valign="top"><input name="product_opt2" type="text" id="product_opt2" size="25" value="<?=$_POST['product_opt2']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>K�������� ���������</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top"><input name="product_opt3" type="text" id="product_opt3" size="25" value="<?=$_POST['product_opt3']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>��.������ ���������</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top"><input name="product_opt4" type="text" id="product_opt4" size="25" value="<?=$_POST['product_opt4']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>����������� / ���������</strong></td>
      <td align="center" valign="top">EER</td>
      <td valign="top"><input name="product_opt5" type="text" id="product_opt5" size="25" value="<?=$_POST['product_opt5']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>��������� ���������</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top"><input name="product_opt6" type="text" id="product_opt6" size="25" value="<?=$_POST['product_opt6']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>��.������ ���������</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top"><input name="product_opt7" type="text" id="product_opt7" size="25" value="<?=$_POST['product_opt7']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>����������� / ���������</strong></td>
      <td align="center" valign="top">COP</td>
      <td valign="top"><input name="product_opt8" type="text" id="product_opt8" size="25" value="<?=$_POST['product_opt8']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>���. ��������� (-10&deg;C)</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top"><input name="product_opt9" type="text" id="product_opt9" size="25" value="<?=$_POST['product_opt9']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>�������� ������ (-10&deg;C)</strong></td>
      <td align="center" valign="top">kW</td>
      <td valign="top"><input name="product_opt10" type="text" id="product_opt10" size="25" value="<?=$_POST['product_opt10']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>����� ����������</strong></td>
      <td align="center" valign="top">m3/h</td>
      <td valign="top"><input name="product_opt11" type="text" id="product_opt11" size="25" value="<?=$_POST['product_opt11']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>������ ��������</strong></td>
      <td align="center" valign="top">dB(A)</td>
      <td valign="top"><input name="product_opt12" type="text" id="product_opt12" size="25" value="<?=$_POST['product_opt12']?>"></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>������� � <em>mm</em></strong></td>
      <td align="center" valign="top">�/�/�</td>
      <td valign="top"><input name="product_opt13" type="text" id="product_opt13" size="25" value="<?=$_POST['product_opt13']?>"></td>
    </tr>
    <tr>
      <td align="left" valign="top">&nbsp;</td>
      <td colspan="2" valign="top">&nbsp;</td>
      </tr>
    <tr>
      <td align="right" valign="top"><strong>������:</strong></td>
      <td colspan="2" valign="top"><input type="file" name="product_image" title="������" id="product_image">
        </td>
    </tr>
    <tr>
      <td colspan="3" valign="top">&nbsp;</td>
    </tr> 
	<tr>
	    <td align="right"><strong>����:</strong> *     
	    <td colspan="2" align="left"><input name="product_price" type="text" id="product_price" size="25" value="<?=$_POST['product_opt13']?>" />
      <tr>
        <td align="right">
        <td colspan="2" align="left"><label>
          <select name="product_price_type" id="product_price_type" style="width:180px">
            <option value="0" selected="selected">���������� ����</option>
            <option value="1">���� �� ���������</option>
            <option value="2">������������� ����</option>
          </select>
        </label>
      <tr>
        <td align="right">      
        <td colspan="2" align="left">      
      <tr>
        <th colspan="3" >������ �������
      <tr>
        <td align="right"><strong>�����:        
        </strong>
        <td colspan="2" align="left"><input name="out_opt1" type="text" id="out_opt1" size="25" value="<?=$_POST['out_opt1']?>" />      
      <tr>
        <td align="right"><strong>���������:
        </strong>
        <td colspan="2" align="left"><input name="out_opt2" type="text" id="out_opt2" size="25" value="<?=$_POST['out_opt2']?>" />      
      <tr>
        <td align="right"><strong>��. ����������:
        </strong>
        <td colspan="2" align="left"><input name="out_opt3" type="text" id="out_opt3" size="25" value="<?=$_POST['out_opt3']?>" />      
      <tr>
        <td align="right"><strong>������ �������� (db/A):
        </strong>
        <td colspan="2" align="left"><input name="out_opt4" type="text" id="out_opt4" size="25" value="<?=$_POST['out_opt4']?>" />      
      <tr>
        <td align="right"><strong>����. ������ ��� / �����������
        </strong>
        <td colspan="2" align="left"><input name="out_opt5" type="text" id="out_opt5" size="25" value="<?=$_POST['out_opt5']?>" />
      <tr>
        <td align="right"><strong>����� ����� ���� / ���      
        </strong>
        <td colspan="2" align="left"><input name="out_opt6" type="text" id="out_opt6" size="25" value="<?=$_POST['out_opt6']?>" />      
      <tr>
        <td align="right"><strong>t&deg; �� ����������      
        </strong>
        <td colspan="2" align="left"><input name="out_opt7" type="text" id="out_opt7" size="25" value="<?=$_POST['out_opt7']?>" />      
      <tr>
        <td align="right"><strong>������ � <em>mm</em>
        </strong>
        <td colspan="2" align="left"><input name="out_opt8" type="text" id="out_opt8" size="25" value="<?=$_POST['out_opt8']?>" />      
      <tr>
	    <td align="right"><strong>����� � <em>kg</em>
	    </strong>
	    <td colspan="2" align="left"><input name="out_opt9" type="text" id="out_opt9" size="25" value="<?=$_POST['out_opt9']?>" />
      </tbody>
      <tfoot>
      <tr><td colspan="3" align="center">
	  <input type="button" value="������ ��������" id="button_submit">
	  <input type="hidden" name="action" value="add">
  </tfoot></table>

</form>
<?
	}
}
else
	mk_output_message("error", "������ ������ �� ���� �������� !");
?>