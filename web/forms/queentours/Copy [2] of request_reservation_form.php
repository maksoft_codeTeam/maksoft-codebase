<script language="JavaScript" src="lib/lib_calendar.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.title; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' <?=FORM_MESSAGE_VALID_EMAIL?> \n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' <?=FORM_MESSAGE_MUST_CONTAIN_NUMBERS?>\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' <?=FORM_MESSAGE_MUST_CONTAIN_NUMBER_BETWEEN?> '+min+' - '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' <?=FORM_MESSAGE_REQUESTED_FIELD?>\n'; }
  } if (errors) alert('<?=FORM_MESSAGE_ALL_REQUESTED_FIELD?>\n'+errors);
  document.MM_returnValue = (errors == '');
}

function setDateLeave()
{
	var Months = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	obj1 = document.getElementById('date_arrive');
	
	if(obj1.value == "") return false;
	
	obj2 = document.getElementById('date_leave');
	count_nights = document.getElementById('count_nights')
	
	day = parseFloat(obj1.value.substring(8,10))
	month = parseFloat(obj1.value.substring(5,7))
	year = obj1.value.substring(0,4)
	
	day_leave = parseInt(day) + parseInt(count_nights.value)
	monthdays = Months[parseInt(month - 1)]
	
	if(day_leave > monthdays)
		{
			day_leave = parseInt(day_leave) - parseInt(monthdays)
			month = parseFloat(month + 1);
			if(month > 12)
			{
				month = 1
				year = parseInt(year) + 1
			}
		}
	
	if(day_leave < 10) day_leave = "0" + day_leave
	if(month < 10) month = "0" + month
		
	//date_leave = day_leave + "-" + month + "-" + year;
	date_leave = year + "-" + month + "-" + day_leave;
	obj2.value = date_leave
}

function show_kids_ages()
{
	table = document.getElementById('kids_ages')
	table.style.display = '';
	kids_counter = document.getElementById('person_kids')
	
	dplRow = 0;
	for(i=0; i<4; i++)
	{	
		if (table.rows.length > 1) table.deleteRow(table.rows.length - 1);
	}
	
	if(kids_counter.value == 0) table.style.display = 'none';	
	
	for(i=1; i<kids_counter.value; i++)
	{
   		var newRow = table.tBodies[0].rows[dplRow].cloneNode(true);		
		table.tBodies[0].appendChild(newRow);
	}
	//alert(kids_counter.value)
}

//-->
</script>
<center>
<div class="request_table">
<form name="request_form" method="post" action="/form-cms.php?<?php echo("n=$n&SiteID=$SiteID"); ?>">
<?php
$search_query = mysqli_query("SELECT * FROM pages p, images im WHERE p.ParentPage = '".$id."' AND p.imageNo = im.imageID");
//echo  mysqli_num_rows($search_query);

			  $info_box_contents = array();
			  $info_box_contents[] = array('text' => BOX_HEADING_REQUEST_RESERVATION);
			
			  new infoBoxHeading($info_box_contents, true, true, false);

 $info_box_contents = array();
 //antibot code
 $code = rand(0, 65535);   

//collect sent information

if(isset($id) && mysqli_num_rows($search_query) >0)
  	{
  				
		$offer = get_page($id);
		$offer_name = $offer->Name;
		$offer_img = "";
		if(file_exists($offer->image_src))
			{
				$offer_img = "<a href=\"".$offer->image_src."\" rel=\"lightbox[offer_gallery]\" title=\"".$offer->Name."\"><img src=\"img_preview.php?image_file=".$offer->image_src."&img_width=280\" border=\"0\" style=\"margin: 10px;\" class=\"border_image\" alt=\"".$offer->Name."\"></a>";
			}
				$offer_gallery = "";
				
				$gallery_query = mysqli_query("SELECT * FROM pages p left join images im on p.imageNo = im.imageID WHERE p.ParentPage = '".$id."' LIMIT 9");
				while($gallery = mysqli_fetch_object($gallery_query))
					$offer_gallery.= "<a href=\"".$gallery->image_src."\" rel=\"lightbox[offer_gallery]\" title=\"".$offer->Name ." / " . $gallery->Name . "\"><img src=\"img_preview.php?image_file=".$gallery->image_src."&img_width=80\" class=\"border_image\" alt=\"".$offer->Name ." / " . $gallery->Name."\"></a>";
			
			 
			  //person_old select menu
			  $select_person_old = "<select name=\"person_old\">";
			  for($j=1; $j<10; $j++)
			  	$select_person_old.= "<option value=\"$j\">$j</option>"; 
			  $select_person_old.= "<option value=\"10+\">10+</option>";
			  $select_person_old.= "</select>";
			  
			  //kids select menu
			  $select_kids = "<select name=\"person_kids\" id=\"person_kids\" onChange=\"show_kids_ages()\">";
			  for($j=0; $j<=4; $j++)
			  	$select_kids.= "<option value=\"$j\">$j</option>";
			  $select_kids.= "</select>";
			  
			  //count nights select menu
			  $count_nights = "<select name=\"count_nights\" onChange=\"setDateLeave()\">";
			  $count_nights.= "<option value=\"1\" selected>1". TEXT_NIGHTS . "</option>";
			  for($j=2; $j<=15; $j++)
			  	$count_nights.= "<option value=\"$j\">". $j . TEXT_NIGHTS . "</option>";
			  $count_nights.= "</select>";
			  			
			  $info_box_contents[] = array(array('text' => FORM_LABEL_HOTEL.' *', 'params'=>"class=label"), array('text' => FORM_LABEL_GALLERY, 'params'=>"class=label"));
			  $info_box_contents[] = array(array('text' => '<div class="text_field"><input name="hotel_name" title="'.FORM_LABEL_HOTEL.'" type="text" value="'.$offer->Name.'" size="35"></div>'), array('text' => $offer_img . $offer_gallery, 'params'=>"rowspan=16 valign=top width=300 align=center")); 
			  $info_box_contents[] = array(array('text' => FORM_LABEL_DATE_ARRIVE, 'params'=>"class=label valign=top"));
			  $info_box_contents[] = array(array('text' => '<input name="date_arrive" id="date_arrive" type="text" value="" size="30" disabled style="float: left;"> <a href="javascript: void()" onClick="javascript:MyCalendar(\'request_form\', \'date_arrive\'); return false" onFocus="setDateLeave()" class="calendar" title="'.FORM_LABEL_SLECT_ARRIVE_DATE.'"></a>'));
			  $info_box_contents[] = array(array('text' => FORM_LABEL_COUNT_NIGHTS, 'params'=>"class=label valign=top"));
			  $info_box_contents[] = array(array('text' => $count_nights));
			  $info_box_contents[] = array(array('text' => FORM_LABEL_DATE_LEAVE, 'params'=>"class=label valign=top"));
			  $info_box_contents[] = array(array('text' => '<input name="date_leave" id="date_leave" type="text" value="" size="35" disabled style="float: left;">'));
			  $info_box_contents[] = array(array('text' => '<table border=0 width="280"><tr><td class=label width=140>'.FORM_LABEL_ROOM_TYPE.'<td class=label width=140>'.FORM_LABEL_ROOM_COUNT.'<tr><td>'.FORM_SELECT_ROOM_TYPE.'<td><input name="room_count" type="text" value="" size="14"></table>'));
			  //$info_box_contents[] = array(array('text' => '<table border=0 width="280"><tr><td class=label width=140>'.FORM_LABEL_PEOPLE.'<td class=label width=140>'.FORM_LABEL_KIDS.'<tr><td>'.$select_person_old.' <td> '.$select_kids.'<tr><td colspan="2"><table id="kids_ages" name="kids_ages" style="display: none; border: 1px solid #f2f2f2; background: #1a6f8e; color: #FFF" cellpadding=2 cellspacing=0 width="100%" valign="middle"><tr><td align="right">'.FORM_LABEL_YEARS.'<td align="left" width="125"><input type="text" name="kids_age[]" size="14" maxlength="2"></table></table>'));
			  $info_box_contents[] = array(array('text' => '<table border=0 width="280"><tr><td class=label width=140>'.FORM_LABEL_PEOPLE.'<td class=label width=140>'.FORM_LABEL_KIDS.'<tr><td>'.$select_person_old.' <td> '.$select_kids.'<tr><td colspan="2"><table id="kids_ages" name="kids_ages" style="display: none; border: 1px solid #f2f2f2; background: #1a6f8e; color: #FFF" cellpadding=2 cellspacing=0 width="100%" valign="middle"><tr><td align="right">'.FORM_LABEL_BIRTHDAY.'<td align="left" width="125"><input type="text" name="kids_birthday[]" id="kids_birthday[]" size="14" placeholder="YYYY�MM�DD"></table></table>'));
			  $info_box_contents[] = array(array('text' => FORM_LABEL_NAME_FAMILY.' *', 'params'=>"class=label"));
			  $info_box_contents[] = array(array('text' => '<div class="text_field"><input name="name" title="'.FORM_LABEL_NAME.'"  type="text" value="" size="35"></div>'));
			  $info_box_contents[] = array(array('text' => FORM_LABEL_PHONE.' *', 'params'=>"class=label"));
			  $info_box_contents[] = array(array('text' => '<div class="text_field"><input name="phone" title="'.FORM_LABEL_PHONE.'" type="text" value="" size="35"></div>'));
			  $info_box_contents[] = array(array('text' => FORM_LABEL_EMAIL.' *', 'params'=>"class=label"));
			  $info_box_contents[] = array(array('text' => '<div class="text_field"><input name="EMail" title="'.FORM_LABEL_EMAIL.'" type="text" value="" size="35"></div>'));
			  $info_box_contents[] = array(array('text' => FORM_LABEL_ALTERNATIVE, 'params'=>"class=label"));
			  $info_box_contents[] = array(array('text' => '<div class="text_field"><input name="alternative" type="radio" value="true">'.FORM_LABEL_YES.'<input name="alternative" type="radio" value="false">'.FORM_LABEL_NO.'</div>'));
			  $info_box_contents[] = array(array('text' => FORM_LABEL_COMMENTS, 'params'=>"class=label"));
			  $info_box_contents[] = array(array('text' => '<div class="textarea"><textarea name="comments" cols="27" rows="10"></textarea></div>'), 
			  array('text' => sprintf(MESSAGE_FORM_FIELDS_REQUIRED, $Site->EMail) . '<br><img src="http://www.maksoft.net/gen.php?code='.$code.'" align="middle" vspace="5" hspace="5"><div class="label">code:<br><input name="codestr" type="text" id="Eia ca neao?iino" size="35"></div>
			  <a href="javascript: void(0);" class="button_submit" onClick="MM_validateForm(\'EMail\',\'\',\'RisEmail\', \'name\', \'\', \'R\', \'hotel_name\', \'\', \'R\', \'phone\', \'\', \'RisNum\'); if(document.MM_returnValue) document.request_form.submit()">'.FORM_BUTTON_SEND.'</a>', 'params'=>"width=300 align=center valign=top"));
			
			  

	}
	else 
		{
			$query = mysqli_query("SELECT * FROM pages WHERE ParentPage = '".N_CATALOG."' AND SiteID = '".$SiteID."'");
			$other_links = "";
			while($links = mysqli_fetch_array($query))
				$other_links.= "<a href=\"page.php?n=".$links['n']."&SiteID=".$links['SiteID']."\">".$links['Name']."</a> | ";
				
			$info_box_contents[] = array('text' => sprintf(MESSAGE_SELECT_OFFER, $other_links), 'align'=>"center");
		}
new contentBox($info_box_contents);
?>
        <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
		<input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
        <input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$Site->EMail"); ?>"> 
        <input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$n&SiteID=$SiteID"); ?>">
        <input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
		<input name="code" type="hidden" value="<?=$code?>">
		<input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">

<?php
			
			  $info_box_contents = array();
			  $info_box_contents[] = array('text' => "&nbsp;");
  			new infoBoxFooter($info_box_contents, true, true);
?>
</form>
</div>
</center>