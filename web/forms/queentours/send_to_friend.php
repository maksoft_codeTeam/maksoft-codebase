<?php
function sendMail()
{
	global $sender_mail, $EMail, $ToMail, $subject, $view_url;

	$email_to = $ToMail; 
	$email_from = $sender_mail;
	$email_subject = iconv("cp1251", "UTF-8", MESSAGE_SEND_TO_FRIEND_SUBJECT) . $sender_mail;
	$email_message = stripslashes(sprintf(MESSAGE_SEND_TO_FRIEND, $view_url));

	$headers = 'From: '.$EMail."\n" .   'Reply-To: '.$email_from."\n" .  'X-Mailer: PHP/' . phpversion();

	mail($email_to, $email_subject, $email_message, $headers);
	echo "<div class=\"message_normal\">".MESSAGE_SENT."</div>";
}
?>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.title; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' <?=FORM_MESSAGE_VALID_EMAIL?> \n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' <?=FORM_MESSAGE_MUST_CONTAIN_NUMBERS?>\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' <?=FORM_MESSAGE_MUST_CONTAIN_NUMBER_BETWEEN?> '+min+' - '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' <?=FORM_MESSAGE_REQUESTED_FIELD?>\n'; }
  } if (errors) alert('<?=FORM_MESSAGE_ALL_REQUESTED_FIELD?>\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>
<center>
<div class="contact_table">
<?php

if($_POST['send_mail'] == 1) 
	{	
		sendMail();
	}
else
	{
	?>
	<form name="contact_form" method="post" action="/page.php?<?php echo("n=$n&SiteID=$SiteID"); ?>">
	<?php
	
	  if(!isset($id)) $id = $n;
	  $url = "http://www.queentours.bg/page.php?n=".$id."&SiteID=".$SiteID;
	  $send_page = get_page($id);
	  $send_page_name = $send_page->Name;
	  
	  $info_box_contents = array();
	  $info_box_contents[] = array('text' => sprintf(BOX_HEADING_SEND_TO_FRIEND, "<a href=".$url." target=\"_blank\">".$send_page_name."</a>"));
	
	  new infoBoxHeading($info_box_contents, true, true, false);
	
	  $info_box_contents = array();
	 /*
	  $info_box_contents[] = array(array('text' => '������ ��� : *', 'params'=>"class=label"), array('text' => '���������:', 'params'=>"class=label"));
	  $info_box_contents[] = array(array('text' => '<div class="text_field"><input name="sender_name" title="���" type="text" value="'.$user->Name.'" size="35"></div>'), array('text' => '<div class="textarea"><textarea name="message" cols="27" rows="10" disabled>'.sprintf(MESSAGE_SEND_TO_FRIEND, 'www.queentours.bg').'</textarea></div>', 'params'=>"rowspan=4 valign=top width=300"));
	  $info_box_contents[] = array(array('text' => 'e-mail: *', 'params'=>"class=label"));
	  $info_box_contents[] = array(array('text' => '<div class="text_field"><input name="ToMail" title="E-mail" type="text" value="" size="35"></div>'));
	  $info_box_contents[] = array(array('text' => sprintf(MESSAGE_FORM_FIELDS_REQUIRED, $to_mail), 'params'=>"width=300"), array('text'=>''));
	  $info_box_contents[] = array(array('text' => "<a href=\"javascript: void(0)\" class=\"button_submit\" onClick=\"MM_validateForm('ToMail','','RisEmail', 'Name','','R'); if(document.MM_returnValue) document.contact_form.submit()\">���������</a>", 'params'=>" align=center valign=top colspan=2"));
	  */
	  $info_box_contents[] = array(array('text' => FORM_LABEL_SENDER_EMAIL, 'params'=>"class=label"), array('text' => FORM_LABEL_RECEIVER_EMAIL, 'params'=>"class=label"));
	  $info_box_contents[] = array(array('text' => '<div class="text_field"><input name="sender_mail" title="'.FORM_LABEL_SENDER_EMAIL.'" type="text" value="'.$user->Name.'" size="35"></div>'), array('text' => '<div class="text_field"><input name="ToMail" title="'.FORM_LABEL_RECEIVER_EMAIL.'" type="text" value="" size="35"></div>'));
	  $info_box_contents[] = array(array('text' => "<a href=\"javascript: void(0)\" class=\"button_submit\" onClick=\"MM_validateForm('sender_mail','','RisEmail', 'ToMail','','RisEmail'); if(document.MM_returnValue) document.contact_form.submit()\">".FORM_BUTTON_SEND."</a>", 'params'=>" align=center valign=top colspan=2"));
	
	
	  new contentBox($info_box_contents);
	
	  $info_box_contents = array();
	  $info_box_contents[] = array('text' => "&nbsp;");
	
	?>
			<input name="EMail" type="hidden" value="web@queentours.bg"> 
			<input name="view_url" type="hidden" value="<?=$url?>">
			<input name="send_mail" type="hidden" value="1">
	<?php
	  new infoBoxFooter($info_box_contents, true, true);
	?>
	</form>
	<?php
	}
?>
</div>
</center>