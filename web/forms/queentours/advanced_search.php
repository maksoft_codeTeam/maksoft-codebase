<?php
	//load country menu
	if(!isset($country)) $country = N_CATALOG;
	
	$check_page = get_page($country);
	if($check_page->an >0 ) $country = $check_page->an;

	$select_countries = "<select name=\"country\"><option value=\"0\" selected>" . FORM_SELECT_COUNTRY . "</option>";
	$countries_query = mysqli_query("SELECT * FROM pages_to_groups pg, pages p WHERE pg.group_id = '3' AND pg.n = p.n AND p.SiteID = '".$SiteID."' ORDER by p.Name");
	//$countries_query = mysqli_query("SELECT * FROM pages WHERE SiteID = '".$Site->SitesID."' AND ParentPage = ".N_CATALOG." ORDER by Name ASC");
	while($countries = mysqli_fetch_array($countries_query))
		$select_countries.= "<option value=\"".$countries['n']."\">".$countries['Name']."</option>";	

	$select_countries.="</select>";
?>
<center>
<div class="contact_table">
<form name="search_form" method="get">
<input type="hidden" name="n" value="<?=N_SEARCH_RESULTS?>">
<input type="hidden" name="SiteID" value="<?=$SiteID?>">
<input type="hidden" name="do_search" value="true">
<?php
  $info_box_contents = array();
  $info_box_contents[] = array('text' => BOX_HEADING_ADVANCED_SEARCH);

  new infoBoxHeading($info_box_contents, true, true, false);

  $info_box_contents = array();
  $info_box_contents[] = array(array('text' => FORM_LABEL_COUNTRY, 'params'=>"class=label"), array('text' => sprintf(MESSAGE_FORM_FIELDS_REQUIRED, "<a href=\"page.php?n=".N_CONTACTS."\">".BOX_HEADING_CONTACTS."</a>") . "<br><br><br><a href=\"javascript: void(0)\" class=\"button_submit\" onClick=\"document.search_form.submit()\">".FORM_BUTTON_SEARCH."</a>", 'params'=>"width=300 align=center valign=top", 'params'=>"rowspan=8 valign=top", 'align'=>"center"));
  $info_box_contents[] = array(array('text' => '<div class="text_field">'.$select_countries.'</div>'));
  $info_box_contents[] = array(array('text' => FORM_LABEL_RESORT, 'params'=>"class=label"));
  $info_box_contents[] = array(array('text' => '<div class="text_field"><input name="resort" type="text" value="" size="35"></div>'));
  $info_box_contents[] = array(array('text' => FORM_LABEL_HOTEL, 'params'=>"class=label"));
  $info_box_contents[] = array(array('text' => '<div class="text_field"><input name="hotel_name" type="text" value="" size="35"></div>'));
  $info_box_contents[] = array(array('text' => FORM_LABEL_HOTEL_CATEGORY, 'params'=>"class=label"));
  $info_box_contents[] = array(array('text' => '<div class="text_field"><input type="checkbox" name="hotel_category[0]" value="2*"> 2*<input type="checkbox" name="hotel_category[1]" value="3*"> 3*<input type="checkbox" name="hotel_category[2]" value="4*"> 4*<input type="checkbox" name="hotel_category[3]" value="5*"> 5*<input type="checkbox" name="hotel_category[4]" value="6*"> 6*</div>'));

  new contentBox($info_box_contents);

  $info_box_contents = array();
  $info_box_contents[] = array('text' => "&nbsp;");
  new infoBoxFooter($info_box_contents, true, true);
?>
</form>
</div>
</center>