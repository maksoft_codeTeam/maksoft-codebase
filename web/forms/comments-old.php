<link rel="stylesheet" href="web/forms/comments/style.css">

<?php
require_once "/hosting/maksoft/maksoft/global/recaptcha/init.php";

if($o_page->_site['language_id'] != 1){
    $lang = "en";
}

if($_SERVER['REQUEST_METHOD'] === "POST"){
    $recaptcha = new \ReCaptcha\ReCaptcha($secretKey);
    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
    if ($resp->isSuccess()) {
        $passedSpamCheck = True;
    } else {
        $errors = $resp->getErrorCodes();
    }
}
?>
<script language="JavaScript">
<!--
$j(document).ready(function(){
	$j('#send_comments').click(function(){
	
		var comment_n = $j('#comment_n').val();
		var comment_author = $j('#comment_author').val();
		var comment_author_email = $j('#comment_author_email').val();
		var comment_author_url = $j('#comment_author_url').val();
		var comment_author_ip = $j('#comment_author_ip').val();
		var comment_date = $j('#comment_date').val();
		var comment_text = $j('#comment_text').val();
		var comment_parent = $j('#comment_parent').val();
		var comment_user_id = $j('#comment_user_id').val();
		var comment_status = $j('#comment_status').val();
		
		//alert(comment_author_ip);
		//$j('#list_comments').load("web/admin/add_comments.php", "comment_n="+comment_n+"&comment_author="+comment_author+"&comment_author_email="+comment_author_email+"&comment_author_url="+comment_author_url+"&comment_author_ip="+comment_author_ip+"&comment_date="+comment_date+"&comment_text="+comment_text+"&comment_parent="+comment_parent+"&comment_user_id="+comment_user_id+"&comment_status="+comment_status);
		//alert(isValidEmailAddress(comment_author_email));
		
		if(comment_author_email != "" && comment_text != "")
			if(isValidEmailAddress(comment_author_email))
				$j('#form_comments').submit();
			else alert("���� �������� ������� e-mail ����� !");
		else alert("�������� ���������� � * �� ������������ !");
		
	})
	
	$j('#show_comments_form').click(function(){
		$j('#form_comments').toggle();
		$j('#comment_parent').val(0);
	})
	
	$j('#show_comments_form2').click(function(){
		$j('#form_comments').toggle();
		$j('#comment_parent').val(0);
	})
});

//-->
</script>

	<?php
		/*if(isset($_POST["remote_address"])){
			include_once("../../lib/lib_page.php");
		}*/

		// verify user comment   -1 regected   0-pending approval    1-approved
		function comment_verify($comment_text) {
		 $add_comment = 0;
		 $cp_query = mysqli_query("SELECT * FROM comments_prohibited WHERE language_id = '1' "); 
		 while ($cp_row=mysqli_fetch_array($cp_query)) {
			  if(eregi($cp_row['cp_text']." " , $comment_text) OR eregi(" ".$cp_row['cp_text'] , $comment_text)  ) {
			  	//$add_comment = $cp_row['cp_level']; 
				$add_comment  = -1; 
			  }
		 }
		 return $add_comment; 
		}
	?>
	<?php
	$add_comment = $_POST['add_comment'];

	if($add_comment == 1)
		{	
			if(comment_verify($comment_text)>=0) {
				if(check_code_str($code, $codestr, $passedSpamCheck)) { 
						//include "web/admin/add_comments.php";
						$comment_author_url = str_replace("http://", "", $comment_author_url);
						$num_rows = 0;
						$comment_text = strip_tags($comment_text);
						if($comment_subject == "")
							$comment_subject = "�������� ���: ".$o_page->get_pName();
						$add_sql = "INSERT INTO comments SET comment_n = '".$comment_n."', comment_subject = '".$comment_subject."', comment_author = '".$comment_author."', comment_author_email = '".$comment_author_email."', comment_author_url = '".$comment_author_url."', comment_author_ip = '".$comment_author_ip."', comment_date = now(), comment_text = '".$comment_text."', comment_parent = '".$comment_parent."', comment_user_id = '".$comment_user_id."', comment_status = 0";
						mysqli_query($add_sql);
						mk_output_message("warning", "����� �������� � �������� �������!");
                } else{
                        mk_output_message("error", "������ ��� �� ���������. ����� �������� �� � ������� !<br><br><a href=\"#add_comments\" id=\"show_comments_form\">������ ������</a>");
                }
			} else {
						mk_output_message("error", "������ ��� ����������� ���� � ������ �������� �� � �������. ���� ������ �� ��������� ���� � �� ���������� �� ��������!");
			}
		}
		
	$commented = NULL;
	if(isset($parent_comment))
		$commented = $o_page->get_comment($parent_comment);
	?>
	<div id="list_comments">
	<h3 class="head_text"><i class="fa fa-comments-o colorfa" aria-hidden="true"></i> ���������</h3>
	<?php
		//get all page comments or single comment
		if(isset($comment_id))
			$comments[0] = $o_page->get_comment($comment_id, "comment_n=".$o_page->n);
		else
		if(isset($parent_comment))
			$comments[0] = $o_page->get_comment($parent_comment, "comment_n=".$o_page->n);
		else
			$comments = $o_page->get_pComments($o_page->n, "comment_parent=0");
		
		if(count($comments) == 0)
			{
				mk_output_message("normal", "���� �������� ���������. ������ �� �������� ������ ��������.");
			}
			
		$del_str = "&amp;"; 
		if(($o_page->_user['AccessLevel'] == 0) && ($o_page->_site['url_type'] > 0)) $del_str = "?"; 
			
		for($i=0; $i<count($comments); $i++)
			{
				$comments_author = $comments[$i]['comment_author'];
				if($comments[$i]['comment_author_url'] != "")
					$comments_author = "<a href=\"http://".$comments[$i]['comment_author_url']."\" target=\"_blank\">".$comments[$i]['comment_author']."</a>";
				?>
				<div class="comment">
					<a name="comment_<?=$comments[$i]['comment_id']?>"></a>
					<div class="bubble"></div>
                    <?php echo ($comments[$i]['comment_subject'] != "") ? "<h4><a href=\"".$o_page->get_pLink().$del_str."comment_id=".$comments[$i]['comment_id']."\">".$comments[$i]['comment_subject']."</a></h4>":""; ?>
					<span class="text"><?=$comments[$i]['comment_text']?>&nbsp;<a href="<?=$o_page->get_pLink().$del_str?>parent_comment=<?=$comments[$i]['comment_id']?>#reply" class="quote" id="quote_comment">(�������)</a></span>
					<?php
						$sub_comments = $o_page->get_pComments($o_page->n, "comment_parent=".$comments[$i]['comment_id'], "comment_date ASC");
						if(count($sub_comments) > 0)
							{
								echo "<div class=\"subcomments\"><ol>";
								for($j=0; $j<count($sub_comments); $j++)
									{
										if($sub_comments[$j]['comment_author_url'] != "")
											$sub_comments_author = "<a href=\"http://".$sub_comments[$j]['comment_author_url']."\" target=\"_blank\" name=\"comment_".$sub_comments[$j]['comment_id']."\">".$sub_comments[$j]['comment_author']."</a>";
										else
											$sub_comments_author = $sub_comments[$j]['comment_author'];

										echo "<li><span class=\"date\">".$sub_comments[$j]['comment_date']."</span>, <span class=\"author\">".$sub_comments_author."</span><br><em>".$sub_comments[$j]['comment_text']."</em>";
									}
								echo "</ol></div>";
							}

					?>
					<div style="text-align: right;">
						<div class="user"></div>
						<span class="author"><?=$comments_author?></span>, 
						<span class="date"><?=$comments[$i]['comment_date']?></span>
					</div>
				</div>
				<?
				/*
				echo "<div class=\"comment\"><div class=\"bubble\"></div><span class=\"date\">".$comments[$i]['comment_date']."</span>, ";
				if($comments[$i]['comment_author_url'] != "")
					echo "<a href=\"http://".$comments[$i]['comment_author_url']."\" target=\"_blank\" name=\"comment_".$comments[$i]['comment_id']."\">".$comments[$i]['comment_author']."</a>";
				else 
					echo $comments[$i]['comment_author'];
				
				echo " (<a href=\"page.php?n=".$n."&amp;SiteID=".$SiteID."&amp;parent_comment=".$comments[$i]['comment_id']."\">����������</a>) <br><span class=\"text\">".$comments[$i]['comment_text']."</span>";
				$sub_comments = $o_page->get_pComments($o_page->n, "comment_parent=".$comments[$i]['comment_id']);
				if(count($sub_comments) > 0)
					{
						echo "<ol>";
						for($j=0; $j<count($sub_comments); $j++)
							echo "<li><span class=\"date\">".$sub_comments[$j]['comment_date']."</span>, <a href=\"http://".$sub_comments[$j]['comment_author_url']."\" target=\"_blank\" name=\"comment_".$sub_comments[$j]['comment_id']."\">".$sub_comments[$j]['comment_author']."</a><br><em>".$sub_comments[$j]['comment_text']."</em>";
						echo "</ol>";
					}
				echo "</div>";
				*/
			}
	?>
	</div>
	<?php
		if($user->ID >= 0)
		{	
			if(empty($comment_author)) $comment_author = $user->Name;
			if(empty($comment_author_email)) $comment_author_email = $user->EMail;
		if(!isset($parent_comment)) { ?> 
			<a href="javascript: void(0)" id="show_comments_form" name="add_comments" class="add_comment">������ ��������</a>
		<? } else{
			?> <a name="reply"></a> <?
        }
	?>
	<form name="comments" method="post" id="form_comments" style="display: <?=(($parent_comment > 0)? "":"none;")?>">
	<table class="border_table" border="0" width="100%" cellspacing="1" cellpadding="2">
		<textarea id="myTextarea" style="width: 100%; height: 200px;"></textarea>
<!--		<thead>
			<tr><th colspan="2"><?php if($commented) { echo "�������� �� &raquo; " . $commented['comment_author'] ." ��: " . $commented['comment_date']; } else echo "������ ��������"; ?></b>
		</thead>
		<tbody>
			<tr>
			  <td align="center"><input name="comment_subject" type="text" id="comment_subject" placeholder="����" size="50" value="<?=$comment_subject?>" />
		  <tr>
		    <td align="center" width="80%"><input name="comment_author" type="text" id="comment_author" size="50" placeholder="���" value="<?=$comment_author?>">
			<tr>
			  <td align="center"><input name="comment_author_email" type="text" id="comment_author_email" size="50" placeholder="E-mail *" value="<?=$comment_author_email?>">
			<tr>
				<td align="center"><input name="comment_author_url" type="text" id="comment_author_url" placeholder="URL" size="50" value="<?=$comment_author_url?>">
			<tr>
				<td align="center"><textarea name="comment_text" cols="38" rows="5" placeholder="�������� *" id="textarea"><?=$comment_text?></textarea>
			<tr>
                <?php if($reCaptchaKey and $secretKey != $secret){ ?>
                <td align="center">
                    <div class="g-recaptcha" data-sitekey="<?php echo $secret; ?>"></div>
                    <script type="text/javascript"
                             src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>">
                    </script>
				</td>
                <?php } else { ?>
				<td>
				<?php
					$code = rand(0, 65535);   
					echo("<img src=\"http://www.maksoft.net/gen.php?code=$code\" align=\"middle\" vspace=5 hspace=5><input type=\"hidden\" name=\"code\" value=\"$code\">"); 
				?>
        	        <br><input name="codestr" type="text" id="��� �� ���������" size="10">
            <?php } ?>
			<tr>
				<td colspan="2" align="center">
					<input type="hidden" name="comment_n" id="comment_n" value="<?=$n?>">
					<input type="hidden" name="comment_author_ip" id="comment_author_ip" value="<?=$ip?>">
					<input type="hidden" name="comment_date" id="comment_date" value="<?=date('Y-m-d H:i:s')?>">
					<input type="hidden" name="comment_parent" id="comment_parent" value="<?=$parent_comment?>">
					<input type="hidden" name="comment_user_id" id="comment_user_id" value="<?=$user->ID?>">
					<input type="hidden" name="comment_status" id="comment_status" value="1">
					<input type="hidden" name="add_comment" value="1">
					<input type="button" value="������� ��������" id="send_comments" style="margin-bottom: 20px;">

		</tbody>-->
	</table>
</form>
<div class="wmd-preview"></div>
<?php
	}
?>

<script type="text/javascript" src="web/forms/comments/wmd/wmd.js"></script>
