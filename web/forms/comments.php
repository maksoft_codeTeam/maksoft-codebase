<?php
require_once "modules/vendor/autoload.php";
require_once "lib/Database.class.php";
require_once "forms/comments/InsertNewComment.php";
require_once "global/recaptcha/init.php";
#var_dump($_SESSION['oauth']);
$db = new Database();
$db->exec("SET NAMES UTF8");
$gate = new Maksoft\Gateway\Gateway($db);
if($oauth_data = $gate->auth()->isLoggedWithFacebook()){
    echo 'successfuly logged with facebook';
}

$post_comment = new InsertNewComment($gate->page(), $o_page->_page['n'], $_POST);

$me = false;
if(isset($_SESSION['oauth']['me'])){
    $me = $_SESSION['oauth']['me'];
}

$comments = $o_page->get_pComments($o_page->_page['n']);

function get_me_fields(){
     $token = "EAACLALmB3WABANYrFrzXDNr80SqfH4XgZAJ0DPVjdC4bBvTMYIBoIRWM6igmFGnqalER9gqZAf17VSEwikENDDdNnIQzbJ2BnLZC1nPGJL1ZBq8dCBF75xfLDZBXycDQcbJ4qfgri4hZATQGdqhZCXZCbs0IxJxK1qjeZAhwwM3YU9369enlNo1FO0PpzVHE4sqsZD";
     $url = "https://graph.facebook.com/v2.8/me?fields=id%2Cname%2Cpicture%2Cemail&access_token=".$token;
     $response = file_get_contents($url);
     return json_decode($response);
}


switch($o_page->_site['language_id']){
    case 1:
        $lang="bg";
        break;
    default:
        $lang="en";
}

if($_SERVER['REQUEST_METHOD'] === "POST"){
    $recaptcha = new \ReCaptcha\ReCaptcha($secretKey);
    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
    if ($resp->isSuccess()) {
        switch($action=$_POST['action']){
            case $post_comment->action->value:
                try{
                $post_comment->is_valid();
                $post_comment->save();
                } catch (\Exception $e){
                    $errors = array();
                    foreach($post_comment as $field){
                        if($errors = $field->get_errors()){
                            $errors[$field->name] = $errors;
                        }
                    }
                }
                break;
            default:
                break;
        }
    } else {
        $errors = $resp->getErrorCodes();
    }
}


?>

<link rel="stylesheet" href="web/forms/comments/style.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.min.js" integrity="sha256-1O3BtOwnPyyRzOszK6P+gqaRoXHV6JXj8HkjZmPYhCI=" crossorigin="anonymous"></script>
<!-- JavaScript -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.9.0/alertify.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>

<script type="text/javascript" src="web/forms/comments/wmd/wmd.js"></script>

<script src="web/assets/js/notify.min.js"></script>
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>&render=explicit"> </script>
<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/alertify.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/themes/bootstrap.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/themes/bootstrap.rtl.min.css"/>

<?php echo $fb->script;?>


<div id="list_comments">
    <div id="status"></div>
    <h3 class="head_text">���������</h3>

    <?php if($o_page->_user['AccessLevel'] > 0) { ?>
        <button onclick="showForm(this, {parent: 0, author: '<?=$o_page->_user['Name']?>'})" class="send-button">��� ��������</button>
    <?php } elseif($oauth_data) { ?>
        <button onclick="showForm(this, {parent: 0, author: '<?php echo $oauth_data->name;?>'})" class="send-button">��� ��������</button>
    <?php } else { ?>
        <button onclick="showForm(this, {parent: 0, author: '����'})" class="send-button">��� ��������</button>
    <?php } ?>

    <?php 
        $sorted_comments = array(
            'parent' => array(),
            'child'  => array(),            
        );
        foreach($comments as $comment){
            if($comment['comment_parent'] != 0){
                if(!isset($sorted_comments['child'][$comment['comment_parent']])){
                    $sorted_comments['child'][$comment['comment_parent']] = array();
                }

                $sorted_comments['child'][$comment['comment_parent']][] = $comment;
                continue;
            }

            $sorted_comments['parent'][] = $comment;
        }
    ?>
    <ol class="commentlist group">
        <?php $parent_comment_id = 0; ?>
        <?php foreach($sorted_comments['parent'] as $comment){  ?>
            <li id="comment-<?php echo $comment['comment_id'];?>" class="comment even thread-even depth-1">
              <div class="grid group comment-wrap">
                <div class="comment-avatar grid-1-5">
                <img src="<?php echo $comment['comment_image'];?>" width="50" height="50" class="lazyload-gravatar" alt="<?php echo $comment['comment_author'];?>">
                </div>
                <div class="comment-body group grid-4-5">
                  <div class="comment-author-wrap vcard">
                    <div class="comment-author"><?php echo $comment['comment_author'];?></div> 
                    <div class="comment-time">
                        <time><?php echo $comment['comment_date'];?></time>
                    </div>
                  </div>
                  <div class="comment-content">
                    <?php echo  strip_tags($comment['comment_text'], '<ul><li><div><ol><h1><h2><h3><h4><pre><code><p><em><strong><blockquote>');?>
                    <div class="reply">
                        <button class="comment-reply-link" onclick="showForm(this, {parent: '<?php echo $comment['comment_id']?>', author: '<?php echo $comment['comment_author'];?>' })">�������� <i class="fa fa-long-arrow-down" aria-hidden="true"></i></button>            
                    </div>
                  </div>
                </div>
              </div>
                <?php if(isset($sorted_comments['child'][$comment['comment_id']])){ ?>
                    <?php foreach($sorted_comments['child'][$comment['comment_id']] as $c_comment){?>
                        <ul class="children">
                            <li class="comment odd alt depth-2" id="comment-<?php echo $c_comment['comment_id']?>">
                                <div class="grid group comment-wrap" id="comment-<?php echo $c_comment['comment_id']?>">
                                    <div class="comment-avatar grid-1-5">
                                    <img src="<?php echo $c_comment['comment_image'];?>" width="50" height="50" class="lazyload-gravatar" alt="<?php echo $c_comment['comment_author']?>">
                                    </div>

                                    <div class="comment-body group grid-4-5">

                                    <div class="comment-author-wrap vcard">
                                    <div class="comment-author"><?php echo $c_comment['comment_author'];?></div>
                                            <div class="comment-time">
                                            <time><?php echo $c_comment['comment_date'];?></time>
                                            </div>
                                        </div>
                                        <div class="comment-content">
                                        <?php echo  strip_tags($c_comment['comment_text'], '<ul><li><div><ol><h1><h2><h3><h4><pre><code><p><em><strong><blockquote>');?>
                                            <div class="reply"> </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    <?php } ?>
                <?php } ?>
            </li>
        <?php  } ?>
    </ol>
</div>

<script id="comment-form" type="text/x-handlebars-template">
    <form class="form_comment" id="form_comment">
        <div id="summary"></div>
        <h4 class="commented"><em></em></h4>
        <textarea id="textarea" name="comment_text" placeholder="��������" minlength="2" style="width: 100%; height: 200px;" required><?=$comment_text?></textarea>
        <div id="textarea-error"></div>
        <div class="wmd-preview" id="textarea-formated"></div>
        <div class="login-or-guest"> 
            <div class="row">
            <?php $guest_title = "���������� ���� ����"; ?>
            <?php if($o_page->_user['AccessLevel'] == 0){ ?>
                <div class="col-md-6">
                <?php if ($oauth_data) { ?>
                    <input name="comment_author" type="hidden" id="comment_author" value="<?=iconv('utf8', 'cp1251', $oauth_data->name);?>">
                    <input name="comment_author_email" type="hidden" id="comment_author_email" value="<?=$oauth_data->email;?>">
                    <input name="comment_author_pic" type="hidden" id="comment_author_pic" value="<?=$oauth_data->picture;?>">
                ��� ��� ������ � facebook
                <br>
                <div if="fb-login" class="preferred-login facebook-login">
                    <a href="/fb.php?c=logout"><i style="font-size: 1.3em;" class="fa fa-facebook-official" aria-hidden="true"></i>����� �� ������� ��</a>
                </div>
                <?php } else { ?>
                <h4>����� ��</h4>
                    <div class="new-login-left">
                        <!--
                        <div class="preferred-login google-login">
                            <p><span class="icon"></span><span>���� � Google</span></p>
                        </div>
                        -->
                        <div if="fb-login" class="preferred-login facebook-login">
                            <a href="/fb.php"><i style="font-size: 1.3em;" class="fa fa-facebook-official" aria-hidden="true"></i> ���� � Facebook </a>
                        </div>
                    </div>
                <?php } ?>
                </div>
                <?php if(!$oauth_data){ ?>
                <div class="col-md-6">
                    <h4><?=$guest_title;?></h4>
                    <div class="new-login-left">
                        <div class="guest-login">
                            <input name="comment_author" type="text" id="comment_author" placeholder="��� *" value="<?=$o_page->_user['Name'];?>" required>{{name}}
                            <div id="comment_author-error"></div>
                            <font style="font-size:2em">&nbsp;</font>
                            <input name="comment_author_email" type="email" id="comment_author_email" placeholder="E-mail *" value="<?=$o_page->_user['EMail'];?>" required>
                            <div id="comment_author-email-error"></div>
                        </div>
                    </div>
                </div>  
                <?php } ?>
             <?php } else { ?>
                <div class="col-md-6">
                    <h4>���������� ����: <?=$o_page->_user['Name']?></h4>
                    <div class="new-login-left">
                        <div class="guest-login">
                        <input name="comment_author" type="hidden" id="comment_author" placeholder="��� *" value="<?=$o_page->_user['Name'];?>" required>{{name}}
                        </div>
                        <div class="guest-login">
                        <input name="comment_author_email" type="hidden" id="comment_author_email" placeholder="E-mail *" value="<?=$o_page->_user['EMail'];?>" required>
                        </div>
                    </div>
                </div>  
             <?php } ?>
            </div>
        <hr>
        <div id='tete' class="imnotrobot">
            <div class="row">
                <div id="captcha"></div>
                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
            </div>
    </form>
</script>

<script>
var $ = jQuery;

var logged = false;

function changeStatus(is_logged){
    logged = is_logged;
}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};

function printErrorMsg(selector, msg){
    $("#"+selector.attr('id') +'-error').html('<div style="border: 1px solid;margin: 10px 0px;padding:15px 10px 15px 50px;background-repeat: no-repeat;'+
'background-position: 10px center;color: #D8000C;background-color: #FFBABA;">'+
'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>'+msg+'</div>');
}

function is_valid(field, msg){
    //isset field
  if(field.length==0){
        printErrorMsg(field, msg);
        return false;
  }
  
  if(field.val()=="" || field.val().length < 5){
        printErrorMsg(field, msg);
        return false;
  }
  
  $("#"+field.attr('id')+'-error').html('');
  return true;
}


function is_valid_email(field, msg){
    if(field.length==0){
        printErrorMsg(field, msg);
        return false;
    }
    if(isValidEmailAddress(field.val())){
        $("#"+field.attr('id')+'-error').html('');
        return true;
    }
    printErrorMsg(field, msg);
    return false;
}

function validate_form() {
    var textarea = $(document.forms["form_comment"]["textarea"]);
    var name = $(document.forms["form_comment"]["comment_author"]);
    var email = $(document.forms["form_comment"]["comment_author_email"]);
    var results = [];
    results.push(is_valid(textarea, "���������� ������ �� ������� ������� 5 �������."));
    results.push(is_valid(name, "�����, ����� ��� ������ � ��������� �����."));
    results.push(is_valid_email(email, "����� �������, ����� ��� ������ � ���������."));
    return results.every(function(el){
      return el===true;
    });
};

function get(url) {
  // Return a new promise.
  return new Promise(function(resolve, reject) {
    // Do the usual XHR stuff
    var req = new XMLHttpRequest();
    req.open('GET', url);

    req.onload = function() {
      // This is called even on 404 etc
      // so check the status
      if (req.status == 200) {
        // Resolve the promise with the response text
        resolve(req.response);
      }
      else {
        // Otherwise reject with the status text
        // which will hopefully be a meaningful error
        reject(Error(req.statusText));
      }
    };

    // Handle network errors
    req.onerror = function() {
      reject(Error("Network Error"));
    };

    // Make the request
    req.send();
  });
}

wmd_options = { autostart: false };

function createRecaptcha() {
    grecaptcha.render("captcha", {sitekey: "<?=$secret?>", theme: "light", callback: addComment});
}


var recaptcha;

function addComment(recapResponse){
    recaptcha = recapResponse;
}

function getCaptcha(){
    return recaptcha;
}


function showForm(e, data){
    var data                = data;
    var pic                 = "http://maksoft.net/web/forms/comments/wmd/images/avatar.jpg";
    var user_id             = 0;
    var commentFormTemplate = $("#comment-form").html();
    var postComment         = Handlebars.compile(commentFormTemplate);
    var captcha             = recaptcha;
    var alertTitle              = "�������� �� ��������...";

    if(data.parent > 0){
        alertTitle = "�������� �� " + data.author;
    }

    if($("comment_author_pic").length > 0){
        pic = $("comment_author_pic").val();
    }

    if($("comment_author_id").length > 0){
        user_id = $("comment_author_id").val();
    }

    data['pic'] = pic;
    data['id'] = user_id;
    data['name'] = $("#comment_author").val();
    data['email'] = $("#comment_author_email").val();

    alertify.confirm(alertTitle, postComment(data), function(el){
        if(!validate_form()){
            alertify.error("����� �� �������� ��� ������� � ���������.");
            return false;
        }
        if(grecaptcha.getResponse() != undefined){
            saveComment(data.name, data.email, data.parent, data.pic, data.id, 1, getCaptcha());
            alertify.success("������ �������� � ������� �������");
            setTimeout(function(){
                window.location.reload();
            }, 1000);
        } else {
          alertify.error("����, ����������, �� �� ��� �����");
          return false;
        }
    }, 
    function(){ $("#captcha").html("");
        grecaptcha.reset();
    })
    .set({
       onshow: createRecaptcha(),
    }); 
}

function validateTextArea(){
    return $("#textarea-formated").html().length > 0;
}


function saveComment(author, email, parentComm, image, user_id, stat=0, recaptcha, subject=''){
    $.ajax({
        url: '<?php echo $_SERVER["REQUEST_URI"]?>',
        type: 'POST',
        dataType: 'json',
        data: {
            author: $("#comment_author").val(),
            email: $("#comment_author_email").val(),
            user_id: user_id,
            subject: subject,
            text: $('#textarea-formated').html(),
            'parent': parentComm,
            image: image,
            'g-recaptcha-response': recaptcha,
            'status': stat,
            action: "<?php echo $post_comment->action->value;?>",
        }
    })
    .done(function() {

    })
    .fail(function() {
    })
    .always(function() {
    });

}

</script>
