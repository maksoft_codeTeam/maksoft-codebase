<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<style>
#column_right {
	display: none;
}
#pageContent {
	width: 100%;
}
legend {
	font-size: 25px;
	color: #c4161c;
	border-bottom: 1px solid #c4161c;
	padding: 10px 0;
}
.form-group { margin-bottom: 8px;}
</style>
<a name="message-sent"></a>
<?php
	if($sent == 1)
		{
			?>
			<div class="row">
			<div class="col-sm-12">
			<div class="alert alert-info alert-dismissible fade in" role="alert">
			  <?=MESSAGE_CAREERS_SENT?>
			</div>
			</div>
			</div>
			<?
			
		}
?>
<div id="careers-form" class="row">
	<div class="col-md-12">
		<form class="form-vertical well" action="form-cms.php" method="post" enctype="multipart/form-data">
		
			<fieldset>
				<!-- <legend><i class="fa fa-angle-down"></i> <?=FORM_TITLE?></legend>//-->

				  <div class="form-group col-sm-6">
					<label for="name" class="control-label">* <?=LABEL_NAME?></label>
					<div class="">
					  <input type="text" class="form-control" id="name" placeholder="<?=LABEL_NAME?>" name="name" required>
					</div>
				  </div>
				  
				  <div class="form-group col-sm-6">
					<label for="surname" class="control-label"><?=LABEL_SURNAME?></label>
					<div class="">
					  <input type="text" class="form-control" id="surname" placeholder="<?=LABEL_SURNAME?>" name="surname">
					</div>
				  </div>

				  <div class="form-group col-sm-6">
					<label for="phone" class="control-label">* <?=LABEL_PHONE?></label>
					<div class="">
					  <input type="text" class="form-control" id="phone" placeholder="<?=LABEL_PHONE?>" name="phone">
					</div>
				  </div>

				  <div class="form-group col-sm-6">
					<label for="email" class="control-label">* <?=LABEL_EMAIL?></label>
					<div class="">
					  <input type="email" class="form-control" id="email" placeholder="<?=LABEL_EMAIL?>" name="EMail" required>
					</div>
				  </div>

				  <div class="form-group col-sm-6">
					<label for="city" class="control-label"><?=LABEL_CITY?></label>
					<div class="">
					  <input type="text" class="form-control" id="city" placeholder="<?=LABEL_CITY?>" name="city">
					</div>
				  </div>

				  <div class="form-group col-sm-6">
					<label for="upload-cv" class="control-label"><?=LABEL_UPLOAD_CV?></label>
					<div class="">
					  <input type="file" class="form-control" name="user-cv" id="upload-cv" placeholder="<?=LABEL_UPLOAD_CV?>">
					</div>
				  </div>		  
						

				<div class="form-group col-sm-6">
					<div class="">
					  <div class="checkbox">
						<label>
						  <input type="checkbox"> <?=LABEL_COPY_TEXT?>
						</label>
					  </div>
					</div>
				</div>	
				
				<div class="col-sm-6">
					<div class="row">
					<div class="col-sm-6">
					<label class="control-label"></label>
					<?php
						$code = rand(0, 65535);   

						echo("
						<img src=\"http://www.maksoft.net/gen.php?code=$code\" align=\"middle\" class=\"img-responsive\">
						<input type=\"hidden\" name=\"code\" value=\"$code\">
						"); 

						if (strlen($ToMail)<5) $ToMail = $Site->EMail; 
						if (strlen($ToMail)<5) $ToMail = $o_page->_site['EMail']; 
					?>
					</div>
					<div class="col-sm-6">
						<label for="code" class="control-label"><?=LABEL_CODE?></label>
						<input name="codestr" type="text" id="code" class="form-control">
					</div>
					</div>
				</div>
				<div class="form-group col-sm-12">
					<hr>
					<div class="">
					  <button type="submit" class="btn btn-danger"><?=LABEL_APPLY?></button>
					</div>
				</div>
				
			</fieldset>

			<input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
			<input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
			<input name="ToMail" type="hidden" id="ToMail" value="careers@bestamed.com"> 
			<input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$n&SiteID=$SiteID&sent=1#message-sent"); ?>">
			<input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">			
		</form>	
	</div>
</div>