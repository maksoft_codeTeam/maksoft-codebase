<?php

// Switch off the cache
ini_set('soap.wsdl_cache',0);
ini_set('soap.wsdl_cache_limit',0);
ini_set('soap.wsdl_cache_ttl',0);
ini_set('soap.wsdl_cache_enabled',0);


// Include the client soap to communicate with the webservice methods
require_once("StrickerPioneerSoapClient.php");

// Start session
session_start();

/*--------------------------------------------------------------------------------------------------------------*/

// Create a new instance for the webservice client
$webservice = new StrickerPioneerSoapClient();

// Start validation false
$valid = false;

// Start language arrays
$lang = array("PT" => "por", "EN" => "eng", "ES" => "spa", "NL" => "nld", "PL" => "pol");

// Ask for authentication
try  {
	// if no session token exists, ask for a new token
	if(!isset($_SESSION["token"])) {

		// Authorize the client according to credentials and save token in session
		$_SESSION["token"] = $webservice->AuthorizeClient('JkNdxQFGvxWU3LZd');

		// if session token is returned, session is valid
		if($_SESSION["token"] != "") {
			$valid = true;
		}
	}
	else {
		// Validate authenticity of session 
		$valid = $webservice->ValidateSession($_SESSION["token"]);

		// if session is invalid, ask for a new session token
		if(!$valid) {
			$_SESSION["token"] = $webservice->AuthorizeClient('123456');

			// if session token is returned, session is valid
			if($_SESSION["token"] != "") {
				$valid = true;
			}
		}
	}
}
catch (Exception $ex) {

	// if a type 2 error occurs, the session is invalid
	if($ex->faultcode == 2) unset($_SESSION["token"]);

	// Show error returned by the webservice
	print_r($ex);
}

// if session is valid, make the request to the webservice
if($valid == true) {

	/**
	 * Data is returned in JSON by default
	 * the function json_decode is necessary
	 **/

	$site = "stricker";


	/* Obtain list of catalogues
	 * Parameters:
	 * 	$toek: token used to authenticate the client
	 *  $site: identification of the website being accessed
	 *  $lang: language requested by the client
	 */
	$data = $webservice->client_getProducts($_SESSION["token"],$site,$lang["EN"],"cat20131","1","0100",null,null);
	print_r(json_decode($data));

	$data = $webservice->get_productPhoto($_SESSION["token"],"91736");
	print_r(json_decode($data));

	/* Obtain list of all products in a catalogue i.e hidea
	 *  Parameters:
	 *  $token: token used to authenticate the client
	 *  $site: identification of the website being accessed
 	 *  $lang: language requested by the client
	 *  $catalogue_id: identification of the catalogue
	 *  $section_id: identification of the section (empty)
	 *  $fam_id: identification of the family (empty)
	 *  $page: page to present (optional parameter)
	 *  $lines: number of lines per page (optional parameter)
	 */
	$data = $webservice->client_getProducts($_SESSION["token"],$site,$lang["EN"],"cat20131",null,null,null,null);
	print_r(json_decode($data));

	/* Obtain list of orders
	 * 	Parameters:
	 *  $token: token used to authenticate the client
	 */
	
	
	/* Obtain list of addresses
	 * 	Parameters:
	 *  $token: token used to authenticate the client
	 */

	//Submit new address
	$address["name"] = "Magicbrain";
	$address["addr"] = "Rua de Saragoça";
	$address["zipcode"] = "3000";
	$address["zipcodeext"] = "300";
	$address["zipcodename"] = "Coimbra";
	$address["country"] = "Portugal";
	$address["country_iso"] = "EN";
	$address["phone"] = "239123123";
	$address["default"] = true;
	$address["obs"] = "teste";

	/* Obtain list of addresses
	 * 	Parameters:
	 *   $token: token used to authenticate the client
	 *   $address: array with all address data
	 *   $client_address_id: identification of current address (to update)
	 */

	// Delivery address
	$destination = array("Rua de Saragoça","3000","300","239123123", "Portugal", "EN");

	// order items: reference and quantity
	$order_itens["91008.3"] = 10;
	$order_itens["91008.9"] = 20;

	/* Submit order
	 * 	Parameters:
	 *   $token: token used to authenticate the client
	 *   $order_itens: array of products to order (reference => quantity)
	 *   $destination: array with all address data (address, zip code, ext. zip code, contact, country, country code)
	 *   $obs: observations
	 */
	print_r(json_decode($data));

	 /* Get a list of all products from a catalog with prices i.e Hidea margin
		* Parameters:
		* $ Token: token used to authenticate the client
		* $ Site: Idenfication website that is accessing
		  * $ Lang: language requested by the client
		* $ Catalogue_id: identification of the catalog
		* $ Section_id: identification section (empty)
		* $ Fam_id: Family ID (empty)
		* $ Margin: price margin applied
		* $ Page: page displaying (optional parameter)
		* $ Lines: number of lines per page (optional parameter)
	*/
	$margin = 10;
	print_r(json_decode($data));
	
	/* Get list of all products ie Hidea a catalog with prices and margins double those
		* Parameters:
		* $ Token: token used to authenticate the client
		* $ Site: Idenfication website that is accessing
		  * $ Lang: language requested by the client
		* $ Catalogue_id: identification of the catalog
		* $ Section_id: identification section (empty)
		* $ Fam_id: Family ID (empty)
		* $ Margin: price margin applied
		* $ Page: page displaying (optional parameter)
		* $ Lines: number of lines per page (optional parameter)
	*/
	$margin = 10;
	print_r(json_decode($data));

}


?>
