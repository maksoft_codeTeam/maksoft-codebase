<?php
	if($user->AccessLevel >= $row->SecLevel)
	//if($user->ID == 196)
	{
?>
<script language="JavaScript" type="text/JavaScript">
<!--

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.title; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' �������� ������� e-mail �����! \n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' � ������������ ����!\n'; }
  } if (errors) alert('������������ ��� * ������ �� ������������! ���� ��������� ��:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>
<?php
	require_once "lib/lib_page.php";
	
	$o_user = new user($user->username, $user->pass);
	$o_site = new site();
	$o_page = new page();
	
	//carextras
	$carextras = array(
	"��������",
	"�����������",
	"����� �����",
	"��.������",
	"��.��������",
	"��.�������",
	"���������� �� �������",
	"�������",
	"����� ������",
	"���������� ������",
	"4�4",
	"ABS",
	"ESP",
	"Airbag",
	"��������� ������",
	"��������� ������",
	"������� �������",
	"����������",
	"������",
	"�����������",
	"�����. ����������",
	"�����������",
	"��������� / �����������",
	"���������",
	"����� ����������",
	"������� ��������",
	"�������� ������",
	"��������",
	"������������ �������",
	"����� �����",
	"������",
	"��������� ������"
	);
	
	$car_brands = array(
	"A" => array("Acura", "Aixam", "Alfa Romeo", "Alpina", "Aro", "Asia Motors", "Aston Martin", "Audi"),
	"B" => array("Bentley", "BMW", "Brilliance", "Bugatti", "Buick"),
	"C" => array("Cadillac", "Chevrolet", "Chrysler", "Citroen", "Corvette"),
	"D" => array("Dacia", "Daewoo", "DAF", "Alpina", "Daihatsu", "Datsun", "Dodge"),
	"E" => array("Excalibur"),
	"F" => array("Ferrari", "Fiat", "Ford"),
	"G" => array("GAZ", "Geo", "GMC", "Great wall"),
	"H" => array("Honda", "Hummer", "Hyundai"),
	"I" => array("Infiniti", "Isuzu", "Iveco"),
	"J" => array("Jaguar", "Jeep"),
	"K" => array("Kia", "Koenigsegg"),
	"L" => array("Lada", "Lamborghini", "Lancia", "Land Rover", "Landwind", "Lexus", "Ligier", "Lincoln", "Lotus"),
	"M" => array("Mahindra", "Maserati", "Matra", "Maybach", "Mazda", "Mercedes-Benz", "MG", "Mini", "Mitsubishi", "Morgan", "Moskvich"),
	"N" => array("Nissan"),
	"O" => array("Oldsmobile", "Oltsit", "Opel"),
	"P" => array("Peugeot", "Piaggio", "Plymouth", "Pontiac", "Porsche"),
	"R" => array("Renault", "Rolls Royce", "Rover"),
	"S" => array("Saab", "Scion", "Seat", "Skoda", "Smart", "Ssangyong", "Subaru", "Suzuki"),
	"T" => array("Talbot", "Tata", "Toyota", "Trabant", "Triumph", "TVR"),
	"U" => array("UAZ"),
	"V" => array("Volga", "Volvo", "VW"),
	"W" => array("Wartburg", "Wiesmann"),
	"Z" => array("Zastava", "Zaz")
	);
	
	//offer title
	$offer_title = "";	
	if($_POST['car_brand']) $offer_title.= $_POST['car_brand']." ";
	if($_POST['car_model']) $offer_title.= $_POST['car_model']." ";
	if($_POST['car_engine']) $offer_title.= $_POST['car_engine'];
	
	//offer short description
	$offer_info = "<ul class=\"offer_info\">";
	if($_POST['car_year']) $offer_info.= "<li>�����������: <b>".$_POST['car_year']."</b>";
	if($_POST['car_fuel']) $offer_info.= "<li>��������: <b>".$_POST['car_fuel']."</b>";
	if($_POST['car_run']) $offer_info.= "<li>������: <b>".$_POST['car_run']."</b>";
	if($_POST['car_price']) $offer_info.= "<li>����: <b>".$_POST['car_price'] . "&nbsp;" . $_POST['currency']."</b>";
	$offer_info.= "</ul>";
	
	//offer data
	$offer_data = "<p></p><p><b>�����</b><br>";
	$offer_data.= "<ul>";
	if($_POST['car_type']) $offer_data.= "<li>��� �� ����������: <b>". $_POST['car_type']."</b></li>";
	if($_POST['car_category']) $offer_data.= "<li>���������: <b>". $_POST['car_category']."</b></li>";
	if($_POST['car_fuel']) $offer_data.= "<li>��� ��������: <b>". $_POST['car_fuel']."</b></li>";
	if($_POST['car_power']) $offer_data.= "<li>������� (�.�):<b> ". $_POST['car_power']."</b></li>";
	if($_POST['car_cubature']) $offer_data.= "<li>�������� (���.�): <b>". $_POST['car_cubature']."</b></li>";
	if($_POST['car_gear']) $offer_data.= "<li>��������� �����: <b>". $_POST['car_gear']."</b></li>";
	if($_POST['car_run']) $offer_data.= "<li>������: <b>". $_POST['car_run']." ��.</b></li>";
	if($_POST['car_month'] || $_POST['car_year']) $offer_data.= "<li>���� �� ������������: <b>". $_POST['car_month'] . "&nbsp;" . $_POST['car_year']."</b></li>";
	if($_POST['car_doors']) $offer_data.= "<li>���� �����: <b>". $_POST['car_doors']."</b></li>";
	if($_POST['car_color'])
		{
			$offer_data.= "<li>����: <b>". $_POST['car_color']."</b> ";
			if($_POST['metallic']) $offer_data.= " (�������)";
			$offer_data.= "</li>";
		}
		
	if($_POST['car_usage']) $offer_data.= "<li><b>��� ����</b></li>";
	if($_POST['car_condition']) $offer_data.= "<li>���������: <b>". $_POST['car_condition']."</b></li>";
	$offer_data.= "</ul></p>";
	
	//offer extras
	$offer_extras = "<p><b>����������</b><br>";
	$offer_extras.= "<ul>";
	if($_POST['car_doors']) $offer_extras.= "<li>". $_POST['car_doors']."</li>";
	for($i=0; $i<count($_POST['carextra']); $i++)
		if($carextra[$i]) $offer_extras.= "<li>".$carextra[$i]. " </li> ";
	$offer_extras.= "</ul></p>";
	
	//offer extra info
	$offer_notes = "<p><b>������������ ����������</b><br><br>";
	if($_POST['dcredit']) $offer_notes.= "����� �� ������� ������ / ";
	if($_POST['barter']) $offer_notes.= "���������� �� ������ / ";
	if($_POST['leasing']) $offer_notes.= "�������� ������ / ";
	if($_POST['notes']) $offer_notes.= $_POST['notes'];
	$offer_notes.= "</p>";

	//offer contacts
	$offer_contact = "<p><b>�� ��������</b><br><br>";
	if($_POST['contact_name']) $offer_contact.= "���: ".$_POST['contact_name']."<br>";
	if($_POST['contact_phone']) $offer_contact.= "���.: ".$_POST['contact_phone']."<br>";
	if($_POST['contact_email']) $offer_contact.= "e-mail: ".$_POST['contact_email']."<br>";
	if($_POST['contact_city']) $offer_contact.= "����: ".$_POST['contact_city']."<br><br>";
	$offer_contact.= "<a href=\"http://www.emoauto.mobile.bg\" target=\"_blank\">www.emoauto.mobile.bg</a>";
	$offer_contact.= "</p>";
		
	$pParent = $_POST['ParentPage'];
	$pName = $offer_title;
	$pText = $offer_info . $offer_data . $offer_extras . $offer_notes .$offer_contact;
	$pImage = $_FILES['offer_image'];
	$pSubImages = $_FILES['offer_images'];
	$pSubPagesColls = 4;
	$pShowLinks = 9;
	$pMakeLinks = 4;
	$pImageWidth = 320;
	$pImageAlign = 3; 
	//echo "<pre>";
	//print_r($pSubImages);
	//echo "</pre>";
	
	if($_POST['action'] == "add")
		{
			$inserted_id = $o_page->add($pParent, $pName);
			//$inserted_id = 1;
			if($inserted_id > 0)
				{
					for($i=0; $i<count($pSubImages['name']); $i++)
						{
								if($pSubImages['name'][$i] != "")
									{
										$pImage['name'] = $pSubImages['name'][$i];
										$pImage['tmp_name'] = $pSubImages['tmp_name'][$i];
										$pImage['type'] = $pSubImages['type'][$i];
										$pImage['size'] = $pSubImages['size'][$i];
										$pText = "";
										$pImageAlign = 1;
										$o_page->add($inserted_id, $pName . " " .$i);
									}
						}
				mk_output_message("normal", "������ ������ � �������� �������: <a href=\"page.php?n=".$inserted_id."&SiteID=".$SiteID."\" target=\"_blank\">".$pName."</a>");
				}
		}
	
	/*
	if($_POST['action'] == "confirm")
		{
			?>
			<form method="post">
			<table class="border_table" border="0" cellpadding="2" cellspacing="5" align="center" width="100%">
			<thead><tr><th colspan="3">������������</thead>
			<?
			echo "<tr><td align=\"right\" valign=\"top\">��������: <td>".$pName;
			echo "<tr><td align=\"right\" valign=\"top\">������: <td>".$pImage['name'];
			echo "<tr><td align=\"right\" valign=\"top\">����������: <td>".$pText;
			echo "<tr><td align=\"right\" valign=\"top\">����� ������: <td>";
			for($i=0; $i<count($pSubImages['name']); $i++)
				if($pSubImages['name'][$i] != "") echo "<li>".$pSubImages['name'][$i];
			?>
			</table>
			<input type="hidden" name="pName" value="<?=$pName?>">
			<input type="hidden" name="pImage" value="<?=$pImage?>">
			<input type="hidden" name="pSubImages" value="<?=$pSubImages?>">
			<input type="hidden" name="pText" value="<?=htmlspecialchars($pText)?>">
			<input type="hidden" name="pParent" value="<?=$_POST['ParentPage']?>">
			<input type="hidden" name="action" value="add">
			<input type="submit" value="������">
			</form>
			<?php
		}
		*/
if($_POST['action'] == "")
{
?>
<form method="post" enctype="multipart/form-data">
  <table class="border_table" border="0" cellpadding="2" cellspacing="5" align="center">
	 <thead><tr><th colspan="3">���� �����</thead>
   <tbody>
     <tr>
       <td align="right" valign="middle"><b>* ����� ��:</b></td>
       <td colspan="2">
	   <select name="ParentPage">
         <option value="34266" selected="selected">���������</option>
         <option value="34270">���</option>
         <option value="34267">�����</option>
         <option value="34269">����, �����, �������</option>
         <option value="34268">����������</option>
       </select></td>
     </tr>
   <tr>
    <td width="220" align="right" valign="middle"><b>* �����:</b></td>
    <td colspan="2">
	
     <select name="car_brand" style="width: 160px;">
		<?php
			$letters = array_keys($car_brands);
			for($i=0; $i<count($letters); $i++)
				{
					echo "<optgroup label=\"".$letters[$i]."\">";
					for($j=0; $j<count($car_brands[$letters[$i]]); $j++)
						echo "<option value=\"".$car_brands[$letters[$i]][$j]."\">".$car_brands[$letters[$i]][$j]."</option>";
					echo "</optgroup>";
				}
		?>
	</select>

    </td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>* �����:</b></td>
    <td colspan="2">

     <div id="model"><input type="text" name="car_model" title="�����"></div>
    </td>
   </tr>
   <tr>
    <td width="220" align="right" valign="middle"><b>�����������:</b></td>

    <td colspan="2">
      <input name="car_engine" value="" style="width: 155px;" maxlength="15" type="text">
      <small>(������: 1.6i, CDI, TDI...)</small>      </td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>* ��� �� ����������:</b></td>
    <td colspan="2">
      <select name="car_type" id="select" style="width: 160px;">
        <option value="�����������">�����������</option>
        <option value="���">���</option>
        <option value="Bayback">Buyback</option>
        <option value="���������������">���������������</option>
      </select>
  	</td>
   </tr>
   <tr>

    <td align="right" valign="middle"><b>* ���������:</b></td>
    <td colspan="2">
     <select name="car_category" title="���������" style="width: 160px;">
		<option value="">--------</option>
		<option value="�����">�����</option>
		<option value="������">������</option>
		<option value="��������">��������</option>
		<option value="�����">�����</option>
		<option value="K���">K���</option>
		<option value="������">������</option>
		<option value="����">����</option>
		<option value="�����">�����</option>

		<option value="���">��� </option>
    </select>
    </td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>* ����:</b></td>
    <td colspan="2">

     <input name="car_price" title="����" value="" style="width: 94px;" type="text">
     <select name="currency">
		<option value="��.">��.</option>
		<option value="USD">USD</option>
		<option value="EUR">EUR</option>
     </select>
    </td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>����� �� ������� ������:</b></td>
    <td colspan="2"><input name="dcredit" type="checkbox"></td>
   </tr>
    <tr>
    <td align="right" valign="middle"><b>�������� ������:</b></td>

    <td colspan="2"><input name="barter" type="checkbox"></td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>�������� ������:</b></td>
    <td colspan="2"><input name="leasing" type="checkbox"></td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>* ������:</b></td>

    <td colspan="2">
     <select name="car_fuel" title="������" style="width: 160px;">
		<option value="������">������</option>
		<option value="�����">�����</option>
		<option value="���/������">���/������</option>
		<option value="�����/������">�����/������</option>
		<option value="������">������</option>
	</select>
    </td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>* �������:</b></td>
    <td colspan="2">

     <input name="car_power" type="text" title="�������" style="width: 102px;" value="">
     <select name="powerunit" id="powerunit">
            <option value="�.�">�.�.</option>
            <option value="kW">kW</option>
          </select>
    </td>
   </tr>
   <tr>

    <td align="right" valign="middle"><b>��������:</b></td>
    <td colspan="2"><input name="car_cubature" type="text" title="��������" style="width: 155px;" value=""> 
    <small>(���.��)</small></td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>* ��������:</b></td>
    <td colspan="2">

     <select name="car_gear" title="��������" style="width: 160px;">
      <option value="">-----</option>
            <option value="�����">�����</option>
            <option value="�����������">�����������</option>
            <option value="���������������">���������������</option>
          </select>
    </td>

   </tr>
   <tr>
    <td align="right" valign="middle"><b>* ������:</b></td>
    <td colspan="2"><input name="car_run" type="text" title="������" style="width: 155px;" value=""> 
    <small>(��.)</small></td>
   </tr>
   <tr>

    <td align="right" valign="middle"><b>* ������ �� ������������:</b></td>
    <td colspan="2">
     <select name="car_month" title="�����">
      <option value="">-----</option>
            <option value="������">������</option>
            <option value="��������">��������</option>
			<option value="����">����</option>
            <option value="�����">�����</option>
            <option value="���">���</option>
            <option value="���">���</option>
            <option value="���">���</option>
            <option value="������">������</option>
            <option value="���������">���������</option>
            <option value="��������">��������</option>
            <option value="�������">�������</option>
            <option value="��������">��������</option>
          </select>
     <input name="car_year" type="text" title="������" style="width: 50px;" value="">�.
    </td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>* ���� �����:</b></td>
    <td colspan="2">
    <select name="car_doors" title="���� �����" style="width: 160px;">
		<option value="2/3 �����">2/3</option>
		<option value="4/5 �����" selected>4/5</option>
    </select>
    </td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>* ����:</b></td>
    <td colspan="2">

     <select name="car_color" title="����" style="width: 160px;">
		<option value="">-------</option>
		<option value="�����">�����</option>
		<option value="�����">�����</option>
		<option value="�������">�������</option>
		<option value="���">���</option>
		<option value="��������">��������</option>
		<option value="����">����</option>
		<option value="�����">�����</option>
		<option value="������">������</option>
		<option value="�����">�����</option>
		<option value="�����">�����</option>
		<option value="�������">�������</option>
		<option value="���">���</option>
		<option value="���">���</option>
		<option value="��������">��������</option>
		<option value="������">������</option>
		<option value="�����">�����</option>
    </select>
    <input name="metallic" value="1" id="metallic" type="checkbox"> <label for="metallic">�������</label>
    </td>
   </tr>
   <tr>
    <td align="right" valign="middle"><b>��� ����:</b></td>
    <td colspan="2"><input name="car_usage" type="checkbox" id="car_usage"></td>

   </tr>
   <tr>
    <td align="right" valign="middle"><b>* ���������:</b></td>
    <td colspan="2">
       <input name="car_condition" value="� ��������" checked="checked" type="radio">
       � ��������
       &nbsp;
       <input name="car_condition" value="���������/�������" type="radio">
       ���������/�������
      
       
    </td>

   </tr>
   <tr>
    <td align="right" valign="middle"><b>������������<br>����������:</b></td>
    <td colspan="2" valign="top"><textarea name="notes" style="width: 360px; height: 100px;"></textarea><br><small>(�������������� � ������������ �� �������� ��� ����������� �� ���� ����.)</small></td>
   </tr> 
    <tr align="left">
      <td align="right" valign="middle"><strong>������� ������:</strong></td>
      <td colspan="2" valign="top"><input type="file" name="offer_image" title="������"></td>
    </tr>
    <tr align="left">
      <td align="right" valign="middle"><strong>������������ ������:</strong></td>
      <td colspan="2" valign="top">
	  <table id="offer_images" align="left"><tr><td><input name="offer_images[]" type="file"  title="������" multiple="multiple"></td></table>
	  <!--<input type="button" onClick="duplicateRow('offer_images', 0)" value="+">//-->
      </tr>
    <tr align="left">
      <td colspan="3" valign="top"><b>������ � �����������</b></td>
      </tr>
    <tr>
      <td align="left" valign="top"><b>�������</b> </td>
      <td align="left" valign="top"><b>���������</b> </td>
      <td align="left" valign="top"><b>�����</b></td>
    </tr>
    <tr>
      <td align="left" valign="top">      <?php
	  	for($i=0; $i<10; $i++)
			echo "<input type=\"checkbox\" name=\"carextra[".$i."]\" value=\"".$carextras[$i]."\"><label for=\"carextra[".$i."]\"> ".$carextras[$i]."</label><br>";
	  ?>
	  </td>
      <td align="left" valign="top">      <?php
	  	for($i=10; $i<22; $i++)
			echo "<input type=\"checkbox\" name=\"carextra[".$i."]\" value=\"".$carextras[$i]."\"><label for=\"carextra[".$i."]\"> ".$carextras[$i]."</label><br>";
	  ?>
	  </td>
      <td align="left" valign="top">      <?php
	  	for($i=22; $i<count($carextras); $i++)
			echo "<input type=\"checkbox\" name=\"carextra[".$i."]\" value=\"".$carextras[$i]."\"><label for=\"carextra[".$i."]\"> ".$carextras[$i]."</label><br>";
	  ?>
		</td>
    </tr> 
	<tr align="left">
	  <td colspan="3"><b>���� �� �������</b>    
	  <tr>
	    <td align="right"><strong>���, �������:</strong> *     
	    <td colspan="2" align="left"><input name="contact_name" type="text" style="width: 160px;" title="���, �������" value="<?=$user->Name?>">      
      <tr>
        <td align="right"><strong>�������: *</strong>        
        <td colspan="2" align="left"><input name="contact_phone" type="text" style="width: 160px;" title="�������" value="">      
      <tr>
        <td align="right"><strong>E-mail: *</strong>      
        <td colspan="2" align="left"><input name="contact_email" type="text" style="width: 160px;" title="E-mail" value="<?=$user->EMail?>">      
      <tr>
	    <td align="right"><strong>����: *</strong>        
	    <td colspan="2" align="left"><input name="contact_city" type="text" style="width: 160px;" title="����" value="�����">            
	  <tr><td colspan="3" align="center">
	  <input type="hidden" name="action" value="confirm">
	  <input type="submit" value="������" onClick="MM_validateForm('ParentPage','','R', 'car_brand', '', 'R', 'car_model', '', 'R', 'car_type', '', 'R', 'car_category', '', 'R', 'car_price', '', 'R', 'car_fuel', '', 'R', 'car_power', '', 'R', 'car_gear', '', 'R', 'car_run', '', 'R', 'car_month', '', 'R', 'car_year', '', 'RisNum', 'car_doors', '', 'R', 'car_color', '', 'R', 'contact_name', '', 'R', 'contact_phone','','RisNum', 'contact_email','','RisEmail', 'contact_city', '', 'R');return document.MM_returnValue">
		<!--<input type="submit" value="������" />//-->
	<input type="hidden" name="action" value="add">
	<!--<input type="submit">//-->
  </tbody></table>

</form>
<?
}

}
else
	mk_output_message("error", "������ ������ �� ���� �������� !");
	//mk_output_message("error", "� ������� ���� ����� �� ��������. �.������, 02/846 46 46.");
?>