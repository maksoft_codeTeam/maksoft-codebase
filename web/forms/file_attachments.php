<?php
$autoload = realpath(__DIR__.'/../../modules/vendor/autoload.php');
require_once $autoload; 

$attrs = array(
    "name"     => "attachment",
    "id"       => "attachment",
    "class"    => "form-control",
);
$max_size = 2097152;
$upload_dir = realpath(__DIR__.DIRECTORY_SEPARATOR."attachments");
$file_input = new \Maksoft\Form\Fields\FileInputField($attrs);
$file_input->add_validators(new \Maksoft\Form\Validators\NotBiggerThan($max_size));
if(!empty($_FILES) && array_key_exists($attrs["name"], $_FILES)){
    $file_input->value = $_FILES[$attrs["name"]];
    $file_input->is_valid();
    $zip = new ZipArchive;
    if($zip->open($upload_dir.DIRECTORY_SEPARATOR.$form_id.".zip") === True){
        $zip->addFile($_FILES[$attrs["name"]]["tmp_name"], $_FILES[$attrs["name"]]["name"]);
        $zip->close();
    } else {
        throw new Exception("Неуспешно качване на файл! Моля, опитайте пак.");
    }
}

echo $file_input;
