<link rel="stylesheet" href="web/forms/comments/style.css">

<?php
require_once "modules/vendor/autoload.php";
require_once "lib/Database.class.php";
require_once "global/comments/forms/NewComment.php";
require_once "global/recaptcha/init.php";

$FB_INIT_SCRIPT = require_once "/hosting/maksoft/maksoft/global/facebook/login.php";
$fb = new StdClass();
if($FB_INIT_SCRIPT){
    list($fb->api_key, $fb->script, $fb->button) = $FB_INIT_SCRIPT;
}

$gate = new Maksoft\Gateway\Gateway(new Database());
$newComment = new NewComment($gate->page(), $o_page->_page['n'], $_POST);
echo $newComment;
//var_dump($gate->page()->getPage(42168));

echo $fb->script;

if($o_page->_site['language_id'] != 1){
    $lang = "en";
}

if($_SERVER['REQUEST_METHOD'] === "POST"){
    $recaptcha = new \ReCaptcha\ReCaptcha($secretKey);
    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
    if ($resp->isSuccess()) {
        $passedSpamCheck = True;
    } else {
        $errors = $resp->getErrorCodes();
    }
}
?>
<script language="JavaScript">
<!--
$j(document).ready(function(){
	$j('#send_comments').click(function(){
	
		var comment_n = $j('#comment_n').val();
		var comment_author = $j('#comment_author').val();
		var comment_author_email = $j('#comment_author_email').val();
		var comment_author_url = $j('#comment_author_url').val();
		var comment_author_ip = $j('#comment_author_ip').val();
		var comment_date = $j('#comment_date').val();
		var comment_text = $j('#comment_text').val();
		var comment_parent = $j('#comment_parent').val();
		var comment_user_id = $j('#comment_user_id').val();
		var comment_status = $j('#comment_status').val();
		
		//alert(comment_author_ip);
		//$j('#list_comments').load("web/admin/add_comments.php", "comment_n="+comment_n+"&comment_author="+comment_author+"&comment_author_email="+comment_author_email+"&comment_author_url="+comment_author_url+"&comment_author_ip="+comment_author_ip+"&comment_date="+comment_date+"&comment_text="+comment_text+"&comment_parent="+comment_parent+"&comment_user_id="+comment_user_id+"&comment_status="+comment_status);
		//alert(isValidEmailAddress(comment_author_email));
		
		if(comment_author_email != "" && comment_text != "")
			if(isValidEmailAddress(comment_author_email))
				$j('#form_comments').submit();
			else alert("���� �������� ������� e-mail ����� !");
		else alert("�������� ���������� � * �� ������������ !");
		
	})
	
	$j('#show_comments_form').click(function(){
		$j('#form_comments').toggle();
		$j('#comment_parent').val(0);
	})
	
	$j('#show_comments_form2').click(function(){
		$j('#form_comments').toggle();
		$j('#comment_parent').val(0);
	})
});

//-->
</script>

	<?php
		/*if(isset($_POST["remote_address"])){
			include_once("../../lib/lib_page.php");
		}*/

		// verify user comment   -1 regected   0-pending approval    1-approved
		function comment_verify($comment_text) {
		 $add_comment = 0;
		 $cp_query = mysqli_query("SELECT * FROM comments_prohibited WHERE language_id = '1' "); 
		 while ($cp_row=mysqli_fetch_array($cp_query)) {
			  if(eregi($cp_row['cp_text']." " , $comment_text) OR eregi(" ".$cp_row['cp_text'] , $comment_text)  ) {
			  	//$add_comment = $cp_row['cp_level']; 
				$add_comment  = -1; 
			  }
		 }
		 return $add_comment; 
		}
	?>
	<?php
	$add_comment = $_POST['add_comment'];

	if($add_comment == 1)
		{	
			if(comment_verify($comment_text)>=0) {
				if(check_code_str($code, $codestr, $passedSpamCheck)) { 
						//include "web/admin/add_comments.php";
						$comment_author_url = str_replace("http://", "", $comment_author_url);
						$num_rows = 0;
						$comment_text = strip_tags($comment_text);
						if($comment_subject == "")
							$comment_subject = "�������� ���: ".$o_page->get_pName();
						$add_sql = "INSERT INTO comments SET comment_n = '".$comment_n."', comment_subject = '".$comment_subject."', comment_author = '".$comment_author."', comment_author_email = '".$comment_author_email."', comment_author_url = '".$comment_author_url."', comment_author_ip = '".$comment_author_ip."', comment_date = now(), comment_text = '".$comment_text."', comment_parent = '".$comment_parent."', comment_user_id = '".$comment_user_id."', comment_status = 0";
						mysqli_query($add_sql);
						mk_output_message("warning", "����� �������� � �������� �������!");
                } else{
                        mk_output_message("error", "������ ��� �� ���������. ����� �������� �� � ������� !<br><br><a href=\"#add_comments\" id=\"show_comments_form\">������ ������</a>");
                }
			} else {
						mk_output_message("error", "������ ��� ����������� ���� � ������ �������� �� � �������. ���� ������ �� ��������� ���� � �� ���������� �� ��������!");
			}
		}
		
	$commented = NULL;
	if(isset($parent_comment))
		$commented = $o_page->get_comment($parent_comment);
	?>
	<div id="list_comments">
	<h3 class="head_text"><?php if($commented) { echo "��������"; } else echo "���������"; ?></h3>
	<ol class="commentlist group">
	<?php
		//get all page comments or single comment
		if(isset($comment_id))
			$comments[0] = $o_page->get_comment($comment_id, "comment_n=".$o_page->n);
		else
		if(isset($parent_comment))
			$comments[0] = $o_page->get_comment($parent_comment, "comment_n=".$o_page->n);
		else
			$comments = $o_page->get_pComments($o_page->n, "comment_parent=0");
		
		if(count($comments) == 0)
			{
				mk_output_message("normal", "���� �������� ���������. ������ �� �������� ������ ��������.");
			}
			
		$del_str = "&amp;"; 
		if(($o_page->_user['AccessLevel'] == 0) && ($o_page->_site['url_type'] > 0)) $del_str = "?"; 
			
		for($i=0; $i<count($comments); $i++)
			{
				$comments_author = $comments[$i]['comment_author'];
				if($comments[$i]['comment_author_url'] != "")
					$comments_author = "<a href=\"http://".$comments[$i]['comment_author_url']."\" target=\"_blank\">".$comments[$i]['comment_author']."</a>";
				?>
				
<li class="comment even thread-even depth-1">

      <div class="grid group comment-wrap">

        <div class="comment-avatar grid-1-5">
          <img src="web/forms/comments/wmd/images/avatar.jpg" width="50" height="50" class="lazyload-gravatar" alt="������ �� �����������">
        </div>

        <div class="comment-body group grid-4-5">

          <div class="comment-author-wrap vcard">
            <div class="comment-author"><?=$comments_author?></div> <div class="comment-time"><time><?=$comments[$i]['comment_date']?></time></div>
            
          </div>
          
          <div class="comment-content">

             <?=$comments[$i]['comment_text']?>

            <div class="reply">
              <a rel="nofollow" class="comment-reply-link" href="<?=$o_page->get_pLink().$del_str?>parent_comment=<?=$comments[$i]['comment_id']?>#reply">�������� <i class="fa fa-long-arrow-down" aria-hidden="true"></i></a>            
            </div>

          </div>
        </div>
      </div>
      
					<?php
						$sub_comments = $o_page->get_pComments($o_page->n, "comment_parent=".$comments[$i]['comment_id'], "comment_date ASC");
						if(count($sub_comments) > 0)
							{
								echo "<ul class=\"children\">";
								for($j=0; $j<count($sub_comments); $j++)
									{
										if($sub_comments[$j]['comment_author_url'] != "")
											$sub_comments_author = "<a href=\"http://".$sub_comments[$j]['comment_author_url']."\" target=\"_blank\" name=\"comment_".$sub_comments[$j]['comment_id']."\">".$sub_comments[$j]['comment_author']."</a>";
										else
											$sub_comments_author = $sub_comments[$j]['comment_author'];

										?>

    <li class="comment odd alt depth-2" id="li-comment-641151">

      <div class="grid group comment-wrap" id="comment-641151">

        <div class="comment-avatar grid-1-5">
          <img src="web/forms/comments/wmd/images/avatar.jpg" width="50" height="50" class="lazyload-gravatar" alt="������ �� �����������">
        </div>

        <div class="comment-body group grid-4-5">

          <div class="comment-author-wrap vcard">
            <div class="comment-author"><?=$sub_comments_author?></div> <div class="comment-time"> <time><?=$sub_comments[$j]['comment_date']?></time></div>
            
          </div>
          
          <div class="comment-content">

           <?=$sub_comments[$j]['comment_text']?>

            <div class="reply">
            </div><!-- .reply -->

          </div>
        </div>
      </div>
    
  </li>

<?php } echo "</ul>"; } ?>

</li>
			
	<?

			}
	?>
		</ol>
	</div>
	<?php
		if($user->ID >= 0)
		{	
			if(empty($comment_author)) $comment_author = $user->Name;
			if(empty($comment_author_email)) $comment_author_email = $user->EMail;
		if(!isset($parent_comment)) { ?> 
			<!--<a href="javascript: void(0)" id="show_comments_form" name="add_comments" class="add_comment">������ ��������</a>-->
		<? } else{
			?> <a name="reply"></a> <?
        }
	?>
	<form name="comments" class="form_comment" method="post" id="form_comments">
		<h4 class="commented"><em><?php if($commented) { echo "������� ��: <strong>" . $commented['comment_author']. "</strong> ��: " . $commented['comment_date']; } ?></em></h4>
		
	<textarea id="textarea" name="comment_text" placeholder="��������" style="width: 100%; height: 200px;"><?=$comment_text?></textarea>
   
    <div class="wmd-preview"></div>
   
    <div class="login-or-guest"> 
    <div class="row">
    
    <div class="col-md-6">
		<h4>����� ��</h4>
		<div class="new-login-left">
            <div class="preferred-login google-login">
                <p><span class="icon"></span><span>���� � Google</span></p>
            </div>
            <div class="preferred-login facebook-login">
                <?php echo $fb->button; ?>
            </div>
		</div>
	</div>
	<div class="col-md-6">
		<h4>���������� ���� ����</h4>
		<div class="new-login-left">
		<div class="guest-login">
            <input name="comment_author" type="text" id="comment_author" placeholder="��� *" value="<?=$comment_author?>" required>
        </div>
		<div class="guest-login">
			<input name="comment_author_email" type="text" id="comment_author_email" placeholder="E-mail *" value="<?=$comment_author_email?>" required>
		</div>
		</div>
	</div>	
	
	</div>
	<div class="imnotrobot">
	<div class="row">
			<?php if($reCaptchaKey and $secretKey != $secret){ ?>
                <td align="center">
                    <div class="g-recaptcha" data-sitekey="<?php echo $secret; ?>"></div>
                    <script type="text/javascript"
                             src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>">
                    </script>
				</td>
                <?php } else { ?>
				<td>
				<?php
					$code = rand(0, 65535);   
					echo("<img src=\"http://www.maksoft.net/gen.php?code=$code\" align=\"middle\" vspace=5 hspace=5><input type=\"hidden\" name=\"code\" value=\"$code\">"); 
				?>
        	        <br><input name="codestr" type="text" id="��� �� ���������" size="10">
            <?php } ?>
	</div>
	</div>
	<div class="row">
					<input type="hidden" name="comment_n" id="comment_n" value="<?=$n?>">
					<input type="hidden" name="comment_author_ip" id="comment_author_ip" value="<?=$ip?>">
					<input type="hidden" name="comment_date" id="comment_date" value="<?=date('Y-m-d H:i:s')?>">
					<input type="hidden" name="comment_parent" id="comment_parent" value="<?=$parent_comment?>">
					<input type="hidden" name="comment_user_id" id="comment_user_id" value="<?=$user->ID?>">
					<input type="hidden" name="comment_status" id="comment_status" value="1">
					<input type="hidden" name="add_comment" value="1">
					<input type="button" value="������� ��������" id="send_comments" class="send-button">
	</div>
	</div>
	
</form>

<?php
	}
?>

<script type="text/javascript" src="web/forms/comments/wmd/wmd.js"></script>
<script src="web/assets/js/notify.min.js"></script>
<script>

</script>
