<meta charset="utf8">
<?php
include 'Database.php';
include 'XmlToArrayClass.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of parser
 *
 * @author Radoslav Yordanov
 */
class Parser {

    private static $url;
    private $table_name;
    private $result;

    public function __construct() {
        echo __CLASS__;
    }

    public function set_url($url) {
        self::$url = $url;
        $this->parse_array();
    }

    public function get_url() {
        return self::$url;
    }

    public function set_table_name($name) {
        $this->table_name = $name;
    }

    private function xml_to_array($xml, $main_heading = '') {
        if (@simplexml_load_file($xml)) {
            $deXml = simplexml_load_file($xml);
            $deJson = json_encode($deXml);
            $xml_array = json_decode($deJson, TRUE);
            if (!empty($main_heading)) {
                $returned = $xml_array[$main_heading];
                return $returned;
            } else {
                return $xml_array;
            }
        } else {
            return 999;
        }
    }

    private function check_xml() {
        $dom = new \DOMDocument();
        try {
            $dom->load(self::$url);
            $xml = $dom->saveXML();
            $array = new \SimpleXMLElement($xml);
            if ($array->count() > 0):
                return $array;
            else:
                return FALSE;
            endif;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function get_dom() {
        return $this->check_xml();
    }

    private function parse_array() {
        $exception = 0;
        $i = 0;
        if ($this->check_xml()):
            $xml_array = $this->xml_to_array(self::$url);
            try {
                $array = new \RecursiveArrayIterator($xml_array);
            } catch (InvalidArgumentException $exc) {
                echo $exc->getTraceAsString();
                //$db_insert->query("INSERT INTO `" . $table_name . "` (`name`) VALUES ('InvalidArgumentException')");
                $exception = 1;
            }
            $this->array_depth();
            if ($exception == 2):
                while ($array->valid()):
                    echo '<br><b>' . $i . '</b><br>';
                    foreach ($array->getChildren() as $key => $value):
                        if (is_array($value)):

                        else:
                        // $this->insert_values($value);
                        endif;
                    endforeach;
                    $array->next();
                    $i++;
                //var_dump($this->result);
                endwhile;
            endif;
        else:
            echo 'invalid xml or invalid url... check again';
        endif;
    }

    private function insert_values($data, &$cats = '', &$vals = '') {
        $doc = new DOMDocument;

// This is required in order to ignore the pretty-print whitspace
        $doc->preserveWhiteSpace = false;

// load the data into the object
        $doc->load(self::$url);
        $doc->saveXML();
        $xpath = new DOMXpath($doc);

        $result = array();

// Get the text nodes and loop them
        foreach ($xpath->query('/') as $record) {
            //$result[] = $record->data;
            var_dump($record);
        }

        print_r($result);
    }

    private function array_depth() {
        $doc = new DOMDocument;

// This is required in order to ignore the pretty-print whitspace
        $doc->preserveWhiteSpace = false;

// load the data into the object
        $doc->load(self::$url);
        $xml = $doc->saveXML();
        $array = XML2Array::createArray($xml);
        $array2 = new RecursiveArrayIterator($array);
        while ($array2->valid()):
            foreach ($array2->current() as $key => $value):
                var_dump($value);
            endforeach;
            $array2->next();
        endwhile;
    }

}

//$result = new Parser();
//$result->set_table_name('adsys');
//$result->set_url('xml/adsys.xml');

$solytron = new Parser();
$solytron->set_table_name('solytron');
$solytron->set_url('http://solytron.bg/products/xml/catalog-category.xml?j_u=stech_xml&j_p=3fb71428');

