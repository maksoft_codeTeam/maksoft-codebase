<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Radoslav Yordanov
 */
class Database extends PDO {

    private static $dsn;
    private static $username;
    private static $passwd;
    private static $db_name;
    private $conn;
    private static $table_name;

    public function __construct($dsn = NULL, $username = NULL, $passwd = NULL, $db_name = NULL) {

        self::$dsn = ($dsn === NULL) ? 'localhost' : $dsn;
        self::$username = ($username === NULL) ? 'root' : $username;
        self::$passwd = ($passwd === NULL) ? '' : $passwd;
        self::$db_name = ($db_name === NULL) ? 'magento' : $db_name;
        parent::__construct("mysql:host=" . self::$dsn . ";dbname=" . self::$db_name . "", self::$username, self::$passwd);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        $this->exec("SET CHARACTER SET utf8");
    }

    public function set_table_name($table_name) {
        self::$table_name = $table_name;
    }

    public function make_columns($table_name='', $data, $type = 'varchar(255)') {
        if(empty($table_name)):
            $table=  self::$table_name;
        else:
            $table=$table_name;
        endif;
        $validator = false;
        $tableExists = true;
        $column_names = '';
        $i = 0;
        $conn = '';
        try {
            $tableExists = gettype($this->query("SELECT count(*) FROM $table")) == 'integer';
        } catch (PDOException $e) {
            echo '<br><b>Table: ' . $table . '</b> doesnt exist... we must create it<br>';
        }
        $sql = "CREATE TABLE $table (p_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY)";
        try {
            $this->exec($sql);
        } catch (PDOException $exc) {
           // echo $exc->getTraceAsString();
        }
        $array = $data;
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if(is_array($value)):
                $this->make_columns($table_name, $value);
                endif;
                try {
                    $sql = "ALTER TABLE $table ADD `$value` " . $type . " CHARACTER SET utf8 COLLATE utf8_general_ci";
                    $this->exec($sql);
                } catch (PDOException $e) {
                    //echo '<br> Column ' . $value . ' already exist...continue';
                }
                $i++;
            }
        } else {
            try {
                $sql2 = "ALTER TABLE $table ADD $array varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci";
                $this->exec($sql2);
            } catch (PDOException $e) {
                //echo $e->getCode() . '<br>' . $e->getMessage() . '<br>';
                //echo '<br> Column ' . $array . ' already exist...continue';
            }
        }
    }

    public function insert_data($table_name, $columns, $values) {
        //echo 'test';
        $this->exec("INSERT INTO $table_name ($columns) VALUES ($values)");
        try {
            
        } catch (PDOException $e) {
            $e->getTraceAsString();
        }
    }

    public function is_column_exists($column, $table) {
        try {
            $sql = "SELECT * FROM information_schema.COLUMNS 
                    WHERE TABLE_NAME    =  '" . $table . "' 
                    AND TABLE_SCHEMA     = '" . self::$db_name . "'
                    AND COLUMN_NAME     = '" . $column . "'";
            $result = $this->getQueryResult($sql);
            return count($result);
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    private function getQueryResult($sql) {
        $sth = $this->prepare($sql);
        $sth->execute();
        $result = $sth->fetchAll();
        return $result;
    }

    public function __destruct() {
        
    }

}
