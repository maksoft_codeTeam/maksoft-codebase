<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function xml_to_array($xml, $main_heading = '') {
    $deXml = simplexml_load_file($xml);
    $deJson = json_encode($deXml);
    $xml_array = json_decode($deJson, TRUE);
    if (!empty($main_heading)) {
        $returned = $xml_array[$main_heading];
        return $returned;
    } else {
        return $xml_array;
    }
}


function array_to_html_table($array){
	
	$html = '<table>';
	for($i=0; $i < count($array); $i++):
		$html .= '<tr>
				<td>'.$array[$i]['Name'].'</td>';
		$html .= '	<td>'.$array[$i]['Value'].'</td>
			</tr>';
	endfor;
	$html .= '</table>';

	return $html;
}