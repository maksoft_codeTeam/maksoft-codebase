<?php

include 'DatabaseClass.php';

/**
 * Created by PhpStorm.
 * User: jokuf
 * Date: 10/25/15
 * Time: 12:21 PM
 */
class XLS_Parser {

    private $table_name;
    private $file_name;
    private $conn;

    public function __construct($table_name, $file_name) {
        $this->table_name = $table_name;
        $this->file_name = '..//..//excel_sheets//' . $file_name;
        echo $this->file_name;
        $this->conn = new Database();
        $this->main();
    }

    private function main() {
        include 'Excel/reader.php';
        $fileData = $this->readFile(utf8_decode($this->file_name));
        $xls_col = $fileData['cells'][1][1];  //[1] & [2]
        $xlsTotalNumCols = $fileData['numCols'];
        $xlsColName = $fileData['cells'][1][2];
        $Val = $fileData['cells'];
        $xlsTotalNumRows = count($fileData['cells']);
        return $this->get_column_names($Val);
    }

    private function readFile() {

        $excel = new Spreadsheet_Excel_Reader();
        $excel->setOutputEncoding('UTF-8');
        $excel->read($this->file_name);

        return $excel->sheets[0];
    }

    public function get_column_names($data) {
        $table = $this->table_name;
        $validator = false;
        $tableExists = true;
        $column_names = '';
        $i = 0;
        $array = $data;
        if (is_array($array)) {
            $obj = new RecursiveArrayIterator($array);
            foreach ($obj->current() as $key => $value) {
                if (!is_array($value)):
                    $column_names.="`" . $value . "`";
                    $column_names.=',';
                    $this->conn->make_columns($this->table_name, $array);
                else:
                    foreach ($value as $k => $v):
                        $column_names.="`" . $v . "`";
                        $column_names.=',';
                        $this->conn->make_columns($this->table_name, $array);
                    endforeach;
                endif;
            }
        }
        $this->insert_values(rtrim($column_names, ','), $array);
    }

    private function insert_values($column_names, $data) {
        $table = $this->table_name;
        $array = $data;
        $exception = 0;
        try {
            $obj = new RecursiveArrayIterator($array);
        } catch (InvalidArgumentException $exc) {
            echo $exc->getTraceAsString();
            $exception = 1;
        }
        if ($exception = 0):
            while ($obj->hasChildren()) {
                $values = '';
                foreach ($obj->current() as $key => $value) {
                    if (!is_array($value)):
                        $values.="'" . $value . "'";
                        $values.=',';
                    endif;
                }
                echo '<br>';
                echo '<br>';
                var_dump($values);
                echo '<br>';
                try {
                    $this->conn->exec("set names utf8");
                    $values = rtrim($values, ',');
                    //echo $tables;
                    $this->conn->insert_data($this->table_name, $column_names, $values);
                } catch (PDOException $e) {
                    //echo '<br> Column '.$value.' already exist...continue';
                    echo $e . '<br>';
                }
                unset($values);
                $obj->next();
            }
        endif;
    }

}
