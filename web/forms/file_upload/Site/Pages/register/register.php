<?php
ob_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/ftp_client/Site/Templates/base.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/ftp_client/' . 'Class/lib_pass.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/ftp_client/' . 'Class/DatabaseClass.php';
?>
<script type="text/javascript" src="http://www.clubdesign.at/floatlabels.js"></script>

<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <?php if (isset($_SESSION['error_message'])): ?>
                <div class="alert alert-danger" role="alert"><?php echo $_SESSION['error_message'];?></div>
                    <?php
                    endif;
                    ?>
            <?php if (isset($_SESSION['success'])): ?>
                <div class="alert alert-success" role="alert"><?php echo $_SESSION['success'];?></div>
                    <?php
                    endif;
                    ?>
            <form method="post" class="form-signin">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">

                            <form class="form-horizontal" action="" method="POST">
                                <fieldset>
                                    <div id="legend">
                                        <legend class="">Форма за регистрация в Maksoft ftp-server (само за клиенти)</legend>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="username">Потребителско име:</label>
                                        <div class="controls">
                                            <input type="text" id="username" name="username" placeholder="" class="form-control input-lg">
                                            <p class="help-block">Потребителското име трябва да съдържа само букви и цифри без интервали</p>
                                        </div>
                                    </div>
                                     <div class="control-group">
                                        <label class="control-label" for="first_name">Име:</label>
                                        <div class="controls">
                                            <input type="text" id="first_name" name="first_name" placeholder="" class="form-control input-lg">
                                            <p class="help-block">Моля въведете вашето име</p>
                                        </div>
                                    </div>
                                     <div class="control-group">
                                        <label class="control-label" for="last_name">Фамилия:</label>
                                        <div class="controls">
                                            <input type="text" id="last_name" name="last_name" placeholder="" class="form-control input-lg">
                                            <p class="help-block">Моля, въведете вашета фамилия</p>
                                        </div>
                                    </div>
                                     <div class="control-group">
                                        <label class="control-label" for="company">Име на фирма:</label>
                                        <div class="controls">
                                            <input type="text" id="company" name="company" placeholder="" class="form-control input-lg">
                                            <p class="help-block">Моля, въведете име на вашата компания</p>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="email">E-mail:</label>
                                        <div class="controls">
                                            <input type="email" id="email" name="email" placeholder="" class="form-control input-lg">
                                            <p class="help-block">Моля, въведете вашата електронна поща</p>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="password">Парола:</label>
                                        <div class="controls">
                                            <input type="password" id="password" name="password" placeholder="" class="form-control input-lg">
                                            <p class="help-block">Паролата трябва да е с минимална дължина 6 символа</p>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="password_confirm">Потвърдете паролата:</label>
                                        <div class="controls">
                                            <input type="password" id="password_confirm" name="password_confirm" placeholder="" class="form-control input-lg">
                                            <p class="help-block">Моля, въведете отново вашата парола</p>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <!-- Button -->
                                        <div class="controls">а
                                            <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">Регистрирай се!</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>

                        </div> 
                    </div>
                </div>
            </form>
        </div>
    </div>


    <?php 


    $db = new Database('localhost','root','','maksoft_webftp');
    $sql_database = "
        CREATE DATABASE IF NOT EXISTS maksoft_webftp CHARACTER SET utf8 COLLATE utf8_general_ci;
    ";
    $sql = "CREATE TABLE IF NOT EXISTS `users` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `username` varchar(50) NOT NULL,
            `email` varchar(50) NOT NULL,
            `password` varchar(150) NOT NULL,
            `company` varchar(80) NOT NULL,
            `first_name` varchar(50) NOT NULL,
            `last_name` varchar(50) NOT NULL,
            `created_at` date NOT NULL,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ";
    $db->exec($sql_database);
    $db->exec($sql);
    $error = array(
            'error' =>0,
            1 => 'Вие въведохте невалиден мейл адрес, моля въведете коректен такъв.',
            2 => 'Това поле е задължитено, моля въведете коректни данни.',
            3 => 'Въведените пароли не съвпадат, моля опитайте отново.',
            4 => 'Невалидни данни'
        );
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['submit'])): 
        // Password verification
        if ($_POST['password'] == $_POST['password_confirm'] and strlen($_POST['password']) >4):
            $hash = password_hash($_POST['password'], PASSWORD_BCRYPT);
        else:
            $_SESSION['error_message'] = $error[2].'<br>'.$error[4];
            $error['error'] += 1;
        endif;
        // Email verification
        if ($_POST['email'] != Null):
            preg_match("/([A-Za-z0-9-._]+[@]+[A-Za-z0-9-]+[.]+[A-Za-z]{2,3}+)/", $_POST['email'], $output_array);
    
            //if (strlen($output_array) >0):
            $mail = $output_array[0];
            //else:
                //$_SESSION['error_message'] = $error[1];
               // $error['error'] += 1;
           // endif;
        else:
            $_SESSION['error_message'] = $error[1];
            $error['error'] += 1;
        endif;
        if(strlen($_POST['first_name'])>3 && strlen($_POST['first_name']) >3 && strlen($_POST['first_name']) >3):
            $error['error'] += 0;
        else:
            $_SESSION['error_message'] = $error[4];
            $error['error'] += 1;
        endif;   
        $sql = "
                INSERT INTO users (username, email, password, company, first_name, last_name)
                 VALUES (:username, :email, :password, :company, :first_name, :last_name)
        ";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':username',$_POST['username']);
        $stmt->bindParam(':email', $mail);
        $stmt->bindParam(':password', $hash);
        $stmt->bindParam(':company', $_POST['company']);    
        $stmt->bindParam(':first_name', $_POST['first_name']);
        $stmt->bindParam(':last_name', $_POST['last_name']);
        if($error['error'] == 0):
            $stmt->execute();
            unset($_SESSION['error_message']);
            $_SESSION['success'] = 'Успешна регистрация! Благодарим Ви!';
        endif;
    endif;

ob_end_flush();

?>