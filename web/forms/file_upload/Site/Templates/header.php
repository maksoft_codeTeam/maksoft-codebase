<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <h3>Максофт - най-големият издател на календари в България и водещ вносител на рекламни сувенири.</h3>
        </div>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="/ftp_client/">Начало</a></li>
             <?php
            if (isset($_SESSION['loged'])):
                ?>
                
            <li><a href="http://maksoft.bg/">Maksoft.BG</a></li>
            <li>

            <form method="post">
                <input name="return" type="hidden" value="<?php echo urlencode($_SERVER["PHP_SELF"]); ?>" />
                <input  class="btn btn-warning" type="submit" value="logout" />
            </form>
            </li>
            <?php
            endif;
            ?>
        </ul>
    </div>
</nav>

<?php
if (isset($_POST['return'])):
    destroySession();
endif;

