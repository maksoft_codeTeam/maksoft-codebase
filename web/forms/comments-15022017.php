<?php
require_once "modules/vendor/autoload.php";
require_once "lib/Database.class.php";
require_once "global/comments/forms/NewComment.php";
require_once "global/recaptcha/init.php";
$db = new Database();
$db->exec("SET NAMES UTF8");
$gate = new Maksoft\Gateway\Gateway($db);

$post_comment = new NewComment($gate->page(), $o_page->_page['n'], $_POST);

$fb = new StdClass();

$FB_INIT_SCRIPT = require_once "/hosting/maksoft/maksoft/global/facebook/login.php";

if($FB_INIT_SCRIPT){
    list($fb->api_key, $fb->script, $fb->button) = $FB_INIT_SCRIPT;
}

switch($o_page->_site['language_id']){
    case 1:
        $lang="bg";
        break;
    default:
        $lang="en";
}

if($_SERVER['REQUEST_METHOD'] === "POST"){
    $recaptcha = new \ReCaptcha\ReCaptcha($secretKey);
    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
    if ($resp->isSuccess()) {
        switch($action=$_POST['action']){
            case $post_comment->action->value:
                $post_comment->is_valid();
                $post_comment->save();
                break;
            default:
                break;
        }
    } else {
        $errors = $resp->getErrorCodes();
    }
}
?>
<link rel="stylesheet" href="web/forms/comments/style.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.min.js" integrity="sha256-1O3BtOwnPyyRzOszK6P+gqaRoXHV6JXj8HkjZmPYhCI=" crossorigin="anonymous"></script>
<!-- JavaScript -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.9.0/alertify.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>

<script type="text/javascript" src="web/forms/comments/wmd/wmd.js"></script>

<script src="web/assets/js/notify.min.js"></script>
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>&render=explicit"> </script>
<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/alertify.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/themes/bootstrap.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/themes/bootstrap.rtl.min.css"/>

<?php echo $fb->script;?>


<div id="list_comments">
    <div id="status"></div>
    <h3 class="head_text">���������</h3>

    <?php if($o_page->_user['AccessLevel'] > 0) { ?>
        <button onclick="showForm(this, {parent: 0, author: '<?=$o_page->_user['Name']?>'})" class="send-button">��� ��������</button>
    <?php } else { ?>
        <button onclick="showForm(this, {parent: 0, author: 'asd'})" class="send-button">��� ��������</button>
    <?php } ?>
    <ol class="commentlist group"> </ol>
</div>

<script id="parent-comment" type="text/x-handlebars-template">
<li id="comment-{{id}}" class="comment even thread-even depth-1">
  <div class="grid group comment-wrap">
    <div class="comment-avatar grid-1-5">
      <img src="{{image}}" width="50" height="50" class="lazyload-gravatar" alt="{{author}}">
    </div>
    <div class="comment-body group grid-4-5">
      <div class="comment-author-wrap vcard">
        <div class="comment-author">{{author}}</div> 
        <div class="comment-time">
            <time>{{date}}</time>
        </div>
      </div>
      <div class="comment-content">
            {{text}}
        <div class="reply">
          <button class="comment-reply-link" onclick="showForm(this, {parent: {{id}}, author: '{{author}}' })">�������� <i class="fa fa-long-arrow-down" aria-hidden="true"></i></button>            
        </div>
      </div>
    </div>
  </div>
</li>
</script>
<script id="child-comment" type="text/x-handlebars-template">
<ul class="children">
    <li class="comment odd alt depth-2" id="comment-{{id}}">
        <div class="grid group comment-wrap" id="comment-{{id}}">
            <div class="comment-avatar grid-1-5">
                <img src="{{image}}" width="50" height="50" class="lazyload-gravatar" alt="{{author}}">
            </div>

            <div class="comment-body group grid-4-5">

            <div class="comment-author-wrap vcard">
                <div class="comment-author">{{author}}</div>
                    <div class="comment-time">
                         <time>{{date}}</time>
                    </div>
                </div>
                <div class="comment-content">
                    {{text}}
                    <div class="reply"> </div>
                </div>
            </div>
        </div>
    </li>
</ul>
</script>
<script id="comment-form" type="text/x-handlebars-template">
    <form class="form_comment" id="form_comment">
        <div id="summary"></div>
        <h4 class="commented"><em><?php if($commented) { echo "������� ��: <strong>" . $commented['comment_author']. "</strong> ��: " . $commented['comment_date']; } ?></em></h4>
        <textarea id="textarea" name="comment_text" placeholder="��������" minlength="2" style="width: 100%; height: 200px;" required><?=$comment_text?></textarea>
        <div class="wmd-preview" id="textarea-formated"></div>
        <div class="login-or-guest"> 

            <div class="row">
            <?php $guest_title = "���������� ���� ����"; ?>
            <?php if($o_page->_user['AccessLevel'] == 0){ ?>
                <?php $guest_title = "���������� ���� ����"; ?>
                <div class="col-md-6">
            {{# if logged }}
            ��� ��� ������ � facebook
            {{ else }}
            <h4>����� ��</h4>
            {{/if}}
                    <div class="new-login-left">
                        <!--
                        <div class="preferred-login google-login">
                            <p><span class="icon"></span><span>���� � Google</span></p>
                        </div>
                        -->
                        <div if="fb-login" class="preferred-login facebook-login">
                            <?php echo $fb->button; ?>
                        </div>
                    </div>
                </div>
             <?php } ?>
                <div class="col-md-6">
                    {{#if logged }}
                    {{ else }}
                    <h4><?php echo $guest_title;?></h4>
                    <div class="new-login-left">
                        <div class="guest-login">
                            <input name="comment_author" type="text" id="comment_author" placeholder="��� *" value="{{user.name}}" required>{{name}}
                        </div>
                        <div class="guest-login">
                            <input name="comment_author_email" type="email" id="comment_author_email" placeholder="E-mail *" value="{{email}}" required>
                        </div>
                    </div>
                    {{/if}}
                </div>  
            </div>

        <div id='tete' class="imnotrobot">
            <div class="row">
                <div id="captcha"></div>
                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
            </div>
    </form>
</script>

<script>
var $ = jQuery;

var logged = false;

function changeStatus(is_logged){
    logged = is_logged;
}

function isLoggedWithFacebook(){
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        var data = [];

        return data;
      } else if (response.status === 'not_authorized') {
        // the user is logged in to Facebook, 
        // but has not authenticated your app
        return false;
      } else {
        return false;
      }
     });
}
function get(url) {
  // Return a new promise.
  return new Promise(function(resolve, reject) {
    // Do the usual XHR stuff
    var req = new XMLHttpRequest();
    req.open('GET', url);

    req.onload = function() {
      // This is called even on 404 etc
      // so check the status
      if (req.status == 200) {
        // Resolve the promise with the response text
        resolve(req.response);
      }
      else {
        // Otherwise reject with the status text
        // which will hopefully be a meaningful error
        reject(Error(req.statusText));
      }
    };

    // Handle network errors
    req.onerror = function() {
      reject(Error("Network Error"));
    };

    // Make the request
    req.send();
  });
}
$("button[class=comment-reply-link").each(function(i,el){
    console.log(el);
});
wmd_options = { autostart: false };
var parentCommentTemplate   = $("#parent-comment").html();
var parentComment           = Handlebars.compile(parentCommentTemplate);
var childCommentTemplate    = $("#child-comment").html();
var childComment            = Handlebars.compile(childCommentTemplate);

function createRecaptcha() {
    grecaptcha.render("captcha", {sitekey: "<?=$secret?>", theme: "light", callback: addComment});
    FB.XFBML.parse(document.getElementById('fb-login'));
}


var recaptcha = 12312;
function addComment(recapResponse){
    recaptcha = recapResponse;
}

function getCaptcha(){
    return recaptcha;
}

function showForm(e, data){
    var data                    = data;
    var commentFormTemplate     = $("#comment-form").html();
    var postComment             = Handlebars.compile(commentFormTemplate);
    var captcha = recaptcha;

    alertTitle = "�������� �� ��������...";
    if(data.parent > 0){
        alertTitle = "�������� �� ������� �� " + data.author;
    }

    var user = [];
    data['logged'] = logged;
    FB.api(
        "/me?fields=name,picture,first_name,last_name,email,about",
        function (response) {
          if (response && !response.error) {
              user = response;
              console.log(user);
              data['user'] = user;
              data['id'] = user.id;
              data['pic'] = user.picture.data.url;
              data['name'] = user['name'];
              data['email'] = user['email'];
          } else {
            data['id'] = 0;
            data['name'] = $("#comment_author").val();
            data['pic'] = "http://maksoft.net/web/forms/comments/wmd/images/avatar.jpg";
            data['email'] = $("#comment_author_email").val();
          }
        }
    );

    alertify.confirm(alertTitle, postComment(data), function(el){
        $(el).on('click', function(e){
            e.preventDefault();
        });

        <?php if($o_page->_user['AccessLevel'] > 0){ ?>
            saveComment("<?=$o_page->_user['Name']?>", "<?=$o_page->_user['EMail'];?>", data.parent, "<?=$o_page->_user['picture'];?>","<?php echo $o_page->_user['ID'];?>", 1);
            alertify.success("������ �������� � ������� �������");
        <?php } else { ?>

            console.log(data);
            var form = $("#form_comment");
            form.validate({
                showErrors: function(errorMap, errorList) {
                    $("#summary").html("������� �������  " + this.numberOfInvalids() + " ������, ����� �� ��-����.");
                    this.defaultShowErrors();
                },
                rules: {
                    comment_author_email: {
                        required: true,
                        email: true,
                    },
                    comment_author:{
                        required: true,
                        minlength: 3,
                    },
                    textarea: {
                        required: true,
                        minlength: 5
                    },
                    hiddenRecaptcha: {
                        required: function(){
                            if(grecaptcha.getResponse() == ""){
                                return true;
                            }
                            return false;
                        }
                    }
                },
                messages: {
                    comment_author: "����, ��������� ������ ���.",
                    comment_author_email: {
                        required: "������� �� �� ������ �����",
                        email: "������ ����� ����� ������ �� ���� ��� ������ name@domain.com"
                    },
                    textarea:{
                        required:  "�� ���� �� ����������� ������ ��������.",
                        minlength: jQuery.validator.format("����������� ������� �� �������� � {0} �������!")
                    }
                }
            });
            if(form.valid()){
              if(grecaptcha.getResponse() === "" || grecaptcha.getResponse() != undefined){
                    saveComment(data.name, data.email, data.parent, data.pic, data.id, 1, getCaptcha());
                    alertify.success("������ �������� � ������� �������");
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
              } else {
                alertify.error("������� � ������������, ���� ����������� ����������");
              }
            } else {
                alertify.error("�� ���� �� ����������� ������ ��������.");
            }


              $(".commentlist").html('');
              listComments();
       <?php } ?> }, function(){ $("#captcha").html("");
        grecaptcha.reset();
    }).set({onshow: createRecaptcha() }); 
}


function validateTextArea(){
    return $("#textarea-formated").text().length > 0;
}


function saveComment(author, email, parent, image, user_id, status=0, recaptcha, subject=''){
    $.ajax({
        url: '<?php echo $_SERVER["REQUEST_URI"]?>',
        type: 'POST',
        dataType: 'json',
        data: {
            n: "<?php echo $o_page->_page['n']; ?>",
            author: author,
            email: email,
            user_id: user_id,
            subject: subject,
            text: $('#textarea-formated').html(),
            parent: parent,
            image: image,
            'g-recaptcha-response': recaptcha,
            status: status,
            action: "<?php echo $post_comment->action->value;?>",
        }
    })
    .done(function() {
        alertify.success("������ �������� � ������� �������");
    })
    .fail(function() {
    })
    .always(function() {
    });

}

(function(){
    listComments();
}());

function listComments(){
    $(".commentlist").html('');
    get("/api/?command=getComments&n=<?php echo $o_page->_page['n'];?>").then(function(response){
        comments = JSON.parse(response);
        console.log(comments);
        var inserted = [];
        for(i in comments){
            var comment = comments[i];
            var context = {
                id: comment.comment_id,
                subject: comment.comment_subject,
                author: comment.comment_author,
                date: comment.comment_date,
                image: comment.comment_image,
                text: comment.comment_text
            }
            if(comment.comment_parent == 0){
                var html = parentComment(context);
                $(".commentlist").append(html);
            }
            inserted.push(comment.comment_id);
        }
        return {comments: comments, inserted: inserted};
    }).then(function(response){
        comments = response.comments;
        inserted = response.inserted;
        for(i in comments){
            comment = comments[i];
            if(comment.comment_parent > 0){
                var context = {
                    id: comment.comment_id,
                    subject: comment.comment_subject,
                    author: comment.comment_author,
                    date: comment.comment_date,
                    text: comment.comment_text,
                    image: comment.comment_image
                }
                var html = childComment(context);
                $("#comment-"+comment.comment_parent).append(html);
            }
        }
    });
}

</script>
