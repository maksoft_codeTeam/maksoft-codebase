<?php

$pPage_settings = array('text_limit'=>80, 'page_limit'=>3, 'button_text'=>'Разбери повече', 'default_page'=> $o_site->_site['news']);
$_sub_pages = $o_page->get_pSubpages($pPage_settings['default_page'], "p.sort_n desc limit ".$pPage_settings['page_limit']);
$t = '<div class="row">';
for ($i=0; $i < count($_sub_pages); $i++) {
    $content = substr(strip_tags($_sub_pages[$i]["textStr"]), 0, $pPage_settings['text_limit']);
      $t .= '<div class="col-sm-6 col-md-4">
        <div class="thumbnail">
          <img src="'.$_sub_pages[$i]["image_src"].'" alt="'.$_sub_pages[$i]["imageName"].'">
          <div class="caption">
            <h3>'.$_sub_pages[$i]["Name"].'</h3>
            <p>'.$content.'</p>
            <p><a href="'.$_sub_pages[$i]["page_link"].'" class="btn btn-default" role="button">'.$pPage_settings['button_text'].'</a></p>
          </div>
        </div>
      </div>';
}
echo $t.'</div>';
