<center>
<table border="0" cellspacing="0" cellpadding="2" class="main_table">
  <form method="post" name="inquiry_form" action="/form-cms.php">
    <caption>
	
       <b>I am interested in the property listed above</b>: <i><?=$row->Name?></i>
    <hr>
	</caption>
	<tr valign="top">
      <td  align="center" >
        <table border="0"  cellspacing="2" cellpadding="2">
          <tr>
            <td><b>Please</b> :</td>
          </tr>
          <tr>
            <td class="t3">
              <input type="checkbox" name="opt_1" value="Send me more details about this property">
              Send me more details about this property.
            </td>
          </tr>
          <tr>
            <td class=t3>
              <input type="checkbox"  checked name="opt_2" value="Put me on your mailing list">
              Put me on your mailing list.
            </td>
          </tr>
          <tr>
            <td class=t3>
              <input type="checkbox" name="opt_3" value=" I have got a property to sell and I would like to be contacted  to arrange a valuation." >
              I have got a property to sell and I would like to be contacted
              to arrange a valuation.
            </td>
          </tr>
          <tr>
            <td class=t3>
              <input type="checkbox" name="opt_10" checked value="Send me details on saving money with Foreign Currency" >
              Send me details on saving money with Foreign Currency.
            </td>
          </tr>
          <tr>
            <td class=t3>
              <input type="checkbox" name="opt_4" value="Make an appointment for me to see this property">
              Make an appointment for me to see this property.
</td>
          </tr>
        </table>
        <b><br>
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>
              <p class="t3">Comments <i>(use this box to enter any comments or questions you have)</i>
              <textarea name="comments" rows="10" cols="30" id="textarea">I'm interested in "<?=$row->Name?>"</textarea>
              </p>
             
              <p class="t3" align="justify">All of the property details above will be forwarded with this
                form including the property reference. However, if there are
                further properties that you wish to view please enter their <b>reference
                numbers</b> in the box below.<br>
                <textarea name="txt_OtherPropRefs" rows="5" cols="30" ></textarea>
              </p>
            </td>
          </tr>
        </table>
      </b></td>
      <td align="center">
        <b><br>
        <input name="button" type="button" onClick="show_hide('info_visit')" value="Show info for your visit">
        <div id="info_visit" style="display:none"><br>
            <table border="0" class="border_table" cellspacing="0" cellpadding="0" >
              <caption>
              <strong>Please tell us about your planned visit</strong>
              </caption>
              <tr>
                <td >Arrival Date</td>
                <td>
                  <input name="txt_arrivedate" type="text"  size="10">
                </td>
              </tr>
              <tr>
                <td >Length of Stay </td>
                <td>
                  <input type="text" name="txt_staylenght" size="4" maxlength="2">
        Nights </td>
              </tr>
              <tr>
                <td >How many people traveling </td>
                <td>
                  <input type="text" name="txt_numpeople" size="4" maxlength="2" >
                </td>
              </tr>
              <tr>
                <td >Date of proposed viewing</td>
                <td>
                  <input name="txt_dateviewing" type="text" size="10">
                </td>
              </tr>
              <tr>
                <td >Maximum Budget (specify currency)</td>
                <td>
                  <input name="txt_maxbudget" type="text" size="10">
                </td>
              </tr>
              <tr>
                <td >Is this a cash purchase
                </td>
                <td>
                  <input type="checkbox" name="opt_9" value="It is a cash purchase" >
                </td>
              </tr>
              <tr>
                <td >Do you require us to arrange <b>flights</b>
                </td>
                <td>
                  <input type="checkbox" name="opt_5" value="Arrange flights" >
                </td>
              </tr>
              <tr>
                <td >Do you require us to arrange <b>accommodation</b>
                </td>
                <td>
                  <input type="checkbox" name="opt_6" value="Arrange accommodation">
                </td>
              </tr>
              <tr>
                <td >Do you require us to arrange <b>car hire</b>
                </td>
                <td>
                  <input type="checkbox" name="opt_7" value="Arrange car hire" >
                </td>
              </tr>
              <tr>
                <td >Do you require assistance with a <b>mortgage</b>
                </td>
                <td>
                  <input type="checkbox" name="opt_8" value="Assist with a mortgage" >
                </td>
              </tr>
            </table>
        </div>
        <br>
        </b><br>
        <table  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><br>your e-mail address</td>
          </tr>
          <tr>
            <td>
              <input type=text name="EMail" size="30" value="" id="EMail">
            </td>
          </tr>
          <tr>
            <td>First Name</td>
          </tr>
          <tr>
            <td>
              <input type=text name="firstname" size="30" value="" id="firstname">
            </td>
          </tr>
          <tr>
            <td>Surname</td>
          </tr>
          <tr>
            <td>
              <input type=text name="surname" size="30" value="" id="surname">
            </td>
          </tr>
          <tr>
            <td>Phone</td>
          </tr>
          <tr>
            <td>
              <input type=text name="phone" size="30" value="" id="phone">
            </td>
          </tr>
          <tr>
            <td>Mobile Phone</td>
          </tr>
          <tr>
            <td>
              <input type=text name="mobilephone" size="30" value="" id="mobilephone">
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Address</td>
          </tr>
          <tr>
            <td>
              <input type=text name="addr1" size="30" value="" id="addr1">
            </td>
          </tr>
          <tr>
            <td>
              <input type=text name="addr2" size="30" value="" id="addr2">
            </td>
          </tr>
          <tr>
            <td>Postcode<br>
                <input type=text name="postcode" size="15" value="" id="postcode">
            </td>
          </tr>
        </table>
        <br>        
        <br>
        <br>
        <br>
        <input type="submit" value="Send Inquiry" name="btn_send" class="button_submit">
      </td>
    </tr>
    <input type="hidden" name="n" value="<?=$n?>">
    <input type="hidden" name="SiteID" value="<?=$SiteID?>">
	<input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$Site->EMail"); ?>">
	<input type="hidden" name="str" value="Form submited successful">
  </form>
</table>
<hr>
</center>