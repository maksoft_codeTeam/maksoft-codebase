<script language="javascript" type="text/javascript">
$j(document).ready(function(){
	
	$j("#FluidType").change(function(){
		if($j(this).val() == "")
			{
				$j("#FluidTypeOther").show();
			}
		else $j("#FluidTypeOther").hide();
	
	});

	$j("#FluidType1").change(function(){
		if($j(this).val() == "")
			{
				$j("#FluidTypeOther1").show();
			}
		else $j("#FluidTypeOther1").hide();
	
	});

	$j("#HeatType").change(function(){
		if($j(this).val() == "")
			{
				$j("#HeatTypes").show();
			}
		else $j("#HeatTypes").hide();
	
	});
	
	$j("#submit").click(function(){
		var email = $j("#EMail").val();
		var phone = $j("#Telephone").val();
		$j("#EMail").attr("style","");
		$j("#Telephone").attr("style","");
		error = false;
		error_message = "";
		
		if(email == "")
			{
				//$j("#form_message").show();
				error_message = error_message + "Enter e-mail<br>";
				$j("#EMail").attr("style","border: 1px solid #F00");
				error = true;
			}
		else if(!isValidEmailAddress(email))
			{
				error_message = error_message + "Enter valid e-mail<br>";
				$j("#EMail").attr("style","border: 1px solid #F00");
				error = true;
			}
						
		if (phone == "")
			{
				//$j("#form_message").show();
				error_message= error_message + "Enter phone<br>";
				$j("#Telephone").attr("style","border: 1px solid #F00");
				error = true;
			}
			
		
		if(!error)
			$j("#request_form").submit();
		else
			{
				$j("#form_message").html("<div class=\"message_error\">"+error_message+"</div>");
				$j("#form_message").show();
			}		
		});
})
</script>
<?php
$labels = array(
	'CompanyName' => "Company Name ",
    'ContactName' => "Contact Name",
    'Telephone' => "Telephone ",
	'Fax' => "Fax",
    'EMail' => "E-mail",
    'applicationfield' => "Application field",
    'HeatType' => "Type of heat exchanger",
    'Quantity' => "Quantity",
    'HeatTransfer' => "Capacity",
    'FluidType' => "Fluid Name I",
	//'FluidTypeOther' => "���� ��� �� ������ I",
    'FluidType1' => "Fluid Name II",
	//'FluidTypeOther1' => "���� ��� �� ������ II",
    'TempIn' => "Temp In I",
    'TempIn1' => "Temp In II",
    'TempOut' => "Temp Out I",
    'TempOut1' => "Temp Out II",
    'FlowRate' => "Flow Rate I",
    'FlowRate1' => "Flow Rate II",
    'PressureDrop' => "Pressure Drop I",
    'PressureDrop1' => "Pressure Drop II",
    'MaxPressureTemp' => "Max. working temperature I",
    'MaxPressureTemp1' => "Max. working temperature II",
    'MaxPressureDrop' => "Max. working pressure I",
    'MaxPressureDrop1' => "Max. working pressure II",
    'Viscosity' => "Viscosity I",
    'Viscosity1' => "Viscosity II",
	);
	
	
	if($_POST['action'] == "submit_request")
		{
			/*
			echo "<pre>";
			print_r($_POST);
			echo "</pre>";
			*/
			$request_content = "";
			$request_table_content = "<form name=\"\" method=\"POST\" action=\"form-cms.php?n=$n&SiteID=$SiteID\"><table width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" class=\"border_table\"><thead><tr><th colspan=\"3\">Request</thead>";
			
			$label_keys = array_keys($labels);
			for($i=0; $i<count($label_keys); $i++)
				{
					$request_content.= $labels[$label_keys[$i]] . " : " . $_POST[$label_keys[$i]]." ".$_POST[$label_keys[$i]."s"]."\n";
					$request_table_content.= "<tr><td><b>".($i+1)."</b><td>".$labels[$label_keys[$i]]."<td>".$_POST[$label_keys[$i]]." ".$_POST[$label_keys[$i]."s"];
				}
			
			$code = rand(0, 65535);   
			$request_table_content.= "<thead><tr><th colspan=3>Please enter security key</thead><tr><td><td><img src=\"http://www.maksoft.net/gen.php?code=$code\" align=\"middle\" vspace=5 hspace=5><input type=\"hidden\" name=\"code\" value=\"$code\"><td><input name=\"codestr\" type=\"text\" size=\"10\">"; 
				 
			$request_table_content.="<tr><td colspan=3><input type=\"hidden\" name=\"mail_content\" value=\"".htmlspecialchars(CyrLat($request_content))."\"><input type=\"hidden\" name=\"EMail\" value=\"".$_POST['EMail']."\"><input type=\"hidden\" name=\"ToMail\" value=\"".$Site->EMail."\"><input name=\"conf_page\" type=\"hidden\" value=\"page.php?n=$n&SiteID=$SiteID&sent=1\"><input name=\"n\" type=\"hidden\" value=\"$n\"><input name=\"SIteID\" type=\"hidden\" value=\"$SiteID\"><input type=\"submit\" value=\"Submit request\">";	
			$request_table_content.= "</table></form>";
			echo $request_table_content;
		}
	else
	{
		if($sent == 1) mk_output_message("normal", "Your request had been sent !");
?>
<form method="post" id="request_form">
<table cellpadding="5" cellspacing="0" width="100%" border="0" class="border_table">
    <thead>
    	<tr>
        	<th colspan="2"><b>Please complete the form below.</b> </th>
		</tr>
    </thead>         
    <tbody>
        <tr>
            <td width="100px">Company Name</td>
            <td align="left">
            <input name="CompanyName" type="text" size="57"></td>
        </tr>
        <tr >
            <td><span lang="en">Contact Name</span></td>
            <td align="left">
            <input name="ContactName" type="text" size="57"></td>
        </tr>
        <tr >
            <td><span lang="en">*Phone</span></td>
            <td align="left"><input name="Telephone" type="text" size="57" id="Telephone"></td>
        </tr>
        <tr >
            <td><span lang="en">Fax:</span></td>
            <td align="left"><input name="Fax" type="text" size="57" id="Fax"></td>
        </tr>
        <tr >
            <td><span lang="en">*</span>E-mail</td>
            <td align="left"><input name="EMail" type="text" size="57" id="EMail"></td>
        </tr>
        <tr >
            <td><span lang="en">Application field</span></td>
            <td align="left">
            <input name="applicationfield" type="text" size="57"></td>
        </tr>
        <tr >
            <td>Type of heat exchanger</td>
            <td align="left">
            <select class="sel" name="HeatType" size="1" style="width: 100%" id="HeatType">
				<option value="no choice">choice</option>
				<option value="Brazed plate heat exchanger">Brazed plate heat exchanger</option>
				<option value="Gasket plate heat exchanger">Gasket plate heat exchanger</option>
				<option value="Welded plate heat exchanger">Welded plate heat exchanger</option>
				<option value="Shell-tube heat exchanger">Shell-tube heat exchanger</option>
				<option value="">Other</option>
            </select>
			<input name="HeatTypes" type="text" id="HeatTypes" style="display: none;" size="57">
			</td>
        </tr>
        <tr >
            <td>Quantity</td>
            <td align="left"><input name="Quantity" type="text" size="57" id="Quantity"></td>
        </tr>
    </tbody>
</table><br>
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="border_table">
     <thead>
    	<tr>
        	<th width="100">
            <th>HOT SIDE</th>
            <th>COLD SIDE</th>
		</tr>
    </thead>  
    <tbody>
        <tr >
          <td><span lang="en">Capacity</span></td>
            <td colspan="2" align="left" >
            <input class="sel01" name="HeatTransfer" type="text" size="12">
            <select class="sel" name="HeatTransfers" id="HeatTransfers" size="1" style="width: 50px" >
            <option value="KW">KW</option>
            <option value="Btu">Btu</option>
            <option value="Kcal/h">Kcal/h</option></select></td>
        </tr>
        <tr >
          <td><span lang="en">Fluid Name</span></td>
            <td align="left" valign="top" >
            <select class="sel" name="FluidType" size="1" style="width: 165px" id="FluidType" >
						<option value="water">water</option>
						<option value="ethylene glycol">ethylene glycol 10%
						</option>
						<option value="ethylene glycol 15%">ethylene glycol 15%
						</option>
						<option>ethylene glycol 20%</option>
						<option value="ethylene glycol 30%">ethylene glycol 30%
						</option>
						<option value="ethylene glycol 35%">ethylene glycol 35%
						</option>
						<option value="ethylene glycol 40%">ethylene glycol 40%
						</option>
						<option value="propylene glycol 10%">propylene glycol 10%
						</option>
						<option value="propylene glycol 15%">propylene glycol 15%
						</option>
						<option value="propylene glycol 20%">propylene glycol 20%
						</option>
						<option value="propylene glycol 30%">propylene glycol 30%
						</option>
						<option value="propylene glycol 35%">propylene glycol 35%
						</option>
						<option value="propylene glycol 40%">propylene glycol 40%
						</option>
						<option value="sea water">sea water</option>
						<option value="steam">steam</option>
						<option value="milk">milk</option>
						<option>skim milk</option>
						<option value="beer">beer</option>
						<option>juice</option>
						<option value="wine">wine</option>
						<option value="vegetable oil">vegetable oil</option>
						<option value="NH3">NH3</option>
						<option value="R114">R114</option>
						<option value="R12">R12</option>
						<option value="R134A">R134A</option>
						<option value="R22">R22</option>
						<option>R404A</option>
						<option>R407C</option>
						<option>R500</option>
						<option>R410A</option>
						<option value="">other</option>
            </select><br><br>
			<input name="FluidTypes" type="text" id="FluidTypeOther" style="display: none;" size="21">
		  </td>
            <td align="left" valign="top" >
            <select class="sel" name="FluidType1" size="1" style="width: 165px" id="FluidType1" >
				<option value="water">water</option>
				<option value="ethylene glycol">ethylene glycol 10%
				</option>
				<option value="ethylene glycol 15%">ethylene glycol 15%
				</option>
				<option>ethylene glycol 20%</option>
				<option value="ethylene glycol 30%">ethylene glycol 30%
				</option>
				<option value="ethylene glycol 35%">ethylene glycol 35%
				</option>
				<option value="ethylene glycol 40%">ethylene glycol 40%
				</option>
				<option value="propylene glycol 10%">propylene glycol 10%
				</option>
				<option value="propylene glycol 15%">propylene glycol 15%
				</option>
				<option value="propylene glycol 20%">propylene glycol 20%
				</option>
				<option value="propylene glycol 30%">propylene glycol 30%
				</option>
				<option value="propylene glycol 35%">propylene glycol 35%
				</option>
				<option value="propylene glycol 40%">propylene glycol 40%
				</option>
				<option value="sea water">sea water</option>
				<option value="steam">steam</option>
				<option value="milk">milk</option>
				<option>skim milk</option>
				<option value="beer">beer</option>
				<option>juice</option>
				<option value="wine">wine</option>
				<option value="vegetable oil">vegetable oil</option>
				<option value="NH3">NH3</option>
				<option value="R114">R114</option>
				<option value="R12">R12</option>
				<option value="R134A">R134A</option>
				<option value="R22">R22</option>
				<option>R404A</option>
				<option>R407C</option>
				<option>R500</option>
				<option>R410A</option>
				<option value="">other</option>
            </select><br><br>
			<input name="FluidType1s" type="text" id="FluidTypeOther1" style="display: none;" size="21">
		  </td>
        </tr>
        <tr >
          <td><span lang="en">Temp. In</span></td>
            <td align="left">
            <input class="sel01" name="TempIn" type="text" size="12">
            <select class="sel" name="TempIns" size="1" style="width: 50px" id="TempIns" >
            <option value="<sup>o</sup>C">�C</option>
            <option value="K">K</option></select></td>
            <td align="left">
            <input class="sel01" name="TempIn1" type="text" size="12">
            <select class="sel" name="TempIn1s" size="1" style="width: 50px" id="TempIn1s" >
            <option value="<sup>o</sup>C">�C</option>
          <option value="K">K</option></select></td>
        </tr>
        <tr >
          <td><span lang="en">Temp. Out</span></td>
            <td align="left">
            <input class="sel01" name="TempOut" type="text" size="12">
            <select class="sel" name="TempOuts" size="1" style="width: 50px" >
            <option value="�C">�C</option>
            <option value="K">K</option></select></td>
            <td align="left">
            <input class="sel01" name="TempOut1" type="text" size="12">
            <select class="sel" name="TempOut1s" size="1" style="width: 50px" id="TempOut1s" >
            <option value="�C">�C</option>
          <option value="K">K</option></select></td>
        </tr>
        <tr >
          <td>Flow Rate</td>
            <td align="left">
            <input class="sel01" name="FlowRate" type="text" size="12">
            <select class="sel" name="FlowRates" style="width: 50px">
            <option value="m3/h">m3/h</option>
            <option value="Kg/s">Kg/s</option>
            <option value="Kg/h">Kg/h</option>
            <option value="l/min">l/min</option>
            <option value="gal/min">gal/min</option>
            <option value="caft/min">caft/min</option></select></td>
            <td align="left">
            <input class="sel01" name="FlowRate1" type="text" size="12">
            <select class="sel" name="FlowRate1s" size="1" style="width: 50px" id="FlowRate1s">
            <option value="m3/h">m3/h</option>
            <option value="Kg/s">Kg/s</option>
            <option value="Kg/h">Kg/h</option>
            <option value="l/min">l/min</option>
            <option value="gal/min">gal/min</option>
          <option value="caft/min">caft/min</option></select></td>
        </tr>
        <tr >
          <td>Pressure Drop</td>
            <td align="left">
            <input class="sel01" name="PressureDrop" type="text" size="12">
            <select class="sel" name="PressureDrops" size="1" style="width: 50px">
            <option value="Kpa">kPa</option>
            <option value="bar">bar</option>
            <option value="mbar">mbar</option>
            <option value="PSI">PSI</option>
            <option value="mmWS">mmWS</option></select></td>
            <td align="left">
            <input class="sel01" name="PressureDrop1" type="text" size="12">
            <select class="sel" name="PressureDrop1s" size="1" style="width: 50px" id="PressureDrop1s">
            <option value="Kpa">kPa</option>
            <option value="bar">bar</option>
            <option value="mbar">mbar</option>
            <option value="PSI">PSI</option>
          <option value="mmWS">mmWS</option></select></td>
        </tr>
        <tr >
          <td>Max. working temperature</td>
            <td align="left">
            <input class="sel01" name="MaxPressureTemp" type="text" size="12" id="MaxPressureTemp">
            <select class="sel" name="MaxPressureTemps" size="1" style="width: 50px" id="MaxPressureTemps" >
            <option value="<sup>o</sup>C">�C</option>
          <option value="K">K</option></select></td>
            <td align="left">
            <input class="sel01" name="MaxPressureTemp1" type="text" size="12" id="MaxPressureTemp1">
            <select class="sel" name="MaxPressureTemp1s" size="1" style="width: 50px" id="MaxPressureTemp1s" >
            <option value="<sup>o</sup>C">�C</option>
          <option value="K">K</option></select></td>
        </tr>
        <tr >
          <td>Max. working pressure</td>
            <td align="left">
            <input class="sel01" name="MaxPressureDrop" type="text" size="12" id="MaxPressureDrop">
            <select class="sel" name="MaxPressureDrops" size="1" style="width: 50px" id="MaxPressureDrops" >
            <option>bar</option>
            <option>atm</option>
            <option>KPa</option>
            <option>Psi</option>
          </select></td>
            <td align="left">
            <input class="sel01" name="MaxPressureDrop1" type="text" size="12" id="MaxPressureDrop1">
            <select class="sel" name="MaxPressureDrop1s" size="1" style="width: 50px" id="MaxPressureDrop1s" >
            <option>bar</option>
            <option>atm</option>
            <option>KPa</option>
            <option>Psi</option>
          </select></td>
        </tr>
        <tr >
          <td>Viscosity</td>
            <td align="left">
            <input class="sel01" name="Viscosity" type="text" size="12">
            <select class="sel" name="Viscositys" size="1" style="width: 50px" id="Viscositys" >
            <option>Cp</option>
            <option value="#">Cp</option>
            <option value="#">mPas</option>
            <option value="#">kg/m.s</option>
            <option value="#">Ns/m</option>
          <option value="#">lb/ft.h</option></select></td>
            <td align="left">
            <input class="sel01" name="Viscosity1" type="text" size="12" id="Viscosity1">
            <select class="sel" name="Viscosity1s" size="1" style="width: 50px" id="Viscosity1s" >
            <option>Cp</option>
            <option value="#">Cp</option>
            <option value="#">mPas</option>
            <option value="#">kg/m.s</option>
            <option value="#">Ns/m</option>
          <option value="#">lb/ft.h</option></select></td>
        </tr>
    </tbody>
</table>
<div id="form_message" style="display:none;"><?=mk_output_message("error", "�������� ��� * �� ������������")?></div>
<input type="hidden" name="action" value="submit_request">
</form><br><br>
<input class="submit" type="button" name="submit" id="submit" value="Send request">
	<?php
	
	}
	
	?>