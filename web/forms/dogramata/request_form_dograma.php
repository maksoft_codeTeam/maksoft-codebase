<script language="JavaScript">
$j(document).ready(function(){
		
		$j("#button_submit").click(function(){
			
			var client_name = $j("#client_name").val();
			var client_email = $j("#client_email").val();
			
			if(client_name == "") $j("#client_name").attr("class", "error");
			else $j("#client_name").attr("class", "");
			
			if(client_email == "") $j("#client_email").attr("class", "error");
			else $j("#client_email").attr("class", "");
						
			if(client_name != "" && client_email != "")
				$j("#request_form").submit();	
			else alert("�������� ���������� � * �� ������������!");
		})
		
	
		$j(".window_table").each(function(){
			var root = $j(this);

			//disable all forms
			$j(root).find("select").attr("disabled", "disabled");
			$j(root).find("input.dimension").attr("disabled", "disabled");
			$j(root).find("input.model").attr("disabled", "disabled");

			$j(root).find(".checkbox").click(function(){
				if($j(root).find(".checkbox").attr("checked"))	
					{
						$j(root).find("select").removeAttr("disabled");
						$j(root).find("input.dimension").removeAttr("disabled");
						$j(root).find("input.model").removeAttr("disabled");					
					}
				else
					{
						$j(root).find("select").attr("disabled", "disabled");
						$j(root).find("input.dimension").attr("disabled", "disabled");
						$j(root).find("input.model").attr("disabled", "disabled");
					}
				})
			})		
		
		function load_profile(i)
			{
				var brand = $j("#brand_"+i).val();
				//alert(brand);	
				$j("#profile_"+i).load("web/forms/dogramata/request_functions.php", "profile="+brand);
			}
		
		$j("#brand_0").change(function(){ load_profile(0);})
		$j("#brand_1").change(function(){ load_profile(1);})
		$j("#brand_2").change(function(){ load_profile(2);})
		$j("#brand_3").change(function(){ load_profile(3);})
		$j("#brand_4").change(function(){ load_profile(4);})
		$j("#brand_5").change(function(){ load_profile(5);})
		$j("#brand_6").change(function(){ load_profile(6);})
		$j("#brand_7").change(function(){ load_profile(7);})

});

</script>
<style type="text/css">
<!--
table.window_table{border: 0px solid #4c95b4; margin: 0 0 10px 0; background: #f2f2f2; color: #4c95b4} 
table.window_table table {color: #4c95b4}
table.window_table th {background: #4c95b4; color: #FFF; font-size: 14px;}
td.border {	border: 0px solid #4c95b4; }
td.color {background: #4c95b4; color: #FFF}
tfoot td.color {background: #c7c7c7; color: #FFF}
tfoot td.color2 {background: #e3e3e3; color: #4c95b4}
table.window_table select {width: 150px;}
table.window_table select.small {width: 120px;}
input.error {border: 1px solid #F00}
-->
</style>
<?php
	include "web/mime_mail.inc";
	$mail = new mime_mail();

	#1 ����� �������
	$window_brand = array("- �������� -", "ETEM", "REHAU", "TROCAL", "SALAMANDER", "KOMMERLING", "VEKA", "PROFILINK", "Thyssen INOUTIC ", "SCHUCO");
	if($SiteID == 211)
		$window_brand = array("- �������� -", "SALAMANDER", "PROFILINK");
		
	#2 �������� �������
	$window_profile = array("null"=> array("- �������� -"),
							"ETEM" => array("ETEM ��������"), 
							"REHAU" => array("Euro 60", "Euro 70", "Brilliant Design"),
							"TROCAL" => array("5K", "6K"), 
							"SALAMANDER" => array("2D", "2D/3D", "3D", "Streamline � ���� �����", "Streamline � ����� �����", "Brugmann � ���� �����", "Brugmann � ����� �����"), 
							"KOMMERLING" => array("Futur Classic", "Futur Elegance", "88 Plus"), 
							"VEKA" => array("Euroline", "Softline 70"), 
							"PROFILINK" => array("Classic", "Premium"), 
							"Thyssen INOUTIC" => array("3K", "Elite 5K"), 
							"SCHUCO" => array("Corona CT 70AS Classic", "Corona CT 70AS Rondo")				
							);
	
	#3 �����������
	$window_glass = array("- �������� -", "���� 4 ��/���� 4 ��", "���� ������ 4 ��/���� 4��", "��������������� 4 ��/���� 4 ��", "���� 4 ��/���� 5 ��", "���� 5��/���� ������ 4 ��" ,"��������������� 4 ��/���� 5 ��", "���� 4 ��/���� 6 ��", "���� 6��/���� ������ 4 ��", "��������������� 4 ��/���� 6 ��", "���� 4 ��/������ 4 ��", "������ 4��/���� ������ 4 ��", "���� 4 ��/������ 4 ��", "������ 4��/���� ������ 4 ��", "���� 4 ��/����� 4 ��", "����� 4��/���� ������ 4 ��", "���� 4 ��/��� 4 ��", "��� 4��/���� ������ 4 ��", "���� 4 ��/���� 4 ��/���� 4 ��", "���� 4 ��/���� 4 ��/���� ������ 4 ��", "��������������� 4 ��/���� 4 ��/���� 4 ��", "��������������� 4 ��/���� 4 ��/���� ������");
	
	#4 ������
	$window_fill = array("- �������� - ", "PVC �����", "AL �����", "PVC �����", "AL �����");
	
	#5 �������� ������
	$window_ledge_int = array("- �������� -", "PVC 15 ��", "PVC 20 ��", "PVC 25 ��", "PVC 30 ��");
	
	#6 ������ ������
	$window_ledge_ext = array("- �������� -", "AL 10 ��", "AL 16 ��", "AL 21 ��", "AL 25 ��");
	
	#7 �������� �����
	$window_ins_screen_door = array("- �������� -", "������������ �������", "���������� �������", "���������� �����", "���������� ��������", "��� ����");
	
	#8 �������� ��������
	$window_ins_screen_glass = array("- �������� -", "- �������� -", "��������", "���������� �������", "�� ��������", "���������� �����");
	
	#9 ���� ������
	$window_blinds_ext = array("- �������� -", "PVC ������", "���������� ������");
	
	#10 ���� ��������
	$window_blinds_int = array("- �������� -", "A���������", "����������", "���������", "���������", "��� � ���");
	
	#11 ����
	$window_color = array("- �������� -", "����", "� ����");
	
	#12 �����
	$window_hardware = array("- �������� -", "�������� ��������", "�������� ��������", "���������������", "�������� �����");
	
	###########################################
	function list_menu($name, $data, $params)
		{
			$content = "";
			$content.= "<select name=\"".$name."\" $params>";
			for($i=0; $i<count($data); $i++)
				$content.= "<option value=\"".$data[$i]."\">".$data[$i]."</option>";
			$content.= "</select>";
			return $content;
		}
	##########################################
	
	############# TEMPLATES #################
	
	$templates = array(
		array("image"=>"t1.jpg", "dimensions"=>"H:B", "hardware"=> "�����", "model"=> "DGR 01", "extra_fill"=> 0),
		array("image"=>"t2.jpg", "dimensions"=>"H:B:b", "hardware"=> "�����", "model"=> "DGR 02", "extra_fill"=> 0),
		array("image"=>"t3.jpg", "dimensions"=>"H:B:b", "hardware"=> "�����", "model"=> "DGR 03", "extra_fill"=> 0),
		array("image"=>"t4.jpg", "dimensions"=>"H:B:b", "hardware"=> "����� 1:����� 2", "model"=> "DGR 04", "extra_fill"=> 0),
		array("image"=>"t5.jpg", "dimensions"=>"H:B", "hardware"=> "����� 1:����� 2", "model"=> "DGR 05", "extra_fill"=> 0),
		array("image"=>"t6.jpg", "dimensions"=>"H:B:b", "hardware"=> "����� 1:����� 2", "model"=> "DGR 06", "extra_fill"=> 0),
		array("image"=>"t7.jpg", "dimensions"=>"H:B:b:h", "hardware"=> "�����", "model"=> "DGR 07", "extra_fill"=> 1),
		array("image"=>"t8.jpg", "dimensions"=>"H:B:b:h", "hardware"=> "����� 1:����� 2", "model"=> "DGR 08", "extra_fill"=> 1)
	);
	
	if($_POST['submit_request'] == 1)
		{
			$window = $_POST['window'];
			//echo "<pre>";
			//print_r($_POST);
			//echo "</pre><hr>";

			$mail_content = "<h3>�������� ��������</h3>";
			$window_keys = array_keys($window);
			if(count($window) == 0)
				$mail_content.="���� ������� �������� !";
			else
			for($i=0; $i<count($window); $i++)
				{
					$mail_content.="<b><u># ".$window[$window_keys[$i]]['model']."</u></b><br><br>";
					$mail_content.="<b>�����: </b>".$window[$window_keys[$i]]['brand']."<br>";
					$mail_content.="<b>�������� �������: </b>".$window[$window_keys[$i]]['profile']."<br>";
					$mail_content.="<b>�����������: </b>".$window[$window_keys[$i]]['glass']."<br>";
					if($window[$i]['fill'] != "")
						$mail_content.="<b>���� ������: </b>".$window[$window_keys[$i]]['fill']."<br>";
					$mail_content.="<b>�������� ������: </b>".$window[$window_keys[$i]]['ledge_int']."<br>";
					$mail_content.="<b>������ ������: </b>".$window[$window_keys[$i]]['ledge_ext']."<br>";
					$mail_content.="<b>�������� �����: </b>".$window[$window_keys[$i]]['ins_screen_door']."<br>";
					$mail_content.="<b>�������� ��������: </b>".$window[$window_keys[$i]]['ins_screen_glass']."<br>";
					$mail_content.="<b>���� ��������: </b>".$window[$window_keys[$i]]['blinds_ext']."<br>";
					$mail_content.="<b>���� ������: </b>".$window[$window_keys[$i]]['blinds_int']."<br>";
					$mail_content.="<b>����: </b>".$window[$window_keys[$i]]['color']."<br>";
					
					$hardware_keys = array_keys($window[$window_keys[$i]]['hardware']);
					for($j=0; $j<count($window[$window_keys[$i]]['hardware']); $j++)
						$mail_content.= "<b>".$hardware_keys[$j].": </b>".$window[$window_keys[$i]]['hardware'][$hardware_keys[$j]]."<br>";
						
					$dimensions_keys = array_keys($window[$window_keys[$i]]['dimensions']);
					$mail_content.= "<b>�������: </b>";
					for($j=0; $j<count($window[$window_keys[$i]]['dimensions']); $j++)
						$mail_content.= $dimensions_keys[$j].": ".$window[$window_keys[$i]]['dimensions'][$dimensions_keys[$j]]."&nbsp;&nbsp;";
	
					$mail_content.="<hr>";							
					
				}
			
			$mail_content.="<h3>���������� �� �������</h3>";	
			$mail_content.= "<b>���: </b>".$client_name."<br>";
			$mail_content.= "<b>E-mail: </b>".$client_email."<br>";
			$mail_content.= "<b>������� �� ������: </b>".$client_phone."<br>";
			$mail_content.= "<b>���������: </b><br>".strip_tags($client_comments)."<br>";
			
			echo $mail_content;
		}
	
	$check_result = check_code_str($_POST['code'], $_POST['codestr']);
	
	if($_POST['submit_request'] == 1 || ($_POST['code'] != "" && !$check_result))
		{
				mk_output_message("warning", "�� �� ���������� ����������� ���� �������� ��������� ���!");
			?>
				<form method="post" action="page.php?n=<?=$n?>&amp;SiteID=<?=$SiteID?>" id="confirm_form">
				<table align="center" class="window_table" cellpadding="5" cellspacing="1" width="100%">
					<tr><th align="center">������������
					<tr><td align="center">
					<?php $code = rand(0, 65535); ?> 
					<img src="http://www.maksoft.net/gen.php?code=<?=$code?>" align="middle" vspace="5" hspace="5"><br>
					��� �� ���������:<br><input name="codestr" type="text" size="25">
					<input type="hidden" name="code" value="<?=$code?>">
					<input type="hidden" name="client_email" value="<?=$client_email?>">
					<input type="hidden" name="mail_content" value="<?=htmlspecialchars($mail_content)?>">
					<tfoot>
					<tr><td align="center" class="color2"><input type="submit" value="�������������">
					</tfoot>
				</table>
				</form>
			<?
		}
		else mk_output_message("normal", "��������� �� ���� �� ������� ������ �� ��������� ���� ��������� ���������� ������� ��-����. ��� �������� �� � �������� � �� �� ������ � ��� �� ���������.");

	if($check_result)
		{
			$mail->to = $o_site->get_sEMail();  
			//$mail->from =  $_POST['client_email'];
			$mail->from =  "web-request@dogramata.bg";
			$mail->subject =  "Request from Dogramata.bg";
			$mail->body =  $_POST['mail_content'];

			$mail->send();
			mk_output_message("normal", "������ ��������� � ��������� �������. ��� �������� �� �� �������� � �� �� ������ � ��� �� ���������.<br><b>���������� �� �� ��������� �������.</b>");
		}
	
	?>
	<form method="post" action="page.php?n=<?=$n?>&amp;SiteID=<?=$SiteID?>" id="request_form">
	
	<?php
	if($_POST['submit_request'] != 1 && $_POST['submit_request'] != 2)
	{
	for($i=0; $i<count($templates); $i++)
		{
			?>
				
				<table border="0" width="100%" class="window_table" cellspacing="1" cellpadding="5" id="window_<?=$i?>">
					<tr>
						<th colspan="2">�����: <?=$templates[$i]['model']?><input type="hidden" name="window[<?=$i?>][model]" value="<?=$templates[$i]['model']?>" class="model"></th>
						<th align="right">������ <input type="checkbox" id="checkbox_<?=$i?>" class="checkbox"></th>
					</tr>
					<tr>
						<td width="280px" valign="top">
						<a href="web/forms/dogramata/images/<?=$templates[$i]['image']?>" rel="lightbox[]"><img src="img_preview.php?image_file=web/forms/dogramata/images/<?=$templates[$i]['image']?>&img_width=280" border="0"></a>
						<td class="border" valign="top">
						�����:<br>
						<?php echo list_menu("window[$i][brand]", $window_brand, "id=\"brand_".$i."\""); ?><br>
						�������� �������:<br>
						<?php echo list_menu("window[$i][profile]", $window_profile['null'], "id=\"profile_".$i."\""); ?><br>
						�����������:<br>
						<?=list_menu("window[$i][glass]", $window_glass)?><br>
						<?php
							if($templates[$i]['extra_fill'] == 1)
							{
						?>
						���� ������ **:<br>
						<?=list_menu("window[$i][fill]", $window_fill)?><br>
						<?php
							}
						?>
						�������� ������:<br>
						<?=list_menu("window[$i][ledge_int]", $window_ledge_int)?><br>
						������ ������:<br>
						<?=list_menu("window[$i][ledge_ext]", $window_ledge_ext)?><br>
						<td class="border" valign="top">
						�������� �����:<br>
						<?=list_menu("window[$i][ins_screen_door]", $window_ins_screen_door)?><br>
						�������� ��������:<br>
						<?=list_menu("window[$i][ins_screen_glass]", $window_ins_screen_glass)?><br>
						���� ������:<br>
						<?=list_menu("window[$i][blinds_ext]", $window_blinds_ext)?><br>
						���� ��������:<br>
						<?=list_menu("window[$i][blinds_int]", $window_blinds_int)?><br>
						����:<br>
						<?=list_menu("window[$i][color]", $window_color)?><br>
					<tfoot>
					<tr>
						<td>
						<table cellpadding="0" cellspacing="0" width="100%"><tr>
						<?php
							$hardware = split(":", $templates[$i]['hardware']);
							for($j=0; $j<count($hardware); $j++)
								echo "<td align=\"center\">".$hardware[$j];
						?>
						</table>
						<td colspan="2">�������:
					<tr>
						<td class="color" align="center">
						<table cellpadding="0" cellspacing="1" width="100%"><tr>
						<?php
							$hardware = split(":", $templates[$i]['hardware']);
							for($j=0; $j<count($hardware); $j++)
								echo "<td class=\"color\" align=\"center\">".list_menu("window[$i][hardware][".$hardware[$j]."]", $window_hardware, "class=\"small\"")."<br>";
						?>
						</table>					
						<td colspan="2" class="color2">
						<?php 
							$dimensions = split(":", $templates[$i]['dimensions']);
							for($j=0; $j<count($dimensions); $j++)
								echo $dimensions[$j]."&nbsp;<input type=\"text\" size=\"5\" value=\"\" name=\"window[".$i."][dimensions][".$dimensions[$j]."]\" class=\"dimension\">&nbsp;&nbsp;";
						?>
					</tfoot>
				</table>
			<?
			
		
		}
?>
<table border="0" class="window_table" cellpadding="5" cellspacing="1">
	<tr>
		<th colspan="2">���������� �� �������</th>
	</tr>
	<tr>
		<td width="280" valign="top">��� *<br>
		  <input type="text" size="43" name="client_name" id="client_name">
	  <td rowspan="3" width="100%" valign="top">���������:<br><textarea cols="33" rows="7" name="client_comments"></textarea>
	</tr>
	<tr>
		<td valign="top">e-mail *<br>
		  <input type="text" size="43" name="client_email" id="client_email">
	</tr>
	<tr>
		<td valign="top">������� �� ������:<br>
		  <input type="text" size="43" name="client_phone">
	</tr>
	<tfoot>
	<tr align="center">
		<td colspan="2" class="color2"><input type="button" value="������� ���������" id="button_submit">
	</tr>
	</tfoot>
</table>
<input type="hidden" name="submit_request" value="1">
</form>
<?
}
?>