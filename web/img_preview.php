<?php

@$pic_name = $_GET['pic_name'];
@$pic_dir = $_GET['pic_dir'];

$image_file = $pic_name;
$img = imagecreatefromjpeg("$pic_dir/$image_file");
$image_width=imagesx($img); 
$image_height=imagesy($img);
$img_border = 1;
$border_bottom = 0; //botom offset

$new_image_max_size = 150+2*$img_border; // max image width

if($image_width > $image_height)
{
	$relation = $image_width/$new_image_max_size;
	$new_image_width = $new_image_max_size;
	$new_image_height = $image_height/$relation;
}
else
{
	$relation = $image_height/$new_image_max_size;
	$new_image_height = $new_image_max_size;
	$new_image_width = $image_width/$relation;
}

$new_image_width = $new_image_width+$img_border;
$new_image_height = $new_image_height+$img_border;

$new_image = imagecreatetruecolor($new_image_width, $new_image_height+$border_bottom);
$img_border_color = imagecolorallocate($new_image, 115,115, 115);

imagefill($new_image, 0, 0, $img_border_color); 
$dist_X = $img_border;
$dist_Y = $img_border;

imagecopyresampled($new_image, $img, $dist_X, $dist_Y, 0, 0, $new_image_width-(2*$img_border), $new_image_height-(2*$img_border), $image_width, $image_height);

Header('Content type: image/jpeg');
imagejpeg($new_image, '', $new_image_max_size);
?>