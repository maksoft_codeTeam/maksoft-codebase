<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>���������� ����</legend>

<div class="form-group row">
<!-- Text input-->
  <div class="col-md-6">
  <label class="col-md-4 control-label" for="check_in">���� �� �����������:</label>  
  <input id="check_in" name="check_in" type="text" placeholder="" class="col-md-8" required="">
    
  </div>

<!-- Text input-->
  <div class="col-md-6">
  <label class="col-md-4 control-label" for="check_out">���� �� ���������:</label>  
  <input id="check_out" name="check_out" type="text" placeholder="" class="col-md-8" required="">
  </div>
</div>

<!-- Select Basic -->
    <div class="form-group row">

      <div class="col-md-6">
        <label class="col-md-4 control-label" for="persons">���������:</label>
        <select id="persons" name="persons" class="form-control">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
        </select>
      </div>


      <div class="col-md-6">
        <label class="col-md-4 control-label" for="children">����:</label>
        <select id="children" name="children" class="form-control">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
        </select>
      </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-success">�����</button>
  </div>
</div>

</fieldset>
</form>
<script>
$j( function() {
      $j( "#check_in" ).datepicker(
        {
            showButtonPanel: true,
            dateFormat: 'dd.mm.yy'
        }
      );
      $j( "#check_out" ).datepicker(
        {
            showButtonPanel: true,
            dateFormat: 'dd.mm.yy'
        }
      );
});
var currentDate = $j( "#check_in" ).datepicker( "getDate" );
$j("#check_in").value(currentDate);
</script>
