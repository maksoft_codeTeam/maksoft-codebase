<?php
include 'helpers/helpers.php';
$__print_settings = array('text_limit'=>110, 'page_limit'=>3,
                          'button_text'=>'������� ������',
                          'default_page'=> $o_site->_site['news'],
                          'template' => 'bootstrap');

function print_news($settings)
{
    global $o_site;
    global $o_page;
    $templates = array('bootstrap'=> array('bootstrap_newsfeed-loop.html', 'bootstrap_newsfeed.html'));
    $_sub_pages = $o_page->get_pSubpages($settings['default_page'],
                                         "p.sort_n desc limit ".$settings['page_limit']);
        $loop_template = Template::load('web/views/templates/'.$templates[$settings['template']][0]);
    $__rend = '';
    for ($i=0; $i < count($_sub_pages); $i++) {
            $title = $_sub_pages[$i]["Name"];
            $content = substr(strip_tags($_sub_pages[$i]["textStr"]), 0, $settings['text_limit']);
            $img_src = $_sub_pages[$i]["image_src"];
            $img_alt = $_sub_pages[$i]["imageName"];
            $page_link = $_sub_pages[$i]["page_link"];
            $button_text = $settings['button_text'];
        $__rend .= Template::replace(get_defined_vars(), $loop_template);
    }
    $__templ = Template::load('web/views/templates/'.$templates[$settings['template']][1]);
    return str_replace("{{content}}", $__rend, $__templ);
}


function print_page($settings)
{
    global $o_site;
    global $o_page;
    $templates = array('bootstrap'=> array('bootstrap_newsfeed-loop.html', 'bootstrap_newsfeed.html'));
    $_sub_pages = $o_page->get_page($settings['default_page']);
    $loop_template = Template::load('web/views/templates/'.$templates[$settings['template']][0]);
    $title = $_sub_pages["Name"];
    $content = substr(strip_tags($_sub_pages["textStr"]), 0, $settings['text_limit']);
    $img_src = $_sub_pages["image_src"];
    $img_alt = $_sub_pages["imageName"];
    $page_link = $_sub_pages["page_link"];
    $button_text = $settings['button_text'];
    $__rend = Template::replace(get_defined_vars(), $loop_template);
    $__templ = Template::load('web/views/templates/'.$templates[$settings['template']][1]);
    return str_replace("{{content}}", $__rend, $__templ);
}
?>
