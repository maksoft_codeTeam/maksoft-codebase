<?php
include 'helpers/helpers.php';
include 'helpers/WideImage/WideImage.php';
define('STATIC_FOLDER', $_SERVER['DOCUMENT_ROOT'].'/web/views/static/');
$__print_settings = array('text_limit'=>140,
			  'page_limit'=>3,
			  'title_limit' => 40,
                          'button_text'=>'������� ������',
                          'default_page'=> $o_site->_site['news'],
                          'template' => 'bootstrap');
$templates = array('bootstrap'=> array('bootstrap_newsfeed-loop.html', 'bootstrap_newsfeed.html'));
function print_news($settings)
{
    global $o_site;
    global $o_page;
    global $templates;
    $_sub_pages = $o_page->get_pSubpages($settings['default_page'],
                                         "p.sort_n desc limit ".$settings['page_limit']);
    $loop_template = Template::load('web/views/templates/'.$templates[$settings['template']][0]);
    $__rend = '<div class="col-sm-4  col-md-4">';
    /*
     *  DEFAULT IMAGE
     */
    $default_image = 'web/views/mbus-default.jpg';
    for ($i=0; $i < count($_sub_pages); $i++) {
            $title = $_sub_pages[$i]["Name"];
	    if(strlen($title) > $__print_settings['title_limit'])
            $title = substr($title,0, $__print_settings['title_limit']-4).' ...';
	    else
            $title = str_pad($title, $__print_settings['title_limit'], ' ', STR_PAD_BOTH);
            $content = substr(strip_tags($_sub_pages[$i]["textStr"]), 0, $settings['text_limit']);
	    if(strlen($content) < $__print_settings['text_limit'])
            $content = str_pad($content,$__print_settings['text_limit'], ' ', STR_PAD_BOTH);
	    else
            $content = substr($content, 0, $__print_settings['text_limit']-4).' ...';
            $img_alt = $_sub_pages[$i]["imageName"];
            $page_link = $_sub_pages[$i]["page_link"];
            $page_link = $o_page->get_pLink($_sub_pages[$i]['n']);
            $button_text = $settings['button_text'];
	if($i%3)
	    $__rend .= '</div><div class="col-sm-4  col-md-4">';
    if(!$image = $_sub_pages[$i]['image_src']) {
        $image = $default_image;
    }
	$img_src = '/img_preview.php?image_file='.$image.'&amp;img_width=200';
        $__rend .= Template::replace(get_defined_vars(), $loop_template);
    }
    $__rend .= '</div>';
    $__templ = Template::load('web/views/templates/'.$templates[$settings['template']][1]);
    return str_replace("{{content}}", $__rend, $__templ);
}


function print_page($settings)
{
    global $o_site;
    global $o_page;
    global $__print_settings;
    global $templates;
    $_sub_pages = $o_page->get_page($__print_settings['default_page']);
    $loop_template = Template::load('web/views/templates/'.$templates[$__print_settings['template']][0]);
    $title = $_sub_pages["Name"];
    $content = substr(strip_tags($_sub_pages["textStr"]), 0, $__print_settings['text_limit']);
    $img_src = $_sub_pages["image_src"];
    $img_alt = $_sub_pages["imageName"];
    $page_link = $o_page->get_pLink($_sub_pages['n']);
    $button_text = $__print_settings['button_text'];
    $__rend = Template::replace(get_defined_vars(), $loop_template);
    $__templ = Template::load('web/views/templates/'.$templates[$__print_settings['template']][1]);
    return str_replace("{{content}}", $__rend, $__templ);
}
?>
