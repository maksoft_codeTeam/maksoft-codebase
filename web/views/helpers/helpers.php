<?php

class InvalidMethod extends Exception{}

class Template
{
    public static function load($path)
    {
        $h = fopen($path, 'rb');
        $template = fread($h, filesize($path));
        fclose($h);
        return $template;
    }

    public static function replace($vars, $template)
    {
        $data = Template::change_keys($vars);
        return str_replace(array_keys($data), $data, $template);
    }

    private static function change_keys($arr_data)
    {
        $tmp = array();
        foreach ($arr_data as $key => $value) {
            $tmp["{{".$key."}}"] = $value;
        }
        return $tmp;
    }

    public static function load_404($message)
    {
        $templ = Template::load(FILE_PATH.'views/404.html');
        return str_replace('{{message}}', $message, $templ);
    }
}


class Mixin
{
    public static function tomorrow($date)
    {
        $datetime = new DateTime($date);
        $datetime->modify('+1 day');
        return $datetime->format('d.m.Y');
    }
}
