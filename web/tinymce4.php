<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=vgzrspo9yv6rcjc9x6kh90ahdlwzqorexhqo0rek7kxjsf1b"></script>
  <script>
tinymce.init({
  selector: "textarea",
  height: 500,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css']
});
</script>

<textarea name="textStr">Next, get a free TinyMCE Cloud API key!</textarea>