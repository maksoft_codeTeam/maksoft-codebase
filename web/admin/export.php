<?php require_once $_SERVER['DOCUMENT_ROOT'].'/fak/base.php'; ?>
<?php
if($user->AccessLevel >= $row->SecLevel && $user->AccessLevel >0)
{
?>
<form method="post" name="archive">
<textarea cols="80" rows="20" name="archive_content">
<?php
$s_content = "<?xml version=\"1.0\" encoding=\"windows-1251\"?>";
$s_content.= "<maksoft>";

$s_pages = $o_page->db_query("SELECT * FROM pages WHERE SiteID='$SiteID'  LIMIT 5"); 
while ($row=$o_page->fetch("object", $s_pages)) {
   $s_content.= "
   <pages>
        <n>$row->n</n>
        <an>$row->an</an>
        <ParentPage>$row->ParentPage</ParentPage>
        <Name>$row->Name</Name>
		<title>$row->title</title>
		<tags>$row->tags</tags>
        <PHPvars>".htmlspecialchars($row->PHPvars)."</PHPvars>
        <textStr>".htmlspecialchars($row->textStr)."</textStr>
        <PHPcode>".htmlspecialchars($row->PHPcode)."</PHPcode>
        <toplink>$row->toplink</toplink>
        <show_link>$row->show_link</show_link>
        <show_link_order>$row->show_link_order</show_link_order>
        <show_link_cols>$row->show_link_cols</show_link_cols>
        <make_links>$row->make_links</make_links>
        <form>".htmlspecialchars($row->form)."</form>
        <PageURL>$row->PageURL</PageURL>
        <imageNo>$row->imageNo</imageNo>
        <imageWidth>$row->imageWidth</imageWidth>
        <image_allign>$row->image_allign</image_allign>
        <ankID>$row->ankID</ankID>
        <PrintVer>$row->PrintVer</PrintVer>
        <SiteID>$row->SiteID</SiteID>
        <date_added>$row->date_added</date_added>
        <date_modified>$row->date_modified</date_modified>
        <preview>$row->preview</preview>
		<addtype>$row->addtype</addtype>
        <SecLevel>$row->SecLevel</SecLevel>
        <sort_n>$row->sort_n</sort_n>
   </pages>
   "; 
  }
$s_content.="</maksoft>"; 
//echo $s_content;
echo $s_content;
?>
</textarea>
<a href="#" onClick="document.archive.submit()"><img src="web/admin/images/buttons/button_archive.png" border="0"></a>
<input type="hidden" name="write_archive" value="1">
</form>
<?php

	if($_POST['write_archive'] == 1)
		{
			$f_name = "pages_".$SiteID.".txt";
			$a_file = "web/pub/".$f_name;
			$fp = fopen($a_file, "w");
			fwrite($fp, $s_content);
			fclose($fp);
			//echo "<a href=\"page.php?n=".$n."&SiteID=".$SiteID."&df=".$f_name."\">DOWNLOAD FILE HERE !</a>";
			echo "<a href=\"".$a_file."\">DOWNLOAD FILE HERE !</a>";
		}
}
?>
