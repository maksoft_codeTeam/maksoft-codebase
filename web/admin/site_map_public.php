<?php require_once $_SERVER['DOCUMENT_ROOT'].'/fak/base.php'; ?>
<?php
//list site pages tree
function list_site_map($Site, $ParentPage, $user, $level=0)
{
	global $n, $SiteID, $count_subpages, $view_map_branches, $no;
    $o_page = page::getInstance();
	
	//check for previous declarations
	if(!isset($view_map_branches))
		$view_map_branches = true;
	
	if(!isset($count_subpages))
		$count_subpages = false;
					
	$query = "SELECT * FROM pages WHERE SiteID = '$Site->SitesID' AND ParentPage = '$ParentPage' AND status >= 0 ORDER by sort_n ASC";
	$result = $o_page->db_query($query);
	
	if(!$view_map_branches && $level >0)
		echo "<ul id=\"branch_".$ParentPage."\" style=\"display: none\">";
	else
		echo "<ul id=\"branch_".$ParentPage."\">";
		
	while($page_list = $o_page->fetch("object", $result))
		{
			
			$show_hide_str = "<img src=\"web/images/icons/empty.gif\"> ";
			$branches_counter_str = ""; 
			
			//check for page branches
			$branch_query = "SELECT * FROM pages WHERE ParentPage = '$page_list->n' AND SiteID = '$page_list->SiteID' AND status >= 0";
			$branch_result = $o_page->db_query($branch_query);
			
			if($o_page->db_count($branch_result) !=0)
				{
					if($count_subpages)
						$branches_counter_str = "(".$o_page->db_count($branch_result).")";
					if($view_map_branches)
						$show_hide_str = "<a href=\"javascript: show_hide('branch_".$page_list->n."')\"><img src=\"web/images/icons/minus.gif\"></a> ";
					else $show_hide_str = "<a href=\"javascript: show_hide('branch_".$page_list->n."')\"><img src=\"web/images/icons/plus.gif\"></a> ";
				}
			
			if($user->ReadLevel > $page_list->SecLevel)
				echo "<li>".$show_hide_str ."<a href=\"page.php?n=$page_list->n&SiteID=$page_list->SiteID\" class=\"level$level\">".$page_list->Name."</a> ".$branches_counter_str."</li>"; 

			if($o_page->db_count($branch_result) !=0 && $level <5 )
					list_site_map($Site, $page_list->n, $user, $level+1);

		}

	echo "</ul>";
}


	echo "<div id=\"sitemap\">";	
	//list public sitemap
	list_site_map($Site, $Site->StartPage, $user, 0);
	echo "</div>";
?>
