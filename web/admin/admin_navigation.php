<?php
     if(isset($tmpl_config_support) and count($tmpl_config_support)>0)
        {
            ?>
            
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">

<link rel="stylesheet" href="/lib/jquery-ui/themes/base/jquery.ui.all.css">
<!--<script src="/lib/jquery-ui/ui/jquery.ui.core.js"></script>-->
<!--<script src="/lib/jquery-ui/ui/jquery.ui.widget.js"></script>-->
<script src="/lib/jquery-ui/ui/jquery.ui.mouse.js"></script>
<script src="/lib/jquery-ui/ui/jquery.ui.draggable.js"></script>
<script src="/lib/jquery-ui/ui/jquery.ui.slider.js"></script>
<script src="/lib/jquery-ui/ui/jquery.ui.resizable.js"></script>

<script language="javascript" type="application/javascript">
    
    function set_message(msg)
        {
            $j('#message').html(msg)    
        }
        
    $j(document).ready(function(){
        
        
        $j('#cms_admin_navigation').draggable();
        $j('#slide_line').click(function(){
            $j("div.cms_admin_content").toggle(300);
            })

        //change CSS color scheme
        $j("a.color_scheme").each(function(i){
                $j(this).click(function(){
                    $j("link#base-style").attr("href",$j(this).attr('rel'));
                    //set_message($j(this).attr('rel'));
                    $j('#SITE_CSS_ID').val($j(this).attr('id'));
                })
            })
        
        //change the layout
        $j("a.layout").each(function(i){
                $j(this).click(function(){
                    $j("link#layout-style").attr("href",$j(this).attr('rel'));
                    $j("a.layout").removeClass("selected");
                    $j(this).addClass("selected");
                    //set_message($j(this).attr('rel'));
                    $j('#TMPL_CSS_LAYOUT').val($j(this).attr('rel'));
                })
            })

            
        $j("link#base-style").attr("href",$j("#site_css option:selected").attr('rel')); 
        //change CSS style
        $j('#site_css').change(function(){
                $j("link#base-style").attr("href",$j("#site_css option:selected").attr('rel'));
            })
        //set banner
        $j('#page_banner').change(function(){
                $j(".banner img.banner-image").attr("src", "img_preview.php?image_file="+$j("#page_banner").find("option:selected").attr('rel')+"&img_width=960&ratio=strict");
            })
        
        //set background
        $j('#body_background').change(function(){
                $j("body").attr("style", "background: url("+$j("#body_background").find("option:selected").attr('rel')+")");
                $j('#header').css("background", "transparent");
                $j('#header').css("box-shadow", "none");
                $j('#footer').css("background", "transparent");
                $j('#footer').css("box-shadow", "none");
                $j('#page_container').css("background", "#FFF");
                $j('#page_container').css("padding", "15px 0");
            })      
            
        //set site max width
        $j('#CMS_MAX_WIDTH').change(function(){
                $j('#page_container').css("width", $j(this).val());
                $j('#header .header-content').css("width", $j(this).val());
                $j('#footer .footer-content').css("width", $j(this).val());
            })  

        //site dimmensions
        var main_width = parseInt($j("#pageContent").css("width").replace("px", ""));
        if($j("#column_left"))
            var column_left_width = parseInt($j("#column_left").css("width").replace("px", ""))+ parseInt($j("#column_left").css("margin").replace("px", ""));
        if($j("#column_right"))
            var column_right_width = parseInt($j("#column_right").css("width").replace("px", "")) + parseInt($j("#column_right").css("margin").replace("px", ""));
        var pageContent_margin = $j('#pageContent').css("margin-left");

        //set site width
        $j("#w_slider" ).slider({
                    value: $j("#CMS_MAX_WIDTH").val(),
                    min: 600,
                    max: 1200,
                    step: 10,
                    slide: function( event, ui ) {
                        $j( "#CMS_MAX_WIDTH" ).val( ui.value );
                        $j('#page_container').css("width", ui.value);
                        $j('#header .header-content').css("width", ui.value);
                        $j('#footer .footer-content').css("width", ui.value);
                        var new_main_width = parseInt($j("#pageContent").css("width").replace("px", "")) - parseInt($j("#pageContent .page-content").css("padding").replace("px", ""))*2;
                        $j("#CMS_MAIN_WIDTH").val(new_main_width);
                    }
                });
                //$j("#CMS_MAX_WIDTH").val($j("#w_slider").slider( "value" ));

        //set content width
        if($j("#CMS_MAIN_WIDTH").val() == "")
            $j("#CMS_MAIN_WIDTH").val(main_width);
        /*
        $j("#main_slider" ).slider({
                    value: main_width,
                    min: 450,
                    max: $j("#CMS_MAX_WIDTH").val(),
                    step: 10,
                    slide: function( event, ui ) {
                        $j( "#CMS_MAIN_WIDTH" ).val( ui.value );
                        $j('#pageContent').css("width", ui.value + "px");
                        var  page_container_width = parseInt(ui.value) + parseInt(column_left_width) + parseInt(column_right_width) - parseInt(pageContent_margin);
                        $j('#page_container').css("width",  page_container_width + "px");
                        $j('#header .header-content').css("width", ui.value);
                        $j('#footer .footer-content').css("width", ui.value);
                        
                    }
                });
            */

        /*
        $j('#column_left').resizable({
            resize: function(event, ui){
                    var w = ui.size['width'];
                    
                }
            });
        */
        //set font size
        $j("#fs_slider").slider({
                    value: $j('#CSS_FONT_SIZE').val(),
                    min: 0.8,
                    max: 1.5,
                    step: 0.1,
                    slide: function( event, ui ) {
                        $j('#CSS_FONT_SIZE').val( ui.value );
                        $j('#pageContent p').css("font-size", ui.value+"em");
                        }
            });
        //$j('#CSS_FONT_SIZE').val($j("#fs_slider").slider("value"));
        
        <?php
        if($tmpl_config_support['change_logo'])
        {
        ?>
        //set logo position
        $j('#header .header-content .logo').draggable({
            containment: "#header .header-content .banner",
            grid: [5,5],
            drag: function(event, ui){
                $j('#TMPL_LOGO_POSITION').val(ui.position['left'] + "x" + ui.position['top']);
                $j('#text_logo_position').text(ui.position['left'] + "x" + ui.position['top']);
                $j(this).css("margin", "0");
                },
            scroll: false
            });
        
        //set logo dimensions
        $j('#text_logo_size').text($j('#header .logo img').width()+' px');
        $j('#header .logo').hover(function(){
            var logo_w = $j('#header .logo img').width();
            var logo_h = $j('#header .logo img').height();

            $j('#header .logo img').resizable({
                aspectRatio: logo_w / logo_h,
                maxHeight: 200,
                maxWidth: 350,
                minHeight: 50,
                minWidth: 100,
                grid: 5,
                helper: "ui-resizable-helper",
                resize: function(event, ui){
                    $j('#TMPL_LOGO_SIZE').val(ui.size['width']);
                    $j('#text_logo_size').text(ui.size['width']+' px');
                    },
                stop: function(event, ui){
                    $j('#header .logo img').resizable("destroy");
                    } 
                });
            })
            $j('#TMPL_LOGO_SIZE').change(function(){
                    $j('#header .logo img').width($j(this).val());
                })
            <?
            }
            ?>  
            //toggle boxes
            $j(".label").each(function(){
                
                $j("#"+$j(this).attr("rel")).hide();
                
                $j(this).click(function(){
                    $j("#"+$j(this).attr("rel")).toggle(100);
                    //alert($j(this).attr("rel"));
                    })
                })
        })
</script>
<style type="text/css">
#cms_admin_navigation {cursor: move; display: block; max-width:200px; box-shadow: 0px 0px 5px 1px #000; -moz-box-shadow: 0px 0px 5px 1px #000; -webkit-box-shadow: 0px 0px 5px 1px #000; float: left; position: absolute; z-index:100; top: 10px; left: 10px; background: #444444; color: #999; text-align: left; margin: 0px; padding: 10px;}
#cms_admin_navigation select {width: 100%;}
#cms_admin_navigation .cms_admin_content {min-width: 200px;}
#slide_line {display: block; float: left; width: 20px; height: 20px; background: #999; color: #FFF; text-align:center; line-height:20px; clear:both; text-decoration:none; font-weight:bold;}
.color_scheme {display: block; float:left; background: #FFF; padding: 5px; border: 1px solid #CCC; margin: 2px}
.color_scheme .color {display: block; float: left; margin: 1px; width: 15px; height: 15px;}
.layout {display: block; width: 70px; height: 50px; float: left; margin: 2px; border: 2px solid #000; background-image: url(https://www.maksoft.net/web/admin/images/tmpl_layouts.jpg); background-repeat: no-repeat;}
.layout:hover, .layout.selected {border: 2px solid #FC0;}
form span.label {color: #CCC; border-bottom: 1px solid #CCC; cursor: pointer; display:block;}
.ui-resizable-helper { border: 2px dotted #F00; }

.layout.tmpl-01{background-position: 0% 0%;}
.layout.tmpl-02{background-position: 25% 0%;}
.layout.tmpl-03{background-position: 50% 0%;}
.layout.tmpl-04{background-position: 75% 0%;}
.layout.tmpl-05{background-position: 0% 50%;}
.layout.tmpl-06{background-position: 25% 50%;}
.layout.tmpl-07{background-position: 50% 50%;}
.layout.tmpl-08{background-position: 75% 50%;}

</style>
<?php
    
    //reset template cofigurations
    if(isset($_POST['action']) and $_POST['action'] == "reset_templ_config")
        {
            $o_site->reset_sConfigurations();   
        }
        
    //save template configurations 
    if(isset($_POST['action']) and $_POST['action'] == "save_templ_config")
        {
            $o_site->print_sConfigurations();
            //print_r($_POST);
            
            if(isset($_POST['SITE_CSS_ID']))
                {
                    $site_upadate_sql = "UPDATE Sites SET css = '".$_POST['SITE_CSS_ID']."' WHERE SitesID = '".$o_site->_site['SitesID']."' LIMIT 1";
                    //echo $site_upadate_sql;
                    $o_page->db_query($site_upadate_sql);
                }

            //set template layout
            if(isset($_POST['TMPL_CSS_LAYOUT']) && $_POST['TMPL_CSS_LAYOUT'])
                {
                    //$css_content.= "body {background: url(\"".$body_background."\") 50% 0% no-repeat;}";
                    $o_site->set_sConfiguration("TMPL_CSS_LAYOUT", $_POST['TMPL_CSS_LAYOUT'], $o_site->SiteID, $o_site->_site['Template']);
                
                }
                            
            //set site width
            if(isset($_POST['CMS_MAX_WIDTH']))
                {
                    $o_site->set_sConfiguration("CMS_MAX_WIDTH", $_POST['CMS_MAX_WIDTH'], $o_site->SiteID, $o_site->_site['Template']);
                }

            //set page width
            if(isset($_POST['CMS_MAIN_WIDTH']))
                {
                    $o_site->set_sConfiguration("CMS_MAIN_WIDTH", $_POST['CMS_MAIN_WIDTH'], $o_site->SiteID, $o_site->_site['Template']);
                }
                                            
            //set site banner
            if(isset($_POST['banner']) && $_POST['banner'] >0)
                {
                    $site_upadate_sql = "UPDATE pages SET imageNo = '".$_POST['banner']."', image_allign = '21' WHERE n = '".$o_site->StartPage."' LIMIT 1";
                    //echo $site_upadate_sql;
                    $o_page->db_query($site_upadate_sql);
                }
            
            //set template background
            if(isset($_POST['TMPL_BODY_BACKGROUND']) && $_POST['TMPL_BODY_BACKGROUND'])
                {
                    //$css_content.= "body {background: url(\"".$body_background."\") 50% 0% no-repeat;}";
                    $o_site->set_sConfiguration("TMPL_BODY_BACKGROUND", $_POST['TMPL_BODY_BACKGROUND'], $o_site->SiteID, $o_site->_site['Template']);
                
                }
                
            //set logo position
            if(!empty($_POST['TMPL_LOGO_POSITION']) && $_POST['TMPL_LOGO_POSITION'])
                {
                    $o_site->set_sConfiguration("TMPL_LOGO_POSITION", $_POST['TMPL_LOGO_POSITION'], $o_site->SiteID, $o_site->_site['Template']);   
                }
                
            //set font size
            if(!empty($_POST['CSS_FONT_SIZE']))
                {
                    $o_site->set_sConfiguration("CSS_FONT_SIZE", $_POST['CSS_FONT_SIZE'], $o_site->SiteID, $o_site->_site['Template']); 
                }
            
            //set logo size
            if(!empty($_POST['TMPL_LOGO_SIZE']))
                {
                    $o_site->set_sConfiguration("TMPL_LOGO_SIZE", $_POST['TMPL_LOGO_SIZE'], $o_site->SiteID, $o_site->_site['Template']);   
                }
                
            //upload logo
            if(strlen($_FILES['site_logo']['name'])>=5)
                {
                    include "web/admin/upload_file.php";
                    //print_r($_FILES['site_logo']);
                    $imageNo = upload_file($imageID, "Logo", $Site, $_FILES['site_logo']['name'], $_FILES['site_logo']['tmp_name'], $_FILES['site_logo']['size'], $_FILES['site_logo']['type'], 2, $o_site->get_sImageDir('ID'));   
                    $o_site->db_query("UPDATE Sites SET Logo = '".$_FILES['site_logo']['name']."' WHERE SitesID = '".$o_site->SiteID."' LIMIT 1");
                }
            //remove logo
            if($_POST['remove_logo'])
                $o_site->db_query("UPDATE Sites SET Logo = '' WHERE SitesID = '".$o_site->SiteID."' LIMIT 1");
        }
?>


<div id="message"></div>
<div id="cms_admin_navigation">
<a href="#" id="slide_line">X</a><br clear="all">
<div class="cms_admin_content" style="float: left; display: none;">
            <form method="post" enctype="multipart/form-data">
            <?php
                    //layout roller
                    if($tmpl_config_support['change_layout'])
                    {
                        ?>
                        <span class="label" rel="box_layout">������</span><br clear="all">
                        <div id="box_layout">
                            <a href="#" class="layout tmpl-01" rel="/Templates/base/tmpl_001/layout_001.css"></a>
                            <a href="#" class="layout tmpl-05" rel="/Templates/base/tmpl_001/layout.css"></a>
                            <a href="#" class="layout tmpl-02" rel="/Templates/base/tmpl_001/layout_002.css"></a>
                            <a href="#" class="layout tmpl-06" rel="/Templates/base/tmpl_001/layout_006.css"></a>
                            <input type="hidden" value="0" id="TMPL_CSS_LAYOUT" name="TMPL_CSS_LAYOUT">
                        <br clear="all">
                        </div>
            
            <?php
                    }
                    
                //theme roller
                if($tmpl_config_support['change_css'])
                {
                    ?>
                    <span class="label" rel="box_color_scheme">������� ����</span><br clear="all">
                    <div id="box_color_scheme">
                    <?php 
                     $result_css = mysqli_query("SELECT * FROM CSSs WHERE (css_group='".$Template->ID."' || cssID = '".$Template->default_css."') AND (status = 1 || status = ".$o_site->_site['SitesID'].") ORDER BY cssID ASC");
                     while($row_css = mysqli_fetch_object($result_css))
                        {
                            $colors = split(";", $row_css->color_scheme);
                            echo "<a href=\"javascript: void()\" class=\"color_scheme\" id=\"".$row_css->cssID."\" title=\"".$row_css->cssName .": ".$row_css->cssID."\" rel=\"".$row_css->css_url."\">";
                            for($i=0; $i<count($colors); $i++)
                                echo "<div class=\"color\" style=\"background: ".$colors[$i]."\"></div>";
                            echo "</a>";
                        }
                    ?>
                    </div>
                    <input type="hidden" name="SITE_CSS_ID" id="SITE_CSS_ID" value="<?=$o_site->_site['css']?>">
                    <br clear="all">                    
                    <?
                }
                
                 if($tmpl_config_support['change_banner'])
                   {
                        ?>
                        <span class="label" rel="box_banner">�������� �����</span><br clear="all"> 
                        <div id="box_banner">
                        <select name="banner" id="page_banner">
                        <?php
                        //get current site banner
                        $p_banner =   $o_page->get_pBanner();
                        echo "<option rel=\"".$p_banner."\" value=\"0\" selected>- select banner -</option>";
                        $banners = $o_site->get_sImages(444);
                        for($i=0; $i<count($banners); $i++)
                            {
                                $selected = "";
                                if($banners[$i]["imageID"] == $o_page->get_pInfo("imageNo", $o_site->_site['StartPage']))
                                    $selected = "selected";
                                echo "<option rel=\"".$banners[$i]["image_src"]."\" value=\"".$banners[$i]["imageID"]."\" $selected>".$banners[$i]["imageName"]."</option>";
                            }
                        ?>
                        </select>
                        <br clear="all">
                        </div>
                        
                <?
                   }
                 
                 if($tmpl_config_support['change_background'])
                     {
                        ?>
                        <span class="label" rel="box_background">��� �� �����</span><br clear="all"> 
                         <div id="box_background">
                         <select name="TMPL_BODY_BACKGROUND" id="body_background">
                          <option rel="" value=0>- �������� ��� -</option>
                          <?php
                            $backgrounds = $o_site->get_sImages(977);
                            for($i=0; $i<count($backgrounds); $i++)
                                echo "<option rel=\"".$backgrounds[$i]["image_src"]."\" value=\"".$backgrounds[$i]["image_src"]."\">".$backgrounds[$i]["imageName"]."</option>";
                          ?>
                          </select>
                         <br clear="all">
                         </div>
                        
                        <?
                     }
                     
                 // SITE DIMMENSIONS
                 if($tmpl_config_support['change_max_width'] || $tmpl_config_support['change_menu_width'] || $tmpl_config_support['change_main_width'])
                     {
                          ?>
                          <span class="label" rel="box_dimmensions">������� �� �����</span><br clear="all"> 
                          <div id="box_dimmensions">
                          <ol>
                          <?
                        
                        // MAX WIDTH
                         if($tmpl_config_support['change_max_width'])
                             { 
                                ?>   
                                <li><span class="label" rel="box_w_slider">������ ����</span><br clear="all"></li>
                                <div id="box_w_slider">
                                    <div id="w_slider"></div>
                                    <input type="text" name="CMS_MAX_WIDTH" id="CMS_MAX_WIDTH" value="<?=$o_site->get_sConfigValue("CMS_MAX_WIDTH")?>">
                                    <br clear="all">  
                                </div>
                                <?
                             }
                             
                        // MENU WIDTH
                         if($tmpl_config_support['change_menu_width'])
                             { 
                                ?>   
                                <li><span class="label" rel="box_menu_slider">������ ����</span><br clear="all"></li>
                                <div id="box_menu_slider">
                                    <div id="menu_slider"></div>
                                    <input type="text" name="CMS_MENU_WIDTH" id="CMS_MENU_WIDTH" value="<?=$o_site->get_sConfigValue("CMS_MENU_WIDTH")?>">
                                    <br clear="all">  
                                </div>
                                <?
                             }

                        // MAIN WIDTH
                         if($tmpl_config_support['change_main_width'])
                             { 
                                ?>   
                                <li><span class="label" rel="box_main_slider">������ ����������</span><br clear="all"></li>
                                <div id="box_main_slider">
                                    <div id="main_slider"></div>
                                    <input type="text" name="CMS_MAIN_WIDTH" id="CMS_MAIN_WIDTH" value="<?=$o_site->get_sConfigValue("CMS_MAIN_WIDTH")?>">
                                    <br clear="all">  
                                </div>
                                <?
                             }

                             ?>
                             </ol>
                             </div>
                             <?
                     }
                     
                // FONTIZE
                 if($tmpl_config_support['change_fontsize'])
                     {
                        ?>  
                        <span class="label" rel="box_fs_slider">�������� �� ������</span><br clear="all"> 
                        <div id="box_fs_slider">
                        <div id="fs_slider"></div>
                        <input type="text" name="CSS_FONT_SIZE" id="CSS_FONT_SIZE" value="<?=$o_site->get_sConfigValue('CSS_FONT_SIZE')?>">
                        <br clear="all"> 
                        </div> 
                        <?
                     }
            
                 if($tmpl_config_support['change_logo'])
                     {
                        ?>  
                            <span class="label" rel="box_logo">����</span><br clear="all"> 
                            <div id="box_logo">
                            <img src="<?=$tmpl_config['default_logo']?>" width="180">
                            <br clear="all">
                            <span id="text_logo_position">0x0</span> / <span id="text_logo_size"></span>
                            <input type="hidden" name="TMPL_LOGO_POSITION" id="TMPL_LOGO_POSITION" value="<?=$o_site->get_sConfigValue("TMPL_LOGO_POSITION")?>">
                            <input type="hidden" name="TMPL_LOGO_SIZE" id="TMPL_LOGO_SIZE" value="">
                            <input type="checkbox" name="remove_logo" value="1"> �������� ������
                            <br clear="all">
                            <input type="file" name="site_logo" style="width: 90%">
                            </div>
                        <?
                     }
                ?>
                <hr>
                  <input type="hidden" name="action" value="save_templ_config">
                  <input type="submit" value="save">
                  </form>
                  <form method="post">
                  <input type="hidden" name="action" value="reset_templ_config"> 
                  <input type="submit" value="reset">
                  </form>
</div>
</div>
                <?
            }
    //admin menu
     
?>
