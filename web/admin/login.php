<?php
error_reporting(E_ALL);
ini_set("display_errors", 1); 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
require_once __DIR__.'/../../loader.php';
require_once __DIR__.'/../../lib/messages.php';
$_SESSION = array();

$username = $request->request->get('username');
$pass = $request->request->get('pass');
$templ = $request->query->get('templ', false);

$url_query = array();
$redirect_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER']: $o_page->scheme.$o_page->_site['primary_url'];
$response = new RedirectResponse($redirect_url , Code::HTTP_MOVED_PERMANENTLY);

if($request->server->get("REQUEST_METHOD") === 'GET') {
    Message\error('��������� ����');
    $response->setStatusCode(Code::HTTP_METHOD_NOT_ALLOWED);
    $response->send();
    exit();
}

if (!(stristr("#", "$username")) && (strlen($username)<=20) ) {   
    $result = $o_page->db_query("SELECT * FROM users WHERE username= BINARY('$username')");
} else {
    Message\error('��������� ������������� ���');
    $response->send();
}

if ($o_page->db_count($result) > 1) {
    Message\error('��������� ������������� ���');
    $response->send();
    exit();
}

$result = $o_page->db_query("SELECT 
   users.ID, users.Name, users.EMail, users.username, users.pass, users.InitPage, users.InitSiteID, users.AccessLevel, 
    Firmi.Name as FName, Firmi.sl, Firmi.RA, Firmi.Phone, Firmi.Fax, Firmi.Address, Firmi.MOL, Firmi.dn, Firmi.bulstat
    FROM users
    LEFT JOIN Firmi ON users.FirmID = Firmi.ID
    WHERE users.username= BINARY('$username') AND pass= BINARY('$pass')");

if ($o_page->db_count($result) < 1) {
    Message\error('������ ������������� ��� ��� ������');
    $response->setStatusCode(Code::HTTP_MOVED_PERMANENTLY);
    $response->send();
    exit();
}

$row_user = $o_page->fetch("object", $result); 
$_SESSION['user'] = $row_user;

if ($templ) {
    $url_query['templ'] = $templ;
}

$n=$row_user->InitPage;
$SiteID=$o_page->_site['SitesID'];
$exist_query = "SELECT * FROM Sites WHERE SitesID='$SiteID' ";
$exist = $o_page->db_count($o_page->db_query($exist_query));

if($exist == 0) {
    $site_access = $o_page->db_query("SELECT * FROM SiteAccess sa left join Sites s on sa.SiteID=s.SitesID WHERE userID = '$row_user->ID' ORDER by sa.SiteAccNo DESC LIMIT 1 ");
    $user_access = $o_page->fetch("object", $site_access);
    if($user_access) {
        $o_page->db_query("UPDATE users SET InitSiteID = '$user_access->SiteID', InitPage=11 WHERE ID='$user_access->userID' ");
        $exist = 1;
        $n = $user_access->StartPage;
        $SiteID = $user_access->SiteID;
    }
} 

if($request->request->get('n', false)) {
    $n = $_POST['n'];
}

if($request->request->get('SiteID', false)) {
    $SiteID = $_POST['SiteID'];
}

$url_query['n'] = $n;
$url_query['SiteID'] = $SiteID ? $SiteID : $row_user->InitSiteID;

$o_page->db_query("DELETE FROM user_logs WHERE  logTime<=DATE_SUB(CURDATE(),INTERVAL 366 DAY);"); 
 
$today = date("YmdHis");
$ip = GetHostByName($_SERVER['REMOTE_ADDR']);
$o_page->db_query("INSERT INTO user_logs SET user='$row_user->ID', IP='$ip',  logTime='$today', n='$n', SiteID='$SiteID' ");
$res = $o_page->db_query("SELECT * FROM sl WHERE userID = '$row_user->ID' ");
if($sl_row = $o_page->fetch("object", $res)) {
    $res = $o_page->db_query("SELECT * FROM sl_work WHERE userID = '$row_user->ID' AND ((end_time is NULL AND start_time > DATE_SUB(NOW(), INTERVAL 1 HOUR)) OR end_time > DATE_SUB(NOW(), INTERVAL 3 HOUR)) ");
    if($o_page->db_count($res) == 0) {
        $o_page->db_query("INSERT INTO sl_work SET userID = '$row_user->ID',  start_time = NOW() ");
    }
}

Message\success('������� ����');
$response = new RedirectResponse('/page.php?'.http_build_query($url_query) , Code::HTTP_FOUND);
$response->send();
?>

