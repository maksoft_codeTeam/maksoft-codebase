<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
require_once __DIR__.'/../../modules/vendor/autoload.php';
require_once __DIR__.'/../../lib/lib_calendar.php';
require_once __DIR__.'/../../global/admin/CreatePageForm.php';
require_once __DIR__.'/../../global/admin/AddPriceForm.php';
if(!isset($di_containere)) {
    $di_container = new \Pimple\Container();
    $di_container->register(new \Maksoft\Containers\GlobalContainer());
}
$page = (object) CMSTranslation::$emptyPage;
$page->SiteID = $o_page->_site['SitesID'];
$gate = $di_container['gate'];
$priceForm = new AddPriceForm($gate, null, null, $_POST);

if(isset($_GET['no'])) {
    $temp_page = $gate->page()->getPage($_GET['no']);
    if($temp_page->SiteID == $o_page->_site['SitesID']) {
        $page = $temp_page;
    }
}
$p_parent = $gate->page()->getPage(!empty($page->ParentPage) ? $page->ParentPage : $o_page->_site['StartPage']);
$createPageForm = new CreatePageForm($di_container['gate'], $page, $o_page, $_POST, $_FILES);
if($_SERVER['REQUEST_METHOD'] === 'POST' ){
    try {
        $createPageForm->is_valid();
        $n = $createPageForm->save();
        $prices = array_filter($request->request->get('price', array()), function($e) {
            return !empty($e['code']);
        });
        foreach($prices as $price) {
            $p = new AddPriceForm($gate, $n, $page->SiteID, $price);
            if($p->is_valid()) {
                $p->save();
            }
        }
    } catch (\Exception $e) {
        print_r($e->getMEssage());
    }
}

/*
 * ������� �� �������
 */
$img_dirs = array();
array_map(function($dir) use (&$img_dirs) {
    $img_dirs[$dir["ID"]] = $dir['Name'];
}, $o_site->get_sImageDirectories());

$pageGroups = $gate->site()->getSiteGroups(empty($page->SiteID) ? $o_page->_site['SitesID'] : $page->SiteID);
foreach ($pageGroups as $group) {
    $groups[$group->group_id] = $group->group_title;
}

$pTemplate = $o_page->get_pTemplate($no);
$templates = array();
$template_id = isset($pTemplate['pt_id']) ? $pTemplate['pt_id'] : null;
foreach($o_page->get_pTemplates() as $pTemplate) {
    $templates[$pTemplate['pt_id']] = $pTemplate['pt_title'];
}

$siteAddons = $gate->site()->getSiteAddons($o_site->_site['SitesID']);
$addons = array();
foreach($siteAddons as $addon) {
    $addons[$addon->ID] = $addon->Name;
}

$currencies = array();
foreach ($gate->page()->getCurrencyList() as $c) {
    $currencies[$c->currency_id] = $c->currency_key .', '.$c->currency_string;
}
$pagePrices = $gate->page()->getPagePrice($page->n);
$pagePrices = array_merge($pagePrices ? $pagePrices : array(), array(0 => array()));
?>
<?=$createPageForm->start();?>

<div class='container'>
    <div class='row'>
        <div class="row">
            <div class="col-md-6">
                <label for=" <?=$createPageForm->Name->name?>"><?=$createPageForm->Name->label?></label>
                <?=$createPageForm->Name?>
                <?=$createPageForm->Name->print_errors()?>
            </div>
            <div class="col-md-6">
                <label for=" <?=$createPageForm->ParentPage->name?>"><?=$createPageForm->ParentPage->label?></label>
                <div  id="ParentPage">
            <?php if(!isset($_GET['no']) && $SiteID == $p_parent->SiteID):?>
                <input class='<?=$createPageForm->ParentPage->class?>' type="text" value="<?=$p_parent->Name?>" disabled>
                <input type="hidden" name="ParentPage" value="<?=$p_parent->n?>">
            <?php elseif($no == $Site->StartPage): ?>
                  <input class='<?=$createPageForm->ParentPage->class?>' type="text" value="- no parent page -" disabled>
                  <input type="hidden" name="ParentPage" value="0">
            <?php else: ?>
                   <select class='<?=$createPageForm->ParentPage->class?>'
                           name="<?=$createPageForm->ParentPage->name?>"
                           id="<?=$createPageForm->ParentPage->id?>"
                           onChange="<?=$createPageForm->ParentPage->onchange?>()">
                       <option value="<?=$page->ParentPage?>"><?=$o_page->get_pName($page->ParentPage)?></option>
                       <option value="<?=$p_parent->ParentPage?>"> - ������ ��������� ��������-</option>
                   </select>
            <?php endif;?>
                </div>
                <?=$createPageForm->ParentPage->print_errors()?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for=" <?=$createPageForm->title->name?>"><?=$createPageForm->title->label?></label>
                <?=$createPageForm->title?>
                <?=$createPageForm->title->print_errors()?>
            </div>

            <div class="col-md-6">
                <label for=" <?=$createPageForm->slug->name?>"><?=$createPageForm->slug->label?></label>
                <?=$createPageForm->slug?>
                <?=$createPageForm->slug->print_errors()?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for=" <?=$createPageForm->textStr->name?>"><?=$createPageForm->textStr->label?></label>
                <?php $createPageForm->textStr->class .= ' '.$user->WriteLevel >=4 ? " tinymce4-admin" : " tinymce4-user"; ?>
                <textarea class="<?=$createPageForm->textStr->class?>" name="<?=$createPageForm->textStr->name?>"> <?=$createPageForm->textStr->value?> </textarea>
                <?=$createPageForm->textStr->print_errors()?>
            </div>
            <div class='col-md-12 '>
                <div class='checkbox'>
                    <label for=" <?=$createPageForm->notags->name?>"><?=$createPageForm->notags->label?></label>
                    <?=$createPageForm->notags?>
                    <?=$createPageForm->notags->print_errors()?>
                </div>
            </div>
            <div class='col-md-12'>
                <label for=" <?=$createPageForm->tags->name?>"><?=$createPageForm->tags->label?></label>
                <?=$createPageForm->tags?>
                <?=$createPageForm->tags->print_errors()?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <a href="#" class="thumbnail">
                    <?php $base_image = $page->image_src ? $page->image_src : 'https://st3.depositphotos.com/1654249/12728/i/170/depositphotos_127280100-stock-photo-blank-hexagon-signboards-on-white.jpg'; ?>
                   <img id='preview_image' src="<?=$base_image?>" alt="...">
                </a>
                <a href="/web/admin/includes/images_manager.php?n=<?=$page->n?>&amp;SiteID=$page->SiteID"
                   class="btn btn-info"
                       title="������� �� ������������� �� �������� <?=$page->Name?>" id="change_image_link">�����
                </a>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6">
                        <label for=" <?=$createPageForm->userFile->name?>"><?=$createPageForm->userFile->label?></label>
                        <?=$createPageForm->userFile?>
                        <?=$createPageForm->userFile->print_errors()?>
                    </div>
                    <div class="col-md-6">
                        <label for=" <?=$createPageForm->imageName->name?>"><?=$createPageForm->imageName->label?></label>
                        <?=$createPageForm->imageName?>
                        <?=$createPageForm->imageName->print_errors()?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for=" <?=$createPageForm->img_dir->name?>"><?=$createPageForm->img_dir->label?></label>
                        <select name='<?=$createPageForm->img_dir->name?>' class='<?=$createPageForm->img_dir->class?>'>
                        <?=$createPageForm->print_select(array_filter($img_dirs, function($el){
                                return !empty($el);
                            }), $createPageForm->img_dir->value)?>
                        </select>
                        <?=$createPageForm->img_dir->print_errors()?>
                    </div>
                    <div class="col-md-4">
                        <label for=" <?=$createPageForm->image_allign->name?>"><?=$createPageForm->image_allign->label?></label>
                        <select name='<?=$createPageForm->image_allign->name?>' class='<?=$createPageForm->image_allign->class?>'>
                            <?=$createPageForm->print_select(CMSTranslation::$config['bg']['image_allign'], $createPageForm->image_allign->value)?>
                        </select>
                        <?=$createPageForm->image_allign->print_errors()?>
                    </div>
                    <div class="col-md-4">
                        <label for=" <?=$createPageForm->imageWidth->name?>"><?=$createPageForm->imageWidth->label?></label>
                        <?=$createPageForm->imageWidth?>
                        <?=$createPageForm->imageWidth->print_errors()?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class='col-md-12'>
                <label for=" <?=$createPageForm->PHPcode->name?>"><?=$createPageForm->PHPcode->label?></label>
                 <?=$createPageForm->PHPcode?>
                 <?=$createPageForm->PHPcode->print_errors()?>
            </div>
        </div>
        <div class="row">
            <div class='col-md-12'>
                <label for=" <?=$createPageForm->PHPvars->name?>"><?=$createPageForm->PHPvars->label?></label>
                 <?=$createPageForm->PHPvars?>
                 <?=$createPageForm->PHPvars->print_errors()?>
            </div>
        </div>

        <div class='row'>
            <div class="bs-callout bs-callout-info">
                <h4> ��������� �� ������������ <h4>
            </div>
            <div class='col-md-6'>
                <label for="<?=$createPageForm->show_link->name?>"><?=$createPageForm->show_link->label?></label>
                 <select name='<?=$createPageForm->image_allign->name?>' class='<?=$createPageForm->image_allign->class?>'>
                    <?=$createPageForm->print_select(CMSTranslation::$config['bg']['show_link'])?>
                 </select>
                 <?=$createPageForm->show_link->print_errors()?>
            </div>

            <div class='col-md-6'>
                 <label for=" <?=$createPageForm->show_link_order->name?>"><?=$createPageForm->show_link_order->label?></label>
                 <select name='<?=$createPageForm->show_link_order->name?>' class='<?=$createPageForm->show_link_order->class?>'>
                    <?=$createPageForm->print_select(CMSTranslation::$config['bg']['show_link_order'])?>
                 </select>
                 <?=$createPageForm->show_link_order->print_errors()?>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-6'>
                <label for="<?=$createPageForm->show_link_cols->name?>"><?=$createPageForm->show_link_cols->label?></label>
                <?=$createPageForm->show_link_cols?>
                <?=$createPageForm->show_link_cols->print_errors()?>
            </div>

            <div class='col-md-6'>
                <label for="<?=$createPageForm->make_links->name?>"><?=$createPageForm->make_links->label?></label>
                <?=$createPageForm->make_links?>
                <?=$createPageForm->make_links->print_errors()?>
            </div>
            <div class='col-md-6'>
                <label for="<?=$createPageForm->show_group_id->name?>"><?=$createPageForm->show_group_id->label?></label>
                 <select name='<?=$createPageForm->show_group_id->name?>' class='<?=$createPageForm->show_group_id->class?>'>
                    <option value="0"> - �������� - </option>
                    <?=$createPageForm->print_select($groups, $createPageForm->show_group_id->value)?>
                 </select>
                 <?=$createPageForm->show_group_id->print_errors()?>
            </div>
            <div class='col-md-6'>
                <a href="page.php?n=63931&amp;SiteID=<?=$o_page->_site['SitesID']?>" target="_blank" class="add"> ������ �����</a>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-12'>
                <label for="<?=$createPageForm->page_template->name?>"><?=$createPageForm->page_template->label?></label>
                 <select name='<?=$createPageForm->page_template->name?>' class='<?=$createPageForm->page_template->class?>'>
                    <?=$createPageForm->print_select($templates, $createPageForm->page_template->value, $template_id)?>
                 </select>
                 <?=$createPageForm->page_template->print_errors()?>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-12'>
                <label for="<?=$createPageForm->PageURL->name?>"><?=$createPageForm->PageURL->label?></label>
                 <select name='<?=$createPageForm->PageURL->name?>' class='<?=$createPageForm->PageURL->class?>'>
                    <option value="">- �������� -</option>
                    <?=$createPageForm->print_select($addons, $createPageForm->PageURL->value, $template_id)?>
                 </select>
                 <?=$createPageForm->PageURL->print_errors()?>
            </div>
        </div>
        <div class='row'>
            <div class="col-md-4">
                <label for="<?=$createPageForm->toplink->name?>"><?=$createPageForm->toplink->label?></label>
                <?=$createPageForm->toplink?>
                <?=$createPageForm->toplink->print_errors()?>
            </div>
            <div class='col-md-8'>
                <label for="<?=$createPageForm->status->name?>"><?=$createPageForm->status->label?></label>
                <?=$createPageForm->status?>
                <?=$createPageForm->status->print_errors()?>
            </div>
        </div>
        <div class='row'>
            <div class="col-md-4">
                <label for="<?=$createPageForm->ptg_group_id->name?>"><?=$createPageForm->ptg_group_id->label?></label>
                 <select name='<?=$createPageForm->ptg_group_id->name?>' class='<?=$createPageForm->ptg_group_id->class?>'>
                    <option value="0"> - �������� - </option>
                    <?=$createPageForm->print_select($groups)?>
                 </select>
                 <?=$createPageForm->ptg_group_id->print_errors()?>
            </div>
            <div class='col-md-4'>
                <label for="<?=$createPageForm->ptg_label->name?>"><?=$createPageForm->ptg_label->label?></label>
                <?=$createPageForm->ptg_label?>
                <?=$createPageForm->ptg_label->print_errors()?>
            </div>
            <div class='col-md-4'>
                <label for="<?=$createPageForm->ptg_end_date->name?>"><?=$createPageForm->ptg_end_date->label?></label>
                <?=$createPageForm->ptg_end_date?>
                <?=$createPageForm->ptg_end_date->print_errors()?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">1<?=$priceForm->description->label?></div>
                    <div class="col-md-2"><?=$priceForm->code->label?></div>
                    <div class="col-md-2"><?=$priceForm->pcs->label?></div>
                    <div class="col-md-2"><?=$priceForm->qty->label?></div>
                    <div class="col-md-2"><?=$priceForm->price->label?></div>
                    <div class="col-md-2"><?=$priceForm->currency->label?></div>
                </div>
            </div>
            <div class="col-md-12">
            <?php foreach($pagePrices as $i => $price): ?>
                <div class="row">
                    <div class="col-md-2">
                        <input class="<?=$priceForm->description->class?>"
                               name="price[<?=$i?>][description]"
                               type="text" id="price[<?=$i?>][description]"
                               value="<?=$price->price_description?>">
                    </div>
                    <div class="col-md-2">
                        <input class="<?=$priceForm->code->class?>"
                               name="price[<?=$i?>][code]"
                               type="text"
                               id="price[<?=$i?>][code]"
                               value="<?=$price->price_code?>">
                    </div>
                    <div class="col-md-2">
                        <input class="<?=$priceForm->pcs->class?>"
                               name="price[<?=$i?>][pcs]"
                               type="text"
                               id="price[<?=$i?>][pcs]"
                               value="<?=$price->price_pcs?>">
                    </div>
                    <div class="col-md-2">
                        <input class="<?=$priceForm->qty->class?>"
                               name="price[<?=$i?>][qty]"
                               type="text"
                               id="price[<?=$i?>][qty]"
                               value="<?=$price->price_qty?>">
                    </div>
                    <div class="col-md-2">
                        <input class="<?=$priceForm->price->class?>"
                               name="price[<?=$i?>][price]"
                               type="text"
                               id="price[<?=$i?>][price]"
                               value="<?=$price->price_value?>">
                    </div>
                    <div class="col-md-2">
                         <select name='price[<?=$i?>][currency]' class='<?=$priceForm->currency->class?>'>
                            <option value="">- �������� -</option>
                            <?=$createPageForm->print_select($currencies, $price->price_currency)?>
                         </select>
                         <a href="javascript: void(0)" class="delete" onClick="remove_price(this)">
                             <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                         </a>
                    </div>
                </div>
            <?php endforeach;?>
            </div>
            <a href="javascript: void(0)" id="add_page_price" class="add">������ ����</a>
        </div>
        <div class="row">
            <div class='col-md-6'>
                <label for="<?=$createPageForm->SecLevel->name?>"><?=$createPageForm->SecLevel->label?></label>
                 <select name='<?=$createPageForm->SecLevel->name?>' class='<?=$createPageForm->SecLevel->class?>'>
                    <?=$createPageForm->print_select(range(0, $user->AccessLevel))?>
                 </select>
            </div>
            <div class='col-md-6'>
                <span>���� �� ��������:<br> <?=$page->date_added?></span>
                <br>
                <input type="hidden" name="date_added" value="2016-03-18 15:36:59">
                �����: <?=$o_page->extract_uInfo($o_page->get_pInfo('author', $no), 'Name')?>
                <?php
                if($user->AccessLevel >=3) {
                    $page_edit_log_q = $o_page->db_query("SELECT DISTINCT(uID) FROM page_edit_log WHERE n='$no' ");
                    if(mysqli_num_rows($page_edit_log_q)>0) {
                        echo("<br>���������� � ������������� ��: ");
                    }
                    $users = array();
                    while($row_log = mysqli_fetch_object($page_edit_log_q)) {
                        $users[] = $o_page->extract_uInfo($row_log->uID, 'Name');
                    }
                    echo implode(', ', $users);
                    echo("<br>");
                }
                ?>
            </div>
        </div>
        <div class='row'>
            <hr>
            <div class='col-md-6  text-center'>
                <?=$createPageForm->date_added?>
                <?=$createPageForm->action?>
                <?=$createPageForm->lang_id?>
                <?=$createPageForm->imageNo?>
                <?=$createPageForm->old_image_src?>
                <?=$createPageForm->old_imageNo?>
                <?=$createPageForm->no?>
                <?=$createPageForm->submit?>
            </div>
            <div class='col-md-6  text-center'>
            <?php if( ($page->n > 0 ) && ($user->AccessLevel >=2 || $site->_site['netservice']>0) ) {
                include __DIR__ .'/../stats/site_page_analitycs.php'; 
            } ?>
            </div>
        </div>
    </div>
</div>
<?=$createPageForm->end();?>
<?php
require_once __DIR__.'/../../global/tinymce/tinymce.php';
?>

<script type="text/javascript">
function set_parent_page()
{
    //$no - n of the edited page
    //n - selected value from the menu
    var n = jQuery('#site_list_pages').val();

    jQuery('#ParentPage').html('<input value="Loading menu ..." disabled>');
    jQuery.get('web/admin/includes/site_list_pages.php?n='+n+'&SiteID=<?=$SiteID?>&no=<?=$page->n?>&class=form-control', function(data) {
        jQuery('#ParentPage').html(data);
    })
}

function set_image()
{

    var im=document.getElementById("preview_image");
    var iml=document.getElementById("preview_image_link");
    var x=document.getElementById("imageNo").selectedIndex;
    var y=document.getElementById("imageNo").options;

    im.src = y[x].getAttribute('rel');
    //alert(iml.getAttribute("href"));
    //iml.href = y[x].getAttribute('rel');
}

function remove_price(id) {
    jQuery(id).parent().parent().remove();
}

jQuery("#add_page_price").click(function(){
    jQuery("#page_prices tbody tr:last").clone().appendTo("#page_prices tbody");
});

jQuery("#change_image_link").fancybox({
    'width'             : '75%',
    'height'            : '75%',
    'autoScale'         : false,
    'transitionIn'      : 'none',
    'transitionOut'     : 'none',
    'autoDimensions'    : false,
    'type'              : 'iframe'
});

</script>

