<?php

//NEW CMS CONTENT
//$ip == "195.234.238.226" ; $ip =="89.190.198.100" ; $user->ID==196

/*
I. 	Add links
	1- 	add links
	
II.	Add content
	2-	title + picture + text
	5-	picture + text
	6-	text
	
III.Photoalbum
	3-	title + picture
	9-	picture
	
IV.	Announce
	4-	title + picture + paragraph
	7-	picture + paragraph
	8-	paragraph
	
V.	No links
	0- no links
*/

function get_content2($page, $ppage, $Site, $show_link, $show_link_cols, $wdir, $user)
	{

//$ppage is the parent page - the page that contents all the subpages - $page

/*####################
1. initialize temporary variables
####################*/

		//temporary subpage title
		$str_title = "";
		//temporary subpage image
		$str_img = "";
		//temporary subpage full text
		$str_text = "";
		//temporary subpage 1-st paragraph
		$str_paragraph = "";
		//more link text
		$str_more = "";
		//subpage item
		$str_subpage = "";
		//link to the subpage
		$str_link = "page.php?n=$page->n&SiteID=$Site->SitesID";
		//admin message
		$str_admin_message = "";
		//page price
		global $add_to_cart;
		$str_price = outup_page_prices($page->n, $Site->SitesID, "ALL", $add_to_cart);
	
		//get subpage title / title link
		$str_title = $page->Name;
		if($ppage->make_links != 0) $str_title ="<a href=\"page.php?n=$page->n&SiteID=$Site->SitesID\" class=\"main_link\"><strong>".$str_title."</strong></a>";
			
		//get the page image		 
		if (!(empty($page->image_src)) &&  ($page->image_allign < 10)  && file_exists($page->image_src)) 
			{   
			    $query_img = mysqli_query("SELECT * FROM images i, alligns a WHERE  i.imageID = '$page->imageNo' AND a.allignID = '$page->image_allign' ");
				$img = mysqli_fetch_object($query_img);
				
				//get image physical image size
				$size = GetImageSize($page->image_src); 
				$str_image_width = $size[0];
				$str_img_align = $img->allignPos;

				if($ppage->show_link_cols == 1)
					if($page->imageWidth == 0)
						{
							if($size[0]<=550)
								$str_img_width = $size[0];
							else $str_img_width = 550;
						}
					else
						{
							if($page->imageWidth<=550)
								$str_img_width = $page->imageWidth;
							else $str_img_width = 550;
						}
				else $str_img_width = (int)(550/$ppage->show_link_cols - 10);
				//if image is extended - get real size
				if($str_img_width > $size[0]) $str_img_width = $size[0]; 
				
				//reduce img width and img align when we have picture + text, picture + paragraph
				if($ppage->show_link != 3 && $ppage->show_link != 6 && $ppage->show_link != 9 && $ppage->show_link != 8 && $ppage->show_link != 0)
					{
						if(strlen($page->textStr)>0 && $page->image_allign != 1) 
							{		
								$str_img_width = (int)($str_img_width/2);
								//$str_img_align = "left";
							}
						else $str_img_align = "default";
					}
				$picture = pathinfo($page->image_src);
				$pic_name = $picture["basename"];
				$pic_dir = $picture["dirname"];				
				//$str_img = "<img src=\"http://maksoft.net/".$page->image_src."\" class=\"border_image\" border=0 align=".$str_img_align." alt=\"".$img->imageName."\" width=".$str_img_width.">";

				//use img_preview.php to generate image
				$str_img = "<img src=\"http://maksoft.net/img_preview.php?pic_name=".$pic_name."&pic_dir=".$pic_dir."&img_width=".$str_img_width."\" class=\"main_image\" border=0 align=".$str_img_align." alt=\"".$img->imageName."\" >";
				if($ppage->make_links == 1)
					$str_img = "<a href=\"".$str_link."\" >".$str_img."</a>";					
			}
		//user see empty image container
		else
			{
				//user is loged and have access to write to the page -> see an empty image message
				
				if(($user->WriteLevel >= $ppage->SecLevel) && ($user->AccessLevel > 0))
				{
					$str_empty_img = "<img src=\"http://maksoft.net/web/admin/images/no_picture.jpg\" class=\"main_image\" border=0 alt=\"no image. you won't see this when you logout\" width=75 height=75><br>";
					if($ppage->make_links == 1)
						$str_empty_img = "<a href=\"".$str_link."\" >".$str_empty_img."</a>";
				}

			}
			
		
		//get the admin message
		if($user->AccessLevel>0 && $ppage->n == 11)
			{
				$admin_message_res = mysqli_query("SELECT * FROM pages WHERE ParentPage = '12010' ORDER by RAND()");
				$admin_message = mysqli_fetch_object($admin_message_res);
				$str_admin_message = "<div class=\"message_normal\"><strong>".$admin_message->Name."</strong>
				".crop_text($admin_message->textStr)." <a href=\"page.php?n=$admin_message->n&SiteID=$Site->SitesID\" target=_blank class=\"more_link\"><b>".$Site->MoreText."</b></a></div>";
			}
			
		//get the page text
		$str_text = $page->textStr;
		if(strlen($str_text)<=3 && $user->AccessLevel>0)
			$str_text ="<br><em>page content empty</em>";
		if($ppage->make_links == 1)
			$str_text = "<a href=".$str_link." class=\"main_link\">".$str_text."</a>";
		//get page more link
		$str_more = "<a href=\"".$str_link."\" class=\"next_link\">".$Site->MoreText."</a>";
		//get the page 1-st paragraph
		$str_paragraph = crop_text($str_text);	

		

/*############################
2. construct subpage item content
############################*/		

		//1.	title
		if($ppage->show_link == 1)
				$str_subpage.="<li>".$str_title . $str_price;
		
		//2.	title + picture + text
		if($ppage->show_link == 2)
			{
				if(strlen($str_img)<=0) $str_img = $str_empty_img;
				$str_subpage.="<table width=100%><th align=left>".$str_title."</th>";
				//image is background
				if($page->image_allign == 22)
						$str_subpage.="<tr><td valign=top align=justify style=\"background: url(http://maksoft.net/".$page->image_src.")\">". $str_img . $str_paragraph."</td></tr>";
				else $str_subpage.="<tr><td valign=top align=justify>". $str_img . $str_text."</td></tr>";
				$str_subpage.= "</table>".$str_price;
			}

		//5.	picture + text
		if($ppage->show_link == 5 && (strlen($str_text)>0 || strlen($str_img)>0))
			{
				if(strlen($str_img)<=0) $str_img = $str_empty_img;
				$str_subpage.="<table class=\"main_table\"><tr><td>";
				$str_subpage.=$str_img . $str_text."</td></tr></table>";
			}
		//6.	text
		if($ppage->show_link == 6)
			{
				if($ppage->make_links == 1)
					$str_subpage.="<div onMouseOver=\"javascript: this.className='bgcolor_link';\" onMouseOut=\"this.className=''\" width=\"100%\">".$str_text."</div>";
				else
					$str_subpage.="<div class=\"main_table\" width=\"100%>".$str_text."</div>";
			}
			
		//3.	title + picture
		if($ppage->show_link == 3)
			{
				if(strlen($str_img)<=0) $str_img = $str_empty_img;
				$str_subpage.="<table class=\"main_table\" width=100%><tr class=\"main_tr\"><td>".$str_title."</th>";
				$str_subpage.="<tr class=\"main_tr\"><td valign=top align=center>". $str_img."</td></tr></table>";
			}			
			
		//9.	picture
		if($ppage->show_link == 9)
			{
				if(strlen($str_img)<=0) $str_img = $str_empty_img;
				$str_subpage.="<table width=100%>";
				$str_subpage.="<tr><td valign=top align=center>". $str_img."</td></tr></table>";
			}
			
		//4.	title + picture + paragraph
		if($ppage->show_link == 4)
			{
				if(strlen($str_img)<=0) $str_img = $str_empty_img;
				$str_subpage.="<table width=100%><th align=left>".$str_title."</th>";
				$str_subpage.="<tr><td valign=top align=justify>". $str_img . $str_paragraph.$str_more."</td></tr></table>";
			}

		//7.	picture + paragraph
		if($ppage->show_link == 7)
			{
				if(strlen($str_img)<=0) $str_img = $str_empty_img;
				$str_subpage.=$str_img . $str_paragraph . $str_more;
			}

		//8.	paragraph
		if($ppage->show_link == 8)
			{
				if($ppage->make_links == 1)
					$str_subpage.="<div onMouseOver=\"javascript: this.className='bgcolor_link';\" onMouseOut=\"this.className=''\"  width=\"100%\">".$str_paragraph."</div>".$str_more;
				else
					$str_subpage.="<div class=\"main_table\"  width=\"100%\">".$str_paragraph."</div>".$str_more;

			}
						
		//if page has to genrate its own conent: $ppage == $page

		if($page->n == $ppage->n)
			{
				//get the current page text
				$str_text = $str_admin_message . $page->textStr;
		
				if (!(empty($page->image_src)) &&  ($page->image_allign < 10)  ) 
					{
						if($img->allignPos=="left" || $img->allignPos=="default") $margin = "margin-right: 25px;";
						if($img->allignPos=="right") $margin = "margin-left: 25px;";
						
						$str_img = "<img src=\"http://maksoft.net/".$img->image_src."\" border=0 align=".$img->allignPos." alt=\"".$img->imageName."\" width=".$page->imageWidth." style=\"".$margin."\">";
					}
				else $str_img= "";
				$str_subpage = $str_img . $str_text;
				
				//image is a background
				if($page->image_allign == 22)
					{
							$bg_style="";
							if($page->imageWidth == 0) $bg_style = "background-image: url(http://maksoft.net/".$page->image_src."); background-repeat: no-repeat;";
								else  $bg_style = "background-image: url(http://maksoft.net/".$page->image_src.");";
							$str_subpage ="<table width=\"100%\" class=\"main_table\" border=0 cellspacing=0 cellpadding=0 style=\"".$bg_style."\"><tr><td valign=top>". $str_text."</td></tr></table>";
					}
			}
												
		//return subpage item content
		return $str_subpage;		
	}

/*####################
3. display page content
####################*/	

if (!(empty($n)))	
{

	if($user->ReadLevel>=$row->SecLevel)
		{
			//position of the subpage links (default = top) 
			if(!isset($page_links_position)) $page_links_position = "bottom"; 
	
			//read current page content
			$page_str.=get_content2($row, $row, $Site, 0, 1, $wdir, $user);
			
			//place page content above subpage links
			if($page_links_position == "bottom") $str.= $page_str;
						
			$page_result = mysqli_query("SELECT * FROM pages left join images on pages.imageNo = images.imageID WHERE ParentPage = '$row->n' ORDER BY `n` $row->show_link_order");
			$i=1;
			$str.= "<table cellspacing=0 cellpadding=5 class=\"main_table\" border=0 ><tr valign=top>";
			if(!isset($row->show_link_cols) || $row->show_link_cols<=0) $row->show_link_cols = 1;
			$cell_width = ((int)(100/$row->show_link_cols))."%";
			//$cell_width = "100%";
			while ($page = mysqli_fetch_object($page_result)) 
						{ 	
							//check for user access
							if($page->SecLevel <= $user->ReadLevel)
							{
								if($i>$row->show_link_cols)
									{
										$str.="<tr><td width=\"".$cell_width."\" valign=top>"; $i=1;
									}
								else 
									$str.="<td width=\"".$cell_width."\" valign=top>";
	
								$str.= get_content2($page, $row, $Site, $row->show_link, $row->show_link_cols, $wdir, $user);
								$i++;
							}
						}
			$str.="</table>";
			//get content of the current page
			//place page content above subpage links
			if($page_links_position == "top") $str.= $page_str;
			//$str.="<hr>";
		}
	else
		{
			if ($SiteID == 1) 
				$reg_str = "no acces to view site content";
			else $reg_str = "";
			
			$str='<div align=center>
			�������� �� ���� �������� � ��������� ���� �� ������������ ����������� !<br>
			���� �������� ������ ������������� ��� � ������ !   
			<br><br>
			<table align="center" width="500"><tr><td>
			<fieldset>
			<legend>��������������� ����</legend>
					<table align="center" width="400">
					<form name="form" method="post" action="http://www.maksoft.net/web/admin/login.php">
					<tr>
						<td width="150" class="small_text">������������� ���: 
						<td width="250"><input type="text" name="username">
					<tr>
						<td width="150" class="small_text">������:
						<td width="250"><input type="password" name="pass">
					<tr>
						<td colspan="2" align="center">
						<input name="n" type="hidden" id="n" value='.$n.'>
						<input name="SiteID" type="hidden" id="SiteID" value='.$SiteID.'>
					  <input type="submit" name="Submit" value=" � � � � " class=\"button_submit\">
					</form>
					</table>
			</fieldset>
			</table>
			
			<center>
			<a href=http://www.maksoft.net/page.php?n=247&SiteID='.$SiteID.'>��������� ������</a>
			</center>
			</div>';
		}
			
	function make_links($links, $from_page, $user, $Site, $level) 
	{
		if ($StartPage == 0) 
			{
				$StartPage = $Site->StartPage; 
			}
			
		if ($level <=1) 
			{
				$level = $level+1; 
				$query_text = "SELECT * FROM pages WHERE ParentPage = '$from_page' AND SecLevel <= '$user->ReadLevel' ORDER BY n ASC "; 
				$links = "$links";
				$query_pages = mysqli_query(" $query_text "); 
			   
				//reading the links
				while ($row = mysqli_fetch_object($query_pages) ) 
					{ 
					$indent = 0; 
					for ($i=0; $i<=$level; $i++) 
						{
							$indent =$indent+10; 
						}
						
					//$indent = "&raquo; "; 
			   
					if ($level < 2) {$row->Name = "<b>$row->Name</b>";}
					$links.= "<a href=\"page.php?n=$row->n&SiteID=$Site->SitesID\" class=\"main_link\" style=\"display:block; text-indent: ".$indent."px;\">&raquo; $row->Name</a>"; 
						//recursion
						$links = make_links($links, $row->n, $user, $Site, $level, 0);
					}    
			}
		  
	   return $links; 
	}		

	// show standart related links as a main menu 
	$links = make_links($links, $Site->StartPage, $user, $Site, 0);

}

?>
