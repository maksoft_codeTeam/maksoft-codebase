<?php
    if(!isset($te_theme) && empty($te_theme)) {
        $te_theme = $Site->te_theme;
        
        if(isset($HTTP_GET_VARS['te_theme']))
            $te_theme = $HTTP_GET_VARS['te_theme'];         
        
        if(defined(CMS_TE_THEME)) $te_theme = CMS_TE_THEME;
    }
    
    if($user->AccessLevel <= 1)
            $te_theme = "simple";
    
    if($user->AccessLevel == 2)
            $te_theme = "basic";
                
    if($user->AccessLevel == 3)
            $te_theme = "default";
    
    if($user->AccessLevel > 3)
            $te_theme = "advanced";
    
    //page is new
    if(!isset($no))
        $te_theme = "simple";
    
    //check for individual text editor setup in folder text_editor/separate/<SiteID>.php
    //this skips the text_editor theme switch
    $separate_te_theme = "text_editor/separate/".$SiteID.".php";

    if(file_exists($separate_te_theme)) {
            //echo "TEST";
            //generate images list for TEXT editor image url list
            include "web/admin/image_list_generator.php";
            //generate links list for TEXT editor url list, generates as reading PUB dir
            include "web/admin/links_list_generator.php";
            include $separate_te_theme;
    } else {    
        switch($te_theme)
        {
            case "basic": 
                                {   
                                include "text_editor/basic.php";    
                                break;
                                }
            case "simple": 
                                {   

                                include "text_editor/simple.php";   
                                $no_tags_selected = "checked";
                                break;
                                }
            case "advanced": 
                                {   
                                    //generate images list for TEXT editor image url list
                                    include "web/admin/image_list_generator.php";
                                    //generate links list for TEXT editor url list, generates as reading PUB dir
                                    include "web/admin/links_list_generator.php";
                                    //include text editor
                                    include "text_editor/advanced.php";
                                    break;
                                }
            case "default" :
            default :
                                {
                                    //generate images list for TEXT editor image url list
                                    include "web/admin/image_list_generator.php";
                                    //generate links list for TEXT editor url list, generates as reading PUB dir
                                    include "web/admin/links_list_generator.php";
                                    include "text_editor/default.php";
                                }
        }
    }

    if(strlen($row_no->textStr)>400) 
        $edit_rows=22;  
