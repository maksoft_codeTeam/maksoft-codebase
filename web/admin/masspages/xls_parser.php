<head>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/bootstrap-filestyle.min.js"> </script>
</head>

<?php

require_once $_SERVER["DOCUMENT_ROOT"].'/web/admin/masspages/Excell/reader.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/web/admin/masspages/helpers.php';
require_once $_SERVER['DOCUMENT_ROOT']."/lib/prettytable.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/Database.class.php";

if(!isset($__template_tags))
    $__template_tags = array('ParentPage', 'Name', 'title', 'textStr');
?>
<p><h2>����� �� ������ ��������� �� ���� �������� � cms ��������� �� �������</h2></p>
<p>
    <h3>
        �� �� ������� ������ ������ �� ��������� �������� ������� � ������ xls � ����� �� ��������:
        ParentPage, Name, title, textStr, slug
    </h3>
    <ul>
        <li> ����� � ����� �� ���� ������ ������ � ���������. A�� �� ������� ������� ����� ������ �� ���� ����������� ������ � ������, ���� ��������� � � null</li>
    </ul>
</p>

<form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data">
    <div class="form-group">
        <label class="control-label">Select File</label>
        <input type="file" name="file" id="fileToUpload">
    </div>
    <button type="submit" name='submit' class="btn btn-default">Submit</button>
</form>

<?php
$insert_query = "REPLACE INTO pages (".implode(',', $__template_tags).")
                 VALUES (:".implode(', :', $__template_tags)." :SiteID, :date_added)";

$db = new Database();
$table = new PrettyTable();
echo $o_page->_page['SiteID'];
$table->set_field_names($__template_tags);
if (isset($_FILES) && isset($_POST['submit'])) {
    if($_FILES['file']['type'] == 'application/vnd.ms-excel'){
        $excel = new Spreadsheet_Excel_Reader();
        $excel->setOutputEncoding('utf-8');
        $excel->read($_FILES['file']['tmp_name']);
        if(count(array_diff($excel->sheets[0]['cells'][1], $__template_tags)) == 0){
            $x = 2; //from second row to end
            $i = 1; //counter for columns
            $stmt = $db->prepare($insert_query);
            $data = array();
            while ($x <= $excel->sheets[0]['numRows']) {
                for ($i = 1; $i < count($excel->sheets[0]['cells'][1]) + 1; $i++):
                    $stmt->bindValue(':'.$__template_tags[$i-1],  $excel->sheets[0]['cells'][$x][$i]);
                    $data[] =  $excel->sheets[0]['cells'][$x][$i];
                endfor;
                $table->add_row($data);
                $stmt->bindValue(":SiteID", $o_page->_page['SiteID']);
                $stmt->bindValue(":date_added", date("Y-m-d H:i:s"));
                try {
                    $stmt->execute();
                } catch (PDOException $e) {
                    $error_message = '������ ��� ����������� �� ����������. ����, ���������� �������� ��� ������ �������� �������.';
                    $template = Template::load($_SERVER["DOCUMENT_ROOT"].'/web/admin/masspages/templates/alert-danger.html');
                    echo Template::replace(array('error_message'=>$error_message), $template);
                    break;
                }
                $x++;
            }
        }
    }else{
        $error_message = '��������� ������ ������, ������� *.xls';
        $template = Template::load($_SERVER["DOCUMENT_ROOT"].'/web/admin/masspages/templates/alert-danger.html');
        echo Template::replace(array('error_message'=>$error_message), $template);
    }
}
