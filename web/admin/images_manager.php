<script src="//cdn.jsdelivr.net/alertifyjs/1.9.0/alertify.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/alertify.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/themes/bootstrap.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/themes/bootstrap.rtl.min.css"/>
<script language="JavaScript" type="text/javascript" src="http://www.maksoft.net/lib/custom-form-elements.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.6.0/clipboard.min.js"></script>
<script language="JavaScript" type="text/javascript">
$j(document).ready(function(){

	var count_files = $j('#file_inc span').html();

	$j('#icon_inc').click(function(){
		count_files++;	
		$j('#file_inc span').html(count_files);
	});

	$j('#icon_dec').click(function(){
		if(count_files > 1) count_files--;	
		$j('#file_inc span').html(count_files);
	});
	
	$j("#filter_link").click(function(){
		$j('#filter_table').toggle();
	})
	
	$j("#select_all").click(function(){
		if($j("#select_all").is(':checked'))
			{
				$j(":checkbox").attr('checked', true);
			}
		else 
			{
				$j(":checkbox").attr('checked', false);
			}
		
		});


	$j('#submit').click(function(){
		var error = false;
		$j("input.extraUserfilenames").each(function(){
			if($j(this).val() == "")
				{
					error = true;
					$j(this).attr("class","error");
				}
			else $j(this).attr("class","");

		});
				
		if(error)
			{
				$j('#message').attr("class","message_error");
				$j('#message').html("���� ��������� ������ ������! �������� ��� �������� ���� �� ����� ������ !");
				$j('#message').fadeIn();
				return false;
			}
		
		//else $j('#multiple_form').submit();
		
	})
	
	$j('#open-missing-files-list').click(function(){
		$j('#missing-files-list').slideToggle(500);
		})
})

function set_selected(id)
	{
		var cb = document.getElementById("cb_"+id);
		var dir = document.getElementById("item_"+id);
		if(cb.checked) change_class("item_"+id, "directory_selected")
		else  change_class("item_"+id, "directory")
	}
</script>
<?php
if($user->AccessLevel >= $o_page->_page['SecLevel'])
{

$action = $_POST['action'];
$edit_image = $_POST['edit_image'];

$imageID = $_POST['imageID'];
$imageName = $_POST['imageName'];

switch($action)
	{
		case "delete_images":
			{
				//print_r($delete_images);
				$message_normal = "";
				$message_error = "";
				
				for($i=0; $i<count($delete_images); $i++)
					{
						$img_query = $o_page->db_query("SELECT * FROM images WHERE imageID='".$delete_images[$i]."'"); 
						$img_row = $o_page->fetch("array", $img_query);
						if(file_exists($img_row['image_src']))
							{
								if(unlink($img_row['image_src']))
									$message_normal.= "<li>".$img_row['image_src'];
								else 
									$message_error.=  "<li>".$img_row['image_src'];

							}
							
						$o_page->db_query("DELETE FROM images WHERE imageID='".$img_row['imageID']."'");
					}
				if($message_error != "") mk_output_message("error", "������ ��� ��������� �� ��o��������:<br>".$message_error);
				if($message_normal != "") mk_output_message("normal", "��������� �� ��� ����������� �� �������:<br>".$message_normal);	
				break;
			};
		case "upload_images":
			{
				//echo "upload images";
				
				include "upload_file.php";
				//print_r($extraUserfilenames);
				for($i=0; $i<count($extraUserfiles);$i++)
					if(strlen($extraUserfilenames[$i]) > 0)
						upload_file(0, $extraUserfilenames[$i], $Site, $_FILES['extraUserfiles']['name'][$i], $_FILES['extraUserfiles']['tmp_name'][$i], $_FILES['extraUserfiles']['size'][$i], $_FILES['extraUserfiles']['type'][$i], 2, $im_dir);			
				break;
			};
		case "edit_images":
			{
				
				//upload / rename file / save to DB
				include "upload_file.php";
				$imageID = upload_file($imageID, $imageName, $Site, $_FILES['userfile']['name'], $_FILES['userfile']['tmp_name'], $_FILES['userfile']['size'], $_FILES['userfile']['type'], 2, $im_dir);
				if($o_page->db_query("UPDATE images SET imageName='$imageName', image_dir='$im_dir', image_date_modified = now() WHERE imageID='$imageID'"))
					mk_output_message("warning", "������������ �� ����������� $imageID � �������������."); 

				//mpetrov 10.03.2014	
				if($_POST['image_to_logo'])
					{
						$query = "UPDATE Sites SET Logo='".$_POST['file_name']."' WHERE SitesID = '".$o_site->SiteID."' LIMIT 1";
						//echo $query;
						if($o_page->db_query($query))
							mk_output_message("warning", "������������� �� �������� ���� ���� �� �����!");
					}

				break;
			};
		default: "";
		
	}

$im_dir = $_REQUEST['im_dir'];
$dInfo = $o_site->get_sDirectoryInfo($im_dir);

if(!isset($im_dir))
	$im_dir = $o_site->get_sInfo("imagesdir");

//images per page
$images_per_page = 20;

//page range - how many pages should be visible
$p_range = 10; 

//current page
if(!isset($p)) $p = 1;

//start page
if(!isset($sp)) $sp = 1; 

//start image listing
$start = ($p-1)*$images_per_page;

//limitations
$im_limit = 20000;
$p_limit = $im_limit/$images_per_page;

//images filter
	$add_sql = "1";

	//filter by name
	if($f_name != "")
		$add_sql.= " AND im.imageName LIKE '%".$f_name."%'";
		
	//sort filter
	if(isset($f_sort))
		$add_sql.= " ORDER by im.imageID $f_sort";

//all images limited to 1000
$all_images = $o_site->get_sImages($im_dir, "$add_sql LIMIT $im_limit", $f_usage);
$count_images = count($all_images);
	
//page limit
if($count_images >= $im_limit)
	{
		$pages = $p_limit;
		mk_output_message("warning", "��������! ����� ���� ��� ������ ����� ������. �� �� ������ ��-����� � ������� �� ������ ���� <b>$im_limit</b> ������ ����������� � <b>$p_limit</b> ��������!");
		
	}

if($count_images > 2000)
	{
		$images_per_page = 50;
		$p_range = 20;
	}		
//images in all visible pages
//$images = $o_site->get_sImages($im_dir, "$add_sql LIMIT $start, $images_per_page", $f_usage);
$pages = $count_images / $images_per_page;
$images = $o_site->get_sImages($im_dir, "$add_sql LIMIT ".$start.", ".$images_per_page*($sp-1+$p_range)."", $f_usage);

if($pages > 0 && $pages < 1) $pages = 1;

if($pages > round($pages)) $pages = round($pages) +1;
else $pages = round($pages);

$np_sp = $sp+$p_range;
$np_p = $sp+$p_range;

if($np_sp <= $p_limit && $pages > $p_range)
	$next_pages = "<a href=\"page.php?n=$n&SiteID=$SiteID&im_dir=$im_dir&sp=".$np_sp."&p=".$np_p."\" class=\"page\">&raquo;</a>";
else $next_pages = "";

if(($sp-$p_range) <= 0)
	{	
		$pp_sp = 1;
		$pp_p = 1;
	}
else
	{	
		$pp_sp = $sp-$p_range;
		$pp_p = $sp-$p_range;
	}

if($sp > 1)		
	$prev_pages = "<a href=\"page.php?n=$n&SiteID=$SiteID&im_dir=$im_dir&sp=".$pp_sp."&p=".$pp_p."\" class=\"page\">&laquo;</a>";
else $prev_pages = "";

$pagination = "<div id=\"pagination\">";
$pagination.= $prev_pages;

if($pages > ($sp+$p_range))
	$pages = ($sp+$p_range);
	
for($i=$sp; $i<=$pages; $i++)
	{
		if($i == $p) $class = "page_selected";
		else $class = "page";
		//$pagination.= "<a href=\"page.php?n=$n&SiteID=$SiteID&im_dir=$im_dir&sp=".$sp."&p=".$i."\" class=\"$class\">".$i."</a>";
		$pagination.= "<a href=\"".mk_href_link("&sp=".$sp."&p=".$i)."\" class=\"$class\">".$i."</a>";
	}


$pagination.= $next_pages."</div>";	

/*MISSING FILES*/

if($user->AccessLevel >0)
	{
		$missing_files = array();
		$mf_query = "SELECT * FROM images WHERE image_dir = '".$im_dir."' ORDER by image_date_added DESC";
		$mf_res = $o_page->db_query($mf_query);
		while($file = $o_page->fetch("array", $mf_res))
			{
				if(!file_exists($file['image_src']) || is_dir($file['image_src']))
					$missing_files[] = $file;
			}	
	if(count($missing_files)>0)
		{
			echo "<div class=\"message_normal\" style=\"font-size: 12px;\"><b>��������</b>: ��� ����� ���� ��� <b>".count($missing_files)."</b> �������� ������� �� ��������. ���������� � �� �� �������� � ����������� ���� �� ������� �� �� �� �����������: <a href=\"mailto:support@maksoft.net\" style=\"text-decoration: underline; font-size: 12px;\">support@maksoft.net</a><br>������ ���������� ������ �� �������� ���: <a href=\"http://www.maksoft.net/bulletin/server_report_201013.htm\" target=\"_blank\" style=\"text-decoration: underline; font-size: 12px;\">�������� ������ � �����</a>.<br><br><button id=\"open-missing-files-list\" class=\"btn btn-danger btn-small\">������ �������� �������</button></div>";
			echo "<ol id=\"missing-files-list\" style=\"display: none; margin: 0; list-style-position: inside; padding: 20px 0; border: 1px dashed #c4161c; background: #f0f0f0\">";
			for($i=0; $i<count($missing_files); $i++)
				echo "<li style=\"padding: 0 20px;\">".$missing_files[$i]['image_src']." [".$missing_files[$i]['image_date_added']."]</li>";
			echo "</ol>";
		}
	}
?>

<div id="file_manager">
<fieldset><legend>- <?=$dInfo['Name']?> - [<a href="<?=$o_page->get_pLink(683)?>">������ ����������</a>] [<a href="javascript: void(0)" id="filter_link">������</a>]</legend>
<form name="filter" method="get" action="<?=mk_href_link()?>">
<input type="hidden" name="n" value="<?=$o_page->n?>">
<input type="hidden" name="SiteID" value="<?=$o_site->SiteID?>">
<input type="hidden" name="im_dir" value="<?=$im_dir?>">
<table cellspacing="0" width="100%" border="0" cellpadding="5" id="filter_table" style="display: none; background: #">
<tbody>
<tr>
	<td>�� ���:<div class="field"><input type="text" name="f_name" value="<?=$f_name?>"></div><td>���������:<div class="field"><select name="f_sort"><option value="ASC" <?=($f_sort == "ASC") ? "selected":""?>>�������� ��� 0-9</option><option value="DESC" <?=($f_sort == "DESC") ? "selected":""?>>�������� ��� 9-0</option></select></div>
<tr>
	<td><div class="field"><input type="checkbox" name="f_usage" <?=($f_usage == true)? "checked":""?> class="styled">���� ��������������</div><td><input type="submit" value="���������" class="submit"> / <a href="javscript: void(0)" onClick="document.filter.reset()">�������</a>
<tr>
	<td colspan="2"><hr>
</tbody>
</table>
</form>
<?php
if($count_images == 0)
	mk_output_message("warning", "������������ � ������ !");
else
	{	
	//list selected images
	$image_list = $o_site->get_sImages($im_dir, "$add_sql LIMIT $start, $images_per_page", $f_usage); 
	//count images that could be deleted
	$free_images_counter = 0;

?>
<form action="<?=mk_href_link("n=$n&SiteID=$SiteID&im_dir=$im_dir", "strict")?>" method="post">
<table cellspacing="5" width="100%" border="0" cellpadding="0">
<thead>
	<tr><th colspan="2" align="right" height="30px" valign="middle"><?php echo $pagination; ?>
</thead>
<tbody>
<tr>
<?php
/*
echo "<pre>";
print_r($image_list);
echo "</pre>";
*/
$j=0;
for($i=0; $i<count($image_list); $i++)
	{
		if($j==2)
			{
				echo "<tr>";
				$j=0;
			}
			if($j==1) $class = "border";
			else $class = "";
		
		$filesize = round(filesize($image_list[$i]['image_src'])/1024);
	
		echo"<td width=\"50%\" class=\"".$class."\" valign=\"middle\">";
		//if image is not used in any page
		if($image_list[$i]['usage'] == 0)
			{
				$free_images_counter ++;
				echo "<input type=\"checkbox\" id=\"cb_".$image_list[$i]['imageID']."\" name=\"delete_images[]\" value=\"".$image_list[$i]['imageID']."\" onClick=\"set_selected(".$image_list[$i]['imageID'].")\">";
			}
		echo "<a href=\"".$image_list[$i]['image_src']."\" rel=\"lightbox[]\" class=\"directory\" id=\"item_".$image_list[$i]['imageID']."\" title=\"".$image_list[$i]['imageName']."\"><img src=\"spacer.gif\" class=\"image\" style=\"background: url(img_preview.php?image_file=".$image_list[$i]['image_src']."&img_width=80) 50% 50% no-repeat #FFF\"><div class=\"text\">".str_replace(",", ", ", $image_list[$i]['imageName'])."<br><small>".date("d.m.y", strtotime($image_list[$i]['image_date_added']))."<br><br>".$filesize." KB</small></div></a><a href=\"page.php?n=2017&SiteID=$SiteID&im_dir=$im_dir&imageID=".$image_list[$i]['imageID']."\" class=\"edit\" title=\"�������� �� �������������\"></a><button class=\"shortcode\" title=\"�������� �� shortcode\" data-clipboard-text=\"[get_picture id='".$image_list[$i]['imageID']."']\"><img src=\"http://www.maksoft.net/web/images/icons/copy.png\" alt=\"\"/></button>"; 
		$j++;
	
	}
	if($free_images_counter > 0)
		{
	?>
        <tr><td colspan="2" align="center"><hr>
		<input type="hidden" value="delete_images" name="action"><input type="submit" value=" ������ �������� " class="submit">
	<?
		}
	?>
</tbody>
<tfoot>
<tr><td colspan="2"><input type="checkbox" id="select_all"> ������ ������
<tr><td colspan="2" align="right" height="30px" valign="middle"><?php echo $pagination; ?>
<tr><td colspan="2" align="right" height="30px" valign="middle"><em>���� �������: <b><?=$count_images?></b>&nbsp;&nbsp;&nbsp;</em>

</tfoot>
</table>
</form>
<?php
	}
?>
</fieldset>
<form action="<?php echo("?n=684&SiteID=$SiteID&im_dir=$im_dir");?>" method="post" enctype="multipart/form-data" name="multiple_form" id="multiple_form">
<fieldset><legend>- �������� �� ����������� -</legend>
<table cellspacing="5" width="100%" border="0" cellpadding="0">
<tbody>
<tr>
	<td align="right"><div id="file_inc" style="width: 80px; height: 22px; background: #f9f9f9; color: #F00; text-align: center; line-height: 22px; margin: -25px 0 0 0; position: relative;"><img src="web/admin/images/icons/icon_arrow_left.png" id="icon_dec" onClick="deleteRow('table_upload', 2)" align="left" style="margin: 0 0 0 -5px; float: left; cursor: pointer;"><img src="web/admin/images/icons/icon_arrow_right.png" id="icon_inc" onClick="duplicateRow('table_upload', 1)" align="right" style="margin: 0 -5px 0 0; float: right; cursor: pointer;">- <span>1</span> -</div>
<tr>
	<td>
		<table cellspacing="0" width="100%" border="0" cellpadding="5" id="table_upload">
			<tr>
					<td width="33px">
					<td align="left" width="50%"><span class="label">��������:</span>
					<td align="left" width="50%" class="border"><span class="label">����:</span>
			</tr>
			<tr>
				<td width="33px"><img src="web/admin/images/icons/icon_arrow_up.jpg">
				<td align="left" width="50%"><div class="field"><input name="extraUserfilenames[]" type="text" class="extraUserfilenames" value=""></div>
				<td align="left" width="50%" class="border"><div class="field"><input name="extraUserfiles[]" type="file" id="extraUserfiles[]"></div>
			</tr>
		</table>
		<div id="message" style="display: block;"></div>
</tbody>
<tfoot>
	<tr>
		<td align="center"><input type="submit" value=" ������� �������� " class="submit" id="submit">
		<br><br><em>* ���������� ������ �� ���� ���� - 256Kb</em>

</tfoot>
</table>					
<input name="action" type="hidden" value="upload_images">
<input name="im_dir" type="hidden" id="im_dir" value="<?php echo("$im_dir"); ?>">
</fieldset>
</form>
</div>
<?php
}
//else mk_output_message("warning", "� ������� ������ �� ��������. �� ���� �������� ���� 2 ����. ���� �� �� ��������.");
?>
<script type="text/javascript">
jQuery.each(jQuery(".shortcode"), function(i, button){
    jQuery(button).on("click", function(e){
        e.preventDefault();

    });
});
var clipboard = new Clipboard('.shortcode');
clipboard.on('success', function(e) {
    var notification = alertify.notify('������� ��������� �������. ��������� �� � CTRL+V � ������ �� ����������', 'success', 5);
});

</script>
