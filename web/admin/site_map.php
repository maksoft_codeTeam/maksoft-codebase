<?php require_once $_SERVER['DOCUMENT_ROOT'].'/fak/base.php'; ?>
<?php

//echo "eee: ".$pos." ".$ppage." ".$no;
//swap pages, swap sort_n fields
function swap_pages ($n1, $n2)
{

    $o_page = page::getInstance();
	$page_info = get_page($n1);
	$sort_n1 = $page_info->sort_n;
	
	$page_info = get_page($n2);
	$sort_n2 = $page_info->sort_n;
	
	$query1 = "UPDATE pages SET sort_n = '$sort_n2' WHERE n = '$n1' ";	
	$query2 = "UPDATE pages SET sort_n = '$sort_n1' WHERE n = '$n2' ";
	if($o_page->db_query($query1) && $o_page->db_query($query2))
		mk_output_message("normal","������� ����������� �� ���������� !");
	else
		mk_output_message("error","������ ��� ������������� �� ��������� �� ��� �������� !");
}

//return swap page n
function get_swap_n2($n, $pos)
{

	global $user;
    $o_page = page::getInstance();
	$page = get_page($n);
	switch($pos)
		{
			case "Up":
				{
					$page_up_query = "SELECT n, sort_n, ParentPage FROM pages WHERE sort_n < '$page->sort_n' AND ParentPage = '$page->ParentPage'  AND SiteID = '$page->SiteID' ORDER by sort_n DESC LIMIT 1";
					$page_up_result = $o_page->db_query($page_up_query);
					$page_up = $o_page->fetch("object", $page_up_result);
					$swap_n2 = $page_up->n;
					break;
				}
			
			case "Down":
				{
					$page_down_query = "SELECT n, sort_n, ParentPage FROM pages WHERE sort_n>'$page->sort_n' AND ParentPage = '$page->ParentPage' AND SiteID = '$page->SiteID' ORDER by sort_n ASC LIMIT 1";
					$page_down_result = $o_page->db_query($page_down_query);
					$page_down = $o_page->fetch("object", $page_down_result);
					$swap_n2 = $page_down->n;
					break;
				}
			default: $swap_n2 = NULL;
		}
	return $swap_n2;
}


//list site pages tree
function list_site_map($Site, $ParentPage, $user, $level=0)
{
	global $n, $SiteID, $count_subpages, $view_map_branches, $view_map_branches_level, $no, $site_map_level, $ppage, $view_page_visits;
    $o_page = page::getInstance();
	
	$n = $_GET['n'];
	$SiteID = $_GET['SiteID'];
	$no = $_GET['no'];
	//$ppage = $_GET['ppage'];
	
	//check for previous declarations
	if(!isset($view_map_branches))
		$view_map_branches = true;
	
	if(!isset($count_subpages))
		$count_subpages = false;
		
	if(!isset($site_map_level))
		$site_map_level = 5;
	
	if(!isset($view_map_branches_level))
		$view_map_branches_level = $site_map_level;

	if(!isset($view_page_visits))
		$view_page_visits = true;
						
	$query = "SELECT * FROM pages WHERE SiteID = '$Site->SitesID' AND ParentPage = '$ParentPage' ORDER by sort_n ASC";
	$result = $o_page->db_query($query);
	
	if(!$view_map_branches && $level >0 || $view_map_branches_level < $level)
		echo "<ul id=\"branch_".$ParentPage."\" style=\"display: none\">";
	else
		echo "<ul id=\"branch_".$ParentPage."\">";
		
	while($page_list = $o_page->fetch("object", $result))
		{
			
			$show_hide_str = "<img src=\"web/images/icons/empty.gif\"> ";
			$branches_counter_str = ""; 
			$page_visits_str = "";

			if($view_page_visits)
				$page_visits_str = "[".$page_list->preview."]";			

			//admin string
			if($user->WriteLevel >= $page_list->SecLevel && $user->WriteLevel >0)
				{
					//$edit_str =  "<a href=\"page.php?n=132&SiteID=$SiteID&no=$page_list->n\"><img src=\"web/images/icons/edit-page-blue.gif\"></a>";
					$edit_str =  "[<a href=\"page.php?n=132&SiteID=$SiteID&no=$page_list->n\">���������� </a> / <a href=\"page.php?n=143&SiteID=$SiteID&del_page_no=$page_list->n\">������</a>]";
					$up_str = " <a href=\"page.php?n=$n&SiteID=$SiteID&ppage=$ppage&no=$page_list->n&pos=Up\" ><img src=\"web/images/icons/arrow_up.gif\" border=0></a> ";
					$down_str = " <a href=\"page.php?n=$n&SiteID=$SiteID&ppage=$ppage&no=$page_list->n&pos=Down\"><img src=\"web/images/icons/arrow_down.gif\" border=0></a> ";
					$attached_str = "";
					if($page_list->SecLevel > 0)
						$restrict_str = " <img src=\"web/images/icons/restrict-page-red.gif\" alt=\"���������� � � ���� �� ������: $page_list->SecLevel\" border=0> ";
					else $restrict_str = "";
					
					$admin_str = $restrict_str. " | ". $up_str ." | ". $down_str ." | ". $edit_str;

				}
			else $admin_str = "";
			
			//check for page branches
			$branch_query = "SELECT * FROM pages WHERE ParentPage = '$page_list->n' AND SiteID = '$page_list->SiteID' ";
			$branch_result = $o_page->db_query($branch_query);
			
			if($o_page->db_count($branch_result) !=0)
				{
					if($count_subpages)
						$branches_counter_str = "(".$o_page->db_count($branch_result).")";
					
					if($view_map_branches)
						{
							if($site_map_level == $level)
								$show_hide_str = "<a href=\"javascript: show_hide('branch_".$page_list->n."')\"><img src=\"web/images/icons/plus.gif\" border=0></a> ";
							else $show_hide_str = "<a href=\"javascript: show_hide('branch_".$page_list->n."')\"><img src=\"web/images/icons/minus.gif\" border=0></a> ";
						}
					else $show_hide_str = "<a href=\"javascript: show_hide('branch_".$page_list->n."')\"><img src=\"web/images/icons/plus.gif\" border=0></a> ";
				}
			
			if($no == $page_list->n) $page_list->Name = str_replace($page_list->Name, "<b>".$page_list->Name."</b>", $page_list->Name);
			//pages in trash
			if($page_list->status <0)
				$page_list->Name = "<span style=\"text-decoration: line-through;\">".$page_list->Name."</span>";
			//pages locked for editing
			if($page_list->status ==0)
				$page_list->Name = "<span style=\"color: #F00\">".$page_list->Name."</span>";
			
			echo "<li>".$show_hide_str ."<a href=\"page.php?n=$page_list->n&SiteID=$page_list->SiteID\" class=\"level$level\" name=\"".$page_list->n."\">".$page_list->Name."</a> ".$branches_counter_str . $page_visits_str . $admin_str."</li>"; 

			if($o_page->db_count($branch_result) !=0 && $level < $site_map_level )
					list_site_map($Site, $page_list->n, $user, $level+1);

		}

	echo "</ul>";
}


if(isset($_GET['pos']) && isset($_GET['no']))
	{
		$pos = $_GET['pos'];
		$no = $_GET['no'];
		$page = get_page($no);
		//print_r($page);
		//echo "SiteID: ".$SiteID. " page->SiteID: ".$page->SiteID;
		//echo get_swap_n2(173774, "Up");
		if($SiteID == $page->SiteID)
			$no2 = get_swap_n2($no, $pos);
		//echo "TEST: ".$no2;
		if($no2 != NULL)
		{
			
			if($user->WriteLevel >= $row->SecLevel && $user->WriteLevel >0)
				{
					swap_pages ($no, $no2);
					//position to the affected row
					echo "
					<script language=\"JavaScript\">
						document.location = 'page.php?n=".$n."&SiteID=".$SiteID."&ppage=".$ppage."&no=".$no."#".$no."'
					</script>
					";
				}
			else 
				echo mk_output_message('warning',"������ ����� �� ����� !");
		}
		else 
			echo mk_output_message('warning',"��������! ���������� ��� �������/������� ������� �� �����! ���������� �� � ����������.");
	}

	echo "<div id=\"sitemap\" align=left>";	
	//list public sitemap
	if(!isset($ppage)) $ppage = $Site->StartPage;
	$start_page = get_page($ppage);
	echo "<ul><li><a href=\"page.php?n=$start_page->n&SiteID=$start_page->SiteID\" class=\"level0\">".$start_page->Name."</a>";
	if($user->AccessLevel > 0) echo "| <a href=\"page.php?n=132&SiteID=$SiteID&no=$start_page->n\">[ ���������� ]</a>";
	echo "<hr>"; 
	list_site_map($Site, $ppage, $user, 0);
	echo "</ul></div>";

?>
