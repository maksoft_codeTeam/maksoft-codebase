<?php
	
 include("db/logOn.inc.php");
 if (isset($no) ) {
	 $result = mysqli_query("SELECT * FROM pages p left join images im on p.imageNo = im.imageID WHERE p.n='$no'");
	 $row_no = mysqli_fetch_object($result);
	 $no_tags_selected = ""; 
 } else //NEW page
 {
   $row_no->show_link = 1;
   $no_tags_selected = "checked"; 
  }
?>
<!--
<script type="text/javascript" src="lib/test/fancybox-1.3.4/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="lib/test/fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="lib/test/fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" media="screen" />	
//-->
<script language="JavaScript" type="text/javascript">
$j(document).ready(function(){
		$j('#submit').click(function(){
			var error = false;
			var p_name = $j('#p_name').val();
			if(p_name == "")
				$j('#p_name').attr("class","error");
			if(p_name == "")
				error = true;
			
			if(error)
				{
					$j('#message').attr("class","message_error");
					$j('#message').html("���� ��������� ������ ������ !");
					$j('#message').fadeIn();
					return false;
				}
		})	

			//switch page URL box
			$j('#switch_page_url').click(function(){
				$j('#PageURL').toggle();
				$j('#PageURL2').attr('disabled', '');
				$j('#PageURL2').toggle();
				
			})
			
			$j('#PageURL').change(function(){
				$j('#PageURL2').attr('disabled', 'disabled');	
			})
			
			//pop-up file manager
			/*
			$j('#popup_file_manager').click(function(){
				window.open("web/admin/includes/_file_manager.php?n=<?=$no?>&SiteID=<?=$SiteID?>", "Images manager", "width=200,height=200");
			})
			*/
			
			//$j("#popup_file_manager").fancybox({
			$j("#change_image_link").fancybox({
				'width'				: '75%',
				'height'			: '75%',
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'autoDimensions'	: false,
				'type'				: 'iframe'
			});

			//popup image for preview
			$j("#preview_image_link").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'autoDimensions'	: true,
				'type'				: 'image'
			});

		//reset / undeo reset page image
		$j('#reset_page_image').click(function(){
				$j('#imageNo').val(0);
				$j('#preview_image').attr("src", "spacer.gif");
				$j('#reset_page_image').toggle();
				$j('#undo_reset_page_image').toggle();
			})
				
		$j('#undo_reset_page_image').click(function(){
				$j('#imageNo').val($j('#old_imageNo').val());
				$j('#preview_image').attr("src", $j('#old_image_src').val());
				$j('#reset_page_image').toggle();
				$j('#undo_reset_page_image').toggle();
			})		

		//show-hide page image content table
		$j('#show_page_image').click(function(){
				$j('#page_image').toggle();
				$j('#upload_image').toggle();
			})
			
		$j('#upload_image').click(function(){
				$j('#page_image').toggle();
				$j('#upload_image').toggle();
			})
		if($j("#page_image").is(':visible'))
			$j('#upload_image').hide();
				
		//show-hide php content tables
		$j('#show_php_content').click(function(){
				$j('#php_content').toggle();
				//$j('#page_php_vars').toggle();
				$j('#page_php_code').toggle();
			})
		
		//swap php code tables with php vars table
		$j('#show_php_code').click(function(){
			if(!$j('#page_php_vars').is(':visible') || !$j('#page_php_code').is(':visible'))
				{
					$j('#page_php_vars').toggle();
					$j('#page_php_code').toggle();
				}
			})

		$j('#show_php_vars').click(function(){
			if(!$j('#page_php_vars').is(':visible') || !$j('#page_php_code').is(':visible'))
				{
					$j('#page_php_vars').toggle();
					$j('#page_php_code').toggle();
				}
			})
			
		$j('#show_page_additions').click(function(){
			$j('#page_additions').fadeToggle(1000);
			})
		
		$j('#show_page_special_options').click(function(){
			$j('#page_special_options').fadeToggle(1000);
			})
		
		$j('#show_page_prices').click(function(){
			$j('#page_prices').fadeToggle(1000);
			})
					
		$j('#show_admin_functions').click(function(){
			$j('#page_admin_functions').fadeToggle(1000);
			})
					
		//set default image name
		$j('#imageName').focus(function(){
			var image_name = $j('#p_name').val();
			if($j(this).val() == "") $j(this).val(image_name);
		})
			
		$j('#p_name').keyup(function(){
			$j('#imageName').val($j(this).val());
		})
		
		//image align
		$j('#box_image_align a').click(function(){
			$j('#box_image_align a img').css("border", "1px solid #FFF")
			$j(this).find("img").css("border", "1px solid #F00")
			$j('#image_align').val($j(this).attr("rel"));
			$j('#image_align_label').text($j(this).attr("title"));
		})
		
		//add price row
		$j("#add_page_price").click(function(){
			$j("#page_prices tbody tr:last").clone().appendTo("#page_prices tbody");
			})			
					
	})
	
	//remove price row
	function remove_price(id)
		{
			$j(id).parent("td").parent("tr").find("#price_value").val(0);
			$j(id).parent("td").parent("tr").hide();
			
		}
	
	function set_parent_page()
		{
			//$no - n of the edited page
			//n - selected value from the menu
			var n = $j('#site_list_pages').val();
			
			$j('#ParentPage').html('<input value="Loading menu ..." disabled>');
			$j.get('web/admin/includes/site_list_pages.php?n='+n+'&SiteID=<?=$SiteID?>&no=<?=$row_no->n?>', function(data) {
				$j('#ParentPage').html(data);				
			})		
		}

	function set_image()
		{
			
			var im=document.getElementById("preview_image");
			var iml=document.getElementById("preview_image_link");
			var x=document.getElementById("imageNo").selectedIndex;
			var y=document.getElementById("imageNo").options;
			
			im.src = y[x].getAttribute('rel');
			//alert(iml.getAttribute("href"));
			//iml.href = y[x].getAttribute('rel');				
		}
</script>

<?php
//get user level from GET vars for preview
if(isset($_GET['u_level']) && $_GET['u_level'] <= $user->AccessLevel && $user->AccessLevel > 0)
	$user->AccessLevel = $_GET['u_level'];

if ($user->WriteLevel>= $row_no->SecLevel)
{
//set default page status
if(!isset($row_no->status)) $row_no->status = 1; 

//page date modified / date added
if(!isset($no) || $row_no->date_added == "0000-00-00 00:00:00") $date_added = date_time("Y-m-d H:i:s");
else $date_added = $row_no->date_added;

if(!isset($no) || $row_no->date_modified == "0000-00-00 00:00:00") $date_modified = date_time("Y-m-d H:i:s");
else $date_modified = $row_no->date_modified;		

//page is new
if(!isset($no))
	{
		if(!isset($ParentPage)) $ParentPage = $o_site->_site['StartPage'];
		$p_parent = $o_page->get_page($ParentPage);
		$image_width = 600;
		$row_no->image_src = "spacer.gif";
	}
else
{
	$p_parent = $o_page->get_page($row_no->ParentPage);
	if($row_no->image_src == "") 
		{
			if(defined(CMS_MAIN_WIDTH))
				$image_width = CMS_MAIN_WIDTH;
			$image_width = "600";
			$row_no->image_src = "spacer.gif";
		}
	else
		{
			$image_size = GetImageSize($row_no->image_src);
			$image_width = $image_size[0];
			$image_height = $image_size[1];
		}
}	

	$img_preview_width = 200;
	$img_preview_height = 150;
	/*
	$img_preview_ratio = $image_width / $image_height;

	if($img_preview_ratio <1)
		$img_width = $img_preview_height * $img_preview_ratio;
	
	if($image_width > $img_preview_width) $image_width = 200;
	if($image_height > $img_preview_height) $image_height = 150;
	*/
	$img_preview_size = "width=\"".$img_preview_width."\"";
	
?>
<div id="cms_admin">
<form action="page.php?n=112&SiteID=<?=$SiteID?>" method="post" enctype="multipart/form-data" name="addPage" id="addPage">
<?php
	//mk_output_message("warning", "�������E: ��������� <a href=\"page.php?n=157421&SiteID=1\">����� ��������</a> � �������� �������� !<br><br><b>�� ������ ����������</b>: <a href=\"page.php?n=157422&SiteID=1\">��� ������ ���������</a> / <a href=\"page.php?n=157423&SiteID=1\">�������� � ������</a>");
	if($row_no->SiteID != $SiteID && isset($no)) die ("<div class=\"message_error\">������ ����� �� ����������� ���� ��������. �� �� ���������� �� ����� ����.</div>");

	if($user->AccessLevel >=4 && $no>0)
		{
		?>		
		<fieldset class="level5"><legend id="show_admin_functions">- A�������������� ������� -</legend>
		<table border="0" cellpadding="0" cellspacing="10" class="content" id="page_admin_functions" style="display: none;">
			<tr>
				<td class="border" width="33%"><?php if($user->WriteLevel>=$page->AccessLevel) echo "<a href='page.php?n=1890&PPageNo=$no&SiteID=$SiteID'\" title='��������� �� ����������'><img src=\"web/admin/images/icons/icon_page_duplicate.jpg\"></a>";?>
				<td class="border" width="33%"><?php
					if($row_no->status == 0)
						echo "<a href='page.php?n=143&del_page_no=$no&SiteID=$SiteID'\" title='�������� ����������'><img src=\"web/admin/images/icons/icon_page_empty.jpg\"></a>";
						//echo "<input type=button class=\"button_submit\"  onclick=\"javascript: document.location='page.php?n=143&del_page_no=$no&SiteID=$SiteID'\" value='�������� ����������'>";
					else
						echo "<a href='page.php?n=143&del_page_no=$no&SiteID=$SiteID'\" title='��������� �� ����������'><img src=\"web/admin/images/icons/icon_page_delete.jpg\"></a>";
						//echo "<input type=button class=\"button_submit\"  onclick=\"javascript: document.location='page.php?n=143&del_page_no=$no&SiteID=$SiteID'\" value='��������� �� ����������'>";
					?>
				<td width="33%"><a href="page.php?n=145&SiteID=<?=$SiteID?>&ppage=<?=$no?>" title="��� � ����� �� �����"><img src="web/admin/images/icons/icon_search.jpg"></a>
		<tr>
			<td><span class="label">��������� ��������</span><div class="field"><input type="text" id="mirror_page" name="an" value="<?=$row_no->an?>"></div></td>
			<td></td>
			<td></td>
		</table>
		</fieldset>
		<?php
	}
		
		//just for demo list userlevels
		if($user->AccessLevel > 5)
		{
		echo "<div class=\"user_levels\">";
		for($i=1; $i<=$user->AccessLevel; $i++)
			{
				$class = "";
				if($u_level == $i) $class = "selected";
				echo "<a href=\"page.php?n=$n&no=$no&SiteID=$SiteID&u_level=$i\" class=\"".$class."\">user level $i</a>";
			}
		echo "</div><br clear=\"all\">";
		}
?>
<table border="0" cellpadding="10" cellspacing="0" class="content">
	<tr>
		<td width="50%" class="border"><span class="label">���:</span><div class="field"><input name="NamePage" id="p_name" type="text" value="<?php echo $row_no->Name;?>"></div>
		<td width="50%"><span class="label">��������� ��������:</span>
		<div class="field" id="ParentPage">
		<?php
			
			if(!isset($no) && $SiteID == $p_parent['SiteID'])
				{
					
					echo "<input type=\"text\" value=\"".$p_parent['Name']."\" disabled><input type=\"hidden\" name=\"ParentPage\" value=\"".$ParentPage."\">";
				}
			else
				{
				if($no == $Site->StartPage)
					echo "<input type=\"text\" value=\"- no parent page -\" disabled><input type=\"hidden\" name=\"ParentPage\" value=\"0\">";
				else
				echo "<select name=\"ParentPage\" id=\"site_list_pages\" onChange=\"set_parent_page()\"><option value=\"".$row_no->ParentPage."\">".$o_page->get_pName($row_no->ParentPage)."</option><option  value=\"".$p_parent['ParentPage']."\"> - ������ ��������� �������� -</option></select>";
				}
		?>
		</div>
	</tr>
	<tr>
		<td colspan="2" width="100%"><span class="label">��������:</span><div class="field"><input name="title" type="text" value="<?php echo $row_no->title;?>"></div>
	</tr>
	<tr>
		<td colspan="2">
<!-- TEXT EDITOR //-->

<?php
		
	if(!isset($te_theme) && empty($te_theme))
	{
		$te_theme = $Site->te_theme;
		
		if(isset($_GET['te_theme']))
			$te_theme = $_GET['te_theme'];			
		
		if(defined(CMS_TE_THEME)) $te_theme = CMS_TE_THEME;

	}
	
	if($user->AccessLevel <= 1)
			$te_theme = "simple";
	
	if($user->AccessLevel == 2)
			$te_theme = "basic";
				
	if($user->AccessLevel == 3)
			$te_theme = "default";
	
	if($user->AccessLevel > 3)
			$te_theme = "advanced";
	
	//page is new
	if(!isset($no))
		$te_theme = "simple";
	
	//check for individual text editor setup in folder text_editor/separate/<SiteID>.php
	//this skips the text_editor theme switch
	$separate_te_theme = "text_editor/separate/".$SiteID.".php";

	if(file_exists($separate_te_theme))
		{
			//generate images list for TEXT editor image url list
			include "web/admin/image_list_generator.php";
			//generate links list for TEXT editor url list, generates as reading PUB dir
			include "web/admin/links_list_generator.php";
			include $separate_te_theme;
		}
	else	
	switch($te_theme)
	{
		case "basic": 
							{	
							include "text_editor/basic.php";	
							break;
							}
		case "simple": 
							{	

							include "text_editor/simple.php";	
							$no_tags_selected = "checked";
							break;
							}
		case "advanced": 
							{	
								//generate images list for TEXT editor image url list
								include "web/admin/image_list_generator.php";
								//generate links list for TEXT editor url list, generates as reading PUB dir
								include "web/admin/links_list_generator.php";
								//include text editor
								include "text_editor/advanced.php";
								break;
							}
		case "default" :
		default :
							{
								include "text_editor/default.php";
							}
	}
		 
$edit_rows=14; 
if(strlen($row_no->textStr)>400) 
	$edit_rows=22; 	

?>
		<span class="label">����� �� ����������</span>
		<textarea id="text_editor" name="textStr" style="width: 100%" rows="<?=$edit_rows?>" wrap="VIRTUAL"><?php echo $row_no->textStr;?></textarea>
<!-- END TEXT  EDITOR //-->
<div style="background: #416680; color: #FFF; display: block; height: 20px; line-height: 20px; width: 300px; padding: 5px;"><input name="notags" type="checkbox" style="background: transparent;" value="1" <?php echo("$no_tags_selected"); ?> class="checkbox"><a href="/page.php?n=1858&SiteID=1" target="_blank" style="color: #FFF; text-decoration: none; text-transform: uppercase;">������ ��������� ������ �� �����</a></div>
	<tr>
		<td colspan="2"><span class="label">������� ����</span><div class="field"><input name="tags" type="text" id="tags" value="<?=$row_no->tags ?>"></div>
        <?php
       		if($user->AccessLevel >=5)
				{
					?>
                    <fieldset class="level5"><legend>�������� �� ������� ����</legend>
                    <?
					$keywords_query = "SELECT DISTINCT keywords, n, SiteID FROM keywords WHERE SiteID = '".$SiteID."' AND keywords != '' AND n = '".$no."' ORDER BY enter_time ASC LIMIT 25";
					$keywords_result = mysqli_query($keywords_query);
					while($keywords = mysqli_fetch_array($keywords_result))
						echo "<em>".$keywords['keywords']."</em>, ";
					?>
                    </fieldset>
                    <?
				}
		?>
	</tr>
</table>
<br clear="all">
<fieldset><legend id="show_page_image">- ����������� -</legend>
<center><input type="button" class="button" value="+ ������ �����������" id="upload_image"></center>
<table border="0" cellpadding="10" cellspacing="1" class="content" id="page_image" style="display:<?=($row_no->imageNo == 0)? "none":""?>;">
	<tr>
		<tr><td width="200px" class="border" valign="top" rowspan="2"><div class="page_image"><a href="page.php?n=2017&SiteID=<?=$o_site->SiteID?>&imageID=<?=$row_no->imageNo?>&im_dir=<?=$row_no->image_dir?>" class="edit" title="�������� �� �������������"></a><a href="<?=$row_no->image_src?>" id="preview_image_link" title="������: <?=$row_no->imageName?>"><img src="<?=$row_no->image_src?>" id="preview_image" border="0" <?=$img_preview_size?>></a></div>
			<a href="web/admin/includes/images_manager.php?n=<?=$no?>&SiteID=<?=$SiteID?>" class="change" title="������� �� ������������� �� �������� <?=$row_no->Name?>" id="change_image_link">�����</a>	
			<a href="javascript:;" class="change" id="reset_page_image">��������</a>
			<a href="javascript:;" class="change" id="undo_reset_page_image" style="display: none;">����������</a>
			<input type="hidden" name="old_image_src" id="old_image_src" value="<?=$row_no->image_src?>">
			<input type="hidden" name="old_imageNo" id="old_imageNo" value="<?=$row_no->imageNo?>">
			<td valign="top" style="background: #f9f9f9">
		<!-- for old drop down menu ask M.Petrov //-->
	  	<input type="hidden" name="imageNo" value="<?=$row_no->imageNo?>" id="imageNo">
		<span class="label">���� ������:</span><div class="field"><input name="userfile" type="file" id="userfile"></div>
		<span class="label">�������� �� ��������:</span><div class="field"><input name="imageName" type="text" id="imageName" value="<?=$row_no->Name?>"></div>
        <span class="label">������ � ����������:</span><div class="field">
        <select name="img_dir">
			<?php
                    $s_dirs = $o_site->get_sImageDirectories();
					for($i=0; $i<count($s_dirs); $i++)
                        echo "<option value=\"".$s_dirs[$i]["ID"]."\" ".(($s_dirs[$i]["ID"] == $o_site->_site['imagesdir']) ? "selected":"").">".$s_dirs[$i]["Name"]."</option>";
            ?>
		</select>
        </div>
		<tr><td valign="top">
        <div style="width: 49%; float: left;">
		<span class="label">������������ �� ��������: <b id="image_align_label"></b></span>	
			<div class="field">
			<select name="image_allign" >
				<option value="1" selected>--- �� ������������ --- </option>
				<?php 
				$alligns_cat = mysqli_query("SELECT DISTINCT valign FROM alligns");
				while($cat = mysqli_fetch_object($alligns_cat))
					{		
						$label = "";
						if($cat->valign == "top") $label = "��� ������";
						if($cat->valign == "bottom") $label = "��� ������";
						if($cat->valign == "") $label = "���������";
						
						echo "<optgroup label=\"".$label."\">";	
					
						$alligns = mysqli_query("SELECT * FROM alligns WHERE status = 1 AND valign= '$cat->valign' ORDER by allignID ASC");
						
							while ($allign_list = mysqli_fetch_object($alligns))
							{
								if ($allign_list->allignID == $row_no->image_allign)
									$selected = "selected";
								else $selected="";
								
								echo "<option value=$allign_list->allignID $selected > $allign_list->AllignText</option>";
							}
						echo "</optgroup>";
					}
				?>
			</select>
		  </div>
          </div>
          <div style="width: 49%; float: left; margin: 0 0 0 2px;">
		  <div id="box_image_align" class="box_image_align">
		  	<a href="javascript:;" rel="0" title="�� ������������"><img src="web/admin/images/icons/img_align_default.jpg"></a>
			<a href="javascript:;" rel="1" title="� ���� ��� ������"><img src="web/admin/images/icons/img_align_left.jpg"></a>
			<a href="javascript:;" rel="2" title="� ����� ��� ������"><img src="web/admin/images/icons/img_align_right.jpg"></a>
			<a href="javascript:;" rel="3" title="� ������� ��� ������"><img src="web/admin/images/icons/img_align_center.jpg"></a>
			<a href="javascript:;" rel="4" title="� ���� ��� ������"><img src="web/admin/images/icons/img_align_left2.jpg"></a>
			<a href="javascript:;" rel="5" title="� ����� ��� ������"><img src="web/admin/images/icons/img_align_right2.jpg"></a>
			<a href="javascript:;" rel="6" title="� ������� ��� ������"><img src="web/admin/images/icons/img_align_center2.jpg"></a>
			<a href="javascript:;" rel="21" title="�������� �����"><img src="web/admin/images/icons/img_align_banner.jpg"></a>
		</div>
		<span class="label">������: (�� <b><?=$image_width?></b>px.)</span> <div class="field"><input name="imageWidth" type="text" id="imageWidth" value="<?php if ($no>0) {echo("$row_no->imageWidth"); } else {echo("200");}?>"></div>
		</div>
</table><br>
</fieldset>
<br>
<?php
	if($user->AccessLevel >= 5 && $no>0)
		{
		?>
		<fieldset class="level5"><legend id="show_php_content">- PHP content-</legend>
		<table border="0" cellpadding="10" cellspacing="0" class="content" id="php_content" style="display:<?=(empty($row_no->PHPvars) && empty($row_no->PHPcode))?"none":""?>;">
			<tr id="page_php_vars" style="display:<?=(empty($row_no->PHPvars))?"none":""?>;">
				<td class="border"><span class="label">PHP variables</span><textarea name="PHPvars" rows="10" class="code"><?=$row_no->PHPvars?></textarea><br><a href="javascript:;" id="show_php_code">+ PHPcode</a>
			<tr id="page_php_code" style="display:<?=(empty($row_no->PHPcode))?"none":""?>;">
            	<td><span class="label">PHP code</span><textarea name="PHPcode" class="code" rows="10"><?=$row_no->PHPcode?></textarea><br><a href="javascript:;" id="show_php_vars">+ PHPvars</a>
		</table>
		</fieldset>
		<?php
		}
?>

<fieldset><legend>- ��������� �� ������������ -</legend>
<table border="0" cellpadding="10" cellspacing="0" class="content">
  <tr>
    <td width="50%" class="border">
	<div class="field">
      <select name="show_link" id="select2">
        <option value="1" <?php if ($row_no->show_link == 1) {echo("selected");} ?> >������
        ������ ��� �������������</option>
        <optgroup label="������ ������������ �� �������������">
        <option value="2" <?php if ($row_no->show_link == 2) {echo("selected");} ?> >������
        ����������, ������ � �������� �� �������������</option>
        <option value="5" <?php if ($row_no->show_link == 5) {echo("selected");} ?> >������
        ������ � �������� �� �������������</option>
        <option value="6" <?php if ($row_no->show_link == 6) {echo("selected");} ?> >������
        ���� ������������ �� �������������</option>
        <option value="10" <?php if ($row_no->show_link == 10) {echo("selected");} ?> >������
        ���������� � ������������ �� �������������</option>
        </optgroup>
        <optgroup label="���������">
        <option value="3" <?php if ($row_no->show_link == 3) {echo("selected");} ?> >������
        ���������� � �������� �� �������������</option>
        <option value="9" <?php if ($row_no->show_link == 9) {echo("selected");} ?> >������
        ���� �������� �� �������������</option>
        </optgroup>
        <optgroup label="����� ��� �������������">
        <option value="4" <?php if ($row_no->show_link == 4) {echo("selected");} ?> >������
        ��������, ���������� � ������� ����� �� �������������</option>
        <option value="7" <?php if ($row_no->show_link == 7) {echo("selected");} ?> >������
        �������� � ������� ����� �� �������������</option>
        <option value="8" <?php if ($row_no->show_link == 8) {echo("selected");} ?> >������
        ������� ����� �� �������������</option>
        <option value="11" <?php if ($row_no->show_link == 11) {echo("selected");} ?> >������
        ���������� � ������ ����� �� �������������</option>
        </optgroup>
        <optgroup label="������������ ������">
        <option value="13" <?php if ($row_no->show_link == 13) {echo("selected");} ?> >������
        ��������, ���������� � �������������� ������</option>
        <option value="14" <?php if ($row_no->show_link == 14) {echo("selected");} ?> >������
        �������� � �������������� ������</option>
        <option value="15" <?php if ($row_no->show_link == 15) {echo("selected");} ?> >������
        ���������� � �������������� ������</option>
        </optgroup>
        <optgroup label="����� ������">
        <option value="0" <?php if ($row_no->show_link == 0) {echo("selected");} ?> > ��
        ������������ ������ ��� �������������</option>
        </optgroup>
      </select>
	  </div>
      <div class="field">
      <select name="show_link_cols">
        <option value="1" <?php if ($row_no->show_link_cols == 1) {echo("selected");} ?> >�
        1 ������</option>
        <option value="2" <?php if ($row_no->show_link_cols == 2) {echo("selected");} ?> >�
        2 ������</option>
        <option value="3" <?php if ($row_no->show_link_cols == 3) {echo("selected");} ?> >�
        3 ������</option>
        <option value="4" <?php if ($row_no->show_link_cols == 4) {echo("selected");} ?> >�
        4 ������</option>
        <option value="5" <?php if ($row_no->show_link_cols == 5) {echo("selected");} ?> >�
        5 ������</option>
        <option value="6" <?php if ($row_no->show_link_cols == 6) {echo("selected");} ?> >�
        6 ������</option>
        <option value="7" <?php if ($row_no->show_link_cols == 7) {echo("selected");} ?> >�
        7 ������</option>
        <option value="8" <?php if ($row_no->show_link_cols == 8) {echo("selected");} ?> >�
        8 ������</option>
      </select>
	  </div>
    </td>
    <td width="50%">
	<div class="field">
	<select name="show_link_order" id="select6">
      <optgroup label="�� ������������">
      <option <?php if ($row_no->show_link_order == "ASC") echo "selected"; ?> value="ASC">�������� ��� 0-9</option>
      <option <?php if ($row_no->show_link_order == "DESC") echo "selected"; ?> value="DESC">�������� ��� 9-0</option>
      </optgroup>
      <optgroup label="�� ��� �� ����������">
      <option <?php if ($row_no->show_link_order == "p.Name ASC") echo "selected"; ?> value="p.Name ASC">���
      �������� ��� A-Z</option>
      <option <?php if ($row_no->show_link_order == "p.Name DESC") echo "selected"; ?> value="p.Name DESC">�
      �������� ��� Z-A</option>
      </optgroup>
      <optgroup label="�� ����">
      <option <?php if ($row_no->show_link_order == "p.date_added ASC") echo "selected"; ?> value="p.date_added ASC">���
      �������� ��� 0-9</option>
      <option <?php if ($row_no->show_link_order == "p.date_added DESC") echo "selected"; ?> value="p.date_added DESC">�
      �������� ��� 9-0</option>
      </optgroup>
      <optgroup label="�� ������">
      <option <?php if ($row_no->show_link_order == "p.toplink ASC") echo "selected"; ?> value="p.toplink ASC">��������
      ���</option>
      <option <?php if ($row_no->show_link_order == "p.toplink DESC") echo "selected"; ?> value="p.toplink DESC">��������
      ���</option>
      </optgroup>
      <optgroup label="�����">
      <option <?php if ($row_no->show_link_order == "RAND()") echo "selected"; ?> value="RAND()">����������</option>
      </optgroup>
    </select>
     </div>
	 <div class="field">
      <select name="make_links" id="select7">
        <option value="1" <?php if (($row_no->make_links == 1) || ($n == 111)) {echo("selected");} ?> >�
        ��������� ������</option>
        <option value="2" <?php if ($row_no->make_links == 2) {echo("selected");} ?> >������
        ���� �� ����������</option>
        <option value="3" <?php if ($row_no->make_links == 3) {echo("selected");} ?> >������
        ���� �� �������� (popup)</option>
        <option value="4" <?php if ($row_no->make_links == 4) {echo("selected");} ?> >������
        �� ������ ������ + ���������� (�������)</option>
        <option value="0" <?php if (($row_no->make_links == 0) && ($n != 111) ) {echo("selected");} ?>>���
        ������</option>
      </select>
	  </div>
	  </td>
  </tr>
<?php
if($user->AccessLevel >=3 && $no>0)
	{
	?>
    	<tr>
            <td width="50%" class="border"><span class="label">������ ������������ �� �����:</span><div class="field"><?=list_page_groups("show_group_id", $row_no->show_group_id, "style=\"width: 100%;\"")?></div>
			<td width="50%"><br><a href="page.php?n=63931&SiteID=<?=$SiteID?>" target="_blank" class="add"> ������ �����</a>
		</tr>
	<?php
	}
	?>
  
</table>
</fieldset>
   
<?php
	if($user->AccessLevel >=3 && $no>0)
		{
		?>		
		<fieldset class="level3"><legend id="show_page_additions">- ���������� -</legend>
		<table border="0" cellpadding="10" cellspacing="0" class="content" id="page_additions" style="display:<?=(empty($row_no->PageURL))?"none":""?>;">
			<tr><td>
		<div class="field">
		<?php 
					echo("<select name=\"PageURL\" id=\"PageURL\">");
					if (!(empty($row_no->PageURL)) )
						{
							//echo("<option value=\"$row_no->PageURL\" selected>- $row_no->PageURL -</option>");
							echo(" <option value=''>- ���������� ������������ -</option> ");
						}
					else
						echo(" <option value=''>- �������� -</option> ");
					
					$personal_urls = "";
					$public_urls = "";
					$urls = mysqli_query("SELECT * FROM PageURLs WHERE (SiteID = '0' OR SiteID = '$SiteID') ORDER BY Name ASC ");
					while ($url = mysqli_fetch_object($urls))
					   {
							if ($url->filename == $row_no->PageURL) $url_selected = "selected";
								else $url_selected="";
							if($url->SiteID == $SiteID)
								$personal_urls.= "<option value=$url->filename $url_selected> $url->Name </option>";
							else
							{
								$public_urls.="<option value=$url->filename $url_selected> $url->Name </option>";
							}
						}
					echo "<optgroup label=\"���������� ����������\">".$personal_urls . "<optgroup label=\"�������� ����������\">".$public_urls;	
					echo "</select>";
					//show only when 
					echo("<input name=\"PageURL\" id=\"PageURL2\" style=\"display: none;\" type=\"text\" value=\"$row_no->PageURL\" disabled>");
			 
		?>
		</div>
		<?php
			if($user->AccessLevel >=5)
				echo "<a href=\"javascript: void(0)\" id=\"switch_page_url\">+ �����</a><br>";
		?>
		</table>
		</fieldset>
		<?php
	}
?>
<?php
	if($user->AccessLevel >=3 && $no>0)
		{
		?>		
		<fieldset class="level3"><legend id="show_page_special_options">- ��������� ����� -</legend>
		<table border="0" cellpadding="10" cellspacing="0" class="content" style="display:none;" id="page_special_options">
			<tr><td class="border">
			<span class="label">��� �� ����������</span>
			<div class="field">
			<select name="toplink">
				<option value="0" <?php if(($row_no->toplink)==0) echo "selected"; ?>>-
				<option value="1" <?php if(($row_no->toplink)==1) echo "selected"; ?>>��� ������
				<option value="2" <?php if(($row_no->toplink)==2) echo "selected"; ?>>������� ����
				<option value="3" <?php if(($row_no->toplink)==3) echo "selected"; ?>>���. ������
				<option value="4" <?php if(($row_no->toplink)==4) echo "selected"; ?>>vip ������
				<option value="5" <?php if(($row_no->toplink)==5) echo "selected"; ?>>����� ������
			</select>
			</div>
			<?php
				if($user->AccessLevel >=5)
					{
					?>
					<td class="border">
					<span class="label">������ �� ����������:</span><div class="field">
						<select name="status">
							<option value="1" <?=($row_no->status == 1)? "selected":""?>>������
							<option value="0" <?=($row_no->status == 0)? "selected":""?>>���������
							<option value="-1" <?=($row_no->status == -1)? "selected":""?>>����������
						</select>
						</div>
					<?php
					}
					
				//add page directly to a selected group
				if($user->AccessLevel >=3)
					{
						?>
						<tr>
						<td class="border">
						<span class="label">������ ���������� � �����:</span>
						<div class="field"><?=list_page_groups("ptg_group_id", 0, "style=\"width: 100%;\"")?></div>
						<td class="border">
						<span class="label">��������� ��� �������:</span><span class="label" style="padding: 0 0 0 70px">���� �� ��������:</span>
						<div class="field"><input type="text" name="ptg_label" style="width: 49%; float: left;"><input type="text" name="ptg_end_date" id="datepicker" value="0000-00-00" style="width: 49%; float: left; border-left: 1px solid #999"></div>
						<?php
					}
			?>
		</table>
		</fieldset>
		<?php
	}
	
	//page prices module
	$pPrices = $o_page->get_pPrice($no, "ASC");
	
	if(isset($no) && count($pPrices) > 0)
		{
			?>
			<fieldset class="level3"><legend id="show_page_prices">- ���� -</legend>
			<table border="0" cellpadding="5" cellspacing="0" class="content" id="page_prices">
            <thead>
            	<tr>
                	<th>��������</th>
                    <th>���</th>
                    <th>����������</th>
                    <th>����</th>
                    <th width="25"></th>
            </thead>
            <tbody>    
			<?php
				for($i=0; $i<count($pPrices); $i++)
					{
						echo "<tr>";
						echo "<td><input name=\"price_description[]\" type=\"text\" id=\"price_description\" size=\"20\" value=\"".$pPrices[$i]['description']."\">";
						echo "<td><input name=\"price_code[]\" type=\"text\" id=\"price_code\" size=\"5\" value=\"".$pPrices[$i]['code']."\">";
						echo "<td><input name=\"price_qty[]\" type=\"text\" id=\"price_qty\" size=\"5\" value=\"".$pPrices[$i]['qty']."\">";
						echo "<td><input name=\"price_value[]\" type=\"text\" id=\"price_value\" size=\"10\" maxlength=\"10\" value=\"".$pPrices[$i]['price_value']."\">"; 
						echo "<td><a href=\"javascript: void(0)\" class=\"delete\" onClick=\"remove_price(this)\"></a><input name=\"price_id[]\" type=\"hidden\" id=\"price_id\" size=\"10\" maxlength=\"10\" value=\"".$pPrices[$i]['id']."\">";
						echo "</tr>";
					}
			?>
            </tbody>
			<tfoot>
            	<tr><td colspan="5"> <a href="javascript: void(0)" id="add_page_price" class="add">������ ����</a>
            </tfoot>
            </table>
           
			</fieldset>
			<?
			
		}
?>
		<fieldset><legend>- ��������� �� ���������� -</legend>
		<table border="0" cellpadding="10" cellspacing="0" class="content">
			<tr>
				<td colspan="2">
			<tr>
				<td width="50%" class="border">
					<div class="field">
					<select name="SecLevel" id="SecLevel">
					<?php
					for ($i=0; $i<=$user->WriteLevel; $i++) {
					if ($row_no->SecLevel == $i) {$selected="selected";} else {$selected="";}
					if ($i == 0) {
					$textLevel = "������������";
					} else {
					$textLevel = "���� �� ������"; 
					$textLevel = "$textLevel: $i"; 
					}
					echo("<option value=\"$i\" $selected>$textLevel</option>");
					}
					?>
					</select>
					</div>
				<td width="50%">���� �� ��������:<br><?=$date_added?><br><input type="hidden" name="date_added" value="<?=$date_added?>">
                <br>���� �� �������� ��������<br><?=$date_modified?>
                
		</table>
		</fieldset>
<?php
		//}
?>
<div id="message" style="display: none;"></div>
<table border="0" cellpadding="10" cellspacing="0" class="content">
	<tr>
	<?php
		if($no>0)
			echo "<td width=\"100px\" align=\"left\"><a href=\"page.php?n=143&SiteID=$SiteID&del_page_no=$no\" class=\"delete\">������</a>";
	?>
	<td align="center"><input name="submit" type="submit" id="submit" value="������ ����������" class="submit">
</table>
<input type="hidden" name="no" value="<?=$no?>">
<input type="hidden" name="SiteID" id="SiteID" value="<?=$SiteID?>">
</form>
<?php
}
else mk_output_message("warning", "������ ����� �� <b>�����</b> �� ���� �������� !");
?>
</div>