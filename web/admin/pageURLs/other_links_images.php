<?php
$other_links_sql = "SELECT * FROM pages p join images i on i.imageID = p.imageNo  WHERE p.SiteID = '$SiteID' AND ParentPage = '$row->ParentPage' AND p.SecLevel=0 ORDER BY RAND() ";

if ($row->ParentPage==0) {
    $other_links_sql = "SELECT * FROM pages p join images i on i.imageID = p.imageNo  WHERE p.SiteID = '$SiteID' AND ParentPage = '$n' AND p.SecLevel=0 ORDER BY RAND() ";
}
        
$result = $o_page->db_query($other_links_sql);

?>
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=latin-ext' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="/web/admin/pageURLs/other-links-images/css/ideaboxTimeline.css"/>


<style>
    .it-box{font-family:'PT Sans'; font-size:16px;}
body {
  font: 12px/1.5 'PT Sans', serif;
  margin: 25px;
}

.tags {
  list-style: none;
  margin: 0;
  overflow: hidden; 
  padding: 0;
}

.tags li {
  float: left; 
}

.tag {
  background: #eee;
  border-radius: 3px 0 0 3px;
  color: #999;
  display: inline-block;
  height: 26px;
  line-height: 26px;
  padding: 0 20px 0 23px;
  position: relative;
  margin: 0 10px 10px 0;
  text-decoration: none;
  -webkit-transition: color 0.2s;
}

.tag::before {
  background: #fff;
  border-radius: 10px;
  box-shadow: inset 0 1px rgba(0, 0, 0, 0.25);
  content: '';
  height: 6px;
  left: 10px;
  position: absolute;
  width: 6px;
  top: 10px;
}

.tag::after {
  background: #fff;
  border-bottom: 13px solid transparent;
  border-left: 10px solid #eee;
  border-top: 13px solid transparent;
  content: '';
  position: absolute;
  right: 0;
  top: 0;
}

.tag:hover {
  background-color: crimson;
  color: white;
}

.tag:hover::after {
   border-left-color: crimson; 
}
</style>
<div style="width:100%; max-width:960px; margin:0 auto; padding:0 20px 50px 20px; box-sizing:border-box;">

    <div class="ideaboxTimeline" id="i-timeline">
        <div class="it-spine"></div>
        <?php while($other_links = $o_page->fetch("object", $result)): ?>
           <div class="it-box">
            <div class="it-content">
                <h2><a href="page.php?n=<?=$other_links->n.'&SiteID='.$other_links->SiteID?>" title="<?=$Site->Title?> - <?=$other_links->Name?>" rel="nofollow">+ <?=$other_links->Name?></a></h2>
                <a href="page.php?n=<?=$other_links->n.'&SiteID='.$other_links->SiteID?>" class="it-image">
                    <img src="<?=$other_links->image_src?>" alt="" />
                    <span><i></i></span></a>
                <div class="it-infobar">
                    <a href="page.php?n=<?=$other_links->n.'&SiteID='.$other_links->SiteID?>" class="it-readmore">��� ������</a>
                </div>
            </div>
            <div class="it-iconbox">
                <span></span>
            </div>
        </div>
<?php endwhile; ?>
    </div>  
</div>
<ul class="tags">
<?php
    $tags_arr = split(",", $o_page->_page['tags'], 10); 
    if(count($tags_arr)>1){
        echo "";
        foreach($tags_arr as $tagword) {
            $pages_tagged_q = $o_page->db_query("SELECT * FROM pages WHERE SiteID='$SiteID' AND  tags LIKE '%$tagword%' "); 
            if($o_page->db_count($pages_tagged_q)>2) {
                $tagword = preg_replace("/^\s+/",  "", $tagword); 
                $link = $o_page->scheme.$o_page->_site['primary_url'] .'/tag/'.$tagword; 
                echo "<li><a href='$link' class='tag'>$tagword</a></li>";
            }
        }
    }
?>
</ul>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
<script src="/web/admin/pageURLs/other-links-images/js/ideaboxTimeline.js"></script>
<script>
    jQuery(document).ready(function(e) {
        jQuery("#i-timeline").ideaboxTimeline();
    });
 </script>
