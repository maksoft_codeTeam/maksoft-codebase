<?php
#echo '<h1> ������� �������� �� �������� �� ������� ������</h1>';
require_once "/hosting/maksoft/maksoft/modules/vendor/autoload.php";
require_once __DIR__.'/../../lib/Database.class.php';
require_once __DIR__.'/../../global/admin/PageTranslationForm.php';
use \Maksoft\Gateway\Gateway;
$db = new Database();
$gate = new Gateway($db);
$_SERVER['referer'] = $_SERVER['PHP_SELF'];
    
// include("db/logOn.inc.php");
if (isset($no) ) {
    $row_no = $o_page->get_page($no, 'object');
    $no_tags_selected = ""; 
    $form = new PageTranslationForm($gate, $row_no->n, $_POST, $o_site->_site['SitesID']);
    if($_SERVER['REQUEST_METHOD'] === 'POST' and isset($_POST['action']) and $_POST['action'] == $form->action->value) {
        try{
        $form->is_valid();
        $res = $o_page->db_query("INSERT INTO page_slug SET slug = '".$_POST['slug']."',  n = '$no', SiteID = '$SiteID'  ");     
        $form->save();
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo '<h3> �������� �� �������� ������ </h3>';
            echo '<ul>';
            foreach($form as $field){
                foreach($field->get_errors() as $err){
                    echo '<li>'.$err.'</li>';
                }
            }
            echo '</ul>';
        }
    }
} else {
    $row_no->show_link = 1;
    $no_tags_selected = "checked"; 
}

$img_preview_width = 200;

$img_preview_height = 150;

$img_preview_size = "width=\"".$img_preview_width."\"";

$parent_page = $row_no->parentPage;

$edit_rows=14; 
?>

<script language="JavaScript" type="text/javascript">
$j(document).ready(function(){
    $j('#submit').click(function(){
        $j(this).hide();
        var error = false;
        var p_name = $j('#p_name').val();
        if(p_name == "")
            $j('#p_name').attr("class","error");
        if(p_name == "")
            error = true;
        
        if(error)
            {
                $j('#message').attr("class","message_error");
                $j('#message').html("���� ��������� ������ ������ !");
                $j('#message').fadeIn();
                return false;
            }
    })  

    //switch page URL box
    $j('#switch_page_url').click(function(){
        $j('#PageURL').toggle();
        $j('#PageURL2').attr('disabled', '');
        $j('#PageURL2').toggle();
        
    })
    
    $j('#PageURL').change(function(){
        $j('#PageURL2').attr('disabled', 'disabled');   
    })
    
    $j("#change_image_link").fancybox({
        'width'             : '75%',
        'height'            : '75%',
        'autoScale'         : false,
        'transitionIn'      : 'none',
        'transitionOut'     : 'none',
        'autoDimensions'    : false,
        'type'              : 'iframe'
    });

    //popup image for preview
    $j("#preview_image_link").fancybox({
        'autoScale'         : true,
        'transitionIn'      : 'none',
        'transitionOut'     : 'none',
        'autoDimensions'    : true,
        'type'              : 'image'
    });

    //reset / undeo reset page image
    $j('#reset_page_image').click(function(){
            $j('#imageNo').val(0);
            $j('#preview_image').attr("src", "spacer.gif");
            $j('#reset_page_image').toggle();
            $j('#undo_reset_page_image').toggle();
        })
            
    $j('#undo_reset_page_image').click(function(){
            $j('#imageNo').val($j('#old_imageNo').val());
            $j('#preview_image').attr("src", $j('#old_image_src').val());
            $j('#reset_page_image').toggle();
            $j('#undo_reset_page_image').toggle();
        })      

    //show-hide page image content table
    $j('#show_page_image').click(function(){
            $j('#page_image').toggle();
            $j('#upload_image').toggle();
        })
        
    $j('#upload_image').click(function(){
            $j('#page_image').toggle();
            $j('#upload_image').toggle();
        })
    if($j("#page_image").is(':visible')){
        $j('#upload_image').hide();
    }
            
    //show-hide php content tables
    $j('#show_php_content').click(function(){
            $j('#php_content').toggle();
            $j('#page_php_vars').hide();
            $j('#page_php_code').show();
        })
    
    //swap php code tables with php vars table
    $j('#show_php_code').click(function(){
        if(!$j('#page_php_vars').is(':visible') || !$j('#page_php_code').is(':visible'))
            {
                $j('#page_php_vars').toggle();
                $j('#page_php_code').toggle();
            }
        })

    $j('#show_php_vars').click(function(){
        if(!$j('#page_php_vars').is(':visible') || !$j('#page_php_code').is(':visible'))
            {
                $j('#page_php_vars').toggle();
                $j('#page_php_code').toggle();
            }
        })
        
    $j('#show_page_additions').click(function(){
        $j('#page_additions').fadeToggle(1000);
        })
    
    $j('#show_page_special_options').click(function(){
        $j('#page_special_options').fadeToggle(1000);
        })
    
    $j('#show_page_prices').click(function(){
        $j('#page_prices').fadeToggle(1000);
        })
                
    $j('#show_admin_functions').click(function(){
        $j('#page_admin_functions').fadeToggle(1000);
        })
                
    //set default image name
    $j('#imageName').focus(function(){
        var image_name = $j('#p_name').val();
        if($j(this).val() == "") $j(this).val(image_name);
    })
        
    $j('#p_name').keyup(function(){
        $j('#imageName').val($j(this).val());
    })
    
    //image align
    $j('#box_image_align a').click(function(){
        $j('#box_image_align a img').css("border", "1px solid #FFF")
        $j(this).find("img").css("border", "1px solid #F00")
        $j('#image_align').val($j(this).attr("rel"));
        $j('#image_align_label').text($j(this).attr("title"));
    })
    
    //add price row
    $j("#add_page_price").click(function(){
        $j("#page_prices tbody tr:last").clone().appendTo("#page_prices tbody");
    })          
                
});
//remove price row
function remove_price(id) {
    $j(id).parent("td").parent("tr").find("#price_value").val(0);
    $j(id).parent("td").parent("tr").hide();
}

function set_parent_page() {
    //$no - n of the edited page
    //n - selected value from the menu
    var n = $j('#site_list_pages').val();
    
    $j('#ParentPage').html('<input value="Loading menu ..." disabled>');
    $j.get('web/admin/includes/site_list_pages.php?n='+n+'&SiteID=<?=$SiteID?>&no=<?=$row_no->n?>', function(data) {
        $j('#ParentPage').html(data);               
    })      
}

function set_image() {
    
    var im=document.getElementById("preview_image");
    var iml=document.getElementById("preview_image_link");
    var x=document.getElementById("imageNo").selectedIndex;
    var y=document.getElementById("imageNo").options;
    
    im.src = y[x].getAttribute('rel');
    //alert(iml.getAttribute("href"));
    //iml.href = y[x].getAttribute('rel');              
}
</script>

<?php
//get user level from GET vars for preview
if(isset($_GET['u_level']) && $_GET['u_level'] <= $user->AccessLevel && $user->AccessLevel > 0)
    $user->AccessLevel = $_GET['u_level'];

if ($user->WriteLevel>= $row_no->SecLevel && $user->WriteLevel > 0) {
    //set default page status
    if(!isset($row_no->status))
        $row_no->status = 1; 

    //page date modified / date added
    if(!isset($no) || $row_no->date_added == "0000-00-00 00:00:00")
        $date_added = date_time("Y-m-d H:i:s");
    else
        $date_added = $row_no->date_added;

    if(!isset($no) || $row_no->date_modified == "0000-00-00 00:00:00") 
        $date_modified = date_time("Y-m-d H:i:s");
    else 
        $date_modified = $row_no->date_modified;        

    //page is new
    if(!isset($no)) {
        if(!isset($ParentPage)) $ParentPage = $o_site->_site['StartPage'];
        $p_parent = $o_page->get_page($ParentPage);
        $image_width = 600;
        $row_no->image_src = "spacer.gif";
    } else {
        $p_parent = $o_page->get_page($row_no->ParentPage);
        if($row_no->image_src == "") {
            if(defined(CMS_MAIN_WIDTH))
                $image_width = CMS_MAIN_WIDTH;
            $image_width = "600";
            $row_no->image_src = "spacer.gif";
        } else {
            $image_size = GetImageSize($row_no->image_src);
            $image_width = $image_size[0];
            $image_height = $image_size[1];
        }
    }   

    $langbrack = '<small>(EN)</small>';

    $main_language = $o_site->get_sLanguage(null, true);
    $site_versions = $o_site->get_sVersions();

?>

<div id="cms_admin">
<div id="exTab2" class="container"> 
    <ul class="nav nav-tabs">
        <li class="active"> <a href="#<?=$main_language['language_id'];?>" data-toggle="tab"><?=$main_language['language_text'];?></a> </li>
        <?php foreach($site_versions as $version) { ?>
            <?php if ($version['language_id'] == $main_language['language_id']) { continue; }; ?>
            <li><a href="#<?=$version['language_id'];?>" data-toggle="tab"><?=$version['language_text'];?></a> </li>
        <?php } ?>
    </ul>
    <div class="tab-content ">
      <div class="tab-pane active" id="1">
        <form action="page.php?n=112&SiteID=<?=$SiteID?>" method="post" enctype="multipart/form-data" name="addPage" id="addPage">
            <?php
                if($row_no->SiteID != $SiteID && isset($no)) { 
                    die ("<div class=\"message_error\">������ ����� �� ����������� ���� ��������. �� �� ���������� �� ����� ����.</div>");
                }

                if($user->AccessLevel >=4 && $no>0) {      
                    include __DIR__ . '/page/page_admin_panel.php';
                } 

                include __DIR__ . '/page/page_texts_panel.php';
            ?>
            <br clear="all">
            <fieldset>
                <legend id="show_page_image">- ����������� -</legend>
                <center> 
                    <input type="button" class="button" value="+ ������ �����������" id="upload_image">
                </center>
                <table border="0" cellpadding="10" cellspacing="1" class="content" id="page_image" style="display:<?=($row_no->imageNo == 0)? "none":""?>;">
                    <tr>
                        <td width="200px" class="border" valign="top" rowspan="2">
                            <div class="page_image">
                                <a href="page.php?n=2017&SiteID=<?=$o_site->SiteID?>&imageID=<?=$row_no->imageNo?>&im_dir=<?=$row_no->image_dir?>" class="edit" title="�������� �� �������������"></a>
                                <a href="<?=$row_no->image_src?>" id="preview_image_link" title="������: <?=$row_no->imageName?>">
                                    <img src="<?=$row_no->image_src?>" id="preview_image" border="0" <?=$img_preview_size?>>
                                </a>
                            </div>
                            <a href="web/admin/includes/images_manager.php?n=<?=$no?>&SiteID=<?=$SiteID?>" class="change" title="������� �� ������������� �� �������� <?=$row_no->Name?>" id="change_image_link">�����</a>  
                            <a href="javascript:;" class="change" id="reset_page_image">��������</a>
                            <a href="javascript:;" class="change" id="undo_reset_page_image" style="display: none;">����������</a>
                            <input type="hidden" name="old_image_src" id="old_image_src" value="<?=$row_no->image_src?>">
                            <input type="hidden" name="old_imageNo" id="old_imageNo" value="<?=$row_no->imageNo?>">
                        </td>
                        <td valign="top" style="background: #f9f9f9">
                            <!-- for old drop down menu ask M.Petrov //-->
                            <input type="hidden" name="imageNo" value="<?=$row_no->imageNo?>" id="imageNo">
                            <span class="label">���� ������:</span><div class="field"><input name="userfile" type="file" id="userfile"></div>
                            <span class="label">�������� �� ��������:</span>
                                <div class="field">
                                    <input name="imageName" type="text" id="imageName" value="<?php echo(htmlspecialchars($row_no->Name));  ?>">
                                </div>
                            <span class="label">������ � ����������:</span>
                                <div class="field">
                                    <select name="img_dir">
                                        <?php
                                        $s_dirs = $o_site->get_sImageDirectories();
                                        for($i=0; $i<count($s_dirs); $i++)
                                            echo "<option value=\"".$s_dirs[$i]["ID"]."\" ".(($s_dirs[$i]["ID"] == $o_site->_site['imagesdir']) ? "selected":"").">".$s_dirs[$i]["Name"]."</option>";
                                        ?>
                                    </select>
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <div style="width: 49%; float: left;">
                                <span class="label">������������ �� ��������: <b id="image_align_label"></b></span> 
                                <div class="field">
                                    <select name="image_allign" >
                                        <option value="1" selected>--- �� ������������ --- </option>
                                        <?php 
                                        $alligns_cat = mysql_query("SELECT DISTINCT valign FROM alligns");
                                        while($cat = mysql_fetch_object($alligns_cat))
                                            {       
                                                $label = "";
                                                if($cat->valign == "top") $label = "��� ������";
                                                if($cat->valign == "bottom") $label = "��� ������";
                                                if($cat->valign == "") $label = "���������";
                                                
                                                echo "<optgroup label=\"".$label."\">"; 
                                            
                                                $alligns = mysql_query("SELECT * FROM alligns WHERE status = 1 AND valign= '$cat->valign' ORDER by allignID ASC");
                                                
                                                    while ($allign_list = mysql_fetch_object($alligns))
                                                    {
                                                        if ($allign_list->allignID == $row_no->image_allign)
                                                            $selected = "selected";
                                                        else $selected="";
                                                        
                                                        echo "<option value=$allign_list->allignID $selected > $allign_list->AllignText</option>";
                                                    }
                                                echo "</optgroup>";
                                            }
                                        ?>
                                    </select>
                              </div>
                          </div>
                          <div style="width: 49%; float: left; margin: 0 0 0 2px;">
                              <div id="box_image_align" class="box_image_align">
                                <a href="javascript:;" rel="0" title="�� ������������"><img src="web/admin/images/icons/img_align_default.jpg"></a>
                                <a href="javascript:;" rel="1" title="� ���� ��� ������"><img src="web/admin/images/icons/img_align_left.jpg"></a>
                                <a href="javascript:;" rel="2" title="� ����� ��� ������"><img src="web/admin/images/icons/img_align_right.jpg"></a>
                                <a href="javascript:;" rel="3" title="� ������� ��� ������"><img src="web/admin/images/icons/img_align_center.jpg"></a>
                                <a href="javascript:;" rel="4" title="� ���� ��� ������"><img src="web/admin/images/icons/img_align_left2.jpg"></a>
                                <a href="javascript:;" rel="5" title="� ����� ��� ������"><img src="web/admin/images/icons/img_align_right2.jpg"></a>
                                <a href="javascript:;" rel="6" title="� ������� ��� ������"><img src="web/admin/images/icons/img_align_center2.jpg"></a>
                                <a href="javascript:;" rel="21" title="�������� �����"><img src="web/admin/images/icons/img_align_banner.jpg"></a>
                            </div>
                            <span class="label">������: (�� <b><?=$image_width?></b>px.)</span>
                            <div class="field">
                                <input name="imageWidth" type="text" id="imageWidth" value="<?php if ($no>0) {echo("$row_no->imageWidth"); } else {echo("200");}?>">
                            </div>
                            <p> </p>
                        </div>
                    </tr>
                </table>
                <?php 
                $_img_link = $o_page->get_pImage($_GET["no"]);
                if($_img_link){
                    echo '<div class="field"><input id="cliplink" value="';
                    echo "http://".$o_page->_site["primary_url"]."/".$_img_link;
                    echo '"></div>';
                }
                ?>
                <br>
            </fieldset>
            <br>

            <?php if($o_page->_user['AccessLevel'] > 3 or $o_page->_user['ID'] == 434){ ?>
                <fieldset><legend>-������ �� ������� �� csv ����-</legend>
                    <input type="file" name="csv-table"> 
                </fieldset>
            <?php } ?>
            <?php if($user->AccessLevel >= 5 && $no>0) { ?>
                <fieldset class="level5"><legend id="show_php_content">- PHP content-</legend>
                <table border="0" cellpadding="10" cellspacing="0" class="content" id="php_content" style="display:<?=(empty($row_no->PHPvars) && empty($row_no->PHPcode))?"none":""?>;">
                    <tr id="page_php_vars" style="display:<?=(empty($row_no->PHPvars))?"none":""?>;">
                        <td class="border"><span class="label">PHP variables</span><textarea name="PHPvars" rows="10" class="code"><?=$row_no->PHPvars?></textarea><br><a href="javascript:;" id="show_php_code">+ PHPcode</a>
                    <tr id="page_php_code" style="display:<?=(empty($row_no->PHPcode))?"none":""?>;">
                        <td><span class="label">PHP code</span><textarea name="PHPcode" class="code" rows="10"><?=$row_no->PHPcode?></textarea><br><a href="javascript:;" id="show_php_vars">+ PHPvars</a>
                </table>
                </fieldset>
            <?php } ?>

            <fieldset><legend>- ��������� �� ������������ -</legend>
            <table border="0" cellpadding="10" cellspacing="0" class="content">
              <tr>
                <td width="50%" class="border">
                <div class="field">
                  <select name="show_link" id="select2">
                    <option value="1" <?php if ($row_no->show_link == 1) {echo("selected");} ?> >������
                    ������ ��� �������������</option>
                    <optgroup label="������ ������������ �� �������������">
                    <option value="2" <?php if ($row_no->show_link == 2) {echo("selected");} ?> >������
                    ����������, ������ � �������� �� �������������</option>
                    <option value="5" <?php if ($row_no->show_link == 5) {echo("selected");} ?> >������
                    ������ � �������� �� �������������</option>
                    <option value="6" <?php if ($row_no->show_link == 6) {echo("selected");} ?> >������
                    ���� ������������ �� �������������</option>
                    <option value="10" <?php if ($row_no->show_link == 10) {echo("selected");} ?> >������
                    ���������� � ������������ �� �������������</option>
                    </optgroup>
                    <optgroup label="���������">
                    <option value="3" <?php if ($row_no->show_link == 3) {echo("selected");} ?> >������
                    ���������� � �������� �� �������������</option>
                    <option value="9" <?php if ($row_no->show_link == 9) {echo("selected");} ?> >������
                    ���� �������� �� �������������</option>
                    </optgroup>
                    <optgroup label="����� ��� �������������">
                    <option value="4" <?php if ($row_no->show_link == 4) {echo("selected");} ?> >������
                    ��������, ���������� � ������� ����� �� �������������</option>
                    <option value="7" <?php if ($row_no->show_link == 7) {echo("selected");} ?> >������
                    �������� � ������� ����� �� �������������</option>
                    <option value="8" <?php if ($row_no->show_link == 8) {echo("selected");} ?> >������
                    ������� ����� �� �������������</option>
                    <option value="11" <?php if ($row_no->show_link == 11) {echo("selected");} ?> >������
                    ���������� � ������ ����� �� �������������</option>
                    </optgroup>
                    <optgroup label="������������ ������">
                    <option value="13" <?php if ($row_no->show_link == 13) {echo("selected");} ?> >������
                    ��������, ���������� � �������������� ������</option>
                    <option value="14" <?php if ($row_no->show_link == 14) {echo("selected");} ?> >������
                    �������� � �������������� ������</option>
                    <option value="15" <?php if ($row_no->show_link == 15) {echo("selected");} ?> >������
                    ���������� � �������������� ������</option>
                    </optgroup>
                    <optgroup label="����� ������">
                    <option value="0" <?php if ($row_no->show_link == 0) {echo("selected");} ?> > ��
                    ������������ ������ ��� �������������</option>
                    </optgroup>
                  </select>
                  </div>
                  <div class="field">
                  <select name="show_link_cols">
                    <option value="1" <?php if ($row_no->show_link_cols == 1) {echo("selected");} ?> >�
                    1 ������</option>
                    <option value="2" <?php if ($row_no->show_link_cols == 2) {echo("selected");} ?> >�
                    2 ������</option>
                    <option value="3" <?php if ($row_no->show_link_cols == 3) {echo("selected");} ?> >�
                    3 ������</option>
                    <option value="4" <?php if ($row_no->show_link_cols == 4) {echo("selected");} ?> >�
                    4 ������</option>
                    <option value="5" <?php if ($row_no->show_link_cols == 5) {echo("selected");} ?> >�
                    5 ������</option>
                    <option value="6" <?php if ($row_no->show_link_cols == 6) {echo("selected");} ?> >�
                    6 ������</option>
                    <option value="7" <?php if ($row_no->show_link_cols == 7) {echo("selected");} ?> >�
                    7 ������</option>
                    <option value="8" <?php if ($row_no->show_link_cols == 8) {echo("selected");} ?> >�
                    8 ������</option>
                  </select>
                  </div>
                </td>
                <td width="50%">
                <div class="field">
                <select name="show_link_order" id="select6">
                  <optgroup label="�� ������������">
                  <option <?php if ($row_no->show_link_order == "ASC") echo "selected"; ?> value="ASC">�������� ��� 0-9</option>
                  <option <?php if ($row_no->show_link_order == "DESC") echo "selected"; ?> value="DESC">�������� ��� 9-0</option>
                  </optgroup>
                  <optgroup label="�� ��� �� ����������">
                  <option <?php if ($row_no->show_link_order == "p.Name ASC") echo "selected"; ?> value="p.Name ASC">���
                  �������� ��� A-Z</option>
                  <option <?php if ($row_no->show_link_order == "p.Name DESC") echo "selected"; ?> value="p.Name DESC">�
                  �������� ��� Z-A</option>
                  </optgroup>
                  <optgroup label="�� ����">
                  <option <?php if ($row_no->show_link_order == "p.date_added ASC") echo "selected"; ?> value="p.date_added ASC">���
                  �������� ��� 0-9</option>
                  <option <?php if ($row_no->show_link_order == "p.date_added DESC") echo "selected"; ?> value="p.date_added DESC">�
                  �������� ��� 9-0</option>
                  </optgroup>
                  <optgroup label="�� ������">
                  <option <?php if ($row_no->show_link_order == "p.toplink ASC") echo "selected"; ?> value="p.toplink ASC">��������
                  ���</option>
                  <option <?php if ($row_no->show_link_order == "p.toplink DESC") echo "selected"; ?> value="p.toplink DESC">��������
                  ���</option>
                  </optgroup>
                  <optgroup label="�����">
                  <option <?php if ($row_no->show_link_order == "RAND()") echo "selected"; ?> value="RAND()">����������</option>
                  </optgroup>
                </select>
                 </div>
                 <div class="field">
                  <select name="make_links" id="select7">
                    <option value="1" <?php if (($row_no->make_links == 1) || ($n == 111)) {echo("selected");} ?> >�
                    ��������� ������</option>
                    <option value="2" <?php if ($row_no->make_links == 2) {echo("selected");} ?> >������
                    ���� �� ����������</option>
                    <option value="3" <?php if ($row_no->make_links == 3) {echo("selected");} ?> >������
                    ���� �� �������� (popup)</option>
                    <option value="4" <?php if ($row_no->make_links == 4) {echo("selected");} ?> >������
                    �� ������ ������ + ���������� (�������)</option>
                    <option value="0" <?php if (($row_no->make_links == 0) && ($n != 111) ) {echo("selected");} ?>>���
                    ������</option>
                  </select>
                  </div>
                  </td>
              </tr>
            <?php if($user->AccessLevel >=3 && $no>0) { ?>
                <tr>
                    <td width="50%" class="border"><span class="label">������ ������������ �� �����:</span><div class="field"><?=list_page_groups("show_group_id", $row_no->show_group_id, "style=\"width: 100%;\"")?></div>
                    <td width="50%"><br><a href="page.php?n=63931&SiteID=<?=$SiteID?>" target="_blank" class="add"> ������ �����</a>
                </tr>
            <?php } ?>
              
            </table>
            </fieldset>
               
            <?php
                //page templates
                $pTemplates = $o_page->get_pTemplates();
                $pTemplate = $o_page->get_pTemplate($no);
                if($user->AccessLevel >=5) {
                    ?>
                    <fieldset class="level5"><legend id="show_page_templates">- �������� �� ���������� -</legend>
                    <table border="0" cellpadding="10" cellspacing="0" class="content" id="page_templates">
                        <tr><td>
                            <div class="field">
                            <select name="page_template">                   
                                <?=(count($pTemplate['pt_id'])>0)? "<option value=\"-1\">- ��� �������� -</option>" : "<option value=\"0\">- �������� �������� -</option>";?>
                                <?php

                                    for($i=0; $i<count($pTemplates); $i++)
                                        {
                                            $selected = "";
                                            if($pTemplates[$i]['pt_id'] == $pTemplate['pt_id']) $selected = "selected";
                                            echo "<option ".$selected." value=\"".$pTemplates[$i]['pt_id']."\">".$pTemplates[$i]['pt_title']."</option>";
                                        }
                                ?>
                            </select>
                            </div>
                    </table>        
                    </fieldset>
                    <?
                } else {
                    echo "<input type=\"hidden\" name=\"page_template\" value=\"".$pTemplate['pt_id']."\">";
                }
                
            if($user->AccessLevel >=2 && $no>0) {
                ?>      
                <fieldset class="level3"><legend id="show_page_additions">- ���������� -</legend>
                    <table border="0" cellpadding="10" cellspacing="0" class="content" id="page_additions" style="display:<?=(empty($row_no->PageURL))?"none":""?>;">
                        <tr>
                            <td>
                                <div class="field">
                                    <?php 
                                    echo("<select name=\"PageURL\" id=\"PageURL\">");
                                    if (!(empty($row_no->PageURL)) )
                                        {
                                            //echo("<option value=\"$row_no->PageURL\" selected>- $row_no->PageURL -</option>");
                                            echo(" <option value=''>- ���������� ������������ -</option> ");
                                        }
                                    else
                                        echo(" <option value=''>- �������� -</option> ");
                                    
                                    $personal_urls = "";
                                    $public_urls = "";
                                    $urls = mysql_query("SELECT * FROM PageURLs WHERE (SiteID = '0' OR SiteID = '$SiteID') ORDER BY Name ASC ");
                                    while ($url = mysql_fetch_object($urls))
                                       {
                                            if ($url->filename == $row_no->PageURL) $url_selected = "selected";
                                                else $url_selected="";
                                            if($url->SiteID == $SiteID)
                                                $personal_urls.= "<option value=$url->filename $url_selected> $url->Name </option>";
                                            else
                                            {
                                                $public_urls.="<option value=$url->filename $url_selected> $url->Name </option>";
                                            }
                                        }
                                    echo "<optgroup label=\"���������� ����������\">".$personal_urls . "<optgroup label=\"�������� ����������\">".$public_urls; 
                                    echo "</select>";
                                    //show only when 
                                    echo("<input name=\"PageURL\" id=\"PageURL2\" style=\"display: none;\" type=\"text\" value=\"$row_no->PageURL\" disabled>");
                                    ?>
                                </div>
                                <?php if($user->AccessLevel >=5) { echo "<a href=\"javascript: void(0)\" id=\"switch_page_url\">+ �����</a><br>"; } ?>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <?php
            }
            if($user->AccessLevel >=3 && $no>0) { ?>      
                <fieldset class="level3"><legend id="show_page_special_options">- ��������� ����� -</legend>
                    <table border="0" cellpadding="10" cellspacing="0" class="content" style="display:none;" id="page_special_options">
                        <tr>
                            <td class="border">
                                <span class="label">��� �� ����������</span>
                                <div class="field">
                                    <select name="toplink">
                                        <option value="0" <?php if(($row_no->toplink)==0) echo "selected"; ?>>-
                                        <option value="1" <?php if(($row_no->toplink)==1) echo "selected"; ?>>��� ������
                                        <option value="2" <?php if(($row_no->toplink)==2) echo "selected"; ?>>������� ����
                                        <option value="3" <?php if(($row_no->toplink)==3) echo "selected"; ?>>���. ������
                                        <option value="4" <?php if(($row_no->toplink)==4) echo "selected"; ?>>vip ������
                                        <option value="5" <?php if(($row_no->toplink)==5) echo "selected"; ?>>����� ������
                                    </select>
                                </div>
                        <?php if($user->AccessLevel >=5) { ?>
                                <td class="border">
                                    <span class="label">������ �� ����������:</span>
                                    <div class="field">
                                        <select name="status">
                                            <option value="1" <?=($row_no->status == 1)? "selected":""?>>������
                                            <option value="0" <?=($row_no->status == 0)? "selected":""?>>���������
                                            <option value="-1" <?=($row_no->status == -1)? "selected":""?>>����������
                                        </select>
                                    </div>
                                </td>
                        <?php } ?>
                            </tr>
                            <?php if($user->AccessLevel >=3) { ?>
                            <tr>
                                <td class="border">
                                    <span class="label">������ ���������� � �����:</span>
                                    <div class="field"><?=list_page_groups("ptg_group_id", 0, "style=\"width: 100%;\"")?></div>
                                </td>
                                <td class="border">
                                    <span class="label">��������� ��� �������:</span><span class="label" style="padding: 0 0 0 70px">���� �� ��������:</span>
                                    <div class="field">
                                        <input type="text" name="ptg_label" style="width: 49%; float: left;">
                                        <input type="text" name="ptg_end_date" id="datepicker" value="0000-00-00" style="width: 49%; float: left; border-left: 1px solid #999">
                                    </div>
                            </tr>
                            <?php } ?>
                        </table>
                    </fieldset>
            <?php
                }
                //page prices module
                $pPrices = $o_page->get_pPrice($no, "ASC");
                //if(isset($no) && (count($pPrices) > 0 || $user->AccessLevel >5))
                if(isset($no) && (count($pPrices) > 0 || ($user->AccessLevel >= 2 && $user->ReadLevel >=2))) { ?>
                    <fieldset class="level2"><legend id="show_page_prices">- ���� -</legend>
                        <table border="0" cellpadding="5" cellspacing="0" class="content" id="page_prices">
                            <thead>
                                <tr>
                                    <th>��������</th>
                                    <th>���</th>
                                    <th>��. ������</th>
                                    <th>����������</th>
                                    <th>����</th>
                                    <th>������</th>
                                    <th width="25"></th>
                                </tr>
                            </thead>
                            <tbody>    
                            <?php
                                $count_prices = count($pPrices);
                                if($count_prices == 0 && $user->AccessLevel >=2)
                                    $count_prices =1;

                                for($i=0; $i<$count_prices; $i++) {
                                    echo "<tr>";
                                    echo "<td><input name=\"price_description[]\" type=\"text\" id=\"price_description\" size=\"20\" value=\"".$pPrices[$i]['description']."\">";
                                    echo "<td><input name=\"price_code[]\" type=\"text\" id=\"price_code\" size=\"5\" value=\"".$pPrices[$i]['code']."\">";
                                    echo "<td><input name=\"price_pcs[]\" type=\"text\" id=\"price_pcs\" size=\"5\" value=\"".$pPrices[$i]['pcs']."\">";
                                    echo "<td><input name=\"price_qty[]\" type=\"text\" id=\"price_qty\" size=\"5\" value=\"".$pPrices[$i]['qty']."\">";
                                    echo "<td><input name=\"price_value[]\" type=\"text\" id=\"price_value\" size=\"10\" maxlength=\"10\" value=\"".$pPrices[$i]['price_value']."\">"; 
                                    echo "<td>";
                                    list_price_currencies("price_currency[]", $pPrices[$i]['currency_id']);
                                    echo "<td><a href=\"javascript: void(0)\" class=\"delete\" onClick=\"remove_price(this)\"></a><input name=\"price_id[]\" type=\"hidden\" id=\"price_id\" size=\"10\" maxlength=\"10\" value=\"".$pPrices[$i]['id']."\">";
                                    echo "</tr>";
                                } ?>
                            </tbody>
                            <tfoot>
                                <tr><td colspan="7"> <a href="javascript: void(0)" id="add_page_price" class="add">������ ����</a>
                            </tfoot>
                        </table>
                    </fieldset>
                    <?  } ?>

                    <fieldset>
                        <legend>- ��������� �� ���������� -</legend>
                        <table border="0" cellpadding="10" cellspacing="0" class="content">
                            <tr>
                                <td colspan="2"> </td>
                            </tr>
                            <tr>
                                <td width="50%" class="border">
                                    <div class="field">
                                        <select name="SecLevel" id="SecLevel">
                                        <?php 
                                        $selected="";
                                        for ($i=0; $i<=$user->WriteLevel; $i++) {
                                            if ($row_no->SecLevel == $i) {
                                                $selected="selected";
                                            }

                                            if ($i == 0) {
                                                $textLevel = "������������";
                                            } else {
                                                $textLevel = "���� �� ������"; 
                                                $textLevel = "$textLevel: $i"; 
                                            }
                                            echo("<option value=\"$i\" $selected>$textLevel</option>");
                                            $selected="";
                                        }
                                        ?>
                                        </select>
                                    </div>
                                </td>
                                <td width="50%">
                                    <span>���� �� ��������:<br><?=$date_added?></span>
                                    <br>
                                    <input type="hidden" name="date_added" value="<?=$date_added?>"> �����: <?=$o_page->extract_uInfo($o_page->get_pInfo('author', $no), 'Name')?>
                                    <?php
                                    if($user->AccessLevel >=3) {
                                        $page_edit_log_q = $o_page->db_query("SELECT DISTINCT(uID) FROM page_edit_log WHERE n='$no' "); 
                                        if(mysql_num_rows($page_edit_log_q)>0) echo("<br>���������� � ������������� ��: "); 
                                        while($row_log = mysql_fetch_object($page_edit_log_q)) {
                                            echo($o_page->extract_uInfo($row_log->uID, 'Name'))." ";
                                        }
                                        echo("<br>"); 
                                    }
                                    ?>
                                    <br><br>���� �� �������� ��������<br><?=$date_modified?>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
            <div id="message" style="display: none;"></div>
            <table border="0" cellpadding="10" cellspacing="0" class="content">
                <tr>
                <?php
                    if($no>0)
                        echo "<td width=\"100px\" align=\"left\"><a href=\"page.php?n=143&SiteID=$SiteID&del_page_no=$no\" class=\"delete\">������</a>";
                ?>
                <td align="center"><input name="submit" type="submit" id="submit" value="������ ����������" class="submit">
            </table>
            <input type="hidden" name="no" value="<?=$no?>">
            <input type="hidden" name="SiteID" id="SiteID" value="<?=$SiteID?>">
            </form>
          </div>
            <?php 
                while($lang = array_shift($site_versions)) { ?>
            <?php if ($version['language_id'] == $main_language['language_id']) { continue; }; ?>
            <div class="tab-pane" id="<?=$lang['language_id'];?>">
            <?php 
                $form->set_id('lang'.$lang['language_id']);;
                $form->set_action('/forms/global/save.php');
                echo $form->start();
                echo $form->no;
                $row_no = $o_page->get_page($no, 'object', $lang['language_id']);
                include __DIR__.'/page/page_texts_panel.php';
                $form->lang_id->value = $lang['language_id'];
                echo $form->action;
            ?>
            <input name='lang_id' type='hidden' value='<?=$lang['language_id'];?>'>
            <div id="message" style="display: none;"></div>
            <table border="0" cellpadding="10" cellspacing="0" class="content">
                <tr>
                <?php
                    if($no>0)
                        echo "<td width=\"100px\" align=\"left\"><a href=\"page.php?n=143&SiteID=$SiteID&del_page_no=$no\" class=\"delete\">������</a>";
                ?>
                <td align="center">
                    <input name="submit" type="submit" id="submit" value="������ ����������" class="submit">
                </td>
            </table>
            <input type="hidden" name="no" value="<?=$no?>">
            <input type="hidden" name="SiteID" id="SiteID" value="<?=$SiteID?>">
            <?=$form->end();?>
          </div>
            <?php } ?>
<?php
} else {
    mk_output_message("warning", "������ ����� �� <b>�����</b> �� ���� �������� !");
}
?>
</div>


<?php include __DIR__.'/texteditor.php';?>

<?php
if( ($no>0) && ($user->AccessLevel >=2 || $site->_site['netservice']>0) ) {
    include("web/stats/site_page_analitycs.php"); 
}
?>
<?php include __DIR__.'/texteditor.php';?>

