<?php
    if(!$user->Accesslevel > 1){
	header('Location: '.$_SERVER['SERVER_NAME']);
    }
?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<style>
.bs-callout {
    padding: 20px;
    margin: 20px 0;
    border: 1px solid #eee;
    border-left-width: 5px;
    border-radius: 3px;
}
.bs-callout h4 {
    margin-top: 0;
    margin-bottom: 5px;
}
.bs-callout p:last-child {
    margin-bottom: 0;
}
.bs-callout code {
    border-radius: 3px;
}
.bs-callout+.bs-callout {
    margin-top: -5px;
}
.bs-callout-default {
    border-left-color: #777;
}
.bs-callout-default h4 {
    color: #777;
}
.bs-callout-primary {
    border-left-color: #428bca;
}
.bs-callout-primary h4 {
    color: #428bca;
}
.bs-callout-success {
    border-left-color: #5cb85c;
}
.bs-callout-success h4 {
    color: #5cb85c;
}
.bs-callout-danger {
    border-left-color: #d9534f;
}
.bs-callout-danger h4 {
    color: #d9534f;
}
.bs-callout-warning {
    border-left-color: #f0ad4e;
}
.bs-callout-warning h4 {
    color: #f0ad4e;
}
.bs-callout-info {
    border-left-color: #5bc0de;
}
.bs-callout-info h4 {
    color: #5bc0de;
}
</style>
<?php
require 'lib/Database.class.php';
$db = new Database();
function get_type(Database $db, $type_id)
{
    $select = "SELECT * FROM configuration_types WHERE conf_type_id = :conf_type_id";
    $stmt = $db->prepare($select);
    $stmt->bindValue(':conf_type_id', $type_id);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_OBJ);
}

function conf_group_options(Database $db)
{
    $select_query = "SELECT * FROM configuration_group";
    $stmt = $db->prepare($select_query);
    $stmt->execute();
    $options = array();
    while($row = $stmt->fetch(PDO::FETCH_OBJ)){
        $options[$row->conf_group_id] = $row->conf_group_title;
    }
    return $options;
}

function conf_type_options($db, $conf_group_id)
{
    $select_query = "SELECT * FROM configuration_types WHERE conf_group_id=:conf_id ORDER BY conf_type_id";
    $stmt = $db->prepare($select_query);
    $stmt->bindValue(":conf_id", $conf_group_id);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function check(Database $db, Page $o_page, $group_id, $type_id)
{
    $select = "SELECT * FROM configuration as conf
               JOIN configuration_types conf_types ON conf.conf_key=conf_types.conf_type_key
               WHERE conf.SiteID = :SiteID
               AND conf.conf_group_id=:group_id
               AND conf_types.conf_type_id=:conf_type_id
               AND conf.templ_id in (0,:templ_id)";
    $stmt = $db->prepare($select);
    $stmt->bindValue(":group_id", $group_id);
    $stmt->bindValue(":conf_type_id", $type_id);
    $stmt->bindValue(":templ_id", $o_page->_site["Template"]);
    $stmt->bindValue(":SiteID", $o_page->_site['SitesID']);
    $stmt->execute();
    $result = $stmt->fetchAll();
    if(count($result) > 0){
        return $result[0];
    }
    return False;
}

function save(Database $db, Page $o_page, $post_data)
{
    $id =check($db, $o_page, $post_data['group_id'], $post_data['type_id']);
    var_dump($id);
    if($id['conf_id']){
	update($db, $post_data['conf_value'], $id['conf_id']);
	return;
    }
    insert($db, $o_page, $post_data);
}

function update(Database $db, $value, $id)
{
    $update = "UPDATE configuration SET conf_value=:value, last_modified=:now WHERE conf_id=:id";
    $stmt = $db->prepare($update);
    $stmt->bindValue(":value", $value);
    $stmt->bindValue(":now", date('Y-m-d H:m:s'));
    $stmt->bindValue(":id", $id);
    $stmt->execute();
}

function insert(Database $db, Page $o_page, $post_data)
{
    $insert = "INSERT configuration (conf_title, conf_key, conf_value,
				     conf_group_id, sort_order, last_modified,
				     SiteID, templ_id, conf_description, date_added)
	       VALUES (:conf_title, :conf_key, :conf_value, :conf_group_id,
		       :sort_order, :last_modified, :SiteID, :templ_id, :conf_description, :date_added)";
    $type = get_type($db, $post_data['type_id']);
    $stmt = $db->prepare($insert);
    $stmt->bindValue(':conf_title', $type->conf_type_title);
    $stmt->bindValue(':conf_key', $type->conf_type_key);
    $stmt->bindValue(':conf_value', $post_data['conf_value']);
    $stmt->bindValue(':conf_group_id', $type->conf_group_id);
    $stmt->bindValue(':sort_order', 0);
    $stmt->bindValue(':last_modified', date("Y-m-d H:m:s"));
    $stmt->bindValue(':SiteID', $o_page->get_pSiteID());
    $stmt->bindValue(':templ_id', $o_page->get_sTemplate());
    $stmt->bindValue(':conf_description', $type->conf_type_description);
    $stmt->bindValue(':date_added', date("Y-m-d H:m:s"));
    $stmt->execute();
}
echo $o_page->get_pSiteID().'<br>'.$o_page->get_sTemplate();
echo '<br>'.$_SERVER['REQUEST_URI'].'<br>';
?>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
    <?php
        foreach(conf_group_options($db) as $group_id=>$group_name){
    ?>
      <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne<?php echo $group_id;?>">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $group_id;?>" aria-expanded="true" aria-controls="collapseOne<?php echo $group_id;?>">
                      <?php echo $group_name;?>
                </a>
              </h4>
            </div>
        <div id="collapseOne<?php echo $group_id;?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne<?php echo $group_id;?>">
            <div class="panel-body">
                <?php
                foreach(conf_type_options($db, $group_id) as $t=>$type){
		$value = check($db, $o_page, $group_id, $type['conf_type_id']);
                ?>
              <div class="bs-callout bs-callout-info">
                  <h4 id="callout-progress-csp"><?php echo $type['conf_type_title'];?></h4>
                  <p><?php echo $type['conf_type_description'];?></p>
              </div>
              <form class="form-horizontal" method="POST" action="<?php echo $_SERVER['REQUEST_URI'];?>">
              <div class="form-group">
                  <label for="type_id<?php echo $type['conf_type_id'];?>" class="control-label col-xs-2"><?php echo $type['conf_type_title'];?></label>
                  <div class="col-xs-10">
                      <input type="text" class="form-control" name="conf_value" id="type_id<?php echo $type['conf_type_id'];?>" placeholder="<?php echo $type['conf_type_key'];?>" <?php if($value){ echo "value='".$value['conf_value']."'";}?>>
                  </div>
              </div>
              <div class="form-group">
                  <div class="col-xs-offset-2 col-xs-10">
                      <button name="submit" value="dadsdja" type="submit" class="btn btn-primary">Запази</button>
                  </div>
              </div>
		<input type='hidden' name="group_id" value="<?php echo $group_id;?>">
		<input type='hidden' name="type_id"  value="<?php echo $type['conf_type_id'];?>">
              </form>
              </div>
            <?php } ?>
        </div>
        </div>
      </div>
    <?php
    }

if(isset($_POST['conf_value'])){
    try{
	$cleaned_data = post_validator($_POST);
	save($db, $o_page, $cleaned_data);
    } catch ( Exception $e ) {
	header('Location: '.$_SERVER['REQUEST_URI']);
    }
}


function post_validator($post_data){
    $cleaned_data = array();
    foreach($post_data as $key=>$value){
	try{
	    $cleaned_data[$key] = field_validator($value);
	} catch ( Exception $e ) {
	    throw new Exception("Invalid post data field ".$key." has invalid value!");
	}
    }
    return $cleaned_data;
}


function field_validator($field)
{
    if($field=='' or is_null($field)){
	throw new Exception("Cant be empty!");
    }
    return $field;
}
