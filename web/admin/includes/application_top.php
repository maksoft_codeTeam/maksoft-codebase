<?php
	session_start();
	
	//includes everything needed to load all maksoft libraries for use in admin mode
	//path: web/admin/includes/
	
	//include page object
	include "../../../lib/lib_page.php";
	
	//include php functions
	include "../../../lib/lib_functions.php";
	
	//include js functions
	
	//include login functions
	
	//include db functions
	
	//path to the rooth directory from web/admin/includes/
	define("ROOTH_PATH","../../");
?>
<link rel="stylesheet" type="text/css" href="/css/base_style.css" media="screen">
<link rel="stylesheet" type="text/css" href="/css/admin_classes.css" media="screen">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script type="text/javascript">
	//no conflict with Prototype Lib
	var $j = jQuery.noConflict();
</script>
