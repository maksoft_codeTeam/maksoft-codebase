<?php
require_once __DIR__.'/../../modules/vendor/autoload.php';
include_once __DIR__.'/../../forms/topexpress/cart/Remove.php';


setlocale(LC_MONETARY, 'bg_BG');
bcscale(2);
$cart_items = isset($_SESSION['cart_items']) ? $_SESSION['cart_items'] : array();
function delivery($SiteID)
{
    $shipping = array(
        312.50 => 0,
    );
    $sites = array(2,383);
    if(!in_array($SiteID, $sites)){
        return money_format('%.2n', 0);
    }
    $total = $_SESSION["cart_total_price"];
    foreach($shipping as $rule=>$cost){
        if($total >= $rule){
            return money_format('%.2n', $cost);
        }
    }
    return money_format("%.2n", 15 / 1.2 );
}
$action = isset($_POST['action']) ? $_POST['action'] : '';
$_cart = new \Maksoft\Cart\Cart("BG");
if($_SERVER["REQUEST_METHOD"] === "POST" && $action){
    $_cart->load($_SESSION[\Maksoft\Cart\Cart::getName()]);
    try{
        switch($action){
            case "remove_product":
                $remove_item = new Remove($_cart, $_POST['sku'], $_POST);
                $remove_item->is_valid();
                $_cart = $remove_item->remove();
                break;
            case "edit":
                $editOrder = new \Maksoft\AFI\Forms\Edit($_cart, $_POST);
                $editOrder->is_valid();
                $editOrder->action();
                header("Location: ".$_SERVER['REQUEST_URI']);
                break;
        }
        $_SESSION[\Maksoft\Cart\Cart::getName()] = $_cart->save();
        ?>
        <script> document.location = "page.php?n=13202&SiteID=<?=$SiteID?>";</script>
        <?
    } catch (Exception $e){
    }
}

//action
$action = isset($_GET['action']) ? $_GET['action'] : '';

//the page you're comming from
$no = isset($_GET['no']) ? $_GET['no'] : '';

//price id
$price_no = isset($_GET['pid']) ? $_GET['pid'] : '';

//insert item
if($action == "insert")
    {
        if(!isset($_SESSION['cart_items']))
            {
                $_SESSION['cart_items'] = array();
                $_SESSION['cart_total_price'] ='0.00';
            }
            
        $_SESSION['cart_last_item']= $no;
        $_SESSION['cart_items'][$price_no] += ($_GET["moq"]<1 ? 1 : $_GET["moq"]);
        ?>
        <script> document.location = "page.php?n=13202&SiteID=<?=$SiteID?>";</script>
        <?
    }   

//remove item
if($action == "remove")
        {
            unset($_SESSION['cart_items'][$price_no]);
            mk_output_message("normal","�������� � ��������� !");
        }

//remove all items
if($action == "remove_all")
        {
            unset($_SESSION['cart_items']);
            $_SESSION['cart_total_price'] ='0.00';
            ?>
            <script>
                document.location = "page.php?n=13202&SiteID=<?=$SiteID?>";
            </script>
            <?
        }

//update shoping cart
if($action == "update")
    {
        //print_r($item_qty);
        $item_qty_keys = array_keys($item_qty);
        for($i=0; $i<count($item_qty); $i++)
            if($item_qty[$item_qty_keys[$i]] == 0)
                {
                    unset($_SESSION['cart_items'][$item_qty_keys[$i]]);
                    mk_output_message("normal","�������� � ��������� !");
                }
            else $_SESSION['cart_items'][$item_qty_keys[$i]] = $item_qty[$item_qty_keys[$i]];
    }

    //print_r($_SESSION['cart_items']);

//submit shopping content   
if($action == "sent")
    {
        unset($_SESSION['cart_items']);
        unset($_SESSION['cart_last_item']);
        $_SESSION['cart'] = array();
        mk_output_message("normal", "������ ������� � ��������� ������� ! <br><br><a href=\"".$o_page->get_pLink(167077)."\"><u>����� ������ ���� ������� � ���������� ��� !</u></a>");
    }   

if($action != "process" && $action != "sent" &&$action != "verify")
{   
    //view cart items
    if (isset($cart_items) and count($cart_items)>0 or (isset($_SESSION['cart']) and count($_SESSION['cart']) > 0)) 
        {
            $cart_items_keys = array_keys($cart_items);
            ?>
            <form name="order" method="POST" action="page.php?n=13202&SiteID=<?=$SiteID?>&action=update">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" id="shopping_cart" class="border_table">
            <thead><tr><td><td>�������<td>��������<td>���
              <td>���.
              <td>����<td>����</thead>
            <tbody>
            <?
            $i=0;
            $cart_content_text = "";
            $_SESSION['cart_total_price'] = 0;
            for($j=0; $j<count($cart_items_keys); $j++)
                {   

                    $item_sql = "SELECT * FROM prices, pages WHERE prices.price_n = pages.n AND prices.price_id = '".$cart_items_keys[$j]."' ";
                    $item_res = mysqli_query($item_sql);
                    $item = mysqli_fetch_object($item_res);
                    $price = $item->price_value/$item->price_pcs;
                    if(array_key_exists($item->price_id, $_SESSION["cart_items"])){
                        $item->price_pcs = $_SESSION["cart_items"][$item->price_id];
                    }
                    ?>
                    <tr>
                        <td><a href="page.php?n=13202&SiteID=<?=$SiteID?>&amp;pid=<?=$cart_items_keys[$j]?>&amp;action=remove"><img src="web/images/icons/omit-page-yellow.gif" border="0" alt="�������� �� ���������" align="middle"></a>
                        <td><a href="page.php?n=<?=$item->price_n?>&amp;SiteID=<?=$SiteID?>"><?=$item->Name?></a>
                        <td align="center"><?=$item->price_description?>
                        <td align="center"><?=$item->price_code?>
                        <td width="60">
                            <input type="number" step="<?=$item->price_pcs?>" min="<?=$item->price_pcs?>"  name="item_qty[<?=$item->price_id?>]" size="1" value="<?=$item->price_pcs?>"> x
                            <input type=hidden  name="item_unit_price" size="1" value="<?=$price?>" disabled>
                        <td width="50" align="center" class="item_price"><?=$price?>
                      <td width="50" align="center" class="item_total_price"><?=$price*$cart_items[$cart_items_keys[$j]]?><input type="hidden" name="item_price" size="3" value="<?=$price*$cart_items[$cart_items_keys[$j]]?>">
                    </tr>
                    <?
                    $_SESSION['cart_total_price']+= $price*$cart_items[$cart_items_keys[$j]]; 
                    $i++;
                }
                foreach($_SESSION['cart'] as $cartItem){
                    ?>
                    <tr>
                        <td>
                            <?php
                        ?>
                        <td><a href="<?=$cartItem['link']?>"><?=$cartItem['name']?></a>
                        <td align="center"><?=$cartItem['attributes']['catalog']?>
                        <td align="center"><?=$cartItem['sku']?>
                        <td width="60">
                            <?php
                            $editOrder = new \Maksoft\AFI\Forms\Edit($_cart, $_POST);
                            echo $editOrder->start();
                            $editOrder->sku->value = $cartItem['sku'];
                            echo $editOrder->sku;
                            echo $editOrder->action;
                            ?>
                            <input type="number" step="1" min="1"  name="qty" size="1" style="width:100px;" value="<?=$cartItem['qty']?>"> x
                            <button type="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                            <?php
                            $editOrder->end();
                            ?>
                            <input type=hidden  name="item_unit_price" size="1" value="<?=$cartItem['price']?>" disabled>
                        <td width="50" align="center" class="item_price"><?=$cartItem['price']?>
                      <td width="50" align="center" class="item_total_price"><?=$cartItem['total']?><input type="hidden" name="item_price" size="3" value="<?=$cartItem['total']?>">
                    </tr>
                    <?php
                    $_SESSION['cart_total_price'] += $cartItem['total'];
                }
            $_SESSION['cart_total_price'] += delivery($SiteID);
            ?>
            </tbody>
            <tfoot>
             <?php
                $sites = array(2,383);
                if(in_array($SiteID, $sites)) {
                ?>

                <?php
                        if(delivery($SiteID) > 0){
                ?>
                <tr>
                    <td colspan="2">
                        <td colspan="5" align="right">���� ��������: <span class="cart_total_price"><?=money_format("%.2n", $_SESSION['cart_total_price'] - delivery($SiteID)) ?> ��.</span></td>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <td colspan="5" align="right">��������: <span class="cart_total_price"><?=delivery($SiteID)?> ��.</span>
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="2">
                        <td colspan="5" align="right">������� ������: <span class="cart_total_price">
                        <?=money_format("%.2n",$_SESSION['cart_total_price']) ?> ��
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="����������">
                        <td colspan="5" align="right">
                            ���� � ���:
                            <span class="cart_total_price"><?=money_format("%.2n",$_SESSION['cart_total_price']*1.2) ?> ��.
                            </span>
                        <input type="hidden" id="total_price" name="total_price" size=3 value="<?=$_SESSION['cart_total_price']?>">
                    </td>
                </tr>
                <?php
                } else {
                ?>

                <tr>
                    <td colspan="2">
                        <input type="submit" value="����������"><td colspan="5" align="right">���� : <span class="cart_total_price"> <?=money_format('%.2n', $_SESSION['cart_total_price']);?>��.</span><input type="hidden" id="total_price" name="total_price" size=3 value="<?=$_SESSION['cart_total_price']?>">
                    </td>
                </tr>

                <?php
                }
                ?>
            </tfoot>
            </table>
            <div id="cart_buttons">
            <a href="<?=$o_page->get_pLink($_SESSION['cart_last_item'])?>">������ �������</a> <a href="page.php?n=13202&SiteID=<?=$SiteID?>&action=remove_all">�������� ���������</a> <a href="page.php?n=13202&SiteID=<?=$SiteID?>&action=process" class="process">�������</a>
            </div>
            </form>
            <?
        }
    else mk_output_message("normal", "��������� � ������ !");
}

//view shopping cart info
if($action == "process")
    {
        $cart_items_keys = array_keys($cart_items);
        $cart_content_text = "<br><b>�������: ".date_time()."</b><br>======================================================";
        $total_price = "0";
        //print_r($cart_items);
        for($i=0; $i<count($cart_items_keys); $i++)
        {
            $item_sql = "SELECT * FROM prices pr, pages p WHERE pr.price_n = p.n AND pr.price_id = '".$cart_items_keys[$i]."' ";
            $item_res = mysqli_query($item_sql);
            $item = mysqli_fetch_object($item_res);
            $item->Name = str_replace("\"", "", $item->Name);
            $item->Name = str_replace("'", "", $item->Name);
            
            //$cart_content_text .="<br><br>�������: <br><a href=\"http://".$o_site->_site['primary_url']."/".$o_page->get_pLink($item->price_n)."\" target=\"_blank\" class=\"color_link\">".$item->Name."</a>, ".$item->price_code." ( ".$item->price_description . " )";
            $cart_content_text .="<br><br>�������: <br>".$item->Name.", ".$item->price_code." ( ".$item->price_description . " )";
            $cart_content_text .="<br>����: ".$cart_items[$cart_items_keys[$i]] . " x ";
            $price_pcs = $item->price_value;
            $item_price = $cart_items[$cart_items_keys[$i]]* $price_pcs;
            $cart_content_text .= money_format('%.2n' , $price_pcs) . " = ".$item_price."<br><br>======================================================";
            $total_price = bcadd($total_price, $item_price);        
        }

        foreach($_SESSION['cart'] as $item){
            $item['name'] = str_replace("\"", "", $item['name']);
            $item['name'] = str_replace("'", "", $item['name']);
            $cart_content_text .="<br><br>�������: <br>".$item['name'].", ".'hiidea';
            $cart_content_text .="<br>����: ".$item['qty'] . " x ";
            $cart_content_text .= money_format('%.2n' , $item['price']) . " = ".$item['total']."<br><br>======================================================";
            $total_price = bcadd($total_price, $item['total']);
        }

        $_SESSION['cart'] = array();
    

        if(in_array($o_page->_site["SitesID"], array(2,383))){

            $delivery_sum = delivery($SiteID);
            $price_without_vat = bcadd($total_price, $delivery_sum);
            $vat = bcsub($price_without_vat, bcdiv($price_without_vat, "1.2"));
            $total_sum = bcmul($price_without_vat, "1.2");
            $cart_content_text .= "<br> <hr>
            <p> �������� ���� ��������: <b>{$total_price}��.</b></p>
            <p> ��������: <b>{$delivery_sum}��.</b></p>
            <p> ������� ������: <b>{$price_without_vat}</b></p>
            <p> ���: <b>{$vat}</b></p>
            <p> ����: <b>{$total_sum}</b></p>
            <hr>
            <br>";
        } else {
            $cart_content_text.="<br><br>���� �� ���������: $total_price";
        }
        //echo $cart_content_text;
        ?>
            <form method="post" name="cart_order" action="page.php?n=13202&SiteID=<?=$SiteID?>&amp;action=verify">
              <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" class="shopping_cart_info">
                <thead>
                    <tr><td colspan="2">����� �� ���������
                </thead>
                <tbody>
                <tr > 
                  <td width="150" align="right" valign="top"> ��� / ������� :<br>
                   </td>
                  <td><input name="Name" type="text" id="Name" value="<?php echo("$user->Name"); ?>" size="55"></td>
                </tr>
                <tr > 
                  <td align="right" valign="top">E-mail*<br>
                  </td>
                  <td><input name="EMail" type="text" id="EMail2" value="<?php echo("$user->EMail"); ?>" size="55"></td>
                </tr>
                <tr  > 
                  <td align="right" valign="top">������� �� ������:<br>
                    </td>
                  <td><input name="Phone" type="text" id="Phone2" size="55"></td>
                </tr>
                <tr  >
                    <td align="right" valign="top">�����:<br>
                    </td>
                    <td><input name="Firma" type="text" id="Firma" size="55"></td>
                </tr>
                <tr  >
                    <td align="right" valign="top">�������:<br>
                    </td>
                    <td><input name="bulstat" type="text" id="bulstat" size="55"></td>
                </tr>
                <tr > 
                  <td align="right" valign="top">����� �� ��������:<br>
                    <br>
                  </td>
                  <td><textarea name="shopping_cart_address" cols="42" rows="6" id="textarea3"></textarea></td>
                </tr>
                <tr >
                  <td align="right" valign="top">���������:<br>
            </td>
                  <td><textarea name="shopping_cart_comments" cols="42" rows="6" id="textarea2"></textarea>
                </tr>
                <tr  > 
                  <td colspan="2" align="center" valign="middle">
                    <input name="Submit" type="submit" onClick="MM_validateForm('Name','','R');return document.MM_returnValue" value=" ��������� ">
                    <input name="n" type="hidden" id="n" value="<?php echo("$Site->StartPage"); ?>"> 
                    <input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
                    <input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$Site->EMail"); ?>"> 
                    <input type="hidden" name="request_content" value="<?=htmlspecialchars($cart_content_text)?>">
                    <input type="hidden" name="mail_content" value="<?=htmlspecialchars(strip_tags(str_replace("<br>","\n",$cart_content_text)))?>">
                  </td>
                </tr>
                </tbody>
                <tfoot>
                <tr align="center" valign="middle"> 
                  <td colspan="2" align="center"><br>        <em> * �������������� ������ !</em></div></td>
                </tr>
                </tfoot>
              </table>  
            
            </form>     
        <?  
    }
else
if($action == "verify")
    {
        $request_content = stripslashes(htmlspecialchars_decode($_POST['request_content']));
        $mail_content = stripslashes(htmlspecialchars_decode($_POST['mail_content']));
        $separator = "<br>======================================================<br>";
        $request_data= "<br><br>";
        $request_data.= "<b>���:</b> ".$_POST['Name']."<br>";
        $request_data.= "<b>�������:</b> ".$_POST['Phone']."<br>";
        $request_data.= "<b>e-mail:</b> ".$_POST['EMail']."<br>";
        $request_data.= "<b>����� �� ��������</b>: ".$shopping_cart_address."<br>";
        $request_data.= "<br><br>";
        $request_data.= $separator."��� �� �����: ".$_POST['Firma'].$separator."�������: ".$_POST['bulstat'];
        $request_data.= $separator."���������".$separator;
        $request_data.= $shopping_cart_comments;

        
        $request_content.= $request_data;
        $mail_content.= strip_tags(str_replace("<br>","\n",$request_data));
        
        $request_content = htmlspecialchars($request_content);
        $mail_content = htmlspecialchars($mail_content);
        
        //echo $request_content;
        ?>
            <form method="post" action="form-cms.php">
                <center>
                <?php mk_output_message("warning", "�� �� ��������� ������ ������� ���� �������� ���� �� ���������� !")?>
                <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" class="shopping_cart_info">
                <thead>
                    <tr><td colspan="2">����������� �� ���������
                </thead>
                <tbody>
                <tr>
                    <td align="right">  
                    <?php
                             $code = rand(0, 65535);   
                            
                             echo("
                             <img src=\"http://www.maksoft.net/gen.php?code=$code\" align=\"middle\" vspace=5 hspace=5>
                             <input type=\"hidden\" name=\"code\" value=\"$code\">
                             "); 
                             
                             /*
                             //try to skip code if user is loged
                             if($user->ID == 196)
                                {
                                    echo $code;
                                    //$code = 61970;
                                    $rowC_query = mysqli_query("SELECT * FROM tmpcodes WHERE code='".$code."' LIMIT 1");
                                    $rowC = mysqli_fetch_object($rowC_query); 
                                    //echo $code."->".$rowc->codestr;
                                    print_r($rowC);
                                    }
                            */ 
                    ?>
                    <td align="left">
                    �������� ����: <br><input name="codestr" type="text" size="14">
                    </tbody>
                    <tfoot>
                    <tr><td align="center" colspan="2">
                    <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
                    <input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
                    <input name="EMail" type="hidden" id="ToMail" value="<?=$_POST['EMail']?>">
                    <input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$Site->EMail"); ?>"> 
                    <input type="hidden" name="request_content" value="<?=$request_content?>">
                    <input type="hidden" name="mail_content" value="<?=$mail_content?>">
                    <input type="hidden" name="Name" value="<?=$_POST['Name']?>">
                    <input type="hidden" name="Zapitvane" value="������� ��: <?=$_POST['Name']?><br>e-mail: <?=$_POST['EMail']?><br>���.: <?=$_POST['Phone']?>">
                    <input name="conf_page" type="hidden" id="conf_page" value="<?php echo("/page.php?n=$n&SiteID=$SiteID&action=sent"); ?>">

                    <input name="Firma"   type="hidden" id="Firma" value="<?=$_POST['Firma'];?>">
                    <input name="bulstat" type="hidden" id="bulstat" value="<?=$_POST['bulstat'];?>" >
                    <input type="submit" value="������� ���������">
                    </tfoot>
                    </table>
                    </center>
            </form>
        <?  
    }

?>

<!--<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Total</th>
                        <th>�</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-sm-8 col-md-6">
                        <div class="media">
                            <a class="thumbnail pull-left" href="#"> <img class="media-object" src="http://icons.iconarchive.com/icons/custom-icon-design/flatastic-2/72/product-icon.png" style="width: 72px; height: 72px;"> </a>
                            <div class="media-body">
                                <h4 class="media-heading"><a href="#">Product name</a></h4>
                                <h5 class="media-heading"> by <a href="#">Brand name</a></h5>
                                <span>Status: </span><span class="text-success"><strong>In Stock</strong></span>
                            </div>
                        </div></td>
                        <td class="col-sm-1 col-md-1" style="text-align: center">
                        <input type="email" class="form-control" id="exampleInputEmail1" value="3">
                        </td>
                        <td class="col-sm-1 col-md-1 text-center"><strong>$4.87</strong></td>
                        <td class="col-sm-1 col-md-1 text-center"><strong>$14.61</strong></td>
                        <td class="col-sm-1 col-md-1">
                        <button type="button" class="btn btn-danger">
                            <span class="glyphicon glyphicon-remove"></span> Remove
                        </button></td>
                    </tr>
                    <tr>
                        <td class="col-md-6">
                        <div class="media">
                            <a class="thumbnail pull-left" href="#"> <img class="media-object" src="http://icons.iconarchive.com/icons/custom-icon-design/flatastic-2/72/product-icon.png" style="width: 72px; height: 72px;"> </a>
                            <div class="media-body">
                                <h4 class="media-heading"><a href="#">Product name</a></h4>
                                <h5 class="media-heading"> by <a href="#">Brand name</a></h5>
                                <span>Status: </span><span class="text-warning"><strong>Leaves warehouse in 2 - 3 weeks</strong></span>
                            </div>
                        </div></td>
                        <td class="col-md-1" style="text-align: center">
                        <input type="email" class="form-control" id="exampleInputEmail1" value="2">
                        </td>
                        <td class="col-md-1 text-center"><strong>$4.99</strong></td>
                        <td class="col-md-1 text-center"><strong>$9.98</strong></td>
                        <td class="col-md-1">
                        <button type="button" class="btn btn-danger">
                            <span class="glyphicon glyphicon-remove"></span> Remove
                        </button></td>
                    </tr>
                    <tr>
                        <td> � </td>
                        <td> � </td>
                        <td> � </td>
                        <td><h5>Subtotal</h5></td>
                        <td class="text-right"><h5><strong>$24.59</strong></h5></td>
                    </tr>
                    <tr>
                        <td> � </td>
                        <td> � </td>
                        <td> � </td>
                        <td><h5>Estimated shipping</h5></td>
                        <td class="text-right"><h5><strong>$6.94</strong></h5></td>
                    </tr>
                    <tr>
                        <td> � </td>
                        <td> � </td>
                        <td> � </td>
                        <td><h3>Total</h3></td>
                        <td class="text-right"><h3><strong>$31.53</strong></h3></td>
                    </tr>
                    <tr>
                        <td> � </td>
                        <td> � </td>
                        <td> � </td>
                        <td>
                        <button type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                        </button></td>
                        <td>
                        <button type="button" class="btn btn-success">
                            Checkout <span class="glyphicon glyphicon-play"></span>
                        </button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
-->
