<?php 
//image list generator, read directory
$output = '';
$delimiter = "\n";

$output .= 'var tinyMCEImageList = new Array(';
/*
// Use your correct (relative!) path here
$directory = "web/images/icons/"; 

// Since TinyMCE3.x you need absolute image paths in the list...
$abspath = preg_replace('~^/?(.*)/[^/]+$~', '/\\1', $_SERVER['SCRIPT_NAME']);

if (is_dir($directory)) {
    $direc = opendir($directory);

    while ($file = readdir($direc)) {
        if (!preg_match('~^\.~', $file)) { // no hidden files / directories here...
            if (is_file("$directory/$file")) {
                // We got ourselves a file! Make an array entry:
                $output .= $delimiter
                    . '["'
                    . utf8_encode($file)
                    . '", "'
                    . utf8_encode($abspath . $directory . $file)
                    . '"],';
            }
        }
    }

    $output = substr($output, 0, -1); // remove last comma from array item list (breaks some browsers)
    $output .= $delimiter;

    closedir($direc);
}

$output .= ');'; 
//END read directory
*/

//image list generator, read DB
	$im_dir_query = "SELECT * FROM images im left join image_dirs id on im.image_dir = id.ID WHERE im.image_dir = '$Site->imagesdir' LIMIT 500";
	$images_query = $o_page->db_query($im_dir_query);
	$i=1;
	while ($images_list = $o_page->fetch('object', $images_query)) 
		{ 
			// Use your correct (relative!) path here
			$directory = $images_list->image_dir; 
			if($images_list->imageName != "") $title = $images_list->imageName;
			else $title = basename($images_list->image_src); 
			
			$abspath = preg_replace('~^/?(.*)/[^/]+$~', '/\\1', $_SERVER['SCRIPT_NAME']);
					 	
			$output .= $delimiter
                    . '["'
                    . $i . ". " .str_replace("\"", "", str_replace("\'", "", $title))
                    . '", "'
                    . $images_list->image_src
                    . '"],';
			$i++;	
		 }


    $output = substr($output, 0, -1); // remove last comma from array item list (breaks some browsers)
    $output .= $delimiter;
	$output .= ');'; 
// END read DB
	

$file_image_list = "web/images/lists/". $Site->SitesID ."_image_list.js";
/*
if(file_exists($file_image_list))
{
	mk_output_message("normal", "Images list exists");
}
else
{
*/
	$fp = fopen($file_image_list, "w+");
	fwrite($fp, $output);
	//mk_output_message("normal", "Images list genareted :".$file_image_list);

//}
?>
