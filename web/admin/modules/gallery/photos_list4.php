<?PHP
//load ajax library
if($row->SiteID==$SiteID)
{
	?>
	<script type="text/javascript" src="lib/lightbox/prototype.js"></script>
	<script type="text/javascript" src="lib/lightbox/scriptaculous.js?load=effects"></script>
	<script type="text/javascript" src="lib/lightbox/lightbox.js"></script>
	<link rel="stylesheet" href="lib/lightbox/lightbox.css" type="text/css" media="screen">
	<?php
}
//default domain
$default_domain_url = "http://www.maksoft.net";

//domain url
if(!isset($domain_url) || $domain_url =='')

	$domain_url = $default_domain_url;

//images directory
$img_dir = $photo_dir;
if(!isset($img_dir) || empty($img_dir) || $img_dir=='')
	$img_dir = 'img';
	
//images per row
if(!isset($imgs_per_row) || empty($imgs_per_row) || $imgs_per_row=='') {
   if (!isset($table_cols)) {
	$imgs_per_row = 6;
	} else {
	$imgs_per_row = $table_cols;
	}
}

// set to 1 if you want the image filename below the thumb, 0 otherwise
if(!isset($thumb_title) || empty($thumb_title) || $thumb_title=='')
	$thumb_title = 0;  
	
// set the thickness of the border around the thumb, 0 for no border
if(!isset($thumb_border) || empty($thumb_border) || $thumb_border=='')
	$thumb_border = 8; 

$thumb_border_color = '#EBEBEB'; // thumbs border color

define('NL', "\n");

//function
function error_page($error, $line)
        {
        print '<B>Error on line '.$line.':</B>'.NL;
        print '<BR>'.NL;
        print $error.NL;
        print '</HTML>';

        }


if(!is_dir($img_dir))
        {
        error_page('The image directory does not exist', __LINE__);
        }
$DIR = @opendir($img_dir) or error_page('can\'t open image directory', __LINE__);
$thumbs = array('');
while($thumb = readdir($DIR))
        {
        if(eregi('jpg|jpeg|gif|tif|bmp|png|avi', $thumb))
                {
                 array_push($thumbs, str_replace(' ','%20',$thumb));
                }
        }

sort($thumbs);

        print '<TABLE align="center"  cellspacing=0 cellpadding=10 border=0>'.NL;
        print '<TR>'.NL;


for($i = 1; $i < sizeof($thumbs); $i++)
        {
         if(($i % $imgs_per_row == 1) && ($i != 1))
                {
                 print '</TR>'.NL.'<TR>'.NL;
                }
        print '<TD align="center" class=\"border_td\">';
		
		
		$thumbs[$i]= htmlspecialchars($thumbs[$i]); 
		
       //echo "$domain_url/$img_dir/$thumbs[$i]"; 
       print" <a href='".$domain_url."/".$img_dir."/".$thumbs[$i]."' rel='lightbox[gallery]' title=\"$row->Name\">
	   <img  src='http://www.maksoft.net/img_preview.php?pic_name=".$thumbs[$i]."&pic_dir=".$img_dir."&img_width=".$img_width."' border=".$thumb_border." class=\"link_image\"></a>";

        if($thumb_title)
        {
         	print "<br><a href='".$domain_url."/".$img_dir."/".$thumbs[$i]."'>|".$thumbs[$i].'|</a>';

        }
        	print '</TD>'.NL;

        }

print '</TR>'.NL;
print '</TABLE>'.NL;

?>