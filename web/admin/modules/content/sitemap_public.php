<?php require_once $_SERVER['DOCUMENT_ROOT'].'/fak/base.php'; ?>
<?php
//list site pages tree
function list_site_map($Site, $ParentPage, $user, $level=0)
{
	global $n, $SiteID, $count_subpages, $view_map_branches, $view_map_branches_level, $no, $site_map_level;

    $o_page = page::getInstance();
	
	//check for previous declarations
	if(!isset($view_map_branches))
		$view_map_branches = true;
	
	if(!isset($count_subpages))
		$count_subpages = false;
		
	if(!isset($site_map_level))
		$site_map_level = 5;
	
	if(!isset($view_map_branches_level))
		$view_map_branches_level = $site_map_level;
				
	$query = "SELECT * FROM pages WHERE SiteID = '$Site->SitesID' AND ParentPage = '$ParentPage' ORDER by sort_n ASC";
	$result = $o_page->db_query($query);
	
	if(!$view_map_branches && $level >0 || $view_map_branches_level < $level)
		echo "<ul id=\"branch_".$ParentPage."\" style=\"display: none\">";
	else
		echo "<ul id=\"branch_".$ParentPage."\">";
		
	while($page_list = $o_page->fetch("object", $result))
		{
			
			$show_hide_str = "<img src=\"web/images/icons/empty.gif\"> ";
			$branches_counter_str = ""; 
			/*
			//admin string
			if($user->WriteLevel >= $page_list->SecLevel && $user->WriteLevel >0)
				{
					//$edit_str =  "<a href=\"page.php?n=132&SiteID=$SiteID&no=$page_list->n\"><img src=\"web/images/icons/edit-page-blue.gif\"></a>";
					$edit_str =  " <a href=\"page.php?n=132&SiteID=$SiteID&no=$page_list->n\">[ ���������� ]</a> ";
					$up_str = " <a href=\"page.php?n=$n&SiteID=$SiteID&no=$page_list->n&pos=Up\" ><img src=\"web/images/icons/arrow_up.gif\" border=0></a> ";
					$down_str = " <a href=\"page.php?n=$n&SiteID=$SiteID&no=$page_list->n&pos=Down\"><img src=\"web/images/icons/arrow_down.gif\" border=0></a> ";
					$attached_str = "";
					if($page_list->SecLevel > 0)
						$restrict_str = " <img src=\"web/images/icons/restrict-page-red.gif\" alt=\"���������� � � ���� �� ������: $page_list->SecLevel\" border=0> ";
					else $restrict_str = "";
					
					$admin_str = $restrict_str. " | ". $up_str ." | ". $down_str ." | ". $edit_str;

				}
			else $admin_str = "";
			*/
			//check for page branches
			$branch_query = "SELECT * FROM pages WHERE ParentPage = '$page_list->n' AND SiteID = '$page_list->SiteID' ";
			$branch_result = $o_page->db_query($branch_query);
			
			if($o_page->db_count($branch_result) !=0)
				{
					if($count_subpages)
						$branches_counter_str = "(".$o_page->db_count($branch_result).")";
					if($view_map_branches)
						{
							if($site_map_level == $level)
								$show_hide_str = "<a href=\"javascript: show_hide('branch_".$page_list->n."')\"><img src=\"web/images/icons/plus.gif\" border=0></a> ";
							else $show_hide_str = "<a href=\"javascript: show_hide('branch_".$page_list->n."')\"><img src=\"web/images/icons/minus.gif\" border=0></a> ";
						}
					else $show_hide_str = "<a href=\"javascript: show_hide('branch_".$page_list->n."')\"><img src=\"web/images/icons/plus.gif\" border=0></a> ";
				}
			
			if($no == $page_list->n) $page_list->Name = str_replace($page_list->Name, "<b>".$page_list->Name."</b>", $page_list->Name);
				
			if($user->ReadLevel >= $page_list->SecLevel)
				echo "<li>".$show_hide_str ."<a href=\"page.php?n=$page_list->n&SiteID=$page_list->SiteID\" class=\"level$level\" name=\"".$page_list->n."\">".$page_list->Name."</a> ".$branches_counter_str . $admin_str."</li>"; 

			if($o_page->db_count($branch_result) !=0 && $level < $site_map_level )
					list_site_map($Site, $page_list->n, $user, $level+1);

		}

	echo "</ul>";
}

	echo "<div id=\"sitemap\" align=left>";	
	//list public sitemap
	list_site_map($Site, $Site->StartPage, $user, 0);
	echo "</div>";

?>
