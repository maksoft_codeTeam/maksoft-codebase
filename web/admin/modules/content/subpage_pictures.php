<table cellpadding="0" cellspacing="0"><tr><td class="t4">
<?php		
/*
##########################################################
#	module name: 	Subpage pictrues usign table box class and img_preview
#	description:		show all subpage pictures as gallery links
							select strart-menu page, show / skip pages 			
#	directory:			web/admin/modules/menu/
#	author:				M.Petrov, petrovm@abv.bg
##########################################################
*/

if(!isset($img_width))
	$img_width = $MENU_WIDTH;

//select the parent page of the folowing menu items (n=$Site->StartPage)
$menu_parent_item = $row->n;
$res = mysqli_query("SELECT * FROM pages, images WHERE pages.ParentPage = $menu_parent_item AND pages.imageNo = images.imageID ORDER by n"); 
while ($menu_row = mysqli_fetch_object($res)) 
		 {
				
				$img_src = "<img src=\"img_preview.php?pic_name=".basename($menu_row->image_src)."&pic_dir=".dirname($menu_row->image_src)."&img_width=$img_width\" class=\"image_link\" alt=\"$menu_row->Name\" border=0 width=\"".$img_width."\">";
				$info_box_contents = array();			
				$content = "<a href='/page.php?n=".$menu_row->n."&SiteID=".$SiteID."&view_id=".$menu_row->n."'>".$img_src."</a>"; 
				$info_box_contents[] = array(array('text' => $content), array('text' => "<a href=\"page.php?n=$menu_row->n&SiteID=$menu_row->SiteID\" class=\"next_link\">&raquo;</a>", 'params' => "width=20 valign=bottom"));
				
				new contentBox($info_box_contents);
		}		
	
?>
</table>