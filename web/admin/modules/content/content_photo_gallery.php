<?php require_once $_SERVER['DOCUMENT_ROOT'].'/fak/base.php'; ?>
<?php		
/*
##########################################################
#	module name: 	Subpage pictrues usign table box class and img_preview
#	description:		show all subpage pictures as gallery links
							select strart-menu page, show / skip pages 			
#	directory:			web/admin/modules/menu/
#	author:				M.Petrov, petrovm@abv.bg
##########################################################
*/

//images per row
if(!isset($imgs_per_row) || empty($imgs_per_row) || $imgs_per_row=='') {
   if (!isset($table_cols)) {
	$imgs_per_row = 6;
	} else {
	$imgs_per_row = $table_cols;
	}
}

//images per page
if(!isset($imgs_per_page) || empty($imgs_per_page) || $imgs_per_page=='') {
   	if (!isset($table_rows))  $table_rows = 5;
	//default imgs_per_page = 30
	$imgs_per_page = $imgs_per_row*$table_rows;
	
}

// set to 1 if you want the image filename below the thumb, 0 otherwise
if(!isset($thumb_title) || empty($thumb_title) || $thumb_title=='')
	$thumb_title = 0;  
	
// set the thickness of the border around the thumb, 0 for no border
if(!isset($thumb_border) || empty($thumb_border) || $thumb_border=='')
	$thumb_border = 8; 

// thumbs border color
if(!isset($thumb_border_color) || empty($thumb_border_color) || $thumb_border_color=='')
	$thumb_border_color = '#EBEBEB'; 

//image width
if(!isset($MAIN_WIDTH) || $MAIN_WIDTH == "100%")
	$MAIN_WIDTH = 550;
$image_width = (int)($MAIN_WIDTH/$imgs_per_row)-25;

//image thumbnails array
$thumbs = array();
$menu_parent_item = $row->n;
$res = $o_page->db_query("SELECT * FROM pages, images WHERE pages.ParentPage = $menu_parent_item AND pages.imageNo = images.imageID ORDER by n"); 
$i=0;
while($gallery_row = $o_page->fetch("object", $res)) 
        {
			$thumbs[$i]['name'] = $gallery_row->Name;
			$thumbs[$i]['img'] = $gallery_row->image_src;
			$i++;
        }

	//page steps
	$step = $_POST['step'];
	if(!isset($step) || $step<0) $step = 0;
	//lock to the last step
	if($step>(count($thumbs)/$imgs_per_page)) $step = $step-1;

	$ai = $step*$imgs_per_page;			//start element
	$an = ($step+1)*$imgs_per_page;	//end element

?>
<TABLE align="center"  cellspacing=5 cellpadding=0 border=0>
<TR>
		<td colspan="<?=$imgs_per_row?>">
		<TABLE align="center"  cellspacing=0 cellpadding=2 >
				<tr>
					<td width="10" align="center" valign="middle">
						<form method="post">
						<input type="hidden" name="step" value="<?=$step-1?>">
						<input type="submit" value="&laquo;">
						</form>
					<?
					for($i=0; $i<(count($thumbs)/$imgs_per_page); $i++)
						{
						echo '<td><form method="post"><input type="hidden" name="step" value="'.$i.'">';
						echo '<input type="submit" value="'.($i+1).'" class="button_submit"></form>';
						}
					?>
					
					<td>
						<form method="post">
						<input type="hidden" name="step" value="<?=$step+1?>">
						<input type="submit" value="&raquo;">
						</form>
				</tr>
		</table>

<TR>
<?
//imgs_per_row counter
$j=0;

for($i=0;$i<sizeof($thumbs);$i++)
        {
			if($i>=$ai && $i<$an)
				{
					 if(($j >= $imgs_per_row))
					 		{
								print '<TR>';
								$j=1;					 		
							}
					 else $j++;
							
					print '<TD align="center">';
						
					//$thumbs[$i][]= htmlspecialchars($thumbs[$i]); 
					
				   print" <a href=javascript:popImage('".$img_dir."/".$thumbs[$i]['img']."','')>
							<img  src='http://www.maksoft.net/img_preview.php?pic_name=".$thumbs[$i]['img']."&pic_dir=".$img_dir."&img_width=".$image_width."' class=\"border_image\"></a>";
					echo "<br>".$thumbs[$i]['name'];

					print '</TD>';
				}
        }
?>
<tr>
<TR>
		<td colspan="<?=$imgs_per_row?>">
		<TABLE align="center"  cellspacing=0 cellpadding=2 >
				<tr>
					<td width="10" align="center" valign="middle">
						<form method="post">
						<input type="hidden" name="step" value="<?=$step-1?>">
						<input type="submit" value="&laquo;">
						</form>
					<?
					for($i=0; $i<(count($thumbs)/$imgs_per_page); $i++)
						{
						echo '<td><form method="post"><input type="hidden" name="step" value="'.$i.'">';
						echo '<input type="submit" value="'.($i+1).'" class="button_submit"></form>';
						}
					?>
					
					<td>
						<form method="post">
						<input type="hidden" name="step" value="<?=$step+1?>">
						<input type="submit" value="&raquo;">
						</form>
				</tr>
		</table>

</TABLE>
