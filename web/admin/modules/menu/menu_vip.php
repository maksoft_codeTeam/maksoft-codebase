<?php		
/*
##########################################################
#	module name: 	Standart Main Menu 
#	description:		show all pages defined as vip pages for all over the site
#	directory:			web/admin/modules/menu/
#	author:				M.Petrov, petrovm@abv.bg
##########################################################
*/

//select the parent page of the folowing menu items (n=$Site->StartPage)
if(!isset($menu_parent_item))
	$menu_parent_item = $Site->StartPage;

//show / hide items numbering
if($view_numbering == true)
	$start_number = 1;
	
//show / hide submenu bullet
if(!isset($submenu_bullet))
	$submenu_bullet = " &raquo; ";

//show page with selected status (0-default, 1-toplink, 2-drop_down, 3-main_link, 4-vip_link, 5-second_link)
$view_menu_item = "4";
if(!isset($view_menu_item))
	{
		$view_menu_item = 0;
		if(isset($skip_menu_item)) 
			//skip selected pages
			$skip_menu_item_sql = " AND toplink!=$skip_menu_item ";
		else $skip_menu_item_sql = " ";
	}
else 
{
	unset($skip_menu_item);
	//view selected pages	
	$view_menu_item_sql = "toplink = '". $view_menu_item."' ";
}
//view all pages
if($view_menu_item == "all")  $view_menu_item_sql = "1"; 

		 //reading main menu		  
		 $res = mysqli_query("SELECT * FROM pages WHERE SiteID = '$SiteID' AND  $view_menu_item_sql $skip_menu_item_sql AND SecLevel <= '$user->ReadLevel' ORDER by sort_n ASC"); 
		 while ($menu_row = mysqli_fetch_object($res)) 
		 {

			if($view_numbering)
				$content ='<a href="/page.php?n='.$menu_row->n.'&SiteID='.$SiteID.'&view_id='.$menu_row->n.'" class="menu_button" style="display: block;">'.$start_number . $menu_row->Name.'</a>';
			else $content ='<a href="/page.php?n='.$menu_row->n.'&SiteID='.$SiteID.'&view_id='.$menu_row->n.'" class="menu_button" style="display: block;">'.$menu_row->Name.'</a>';
			
			if($_GET['view_id'] == $menu_row->n)
				$content ='<a href="/page.php?n='.$menu_row->n.'&SiteID='.$SiteID.'&view_id='.$menu_row->n.'" class="menu_button" style="display: block;"><b> '.$start_number . $menu_row->Name.'</b></a>';
				
		  		//$content .= "<a href='#' class=\"menu_button\" onClick=\"javascript: redirect('page.php?n=".$menu_row->n."&SiteID=".$SiteID."&view_id=".$menu_row->n."'); \" >".$menu_row->Name."</a>"; 
				$sub_menu_res = mysqli_query("SELECT * FROM pages WHERE ParentPage = '$menu_row->n' $skip_menu_item_sql ORDER by n");
				
				switch($view_submenu)
				{
				case "expand":{
								$submenu_display = "";
								if($_GET['n'] == $menu_row->n) $submenu_display = "";
								break;
							}
				case "open":{
								$submenu_display = "none";
								if($_GET['n'] == $menu_row->n) $submenu_display = "";
								break;
							}
				case "none":
				default:{$submenu_display = ""; break;}
				}
				$submenu_display = "expand";
				/*
				if($view_submenu) 
				{
					$submenu_display = "none";
					if($_GET['view_id'] == $menu_row->n) $submenu_display = "";
				}
				else $submenu_display = "none";
				*/
				
						
				$content.= "<table id=".$menu_row->n." style='display: ".$submenu_display."' border='0' cellspacing='0' cellpadding='0'>";
				while($sub_menu_row = mysqli_fetch_object($sub_menu_res))
				{
					 if($user->AccessLevel >= $sub_menu_row->SecLevel) 
						$content.= "<tr><td><a href='/page.php?n=".$sub_menu_row->n."&SiteID=".$SiteID."' class='submenu_button'>".$submenu_bullet . $sub_menu_row->Name."</a></td></tr>"; 
				}
				//if(mysqli_num_rows($sub_menu_res)==0) echo "<tr><td>No Items";
				$content.= "</table>";
				if($view_numbering)
					$start_number++;
				echo $content;
		 } 
?>