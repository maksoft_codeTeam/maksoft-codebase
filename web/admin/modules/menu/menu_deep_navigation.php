<?php		
/*
##########################################################
#	module name: 	Deep level Main Menu 
#	description:		show all pages defined as subpages of home page
							select strart-menu page, show / skip pages 
							show / hide / expand submenus and deep level submenus
							Deep level submenu generate only the entrance page like NAVBAR
#	directory:			web/admin/modules/menu/
#	author:				M.Petrov, petrovm@abv.bg
##########################################################
*/

//select the parent page of the folowing menu items (n=$Site->StartPage)
if(!isset($menu_parent_item))
	$menu_parent_item = $Site->StartPage;

//show / hide items numbering
if($view_numbering == true)
	$start_number = 1;


//show / hide menu bullet
if(!isset($menu_bullet))
	$menu_bullet = "<div class=\"bullet1\" style=\"float: left;\">&nbsp;</div>";
	
//show / hide submenu bullet
if(!isset($submenu_bullet))
	$submenu_bullet = "<div class=\"bullet2\" style=\"float: left;\">&nbsp;</div>";

//start button
if($view_home == true)
	$start_button = '<tr><td><a href="/page.php?n='.$Site->StartPage.'&SiteID='.$SiteID.'" class="menu_button" style="display: block;">'. $menu_bullet . $Site->Home.'</a>';
else $start_button = "";

//show page with selected status (0-default, 1-toplink, 2-drop_down, 3-main_link, 4-vip_link, 5-second_link)
if(!isset($view_menu_item))
	{
		$view_menu_item = 0;
		if(isset($skip_menu_item)) 
			//skip selected pages
			$skip_menu_item_sql = " AND toplink!=$skip_menu_item ";
		else $skip_menu_item_sql = " ";
	}
else 
{
	unset($skip_menu_item);
	//view selected pages	
	$view_menu_item_sql = "toplink = '". $view_menu_item."' ";
}
//view all pages
if($view_menu_item == "all")  $view_menu_item_sql = "1"; 

		echo $start_button;
		$sort_order = "sort_n ASC";
			if($row->show_link_order == "ASC" || $row->show_link_order == "DESC")
				$sort_order = "sort_n ".$row->show_link_order;
			else $sort_order = $row->show_link_order;
		 //reading main menu		  
		 $res = mysqli_query("SELECT * FROM pages WHERE ParentPage = $menu_parent_item AND  $view_menu_item_sql $skip_menu_item_sql AND SecLevel <= '$user->ReadLevel' ORDER by $sort_order"); 
		 while ($menu_row = mysqli_fetch_object($res)) 
		 {
			
			$content = "<div>";
			
			if($view_numbering)
				$content.="<a href=\"page.php?n=".$menu_row->n."&SiteID=".$SiteID."\" class=\"menu_button\" style=\"display: block;\">".$menu_bullet . $start_number . $menu_row->Name."</a>";
			else $content.="<a href=\"page.php?n=".$menu_row->n."&SiteID=".$SiteID."\" class=\"menu_button\" style=\"display: block;\">".$menu_bullet . $menu_row->Name."</a>";
			
			if($_GET['n'] == $menu_row->n)
				$content ='<a href="/page.php?n='.$menu_row->n.'&SiteID='.$SiteID.'" class="menu_button" style="display: block;"><b> '. $menu_bullet . $start_number . $menu_row->Name.'</b></a>';
				
				$sub_sort_order = "sort_n ASC";
				if($menu_row->show_link_order == "ASC" || $menu_row->show_link_order == "DESC")
					$sub_sort_order = "sort_n ".$menu_row->show_link_order;
				else $sub_sort_order = $menu_row->show_link_order;
			
				$sub_menu_res = mysqli_query("SELECT * FROM pages WHERE ParentPage = '$menu_row->n' $skip_menu_item_sql ORDER by $sub_sort_order");
				
				//read submenu
				switch($view_submenu)
				{
				case "expand":{
								$submenu_display = "";
								if($_GET['n'] == $menu_row->n) $submenu_display = "";
								break;
							}
				case "open":{
								$submenu_display = "none";
								if($_GET['n'] == $menu_row->n) $submenu_display = "";
								break;
							}
				case "none":
				default:{$submenu_display = "none"; break;}
				}
							

				$sub_content = "<div id=\"submenu\" style='display: ".$submenu_display."'>";
				while($sub_menu_row = mysqli_fetch_object($sub_menu_res))
				{
					 if($user->AccessLevel >= $sub_menu_row->SecLevel) 
						$sub_content.= "<a href='/page.php?n=".$sub_menu_row->n."&SiteID=".$SiteID."' class='submenu_button'>".$submenu_bullet . $sub_menu_row->Name."</a>"; 
				}

				$sub_content.= "</div>";
				
				//read deep navigation
				$menu_parent = $row->n;
				$view_deep_menu = false;
				$deep_menu = "<div>";
				$top_n =0;
				$level = 5;
				while ( ($menu_parent >= 1) && ($menu_parent != $Site->StartPage)  && $level >=0)
					{
						$sub_menu_bullet = "<div class=\"bullet".$level."\" style=\"float: left;\">&nbsp;</div>";
						$query = "SELECT * FROM pages WHERE n = '$menu_parent' AND SiteID = '$SiteID' "; 
						$result= mysqli_query("$query");
						$rowParent =  mysqli_fetch_object($result);
						if($rowParent->n == $menu_row->n)
								{
									$top_n = $menu_row->n;
									$content = "<div><a href=\"page.php?n=".$menu_row->n."&SiteID=".$SiteID."\" class=\"menu_button_selected\" style=\"display: block;\" title=\"".$menu_row->Name."\">". $menu_bullet . $menu_row->Name."</a>";
									break;
								}
						else $top_n =0;
						$Name = $rowParent->Name;
						$deep_menu= "<tr><td><a href=\"page.php?n=".$rowParent->n."&SiteID=".$SiteID."\" class=\"menu_button_".$level."\" style=\"display: block;\" title=\"".$Name."\">".$sub_menu_bullet . $Name ."</a>". $deep_menu;
						$level = $level-1 ;
						$menu_parent = $rowParent->ParentPage;
						$view_deep_menu = true;
					}
					
				$deep_menu.= "</div>";
				
				if($top_n != $menu_row ->n)
					$deep_menu = "";
			
				//show submenu or deep menu
				if($view_deep_menu)
					$sub_content = $deep_menu;
					
				$content.=$sub_content;
				
				if($view_numbering)
					$start_number++;
				echo $content . "</div>";
		 } 
?>