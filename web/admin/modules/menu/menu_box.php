<table width="100%" cellpadding="0" cellspacing="0"><tr><td class="t4">
<?php		
/*
##########################################################
#	module name: 	Standart Main Menu usign table box class
#	description:		show all pages defined as subpages of home page
							select strart-menu page, show / skip pages 
							show / hide / expand submenus
#	directory:			web/admin/modules/menu/
#	author:				M.Petrov, petrovm@abv.bg
##########################################################
*/

//select the parent page of the folowing menu items (n=$Site->StartPage)
if(!isset($menu_parent_item))
	$menu_parent_item = $Site->StartPage;

$parent_page = get_page($menu_parent_item);

if(!isset($menu_bullet)) 
	$menu_bullet = "&raquo;";

//show page with selected status (0-default, 1-toplink, 2-drop_down, 3-main_link, 4-vip_link, 5-second_link)
if(!isset($view_menu_item))
	{
		$view_menu_item = "0";
		if(isset($skip_menu_item)) 
			//skip selected pages
			$skip_menu_item_sql = " AND toplink!=$skip_menu_item ";
		else $skip_menu_item_sql = " ";
	}
else 
{
	unset($skip_menu_item);
	//view selected pages	
	
}

$view_menu_item_sql = "toplink = '". $view_menu_item."' ";

//view all pages
if($view_menu_item == "all") $view_menu_item_sql = "1"; 

		 //reading main menu		  
				$info_box_contents = array();
				$info_box_contents[] = array('text' => $parent_page->Name);
				new infoBoxHeading($info_box_contents, true, true, false);
		$content = "";				
		$res = mysqli_query("SELECT * FROM pages WHERE ParentPage = '".$menu_parent_item."' AND  ".$view_menu_item_sql." ORDER by sort_n"); 
		while ($menu_row = mysqli_fetch_object($res)) 
		 {
				$info_box_contents = array();			
				
				switch($view_submenu)
				{
				case "expand":{
								$submenu_display = "";
								if($_GET['view_id'] == $menu_row->n) $submenu_display = "";
								break;
							}
				case "open":{
								$submenu_display = "none";
								if($_GET['view_id'] == $menu_row->n) $submenu_display = "";
								break;
							}
				case "none":
				default:{$submenu_display = "none"; break;}
				}
		
				$content .= "<a href='/page.php?n=".$menu_row->n."&SiteID=".$SiteID."&view_id=".$menu_row->n."' class='menu_button'>$menu_bullet ".$menu_row->Name."</a>"; 

				$sub_menu_res = mysqli_query("SELECT * FROM pages WHERE ParentPage = '$menu_row->n' $skip_menu_item_sql ORDER by sort_n");
				$content .= "<table id=".$menu_row->n." width=100% style='display: ".$submenu_display."' border='0' cellspacing='0' cellpadding='0'>";
				while($sub_menu_row = mysqli_fetch_object($sub_menu_res))
				{
					$content .= "<tr><td class='submenu_button'><a href='/page.php?n=".$sub_menu_row->n."&SiteID=".$SiteID."&view_id=".$menu_row->n."'>$menu_bullet ".$sub_menu_row->Name."</a></td></tr>"; 
				}
				//if(mysqli_num_rows($sub_menu_res)==0) echo "<tr><td>No Items";
				$content .= "</table>";
				
		 } 

				$info_box_contents[] = array('text' => $content);
				new infoBox($info_box_contents);
				
				$info_box_contents = array();
				$info_box_contents[] = array('text' => '&nbsp;');
				new infoBoxFooter($info_box_contents, true, true, true);
				echo "<br>";
				
?>
</table>