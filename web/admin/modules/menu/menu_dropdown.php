<script language=JavaScript>
function redirect() {
var number = quick_menu.selectedIndex;
location.href = quick_menu.options[number].value; }
// -->
</script> 

<?php		
/*
##########################################################
#	module name: 	Dropdown Main Menu 
#	description:		show all pages defined as subpages of the sellected page
							select strart-menu page, show / skip pages 
							show / hide / expand submenus
#	directory:			web/admin/modules/menu/
#	author:				M.Petrov, petrovm@abv.bg
##########################################################
*/
//view  homepage link
if(!isset($view_home_page_link))
	$view_home_page_link = false;
	
//select the parent page of the folowing menu items (n=$Site->StartPage)
if(!isset($menu_parent_item))
	$menu_parent_item = $Site->StartPage;

//show page with selected status (0-default, 1-toplink, 2-drop_down, 3-main_link, 4-vip_link, 5-second_link)
if(!isset($view_menu_item))
	{
		$view_menu_item = 0;
		if(isset($skip_menu_item)) 
			//skip selected pages
			$skip_menu_item_sql = " AND toplink!=$skip_menu_item ";
		else $skip_menu_item_sql = " ";
	}
else 
{
	unset($skip_menu_item);
	//view selected pages	
	$view_menu_item_sql = "toplink = '". $view_menu_item."' ";
}
//view all pages
if($view_menu_item == "all")  $view_menu_item_sql = "1"; 

		 //reading main menu		  
		$res = mysqli_query("SELECT * FROM pages WHERE ParentPage = $menu_parent_item AND  $view_menu_item_sql AND SiteID='$SiteID' ORDER by n");
		if(mysqli_num_rows($res) != 0)
		{
			$content = "<select id=\"quick_menu\" name=\"quick_menu\" onChange=\"redirect(this.form)\"><option value=\"page.php?n=".$row->ParentPage."&SiteID=".$SiteID."\">- - -</option>";
			 
			if($view_home_page_link)
				$home_page_link =  "<option value=\"page.php?n=".$Site->StartPage."&SiteID=".$SiteID."\">".$Site->Home."</option>";
			
			$content.= $home_page_link;
			$selected = "";
			 while ($menu_row = mysqli_fetch_object($res))
				{ 
					if($menu_row->n == $n) $selected = "selected";
					else $selected = "";
					//$content.= '<option value="/page.php?n='.$menu_row->n.'&SiteID='.$SiteID.'&view_id='.$menu_row->n.'" '.$selected.'>'.$menu_row->Name.'</option>';		
					$content.= '<option value="/page.php?n='.$menu_row->n.'&SiteID='.$SiteID.'" '.$selected.'>'.$menu_row->Name.'</option>';		
				}
			$content.="</select>";
				
			 echo $content;
		}
?>