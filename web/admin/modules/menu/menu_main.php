<?php		
/*
##########################################################
#	module name: 		Standart Main Menu 
#	description:		show all pages defined as subpages of home page
						select strart-menu page, show / skip pages 
						show / hide / expand submenus
#	directory:			web/admin/modules/menu/
#	author:				M.Petrov, petrovm@abv.bg
##########################################################
*/

$site_url = "";
if($o_site->_site['primary_url'] != "")
	$site_url = "http://".$o_site->_site['primary_url']."/";
elseif($o_site->get_sURL() != "")
	$site_url = "http://".$o_site->get_sURL()."/";

//if user is logged in use www.maksoft.net
if($user->AccessLevel > 0)
	$site_url = "";
	
//select the parent page of the folowing menu items (n=$Site->StartPage)
if(!isset($menu_parent_item))
	$menu_parent_item = $Site->StartPage;

//show / hide items numbering
if($view_numbering == true)
	$start_number = 1;

//start button
if($view_home == true)
	$start_button = '<a href="'.$site_url.'page.php?n='.$Site->StartPage.'&amp;SiteID='.$SiteID.'" class="menu_button" style="display: block;">'.$Site->Home.'</a>';
else $start_button = "";
	
//show / hide submenu bullet
if(!isset($menu_bullet))
	$menu_bullet = "";
	
//show / hide submenu bullet
if(!isset($submenu_bullet))
	$submenu_bullet = " &raquo; ";

//show page with selected status (0-default, 1-toplink, 2-drop_down, 3-main_link, 4-vip_link, 5-second_link)
if(!isset($view_menu_item))
	{
		$view_menu_item = 0;
		if(isset($skip_menu_item)) 
			//skip selected pages
			$skip_menu_item_sql = " AND toplink!=$skip_menu_item ";
		else $skip_menu_item_sql = " ";
	}
else 
{
	unset($skip_menu_item);
	//view selected pages	
	$view_menu_item_sql = "toplink = '". $view_menu_item."' ";
}

//show / hide submneu
if(!isset($view_submenu)) $view_submenu = "none";

//show / hide selected submenus - only when view_submenu = epxpand !!!
if(!isset($view_submenu_array)) $view_submenu_array = NULL;

//view all pages
if($view_menu_item == "all")  $view_menu_item_sql = "1"; 

		 echo $start_button;
		 if($menu_parent_item == 0)
		 	$parent_page_sql = "1";
		else $parent_page_sql = "ParentPage = $menu_parent_item";
		 //reading main menu		  
		 $res = mysqli_query("SELECT * FROM pages WHERE $parent_page_sql AND  $view_menu_item_sql $skip_menu_item_sql AND SecLevel <= '$user->ReadLevel' AND SiteID = '".$SiteID."' AND status >=0 ORDER by sort_n ASC"); 

		 while ($menu_row = mysqli_fetch_object($res)) 
		 {
			if($menu_row->title == "")
				$menu_row->title = $menu_row->Name;
				
			$class = "";
			if($_GET['n'] == $menu_row->n || $o_page->get_pParent($n) == $menu_row->n) $class = " selected";
			
			if(in_array($menu_row->n, $view_submenu_array))
				$class.= " open";
				
			if($view_numbering)
				$content ='<a href="'.$site_url.'page.php?n='.$menu_row->n.'&amp;SiteID='.$SiteID.'" class="menu_button'.$class.'" style="display: block;" title="'.$menu_row->title.'">' . $menu_bullet .  $start_number . $menu_row->Name.'</a>';
			else $content ='<a href="'.$site_url.'page.php?n='.$menu_row->n.'&amp;SiteID='.$SiteID.'" class="menu_button'.$class.'" style="display: block;" title="'.$menu_row->title.'">' . $menu_bullet .  $menu_row->Name.'</a>';
			
			if($row->n == $menu_row->n)
				$content ='<a href="'.$site_url.'page.php?n='.$menu_row->n.'&amp;SiteID='.$SiteID.'" class="menu_button'.$class.'" style="display: block;" title="'.$menu_row->title.'"><b> '. $menu_bullet .  $start_number . $menu_row->Name.'</b></a>';
				
				$sub_menu_res = mysqli_query("SELECT * FROM pages WHERE ParentPage = '$menu_row->n' $skip_menu_item_sql AND status >=0 ORDER by sort_n ASC");
				$count_subpages = mysqli_num_rows($sub_menu_res);
				
				switch($view_submenu)
				{
				case "expand":{
								$submenu_display = "";
								if(is_array($view_submenu_array) && count($view_submenu_array) >0)
									{
										if(in_array($menu_row->n, $view_submenu_array))
											$submenu_display = "";
										else
											$submenu_display = "none";
									}
								else
									if($_GET['n'] == $menu_row->n) $submenu_display = "";
								break;
							}
				case "open":{
								$submenu_display = "none";
								$pPage = get_page($_GET['n']);
								
								if($_GET['n'] == $menu_row->n || $pPage->ParentPage == $menu_row->n) $submenu_display = "";
								break;
							}
				case "none":
				default:{$submenu_display = "none"; break;}
				}

				if($count_subpages>0 && $submenu_display != "none")
					{		
						$sub_content = "<div id=\"submenu_".$menu_row->n."\" style='display: ".$submenu_display.";' class=\"submenu\">";

						while($sub_menu_row = mysqli_fetch_object($sub_menu_res))
							{
								 if($sub_menu_row->title == "")
									$sub_menu_row->title = $sub_menu_row->Name;
								
								 $class = "";
								 if($_GET['n'] == $sub_menu_row->n) $class = " sbselected";
								 if($user->AccessLevel >= $sub_menu_row->SecLevel) 
									$sub_content.= "<a href='".$site_url."page.php?n=".$sub_menu_row->n."&amp;SiteID=".$SiteID."' class='submenu_button$class' title='".$sub_menu_row->title."'>".$submenu_bullet .  $sub_menu_row->Name."</a>"; 
							}
		
						$sub_content.= "</div>";
					}
				else
					{
						$sub_content = "";

					}
				
				$content.=$sub_content;
				if($view_numbering)
					$start_number++;
				echo $content;
		 } 
?>