<?php
/*
#####################################################
#	module name: 	Standart Navigation
#	description:		show all pages defined as subpages of home page
#	directory:			web/admin/modules/navbar/
#	author:				M.Petrov, petrovm@abv.bg
#####################################################
*/

//reading top links menu		  
 $res = mysqli_query("SELECT * FROM pages WHERE SiteID = $SiteID AND toplink = 1 ORDER by sort_n ASC"); 
 $i=0;
 while ($top_links_row = mysqli_fetch_object($res)) 
	 {
			if($i>0) echo " / ";
			echo "<a href='page.php?n=".$top_links_row->n."&SiteID=".$SiteID."' title=\"".$top_links_row->Name."\">".$top_links_row->Name."</a>";
			$i++;
	 }
?>