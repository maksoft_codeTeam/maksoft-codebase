<?php
/*
#####################################################
#	module name: 	Tab Navigation
#	description:		show only pages defined with toplink = 1
#	directory:			web/admin/modules/navbar/
#	author:				M.Petrov, petrovm@abv.bg
#####################################################
*/

echo '<ul id=navbar valign=bottom>';
//echo '<li><a href="page.php?n='.$Site->StartPage.'&SiteID='.$SiteID.'">'.$Site->Home.'</a></li>';
			 
//reading top links menu		  
 $res = mysqli_query("SELECT * FROM pages WHERE SiteID = $SiteID AND toplink = 1 ORDER by n "); 
 while ($top_links_row = mysqli_fetch_object($res)) 
	 {
			if($_GET['n'] == $top_links_row->n) $class = "class=cur"; else $class="";
			echo "<li $class><a href='page.php?n=".$top_links_row->n."&SiteID=".$SiteID."'>".$top_links_row->Name."</a></li>";
	 }
echo "</ul>";
?>