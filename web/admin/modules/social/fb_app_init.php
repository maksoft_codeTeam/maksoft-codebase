<?php
if(defined('FB_COMMENTS_APP_ID')) {
?>
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?=FB_COMMENTS_APP_ID?>',
      xfbml      : true,
      version    : 'v2.1'
    });

	/*
	FB.getLoginStatus(function(response) {
	  if (response.status === 'connected') {
		console.log('Logged in.');
	  }
	  else {
		FB.login();
	  }
	});
	*/		
  };
  
  
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/bg_BG/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));  
</script>
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'bg'}
</script>
<?php
} // end fb
?>