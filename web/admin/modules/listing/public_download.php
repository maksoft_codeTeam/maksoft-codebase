<?php
//public files array
$pub_files = array(array());
$img_dir = "http://maksoft.net/web/file_icons";

$fs_pub_dir = DIR_FS_PUB.$pub_dir;

if(is_dir($fs_pub_dir."/"))
	{	
			if ($handle = opendir($fs_pub_dir."/")) 
				while (false !== ($file = readdir($handle)))
					if ($file != "." && $file != ".." && $file!="index.php")  
					{
					   $file_info = pathinfo($file);
					   $file_type = $file_info['extension'];
					   //$tmp_ext = strstr($file,'.');
					   //$file_type = str_replace('.','',strstr($file,'.'));
					   
					   //understanding file types
					   switch ($file_type)
					   {   //picture files
						   case 'gif': case 'GIF' : $type = "gif"; $type_pic = $img_dir."/gif.gif"; break;
						   case 'jpg': case 'JPG' : $type = "jpg"; $type_pic = $img_dir."/jpg.gif"; break;
						   case 'png': case 'PNG' : $type = "png"; $type_pic = $img_dir."/png.gif"; break;
						   case 'psd': case 'PSD' : $type = "psd"; $type_pic = $img_dir."/psd.gif"; break;
						   case 'bmp': case 'BMP' : $type = "bmp"; $type_pic = $img_dir."/bmp.gif"; break;
						   case 'ico': case 'ico' : $type = "ico"; $type_pic = $img_dir."/ico.gif"; break;
						   case 'jpeg': case 'JPEG' : $type = "jpeg"; $type_pic = $img_dir."/jpeg.gif"; break;
						   //animation files
						   case 'swf': case 'SWF' : $type = "swf"; $type_pic = $img_dir."/swf.gif"; break;
						   case 'fla': case 'FLA' : $type = "fla"; $type_pic = $img_dir."/fla.gif"; break;
						   //document files
						   case 'doc': case 'DOC' : $type = "doc"; $type_pic = $img_dir."/doc.gif"; break;
						   case 'xls': case 'XLS' : $type = "xls"; $type_pic = $img_dir."/xls.gif"; break;
						   case 'pdf': case 'PDF' : $type = "pdf"; $type_pic = $img_dir."/pdf.gif"; break;
						   //text documets
						   case 'txt': case 'TXT' : $type = "txt"; $type_pic = $img_dir."/txt.gif"; break;
						   case 'inc': case 'INC' : $type = "inc"; $type_pic = $img_dir."/txt.gif"; break;
						   //IE documents
						   case 'html': case 'HTML' : $type = "html"; $type_pic = $img_dir."/html.gif"; break;
						   case 'htm': case 'HTM' : $type = "htm"; $type_pic = $img_dir."/htm.gif"; break;
						   case 'js': case 'JS' : $type = "js"; $type_pic = $img_dir."/js.gif"; break;
						   case 'php': case 'PHP' : $type = "php"; $type_pic = $img_dir."/php.gif"; break;
						   case 'css': case 'CSS' : $type = "css"; $type_pic = $img_dir."/css.gif"; break;
						   case 'jse': case 'JSE' : $type = "jse"; $type_pic = $img_dir."/js.gif"; break;
						   //Applications
						   case 'exe': case 'EXE' : $type = "exe"; $type_pic = $img_dir."/exe.gif"; break;
						   //Video files
						   case 'avi': case 'AVI' : $type = "avi"; $type_pic = $img_dir."/avi.gif"; break;
						   case 'mpg': case 'MPG' : $type = "mpg"; $type_pic = $img_dir."/mpg.gif"; break;
						   case 'mpeg': case 'MPEG' : $type = "mpeg"; $type_pic = $img_dir."/mpeg.gif"; break;
						   case 'asf': case 'ASF' : $type = "asf"; $type_pic = $img_dir."/asf.gif"; break;
						   case 'mov': case 'MOV' : $type = "mov"; $type_pic = $img_dir."/mov.gif"; break;
						   case 'wmv': case 'WMV' : $type = "wmv"; $type_pic = $img_dir."/wmv.gif"; break;
						   //Audio files
						   case 'mp3': case 'MP3' : $type = "mp3"; $type_pic = $img_dir."/mp3.gif"; break;
						   case 'wav': case 'WAV' : $type = "wav"; $type_pic = $img_dir."/wav.gif"; break;
						   case 'mid': case 'MID' : $type = "mid"; $type_pic = $img_dir."/mid.gif"; break;
						   //Archives
						   case 'zip': case 'ZIP' : $type = "zip"; $type_pic = $img_dir."/zip.gif"; break;
						   case 'rar': case 'RAR' : $type = "rar"; $type_pic = $img_dir."/rar.gif"; break;
						   default :  $type = "n/a"; $type_pic = $img_dir."/unknown.gif";
				
					 }
							array_push( $pub_files, array('name'=>$file, 'ext'=>$type, 'size'=>filesize($fs_pub_dir."/".$file)/1024, 'pic'=>$type_pic)); 
				}
				
				closedir($handle); 
				sort($pub_files);
	}

if(count($pub_files)>1)
{
	echo "<table width=100% id=\"border_table\">";
	for($i=1;$i<count($pub_files);$i++)
		echo "<tr><td width=20><a href='http://www.maksoft.net/web/pub/".$pub_dir."/".$pub_files[$i]['name']."' target=_blank><img src='".$pub_files[$i]['pic']."' height=20 border=0></a><td width=20><b>".($i).".</b><td><a href='http://www.maksoft.net/web/pub/".$pub_dir."/".$pub_files[$i]['name']."' target=_blank >".$pub_files[$i]['name']."</a></tr>";
	echo "</table>";
}


?>