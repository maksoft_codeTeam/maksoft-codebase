<link rel="stylesheet" href="http://www.maksoft.net/lib/jquery-ui/themes/smoothness/jquery.ui.all.css">
<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.core.js"></script>
<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.widget.js"></script>
<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.mouse.js"></script>
<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.button.js"></script>
<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.sortable.js"></script>
<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.tabs.js"></script>
<script type="text/javascript">
$j(document).ready(function(){
 
 	
  $j("#page_name").keyup(function(){
  	name = $j("#page_name").val();
  	$j("#pages_container").load("web/admin/site_search_pages.php", "name="+name+"&SiteID=<?=$SiteID?>")
  })
  
	//init sortable
	$j("#group_pages").sortable({
		placeholder: "ui-state-highlight"
		});
	
	$j("#group_pages").disableSelection();
	
	$j("#add_page_to_group").hide();
	
	$j("#form_add_group input:submit").button();
	
	$j("#cms_groups_manager a.submit").button();
	$j("#cms_groups_manager a.submit").click(function(){
		$j("#form_ptg_sort").submit();
		});
	
	$j("#cms_groups_manager .add_page").button({
		 icons: {
                primary: "ui-icon-plus"
		 }
	});
	
	$j("#cms_groups_manager .add_page").click(function(){
		$j("#add_page_to_group").toggle();
		})
	
	$j("#cms_groups_manager .view_groups").button({
		 icons: {
                primary: "ui-icon-folder-collapsed"
		 }
	});
	
	//delete pages checkbox
	$j("#cms_groups_manager input:checkbox").click(function(){
		$j(this).parent("div").parent("li").toggleClass("selected");
		})
})

  function edit_item(id){
  	title = $j('#title_'+id).html();
	$j('#'+id).html("<form method=\"post\"><input type=\"text\" name=\"group_title\" value=\""+title+"\"><input type=\"hidden\" name=\"action\" value=\"edit_group\"><input type=\"hidden\" name=\"group_id\" value=\""+id+"\"><input type=\"submit\" value=\"save\"></form>");
	
  }
</script>
<?php

if($user->AccessLevel > 0)
{
if($_POST['action'] == "bulk_delete")
	{
		//print_r($del_id);
		$counter = 0;
		for($i=0; $i<count($del_id); $i++)
				if(mysqli_query("DELETE FROM pages_to_groups WHERE id = '".$del_id[$i]."' LIMIT 1"))
					$counter++;
		if($counter>0)
			mk_output_message("normal", "Deleted <b>".$counter."</b> record(s) !");
	}

if(isset($del_group_id))
	{
		$search_query = mysqli_query("SELECT * FROM pages_to_groups WHERE group_id = '".$del_group_id."' ");
		$count_records = mysqli_num_rows($search_query);
		if($count_records == 0) 
			{
				$sql = "DELETE FROM groups WHERE group_id = '".$del_group_id."' AND SiteID = '".$SiteID."' LIMIT 1";
				mysqli_query($sql);
				mk_output_admin_message("normal", "Deleted <b>".mysqli_affected_rows()."</b> group(s) !");
			}
		else
			mk_output_admin_message("warning", "Nothing deleted! Selected group contains <b>".$count_records."</b> record(s). Remove them first !");
	}
	
if($_POST['action'] == "add_page")
	{
		$ptg_n = $_POST['page_n'];
		$sort_order = $_POST['group_pages_next_order'];
		if($ptg_n > 0)
		{
			
			//insert page only
			if($_POST['insert_type'] == "page")
				{			
					
					$search_query = mysqli_query("SELECT * FROM pages_to_groups WHERE n = '".$ptg_n."' AND group_id = '".$_POST['group_id']."' ");
					if(mysqli_num_rows($search_query) == 0)
						{
							mysqli_query("INSERT INTO pages_to_groups SET n = '".$ptg_n."', group_id = '".$_POST['group_id']."', start_date = now(), end_date = '".$end_date."', sort_order = '".$sort_order."'");
							if(mysqli_affected_rows() == 1)
								mk_output_admin_message("normal", "������� ��������� 1 ����� !");
						}
					else mk_output_admin_message("warning", "���������� �������� ���� � �������� � ���� ����� !");
					
				}
			
			//insert subpages
			if($_POST['insert_type'] == "subpages")			
				{
					$subpages = $o_page->get_pSubpages($ptg_n);
					$count_success = 0;
					$count_unsuccess = 0;
					for($i=0; $i<count($subpages); $i++)
						{
							$search_query = mysqli_query("SELECT * FROM pages_to_groups WHERE n = '".$subpages[$i]['n']."' AND group_id = '".$_POST['group_id']."' ");
							if(mysqli_num_rows($search_query) == 0)
								{ 
									mysqli_query("INSERT INTO pages_to_groups SET n = '".$subpages[$i]['n']."', group_id = '".$_POST['group_id']."', start_date = now(), end_date = '".$end_date."'");
									$count_success++;
								}
							else $count_unsuccess++;
						}
					mk_output_message("warning", "������� ��������� <b>".$count_success."</b> ��������!");
					if($count_unsuccess > 0)
						mk_output_message("error", "<b>".$count_unsuccess."</b> ���� �� �������� � ���� �����!");
				}
		}
		else mk_output_admin_message("error", "���� ���� ������ �� � �������! <br>������ �� �������� �������� �� ����� ���� !");

	}


if($_POST['action'] == "add_group")
	{
		$group_title = $_POST['group_title'];
		$sql = "INSERT INTO groups SET group_title = '".$group_title."', SiteID = '".$SiteID."', date_added = now()";
		mysqli_query($sql);
		if(mysqli_affected_rows() > 0)
			mk_output_admin_message("normal", "������� ��������� 1 ����� !");	
	}

if($_POST['action'] == "edit_group")
	{
		$group_title = $_POST['group_title'];
		$group_id = $_POST['group_id'];
		$sql = "UPDATE groups SET group_title = '".$group_title."' WHERE group_id = '".$group_id."'";
		mysqli_query($sql);
		if(mysqli_affected_rows() > 0)
			mk_output_admin_message("normal", "������� ����������� 1 ����� !");
		
		//remove group_id
		unset($group_id);
	}

//sort pages in group / delete pages in group	
if($_POST['action'] == "ptg_sort")
	{
		//print_r($_POST['del_pages']);
		$del_pages = $_POST['del_pages'];
		$del_pages_n = array_keys($del_pages);
		//remove selected pages
		for($i=0; $i<count($del_pages_n); $i++)
			if(!mysqli_query("DELETE FROM pages_to_groups WHERE n = '".$del_pages_n[$i]."' AND group_id = '".$group_id."' LIMIT 1"))
				mk_output_admin_message("error", "������ ��� ���������� �� ���������� !");
			
		$sort_counter = 0;
		for($i=0; $i<count($_POST['ptg']); $i++)
			{
				$sql = "UPDATE pages_to_groups SET sort_order = '".($i+1)."' WHERE n = '".$_POST['ptg'][$i]."' AND group_id = '".$group_id."'";
				mysqli_query($sql);
				if(mysqli_affected_rows() > 0)
					$sort_counter++; 
			}
		if($sort_counter>0) mk_output_admin_message("normal", "������� ���������� <b>".$sort_counter."</b> �����(�) !");	
	}

?>
<div id="cms_groups_manager">
<?

//view group content	
if(isset($group_id))
	{
		$group_query = mysqli_query("SELECT * FROM groups WHERE group_id = '".$group_id."' LIMIT 1");
		$group = mysqli_fetch_array($group_query);
		$group_pages_next_order = 1;
		
		echo "<a href=\"".$o_page->get_pLink(63931)."\" class=\"view_groups\">��� ������ �����</a><a href=\"#add_page\" class=\"add_page\">������ ��������</a>";
		echo "<fieldset><legend>".$group['group_title']."</legend>";
		
		$gp_query = mysqli_query("SELECT * FROM pages_to_groups pg, pages p WHERE pg.group_id = '".$group_id."' AND pg.n = p.n ORDER by pg.sort_order ASC, p.Name ASC");
        if(mysqli_num_rows($gp_query)==0)
			mk_output_admin_message("normal", "���� �������� � ���� ����� !");
		else
		{
			$content = "";
			$content.= "<form method=\"post\" id=\"form_ptg_sort\"><ul id=\"group_pages\">";
			while($group_pages = mysqli_fetch_array($gp_query))
					{
						$content.="<li class=\"ui-state-default\" title=\"".$group_pages['Name']."\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span><input type=\"hidden\" name=\"ptg[]\" value=\"".$group_pages['n']."\">".$group_pages['sort_order'].". ".cut_text($group_pages['Name'], 50)."<div class=\"p-options\"><span class=\"date\">".(($group_pages['end_date']!="0000-00-00") ? $group_pages['end_date']:"")."</span><input type=\"checkbox\" name=\"del_pages[".$group_pages['n']."]\"></div>";
						$group_pages_next_order++;
					}
			echo $content;
		}
		$content= "</ul><br clear=\"all\">";
		$content.="<center><a href=\"javascript: void()\" class=\"submit\">������ ���������</a></center><input type=\"hidden\" name=\"action\" value=\"ptg_sort\"></form>";
		$content.= "<br><br><a name=\"add_page\"></a><table width=\"100%\" border=1 cellpadding=\"5\" class=\"border_table\" id=\"add_page_to_group\"><tr><th colspan=\"4\">������ � ������� ...</th><tfoot><tr><td><td colspan=3 valign=\"center\">";
		$content.= "<form method=\"post\" action=\"page.php?n=$n&SiteID=$SiteID&group_id=$group_id\"><input type=\"hidden\" name=\"group_id\" value=\"".$group_id."\">��������: <input type=\"text\" size=\"50\" name=\"page_name\" id=\"page_name\"\"><input size=\"20\" type=\"hidden\" name=\"page_n\" id=\"page_n\" value=\"0\"><input type=\"hidden\" name=\"action\" value=\"add_page\"><input id=\"submit\" type=\"submit\" onClick=\"this.submit()\" value=\"+ ������\" disabled><br>";
		$content.= "<br><fieldset id=\"insert_type\" style=\"display: none\"><legend class=\"t1\">����� �� ������?</legend><input type=\"radio\" name=\"insert_type\" value=\"page\" checked>������ ��������� ��������<br><input type=\"radio\" name=\"insert_type\" value=\"subpages\">������ ������������� �� ��������� ��������</fieldset><div class=\"message_warning\">���� �� ��������: (����-��-��) <input type=\"text\" name=\"end_date\" value=\"0000-00-00\"></div><input type=\"hidden\" name=\"group_pages_next_order\" value=\"".$group_pages_next_order."\"></form>";		
		$content.="<tr><td colspan=4><span id=\"pages_container\"></span></table>";
		echo $content;
		//echo "<a href=\"\" class=\"add_page\">������ ��������</a>";
		echo "</fieldset>";
	}
else 
	{
		$group_query = mysqli_query("SELECT * FROM groups WHERE SiteID = '".$SiteID."' ORDER by group_title");	
		$content = "";
		$content.="<table width=\"100%\" cellpadding=\"5\" class=\"border_table\"><tr><th colspan=3>����� 2</th>";
		$i=1;
		if(mysqli_num_rows($group_query) > 0)
			{
				while($group = mysqli_fetch_array($group_query))
					{
						$search_query = mysqli_query("SELECT * FROM pages_to_groups WHERE group_id = '".$group['group_id']."' ");
						if(mysqli_num_rows($search_query) == 0) $del_group = "<a href=\"#\" onClick=\"if(confirm('Are you sure?')) (document.location='page.php?n=$n&SiteID=$SiteID&del_group_id=".$group['group_id']."')\"><img src=\"http://www.maksoft.net/web/images/icons/delete-page-red.gif\" border=\"0\" hspace=\"5\"></a><a href=\"#edit\"><img src=\"http://www.maksoft.net/web/images/icons/edit-page-blue.gif\" border=\"0\" onClick=\"edit_item('".$group['group_id']."')\"></a>";
						else $del_group ="&nbsp;";
										
						$content.="<tr><td width=\"50\">".$i++."<td id=\"".$group['group_id']."\"><a id=\"title_".$group['group_id']."\" href=\"page.php?n=$n&SiteID=$SiteID&group_id=".$group['group_id']."\">".$group['group_title']."</a><td width=\"50\" align=\"center\">".$del_group;
					}
			}
		else $content.="<tr><td colspan=3><div class=\"message_warning\">No groups found !</div>";
		$content.="<tfoot><td><td><form method=\"post\" action=\"page.php?n=$n&SiteID=$SiteID\" id=\"form_add_group\"><input type=\"text\" name=\"group_title\" size=\"50\" title=\"�������� �� �������\"><input type=\"submit\" value=\" + ������ �����\"><input type=\"hidden\" name=\"action\" value=\"add_group\"><td width=\"100\" align=\"center\">";
		$content.="</table></form>";
		
		echo $content;
	}
}
?>
</div>