<?php
function draw_separator($src, $w=20, $h=20)
{
return "<img src='http://www.maksoft.net/web/images/".$src."' width=".$w." height=".$h.">";
}

  class tableBox {
    var $table_border = '0';
    var $table_width = '100%';
    var $table_cellspacing = '0';
    var $table_cellpadding = '0';
    var $table_parameters = '';
    var $table_row_parameters = '';
    var $table_data_parameters = '';

// class constructor
    function __construct($contents, $direct_output = false) {
      $tableBox_string = '<table border="' . ($this->table_border) . '" width="' . ($this->table_width) . '" cellspacing="' . ($this->table_cellspacing) . '" cellpadding="' . ($this->table_cellpadding) . '"';
      if (!empty($this->table_parameters)) $tableBox_string .= ' ' . $this->table_parameters;
      $tableBox_string .= '>' . "\n";

      for ($i=0, $n=sizeof($contents); $i<$n; $i++) {
        if (isset($contents[$i]['form']) && !empty($contents[$i]['form'])) $tableBox_string .= $contents[$i]['form'] . "\n";
        $tableBox_string .= '  <tr';
        if (!empty($this->table_row_parameters)) $tableBox_string .= ' ' . $this->table_row_parameters;
        if (isset($contents[$i]['params']) && !empty($contents[$i]['params'])) $tableBox_string .= ' ' . $contents[$i]['params'];
        $tableBox_string .= '>' . "\n";

        if (isset($contents[$i][0]) && is_array($contents[$i][0])) {
          for ($x=0, $n2=sizeof($contents[$i]); $x<$n2; $x++) {
            if (isset($contents[$i][$x]['text']) && !empty($contents[$i][$x]['text'])) {
              $tableBox_string .= '    <td';
              if (isset($contents[$i][$x]['align']) && !empty($contents[$i][$x]['align'])) $tableBox_string .= ' align="' . ($contents[$i][$x]['align']) . '"';
              if (isset($contents[$i][$x]['params']) && !empty($contents[$i][$x]['params'])) {
                $tableBox_string .= ' ' . $contents[$i][$x]['params'];
              } elseif (!empty($this->table_data_parameters)) {
                $tableBox_string .= ' ' . $this->table_data_parameters;
              }
              $tableBox_string .= '>';
              if (isset($contents[$i][$x]['form']) && !empty($contents[$i][$x]['form'])) $tableBox_string .= $contents[$i][$x]['form'];
              $tableBox_string .= $contents[$i][$x]['text'];
              if (isset($contents[$i][$x]['form']) && !empty($contents[$i][$x]['form'])) $tableBox_string .= '</form>';
              $tableBox_string .= '</td>' . "\n";
            }
          }
        } else {
          $tableBox_string .= '    <td';
          if (isset($contents[$i]['align']) && !empty($contents[$i]['align'])) $tableBox_string .= ' align="' . ($contents[$i]['align']) . '"';
          if (isset($contents[$i]['params']) && !empty($contents[$i]['params'])) {
            $tableBox_string .= ' ' . $contents[$i]['params'];
          } elseif (!empty($this->table_data_parameters)) {
            $tableBox_string .= ' ' . $this->table_data_parameters;
          }
          $tableBox_string .= '>' . $contents[$i]['text'] . '</td>' . "\n";
        }

        $tableBox_string .= '  </tr>' . "\n";
        if (isset($contents[$i]['form']) && !empty($contents[$i]['form'])) $tableBox_string .= '</form>' . "\n";
      }
	  
      $tableBox_string .= '</table>';

      if ($direct_output == true) echo $tableBox_string;

      return $tableBox_string;
    }
  }

  class infoBox extends tableBox {
    function __construct($contents, $table_id='') {
      $info_box_contents = array();
      $info_box_contents[] = array('text' => $this->infoBoxContents($contents));
      $this->table_cellspacing = '0';
      $this->table_parameters = 'class="infoBox"';
	  $this->table_id = $table_id;
      parent::__construct($info_box_contents, true);
    }

    function infoBoxContents($contents) {
      $this->table_cellpadding = '0';
      $this->table_parameters = 'class="infoBoxContents"';
      $info_box_contents = array();
      $info_box_contents[] = array(array('text' => ''));
      for ($i=0, $n=sizeof($contents); $i<$n; $i++) {
        $info_box_contents[] = array(array('align' => (isset($contents[$i]['align']) ? $contents[$i]['align'] : ''),
                                           'form' => (isset($contents[$i]['form']) ? $contents[$i]['form'] : ''),
                                           'params' => 'class="boxText"',
                                           'text' => (isset($contents[$i]['text']) ? $contents[$i]['text'] : '')));
      }
      $info_box_contents[] = array(array('text' => ''));
      return parent::__construct($info_box_contents);
    }
  }

  class infoBoxHeading extends tableBox {
    function __construct($contents, $left_corner = true, $right_corner = true, $right_arrow = true, $table_id='') {
      $this->table_cellpadding = '0';

      if ($left_corner == true) {
        $left_corner = "<td class='corner_top_left'>".draw_separator("pixel.gif")."</td>";
      } else {
        $left_corner = "";
      }
	  
      if ($right_arrow == true) {
	  //show or hide the content table if an ID is given
        $right_arrow = ' <a href="javascript: show_hide(\''.$table_id.'\')">&raquo;</a>';
      } else {
        $right_arrow = ' ';
      }
	  
      if ($right_corner == true) {
        $right_corner = "<td class='corner_top_right'>".draw_separator("pixel.gif",50, 20)."</td>";
      } else {
        $right_corner ="";
      }

      $info_box_contents = array();
      $info_box_contents[] = array(array('params' => 'height="20" ',
                                         'text' => $left_corner),
                                   array('params' => 'width="100%" height="20" class="infoBoxHeading"',
                                         'text' => $contents[0]['text'].$right_arrow),
                                   array('params' => 'height="20" ',
                                         'text' => $right_corner));

      parent::__construct($info_box_contents, true);
    }
  }

  class infoBoxFooter extends tableBox {
    function __class($contents, $left_corner = true, $right_corner = true, $right_arrow = false) {
      $this->table_cellpadding = '0';

      if ($left_corner == true) {
        $left_corner = "<td class='corner_bottom_left'>".draw_separator("pixel.gif")."</td>";
      } else {
        $left_corner = "";
      }
      if ($right_arrow == true) {
        $right_arrow = '<a href="#">&raquo;</a>';
      } else {
        $right_arrow = '';
      }
      if ($right_corner == true) {
        $right_corner = "<td class='corner_bottom_right'>".draw_separator("pixel.gif",20, 20)."</td>";
      } else {
        $right_corner = "";
      }

      $info_box_contents = array();
      $info_box_contents[] = array(array('params' => 'height="20" ',
                                         'text' => $left_corner),
                                   array('params' => 'width="100%" height="20" class="infoBoxFooter"',
                                         'text' => $contents[0]['text']),
                                   array('params' => 'height="20" ',
                                         'text' => $right_corner));

      parent::__construct($info_box_contents, true);
    }
  }

//normal content BOX
  class contentBox extends tableBox {
    function __construct($contents) {
      $info_box_contents = array();
      $info_box_contents[] = array('text' => $this->contentBoxContents($contents));
      $this->table_cellpadding = '0';
      $this->table_parameters = 'class="infoBox"';
      parent::__construct($info_box_contents, true);
    }

    function contentBoxContents($contents) {
      $this->table_cellpadding = '4';
      $this->table_parameters = 'class="infoBoxContents"';
      return parent::__construct($contents);
    }
  }

//content BOX1
  class contentBox1 extends tableBox {
    function __construct($contents) {
      $info_box_contents = array();
      $info_box_contents[] = array('text' => $this->contentBox1Contents($contents));
      $this->table_cellpadding = '1';
      $this->table_parameters = 'class="contentBox1"';
      parent::__construct($info_box_contents, true);
    }

    function contentBox1Contents($contents) {
      $this->table_cellpadding = '4';
      $this->table_parameters = 'class="contentBox1Contents"';
      return parent::__construct($contents);
    }
  }

//content BOX2
  class contentBox2 extends tableBox {
    function __construct($contents) {
      $info_box_contents = array();
      $info_box_contents[] = array('text' => $this->contentBox2Contents($contents));
      $this->table_cellpadding = '1';
      $this->table_parameters = 'class="contentBox2"';
      parent::__construct($info_box_contents, true);
    }

    function contentBox2Contents($contents) {
      $this->table_cellpadding = '4';
      $this->table_parameters = 'class="contentBox2Contents"';
      return parent::__construct($contents);
    }
  }


  class contentBoxHeading extends tableBox {
    function __construct($contents) {
      $this->table_width = '100%';
      $this->table_cellpadding = '0';

      $info_box_contents = array();
      $info_box_contents[] = array(array('params' => 'height="14" class="infoBoxHeading"',
                                         'text' => 'X'),
                                   array('params' => 'height="14" class="infoBoxHeading" width="100%"',
                                         'text' => $contents[0]['text']),
                                   array('params' => 'height="14" class="infoBoxHeading"',
                                         'text' => 'X'));

      parent::__construct($info_box_contents, true);
    }
  }

  class errorBox extends tableBox {
    function __construct($contents) {
      $this->table_data_parameters = 'class="errorBox"';
      parent::__construct($contents, true);
    }
  }

  class productListingBox extends tableBox {
    function __construct($contents) {
      $this->table_parameters = 'class="productListing"';
      parent::__construct($contents, true);
    }
  }

 
?>
