<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=vgzrspo9yv6rcjc9x6kh90ahdlwzqorexhqo0rek7kxjsf1b"></script>
<script>
tinymce.init({
  selector: ".tinymce4",
  language: 'bg_BG',
  height: 450,
  menubar: false,
  theme: "modern",
  plugins: [
    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "table contextmenu directionality emoticons template textcolor paste textcolor colorpicker textpattern"
  ],
  toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
  toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
  toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | visualchars visualblocks nonbreaking template pagebreak restoredraft",
  toolbar_items_size: 'small'
});
</script>