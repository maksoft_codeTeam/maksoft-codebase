<?php
require_once $_SERVER['DOCUMENT_ROOT']."/lib/xml2array.php";
// $realm = 'Restricted area';

// //user => password
// $users = array('ticksys' => '$1$895d9a8e$keWiuHIJJh6XNH0S6sK3P1');


// if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
//     header('HTTP/1.1 401 Unauthorized');
//     header('WWW-Authenticate: Digest realm="'.$realm.
//            '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

//     die('Restricted Area...');
// }


// // analyze the PHP_AUTH_DIGEST variable
// if (!($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
//     !isset($users[$data['username']]))
//     die('Wrong Credentials!');


// // generate the valid response
// $A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
// $A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
// $valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

// if ($data['response'] != $valid_response)
//     die('Wrong Credentials!');

// // ok, valid username & password.
$now = date('d.m.Y', time());
header('Content-type: application/json');
if(isset($_GET["FIRST_CITY"]) && isset($_GET["LAST_CITY"])):
	$url1 = sprintf("http://78.128.1.75:2008/?command=GET_SCHEDULES&PASS=12TfrS&FOR_DATE=%s&FIRST_CITY=%s&LAST_CITY=%s&company=4&MIN_FREE_PLACES=0", $now, $_GET["FIRST_CITY"], $_GET["LAST_CITY"]);
	$xml = new DomDocument();
	$xml->load($url1);
	$data = new XML2Array();
	$json = $data->createArray($xml->saveXML());
	echo json_encode($json["RESULT"]["COMPANY"]["SCHEDULE"], JSON_FORCE_OBJECT);
endif;

// function to parse the http auth header
function http_digest_parse($txt)
{
    // protect against missing data
    $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
    $data = array();
    $keys = implode('|', array_keys($needed_parts));

    preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

    foreach ($matches as $m) {
        $data[$m[1]] = $m[3] ? $m[3] : $m[4];
        unset($needed_parts[$m[1]]);
    }

    return $needed_parts ? false : $data;
}
?>