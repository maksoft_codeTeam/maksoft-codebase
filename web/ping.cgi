$p = Net::Ping->new('icmp'); 
 
$log_file = '/root/ping.stat'; 
 
$host = $ARGV[0]; 
 
if (!$host) { 
        print "Usage ping.pl host\n"; 
        exit(0); 
} 
 
open(F, ">>$log_file"); 
$time = time; 
$date = `date +%Y%m%d%H%M%S`; 
$date = substr($date, 0, length($date)-1); 
 
if (!$p->ping($host, 3)) { 
        print F "$date | err | $time\n"; 
} else { 
        print F "$date | ok  | $time\n"; 
} 
 
 
close(F);
