<?php
/**
* @version :    2.0
* @copyright	Copyright (C) 2007 - 2010 GetRank.Org  All rights reserved.
* @license		GNU/GPL
* @package		GetRank PageRank Checker
*/

include('config.php');

$time        = time();
$clear_time  = $time - 600;
$update_time = $time - 30;
$ip          = $_SERVER['REMOTE_ADDR'];


$onQuery = mysqli_query("SELECT ip FROM c_online WHERE ip = '".$ip."' LIMIT 1") or die(mysqli_error());
$onCount = mysqli_num_rows($onQuery);

if($onCount < 1) {

        mysqli_query("INSERT INTO c_online (time, ip) VALUES (".$time.", '".$ip."')") or die(mysqli_error());

} else {

        $onResults = mysqli_fetch_assoc($onQuery);

        if($onResults['time'] < $update_time) {

                mysqli_query("UPDATE c_online SET time = ".$time." WHERE ip = '".$ip."' LIMIT 1") or die(mysqli_error());

        }

}

mysqli_query("DELETE FROM c_online WHERE time < ".$clear_time) or die(mysqli_error());

$coQuery = mysqli_query("SELECT COUNT(ip) FROM c_online") or die(mysqli_error());
list($coResult) = mysqli_fetch_row($coQuery);

if($coResult == 1) {

        echo '1 Visitor online | ';

}  else {

        echo $coResult.' Visitors online | ';

}

?>
