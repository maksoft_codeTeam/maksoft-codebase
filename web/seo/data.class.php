<?php
defined( 'GetRank' ) or die( 'Restricted access' );

define('GetRank', 1);
include('data.pagerank.php');

/**
* @version :    2.0
* @copyright	Copyright (C) 2007 - 2010 GetRank.Org  All rights reserved.
* @license		GNU/GPL
* @package		GetRank PageRank Checker
*/
    class pagerank {
        
        var $url;
		
        
        function pagerank ($url) {
            set_time_limit(0);
            $this->url = parse_url('http://' . ereg_replace('^http://', '', $url));
            $this->url['full'] = 'http://' . ereg_replace('^http://', '', $url);
        }

        function getPage ($url) {
            if (function_exists('curl_init')) {
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
                curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com/search?hl=en&q=google&btnG=Google+Search');
                return curl_exec($ch);
            } else {
                return file_get_contents($url);
            }
        }

        function getPagerank () {
		$url=$_REQUEST['urls'];
		$chwrite = CheckHash(HashURL($url));
		
			$url="http://toolbarqueries.google.com/search?client=navclient-auto&ch=".$chwrite."&features=Rank&q=info:".$url."&num=100&filter=0";			
            $data = $this->getPage($url);
            preg_match('#Rank_[0-9]:[0-9]:([0-9]+){1,}#si', $data, $p);
            $value = ($p[1]) ? $p[1] : 0;
            return $value;
        }

        function getAlexaRank ($url) {
        $xml = simplexml_load_file('http://data.alexa.com/data?cli=10&url=' . $url);
        return $xml->SD->POPULARITY['TEXT'];
        }

        
        function getDmoz () {
            $url = ereg_replace('^www\.', '', $this->url['host']);
            $url = "http://search.dmoz.org/cgi-bin/search?search=$url";
            $data = $this->getPage($url);
            if (ereg('<center>No <b><a href="http://dmoz\.org/">Open Directory Project</a></b> results found</center>', $data)) {
                $value = false;
            } else {
                $value = true;
            }
            return $value;
        }
        
        function getYahooDirectory () {
            $url = ereg_replace('^www\.', '', $this->url['host']);
            $url = "http://search.yahoo.com/search/dir?p=$url";
            $data = $this->getPage($url);
            if (ereg('No Directory Search results were found\.', $data)) {
                $value = false;
            } else {
                $value = true;
            }
            return $value;
        }
        
        function getBacklinksGoogle () {
            $url = $this->url['host'];
            $url = 'http://www.google.com/search?q=link%3A'.urlencode($url);
            $data = $this->getPage($url);
            preg_match('/of about \<b\>([0-9\,]+)\<\/b\>/si', $data, $p);
            $value = ($p[1]) ? number_format($this->toInt($p[1])) : 0;
            return $value;

        }

        function getBacklinksYahoo () {
            $url = $this->url['host'];
            $url = 'http://siteexplorer.search.yahoo.com/search?p='.urlencode("http://$url");
            $data = $this->getPage($url);
            preg_match('/Inlinks \(([0-9\,]+)\)/si', $data, $p);
            $value = ($p[1]) ? number_format($this->toInt($p[1])) : 0;
            return $value;
        }
        
        function getResultsAltaVista () {
            $url = $this->url['host'];
            $url = 'http://www.altavista.com/web/results?q=link%3A'.urlencode($url);
            $data = $this->getPage($url);
            preg_match('#AltaVista found ([0-9,]+){1,} results#si', $data, $p);
            $value = ($p[1]) ? number_format($this->toInt($p[1])) : 0;
            return $value;
        }
        
        function getResultsAllTheWeb () {
            $url = $this->url['host'];
            $url = 'http://www.alltheweb.com/search?q=link%3A'.urlencode($url).'&_sb_lang=any';
            $data = $this->getPage($url);
            preg_match('#<span class="ofSoMany">([0-9,]+){1,}</span>#si', $data, $p);
            $value = ($p[1]) ? number_format($this->toInt($p[1])) : 0;
            return $value;
        }
        
        
        function getAge () {
            $url = ereg_replace('^www\.', '', $this->url['host']);
            $url = "http://www.who.is/whois-com/ip-address/$url";
            $data = $this->getPage($url);
            preg_match('#Creation Date: ([a-z0-9-]+)#si', $data, $p);
            if ($p[1]) {
                $time = time() - strtotime($p[1]);
                $years = floor($time / 31556926);
                $days = round(($time % 31556926) / 86400);
                $value = "$years years, $days days";
            } else {
                $value = 'Unknown';
            }
            return $value;
        }
        
        function toInt ($string) {
            return preg_replace('#[^0-9]#si', '', $string);
        }

    }
$options = array(
        'pagerank' => true,
        'dmoz' => true,
        'yahooDirectory' => true,
        'backlinksYahoo' => true,
        'backlinksGoogle' => true,
        'altavista' => true,
        'alltheweb' => true,
        'alexarank' => true,
        'age' => true,
        'thumb' => true
    );
    if ($_POST['urls']) {
        $rep=array("\r"," ");
        $_POST['urls']=str_replace($rep,'',$_POST['urls']);
        $urls = split("\n", $_POST['urls']);
        $results = array();
        foreach ($urls as $url) {
            if(!empty($url)||trim($url!='')){
                $data = new pagerank(trim($url));
                $results[] = array(
                    'url' => $data->url['host'],
                    'pagerank' => $data->getPagerank(),
                    'dmoz' => $data->getDmoz(),
                    'yahooDirectory' => $data->getYahooDirectory(),
                    'backlinksYahoo' => $data->getBacklinksYahoo(),
                    'backlinksGoogle' => $data->getBacklinksGoogle(),
                    'altavista' => $data->getResultsAltaVista(),
                    'alltheweb' => $data->getResultsAllTheWeb(),
                    'alexarank' => $data->getAlexaRank($url),
                    'age' => $data->getAge(),
                    'thumb' =>"http://images.websnapr.com/?size=T&key=C5VIuGtdv2Kd&url=".$url

                );
            }
        }
    }

?>
