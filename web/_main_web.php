<html>
<head>
<title>������� - Web Design � �������</title>
<META http-equiv=Keywords 
content="Web Design, �������� ��������, ����������� ������, ������� .COM .NET .ORG, ��������� �������, �������">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">

<script language="JavaScript">
<!--




<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
<link rel="stylesheet" href="../css/styles" type="text/css">
<link rel="stylesheet" href="css/styles.css" type="text/css">
</head>

<body bgcolor="#ffffff">
<table width="775" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td valign="bottom" colspan="4"><b><font color="#000099"><span class="description"><a name="Domain"></a></span><span class="Title">����������� 
      �� ������<br>
      </span><span class="description"><br>
      </span></font></b><span class="description">���������� ������� � ���� �� 
      �� � ����������� ����� ����� ���. <br>
      ������� �� <strong>������� ����� <font color="#FF0000">�� 29.90�� �� 9.90��</font></strong> 
      <br>
      � ���������� �� ��������� ��������� �� �����.</span></td>
    <td valign="top" colspan="2">&nbsp;</td>
    <td width="153"></td>
  </tr>
  <tr> 
    <td valign="top" align="left" colspan="2" rowspan="2"><a href="#Domain"><br>
      <span class="description">��������� ���� �������� �� ��� ��� � ��������:</span></a></td>
    <td valign="top" colspan="5" align="center"> 
      <form name="RegisterDomain" method="post" action="http://www.maksoft.net/web/regdomain.php">
        <p><font color="#FF0000"><b>www.</b></font> 
          <input type="text" name="domain">
          <b><font color="#FF0000">. </font> </b> 
          <select name="tld">
            <option selected>com</option>
            <option>net</option>
            <option>biz</option>
            <option>info</option>
            <option>org</option>
          </select>
          <input type="submit" name="Submit2" value="���������">
        </p>
      </form></td>
  </tr>
  <tr> 
    <td width="132"></td>
    <td width="66"></td>
    <td width="97"></td>
    <td width="3"></td>
    <td width="153"></td>
  </tr>
  <tr> 
    <td valign="top" colspan="7"> 
      <hr width="100%" size="1" align="center"> 
    </td>
  </tr>
  <tr bgcolor="#F4F4F4"> 
    <td height="297" colspan="7" valign="top"> 
      <table width="100%" border="0">
        <tr>
          <td width="70%" bgcolor="#FFFFCC"> <p><b><font color="#000099"><span class="Title"><a name="submit"></a><font color="#FF0000">�������� 
              � ������������ <font color="#000099"><br>
              ����� <font color="#FF0000">�������� ����</font> ����<br>
              </font></font></span></font></b><span class="Title"><b><font color="#000099">�������� 
              �� ������ ���� <br>
              ��� ��������� �������� � ������������ ��������<span class="Title"><font color="#FF0000">*</font></span></font></b></span></p>
            </td>
          <td width="30%" bgcolor="#FFFFCC">
<form name="form2" method="post" action="http://www.maksoft.net/web/admin/login.php">
              <div align="center">
                <table width="100%" border="0">
                  <tr> 
                    <td><span class="description">����������: </span>
                        <input name="username" type="text" class="description" id="username3" value="">
                    </td>
                    <td width="14%"><span class="description"><em><a href=http://www.maksoft.net/page.php?n=174>�����������</a> 
                      </em></span></td>
                  </tr>
                  <tr>
                    <td><span class="description">������: </span>
                      <input name="pass" type="password" class="description" id="pass2" value="">
                    </td>
                    <td><a href=http://www.maksoft.net/page.php?n=174><span class="description"><em>
                      <input type="submit" name="Submit222" value="����">
                    </em></span></a></td>
                  </tr>
                </table>
              </div>
            </form></td>
        </tr>
      </table>
      <p><span class="description">���� <strong>������������� ������� �� ���������� 
        �� ���������� </strong>��� ���� ������ <strong>�� ��������� � �����������</strong> 
        ���� ���� ��� ����� �����. ������������ ���������� ������� � �� ����������� 
        � �������� � �������� ������. �������� �� ����������� ����������� Internet 
        Explorer � ������� �� ��� ������������� ��� � ������.<br>
        <br>
        �������� � �������� ����� �� <strong>���� ���� � �� ���� �������� �������� 
        �������</strong>.</span></p>
      <p class="description"><font color="#FF0000"><strong>�����������</strong></font> 
        � �� ���� <strong>������ ��������� ��� ������ ����</strong> � ������� 
        �� <font color="#FF0000"><strong>9.90�� </strong></font>�� 1000 ���������.<br>
        ���������� �� ��������� �� ����������� � �������� �� ���������� �� �� 
        �������. <br>
        <br>
        �������� �� ������ ������� �� ����� ������ �� �������� ���� �� ������ 
        ����. <br>
        <a href="#WebDesign">������� ��������<span class="description"> � ������������ 
        ������� �� Web ������ � ������������ � ���������� �� ����������� �����.</span></a></p>
      <form name="form1" method="post" action="/page.php?n=174">
        <span class="description"><em> 
        <input type="submit" name="Submit22" value="����������� � ��� ������ �� ���">
        <font color="#FF0000">*</font> ������ ���������� ���� �� ����� � �����������!</em></span> 
      </form>
    </td>
  </tr>
  <tr> 
    <td colspan="7" valign="top"> 
      <hr width="100%" size="1" align="center"> 
    </td>
  </tr>
  <tr> 
    <td valign="top" colspan="7"> 
      <p><span class="Title"><a name="Hosting"></a><font color="#FF0000">��������������� 
        �������</font> <font color="#000099">� �������� ��� ���</font></span><br>
        <span class="description"><b><a href="hosting.php">�� ����� ������ ����� 
        ��������� ���!</a></b></span><br>
        <br>
        <b><font color="#000099"><span class="description">��� �� ��� <font color="#FF0000">���������</font> 
        � ����� ������� </span></font><span class="description">��������� 5 �������� 
        ������� ����� �� ���� ��������� �� 49.90��/������� � ���������� �� �������� 
        ������� �� ������� � �� �������� ���-������� ������� �� ������ �� �������� 
        ��� �������:</span></b><span class="description"><br>
        <b>- <a href="hosting.php">����� ������</a> - </b>������ � ������ �� ��������� 
        �� ����� ���� ���� � ��������, ������� �� ������������ ����� ������ ��� 
        ����������� ���� ��� ������ �� �� �������� ������� �� �������� ������� 
        � ������. ��� ���� ����� � ��������� ��������� �� ������ ��� �� ������ 
        �� ���� ������, � ���������� �� �������������� �� ������� ��� ���������� 
        �� ���� ������� (Frame/Forward)<br>
        <b>-<a href="hosting.php"> ����� ���</a></b> - �� ����� �������, ������������� 
        ��� ���������� �� ����� ���������, � ��������� ���������� �� ����������� 
        �� ��������� � ��������� �� ����� �� ����������;<br>
        - <a href="hosting.php">����� <b>�������</b> � ����� <b>������</b></a> 
        � ����������� �� FTP ������, <font color="#FF0000">����� Web �������� 
        ����������, FTP ������, Perl, PHP, CGI-Bin, SSH,POP3 ���� �������� � ������</font>, 
        ���������� �� ����������<br>
        <b>- <a href="hosting.php">����� MAX</a> -</b><font color="#FF0000"> </font>FTP 
        ������, Perl, PHP, CGI-Bin, CGI-Bin, SSH, <font color="#FF0000">MySQL 
        ���� ����� � PHPMyAdmin,</font> ����� Web �������� ����������, <font color="#FF0000">����������� 
        ���� �-mail ������</font></span></p>
      <p><span class="description">��������������� ������ �� ������������� �������� 
        �� ������ �������� ��������� �������� �� FTP ������- <a href="/download/cftp302.exe">CuteFTP 
        ������ �� ��������� �� ���!</a></span></p></td>
  </tr>
  <tr> 
    <td colspan="7" valign="top"> 
      <hr width="100%" size="1" align="center"> 
    </td>
  </tr>
  <tr> 
    <td valign="top" colspan="3" bgcolor="#FFFFCC"> 
      <p class="description"><span class="Title"><a name="WebDisign"></a>Web 
        ������</span><br>
        <br>
        <b><font color="#000099"><span class="description">������� </span></font></b><span class="description"></span>�������� 
        ������������� �������� �������, ���������� ������������� �� ���������� 
        HTML, Java Script, ��������� �� ������� ����� � PHP ������, ���������� 
        �� MySQL ���� �����, ���������� �� ������� �� ���������� �������� � �����.<br>
        ���������� � �� �� �����, �� ������� �������� ���� �� ������ ���� �� �������� 
        ������ ���. �� ��� ����� �� ���������, ����� ������������ � �������� �� 
        �� ������. <b>����� � ��������� ������, ����� ��������� ������ � ������ 
        �������. </b>�������� �� ���������� ��� ����� ������ ������� ��������� 
        �-mail � ������� �� ��������� ����� ��� ���� �������, ��������� �� ���������� 
        �� ��� ����������, ����� � ������������ �� ������� �� ������ ���������� 
        ���� �/��� ������� �������. ���������� � �� ������� �������- ������ ����������� 
        ������� ������ �� ������ ������� �������. � ��� ��-������ � ������ �� 
        �� � ���� ��������, �� ������ �� �� ������ ������ � ������ �� ����. <b><font color="#000099"><span class="description"><br>
        <i>������� </i></span></font></b><i>��� ��������� ������������� ������� 
        � ��������� ���������� � ���������� ��� 100 �� DIR.BG. </i></p></td>
    <td valign="top" colspan="4" align="center" class="Title" bgcolor="#FFFFCC"> 
      <p>�������� ��������:<br>
        80.00 ��</p>
      <p><font color="#FF0000">�������� ��������:<br>
        30.00 ��</font></p>
      <p>���������� <br>
        �� PHP ���<br>
        25.00��/���</p>
      <p>&nbsp;</p></td>
  </tr>
  <tr> 
    <td colspan="7" valign="top"> 
      <hr width="100%" size="1" align="center"> 
    </td>
  </tr>
  <tr> 
    <td colspan="7" valign="top"> 
      <p><font color="#CCCCCC" class="Title"><a name="Support"></a><font color="#000000">���������</font></font><br>
        <br>
        <span class="description">�� ������� �� ��������� �� ���� ������������� 
        ��� Web ���� �������� �� ���� �� �������� �������� ��������. �� ���� � 
        ������ ����� �: ��������� ����� ��������� ��, ����������� ���������, ����� 
        ��������� � ����� �����, �������� �� ����� � ���. ����� � ����� �������, 
        ����� �� ����� ������������ �� ������ Web ���� �� ���� �������� � � ������������ 
        ���� � ������. <br>
        �� ������ ���������� ������ ��� ����� ����� � ����� �� <b><font color="#000099">������� 
        </font></b><a href="http://www.maksoft.net/Sofia" target="_blank">�� ���.�.������ 
        150 � �����. </a>�� �� �������� �� ��������� �� ������ ��������� �� ��� 
        ������� <a href="http://www.maksoft.net/Sofia">�� �����</a> ��� �� <a href="mailto:ad@maksoft.net">e-mail</a>.</span></p></td>
  </tr>
  <tr> 
    <td colspan="7" valign="top"> 
      <hr width="100%" size="1" align="center"> 
    </td>
  </tr>
  <tr> 
    <td colspan="7" valign="top"> 
      <p><font color="#000000" class="Title">������ 
        ����� ������ ...</font> <span class="description"><a href="#Domain">��������� 
        ���� �������� �� ��� ������ ��� ��� � ��������.</a></span></p></td>
  </tr>
  <tr> 
    <td colspan="7" valign="top"> 
      <hr width="100%" size="1" align="center"> 
    </td>
  </tr>
  <tr> 
    <td colspan="7" valign="top" align="center" class="description"> 
      <div align="center"><br>
        (c)2002 <a href="mailto:ad@maksoft.net">�������</a></div></td>
  </tr>
  <tr> 
    <td width="264"></td>
    <td width="60"></td>
    <td width="132"></td>
    <td width="66"></td>
    <td width="97"></td>
    <td width="3"></td>
    <td width="153"></td>
  </tr>
</table>
</body>
</html>