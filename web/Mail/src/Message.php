<?php
namespace Maksoft\Mail;


class Message extends \PHPMailer
{
    public function __construct($username, $password, $smtp1, $smtp2)
    {

    }
    /*
    *   
    *   to      -> The receiver
    *   subject -> The mail subject
    *   message -> The mail body, see imap_mail_compose()
    *   cc
    *   bcc     -> The receivers specified in bcc will get the mail, but are excluded from the headers.
    *   rpath   -> Use this parameter to specify return path upon mail delivery failure. 
    *              This is useful when using PHP as a mail client for multiple users.
    */
    public function sendMessage($to, $subject='', $message='', $cc=null, $bcc=null, $rpath=null)
    {
        imap_mail($to, $subject, $message, $cc, $bcc, $rpath);
    }

    public function validate()
    {
        Assert::stringNotEmpty($_POST['to'], 'Полето получател трябва да съдържа текст и валиден мейл');
        $senders = explode(', ', $_POST['to']);
        foreach($senders as $sender){
            if (!filter_var($_POST['to'], FILTER_VALIDATE_EMAIL) === false) {
                throw new \Exception('Въвели сте невалиден мейл адрес '.$sender);
            }
        }
    }

    public function validateAttachment()
    {
        //Check that a value is an existing path
        Assert::fileExists($_FILES['userfile']['tmp_name'], 'path doesn exist');
        //Check that size is integer
        Assert::integerish($_FILES['userfile']['size'], 'unknow filesize %s');
        //Check that is bigger than
        Assert::range($_FILES['userfile']['size'], 300, 6291456, 'max filesize must not exceed 6MB');
        $file_ext=explode('.',$_FILES['userfile']['name']);
        $file_ext = array_pop($file_ext);
        $file_ext =strtolower($file_ext);
        $expensions= array("jpeg","jpg","png");
        if(in_array($file_ext,$expensions)=== false){
             throw new \InvalidArgumentException(
                 "extension not allowed, please choose a JPEG or PNG file."
             );
        }
    }

}
