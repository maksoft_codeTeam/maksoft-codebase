<?php 
$ds = DIRECTORY_SEPARATOR;
use Webmozart\Assert\Assert;

require_once __DIR__.$ds.'vendor'.$ds.'autoload.php';
require_once __DIR__.$ds.'..'.$ds.'..'.$ds.'lib'.$ds.'Database.class.php';
require_once __DIR__.$ds.'header.php';

function conv($string) {
    return iconv('utf8', 'cp1251', $string);
}

$db = new Database();

if(!isset($_SESSION['messages'])){
    $_SESSION['messages'] = array('success' => array(), 'errors' => array());
}

#$url = $o_page->get_pLink();
#$stmt = $db->prepare("SELECT * FROM mail_settings WHERE user_id=:logged_user");
#$stmt->bindValue(':logged_user', $_SESSION['user']->ID); //$o_page->get_uID());
#$stmt->execute();
#$user=$stmt->fetch(PDO::FETCH_OBJ);
$server = new \Fetch\Server('server.maksoft.net', 993);
$server->setAuthentication('cc@maksoft.bg', 'maksoft@cc');
$incoming = 'server.maksoft.net';
$port = 993;
#$server = new \Fetch\Server($user->incoming_server, $user->incoming_server_port);
#$server->setAuthentication($user->incoming_server_username, $user->incoming_server_password);
$box = array();
foreach($server->listMailBoxes() as $mailbox){
    $box[] =  str_replace('{'.$incoming.':'.$port.'/ssl}','', $mailbox);
}
if(isset($_GET['folder']) && $server->hasMailBox($box[$_GET['folder']])){
    $server->setMailBox($box[$_GET['folder']]);
}else{
    $server->setMailBox('INBOX');
}
if($_SESSION['messages']['errors']){
    foreach($_SESSION['messages']['errors'] as $error){
        echo '<pre>'.$error.'</pre></br>';
    }
    $_SESSION['messages']['errors'] = array();
}



$messages = $server->getOrderedMessages(1,1);

?>



<?php

if(isset($_GET['uid']) && is_numeric($_GET['uid']) && $_GET['uid'] > 0){
    $mail_box = false;
    $msg = new \Fetch\Message($_GET['uid'], $server); 
    $sender = $msg->getAddresses('from');
    $recipients = $msg->getAddresses('to');
    if(isset($_GET['reply']) && $_GET['reply']==0){
        require_once __DIR__.$ds.'reply.php';
    }else{
        require_once __DIR__.$ds.'read.php';
    }
}elseif(isset($_GET['write']) and $_GET['write'] == 0){
    require_once __DIR__.$ds.'new.php';
}else{
    require_once  __DIR__.$ds.'inbox.php';
}

if(isset($_POST['send'])){
    require_once __DIR__.$ds.'send.php';
}


?>

<script>
$(document).ready(function(){
    $('button').click(function(){
        var clickBtnValue = $(this).val();
        var ajaxurl = '/web/Mail/delete.php',
        data =  {
            'uid': clickBtnValue,
            'delete': 0,
            };
        $.post(ajaxurl, data, function (response) {
            // Response div goes here.
            alert("action performed successfully");
        });
    });

});
</script>
