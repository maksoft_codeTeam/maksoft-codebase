<?php
session_start();
require __DIR__.$ds.'vendor'.$ds.'autoload.php';

use Webmozart\Assert\Assert;
$_SESSION['messages'] = array('success' => array(), 'errors' => array());

$_SESSION['messages']['success'] = array();

$_SESSION['messages']['errors'] = array();

$mail = new PHPMailer(true);
$to_arr = function($field){
    return explode(',', $field);
};

function validateFILE()
{
    //Check that a value is an existing path
    Assert::fileExists($_FILES['attachment']['tmp_name'], 'path doesn exist');
    //Check that tmp is an existing file
    Assert::file($_FILES['attachment']['tmp_name'], 'file doesnt upload correctly try again');
    //Check that size is integer
    Assert::integerish($_FILES['attachment']['size'], 'unknow filesize %s');
    //Check that is bigger than
    Assert::range($_FILES['attachment']['size'], 300, 6291456, 'max filesize must not exceed 6MB');
}

try {
    $mail->SMTPDebug = false;
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->CharSet = 'utf8';
    $mail->Host = 'mail.maksoft.bg';  // Specify main and backup SMTP servers
    $mail->Port = 587;                                    // TCP port to connect to
    $mail->Username = 'cc@maksoft.bg';                 // SMTP username
    $mail->Password = 'maksoft@cc';                           // SMTP password
    $mail->Mailer = 'smtp';
    $mail->isHTML(true);

    //Set who the messages is to be sent from
    $mail->setFrom('cc@maksoft.bg', 'Radoslav Yordanov');
    //Set an alternative reply-to address
    $mail->addReplyTo('replyto@example.com', 'First Last');
    //Set who the messages is to be sent to
    foreach($to_arr($_POST['to']) as $recipient){
        $mail->addAddress(trim($recipient));
    }
    //Set the subject line
    $mail->Subject = iconv('cp1251', 'utf8', $_POST['subject']);
    //Read an HTML messages body from an external file, convert referenced images to embedded,
    //and convert the HTML into a basic plain-text alternative body
    $mail->msgHTML(iconv('cp1251', 'utf8', $_POST['message']));
    //Attach an image file
    if(!isset($_FILES['attachment']['error'])){
        validateFILE();
        $mail->addAttachment($_FILES['attachment']['tmp_name']);
    }
    if(strlen($_POST['cc']) > 5){
        foreach($to_arr($_POST['cc']) as $cc){
            $mail->AddCC($cc);
        };
    }

    if(strlen($_POST['bcc']) > 5){
        foreach($to_arr($_POST['bcc']) as $bcc){
            $mail->AddBCC($bcc);
        }
    }

    //send the messages
    //Note that we don't need check the response from this because it will throw an exception if it has trouble
    if(!$mail->send()){
        $_SESSION['messages']['errors'][] = 'Съобщението не беше изпратено, моля опитайте пак';
        $_SESSION['messages']['errors'][] = $mail->ErrorInfo();
    }
    $_SESSION['messages']['success'][] = 'Съобщението е изпратено успешно!';
} catch (phpmailerException $e) {
    $_SESSION['messages']['errors'][] = $e->errorMessage();
} catch (Exception $e) {
    $_SESSION['messages']['errors'][] = $e->getMessage();
}
