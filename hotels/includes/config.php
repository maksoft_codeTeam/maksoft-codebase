<?php
//hotels module default configurations
if(!isset($hotel_SiteID))
	$hotel_SiteID = $SiteID;
	
define('SITE_ROOT', "http://".$o_page->_site['primary_url']."/");
define('DIR_WS_HOTELS_UPLOAD', 'web/images/upload/'.$hotel_SiteID."/hotels/");
define('DIR_WS_HOTELS_IMAGES', 'hotels/images/');
define('DIR_WS_HOTELS_INCLUDES', 'hotels/includes/');
define('DIR_WS_HOTELS_LANGUAGES', DIR_WS_HOTELS_INCLUDES . 'languages/');

//max filesize upload 512KB
$max_upload_size = 524288;
?>