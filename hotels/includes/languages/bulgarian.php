<?php
//SYSTEM CONFIGURATIONS
define("DATETIME_FORMAT_SHORT", "d.m");
define("DATETIME_FORMAT_LONG", "d.m.Y");

//LABELS
define("LABEL_HOTEL_RESORT", "<b>������: </b>");
define("LABEL_HOTEL_CATEGORY", "<b>���������: </b>");
define("LABEL_HOTEL_STATUS", "<b>������: </b>");
define("LABEL_PRICE_FROM", "���� �� ");

//FORM TEXT
define("FORM_SELECT_HOTEL", "- �������� ����� -");
define("FORM_SELECT_RESORT", "- �������� ������ -");
define("FORM_SELECT_HOTEL_CATEGORY", "- �������� ��������� -");
define("FORM_BUTTON_SEARCH_HOTELS", "����� ������");
define("FORM_SELECT_TOUR_TYPE", "- ��� ������� -");

//TEXT
define("TEXT_BACK_TO_HOTELS", "����� ��� ������ ������");
define("TEXT_BACK_TO_RESORTS", "����� ��� ������ �������");
define("TEXT_NEXT_HOTEL", "������� �����: ");
define("TEXT_PREVIOUS_HOTEL", "�������� �����: ");

//MESSAGES

//TITLES
define("TITLE_HOTELS_IN_RESORT", "������ � ");
define("TITLE_HOTEL_DESCRIPTION", "����������� �� ������");
define("TITLE_HOTEL_EXTRA_DESCRIPTION", "�������� ������ �� ������");
define("TITLE_SEARCH_HOTELS", "������� �� ������");
define("TITLE_SEARCH_HOTELS_RESULTS", "��������� �� ���������: ");
define("TITLE_HOTEL_SEND_REQUEST", "������� ���������");
define("TITLE_HOTEL_ABOUT", "�� ������");
define("TITLE_HOTEL_PRICES", "����");
define("TITLE_HOTEL_OFFERS", "�������� / ������");

//LINKS
define("LINK_BACK", "�����");
define("LINK_NEXT", "�������");

//TABLE HOTEL PRICES
define("TITLE_SEASONS", "������");
define("LABEL_ROOM_TYPE", "��� �� ������");
define("LABEL_ACCOMMODATION_TYPE", "�����������");
define("LABEL_ROOM_STANDART_BEDS", "������� �����");
define("LABEL_ROOM_EXTRA_BEDS", "���. �����");
define("MESSAGE_PRICES_IN_CURRENCY", "������ �� � %s");
define("MESSAGE_PRICES_FROM_TO", "������ �� �� %s �� %s");
?>