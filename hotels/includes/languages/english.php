<?php
//SYSTEM CONFIGURATIONS
define("DATETIME_FORMAT_SHORT", "d.m");
define("DATETIME_FORMAT_LONG", "d.m.Y");

//LABELS
define("LABEL_HOTEL_RESORT", "<b>resort: </b>");
define("LABEL_HOTEL_CATEGORY", "<b>category: </b>");
define("LABEL_HOTEL_STATUS", "<b>status: </b>");
define("LABEL_PRICE_FROM", "from ");

//FORM TEXT
define("FORM_SELECT_HOTEL", "- select hotel -");
define("FORM_SELECT_RESORT", "- select resort -");
define("FORM_SELECT_HOTEL_CATEGORY", "- category -");
define("FORM_BUTTON_SEARCH_HOTELS", "search for hotel");
define("FORM_SELECT_TOUR_TYPE", "- tourism type -");

//TEXT
define("TEXT_BACK_TO_HOTELS", "back to all hotels");
define("TEXT_BACK_TO_RESORTS", "back to all resorts");
define("TEXT_NEXT_HOTEL", "Next hotel: ");
define("TEXT_PREVIOUS_HOTEL", "Previous hotel: ");

//MESSAGES

//TITLES
define("TITLE_HOTELS_IN_RESORT", "Hotels in ");
define("TITLE_HOTEL_DESCRIPTION", "Hotel details");
define("TITLE_HOTEL_EXTRA_DESCRIPTION", "Actual hotel offers");
define("TITLE_SEARCH_HOTELS", "Hotel search");
define("TITLE_SEARCH_HOTELS_RESULTS", "Search results: ");
//define("TITLE_HOTEL_REQUEST", "Send request");
define("TITLE_HOTEL_SEND_REQUEST", "Send request");
define("TITLE_HOTEL_ABOUT", "About the hotel");
define("TITLE_HOTEL_PRICES", "Prices");
define("TITLE_HOTEL_OFFERS", "Promotions / Packages");

//LINKS
define("LINK_BACK", "back");
define("LINK_NEXT", "next");

//TABLE HOTEL PRICES
define("TITLE_SEASONS", "Seasons");
define("LABEL_ROOM_TYPE", "room type");
define("LABEL_ACCOMMODATION_TYPE", "accommodation");
define("LABEL_ROOM_STANDART_BEDS", "beds");
define("LABEL_ROOM_EXTRA_BEDS", "extra beds");
define("MESSAGE_PRICES_IN_CURRENCY", "all prices are in %s");
define("MESSAGE_PRICES_FROM_TO", "prices from %s to %s");
?>