<?php
	define("FORM_LABEL_HOTEL", "hotel:");
	define("FORM_LABEL_DATE_ARRIVE", "arrive date");
	define("FORM_LABEL_DATE_LEAVE", "leave date");
	define("FORM_LABEL_ROOM_TYPE", "room type");
	define("FORM_LABEL_ROOM_COUNT", "room count");
	define("FORM_LABEL_VISITORS", "visitors");
	define("FORM_LABEL_PEOPLE", "peple");
	define("FORM_LABEL_KIDS", "kids");
	define("FORM_LABEL_NAME", "name");
	define("FORM_LABEL_FAMILY", "family");
	define("FORM_LABEL_NAME_FAMILY", "".FORM_LABEL_NAME.", ".FORM_LABEL_FAMILY.":");
	define("FORM_LABEL_PHONE", "phone");
	define("FORM_LABEL_EMAIL", "e-mail");
	define("FORM_LABEL_COMMENTS", "comments");
	define("FORM_LABEL_PAYMENT", "payment");
	define("FORM_LABEL_PRICE", "���� ���� �� ������������: <span class=\"price\">%s</price>");
	
	define("FORM_LABEL_SLECT_ARRIVE_DATE", "�������� ���� �� ����������� !");
	define("FORM_LABEL_SLECT_LEAVE_DATE", "�������� ���� �� ��������� !");
	define("FORM_LABEL_COUNT_NIGHTS", "���� �������");
	define("FORM_LABEL_YEARS", "������");
	define("FORM_LABEL_SECURE_CODE", "��� �� ���������:");
	
	define("FORM_MESSAGE_VALID_EMAIL", "�������� ������� e-mail ����� !");
	define("FORM_MESSAGE_MUST_CONTAIN_NUMBERS", "������ �� � �������� �� ����� !");
	define("FORM_MESSAGE_MUST_CONTAIN_NUMBER_BETWEEN", "������ �� ����� �����");
	define("FORM_MESSAGE_REQUESTED_FIELD", "� ������������ ���� !");
	define("FORM_MESSAGE_ALL_REQUESTED_FIELD", "������������ ��� * ������ �� ������������! ���� ��������� ��:");
	
	define("TEXT_NIGHTS", " �������");
	define("FORM_BUTTON_SEND", "�������");
?>