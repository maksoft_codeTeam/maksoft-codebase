<?php
	define("FORM_LABEL_HOTEL", "hotel:");
	define("FORM_LABEL_DATE_ARRIVE", "accommodation date:");
	define("FORM_LABEL_DATE_LEAVE", "leaving date:");
	define("FORM_LABEL_ROOM", "room:");
	define("FORM_LABEL_ROOM_TYPE", "type:");
	define("FORM_LABEL_ROOM_COUNT", "count:");
	define("FORM_LABEL_VISITORS", "count visitors:");
	define("FORM_LABEL_PEOPLE", "people");
	define("FORM_LABEL_KIDS", "kids");
	define("FORM_LABEL_NAME", "name");
	define("FORM_LABEL_FAMILY", "family");
	define("FORM_LABEL_NAME_FAMILY", "".FORM_LABEL_NAME.", ".FORM_LABEL_FAMILY.":");
	define("FORM_LABEL_PHONE", "phone:");
	define("FORM_LABEL_EMAIL", "e-mail:");
	define("FORM_LABEL_COMMENTS", "comments:");
	define("FORM_LABEL_RESORT", "resort (city):");
	define("FORM_LABEL_HOTEL_CATEGORY", "category:");
	define("FORM_LABEL_SLECT_ARRIVE_DATE", "Please select accommodation date !");
	define("FORM_LABEL_SLECT_LEAVE_DATE", "Please select leaving date !");
	define("FORM_LABEL_COUNT_NIGHTS", "count nights:");
	define("FORM_LABEL_YEARS", "years:");
	define("FORM_LABEL_SECURE_CODE", "security code:");
	
	define("FORM_MESSAGE_VALID_EMAIL", "input valid e-mail !");
	define("FORM_MESSAGE_MUST_CONTAIN_NUMBERS", "must contain numbers !");
	define("FORM_MESSAGE_REQUESTED_FIELD", "is required field !");
	define("FORM_MESSAGE_ALL_REQUESTED_FIELD", "Fields marked with * are mandantory ! Pleace fill the required fields :");
	
	define("TEXT_NIGHTS", " nights");
	define("FORM_SELECT_ROOM_TYPE","<select name=\"room_type\"><option value=\"single\">single</option><option value=\"double\">double</option><option value=\"triple\">triple</option><option value=\"suit\">suit</option><option value=\"appartment\">appartment</option><option value=\"family\">family</option><option value=\"other\">other</option></select>");	
	define("FORM_BUTTON_SEND", "Send request");
?>