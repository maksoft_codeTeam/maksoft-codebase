<?php
	define("SEARCH_TITLE", "Search hotels");
	define("SEARCH_LABEL_RESORT", "city/resort");
	define("SEARCH_LABEL_TOUR_TYPE", "type of tourism");
	define("SEARCH_LABEL_HOTEL_CATEGORY", "category");
	define("SEARCH_LABEL_HOTEL_STATUS", "status");
	define("SEARCH_LABEL_PRICE_FOR_ROOM", "room price");
	define("SEARCH_LABEL_PRICE_TO", "price from");
	define("SEARCH_BY_NAME", "Search by name");
?>