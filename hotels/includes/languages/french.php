<?php
//SYSTEM CONFIGURATIONS
define("DATETIME_FORMAT_SHORT", "d.m");
define("DATETIME_FORMAT_LONG", "d.m.Y");

//LABELS
define("LABEL_HOTEL_RESORT", "<b>station: </b>");
define("LABEL_HOTEL_CATEGORY", "<b>categorie: </b>");
define("LABEL_HOTEL_STATUS", "<b>status: </b>");
define("LABEL_PRICE_FROM", "de ");

//FORM TEXT
define("FORM_SELECT_HOTEL", "- selectioner un hotel -");
define("FORM_SELECT_RESORT", "- selectioner une station -");
define("FORM_SELECT_HOTEL_CATEGORY", "- choisir une categorie -");
define("FORM_BUTTON_SEARCH_HOTELS", "trouver un hotel");
define("FORM_SELECT_TOUR_TYPE", "- type de tourisme -");

//TEXT
define("TEXT_BACK_TO_HOTELS", "retour vers les hotels");
define("TEXT_BACK_TO_RESORTS", "retour vers les stations");
define("TEXT_NEXT_HOTEL", "Hotel suivant: ");
define("TEXT_PREVIOUS_HOTEL", "Hotel precedent: ");

//MESSAGES

//TITLES
define("TITLE_HOTELS_IN_RESORT", "Hotels a ");
define("TITLE_HOTEL_DESCRIPTION", "Description de l'hotel");
define("TITLE_HOTEL_EXTRA_DESCRIPTION", "Prix paquets");
define("TITLE_SEARCH_HOTELS", "Trouver un hotel");
define("TITLE_SEARCH_HOTELS_RESULTS", "Resultat de la recherche: ");
define("TITLE_HOTEL_SEND_REQUEST", "envoyer une demande");
define("TITLE_HOTEL_ABOUT", "Pour l'hotel");
define("TITLE_HOTEL_PRICES", "Prix");
define("TITLE_HOTEL_OFFERS", "Offres");

//LINKS
define("LINK_BACK", "precedent");
define("LINK_NEXT", "suivant");

//TABLE HOTEL PRICES
define("TITLE_SEASONS", "Saisons");
define("LABEL_ROOM_TYPE", "Type de la chambre");
define("LABEL_ACCOMMODATION_TYPE", "type d'hebergement");
define("LABEL_ROOM_STANDART_BEDS", "lits reguliers");
define("LABEL_ROOM_EXTRA_BEDS", "lits suplementaires");
define("MESSAGE_PRICES_IN_CURRENCY", "prix en %s");
define("MESSAGE_PRICES_FROM_TO", "prix de %s a %s");
?>