<?php
	define("SEARCH_TITLE", "chercher un hotel");
	define("SEARCH_LABEL_RESORT", "ville/station");
	define("SEARCH_LABEL_TOUR_TYPE", "type de tourisme");
	define("SEARCH_LABEL_HOTEL_CATEGORY", "categorie");
	define("SEARCH_LABEL_HOTEL_STATUS", "statut");
	define("SEARCH_LABEL_PRICE_FOR_ROOM", "prix par chambre");
	define("SEARCH_LABEL_PRICE_TO", "prix de");
?>