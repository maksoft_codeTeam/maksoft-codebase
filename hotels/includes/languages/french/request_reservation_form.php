<?php
	//define("FORM_LABEL_HOTEL", "hotel:");
	define("FORM_LABEL_DATE_ARRIVE", "date de l'arrivee:");
	define("FORM_LABEL_DATE_LEAVE", "date du depart :");
	define("FORM_LABEL_ROOM", "chambre:");
	define("FORM_LABEL_ROOM_TYPE", "type de la chambre:");
	define("FORM_LABEL_ROOM_COUNT", "nombre:");
	define("FORM_LABEL_VISITORS", "nombre des visiteurs:");
	define("FORM_LABEL_PEOPLE", "adults");
	define("FORM_LABEL_KIDS", "enfants");
	define("FORM_LABEL_NAME", "prenom");
	define("FORM_LABEL_FAMILY", "nom");
	define("FORM_LABEL_NAME_FAMILY", "".FORM_LABEL_NAME.", ".FORM_LABEL_FAMILY.":");
	define("FORM_LABEL_PHONE", "telephone:");
	define("FORM_LABEL_EMAIL", "e-mail:");
	define("FORM_LABEL_COMMENTS", "commentaires:");
	define("FORM_LABEL_RESORT", "station (ville):");
	define("FORM_LABEL_HOTEL_CATEGORY", "categorie:");
	define("FORM_LABEL_SLECT_ARRIVE_DATE", "Choisir date d'arrivee !");
	define("FORM_LABEL_SLECT_LEAVE_DATE", "Choisir date de depart !");
	define("FORM_LABEL_COUNT_NIGHTS", "nombre de nuitees:");
	define("FORM_LABEL_YEARS", "age:");
	define("FORM_LABEL_SECURE_CODE", "code de securite:");
	
	define("FORM_MESSAGE_VALID_EMAIL", "enrez votre e-mail adresse !");
	define("FORM_MESSAGE_MUST_CONTAIN_NUMBERS", "doit contenir des chifres !");
	define("FORM_MESSAGE_MUST_CONTAIN_NUMBER_BETWEEN", "le message doit contenir des chifres entre");
	define("FORM_MESSAGE_REQUESTED_FIELD", "champ obligatoire !");
	define("FORM_MESSAGE_ALL_REQUESTED_FIELD", "Les champs marques avec une etoile * sont obligatoires. Veuillez les remplir:");
	
	define("TEXT_NIGHTS", " nuitees");
	define("FORM_SELECT_ROOM_TYPE","<select name=\"room_type\"><option value=\"single\">single</option><option value=\"double\">double</option><option value=\"triple\">triple</option><option value=\"suit\">suite</option><option value=\"appartment\">appartement</option><option value=\"family\">familiale</option><option value=\"other\">autre</option></select>");	
	define("FORM_BUTTON_SEND", "Envoyer la demnde");
?>