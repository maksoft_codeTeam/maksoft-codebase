<?php
	//define db table names
	define("TABLE_ACCOMMODATION_TYPES","accommodation_types");
	define("TABLE_CURRENCIES","currencies");
	define("TABLE_EP_TYPES","ep_types");
	define("TABLE_EXTRA_PAYMENTS","extra_payments");
	define("TABLE_HOTELS","hotels");
	define("TABLE_HOTELS_DESCRIPTION","hotels_description");
	define("TABLE_HOTELS_TO_TYPES","hotels_to_types");
	define("TABLE_LANGUAGES","languages");
	define("TABLE_PERIODS","periods");
	define("TABLE_PERIODS_TYPE","periods_type");
	define("TABLE_PERIODS_TYPE_DESCRIPTION","periods_type_description");
	define("TABLE_PROMOTIONS","promotions");
	define("TABLE_PROMOTIONS_DESCRIPTION","promotions_description");
	define("TABLE_RESORTS","resorts");
	define("TABLE_RESORTS_DESCRIPTION","resorts_description");
	define("TABLE_ROOMS","rooms");
	define("TABLE_ROOMS_DESCRIPTION","rooms_description");
	define("TABLE_ROOMS_DESCRIPTION_TITLES","rooms_description_titles");
	define("TABLE_ROOMS_PRICES","rooms_prices");
	define("TABLE_ROOMS_TYPE","rooms_type");
	define("TABLE_TOUR_TYPES","tour_types");
	define("TABLE_TOUR_TYPES_DESCRIPTION","tour_types_description");
?>