<br style="clear:both">
<div id="action_list">
	<a href="<?=tep_href_link("action=list_hotels")?>" <?=$action=='list_hotels' ? "class='selected'":''?>>Hotels</a>
	<a href="<?=tep_href_link("action=list_resorts")?>" <?=$action=='list_resorts' ? "class='selected'":''?>>Resorts</a>
	<a href="<?=tep_href_link("action=list_periods")?>" <?=$action=='list_periods' ? "class='selected'":''?>>Periods</a>
	<a href="<?=tep_href_link("action=list_prices")?>" <?=$action=='list_prices' ? "class='selected'":''?>>Prices</a>
	<a href="<?=tep_href_link("action=list_ep")?>" <?=$action=='list_ep' ? "class='selected'":''?>>Extra payments</a>
	<a href="<?=tep_href_link("action=list_reservations")?>" <?=$action=='list_reservations' ? "class='selected'":''?>>Reservations</a>
	<a href="<?=tep_href_link("action=list_promotions")?>" <?=$action=='list_promotions' ? "class='selected'":''?>>Promotions</a>
</div>
<br style="clear:both">