<?php
include "../../web/admin/db/logOn.inc.php";
mysql_select_db("hotels");

//check hotel dates availability	
function check_hotel_date_availability($hotel_id, $date_from, $date_to)
	{

		//get start / end hotel dates
		$result = mysqli_query("SELECT MIN(p.date_from) as start_date, MAX(p.date_to) as end_date FROM periods_type pt left join periods p on pt.period_type_id = p.period_type_id WHERE pt.hotel_id = '".$hotel_id."' ORDER by p.date_from ASC"); 
		$min_max = mysqli_fetch_array($result);
		$start_date = $min_max['start_date'];
		$end_date = $min_max['end_date'];
		if($date_from >= $start_date && $date_to <= $end_date)
			return true;
		else return false;
	}	

if($_POST['f'] == "check_hotel_date_availability")
	{
		$hotel_id = $_POST['hotel_id'];
		$date_from = $_POST['date_from'];
		$date_to = $_POST['date_to'];
		
		echo check_hotel_date_availability($hotel_id, $date_from, $date_to);
	}

//return all rooms defined for selected hotel
function list_hotel_rooms($hotel_id=0, $name="room_description_id", $selected = 0, $params="")
	{
		if($hotel_id == 0)
			return false;
		else
		{
			//$query = mysqli_query("SELECT * FROM rooms_description rd, rooms r WHERE rd.room_id = r.room_id AND r.hotel_id = '".$hotel_id."'");
			//$query = mysqli_query("SELECT * FROM rooms_description rd, rooms r WHERE rd.room_id = r.room_id AND r.hotel_id = '".$hotel_id."'");
			$query = mysqli_query("SELECT DISTINCT rd.room_description, rd.room_id, rd.room_description_id, r.room_id, r.hotel_id FROM rooms_description rd, rooms r WHERE rd.room_id = r.room_id AND r.hotel_id = '".$hotel_id."'");
			if(mysqli_num_rows($query) > 0)
				{
					$content = "<option value=\"0\">- select room -</option>";
					//$content = "<select name=\"".$name."\" $params style=\"width: 200px\" id=\"room_description_id\"><option value=\"0\">- select room -</option>";
					while($room = mysqli_fetch_array($query))
						$content.= "<option value=\"".$room['room_description_id']."\">".htmlentities($room['room_description'], ENT_NOQUOTES, 'cp1251')." / ".$room['room_description_id']."</option>";
					//$content.="</select>";
				}
			else $content = "No rooms found !";
			return $content;
		}
	}

if($_POST['f'] == "list_hotel_rooms")
	{
		$hotel_id = $_POST['hotel_id'];
		$name = $_POST['name'];
		$params = $_POST['params'];
		
		echo list_hotel_rooms($hotel_id, $name, 0, $params);
	}


//return date difference Y-years, M-months, D-days (default)
function get_date_difference($date_from, $date_to, $return="D")
	{
		$date_from = strtotime($date_from);
		$date_to = strtotime($date_to);
		$difference = $date_to - $date_from;
		switch($return)
			{
				case "Y": { $difference = $difference / (60*60*24*365); break;}
				case "M": { $difference = $difference / (60*60*24*30); break;}
				case "D": 
				default	: { $difference = $difference / (60*60*24); break;}
			}
		return round($difference);
	}

if($_POST['f'] == "get_date_difference")
	{
		$date_from = $_POST['date_from'];
		$date_to = $_POST['date_to'];
		
		echo get_date_difference($date_from, $date_to, "D");
	}		

//return hotel currency as array
function get_hotel_currency($hotel_id, $return = 'currency_string')
	{
		$result = mysqli_query("SELECT * FROM currencies c, hotels h WHERE h.currency_id = c.currency_id AND h.hotel_id = '".$hotel_id."'");
		$currency = mysqli_fetch_array($result);
		return $currency[$return];
	}
	
//return room price for selected period	
function get_room_price($room_description_id, $period_type_id)
	{
		if(!isset($room_description_id) || !isset($period_type_id))
			return "NA";
		
		$result = mysqli_query("SELECT * FROM rooms_prices WHERE room_description_id = '".$room_description_id."' AND period_type_id = '".$period_type_id."'");
		if(mysqli_num_rows($result) > 0)
			{
				$price = mysqli_fetch_array($result);
				return $price['price_value'];
			}
		else return 0;
	}

//format a given price
function format_price($value, $hotel_id=0)
	{
		if($value > 0)
			return sprintf("%.2f", $value)."&nbsp;". get_hotel_currency($hotel_id);
	}

//return period(s) by selected date_from / date_to. If dates are in 2 periods return array
function check_period_dates($hotel_id, $date_from, $date_to)
	{
		//get start / end hotel dates
		$result = mysqli_query("SELECT MIN(p.date_from) as start_date, MAX(p.date_to) as end_date FROM periods_type pt left join periods p on pt.period_type_id = p.period_type_id WHERE pt.hotel_id = '".$hotel_id."' ORDER by p.date_from ASC"); 
		$min_max = mysqli_fetch_array($result);
		$start_date = $min_max['start_date'];
		$end_date = $min_max['end_date'];

		if($date_from >= $start_date && $date_to <= $end_date)
			{
				$sql = "SELECT * FROM periods_type pt left join periods p on pt.period_type_id = p.period_type_id WHERE pt.hotel_id = '".$hotel_id."' AND ((p.date_from <= '".$date_from."' AND '".$date_from."' <= p.date_to) OR (p.date_from <= '".$date_to."' AND '".$date_to."' <= p.date_to ) OR (p.date_from > '".$date_from."' AND '".$date_to."' > p.date_to )) ORDER by p.date_from ASC";
				$result = mysqli_query($sql);
				//store noghts count for each period
				$nights_array = array();
				$nights = 0;
				while($period = mysqli_fetch_array($result))
					{
						
						if($date_from >= $period['date_from'] && $date_from <= $period['date_to'] && $date_to >= $period['date_from'] && $date_to <= $period['date_to'])
								$nights = strtotime($date_to) - strtotime($date_from);
						else if($date_from < $period['date_from'] && $date_to > $period['date_to'])
									$nights = strtotime($period['date_to']) - strtotime($period['date_from']);
						else if($date_from >= $period['date_from'] && $date_from <= $period['date_to'])
									$nights = strtotime($period['date_to']) - strtotime($date_from);
						else if($date_to >= $period['date_from'] && $date_to <= $period['date_to'])
									$nights = strtotime($date_to) - strtotime($period['date_from']);
		
						$period['nights'] = (int)($nights/(60*60*24));	
						$periods[] = $period;
					}
				return $periods;
			}
		else return NULL;
	}

//calculate	reservation price
function calculate_reservation_price($hotel_id, $room_description_id, $date_from, $date_to, $people=1, $kids=0)
	{
		//get periods containing selected dates
		$periods = check_period_dates($hotel_id, $date_from, $date_to);
		$price = 0;
		for($i=0; $i<count($periods); $i++)
			{
				$price += $periods[$i]['nights'] * get_room_price($room_description_id, $periods[$i]['period_type_id']) * $people;
			}
		return format_price($price, $hotel_id);
	}

if($_POST['f'] == "calculate_reservation_price")
	{
		$date_from = $_POST['date_from'];
		$date_to = $_POST['date_to'];
		$hotel_id = $_POST['hotel_id'];
		$room_description_id = $_POST['room_description_id'];
		$people = $_POST['people'];
		
		echo calculate_reservation_price($hotel_id, $room_description_id, $date_from, $date_to, $people, 0);
	}

//list max count of people in a room 
function list_room_people($room_id)
	{
		$content = "";
		$sql = "SELECT * FROM rooms_description WHERE room_description_id = '".$room_id."' LIMIT 1";
		$result = mysqli_query($sql);
		$room = mysqli_fetch_array($result);
		for($i=1; $i<=$room['room_standart_beds']; $i++)
			$content.= "<option value=\"".$i."\">".$i."</option>";
		return $content;
	}
	
if($_POST['f'] == "list_room_people")
	{
		$room_id = $_POST['room_description_id'];
		echo list_room_people($room_id);
	}
	
?>