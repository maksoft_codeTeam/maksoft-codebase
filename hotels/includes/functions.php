<?php
// The HTML href link wrapper function
function tep_href_link($parameters = '')
  {
	global $n, $SiteID, $language_id, $action, $hotel_id, $resort_id;
	 	
 	$page = $PHP_SELF;
	
	$s_params ="n=".$n."&SiteID=".$SiteID;
	$add_params = "";
	/*
	if(isset($action) && !in) $add_params.="&action=".$action;
	if(isset($hotel_id)) $add_params.="&hotel_id=".$hotel_id;
	if(isset($resort_id)) $add_params.="&resort_id=".$resort_id;
	*/
	//$params_array = explode("&", $parameters);
	
	$parameters = $s_params. $add_params . "&".$parameters;
    $link = "";

    if (!empty($parameters)) {
 	  $link .= $page . '?' . $parameters;
      $separator = '&';
    } else {
      $link .= $page;
      $separator = '?';
    }

    while ( (substr($link, -1) == '&') || (substr($link, -1) == '?') ) $link = substr($link, 0, -1);
   	return $link;
  }

//get current language
function get_language($language_id)
	{
		$sql = "SELECT * FROM languages WHERE language_id = '".$language_id."' LIMIT 1";
		$query = mysqli_query($sql);
		$language = mysqli_fetch_array($query);
		return $language;
	}

//get all languages
function get_languages()
	{
		global $hotels_available_languages;
		if(!array($hotels_available_languages) || count($hotels_available_languages) == 0)
			$hotels_available_languages = array("BG", "EN", "RU", "FR");
		
		$sql = "SELECT * FROM languages";
		$query = mysqli_query($sql);
		$languages_array = array();
		while($languages = mysqli_fetch_array($query))
			if(in_array($languages['language_key'], $hotels_available_languages))
				$languages_array[] = $languages;
		
		return $languages_array;
	}
	
//list all resorts in select menu
function list_resorts($selected_id = 0, $params="", $force_select = false, $default_value = true)
	{
		global $language_id;
		//if force select -> select the last inserted resort
		if($selected_id ==0 && $force_select)
			{
				//select last inserted resort
				$res = mysqli_query("SELECT * FROM resorts r left join resorts_description rd on r.resort_id = rd.resort_id WHERE rd.language_id = '".$language_id."' ORDER by r.resort_id DESC LIMIT 1");
				$last_resort = mysqli_fetch_array($res);
				$selected_id = $last_resort['resort_id'];
			}
			
		if($default_value) $content = "<select name=\"resort_id\" $params><option value=\"0\">".FORM_SELECT_RESORT."</option>";
		else $content = "<select name=\"resort_id\" $params>";
		
		$sql = "SELECT * FROM resorts r left join resorts_description rd on r.resort_id = rd.resort_id WHERE rd.language_id = '".$language_id."' ORDER by rd.resort_title ASC";
		$query = mysqli_query($sql);
		while($resort = mysqli_fetch_array($query))
			{
				if($resort['resort_id'] == $selected_id) $selected = "selected";
				else $selected = "";				
				$content.= "<option $selected value=\"".$resort['resort_id']."\">".$resort['resort_title'] ."</option>";
			}
		$content.= "</select>";
		echo $content;
		//echo $sql;
	}
function getAllHotels($kurort=NULL){
	$hotels = array();
	global $language_id;
	if($kurort==NULL){
		$query = mysqli_query("
		SELECT * FROM ".TABLE_HOTELS_DESCRIPTION."
		WHERE language_id = ".$language_id."
		ORDER BY hotel_title");
		while($hotel = mysqli_fetch_array($query)){
			$hotelInfo["hotel_id"] = $hotel['hotel_id'];
			$hotelInfo["hotel_title"] = addslashes($hotel['hotel_title']);
			$hotels[] = $hotelInfo;
		}
	}else{
		$query = mysqli_query("
		SELECT * FROM ".TABLE_HOTELS." th
		LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd on th.hotel_id = thd.hotel_id 
		WHERE thd.language_id = ".$language_id."
		AND th.resort_id = ".$kurort."
		ORDER BY hotel_title");
		while($hotel = mysqli_fetch_array($query)){
			$hotelInfo["hotel_id"] = $hotel['hotel_id'];
			$hotelInfo["hotel_title"] = addslashes($hotel['hotel_title']);
			$hotels[] = $hotelInfo;
		}
		
		
	}
		
	return $hotels;
}
function searchbar($searchstring=""){
	$kurort=NULL;
	if($_GET["resort_id"]!=0){
		$kurort=$_GET["resort_id"];
	}
	/* $tipTurizam=NULL;
	$kategoria=NULL;
	if($_GET["tour_type_id"]!=0){
		$tipTurizam=$_GET["tour_type_id"];
	}
	if($_GET["hotel_category"]!=0){
		$kategoria=$_GET["hotel_category"];
	} */
	$hotelsArray = getAllHotels($kurort);
	?>
		<input type="text" class="autocomplete" id="autocomplete" name="hotelname" value="<?=$searchstring;?>" style="width:392px;"/>
		
	
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet"  href="http://www.maksoft.net/hotels/styles/autocomplete.css" type="text/css"/>
	<script>
			var hotels = [
			   <?php
				foreach($hotelsArray as $hotel){
					?>
					{ value: '<?=$hotel['hotel_title'];?>', data: '<?=SITE_ROOT;?>page.php?n=76216&hotel_id=<?=$hotel['hotel_id'];?>#<?=$hotel['hotel_title'];?>' },
					<?php
				}
			   ?>
			];
			console.log(hotels);
			$j('#autocomplete').autocomplete({
				source: hotels,
				select: function (event, ui) {
					var url=ui.item.data;
					window.open(url, '_blank');
				},
				minLength: 0,
				/* shownoresults: true, */
			}).focus(function(){            
				$j(this).trigger('keydown');
			});
			
		
	</script>
	<?php
	
}
	
function list_hotels($selected_id, $params="", $conditions = "1")
	{
		global $hotel_SiteID, $language_id;
		
		$content = "<select name=\"hotel_id\" $params><option value=\"0\">".FORM_SELECT_HOTEL."</option>";
		$sql = "SELECT * FROM hotels h left join hotels_description hd on h.hotel_id = hd.hotel_id, resorts r left join resorts_description rd on r.resort_id = rd.resort_id WHERE h.hotel_SiteID='".$hotel_SiteID."' AND h.resort_id = r.resort_id AND $conditions AND hd.language_id = '".$language_id."' AND rd.language_id = '".$language_id."' ORDER by rd.resort_title ASC, hd.hotel_title ASC"; 
		$query = mysqli_query($sql);
		$resort_id = 0;
		while($hotel = mysqli_fetch_array($query))
			{
				if($hotel['hotel_id'] == $selected_id) $selected = "selected";
				else $selected = "";
				if($resort_id != $hotel['resort_id'])
					{
						$resort_id = $hotel['resort_id'];
						$content.= "<optgroup label=\"".$hotel['resort_title']."\">";
					}
				$content.= "<option $selected value=\"".$hotel['hotel_id']."\">".$hotel['hotel_title'] ."</option>";
			}
		$content.= "</select>";
		return $content;		
	}

//list all resorts in select menu
function list_tour_types($selected_id = 0, $name = "tour_type_id", $params="")
	{
		global $language_id;
		
		$content = "<select name=\"".$name."\" $params>";
		$content.= "<option value=\"0\">".FORM_SELECT_TOUR_TYPE."</option>";
		$query = mysqli_query("SELECT * FROM tour_types tt left join tour_types_description ttd on tt.tour_type_id = ttd.tour_type_id WHERE ttd.language_id = '".$language_id."'");
		//$query = mysqli_query("SELECT * FROM tour_types");
		while($tour = mysqli_fetch_array($query))
			{
				if($tour['tour_type_id'] == $selected_id) $selected = "selected";
				else $selected = "";
				$content.= "<option $selected value=\"".$tour['tour_type_id']."\">".$tour['tour_type_title'] ."</option>";
			}
		$content.= "</select>";
		echo $content;
	}
	
//list hotel categories - 1 - 5 stars
function get_hotel_category($selected_id, $params="")
	{
		$content = "<select name=\"hotel_category\" $params><option value=\"0\" value=\"0\">".FORM_SELECT_HOTEL_CATEGORY."</option>";
		for($i=1; $i<=5; $i++)
			{
				if($selected_id == $i) $selected = "selected";
				else $selected = "";
				$content.= "<option $selected value=\"".$i."\">";
				for($j=0; $j<$i; $j++)
					$content.= "*";	
				$content.="</option>";
			}
		$content.="</select>";
		return $content;
	}
		
function list_hotel_category($selected_id, $params="")
	{
		echo get_hotel_category($selected_id, $params);
	}	

//list room types (SNG/DBL ..)
function list_room_types($selected_id=0, $params = "")
	{
		$query = mysqli_query("SELECT * FROM rooms_type ORDER by room_order ASC");
		$content = "<select name=\"room_type_id[]\"><option value=\"0\">- select room type -</option>";
		while($room_type = mysqli_fetch_array($query))
			{
				if($room_type['room_type_id'] == $selected_id) $selected = "selected";
				else $selected = "";
				$content.= "<option $selected value=\"".$room_type['room_type_id']."\">".$room_type['room_type_title']."</option>";
			}
		$content.="</select>";
		echo $content;
	}

//return array of period types for selected hotel
function get_period_types($name="period_type_id", $hotel_id, $selected_id=0, $params = "")
	{
		global $SiteID, $language_id;
		//if no period dates are defined nothing will be returned
		$sql = "SELECT * FROM periods_type pt left join periods_type_description ptd on pt.period_type_id = ptd.period_type_id WHERE ptd.language_id = '".$language_id."' AND pt.SiteID = '".$SiteID."' AND pt.hotel_id = '".$hotel_id."' ORDER by pt.period_order ASC";
		$query = mysqli_query($sql);
		$content = "<select name=\"".$name."\" ".$params."><option value=\"0\">- period - </option>";
		while($period_type = mysqli_fetch_array($query))
			{
				$squery = mysqli_query("SELECT * FROM periods WHERE period_type_id = '".$period_type['period_type_id']."' AND hotel_id = '".$hotel_id."'");
				if(mysqli_num_rows($squery) > 0 || $name=="period_type_id")
					{
						if($period_type['period_type_id'] == $selected_id) $selected = "selected";
						else $selected = "";
						$content.= "<option $selected value=\"".$period_type['period_type_id']."\">".$period_type['period_type_title']."</option>";
					}
			}
		$content.="</select>";
		return $content;
		//return $sql;
	}
	
//list period types 
function list_period_types($name="period_type_id", $hotel_id, $selected_id=0, $params = "")
	{
		echo get_period_types($name, $hotel_id, $selected_id, $params);
	}

//return hotel stars image
function get_hotel_stars($hotel_id, $params="")
	{
		
		$query = mysqli_query("SELECT * FROM hotels WHERE hotel_id = '".$hotel_id."' "); 
		$hotel = mysqli_fetch_array($query);
		if(mysqli_num_rows($query) > 0)
			{
				$stars_img = "<img src=\"".SITE_ROOT.DIR_WS_HOTELS_IMAGES.$hotel['hotel_category']."stars.gif\" alt=\"".$hotel['hotel_category']." stars category\">";
				return $stars_img;
			}
		else return NULL;

	}
	
//return hotel stars image
function print_hotel_stars($hotel_id, $params="")
	{
		echo get_hotel_stars($hotel_id, $params);
	}	
	
//list hotel status 

function list_hotel_status($selected=1, $params = "")
	{
		//0 - hidden / 1 - default / 2 - top_hotel / 3 - last_minute / 4 - seasonal_promo / 5 - stop_sale / 
		$content = "<select name=\"hotel_status\" $params><option value=\"-1\"> - select status - </option>";
		$content.= "<option value=\"1\" ".($selected == 1 ? "selected":'').">active</option>";
		$content.= "<option value=\"2\" ".($selected == 2 ? "selected":'').">top hotel / offer</option>";
		$content.= "<option value=\"3\" ".($selected == 3 ? "selected":'').">last minute</option>";
		$content.= "<option value=\"4\" ".($selected == 4 ? "selected":'').">seasonal promo</option>";
		$content.= "<option value=\"5\" ".($selected == 5 ? "selected":'').">stop sale</option>";
		$content.= "<option value=\"0\" ".($selected == 0 ? "selected":'').">inactive</option>";
		$content.= "</select>";
		echo $content;
	}

//return accommodation types select menu 
function list_accommodation_types($name = "accommodation_type", $selected_id=1, $params="")
	{
		global $hotel;
		$acc_types = $hotel->get_accommodation_types();
		$content = "<select name=\"".$name."\" $params>";
		$selected = "";
		for($i=0; $i<count($acc_types); $i++)
			{
				if($acc_types[$i]['accommodation_type_id'] == $selected_id) $selected = "selected";
				else $selected = "";
				$content.="<option value=\"".$acc_types[$i]['accommodation_type_id']."\" $selected>".$acc_types[$i]['accommodation_type_name']."</option>";
			}
		$content.="</select>";
		echo $content;
	}

//return select menu with currencies
function list_currencies($name = "currency_id", $selected_id = 0, $params = "")
	{
		global $hotel;
		$currencies = $hotel->get_currencies();
		$content = "<select name=\"".$name."\" $params>";
		for($i=0; $i<count($currencies); $i++)
			{
				$selected = "";
				if($currencies[$i]['currency_id'] == $selected_id) $selected = "selected";
				$content.= "<option value=\"".$currencies[$i]['currency_id']."\" $selected>".$currencies[$i]['currency_key'].", ".$currencies[$i]['currency_string']."</option>";
			}
		$content.= "</select>";
		echo $content;
	}	
		
//return hotel type img (sea / spa / moutine etc.)
function get_type_image($tour_type_key, $params="")
	{
		
		$img = "<img src=\"".SITE_ROOT.DIR_WS_HOTELS_IMAGES . "icon_".$tour_type_key.".jpg\" hspace=\"1\" $params>";
		return $img; 
	}

//return hotel currency as array
function get_hotel_currency($hotel_id, $return = 'currency_string')
	{
		$result = mysqli_query("SELECT * FROM currencies c, hotels h WHERE h.currency_id = c.currency_id AND h.hotel_id = '".$hotel_id."'");
		$currency = mysqli_fetch_array($result);
		return $currency[$return];
	}
	
//return room price for selected period	
function get_room_price($room_description_id, $period_type_id)
	{
		if(!isset($room_description_id) || !isset($period_type_id))
			return "NA";
		
		$result = mysqli_query("SELECT * FROM rooms_prices WHERE room_description_id = '".$room_description_id."' AND period_type_id = '".$period_type_id."'");
		if(mysqli_num_rows($result) > 0)
			{
				$price = mysqli_fetch_array($result);
				return $price['price_value'];
			}
		else return 0;
	}

//format a given price
function format_price($value, $hotel_id=0)
	{
		if($value >= 0)
			return sprintf("%.2f", $value)."&nbsp;". get_hotel_currency($hotel_id);
	}

//return MIN or MAX price	
function get_hotel_min_max_price($hotel_id, $return = "MIN")
	{
		//get all prices defined as payment type 0 / other payment types are interpreted as EXTRA payments
		$result = mysqli_query("SELECT * FROM rooms_prices rp left join periods_type pt on rp.period_type_id = pt.period_type_id, rooms_description rd, periods p WHERE pt.hotel_id = '".$hotel_id."' AND rp.room_description_id = rd.room_description_id AND rd.room_ep_type = 0 AND pt.period_type_id = p.period_type_id ORDER by rp.price_value ASC");
		//$result = mysqli_query("SELECT * FROM rooms r left join rooms_description rd on r.room_id = rd.room_id, rooms_prices rp WHERE r.hotel_id = '".$hotel_id."' AND rd.room_description_id = rp.room_description_id ORDER by rp.price_value ASC");
		while($price = mysqli_fetch_array($result))
			$hotel_prices[] = $price['price_value'];
		switch($return)
			{
				case "MAX" : return $hotel_prices[(count($hotel_prices)-1)];
				case "MIN" : 
				default: return $hotel_prices[0];
				
			}	
	}

//search for hotel price in selected borders
function search_hotel_price($hotel_id, $price_from = 0, $price_to = 10000)		
	{
		$result = mysqli_query("SELECT * FROM rooms r left join rooms_description rd on r.room_id = rd.room_id, rooms_prices rp WHERE r.hotel_id = '".$hotel_id."' AND rd.room_description_id = rp.room_description_id AND rp.price_value >= '".$price_from."' AND rp.price_value <= '".$price_to."' ORDER by rp.price_value ASC");
		if(mysqli_num_rows($result) > 0) return true;
		else return false;
	}
	
//return all prices in defined in selected period_type
function get_period_prices($period_type_id)
	{
		global $hotel_SiteID;
		$result = mysqli_query("SELECT * FROM rooms_prices WHERE period_type_id = '".$period_type_id."'");
		return mysqli_num_rows($result); 
	}


//return all rooms defined for selected hotel
function list_hotel_rooms($hotel_id, $name="room_description_id[]", $selected = 0, $params="")
	{
		$query = mysqli_query("SELECT * FROM rooms_description rd, rooms r WHERE rd.room_id = r.room_id AND r.hotel_id = '".$hotel_id."'");
		$content = "<select name=\"".$name."\" $params><option value=\"0\">- select room -</option>";
		while($room = mysqli_fetch_array($query))
			$content.= "<option value=\"".$room['room_description_id']."\">".$room['room_description']."</option>";
		$content.="</select>";
		print $content;
	}

//return extra payment types
function list_ep_types($name = "ep_type_id", $selected = "0", $params="")
	{
		$content = "<select name=\"".$name."\" $params><option value=0> - select type - </option>";
		$query = mysqli_query("SELECT * FROM ep_types");
		while($ep = mysqli_fetch_array($query))
			{
				if($selected == $ep['ep_type_id']) $select = "selected";
				else $select = "";
				$content.= "<option value=\"".$ep['ep_type_id']."\" ".$select.">".$ep['ep_type_description']."</option>";
			}
		$content.="</select>";
		print $content;
	}

//check hotel dates availability	
function check_hotel_date_availability($hotel_id, $date_from, $date_to)
	{

		//get start / end hotel dates
		$result = mysqli_query("SELECT MIN(p.date_from) as start_date, MAX(p.date_to) as end_date FROM periods_type pt left join periods p on pt.period_type_id = p.period_type_id WHERE pt.hotel_id = '".$hotel_id."' ORDER by p.date_from ASC"); 
		$min_max = mysqli_fetch_array($result);
		$start_date = $min_max['start_date'];
		$end_date = $min_max['end_date'];
		if($date_from >= $start_date && $date_to <= $end_date)
			return true;
		else return false;
	}
	
//return date difference Y-years, M-months, D-days (default)
function get_date_difference($date_from, $date_to, $return="D")
	{
		$date_from = strtotime($date_from);
		$date_to = strtotime($date_to);
		$difference = $date_to - $date_from;
		switch($return)
			{
				case "Y": { $difference = $difference / (60*60*24*365); break;}
				case "M": { $difference = $difference / (60*60*24*30); break;}
				case "D": 
				default	: { $difference = $difference / (60*60*24); break;}
			}
		return $difference;
	}


//return period(s) by selected date_from / date_to. If dates are in 2 periods return array
function check_period_dates($hotel_id, $date_from, $date_to)
	{
		//$sql = "SELECT * FROM periods_type pt left join periods p on pt.period_type_id = p.period_type_id WHERE pt.hotel_id = '".$hotel_id."' AND ((p.date_from <= '".$date_from."' AND '".$date_from."' <= p.date_to) OR (p.date_from <= '".$date_to."' AND '".$date_to."' <= p.date_to )) ORDER by p.date_from ASC";
		//get start / end hotel dates
		$result = mysqli_query("SELECT MIN(p.date_from) as start_date, MAX(p.date_to) as end_date FROM periods_type pt left join periods p on pt.period_type_id = p.period_type_id WHERE pt.hotel_id = '".$hotel_id."' ORDER by p.date_from ASC"); 
		$min_max = mysqli_fetch_array($result);
		$start_date = $min_max['start_date'];
		$end_date = $min_max['end_date'];

		if($date_from >= $start_date && $date_to <= $end_date)
			{
				//$sql = "SELECT * FROM periods_type pt left join periods p on pt.period_type_id = p.period_type_id WHERE pt.hotel_id = '".$hotel_id."' ORDER by p.date_from ASC";
				$sql = "SELECT * FROM periods_type pt left join periods p on pt.period_type_id = p.period_type_id WHERE pt.hotel_id = '".$hotel_id."' AND ((p.date_from <= '".$date_from."' AND '".$date_from."' <= p.date_to) OR (p.date_from <= '".$date_to."' AND '".$date_to."' <= p.date_to ) OR (p.date_from > '".$date_from."' AND '".$date_to."' > p.date_to )) ORDER by p.date_from ASC";
				$result = mysqli_query($sql);
				//store noghts count for each period
				$nights_array = array();
				$nights = 0;
				while($period = mysqli_fetch_array($result))
					{
						
						if($date_from >= $period['date_from'] && $date_from <= $period['date_to'] && $date_to >= $period['date_from'] && $date_to <= $period['date_to'])
							{
								$nights = strtotime($date_to) - strtotime($date_from);
								//$nights_array[$period['period_type_id']] = $nights/(60*60*24);			
							}
						else
						if($date_from < $period['date_from'] && $date_to > $period['date_to'])
							{
								$nights = strtotime($period['date_to']) - strtotime($period['date_from']);
								//$nights_array[$period['period_type_id']] = $nights/(60*60*24);
							}
						else
						if($date_from >= $period['date_from'] && $date_from <= $period['date_to'])
							{
								$nights = strtotime($period['date_to']) - strtotime($date_from);
								//$nights_array[$period['period_type_id']] = $nights/(60*60*24);
							}
						else	
						if($date_to >= $period['date_from'] && $date_to <= $period['date_to'])
							{
								$nights = strtotime($date_to) - strtotime($period['date_from']);
								//$nights_array[$period['period_type_id']] = $nights/(60*60*24);
							}

											
						$period['nights'] = (int)($nights/(60*60*24));	
						$periods[] = $period;
					}
				//return $nights_array;
				return $periods;
			}
		else return NULL;
	}

//calculate	reservation price
function calculate_reservation_price($hotel_id, $room_description_id, $date_from, $date_to, $people=1, $kids=0)
	{
		//get periods containing selected dates
		$periods = check_period_dates($hotel_id, $date_from, $date_to);
		$price = 0;
		for($i=0; $i<count($periods); $i++)
			$price += $periods[$i]['nights'] * get_room_price($room_description_id, $periods[$i]['period_type_id']) * $people;
			
		return $price;
	}	
?>