<?
include "hotels/editor/simple.php";
//edit hotel
$edit_id = $_GET['edit_id'];
if(isset($edit_id))
	{
		$resort_info = $hotel->get_resort($edit_id, $language_id);
		$form_fields = "<input type=\"hidden\" name=\"action\" value=\"edit_resort_confirm\"><input type=\"hidden\" name=\"edit_id\" value=\"".$edit_id."\"><img src=\"".DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES."button_update.jpg\" border=0 onClick=\"document.resorts_add.submit()\">";
		$hotel_info = $hotel->get_hotel($edit_id, $language_id);	
	}
else $form_fields = "<input type=\"hidden\" name=\"action\" value=\"add_resort_confirm\"><img src=\"".DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES."button_save.jpg\" border=0 onClick=\"document.resorts_add.submit()\">";
?>
<form action="<?=tep_href_link("action=list_resorts")?>" method="post" name="resorts_add" target="_self" enctype="application/x-www-form-urlencoded">
<!--<form action="form_test_vars.php" method="post" name="resorts_add" target="_self" >//-->

<table class="border_table" cellpadding="5" width="100%">
<tr><td>short name:
  <tr><td><input type="text" name="resort_name" size="50" value="<?=$resort_info['resort_name']?>">
<tr><td>
<fieldset><legend>title:</legend>
<table cellpadding="5">
<?php
		for($i=0; $i<count($languages); $i++)
			{
				$lang_id = $languages[$i]['language_id'];
				$resort_title = $hotel->get_rInfo($edit_id, "resort_title", $lang_id);
				$resort_description = $hotel->get_rInfo($edit_id, "resort_description", $lang_id);
				$language_image = "<img src=\"".DIR_WS_HOTELS_LANGUAGES . $languages[$i]['language_dir']."/images/".$languages[$i]['language_flag']."\">";
?>
<tr><td><?=$language_image?><td><textarea name="resort_title[<?=$lang_id?>]" cols="50" id="text_editor<?=$i?>" rows="1"><?=$resort_title?></textarea>
<?
			}
?>
</table>
</fieldset>
<tr><td>
<?=$form_fields?>
</table>
</form>