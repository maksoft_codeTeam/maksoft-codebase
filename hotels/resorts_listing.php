<?php
	include_once DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir'] . "/resorts_listing.php";
?>
<br>
<table class="border_table" width="100%" cellpadding="5">
<tr><th colspan="2"><?=HEADING_TITLE?>
<tr><th><?=LABEL_TITLE?><th width="100">
<?php

//get all hotels for selected SiteID
$resorts_array = $hotel->get_resorts();
//list resorts
for($i=0; $i<count($resorts_array); $i++)
{
	?>
		<tr><td valign="top"><a href="<?=tep_href_link("action=list_hotels&resort_id=".$resorts_array[$i]['resort_id'])?>"><?=$resorts_array[$i]['resort_title']?></a>
				<td width="100">
				<a href="<?=tep_href_link("action=edit_resort&edit_id=".$resorts_array[$i]['resort_id'])?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_edit.jpg" border=0></a>
	<?php
}
?>
</table>
<a href="<?=tep_href_link("action=add_resort")?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_add.jpg" border=0></a>