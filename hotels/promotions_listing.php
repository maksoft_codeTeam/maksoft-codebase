<?php
	include_once DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir'] . "/promotions_listing.php";
	if($action_message != "")
		mk_output_message("normal", $action_message);
?>
<br>
<table class="border_table" width="100%" cellpadding="5">
<tr><th colspan="3"><?=HEADING_TITLE?>
<tr>
	<td>
	<td>
	<form method="get">
	<input type="hidden" name="n" value="<?=$n?>">
	<input type="hidden" name="SiteID" value="<?=$SiteID?>">
	<input type="hidden" name="action" value="<?=$action?>">
	<?=list_hotels($hotel_id, "onChange=\"this.form.submit()\"")?>
	</form>
	<td>
</tr>
<?php
$promotions_array = $hotel->get_hotel_promotions($hotel_id);
if(count($promotions_array)>0)
	{
		if($hotel_id >0 )
		{
		?>
		<form name="promotions_reorder" action="<?=tep_href_link("action=list_promotions&amp;hotel_id=".$hotel_id)?>" method="post">
		<input type="hidden" name="action" value="promotions_order">
		<?
		}
		
		for($i=0; $i<count($promotions_array); $i++)
			{
					if($hotel_id >0 )
						$counter = "<input type=\"text\" size=\"3\" value=\"".$promotions_array[$i]['sort_n']."\" name=\"promotions_order[".$promotions_array[$i]['promo_id']."]\">";
					else $counter = "<b>".($i+1).".</b>";
				?>
					<tr>
						<td align="right" width="20"><?=$counter?>	
						<td valign="top"><a href="<?=tep_href_link("action=edit_promotion&edit_id=".$promotions_array[$i]['promo_id'])?>"><?=$promotions_array[$i]['promo_title']?></a>
						<td width="120">							
						<a href="<?=tep_href_link("action=edit_promotion&edit_id=".$promotions_array[$i]['promo_id'])?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_edit.jpg" border=0></a>
						<a href="#" onClick="if(confirm('Are you sure?')) document.location = '<?=tep_href_link("action=delete_promotion&del_id=".$promotions_array[$i]['promo_id'])?>';"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_delete.jpg" border=0></a>
				<?php
			}
			if($hotel_id >0)
				echo "<tr><td><a href=\"#\" onClick=\"document.promotions_reorder.submit()\">reorder</a><td><td>";
			if($hotel_id >0 )
			{
				?>
				</form>
				<?
			}
	}
else 
	{
		?>
		<tr><td colspan="2">
		<?php
			mk_output_message("warning", "No promotions found !");
	}
?>
</table>
<a href="<?=tep_href_link("action=add_promotion")?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_add.jpg" border=0></a>