<br>
<?php
	if(isset($action_message))
		mk_output_message("normal", $action_message);
		
	if(isset($hotel_id))
		{
			
			$extra_payments = $hotel->get_extra_payments($hotel_id);
			//print_r($extra_payments);
			?>
            <form name="add_ep" method="post" action="page.php?n=<?=$n?>&amp;SiteID=<?=$SiteID?>&amp;action=list_ep&amp;hotel_id=<?=$hotel_id?>">
			<br />
			<table class="hotel_prices" cellpadding="5" width="100%">
				<tr><th colspan=7>Extra Payments / <?=$hotel->get_hName($hotel_id, $language_id)?></th>
                <tr><td class="header">#<td class="header">period<td class="header">room type<td class="header">acc. type<td class="header">payment type<td class="header">price<td class="header">&nbsp;
				<?php
                	if(count($extra_payments) == 0)
						{
							echo "<tr><td colspan=\"7\">";
							mk_output_message("warning", "No payments found !");
						}
					else
						{
							for($i=0; $i<count($extra_payments); $i++)
								echo "<tr><td>".($i+1)."<td class=\"title\">".$extra_payments[$i]['period_type_title'] ."<td class=\"title\">".  $extra_payments[$i]['room_description'] ."<td class=\"title\">". $extra_payments[$i]['accommodation_type_string'] . "<td class=\"title\">".$extra_payments[$i]['ep_type_description'] ."<td class=\"price\">".format_price($extra_payments[$i]['ep_price'], $hotel_id)."<td><a href=\"#\" tiltle=\"Delete price\" onClick=\"if(confirm('Are you sure?')) document.location = '".tep_href_link("action=delete_ep&amp;del_id=".$extra_payments[$i]['ep_id'] ."&amp;hotel_id=".$hotel_id)."'\" ><img src=\"".DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES."button_delete_small.gif\" alt=\"Delete price?\" border=\"0\" hspace=\"3\" align=\"middle\"></a>";
						}
				?>
			</table>
            <br /><br />
            <table cellpadding="5" cellspacing="2" class="hotel_prices" width="100%">
            	<tr><th colspan="6">+ add extra payment
                <tr><td>period<td>room type<td>acc. type<td>payment type<td>price<td>&nbsp;
                <tr><td><?=list_period_types("period_type_id", $hotel_id, 0, "style='width: 80px'")?><td><?=list_hotel_rooms($hotel_id, "room_description_id", 0, "style='width: 200px'")?><td><?=list_accommodation_types("accommodation_type_id", 0, "style='width: 50px'")?><td><?=list_ep_types("ep_type_id", 0, "style='width: 150px'")?><td><input size="5" type="text" name="ep_price" value="0.00"><td><a href="#" onclick="document.add_ep.submit()"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_add.jpg" border=0></a>
            </table>
            <input type="hidden" name="action" value="add_extra_payment">
            <input type="hidden" name="hotel_id" value="<?=$hotel_id?>">
            </form>
			<?php
		}
	else
		{
?>
		<form method="get">
		<input type="hidden" name="n" value="<?=$n?>">
		<input type="hidden" name="SiteID" value="<?=$SiteID?>">
		<input type="hidden" name="action" value="<?=$action?>">
		<table class="border_table" cellpadding="5" width="100%"><tr><th colspan=2>Extra Payments<tr><td>#<td><?=list_resorts($resort_id, "onChange=\"this.form.submit()\"")?>
		<?php
		if($resort_id > 0)
			$hotels_array = $hotel->get_hotels($SiteID, "1 AND r.resort_id = '".$resort_id."' ORDER by hd.hotel_title ASC");
		else
			$hotels_array = $hotel->get_hotels($SiteID, "1 ORDER by hd.hotel_title ASC, rd.resort_title ASC");
			
		if(count($hotels_array) == 0)
			{
			?>
				<tr><td colspan="4"><?=mk_output_message("warning", "No hotels found !")?>
			<?php
			}
		for($i=0; $i<count($hotels_array); $i++)
			{
			?>
					<tr><td width="30" align="center">#<?=$i+1?><td><a href="<?=tep_href_link("action=list_ep&amp;hotel_id=".$hotels_array[$i]['hotel_id'])?>"><?=$hotels_array[$i]['hotel_title']?></a>&nbsp;(<em><?=$hotels_array[$i]['resort_title']?></em>)
			<?php
			}
		?>
		</table>
		</form>
<?php
		}
?>