<script type="text/javascript" src="http://www.maksoft.net/hotels/includes/jquery.min.js"></script>
<script type="text/javascript" src="http://www.maksoft.net/hotels/includes/photo_gallery.js"></script>
<link rel="stylesheet" href="lib/lightbox/lightbox.css" type="text/css" media="screen">

<style type="text/css">

.stepcarousel{
position: relative; /*leave this value alone*/
border: 1px solid #f0f0f0;
overflow: scroll; /*leave this value alone*/
width: 425px; /*Width of Carousel Viewer itself*/
height: 215px; /*Height should enough to fit largest content's height*/
}

.stepcarousel .belt{
position: absolute; /*leave this value alone*/
left: 0;
top: 0;
}

.stepcarousel .panel{
float: left; /*leave this value alone*/
overflow: hidden; /*clip content that go outside dimensions of holding panel DIV*/
margin: 5px; /*margin around each panel*/
background-color: #fff;
height: 200px;
text-align: center;
width: 100px; /*Width of each panel holding each content. If removed, widths should be individually defined on each content DIV then. */
}

.panel a.border_image {margin: 1px; display: block; width: 100px; height: 100px; background-color: #f0f0f0; padding: 0px; border: none; background-position: 50% 50%; background-repeat: no-repeat;}
.panel a.border_image img {border: none;}
</style>



<script type="text/javascript">
stepcarousel.setup({
	galleryid: 'mygallery', //id of carousel DIV
	beltclass: 'belt', //class of inner "belt" DIV containing all the panel DIVs
	panelclass: 'panel', //class of panel DIVs each holding content
	autostep: {enable:true, moveby:1, pause:3000},
	panelbehavior: {speed:1500, wraparound:true, persist:true},
	defaultbuttons: {enable: false, moveby: 4, leftnav: ['http://i34.tinypic.com/317e0s5.gif', -5, 80], rightnav: ['http://i38.tinypic.com/33o7di8.gif', -20, 80]},
	statusvars: ['statusA', 'statusB', 'statusC'], //register 3 variables that contain current panel (start), current panel (last), and total panels
	contenttype: ['inline'] //content setting ['inline'] or ['ajax', 'path_to_external_file']
})
</script>	
<?php

//default domain
$default_domain_url = "http://www.maksoft.net";

//domain url
if(!isset($domain_url) || $domain_url =='')
	$domain_url = $default_domain_url;

//images directory
if(!isset($photo_dir) || empty($photo_dir) || $photo_dir=='')
	$photo_dir = 'img';
	
//image width
if(!isset($img_width))
	$img_width = 90;

//check for directory existance
if(!is_dir($photo_dir)) mk_output_message("error",'The image directory does not exist: <b>'.$domain_url.'/'.$photo_dir.'</b>');
$DIR = @opendir($photo_dir) or mk_output_message("error", 'can\'t open image directory: <b>'.$domain_url.'/'.$photo_dir.'</b>');

//image thumbnails array
$thumbs = array('');
while($thumb = readdir($DIR))
        {
        if(eregi('jpg|jpeg|gif|tif|bmp|png|avi', $thumb))
                {
                 array_push($thumbs, str_replace(' ','%20',$thumb));
                }
        }

sort($thumbs);
$gallery_content = "<div id=\"mygallery\" class=\"stepcarousel\"><div class=\"belt\"><div class=\"panel\">";
$j= 1;
for($i = 1; $i < sizeof($thumbs); $i++)
	{
		if($j <= 2)
			{
				$gallery_content.= "<a href='".$domain_url."/".$photo_dir.$thumbs[$i]."' rel='lightbox[hotel_gallery]' title=\"".$hotels_array[0]['hotel_title']."\" class=\"border_image\" style=\"background-image: url(".$domain_url."/img_preview.php?image_file=".$photo_dir.$thumbs[$i]."&img_width=".$img_width.")\"></a>";
				$j++;
			}	
		else
			{
				$gallery_content.="</div><div class=\"panel\">";
				$j=1;
			}

		
	}
if($j<4) $gallery_content.="</div>";
$gallery_content.= "</div></div>";
echo $gallery_content;
?>
<table width="425" border="0" cellpadding="0" cellspacing="5"><tr>
<td width="50" align="left"><a href="javascript:stepcarousel.stepBy('mygallery', -4)" class="next_link">&laquo; back</a>
<td id="mygallery-paginate" style="width: 325; text-align:center">
<img src="hotels/images/opencircle.png" data-over="hotels/images/graycircle.png" data-select="hotels/images/closedcircle.png" data-moveby="1" />
<td width="50" align="right"><a href="javascript:stepcarousel.stepBy('mygallery', 4)" class="next_link">next &raquo;</a>
</table>
