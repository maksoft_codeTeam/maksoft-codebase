<script language="JavaScript">
$j(document).ready(function(){

		var now = new Date();
		$j('#calendar2').hide();

		if($j('#date_to').val())
			$j('#calendar2').show();
			
		//calendar picker DATE_FROM	
		$j('#calendar1').DatePicker({
			format:'Y-m-d',
			date: $j("[name='today']").val(),
			current: $j("[name='today']").val(),
			starts: 1,	
			position: 'bottom',
			onRender: function(date) {
				return {
					disabled: (date.valueOf() < now.valueOf()),
					className: date.valueOf() == now.valueOf() ? 'datepickerSpecial' : false
				}
			},			
			onChange: function(formated, dates){
				$j('#date_from').val(formated);
				$j('#calendar1').DatePickerHide();
				$j('#calendar2').show();
			}
		});


		//calendar picker DATE_TO
		$j('#calendar2').DatePicker({
			format:'Y-m-d',
			date: $j('#date_from').val(),
			current: $j('#date_from').val(),
			starts: 1,	
			position: 'bottom',
			onBeforeShow: function(){
				$j('#calendar2').DatePickerSetDate($j('#date_from').val(), true);
			},
			onRender: function(date) {
				return {
					disabled: (date.valueOf() < now.valueOf()),
					className: date.valueOf() == now.valueOf() ? 'datepickerSpecial' : false
				}
			},

			onChange: function(formated, dates){
				$j('#date_to').val(formated);
				$j('#calendar2').DatePickerHide();			
				//unfocus_object('#date_leave');
			}

		});

		$j('#button_save').click(function(){
			hotel_id = $j("[name='hotel_id']").val();
			promo_title = $j("[name='promo_title[1]']").val();
			//alert($j('#promo_title').length);
			
			message = "";
			if(hotel_id == 0)
				//alert("Select Hotel !");
				message = message + "Select Hotel\n";
			
			if(promo_title == "")
				message = message + "Enter at least one promotion title !";
			
			if(hotel_id != 0 && promo_title != "")
				document.promo_add.submit();
			else
				alert(message);
			
		})
});
</script>
<?php
	include "hotels/editor/default.php";
	if(isset($edit_id)) $promotion = $hotel->get_promotion($edit_id, $language_id);
	//echo "<pre>";
	//print_r($promotion);
	//echo "</pre>";
	
?>

<form method="post" name="promo_add" target="_self" action="<?=tep_href_link("action=list_promotions")?>">
<table class="border_table" cellpadding="5" width="100%">
<tr><th colspan="3"><?=list_hotels($promotion['hotel_id'])?>
<tr><td colspan="2">
	<fieldset><legend>promotion title</legend>
	<table cellpadding="1" id="new_period">
	<?php
			for($i=0; $i<count($languages); $i++)
				{
					$lang_id = $languages[$i]['language_id'];
					if(isset($edit_id)) $promotion_title = $hotel->get_promotion($edit_id, $lang_id);
					
					$language_image = "<img src=\"".DIR_WS_HOTELS_LANGUAGES . $languages[$i]['language_dir']."/images/".$languages[$i]['language_flag']."\">";
	?>
	<tr><td><?=$language_image?><td><input type="text" name="promo_title[<?=$lang_id?>]" size="50" value="<?=$promotion_title['promo_title']?>" id="promo_title">
	<?
				}
	?>
	</table>
	</fieldset>
<tr><td colspan="2">
	<fieldset><legend>promo description</legend>
	<table cellpadding="1">
	<?php
			for($i=0; $i<count($languages); $i++)
				{
					$lang_id = $languages[$i]['language_id'];
					if(isset($edit_id)) $promo_description = $hotel->get_promotion($edit_id, $lang_id);
					$language_image = "<img src=\"".DIR_WS_HOTELS_LANGUAGES . $languages[$i]['language_dir']."/images/".$languages[$i]['language_flag']."\">";
	?>
	<tr><td><?=$language_image?><td><textarea name="promo_description[<?=$lang_id?>]" id="text_editor<?=$i?>"><?=$promo_description['promo_description']?></textarea>
	<?
				}
	?>
	</table>
	</fieldset>
<tr>
<td>start date
<td>end date
<tr><td><input name="date_from" type="text" id="date_from" size="20" value="<?=$promotion['promo_date_from']?>"><a href="#" class="calendar" id="calendar1" title="<?=FORM_LABEL_SLECT_ARRIVE_DATE?>"><img src="<?=DIR_WS_HOTELS_IMAGES?>icon_calendar.jpg" border="0"></a>
  <td><input name="date_to" type="text" id="date_to" size="20" value="<?=$promotion['promo_date_to']?>">
    <a href="#" class="calendar" id="calendar2" title="<?=FORM_LABEL_SLECT_LEAVE_DATE?>"><img src="<?=DIR_WS_HOTELS_IMAGES?>icon_calendar.jpg" border="0"></a>  
<tr>
  <td>sort number:  
  <td>status  
<tr>
  <td><input name="sort_n" type="text" id="sort_n" value="<?=$promotion['sort_n']?>" size="10">  
  <td><input name="promo_status" type="text" size="10" value="<?=$promotion['promo_status']?>">
    <input type="hidden" name="edit_id" value="<?=$edit_id?>">
    <input type="hidden" name="action" value="add_promo_confirm">
    <input type="hidden" name="today" value="<?=date("Y-m-d")?>">  
<tr align="center">
  <td colspan="2">
  <?php
  	echo "<img src=\"".DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES."button_save.jpg\" border=0 id=\"button_save\">";
  ?>  
<tr>
  <td colspan="2">&nbsp;
  	</table>
</form>