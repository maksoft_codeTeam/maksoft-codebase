<?php
	include_once DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir'] . "/hotels_listing.php";
	//get the first resort - Albena
	if(!isset($resort_id)) $resort_id = 18;

?>
<table class="border_table" cellpadding="5">
<tr><th colspan="2"><?=HEADING_TITLE?>
<tr>
<form method="get">
<th align="right">
<input type="hidden" name="n" value="<?=$n?>">
<input type="hidden" name="SiteID" value="<?=$SiteID?>">
<input type="hidden" name="action" value="list_hotels">
<?=list_resorts($resort_id, "onChange=\"this.form.submit()\"")?>
<th>
<?=list_tour_types($tour_type_id, "tour_type_id", "onChange=\"this.form.submit()\"")?>
<input type="text" name="search_name" size="25" value="<?=$search_name?>"><input type="submit" value="����� �� ���">
</form>
<?php

$hotels_add_sql = "1";

if(isset($resort_id) && $resort_id != 0)
	$hotels_add_sql.=" AND h.resort_id = '".$resort_id."'";

if(isset($tour_type_id) && $tour_type_id != 0)
	$hotels_add_sql.=" AND tt.tour_type_id = '".$tour_type_id."'";
	
if($search_name != "")
	$hotels_add_sql.=" AND hd.hotel_title LIKE '%".$search_name."%'";
	
//get all hotels for selected SiteID
/*
if(isset($resort_id) && $resort_id != 0)
	$hotels_array = $hotel->get_hotels($SiteID, "h.resort_id = '".$resort_id."' AND '".$hotels_add_sql."' ORDER by hd.hotel_title, rd.resort_title ASC");
else if(isset($tour_type_id) && $tour_type_id != 0)
	$hotels_array = $hotel->get_hotels($SiteID, "tt.tour_type_id = '".$tour_type_id."'  AND '".$hotels_add_sql."' ORDER by hd.hotel_title, rd.resort_title ASC");
else 
*/
	$hotels_array = $hotel->get_hotels($SiteID, $hotels_add_sql." ORDER by hd.hotel_title, rd.resort_title ASC");
	
//list hotels
for($i=0; $i<count($hotels_array); $i++)
{
	if(is_file(DIR_WS_HOTELS_UPLOAD . $hotels_array[$i]['hotel_image']))
		$hotel_image = DIR_WS_HOTELS_UPLOAD . $hotels_array[$i]['hotel_image'];
	else $hotel_image = DIR_WS_HOTELS_IMAGES . "no_photo.jpg";
	
	$count_hotel_rooms = count($hotel->get_rooms($hotels_array[$i]['hotel_id']));
?>
<tr>
	<td width="170" valign="top" align="center">
	<!--<a href="<?=DIR_WS_HOTELS_UPLOAD . $hotels_array[$i]['hotel_image']?>" rel="lightbox[]" ><img src="img_preview.php?image_file=<?=$hotel_image?>&img_width=150" border="0" class="border_image"></a> //-->
	<div style="display: block; background-color: #F00; color: #FFF; text-align: center; position: relative; width: 30px; height: 30px; line-height: 30px; font-size: 12px; margin-bottom: -30px; margin-left: 10px;"><?=$i+1?>&nbsp;</div>
	<a href="<?=tep_href_link("action=hotel_info&hotel_id=".$hotels_array[$i]['hotel_id'])?>" title="<?=$hotels_array[$i]['hotel_title']?>"><img src="img_preview.php?image_file=<?=$hotel_image?>&img_width=150" border="0" class="border_image" alt="<?=$hotels_array[$i]['hotel_title']?>"></a>
	<td valign="top">
			<div class="head_text">
			<a href="<?=tep_href_link("action=hotel_info&hotel_id=".$hotels_array[$i]['hotel_id'])?>"><?=$hotels_array[$i]['hotel_title']?></a>, <?=$hotels_array[$i]['hotel_category']?>*</div>
			<em><?=$hotels_array[$i]['resort_title']?></em><br>
			<em>
			<?php
				$hotel_types = $hotel->get_types($hotels_array[$i]['hotel_id']);
				for($j=0; $j<count($hotel_types); $j++)
					echo $hotel_types[$j]['tour_type_title']." / "
			?>
			</em>			
			<br><br>
			<?=strip_tags(crop_text($hotels_array[$i]['hotel_description']), "")?><br>
			<br>
			rooms: <?=$count_hotel_rooms?>
			<hr>
			<a href="<?=tep_href_link("action=edit_hotel&edit_id=".$hotels_array[$i]['hotel_id'])?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_edit.jpg" border=0></a>
			<a href="javascript: void(0)" onClick="if(confirm('Are you sure?')) document.location='<?php echo tep_href_link("action=delete_hotel&del_id=".$hotels_array[$i]['hotel_id']);?>' "><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_delete.jpg" border=0></a>
			<a href="<?=tep_href_link("action=add_prices&hotel_id=".$hotels_array[$i]['hotel_id'])?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_add_price.jpg" border=0></a>
			<a href="<?=tep_href_link("action=list_reservations&hotel_id=".$hotels_array[$i]['hotel_id'])?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_reserve.jpg" border=0></a>
	</td>
<?php
}
?>
</table>
<a href="<?=tep_href_link("action=add_hotel")?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_add.jpg" border=0></a>