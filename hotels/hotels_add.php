<?php
//include "text_editor/hotels.php";
include "hotels/editor/advanced.php";
//edit hotel
$edit_id = $_GET['edit_id'];
if(isset($edit_id))
	{
		$form_fields = "<input type=\"hidden\" name=\"action\" value=\"edit_hotel_confirm\"><input type=\"hidden\" name=\"edit_id\" value=\"".$edit_id."\"><img src=\"".DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES."button_update.jpg\" border=0 onClick=\"document.hotels_add.submit()\">";
		$hotel_info = $hotel->get_hotel($edit_id, $language_id);
		
		if(is_file(DIR_WS_HOTELS_UPLOAD . $hotel_info['hotel_image']))
			$hotel_image = DIR_WS_HOTELS_UPLOAD . $hotel_info['hotel_image'];
		else $hotel_image = DIR_WS_HOTELS_IMAGES . "no_photo.jpg";
		?>
		<script language="JavaScript" type="text/javascript">
			//alert("Edit mode !");
		$j(document).ready(function(){
			
			var cl = $j("#count_languages").val();

			$j("#info_box1").hide();
			$j("#info_box2").hide();
			$j("#info_box3").hide();
			$j("#info_box4").hide();
			
			$j("#flag1").click(function(){
				$j("#info_box1").toggle();
			});
			$j("#flag2").click(function(){
				$j("#info_box2").toggle();
			});
			$j("#flag3").click(function(){
				$j("#info_box3").toggle();
			});
			$j("#flag4").click(function(){
				$j("#info_box4").toggle();
			});						
			/*
			for(i=1; i<=cl; i++)
				{
					$j("#info_box"+i).hide();
					
					$j("#flag"+i).click(function(){
						$j("#info_box"+i).toggle();
					});

				}
			*/
			});
		</script>
		<?php
		
		//set description table style
		$style = "display: none;";	
			
	}
else $form_fields = "<input type=\"hidden\" name=\"action\" value=\"add_hotel_confirm\"><img src=\"".DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES."button_save.jpg\" border=0 onClick=\"document.hotels_add.submit()\">";

$view_extra_description = false;

if($hotel_info['hotel_status'] !=1)
	$view_extra_description = false;

?>
<form action="<?=tep_href_link("action=list_hotels")?>" method="post" enctype="multipart/form-data" name="hotels_add" target="_self">
<table width="100%" class="border_table" cellpadding="5"><tr><th colspan="2">Hotel add/edit</th>
<tr>
	<td colspan="2">
	<?php
		for($i=0; $i<count($languages); $i++)
			{
				$lang_id = $languages[$i]['language_id'];
				$hotel_title = $hotel->get_hInfo($edit_id, "hotel_title", $lang_id);
				$hotel_description = $hotel->get_hInfo($edit_id, "hotel_description", $lang_id);
				$hotel_conditions = $hotel->get_hInfo($edit_id, "hotel_conditions", $lang_id);
				$hotel_description_extra = $hotel->get_hInfo($edit_id, "hotel_description_extra", $lang_id);
				$language_image = "<img src=\"".DIR_WS_HOTELS_LANGUAGES . $languages[$i]['language_dir']."/images/".$languages[$i]['language_flag']."\" id=\"flag".$lang_id."\">";
			
			?>
			<fieldset><legend><?=$language_image?></legend><em>click the flag to open the content</em>
			<table cellpadding="10" id="info_box<?=$lang_id?>" style="<?=$style?>">
				<tr><td>title:<br><input type="text" name="hotel_title[<?=$languages[$i]['language_id']?>]" value="<?=$hotel_title?>" size="64" maxlength="64"><br>
				<?php
					
					if($view_extra_description)
					{
						?>
							<tr><td>extra description:<br><textarea id="title_editor<?=$i?>" name="hotel_description_extra[<?=$languages[$i]['language_id']?>]" cols="50" rows="10"><?=$hotel_description_extra?></textarea> 
						<?php
					}
				?>
				<tr><td>description:<br><textarea id="text_editor<?=$i?>" name="hotel_description[<?=$languages[$i]['language_id']?>]" rows="10" cols="50"><?=$hotel_description?></textarea>
			</table>
			</fieldset>
			<?php
			}
	?>
		<br>
		<tr><td>image (<em><?=$hotel_image?></em>)
		  <td>infant age:          
		<tr><td><input type="file" name="hotel_image" size="24"><br>
		  <td><input type="text" name="hotel_infant_age" value="<?=$hotel_info['hotel_infant_age']>0 ? $hotel_info['hotel_infant_age']:2?>">          
		<tr>
		  <td>category:
		  <td>child age:          
		<tr><td><?=list_hotel_category($hotel_info['hotel_category'])?>		  
		  <td><input type="text" name="hotel_child_age" value="<?=$hotel_info['hotel_child_age']>0 ? $hotel_info['hotel_child_age']:12?>">          
		<tr>
		<?php
			if($hotel_info['hotel_id'] > 0)
				{
					$hotel_types = $hotel->get_types($hotel_info['hotel_id']);
					$count_hotel_types = count($hotel_types);
				}
			else $count_hotel_types = 1;
		?>
		  <td>hotel type: <img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_add.jpg" onClick="duplicateRow('hotel_types', 0)"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_delete.jpg" onClick="deleteRow('hotel_types', 1)">
		  <td>resort:
		<tr><td>
			<table id="hotel_types">
			<?php
				//print_r ($hotel_types); 
				if($count_hotel_types == 0 || $action == "add_hotel")
					{
						?>
						<tr><td><?=list_tour_types(1, "tour_type_id[]")?>
						<?php
					}
				else
				for($i=0; $i<count($hotel_types); $i++)
					{
				?>	
					<tr><td><?=list_tour_types($hotel_types[$i]['tour_type_id'], "tour_type_id[]")?>
				<?php
					}
			?>
			</table>		  	
		  	<td>
			<?php
				list_resorts($hotel_info['resort_id'], "", true);	
			?>
		<tr><td>hotel status:<br><?=list_hotel_status($hotel_info['hotel_status'])?>	
      	  <td>default currency:<br><?=list_currencies("currency_id", $hotel_info['currency_id'])?>      
		<tr><td colspan="2">
		 <?php
		 	$hotel_rooms = $hotel->get_rooms($hotel_info['hotel_id']);
		 ?>
		 <img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_add.jpg" onClick="duplicateRow('hotel_rooms', 1)"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_delete.jpg" onClick="deleteRow('hotel_rooms',  <?php echo (count($hotel_rooms)+2);?>)">
         <table width="100%" id="hotel_rooms" class="border_table" cellpadding="5">
		 	<tr><th>key<th>title
		 	<th>regular beds
		 	<th>extra beds<th>qty	
			<?php
				
					if(count($hotel_rooms) == 0)
					{
					?>
					<tr><td><input type="text" name="room_type_key[]" size="5" maxlength="10" style="text-transform: uppercase;"><td><input type="text" name="room_type_title[]" size="20"><td><input type="text" name="room_max_standart_beds[]" value="1">
					<td><input type="text" name="room_max_extra_beds[]" value="1">
					<td><input type="text" name="room_qty[]" value="1">
					</tr>
					<?php					
					}
					else
					for($i=0; $i<count($hotel_rooms); $i++)
					{
					?>
					<tr><td><input type="text" name="room_type_key[]" size="5" maxlength="10" value="<?=$hotel_rooms[$i]['room_type_key']?>" style="text-transform: uppercase;"><td><input type="text" name="room_type_title[]" value="<?=$hotel_rooms[$i]['room_type_title']?>" size="20"><td><input type="text" name="room_max_standart_beds[]" value="<?=$hotel_rooms[$i]['room_max_standart_beds']?>">
					<td><input type="text" name="room_max_extra_beds[]" value="<?=$hotel_rooms[$i]['room_max_extra_beds']?>">
					<td><input type="text" name="room_qty[]" value="<?=$hotel_rooms[$i]['room_qty']?>">
					</tr>
					<?php
					}
			?>
		 </table>  		              
	<tr><td colspan="2" align="center">
		<input type="hidden" name="hotel_image_old" value="<?=$hotel_info['hotel_image']?>">
		<input type="hidden" name="hotel_SiteID" value="<?=$SiteID?>">
		<input type="hidden" id="count_languages" name="count_languages" value="<?=count($languages)?>">
		<?=$form_fields?>
</table>
</form>