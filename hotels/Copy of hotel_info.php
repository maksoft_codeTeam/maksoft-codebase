<br>
<table class="border_table" width="100%" cellpadding="5">
<?php

//get selected hotel
$hotel_id = (int)$_GET['hotel_id'];
$hotels_array = $hotel->get_hotels($SiteID, "h.hotel_id = '".$hotel_id."' LIMIT 1");
	
//list hotels
	if(is_file(DIR_WS_HOTELS_UPLOAD . $hotels_array[0]['hotel_image']))
		$hotel_image = DIR_WS_HOTELS_UPLOAD . $hotels_array[0]['hotel_image'];
	else $hotel_image = DIR_WS_HOTELS_IMAGES . "no_photo.jpg";
?>
<tr><td colspan="2"><h1 class="head_text"><?=$hotels_array[0]['hotel_title']?>, <?=$hotels_array[0]['hotel_category']?>*</h1>
<tr>
	<td width="270" valign="top">
	<a href="<?=DIR_WS_HOTELS_UPLOAD . $hotels_array[0]['hotel_image']?>" rel="lightbox[hotel_gallery]" title="<?=$hotels_array[0]['hotel_title']?>"><img src="img_preview.php?image_file=<?=$hotel_image?>&img_width=250" border="0" class="border_image" alt="<?=$hotels_array[0]['hotel_title']?>"></a>
	<div class="hotel_gallery">
		<?php
		$photo_dir = DIR_WS_HOTELS_UPLOAD . $hotel_id."/";
		$imgs_per_row = 4;
		$img_width = 55;
		include "hotels/hotel_gallery.php";
		?>
	</div>
	<td valign="top">
			<em><?=$hotels_array[0]['resort_title']?></em><br><br>
			<?=$hotels_array[0]['hotel_description']?><br>
			<hr>
			<a href="<?=tep_href_link("action=edit_hotel&edit_id=".$hotels_array[0]['hotel_id'])?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_edit.jpg" border=0></a><a href="javascript: void(0)" onClick="if(confirm('Are you sure?')) document.location='<?php echo tep_href_link("action=delete_hotel&del_id=".$hotels_array[0]['hotel_id']);?>' "><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_delete.jpg" border=0></a>
	</td>
</table>
<a href="<?=tep_href_link("action=add_hotel")?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_add.jpg" border=0></a>
