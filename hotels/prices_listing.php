<br>
<?php
		if(isset($hotel_id))
				{
					include "hotels/editor/default.php";
					
					//$period_types = $hotel->get_period_types($hotel_id, true);
					$period_types = $hotel->get_period_types($hotel_id, true, 1, date("Y-m-d"));
					$count_period_types = count($period_types);
					
					$extra_payments = $hotel->get_extra_payments($hotel_id);
					
					$periods_type_td = "";
					$periods_dates_td = "";
					$room_description_tr_td = "";
					$room_ep_description_tr_td = "";
					
					for($i=0; $i<count($period_types); $i++)
						{ 
							$periods_type_td.= "<th>".$period_types[$i]['period_type_title'];
							$periods_dates_td.= "<td>";
							$period_dates = $hotel->get_period_dates($period_types[$i]['period_type_id']);
							
							for($j=0; $j<count($period_dates); $j++)
								$periods_dates_td.= date("d.m", strtotime($period_dates[$j]['date_from'])) ." - ". date("d.m.Y", strtotime($period_dates[$j]['date_to']))."<br>";
						}
					
					$rooms = $hotel->get_rooms($hotel_id);
					for($i=0; $i<count($rooms); $i++)
						{	
							$room_descriptions = $hotel->get_room_descriptions($rooms[$i]['room_id']);
							
							for($j=0; $j<count($room_descriptions); $j++)
							{
								if($room_descriptions[$j]['room_ep_type'] == 0)
									{
										$room_description_tr_td.="<tr><td class=\"title\"><b>".$rooms[$i]['room_type_key']."</b>&nbsp;".$room_descriptions[$j]['room_description_title'] . "<a href=\"#\" tiltle=\"Delete price\" onClick=\"if(confirm('Are you sure?')) document.location = '".tep_href_link("action=delete_prices&amp;del_id=".$room_descriptions[$j]['room_description_id'] ."&amp;hotel_id=".$hotel_id)."'\" ><img src=\"".DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES."button_delete_small.gif\" alt=\"Delete price?\" border=\"0\" hspace=\"3\" align=\"middle\"></a><a href=\"".tep_href_link("action=edit_prices&hotel_id=$hotel_id&edit_id=".$room_descriptions[$j]['room_description_id'])."\"><img src=\"".DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES."button_edit_small.gif\" alt=\"Edit price\" border=\"0\" hspace=\"3\" align=\"middle\"></a>
										<td><a href=\"#\" title=\"".$room_descriptions[$j]['accommodation_type_string']."\">".$room_descriptions[$j]['accommodation_type_name']."</a><td>".$room_descriptions[$j]['room_standart_beds']."<td>".$room_descriptions[$j]['room_extra_beds'];
											for($k=0; $k<$count_period_types; $k++)
												$room_description_tr_td.= "<td class=\"price\">".get_room_price($room_descriptions[$j]['room_description_id'], $period_types[$k]['period_type_id']);
									}
								else
									{
										$room_ep_description_tr_td.="<tr><td class=\"title\"><b>".$rooms[$i]['room_type_key']."</b>&nbsp;".$room_descriptions[$j]['room_description_title'] . "<a href=\"#\" tiltle=\"Delete price\" onClick=\"if(confirm('Are you sure?')) document.location = '".tep_href_link("action=delete_prices&amp;del_id=".$room_descriptions[$j]['room_description_id'] ."&amp;hotel_id=".$hotel_id)."'\" ><img src=\"".DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES."button_delete_small.gif\" alt=\"Delete price?\" border=\"0\" hspace=\"3\" align=\"middle\"></a><a href=\"".tep_href_link("action=edit_prices&hotel_id=$hotel_id&edit_id=".$room_descriptions[$j]['room_description_id'])."\"><img src=\"".DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES."button_edit_small.gif\" alt=\"Edit price\" border=\"0\" hspace=\"3\" align=\"middle\"></a>
										<td><a href=\"#\" title=\"".$room_descriptions[$j]['accommodation_type_string']."\">".$room_descriptions[$j]['accommodation_type_name']."</a><td>".$room_descriptions[$j]['room_standart_beds']."<td>".$room_descriptions[$j]['room_extra_beds'];
											for($k=0; $k<$count_period_types; $k++)
												$room_ep_description_tr_td.= "<td class=\"price\">".get_room_price($room_descriptions[$j]['room_description_id'], $period_types[$k]['period_type_id']);
									}								
							}
						}
						
						if(($count_period_types > 0 || count($rooms) >0 )&& $user->AccessLevel >0)
						{
						?>
						
						<table width="100%" border="0" cellpadding="5" cellspacing="1" class="hotel_prices">
							<tr><th colspan="<?=(4+$count_period_types)?>" align="center"><?=$hotel->get_hName($hotel_id, $language_id)?>
							<?php
								if($count_period_types >0)
								{
									?>
                            <tr>
								<th colspan="4"><?=TITLE_SEASONS?>
								<?=$periods_type_td?>
							</tr>
                            		<?
								}
							?>
							<tr>
								<td rowspan="2"><?=LABEL_ROOM_TYPE?>
								<td rowspan="2" width="50"><?=LABEL_ACCOMMODATION_TYPE?>
								<td rowspan="2" width="50"><?=LABEL_ROOM_STANDART_BEDS?>
								<td rowspan="2" width="50"><?=LABEL_ROOM_EXTRA_BEDS?>
								<?=$periods_dates_td?>
							</tr>
                            	
							<tr>
                            <?php
								if($count_period_types > 0)
								{
								?>
								<td colspan="<?=$count_period_types?>"><?php printf(MESSAGE_PRICES_IN_CURRENCY, "<b>".get_hotel_currency($hotel_id, "currency_key")."</b>")?>
							</tr>
                            	<?
                                }
                            ?>
							<?=$room_description_tr_td?>
							<tr>
							<th colspan="<?=(4+$count_period_types)?>" align="center">Extra payments
							</tr>
							<?=$room_ep_description_tr_td?>
							<!--<tr><th colspan="<?=(4+$count_period_types)?>" align="center"><?php printf(MESSAGE_PRICES_FROM_TO, "<b>".get_hotel_min_max_price($hotel_id, "MIN")."</b>", "<b>".get_hotel_min_max_price($hotel_id, "MAX")."</b>")?>//-->
						</table>
						<a href="<?=tep_href_link("action=add_prices&hotel_id=".$hotel_id)?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_add.jpg" border=0></a>
                        <?php
						
						}
		else mk_output_message("warning", "No prices defined !");
						
				?>
                <br>
				
                <table class="hotel_prices" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                    <th colspan="6">Other payments
                    <tr><td width="20">#<td width="50">period<td width="200">room description<td>acc.type<td>type<td>price
                    <?php
                        for($i=0; $i<count($extra_payments); $i++)
                            echo "<tr><td>".($i+1)."<td>".$extra_payments[$i]['period_type_title'] ."<td>".  $extra_payments[$i]['room_description'] ."<td>". $extra_payments[$i]['accommodation_type_string'] . "<td>".$extra_payments[$i]['ep_type_description'] ."<td>".$extra_payments[$i]['ep_price']; 
                    ?>
                </table>
				<br>
				<table class="hotel_prices" cellpadding="5" cellspacing="0" width="100%">
					<tr><th>Hotel conditions
					<tr><td><?=$hotel->get_hConditions($hotel_id, $language_id)?>
							<hr><a href="<?=tep_href_link("action=edit_condition&hotel_id=".$hotel_id)?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_edit.jpg" border=0></a>
						</td>
				</table>
                <?php
				}
else
	{
		//list all hotels
		?>
		<form method="get">
		<input type="hidden" name="n" value="<?=$n?>">
		<input type="hidden" name="SiteID" value="<?=$SiteID?>">
		<input type="hidden" name="action" value="<?=$action?>">
		<table class="border_table" cellpadding="5" width="100%"><tr><th colspan=2><?=list_resorts($resort_id, "onChange=\"this.form.submit()\"")?><th>from:<th>
		<?php
		if($resort_id > 0)
			$hotels_array = $hotel->get_hotels($SiteID, "1 AND r.resort_id = '".$resort_id."' ORDER by hd.hotel_title ASC");
		else
			$hotels_array = $hotel->get_hotels($SiteID, "1 ORDER by hd.hotel_title ASC, rd.resort_title ASC");
			
		if(count($hotels_array) == 0)
			{
			?>
				<tr><td colspan="4"><?=mk_output_message("warning", "No hotels found !")?>
			<?php
			}
		for($i=0; $i<count($hotels_array); $i++)
			{
			?>
					<tr><td width="30" align="center">#<?=$i+1?><td><a href="<?=tep_href_link("action=list_prices&amp;hotel_id=".$hotels_array[$i]['hotel_id'])?>"><?=$hotels_array[$i]['hotel_title']?></a>&nbsp;(<em><?=$hotels_array[$i]['resort_title']?></em>)<td width="50" align="center"><?php echo get_hotel_min_max_price($hotels_array[$i]['hotel_id'], "MIN") > 0 ? "<b>".format_price(get_hotel_min_max_price($hotels_array[$i]['hotel_id'], "MIN"), $hotels_array[$i]['hotel_id'])."</b>" : "&nbsp;"?><td width="100" align="center"><a href="<?=tep_href_link("action=add_prices&amp;hotel_id=".$hotels_array[$i]['hotel_id'])?>"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_add.jpg" border=0></a>
			<?php
			}
		?>
		</table>
		</form>
		<?php
	}
	
?>