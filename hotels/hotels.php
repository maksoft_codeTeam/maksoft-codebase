<link type="text/css" href="hotels/base_style.css">
<link type="text/css" href="hotels/admin_style.css">
<link rel="stylesheet" href="lib/datepicker/datepicker.css" type="text/css" />
<script type="text/javascript" src="lib/datepicker/datepicker.js"></script>

<?php

//check form module activation
if(HOTELS_MODULE_ACTIVE == 0)
	mk_output_message("error", "Module HOTELS stoped OR not activated !");
elseif(HOTELS_MODULE_ACTIVE < 0)
	mk_output_message("warning", "Module HOTELS temporary stopped for cleanup !");
else
{	
require_once "hotels/includes/config.php";
//db table names
require_once DIR_WS_HOTELS_INCLUDES . "db_tables.php";
//language dictionary
include DIR_WS_HOTELS_LANGUAGES . "bulgarian.php";
require_once DIR_WS_HOTELS_INCLUDES . "functions.php";
require_once "lib/lib_hotels.php";

$hotel = new hotels();
//$url = "page.php?n=$m&SiteID=$SiteID";
$url = $PHP_SELF;
$p_vars = "";
/*
$i=0;
while (list($var, $value) = each ($_GET))
	{	
		if($i == 0) $url.= "?".$var."=".$value;
		else $url.= "&".$var."=".$value;
		$i++;
	}
echo $url;
*/
if(isset($_GET['language_id']))
	$language_id = $_GET['language_id'];
else $language_id = 1;

$current_language = get_language($language_id);
define("DIR_WS_LANGUAGE_IMAGES", $current_language['language_dir']."/images/");

$languages = get_languages();
echo "<div style=\"background-color: #efefef; padding: 5px;\">";
for($i=0; $i<count($languages); $i++)
	echo "<a href=\"".tep_href_link("language_id=".$languages[$i]['language_id'])."\"><img src=\"".DIR_WS_HOTELS_LANGUAGES . $languages[$i]['language_dir']."/images/".$languages[$i]['language_flag']."\" border=0 title=\"".$languages[$i]['language_text']."\"></a> ";
echo "</div>";

//include top menu
include DIR_WS_HOTELS_INCLUDES . "menu.php";
	
if($_GET['action'] == "delete_hotel")
	{
		$del_id = $_GET['del_id'];
		$hotel->delete_hotel($del_id);
	}

if($_GET['action'] == "delete_period")
	{
		$del_id = $_GET['del_id'];
		$hotel->delete_period($del_id);
	}

if($_GET['action'] == "delete_period_type")
	{
		$del_id = $_GET['del_id'];
		$hotel->delete_period_type($del_id);
	}
		
if($_POST['action'] == "add_hotel_confirm")
	{
		$hotel->add_hotel();

	}

if($_POST['action'] == "add_period_confirm")
	{
		if(isset($hotel_id) && $hotel_id !=0)
			$hotel->add_period();
		else mk_output_message("error", "Nothing inserted ! Select hotel first.");
	}
	
if($_POST['action'] == "edit_hotel_confirm")
	{
		
		$edit_id = $_POST['edit_id'];
		//print_r($room_type_key);
		//print_r($room_type_title);
		$hotel->add_hotel($edit_id);
		/*
		echo "<pre>";
		print_r($_POST);
		echo "</pre>";
		*/
	}


if($_POST['action'] == "add_resort_confirm")
	{
		$hotel->add_resort();
	}

if($_POST['action'] == "edit_resort_confirm")
	{
		$edit_id = $_POST['edit_id'];
		$hotel->add_resort($edit_id);
	}

if($_POST['action'] == "add_price_confirm")
	{
		
		//if($user->ID == 196)
		$hotel->add_prices($hotel_id);
		/*
		echo "<pre>";
		print_r($_POST);
		echo "</pre>";
		*/
	}

if($_POST['action'] == "edit_price_confirm")
	{
		$hotel->edit_price($edit_id);
		/*
		echo "<pre>";
		print_r($_POST);
		echo "</pre>";
		*/
	}


if($_GET['action'] == "delete_prices")
	{
		$del_id = $_GET['del_id'];
		$hotel->delete_prices($del_id);		
	}

//add extra paymnet
if($_POST['action'] == "add_extra_payment")
	{
		if($hotel->add_extra_payment())
			$action_message = "<b>".mysqli_affected_rows()."</b> record(s) inserted !";
		else $action_message = "No records inserted !";
	}
	
//delete extra payment
if($_GET['action'] == "delete_ep")
	{
		$ep_id = $_GET['del_id'];
		if($hotel->delete_extra_payment($ep_id))
			$action_message = "<b>".mysqli_affected_rows()."</b> record(s) deleted";
	}

//add promotion
if($_POST['action'] == "add_promo_confirm")
	{
		$hotel_id = $_POST['hotel_id'];
		$edit_id = $_POST['edit_id'];
		
		if($hotel_id > 0)
			{
				if($edit_id >0)
					$hotel->add_promotion($hotel_id, $edit_id);
				else
					$hotel->add_promotion($hotel_id);
			}
		else
			$action_message = "No promotion inserted ! Select HOTEL !";
	}

//delete promotion
if($_GET['action'] == "delete_promotion")
	{
		$del_id = $_GET['del_id'];
		if($hotel->delete_promotion($del_id))
			$action_message = "Promotion deleted";
		else
			$action_message = "No records deleted";
	}

//edit hotel conditions
if($_POST['action'] == "edit_conditions_confirm")
	{
		if(isset($hotel_id))
			$hotel->edit_hotel_conditions($hotel_id);
	}

//reorder periods
if($_POST['action'] == "periods_order")
	{
		$periods_order = $_POST['periods_order'];
		$periods_id = array_keys($periods_order);
		for($i=0; $i<count($periods_id); $i++)
			{//echo $periods_id[$i] . " / " . $periods_order[$periods_id[$i]]."<br>";
				$sql = "UPDATE ".TABLE_PERIODS_TYPE." SET period_order = '".$periods_order[$periods_id[$i]]."' WHERE period_type_id = '".$periods_id[$i]."'";
				$hotel->db_query($sql);
			}
		//print_r($periods_id);
	}

//reorder promotions
if($_POST['action'] == "promotions_order")
	{
		$promotions_order = $_POST['promotions_order'];
		$promotions_id = array_keys($promotions_order);
		for($i=0; $i<count($promotions_id); $i++)
			{
				$sql = "UPDATE ".TABLE_PROMOTIONS." SET sort_n = '".$promotions_order[$promotions_id[$i]]."' WHERE promo_id = '".$promotions_id[$i]."'";
				$hotel->db_query($sql);
			}
		//print_r($periods_id);
	}
					   
switch($_GET['action'])
	{
		case "edit_hotel":
		case "add_hotel": { include "hotels/hotels_add.php"; break; }
		case "edit_resort":
		case "add_resort": { include "hotels/resorts_add.php"; break; }
		case "hotel_info" : {include "hotels/hotel_info.php"; break;}
		case "list_resorts": {include "hotels/resorts_listing.php"; break;}
		case "edit_prices": {include "hotels/price_edit.php"; break;}
		case "add_prices": {include "hotels/price_add.php"; break;}
		case "edit_condition": {include "hotels/conditions_edit.php"; break;}
		case "delete_prices":
		case "list_prices": {include "hotels/prices_listing.php"; break;}	
		case "edit_period":
		case "add_period": { include "hotels/periods_add.php"; break; }		
		case "delete_period":
		case "delete_period_type":
		case "list_periods": {include "hotels/periods_listing.php"; break;}
		case "list_reservations": {include "hotels/reservations_listing.php"; break;}
		case "delete_promotion":
		case "list_promotions":	{include "hotels/promotions_listing.php"; break;}
		case "add_promotion":	
		case "edit_promotion":	{include "hotels/promotions_add.php"; break;}
		case "delete_ep":
		case "list_ep": {include "hotels/ep_listing.php"; break;}
		case "list_hotels" : {include "hotels/hotels_listing.php"; break;}
		default :  {include DIR_WS_HOTELS_INCLUDES . "home.php";}
		//default :  {include "hotels/hotels_listing.php";}
	}
}
?>