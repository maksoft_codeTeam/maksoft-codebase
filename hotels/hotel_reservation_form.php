<?php
	//cuurent file dictionary
	include DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir'] . "/hotel_reservation_form.php";
?>
<script language="JavaScript">
$j(document).ready(function(){
		
		var now = new Date();

		//global functions
		
		function focus_object(id){
			$j(id).css("border", "1px solid red");
		}
		
		function unfocus_object(id){
			$j(id).css("border", "0px");
		}

		function enable_object(id){
			$j(id).attr('disabled', false);
		}
		
		function disable_object(id){
			$j(id).attr('disabled', true);
		}
		
		function output_message(type, message){
			$j('#message').attr("class", type);
			$j('#message').html(message);
			$j('#message').slideDown();
		}
		
		function update_form(){
		
			//load hotel rooms if hotel selected
		
			date_from = $j('#date_arrive').val();
			date_to = $j('#date_leave').val();
			hotel_id = $j('#hotel_id').val();	

			if(hotel_id !=0 && $j('#room_description_id').val() == 0)
				load_room_types(hotel_id);
	
			if($j("[name='hotel_id']").val() !=0)
				focus_object('#date_arrive');
			
			$j('#hotel_id').change(function(){
				focus_object('#date_arrive');
				load_room_types($j('#hotel_id').val());	
				//update_form();   
			})
			
			calculate_days(date_from, date_to);
			
			if(date_from != "" && date_to != "")
				calculate_reservation_price();
			
			list_room_people();
			
		}

			$j('#room_description_id').change(function(){
					update_form();
			});
			
			$j('#people').change(function(){
					update_form();
			});
			
		//calendar picker DATE_FROM	
		$j('#calendar1').DatePicker({
			format:'Y-m-d',
			date: $j("[name='today']").val(),
			current: $j("[name='today']").val(),
			starts: 1,	
			position: 'bottom',
			onRender: function(date) {
				return {
					disabled: (date.valueOf() < now.valueOf()),
					className: date.valueOf() == now.valueOf() ? 'datepickerSpecial' : false
				}
			},			
			onChange: function(formated, dates){
				$j('#date_arrive').val(formated);
				$j('#calendar1').DatePickerHide();
				$j('#calendar2').show();
				enable_object('#date_leave');
				focus_object('#date_leave');
				unfocus_object('#date_arrive');
				update_form();
			}
		});
		
		$j('#calendar2').hide();
		disable_object('#date_leave');
		
		//calendar picker DATE_TO
		$j('#calendar2').DatePicker({
			format:'Y-m-d',
			date: $j('#date_arrive').val(),
			current: $j('#date_arrive').val(),
			starts: 1,	
			position: 'bottom',
			onBeforeShow: function(){
				$j('#calendar2').DatePickerSetDate($j('#date_arrive').val(), true);
			},
			onRender: function(date) {
				return {
					disabled: (date.valueOf() < now.valueOf()),
					className: date.valueOf() == now.valueOf() ? 'datepickerSpecial' : false
				}
			},

			onChange: function(formated, dates){
				$j('#date_leave').val(formated);
				$j('#calendar2').DatePickerHide();			
				unfocus_object('#date_leave');

				htmlobj = $j.ajax({
				   type: "POST",
				   url: "hotels/includes/functions_call.php",
				   data: "f=check_hotel_date_availability&hotel_id="+hotel_id+"&date_from="+date_from+"&date_to="+date_to,
				   success: function(){
					if(htmlobj.responseText)
						output_message("message_normal", "Hotel Available !")
					else
						output_message("message_error", "Hotel NOT available !")
				   }
				 });
				 update_form();
			}

		});
		

		//load room types select menu		
		function load_room_types(hotel_id){

		$j("#room_description_id").html("Loading ... ");
				$j('#row3').show();
				HRobj = $j.ajax({
				   type: "POST",
				   url: "hotels/includes/functions_call.php",
				   data: "f=list_hotel_rooms&hotel_id="+hotel_id+"&name=room_description_id&params=",
				   success: function(){
				   if(HRobj.responseText)
						$j("#room_description_id").html(HRobj.responseText);
				   else $j("#room_description_id").html("Select hotel");
				   }
				 });
			   
	   }
	   
	   function calculate_days(date_from, date_to){
			if(date_from != "" && date_to != "")
			{
				dateobj = $j.ajax({
				   type: "POST",
				   url: "hotels/includes/functions_call.php",
				   data: "f=get_date_difference&date_from="+date_from+"&date_to="+date_to,
				   success: function(){
					if(dateobj.responseText)
						{
							$j('#nights_row').show();
							$j('#count_nights').html(dateobj.responseText);
						}
				   }
				 });
			 }	   
	   }
	   
	   $j('#message').click(function(){
	   $j('#message').hide("slow");
	   });
	   
	   //calculate reservation price
	   function calculate_reservation_price(){
	   			
				date_from = $j('#date_arrive').val();
				date_to = $j('#date_leave').val();
				hotel_id = $j('#hotel_id').val();
				room_description_id = $j('#room_description_id').val();
				people = $j('#people').val();
				
				pobj = $j.ajax({
					type: "POST",
					url: "hotels/includes/functions_call.php",
					data: "f=calculate_reservation_price&hotel_id="+hotel_id+"&room_description_id="+room_description_id+"&people="+people+"&date_from="+date_from+"&date_to="+date_to,
					success: function(){
						if(pobj.responseText)
								$j(".price").html(pobj.responseText);
					}				
				
				})
	   };
	 
	 function list_room_people(){
	 		room_description_id = $j('#room_description_id').val();
			lrp_obj = $j.ajax({
				type: "POST",
				url: "hotels/includes/functions_call.php",
				data: "f=list_room_people&room_description_id="+room_description_id,
				success: function(){
					if(lrp_obj.responseText)
							$j('#people').html(lrp_obj.responseText);
					else $j('#people').html("<i>select room</i>");
				}
			
			});
	 }
	 
	 //update form on load
	 update_form();  			
});
</script>
<?php
	/*
	$date_from = "2009-12-10";
	$date_to = "2010-01-10";
	echo $date_from . " / " .$date_to."<hr>";
	
	echo "<pre>";
	print_r(check_period_dates($hotel_id, $date_from, $date_to));
	echo "</pre>";
	
	echo calculate_reservation_price($hotel_id, 67, $date_from, $date_to, 1, 0);
	*/
?>
<form>
<input type="hidden" name="today" value="<?=date("Y-m-d")?>">
<div id="message" style="display: none; margin: 10px 0px 10px 0px"></div>
<table width="100%" cellpadding="5" cellspacing="2" class="hotel_request">
  <tr><td align="right" valign="top"><?=FORM_LABEL_HOTEL?> 
  <td align="left" valign="top"><?=list_hotels($hotel_id, "id='hotel_id' style='width: 200px'")?>
  <tr id="row1" style="display: ;">
    <td width="150" align="right" valign="top"><?=FORM_LABEL_DATE_ARRIVE?></td>
    <td align="left" valign="top">
		<input name="date_arrive" id="date_arrive" title="<?=FORM_LABEL_DATE_ARRIVE?>" type="text" value="" size="26">
	  <a href="#" class="calendar" id="calendar1" title="<?=FORM_LABEL_SLECT_ARRIVE_DATE?>"><img src="<?=DIR_WS_HOTELS_IMAGES?>icon_calendar.jpg" border="0"></a>	</td>
  </tr>
  <tr id="row2" style="display: ;">
    <td align="right" valign="top"><?=FORM_LABEL_DATE_LEAVE?></td>
    <td align="left" valign="top">
		<input name="date_leave" id="date_leave" title="<?=FORM_LABEL_DATE_ARRIVE?>" type="text" value="" size="26">
	  	<a href="#" class="calendar" id="calendar2" title="<?=FORM_LABEL_SLECT_LEAVE_DATE?>"><img src="<?=DIR_WS_HOTELS_IMAGES?>icon_calendar.jpg" border="0"></a>
	</td>
  </tr> 
  <tr id="nights_row" style="display: none;">
    <td align="right" valign="top"><?=FORM_LABEL_COUNT_NIGHTS?></td>
    <td align="left" valign="top" id="count_nights"></td> 
  </tr>
  <tr id="row3" style="display:;">
    <td align="right" valign="top"><?=FORM_LABEL_ROOM_TYPE?></td>
    <td align="left" valign="top" id="room_type"><select name="room_description_id" style="width: 200px" id="room_description_id"><option value="0">- select room -</option></select></td> 
  </tr>
  <tr>
  <tr>
    <td align="right" valign="top"><?=FORM_LABEL_VISITORS?></td>
    <td align="left" valign="top"><select name="people" id="people"  style="width: 50px"></select></td> 
  </tr>
  <tr>  
  	<td width="150" align="right" valign="top"><?=FORM_LABEL_NAME?></td>
	<td align="left" valign="top"><input name="name" type="text" size="30"></td>
  </tr>
  <tr>
  	<td align="right" valign="top"><?=FORM_LABEL_FAMILY?></td>
  	<td align="left" valign="top"><input type="text" size="30"></td>
  </tr>
  <tr>
  	<td align="right" valign="top"><?=FORM_LABEL_PHONE?></td>
	<td align="left" valign="top"><input type="text" size="30"></td>
  </tr>
  <tr>
  	<td align="right" valign="top"><?=FORM_LABEL_EMAIL?></td>
	<td align="left" valign="top"><input type="text" size="30"></td>
  </tr>
  <tr>
  	<td align="right" valign="top"><?=FORM_LABEL_COMMENTS?></td>
  	<td align="left" valign="top"><textarea name="comments" id="comments" cols="23" rows="10"></textarea></td>
  </tr>
  <tr>
  	<td align="center" valign="top" colspan="2" id="price"><?php printf(FORM_LABEL_PRICE, format_price(0, $hotel_id));?></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
  	<td align="left" valign="top"><img src="<?=DIR_WS_HOTELS_LANGUAGES . DIR_WS_LANGUAGE_IMAGES?>button_send.jpg"></td>
  </tr>
</table>
</form>