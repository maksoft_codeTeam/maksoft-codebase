<?php
require_once SITE_ROOT."hotels/includes/config.php";
require_once SITE_ROOT."hotels/includes/functions.php";
require_once SITE_ROOT."lib/lib_hotels.php";

$hotel = new hotels();

if(isset($_GET['language_id']))
	$language_id = $_GET['language_id'];
else $language_id = 1;

$current_language = get_language($language_id);
define("DIR_WS_LANGUAGE_IMAGES", $current_language['language_dir']."/images/");

//get all hotels for selected SiteID
if(isset($hotel_id) && $hotel_id != 0)
	$hotels_array = $hotel->get_hotels($SiteID, "h.hotel_id = '".$hotel_id."'");
else if(isset($resort_id) && $resort_id != 0)
	$hotels_array = $hotel->get_hotels($SiteID, "h.resort_id = '".$resort_id."'");
else if(isset($tour_type_id) && $tour_type_id != 0)
	$hotels_array = $hotel->get_hotels($SiteID, "tt.tour_type_id = '".$tour_type_id."'");
else 
	$hotels_array = $hotel->get_hotels($SiteID);

$content = "<table border=0 width=\"100%\" id=\"hotels_listing\"><tr>";	
$j = 1;
//list hotels
for($i=0; $i<count($hotels_array); $i++)
{
	if(is_file("../../".DIR_WS_HOTELS_UPLOAD . $hotels_array[$i]['hotel_image']))
		$hotel_image = DIR_WS_HOTELS_UPLOAD . $hotels_array[$i]['hotel_image'];
	else $hotel_image = DIR_WS_HOTELS_IMAGES . "no_photo.jpg";
	
	$content.= "<td align=\"center\" width=\"150\"><a href=\"#".$hotels_array[$i]['hotel_id']."\" onClick=\"set_hotel_info()\"><img src=\"http://www.maksoft.net/img_preview.php?image_file=".$hotel_image."&img_width=150\" border=\"0\" class=\"border_image\" alt=\"".$hotels_array[$i]['hotel_title']."\"></a></td>";
	$content.= "<td valign=\"top\" width=\"320\"><h1><a href=\"#".$hotels_array[$i]['hotel_id']."\" onClick=\"set_hotel_info()\">".$hotels_array[$i]['hotel_title'].",&nbsp;".$hotels_array[$i]['hotel_category']."*</a></h1><em>".$hotels_array[$i]['resort_title']."</em>";
	if($j>=2) { $content.="<tr>"; $j=1; }
	else $j++;
}
$content.="</table><div id=\"hotel_info\" style=\"border: 1px solid #ebebeb\"></div>";
echo "document.write('".$content."')";
?>