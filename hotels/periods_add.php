<script language="JavaScript">
$j(document).ready(function(){

		var now = new Date();
		$j('#calendar2').hide();

		//calendar picker DATE_FROM	
		$j('#calendar1').DatePicker({
			format:'Y-m-d',
			date: $j("[name='today']").val(),
			current: $j("[name='today']").val(),
			starts: 1,	
			position: 'bottom',
			onRender: function(date) {
				return {
					disabled: (date.valueOf() < now.valueOf()),
					className: date.valueOf() == now.valueOf() ? 'datepickerSpecial' : false
				}
			},			
			onChange: function(formated, dates){
				$j('#date_from').val(formated);
				$j('#calendar1').DatePickerHide();
				$j('#calendar2').show();
			}
		});


		//calendar picker DATE_TO
		$j('#calendar2').DatePicker({
			format:'Y-m-d',
			date: $j('#date_from').val(),
			current: $j('#date_from').val(),
			starts: 1,	
			position: 'bottom',
			onBeforeShow: function(){
				$j('#calendar2').DatePickerSetDate($j('#date_from').val(), true);
			},
			onRender: function(date) {
				return {
					disabled: (date.valueOf() < now.valueOf()),
					className: date.valueOf() == now.valueOf() ? 'datepickerSpecial' : false
				}
			},

			onChange: function(formated, dates){
				$j('#date_to').val(formated);
				$j('#calendar2').DatePickerHide();			
				//unfocus_object('#date_leave');
			}

		});


});
</script>
<?php
if(isset($hotel_id) && !empty($hotel_id) && $hotel_id != 0)
{
	
?>
<form method="post" name="period_add" target="_self" action="<?=tep_href_link("action=list_periods")?>">
<table class="border_table" cellpadding="5" width="100%">
<tr><th colspan="3"><?=list_hotels($hotel_id, "onChange=\"document.location='page.php?n=".$n."&SiteID=".$SiteID."&action=add_period&hotel_id='+this.value+''\"'")?>
<tr><td colspan="2">
<?php 
	list_period_types("period_type_id", $hotel_id, $period_type_info['period_type_id'], "onChange=\"if(this.selectedIndex != 0) document.getElementById('new_period').style.display='none'; else show_hide('new_period')\"");
?>
<tr><td colspan="2">
<fieldset><legend>new period:</legend>
<table cellpadding="5" id="new_period">
<?php
		for($i=0; $i<count($languages); $i++)
			{
				$lang_id = $languages[$i]['language_id'];
				//$hotel_title = $hotel->get_hInfo($edit_id, "hotel_title", $lang_id);
				$language_image = "<img src=\"".DIR_WS_HOTELS_LANGUAGES . $languages[$i]['language_dir']."/images/".$languages[$i]['language_flag']."\">";
				
			//get pariod info
			$period_type_info = $hotel->get_period_type_info($edit_id, $lang_id);
?>
<tr><td><?=$language_image?><td><input type="text" name="period_type_title[<?=$lang_id?>]" size="50" value="<?=$period_type_info['period_type_title']?>">
<?
			}
?>
<tr><td><td>season key
<tr><td><td><input type="text" name="period_type_key" size="50">
<br> (<em>out / advertisement / poor / middle / strong</em>)
<tr>
  <td>
  <td>sort order 
<tr>
  <td>
  <td><input name="period_order" type="text" id="period_order" value="1" size="10">  
</table>
</fieldset>
<tr>
<td>start date
<td>end date
<tr><td><input name="date_from" type="text" id="date_from" size="30">
<a href="#" class="calendar" id="calendar1" title="<?=FORM_LABEL_SLECT_ARRIVE_DATE?>"><img src="<?=DIR_WS_HOTELS_IMAGES?>icon_calendar.jpg" border="0"></a>
  <td><input name="date_to" type="text" id="date_to" size="30">
    <a href="#" class="calendar" id="calendar2" title="<?=FORM_LABEL_SLECT_LEAVE_DATE?>"><img src="<?=DIR_WS_HOTELS_IMAGES?>icon_calendar.jpg" border="0"></a>  
<tr>
  <td>minimum days  
  <td>&nbsp;  
<tr>
  <td><input name="min_days" type="text" size="10" value="1">
    
  <td>&nbsp;  
<tr align="center">
  <td colspan="2"><input type="hidden" name="action" value="add_period_confirm">
    <input type="hidden" name="hotel_id" value="<?=$hotel_id?>">
    <input name="submit" type="submit" value="save">
    <input type="hidden" name="today" value="<?=date("Y-m-d")?>">  
	<input type="hidden" name="edit_id" value="<?=$edit_id?>">
<tr>
  <td colspan="2">&nbsp;  
</table>
</form>
<?php
}
else
{
?>
<table class="border_table" cellpadding="5" width="100%">
	<tr><td><?=mk_output_message("warning", "Select hotel: <a href=\"page.php?n=".$n."&amp;SiteID=".$SiteID."&amp;action=list_periods\">here</a>")?>
</table>
<?php
}
?>