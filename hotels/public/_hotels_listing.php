<?php
require_once "hotels/includes/config.php";
require_once "hotels/includes/functions.php";
require_once "lib/lib_hotels.php";

$hotel = new hotels();
$local_url = "page.php?n=$n&amp;SiteID=$SiteID";

if(isset($_GET['language_id']))
	$language_id = $_GET['language_id'];
else if(!isset($language_id))
	$language_id = 1;

$current_language = get_language($language_id);

		//get CMS page name AND search in hotels TABLE
		$resort_name = $row->Name;
		$hotels_array = $hotel->get_hotels($hotel_SiteID, "rd.resort_title LIKE '".$resort_name."'");

if(!isset($hotels_per_row))
	$hotels_per_row = 1;


$short_content = "<table border=\"0\" class=\"hotels_listing_short\" cellspacing=\"1\" cellpadding=\"2\" width=\"100%\"><tr>";
$j = 1;
for($i=0; $i<count($hotels_array); $i++)
	{
		$short_content.= "<td width=\"20\"><b>".($i+1).".</b><td><a href=\"".$local_url."&amp;hotel_id=".$hotels_array[$i]['hotel_id']."#".$hotels_array[$i]['hotel_title']."\" class=\"head_text\">".$hotels_array[$i]['hotel_title']."</a><td>".get_hotel_stars($hotels_array[$i]['hotel_id'])."<td>";
		if($j>=3) { $short_content.="<tr>"; $j=1; }
		else $j++;		
	}
	
$short_content.= "</table>";
echo $short_content;

?>