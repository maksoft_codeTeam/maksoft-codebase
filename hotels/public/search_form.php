<?php
	//cuurent file dictionary
	include DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir'] . "/search_form.php";
?>

<form name="hotels_search" method="get" class="hotel_search">
<input type="hidden" name="n" value="<?=$n?>">
<input type="hidden" name="SiteID" value="<?=$SiteID?>">
<table class="hotels_search" cellpadding="5" width="100%">
<tr><th colspan="2"><?=SEARCH_TITLE?></th>
<tr><?php $searchstring=""; if(isset($_GET["hotelname"])){$searchstring = $_GET["hotelname"];}?>
	<td align="left" colspan="2"><?=SEARCH_BY_NAME?> <?=searchbar($searchstring)?></td>
</tr>
<tr>
	<td width="150" align="right"><?=SEARCH_LABEL_RESORT?></td>
	<td><?=list_resorts($resort_id)?></td>
</tr>
<tr>
	<td align="right"><?=SEARCH_LABEL_TOUR_TYPE?></td>
	<td><?=list_tour_types($tour_type_id)?></td>
</tr>
<tr>
	<td align="right"><?=SEARCH_LABEL_HOTEL_CATEGORY?></td>
	<td><?=list_hotel_category(isset($hotel_category) ? $hotel_category : -1)?></td>
</tr>
<!--
<tr>
	<td align="right"><?=SEARCH_LABEL_HOTEL_STATUS?>
	<td><?=list_hotel_status(isset($hotel_status) ? $hotel_status : -1)?>
</tr>

<tr>
	<td align="right"><?=SEARCH_LABEL_PRICE_FOR_ROOM?>
	<td>
		<select name="price_from_to">
			<option value="0"> - </option>
			<option value="0-25" <?=$price_from_to == "0-25" ? "selected":""?>>0.00 - 25.00</option>
			<option value="25-50" <?=$price_from_to == "25-50" ? "selected":""?>>25.00 - 50.00</option>
			<option value="50-75" <?=$price_from_to == "50-70" ? "selected":""?>>50.00 - 75.00</option>
			<option value="75-100" <?=$price_from_to == "75-100" ? "selected":""?>>75.00 - 100.00</option>
			<option value="100-125" <?=$price_from_to == "100-125" ? "selected":""?>>100.00 - 125.00</option>
			<option value="125-150" <?=$price_from_to == "125-150" ? "selected":""?>>125.00 - 150.00</option>
			<option value="150-200" <?=$price_from_to == "150-200" ? "selected":""?>>150.00 - 200.00</option>
			<option value="200-250" <?=$price_from_to == "200-250" ? "selected":""?>>200.00 - 250.00</option>
			<option value="250-300" <?=$price_from_to == "250-300" ? "selected":""?>>250.00 - 300.00</option>
			<option value="300-400" <?=$price_from_to == "300-400" ? "selected":""?>>300.00 - 400.00</option>
			<option value="400-500" <?=$price_from_to == "400-500" ? "selected":""?>>400.00 - 500.00</option>
		</select>		
</tr>
//-->
<tr><td colspan="2" align="center"><input type="submit" value="<?=FORM_BUTTON_SEARCH_HOTELS?>" class="button_submit"></td>
</table>
</form><br><br>