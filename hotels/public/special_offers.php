<?php
require_once "hotels/includes/config.php";
require_once "hotels/includes/db_tables.php";
require_once "hotels/includes/functions.php";
require_once "lib/lib_hotels.php";

$hotel = new hotels();
$page_url = "page.php?n=$n&amp;SiteID=$SiteID";
$local_url = $page_url;


if(isset($_GET['language_id']))
	$language_id = $_GET['language_id'];
else if(!isset($language_id))
	$language_id = 1;

$current_language = get_language($language_id);
define("DIR_WS_LANGUAGE_IMAGES", DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir']."/images/");
require_once DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir'].".php";


//init search variables
$resort_id = (int)$resort_id;
$tour_type_id = (int)$tour_type_id;
 
//HOTEL INFO
if((isset($_GET['hotel_id']) && $_GET['hotel_id'] != 0) || (isset($hotel_id) && $hotel_id != 0))
	{
		//get selected hotel
		//$hotel_id = (int)$_GET['hotel_id'];
		$hotel_id = (int)$hotel_id;
		$hotel_info = $hotel->get_hotel($hotel_id, $language_id);
		if($hotel_info[0] > 0)
			include_once "hotels/public/hotel_info.php";
		else mk_output_message("warning", "No hotels found !");
	}
/*	
elseif($resort_id == 0 && $tour_type_id == 0)
	mk_output_message("warning", "Nothing selected");
*/
else
{	
	if(!isset($hotels_per_row))
		$hotels_per_row = 1;
	
	if(!isset($hotels_per_page))
		$hotels_per_page = 10;
	
	if(!isset($p))
		$p = 0;
				
	$add_sql = "1";
	
	if(isset($resort_id))
		{
			if($resort_id !=0)
				$add_sql.= " AND h.resort_id = '".(int)$resort_id."'";
			$local_url.="&amp;resort_id=".$resort_id;
		}
	else $resort_id = 0;
	
	if(isset($tour_type_id) && !empty($tour_type_id))
		{
			$local_url.="&amp;tour_type_id=".$tour_type_id;
			if($tour_type_id > 0)
				$add_sql.=" AND tt.tour_type_id = '".(int)$tour_type_id."'";		
		}
	else unset($tour_type_id);
				
	if(isset($hotel_category) && $hotel_category !=0)
		{
			$add_sql.= " AND h.hotel_category = '".(int)$hotel_category."'";
			$local_url.="&amp;hotel_category=".$hotel_category;
		}
	
	if(isset($hotel_status) && $hotel_status > 0)
		{
			$add_sql.=" AND h.hotel_status = '".(int)$hotel_status."'";
			$local_url.="&amp;hotel_status=".$hotel_status;
		}

	if($hotel_status < 0)	
		$local_url.="&amp;hotel_status=".$hotel_status;
	
	if(isset($price_from_to))
		$local_url.="&amp;price_from_to=".$price_from_to;
			

	/*
	if(isset($language_id) && $language_id > 0)
		{
			$add_sql.=" AND hd.language_id = '".(int)$language_id."'";
		}
	*/
		
	//get all hotels for selected hotel_SiteID
	$all_hotels_array = $hotel->get_hotels($hotel_SiteID, "$add_sql");
	$hotels_array = $hotel->get_hotels($hotel_SiteID, "$add_sql ORDER by hd.hotel_title ASC, h.hotel_category DESC  LIMIT ".($p*$hotels_per_page).", $hotels_per_page");
	$types_array = $hotel->get_types();
	
	$price_from_to_array = split("-", $price_from_to);
	$price_from = $price_from_to_array[0];
	$price_to = $price_from_to_array[1];
					
	//count pages
	//if($price_from_to == "0") 
		$count_all_hotels = count($all_hotels_array);
	/*
	else{
			$count_all_hotels = 0;
			for($i=0; $i<count($all_hotels_array); $i++)
					//check hotel prices
					if(search_hotel_price($all_hotels_array[$i]['hotel_id'], $price_from, $price_to))		
							$count_all_hotels ++;
		}			
	*/
	$count_hotels = count($hotels_array);
	$count_types = count($types_array);
		
	//$count_pages = (count($all_hotels_array))/$hotels_per_page;
	$count_pages = $count_all_hotels/$hotels_per_page;
	$int_count_pages = (int)$count_pages;
	if($count_pages > $int_count_pages)
		$count_pages = $int_count_pages + 1;
	
	//FULL HOTEL LISTING
	$pagination = "<div class=\"pagination\">";
	for($i=0; $i<$count_pages; $i++)
		{
			$class = "";
			if($p==$i) $class = "selected";
			$pagination.="<a href=\"".$local_url."&amp;p=$i#listing\" class=\"".$class."\">".($i+1)."</a>";


		}
	$pagination.="</div>";

	$content = "<table border=\"0\" width=\"100%\" class=\"hotels_listing\">";
	$hpr = 1;
	
	if($count_all_hotels == 0)
		$content.="<tr><td colspan=\"".$hotels_per_row."\" align=\"center\">No hotels found !</center>";
	else	
	{
		for($i=0; $i<$count_types; $i++)
		{
			$type_label = $types_array[$i]['tour_type_title'];
			$content_th = "<tr><th width=\"100%\" valign=\"top\" colspan=\"".$hotels_per_row."\" align=\"center\"> - ".$type_label." - <tr><td width=\"".(100/$hotels_per_row)."%\" valign=\"top\">";											
			$content_td = "";
			$available_hotels = 0;
			
			//loop trough all selected hotels
			for($j=0; $j<$count_hotels; $j++)
			{			
				if(is_file(DIR_WS_HOTELS_UPLOAD . $hotels_array[$j]['hotel_image']))
					$hotel_image = DIR_WS_HOTELS_UPLOAD . $hotels_array[$j]['hotel_image'];
				else $hotel_image = DIR_WS_HOTELS_IMAGES . "no_photo.jpg";
	
				$status = $hotels_array[$j]['status_text'];
				$hotel_types = $hotel->get_types($hotels_array[$j]['hotel_id']);

				for($k=0; $k<count(hotel_types); $k++)
					{
						$tt_compare_id = $hotel_types[$k]['tour_type_id'];
						
						//show hotels only from the selected tour_type
						if(isset($tour_type_id)) $tt_compare_id = $tour_type_id;
						
						if($types_array[$i]['tour_type_id'] == $tt_compare_id)
							{
								$available_hotels++;
								$hotel_price = "";
								if(get_hotel_min_max_price($hotels_array[$j]['hotel_id'], "MIN") > 0)
									$hotel_price = "<div class=\"price\">&nbsp;".LABEL_PRICE_FROM . format_price(get_hotel_min_max_price($hotels_array[$j]['hotel_id'], "MIN"), $hotels_array[$j]['hotel_id'])."</div>";
								
								$content_td.= "<table id=\"preview_".$j."\" border=\"0\" cellspacing=\"2\" cellpadding=\"5\" class=\"hotel_preview\" width=\"100%\" height=\"150\" onMouseOver=\"swap_style('hotel_preview_hover', 'preview_".$j."')\" onMouseOut=\"swap_style('hotel_preview', 'preview_".$j."')\"><tr><td align=\"center\" width=\"160\" valign=\"top\">".get_hotel_stars($hotels_array[$j]['hotel_id'])."<a href=\"$page_url&amp;hotel_id=".$hotels_array[$j]['hotel_id']."#".$hotels_array[$j]['hotel_title']."\" name=\"".$hotels_array[$j]['hotel_title']."\"><br><img src=\"http://www.maksoft.net/img_preview.php?image_file=".$hotel_image."&img_width=140\" border=\"0\" class=\"border_image\" alt=\"".$hotels_array[$j]['hotel_title']."\"></a><br><br>";
								$content_td.= "<td valign=\"top\" width=\"100%\" valign=\"top\"><a href=\"$page_url&amp;hotel_id=".$hotels_array[$j]['hotel_id']."#".$hotels_array[$j]['hotel_title']."\" class=\"head_text\">".$hotels_array[$j]['hotel_title']."</a>";
								$content_td.= "<hr>".$hotel_price.LABEL_HOTEL_RESORT."<em>".$hotels_array[$j]['resort_title']."</em><br>".LABEL_HOTEL_CATEGORY."<em>".$hotels_array[$j]['hotel_category']."*</em><br>".LABEL_HOTEL_STATUS."<em>".$status."</em><br><br>".cut_text(strip_tags(crop_text($hotels_array[$j]['hotel_description'])), 200);
								$content_td.= "<tr><td colspan=\"2\"><a href=\"$page_url&amp;hotel_id=".$hotels_array[$j]['hotel_id']."#".$hotels_array[$j]['hotel_title']."\" class=\"next_link\">".$Site->MoreText."</a></table>";
		
								if($hpr>=$hotels_per_row) { $content_td.="<tr><td width=\"".(100/$hotels_per_row)."%\" valign=\"top\">"; $hpr=1; }
								else { $content_td.="<td width=\"".(100/$hotels_per_row)."%\" valign=\"top\">"; $hpr++;}
							}
						}	
			}
			
			if($available_hotels >0) $content.= $content_th . $content_td;
		}
	}
	
	$content.="</table>";
	echo "<a name=\"listing\"></a>";
	if(isset($resort_id))
		{
			//echo $pagination;
			list_tour_types($tour_type_id, "tour_type_id", "onChange = \"document.location = '".$page_url."&resort_id=".$resort_id."&tour_type_id='+this.value\"") . list_resorts($resort_id, "onChange = \"document.location = '".$page_url."&resort_id='+this.value+'&tour_type_id=".$tour_type_id."'\"");
			echo $content . $pagination;
		}
}
?>