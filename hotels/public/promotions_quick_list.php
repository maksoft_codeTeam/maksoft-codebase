<script type="text/javascript" src="lib/jquery/jquery-accordion/jquery.accordion.js"></script>
	
<script type="text/javascript" language="JavaScript">
$j(document).ready(function(){
		
		$j('#promos_list').accordion({
		active: 100,
		clearStyle: false,
		autoHeight: false
		});
})
</script>

<?php
	
	if(isset($hotel_id))
	{
		require_once "hotels/includes/config.php";
		require_once "hotels/includes/db_tables.php";
		require_once "hotels/includes/functions.php";
		require_once "lib/lib_hotels.php";
		
		$hotel = new hotels();
		$hotel_info = $hotel->get_hotel($hotel_id, $language_id);
		
		$promotions = $hotel->get_hotel_promotions($hotel_info['hotel_id']);
		
		if($hotel_info['hotel_status'] > 0 && count($promotions) > 0)
			{
				?>
					
					<div class="promotions_listing" id="promos_list">
							<?php
								
								for($i=0; $i<count($promotions); $i++)
									echo "<a>".$promotions[$i]['promo_title']."</a><div><p>".$promotions[$i]['promo_description']."</p></div>";
							?>
					</div>
					
				<?php
			}
	}
	
?>