<?php
	//cuurent file dictionary
	include DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir'] . "/request_reservation_form.php";
?>
<script language="JavaScript" src="lib/lib_calendar.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.title; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' <?=FORM_MESSAGE_VALID_EMAIL?> \n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' <?=FORM_MESSAGE_MUST_CONTAIN_NUMBERS?>\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' <?=FORM_MESSAGE_MUST_CONTAIN_NUMBER_BETWEEN?> '+min+' - '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' <?=FORM_MESSAGE_REQUESTED_FIELD?>\n'; }
  } if (errors) alert('<?=FORM_MESSAGE_ALL_REQUESTED_FIELD?>\n'+errors);
  document.MM_returnValue = (errors == '');
}

function setDateLeave()
{
	var Months = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	obj1 = document.getElementById('date_arrive');
	
	if(obj1.value == "") return false;
	
	obj2 = document.getElementById('date_leave');
	count_nights = document.getElementById('count_nights')
	
	day = parseFloat(obj1.value.substring(8,10))
	month = parseFloat(obj1.value.substring(5,7))
	year = obj1.value.substring(0,4)
	
	day_leave = parseInt(day) + parseInt(count_nights.value)
	monthdays = Months[parseInt(month - 1)]
	
	if(day_leave > monthdays)
		{
			day_leave = parseInt(day_leave) - parseInt(monthdays)
			month = parseFloat(month + 1);
			if(month > 12)
			{
				month = 1
				year = parseInt(year) + 1
			}
		}
	
	if(day_leave < 10) day_leave = "0" + day_leave
	if(month < 10) month = "0" + month
		
	//date_leave = day_leave + "-" + month + "-" + year;
	date_leave = year + "-" + month + "-" + day_leave;
	obj2.value = date_leave
}

function show_kids_ages()
{
	table = document.getElementById('kids_ages')
	table.style.display = '';
	kids_counter = document.getElementById('person_kids')
	
	dplRow = 0;
	for(i=0; i<4; i++)
	{	
		if (table.rows.length > 1) table.deleteRow(table.rows.length - 1);
	}
	
	if(kids_counter.value == 0) table.style.display = 'none';	
	
	for(i=1; i<kids_counter.value; i++)
	{
   		var newRow = table.tBodies[0].rows[dplRow].cloneNode(true);		
		table.tBodies[0].appendChild(newRow);
	}
	//alert(kids_counter.value)
}

//-->
</script>
<?php
//antibot code
$code = rand(0, 65535);   

//person_old select menu
$select_person_old = "<select name=\"person_old\">";
for($j=1; $j<10; $j++)
$select_person_old.= "<option value=\"$j\">$j</option>"; 
$select_person_old.= "<option value=\"10+\">10+</option>";
$select_person_old.= "</select>";

//kids select menu
$select_kids = "<select name=\"person_kids\" id=\"person_kids\" onChange=\"show_kids_ages()\">";
for($j=0; $j<=4; $j++)
$select_kids.= "<option value=\"$j\">$j</option>";
$select_kids.= "</select>";

//count nights select menu
$count_nights = "<select name=\"count_nights\" id=\"count_nights\" onChange=\"setDateLeave()\">";
$count_nights.= "<option value=\"1\" selected>1". TEXT_NIGHTS . "</option>";
for($j=2; $j<=15; $j++)
$count_nights.= "<option value=\"$j\">". $j . TEXT_NIGHTS . "</option>";
$count_nights.= "</select>";

?>
<a name="request"></a>
<form name="request_form" method="post" action="form-cms.php">
<input name="send_hotel_request" type="hidden" value="1">
<input name="hotel_name" type="hidden" value="<?=$hotel_info['hotel_title']?>">
<input name="resort_name" type="hidden" value="<?=$hotel_info['resort_title']?>">
<?php
	/*
	if(isset($send_hotel_request_result))
	{
		if($send_hotel_request_result == 1)
			mk_output_message("normal", "Your request has been sent !");
		if($send_hotel_request_result == 0)
			mk_output_message("error", "Your request has NOT been sent !");
	}
	*/
	if($sent == 1) mk_output_message("normal", "Your request has been sent !");
?>
<table width="100%"  border="0" cellpadding="5" cellspacing="2" class="hotel_request">
  <tr>
  	<th colspan="2"><?=$hotel_info['hotel_title']?></th>
  </tr>
  <tr>
    <td align="right" valign="top" width="30%"><?=FORM_LABEL_DATE_ARRIVE?> *</td>
    <td align="left" width="70%"><input name="date_arrive" id="date_arrive" title="<?=FORM_LABEL_DATE_ARRIVE?>" type="text" value="" size="30" style="float: left;" onFocus="setDateLeave()" onClick="if(this.value == '') MyCalendar('request_form', 'date_arrive');"> <a href="javascript: void()" onClick="javascript:MyCalendar('request_form', 'date_arrive'); return false" onFocus="setDateLeave()" class="calendar" title="<?=FORM_LABEL_SLECT_ARRIVE_DATE?>"><img src="<?=DIR_WS_HOTELS_IMAGES?>icon_calendar.jpg" border="0"></a></td>
  </tr>
  <tr>
    <td align="right" valign="top"><?=FORM_LABEL_COUNT_NIGHTS?></td>
    <td><?=$count_nights?></td>
  </tr>
  <tr>
    <td align="right" valign="top"><?=FORM_LABEL_DATE_LEAVE?></td>
    <td><input name="date_leave" id="date_leave" type="text" value="" size="35" style="float: left;" onBlur="setDateLeave()"></td>
  </tr>
  <tr>
    <td align="right" valign="top"><?=FORM_LABEL_ROOM?></td>
    <td>
		<table width="235" border=0 cellpadding="0" cellspacing="2">
			<tr><td width=120 class=label><?=FORM_LABEL_ROOM_TYPE?>
			<td class=label><?=FORM_LABEL_ROOM_COUNT?>
		  <tr><td><?=FORM_SELECT_ROOM_TYPE?><td><input name="room_count" id="room_count" title="<?=FORM_LABEL_ROOM_COUNT?>" type="text" value="1" size="11">
		</table>
	</td>
  </tr>
  <tr>
    <td align="right" valign="top"><?=FORM_LABEL_VISITORS?></td>
    <td>
		<table width="235" border=0 cellpadding="0" cellspacing="2">
			<tr><td width=120 class=label><?=FORM_LABEL_KIDS?><td class=label><?=FORM_LABEL_PEOPLE?>
			<tr><td><?=$select_kids?><td><?=$select_person_old?>
			<tr><td align="left" colspan="2">
				<table id="kids_ages" name="kids_ages" cellpadding=0 cellspacing=0 style="display: none; border: none;">
					<tr><td align="left" style="border: none;"><?=FORM_LABEL_YEARS?><br><input type="text" name="kids_age[]" size="11" maxlength="2">
				</table>
		</table>	
	</td>
  </tr>
  <tr>
    <td align="right" valign="top"><?=FORM_LABEL_NAME_FAMILY?> *</td>
    <td><div class="text_field"><input name="name" id="name" title="<?=FORM_LABEL_NAME_FAMILY?>"  type="text" value="" size="35"></div></td>
  </tr>
  <tr>
    <td align="right" valign="top"><?=FORM_LABEL_PHONE?> *</td>
    <td><div class="text_field"><input name="phone" id="phone" title="<?=FORM_LABEL_PHONE?>" type="text" value="" size="35"></div></td>
  </tr>
  <tr>
    <td align="right" valign="top"><?=FORM_LABEL_EMAIL?> *</td>
    <td><div class="text_field"><input name="EMail" id="EMail" title="<?=FORM_LABEL_EMAIL?>" type="text" value="" size="35"></div></td>
  </tr>
  <tr>
    <td align="right" valign="top"><?=FORM_LABEL_COMMENTS?></td>
    <td><div class="textarea"><textarea name="comments" cols="27" rows="10"></textarea></div></td>
  </tr>
  <tr>
    <td align="right" valign="bottom">&nbsp;  
    <td><img src="http://www.maksoft.net/gen.php?code=<?=$code?>" width="235">  
  </tr>
  <tr>
  	<td align="right"><?=FORM_LABEL_SECURE_CODE?> *
	<td><div class="text_field"><input name="codestr" id="codestr" type="text" title="<?=FORM_LABEL_SECURE_CODE?>"size="35"></div>
  </tr>
  <tr>
  	<td colspan="2" align="center" height="50" valign="middle">
	<input type="button" class="button_submit" onClick="MM_validateForm('date_arrive', '', 'R', 'room_count', '', 'RisNum', 'name', '', 'R', 'phone', '', 'R', 'EMail', '', 'RisEmail', 'codestr', '', 'R'); if(document.MM_returnValue) document.request_form.submit()" value="<?=FORM_BUTTON_SEND?>">
  </tr>
</table>
        <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
		<input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
        <input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$Site->EMail"); ?>">
		<input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$n&SiteID=$SiteID&hotel_id=$hotel_id&sent=1#request"); ?>">
        <input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
		<input name="code" type="hidden" value="<?=$code?>">
		<input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">
</form>