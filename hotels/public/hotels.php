<?php
require_once "hotels/includes/config.php";
//db table names
require_once DIR_WS_HOTELS_INCLUDES . "db_tables.php";
require_once "hotels/includes/functions.php";
require_once "lib/lib_hotels.php";

$hotel = new hotels();
$local_url = "page.php?n=$n&amp;SiteID=$SiteID";
/*
if($user->AccessLevel > 0)
	$local_url = "�����";
*/
if(isset($_GET['language_id']))
	$language_id = $_GET['language_id'];
else if(!isset($language_id))
	$language_id = 1;

$current_language = get_language($language_id);
define("DIR_WS_LANGUAGE_IMAGES", DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir']."/images/");
require_once DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir'].".php";

//send hotel request
if($_POST['send_hotel_request'] == 1)
	{
		//$send_hotel_request_result = $hotel->send_request("petrovm@abv.bg");
	}
	
//HOTEL INFO
if((isset($_GET['hotel_id']) && $_GET['hotel_id'] != 0) || (isset($hotel_id) && $hotel_id != 0))
	{
		//get selected hotel
		//$hotel_id = (int)$_GET['hotel_id'];
		$hotel_id = (int)$hotel_id;
		$hotel_info = $hotel->get_hotel($hotel_id, $language_id);
		if($hotel_info[0] > 0)
			include_once "hotels/public/hotel_info.php";
		else mk_output_message("warning", "No hotels found !");
	}
else
{	
	if(!isset($hotels_per_row))
		$hotels_per_row = 2;
	
	if(!isset($hotels_per_page))
		$hotels_per_page = 10;
	
	if(!isset($p))
		$p = 0;
				
	$add_sql = "1";
	if(isset($hotel_category) && $hotel_category !=0)
		{
			if(isset($_GET['hotel_category']))
				$hotel_category = (int)$_GET['hotel_category'];	
			$add_sql.= " AND h.hotel_category = '".$hotel_category."'";
		}
	
	if(isset($hotel_status) && $hotel_status > 0)
		{
			$add_sql.=" AND h.hotel_status = '".(int)$hotel_status."'";
		}
	
	if(isset($hotel_type) && $hotel_type > 0)
		{
			$add_sql.=" AND tt.tour_type_id = '".(int)$hotel_type."'";
		}	
	
	/*
	if(isset($language_id) && $language_id > 0)
		{
			$add_sql.=" AND hd.language_id = '".(int)$language_id."'";
		}
	*/
		
	//get all hotels for selected hotel_SiteID
	if(isset($resort_id) && $resort_id != 0)
		{
			$all_hotels_array = $hotel->get_hotels($hotel_SiteID, "h.resort_id = '".$resort_id."' AND h.hotel_status > 0 AND $add_sql ORDER by h.hotel_category DESC, hd.hotel_title ASC");
			$hotels_array = $hotel->get_hotels($hotel_SiteID, "h.resort_id = '".$resort_id."' AND h.hotel_status > 0 AND $add_sql ORDER by h.hotel_category DESC, hd.hotel_title ASC LIMIT ".($p*$hotels_per_page).", $hotels_per_page");
		}
	//else if(isset($tour_type_id) && $tour_type_id != 0)
		//$hotels_array = $hotel->get_hotels($hotel_SiteID, "tt.tour_type_id = '".$tour_type_id."'");
	else 
		{
			//get CMS page name AND search in hotels TABLE
			$resort_name = $row->Name;
			$all_hotels_array = $hotel->get_hotels($hotel_SiteID, "rd.resort_title LIKE '".$resort_name."' AND h.hotel_status > 0 AND $add_sql ORDER by h.hotel_category DESC, hd.hotel_title ASC");
			$hotels_array = $hotel->get_hotels($hotel_SiteID, "rd.resort_title LIKE '".$resort_name."' AND h.hotel_status > 0 AND $add_sql ORDER by h.hotel_category DESC, hd.hotel_title ASC LIMIT ".($p*$hotels_per_page).", $hotels_per_page");
			$resort_id = $hotels_array[0]['resort_id'];
			//$hotels_array = $hotel->get_hotels($hotel_SiteID);
		}
	
	//count pages
	$count_pages = round((count($all_hotels_array))/$hotels_per_page);
	if((count($all_hotels_array)/$hotels_per_page) > $count_pages)
		$count_pages++;
		
	//FULL HOTEL LISTING
	$pagination = "<div class=\"pagination\">";
	for($i=0; $i<$count_pages; $i++)
		{
			$class = "";
			if($p==$i) $class = "selected";
			if(isset($hotel_category))
				$pagination.="<a href=\"".$local_url."&amp;hotel_category=$hotel_category&amp;p=$i#listing\" class=\"".$class."\">".($i+1)."</a>";
			else	
				$pagination.="<a href=\"".$local_url."&amp;p=$i#listing\" class=\"".$class."\">".($i+1)."</a>";
		}
	$pagination.="</div>";
		
	$hotel_navigation = '<table width="100%" cellpadding="5" cellspacing="0"><tr><td align="left"><a href="page.php?n='.$row->ParentPage.'&amp;SiteID='.$SiteID.'" title="'.TEXT_BACK_TO_RESORTS.'"><img src="'.SITE_ROOT.DIR_WS_LANGUAGE_IMAGES.'icon_back.jpg" border="0"></a>
	<td align="right">
	</table>';

	$content = "<table border=\"0\" width=\"100%\" class=\"hotels_listing\"><tr><th colspan=\"".$hotels_per_row."\" align=\"left\">".TITLE_HOTELS_IN_RESORT . $row->Name ."&nbsp;".get_hotel_category($hotel_category, "onChange=\"document.location='".$local_url."&amp;hotel_category='+this.value+'#listing'\"");	
	$j = 1;
	$h_category = $hotels_array[0]['hotel_category'];
	if(count($hotels_array) == 0)
		$content.="<tr><td colspan=\"".$hotels_per_row."\" align=\"center\">No hotels found !</center>";
	else	
	{
		$content.= "<tr><th width=\"100%\" valign=\"top\" colspan=\"".$hotels_per_row."\" align=\"center\"> - ".$h_category."* - <tr><td width=\"".(100/$hotels_per_row)."%\" valign=\"top\" class=\"displaytd\">";
		for($i=0; $i<count($hotels_array); $i++)
		{
			if(is_file(DIR_WS_HOTELS_UPLOAD . $hotels_array[$i]['hotel_image']))
				$hotel_image = DIR_WS_HOTELS_UPLOAD . $hotels_array[$i]['hotel_image'];
			else $hotel_image = DIR_WS_HOTELS_IMAGES . "no_photo.jpg";
			
			$hotel_status_label = "";
			if($hotels_array[$i]['hotel_status'] == 2)
				$hotel_status_label = "<img src=\"".SITE_ROOT.DIR_WS_HOTELS_IMAGES."icon_top_offer.png\" style=\"position: absolute; z-index:2; margin: 0px 0 0 10px;\" border=\"0\" width=\"30\" alt=\"Top offer\">";
			
			if($hotels_array[$i]['hotel_status'] == 3)
				$hotel_status_label = "<img src=\"".SITE_ROOT.DIR_WS_HOTELS_IMAGES."icon_last_minute.png\" style=\"position: absolute; z-index:2; margin: 0px 0 0 10px;\" border=\"0\" width=\"30\" alt=\"Last minute offer\">";
			
			if($hotels_array[$i]['hotel_status'] == 4)
				$hotel_status_label = "<img src=\"".SITE_ROOT.DIR_WS_HOTELS_IMAGES."icon_promo.png\" style=\"position: absolute; z-index:2; margin: 0px 0 0 10px;\" border=\"0\" width=\"30\" alt=\"Promo offer\">";
			
			if($hotels_array[$i]['hotel_status'] == 5)
				$hotel_status_label = "<img src=\"".SITE_ROOT.DIR_WS_HOTELS_IMAGES."icon_stop_sale.png\" style=\"position: absolute; z-index:2; margin: 0px 0 0 10px;\" border=\"0\" width=\"30\" alt=\"Stop sale\">";
			
			/*
			switch($hotels_array[$i]['hotel_status'])
				{
					case 0: {$status = "inactive"; break;}
					case 2: {$status = "top hotel"; break;}
					case 3: {$status = "last minute"; break;}
					case 4: {$status = "seasonal promo"; break;}
					case 5: {$status = "stop sale"; break;}
					case 1: 
					default:{$status = "active"; break;}					
				}
			*/
			$status = $hotels_array[$i]['status_text'];
					
			if($h_category != $hotels_array[$i]['hotel_category'])
				{
					$h_category = $hotels_array[$i]['hotel_category'];
					$content.= "<tr><th width=\"100%\" valign=\"top\" colspan=\"".$hotels_per_row."\" align=\"center\"> - ".$h_category."* - <tr><td width=\"".(100/$hotels_per_row)."%\" class=\"displaytd\">";
					$j=1;
				}
			
			$content.= "<table id=\"preview_".$i."\" border=\"0\" cellspacing=\"2\" cellpadding=\"5\" class=\"hotel_preview\" width=\"100%\" height=\"200\" onMouseOver=\"swap_style('hotel_preview_hover', 'preview_".$i."')\" onMouseOut=\"swap_style('hotel_preview', 'preview_".$i."')\"><tr><td align=\"center\" width=\"150\" valign=\"top\">".get_hotel_stars($hotels_array[$i]['hotel_id'])."<a name=\"".$hotels_array[$i]['hotel_title']."\" href=\"$local_url&amp;hotel_id=".$hotels_array[$i]['hotel_id']."#".$hotels_array[$i]['hotel_title']."\"><br><div class=\"price\">".format_price(get_hotel_min_max_price($hotels_array[$i]['hotel_id'], "MIN"), $hotels_array[$i]['hotel_id'])."</div>".$hotel_status_label."<img src=\"".SITE_ROOT."img_preview.php?image_file=".$hotel_image."&img_width=130\" border=\"0\" class=\"border_image\" alt=\"". $hotels_array[$i]['hotel_title'].",&nbsp;". TITLE_HOTELS_IN_RESORT . $hotels_array[$i]['resort_title']."\"></a><br><br>";
			$hotel_types = $hotel->get_types($hotels_array[$i]['hotel_id']);
			for($k=0; $k<count($hotel_types); $k++)
				$content.= get_type_image($hotel_types[$k]['tour_type_key'], "alt=\"".$hotel_types[$k]['tour_type_key']."\"");
							
			$content.= "<td valign=\"top\" width=\"100%\" valign=\"top\"><a href=\"$local_url&amp;hotel_id=".$hotels_array[$i]['hotel_id']."#".$hotels_array[$i]['hotel_title']."\" class=\"head_text\" title=\"". $hotels_array[$i]['hotel_title'].",&nbsp;". TITLE_HOTELS_IN_RESORT . $hotels_array[$i]['resort_title']."\">".$hotels_array[$i]['hotel_title']."</a>";
			//$content.= "<td valign=\"top\" width=\"100%\" valign=\"top\"><a href=\"$local_url/$n/".$hotels_array[$i]['hotel_id']."/".$hotels_array[$i]['hotel_title']."\" class=\"head_text\" title=\"". $hotels_array[$i]['hotel_title'].",&nbsp;". TITLE_HOTELS_IN_RESORT . $hotels_array[$i]['resort_title']."\">".$hotels_array[$i]['hotel_title']."</a>";
			$content.= "<hr>".LABEL_HOTEL_RESORT."<em>".$hotels_array[$i]['resort_title']."</em><br>".LABEL_HOTEL_CATEGORY."<em>".$hotels_array[$i]['hotel_category']."*</em><br>".LABEL_HOTEL_STATUS."<em>".$status."</em><br><br>".cut_text(strip_tags(crop_text($hotels_array[$i]['hotel_description'])), 200);
			$content.= "<tr><td colspan=\"2\"><a href=\"$local_url&amp;hotel_id=".$hotels_array[$i]['hotel_id']."#".$hotels_array[$i]['hotel_title']."\" class=\"next_link\" title=\"���������� �� ����� ". $hotels_array[$i]['hotel_title'].",&nbsp;". $hotels_array[$i]['resort_title']."\">".$Site->MoreText."</a></table>";
	
			if($j>=$hotels_per_row) { $content.="<tr><td width=\"".(100/$hotels_per_row)."%\" valign=\"top\" class=\"displaytd\">"; $j=1; }
			else { $content.="<td width=\"".(100/$hotels_per_row)."%\" valign=\"top\" class=\"displaytd\">"; $j++;}
	
		}
	}
	$content.="</table>";
	echo "<a name=\"listing\"></a>";
	echo $hotel_navigation.$pagination.$content.$pagination;
}
?>