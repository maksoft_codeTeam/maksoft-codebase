<?php
require_once "hotels/includes/config.php";
require_once "hotels/includes/functions.php";
require_once "hotels/includes/db_tables.php";
require_once "lib/lib_hotels.php";

$hotel = new hotels();
$page_url = "page.php?n=$n&amp;SiteID=$SiteID";
$local_url = $page_url;


if(isset($_GET['language_id']))
	$language_id = $_GET['language_id'];
else if(!isset($language_id))
	$language_id = 1;

$current_language = get_language($language_id);
define("DIR_WS_LANGUAGE_IMAGES", DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir']."/images/");
require_once DIR_WS_HOTELS_LANGUAGES . $current_language['language_dir'].".php";

//include search form
require_once "hotels/public/search_form.php";

//init search variables
//$resort_id = (int)$resort_id;
//$tour_type_id = (int)$tour_type_id;
 
 if(isset($_GET['resort_id']) || isset($_GET['tour_type_id']) || isset($_GET['hotel_category']) || isset($_GET['hotelname'])){
 	//var_dump($_GET);
	$resort_id = $_GET['resort_id'];
	$tour_type_id = $_GET['tour_type_id'];
	$hotel_category = $_GET['hotel_category'];
	$hotelname = $_GET['hotelname'];
	if(!isset($hotels_per_page)){
		$hotels_per_page = 10;
	}
	if(!isset($_GET['page'])){
		$page= 0;
	}else{
		$page=$_GET['page'];
	}
	

		$searchDataArray = $hotel->search_hotels($resort_id , $tour_type_id , $hotel_category, $hotelname, $hotels_per_page, $page);
//	echo $numberofpages = $allCount;
	/*foreach($searchDataArray['hotelsIDS'] as $singlehotel){
		echo $singlehotel."<br/>";
		//$hotel_info = $hotel->get_hotel($searchedHotels, $language_id);
	}*/
	
	
	$overallPageNumbers=(int)$searchDataArray['allCount'];
	$numberofpages=$overallPageNumbers/$hotels_per_page;
	$pages = "<div class=\"pagination\">";
	for($i=0; $i<$numberofpages; $i++)
		{
			if($page==$i){
				$class = "selected";
			} else{
				$class = '';
			}
			$pages.="<a href=\"".$local_url."&hotelname=".$hotelname."&resort_id=".$resort_id."&tour_type_id=".$tour_type_id."&hotel_category=".$hotel_category."&page=".$i."#listing\" class=\"".$class."\">".($i+1)."</a>";
		}
	$pages.="</div>";
	
	$counter=0;
	$hotels = "";
	foreach($searchDataArray['hotelsIDS'] as $singlehotel){
		$counter++;
		$info=$hotel->singleHotelInfo($singlehotel, $language_id);
		$mainID=$info['mainID'];
		$getHotelIcons = $hotel->hotel_tour_icons($mainID);
		$iconss="";
		foreach($getHotelIcons as $icon){
			$iconss.="<img src='http://usb-travel.com/hotels/images/icon_".$icon.".jpg' hspace='1' alt='".$icon."'/>";
		}
		$name=$info['Title'];
		$image=$info['image'];
		$category=$info['numberOfStars'];
		$resortName=$info['resortTitle'];
		$hDescr=strip_tags($info['hDescr']);
		$periods='';
		$hDescr=html_entity_decode($hDescr);
		if((strlen($hDescr))>=190){
			$periods='...';
		}
		$hDescr=mb_substr($hDescr, 0, 190);
		$hotels.='
		<td width="50%" valign="top" class="displaytd">
			<table id="preview_'.$counter.'" border="0" cellspacing="2" cellpadding="5" class="hotel_preview" width="100%" height="200" onmouseover="swap_style(\'hotel_preview_hover\',\'preview_'.$counter.'\')" onmouseout="swap_style("hotel_preview","preview_'.$counter.'")">
			
				<tbody>
					<tr>
						<td align="center" width="150" valign="top">
							<img src="http://usb-travel.com/hotels/images/'.$category.'stars.gif" alt="'.$category.' stars category"/>
							<a name="'.$name.'" href="page.php?n=76216&SiteID=702&hotel_id='.$mainID.'#'.$name.'">
								<br>
								<img src="http://www.maksoft.net/img_preview.php?image_file=web/images/upload/702/hotels/'.$image.'&img_width=130" border="0" class="border_image" alt="'.$name.'"/>
							</a>
							'.$iconss.'
						</td>
						<td valign="top" width="100%"><a href="'.$local_url.'&hotel_id='.$mainID.'#'.$name.'" class="head_text">'.$name.'</a>
							<hr><b>������: </b><em>'.$resortName.'</em>
							<br><b>���������: </b><em>'.$category.'*</em>
							<br><b>������: </b><em>�������</em>
							<br>
							<br>
							<p>
							'.$hDescr.$periods.'
							</p>
						</td>
					</tr>
					<tr>
						<td colspan="2"><a href="'.$local_url.'&hotel_id='.$mainID.'#'.$name.'" class="next_link">��� ��� �</a>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
		';
		if($counter%2==0){
			$hotels.="</tr><tr>";
		}
		
	}
	
	echo $pages;
	echo '
	<table border="0" width="100%" class="hotels_listing">
    <tbody>
        <tr>
            <th colspan="2" align="left">��������� �� ���������: '.$overallPageNumbers.'</th>
        </tr>
		<tr>
		'.$hotels.'
		</tr>
	</tbody>
	</table>
	';
	//echo $pages;
 }
 
 
//HOTEL INFO
if((isset($_GET['hotel_id']) && $_GET['hotel_id'] != 0) || (isset($hotel_id) && $hotel_id != 0))
	{
		//get selected hotel
		//$hotel_id = (int)$_GET['hotel_id'];
		$hotel_id = (int)$hotel_id;
		$hotel_info = $hotel->get_hotel($hotel_id, $language_id);
		if($hotel_info[0] > 0)
			include_once "hotels/public/hotel_info.php";
		else mk_output_message("warning", "No hotels found !");
	}
/*	
elseif($resort_id == 0 && $tour_type_id == 0)
	mk_output_message("warning", "Nothing selected");
*/
else
{	/* 
	if(!isset($hotels_per_row))
		$hotels_per_row = 2;
	
	if(!isset($hotels_per_page))
		$hotels_per_page = 10;
	
	if(!isset($p))
		$p = 0;
				
	$add_sql = "1";
	
	if(isset($resort_id))
		{
			if($resort_id !=0)
				$add_sql.= " AND h.resort_id = '".(int)$resort_id."'";
			$local_url.="&amp;resort_id=".$resort_id;
		}
	
	if(isset($tour_type_id))
		{
			$local_url.="&amp;tour_type_id=".$tour_type_id;
			if($tour_type_id > 0)
				$add_sql.=" AND tt.tour_type_id = '".(int)$tour_type_id."'";		
		}
				
	if(isset($hotel_category) && $hotel_category !=0)
		{
			$add_sql.= " AND h.hotel_category = '".(int)$hotel_category."'";
			$local_url.="&amp;hotel_category=".$hotel_category;
		}
	
	if(isset($hotel_status) && $hotel_status > 0)
		{
			$add_sql.=" AND h.hotel_status = '".(int)$hotel_status."'";
			$local_url.="&amp;hotel_status=".$hotel_status;
		}

	if($hotel_status < 0)	
		$local_url.="&amp;hotel_status=".$hotel_status;
	
	if(isset($price_from_to))
		$local_url.="&amp;price_from_to=".$price_from_to;
			

	// WAS COMMENTED START
	if(isset($language_id) && $language_id > 0)
		{
			$add_sql.=" AND hd.language_id = '".(int)$language_id."'";
		}
	
	// WAS COMMENTED END	
	//get all hotels for selected hotel_SiteID

	$all_hotels_array = $hotel->get_hotels($hotel_SiteID, "$add_sql");
	$hotels_array = $hotel->get_hotels($hotel_SiteID, "$add_sql ORDER by h.hotel_category DESC, hd.hotel_title ASC LIMIT ".($p*$hotels_per_page).", $hotels_per_page");

	$price_from_to_array = split("-", $price_from_to);
	$price_from = $price_from_to_array[0];
	$price_to = $price_from_to_array[1];
					
	//count pages
	if($price_from_to == "0") 
		$count_all_hotels = count($all_hotels_array);
	else{
			$count_all_hotels = 0;
			for($i=0; $i<count($all_hotels_array); $i++)
					//check hotel prices
					if(search_hotel_price($all_hotels_array[$i]['hotel_id'], $price_from, $price_to))		
							$count_all_hotels ++;
		}			

	$count_hotels = count($hotels_array);
	
		
	//$count_pages = (count($all_hotels_array))/$hotels_per_page;
	$count_pages = $count_all_hotels/$hotels_per_page;
	$int_count_pages = (int)$count_pages;
	if($count_pages > $int_count_pages)
		$count_pages = $int_count_pages + 1;
	
	//FULL HOTEL LISTING
	$pagination = "<div class=\"pagination\">";
	for($i=0; $i<$count_pages; $i++)
		{
			$class = "";
			if($p==$i) $class = "selected";
			$pagination.="<a href=\"".$local_url."&amp;p=$i#listing\" class=\"".$class."\">".($i+1)."</a>";


		}
	$pagination.="</div>";

	$content = "<table border=\"0\" width=\"100%\" class=\"hotels_listing\"><tr><th colspan=\"".$hotels_per_row."\" align=\"left\">".TITLE_SEARCH_HOTELS_RESULTS . $count_all_hotels;	
	$j = 1;
	$h_category = $hotels_array[0]['hotel_category'];
	if($count_all_hotels == 0)
		$content.="<tr><td colspan=\"".$hotels_per_row."\" align=\"center\">No hotels found !</center>";
	else	
	{
		$content.= "<tr><th width=\"100%\" valign=\"top\" colspan=\"".$hotels_per_row."\" align=\"center\"> - ".$h_category."* - <tr><td width=\"".(100/$hotels_per_row)."%\" valign=\"top\">";
		for($i=0; $i<$count_hotels; $i++)
		{

			//check hotel prices
			
											
					if(is_file(DIR_WS_HOTELS_UPLOAD . $hotels_array[$i]['hotel_image']))
						$hotel_image = DIR_WS_HOTELS_UPLOAD . $hotels_array[$i]['hotel_image'];
					else $hotel_image = DIR_WS_HOTELS_IMAGES . "no_photo.jpg";
					//  WAS COMMENTED START __________________________________
					switch($hotels_array[$i]['hotel_status'])				//|
						{													//|
							case 0: {$status = "inactive"; break;}			//|
							case 2: {$status = "top hotel"; break;}			//|
							case 3: {$status = "last minute"; break;}		//|
							case 4: {$status = "seasonal promo"; break;}	//|
							case 5: {$status = "stop sale"; break;}			//|
							case 1: 										//|
							default:{$status = "active"; break;}			//|	
						}													//|
					// WAS COMMENTED END _____________________________________|
					$status = $hotels_array[$i]['status_text'];
							
					if($h_category != $hotels_array[$i]['hotel_category'])
						{
							$h_category = $hotels_array[$i]['hotel_category'];
							$content.= "<tr><th width=\"100%\" valign=\"top\" colspan=\"".$hotels_per_row."\" align=\"center\"> - ".$h_category."* - <tr><td width=\"".(100/$hotels_per_row)."%\" valign=\"top\">";
							$j=1;
						}
					
							$hotel_price = "";
							if(get_hotel_min_max_price($hotels_array[$i]['hotel_id'], "MIN") > 0)
								$hotel_price = "<div class=\"price\">".LABEL_PRICE_FROM . format_price(get_hotel_min_max_price($hotels_array[$i]['hotel_id'], "MIN"), $hotels_array[$i]['hotel_id'])."</div>";
							
							$content.= "<table id=\"preview_".$i."\" border=\"0\" cellspacing=\"2\" cellpadding=\"5\" class=\"hotel_preview\" width=\"100%\" height=\"200\" onMouseOver=\"swap_style('hotel_preview_hover', 'preview_".$i."')\" onMouseOut=\"swap_style('hotel_preview', 'preview_".$i."')\"><tr><td align=\"center\" width=\"150\" valign=\"top\">".get_hotel_stars($hotels_array[$i]['hotel_id']).$hotel_price."<a name=\"".$hotels_array[$i]['hotel_title']."\" href=\"$page_url&amp;hotel_id=".$hotels_array[$i]['hotel_id']."#".$hotels_array[$i]['hotel_title']."\"><br><img src=\"http://www.maksoft.net/img_preview.php?image_file=".$hotel_image."&img_width=130\" border=\"0\" class=\"border_image\" alt=\"".$hotels_array[$i]['hotel_title']."\"></a><br><br>";
							$hotel_types = $hotel->get_types($hotels_array[$i]['hotel_id']);
							for($k=0; $k<count($hotel_types); $k++)
								$content.= get_type_image($hotel_types[$k]['tour_type_key'], "alt=\"".$hotel_types[$k]['tour_type_key']."\"");
											
							$content.= "<td valign=\"top\" width=\"100%\" valign=\"top\"><a href=\"$page_url&amp;hotel_id=".$hotels_array[$i]['hotel_id']."#".$hotels_array[$i]['hotel_title']."\" class=\"head_text\">".$hotels_array[$i]['hotel_title']."</a>";
							$content.= "<hr>".LABEL_HOTEL_RESORT."<em>".$hotels_array[$i]['resort_title']."</em><br>".LABEL_HOTEL_CATEGORY."<em>".$hotels_array[$i]['hotel_category']."*</em><br>".LABEL_HOTEL_STATUS."<em>".$status."</em><br><br>".cut_text(strip_tags(crop_text($hotels_array[$i]['hotel_description'])), 200);
							$content.= "<tr><td colspan=\"2\"><a href=\"$page_url&amp;hotel_id=".$hotels_array[$i]['hotel_id']."#".$hotels_array[$i]['hotel_title']."\" class=\"next_link\">".$Site->MoreText."</a></table>";
		
						
					if($j>=$hotels_per_row) { $content.="<tr><td width=\"".(100/$hotels_per_row)."%\" valign=\"top\">"; $j=1; }
					else { $content.="<td width=\"".(100/$hotels_per_row)."%\" valign=\"top\">"; $j++;}
				
		}
	}
	$content.="</table>";
	echo "<a name=\"listing\"></a>";
	if(isset($resort_id))
		echo $pagination.$content.$pagination; */
}
?>