<script src="hotels/includes/tabcontent.js" language="JavaScript" type="text/javascript"></script>
<script type="text/javascript" src="lib/jquery/jquery-accordion/jquery.accordion.js"></script>
	
<script type="text/javascript" language="JavaScript">
$j(document).ready(function(){
		/*
		$j('#promos_list').accordion({
		active: 100,
		autoHeight: false
		});
		*/
})
function view_promo(id)
	{
		$j(document).ready(function(){
			$j('#promo_'+id).toggle();
		})
	}
</script>

<?php
//get hotels from the same resort
$hotels_array = $hotel->get_hotels($hotel_SiteID, "h.resort_id = '".$hotel_info['resort_id']."'");
$prev_index = 0;
$next_index = 0;

for($i=0; $i<count($hotels_array); $i++)
	{
		if($hotels_array[$i]['hotel_id'] == $hotel_info['hotel_id'])
			{
				$prev_index = $i-1;
				$next_index = $i+1;
			}
	}

$hotel_info_alt = "����� ". $hotel_info['hotel_title'].",&nbsp;". TITLE_HOTELS_IN_RESORT . $hotel_info['resort_title']."\"";
$hotel_status_label = "";
if($hotel_info['hotel_status'] == 2)
	$hotel_status_label = "<img src=\"".SITE_ROOT.DIR_WS_HOTELS_IMAGES."icon_top_offer.png\" style=\"position: absolute; z-index:2; margin: 0px 0 0 10px;\" border=\"0\" width=\"50\" alt=\"Top offer\">";

if($hotel_info['hotel_status'] == 3)
	$hotel_status_label = "<img src=\"".SITE_ROOT.DIR_WS_HOTELS_IMAGES."icon_last_minute.png\" style=\"position: absolute; z-index:2; margin: 0px 0 0 10px;\" border=\"0\" width=\"50\" alt=\"Last minute offer\">";

if($hotel_info['hotel_status'] == 4)
	$hotel_status_label = "<img src=\"".SITE_ROOT.DIR_WS_HOTELS_IMAGES."icon_promo.png\" style=\"position: absolute; z-index:2; margin: 0px 0 0 10px;\" border=\"0\" width=\"50\" alt=\"Promo offer\">";

if($hotel_info['hotel_status'] == 5)
	$hotel_status_label = "<img src=\"".SITE_ROOT.DIR_WS_HOTELS_IMAGES."icon_stop_sale.png\" style=\"position: absolute; z-index:2; margin: 0px 0 0 10px;\" border=\"0\" width=\"50\" alt=\"Stop sale\">";

?>
<a name="<?=$hotel_info['hotel_title']?>"></a>
<table width="100%" cellpadding="5" cellspacing="0">
	<tr>
	<td align="left"><a href="page.php?n=<?=$n?>&amp;SiteID=<?=$SiteID?>" title="<?=TEXT_BACK_TO_HOTELS?>"><img src="<?=SITE_ROOT.DIR_WS_LANGUAGE_IMAGES?>icon_back.jpg" border="0"></a>
	<td align="right">
<?php
	echo list_hotels($hotel_info['hotel_id'], "onChange=document.location='page.php?n=".$n."&amp;SiteID=".$SiteID."&amp;hotel_id='+this.value+'#'+this.options(this.selectedIndex).text+''", $conditions = "h.resort_id = '".$hotel_info['resort_id']."'");
?>
</table>

<table width="100%" cellpadding="5" cellspacing="2" class="hotel_info" border="0">
<tr><th colspan="2" valign="middle"><img src="<?=SITE_ROOT.DIR_WS_HOTELS_IMAGES."icon_info.png"?>" align="left" alt=""><?=$hotel_info['hotel_title']?>, <em><?=$hotel_info['resort_title']?></em>
<?php
	if(is_file(DIR_WS_HOTELS_UPLOAD . $hotel_info['hotel_image']))
		$hotel_image = DIR_WS_HOTELS_UPLOAD . $hotel_info['hotel_image'];
	else $hotel_image = DIR_WS_HOTELS_IMAGES . "no_photo.jpg";
?>
<tr>
	<td width="250" valign="top" align="center">
	<?=print_hotel_stars($hotel_info['hotel_id'])?><br>
	<?=$hotel_status_label?><a href="<?=SITE_ROOT.DIR_WS_HOTELS_UPLOAD . $hotel_info['hotel_image']?>" class="fresco" data-fresco-caption="<?=$hotel_info['hotel_title']?>"data-fresco-group="gallery"><img src="<?=SITE_ROOT?>img_preview.php?image_file=<?=$hotel_image?>&img_width=200" border="0" class="border_image" alt="<?=$hotel_info_alt?>"></a><br><br>
	<?php
			$hotel_types = $hotel->get_types($hotel_info['hotel_id']);
			for($k=0; $k<count($hotel_types); $k++)
				echo get_type_image($hotel_types[$k]['tour_type_key'], "alt=\"".$hotel_types[$k]['tour_type_key']."\"");	
	?>
	<td valign="top" align="center" width="100%">
	<div class="hotel_gallery">
	<?php
		$photo_dir = DIR_WS_HOTELS_UPLOAD . $hotel_info['hotel_id']."/";
		include "hotels/hotel_gallery.php";
	?>
	</div></td>
</table>
<a name="offers"></a>
<div id="option_tabs"><a href="#" rel="option_info" class="selected"><?=TITLE_HOTEL_ABOUT?></a><?php if(strlen($hotel_info['hotel_status']) > 0) echo "<a href=\"#\" rel=\"option_offers\">".TITLE_HOTEL_OFFERS."</a>"; ?><a href="#" rel="option_prices"><?=TITLE_HOTEL_PRICES?></a><a href="#" rel="option_request"><?=TITLE_HOTEL_SEND_REQUEST?></a></div>
<table width="100%" id="option_info" cellpadding="5" cellspacing="2" class="hotel_info"><tr><th><?=TITLE_HOTEL_DESCRIPTION?><tr><td><?=$hotel_info['hotel_description']?></table>
<?php
	if(strlen($hotel_info['hotel_status']) > 0)
		{
			?>
				<table width="100%" id="option_offers" cellpadding="5" cellspacing="2" class="hotel_info">
						<tr><th class="extra_text"><img src="<?=SITE_ROOT.DIR_WS_HOTELS_IMAGES."icon_info_red.png"?>" align="left" alt=""><?=TITLE_HOTEL_EXTRA_DESCRIPTION?>
						<tr><td>
						<div class="promotions_listing" id="promos_list">
							<?php
								$promotions = $hotel->get_hotel_promotions($hotel_info['hotel_id']);
								for($i=0; $i<count($promotions); $i++)
									echo "<a href=\"javascript: void(0)\" onClick=\"view_promo(".$i.")\">".$promotions[$i]['promo_title'].", ".date(DATETIME_FORMAT_LONG, strtotime($promotions[$i]['promo_date_from']))." - ".date(DATETIME_FORMAT_LONG, strtotime($promotions[$i]['promo_date_to']))."</a><div id=\"promo_".$i."\" style=\"display: none;\"><p>".$promotions[$i]['promo_description']."</p></div>";
							?>
						</div>
				</table>
			<?php
		}
?>
<div class="table-responsive">
<table width="100%" id="option_prices"><tr><td>
		<?php
		
		if(isset($hotel_id))
				{
					//$period_types = $hotel->get_period_types($hotel_id, true);
					$period_types = $hotel->get_period_types($hotel_id, true, 1, date("Y-m-d"));
					$count_period_types = count($period_types);
						
					$periods_type_td = "";
					$periods_dates_td = "";
					$room_description_tr_td = "";
					for($i=0; $i<$count_period_types; $i++)
						{ 
							$periods_type_td.= "<th>".$period_types[$i]['period_type_title'];
							$periods_dates_td.= "<td>";
							$period_dates = $hotel->get_period_dates($period_types[$i]['period_type_id']);
							
							for($j=0; $j<count($period_dates); $j++)
								$periods_dates_td.= date("d.m", strtotime($period_dates[$j]['date_from'])) ." - ". date("d.m", strtotime($period_dates[$j]['date_to']))."<br>";
						}
					
					$rooms = $hotel->get_rooms($hotel_id);
					for($i=0; $i<count($rooms); $i++)
						{	
							$room_descriptions = $hotel->get_room_descriptions($rooms[$i]['room_id']);
									for($j=0; $j<count($room_descriptions); $j++)
									{
										if($room_descriptions[$j]['room_ep_type'] == 0)
											{
												$room_description_tr_td.="<tr><td class=\"title\"><b>".$rooms[$i]['room_type_key']."</b>&nbsp;".$room_descriptions[$j]['room_description_title'] . "<td><a href=\"#\" title=\"".$room_descriptions[$j]['accommodation_type_string']."\">".$room_descriptions[$j]['accommodation_type_name']."</a><td>".$room_descriptions[$j]['room_standart_beds']."<td>".$room_descriptions[$j]['room_extra_beds'];
													for($k=0; $k<$count_period_types; $k++)
														$room_description_tr_td.= "<td class=\"price\">".get_room_price($room_descriptions[$j]['room_description_id'], $period_types[$k]['period_type_id']);
											}
									}

						}
		
		if($count_period_types > 0 || count($rooms) > 0)
		{
		?>
        
        <div class="scroll-right"><i class="fa fa-hand-o-up" aria-hidden="true"></i> <i class="fa fa-arrows-h" aria-hidden="true"></i> </div>
		<table border="0" cellpadding="5" cellspacing="1" class="table hotel_prices">
			<?php
                if($count_period_types >0)
                {
                    ?>
            <tr>
                <th colspan="4"><?=TITLE_SEASONS?>
                <?=$periods_type_td?>
            </tr>
                    <?
                }
            ?>
			<tr>
				<td rowspan="2"><?=LABEL_ROOM_TYPE?>
				<td rowspan="2" width="50"><?=LABEL_ACCOMMODATION_TYPE?>
				<td rowspan="2" width="50"><?=LABEL_ROOM_STANDART_BEDS?>
				<td rowspan="2" width="50"><?=LABEL_ROOM_EXTRA_BEDS?>
				<?=$periods_dates_td?>
			</tr>	
            
			<tr>
			<?php
                if($count_period_types > 0)
                {
                ?>
                <td colspan="<?=$count_period_types?>"><?php printf(MESSAGE_PRICES_IN_CURRENCY, "<b>".get_hotel_currency($hotel_id, "currency_key")."</b>")?>
            </tr>
                <?
                }
            ?>
			<?=$room_description_tr_td?>
			<tr><th colspan="<?=(4+$count_period_types)?>" align="center"><?php printf(MESSAGE_PRICES_FROM_TO, "<b>".format_price(get_hotel_min_max_price($hotel_id, "MIN"), $hotel_id)."</b>", "<b>".format_price(get_hotel_min_max_price($hotel_id, "MAX"),$hotel_id)."</b>")?>	
		</table>
        </div>
		<table width="100%" border="0" cellpadding="5" cellspacing="1" class="hotel_conditions">
			<tr><td><?=$hotel->get_hConditions($hotel_id, $language_id)?>
		</table>

		<?php
		if($user->ID > 0)
			{
				//list all extra payments
				$extra_payments = $hotel->get_extra_payments($hotel_id);
				if(count($extra_payments) > 0)
					{
						echo "<br><br><table cellpadding=\"5\" cellspacing=\"1\" class=\"hotel_prices\" width=\"100%\"><tr><th colspan=6>Extra payments";
						for($i=0; $i<count($extra_payments); $i++)
							echo "<tr><td>".($i+1)."<td>".$extra_payments[$i]['period_type_title'] ."<td>".  $extra_payments[$i]['room_description_title'] ."<td>". $extra_payments[$i]['accommodation_type_string'] . "<td>".$extra_payments[$i]['ep_type_description'] ."<td>".$extra_payments[$i]['ep_price']; 
						echo "</table>";
					}
			}
		}
		else mk_output_message("warning", "No prices defined !");
				}
		?>
        </table>
</div>

<div id="option_request">
<?php 
	include "hotels/public/request_reservation_form.php";
?>
</div>

<table width="100%" cellpadding="5" cellspacing="0" class="hotel_navigation">
	<tr>
	<td align="left" width="50%"><?=TEXT_PREVIOUS_HOTEL?><a href="page.php?n=<?=$n?>&amp;SiteID=<?=$SiteID?>&amp;hotel_id=<?=$hotels_array[$prev_index]['hotel_id']?>#<?=$hotels_array[$prev_index]['hotel_title']?>" class="next_link" title="Go back"><?=$hotels_array[$prev_index]['hotel_title']?></a>
	<td align="left" width="50%"><?=TEXT_NEXT_HOTEL?><a href="page.php?n=<?=$n?>&amp;SiteID=<?=$SiteID?>&amp;hotel_id=<?=$hotels_array[$next_index]['hotel_id']?>#<?=$hotels_array[$next_index]['hotel_title']?>" class="next_link" title="Go back"><?=$hotels_array[$next_index]['hotel_title']?></a>
</table>

<script type="text/javascript">
	var tabs=new ddtabcontent("option_tabs")
	tabs.setpersist(true)
	tabs.setselectedClassTarget("link") //"link" or "linkparent"
	tabs.init()
</script>