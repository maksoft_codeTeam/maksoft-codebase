<?
require_once "../lib/lib_page.php";
$o_site = new site($SiteID);

if(!isset($_GET['n']))
	$n = $o_site->get_sStartPage();
else $n = $_GET['n'];
$o_page = new page($n);
$o_user = new user();

//read categories
$categories = $o_page->get_pSubpages($n);
if(isset($gid))
	$categories = $o_page->get_pGroupContent($gid);

$domain = "http://www.".$o_site->get_sURL();
	
$xml_output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
$xml_output.= "<dir_catalog date=\"".date("Y-m-d H:i:s")."\">\n";   
$xml_output.= "<shop>\n";
$xml_output.= "<name>".$domain."</name>\n";
$xml_output.= "<company>".$domain."</company>\n";
$xml_output.= "<url>".$domain."</url>\n";
$xml_output.= "<currencies>\n";
$xml_output.= "<currency  id=\"BGN\" rate=\"1\"/>\n";
$xml_output.= "</currencies>\n";

//output categories
$xml_output.= "<categories>\n";
for($i=0; $i< count($categories); $i++)
	{
		// Escaping illegal characters 
		$categories[$i]['Name'] = str_replace("&", "&", $categories[$i]['Name']); 
		$categories[$i]['Name'] = str_replace("<", "<", $categories[$i]['Name']); 
		$categories[$i]['Name'] = str_replace(">", "&gt;", $categories[$i]['Name']); 
		$categories[$i]['Name'] = str_replace("\"", "&quot;", $categories[$i]['Name']); 
		$categories[$i]['Name'] = str_replace("*", "", $categories[$i]['Name']);
		$categories[$i]['Name'] = iconv('CP1251', 'UTF-8', $categories[$i]['Name']);
		$xml_output.= "<category id=\"".$categories[$i]['n']."\" parentId=\"".$categories[$i]['ParentPage']."\">".$categories[$i]['Name']."</category>\n";
	} 

$xml_output.= "</categories>\n";

//output offers
$xml_output.= "<offers>\n";
for($i=0; $i< count($categories); $i++)
	{
		
		$offers = $o_page->get_pSubpages($categories[$i]['n']);
		for($j=0; $j < count($offers); $j++)
			{
				// Escaping illegal characters 
				$offers[$j]['Name'] = str_replace("&", "&", $offers[$j]['Name']); 
				$offers[$j]['Name'] = str_replace("<", "<", $offers[$j]['Name']); 
				$offers[$j]['Name'] = str_replace(">", "&gt;", $offers[$j]['Name']); 
				$offers[$j]['Name'] = str_replace("\"", "&quot;", $offers[$j]['Name']); 
				$offers[$j]['Name'] = str_replace("*", "", $offers[$j]['Name']);
				$offers[$j]['Name'] = iconv('CP1251', 'UTF-8', $offers[$j]['Name']); 

				$xml_output.= "<offer id=\"".$offers[$j]['n']."\">\n";
				$xml_output.= "<price>0</price>\n";
				$xml_output.= "<price_special>0</price_special>\n";
				$xml_output.= "<categoryId>".$offers[$j]['ParentPage']."</categoryId>\n";
				$xml_output.= "<picture>" . $domain ."/". $offers[$j]['image_src']."</picture>\n";
				$xml_output.= "<picture_large>" . $domain ."/". $offers[$j]['image_src']."</picture_large>\n";
				$xml_output.= "<name>".htmlspecialchars($offers[$j]['Name'])."</name>\n";
				$xml_output.= "<vendor>".$domain."</vendor>\n";
				//$xml_output.= "<description><![CDATA[\n".$offers[$j]['textSt']."]]></description>\n";
				$xml_output.= "<weight>0</weight>\n";
				$xml_output.= "<url>".$domain."/page.php?SiteID=".$o_site->SiteID."&amp;n=".$offers[$j]['n']."</url>\n";
				$xml_output.= "<product_status></product_status>\n";
				$xml_output.= "</offer>\n";
			}

	} 

$xml_output.= "</offers>\n";
$xml_output.= "</shop>\n";
$xml_output.= "</dir_catalog>";
echo $xml_output ;
?>