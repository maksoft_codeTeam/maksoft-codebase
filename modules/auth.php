<?php
include __DIR__.'/vendor/autoload.php';
include __DIR__.'/settings.php';
date_default_timezone_set('Europe/Sofia');

use Maksoft\Admin\Auth\Login;
use Maksoft\Admin\Auth\FailedAttempts;

$db = new Maksoft\Core\MysqlDb($env);
$gate =new Maksoft\Gateway\Gateway($db);
$login = new Login($gate);
//$login->attach( new Logging($logger, $pdoHandler) );
$login->attach( new FailedAttempts($gate));
 
try{
    $login->init( "ryordanov", "admin00", "127.0.0.1" );
} catch (Exception $e){
    echo $e->getMessage();
}
