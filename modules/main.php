<?php
#error_reporting(E_ERROR | E_PARSE);
$_ds = DIRECTORY_SEPARATOR;
require __DIR__.$_ds.'vendor'.$_ds.'autoload.php';


use Maksoft\Modules\Reservation\HotelReservation;
use Maksoft\Modules\Reservation\Reserve;
use Maksoft\Modules\Reservation\Room;

$hotel = new HotelReservation(431);
$hotel->setLanguage(1);
$column_rigth = array();
#printf('<pre>%s</pre>', print_r($hotel->getHotel()));
$session = new Maksoft\Modules\lib\Session();

if(!isset($_SESSION['booking'])){
    $_SESSION['booking'] = array();
}
$_SESSION['booking']['language_id'] = $hotel->getLanguage();
$_SESSION['booking']['hotel_id'] = $hotel->getId();


#printf('<pre>%s</pre>', print_r($_POST));
#printf('<pre>%s</pre>', print_r($_SESSION['booking']));
#echo session_id();
#echo dirname(  __FILE__  ).'/settings.ini'; 
$rooms = 0;
if(isset($_SESSION['booking']['rooms'])){
    $rooms = count($_SESSION['booking']['rooms']);
}
if($_SERVER['REQUEST_METHOD'] === "POST" && isset($_POST['check_in']) ){
    $checkin = date('Y-m-d', strtotime( $_POST['check_in']));
    $checkout =date('Y-m-d', strtotime( $_POST['check_out']));
    $s_check = 0 ;
    $s_out = 0;
    if(isset($_SESSION['booking']['period']['checkin'])){
        $s_check = strtotime($_SESSION['booking']['period']['checkin']);
    }
    if(isset($_SESSION['booking']['period']['checkout'])){
        $s_out = strtotime($_SESSION['booking']['period']['checkout']);
    }

    if(strtotime($checkin) != $s_check or strtotime($checkout) != $s_out){
        $_SESSION['booking']['rooms'] = array();
        $_SESSION['booking']['total'] = 0;
    }

    $adults = $_POST['persons'];
    $children = $_POST['children'];
    $all_rooms = $hotel->getAllRooms();
    $free_rooms = array();
    if(!isset($_SESSION['booking']['period']) or !isset($_SESSION['booking'])){
        $_SESSION['booking'] = array( 'period' => array());
    }
    $_SESSION['booking']['period']['checkin'] = $checkin;
    $_SESSION['booking']['period']['checkout'] = $checkout;
    $_SESSION['booking']['period']['adults'] = $adults;
    $_SESSION['booking']['period']['children'] = $children;
    $tmp = array();
    foreach($all_rooms as $room){
        $res_count =(int) $hotel->getReservedRoomCount($room->room_id, $checkin)->amount;
        $qty = $room->room_qty - $res_count;
        #echo 'reserved rooms: '.$res_count.'<br> room_qty: '.$room->room_qty.' <br> result: '.$qty.'<br>';
        if($qty > 0){
            $room = new Room($room->room_id, $qty);
            $free_rooms[] = $room;
            for($i=0;$i<$qty;$i++){
                $tmp[] = $room;
            }
        }else{
            $reserved_rooms[] = new Room($room->room_id);
        }
    }
    #$suggestion = $hotel->suggestion($adults, $tmp, array());
	$results_page = 1;
    $check = "������� ������";
    require_once __DIR__.$_ds.'src'.$_ds.'Templates'.$_ds.'search.php';
    require_once __DIR__.$_ds.'src'.$_ds.'Templates'.$_ds.'list_rooms.php';

}elseif(isset($_POST['make_reservation']) and $rooms > 0){
	$results_page = 1;
    require_once __DIR__.$_ds.'src'.$_ds.'Templates'.$_ds.'book_form.php';
}else{
    $check = "��������";
    require_once __DIR__.$_ds.'src'.$_ds.'Templates'.$_ds.'search.php';
}
?>
