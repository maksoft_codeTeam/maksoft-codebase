<?php
namespace Maksoft\Gateway\Domain\Cart;
use Maksoft\Core\MysqlDb;
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_seo >> lib_site >> lib_page >> lib_cms
// constructor
class CartGateway 
{
    protected $db;

    protected $sq;

    public function __construct(MysqlDb $db)
    {
        $this->db = $db;
    }
    public function getAllProducts($SiteID)
    {
        $sql = "SELECT * 
            FROM  `pages` p
            LEFT JOIN images i ON i.ImageID = p.ImageNo
            INNER JOIN prices pr ON pr.price_n = p.n
            WHERE p.PHPcode LIKE  '%p_Id%'
            AND p.SiteID =:SiteID";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp_storage = array();
        while($row = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp_storage[] = $row;
        }
        return $tmp_storage;
    }



    public function create_cart($session_id, $user_id, $SiteID)
    {
        if($cart = $this->get_cart($session_id, $user_id, 0, 0)){
            return $cart;
        }

        $sql = "INSERT INTO cart (session_id, user_id, SiteID)
                VALUES (:sess_id, :user_id, :SiteID)
                ";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':sess_id', $session_id);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->bindValue(':SiteID', $SiteID);
        $stmt->execute();
        $cart = $this->get_cart_by_id($this->db->lastInsertId());
        return $cart;
    } 

    public function get_cart_by_id($id)
    {
        $sql = "SELECT * FROM cart
                LEFT JOIN users ON users.ID = cart.user_id
                WHERE cart.id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function get_order($user_id, $order_id)
    {
        $sql = "SELECT * FROM cart
                WHERE user_id=:user_id
                AND id=:order_id
                AND checkout=1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":order_id", $order_id);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }

    public function get_orders($user_id, $SiteID)
    {
        $sql = "SELECT * FROM cart
                WHERE user_id=:user_id
                AND SiteID = :SiteID
                AND checkout=1
                ORDER BY checkout_date DESC";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(':SiteID', $SiteID);
        $stmt->execute();
        $tmp_storage = array();
        while($row = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp_storage[] = $row;
        }
        return $tmp_storage;
    }

    public function get_all_orders($by_status, $SiteID)
    {
        $sql = "SELECT * FROM cart
                INNER JOIN users u ON u.ID = cart.user_id
                WHERE status=:checkout_status
                AND cart.SiteID = :SiteID
                AND checkout = 1
                ORDER BY checkout_date DESC
                ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":checkout_status", $by_status);
        $stmt->bindValue(':SiteID', $SiteID);
        $stmt->execute();
        $tmp_storage = array();
        while($row = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp_storage[] = $row;
        }
        return $tmp_storage;
    }

    public function update_cart_status($order_id, $status)
    {
        $sql = "UPDATE cart SET status=:status WHERE id=:order_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':status', $status);
        $stmt->bindValue(':order_id', $order_id);
        return $stmt->execute();
    }

    public function get_cart($session_id, $user_id, $checkout=0, $abadoned=0)
    {
        $sql = "SELECT * FROM cart
                WHERE session_id = :session_id
                AND user_id = :user_id
                AND checkout=:checkout
                AND abadoned=:abadoned";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':session_id', $session_id);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->bindValue(':checkout', $checkout);
        $stmt->bindValue(':abadoned', $abadoned);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function delete_cart($cart_id, $user_id)
    {
        $sql = "DELETE FROM cart c
                WHERE c.id = :cart_id
                AND c.user_id = :user_id
                LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':cart_id', $cart_id);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function get_cart_items($cart_id)
    {
        $sql = "SELECT * FROM cart_items WHERE cart_id=:cart_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':cart_id', $cart_id);
        $stmt->execute();
        $results = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $results[$res->id] = $res;
        }
        return $results;
    }

    public function get_cart_sum($cart_id)
    {
        $sql = "
        SELECT SUM( ci.qty * ci.price ) AS sum
        FROM cart c
        JOIN cart_items ci ON ci.cart_id = c.id
        WHERE c.id = :cart_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':cart_id', $cart_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res->sum;
    }

    public function checkout($cart_id)
    {
        $sql = "UPDATE cart SET checkout=1,
                checkout_date= :date 
                WHERE id = :cart_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':date', date("Y-m-d H:i:s", strtotime('+3 hours')));
        $stmt->bindValue(':cart_id', $cart_id);
        return $stmt->execute();
    }

    public function update_item($cart_id, $sku, $qty, $price, $data_json, $SiteID)
    {
        $sql = "UPDATE cart_items 
                SET
                    qty=:qty,
                    price=:price,
                    data_json=:data_json,
                    SiteID=:SiteID
                WHERE cart_id=:cart_id
                AND sku=:sku
                ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':cart_id', $cart_id);
        $stmt->bindValue(':sku', $sku);
        $stmt->bindValue(':qty', $qty);
        $stmt->bindValue(':price', $price);
        $stmt->bindValue(':data_json', $data_json);
        $stmt->bindValue(':SiteID', $SiteID);
        return $stmt->execute();
    }

    public function cart_exist($cart_id)
    {
        $sql = "SELECT id from cart_items WHERE id=:cart_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':cart_id', $cart_id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function insert_item($cart_id, $sku, $qty, $price, $data_json, $SiteID)
    {
        $sql = "INSERT INTO  cart_items 
                (cart_id, sku, qty, price, data_json, SiteID)
                VALUES (
                    :cart_id,
                    :sku,
                    :qty,
                    :price,
                    :data_json,
                    :SiteID
                )
                ON DUPLICATE KEY UPDATE qty=:qty, data_json=:data_json
                ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':cart_id', $cart_id);
        $stmt->bindValue(':sku', $sku);
        $stmt->bindValue(':qty', $qty);
        $stmt->bindValue(':price', $price);
        $stmt->bindValue(':data_json', $data_json);
        $stmt->bindValue(':SiteID', $SiteID);
        $stmt->execute();
        $stmt = $this->db->query("SELECT LAST_INSERT_ID()");
        $lastId = $stmt->fetchColumn();
        return $lastId;
    }

    public function delete_item($cart_id, $sku)
    {
        $sql = "DELETE FROM cart_items
                WHERE cart_id=:cart_id 
                AND sku=:sku LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':cart_id', $cart_id);
        $stmt->bindValue(':sku', $sku);
        return $stmt->execute();
    }
}
