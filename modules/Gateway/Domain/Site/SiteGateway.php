<?php
namespace Maksoft\Gateway\Domain\Site;
use Maksoft\Core\MysqlDb;
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_seo >> lib_site >> lib_page >> lib_cms
// constructor
class SiteGateway
{
    protected $db;

    protected $sq;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function getData()
    {
        $stmt = $this->db->prepare(
            "SELECT * FROM Sites
             WHERE url LIKE :url
             ORDER BY SitesID ASC LIMIT 1");
        $stmt->bindValue(":url", "%".$_SERVER['SERVER_NAME']."%");
        if($stmt->execute()){
            $site = $stmt->fetch(\PDO::FETCH_OBJ);
            return $site;
        }
        throw new \Maksoft\errors\MaksoftCmsException('error');
    }

    public function getSessionUser()
    {
        $stmt = $this->db->prepare("SELECT data from session where id = :session_id");
        $stmt->bindValue(":session_id", session_id());
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function countSitePages($SiteID)
    {
        $stmt = $this->db->prepare("SELECT COUNT(n) FROM pages WHERE SiteID=:SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }


    public function getSiteImageDirs($SiteID) {
        $sql = '
            SELECT * FROM ImageAccess ia 
            RIGHT JOIN image_dirs im ON im.ID = ia.imagesdir
            WHERE ia.SiteID=:SiteID'; 
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    

    public function get_site_permissions(integer $user_id, integer $SiteID)
    {
        $stmt = $this->db->prepare(
            "SELECT * FROM SiteAccess
             WHERE userID=:user
             AND SiteID=:SiteID  LIMIT 1");
        $stmt->bindValue(":user", $userID);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $permissions = $stmt->fetchAll();
        return $permissions;
    }

    public function isActiveCms($SiteID)
    {
        $stmt = $this->db->prepare(
            "SELECT  hostCMS.*,
             DATEDIFF(hostCMS.toDate, hostCMS.Date) as days_total,
             DATEDIFF(hostCMS.toDate, CURDATE()) as days_left
             FROM hostCMS
             WHERE SiteID=:siteID
             AND active='1'
             ORDER BY Date DESC LIMIT 1");
        $stmt->bindParam(":siteID", $SiteID, \PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }

    public function getCurrentSiteArr(integer $SiteID)
    {
        $stmt = $this->db->prepare("SELECT * FROM Sites WHERE SitesID = :SiteID LIMIT 1");
        $stmt->bindParam(":SiteID", $SiteID, \PDO::PARAM_INT);
        $stmt->execute();
        $row=$stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }

    public function getSiteConfiguration($SiteID)
    {
        $conf = array();
        $stmt = $this->db->prepare("SELECT * FROM configuration WHERE SiteID=:SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $tmp = array();
        while($row=$stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getSiteCss($cssID)
    {
        $stmt = $this->db->prepare("SELECT * FROM CSSs WHERE cssID = :css");
        $stmt->bindValue(":css", $cssID);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }
    //return site image directory path
    public function getSiteImageDir($id)
    {
      $stmt = $this->db->prepare("SELECT * FROM image_dirs WHERE ID = :id");
      $stmt->bindValue(":id", $id);
      $stmt->execute();
      $dir = $stmt->fetch(\PDO::FETCH_OBJ);
      return $dir;
    }

    //return array of all image directories accessed by the selected site or a single dir
    public function getSiteImagesDirs($SiteId)
    {
        $stmt = $this->db->prepare("
                SELECT * FROM ImageAccess ia, image_dirs id
                WHERE ia.SiteID=:SiteID
                AND ia.imagesdir=id.ID
                ORDER by id.Name ASC");
        $stmt->bindValue(":SiteID", $SiteId);
        $stmt->execute();
        $tmp = array();;
        while ($dir = $stmt->fetch(\PDO::FETCH_OBJ)){
          $tmp[] = $dir;
        }
        return $tmp;
    }

    //return directory info
    public function get_sDirectoryInfo($id)
    {
      $stmt = $this->db->prepare("SELECT * FROM image_dirs WHERE ID=:id LIMIT 1");
      $stmt->bindValue(":id", $id);
      $stmt->execute();
      $dInfo = $stmt->fetch();
      return $dInfo;
    }

    //return number of files in selected directory
    public function getSiteImages($image_dir, $limit=0, $offset=100)
    {
        $order = in_array($order, array('ASC', 'DESC')) ? $order : 'DESC';
        $stmt = $this->db->prepare("SELECT * FROM images im 
                                    WHERE im.image_dir=:im_dir ORDER BY im.imageID $order");
            
        $stmt->bindValue(":im_dir", $image_dir);
        $stmt->execute();
        $tmp=array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getSiteImagesFilteredByName($image_dir, $name )
    {
        $order = in_array($order, array('ASC', 'DESC')) ? $order : 'DESC';
        $stmt = $this->db->prepare(
            "SELECT * FROM images im 
            WHERE im.image_dir=:im_dir
            AND im.imageName LIKE :name
            ORDER BY im.imageID $order
            ");
            
        $stmt->bindValue(":im_dir", $image_dir);
        $stmt->bindValue(":name", "%$name%");
        $stmt->execute();
        $tmp=array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    //return number of files in selected directory
    public function countDirectoryImages($image_dir)
    {
        $stmt = $this->db->prepare("SELECT COUNT(imageID) as length FROM images im 
                                    WHERE im.image_dir=:im_dir ");
            
        $stmt->bindValue(":im_dir", $image_dir);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res->length ? $res->length : 0;
    }

    public function getImage(integer $img_id)
    {
        $query = "SELECT * FROM images i  WHERE i.imageID=:imageID  LIMIT 1";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":imageID", $img_id);
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_OBJ);
        return $result;
    }

    //return image info
    public function get_sImageInfo(integer $imageID)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM images im
            LEFT JOIN image_dirs imd on im.image_dir = imd.ID
            WHERE im.imageID=:image_id");
        $stmt->bindValue(":image_id", $imageID);
        $stmt->execute();
        $row = $stmt->fetch();
        return $row;
    }

    public function searchImages($SiteID, $needle)
    {
        $query_str = "
            SELECT ImageID, imageName, image_src, image_dir
            FROM images
            WHERE image_dir=:SiteID
            ORDER BY image_date_added DESC";
        $stmt = $this->db->prepare(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row=$stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function searchPages($uLevel, $needle, $SiteID)
    {
        $search_query =
             "SELECT p.n, p.Name, p.title, p.SiteID, p.textStr, im.image_src
              FROM pages p  left join images im on p.imageNo = im.imageID
              WHERE p.SiteID=:SiteID
              AND p.SecLevel <= :u_level
              AND p.Name LIKE :Name)
              ORDER BY p.preview DESC, p.date_modified  DESC";
        $stmt = $this->db->prepare($search_query);
        $stmt->bindValue(":u_level", $uLevel);
        $stmt->bindValue(":Name", $needle);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    // get tags from all site pages
    // TODO yield
    public function getSiteTags($SiteID)
    {
        $tags_array = array();
        $stmt = $this->db->prepare(
            "SELECT * FROM pages
            WHERE SiteID = :SiteID
            AND tags !=''
            ORDER by RAND()");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp =array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[]=$res;
        }
        return $tmp;
    }
    public function insert_stats($n, $SiteID, $user_ip, $entry)
    {
        $stmt = $this->db->prepare("INSERT INTO stats SET sesID=:sesID, n=:n, SiteID=:n, ip=:ip, entry=:entry");
        $stmt->bindValue(":sesID", session_id());
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":ip", $user_ip);
        $stmt->bindValue(":entry", $entry);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function insert_referer($stID, $referer_name, $url_name, $SiteID, $n)
    {
        $stmt = $this->db->prepare("
            INSERT INTO
                referers
            SET
                stID=:stID ,
                referer=:referer,
                url=:url,
                SiteID=:SiteID");
        $stmt->bindValue(":stID", $stID);
        $stmt->bindValue(":referer", $referer_name);
        $stmt->bindValue(":url", $url_name);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function getKeys($n, $time)
    {
        $select_query = "SELECT * FROM keywords WHERE n=:id AND enter_time=:enter_time ";
        $stmt = $this->db->prepare($select_query);
        $stmt->bindValue(':n', $n);
        $stmt->bindValue(':enter_time', $time);
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function setKeywords(
            $keywords,
            $search_engine,
            $search_engine_id,
            $n,
            $g_pos,
            $SiteID,
            $ip,
            $enter_time
        )
    {
        $stmt = $this->db->prepare(
              "INSERT INTO keywords
              SET
              keywords=:keywords,
              search_engine=:search_engine,
              search_engine_id=:search_engine_id,
              g_pos=:g_pos,
              n=:n,
              SiteID=:SiteID,
              ip=:ip,
              enter_time=:enter_time");
        $stmt->bindValue(":keywords", $keywords);
        $stmt->bindValue(":search_engine", $search_engine);
        $stmt->bindValue(":search_engine_id", $search_engine_id);
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":g_pos", $g_pos);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":ip", $ip);
        $stmt->bindValue(":enter_time", $enter_time);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function updateSiteKeywords($keywords, $ip)
    {
        $stmt = $this->db->prepare("
            UPDATE keywords SET repeated='1'
            WHERE keywords=:keywords AND ip<>:ip  LIMIT 5"
        );
        $stmt->bindValue(":keywords", $keywods);
        $stmt->bindValue(":ip", $ip);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function selectSEkeywords($keywords)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM keywords
            WHERE keywords=:keywords
            AND search_engine_id>0
            ");
        $stmt->bindValue(":keywords", $keywords);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        return $rows;
    }

    public function update_keywords_visits($visits, $SiteID, $keywords)
    {
        $stmt = $this->db->prepare("
            UPDATE keywords SET visits=:visits
            WHERE SiteID=:SiteID AND keywords=:keywords
            ORDER BY keysID DESC LIMIT 2  ");
        $stmt->bindValue(':visits', $key_count);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":keywords", $keywords);
        $stmt->execute();
    }


    //return array of all site requests
    //TODO split to 4 methods
    //readed
    //completed
    //deleted
    //junk

    function getSiteRequestsListForMultipleSites() 
    {
        $sites = implode(',', func_get_args());
        $sql = "SELECT COUNT( f.SiteID ) AS requests, s.Name 
                FROM forms f
                RIGHT JOIN Sites s ON f.SiteID=s.SitesID
                WHERE f.SiteID IN ($sites)
                AND f.ftype = 0
                AND f.Data>DATE_SUB(CURDATE(),INTERVAL 5 DAY)
                GROUP BY f.SiteID
                ORDER BY f.Data DESC 
                LIMIT 100";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }
    
    function getSiteRequestsList($SiteID) {
        $sql = "SELECT * FROM forms f
                LEFT JOIN pages p ON f.n=p.n
                WHERE f.SiteID =:SiteID
                ORDER by f.Data DESC LIMIT 0, 100";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    function getSiteRequestsListByStatusCode($SiteID, $status) {
        $query = "SELECT * FROM forms f
                  LEFT JOIN pages p ON f.n=p.n
                  WHERE f.SiteID =:SiteID
                  AND f.ftype =:status
                  ORDER by f.Data DESC LIMIT 0, 100";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":status", $status);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    //return site cms hosting info array
    public function getListHostCms($SiteID, $filter=1)
    {
        $query = "SELECT *, 
                  DATEDIFF(h.toDate, h.Date) as days_total,
                  DATEDIFF(h.toDate, CURDATE()) as days_left
                  FROM hostCMS h 
                  JOIN Sites s on s.SitesID = h.SiteID
                  WHERE h.SiteID = :SiteID AND :filter ORDER by h.Date DESC
            ";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":SiteID", $SiteID, \PDO::PARAM_INT);
        $stmt->bindValue(":filter", $filter);
        $stmt->execute();
        $sCMSHosting = array();
        while($row = $stmt->fetch(\PDO::FETCH_OBJ)){
            $sCMSHosting[] = $row;
        }
        return $sCMSHosting;
    }

    //return site cms hosting info array
    public function getDetailHostCms($SiteID, $id)
    {
        $query = "
            SELECT hostCMSid, SiteID, fID, Visits, maxVisits, extraVisitsPay, packname,
                   DATE(Date) as Date, toDate, active, netservice,
                DATEDIFF(toDate, Date) as days_total,
                DATEDIFF(toDate, CURDATE()) as days_left
            FROM hostCMS
            WHERE
                SiteID = :SiteID
            AND
                hostCMSid=:id
            LIMIT 1";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":SiteID", $SiteID, \PDO::PARAM_INT);
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $sCMSHosting = array();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function insertHostCms($SiteID, $fID, $Visits, $max_visits, $extraVisitsPay, $packname, $Date, $toDate, $active, $netservice)
    {
        $query = 'INSERT INTO
                    `hostCMS`(`SiteID`, `fID`, `Visits`, `maxVisits`, `extraVisitsPay`, `packname`, `DATE`, `toDate`, `active`, `netservice`)
                  VALUES
                    (:SiteID, :fID, :Visits, :max_visits, :extraVisitsPay, :packname, :Date, :toDate, :active, :netservice)';

        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":SiteID", $SiteID, \PDO::PARAM_INT);
        $stmt->bindParam(":fID", $fID, \PDO::PARAM_INT);
        $stmt->bindParam(":Visits", $Visits, \PDO::PARAM_INT);
        $stmt->bindParam(":max_visits", $max_visits, \PDO::PARAM_INT);
        $stmt->bindParam(":extraVisitsPay", $extraVisitsPay, \PDO::PARAM_INT);
        $stmt->bindParam(":packname", $packname, \PDO::PARAM_STR);
        $stmt->bindParam(":Date", $Date, \PDO::PARAM_STR);
        $stmt->bindParam(":toDate", $toDate, \PDO::PARAM_STR);
        $stmt->bindParam(":active", $active, \PDO::PARAM_INT);
        $stmt->bindParam(":netservice", $netservice, \PDO::PARAM_INT);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function host_cms_fID_is_unique($fID, $SiteID)
    {
        $query = "SELECT COUNT( fID ) as len
            FROM  `hostCMS`
            WHERE fID =:fID
            AND SiteID =:SiteID";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":SiteID", $SiteID, \PDO::PARAM_INT);
        $stmt->bindParam(":fID", $fID, \PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res->len < 1;
    }

    //return site versions
    public function getSiteVersions($SiteID)
    {
        $stmt = $this->db->prepare("SELECT * FROM versions v WHERE v.SiteID =:SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($version = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $version;
        }
        return $tmp;
    }
   //return site language: BG=1, EN=2, RU=3, FR=4
    public function getSiteLanguage($sLanguage)
    {
        $stmt = $this->db->prepare("SELECT * FROM languages l WHERE l.language_id = :sLanguage LIMIT 1");
        $stmt->bindValue(":sLanguage", $sLanguage);
        $stmt->execute();
        $language = $stmt->fetch(\PDO::FETCH_OBJ);
        return $language;
    }

    //return aaray of all site groups
    public function getSiteGroups($SiteID, $filter = "1")
    {
        $stmt = $this->db->prepare("
            SELECT g.group_id AS id, g.parent_id AS parent, g.group_title AS title, g.group_date_added AS created_at, (

                SELECT COUNT( * ) 
                FROM pages_to_groups p
                WHERE p.group_id = g.group_id
            ) AS pages_count
            FROM groups g
            WHERE g.SiteID =:SiteID
            AND :filter
        ");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":filter", $filter);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    public function getGroupById($group_id) 
    {
        $stmt = $this->db->prepare("
            SELECT * FROM groups 
            WHERE group_id = :group_id");
        $stmt->bindValue(":group_id", $group_id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
        
    }
    
    public function getGroupByName($group_name, $SiteID) {
        $stmt = $this->db->prepare("
            SELECT * FROM groups 
            WHERE group_title = :group_name AND SiteID = :SiteID");
        $stmt->bindValue(":group_name", $group_name);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getGroupsByParentGroupId($group_id) 
    {
        $stmt = $this->db->prepare("
            SELECT *, (

                SELECT COUNT( * ) 
                FROM pages_to_groups p
                WHERE p.group_id = g.group_id
            ) AS pages_count
            FROM groups g
            WHERE g.parent_id = :group_id"); 
        $stmt->bindValue(":group_id", $group_id);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
        
    }

    public function getGroupPages($group_id)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM pages_to_groups ptg
            JOIN pages p ON p.n = ptg.n
            WHERE ptg.group_id = :group_id"); //TODO outside while
        $stmt->bindValue(":group_id", $group_id);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    //return site options
    public function getSiteOptions($SiteID)
    {
        $stmt = $this->db->prepare("SELECT * FROM options o WHERE o.SiteID = :SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function addPageToGroup($page_id, $group_id, $label, $sort_order, $start_date, $end_date)
    {
        $query = "
            INSERT INTO pages_to_groups 
            (`n`, `group_id`, `label`, `sort_order`, `start_date`, `end_date`)
            VALUES 
            (:page_id, :group_id, :label, :sort_order, :start_date, :end_date)";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":page_id", $page_id);
        $stmt->bindValue(":group_id", $group_id);
        $stmt->bindValue(":label", $label);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->bindValue(":start_date", $start_date);
        $stmt->bindValue(":end_date", $end_date);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function insertGroup($parent_id, $name, $SiteID) {
        $query = "INSERT INTO `groups`(`parent_id`, `group_title`, `SiteID`, `group_date_added`, `sort_order`) VALUES (:parent_id, :name, :SiteID, NOW(), 999)";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":parent_id", $parent_id);
        $stmt->bindValue(":name", $name);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function setGroupSortOrder($id, $sort_order)
    {
        $query = 'UPDATE `groups` SET `sort_order` = :sort_order  WHERE `group_id` = :id';
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
    }

    public function setPageToGroupSortOrder($id, $sort_order) {
        $query = 'UPDATE `pages_to_groups` SET `sort_order` = :sort_order  WHERE `id` = :id';
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
    }


    public function getPageToGroupPage($page_id, $SiteID) 
    {
        $query = "SELECT * FROM pages_to_groups pg JOIN pages p on p.n = pg.n WHERE pg.n = :n and p.SiteID = :SiteID";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":n", $page_id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getPageToGroupPageById($id, $SiteID) 
    {
        $query = "SELECT * FROM pages_to_groups pg JOIN pages p on p.n = pg.n WHERE pg.id = :id and p.SiteID = :SiteID";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }
    
    public function removePageToGroupPage($page_id) 
    {
        $query = 'DELETE FROM `pages_to_groups` WHERE `id` = :id';
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":id", $page_id);
        $stmt->execute();
    }

    public function deleteGroup($group_id) 
    {
        $query = 'DELETE From groups where group_id = :group_id';
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":group_id", $group_id);
        $stmt->execute();

    }

    public function deletePagesToGroupsByGroupId($group_id) {
        $query = 'DELETE FROM `pages_to_groups` WHERE `group_id` = :group_id';
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":group_id", $group_id);
        $stmt->execute();
    }

    //return all comments in site
    public function getSiteComments($SiteID)
    {
        $stmt = $this->db->prepare("SELECT * FROM pages p
                                    RIGHT JOIN comments c on c.comment_n = p.n
                                    WHERE p.SiteID =:SiteID
                                    ORDER by c.comment_date DESC");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp=array();
        while($comment = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[]=$comment;
        }
        return $tmp;
    }

/* ###################### ADMINISTRATIVE FUNCTIONS: add / update / delete etc. ###################### */

//add or update confuguration value
    public function getConfType($conf_key, $conf_value, $SiteID, $templ_id)
    {
        $search_query = "SELECT * FROM configuration_types WHERE conf_type_key =:conf_key LIMIT 1";
        $stmt = $this->db->prepare($search_query);
        $stmt->bindValue(":conf_key", $conf_key);
        $stmt->execute();
        $configuration = $stmt->fetch(\PDO::FETCH_OBJ);
        return $configuration;
    }

    public function updateConfigValue($conf_key, $conv_value, $SiteID, $templ_id)
    {
            $query = "UPDATE configuration
                      SET conf_value = :conf_value, last_modified = now(), templ_id =:templ_id
                      WHERE conf_key = :conf_key
                      AND SiteID = :SiteID
                      AND templ_id=:templ_id";
            $stmt = $this->db->prepare($search_query);
            $stmt->bindValue(":conf_value", $conf_value);
            $stmt->bindValue(":templ_id", $templ_id);
            $stmt->bindValue(":conf_key", $conf_key);
            $stmt->bindValue(":SiteID", $SiteID);
            $stmt->execute();
            return $this->db->lastInsertId();
    }

    public function insertConfigValue(
        $conf_key, $conf_value, $conf_description, $conf_group_id, $SiteID, $templ_id)
    {
        $query = "INSERT INTO configuration
                  SET conf_title = '',
                  conf_key = :conf_key,
                  conf_value = :conf_value,
                  conf_description = :conf_description,
                  conf_group_id = :conf_group_id,
                  sort_order = 0,
                  last_modified = 'now()',
                  date_added = 'now()',
                  SiteID = :SiteID,
                  templ_id = :templ_id";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":conf_key", $conf_key);
        $stmt->bindValue(":conf_value", $conf_value);
        $stmt->bindValue(":conf_description", $configuration['conf_type_description']);
        $stmt->bindValue(":conf_group_id", $configuration['conf_group_id']);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":templ_id", $templ_id);
        $stmt->execute();
        return $this->db->lastInsertId();
    }
//delete all configurations filtered by templ_id and SiteID
    public function deleteConfigurations($templ_id, $SiteID)
    {
        $query = "DELETE FROM configuration WHERE SiteID=:SiteID AND templ_id = :templ_id";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":templ_id", $templ_id);
        $stmt->execute();
    }

    public function setStartPage()
    {
        $query="INSERT INTO pages SET ParentPage='0', Name='������' ";
        $this->db->exec($query);
        $StartPage = $this->db->lastInsertId();
        return $StartPage;
    }

    public function setImageDir($site_name)
    {
        $query="INSERT INTO image_dirs SET Name=:site_name ";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":site_name", $NewSiteName);
        $stmt->execute();
        $imagesdir = $this->db->lastInsertId();
        return $imagesdir;
    }

    public function siteCreate(
        $site_name, $start_page_id, $img_dir_id, $rub, $templ_id, $css, $language
    )
    {
        $query = "
             INSERT INTO Sites
             SET
             Name=:site_name,
             StartPage=:start_page,
             imagesdir=:images_dir,
             Rub=:rub,
             Template=:Template,
             css=:css,
             language_id=:language ";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":site_name", $site_name);
        $stmt->bindValue(":start_page", $start_page_id);
        $stmt->bindValue(":images_dir", $img_dir_id);
        $stmt->bindValue(":rub", $rub);
        $stmt->bindValue(":Template", $templ_id);
        $stmt->bindValue(":css", $css);
        $stmt->bindValue(":language", $language);
        $stmt->execute();
        $SiteID = $this->db->lastInsertId();
        return $SiteID;
    }

    public function updateImgDir($SiteID, $img_dir_id)
    {
        $settings = parse_ini_file(dirname( realpath( __DIR__ ) ).'/../../../settings.ini', true);
        $image_dir_path = $settings['PATHS']['img_dir'].DIRECTORY_SEPARATOR.$SiteID;
        $query="UPDATE image_dirs SET image_dir =:image_dir WHERE ID=:images_dir ";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":image_dir", $image_dir_path);
        $stmt->bindValue(":images_dir", $img_dir_id);
        $stmt->execute();
    }

    public function updateImageNameAndDir($imageId, $imageName, $imageDir) {
        $query = 'UPDATE `images` SET `imageName`=:imageName, `image_dir`=:imageDir WHERE `imageID`=:imageID';
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":imageName", $imageName);
        $stmt->bindValue(":imageID", $imageId);
        $stmt->bindValue(":imageDir", $imageDir);
        $stmt->execute();
    }

    public function setImageAsLogo($imageSrc, $SiteID) 
    {
        $query = "UPDATE Sites SET Logo=:imageSrc Where SitesID = :SiteID";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":imageSrc", $imageSrc);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
    }

    public function updateStartPage($SiteID, $start_page)
    {
        $query="UPDATE pages SET SiteID=:SiteID WHERE n=:start_page";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":start_page", $start_page);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function setSiteAccess($SiteID, $user_id)
    {
        $query="INSERT INTO SiteAccess SET SiteID=:SiteID, userID=:userID";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":userID", $user_id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function getPagesByImage($imageID, $SiteID) {
        $query = "SELECT n, slug, Name from pages WHERE imageNo = :imageID and SiteID = :SiteID";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":imageID", $imageID);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function setImgAccess($SiteID, $img_dir_id)
    {
        $query="INSERT INTO ImageAccess SET SiteID=, imagesdir=:imagesdir ";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":imagesdir", $img_dir_id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function setInitPage($SiteID, $user_id, $init_page=154)
    {
        //update users
        $query="UPDATE users SET InitPage = :init_page, InitSiteID =:SiteID WHERE ID = :user_id ";
        //echo("$query");
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":init_page", $init_page);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $SiteID;
    }

    public function getCache($cvID)
    {
        $stmt=$this->db->prepare("SELECT * FROM cache_vars WHERE cvID=:cvID LIMIT 1 ");
        $stmt->bindValue(":cvID", $cvID);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }

    public function cachePut($val, $cvID){
        $stmt = $this->db->prepare("
            UPDATE cache_vars
            SET val =:val, gen_time = :gen_time
            WHERE cvID = :cvID");
        $stmt->bindValue(":val", serialize($val));
        $stmt->bindValue(":gen_time", microtime(true));
        $stmt->bindValue(":cvID", $cvID);
        $stmt->execute();
    }

    public function getSiteDataByUrl()
    {
        $stmt = $this->db->prepare(
            "SELECT *  FROM `Sites`
            WHERE `url` LIKE :url OR `primary_url` LIKE :url");
        $stmt->bindValue(":url", "%".$_SERVER['HTTP_HOST']."%");
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }

    public function getSiteAddons($SiteID)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM PageURLs
            WHERE (SiteID=0 OR SiteID=:SiteID)
            ORDER BY NAME ASC");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getAdminMessages($msg_group){
        $stmt = $this->db->prepare("
            SELECT * FROM default_messages
            LEFT JOIN messages ON messages.default_message_id = default_messages.id
            WHERE name = :name");
        $stmt->bindValue(":name", $msg_group);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

} // end class site
?>
