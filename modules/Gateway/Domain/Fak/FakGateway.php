<?php
namespace Maksoft\Gateway\Domain\Fak;

class FakGateway
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getProduct($id)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM itemTypes
            WHERE itID=:id LIMIT 1"
        );
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function ordersInProduction($user_id)
    {
        $sql = "
        SELECT Firmi.Name, Srok, fID, Total, Data, DATE_ADD(Data, INTERVAL Srok Day) as end_date, (SELECT IF(SUM(ok)=COUNT(ID), 1, 0) as result FROM `items` WHERE `fID` = docs.fID
) as completed
          FROM `docs`
          JOIN Firmi ON Firmi.ID = docs.ClientID
          WHERE `cr_userID` = :user_id
          AND docID >= 0
          AND Srok < 30
          AND DATE_ADD(Data, INTERVAL Srok Day) > NOW()
";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->execute();
        $results = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $results[] = $res;
        }
        return $results;
    }

    public function productionForReceiving($itGroup=5, $user_id){
        $sql ="
        SELECT items.ID, it.itName, items.fID, items.qty, items.Opisanie, d.Data AS start_date, DATE_ADD( d.Data, INTERVAL d.Srok
        DAY ) AS end_date, items.itype, f.Name, DATE_SUB( NOW( ) , INTERVAL 2 
        DAY ) AS 
        END 
        FROM  `items` 
        JOIN itemTypes it ON items.itype = it.itID
        JOIN docs d ON items.fID = d.fID
        JOIN Firmi f ON f.ID = d.ClientID
        WHERE d.docID >=0
        AND items.ok =0
        AND d.cr_userID = :user_id
        AND it.itGroup IN (0,2,3,4,5,6,7,8,9,10, 11, 12, 15, 17, 31, 50, 51,50) 
        AND it.prior >0
        AND DATEDIFF( DATE_SUB( NOW( ) , INTERVAL 2 
        DAY ) , DATE_ADD( d.Data, INTERVAL d.Srok
        DAY ) ) <2";
        $stmt = $this->db->prepare($sql);
        // $stmt->bindValue(":itGroup", $itGroup);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->execute();
        $results = array();
        while($res = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $results[] = $res;
        }
        return $results;
    }

    public function productionList($itGroup = 5, $user_id)
    {
        $sql = "
        SELECT items.ID, it.itName, items.fID, items.qty, items.Opisanie, d.Data AS start_date, DATE_ADD( d.Data, INTERVAL d.Srok
        DAY ) AS end_date, items.itype, f.Name
        FROM  `items` 
        JOIN itemTypes it ON items.itype = it.itID
        JOIN docs d ON items.fID = d.fID
        JOIN Firmi f ON f.ID = d.ClientID
        WHERE d.docID >=0
        AND d.docID != 5
        AND items.ok =0
        AND it.itGroup IN (0,2,3,4,5,6,7,8,9,10, 11, 12, 15, 17, 31, 50, 51,50) 
        AND d.cr_userID = :user_id
        AND it.prior >0";
        $sql .= " ORDER BY d.Data ASC"; 
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->execute();
        $results = array();
        while($res = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $results[] = $res;
        }
        return $results;
    }

    public function checkCart()
    {
    }

    public function saveCart()
    {
    }

    public function updateCart()
    {
    }

    public function deleteCart()
    {

    }

    public function getQty($itype, $itype_tela, $sold=0)
    {
        $max_str = ""; 
        $doc_ID_val = 0; 
        $query_av_set = "SELECT sum(items.qty) FROM `items` left join docs on items.fID=docs.fID WHERE ( items.itype=:itype OR itype=:tela) AND docs.docID>=:doc_id"; 
        $stmt = $this->db->prepare($query_av_set);
        $stmt->bindValue(":itype", $itype);
        $stmt->bindValue(":tela", $itype_tela);
        $stmt->bindValue(":doc_id", 0);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        
        //echo("$query_av_set"); 
        echo 'asdas';
        
        if($itype_tela > 0) {
            $itype_tela_str = "  OR itID='$itype_tela'  "; 
        }
        $stmt = $this->db->prepare("SELECT qty_on_stock FROM itemTypes WHERE  ( itID=:itype OR itID = :tela )");
        $stmt->bindValue(":itype", $itype);
        $stmt->bindValue(":tela", $itype_tela);
        $stmt->execute();

        $qty_on_stock = $stmt->fetch( \PDO::FETCH_OBJ );
        
        $total_sold = round( $total_sold * 1.005, 0); // +0.5-1% lipsi
                 
        $qty_av = $qty_on_stock->qty_on_stock + $qty_on_stock_tela - $total_sold; // total available qty
        //$qty_av = $qty_av-$qty_on_stock*0.01-$qty_on_stock_tela*0.01;   // namaliavame realnite kolichestva s 1% 
         
        $qty_av_p = $qty_av/($qty_on_stock + $qty_on_stock_tela) * 100; 
        $qty_av_p = round($qty_av_p, 0); 

        $sold_p = 100-$qty_av_p; 
        
        if ($qty_av<($qty_on_stock + $qty_on_stock_tela)*0.02) {
         echo(" - <b><font color=\"#FF0000\">НЯМА НАЛИЧНИ!!!</b></font><br>"); 
        }
        else 
        $qty_av_preview = round($qty_av/100, 0)*100-100; 
        echo(" 
        <table width=\"500\" border=\"0\">
         <tr>
        <td width=$sold_p% class=\"t1\" >$sold_p%</td>
        <td width=$qty_av_p%  class=\"t2\">$qty_av_preview бр.</td>
      </tr>
        </table>
        "); 
    }
    
    public function getAllInvoices($client_id)
    {
        $sql = "SELECT * 
                FROM  `docs` d
                LEFT JOIN docTypes ON docTypes.docTypeID = d.docID
                WHERE  d.docID = 2
                AND  ClientID =:client_id 
                ORDER BY d.fID DESC";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":client_id", $client_id);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getUnpaidDocuments($client_id)
    {
        $sql = "SELECT * 
                FROM  `docs` d
                LEFT JOIN docTypes ON docTypes.docTypeID = d.docID
                WHERE  d.docID > -1
                AND  ClientID =:client_id 
                AND d.total > d.paid
                ORDER BY d.fID DESC";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":client_id", $client_id);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getFirmById($id)
    {
        $sql = "SELECT * 
                FROM  Firmi 
                WHERE  `ID` =:id
                LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getFirmByBulstat($bulstat)
    {
        $sql = "SELECT * 
                FROM  Firmi 
                WHERE  `bulstat` =:bulstat
                LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":bulstat", $bulstat);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getFirmByDn($dn)
    {
        $sql = "SELECT * 
                FROM  Firmi 
                WHERE  `dn` =:dn
                LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $dn);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getDocumentItems($fID)
    {
        $sql = "SELECT * 
        FROM  `items` 
        WHERE  `fID` = :fID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":fID", $fID);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    /*
     * Return list of fID`s that contain uncompleted orders
     * @param int
     * @return array
     * @throw \PDOException
     */
    public function getOrders($client_id, $completed)
    {
        $sql = "
            SELECT * FROM docs doc
            LEFT JOIN docTypes ON docTypes.docTypeID = doc.docID 
            WHERE doc.fID IN (
            SELECT DISTINCT(d.fID) FROM  `docs` d
            LEFT JOIN items i ON i.fID = d.fID
            WHERE d.docID in (2,0)
            AND d.ClientID = :client_id
            AND i.ok = :completed)
            ORDER BY doc.fID DESC
            ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":client_id", $client_id);
        $stmt->bindValue(":completed", (int) $completed);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getFirmAccount($client_id)
    {
        $sql = "
            SELECT * 
            FROM  `Firmi` 
            JOIN sl ON sl.slID = Firmi.sl
            WHERE  ID = :client_id
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":client_id", $client_id);
        $stmt->execute();
        $tmp = $stmt->fetch(\PDO::FETCH_OBJ);
        return $tmp;
    }

    public function getCalculations($client_id, $completed)
    {
        $sql = "
            SELECT * FROM docs doc
            LEFT JOIN docTypes ON docTypes.docTypeID = doc.docID 
            WHERE doc.fID IN (
            SELECT DISTINCT(d.fID) FROM  `docs` d
            LEFT JOIN items i ON i.fID = d.fID
            WHERE d.docID = -1
            AND d.ClientID = :client_id
            AND i.ok = :completed
            )
            ORDER BY doc.fID DESC
            ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":client_id", $client_id);
        $stmt->bindValue(":completed", (int) $completed);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getDocumentsByIds( $ids, $client_id){
        $sql = "
            SELECT *
            FROM  `docs` d
            WHERE d.fID in(:list_of_ids)
            AND d.ClientID = :client_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":client_id", $client_id);
        $stmt->bindValue(":list_of_ids", implode(",", $ids));
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    /*
     * Return array with detailed information about document(items, prices, deadline, etc)
     * @param int
     * @return array
     * @throw \PDOException
     */
    public function getDocumentById($fID)
    {
        $sql = "
            SELECT * 
            FROM  `docs` d
            LEFT JOIN items i ON i.fID = d.fID
            WHERE d.fID =:fID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":fID", $fID);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function deleteDocument($fID, $user_id){
        $sql = "SELECT * FROM docs WHERE fID=:fID AND cr_userID=:user_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":fID", $fID);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->execute();
        $document = $stmt->fetch(\PDO::FETCH_OBJ);
        if($document && $document->docID < 0) {
            $delete_items = "DELETE FROM items WHERE fID=:fID";
            $stmt = $this->db->prepare($delete_items);
            $stmt->bindValue(":fID", $fID);
            $stmt->execute();

            $delete_docs = "DELETE FROM docs WHERE  fID = :fID";
            $stmt = $this->db->prepare($delete_docs);
            $stmt->bindValue(":fID", $fID);
            $stmt->execute();
            return True;
        }
        return False;
    }

    public function getDocumentsByUserID($user_id)
    {
        $sql = "
            SELECT *
            FROM  `docs` d
            JOIN docTypes dt ON dt.docTypeID = d.docID
            WHERE d.cr_userID = :user_id
            ORDER BY Data DESC
            ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getGroups($SiteID=1)
    {
        $sql = "SELECT * 
            FROM  `itGroups` WHERE SiteID=:site_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":site_id", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getItemGroups($groups="5,6,7,8,9,10,11,12,15,17,18,31,51")
    {
        $sql = "
            SELECT g.ID, g.itGName, SUM(i.itGroup) as products
            FROM  `itGroups` g
            left join itemTypes i ON g.ID = i.itGroup
            WHERE find_in_set(cast(g.ID as char), :groups)
            GROUP BY i.itGroup";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":groups", $groups);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getGroupProducts($group_id, $discount=0)
    {
        $sql = "
            SELECT *, (pr.price_value * ( ( 100 - :discount ) /100 )) AS discounted_price
            FROM pages_to_groups pg
            LEFT JOIN pages p ON p.n = pg.n
            LEFT JOIN prices pr ON pr.price_n = p.n
            LEFT JOIN itemTypes it ON it.itID = pr.price_code
            LEFT JOIN images im ON im.imageID = p.imageNo
            WHERE pg.group_id= :group_id
            AND it.prior > 0
            ORDER BY pg.sort_order ASC
        ";
        // WHERE i.itGroup IN (5,6,7,8,9,10,11,12,15,17,31)
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":group_id", $group_id);
        $stmt->bindParam(":discount", $discount, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getMeasurementUnits()
    {
        $sql  = 'SELECT DISTINCT ( m) FROM  `itemTypes`';
        $stmt= $this->db->prepare($sql);
        $stmt->execute();
        $tmp = array();
        $i = 0;
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[$res->m] = $res->m;
            $i++;
        }
        return $tmp;
    }

    public function getGroupDiscount($company_id, $group)
    {
        $sql = "SELECT * FROM  `discount_ra` WHERE ClientID=:company_id AND date_from <= :now AND date_to >= :now AND itGroup=:group";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":company_id", $company_id);
        $stmt->bindValue(":group", $group);
        $stmt->bindValue(":now", date("Y-m-d H:i:s", time()));
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;

    }

    public function getCompanyDiscounts($company_id, $group)
    {
        $sql = "SELECT * FROM  `discount_ra` WHERE ClientID=:company_id AND date_from <= :now AND date_to >= :now AND itGroup=:group";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":company_id", $company_id);
        $stmt->bindValue(":group", $group);
        $stmt->bindValue(":now", date("Y-m-d H:i:s", time()));
        $stmt->execute();
        $tmp = array();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function getMyCompanies($user_id, $site_id)
    {
        $sql = "
            SELECT *  FROM `users` u
            JOIN Firmi f on f.ID = u.ID
            JOIN SiteAccess sa on sa.userID = u.ID 
            WHERE `ParentUserID` = :user_id
            AND sa.SiteiD=:site_id
            GROUP BY f.ID
        ";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":site_id", $site_id);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getUnpaidSum($client_id){
        $sql = "
            SELECT SUM( docs.TOTAL ) AS total, SUM( docs.paid ) AS paid, SUM( docs.TOTAL ) - SUM( docs.paid ) AS unpaid
            FROM docs, docTypes, Firmi, dost
            WHERE docs.ClientID = Firmi.ID
            AND docs.docID = docTypes.docTypeID
            AND dost.dID = docs.dost
            AND docs.ClientID = :client_id
            AND docs.docID > -1
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":client_id", $client_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function getSiteProducts($site_id, $group_id, $discount=0)
    {
        $sql = "
            SELECT *, (pr.price_value * ( ( 100 - :discount ) /100 )) AS discounted_price
            FROM itemTypes i
            LEFT JOIN prices pr ON pr.price_code = i.itID
            LEFT JOIN pages p ON p.n = pr.price_n
            LEFT JOIN images im ON im.imageID = p.imageNo
            WHERE i.itGroup = :group_id
            AND p.SiteID in(:SiteID, 1018)
            AND p.SecLevel=0
            AND i.prior > 0
            AND pr.price_value > 0
            ORDER BY i.prior DESC
            ";
        // WHERE i.itGroup IN (5,6,7,8,9,10,11,12,15,17,31)
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $site_id);
        $stmt->bindValue(":group_id", $group_id);
        $stmt->bindParam(":discount", $discount, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getProductFromSite($itID)
    {
        $sql = "
            SELECT * 
            FROM itemTypes i
            LEFT JOIN prices pr ON pr.price_code = i.itID
            LEFT JOIN pages p ON p.n = pr.price_n
            LEFT JOIN images im ON im.imageID = p.imageNo
            WHERE i.itID = :itID
            AND p.SecLevel=0
            ";
        // WHERE i.itGroup IN (5,6,7,8,9,10,11,12,15,17,31)
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":itID", $itID);
        $stmt->execute();
        $tmp = array();
        return $res = $stmt->fetch(\PDO::FETCH_OBJ);
    }


    public function getClientTotal($client)
    {
        $sql = "
            SELECT SUM( Neto ) as total FROM docs
            WHERE docs.ClientID = :client_id
            AND docs.docID >=0
            AND docs.Data >= DATE_SUB( CURDATE( ) , INTERVAL 366 DAY )";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":client_id", $client);
        $stmt->execute();
        $sum = $stmt->fetch(\PDO::FETCH_OBJ);
        return $sum;
    }

    public function updateFirm($id, $email, $phone, $fax, $mol, $address, $office, $dn, $bulstat)
    {
        $sql = "UPDATE Firmi 
                SET    
                   EMail = :email, 
                   Phone = :phone, 
                   Fax = :fax, 
                   MOL = :mol, 
                   Address = :address, 
                   Office = :office, 
                   dn = :dn, 
                   bulstat = :bulstat
                WHERE ID=:id
                LIMIT 1 "; 
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":phone", $phone);
        $stmt->bindValue(":fax", $fax);
        $stmt->bindValue(":mol", $mol);
        $stmt->bindValue(":address", $address);
        $stmt->bindValue(":office", $office);
        $stmt->bindValue(":dn", $dn);
        $stmt->bindValue(":bulstat", $bulstat);
        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
        return $stmt->execute();
    }

    public function createCalculation($document_notes,
                                      $ClientID, $qty,
                                      $oobem, $Neto, $DDS,
                                      $Total, $pay_terms,
                                      $payable_to, $user_id, $end_date, $doc_id=-2)
    {
        $sql = "
                INSERT INTO  `maksoft`.`docs` (
                `fID` ,
                `No` ,
                `Description` ,
                `document_notes` ,
                `docID` ,
                `fIDparent` ,
                `dost` ,
                `ClientID` ,
                `short` ,
                `Opisanie` ,
                `qty` ,
                `oobem` ,
                `NoDisc` ,
                `Neto` ,
                `DDS` ,
                `DDStype` ,
                `Total` ,
                `pay_terms` ,
                `payable_to` ,
                `payment_doc` ,
                `paid` ,
                `Payment` ,
                `Srok` ,
                `expedition` ,
                `storage` ,
                `Priel` ,
                `cr_userID` ,
                `userID` ,
                `Data` ,
                `reData` ,
                `st`
                )
                VALUES (
                    NULL ,
                    '0', 
                    '', 
                    :document_notes,
                    :docID, 
                    '0', 
                    '6', 
                    :ClientID,
                    '0',  
                    '',  
                    :qty, 
                    :oobem,  
                    '0',  
                    :Neto, 
                    :DDS,  
                    '0',  
                    :Total, 
                    :pay_terms, 
                    :payable_to,  
                    '0',  
                    '0.00',  
                    '1',  
                    :srok,  
                    '0',  
                    '1', 
                    '', 
                    :user_id,  
                    :user_id,  
                    :date,  
                    '0000-00-00',  
                    '200');";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":document_notes", $document_notes);
        $stmt->bindValue(":ClientID", $ClientID);
        $stmt->bindValue(":qty", $qty);
        $stmt->bindValue(":oobem", $oobem);
        $stmt->bindValue(":Neto", $Neto);
        $stmt->bindValue(":DDS", $DDS);
        $stmt->bindValue(":Total", $Total);
        $stmt->bindValue(":pay_terms", $pay_terms);
        $stmt->bindValue(":payable_to", $payable_to);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":srok", $end_date);
        $stmt->bindValue(":docID", $doc_id);
        $stmt->bindValue(":date", date("Y-m-d", time()));
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function getCreatedItemsForUser($user_id)
    {
    }

    public function createItem($binded_array)
    {
        $sql = "
            INSERT INTO
            `itemTypes`
            (`itID`, `itParentID`, `itGroup`, `it_docID`, `itName`, `itStr`, `m`, `it_weight`, `dost_price`, `qty_on_stock`, `price`, `itPeriod`, `commision`, `discount`, `slType`, `prior`) 
            VALUES
            (:itID, :itParentID, :itGroup, :it_docID, :itName, :itStr, :m, :it_weight, :dost_price, :qty_on_stock, :price, :itPeriod, :commision, :discount, :slType, :prior)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute($binded_array);
        return $this->db->lastInsertId();
    }

    public function updateItem($binded_array)
    {
        $sql = "
            UPDATE
               `itemTypes`
            SET
                itParentID = :itParentID,
                itGroup = :itGroup,
                it_docID = :it_docID,
                itName = :itName,
                itStr = :itStr,
                m = :m,
                it_weight = :it_weight,
                dost_price = :dost_price,
                qty_on_stock = :qty_on_stock,
                price = :price,
                itPeriod = :itPeriod,
                commision = :commision,
                discount = :discount,
                slType = :slType,
                prior = :prior
            WHERE itID = :itID ";
        $stmt = $this->db->prepare($sql);
        $stmt->execute($binded_array);
    }


    public function insertDocumentItems($fID, $index, $opisanie, $sku, $m, $qty, $price, $discount, $sum_price)
    {
        $sql = "
            INSERT INTO `items`
                (`fID`, `No`, `Opisanie`, `itype`, `m`, `qty`, `UnitPrice`, `UnitDisc`, `price`, `payfor`, `completed_time`, `ok`)
            VALUES 
                (:fID, :index, :opisanie, :sku, :m, :qty, :price, :discount, :sum_price, 0, :time, 0);
        ";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":fID", $fID);
        $stmt->bindValue(":index", $index);
        $stmt->bindValue(":opisanie", $opisanie);
        $stmt->bindValue(":sku", $sku);
        $stmt->bindValue(":m", $m);
        $stmt->bindValue(":qty", $qty);
        $stmt->bindValue(":price", $price);
        $stmt->bindValue(":discount", $discount);
        $stmt->bindValue(":sum_price", $sum_price);
        $stmt->bindValue(":time", date("Y-m-d G-i:s", time()));
        $stmt->execute();
        return $this->db->lastInsertId();
    }
}
