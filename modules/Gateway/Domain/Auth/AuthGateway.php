<?php
namespace Maksoft\Gateway\Domain\Auth;
use Maksoft\Core\MysqlDb;


class AuthGateway 
{
    protected $db;

    public function  __construct($db)
    {
        $this->db = $db;
    }

    public function checkOauthUser($token)
    {
        $select = "SELECT * FROM oauth_authorizations WHERE oauth_uid=:token";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":token", $token);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res)){
            return false;
        }
        return true;
    }

    public function deleteAuthUser(){
        $token = $_COOKIE['fb'];
        $stmt = $this->db->prepare("DELETE FROM oauth_authorizations WHERE hashed_token = :token");
        $stmt->bindValue(":token", $token);
        $stmt->execute();
    }

    public function isLoggedWithFacebook(){
        if(!isset($_COOKIE['fb'])){
            return false;
        }

        $token = $_COOKIE['fb'];
        $sql = "SELECT * FROM oauth_authorizations WHERE hashed_token = :token LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":token", $token);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function registerOauthUser($provider_id, $oauth_uid, $access_token, $name, $picture, $email, $salt, $hashed_token)
    {
        $insert = "INSERT INTO oauth_authorizations 
            (provider_id, oauth_uid, access_token, name, picture, email, salt, hashed_token)
            VALUES
            (:provider_id, :oauth_uid, :access_token, :name, :picture, :email, :salt, :hashed_token)";
        $stmt = $this->db->prepare($insert);
        $stmt->bindValue(":provider_id", $provider_id);
        $stmt->bindValue(":oauth_uid", $oauth_uid);
        $stmt->bindValue(":access_token", $access_token);
        $stmt->bindValue(":name", $name);
        $stmt->bindValue(":picture", $picture);
        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":salt", $salt);
        $stmt->bindValue(":hashed_token", $hashed_token);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function updateOauthUser($access_token, $name, $picture, $email, $salt, $hashed_token, $oauth_uid){
        $update_query = "
            UPDATE oauth_authorizations 
            SET 
                access_token = :access_token,
                name = :name,
                picture = :picture,
                email = :email,
                salt = :salt,
                hashed_token = :hashed_token
            WHERE
                oauth_uid = :oauth_uid    
            LIMIT 1
        ";

        $stmt = $this->db->prepare($update_query);
        $stmt->bindValue(":oauth_uid", $oauth_uid);
        $stmt->bindValue(":access_token", $access_token);
        $stmt->bindValue(":name", $name);
        $stmt->bindValue(":picture", $picture);
        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":salt", $salt);
        $stmt->bindValue(":hashed_token", $hashed_token);
        $stmt->execute();
    }

    public function getLogin2($username, $pass, $SiteiD){
        $sql = "
            SELECT users.ID, users.Name, users.EMail, users.username, users.pass, users.InitPage, users.InitSiteID, users.AccessLevel, Firmi.Name AS FName, Firmi.sl, Firmi.RA, Firmi.Phone, Firmi.Fax, Firmi.Address, Firmi.MOL, Firmi.dn, Firmi.bulstat
            FROM users
            LEFT JOIN Firmi ON users.FirmID = Firmi.ID
            WHERE users.username = BINARY(  'topexpress' ) 
            AND pass = BINARY(  'topexpress2000' ) 
        ";
    }

    public function getLogin($username, $pass, $SiteID)
    {
        $select = "
            SELECT users.*, sl.slID, SiteAccess.*, oa.* FROM users 
            LEFT JOIN sl on sl.userID=users.ID
            LEFT JOIN oauth_authorizations oa ON oa.user_id = users.ID
            JOIN SiteAccess ON SiteAccess.userID=users.ID
            WHERE users.username=:username AND users.pass=:pass AND SiteAccess.SiteID=:SiteID LIMIT 1";

        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":username", $username);
        $stmt->bindValue(":pass", $pass);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }
    
    public function linkAccounts($user_id, $oauth_uid)
    {
        $update = "UPDATE oauth_authorizations SET user_id=:user_id WHERE oauth_uid=:oauth_uid";    
        $stmt = $this->db($update);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":oauth_uid", $oauth_uid);
        $stmt->execute();
    }

    public function isLogged($ses_id=false)
    {
        if(!$ses_id){
            $ses_id = session_id();
        }
        $stmt = $this->db->prepare(" SELECT * FROM session WHERE id = :session_id AND logged = 1");
        $stmt->bindValue(":session_id", $ses_id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getLinkedData($user_id, $SiteID)
    {
        $select = "
            SELECT users.*, sl.slID, SiteAccess.*, oa.* FROM users 
            LEFT JOIN sl on sl.userID=users.ID
            LEFT JOIN oauth_authorizations oa ON oa.user_id = users.ID
            JOIN SiteAccess ON SiteAccess.userID=users.ID
            WHERE users.ID=:user_id SiteAccess.SiteID=:SiteID LIMIT 1";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function  registerUser()
    {

    }

    public function registerFailedAttempt($username, $ip, $event)
    {
        $update = "
            INSERT INTO `flood`(`event`, `hostname`, `username`, `ip`, `timestamp`)
            VALUES
                (:event, :host, :username, :ip, :now)";
        $stmt = $this->db->prepare($update);
        $stmt->bindValue(":host", $_SERVER['SERVER_NAME'] ?  $_SERVER['SERVER_NAME']:'null');
        $stmt->bindValue(":username", $username);
        $stmt->bindValue(":event", $event);
        $stmt->bindValue(":ip", $ip ? $ip : 0);
        $stmt->bindValue(":now", time());
        $stmt->execute();
    }

    public function registerSuccesfullLogin($user_id, $site_id, $n, $ip)
    {
        $sql = "
            INSERT INTO `user_logs`(`user`, `SiteID`, `n`, `IP`, `logTime`)
                VALUES (:user_id, :site_id, :n, :ip) 
        ";

        $stmt  = $this->db->prepare($sql);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":site_id", $site_id);
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":ip", $ip);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function countFailedAttempts($ip, $event, $block_time=3600)
    {
        $stmt = $this->db->prepare("
            SELECT COUNT(fid) FROM flood 
            WHERE ip=:ip 
            AND event=:event
            AND  `timestamp` > TIMESTAMP - :block_time
            ");
        $stmt->bindValue(":ip", $ip);
        $stmt->bindValue(":event", $event);
        $stmt->bindParam(":block_time", $block_time, \PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res[0];
    }

    public function getLastFailedAttempt($ip, $event)
    {
        $stmt = $this->db->prepare(
            "SELECT * FROM flood WHERE ip=:ip AND event=:event ORDER BY timestamp DESC LIMIT 1");
        $stmt->bindValue(":ip", $ip);
        $stmt->bindValue(":event", $event);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    /**
    * Check if user ip is currently blocked
    *
    * @param string ip of current session user
    * @return boolean
    */
    public function isBlocked($ip, $event)
    {
        $stmt = $this->db->prepare(
            "SELECT * FROM block WHERE ip=:ip AND block_until > :now AND event=:event");
        $stmt->bindValue(":ip", $ip);
        $stmt->bindValue(":now", time());
        $stmt->bindValue(":event", $event);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function checkIp($ip)
    {
        $stmt = $this->db->prepare(
            "SELECT * FROM block WHERE ip=:ip AND event=:event");
        $stmt->bindValue(":ip", $ip);
        $stmt->bindValue(":event", $event);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function blockIp($ip, $block_time, $event)
    {  
        $stmt = $this->db->prepare(
            "REPLACE INTO block SET block_until=:block , active=1, ip=:ip, event=:event"
        );
        $stmt->bindValue(":event", $event);
        $stmt->bindValue(":block", $block_time);
        $stmt->bindValue(":ip", $ip);
        $stmt->execute();
    }
    
    public function removeBlock($ip, $event)
    {
        $stmt = $this->db->prepare(
            "DELETE FROM block WHERE ip=:ip AND event=:event");
        $stmt->bindValue(":ip", $ip);
        $stmt->bindValue(":event", $event);
        $stmt->execute();
    }

    public function getBlockTime($ip, $event)
    {
        $stmt = $this->db->prepare(
            "SELECT * from block where event=:event AND ip=:ip");
        $stmt->bindValue(":event", $event);
        $stmt->bindValue(":ip", $ip);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res->block_until;
    }

    public function removeFailedAttempts($ip, $event)
    {
        $stmt = $this->db->prepare(
            "DELETE FROM flood WHERE ip=:ip AND event=:event AND timestamp <= :blocktime");
        $stmt->bindValue(":ip", $ip);
        $stmt->bindValue(":event", $event);
        $stmt->bindValue(":blocktime", $this->getBlockTime($ip, $event));
        $stmt->execute();
    }
    
}


