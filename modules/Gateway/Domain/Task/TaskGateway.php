<?php
namespace Maksoft\Gateway\Domain\Task;
use Maksoft\lib\MysqlDb;

class TaskGateway
{
    protected $db;

    public function __construct(MysqlDb $db)
    {
        $this->db = $db;
    }

    public function get_tasks($to_user, $start_date="", $filter = 1)
    {
        if($start_date == ""){
            $stmt=$this->db->prepare("
                SELECT * FROM tasks WHERE
                to_user=:to_user
                AND completed='0'
                AND start_date<NOW()
                AND :filter 
                ORDER BY priority ASC,  start_date ASC  ");
            $stmt->bindValue(":to_user", $to_user);
            $stmt->bindValue(":filter", $filter);
        } else {
            $stmt=$this->db->prepare("
                SELECT * FROM tasks 
                WHERE to_user=:to_user 
                AND completed='0'
                AND start_date>=:start_date 
                AND :filter ORDER BY priority ASC,  start_date ASC ");
            $stmt->bindValue(":start_date", $start_date);
        }
        $stmt->bindValue(":to_user", $to_user);
        $stmt->bindValue(":filter", $filter);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch()){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function setCommentStateToReaded($task_id, $user_id) {
        $sql = "UPDATE task_comments SET viewed=1 WHERE task_id = :task_id and user_id = :user_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":task_id", $task_id, \PDO::PARAM_INT);
        $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
        $stmt->execute();
    }

    public function countNewTaskComments($task_id, $user_id) {

        $sql = '
        SELECT COUNT( id ) as lenght 
        FROM  `task_comments` 
        WHERE task_id =:task_id
        AND user_id !=:user_id
        AND viewed = 0
        ';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":task_id", $task_id, \PDO::PARAM_INT);
        $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_OBJ);
        return $result->lenght;
    }

    public function getTasks($user_id)
    {
        $sql = "
            SELECT taskID, priority, start_date, subject, DATE_ADD( start_date, INTERVAL period
            DAY ) AS end_date, u.Name, t.period, t.notes, f.Name as company_name, tt.description as task_type 
            from tasks t
            JOIN users u ON u.ID = t.from_user 
            JOIN task_types tt ON tt.task_type_ID = t.task_type 
            LEFT JOIN Firmi f on f.ID = t.ClientID
            WHERE  t.to_user = :user_id
            AND  t.completed =0
            ORDER BY end_date ASC
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] =$row;
        }
        return $tmp;
    }


    public function getUserTasks($user_id)
    {
        $stmt=$this->db->prepare("
            SELECT * FROM tasks WHERE
            to_user=:to_user
            AND completed='0'
            AND start_date<NOW()
            ORDER BY priority ASC,  start_date ASC  ");
        $stmt->bindValue(":to_user", $user_id);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch()){
            $tmp[] =$row;
        }
        return $tmp;
    }

    public function getUserRepeatedTasks($user_id)
    {
        $stmt=$this->db->prepare("
            SELECT taskID, priority, start_date, subject, DATE_ADD( start_date, INTERVAL period
            DAY ) AS end_date, u.Name, t.period, t.notes, f.Name as company_name, tt.description as task_type 
            from tasks t
            JOIN users u ON u.ID = t.from_user 
            JOIN task_types tt ON tt.task_type_ID = t.task_type 
            LEFT JOIN Firmi f on f.ID = t.ClientID
            WHERE t.to_user=:to_user
            AND t.repeat_in > 0
            AND t.start_date<NOW()
            ORDER BY t.priority ASC,  t.start_date ASC  ");
        $stmt->bindValue(":to_user", $user_id);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getUpcomingTasks($user_id)
    {
        $stmt=$this->db->prepare("
            SELECT taskID, priority, start_date, subject, DATE_ADD( start_date, INTERVAL period
            DAY ) AS end_date, u.Name, t.period, t.notes, f.Name as company_name, tt.description as task_type 
            from tasks t
            JOIN users u ON u.ID = t.from_user 
            JOIN task_types tt ON tt.task_type_ID = t.task_type 
            LEFT JOIN Firmi f on f.ID = t.ClientID
                WHERE t.to_user=:to_user 
                AND t.completed='0'
                AND t.start_date>=NOW() 
                ORDER BY t.priority ASC,  t.start_date ASC ");
        $stmt->bindValue(":to_user", $user_id);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getTaskTypes()
    {
        $stmt = $this->db->prepare("SELECT * FROM task_types;");
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    //return e specific user task
    public function getTask($taskID, $user_id)
    {
        $stmt=$this->db->prepare("
            SELECT taskID, priority, start_date, subject, DATE_ADD( start_date, INTERVAL period
            DAY ) AS end_date, u.Name, t.period, t.notes, f.Name as company_name, tt.description as task_type, t.from_user, t.to_user, t.repeat_in, t.completed, t.time_to_finish, t.ClientID 
            from tasks t
            JOIN users u ON u.ID = t.from_user 
            JOIN task_types tt ON tt.task_type_ID = t.task_type 
            LEFT JOIN Firmi f on f.ID = t.ClientID
            WHERE t.taskID=:taskID 
            AND (t.to_user=:to_user OR t.from_user = :to_user)
            ");
        $stmt->bindValue(":taskID", $taskID);
        $stmt->bindValue(":to_user", $user_id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    function add_date($givendate,$day=0,$mth=0,$yr=0) {
        $cd = strtotime($givendate);
        $newdate = date('Y-m-d h:i:s', mktime(date('h',$cd),
            date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
            date('d',$cd)+$day, date('Y',$cd)+$yr));
        return $newdate;
    }

    public function completeTask($taskID, $userId)
    {
        $row = $this->getTask($taskID, $userId);
        if($row->repeat_in>0) {
            $start_date = $this->add_date($row->start_date, $row->repeat_in, 0, 0);
            $stmt = $this->db->prepare("
                UPDATE tasks 
                SET start_date=:start_date
                WHERE taskID=:taskID ");
            $stmt->bindValue(":start_date", $start_date);
        } else {
            $stmt = $this->db->prepare("
                UPDATE tasks 
                SET completed ='1'
                WHERE taskID=:taskID ");
        }
        $stmt->bindValue(":taskID", $taskID);
        $stmt->execute();
    }

    //return all user tasks assigned to other users
    public function getAssignedTasks($from_user)
    {
        $stmt=$this->db->prepare("
            SELECT taskID, priority, start_date, subject, DATE_ADD( start_date, INTERVAL period
            DAY ) AS end_date, u.Name, t.period, t.notes, f.Name as company_name, tt.description as task_type 
            from tasks t
            JOIN users u ON u.ID = t.from_user 
            JOIN task_types tt ON tt.task_type_ID = t.task_type 
            LEFT JOIN Firmi f on f.ID = t.ClientID
            WHERE t.from_user=:from_user
            AND t.from_user<>t.to_user
            AND t.completed='0'
            AND t.start_date<NOW()
            ORDER BY t.start_date DESC, t.priority ASC ");
        $stmt->bindValue(":from_user", $from_user);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }
    
    //insert a new task assigned by the selected user
    public function write_task($taskID, $task_set)
    {
        if ($taskID>0) {
            $date_modified = date_time("Y-m-d H:i:s");
            $stmt = $this->db->prepare("
                UPDATE tasks
                SET :task_set ,
                date_modified= :date_modified
                WHERE taskID=:taskID");
            $stmt->bindValue(":taskID", $taskID);
        } else { //new task
            $date_added = date_time("Y-m-d H:i:s");
            $stmt = $this->db->prepare("
                INSERT INTO 
                tasks SET :task_set ,
                date_added=:date_modified");
        }
        $stmt->bindValue(":task_set", $task_set);
        $stmt->bindValue(":date_modified", $date_modified);
        $stmt->execute();
    }


    public function createTask
    (
        $subject, $notes, $start_date, $from_user, $to_user, $time_to_finish, 
        $task_type=0, $priority=1, $period=3, $client_ID=0, $completed=0, $repeat_in=0
    )
    {
        $now = new \DateTime();
        $sql = "INSERT INTO
                `tasks`(`task_type`, `priority`, `start_date`, `period`, `ClientID`, `from_user`, `to_user`, `time_to_finish`, `subject`, `notes`, `completed`, `repeat_in`, `date_added`, `date_modified`) 
                VALUES
                (
                :task_type, :priority, :start_date, :period, :client_ID, :from_user, :to_user, :time_to_finish, :subject, :notes, :completed, :repeat_in, :date_added, :date_modified) ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":task_type", $task_type);
        $stmt->bindValue(":priority", $priority);
        $stmt->bindValue(":start_date", $start_date);
        $stmt->bindValue(":period", $period);
        $stmt->bindValue(":client_ID", $client_ID);
        $stmt->bindValue(":from_user", $from_user);
        $stmt->bindValue(":to_user", $to_user);
        $stmt->bindValue(":time_to_finish", $time_to_finish);
        $stmt->bindValue(":subject", $subject);
        $stmt->bindValue(":notes", $notes);
        $stmt->bindValue(":completed", $completed);
        $stmt->bindValue(":repeat_in", $repeat_in);
        $stmt->bindValue(":date_added", $now);
        $stmt->bindValue(":date_modified", $now);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function updateTask
    (
        $taskId, $subject, $notes, $start_date, $from_user, $to_user, $time_to_finish, 
        $task_type=0, $priority=1, $period=3, $client_ID=0, $completed=0, $repeat_in=0
    )
    {
        $now = new \DateTime();
        $sql = "
            UPDATE tasks SET 
            task_type = :task_type,
            priority = :priority,
            start_date = :start_date,
            period = :period,
            ClientID = :client_ID,
            from_user = :from_user,
            to_user = :to_user,
            time_to_finish = :time_to_finish,
            subject = :subject,
            notes = :notes,
            completed = :completed,
            repeat_in = :repeat_in,
            date_modified = :date_modified
            WHERE taskID = :taskId ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":taskId", $taskId);
        $stmt->bindValue(":task_type", $task_type);
        $stmt->bindValue(":priority", $priority);
        $stmt->bindValue(":start_date", $start_date);
        $stmt->bindValue(":period", $period);
        $stmt->bindValue(":client_ID", $client_ID);
        $stmt->bindValue(":from_user", $from_user);
        $stmt->bindValue(":to_user", $to_user);
        $stmt->bindValue(":time_to_finish", $time_to_finish);
        $stmt->bindValue(":subject", $subject);
        $stmt->bindValue(":notes", $notes);
        $stmt->bindValue(":completed", $completed);
        $stmt->bindValue(":repeat_in", $repeat_in);
        $stmt->bindValue(":date_modified", $now);
        $stmt->execute();
    }
    
    public function addCommentToTask($taskId, $userId, $commentId) {
        $sql = 'INSERT INTO `task_comments`(`task_id`, `user_id`, `comment`) VALUES (:task_id, :user_id, :comment_id)';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":task_id", $taskId);
        $stmt->bindValue(":user_id", $userId);
        $stmt->bindValue(":comment_id", $commentId);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function getTaskComments($taskId) {
        $sql = 'SELECT tc.comment, u.Name, tc.created_at, tc.viewed
            FROM  `task_comments` tc 
            JOIN users u on u.ID = tc.user_id
            WHERE  tc.task_id =:task_id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":task_id", $taskId);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }
    public function get_uSites($user_id, $filter=1)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM SiteAccess sa
            RIGHT JOIN Sites s on sa.SiteID = s.SitesID
            WHERE sa.userID = :user_id AND :filter
            ORDER by s.Name ASC");
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":filter", $filter);
        $stmt->execute();
        $data = array();
        while($row = $stmt->fetch()){
            #yield($row);
            $data[] = $row;
        }
        return $data;
    }
}
