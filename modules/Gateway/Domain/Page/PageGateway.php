<?php
namespace Maksoft\Gateway\Domain\Page;
use Maksoft\lib\MysqlDb;


class PageGateway
{
    protected $db;

    public function  __construct($db)
    {
        $this->db = $db;
    }

    public function get_page_strings($n, $lang_id)
    {
        $q = "SELECT * FROM pages_text WHERE n=:n and lang_id=:lang_id";
        $stmt = $this->db->prepare($q);
        $stmt->bindValue(':n', $n);
        $stmt->bindValue(':lang_id', $lang_id);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }


    public function insert_page_version($n, $lang_id, $pages_text_id)
    {
        $q = "INSERT INTO page_versions
                  (n, lang_id, pages_text_id)
              VALUES
                  (:n, :lang_id, :pages_text_id)
              ON DUPLICATE KEY UPDATE lang_id=:lang_id, pages_text_id =:pages_text_id";
        $stmt = $this->db->prepare($q);
        $stmt->bindValue(':n', $n);
        $stmt->bindValue(':lang_id', $lang_id);
        $stmt->bindValue(':pages_text_id', $pages_text_id);
        $stmt->execute();
        $stmt = $this->db->query("SELECT LAST_INSERT_ID()");
        $lastId = $stmt->fetchColumn();
        return $lastId;
    }

    public function getPage($n)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM pages p
            LEFT JOIN images im on p.imageNo = im.imageID
            WHERE p.n=:n LIMIT 1");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }

    public function insert_slug($SiteID, $n, $slug)
    {
        $q = "INSERT INTO `page_slug`
                (`slug`, `n`, `SiteID`)
                VALUES (:slug, :n, :SiteID)
                ON DUPLICATE KEY UPDATE
                    slug=:slug
                ";
        $stmt = $this->db->prepare($q);
        $stmt->bindValue(':n', $n);
        $stmt->bindValue(':slug', $slug);
        $stmt->bindValue(':SiteID', $SiteID);
        $stmt->execute();
        $row = $this->db->lastInsertId();
        return $row;
    }

    public function insert_page_texts($n, $lang_id, $name, $title, $slug, $text_str, $tags)
    {
        $sql = "INSERT INTO `pages_text` (
                             `n`,
                             `lang_id`,
                             `Name`,
                             `slug`,
                             `title`,
                             `tags`,
                             `textStr`)
                VALUES      (:n,
                             :lang_id,
                             :name,
                             :slug,
                             :title,
                             :tags,
                             :text_str)
                ON DUPLICATE KEY UPDATE
                             name=:name,
                             slug=:slug,
                             title=:title,
                             tags=:tags,
                             textStr=:text_str

            ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':n', $n);
        $stmt->bindValue(':lang_id', $lang_id);
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':title', $title);
        $stmt->bindValue(':slug', $slug);
        $stmt->bindValue(':text_str', $text_str);
        $stmt->bindValue(':tags', $tags);
        $stmt->execute();
        $stmt = $this->db->query("SELECT LAST_INSERT_ID()");
        $lastId = $stmt->fetchColumn();
        return $lastId;
    }

    public function getEditPage($n, $SiteID)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM pages p
            LEFT JOIN images im on p.imageNo = im.imageID
            WHERE p.n=:n
            AND p.SiteID=:SiteID
            LIMIT 1");
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $row = $stmt->fetch();
        return $row;
    }

    public function getPageKeywords($n, $SiteID)
    {

        $keywords_query = "SELECT DISTINCT keywords, n, SiteID
            FROM keywords WHERE SiteID = :SiteID
            AND keywords != ''
            AND n = :n
            ORDER BY enter_time ASC
            LIMIT 25";
        $stmt = $this->db->prepare($keywords_query);
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row =$stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] =  $row;
        }
        return $tmp;
    }

    public function getQuest($SiteID, $n)
    {
        $stmt = $this->db->prepare("
            SELECT *  FROM anketi
            WHERE SiteID = :SiteID AND n = :n LIMIT 1");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function getPageSlugByN($pn, $SiteID)
    {
        $stmt = $this->db->prepare("SELECT * FROM page_slug WHERE n=:n AND SiteID=:sID");
        $stmt->bindValue(":n", $pn);
        $stmt->bindValue(":sID", $SiteID);
        if($stmt->execute()){
            $res = $stmt->fetch(\PDO::FETCH_OBJ);
            return $res->slug;
        }
        return false;
    }

    public function getPageNBySlug($slug, $SiteID)
    {
        $stmt = $this->db->prepare("SELECT * FROM page_slug WHERE slug=:slug AND SiteID=:Sid");
        $stmt->bindValue(":slug", iconv('cp1251', 'utf8', $slug));
        $stmt->bindValue(":Sid", $SiteID);
        if($stmt->execute()){
            $res = $stmt->fetch(\PDO::FETCH_OBJ);
                return $res->n;
        }
        return $false;
    }

    public function getParentPageOfIndex($SiteID,  $condition,  $filter,  $sort_order)
    {
        $stmt = $this->db->prepare("
                SELECT * FROM pages p
                left join images im on p.imageNo = im.imageID
                WHERE p.SiteID = :SiteID :condition
                AND :filter ORDER by :sort_order");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":condition", $condition);
        $stmt->bindValue(":filter", $filter);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[]= $row;
        }
        return $tmp;
    }

    public function getParentPage( $n, $condition, $filter,  $sort_order, $user_access_level)
    {
        $query = "
            SELECT * FROM pages p
            LEFT JOIN images im on p.imageNo = im.imageID
            WHERE p.ParentPage = :parent_page
            AND p.SecLevel <= :user_level
            :condition
            AND :filter ORDER by :sort_order";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":parent_page", $n);
        $stmt->bindValue(":condition", $condition);
        $stmt->bindValue(":filter", $filter);
        $stmt->bindValue(":user_level", $user_access_level);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }
        
    public function getPagesByGroupId($group_id, $SiteID){
        $sql = "SELECT * FROM pages p WHERE p.show_group_id = :group_id and p.SiteID = :SiteID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":group_id", $group_id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);

    }


    //return array of all page groups
    public function  getPageGroups( $n, $sort_order = "g.group_id ASC")
    {
        $stmt = $this->db->prepare("
            SELECT * FROM pages_to_groups pg
            LEFT JOIN groups g on pg.group_id = g.group_id
            WHERE pg.n = :n
            ORDER BY :sort_order");
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }
    //return array of all page groups
    public function  getSiteGroups($SiteID)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM groups g WHERE g.SiteID = :SiteID ORDER by g.group_title ASC
        ");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    //return selected group info
    public function  get_pGroupInfo(integer $group_id, $return = NULL)
    {
        $stmt = $this->db->prepare("SELECT * FROM groups WHERE group_id = :group_id LIMIT 1");
        $stmt->bindValue(":group_id", $group_id);
        $stmt->execute();
        $gInfo = $stmt->fetch();
        if($return)
            return $gInfo[$return];
        else
            return $gInfo;
    }

    //return all pages in selected group
    public function  getPageGroupContent($group_id, $SiteID, $userAccessLevel, $sort_order)
    {
        $orders = array(
            "p.n ASC",
            "p.n DESC",
            "p.date_added ASC",
            "p.date_added DESC",
        );
        $orderBy = "";
        if(array_key_exists($sort_order, $orders)){
            $orderBy = "ORDER BY ".$orders[$sort_order];
        }
        $sql = "SELECT * FROM pages p
                LEFT JOIN images im ON p.imageNo = im.imageID
                LEFT JOIN pages_to_groups ptg ON ptg.n = p.n
                LEFT JOIN groups g ON ptg.group_id = g.group_id
                WHERE g.group_id =3394
                AND g.group_id = :group_id
                AND g.SiteID = :SiteID
                AND p.SecLevel <= :sec_lvl
                $orderBy";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":group_id", $group_id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":sec_lvl", $userAccessLevel);
        $stmt->execute();
        $tmp = array();
        while ($row = $stmt->fetch()){
            $tmp[] =  $row;
        }
        return $tmp;
    }

    public function getAlligns($allign)
    {
        $stmt = $this->db->prepare("SELECT * FROM alligns WHERE allignID = :allign LIMIT 1");
        $stmt->bindValue(":allign", $allign);
        $stmt->execute();
        $img_align = $stmt->fetch();
        return $img_align;
    }

    public function get_parentPage(integer $n)
    {
        $select = "SELECT ParentPage FROM pages WHERE n=:n";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function getCurrencyList()
    {
        $stmt = $this->db->prepare('SELECT * FROM `currencies`');
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function  getPagePrice($n, $condition="ASC")
    {
        $sort = array("ASC" => "ASC", "DESC" => "DESC");
        if(array_key_exists($condition, $sort)){
            $sort = $sort[$condition];
        }else{
            $sort = "ASC";
        }
        $page_prices_sql = "SELECT * FROM prices p
                            left join currencies c on p.price_currency = c.currency_id
                            WHERE p.price_n = :price_n
                            ORDER by p.price_value :condition";
        $stmt = $this->db->prepare("
            SELECT * FROM prices p
            LEFT JOIN currencies c on p.price_currency = c.currency_id
            WHERE p.price_n =:n ORDER by p.price_value ".$sort);
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function  getPageBanners($SiteID)
    {
        $query = "SELECT p.n, im.imageName, im.image_src, p.Name, p.title
                  FROM pages p
                  LEFT JOIN images im ON p.imageNo = im.imageID
                  WHERE p.image_allign = 21
                  AND im.image_src != ''
                  AND p.SiteID = :SiteID
                  ORDER by p.sort_n ASC";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($pBanner = $stmt->fetch()){
            $tmp[]= $pBanner;
        }
        return $tmp;
    }

    public function insertComment($n, $subject, $author, $email,  $text, $image, $url="", $user_id=0, $parent=0, $status=0){
        $sql = "INSERT INTO  `maksoft`.`comments`
                    (
                        `comment_id` ,
                        `comment_n` ,
                        `comment_subject` ,
                        `comment_author` ,
                        `comment_author_email` ,
                        `comment_author_url` ,
                        `comment_author_ip` ,
                        `comment_date` ,
                        `comment_text` ,
                        `comment_parent` ,
                        `comment_user_id` ,
                        `comment_image` ,
                        `comment_status`
                    )
                    VALUES (
                        NULL ,  :n,  :subject,  :author,  :email,  :url,  :ip,  now(),  :text,  :parent,  :user_id, :image,  :status
                    );";

        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":subject", $subject);
        $stmt->bindValue(":author", $author);
        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":url", $url);
        $stmt->bindValue(":ip", $_SERVER['REMOTE_ADDR']);
        $stmt->bindValue(":text", $text);
        $stmt->bindValue(":parent", $parent);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":image", $image);
        $stmt->bindValue(":status", $status);
        $stmt->execute();
        return $this->db->lastInsertId();
    }


    public function getPageN(){

    }

    public function getComment($comment_id, $filter)
    {
        $stmt = $this->db->prepare("SELECT * FROM comments WHERE comment_id = :comment_id AND :filter");
        $stmt->bindValue(':comment_id', $comment_id);
        $stmt->bindValue(':filter', $filter);
        $stmt->execute();
        $comment = $stmt->fetch();
        return $comment;
    }

    public function  getPageComments($n, $filter, $sort_order)
    {
        $stmt = $this->db->prepare("SELECT * FROM comments WHERE comment_n = :n
                                    AND comment_status>0
                                    AND :filter ORDER by :sort_order");
        $stmt->bindValue(':n', $n);
        $stmt->bindValue(':filter', $filter);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        $tmp = array();
        while($comment = $stmt->fetch()){
            $tmp[]= $comment;
        }
        return $tmp;
    }

    public function  getPageOptions($n)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM page_options po
            LEFT JOIN options o on po.option_id = o.option_id WHERE po.n = :n");
        $stmt->bindValue(':n', $n);
        $stmt->execute();
        $tmp = array();
        while ($pOption = $stmt->fetch()){
            $tmp[] = $pOption;
        }
        return $tmp;
    }

    public function  getPageTemplates($SiteID)
    {
        $pt_sql = "
            SELECT * FROM pt_descriptions ptd
            WHERE ptd.SiteID = :SiteID";
        $stmt = $this->db->prepare($pt_sql);
        $stmt->bindValue(':SiteID', $SiteID);
        $stmt->execute();
        $tmp = array();
        while($pt = $stmt->fetch())
            $tmp[] =  $pt;
        return $tmp;
    }

    public function  getPageTemplate($n, $templ_id)
    {
        $pt_sql = "SELECT * FROM page_templates pt
                   left join pt_descriptions ptd on pt.pt_id = ptd.pt_id
                   WHERE pt.n = :n
                   AND ptd.template_id = :templ LIMIT 1";
        $stmt = $this->db->prepare($pt_sql);
        $stmt->bindValue(':n', $n);
        $stmt->bindValue(':templ', $templ_id);
        $stmt->execute();
        $pt = $stmt->fetch();
        return $pt;
    }

    public function moveToTrash($n)
    {
        $stmt = $this->db->preapre("UPDATE pages SET status = -1 WHERE n = :n LIMIT 1 ");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
    }

    public function deletePage($n)
    {
        $stmt = $this->db->prepare("DELETE FROM pages WHERE n = :n LIMIT 1");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        $stmt = $this->db->prepare("UPDATE pages SET an='0' WHERE pages.an=:n");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        //remove page from all groups
        $stmt = $this->db->prepare("DELETE FROM pages_to_groups WHERE n=:n");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
    }

    public function getPpagesNotEqualStatusZero($parent_page, $st)
    {
        $status = array(
            0 => ' != 0',
            1 => ' = 0'
        );
        $stmt = $this->db->prepare("SELECT * FROM pages WHERE ParentPage=:parent_page AND status :status");
        $stmt->bindValue(":parent_page", $parent_page);
        $stmt->bindValue(":status", $status[$st]);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] =  $row;
        }
        return $tmp;
    }
    public function emptyPage($n)
    {
        $stmt = $this->db->prepare("
            UPDATE pages
            SET an = 0, tags = '', PHPvars = '',
            textStr = '', PHPcode = '', toplink = 0,
            show_link = 1, show_group_id = 0, show_link_order = 'sort_n ASC',
            show_link_cols = 1, make_links = 1, PageURL = '',
            imageNo = 0, imageWidth = '200', image_allign = 1,
            date_modified = 'now()'
            WHERE n = :n LIMIT 1 ");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
    }

    public function  restorePage($n)
    {
        $stmt = $this->db->prepare("UPDATE pages SET status = 1 WHERE n = :n LIMIT 1");
        $stmt->bindValue(":n", $n);
        return $stmt->execute();
    }

    public function  insertFile($file, $path, $dir_id)
    {
        $sql = "
            INSERT INTO images
            SET imageName = :file_title,
            image_src =  :image_src,
            image_dir = :image_dir";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":file_title", $file);
        $stmt->bindvalue(":image_src", $path);
        $stmt->bindValue(":image_dir", $dir_id);
        $stmt->execute();
        $imageID = $this->db->lastInsertId();
        return $imageID;
    }

    /*
     * count user reprint docs
     */
    public function countReprintDocs($uslID)
    {
        $reprint_query = "
            SELECT * FROM docs d, docTypes dt, Firmi f
            WHERE  d.ClientID=f.ID AND d.reData != '0000-00-00'
            AND (d.reData <= DATE_ADD(CURDATE(), INTERVAL 30 DAY))
            AND d.docID=dt.docTypeID  AND dt.docTypeID>=0
            AND f.sl = :uslID
            ORDER BY d.reData ASC";
        $stmt = $this->db->prepare($reprint_query);
        $stmt->bindValue(":uslID", $uslID);
        $stmt->execute();
        return $stmt->rowCount();
    }


    public function countSiteComments($SiteID)
    {
        $comments_query = "
            SELECT * FROM comments c, pages p
            WHERE c.comment_status=0
            AND p.SiteID = :SiteID
            AND p.n = c.comment_n";
        $stmt = $this->db->prepare($comments_query);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function addPreview(integer $n, $i=1)
    {
        if($i<0 or !is_numeric($i)){
            $i=1;
        }
        $stmt = $this->db->prepare("UPDATE pages SET preview=preview+:i WHERE n=':n");
        $stmt->bindValue(':i', (integer) $i);
        $stmt->bindValue(':n', $n);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function getMirrorPages($n, $an)
    {
        $stmt = $this->db->prepare("SELECT n, an FROM pages WHERE n=:n AND an=:an");
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":an", $an);
        $stmt->execute();
        $row = $stmt->fetchAll();
        return $row;
    }

    public function pageEditLog($no)
    {
        $stmt = $this->db->prepare("SELECT DISTINCT(uID) FROM page_edit_log WHERE n=:n ");
        $stmt->bindValue(":n", $no);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getAllPages($SiteID)
    {
        $select = "
            SELECT
            n, ParentPage, Name,title
            FROM
            pages
            WHERE SiteID=:SiteID
            ORDER BY
            ParentPage, Name ";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row=$stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getSiteMapPages($SiteID)
    {
        $select = "
            SELECT
            n as id, ParentPage as parent, Name as title, SecLevel, preview, status, SiteID, sort_n as sort, date_added
            FROM
            pages
            WHERE SiteID=:SiteID
            ORDER BY
            sort_n";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function updatePageEditLog($n, $user_id)
    {
        $insert = "INSERT INTO page_edit_log  SET n = :n, uID = :user_id";
        $stmt = $this->db->prepare($insert);
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->execute();
    }

    public function insertPage(
        $Name, $ParentPage, $title, $slug,
        $textStr, $show_link, $show_link_order, $show_link_cols,
        $make_links, $tags, $image_allign, $imageWidth, $PageURL, $toplink,
        $status, $SiteID, $SecLevel, $author, $show_group_id=0
    )
    {
        $insert_sql = "
            INSERT INTO pages
            SET
                Name = :Name,
                ParentPage = :ParentPage,
                title=:title,
                slug=:slug,
                textStr=:textStr,
                show_link=:show_link,
                show_link_order = :show_link_order ,
                show_link_cols=:show_link_cols,
                make_links=:make_links,
                tags=:tags,
                image_allign = :image_allign,
                imageWidth=:imageWidth,
                PageURL=:PageURL,
                toplink=:toplink,
                status=:status,
                SiteID=:SiteID,
                SecLevel=:SecLevel,
                date_added= NOW(),
                date_modified = NOW(),
                show_group_id = :show_group_id,
                author = :author
                ";
        $stmt = $this->db->prepare($insert_sql);
        $stmt->bindValue(':Name', $Name);
        $stmt->bindValue(':ParentPage', $ParentPage);
        $stmt->bindValue(':title', $title);
        $stmt->bindValue(':slug', $slug);
        $stmt->bindValue(':textStr', $textStr);
        $stmt->bindValue(':show_link', $show_link);
        $stmt->bindValue(':show_link_order', $show_link_order);
        $stmt->bindValue(':show_link_cols', $show_link_cols);
        $stmt->bindValue(':make_links', $make_links);
        $stmt->bindValue(':tags', $tags);
        $stmt->bindValue(':image_allign', $image_allign);
        $stmt->bindValue(':imageWidth', $imageWidth);
        $stmt->bindValue(':PageURL', $PageURL);
        $stmt->bindValue(':toplink', $toplink);
        $stmt->bindValue(':status', $status);
        $stmt->bindValue(':SiteID', $SiteID);
        $stmt->bindValue(':SecLevel', $SecLevel);
        $stmt->bindValue(':show_group_id', $show_group_id);
        $stmt->bindValue(':author', $author);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function updatePage(
        $Name, $ParentPage, $title, $slug,
        $textStr, $show_link, $show_link_order,
        $show_link_cols, $make_links, $tags, $image_align,
        $imageWidth, $pageURL, $toplink, $status,
        $SecLevel, $n, $show_group_id=0
    )
    {
        $update = "
            UPDATE pages SET
                Name = :Name,
                ParentPage = :ParentPage,
                title=:title,
                slug=:slug,
                textStr=:textStr,
                show_link=:show_link,
                show_link_order = :show_link_order ,
                show_link_cols=:show_link_cols,
                make_links=:make_links,
                tags=:tags,
                image_allign = :image_allign,
                imageWidth=:imageWidth,
                PageURL=:PageURL,
                toplink=:toplink,
                status=:status,
                SecLevel=:SecLevel,
                date_modified = NOW(),
                show_group_id = :show_group_id
            WHERE n=:n";
        $stmt = $this->db->prepare($update);
        $stmt->bindValue(':Name', $Name);
        $stmt->bindValue(':ParentPage', $ParentPage);
        $stmt->bindValue(':title', $title);
        $stmt->bindValue(':slug', $slug);
        $stmt->bindValue(':textStr', $textStr);
        $stmt->bindValue(':show_link', $show_link);
        $stmt->bindValue(':show_link_order', $show_link_order);
        $stmt->bindValue(':show_link_cols', $show_link_cols);
        $stmt->bindValue(':make_links', $make_links);
        $stmt->bindValue(':tags', $tags);
        $stmt->bindValue(':image_allign', $image_allign);
        $stmt->bindValue(':imageWidth', $imageWidth);
        $stmt->bindValue(':PageURL', $pageURL);
        $stmt->bindValue(':toplink', $toplink);
        $stmt->bindValue(':status', $status);
        $stmt->bindValue(':SecLevel', $SecLevel);
        $stmt->bindValue(':show_group_id', $show_group_id);
        $stmt->bindParam(':n', $n, \PDO::PARAM_INT);
        $stmt->execute();
        return $n;
    }

    public function setPageSortN($n)
    {
        $stmt = $this->db->prepare("UPDATE pages SET sort_n=:n WHERE n=:n");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
    }

    public function setPageImage($n, $imageID)
    {
        $stmt = $this->db->prepare("UPDATE pages SET imageNo = :imageID WHERE n=:n");
        $stmt->bindValue(":imageID", $imageID);
        $stmt->bindValue(":n", $n);
        $stmt->execute();
    }

    public function insertSlug($slug, $n, $SiteID)
    {
        $stmt = $this->db->prepare("INSERT INTO page_slug (slug, n, SiteID) VALUES (:slug, :n, :SiteID)");
        $stmt->execute(array(
            ":n" => $n,
            ":slug" => $slug,
            ":SiteID" => $SiteID
        ));
        return $this->db->lastInsertId();
    }

    public function insertImage($img_name, $img_src, $img_dir)
    {
        $replace ="
            INSERT INTO images
            (imageName, image_src, image_dir, image_date_added, image_date_modified)
            VALUES
            (:img_name, :img_src, :img_dir, now(), now())";
        $stmt = $this->db->prepare($replace);
        $stmt->bindValue(":img_name", $img_name);
        $stmt->bindValue(":img_src", $img_src);
        $stmt->bindValue(":img_dir", $img_dir);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function deleteImage($imageID)
    {
        $stmt = $this->db->prepare("DELETE FROM images WHERE imageID=:imageID");
        $stmt->execute(array(":imageID"=>$imageID));
    }

    public function deletePageGroups($group_id)
    {
        $stmt = $this->db->prepare("DELETE FROM pages_to_groups WHERE id = :group_id");
        $stmt->execute(array(":group_id", $group_id));
        return $this->db->lastInsertId();
    }

    public function insertPageToGroup($n, $group_id, $label, $end_date)
    {
        $insert = "
            INSERT INTO pages_to_groups (n, group_id, label, start_date, end_date)
            VALUES (:n, :group_id, :label, NOW(), :end_date) ";
        $stmt = $this->db->prepare($insert);
        $stmt->execute(array(
            ":n" => $n,
            ":group_id" => $group_id,
            ":label" => $label,
            ":end_date" => $end_date
        ));
	    return $this->db->lastInsertId();
    }

    public function insertPageTemplate($n, $pt_id)
    {
        $insert = "INSERT INTO page_templates (n, pt_id)
            VALUES (:n, :pt_id)";
        $stmt = $this->db->prepare($insert);
        $stmt->execute(array(
            ":n" => $n,
            ":pt_id" => $pt_id
        ));
        return $this->db->lastInsertId();
    }

    public function deletePageTemplates($n)
    {
		$stmt = $this->db->prepare("DELETE FROM page_templates WHERE n = :n");
        $stmt->execute(array(":n"=>$n));
    }

    public function get_price($n)
    {
        $sql = "SELECT * FROM prices WHERE price_n=:price_n";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":price_n", $n);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function update_price($n, $price, $code, $description, $moq=1)
    {
        $sql = "
            UPDATE prices
            SET
                price_pcs = :pcs,
                price_value = :price
                price_code = :code,
                currency = :currency
                price_description = :description
                price_changed = :updated_at
            WHERE price_n = :n
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":pcs", $moq);
        $stmt->bindValue(":price", $price);
        $stmt->bindValue(":code", $code);
        $stmt->bindValue(":description", $description);
        $stmt->bindValue(":updated_at", date('Y-m-d g:h:i', time()));
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function deletePrices($n, $SiteID) {
        $sql = "DELETE FROM prices WHERE price_n = :n AND price_SiteID = :SiteID ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
    }

    public function insertPagePrice($n, $site_id, $description, $code, $moq, $qty, $price, $currency=2)
    {
        $sql = "
            INSERT INTO `prices`(`price_n`, `price_SiteID`, `price_description`, `price_code`, `price_pcs`, `price_qty`, `price_value`, `price_currency`, `price_changed`)
            VALUES (
                :price_n,
                :price_SiteID,
                :description,
                :code,
                :price_pcs,
                :qty,
                :price_value,
                :currency,
                :updated_at
            )
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":price_n", $n);
        $stmt->bindValue(":price_SiteID", $site_id);
        $stmt->bindValue(":description", $description);
        $stmt->bindValue(":code", $code);
        $stmt->bindValue(":price_pcs", $moq);
        $stmt->bindValue(":qty", $qty);
        $stmt->bindValue(":currency", $currency);
        $stmt->bindValue(":price_value", $price);
        $stmt->bindValue(":updated_at", date('Y-m-d g:h:i', time()));
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function replacePagePrices($kwargs)
    {
        $price_query = "
            REPLACE INTO prices
            SET
                price_description= :descr, price_code=:code, price_n = :n, price_SiteID = :SiteID,
                price_qty = :qty, price_pcs = :pcs, price_value = :value price_currency = :currency ";
        $stmt = $this->db->prepare($price_query);
        $stmt->execute($kwargs);
        return $this->db->lastInsertId();
    }

    public function getPageAuthor($user_id)
    {
        $stmt = $this->db->prepare("SELECT Name FROM users WHERE users.ID=:user_id");
        $stmt->bindValue(":user_id", $user_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $res;
    }
}
?>
