<?php 
namespace Maksoft\Gateway\Domain\Statistic;
use Maksoft\Core\MysqlDb;
use Maksoft\Gateway\Gateway;



class StatGateway
{
    protected $db;

    public function __construct(MysqlDb $db)
    {
        $this->db = $db;
    }

    public function createdPagesByMonth($SiteID, $interval)
    {
        $stmt = $this->db->prepare("
            SELECT date_added , COUNT( n ) 
            FROM pages
            WHERE date_added >= NOW( ) - INTERVAL :interval MONTH
            AND SiteID = :SiteID
            GROUP BY MONTH( date_added )");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindParam(":interval", $interval);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch()){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getSiteVisitsDaily($SiteID)
    {
        $sql = 'SELECT SUM(entry) as visits, viewTime
            FROM  `stats` 
            WHERE SiteID = :SiteID
            AND viewTime >= NOW() - INTERVAL 30 DAY
            GROUP BY DAY(viewTime)
            ORDER BY viewTime ASC
            ';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getSearchEnginesHits($SiteID) {
        $stmt = $this->db->prepare("
            SELECT DISTINCT(search_engine) FROM keywords WHERE SiteID =:SiteID
        ");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getMostVisitedPagesThisMonth($SiteID){
        $sql = 'SELECT n, SUM(entry) as visits
            FROM  `stats` 
            WHERE SiteID =:SiteID
            AND MONTH(viewTime) = MONTH(CURRENT_DATE())
            AND YEAR(viewTime) = YEAR(CURRENT_DATE())
            GROUP BY n, MONTH(viewTime)
            ORDER BY visits DESC
            ';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getSiteVisitsLastThreeMonths($SiteID)
    {
        $sql = 'SELECT SUM(entry), MONTHNAME(viewTime)
            FROM  `stats` 
            WHERE SiteID = :SiteID
            AND viewTime >= NOW( ) - INTERVAL 3 MONTH
            GROUP BY MONTH(viewTime)';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    
    public function getOnlineUsers()
    {
        $stmt = $this->db->prepare("
            SELECT id 
            FROM  `session` 
            WHERE modified + lifetime < NOW( )
            AND name=:domain");
        $stmt->bindValue(":domain", $_SERVER['HTTP_HOST']);
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function getMostVisitedPages($SiteID, $limit=10)
    {
        $query = '
            SELECT COUNT( entry ) AS views, n
            FROM  `stats` 
            WHERE SiteID = :SiteID
            AND MONTH( viewTime ) = MONTH( CURRENT_DATE( ) ) 
            GROUP BY n
            ORDER BY views DESC 
            LIMIT '.$limit;
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }


    public function siteViewsByMonth($SiteID, $interval)
    {
        $select = "
            SELECT  viewTime , COUNT( n ) 
            FROM stats
            WHERE viewTime >= NOW( ) - INTERVAL :interval MONTH 
            AND SiteID =:SiteID
            GROUP BY MONTH( viewTime ) 
            ORDER BY viewTime ASC";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":interval", $interval);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch()){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getSiteViewsByInterval($SiteID, $date1, $date2)
    {
        $select = "
            SELECT  viewTime , COUNT( n ) 
            FROM stats
            WHERE viewTime >= NOW( ) - INTERVAL :interval MONTH 
            AND SiteID =:SiteID
            GROUP BY MONTH( viewTime ) 
            ORDER BY viewTime ASC";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":interval", $interval);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch()){
            $tmp[] = $res;
        }
        return $tmp;
    }
    
    public function getTopKeywords($SiteID, $interval, $limit)
    {
        $select = "
            SELECT * 
            FROM  `keywords` 
            WHERE  `SiteID` =:SiteID
            AND g_pos >0
            AND enter_time > NOW( ) - INTERVAL :interval MONTH 
            ORDER BY g_pos ASC 
            LIMIT :limit";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":interval", $interval);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch()){
            $tmp[] = $res;
        }
        return $tmp;
    } 

    public function getKeywordPositions($SiteID, $keyword)
    {
        $stmt = $this->db->prepare("
            SELECT * 
            FROM  `keywords` 
            WHERE  `keywords` LIKE :kw
            AND  `SiteID` = :sid
            ORDER BY g_pos ASC
        ");
        $stmt->bindValue(":sid", $SiteID);
        $stmt->bindParam(":kw", $keyword, \PDO::PARAM_STR);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch()){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getLastEditedPages($SiteID)
    {
        $select = "
            SELECT DISTINCT pl.n, pl.pe_time, pages.Name, Sites.primary_url, Sites.SitesID
            FROM  `page_edit_log` pl
            JOIN pages ON pages.n = pl.n
            LEFT JOIN Sites ON Sites.SitesID = pages.SiteID
            WHERE pages.SiteID=:SiteID
            GROUP BY pl.n
            ORDER BY pe_time DESC 
            LIMIT 0 , 20";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getSiteVisits($SiteID)
    {
        $stmt = $this->db->prepare("SELECT count(*) FROM `stats` WHERE SiteID=:SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function getNewestPages($SiteID, $limit)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM pages
            WHERE SiteID=:SiteID 
            ORDER BY date_added DESC
            LIMIT :limit");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch()){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getCountActivePages($SiteID)
    {
        $stmt = $this->db->prepare("
            SELECT COUNT(n) as result FROM pages
            WHERE SiteID=:SiteID 
            AND status=1 
            AND SecLevel=0");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res['result'];
    }
    
    public function getRegisteredUsersCount($SiteID)
    {
        $stmt = $this->db->prepare("
            SELECT COUNT( userID ) as result 
            FROM  `SiteAccess` 
            WHERE SiteID=:SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res['result'];
    }

    public function getLatestPages($SiteID)
    {
        $select = "
            SELECT *  FROM `pages` 
            WHERE `SiteID` = :SiteID
            ORDER BY date_modified DESC LIMIT 5";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch()){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getTopReferers($SiteID, $limit)
    {
        $select = "
            SELECT ref_count, referer
            FROM (

                SELECT DISTINCT ref_count, referer
                FROM referers
                WHERE SiteID =:SiteID
            ) AS c1
            GROUP BY referer
            ORDER BY ref_count DESC 
            LIMIT :limit";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch()){
            $tmp[] = $res;
        }
        return $tmp;
    }
    
    public function getTopViewedPages($SiteID, $limit)
    {
        $select = "
            SELECT * 
            FROM  `pages` 
            WHERE SiteID =:SiteID
            ORDER BY preview DESC 
            LIMIT :limit";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch()){
            $tmp[] = $res;
        }
        return $tmp;
    }
}
