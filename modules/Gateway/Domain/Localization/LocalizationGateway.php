<?php
namespace Maksoft\Gateway\Domain\Localization;

use Maksoft\Core\MysqlDb;


class LocalizationGateway
{
    protected $db;

    protected $sq;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function insertTranslation($lang_id, $site_id, $base, $text) 
    {
        $q = "INSERT INTO localizations
            (lang_id, site_id, base, translation)
            VALUES
            (:lang_id, :site_id, :base, :text)
            ON DUPLICATE KEY UPDATE text = :text, base = :base";
        $stmt = $this->db->prepare($q);
        $stmt->bindValue(':lang_id', $lang_id);
        $stmt->bindValue(':site_id', $site_id);
        $stmt->bindValue(':base', $base);
        $stmt->bindValue(':text', $text);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function deleteTranslation($id, $site_id)
    {
        $q = "DELETE FROM localizations WHERE id=:id and site_id = :site_id";
        $stmt = $this->db->prepare($q);
        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':site_id', $site_id);
        $stmt->execute();
        return true;
    }
    public function getAllTranslations($site_id) {
        $q = "
            SELECT * FROM localizations ls 
            RIGHT JOIN languages lg ON lg.language_id = ls.lang_id
            WHERE site_id = :site_id";
        $stmt = $this->db->prepare($q);
        $stmt->bindValue(':site_id', $site_id);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getTranslations($site_id, $lang_id) {
        $q = "SELECT lower(base) as base, translation FROM localizations WHERE site_id = :site_id AND lang_id = :lang_id";
        $stmt = $this->db->prepare($q);
        $stmt->bindValue(':site_id', $site_id);
        $stmt->bindValue(':lang_id', $lang_id);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_OBJ);
        $temp = array();
        foreach($result as $translation) {
            $temp[md5(trim($translation->base))] = $translation->translation;
        }
        return $temp;
    }
}
