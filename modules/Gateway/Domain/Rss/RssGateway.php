<?php
namespace Maksoft\Gateway\Domain;


class RssGateway
{
    protected $db;

    public function __construct(MysqlDb $db)
    {
        $this->db = $db; 
    }

    public function getRssPages()
    {
        $query_str = "
            SELECT pages.*, images.image_src, Sites.Rub
            FROM pages
            LEFT JOIN images on pages.imageNo = images.imageID 
            LEFT JOIN Sites on pages.SiteID = Sites.SitesID 
            WHERE (SELECT MAX(toDate)
            FROM `hostCMS`
            WHERE hostCMS.SiteID = pages.SiteID 
            AND hostCMS.netservice>0)>NOW()
            AND $SiteIDstr  $ppage_str $keywQ 
            AND SecLevel=0
            AND (LENGTH(textStr)>150)
            AND (LENGTH(pages.Name)>12)
            AND (pages.preview>$preview)
            AND date_modified >= DATE_SUB(CURDATE(), INTERVAL $interval DAY)
            ORDER BY date_modified DESC
            LIMIT $limit";
    }
}
