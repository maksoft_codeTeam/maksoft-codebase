<?php
namespace Maksoft\Gateway\Domain\Calendar;
use Maksoft\Core\MysqlDb;


class CalendarGateway 
{
    public function __construct(MysqlDb $db)
    {
        $this->db = $db;
    }

    public function getAll()
    {
        $stmt = $this->db->prepare("SELECT * FROM events");
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getCountryCodes()
    {
        $stmt = $this->db->prepare("SELECT DISTINCT(c_code) FROM events");
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getCountryCodesBy($day, $month) 
    {
        $sql = "SELECT DISTINCT(c_code), mday from events where mon=:month and mday=:day";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":month", $month);
        $stmt->bindValue(":day", $day);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getByMonth($month) 
    {
        $stmt = $this->db->prepare("SELECT * FROM events WHERE mon=:month");
        $stmt->bindValue(":month", $month);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getByEvType($type)
    {
        $stmt = $this->db->prepare("SELECT * FROM events WHERE ev_type=:type");
        $stmt->bindValue(":type", $type);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getFloatingBy()
    {
        $stmt = $this->db->prepare("SELECT * FROM events WHERE ev_type IN (".implode(', ', func_get_args()) . ") and mday=0 and mon=0");
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }
}
