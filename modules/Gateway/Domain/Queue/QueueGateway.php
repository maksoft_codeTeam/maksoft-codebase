<?php
namespace Maksoft\Gateway\Domain\Queue;
use Maksoft\lib\MysqlDb;


class QueueGateway
{
    protected $db;

    public function  __construct($db)
    {
        $this->db = $db;
    }  

    public function create_task(
        $task, $max_retries, $max_execution_time, 
        $minute, $hour, $dom, $month, $dow, $state,
        $execute_once
    )
    {
        $sql = "INSERT INTO `scheduled_tasks` 
            (`task`, 
             `max_retries`, 
             `max_execution_time`, 
             `minute`, 
             `hour`, 
             `dom`, 
             `month`, 
             `dow`, 
             `state`, 
             `last_executed`, 
             `execute_once`, 
             `created_at`, 
             `updated_at`) 
VALUES      (:task, 
             :max_retries, 
             :max_execution_time, 
             :minute, 
             :hour, 
             :dom, 
             :month, 
             :dow, 
             :state, 
             NOW(), 
             :execute_once,
             NOW(), 
             NOW() 
            )
        ";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":task", $task);
        $stmt->bindValue(":max_retries", $max_retries);
        $stmt->bindValue(":max_execution_time", $max_execution_time);
        $stmt->bindValue(":minute", $minute);
        $stmt->bindValue(":hour", $hour);
        $stmt->bindValue(":dom", $dom);
        $stmt->bindValue(":month", $month);
        $stmt->bindValue(":dow", $dow);
        $stmt->bindValue(":state", $state);
        $stmt->bindValue(":execute_once", $execute_once);
        $stmt->execute();
        return $this->db->lastInsertId();
    }
}
