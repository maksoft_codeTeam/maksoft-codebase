<?php
namespace Maksoft\Gateway\Domain\User;
use Maksoft\Core\MysqlDb;


class UserGateway
{
    protected $db;

    public function  __construct(MysqlDb $db)
    {
        $this->db = $db;
    }

    public function getAvatar($id)
    {
        $sql = "SELECT picture
                FROM  users 
                WHERE  ID =:id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function getEmployee($id) {
        $sql = 'SELECT * 
            FROM  `sl` 
            WHERE  `userID` = :id
            ';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getEmployeePermission($id) {
        $sql = 'SELECT perm 
            FROM  `sl` 
            WHERE  `userID` = :id
            ';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getUsersForTasks() {
        $sql = 'SELECT * 
            FROM  `sl` 
            WHERE  `perm` >= 1
            AND FullName != ""
            ';
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getUsersByCompanyId($companyId) {
        $sql = "SELECT * FROM  users 
                WHERE  FirmID = :companyId";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":companyId", $companyId);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }


    public function setImage($user_id, $img_path)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET 
                picture=:img_path
            WHERE
                ID=:user_id
        ");
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":img_path", $img_path);
        $stmt->execute();
    }

    public function updateProfileInfo($id, $name, $email, $phone)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET
                EMail=:email,
                Name=:name,
                Phone=:phone
            WHERE 
                ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":name", $name);
        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":phone", $phone);
        $stmt->execute();
    }

    public function get_uSites($user_id, $filter=1)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM SiteAccess sa
            RIGHT JOIN Sites s on sa.SiteID = s.SitesID
            WHERE sa.userID = :user_id AND :filter
            ORDER by s.Name ASC");
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":filter", $filter);
        $stmt->execute();
        $data = array();
        while($row = $stmt->fetch(\PDO::FETCH_OBJ)){
            $data[] = $row;
        }
        return $data;
    }

    public function editUserName($id, $name)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET Name=:name
            WHERE ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":name", $name);
        $stmt->execute();
    }

    public function editUserEmail($id, $email)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET EMail=:email
            WHERE ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":email", $email);
        $stmt->execute();
    }

    public function editUserPass($id, $pass)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET pass=:pass
            WHERE ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":pass", $pass);
        $stmt->execute();
    }

    public function editUserPhone($id, $phone)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET Phone=:phone
            WHERE ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":phone", $phone);
        $stmt->execute();
    }

    public function editUserPassword($id, $password)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET password=:password
            WHERE ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":password", $password);
        $stmt->execute();
    }

    public function getUsersByParentUserId($id)
    {
        $stmt = $this->db->prepare("
            SELECT * 
            FROM  users u 
            LEFT JOIN SiteAccess s ON s.userID=u.ID
            WHERE  u.ParentUserID =:id
            GROUP BY u.Name
            ");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $tmp = array();
        while($r = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $r;
        }

        return $tmp;
    }

    public function getUsersBySiteId($id)
    {
        $stmt = $this->db->prepare("
            SELECT * 
            FROM  users u 
            LEFT JOIN SiteAccess s ON s.userID=u.ID
            WHERE  s.SiteID =:id
            GROUP BY u.Name
            ");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $tmp = array();
        while($r = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $r;
        }

        return $tmp;
    }

    public function is_registered($username, $email)
    {
        $sql = "SELECT *  FROM users 
                WHERE username=:username
                AND EMail = :email ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":username", $username);
        $stmt->bindValue(":email", $email);
        $stmt->execute();
        $res = $stmt->fetch();
        return boolval($res);
    }

    public function newUser($firm_id, $parent_user, $name,
                            $email, $phone, $username,
                            $pass, $initPage,
                            $siteID, $access_lvl)
    {
        $sql = "
            INSERT INTO users ( 
                FirmID, 
                ParentUserID,
                Name,
                EMail,
                Phone,
                username,
                pass,
                InitPage,
                InitSiteID,
                AccessLevel) 
            VALUES (
                :firm_id,
                :parent_user,
                :name,
                :email,
                :phone,
                :username,
                :pass,
                :initPage,
                :siteID,
                :access_lvl
            )";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":firm_id", $firm_id);
        $stmt->bindValue(":parent_user", $parent_user);
        $stmt->bindValue(":name", $name);
        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":phone", $phone);
        $stmt->bindValue(":username", $username);
        $stmt->bindValue(":initPage", $initPage);
        $stmt->bindValue(":pass", $pass);
        $stmt->bindValue(":siteID", $siteID);
        $stmt->bindValue(":access_lvl", $access_lvl);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function setSiteAccess($SiteID, $init_page, $readLevel, $writeLevel, $user_id, $notifications=False)
    {
        $sql = "INSERT INTO SiteAccess
                    (SiteID, init_page, ReadLevel, WriteLevel, notifications, userID) 
                VALUES 
                    (:SiteID, :init_page, :readLevel, :writeLevel, :notifications, :user_id)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":init_page", $init_page);
        $stmt->bindValue(":readLevel", $readLevel);
        $stmt->bindValue(":writeLevel", $writeLevel);
        $stmt->bindValue(":notifications", (integer) $notifications);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->execute();
        return $this->db->lastInsertId();
    }
}
