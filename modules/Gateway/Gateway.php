<?php
namespace Maksoft\Gateway;


class Gateway
{
    protected $db;

    public function __construct($db){
        $this->db = $db;
    }

    public function beginTransaction()
    {
        $this->db->beginTransaction();
    }

    public function commit()
    {
        $this->db->commit();
    }

    public function rollBack()
    {
        $this->db->rollBack();
    }

    public function page()
    {
        return new Domain\Page\PageGateway($this->db);
    }

    public function site()
    {
        return new Domain\Site\SiteGateway($this->db);
    }

    public function stats()
    {
        return new Domain\Statistic\StatGateway($this->db);
    }

    public function auth()
    {
        return new Domain\Auth\AuthGateway($this->db);
    }

    public function task()
    {
        return new Domain\Task\TaskGateway($this->db);
    }

    public function rss()
    {
        return new Domain\Rss\RssGateway($this->db);
    }

    public function fak()
    {
        return new Domain\Fak\FakGateway($this->db);
    }

    public function user()
    {
        return new Domain\User\UserGateway($this->db);
    }
    public function cart()
    {
        return new Domain\Cart\CartGateway($this->db);
    }
    public function event()
    {
        return new Domain\Calendar\CalendarGateway($this->db);
    }

    public function queue()
    {
        return new Domain\Queue\QueueGateway($this->db);
    }
    public function lang()
    {
        return new Domain\Localization\LocalizationGateway($this->db);
    }
}
