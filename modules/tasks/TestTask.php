<?php
namespace Maksoft\Task;

use Jokuf\Event\Task;


class TestTask extends Task
{
    public function run($x) {
        return $x * $x * $x;
    }
} 
