<?php
namespace Maksoft\Task;

use \Pimple\ServiceProviderInterface;
use \Jokuf\Event\Task;


class SendEmail extends Task 
{
    public function run ($sender , array $recipients, $subject, $content) 
    {
        $container = new \Pimple\Container();
        $container->register(new \Maksoft\Containers\GlobalContainer());

        $email = $container['email'];
        $email->setFrom($sender['email'], $sender['name']);

        foreach($recipients as $recipient) {
            $email->addAddress($recipient['email'], $recipient['name']);     // Add a recipient
        }
        $email->addBcc($sender['email'], $sender['name']);
        $email->addBcc('cc@maksoft.bg', $subject);
        $email->Subject = $subject;
        $email->Body    = $content;
        if(!$email->send()) {
            throw new \Exception($email->ErrorInfo);
        }
        return True;
    }
}
