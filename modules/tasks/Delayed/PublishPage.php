<?php
namespace Maksoft\Task\Delayed;

use \Pimple\ServiceProviderInterface;
use \Jokuf\Event\Task;


class PublishPage extends Task 
{
    public function run ($page_n, $site_n) 
    {
        $container = new \Pimple\Container();
        $container->register(new \Maksoft\Containers\GlobalContainer());
        //Publish page logic
        return True;
    }
}
