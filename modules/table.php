<?php
require_once __DIR__.DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once __DIR__.DIRECTORY_SEPARATOR."settings.php";
$db = new \Maksoft\Core\MysqlDb($env);

$email = new \Maksoft\Admin\Email();
$save_to_forms = new \Maksoft\Admin\FormCMS\Save($db);


if($_SERVER["REQUEST_METHOD"] === "POST"){
    try{
        $contact = new \Maksoft\Admin\FormCMS\MainForm($o_page, $_POST);
        $contact->attach($email);
        $contact->attach($save_to_forms);
        $contact->notify();
    } catch (Exception $e){
        ?>
            <div class="alert alert-danger" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <span class="sr-only">Error:</span>
                  <?php echo $e->getMessage(); ?>
            </div>
        <?php
    }
} else {
    $contact = new \Maksoft\Admin\FormCMS\MainForm($o_page, $_POST);
    $contact->attach($email);
}

echo $contact;
