<?php
namespace Maksoft\Modules\Gateway;
use Maksoft\Modules\Base\Db;


class Request extends Db
{
    public function getHotel()
    {
        $sql = "SELECT *
                FROM `hotels` h
                JOIN hotels_description hd ON h.hotel_id = hd.hotel_id
                JOIN hotels_to_types ht ON ht.hotel_id = h.hotel_id
                JOIN tour_types tt ON ht.tour_type_id = tt.tour_type_id
                WHERE h.hotel_id = :hotel_id
                AND hd.language_id =:language_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->bindValue(":language_id", $this->language);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function getCurrency()
    { 
	$stmt = $this->prepare("SELECT * FROM currencies WHERE currency_order = :language_id");
        $stmt->bindValue(":language_id", $this->language);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getDescription()
    {
        $sql = "SELECT *
                FROM `hotels_description`
                WHERE `hotel_id` =:hotel_id
                AND language_id = :language";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->bindValue(":language", $this->language);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getResort($resort_id)
    {
        $sql = "SELECT *
                FROM resorts r
                LEFT JOIN resorts_description rd ON r.resort_id = rd.resort_id
                WHERE r.resort_id =:resort_id
                AND rd.language_id =:language_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":resort_id", $resort_id);
        $stmt->bindValue(":language_id", $this->language);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getResortDescription($resort_id)
    {
        $sql = "SELECT resort_description
                FROM resorts_description rd
                WHERE rd.resort_id =:resort_id
                AND rd.language_id =:language_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":resort_id", $resort_id);
        $stmt->bindValue(":language_id", $this->language);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getAllRooms()
    {
        $sql = "SELECT *
                FROM rooms r
                WHERE r.hotel_id =:hotel_id
                ORDER BY r.room_id ASC";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->execute();
        $rooms = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $rooms;
    }

    public function getRoom($room_id)
    {
        $sql = "SELECT *
                FROM rooms r
                WHERE r.hotel_id =:hotel_id
                AND r.room_id = :room_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->bindValue(":room_id", $room_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function getClient($client_id){
        $stmt = $this->prepare("SELECT * FROM client 
                                WHERE idclient =:client_id");
        $stmt->bindValue(":client_id", $client_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function getReservations($start_date=false, $end_date=false)
    {
        $sql = "SELECT DISTINCT (
                    rr.reservations_id
                ) as id
                FROM reserve AS r
                JOIN reservations_has_rooms rr ON rr.reserve_id = r.idreserve
                WHERE r.hotel_id =:hotel_id";
        $prep = array();
        if($start_date){
            $start_date = date('Y-m-d', strtotime($start_date));
            $sql .= " AND check_in >= :start_date";
            $prep[":start_date"] = $start_date;
        }

        if($end_date){
            $end_date = date('Y-m-d', strtotime($end_date));
            $sql .= " AND check_out >= :end_date";
            $prep[':end_date'] = $end_date;
        }
        $stmt = $this->prepare($sql);
        foreach($prep as $key=>$value){
            $stmt->bindValue($key, $value);
        }
        $stmt->bindValue(":hotel_id", $this->getId());
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getReservedRoomsByReservationsId($id)
    {
        $stmt = $this->prepare("SELECT * FROM `reserve`
                                LEFT JOIN reservations_has_rooms rr ON rr.reserve_id = reserve.idreserve 
                                WHERE reservations_id =:id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp [] = $res;
        }
        return $tmp;
    }

    public function getReservation($res_id)
    {
        $sql = "SELECT * FROM reservations WHERE id=:res_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":res_id", $res_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function countReservedRooms($reservations_id)
    {
        $stmt = $this->prepare("
                    SELECT * 
                    FROM  `reservations_has_rooms` 
                    WHERE reservations_id =:reservations_id");
        $stmt->bindValue(":reservations_id", $reservations_id);
        $stmt->execute();
        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return count($res);
    }

    public function getReservationInfo($res_id)
    {
        $tmp = array();
        $sql = "SELECT * 
                FROM reservations_has_rooms rr
                LEFT JOIN reserve r ON r.idreserve = rr.reserve_id
                WHERE rr.reservations_id =:res_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":res_id", $res_id);
        $stmt->execute();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getApprovedReservations()
    {
        $sql = "SELECT * FROM reserve WHERE hotel_id=:hotel_id AND status='approved'";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->getId());
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getCanceledReservations()
    {
        $sql = "SELECT * FROM reserve WHERE hotel_id=:hotel_id AND status='canceled'";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->getId());
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function cancelReservation($reserve_id)
    {
        $rooms = $this->getReservedRoomsByReservationsId($reserve_id);
        foreach($rooms as $room){
            $sql = "UPDATE reserve set status='canceled' WHERE idreserve=:reserve_id"; 
            $stmt = $this->prepare($sql);
            $stmt->bindValue(":reserve_id", $room->idreserve);
            $stmt->execute();
        }
    }

    public function approveReservation($reserve_id)
    {
        $rooms = $this->getReservedRoomsByReservationsId($reserve_id);
        foreach($rooms as $room){
            $sql = "UPDATE reserve set status='approved' WHERE idreserve=:reserve_id";
            $stmt = $this->prepare($sql);
            $stmt->bindValue(":reserve_id", $room->idreserve);
            $stmt->execute();
        }
    }

    public function getReservedRoomCount($room_id, $check_in_date)
    {
        $sql = "SELECT COUNT( idreserve ) as amount , idreserve, rooms_room_id
                FROM `reserve`
                WHERE `check_out` >= :check_in
                AND rooms_room_id = :room_id
                AND status != 'canceled'
                GROUP BY rooms_room_id
                ";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":room_id", $room_id);
        $stmt->bindValue(":check_in", $check_in_date);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ); // връща всички заети стаи групирани по тип
        if($res){ return $res;}
        $std = new \stdClass();
        $std->amount = 0;
        return $std;

        return ($res ? $res : 0);
    }

    public function maxAdults()
    {
        $sql = "SELECT SUM( room_max_standart_beds * room_qty) AS max_adults
                FROM `rooms`
                WHERE hotel_id = :hotel_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res->max_adults;  // Връща максимално допустимото количество възрастни,
                                 // които могат да бъдат настанени без допълнителни легла
    }

    public function maxAdultsWithExtraBeds()
    {
        $sql = "SELECT SUM( room_max_standart_beds * room_qty + room_max_extra_beds ) AS max_adults
                FROM `rooms`
                WHERE hotel_id = :hotel_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res->max_adults;  // Връща максимално допустимото количество възрастни,
                                 // които могат да бъдат настанени
    }

    public function findRoom($adult, $children)
    {
        $sql = "SELECT *
                FROM `rooms`
                WHERE `hotel_id` = :hotel_id
                AND room_max_standart_beds + room_max_extra_beds = :max_guests";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->bindValue(":max_guests", $adults+$children);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
        // Това е за единично настаняване би върнало резултат ако имаме 2 ма възрастни и 1 дете,
        // 3 ма възрастни /единият на отделно легло/ или 4 ма възрастни, но ако бройката надвишава
        // максималният капацитет на стаите в хотела ще върне false
        // Намира тип/типовете стаи които отговарят на максималния брой възрастни и деца
    }

    public function getBiggestRoom()
    {
        $sql = "SELECT MAX( room_max_standart_beds + room_max_extra_beds ) AS max_room, room_id
                FROM `rooms`
                WHERE `hotel_id` = :hotel_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res->max_room;
    }

    public function getSmallestRoom()
    {
        $sql = "SELECT MIN( room_max_standart_beds + room_max_extra_beds ) AS min_room, room_id
                FROM `rooms`
                WHERE `hotel_id` = :hotel_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res->min_room;
    }

    public function returnFreeRoomsByType()
    {
        $sql = "SELECT COUNT( rooms_room_id ) , idreserve, rooms_room_id
                FROM `reserve`
                WHERE `check_out` > '2016-07-20'
                AND `hotel_id` =:hotel_id
                GROUP BY rooms_room_id";

    }


    public function returnReservedRooms()
    {
        $sql = "SELECT * FROM `reserve`
                WHERE `check_out` > NOW()
                AND `hotel_id` = :hotel_id
                GROUP BY rooms_room_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->execute();
        $res = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $res;
    }

    public function checkRoomQty($reserved_room_qty, $room_id)
    {
        $sql = "SELECT (
                CAST( room_qty AS SIGNED ) - :reserved_room_qty
                ) AS free_rooms
                FROM `rooms`
                WHERE hotel_id = :hotel_id
                AND room_id = :room_id";
        $stmt = $this->prepare($sql);
        $stmt->bindParam(":reserved_room_qty", $reserved_room_qty, \PDO::PARAM_INT);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->bindValue(":room_id", $room_id, \PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res->free_rooms;
    }

    public function returnRoomTypes()
    {
        $sql = "SELECT *
                FROM `rooms`
                WHERE `hotel_id` = :hotel_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->execute();
        $res = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $res;
    }

    public function saveReservation($msg, $rooms, $timestamp)
    {
        $stmt = $this->prepare("INSERT INTO `reservations`
                                    (`res_str`, `client_id`, `rooms`, `session`, `timestamp`)
                                VALUES
                                    (:msg, :client_id, :rooms, :session_id, :timestamp)"
                            );
        $stmt->bindValue(":msg", $msg);
        $stmt->bindValue(":client_id", $this->client_id);
        $stmt->bindValue(":rooms", $rooms);
        $stmt->bindValue(":session_id", session_id());
        $stmt->bindValue(":timestamp", $timestamp);
        $stmt->execute();
        return $this->lastInsertId();
    }

    public function loadReservation($client_id, $session, $timestamp)
    {
        $stmt = $this->prepare("SELECT * FROM reservations 
                                WHERE client_id = :client_id
                                AND session = :session
                                AND timestamp = :timestamp");
        $stmt->bindValue(":client_id", $client_id);
        $stmt->bindValue(":session", $session);
        $stmt->bindValue(":timestamp", $timestamp);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function saveRelation($reservation_id, $reserve_id)
    {
        $sql = "INSERT INTO  `hotels`.`reservations_has_rooms` (
                    `reservations_id` ,
                    `reserve_id`
                )
                VALUES (
                    :reservation_id,  :reserve_id
                ) ";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":reservation_id", $reservation_id);
        $stmt->bindValue(":reserve_id", $reserve_id);
        $stmt->execute();
        return $this->lastInsertId();
    }

    public function getRoomInfo()
    {
        $tmp = array();
        $st = array();
        foreach($this->rooms as $room){
            $tmp['name'] = $room->getRoom()->room_type_title;
            $tmp['num'] = 1;
            $tmp['description'] = $room->getRoomDescription()->room_description;
            $descr_id = $room->getDescriptionId();
            $tmp['price_per_night'] = $room->getPrice($descr_id)->price_value;
            $st[] = $tmp;
        }
        return $st;
    }

    public function makeReservation($price_id, $room_id, $client_id, $checkin, $checkout, $requirements, $breakfast, $checkin_hour, $hotel_id , $sum){
        $insert = "
            INSERT INTO  `hotels`.`reserve` (
            `rooms_prices_room_price_id` ,
            `rooms_room_id` ,
            `client_idclient` ,
            `check_in` ,
            `check_out` ,
            `requirements` ,
            `breakfast_included` ,
            `checkin_hour` ,
            `sum`,
            `status` ,
            `hotel_id` ,
            `session_id`
            )
            VALUES (
            :price_id, :room_id, 
            :client_id, :check_in,
            :check_out, :requirements,
            :breakfast, :checkin_hour, :sum, 
            'reserved', :hotel_id,
            :session) ";
        $stmt = $this->prepare($insert);
        $stmt->bindValue(":price_id", $price_id);
        $stmt->bindValue(":room_id", $room_id);
        $stmt->bindValue(":client_id", $client_id);
        $stmt->bindValue(":check_in", $checkin);
        $stmt->bindValue(":check_out", $checkout);
        $stmt->bindValue(":requirements", $requirements);
        $stmt->bindValue(":breakfast", $breakfast);
        $stmt->bindValue(":checkin_hour", $checkin_hour);
        $stmt->bindValue(":sum", $sum);
        $stmt->bindValue(":hotel_id", $hotel_id);
        $stmt->bindValue(":session", session_id());
        $stmt->execute();
        return $this->lastInsertId();
    }

    public function addUser()
    {
        $insert = "REPLACE INTO client (first_name, middle_name, last_name, email, phone) 
                   VALUES (:fName, :mName, :lName, :email, :phone)";
        $stmt = $this->prepare($insert);
        $stmt->bindValue(":fName", iconv('utf8', 'cp1251', $this->getFirstName()));
        $stmt->bindValue(":mName", iconv('utf8', 'cp1251', $this->getMiddleName()));
        $stmt->bindValue(":lName", iconv('utf8', 'cp1251', $this->getLastName()));
        $stmt->bindValue(":email", $this->getEmail());
        $stmt->bindValue(":phone", $this->getPhone());
        $email_data = array();
        $stmt->execute();
        return $this->lastInsertId();
    }

    public function getRoomDescription()
    {
        $sql = "SELECT r.*, rdt.room_description_title as title
                FROM `rooms_description` r
                LEFT JOIN rooms_description_titles rdt ON rdt.room_description_id = r.room_description_id
                AND rdt.language_id = :language
                WHERE r.room_id =:id
                LIMIT 1";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":id", $this->room->room_id);
        $stmt->bindValue(":language", $_SESSION['booking']['language_id']);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function setRoom($room_id)
    {
        $stmt = $this->prepare("SELECT * FROM  `rooms` 
                                WHERE  `room_id` = :room_id");

        $stmt->bindValue(":room_id", $room_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }



    public function getDescriptionTitle($descr_id, $language)
    {
        $sql = "SELECT * 
        FROM  `rooms_description_titles` 
        WHERE  `room_description_id` = :descr_id
        AND language_id = :language_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":descr_id", $descr_id);
        $stmt->bindValue(":language_id", $language);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function getPrice($id)
    {
        $stmt = $this->prepare("
            SELECT *
            FROM `rooms_prices`
            WHERE `room_description_id` = :id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function getPriceId()
    {
        $sql = "SELECT * 
                FROM rooms_prices as p
                WHERE p.room_description_id = (SELECT room_description_id  FROM `rooms_description` WHERE `room_id` = :room_id LIMIT 1)";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(':room_id', $this->getId());
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function setPending($client_id=2)
    {
        $price_id = $this->getPrice($this->getRoomDescription()->room_description_id);
        $stmt = $this->prepare(
            "INSERT INTO reserve
             (`rooms_prices_room_price_id`, `rooms_room_id`, `client_idclient`, `check_in`, `check_out`, `status`, `hotel_id`, `session_id`)
             VALUES (:price_id, :room_id, :client, :check_in, :check_out, 'pending', :hotel_id, :session_id)");
        $stmt->bindValue(":price_id", $price_id->room_price_id);
        $stmt->bindValue(":room_id", $this->getId());
        $stmt->bindValue(":client", $client_id);
        $stmt->bindValue(":check_in", $_SESSION['booking']['period']['check_in']);
        $stmt->bindValue(":check_out", $_SESSION['booking']['period']['check_out']);
        $stmt->bindValue(":hotel_id", $this->getHotel());
        $stmt->bindValue(":session_id", session_id());
        return $stmt->execute();
    }


}
