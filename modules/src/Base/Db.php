<?php
namespace Maksoft\Modules\Base;

class Db extends \PDO
{
    public $db;
    protected $enc = 'cp1251';
    private $dsn, $db_user, $db_pass;
    public function __construct($settings=null) {
        $this->db_user = 'maksoft';
        $this->db_pass = 'mak211';
        parent::__construct("mysql:host=" . 'localhost'.
                            ";dbname=" . 'hotels',
                            $this->db_user,
                            $this->db_pass,
                            array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '".$this->enc."'"));
        $this->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
    }

    public function setCharacterEncoding($enc)
    {
        $this->enc = $enc;
    }
}

?>
