<?php
require __DIR__.DIRECTORY_SEPARATOR.'base.php';
require_once realpath (__DIR__.'/../../vendor/').DIRECTORY_SEPARATOR.'autoload.php';

use Webmozart\Assert\Assert;
use Maksoft\Modules\Reservation\HotelReservation;

$data = array();
$form_fields = array('radios', 'booker_title', 'first_name', 'last_name', 'email', 'email_repeat', 'requirements', 'checkin_eta_hour', 'wantbreakfastincluded_0');
try{
    if($_SERVER['REQUEST_METHOD'] === "POST" ){
        $checkin = $_SESSION['booking']['period']['checkin'];
        $checkout = $_SESSION['booking']['period']['checkout'];
        $reservation = new \Maksoft\Modules\Reservation\Reserve();
        $reservation->validateFormCompleteness($form_fields);
        $reservation->validatePOST();
        $reservation->validatePeriod($checkin, $checkout);
        $reservation->validateGuestsNumber();
        $reservation->setFirstName($_POST['first_name']);
        $reservation->setMiddleName($_POST['middle_name']);
        $reservation->setLastName($_POST['last_name']);
        $reservation->setRequirements($_POST['requirements']);
        $reservation->setPhone($_POST['phone']);
        $reservation->setEmail($_POST['email']);
        $reservation->setCheckinHour($_POST['checkin_eta_hour']);
        $reservation->setBreakfast($_POST['wantbreakfastincluded_0']);
        $cost = 0;
        $hotel = new HotelReservation($_SESSION['booking']['hotel_id']);
        $hotel->setLanguage($_SESSION['booking']['language_id']);
        $all_rooms = $hotel->getAllRooms();
        $room_qty = 0;
        $email_data = array();
        $r = array();
        foreach ($_SESSION['booking']['rooms'] as $res_room){
            foreach($all_rooms as $room){
                if($room->room_id == $res_room['room_id']){
                    $res_count =(int) $hotel->getReservedRoomCount($room->room_id, $checkin)->amount;
                    $qty = $room->room_qty - $res_count;
                     if($qty > 0){
                        $room_qty += 1;
                        for ($i=0; $i< $res_room['num']; $i++){
                            $r[] = $res_room['room_id'];
                            $room = new \Maksoft\Modules\Reservation\Room($res_room['room_id']);
                            $room->setCheckInDate($checkin);
                            $room->setCheckOutDate($checkout);
                            $reservation->addRoom($room);
                            $cost += $room->getTotalSum();
                        }
                     } else {
                         unset($_SESSION['booking']['rooms'][$res_room['id']]);
                         throw new Exception ('Закъсняхте, току-що някой резервира стая номер '.$res_room['room_id']. ', моля опитайте отново');
                     }
                }

            }
        }
        $reservation->validateReservedRoomsCount();
        $mail = new PHPMailer(true);
        $mail->SMTPDebug = false;
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->CharSet = 'utf-8';
        $mail->Host = 'server.maksoft.net';  // Specify main and backup SMTP servers
        $mail->Port = 587;                                    // TCP port to connect to
        $mail->Username = 'cc@maksoft.bg';                 // SMTP username
        $mail->Password = 'maksoft@cc';                           // SMTP password
        $mail->Mailer = 'smtp';
        $mail->isHTML(true);
        $mail->setFrom($o_page->_site['EMail'], iconv('cp1251', 'utf8', $hotel->getDescription()->hotel_title));
        $mail->addReplyTo($o_page->_site['EMail'], $o_page->_site['Name']);
        $mail->addAddress(trim($_POST['email']));
        $mail->Subject =  'Успешно резервирахте стая/и в '.iconv('cp1251', 'utf8', $hotel->getHotel()->hotel_title);
        $r_ids = $reservation->reserve();
        require_once realpath(__DIR__.DIRECTORY_SEPARATOR.'../Templates/').DIRECTORY_SEPARATOR.'book_letter_email.php';
        $mail->msgHTML($msg);
        if(!$mail->send()){
            throw new Exception('Съобщението не беше изпратено, моля опитайте пак. '.$mail->ErrorIngo());
        }
        $id = $reservation->saveReservation($msg, implode(',', $r), $timestamp);
        foreach($r_ids as $reservation_id){
            $reservation->saveRelation($id, $reservation_id);
        }
        $_SESSION['booking']['rooms'] = array();
        $_SESSION['booking']['total'] = 0;
        $data = array('success' => $msg, 'header' => 200);
    }else{
        $_SESSION['booking']['rooms'] = array();
        $_SESSION['booking']['total'] = 0;
        throw new Exception('Method not allowed', 405);
    }
} catch (Exception $e){
    $_SESSION['booking']['rooms'] = array();
    $_SESSION['booking']['total'] = 0;
    unset($_SESSION['booking']['period']);
    $data = array('message'=>$e->getMessage(), 'header' => $e->getCode());
}
echo json_encode($data);

