<?php
require __DIR__.DIRECTORY_SEPARATOR.'base.php';
use Webmozart\Assert\Assert;

$_ds = DIRECTORY_SEPARATOR;
require '/hosting/maksoft/maksoft/modules/vendor/autoload.php';

$data = array();
$check_request = array('id', 'room_id', 'num', 'price', 'sum', 'room_type');
try{
    if($_SERVER['REQUEST_METHOD'] === "POST" ){
        if(array_diff($check_request, array_keys($_POST))){
            throw new Exception('ConditionNotMet', 304);
        }
        $id = $_POST['id'];
        $room_id = $_POST['room_id'];
        $num = $_POST['num'];
        $sum = $_POST['sum'];
        $price = $_POST['price'];
        $room_type =$_POST['room_type'];

        if(!isset($_SESSION['booking'])){
            $_SESSION['booking'] = array('rooms' => array());
        }

        $_SESSION['booking']['rooms'][$id] = array('id'=> $id,
                                          'num'=>$num,
                                          'room_id' => $room_id,
                                          'price' => $price,
                                          'sum' => $sum,
                                          'room_type' => $room_type,
                                        );
        $total = 0;
        foreach($_SESSION['booking']['rooms'] as $room){
            $total = $total + ($days * $room['sum']);

        }
        $_SESSION['booking']['total'] = $total;
        $data = $_SESSION['booking']['rooms'];
        $data['total'] = $_SESSION['booking']['total'];
        $data['success'] = 'Вие запазихте стая номер '.$room_id;
    }else{
        throw new Exception('Method not allowed', 405);
    }
} catch (Exception $e){
    $data = array('message'=>$e->getMessage(), 'header' => $e->getCode());
}
echo json_encode($data);

