<?php
require __dir__.DIRECTORY_SEPARATOR.'base.php';

$data = array();
$check_request = array('res_id');
try{
    if($_SERVER['REQUEST_METHOD'] === "POST" ){
        if(array_diff($check_request, array_keys($_POST)) or !isset($_SESSION['booking'])){
            throw new Exception('Невалиден request!', 304);
        }
        $id = $_POST['res_id'];
        $room_id = $_SESSION['booking']['rooms'][$id];
        $date1 = $_SESSION['booking']['period']['checkin'];
        $date2 = $_SESSION['booking']['period']['checkout'];
        $days = \Maksoft\Modules\Reservation\Reserve::days($date1, $date2);
        $_SESSION['booking']['total'] -= $days * $room_id['sum'];
        if($_SESSION['booking']['total'] < 0){
            $_SESSION['booking']['total'] = 0;
        }
        if(count($_SESSION['booking']['rooms']) ==0 or !isset($_SESSION['booking']['rooms'])){
            $_SESSION['booking']['total'] = 0;
        }
        unset($_SESSION['booking']['rooms'][$id]);
        $data = array(
            "success" => "Успешно изтрихте стая номер ".$room_id['room_id'],
            "total" => $_SESSION['booking']['total'],
            "id" => $room_id['room_id'],
            );
    }else{
        throw new Exception('method not allowed', 405);
    }
} catch (exception $e){
    $data = array('error'=>$e->getMessage(), 'header' => $e->getCode());
}
echo json_encode($data);


