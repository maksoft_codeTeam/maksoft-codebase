<?php
session_start();
header('Content-Type: application/json', 'charset=utf8');
require_once '../../../lib/lib_page.php';
require_once '../../vendor/autoload.php';
$o_page = new page();


$d1 = date_create($_SESSION['booking']['period']['checkin']);
$d2 = date_create($_SESSION['booking']['period']['checkout']);
$days = date_diff($d1, $d2);
$days = $days->format("%a");
