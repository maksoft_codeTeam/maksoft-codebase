<?php
require __DIR__.DIRECTORY_SEPARATOR.'base.php';

$data = array();
try{
    if($_SERVER['REQUEST_METHOD'] === "POST" ){
        if(!isset($_SESSION['booking'])){
            throw new Exception('Няма резервации', 405);
        }
        $data = $_SESSION['booking'];
    }else{
        throw new Exception('Method not allowed', 405);
    }
} catch (Exception $e){
    $data = array('message' => $e->getMessage(), 'header' => $e->getCode());
}
echo json_encode($data);



