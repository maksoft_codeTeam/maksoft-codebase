<?php require_once __DIR__.$_ds.'base.php';
?>
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#999;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}
.tg .tg-f2ao{font-size:medium;font-family:"Palatino Linotype", "Book Antiqua", Palatino, serif !important;;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
</style>



<?php
$checkin  = $_SESSION['booking']['period']['checkin'];
$checkout = $_SESSION['booking']['period']['checkout'];
$adults   = $_SESSION['booking']['period']['adults'];
$children = $_SESSION['booking']['period']['children'];
$d1 = date_create($checkin);
$d2 = date_create($checkout);
$days = date_diff($d1, $d2);
$days = $days->format("%a");
?>


<? include "display_module.php";?>



<?php foreach($free_rooms as $room){
    $rd = $room->getRoomDescription($hotel->getLanguage());
    $s = $room->getDescriptionTitle($rd->room_description_id, $hotel->getLanguage());
    $title = $s->room_description_title;
    ?>
    <div class="row">
    <form method="POST" id="<?=$room->getId();?>" accept-charset="utf-8" >
    <div class="col-md-9">
        <?php if($room->getRoom()->page_id != 0){ ?>
            <?php $o_page->print_pContent($room->getRoom()->page_id);?>
            <?php }?>
    </div>
    <div class="col-md-3">
        <input type="hidden" name="room_name" value="<?=$title;?>">
       <p> <?=$title;?> </p>
    <?php
        $standard_beds = $rd->room_standart_beds;
        $extra_beds = $rd->room_extra_beds;
        $max_beds = $standard_beds + $extra_beds;
        $i=0;
        ?>
        <p>
        <span>
        <?php
          while($i < $standard_beds){ ?>
              <i class="fa fa-male" aria-hidden="true"></i>
        <?php
            $i++;
        } if($extra_beds){?>
        </span> + <span>
        <?php
          $i =0;
          while($i < $extra_beds){ ?>
            <i class="fa fa-child" aria-hidden="true"></i>
        <?php
            $i++;
        } }?>
        </span>
        </p>
        <input type="hidden" name="persons" value="<?=printf("{'standart': %d, 'extra': %d", $standart_beds, $extra_beds);?>">
        <p>

              ���� �� �������� ������ ( <?=$days; $nights = ''; ($days == 1 ? $nights = ' �������' : $nights = '�������' ); echo $nights;?> )
              <h2><span class="label label-success">

              <? $price = $room->getPrice($rd->room_description_id)->price_value;
                  echo $days *  $price . ' '. $hotel->getCurrency()->currency_string;
              ?>
              </span></h2>
              <input type="hidden" name="price" value="<?=$price;?>">
        </p>

        <?php
            ?>
        <select name="num_rooms">
        <?php

        for($i=0; $i<$room->getQty(); $i++){
            printf("<option value='{\"type\": %s, \"count\": %s}'>%d %s</option>",$room->getId(), $i+1, $i + 1, ($i==0 ? '����' : '����'));
        }
        ?>
        </select>
        <div>
            <br>
            <button class="b-button b-button_primary js-rt__summary__reserve-button  hp_rt_inputrt__summary__reserve-button-big " type="submit" >

            <span class="b-button__text"> �������� </span>
            </button>
            <p><span> <i class="fa fa-question-circle" aria-hidden="true"></i> </span></p>
            <p>���������� ��� ���������� ������. (�� ���������� ������������ � �� ������������ ����� �� ������� ��-�����.</p>
        </div>
    </div>
  </form>
  </div>
  <hr>
<?php } ?>

  </div>


<?php include 'already_reserved.php';?>

<script>

var $ = jQuery;

jQuery(function($) {
$('.removebtn').live('click', function () {
        var id = $(this).parent().parent().attr('id');
        $(this).parent().parent().remove();
        $.post('modules/src/api/delete_reserve.php', {res_id: id})
            .done(function(data){
                if(data.success){
                    $.notify(data.success, "success");
                    $("#"+data.id).find('button').show();
                } else {
                    $.notify(data.error, "error");
                }

                $("#total_sum").text(data.total);
                });
    });
});
var res2 = [];

jQuery(function($) {
    $("form").each(function(){
        $(this).submit(function(e){
            var form = $(this);
            var time = $.now(); 
            var form_id = $(this).context.id;

            price = form[0].elements.price.value
            rooms = $.parseJSON(form[0].elements.num_rooms.value);
            room_name = form[0].elements.room_name.value
            var room_data = {
                    id: form_id+time,
                    room_id: form_id,
                    num: rooms.count,
                    price: price,
                    sum: price * rooms.count,
                    room_type: room_name
                };
            res2.push(room_data);

            $.post('modules/src/api/reserve_rooms.php', room_data)
                .done(function(data){ 
                    console.log(data);
                    if(data.message){
                        $.notify(data.message, "error");
                    } else {
                        $.notify(data.success, "success");
                    }
                    $("#total_sum").text(data.total);
                });

           var html = '<div class="row" id="'+ form_id + time +'">';
            html += "<div class='col-md-3'>" + room_name + "</div>";
            html += "<div class='col-md-3'><span>" + price + "</span></div>";
            html += "<div class='col-md-2'><span>" + rooms.count + "</span></div>";
            html += "<div class='col-md-2'><span>" + price * rooms.count + "</span></div>";
            html += '<div class="col-md-2">' +
                '<button class="btn btn-danger removebtn" val="123"> ' + 
                    '<i class="fa fa-trash" aria-hidden="true"></i> </button></div>';
            html += '</div>';
            $("#results").append(html);
            $(this).find('button').hide();

            e.preventDefault();
        });});
});

</script>
