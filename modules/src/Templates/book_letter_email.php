<?php
$currency = iconv('cp1251', 'utf8', $hotel->getCurrency()->currency_string);
$timestamp = time();
$sess = session_id();
$address = iconv('cp1251', 'utf8', $o_page->_site['SAddress']);
$msg = "
<br>
<p><span>-----------------------------------------------------------------------</span></p>
<p>Благодарим Ви, че избрахте да прекарате престоя си при нас.
Моля проверете информацията за вашата резервация по-долу: </p>
<br>
<p>Приета резервация в: ".date('d-m-Y H:i', $timestamp)." </p>
 <br>
<p>Уникален номер на резервацията: ".session_id()." </p>
<p>Код за потвърждение: ".time()." </p>
<p>Резервация на името на: {$_POST['first_name']} {$_POST['last_name']}
<p>Телефон: {$_POST['phone']} </p>
<p>Email: {$_POST['email']}</p>
<p>Хотел: ".iconv('cp1251', 'utf8', $hotel->getHotel()->hotel_title)."</p>
<p>Телефон рецепция: {$o_page->_site['SPhone']}</p>   
<p>Адрес на хотела: {$address}</p>
<p>Дата и час на настаняване: {$_SESSION['booking']['period']['checkin']} {$reservation->getCheckinHour()}:00</p>
<p>Нощувки: ".$days." нощувка/и</p>
";

foreach($reservation->getRoomInfo() as $room){
$msg .=  "<hr>
<p>Вид стая: ".iconv('cp1251', 'utf8', $room['name'])." </p>\n

Описание на стаята:".iconv('cp1251', 'utf8', $room['description'])."\n
<p>Включена закуска: ".($_POST['wantbreakfastincluded_0']==0 ? 'НЕ' : 'ДА')."</p>
<p>Цена на вечер: {$room['price_per_night']} {$currency}</p>
<p>Нощувки: ".$days."</p>
<p>Обща сума за избраната стая: ".$room['price_per_night'] * $days." ".$currency."</p><hr>";
}
$msg .= "<br>
<p>Такси за анулиране на резервацията: </p> 
  <p>В деня на настаняване:  50% от крайната сума</p>
  <p>Без предупреждение: 100% от крайната сума</p>
<p>Общо за плащане: ".$cost." {$currency} （с ДДС）</p>
<p>Допълнителни разходи: Не </p>
<hr>
<p>Ако желаете да смените дата и часа на настаняване, имате въпроси относно </p> 
<p>депозит или цени на стаи, моля свържете се с нас на следните телефони: </p>
<p>".iconv('cp1251', 'utf8', $hotel->getHotel()->hotel_title)." </p>
<p>".$o_page->_site['SPhone']."       </p 
<p>".iconv('cp1251', 'utf8', $o_page->_site['SAddress'])."     </p 
<p>Може да проверите вашата резервация по-всяко време на следният линк:</p>
<p><a href=\"http://".$_SERVER['HTTP_HOST']."/modules/reservation.php?c={$reservation->getClientId()}&s={$sess}&t={$timestamp}\">провери</a></p>
<p>Детайлна информация за хотел ".iconv('cp1251', 'utf8', $hotel->getHotel()->hotel_title)." може да прочетете на следният линк:</p>
<p>".iconv('cp1251', 'utf8', $hotel->getHotel()->hotel_title)." </p>
<p><hr></p> 
<p>Относно този и-мейл:</p>
<p>Ние препоръчваме да принтирате този мейл и да го вземете с вас в хотела.</p>
<br>
<p>При проблеми по-време на вашият престой, моля уведомете персонала за бързото им 
разрешаване.</p>
<br>
<p>Ако престоя Ви е харесал, харесайте нашата фейсбук страница и коментирайте в booking.</p>
<br>
<p>[facebook]</p>
<br>[booking]</p>
<p>[trivago]</p>
<p>---------------------------------------------------------------------------------</p>
<p>maksoft.net </p>        
<p>---------------------------------------------------------------------------------</p>
<br>
";
