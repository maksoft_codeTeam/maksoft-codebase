<?php 
require_once 'base.php';
($date1 ? $date1 : $date1 =  date('Y-m-d', time()));
($date2 ? $date2 : $date2 = date('Y-m-d', strtotime($date1 . "+1 days")));
?>


<form class="form-inline" method="POST" >
<div class="row">

    <!-- Form Name -->
    <div class="col s12 m6 l6">
      <label >���� �� �����������:</label>
      <input id="check_in" name="check_in" type="text" placeholder=""  value="<?=$date1;?>" class="col-md-8" required="">
    </div>
    <div class="col s12 m6 l6">
    <!-- Text input-->
      <label>���� �� ���������:</label>
      <input id="check_out" name="check_out" type="text" value="<?=$date2;?>" placeholder="" class="col-md-8" required="">
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
            <select id="persons" name="persons">
              <?php 
                for($i=1; $i<6; $i++){?>
                <?php 
                $selected = '';
                if(isset($_SESSION['booking']['period']['adults']) and $_SESSION['booking']['period']['adults'] == $i){
                    $selected = 'selected';
                }
                ?>
                <option value="<?=$i;?>" <?=$selected;?>><?=$i;?></option>
                <?php } ?>
            </select>
            <label> ���� ��������: </label>
    </div>

    <div class="input-field col s6">
            <select id="children" name="children">
              <?php 
                for($i=0; $i<4; $i++){?>
                <?php 
                $selected = '';
                if(isset($_SESSION['booking']['period']['children']) and $_SESSION['booking']['period']['children'] == $i){
                    $selected = 'selected';
                }
                ?>
                <option value="<?=$i;?>" <?=$selected;?>><?=$i;?></option>
                <?php } ?>
            </select>
            <label>���� ����:</label>
    </div>
</div>
    <!-- Button -->
<div class="row">
    <div class="col s12 m12 l12">
          <label for="submit"></label>
        <input id="submit" name="submit" type="submit" value="<?=$check;?>" class="waves-effect waves-light btn-large">
    </div>
</div>
</div>
</form>
<script>
$ = jQuery;
jQuery( function($) {
Date.prototype.addDays = function(days) {
        this.setDate(this.getDate() + parseInt(days));
            return this;
};
var dateToday = new Date();
      $( "#check_in" ).datepicker(
        {
            showButtonPanel: true,
            dateFormat: 'dd.mm.yy',
	    minDate: dateToday,
        }
      );
      $( "#check_out" ).datepicker(
        {
            showButtonPanel: true,
            dateFormat: 'dd.mm.yy',
	    minDate: dateToday.addDays(1) ,
        }
      );

var currentDate = $( "#check_in" ).datepicker( "getDate" );
$('select').material_select();

});
            
</script>
