
<?php
    foreach($_SESSION['booking']['rooms'] as $reserved){
            $room_type = iconv('utf8', 'cp1251', $reserved['room_type']);
        ?>
            <div class="row" id="<?=$reserved['id'];?>">
                <div class='col-md-3'><?=$room_type;?> </div>
                <div class='col-md-3'><?=$reserved['price'];?></div>
                <div class='col-md-2'><?=$reserved['num'];?> </div>
                <div class='col-md-2'><?=$reserved['sum'];?></div>
                <div class="col-md-2">
                    <button class="btn btn-danger removebtn">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <?php
    }
