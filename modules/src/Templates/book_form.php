<?php
require_once __DIR__.DIRECTORY_SEPARATOR.'base.php';

?>
<style>
input, textarea {
    height: 55%;
    width: 55%;
}
</style>

<form name="reserve" method="POST" class="form-horizontal" action="/modules/src/api/sale_reserve.php">
<fieldset>

<!-- Form Name -->
<legend>�������� ������� ��</legend>

<div class="form-group">
  <label class="col-md-4 control-label" for="radios">��� �� ����������?</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="radios-0">
      <input type="radio" name="radios" id="radios-0" value="1" checked="checked">
      ������
    </label>
    </div>
  <div class="radio">
    <label for="radios-1">
      <input type="radio" name="radios" id="radios-1" value="2">
      �����������
    </label>
    </div>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group com-md-4">
  <label class="col-md-4 control-label" for="booker_title">Select Basic</label>
  <div class="col-md-2">
    <select id="name" name="booker_title" class="form-control">
      <option value="1">�-�</option>
      <option value="2">�-��</option>
      <option value="3">�-��</option>
    </select>
  </div>
</div>

<!-- Text input-->

<div class="form-group com-md-4">
  <label class="col-md-4 control-label" for="first_name">��� (�� ��������)</label>  
  <div class="col-md-6">
  <input id="first_name" name="first_name" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group com-md-4">
  <label class="col-md-4 control-label" for="last_name">������� (�� ��������)</label>  
  <div class="col-md-6">
  <input id="last_name" name="last_name" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">�����</label>  
  <div class="col-md-6">
  <input id="mail" name="email" type="text" placeholder="���������� �� ���������� ������" class="form-control input-md" required="">
  <span class="help-block">�������������� �� ������������ �� ������� �� ���� �����</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email_repeat">���������� ����� ������</label>  
  <div class="col-md-6">
  <input id="mail_repeat" name="email_repeat" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<div class="form-group com-md-4">
  <label class="col-md-4 control-label" for="phone">������� �� ������</label>  
  <div class="col-md-6">
  <input id="phone" name="phone" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>
<?php
if(isset($_SESSION['booking'])){
    require_once __DIR__.$_ds.'list_reserved_rooms.php';
}
?>
<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="extra_requirements">��������� ����������</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="extra_requirements" name="requirements"></textarea>
  </div>
</div>


<div class="form-group">
<select name="checkin_eta_hour" class="col-md-4 form-control">
    <option value="-1">�� ����</option>
    <option value="0">
    00:00 - 01:00
    </option>
    <option value="1">
    01:00 - 02:00
    </option>
    <option value="2">
    02:00 - 03:00
    </option>
    <option value="3">
    03:00 - 04:00
    </option>
    <option value="4">
    04:00 - 05:00
    </option>
    <option value="5">
    05:00 - 06:00
    </option>
    <option value="6">
    06:00 - 07:00
    </option>
    <option value="7">
    07:00 - 08:00
    </option>
    <option value="8">
    08:00 - 09:00
    </option>
    <option value="9">
    09:00 - 10:00
    </option>
    <option value="10">
    10:00 - 11:00
    </option>
    <option value="11">
    11:00 - 12:00
    </option>
    <option value="12" selected>
    12:00 - 13:00
    </option>
    <option value="13">
    13:00 - 14:00
    </option>
    <option value="14">
    14:00 - 15:00
    </option>
    <option value="15">
    15:00 - 16:00
    </option>
    <option value="16">
    16:00 - 17:00
    </option>
    <option value="17">
    17:00 - 18:00
    </option>
    <option value="18">
    18:00 - 19:00
    </option>
    <option value="19">
    19:00 - 20:00
    </option>
    <option value="20">
    20:00 - 21:00
    </option>
    <option value="21">
    21:00 - 22:00
    </option>
    <option value="22">
    22:00 - 23:00
    </option>
    <option value="23">
    23:00 - 00:00
    </option>
    <option value="24">
    00:00 - 01:00
    (������� ���)
    </option>
    <option value="25">
    01:00 - 02:00
    (������� ���)
    </option>
</select>
����� �� ���������� (�������������� �� ���������)
24-������ ��������: ������ �� �����������, ������ ��������� - ������ �� �� ���� ������ ���� 14:00
</div>
<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="wantbreakfastincluded_0">������� �� ��������� �������?</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="wantbreakfastincluded_0-0">
      <input type="radio" name="wantbreakfastincluded_0" id="wantbreakfastincluded_0-0" value="1" checked="checked">
      ��
    </label>
    </div>
  <div class="radio">
    <label for="wantbreakfastincluded_0-1">
      <input type="radio" name="wantbreakfastincluded_0" id="wantbreakfastincluded_0-1" value="0">
      ��
    </label>
    </div>
  </div>
</div>
<!-- Button -->
<div class="form-group">
  <label class="col-md-12 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">����������</button>
  </div>
</div>
</fieldset>
</form>

<script>


jQuery(function($) {
$('.removebtn').live('click', function () {
        var id = $(this).parent().parent().attr('id');
        $(this).parent().parent().remove();
        $.post('modules/src/api/delete_reserve.php', {res_id: id})
            .done(function(data){
                if(data.success){
                    $.notify(data.success, "success");
                } else {
                    $.notify(data.error, "error");
                }
                });
    });


$('form[name=reserve]').submit(function(e){
    e.preventDefault();   
    var form_data = $(this).serialize();
    console.log(form_data);
    var post = $.post('modules/src/api/sale_reserve.php', form_data).done(function(data){
                if(data.success){
                    $('form[name=reserve]').hide();
                    $('form[name=reserve]').parent().append('<div> <p>'+data.success + '</p></div>');
                    $.notify("������� ����������", "success");
                } else {
                    $.notify(data.message, "error");
                }
                });
    console.log(post);
});

});

</script>

