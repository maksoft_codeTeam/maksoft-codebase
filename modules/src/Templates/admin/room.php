<?php
require_once __DIR__.$_ds.'..'.$ds.'base.php';
?>

<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Мениджър стаи</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="room_type_key">Вид стая:</label>
  <div class="col-md-4">
    <select id="room_type_key" name="room_type_key" class="form-control">
      <option value="1">Option one</option>
      <option value="2">Option two</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="room_type_title">Име на стая:</label>  
  <div class="col-md-4">
  <input id="room_type_title" name="room_type_title" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="room_max_standart_beds">Стандартни легла:</label>  
  <div class="col-md-4">
  <input id="room_max_standart_beds" name="room_max_standart_beds" type="text" placeholder="2" class="form-control input-md" required="">
  <span class="help-block">Брой възрастни в една стая за спалня (2 ма)</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="room_max_extra_beds">Допълнителни легла:</label>  
  <div class="col-md-4">
  <input id="room_max_extra_beds" name="room_max_extra_beds" type="text" placeholder="0" class="form-control input-md" required="">
  <span class="help-block">Максимален брой допълнителни легла</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="room_qty">К-во стаи:</label>  
  <div class="col-md-4">
  <input id="room_qty" name="room_qty" type="text" placeholder="1" class="form-control input-md" required="">
  <span class="help-block">Общ брой стаи</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="page_id">Номер на страница:</label>  
  <div class="col-md-4">
  <input id="page_id" name="page_id" type="text" placeholder="" class="form-control input-md">
  <span class="help-block">Ако стаята има страница напишете номера тук</span>  
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="hotel_id">Хотел:</label>
  <div class="col-md-4">
    <select id="hotel_id" name="hotel_id" class="form-control">
      <option value="1">Option one</option>
      <option value="2">Option two</option>
    </select>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton">Single Button</label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Button</button>
  </div>
</div>

</fieldset>
</form>

