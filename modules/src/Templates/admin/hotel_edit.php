<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Редакция на хотел</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="hotel_title">Име на хотел:</label>  
  <div class="col-md-4">
  <input id="hotel_title" name="hotel_title" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="hotel_description">Описание:</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="hotel_description" name="hotel_description">...</textarea>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="hotel_description_extra">Допълнително описание:</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="hotel_description_extra" name="hotel_description_extra">...</textarea>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="hotel_conditions">Условия на настаняване:</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="hotel_conditions" name="hotel_conditions">...</textarea>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="hotel_child_age">Години деца(малолетни)</label>  
  <div class="col-md-4">
  <input id="hotel_child_age" name="hotel_child_age" type="text" placeholder="12" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="hotel_infant_age">Възраст деца:</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="hotel_infant_age-0">
      <input type="radio" name="hotel_infant_age" id="hotel_infant_age-0" value="0" checked="checked">
      0
    </label> 
    <label class="radio-inline" for="hotel_infant_age-1">
      <input type="radio" name="hotel_infant_age" id="hotel_infant_age-1" value="2">
      2
    </label> 
    <label class="radio-inline" for="hotel_infant_age-2">
      <input type="radio" name="hotel_infant_age" id="hotel_infant_age-2" value="3">
      3
    </label> 
    <label class="radio-inline" for="hotel_infant_age-3">
      <input type="radio" name="hotel_infant_age" id="hotel_infant_age-3" value="4">
      4
    </label> 
    <label class="radio-inline" for="hotel_infant_age-4">
      <input type="radio" name="hotel_infant_age" id="hotel_infant_age-4" value="5">
      5
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="hotel_category">Категоризация:</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="hotel_category-0">
      <input type="radio" name="hotel_category" id="hotel_category-0" value="1" checked="checked">
      1
    </label> 
    <label class="radio-inline" for="hotel_category-1">
      <input type="radio" name="hotel_category" id="hotel_category-1" value="2">
      2
    </label> 
    <label class="radio-inline" for="hotel_category-2">
      <input type="radio" name="hotel_category" id="hotel_category-2" value="3">
      3
    </label> 
    <label class="radio-inline" for="hotel_category-3">
      <input type="radio" name="hotel_category" id="hotel_category-3" value="4">
      4
    </label> 
    <label class="radio-inline" for="hotel_category-4">
      <input type="radio" name="hotel_category" id="hotel_category-4" value="5">
      5
    </label>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="resort_id">Курорт:</label>
  <div class="col-md-4">
    <select id="resort_id" name="resort_id" class="form-control">
      <option value="1">Слънчев бряг</option>
      <option value="2">Банско</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="currency_id">Валута:</label>
  <div class="col-md-4">
    <select id="currency_id" name="currency_id" class="form-control">
      <option value="1">Option one</option>
      <option value="2">Option two</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="tour_type_description">Тип на хотела:</label>
  <div class="col-md-4">
    <select id="tour_type_description" name="tour_type_description" class="form-control">
      <option value="1">море</option>
      <option value="2">планина</option>
      <option value="3">спа</option>
      <option value="4">градски</option>
    </select>
  </div>
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="hotel_image">Снимка на хотел:</label>
  <div class="col-md-4">
    <input id="hotel_image" name="hotel_image" class="input-file" type="file">
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="hotel_status">Активен</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="hotel_status-0">
      <input type="radio" name="hotel_status" id="hotel_status-0" value="0" checked="checked">
      Да
    </label> 
    <label class="radio-inline" for="hotel_status-1">
      <input type="radio" name="hotel_status" id="hotel_status-1" value="1">
      Не
    </label>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton">Single Button</label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Button</button>
  </div>
</div>

</fieldset>
</form>

