<?php

namespace Maksoft\Modules\Interface;


interface Reservation
{
    public function reserve();

    public function print();

    public function save();

    public function delete();
}
