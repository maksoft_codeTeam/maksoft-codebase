<?php
namespace Maksoft\Modules\Reservation;
use \Maksoft\Modules\Gateway\Request;
use \Maksoft\Modules\Interfaces\Reservation;
use \Webmozart\Assert\Assert;


class Reserve extends Request implements Reservation
{
    protected $rooms;

    protected $sum;

    protected $data=array();

    protected $client_id;

    function __construct()
    {
        parent::__construct();
        $this->rooms = new \ArrayObject();
    }

    public function getClientId()
    {
        return $this->client_id;
    }

    public static function days($date1, $date2)
    {
        $date1 = date_create($date1);
        $date2 = date_create($date2);
        $diff = date_diff($date1, $date2);
        return $diff->format("%a");
    }

    public function setRequirements($requirements)
    {
        $this->requirements = iconv('utf8', 'cp1251', $requirements);
    }

    public function getRequirements()
    {
        return $this->requirements;
    }

    public function setFirstName($name)
    {
        $this->data['fName'] = $name;
    }

    public function getFirstName()
    {
        return $this->data['fName'];
    }

    public function setMiddleName($mName=null)
    {
        $this->data['mName'] = $mName;
    }

    public function getMiddleName(){
        return $this->data['mName'];
    }

    public function setLastName($lName)
    {
        $this->data['lName'] = $lName;
    }

    public function getLastName()
    {
        return $this->data['lName'];
    }

    public function setPhone($phone)
    {
        $this->data['phone'] = $phone;
    }

    public function getPhone()
    {
        return $this->data['phone'];
    }

    public function setEmail($email)
    {
        $this->data['email'] = $email;
    }

    public function getEmail()
    {
        return $this->data['email'];
    }

    public function getBreakfast()
    {
        return $this->data['breakfast'];
    }

    public function setBreakfast($breakfast_included)
    {
        $this->data['breakfast'] = $breakfast_included;
    }

    public function setCheckinHour($hour)
    {
        $this->data['checkin_hour'] = $hour;
    }

    public function getCheckinHour()
    {
        return $this->data['checkin_hour'];
    }

    public function addRoom(Room $room)
    {
        $this->rooms->append($room);
    }

    public function getReservedRooms()
    {
        return $this->rooms->count();
    }

    public function validateFormCompleteness($form_fields)
    {
        if(array_diff($form_fields, array_keys($_POST))){
            throw new Exception('ConditionNotMet', 304);
        }
    }

    public function validateReservedRoomsCount()
    {
        Assert::greaterThan($this->getReservedRooms(), 0, 'Няма добавени стаи. Моля добавете стаи и опитайте отново');
    }

    public function validatePOST()
    {
        Assert::stringNotEmpty($_POST['booker_title'], 'booker title must not be empty! Stop playing with our form!');
        Assert::stringNotEmpty($_POST['first_name'], 'Полето Име: не може да бъде празно, моля опитайте отново');
        Assert::stringNotEmpty($_POST['last_name'], 'Полето Фамилия: не може да бъде празно, моля опитайте отново');
        Assert::stringNotEmpty($_POST['email'], 'Поле имейл адрес не може да бъде празно, моля опитайте отново');
        $_POST['email'] = $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
            throw new Exception("Невалиден имейл адрес, моля опитайте отново", 304);
        }
        Assert::eq($_POST['email'], $_POST['email_repeat'], 'Имейл адреса не съвпада, моля опитайте отново.');
    }

    public function validateGuestsNumber()
    {
        Assert::integer((int) $_SESSION['booking']['period']['adults'], 'adults field must be an integer. Got %s');
        Assert::greaterThan((int) $_SESSION['booking']['period']['adults'], 0, 'Брой възрастни трябва да бъде полужително число, вие сте въвели %s');
    }

    public function validatePeriod($date1, $date2)
    {
        $checkin = strtotime($date1);
        $checkout = strtotime($date2);
        if(!$checkin or !$checkout){
            throw new Exception('Въвели сте невалидна дата или сте променили формата за въвеждане.');
        }
        if($checkin > $checkout){
            throw new Exception('Датата на напускане не може да бъде преди датата на настаняване. Моля, въведете коректни дати за настаняване и напускане');
        }
    }

    public function reserve()
    {
        $tmp = array();
        $this->client_id = $this->addUser();
        foreach ($this->rooms as $room) {
            $tmp[] = $this->makeReservation(
                $room->getPriceId()->room_price_id,
                $room->getId(),
                $this->client_id,
                $room->getCheckInDate(),
                $room->getCheckOutDate(),
                $this->getRequirements(),
                $this->getBreakfast(),
                $this->getCheckinHour(),
                $_SESSION['booking']['hotel_id'],
                $room->getTotalSum()
            );
        }
        return $tmp;
    }

    public function __toString(){}

    public function save(){}

    public function del_reserve(){}
}

?>
