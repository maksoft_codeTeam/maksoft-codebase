<?php
namespace Maksoft\Modules\Reservation;
use \Maksoft\Modules\Gateway\Request;
/**
 *
 */
class Room extends Request
{
    protected $room, $check_in, $check_out, $adults, $children, $qty;
    protected $_props = array();
    protected $data = array();


    function __construct($room_id, $qty=1)
    {
        parent::__construct();
        $this->room = $this->setRoom($room_id);
        $this->qty = $qty;
    }

    public function getQty()
    {
        return $this->qty;
    }

    public function getId()
    {
        return $this->room->room_id;
    }



    public function __set($name, $value)
    {
        $this->_props[$name] = $value;
    }

    public function __get($name)
    {
        if (isset($this->_props[$name])){
            return $this->_props[$name];
        }
        return false;
    }

    public function getRoom(){
        return $this->room;
    }

    public function setCheckInDate($check_in)
    {
        $this->data['checkin']  = $check_in;
    }

    public function getCheckInDate()
    {
        return $this->data['checkin'];
    }

    public function setCheckOutDate($check_out)
    {
        $this->data['checkout']  = $check_out;
    }

    public function getCheckOutDate()
    {
        return $this->data['checkout'];
    }

    public function setBreaFast($breakfast=0)
    {
        $this->breakfast = $breakfast;
    }

    public function getBreakFast()
    {
        return $this->breakfast;
    }

    public function setCheckinHour($hour='12:00')
    {
        $this->checkinHour = $hour;
    }

    public function getCheckinHour()
    {
        return $this->checkinHour;
    }

    public function getTotalSum()
    {
        $today_price = $this->getPriceId()->price_value;
        $days = $this->getDays();
        return $today_price * $days;
    }

    public function setAdults()
    {
        $this->adults = $adults;
    }

    public function getAdults()
    {
        return $this->rooms->room_max_standart_beds;
    }

    public function setChildren()
    {
        $this->children = $children;
    }

    public function getChildren()
    {
        return $this->room->room_max_extra_beds;
    }

    protected function isReserved()
    {
        throw new \Exception("Error Processing Request", 1);
    }

    protected function checkRequirements()
    {
        throw new \Exception("Error Processing Request", 1);
    }

    public function getFreeRooms()
    {
        return $this->room->room_qty;
    }

    public function getHotel()
    {
        return $this->room->hotel_id;
    }


    public function getDays()
    {
        $date1 = date_create($this->data['checkin']);
        $date2 = date_create($this->data['checkout']);
        $diff = date_diff($date1, $date2);
        return $diff->format("%a");
    }

    public function getTitle()
    {
        return $this->room->room_type_title;
    }

    public function getDescriptionId(){
        return $this->getRoomDescription($_SESSION['booking']['language_id'])->room_description_id;
    }

    public function getDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function __invoke()
    {
        $this->isReserved();
        $this->checkRequirements();
    }

}


?>
