<?php
namespace Maksoft\Modules\Reservation;
use Maksoft\Modules\Gateway\Request;


class HotelReservation extends Request
{
    public $hotel_id;

    public $language=1;

    public function __construct($hotel_id)
    {
        parent::__construct();
        $this->hotel_id = $hotel_id;
    }

    public function setLanguage($language_id)
    {
        $this->language = $language_id;
    }

    public function getId()
    {
        return $this->hotel_id;
    }

    public function getImg()
    {
        return 'web/images/upload/'.$this->getHotel()->hotel_SiteID.'/hotels/'.$this->getHotel()->hotel_image;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function calcRooms($adults, $children)
    {
        if($adults+$children > $this->maxAdultsWithExtraBeds()){
            throw new \Exception(sprintf(
            "Няма достатъчно места! Максималният капацитет на хотела е %s места!", $this->maxAdultsWithExtraBeds()));
        }
    }

    public function suggestion($guests, $rooms_available, $rooms_so_far=array())
    {
        $sol = array();
        if(is_null($rooms_so_far)){
            $rooms_so_far = array();
        }
        if(array_sum($rooms_so_far) == $guests){
            return $rooms_so_far;
        } elseif (array_sum($rooms_so_far) > $guests){
        } elseif (empty($rooms_available)){
        } else {
            $tmp = $rooms_so_far;
            $tmp[] = $rooms_available[0];
            $sol[] = $this->suggestion($guests, $rooms_available, $tmp);
            $sol[] = $this->suggestion($guests, array_slice($rooms_available, 1), $rooms_so_far);
        }
        return $sol;
    }

    /*
    SELECT (
    CAST( r.room_qty AS SIGNED ) - COUNT( rs.rooms_room_id )
    ) AS available, r.room_id
    FROM `reserve` rs
    JOIN rooms r ON r.hotel_id = rs.hotel_id
    WHERE rs.check_out > '2016-07-20'
    AND rs.hotel_id =38
    GROUP BY rs.rooms_room_id
    LIMIT 0 , 30
     * This sounds like a very familiar problem. Before suggesting any algorithms, I will state
     some assumptions: At the time of this room assignment, the room statuses are staying in the constant
      state (i.e. all free rooms will remain free and all occupied rooms are not available for assignment
       and will remain that way) and the guest list is constant (we are asked to find the optimal arrangement
        for our guests given that no new guests will be signing up) Finally, a guest should be defined
         as a set of people (adults, children and infants) who make a single reservation. In other words,
          a family of two adults, one child and one infant is four people, but only one reservation.

    With these disclamers in mind you want to do something like

    Take the first available room. For every reservation, find the reservation that has the
    correct number of people for that room.
    If no reservation is found that fits the room type completely, try finding a
    reservation with one fewer adults (least restrictive)
    If that doesn't work, then try finding a reservation with one fewer children (next most restrictive)
    If that doesn't work, then try finding a reservation with one fewer infants (lease restrictive)
    If that doesn't work, then try finding a reservation with one fewer adults and one fewer
    children then one fewer adults and one fewer infants and one fewer children and one fewer infants
    etc ...

    This would require many passes through the guest list, but for a usual hotel,
    we are only talking about reservations numbering in low hundreds at most,
    so algorithm execution should not be that great of a concern. Hope this helps.
     *
     *
    Reservation
        HotelReservation
        AirlineReservation
    Bookable
        Hotel
        Airline
    Customer

     *
     *
     * */
}

 ?>
