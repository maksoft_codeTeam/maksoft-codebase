<?php
namespace Maksoft\Modules\Reservation\Hotel\lib;
use \Maksoft\Modules\Base\Db;
/**
 *
 */
class Room extends Db
{
    protected $room, $check_in, $check_out, $adults, $children;

    function __construct()
    {
        parent::__construct();
    }

    public function setRoom($room)
    {
        $this->room = $room;
    }

    public function getRoom()
    {
        return $this->room;
    }

    public function setCheckInDate($check_in)
    {
        $this->check_in  = $check_in;
    }

    public function getCheckInDate()
    {
        return $this->check_in;
    }

    public function setCheckOutDate($check_out)
    {
        $this->check_out  = $check_out;
    }

    public function getCheckOutDate()
    {
        return $this->check_out;
    }

    public function setAdults()
    {
        $this->adults = $adults;
    }

    public function getAdults()
    {
        $this->adults = $adults;
    }

    public function setChildren()
    {
        $this->children = $children;
    }

    public function getChildren()
    {
        return $this->children;
    }

    protected function isReserved()
    {
        throw new \Exception("Error Processing Request", 1);
    }

    protected function checkRequirements()
    {
        throw new \Exception("Error Processing Request", 1);
    }

    protected function getDays()
    {

    }

    protected function getPrice()
    {

    }

    public function getDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function __invoke()
    {
        $this->isReserved();
        $this->checkRequirements();
    }
}


?>
