<?php
namespace Maksoft\Modules\Reservation\Hotel\lib;
use Maksoft\Modules\Base\Db;


class Hotel extends Db
{
    protected $hotel_id;

    protected $language=1;

    public function __construct($hotel_id)
    {
        parent::__construct();
        $this->hotel_id = $hotel_id;
    }

    public function setLanguage($language_id)
    {
        $thus->language = $language_id;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function getHotel()
    {
        $sql = "SELECT *
                FROM `hotels` h
                JOIN hotels_description hd ON h.hotel_id = hd.hotel_id
                JOIN hotels_to_types ht ON ht.hotel_id = h.hotel_id
                JOIN tour_types tt ON ht.tour_type_id = tt.tour_type_id
                WHERE h.hotel_id = :hotel_id
                AND hd.language_id =:language_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->bindValue(":language_id", $this->language);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function getResort($resort_id)
    {
        $sql = "SELECT *
                FROM resorts r
                LEFT JOIN resorts_description rd ON r.resort_id = rd.resort_id
                WHERE r.resort_id =:resort_id
                AND rd.language_id =:language_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":resort_id", $resort_id);
        $stmt->bindValue(":language_id", $this->language);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getResortDescription($resort_id)
    {
        $sql = "SELECT resort_description
                FROM resorts_description rd
                WHERE rd.resort_id =:resort_id
                AND rd.language_id =:language_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":resort_id", $resort_id);
        $stmt->bindValue(":language_id", $this->language);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getAllRooms()
    {
        $sql = "SELECT *
                FROM rooms r
                WHERE r.hotel_id =:hotel_id
                ORDER BY r.room_id ASC";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->execute();
        $rooms = array();
        while($rooms[] = $stmt->fetch(\PDO::FETCH_OBJ)){};
        return $rooms;
    }

    protected function getRoom($room_id)
    {
        $sql = "SELECT *
                FROM rooms r
                WHERE r.hotel_id =:hotel_id
                AND r.room_id = :room_id";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->bindValue(":room_id", $room_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res;
    }

    public function getHotelImages()
    {
        // Добра идея е да изнеса всички снимки в отделна таблица която да е вързана
        // many:1 images -> hotels
        // many:1 images -> rooms_room_id
        // many:1 images -> resorts

    }

    public function getFreeRooms($from, $to)
    {
        $sql = "SELECT COUNT( idreserve ) , rooms_room_id
                FROM `reserve`
                WHERE `check_out` >= :to_date
                GROUP BY rooms_room_id
                ";
        $stmt = $this->prepare($sql);
        $stmt->bindValue(":to_date", $to);
        $stmt->execute();
        $res = $stmt->fetchAll(\PDO::FETCH_OBJ); // връща всички заети стаи групирани по тип
        return $res;
    }

    public function maxAdults()
    {
        $sql = "SELECT SUM( room_max_standart_beds * room_qty) AS max_adults
                FROM `rooms`
                WHERE hotel_id = :hotel_id";
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res->max_adults;  // Връща максимално допустимото количество възрастни,
                                 // които могат да бъдат настанени без допълнителни легла
    }

    public function maxAdultsWithExtraBeds()
    {
        $sql = "SELECT SUM( room_max_standart_beds * room_qty + room_max_extra_beds ) AS max_adults
                FROM `rooms`
                WHERE hotel_id = :hotel_id";
        $stmt->bindValue(":hotel_id", $this->hotel_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_OBJ);
        return $res->max_adults;  // Връща максимално допустимото количество възрастни,
                                 // които могат да бъдат настанени
    }

    public function findRoom($adult, $children)
    {
        $sql = "SELECT *
                FROM `rooms`
                WHERE `hotel_id` =38
                AND room_max_standart_beds + room_max_extra_beds = 3";
        // Това е за единично настаняване би върнало резултат ако имаме 2 ма възрастни и 1 дете,
        // 3 ма възрастни /единият на отделно легло/ или 4 ма възрастни, но ако бройката надвишава
        // максималният капацитет на стаите в хотела ще върне false
        // Намира тип/типовете стаи които отговарят на максималния брой възрастни и деца
    }

    public function getBiggestRoom()
    {
        $sql = "SELECT MAX( room_max_standart_beds + room_max_extra_beds ) AS max_beds, room_id
                FROM `rooms`
                WHERE `hotel_id` =38";
    }

    public function getSmallestRoom()
    {
        $sql = "SELECT MIN( room_max_standart_beds + room_max_extra_beds ) AS max_beds, room_id
                FROM `rooms`
                WHERE `hotel_id` =38";
    }

    protected function returnFreeRoomsByType()
    {
        $sql = "SELECT COUNT( rooms_room_id ) , idreserve, rooms_room_id
                FROM `reserve`
                WHERE `check_out` > '2016-07-20'
                AND `hotel_id` =38
                GROUP BY idreserve
                LIMIT 0 , 30";
    }


    protected function returnReservedRooms($check_out)
    {

    }

    protected function calcRooms($adults, $children)
    {
        if($adults+$children > $this->maxAdultsWithExtraBeds()){
            throw new \Exception(sprintf(
            "Няма достатъчно места! Максималният капацитет на хотела е %s места!", $this->maxAdultsWithExtraBeds()));
        }
    }

    public function search($check_in, $check_out, $adult, $children)
    {
        // Намираме стаите според параметрите $adults

        // $rooms взимам всички възрасни + децата -

        // adults + children = 6
        // търся стая за 6 човека

        // За всяка стая търся дали има не резервирана

        // Връщам array със свободните стаи като обекти
    }

    /*
     * This sounds like a very familiar problem. Before suggesting any algorithms, I will state
     some assumptions: At the time of this room assignment, the room statuses are staying in the constant state (i.e. all free rooms will remain free and all occupied rooms are not available for assignment and will remain that way) and the guest list is constant (we are asked to find the optimal arrangement for our guests given that no new guests will be signing up) Finally, a guest should be defined as a set of people (adults, children and infants) who make a single reservation. In other words, a family of two adults, one child and one infant is four people, but only one reservation.

    With these disclamers in mind you want to do something like

    Take the first available room. For every reservation, find the reservation that has the
    correct number of people for that room.
    If no reservation is found that fits the room type completely, try finding a
    reservation with one fewer adults (least restrictive)
    If that doesn't work, then try finding a reservation with one fewer children (next most restrictive)
    If that doesn't work, then try finding a reservation with one fewer infants (lease restrictive)
    If that doesn't work, then try finding a reservation with one fewer adults and one fewer
    children then one fewer adults and one fewer infants and one fewer children and one fewer infants
    etc ...

    This would require many passes through the guest list, but for a usual hotel,
    we are only talking about reservations numbering in low hundreds at most,
    so algorithm execution should not be that great of a concern. Hope this helps.
     *
     *
    Reservation
        HotelReservation
        AirlineReservation
    Bookable
        Hotel
        Airline
    Customer

     *
     *
     * */
}

 ?>
