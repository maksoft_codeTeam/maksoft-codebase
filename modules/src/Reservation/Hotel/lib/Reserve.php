<?php
namespace Maksoft\Modules\Reservation\Hotel\lib;
use \Maksoft\Modules\Base\Db;
use \Maksoft\Modules\Reservation\Interface\Reservation;
/**
 *
 */
class Reserve extends Db implements Reservation
{
    protected $rooms;

    protected $sum;

    function __construct()
    {
        parent::__construct();
        $this->rooms = new \ArrayObject();
    }

    public function addRoom(Room $room)
    {
        $this->rooms->append($room);
    }

    public function getReservedRooms()
    {
        return $this->rooms->count();
    }

    public function reserve()
    {
        foreach ($this->rooms as $room) {
            try {
                $room();
            } catch (\Exception $e) {

            }
        }
    }


    public function print(){};

    public function save(){};

    public function delete(){};
}

?>
