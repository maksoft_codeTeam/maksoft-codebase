<?php

namespace Maksoft\Modules\Reservation\Interface;


interface Reservation
{
    public function reserve();

    public function print();

    public function save();

    public function delete();
}
