<?php
namespace Maksoft\Modules\lib;
use rcastera\Browser\Session\Session as RcasteraSession;


class Session extends RcasteraSession
{
    protected $db;
    protected $two_weeks=1209600;

    public function __construct()
    {
        session_set_save_handler(
          array($this, "__open"),
          array($this, "__close"),
          array($this, "__read"),
          array($this, "__write"),
          array($this, "__destroy"),
          array($this, "__clean")
        );
        // The following prevents unexpected effects when using objects as save handlers.
        register_shutdown_function('session_write_close');
        session_start();
    }

    public function __open()
    {
        $dsn ='mysql:dbname=maksoft;host=localhost';
        $db_user = 'maksoft';
        $db_pass = 'mak211';
        $this->db = new \PDO($dsn, $db_user, $db_pass);
        $this->db->exec("set names cp1251");
    }

    public function __close()
    {
        $this->db = null;
    }

    public function __read($id)
    {
        $stmt =  $this->db->prepare("SELECT * FROM session WHERE id=:id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $res['data'];
    }

    public function __write($id, $data)
    {
        $stmt = $this->db->prepare("REPLACE INTO session VALUES  (:id, :name, :modified, :lifetime, :data)");
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":name", $_SERVER['HTTP_HOST']);
        $stmt->bindValue(":modified", time());
        $stmt->bindValue(":lifetime", 6000);
        $stmt->bindValue(":data", $data);
        $stmt->execute();
    }

    public function __clean($max)
    {
        $old = time() - $max;
        $stmt = $this->db->prepare("DELETE FROM session WHERE modified < :old");
        $stmt->bindValue(":old", $old);
        $stmt->execute();
    }

    public function __destroy($id)
    {
        $stmt = $this->db->prepare("DELETE FROM session WHERE id=:id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $this->db->query("use hotels");
        $stmt = $this->db->prepare("DELETE FROM reserve WHERE status = 'pending' AND session_id=:ses");
        $stmt->bindValue(":ses", session_id());
        $stmt->execute();
    }

    public function getOnline()
    {
        $stmt = $this->db->prepare("
            SELECT * 
            FROM  `session` 
            WHERE modified + lifetime < NOW( )");
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function __destruct()
    {
        session_write_close();
    }
}
