<script  src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/amstock.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<!--
<style>
#chartdiv {
  width: 100%;
  height: 500px;
}
</style>
 -->
    <div id="key_stat" style="display:none;">

    </div>
<table  class="table">
    <thead>
        <tr>

        </tr>

    </thead>
    <thead>
        <tr>
            <th align="center">#</th>
            <th><span style="font-weight:bold;">ID</span></th>
            <th><span style="font-weight:bold;">����</span></th>
            <th><span style="font-weight:bold;">���� �� �������:</span></th>
            <th><span style="font-weight:bold;">���� �� �������:</span></th>
            <th><span style="font-weight:bold;">�������</span></th>
            <th><span style="font-weight:bold;">���� ������</span></th>
            <th colspan="2" align="center">��������</th>
        <tr>
    </thead> 
</table>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>


<script>
jQuery(function ($) {
    function extractDomain(url) {
        var domain;
        //find & remove protocol (http, ftp, etc.) and get domain
        if (url.indexOf("://") > -1) {
            domain = url.split('/')[2];
        }
        else {
            domain = url.split('/')[0];
        }

        //find & remove port number
        domain = domain.split(':')[0];

        return domain;
    }
    var url = "http://maksoft.net/api/?command=seo_etiketi&n=1&egID=270&date_expire=2016-09-09";
    $.get(url, function(data){ 
        $.each(data, function(i, keyword){
        i += 1;
        row = "<tr id=\""+keyword.eID+"\">";
        row += "<td>"+i+"</td>";
        row += "<td>"+keyword.eID+"</td>";
        row += "<td>"+keyword.Name+"</td>";
        row += "<td>"+keyword.date_start+"</td>";
        row += "<td>"+keyword.date_expire+"</td>";
        row += "<td>������� "+i+"</td>";

        var link_status = '';
        var domain = extractDomain(keyword.url);
        if(keyword.exact_url==1){
            link_status = '<span style="color:green;">' + domain + "</span>";
        } else {
            link_status = '<span style="color:red;">' + domain + "</span>";
        }
        row += "<td>"+link_status+"</td>";
        row += "<td><span><a id=\""+keyword.Name+"\" name=\"edit\" href=\"#\"> �����1 </a><a href=\"#\"> �����1 </a></span></td>";
        row += "</row>";
        $(row).hide().appendTo($("thead").parent()).delay(100*i).fadeIn(1500).show('normal');
      }); 

    $("a[name=edit]").each(function(){
        $(this).on('click', function(e){
           e.preventDefault();
            var canvas = '<canvas id="myChart" width="100%" height="35%"></canvas>';
            $("#key_stat").html(canvas);
            $("#key_stat").show();
            var tr = $(this).parent().parent().parent();
            var keyword = $(this).attr('id');
            etiketi_pos(tr.attr('id'), keyword);
            //generateChartData(tr.attr('id'), keyword);
        });
         
    });
     });

    function etiketi_pos(eID, name){
        var name = name;
        var url = "http://maksoft.net/api/?command=etiketi_pos&n=1&eID="+eID+"&limit=20";
        $.get(url, function(etiketi){
            var data = {
                labels: [],
                datasets: [
                    {
                        label: "������� � ����������� ���������",
                        lineTension: 0.15,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [],
                    }
                ]
            };

            $.each(etiketi, function(i, keyword){
                date_checked = keyword.date_checked.split(' ');
                data.labels.push(date_checked[0]);
                console.log(keyword);
                var g_pos = keyword.g_pos;
                if(g_pos == 0){
                    g_pos = 1;
                }

                data.datasets[0].data.push(g_pos);
            });
            var ctx = $("#myChart");
            var myBarChart = new Chart(ctx, {
                type: 'line',
                data: data,
                options: {
                    title: {
                        display: true,
                        text: 'Custom Chart Title'
                    },
                    hover: {
                        mode: 'label'
                    },
                    responsive: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                min: 0,
                                stepSize: 1,
                                reverse: false
                            }
                        }],
                    },
                }
            });
        });

    }

    function generateChartData(eID, name) {

        var url = "http://maksoft.net/api/?command=etiketi_pos&n=1&eID="+eID+"&limit=120";

        $.get(url, function(etiketi){
            var chartData1 = [];
            $.each(etiketi, function(i, keyword){
                console.log(keyword.date_checked);

                var date_checked = keyword.date_checked.split(' ');
                date_checked=date_checked[0] + ', ' + date_checked[1];
                //date_checked = date_checked.split('-');

                chartData1.push( {
                  "date": date_checked,
                  "value": keyword.g_pos,
                  "volume": keyword.google_result
                } );
            });
            var chart = AmCharts.makeChart("chartdiv", {
              "type": "stock",
              "categoryAxis": {
                      "dateFormats": [{period: "D", format: "YYYY-MM-DD, JJ:NN:SS"}]
                        },
              "theme": "light",
              "parseDates": true,
              "dataSets": [ {
                  "title": name,
                  "fieldMappings": [ {
                    "fromField": "value",
                    "toField": "value"
                  }, {
                    "fromField": "volume",
                    "toField": "volume"
                  } ],
                  "dataProvider": chartData1,
                  "categoryField": "date"
                } 
              ],
            "panels": [ {
                "showCategoryAxis": false,
                "title": "Value",
                "percentHeight": 70,
                "stockGraphs": [ {
                  "id": "g1",
                  "valueField": "value",
                  "comparable": true,
                  "compareField": "value",
                  "balloonText": "[[title]]:<b>[[value]]</b>",
                  "compareGraphBalloonText": "[[title]]:<b>[[value]]</b>"
                } ],
                "stockLegend": {
                  "periodValueTextComparing": "[[percents.value.close]]%",
                  "periodValueTextRegular": "[[value.close]]"
                }
              }, {
                "title": "Volume",
                "percentHeight": 30,
                "stockGraphs": [ {
                  "valueField": "volume",
                  "type": "column",
                  "showBalloon": true,
                  "fillAlphas": 1
                } ],
                "stockLegend": {
                  "periodValueTextRegular": "[[value.close]]"
                }
              } ],

              "chartScrollbarSettings": {
                "graph": "g1"
              },

              "chartCursorSettings": {
                "valueBalloonsEnabled": true,
                "fullWidth": true,
                "cursorAlpha": 0.1,
                "valueLineBalloonEnabled": true,
                "valueLineEnabled": true,
                "valueLineAlpha": 0.5
              },

            } );

            
        });
    }
});
    


</script>
