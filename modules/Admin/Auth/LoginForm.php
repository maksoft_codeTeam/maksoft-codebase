<?php
namespace Maksoft\Admin\Auth;

use \Maksoft\Form\Fields\PasswordField;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\SubmitButton;


class LoginForm extends \Maksoft\Form\Forms\DivForm
{
    private $_gate;

    public function __construct($form_data=null)
    {
        $class = "form-control";
        $this->username = new TextField(
            array(
                "label"=> "������������� ���:",
                "class" => $class,
                "required" => True
            )
        );

        $this->username->add_validators(new \Maksoft\Form\Validators\MinLength(4));
        $this->username->add_validators(new \Maksoft\Form\Validators\MaxLength(20));
        $this->username->add_validators(new \Maksoft\Form\Validators\NotEmpty());

        $this->password = new PasswordField(
            array(
                "label" => "������:",
                "class" => $class,
                "required" => True
            )
        );

        $this->password->add_validators(new \Maksoft\Form\Validators\MinLength(4));
        $this->password->add_validators(new \Maksoft\Form\Validators\NotEmpty());

        $this->submit = new SubmitButton(array("class"=>"btn btn-primary", "style"=>"margin-top:1.5%", "value"=>"����"));
        parent::__construct($form_data);
    }
}
