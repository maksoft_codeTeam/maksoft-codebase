<?php
namespace Maksoft\Admin\Auth;
use \Webmozart\Assert\Assert;
use \Maksoft\Core\Gateway\Gateway;


class Login implements \SplSubject 
{
    const UNKNOWN_USER = 1;
    const INCORRECT_PWD = 2;
    const ALREADY_LOGGED_IN = 3;
    const ALLOW = 4;
    const NOT_LOGGED = 5;
 
    private $status = array();
    private $storage;
    protected $auth;
    private $user_data;
 
    public function __construct($auth)
    {
        $this->gate = $auth;
        $this->storage = new \SplObjectStorage();
    }
 
    public function init($username, $password, $ip)
    {
        /*
         * First validate username and password
         * */

        try{
            $this->isLogged();
            $this->usernameValidator($username);
            $this->passwordValidator($password);
            $user = $this->gate
                ->auth()
                ->getLogin($username, $password, $this->gate->site()->getData()->SitesID);
            if(!$user){
                throw new \Maksoft\Errors\MaksoftCmsException(
                    'Грешно потребителско име или парола! Моля, опитайте отново.', 1);
            }
            $status = 4;
            $msg = 'successfully logged';
        } catch (\InvalidArgumentException  $e){
           $status = $e->getCode();
           $msg = $e->getMessage();
        } catch (\Maksoft\Errors\MaksoftCmsException $e ){
            $status = $e->getCode();
            $msg = $e->getMessage();
        } catch (\Maksoft\Errors\MaksoftCmsLoginException $e){
            $status = $e->getCode();
            $msg = $e->getMessage();
        }      
        $this->setStatus($status, $username, $ip, $msg, $user);
        // Notify all the observers of a change
        $this->notify();
        if ($this->status[0] == self::ALLOW){
            $_SESSION["user"] = $user;
            return true;
        }
        return false;
    }

    protected function usernameValidator($username)
    {
        try{
            Assert::stringNotEmpty($username, 'Потребителското име не може да бъде празно.');
            Assert::minLength($username, 3, 'Потребителското име не може да бъде по-кратко от 3 символа.');
            Assert::maxLength($username, 20, 'Максималната дължина на потребителското име може да бъде 20 символа');
        } catch (\InvalidArgumentException $e){
            throw new \InvalidArgumentException($e->getMessage(), 1);
        }
        return $username;
    }

    protected function passwordValidator($password)
    {
        try{
            Assert::stringNotEmpty($password, 'Невалидна парола');
            Assert::minLength($password, 3, 'Паролата трябва да бъде с дължина поне 3 символа');
            Assert::maxLength($password, 15, 'Максималната дължина не трябва да надвишава 15 символа');
         } catch (\InvalidArgumentException $e){
            throw new \InvalidArgumentException($e->getMessage(), 2);
         }
        return $password;       
    }

    public function isLogged()
    {
        if(isset($_SESSION["user"]) && is_object($_SESSION["user"]) && isset($_SESSION['user']->ID) && isset($_SESSION['user']->ParentUserID)){
            throw new \Maksoft\Errors\MaksoftCmsLoginException("Already logged", self::ALREADY_LOGGED_IN);
        }
        return false;
    }

    private function setStatus( $status, $username, $ip, $msg, $user)
    {
        $this->status = array( $status, $username, $ip , $msg, $user);
    }
 
    public function getStatus()
    {
        return $this->status;
    }

    public static function isBlocked($ip)
    {
        $event = "Maksoft\Admin\Auth\Login"; 
    }
 
    public function attach( \SplObserver $observer )
    {
        $this->storage->attach( $observer );
    }
 
    public function detach( \SplObserver $observer )
    {
        $this->storage->detach( $observer );
    }
 
    public function notify()
    {
        foreach ( $this->storage as $observer ) {
            $observer->update( $this );
        }
    }
}
?>
