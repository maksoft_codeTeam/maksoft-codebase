<?php
namespace Maksoft\Admin;


class Email implements \Maksoft\Interfaces\FormObserver
{
    protected $client;

    public function __construct()
    {
        $this->client = new \PHPMailer(true);
        $this->client->SMTPDebug = false;
        $this->client->SMTPAuth = true;                               // Enable SMTP authentication
        $this->client->CharSet = 'utf-8';
        $this->client->Host = 'server.maksoft.net';  // Specify main and backup SMTP servers
        $this->client->Port = 587;                                    // TCP port to connect to
        $this->client->Username = 'cc@maksoft.bg';                 // SMTP username
        $this->client->Password = 'maksoft@cc';                           // SMTP password
        $this->client->Mailer = 'smtp';
        $this->client->isHTML(true);
    }

    public function update(\Maksoft\Interfaces\FormSubject $form)
    {
        if (is_object($form)){
            $this->client->setFrom($form->fromEmail());
            $this->client->addReplyTo($form->fromEmail(), $form->name->value);
            $this->client->addAddress($form->toEmail());
            $this->client->Subject = $form->subject();
            $this->client->msgHTML($form->content());
            if(!$this->client->send()){
                throw new Exception('Съобщението не беше изпратено, моля опитайте пак. '.$client->ErrorIngo());
            }
            return;
        }
        throw new \Exception("form data cant be null!");
    }
}
