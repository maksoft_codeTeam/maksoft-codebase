<?php
namespace Maksoft\Admin\FormCMS;
use Maksoft\Form\Fields\FileInputField;
use Maksoft\Form\Fields\TextAreaField;
use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Exceptions\ValidationError;
use Maksoft\Form\Forms\DivForm;
use Maksoft\Core\MysqlDB;


class PurchaseOrder extends DivForm
{
    protected $db;

    protected $page;

    public function __construct(\page $o_page, MysqlDB $db, $form_data=null){
        $this->page = $o_page;
        $this->db = $db;
        $this->name = new TextField(array(
            "name"     => "name",
            "size"     => 35,
            "id"       => "name",
            "class"    => "form-control",
            "label"    => "��� / �������:",
            "required" => True ));

        $this->email = new TextField(array(
            "name"     => "email",
            "id"       => "email",
            "label"    => "E-mail:*",
            "class"    => "form-control",
            "required" => True ));

        $this->phone = new TextField(array(
            "name"     => "phone",
            "id"       => "phone",
            "label"    => "������� �� ������:*",
            "class"    => "form-control",
            "required" => True ));

        $this->firma = new Textfield(array(
            "name"     => "company",
            "id"       => "company",
            "label"    => "�����:",
            "class"    => "form-control"));

        $this->bulstat = new Textfield(array(
            "name"     => "vat",
            "id"       => "vat",
            "label"    => "�������:",
            "class"    => "form-control"));

        $this->address = new TextAreaField(array(
            "name"     => "address",
            "id"       => "address",
            "label"    => "����� �� ��������:",
            "class"    => "form-control",
            "required" => True ));


        $this->comment = new TextAreaField(array(
            "name"     => "comment",
            "id"       => "comment",
            "label"    => "���������:",
            "class"    => "form-control"));

        $this->attachment = new FileInputField(array(
            "name"     => "zip",
            "id"       => "zip",
            "class"    => "form-control",
            "label"    => "������� ����"));

        $this->submit = new SubmitButton(array("class"=>"btn btn-default"));

        parent::__construct($form_data);
    }

    protected function save()
    {
        $stmt = $this->db->prepare("
            INSERT INTO forms
             ( ftype, Name, EMail, n, TextForm, SiteID, Zapitvane, ip, Data)
            VALUES
             (:ftype, :Name, :EMail, :n, :TextForm, :SiteID,:Zapitvane, :ip, :Data)");
        $stmt->bindValue(":ftype", $ftype);
        $stmt->bindValue(":Name", $Name);
        $stmt->bindValue(":EMail", $EMail);
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":TextForm", $TextForm);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":Zapitvane", $Zapitvane);
        $stmt->bindValue(":ip", $o_site->user_ip);
        $stmt->bindValue(":Data", $today);
        $stmt->execute();
        return $this->db;
    }

    public function sendMail()
    {

    }
}
