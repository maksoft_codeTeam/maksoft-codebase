<?php
namespace Maksoft\Admin\FormCMS;
use Maksoft\Core\MysqlDb;


class Save implements \Maksoft\Interfaces\FormObserver
{
    protected $_db;

    public function __construct(MysqlDb $db)
    {
        $this->_db = $db;
    }

    public function update(\Maksoft\Interfaces\FormSubject $form)
    {
        $stmt = $this->_db->prepare("
            INSERT INTO forms
             (Name, EMail, n, TextForm, SiteID, Zapitvane, ip, Data)
            VALUES
             (:Name, :EMail, :n, :TextForm, :SiteID,:Zapitvane, :ip, :Data)"
        );
        $stmt->bindValue(":Name", $form->name->value);
        $stmt->bindValue(":EMail", $form->email->value);
        $stmt->bindValue(":n", $form->page()->n);
        $stmt->bindValue(":TextForm", $form->content());
        $stmt->bindValue(":SiteID", $form->site()->SitesID);
        $stmt->bindValue(":Zapitvane", $form->subject());
        $stmt->bindValue(":ip", str_replace(".", "", self::client_ip()));
        $stmt->bindValue(":Data", date("Y-m-d H:i:s"));
        $stmt->execute();
        $this->_id = $this->_db->lastInsertId();
        return $this->_id;
    }

    public static function client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}
