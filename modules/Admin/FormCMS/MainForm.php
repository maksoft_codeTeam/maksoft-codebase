<?php
namespace Maksoft\Admin\FormCMS;
use Maksoft\Form\Fields\FileInputField;
use Maksoft\Form\Fields\TextAreaField;
use Maksoft\Form\Fields\EmailField;
use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Exceptions\ValidationError;
use Maksoft\Form\Forms\DivForm;


class MainForm extends DivForm implements \Maksoft\Interfaces\FormSubject
{

    public $_id;

    protected $_page;

    protected $_storage;

    public function __construct(\page $o_page, $form_data=null)
    {
        $this->_page = $o_page;
        $not_empty = new \Maksoft\Form\Validators\NotEmpty();
        $this->name = new TextField(array(
            "name"     => "name",
            "size"     => 35,
            "id"       => "name",
            "class"    => "form-control",
            "label"    => "��� / �������:",
            "placeholder" => "���/�������",
            "required" => True ));
        $this->name->add_validators($not_empty);

        $this->email = new EmailField(array(
            "name"     => "email",
            "id"       => "email",
            "label"    => "E-mail:*",
            "class"    => "form-control",
            "placeholder" => "E-mail",
            "required" => True ));

        $this->phone = new TextField(array(
            "name"     => "phone",
            "id"       => "phone",
            "label"    => "�������",
            "class"    => "form-control",
            "placeholder" => "�������",
            "required" => True ));
        $this->phone->type = "tel";
        $this->phone->add_validators($not_empty);

        $this->message = new TextAreaField(array(
            "name"     => "message",
            "id"       => "message",
            "label"    => "���������:",
            "placeholder" => "���������",
            "class"    => "form-control",
            "required" => True ));
        $this->message->add_validators($not_empty);
        $this->message->add_validators(new \Maksoft\Form\Validators\MinLength());

        $this->verify = new TextField(array(
            "name"     => "verify",
            "style"     => "display:none;"
        ));
        /** inverse notEmpty validator and throw exception when is not empty */
        $this->verify->add_validators(new \Maksoft\Form\Validators\NotEmpty(False)); 
        $this->submit = new SubmitButton(array("class"=>"btn btn-default"));
        $this->payload = array("email" => array());
        parent::__construct($form_data);
        $this->_storage = new \SplObjectStorage();

    }

    public function attach(\SplObserver $observer)
    {
        $this->_storage->attach( $observer );
    }

    public function detach(\SplObserver $observer)
    {
        $this->_storage->detach( $observer );
    }

    public function notify()
    {
        $this->is_valid();
        foreach($this->_storage as $observer){
            $observer->update($this);
        }
    }

    public function site()
    {
        return (object) $this->_page->_site;
    }

    public function page()
    {
        return (object) $this->_page->_page;
    }

    public function content()
    {
        return iconv("cp1251", "utf8", $this->message->value);
    }

    public function subject()
    {
        $subj = sprintf("%s - ���� ��������� �� %s.", $this->page()->Name, $this->name->value);
        return iconv("cp1251", "utf8", $subj);
    }

    public function fromEmail()
    {
        return $this->email->value;
    }

    public function toEmail()
    {
        return "cc@maksoft.bg";
        #return $this->site()->EMail;
    }
}
