<?php
namespace Maksoft\Admin\Profile;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\SubmitButton;


class Edit extends \Maksoft\Form\Forms\DivForm
{
    protected $form_data;
    private $_gate;

    public function __construct($gate, $form_data=null)
    {
        $this->_gate = $gate;
        $class = "form-control";
        $this->name = new TextField(
            array(
                "id"=>"name",
                "required" => True,
                "label" => "���/�������:*",
                "class" => $class
            )
        );
        $this->name->add_validators(new \Maksoft\Form\Validators\MinLength(5));
        $this->name->add_validators(new \Maksoft\Form\Validators\NotEmpty());

        $this->email = new EmailField(
            array(
                "id"=>"email",
                "label" => "Email �����:*",
                "required" => True,
                "class" => $class
            )
        );

        $this->phone = new TextField(
            array(
                "id"=>"phone",
                "required" => True,
                "label" => "�������:*",
                "class" => $class
            )
        );
        $this->phone->add_validators(new \Maksoft\Form\Validators\MinLength(7));
        $this->phone->add_validators(new \Maksoft\Form\Validators\NotEmpty());
        $this->edit_profile = new \Maksoft\Form\Fields\TextField(
            array(
                "hidden" => True,
                "value"  => "edit_profile"
            )
        );
        $this->submit = new SubmitButton(array("class"=>"btn btn-primary"));
        parent::__construct($form_data);
    }

    public function load(\page $page)
    {
        $this->name->value = $page->_user["Name"];
        $this->email->value = $page->_user["EMail"];
        $this->phone->value = $page->_user["Phone"];
    }

    public function update(\page $page)
    {
        $this->_gate->user()->updateProfileInfo($page->_user["ID"], 
                                       $this->name->value,
                                       $this->email->value,
                                       $this->phone->value);
    }
}
