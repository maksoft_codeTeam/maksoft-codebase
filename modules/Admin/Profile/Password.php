<?php
namespace Maksoft\Admin\Profile;
use \Maksoft\Form\Fields\PasswordField;
use \Maksoft\Form\Fields\RepeatPasswordField;
use \Maksoft\Form\Fields\SubmitButton;


class Password extends \Maksoft\Form\Forms\DivForm
{
    protected $form_data;
    private $_gate;

    public function __construct($gate, $form_data=null)
    {
        $this->_gate = $gate;
        $class = "form-control";
        $this->current_password = new PasswordField(
            array(
                "id"=>"password",
                "required" => True,
                "label" => "������ ������:",
                "class" => $class
            )
        );


        $this->new_password = new PasswordField(
            array(
                "id"=>"new_password",
                "required" => True,
                "label" => "���� ������:",
                "class" => $class
            )
        );
        $this->new_password->add_validators(new \Maksoft\Form\Validators\MaxLength(12));
        $this->new_password->add_validators(new \Maksoft\Form\Validators\MinLength(5));
        $this->new_password->add_validators(new \Maksoft\Form\Validators\HasDigit());
        $this->new_password->add_validators(new \Maksoft\Form\Validators\HasUpperCase());
        $this->repeat_password = new RepeatPasswordField(
            array(
                "id"=>"repeat_password",
                "required" => True,
                "label" => "������� ��������:",
                "class" => $class
            )
        );

        $this->submit = new SubmitButton(array("class"=>"btn btn-primary"));
        parent::__construct($form_data);
    }
    public function validate_password($pwd_field)
    {
        if($this->new_password->value === $this->current_password->value){
            throw new \Maksoft\form\Exceptions\ValidationError("������ � ������� ������ �� ����� �� ����� �������! ����, �������� �������� ������.");
        }
        if($this->new_password->value === $this->password23->value)
            return True;
        throw new \Maksoft\Form\Exceptions\ValidationError("�������� �� ��������, ���� �������� ������.", 1);
    }
    public function load(\page $page){
        $this->current_password->value = $page->_user["pass"];
    }

    public function update(\page $page)
    {
        $this->_gate->user()->editUserPass($page->_user["ID"], $this->new_password->value);

    }
}
