<?php
namespace Maksoft\Admin\Profile;

use \Maksoft\Form\Fields\PasswordField;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\SelectField;
use \Maksoft\Form\Fields\RepeatPasswordField;
use \Maksoft\Form\Fields\SubmitButton;


class AddUser extends \Maksoft\Form\Forms\DivForm
{
    protected $form_data;
    private $_gate;

    public function __construct(\Maksoft\Core\Gate $gate, \page $page, $form_data=null)
    {
        $this->_gate = $gate;
        $this->_page = $page;
        $class = "form-control";
        $this->username = new TextField(
            array(
                "label"=> "������������� ���:",
                "class" => $class,
            )
        );

        $this->name = new TextField(
            array(
                "label"=> "���, �������",
                "class" => $class,
            )
        );

        $this->phone = new TextField(
            array(
                "label"=> "������� �� ������:",
                "class" => $class,
            )
        );

        $this->email = new \Maksoft\Form\Fields\EmailField(
            array(
                "name" => "email",
                "label" => "Email �����:*",
                "class" => $class,
                "required" => True,
            )
        );
        $this->email->add_validators(new \Maksoft\Form\Validators\EmailValidator());

        $this->password = new PasswordField(
            array(
                "id"=>"password",
                "required" => True,
                "label" => "���� ������:",
                "class" => $class
            )
        );
        $this->password->add_validators(new \Maksoft\Form\Validators\MaxLength(12));
        $this->password->add_validators(new \Maksoft\Form\Validators\MinLength(4));
        $this->password->add_validators(new \Maksoft\Form\Validators\HasDigit());
        $this->password->add_validators(new \Maksoft\Form\Validators\HasUpperCase());
        $this->repeat_password = new RepeatPasswordField(
            array(
                "id"=>"repeat_password",
                "required" => True,
                "label" => "������� ��������:",
                "class" => $class
            )
        );

        $this->access = new SelectField(array(
                "name" => "accessLevel",
                "class" => $class,
                "label" => "���� �� ������:",
        )); 
        $this->submit = new SubmitButton(array("class"=>"btn btn-primary btn", "style"=>"margin-top:3%;", "value"=>"������"));
        parent::__construct($form_data);
    }

    public function validate_password($pwd_field)
    {
        if($this->password->value != $this->repeat_password->value){
            throw new \Maksoft\form\Exceptions\ValidationError("���������� ������ ����������! ����, �������� �������� ������.");
        }
        return True;
    }

    private function is_registered()
    {
        if(!$this->_gate->user()->is_registered($this->username->value, $this->email->value)){
            return True;
        }

        throw new \Exception("���������� � ���� ������������� ���/����� ���� ����������");
    }

    public function save()
    {
        $this->is_registered();
        $this->_gate->user()->newUser(
            $this->_page->_user["FirmID"],
            $this->_page->_user["ID"],
            $this->name->value,
            $this->email->value,
            $this->phone->value,
            $this->username->value,
            $this->password->value,
            11,
            $this->_page->_page["SiteID"],
            $this->access->value
        );
    }

    public function load()
    {
        $this->access->options = array();
        $tmp = array();
        foreach(range(1, $this->_page->_user["AccessLevel"]) as $level){
            $tmp[$level] = $level;
        }
        $this->access->options = $tmp;
    }

    public function list_users()
    {
        return $this->_gate->user()->getUsersByParentUserId($this->_page->_user["ID"]);
    }
}
