<?php
namespace Maksoft\Admin\Profile;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\SubmitButton;


class Company extends \Maksoft\Form\Forms\DivForm
{
    protected $form_data;
    private $_gate;
    private $_page;

    public function __construct(\Maksoft\Core\Gate $gate, \page $page, $form_data=null)
    {
        $this->_gate = $gate;
        $this->_page = $page;
        $class = "form-control";
        $this->name = new TextField(
            array(
                "id"=>"name",
                "disabled" => True,
                "label" => "���:*",
                "class" => $class
            )
        );

        $this->address = new TextField(
            array(
                "id"=>"address",
                "required" => True,
                "label" => "�����:*",
                "class" => $class
            )
        );
        $this->address->add_validators(new \Maksoft\Form\Validators\MinLength(5));
        $this->address->add_validators(new \Maksoft\Form\Validators\NotEmpty());


        $this->accountable = new TextField(
            array(
                "id"=>"accountable",
                "required" => True,
                "label" => "���:*",
                "class" => $class
            )
        );
        $this->accountable->add_validators(new \Maksoft\Form\Validators\MinLength(5));
        $this->accountable->add_validators(new \Maksoft\Form\Validators\NotEmpty());
        $this->email = new EmailField(
            array(
                "id"=>"email",
                "label" => "Email �����:*",
                "required" => True,
                "class" => $class
            )
        );

        $this->phone = new TextField(
            array(
                "id"=>"phone",
                "required" => True,
                "label" => "�������:*",
                "class" => $class
            )
        );
        $this->phone->add_validators(new \Maksoft\Form\Validators\MinLength(5));
        $this->phone->add_validators(new \Maksoft\Form\Validators\NotEmpty());

        $this->eik = new TextField(
            array(
                "id"=>"eik",
                "label" => "������� �����:",
                "class" => $class
            )
        );

        $this->bulstat = new TextField(
            array(
                "id"=>"bulstat",
                "required" => True,
                "label" => "�������:*",
                "class" => $class
            )
        );


        $this->fax = new TextField(
            array(
                "id"=>"fax",
                "label" => "����:",
                "class" => $class
            )
        );

        $this->office = new TextField(
            array(
                "id" => "office",
                "label" => "����:",
                "class" => $class
            )
        );


        $this->submit = new SubmitButton(array("class"=>"btn btn-primary"));
        parent::__construct($form_data);
    }

    public function load($firm_id)
    {
        $firm = $this->_gate->fak()->getFirmById($firm_id);
        $this->name->value = $firm->Name;
        $this->address->value = $firm->Address;
        $this->accountable->value = $firm->MOL;
        $this->email->value = $firm->EMail;
        $this->phone->value = $firm->Phone;
        $this->eik->value = $firm->dn;
        $this->bulstat->value = $firm->bulstat;
        $this->fax->value = $firm->Fax;
        $this->office->value = $firm->Office;
    }

    public function update()
    {
        $this->_gate->fak()->updateFirm($this->_page->_user["FirmID"],
                                 $this->email->value,
                                 $this->phone->value,
                                 $this->fax->value,
                                 $this->accountable->value,
                                 $this->address->value,
                                 $this->office->value,
                                 $this->eik->value,
                                 $this->bulstat->value);

    }
}
