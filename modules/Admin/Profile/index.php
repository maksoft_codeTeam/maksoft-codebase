<?php
if( $user->WriteLevel < 1){
    return;
}
mb_internal_encoding("cp1251");
$autoloader = realpath(__DIR__."/../../vendor/autoload.php");
$settings = realpath(__DIR__."/../../settings.php");
require_once($autoloader);
require_once($settings);
$db = new \Maksoft\Core\MysqlDb($env);
$db->exec("SET NAMES cp1251");
$default_image="https://lh3.googleusercontent.com/-H7_QuNyFVMM/AAAAAAAAAAI/AAAAAAAAAAA/1095ekL4OEA/photo.jpg";
$gate = new \Maksoft\Gateway\Gateway($db);
$avatar = $gate->user()->getAvatar($o_page->_user["ID"]);
if($avatar){
    $default_image=$avatar->picture;
}
?>
<section>
    <div class="container">
        <!-- RIGHT -->
        <div class="col-lg-9 col-md-9 col-sm-8 col-lg-push-3 col-md-push-3 col-sm-push-4 margin-bottom-80">
            <ul class="nav nav-tabs nav-top-border">
                <li class="active"><a href="#info" data-toggle="tab">����� �����</a></li>
                <li><a href="#avatar" data-toggle="tab">�����</a></li>
                <li><a href="#password" data-toggle="tab">������</a></li>
                <li><a href="#privacy" data-toggle="tab">������������ �� ���</a></li>
                <li><a href="#my_sites" data-toggle="tab">����� �������</a></li>
            </ul>
            <div class="tab-content margin-top-20">
                <!-- PERSONAL INFO TAB -->
                <div class="tab-pane fade in active" id="info">
                    <p><h3> ������� �� ������������� �����: </h3></p>
                    <hr>
                    <?php
                    if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["edit_profile"])){
                        $profile = new \Maksoft\Admin\Profile\Edit($gate, $_POST);
                        $profile->setName("profile");
                        try {
                            $profile->is_valid();
                            $profile->update($o_page);
                            echo '<div class="alert alert-success" role="alert">������� ���������� ������ ������</div>';
                        } catch (Exception $e) {
                            require_once __DIR__."/errors.php";
                        }
                    } else {
                        $profile = new \Maksoft\Admin\Profile\Edit($gate);
                        $profile->setName("profile");
                        $profile->setID("profile");
                        $profile->load($o_page);
                    }
                    echo $profile;
                    ?>
                </div>
                <!-- /PERSONAL INFO TAB -->
                <!-- COMPANY TAB -->
                <div class="tab-pane fade" id="avatar">
                 <?php 
                    if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['bulstat'])){

                        try {
                            $company = new \Maksoft\Admin\Profile\Company($gate, $o_page, $_POST);
                            $company->is_valid();
                            $company->update();
                            echo '<div class="alert alert-success" role="alert">������� ���������� ������� �� ������ �����</div>';
                        } catch (Exception $e) {
                          require_once __DIR__."/errors.php";
                        }
                    } else {
                        $company = new \Maksoft\Admin\Profile\Company($gate, $o_page);
                    }
                    if($o_page->_user["FirmID"]){
                        $company->load($o_page->_user["FirmID"]);
                    }
                    echo $company;
                 ?>
                </div>
                <!-- /COMPANY TAB -->

                <!-- PASSWORD TAB -->
                <div class="tab-pane fade" id="password">
                    <p><h3> ������� �� ������: </h3></p>
                    <hr>
                    <ul>
                        <li> ���������� ��� ��������: 
                            <ul>
                                <li>���������� ������� - 12 �������</li>
                                <li>��������� ������� - 5 �������</li>
                                <li>���� ���� ������ �����</li>
                                <li>���� ���� �����</li>
                            </ul>
                        </li>
                    </ul>
                    <?php
                    if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['new_password'])){
                        $password = new \Maksoft\Admin\Profile\Password($gate, $_POST);
                        $password->setName("password");
                        try {
                            $password->is_valid();
                            $password->update($o_page);
                            echo '<div class="alert alert-success" role="alert">������� ���������� ������ ������</div>';
                        } catch (Exception $e) {
                            require_once __DIR__."/errors.php";
                        }
                    } else {
                        $password = new \Maksoft\Admin\Profile\Password($gate);
                        $password->setName("password");
                        $password->load($o_page);
                    }
                    echo $password;
                    ?>
                </div>
                <!-- /PASSWORD TAB -->
                <!-- NEW-USER TAB -->
                <div class="tab-pane fade" id="privacy">
                <?php
                    if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['username'])){
                        $new_user = new \Maksoft\Admin\Profile\NewUser($gate, $o_page, $_POST);
                        try{

                           $new_user->is_valid();
                           $new_user->save();
                        } catch (Exception $e) {
                           require_once __DIR__."/errors.php";
                        }

                    } else {
                        $new_user = new \Maksoft\Admin\Profile\NewUser($gate, $o_page);
                    }

                    $new_user->load();

                    $registered_users = $new_user->list_users();
                    ?>
                    <p><h3>��������� �� ��� ���������� � ��������� �� �������CMS </h3></p>
                    <hr>
                    <ul>
                        <li> ���������� ��� ��������: 
                            <ul>
                                <li>���������� ������� - 12 �������</li>
                                <li>��������� ������� - 5 �������</li>
                                <li>���� ���� ������ �����</li>
                                <li>���� ���� �����</li>
                            </ul>
                        </li>
                    </ul>
                    <hr>
                    <table>
                        <th>ID</th>
                        <th>���</th>
                        <th>������������� ���</th>
                        <th>Email</th>
                        <th>���� �� ������</th>
                        <th>������</th>
                        <th>������</th>
                    <?php
                    echo $new_user;
                    $modals = array();
                    $forms = array();
                    foreach($registered_users as $reg_user){
                        $permissions = $gate->site()->get_site_permissions($reg_user->ID, $o_page->_site["SitesID"]);
                        if($permissions){ continue; }
                        ?>
                        <tr>
                            <td><?=$reg_user->ID;?></td>
                            <td><?=$reg_user->Name;?></td>
                            <td><?=$reg_user->username;?></td>
                            <td><?=$reg_user->EMail;?></td>
                            <td><?=$reg_user->AccessLevel;?></td>
                            <td>  
                                <?php
                                    if($reg_user->SiteID == $o_page->_site["SitesID"]){
                                        echo "��� ������";
                                    } else {
                                ?>
                                <button 
                                    type="button"
                                    class="btn btn-success btn-xs"
                                    data-toggle="modal"
                                    data-target="#modal-<?=$reg_user->ID;?>">
                                        ������ ������
                                  </button>
                                  <?php } ?>
                              </td>

                            <td>������</tr>
                        </tr>
                    
                        <?


                $forms[$reg_user->ID] = new \Maksoft\Admin\Profile\Access($gate, $o_page, $reg_user->ID);
                $modals[] = <<<HEREDOC
<div class="modal fade" id="modal-{$reg_user->ID}" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>�������� �� ����� �� ���������� <b>{$reg_user->Name}</b> �� ����: {$o_page->_site['primary_url']}</p>
        {$forms[$reg_user->ID]}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
HEREDOC;
                    }
                    ?>
                    </table>
                    <?php
                echo implode(PHP_EOL, $modals);

                if($_SERVER["REQUEST_METHOD"] === "POST" && array_key_exists($_POST['action'], $forms)){
                    try{

                            $f = new \Maksoft\Admin\Profile\Access($gate, $o_page, 99999);
                            $f->load($_POST);
                            $f->is_valid();
                            $f->save();
                        
                    } catch (\Exception $e){
                        require_once __DIR__."/errors.php";
                    }
                }
                ?>
                
                </div>
                <!-- /NEW-USER TAB -->
                <!-- NEW-USER TAB -->
                <div class="tab-pane fade" id="my_sites">
                <?php
                $sites= $gate->user()->get_uSites($o_page->_user["ID"]);
                ?>
                <table>
                 <tr>
                    <th>#</th>
                    <th>������</th>
                    <th>Email</th>
                    <th>������</th>
                    <th>�����</th>
                  </tr>
                <?php foreach($sites as $site){ ?>
                    <tr>
                        <td><?=$site->SitesID;?></td>
                        <td><a href="<?=$site->primary_url;?>"><?=$site->primary_url;?></a></td>
                        <td><?=$site->EMail;?></td>
                        <td><?=$site->ReadLevel;?></td>
                        <td><?=$site->WriteLevel;?></td>
                     </tr>
                <?php } ?>
                    </table>
                </div>
                <!-- /NEW-USER TAB -->
            </div>
        </div>
        <!-- LEFT -->
        <div class="col-lg-3 col-md-3 col-sm-4 col-lg-pull-9 col-md-pull-9 col-sm-pull-8">
            <div class="thumbnail text-center">
                <img id="profile-picture" src="<?=$default_image;?>" alt="" />
                <h2 class="size-18 margin-top-10 margin-bottom-0"><?=$o_page->_user["Name"];?></h2>
            </div>
            <!-- completed -->
            <div class="margin-bottom-30">
                <?php
                $picture = new \Maksoft\Admin\Profile\Picture($gate, $o_page);
                if($_SERVER["REQUEST_METHOD"]==="POST" && isset($_FILES["avatar"]))
                {
                    try{
                        $picture->save($o_page->_user["ID"]);
                    } catch ( \Exception $e ) {
                        require_once __DIR__."/errors.php";
                    }
                }
                echo $picture;
                ?>
            </div>
            <!-- /completed -->
       </div>
</section>

