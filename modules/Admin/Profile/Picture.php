<?php
namespace Maksoft\Admin\Profile;
use \Maksoft\Form\Fields\FileInputField;
use \Maksoft\Form\Fields\SubmitButton;


class Picture extends \Maksoft\Form\Forms\DivForm
{
    protected $form_data;
    private $_gate;
    private $_page;

    public function __construct(\Maksoft\Core\Gate $gate, \page $page, $form_data=null)
    {
        $this->_gate = $gate;
        $this->_page = $page;
        $class = "form-control";
        $this->avatar = new FileInputField(array(
            "label"    => "����� ��������:",
            "id"       => "avatar",
            "class"    => "form-control",
            "required" => True
            ));
        $this->avatar->add_validators(new \Maksoft\Form\Validators\NotBiggerThan(200000)); // 200kb
        $this->avatar->add_validators(new \Maksoft\Form\Validators\FileTypeMatch("image/jpeg"));
        $this->avatar->add_validators(new \Maksoft\Form\Validators\FileExtensionMatch(".jpg"));

        $this->submit = new SubmitButton(array("class"=>"btn btn-primary", "style"=>"margin-top:4%;"));
        parent::__construct($form_data);
    }

    public function save($user_id)
    {
        $this->is_valid();
        $avatar = $_FILES[$this->avatar->name];
        $img_path = "web/images/profile/".$user_id."/".$avatar["name"];
        $path = $_SERVER["DOCUMENT_ROOT"]."/".$img_path;
        $this->dir_exist($user_id);
        $f = move_uploaded_file($avatar['tmp_name'], $path);
        if($f){
            $imageID = $this->_gate->user()->setImage($user_id, $img_path);
            return $imageID;
        }
    }

    private function dir_exist($id)
    {
        $dir = "/hosting/maksoft/maksoft/web/images/profile/".$id."/";
        if(!is_dir($dir)){
            mkdir($dir, 0755);
        }
    }
}

