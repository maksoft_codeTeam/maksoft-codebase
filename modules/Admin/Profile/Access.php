<?php
namespace Maksoft\Admin\Profile;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\SelectField;
use \Maksoft\Form\Fields\SubmitButton;


class Access extends \Maksoft\Form\Forms\DivForm
{
    private $_gate, $_page;
    public function __construct(\Maksoft\Core\Gate $gate, \page $page,$user_id, $form_data=null)
    {
        $this->_gate = $gate;
        $this->_page = $page;
        $class = "form-control";

        $this->read_level = new SelectField(array(
                "class" => $class,
                "name" => "read_level",
                "label" => "���� �� �����e:",
        )); 
        $tmp = array();
        foreach(range(0, $_SESSION["user"]->ReadLevel) as $level){
            $tmp[$level] = $level;
        }
        $this->read_level->options = $tmp;


        $this->write_level = new SelectField(array(
                "class" => $class,
                "name" => "write_level",
                "label" => "���� �� �����:",
        )); 

        $tmp = array();
        foreach(range(0, $_SESSION["user"]->WriteLevel) as $level){
            $tmp[$level] = $level;
        }
        $this->write_level->options = $tmp;

        $this->action = new TextField(array("hidden"=>"True", "value"=>$user_id));
        $this->action->add_validators(new \Maksoft\Form\Validators\NotEmpty());

        $this->submit = new SubmitButton(array("class"=>"btn btn-primary btn", "style"=>"margin-top:3%;", "value"=>"������"));

        parent::__construct($form_data);
    }

    public function load($post)
    {
        parent::__construct($post);
    }

    public function save()
    {
        $user_id = $this->action->value;
        return $this->_gate->user()->setSiteAccess(
            $this->_page->_site["SitesID"],
            11,
            $this->read_level->value,
            $this->write_level->value,
            $user_id
        );
    }
}
