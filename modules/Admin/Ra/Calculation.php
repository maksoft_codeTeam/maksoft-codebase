<?php
namespace Maksoft\Admin\Ra;
use Maksoft\Form\Forms\DivForm;
use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\HiddenField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Fields\TextAreaField;


class Calculation extends DivForm
{
    private $doc_id = -1;
    private $No = 0;
    private $_gate;
    private $_page;
    private $_cart;

    public function __construct($gate, $page, $cart, $form_data=null)
    {
        $this->_gate = $gate;
        $this->_page = $page;
        $this->_cart = $cart;

        $this->action = new HiddenField(array(
            "value" => "create_calculation",
        ));
        $this->document_notes = new TextAreaField(

            array(
                "name" => "document_notes",
                "class"=>"form-control custom-control",
                "rows" => "3",
                "style"=>"resize:none",
            )
        );
        $today = date("Y-m-d", time());
        $this->end_date = new TextField(
            array(
                "name" => "end_date",
                "id" => "end_date",
                "value" => date('Y-m-d', strtotime($today. ' + 1 days'))
            )
        );
        $this->submit = new SubmitButton(array("class"=>"btn btn-success"));
        parent::__construct($form_data);
    }

    #public function validate_document_notes()
    #{
    #    $config = \HTMLPurifier_Config::createDefault();
    #    $purifier = new \HTMLPurifier($config);
    #    $clean_html = $purifier->purify($this->document_notes);
    #    $this->document_notes->value = $clean_html;
    #    return True;
    #}

    private function deadline()
    {
        $datetime1 = new \DateTime(date("Y-m-d", time()));
        $datetime2 = new \DateTime($this->end_date->value);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%a');
    }

    public function action()
    {
        $fID = $this->_gate->fak()->createCalculation(
            $this->document_notes->value, 
            $this->_page->_user["FirmID"],
            $this->_cart->count(),
            0,
            $this->_cart->sum(),
            $this->_cart->vat(),
            $this->_cart->sum_with_vat(),
            100,
            0,
            $this->_page->_user["ID"],
            $this->deadline()
        );

        $items = $_SESSION['cart'];               

        foreach($items as $index => $item) {
            $it = $item;
            $item = new \Maksoft\Cart\Item($it['sku'], $it['qty'], $it['price'], $it['name'], $it['unit'], $it['category']);

            $this->insert_item($fID, $index+1, $item->getName(), $item->getSku(), $item->getUnit(), $item->getQty(), $item->getPrice(), $item->total());
            if($it["branding"]==1){
                $it["itStr"] = "���������� �� ������� - ".$item->getSku();
                $it["itID"] = 0;
                $it["price"] = 0;
                $this->insert_item($fID, $index+2, $it["itStr"], $it["itID"], $item->getUnit(), $item->getQty(), $item->getPrice(), 0);
            }
        }
        return $fID;
    }

    protected function insert_item($fID, $index, $itStr, $itID, $m, $qty, $price, $total, $unknown=0)
    {
            $this->_gate->fak()->insertDocumentItems(
                $fID,
                $index,
                $itStr,
                $itID,
                $m,
                $qty,
                $price,
                $unknown,
                $total
            );
    }
}
