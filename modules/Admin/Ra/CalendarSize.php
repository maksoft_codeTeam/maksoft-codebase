<?php
namespace Maksoft\Admin\Ra;
use \Maksoft\Form\DivForm;
use \Maksoft\Form\Field\Select;
use \Maksoft\Form\Field\File;
use \Maksoft\Form\Field\Submit;
use \Maksoft\Form\Validators\Integerish;
use \Maksoft\Form\Validators\FileExtensionMatch;


class CalendarSize extends DivForm
{
    private $calendars = array(
        "���������", 
        "���� 3 ����",
        "���� 4 ����",
        "������ 3 ����", 
        "������ 2 ����",
        "���� 3 ����",
        "���� 4 ����",
        "�������",
        "����������",
        "����");
    private $sizes = array(
        array(array(308, 219), array(308, 699)),
        array(array(308, 219), array(308, 604)),
        array(array(308, 219), array(308, 699)),
        array(array(308, 219), array(308, 699)),
        array(array(308, 219), array(308, 699)),
        array(array(334, 237), array(330, 805)),
        array(array(334, 237), array(330, 805)),
        array(array(308, 219), array(308, 544)),
        array(array(308, 219), array(308, 544)),
        array(array(308, 219), array(308, 660)),
    );


    public function __construct($post_data=array())
    {
        $this->select = Select::init()
                            ->add("label", "����� ��������")
                            ->add("required", True)
                            ->add("class", "form-control")
                            ->add("options", $this->calendars)
                            ->add_validator(Integerish::init());
        $this->pdf    = File::init()
                            ->add("label", "PDF ���� �� ��������")
                            ->add("class", "form-control")
                            ->add("required", True)
                            ->add_validator(new FileExtensionMatch('.pdf'));

        $this->submit = Submit::init()
                            ->add("class", "btn btn-default")
                            ->add("value", "�������");
        parent::__construct($post_data);
    }

    public function getCalendars()
    {
        return $this->calendars;
    }

    public function getSizes()
    {
        return $this->sizes;
    }

    public function getCalendarName()
    {
        return $this->calendars[$this->select->value];
    }

    public function calculate()
    {
        $pdf = new \FPDI();
        $pageCount = $pdf->setSourceFile($_FILES[$this->pdf->name]['tmp_name']);  
        $firstPage = 2;
        for($pageNo=1; $pageNo < $firstPage; $pageNo++){
            $templateId = $pdf->importPage($pageNo);
            // get the size of the imported page
            $size = $pdf->getTemplateSize($templateId);
        }
        $calendar = array_shift($this->sizes[$this->select->value]);
        list($x, $y) = $calendar;
        if($size['w'] >= $x or $size['h'] >= $y ){
            return array("msg" => "������� �� ����� � ��������",
                         "status" => "success",
                         "file"=>$size, 
                         "calendar" => array("w"=> $x, "h" => $y)
                );
        }
        return array("msg" => "��������� ������", "status" => "error", "file"=>$size, "calendar" => array("w"=> $x, "h" => $y));
    }
}

