<?php
namespace Maksoft\Admin\Ra;
use Maksoft\Form\Forms\DivForm;
use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\HiddenField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Fields\TextAreaField;


class DeleteCalculation extends DivForm
{
    private $doc_id = -1;
    private $No = 0;
    private $_gate;
    private $_page;
    private $_cart;

    public function __construct($gate, $page, $form_data=null)
    {
        $this->_gate = $gate;
        $this->_page = $page;
        $this->_cart = $cart;

        $this->action = new HiddenField(array(
            "value" => "delete_calculation",
        ));
        $this->fID = new HiddenField(array());
        $this->submit = new SubmitButton(array(
            "class"=>"btn btn-default",
            "id"   => "delete",
            "value" => "������"
        ));
        parent::__construct($form_data);
    }

    public function action()
    {
        $this->_gate->fak()->deleteDocument($this->fID->value, $this->_page->_user["ID"]);
    }
}
