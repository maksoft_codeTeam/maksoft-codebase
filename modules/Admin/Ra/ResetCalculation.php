<?php
namespace Maksoft\Admin\Ra;
use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\TextField;


class ResetCalculation extends DivForm
{
    public function __construct($form_data=null)
    {
        $this->action = new HiddenField( array( "value" => "reset_calculation"));
        $this->submit = new SubmitButton(
            array(
                "class" => "btn btn-default",
                "value" => "������"
            )
        );

        parent::__construct($form_data);
    }

    public function action()
    {
        unset($_SESSION[\Maksoft\Cart\Cart::getName()]);
    }
}
