<?php
namespace Maksoft\Admin\Ra;
require_once '/hosting/maksoft/maksoft/lib/Notify.php';


class Router
{
    protected $map = array();
    private $gate;
    private $twig;
    private $cart;
    private $page;
    private $seller=8582;
    private $client;
    private $group = array();
    private $promotions_group=3293;
    private $discounts = array();
    private $group_discounts = array();

    public function __construct($gate, $page, $twig, \Maksoft\Cart\Cart $cart)
    {
        $this->gate = $gate;
        $this->page = $page;
        $this->twig = $twig;
        $this->cart = $cart;
        $this->notify = new \Notify("e992cf74-4dc5-4b1b-9347-f086e1c3e7f5", "ODZmNjQ2ZmQtYzMyZi00MDEzLTg0NWMtOWVmNDI4N2Q2NGNm");
        $this->client = $this->page->_user["FirmID"];
        $this->map[19340234] = "list_invoices";
        $this->map[19358254] = "not_completed_orders";
        $this->map[19342795] = "completed_orders";
        $this->map[19340233] = "list_products";
        $this->map[19340285] = "list_calculations";
        $this->map[19340423] = "my_orders";
        $this->map[19340230] = "dashboard";
        $this->map[19342166] = "calendar_calculator";
        $this->map[19342796] = "list_unpaid";
        $start_page = $this->page->_site["StartPage"];
        $this->map[$start_page] = "print_index" ;
        $this->can_edit = \site::$uReadLevel;
    }
    public function list_unpaid()
    {
        $vars = array(
            "documents" => $this->gate->fak()->getUnpaidDocuments($this->client),
            "firm" =>  $this->gate->fak()->getFirmById($this->client),
        );
        $template = "list.html";
        return $this->render2($vars, $template);
    }

    public function calendar_calculator()
    {
        $form = new \Maksoft\Admin\Ra\CalendarSize($_POST);
        $form->div_class("form-group");
        $msg = array();
        if($_SERVER['REQUEST_METHOD'] === "POST"){
            try{
            $form->is_valid();
            $msg = $form->calculate();
            } catch (\Exception $e){
                $msg['status'] = 'error';
                $msg['msg'] = $e->getMessage();
            }
        }

        $vars = array('form' =>  $form, "response" => $msg);
        $template = 'calendar_calculator.html';
        return $this->render2($vars, $template);
    }

    public function setInfoGroup($item_group, $page_group_n){
        $this->group[$item_group] = $page_group_n;
    }

    public function setDiscounts(array $discounts)
    {
        $this->discounts = $discounts;
    }

    public function getGroupDiscount($group_id)
    {
        if(array_key_exists($group_id, $this->group_discounts)){
            return $this->group_discounts[$group_id];
        }
        return 1;
    }

    public function setGroupDiscounts(array $default_dis)
    {
        $this->group_discounts = $default_dis;
        return $this;
    }

    public function getTotalTurnover()
    {
        return $this->gate->fak()->getClientTotal($this->client);
    }

    public function getCurrentDiscount()
    {
        $turnover = $this->getTotalTurnover();
        $group = $_GET['group'];
        $default_setted_discount = $this->gate->fak()->getCompanyDiscounts($this->client, 0);
        $c = $this->gate->fak()->getCompanyDiscounts($this->client, $group);
        if($c){
            return $c->disc * $this->getGroupDiscount($group);
        } elseif($default_setted_discount){
            return $default_setted_discount->disc * $this->getGroupDiscount($group);
        }
        $curr_d = 20;

        foreach($this->discounts as $step=>$discount){
            if($turnover->total < $step){
                break;
            }
            $curr_d = $discount;
        }
        return $curr_d * $this->getGroupDiscount($group);
    }

    public function getNextDiscount()
    {
        $stop = false;
        $turnover = $this->getTotalTurnover();
        foreach($this->discounts as $step=>$discount){
            if($stop) {
                return array("discount"=>$discount, "more"=> bcsub((string) $step, (string) $this->getTotalTurnover()->total));
            }
            if($turnover < $step){
                $stop = True;
            }
        }
    }

    public function setSeller($n)
    {
        $this->seller = $n;
    }

    public function get_seller($n)
    {
        return $this->seller;
    }

    public function render()
    {
        if(!isset($_SESSION["user"]) or !is_object($_SESSION["user"])){
            $vars = array(
                "link_log" => $this->page->get_pLink(19340223),
                "name_log" => $this->page->get_pName(19340223),
                "img_log"  => $this->page->get_pImage(19340223),
                "link_reg" => $this->page->get_pLink(19340224),
                "name_reg" => $this->page->get_pName(19340224),
                "img_reg"  => $this->page->get_pImage(19340224),
            );
            $template = "login.html";
            return $this->render2($vars, $template);
        }
        if($_SESSION['user']->ReadLevel < 1){
            return $this->render2(array(), 'login.html');
        }
        if(array_key_exists($this->page->_page["n"], $this->map)){
            $func = $this->map[$this->page->_page["n"]];
            return $this->$func();
        }
        return;
    }

    protected function render2($vars, $template)
    {
        try{
            $template = $this->twig->loadTemplate($template);
        } catch (\Twig_Error $e) {
            $template = $this->twig->loadTemplate('error_page.html');
        }
        return $template->render($vars);
    }

    public function my_orders()
    {
        $del_form = new DeleteCalculation($this->gate, $this->page, $_POST);
        $edit = new \Maksoft\Admin\Product\Edit($this->gate, $this->cart, $_POST);
        if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["action"])){
            switch ($_POST["action"]){
                case "delete_calculation":
                    $del_form->is_valid();
                    $del_form->action();
                    break;
            }
        }
        $vars = array(
            "documents" => $this->gate->fak()->getDocumentsByUserID($this->page->_user["ID"]),
            "del_form" => $del_form,
            "firm" =>  $this->gate->fak()->getFirmById($this->client),
            "user" => $this->page->_user,
        );
        $template = "list.html";
        return $this->render2($vars, $template);
    }

    private function invalid_page()
    {
        return $this->render2(array(), 'error_page.html');
    }

    private function print_index()
    {
        $vars = array(
            "total" => $this->getTotalTurnover(),
            "products" => $this->gate->fak()->getGroupProducts($this->promotions_group, $this->getCurrentDiscount()),
            "view" => $this->can_edit,
            "page" => $this->page,
        );
        $template = "index.html";
        return $this->render2($vars, $template);
    }

    private function list_products()
    {
        $form        = new \Maksoft\Admin\Product\AddProduct($this->gate, $this->cart);
        $formApi     = new \Maksoft\Admin\Product\AddProductFromApi($this->gate, $this->cart);
        $branding    = new \Maksoft\Admin\Product\Brandit($this->gate, $this->cart);
        $remove      = new \Maksoft\Admin\Product\Remove($this->gate, $this->cart);
        $edit        = new \Maksoft\Admin\Product\Edit($this->gate, $this->cart);
        $calculation = new Calculation($this->gate, $this->page, $this->cart);
        $reset_form  = new ResetCalculation($_POST);


        if($this->page->_user['ID'] == 1424){
            //echo '<pre>';
            //var_dump($_SESSION);
            //var_dump($_POST);
            //echo '</pre>';
        }
        if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["action"])){
            switch ($_POST["action"]){
                case "add_product":
                    $form = new \Maksoft\Admin\Product\AddProduct($this->gate, $this->cart, $_POST);
                    $form->is_valid();
                    $form->action();
                    break;
                case "add_product_from_api":
                    $formApi = new \Maksoft\Admin\Product\AddProductFromApi($this->gate, $this->cart);
                    $formApi->is_valid();
                    $formApi->action();
                    break;
                case "create_calculation":
                    $calculation = new Calculation($this->gate, $this->page, $this->cart, $_POST);
                    $calculation->is_valid();
                    $fID = $calculation->action();
                    $this->cart->reset();
                    $customer_name = reset(explode(" ", $this->page->_user["Name"]));
                    $filters = array("field" => "tag", "key" => 'user_id', "relation"  => "=", "value"  => $this->page->_user['ID']);
                    $msg = sprintf("Поздравления %s! Успешно създадохте нова калкулация с номер %s", $customer_name, $fID);  
                    $this->notify->sendMessage($msg, array("Document activity"), false, $filters); 
                    break;
                case "reset_calculation":
                    $this->cart->reset();
                    break;
                case "remove":
                    $remove = new \Maksoft\Admin\Product\Remove($this->gate, $this->cart, $_POST);
                    $remove->is_valid();
                    $remove->action();
                    break;
                case "edit":
                    $edit = new \Maksoft\Admin\Product\Edit($this->gate, $this->cart, $_POST);
                    $edit->is_valid();
                    $edit->action();
                    break;
                case "branding":
                    $branding = new \Maksoft\Admin\Product\Brandit($this->gate, $this->cart, $_POST);
                    $branding->is_valid();
                    $branding->action();
                    break;
            }
        }
        $items = $this->cart->get_items();
        $js = $items;
        array_walk_recursive($js, function(&$item, $key) {
                if ($item == null) $item = '';
        });
        $items_options=array();
        if(array_key_exists($_GET['group'], $this->group)){
            $group_id = $this->group[$_GET['group']];
            $items_options = $this->gate->page()->getPageGroupContent($group_id, 2, $this->page->_user['AccessLevel'], 'p.n DESC');
        }


        $vars = array(
            #"products" => $this->gate->fak()->getSiteProducts(2, $_GET["group"], $this->getCurrentDiscount()),
            "groups" => $this->gate->fak()->getItemGroups(),
            "url" => "/page.php?n=".$this->page->_page["n"]."&SiteID=".$this->page->_site["SitesID"],
            "form" => $form,
            "items" =>  $this->cart->get_items(),
            "json_it" => json_encode($js, true),
            "mak" => $this->gate->fak()->getFirmById($this->seller),
            "firm" => $this->getClient(),
            "cart" => $this->cart,
            "calculation" => $calculation,
            "reset_button" => $reset_form,
            "brandit" => $branding,
            "remove" => $remove,
            "view" => $this->can_edit,
            "options" => $items_options,
            "group" => $_GET["group"],
            "page" => $this->page,
            #"promo_items" => $this->gate->fak()->getGroupProducts($this->promotions_group, $this->getCurrentDiscount()),
            "edit" => $edit,
        );
        if(isset($_GET['group'])){
            $vars['products'] = $this->gate->fak()->getSiteProducts(2, $_GET["group"], $this->getCurrentDiscount()); 
        } else {
            $vars["products"] = $this->gate->fak()->getGroupProducts($this->promotions_group, $this->getCurrentDiscount());
        }

        try{
            $strickerCatalog = \Maksoft\AFI\Provider\Stricker::initialize();
            $catalogues = $strickerCatalog->get("catalogues");
            $vars["catalogues"] = (array)$catalogues; 
            if(isset($_GET['catalogueId']) and !empty($_GET['catalogueId'])){
                if(isset($_GET['sectionId']) and !empty($_GET['sectionId'])){
                    if(isset($_GET['familyId']) and !empty($_GET['familyId'])){
                        $vars["familyId"] = $_GET['familyId'];
                        $vars["catalogProducts"] = (array) $strickerCatalog->products($_GET['catalogueId'], $_GET['sectionId'], $_GET['familyId']);
                    } else {
                        $vars["familyList"] = (array) $strickerCatalog->catalogFamilies($_GET['catalogueId'], $_GET['sectionId']);
                    }
                    $vars["sectionId"] = $_GET['sectionId'];
                } else {
                    $vars["catalogueSections"] = (array) $strickerCatalog->catalogueSection($_GET['catalogueId']);
                }
                $vars["catalogueId"] = $_GET['catalogueId'];
            }
        } catch (\Exception $e){

        }

        $template = "order.html";
        return $this->render2($vars, $template);
    }

    public function getClient()
    {
        return $this->gate->fak()->getFirmById($this->client);
    }

    public function getAccount()
    {
        return $this->gate->fak()->getFirmAccount($this->client);
    }

    private function list_invoices()
    {
        $vars = array(
            "documents" => $this->gate->fak()->getAllInvoices($this->client),
            "firm" =>  $this->gate->fak()->getFirmById($this->client),
        );
        $template = "list.html";
        return $this->render2($vars, $template);
    }

    private function not_completed_orders()
    {
        $vars = array(
            "documents" => $this->gate->fak()->getOrders($this->client, false),
            "firm" =>  $this->gate->fak()->getFirmById($this->client),
        );
        $template = "list.html";
        return $this->render2($vars, $template);
    }

    private function completed_orders()
    {
        $vars = array(
            "documents" => $this->gate->fak()->getOrders($this->client, true),
            "firm" =>  $this->gate->fak()->getFirmById($this->client),
        );
        $template = "list.html";
        return $this->render2($vars, $template);
    }

    private function list_calculations()
    {
        $vars = array(
            "documents" => $this->gate->fak()->getCalculations($this->client, false),
            "firm" =>  $this->gate->fak()->getFirmById($this->client),
        );
        $template = "list.html";
        return $this->render2($vars, $template);
    }
    private function __destruct()
    {
        $_SESSION['cart'] = $this->cart->save();
    }
}
