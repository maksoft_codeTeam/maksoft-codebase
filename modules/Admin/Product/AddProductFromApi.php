<?php
namespace Maksoft\Admin\Product;
use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Forms\DivForm;
use Maksoft\Amount\Money;
use \Maksoft\Cart\Cart;


#ini_set("auto_detect_line_endings", TRUE);
class AddProductFromApi extends DivForm
{
    private $gate;

    private $product;

    private $cart;

    public function __construct($gate, Cart $cart, $post_data=null)
    {
        $this->cart = $cart;
        $this->gate = $gate;
        $this->itID = new TextField(array(
            "name" => "itID",
            "hidden"=>True,
        ));
        $this->qty = new TextField(array(
            "name" => "qty",
            "label" => "�-��:",
            "value" => 1,
        ));

        $this->qty->add_validators(new \Maksoft\Form\Validators\Integerish());

        $this->action = new TextField(array(
            "name" => "action",
            "value" => "add_product_from_api",
            "hidden" => True,
        ));

        $this->price = new TextField(array(
            "name" => "price",
            "value" => "0",
            "hidden" => True,
        ));  

        $this->submit = new SubmitButton(array(
            "class"=>"btn btn-default",
            "style" => "margin-top:3%;",
        ));
        parent::__construct($post_data);
    }

    public function action()
    {
        $price = number_format($_POST['price'], 3, '.', '');
        $product  = new \Maksoft\Cart\Item($this->itID->value, $this->qty->value, $price, $_POST['itName']."| N:".$_POST['itID'], '��.', $_POST['itName']);
        $product->setMoq($this->qty->value);
        $product->setSku($_POST['itID']);
        $product->setCategory($_POST['itName']);
        $this->cart->add($product);
        $_SESSION[\Maksoft\Cart\Cart::getName()] = $this->cart->save();
    }

    private function format_price($price)
    {
        return number_format($price, 2);
    }

}
