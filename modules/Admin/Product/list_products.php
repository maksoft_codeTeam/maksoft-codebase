<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/lib/lib_page.php';
$autoloader = realpath(__DIR__."/../../vendor/autoload.php");
$settings = realpath(__DIR__."/../../settings.php");

require_once $autoloader;
require_once $settings;

function tempFilename()
{
    $tempdir =  sys_get_temp_dir();
    $filename = tempnam($tempdir, "xlsx_writer_");
    return $filename;
}
if(!isset($o_page)){
    $o_page = new page();
}

if(session_id() == '') {
    session_start();
}

$tmp_dir = '/tmp';
$tmp_file_name = sprintf("%s.xlsx", session_name());
$writer = new XLSXWriter();
$full_path = tempFilename();
#echo $full_path;

if($o_page->_user["AccessLevel"] < 1){
    exit();
}

mb_internal_encoding("cp1251");



$db = new \Maksoft\Core\MysqlDb($env);
$db->exec("SET NAMES cp1251");
$default_image="https://lh3.googleusercontent.com/-H7_QuNyFVMM/AAAAAAAAAAI/AAAAAAAAAAA/1095ekL4OEA/photo.jpg";
$gate = new \Maksoft\Gateway\Gateway($db);


$products = $gate->cart()->getAllProducts(1015);

$products_qty = count($products);

$products_without_price = array();
$products_without_picture = array();

function to_cyr($text){
    return iconv('utf8', 'cp1251', $text);
}


function build_url($n, $SiteID){
    return "/page.php?$n&SiteID=$SiteID";
}

function exist($object, $attribute){
    if(!$object->$attribute){
        return '';
    }
    return $object->$attribute;
}

    $header = array(
        'p_Id'           => 'integer',
        'cat_id'         => 'integer',
        'title'          => 'string',
        'content'        => 'string',
        'img'            => 'string',
        'parent_product' => 'string',
        'color'          => 'string',
        'size'           => 'string',
        'weght'          => 'string',
        'pcs_box'        => 'string',
        'origin'         => 'string',
        'tag'            => 'string',
        'available'      => 'string',
        'in_promotion'   => 'string',
        'promo_discount' => 'string',
        'promo_from_date' => 'string',
        'promo_end_date' => 'string'
    );
    $i = 1;
    $data = array();
    $writer->writeSheetHeader('Sheet1', $header );
    foreach($products as $item) {
        eval($item->PHPcode); 
        $product = json_decode($product);
        $row = array(
            $product->p_Id,
            $product->cat_id,
            $product->name,
            exist($product, "content"),
            implode(",", $product->img),
            exist($product, "parent_product"),
            implode(",", $product->color),
            exist($product, "size"),
            exist($product, "weight"),
            exist($product, "pcs_box"),
            exist($product, "origin"),
            implode(",", $product->group),
            exist($product->initial, "in_promotion"),
            exist($product->initial, "promo_discount"),
            exist($product->initial, "promo_from_date"),
            exist($product->initial, "promo_end_date"),
        );
        $writer->writeSheetRow('Sheet1', $row );
    }

    $writer->writeToFile($full_path);
$file = $full_path ;
$file_name = 'export-'.date('H-m-s-d-m-Y', time()).'.xlsx';
header('Content-Disposition: attachment; filename='.$file_name);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Length: ' . filesize($file));
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
readfile($file);


#include_once __DIR__.'/templates/list.php';
