<div class="container row">
    <div class="col-md-12">
        <h2> Total products inserted: <?=$products_qty?> </h2>
        <table class="table table-bordered">
            <thead>
                <th>#</th>
                <th>sku</th>
                <th>page_n</th>
                <th>name</th>
                <th>parent page</th>
                <th>price</th>
                <th>image</th>
                <th>link to product</th>
                <th>groups linked</th>
                <th>controlls</th>
            </thead>
            <tbody>
            <?php
                $i = 1;
                foreach($products as $item) {
                    include __DIR__.'/single.php';
                    $i++;
                } 

            ?>
            </tbody>
        </table>
        <p>Statistics: </p>
        products without picture: <?=count($products_without_picture);?><br>
        <table>
            <thead>
                <th>sku</th>
            </thead>
            <tbody>
                <?php
                foreach($products_without_picture as $product){
                    $sku = $product->p_Id;
                    echo '<tr><td>'.$sku.'</td></tr>';
                }
                ?>

            </tbody>
        </table>
        products without price: <?=count($products_without_price);?><br>
    </div>
</div>
