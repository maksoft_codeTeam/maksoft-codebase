<?php
namespace Maksoft\Admin\Product;
use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Forms\DivForm;
use Maksoft\Amount\Money;
use \Maksoft\Cart\Cart;


#ini_set("auto_detect_line_endings", TRUE);
class AddProduct extends DivForm
{
    private $gate;

    private $product;

    private $cart;

    public function __construct($gate, Cart $cart, $post_data=null)
    {
        $this->cart = $cart;
        $this->gate = $gate;
        $this->itID = new TextField(array(
            "name" => "itID",
            "hidden"=>True,
        ));
        $this->itID->add_validators(new \Maksoft\Form\Validators\Integerish());
        $this->qty = new TextField(array(
            "name" => "qty",
            "label" => "�-��:",
            "value" => 1,
        ));

        $this->qty->add_validators(new \Maksoft\Form\Validators\Integerish());

        $this->action = new TextField(array(
            "name" => "action",
            "value" => "add_product",
            "hidden" => True,
        ));

        $this->price = new TextField(array(
            "name" => "price",
            "value" => "0",
            "hidden" => True,
        ));  

        $this->submit = new SubmitButton(array(
            "class"=>"btn btn-default",
            "style" => "margin-top:3%;",
        ));
        parent::__construct($post_data);
    }

    public function validate_price()
    {
        if(!is_float($this->price->value)){
            if(!is_integer($this->price->value)){
                return False;
            }
        }
        $price = number_format($this->price->value, 3, '.', '');
        if($price > 0){
            return True;
        }
        return False;
    }

    public function action()
    {
        $price = number_format($this->price->value, 3, '.', '');
        $this->product->price_pcs = $this->qty->value;
        $this->product->price_value = $price;
        $this->product->price = $price;
        if(isset($_POST['api']) and $_POST['api'] === 'true' ){
            $product  = new \Maksoft\Cart\Item($this->product->itID, $this->qty->value, $price, $_POST['name'], '����', $_POST['itName']);
            $product->setMoq($this->qty->value);
            $product->setCategory($_POST['itName']);
            $this->cart->add($product);
            $_SESSION[\Maksoft\Cart\Cart::getName()] = $this->cart->save();
            return;
        }
        $product  = new \Maksoft\Cart\Item($this->product->itID, $this->product->price_pcs, $this->product->price_value, $this->product->itStr, $this->product->m, $this->product->itName);
        $product->setMoq($this->product->price_pcs);
        $product->setCategory($this->product->itName);
        $this->cart->add($product);
        $_SESSION[\Maksoft\Cart\Cart::getName()] = $this->cart->save();
    }

    private function format_price($price)
    {
        return number_format($price, 2);
    }

    protected function validate_itID()
    {
        $this->product = $this->gate->fak()->getProductFromSite($this->itID->value);
        return boolval($this->product);
    }
}
