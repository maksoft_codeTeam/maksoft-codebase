<?php
namespace Maksoft\Admin\Product;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Forms\DivForm;


#ini_set("auto_detect_line_endings", TRUE);
class Remove extends DivForm
{
    private $cart;

    private $gate;

    public function __construct(\Maksoft\Gateway\Gateway $gate, \Maksoft\Cart\Cart $cart, $post_data=null)
    {
        $this->cart = $cart;
        $this->gate = $gate;

        $this->remove = new HiddenField(
            array(
                "name" => "remove",
            )
        );

        $this->action = new HiddenField(
            array(
                "name" => "action",
                "value" => "remove",
            )
        );

        parent::__construct($post_data);
    }

    public function action()
    {
        $product = $this->gate->fak()->getProductFromSite($this->remove->value);
        $item = $this->cart->get_by_sku($product->itID);
        #$item = new \Maksoft\Cart\Item($product->itID, $product->price_pcs, $product->price_value, $product->itStr, $product->m, $product->itName);
        $this->cart->remove($item);
        $_SESSION[\Maksoft\Cart\Cart::getName()] = $this->cart->save();
    }
}
