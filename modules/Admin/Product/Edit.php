<?php
namespace Maksoft\Admin\Product;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Forms\DivForm;


class Edit extends DivForm
{
    private $cart;

    private $gate;

    public function __construct(\Maksoft\Gateway\Gateway $gate, \Maksoft\Cart\Cart $cart, $post_data=null)
    {
        $this->cart = $cart;
        $this->gate = $gate;

        $this->sku = new HiddenField(
            array(
                "name" => "sku",
            )
        );

        $this->action = new HiddenField(
            array(
                "name" => "action",
                "value" => "edit",
            )
        );

        $this->qty = new TextField(
            array(
                "name" => "qty",
            )
        );

        $this->qty->add_validators(new \Maksoft\Form\Validators\Integerish());

        parent::__construct($post_data);
    }

    public function action()
    {
        $item = $this->cart->get_by_sku($this->sku->value);
        $item->setQty($this->qty->value);
        $this->cart->edit($item);
        $_SESSION["cart"] = $this->cart->save();
    }
}
