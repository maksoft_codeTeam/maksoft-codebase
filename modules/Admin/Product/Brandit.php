<?php
namespace Maksoft\Admin\Product;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Forms\DivForm;


class Brandit extends DivForm
{
    private $cart;

    private $gate;

    public function __construct(\Maksoft\Gateway\Gateway $gate, \Maksoft\Cart\Cart $cart, $post_data=null)
    {
        $this->cart = $cart;
        $this->gate = $gate;
        $this->sku = new HiddenField(
            array(
                "name" => "sku",
            )
        );

        $this->action = new HiddenField(
            array(
                "name" => "action",
                "value" => "branding",
            )
        );

        parent::__construct($post_data);
    }

    public function action()
    {
        $item = $this->cart->get_by_sku($this->sku->value);
        if($item){
            $item->withBranding();
            $item->brand = True;
            $this->cart->edit($item);
            $_SESSION[\Maksoft\Cart\Cart::getName()] = $this->cart->save();
        }

    }
}
