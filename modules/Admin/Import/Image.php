<?php
namespace Maksoft\Admin\Import;


class Image 
{
    public $image, $n, $name, $gate, $site_id;
    protected $post=false;

    public function __construct($gate, $site_id)
    {
        $this->gate = $gate;
        $this->site_id = $site_id;
    }

    public function setPostImage($image)
    {
        $this->image = $image;
        $this->post = True;
        return $this;
    }

    public function setLocalImage($image)
    {
        $this->image = $image;
        $this->name = $image;
        return $this;
    }
    public function setPage($n)
    {
        $this->n = $n;
    }

    public function getName()
    {
        return $this->name;
    }

    protected function getImgDir()
    {
        $dirs = $this->gate->getSiteImagesDirs($this->site_id);
        $tmp = array_shift($dirs);
        foreach($dirs as $dir){
            if($dir["Name"] == "Продукти"){
                return $dir;
            }
        }
        return $tmp;
    }
    public function setName($name){
        $this->name = $name;
    }

    public function image_exist()
    {
        return is_file($this->getFullPath());
        if(is_file($this->getFullPath)){
            echo '<br>image exist</br>';
            return True;
        }
        echo '<br>image doesnt exist</br>';
        return False;
    }

    protected function getFullPath()
    {
        $img_dir = $this->getImgDir();
        return '/hosting/maksoft/maksoft/'.$img_dir['image_dir'].'/'.$this->getName();
    }

    protected function checkIfExistInDatabase()
    {
        $img_dir = $this->getImgDir();
        $res = $this->gate->getImage($img_dir['image_dir'], $this->getName());
        return (bool) $res->not_empty;
    }

    /**
     * Запазва файловете в публичната директория на сайта
     *
     * Извиква се след, като се валидира формата /метод is_valid()/
     *
     * @param array $site_dirs array with site image dirs 
     *
     * @param object $gate Object who handles all requests to mysql 
     *
     * @return string
     */
    public function save()
    {
        $ds = DIRECTORY_SEPARATOR;
        $img_dir = $this->getImgDir();
        $full_path = '/hosting/maksoft/maksoft/'.$img_dir['image_dir'].$ds.$this->getName();

        //if($this->image_exist($full_path)){
            //$imageID = $this->gate->replaceImage(
                //$this->getName(),
                //$img_dir['image_dir'].$ds.$this->getName(), 
                //$img_dir['ID']
            //);
            //$this->gate->setPageImage($this->n, $imageID);
            //return $imageID;
        //} elseif($this->post) {
        $f = move_uploaded_file($this->image['tmp_name'], $full_path);
        if($this->getName() == '2812.jpg'){
            var_dump($f);
            var_dumP($this->checkIfExistInDatabase());
        }

        if($f and $this->checkIfExistInDatabase()){
            return;
        }
        //}
        if($f){
            $imageID = $this->gate->replaceImage(
                $this->getName(),
                $img_dir['image_dir'].$ds.$this->getName(), 
                $img_dir['ID']
            );
            $this->gate->setPageImage($this->n, $imageID);
            return $imageID;
        }
    }
}
