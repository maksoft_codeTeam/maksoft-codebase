<?php
namespace Maksoft\Admin\Import;


class Product
{
    public $msg = array();
    protected $gate;
    protected $images = array();

    public function __construct($gate, $csv_data, $siteID)
    {
        $this->gate = $gate;
        $this->info = (object) $csv_data;
        $this->site_id = $siteID;
    }

    public function __toString()
    {
        return $this->info->title;
    }

    public function add_image($image)
    {
        $this->images[] = $image;
    }

    public function get_title()
    {
        return $this->info->title;
    }


    public function load()
    {
        return $this->gate->getPageByPHPcode($this->info->p_Id, $this->site_id);
    }

    protected function self_add()
    {
        $this->logger("try to add previously uploaded images");
        foreach(explode(",", $this->info->img) as $img){
            $this->logger("product image is $img");
            $i = new Image($this->gate, $this->site_id);
            $this->logger("set as local image");
            $i->setLocalImage($img);
            $this->images[] = $i;
        }
        return $this;
    }

    protected function compose_page()
    {
        $page = new \StdClass();
        $page->ParentPage = $this->parent_page();
        $page->Name = $this->_($this->info->title);
        $page->title = $page->Name;
        $page->slug = ''; 
        $page->textStr = $this->get_content();
        $page->url_rules = 0;
        $page->toplink = 0;
        $page->show_link=0;
        $page->show_link_order = "ASC";
        $page->show_link_cols = 1;
        $page->make_links = 1;
        $page->PageURL = "";
        $page->imageWidth = 0;
        $page->image_allign = 2;
        $page->SiteID = $this->site_id;
        $page->author = 1424;
        $page->SecLevel = 0;
        $page->PHPcode = '$product = \''.$this->toJSON($page).'\';';
        return $page;
    }

    protected function insert()
    {
        $page = $this->compose_page();
        $n = $this->gate->insertPage((array) $page); // Insert or update page
        $this->logger("insert page");
        $this->logger("link to product: ".$this->log($n));
        $page->ParentPage = $n;
        $this->logger("set parent page = $n");
        if(isset($this->info->tag)){
            foreach(explode(",", $this->info->tag) as $tag){
                $tag =  $this->_($tag);
                // if not exist -> create and get group ID 
                if(false == $group_id = $this->gate->getGroupByTitle($tag, $this->site_id)->group_id){
                    $group_id = $this->gate->createGroup($tag, $this->site_id);
                } 
                // MAP page to group ID
                $this->gate->insertPageGroup($n, $group_id);
            }
        }
        $this->logger("check is there images");

        $this->updateImages($page, $n);
    }

    public function updateImages($page, $n)
    {
        $image = array_pop($this->images);
        if(!is_object($image)){
            return;
        }
        $image->setPage($n);
        $image->save();
        $this->logger("save image");
        while($this->images){
            if(!is_object($image)){
                return;
            }
            $page->title = '';
            $n = $this->gate->insertPage((array) $page);
            $this->updateImage($image, $n);
        }
    }

    protected function updateImage($image, $n)
    {
        $image = array_shift($this->images);
        $image->setPage($n);
        $image->save();
    }

    public function save()
    {
        $this->logger("init");
        if(empty($this->images)){ // empty $_FILES
            $this->logger("there is no uploaded image");
            $this->self_add(); // Check in web/images/SITEDIR/products/ for image
            if(empty($this->images)){
                $this->logger("product doesnt have image ... exit");
                throw new \Exception("Product ".$this->get_sku()." doesnt have image"); // Cant upload products without image!
            }
        }
        $this->logger("check if product exist");
        if(!$product = $this->load()){ // no product
            $this->logger("product doesnt exist, try to insert");
            $this->insert(); // insert
            $this->logger("successfull insert");
            return;
        }

        $this->logger("product exist!");
        $this->logger("try to update");
        return $this->update($product); //update
    }

    function update($product)
    { 
        $p = $this->compose_page();
        $p->n = $product['n'];
        $this->logger("check if image is uploaded");
        $this->updateImages($page, $p->n);
        $this->gate->updatePage((array) $p);
        $this->logger("update page ".$p->n);
        $this->logger($this->log($p->n));
        //echo "update page" . PHP_EOL;
        if(isset($this->info->tag)){
            foreach(explode(",", $this->info->tag) as $tag){
                $tag =  $this->_($tag);
                // if not exist -> create and get group ID 
                if(false == $group_id = $this->gate->getGroupByTitle($tag, $this->site_id)->group_id){
                    $group_id = $this->gate->createGroup($tag, $this->site_id);
                } 
                // MAP page to group ID
                $u = $this->gate->insertPageGroup($p->n, $group_id);
                $this->logger($u);
                $this->logger("Page was successfully added to group: $tag");
            }
        }
        return True;
    }

    protected function checkIfImageIsAlreadyUploaded($byImageDir, $andImageName)
    {
        return $this->gate->getImage($byImageDir, $andImageName);
    }

    public function logger($message)
    {
        $this->msg[]=$message;
    }

    public function log($n){
        return sprintf(
            "<a href=\"/page.php?n=%s&SiteID=%s\">%s</a>",
            $n, 
            $this->site_id,
            $this->_($this->info->title)
        );
    }

    public function toJSON($extra)
    {
        $json = array();
        $json["p_Id"] = $this->info->p_Id;
        $json["cat_id"] = $this->info->cat_id;
        $json["color"] = explode(',', $this->info->color);
        $json["groups"] = explode(',',$this->info->tag);
        $json["img"] = explode(',', $this->info->img);
        $json["name"] = $this->info->title;
        $json["tag"] = $this->info->tag;
        $json["extra"] = $extra;
        $json['initial'] = $this->info;
        return json_encode($json);
    }

    public function parent_page()
    {
        if($response = $this->gate->getPageByPHPcode($this->info->parent_product, $this->site_id)){
            return $response["n"];
        }

        return $this->info->cat_id;
    }
    
    private function _($t)
    {
        return iconv('utf8', 'cp1251', trim($t));
    }

    private function check_price($n)
    {
        echo "<br>";
        echo str_pad("", 100, "=", STR_PAD_BOTH)."<br>";
        echo str_pad("�������: {$this->info->p_Id}", 100, "=", STR_PAD_BOTH)."<br>";
        echo str_pad("�������� �� ����", 100, "=", STR_PAD_BOTH)."<br>";

        if(!isset($this->info->price) && $this->info->price < 0){
            $input = '������ �� � �������� ��� � ��-����� �� 0';
            echo str_pad($input, 100, "=", STR_PAD_BOTH)."<br>";
            return False;
        }

        $this->info->moq = 1;
        if(!isset($this->info->moq) or !is_integer($this->info->moq)){
            $input = '�� � �������� ��������� ���������� �� ��������';

            echo str_pad($input, 100, "=", STR_PAD_BOTH)."<br>";
            return False;
        }
        $this->info->qty = 1;

        if(!isset($this->info->qty) or !is_integer($this->info->qty)){
            $input = '�� � �������� ���������� �� ��������';
            echo str_pad($input, 100, "=", STR_PAD_BOTH)."<br>";
            return False;
        }

        if($this->gate->get_price($n)){
            $input = '�������� ���� ��� �������� ����';
            echo str_pad($input, 100, "=", STR_PAD_BOTH)."<br>";
            $this->gate->page()->update_price(
                $n, 
                $this->price(),
                $this->info->p_Id,
                $this->_($this->info->title),
                $this->moq
            );
            return;

        }
        echo "<br>";
        $price1 = $this->info->price;
        $price2 = $this->price();
        echo str_pad("���� csv $price1", STR_PAD_BOTH)."<br>";
        echo str_pad("������������ ���� csv $price2", STR_PAD_BOTH)."<br>";
        $this->insert_price($n);
        $input = '������� �������� ����';
        echo str_pad($input, 100, "=", STR_PAD_BOTH)."<br>";
    }
    public function get_sku()
    {
        return $this->info->p_Id;
    }

    public function insert_price($n)
    {
        $this->gate->set_price(
            $n,
            $this->site_id,
            $this->_($this->info->title),
            $this->info->p_Id,
            $this->info->moq,
            $this->info->qty,
            $this->info->price
        );

    }

    protected function price()
    {
        bcscale(3);
        $qty = $this->info->qty * $this->info->moq;
        $price = bcmul($this->info->price, $qty);
        return $price;
    }

    public function get_content()
    {
        $tmp = array();

        $tmp[] = "��������� �����: ".$this->_($this->info->p_Id);
        if($this->info->content){
            $tmp[] = "��������: ".$this->_($this->info->content);
        }
        if($this->info->size){
            $tmp[] = sprintf("������: %s", $this->_($this->info->size));
        }

        $tmp[] = sprintf("%s: %s", "���� � ��������", $this->_($this->info->pcs_box));
        if($this->info->weight){
            $tmp[] = sprintf("�����: %s��.", $this->_($this->info->weight));
        }
        if($this->info->origin){
            $tmp[] = sprintf("��������: %s", $this->_($this->info->origin));
        }
        return implode("<br>", $tmp);
    }
}
