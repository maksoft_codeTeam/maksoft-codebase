<?php
use Maksoft\Form\Fields\FileInputField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Exceptions\ValidationError;
use Maksoft\Form\Forms\DivForm;


class CsvUploadForm extends DivForm
{
    public function __construct($form_data=null)
    {
        $this->file = new FileInputField(array(
            "name" => "csv",
            "id"   => "csv",
            "label"=>"Drop csv here",
            "class"=>'form-control',
            "onchange" =>  "uploadCsv(this)",
            "required"=>True ));
        parent::__construct($form_data);
    }
}
