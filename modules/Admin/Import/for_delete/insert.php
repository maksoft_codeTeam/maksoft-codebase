<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

if (isset($_SERVER["HTTP_ORIGIN"])) {
    $address = "http://".$_SERVER["SERVER_NAME"];
    if (strpos($address, $_SERVER["HTTP_ORIGIN"]) !== 0) {
        exit("CSRF protection in POST request: detected invalid Origin header: ".$_SERVER["HTTP_ORIGIN"]);
    }
}
$path = realpath(__DIR__.DIRECTORY_SEPARATOR.'../../vendor/autoload.php');
$base_dir = realpath(__DIR__.DIRECTORY_SEPARATOR.'../../');
require_once $path;
require_once $base_dir.DIRECTORY_SEPARATOR.'settings.php';
$db = new \Maksoft\Core\MysqlDb($env);
$gate = new \Maksoft\Core\Gate($db);

function uploadImage($sPath, $iPath, $img_dir, $n, $gate)
{
    $ds = DIRECTORY_SEPARATOR;
    if($_FILES['img']['size'] > 4812000){
        throw new Exception("Sorry, your image is too large");
    }
    if(file_exists($iPath.$_FILES['img']['name'])){
        throw new Exception ("Sorry, this image already exists! Change its name or delete it.");
    }
    if (is_uploaded_file($_FILES['img']['tmp_name'])) {
       echo "File ". $_FILES['userfile']['name'] ." uploaded successfully.\n";
    } else {
        throw new Exception(" Possible file upload attack: ".PHP_EOL."filename '".$_FILES['img']['tmp_name']."'");
    }
    $img_path = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.$sPath.DIRECTORY_SEPARATOR.$_FILES['img']['name'];
    echo $img_path;
    $f = move_uploaded_file($_FILES['img']['tmp_name'], $img_path);
    var_dump($f);
    if($f){
        $imageID = $gate->replaceImage(
            $_FILES['img']['name'],
            $img_path, 
            $img_dir);
        $gate->setPageImage($n, $imageID);
        $message['success'][] = "Успешно добавена снимка";
        echo 'Успешно добавена снимка';
        return $message;
    }
}

# echo "<pre><code>".print_r($_FILES)."</code></pre>";

header('Content-Type: application/json', 'charset=cp1251');


if($_SERVER['REQUEST_METHOD'] !== "POST"){
    throw new Exception("Request method must be POST");
}

if(isset($_POST['needle'])){
    echo json_encode($gate->searchProduct($_POST['needle']));
    exit();
}

if(isset($_POST['command']) && $_POST['command'] == 'insert_image') {
    //sub_dir = img_dir + $ds;
    $dirs = $gate->getSiteImagesDirs($_POST['SiteID']); // това връща всички директории на сайта
    var_dump($dirs);
    if(count($dirs) <= 0){
        throw new Exception('SiteDirectory Image Directory Exception');
    }
    $img_dir = $dirs[0];
    foreach($dirs as $dir){
        if($dir["Name"] == "Продукти"){
            $img_dir = $dir;
        }
    }
    $iId = $img_dir['ID']; //1128 Top Express
    $sDir =$img_dir['image_dir'].DIRECTORY_SEPARATOR;
    $iPath = '/hosting/maksoft/maksoft/'.$sDir;
    echo uploadImage($sDir, $iPath, $iId, 1, $gate);
    exit();
}



$p = $gate->insertPage($_POST);
$_POST['n'] = $p;
echo json_encode($_POST);
