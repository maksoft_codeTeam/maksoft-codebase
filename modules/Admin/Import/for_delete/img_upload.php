<?php
use Maksoft\Form\Fields\FileInputField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Exceptions\ValidationError;
use Maksoft\Form\Forms\DivForm;


class ImgUploadForm extends DivForm
{
    public function __construct($form_data=null)
    {
        $this->file = new FileInputField(array(
            "label"=>"DropImages Here",
            "name" => "img",
            "id"   => "img",
            "class"=>'form-control',
            "multiple" => True,
            ));
        $this->submit = new SubmitButton(array("class"=>"btn btn-default"));
        parent::__construct($form_data);
    }
}

