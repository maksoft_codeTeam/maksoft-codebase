<?php
define(DS, DIRECTORY_SEPARATOR);
$path = realpath(__DIR__.DIRECTORY_SEPARATOR.'../../vendor/autoload.php');
$base_dir = realpath(__DIR__.DS.'../../');
require_once $path;
require_once __DIR__. DS .'forms'.DS.'img_upload.php';
require_once __DIR__. DS .'forms'.DS.'csv_upload.php';
require_once $base_dir.DS.'settings.php';


$csv = new CsvUploadForm();
$csv->setId('csv_form');
$csv->setAction('modules/Admin/Import/csv_callback.php');
$img = new ImgUploadForm();
$img->setId('img_form');
echo $csv;
?>
    <style>
        .obj {
            width: 5vw;
        }
    </style>
    <div id="errors"> </div>
    <div id="img_drop" style="display:none;">
        <?php echo $img;?>
    </div>
    <div id="preview">
    </div>
    <link rel="stylesheet" type="text/css" href="modules/Admin/Import/assets/css/main.css" />
    <script src="https://www.promisejs.org/polyfills/promise-7.0.4.min.js"></script>
    <script src="modules/git/PapaParse/papaparse.min.js"></script>
    <script src="modules/Admin/Import/assets/js/import.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js">
    </script>

    <script id="exceptions-template" type="text/x-handlebars-template">
        <h2>Apparently there were several errors, please try again</h2> 
        {{msg}}
        <hr>
        Possible reasons:
        <ul>
        {{#each exceptions}}
            <li>{{message}}</li>
        {{/each}}
        </ul>

    </script>

    <script id="products-template" type="text/x-handlebars-template">
        <tr>
            <th># </th>
            <th>p_Id</th>
            <th>cat_id</th>
            <th>color</th>
            <th>content</th>
            <th>img</th>
            <th>origin</th>
            <th>parent_product</th>
            <th>pcs_box</th>
            <th>size</th>
            <th>tag</th>
            <th>title</th>
            <th>weight</th>
        </tr>
        {{#each products}}
        <tr id="{{p_Id}}" style="background-color:#ef5350;">
            <td>{{@index}}</td>
            <td>{{p_Id}} </td>
            <td>{{cat_id}} </td>
            <td>{{color}} </td>
            <td>{{content}} </td>
            <td>{{img}} </td>
            <td>{{origin}} </td>
            <td>{{parent_product}} </td>
            <td>{{pcs_box}} </td>
            <td>{{size}} </td>
            <td>{{tag}} </td>
            <td>{{title}} </td>
            <td>{{weight}} </td>
        </tr>
        {{/each}}
    </script>

<form id="insert" method="POST">
    <input type="file" id="csv-file" name="files"/>
    <input type="file" id="img-file" name="images" multiple/>
    <button type="submit"> ����! </button>
</form>
    <table class="rtable" id="match-table">

    </table>


<script>
var data=[];
var a = Product;
function main(data){
    draw_table(data);
    //csv_parse(data);
}

function draw_table(data){
    var template = Handlebars.compile($('#products-template').html());
    var table = template({ 'products': data.data });
    $('#match-table').html(table);
}

function csv_parse(data){
    var p = '';
    if(data.errors.length == 0){
        for(var j=0; j< data.data.length; j++){
            var p_data = data.data[j];
            var product = new Product(p_data, $, <?php echo $o_page->_site["SitesID"];?>);
            var p = new Product(p_data, $, <?php echo $o_page->_site["SitesID"];?>);
            
        }
        return true;
    }
    throw {name: "invalid data", msg: "invalid csv file"}
}

function generateData(csv, img){
    var match_tbl = {};
    var products_tbl = {};
    var product, d;
    var storage = {'match': {}, 'products': {}};
    for(var j=0; j< data.data.length; j++){
        product = data.data[j];
        product_images = product.img.split(',')
        for(var i=0; i < product_images.length; i++){
            storage['match'][product_images[i]] = product.p_Id;
        }
        storage['products'][product.p_Id] = product;
    }

    for(var j=0; j < img.length; j++){
        if(img[j].name in storage['match']){
            product_id = storage['match'][img[j].name]
            product = storage['products'][product_id];
            p = new Product(product, $, <?php echo $o_page->_site["SitesID"];?>);
            p.validate();
            p.add_image(img[j]);
            p.saveProduct();
            storage['products'][product_id] = product;
        }
    }

}

function handleFileSelect(evt) {
    var file = evt.target.files[0];
    Papa.parse(file, {
      header: true,
      skipEmptyLines: true,
      dynamicTyping: true,
      complete: function(results) {
        data = results;
        main(results);
      }
    });
}

jQuery(function($){
    $("#csv-file").change(handleFileSelect);
    $("form[id=insert]").submit(function(e) {
        e.preventDefault();
        var formData = new FormData($(this));
        var files = document.getElementById('img-file').files;
        var csv = document.getElementById('csv-file').files;
        generateData(csv, files);
    });
});
</script>
