<?php
$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "http://maksoft.net" or $http_origin == "http://www.maksoft.net" or $http_origin == "http://www.topexpress2000.com") {  header("Access-Control-Allow-Origin: $http_origin");
}else{
    throw new Exception("Access-Control-Allow-Origin Violation");
}
# echo "<pre><code>".print_r($_FILES)."</code></pre>";

if($_SERVER['REQUEST_METHOD'] !== "POST"){
    throw new Exception("Request method must be POST");
}
if(empty($_FILES['csv']['name'])){
    throw new Exception(sprintf("Filename cannot be empty! Got %s.", $_FILES['file']['name']));
}

if($_FILES['csv']['type'] !== "text/csv"){
    throw new Exception(sprintf("Expect file type to be text/csv! Got %s.", $_FILES['csv']['type']));
}
$f_path = $_FILES['csv']['tmp_name'];
$handle = fopen($f_path, 'r');
$data = fread($handle, filesize($f_path));
fclose($handle);

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Transfer-Encoding: binary"); 
echo $data;
