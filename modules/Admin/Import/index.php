<?php
error_reporting(E_ALL);
ini_set("display_errors", 1); 
mb_internal_encoding("cp1251");
if($o_page->_user['AccessLevel'] < 3){
    return;
}
define(DS, DIRECTORY_SEPARATOR);
$base_dir = realpath(__DIR__.DS.'../../');
require_once $base_dir.DS.'vendor'.DS.'autoload.php';
require_once $base_dir.DS.'settings.php';

$db = new Maksoft\Core\MysqlDb($env);
$db->exec("SET NAMES cp1251");
$gate = new Maksoft\Core\Gate($db);
$csv = new \Maksoft\Admin\Import\Forms\ValidationForm($_POST);
$price = new \Maksoft\Admin\Import\Forms\PriceUpdate($_POST);
$forms = array();
$forms[$csv->action->value] = $csv;
$forms[$price->action->value] = $price;


if($_SERVER["REQUEST_METHOD"] === "POST" and isset($_POST['action'])){
    if(!array_key_exists($_POST['action'], $forms)){
        ?>
    <div class="alert alert-danger" role="alert">
      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
      <span class="sr-only">Error:</span>
        ������! ������� � ������������!
    </div>
    <?php }
    try{
        $form = $forms[$_POST['action']];
        $form->is_valid();
        $form->save($gate, $o_page->_site['SitesID']);
    } catch( Exception $e) {
        echo $e->getMessage().PHP_EOL;
        echo $form->action->value.PHP_EOL;
        //var_dump($_FILES);
    ?>
    <div class="alert alert-danger" role="alert">
      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
      <span class="sr-only">Error:</span>
        <?php echo $e->getMessage(); ?>
    </div>
    <?php
    }
}
?>
<div class="container">
<div class="row">
  <div class="col-sm-6 col-md-6">
    <div class="thumbnail">
      <div class="caption">
        <h3>Insert/update products</h3>
        <img src="/web/images/upload/1015/csv_icon.png" width="10%" alt="...">
        <p/>
        <form id="csv_form" enctype="multipart/form-data" method="POST">
          <div>
            <label for="csv">Drop csv here</label>
            <input type="file" name="csv" id="csv" label="Drop csv here" class="form-control" required="">
          </div>
          <div>
            <label for="images">DropImages Here</label>
            <input type="file" label="DropImages Here" name="images[]" id="img" class="form-control" multiple>
          </div>
          <div>
            <input type="submit" class="btn btn-success" style="margin-top:2%;" name="submit">
            <?=$csv->action?>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-6">
    <div class="thumbnail">
      <div class="caption">
        <h3>Insert/update prices</h3>
        <img src="/web/images/upload/1015/csv_icon.png" width="10%" alt="...">
        <table class="table table-bordered">
            <tbody>
                <th>p_Id</th>
                <th>qty</th>
                <th>min_qty</th>
                <th>price</th>
            </tbody>
            <tr>
            <td colspan="4"  style="text-align:center"> <p class="bg-warning"><b>������� �� �������� � csv ����� ������� �� ����� ��������� � ����</b></p> </td>
            </tr>
        </table>
        <p/>
        <p>
            <form id="csv_form" enctype="multipart/form-data" method="POST">
            <?=$price->csv?>
            <?=$price->action?>
            <?=$price->submit?>
            </form>
        </p>
      </div>
    </div>
  </div>
</div>
</div>

<style>
.obj {
    width: 5vw;
}
</style>
<script src="modules/git/PapaParse/papaparse.min.js"></script>



