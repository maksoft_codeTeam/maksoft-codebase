<?php
namespace Maksoft\Admin\Import\Forms;
use Maksoft\Form\Fields\FileInputField;
use Maksoft\Form\Fields\HiddenField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Exceptions\ValidationError;
use Maksoft\Form\Forms\DivForm;


class PriceUpdate extends DivForm
{
    public function __construct($form_data=null)
    {
        $this->csv = new FileInputField(array(
            "name"     => "csv",
            "id"       => "csv",
            "label"    => "CSV ���� � ����",
            "class"    => "form-control",
            "required" => True ));

        $this->action = new HiddenField(array(
            "value" => "update_prices"
        ));

        $this->submit = new SubmitButton(array(
            "class"=>"btn btn-success",
            "style" => "margin-top:2%;"
        ));
        parent::__construct($form_data);
    }


    public function save($gate, $siteID)
    {
        $products = self::csv_to_assoc_array($_FILES[$this->csv->name]["tmp_name"]);
        foreach($products as $product){
            $page = $gate->getPageByPHPcode($product["p_Id"], $siteID);
            if(!$page){
                continue;
            }
            $page['price'] = str_replace(',', '.', $page['price']);
            if($page["price_value"] and $page["price_value"] != $product["price"]){
                $gate->update_price($page["n"], $product["price"], $product["p_Id"], $page["Name"]);
                echo "page ".$page["n"]. "was updated ".PHP_EOL;
                continue;
            }
            $id = $gate->set_price(
                        $page["n"],
                        $page["SiteID"],
                        $page["Name"],
                        $product["p_Id"],
                        $product["min_qty"],
                        $product["qty"],
                        $product["price"]
                    );
            echo "new price N: $id".PHP_EOL;
        }
    }

    public static function csv_to_assoc_array($csv_file)
    {
        $rows = array_map("str_getcsv", file($csv_file));
        $header = array_shift($rows);
        $csv = array();
        foreach ($rows as $row) {
          $csv[] = array_combine($header, $row);
        }
        return $csv;
    }
}
