<?php
namespace Maksoft\Admin\Import\Forms;
use Maksoft\Form\Fields\FileInputField;
use Maksoft\Form\Fields\HiddenField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Exceptions\ValidationError;
use Maksoft\Form\Forms\DivForm;
use Maksoft\Admin\Import\Product;
use Maksoft\Admin\Import\Image;


#ini_set("auto_detect_line_endings", TRUE);
class ValidationForm extends DivForm
{
    public $storage = array();
    public function __construct($form_data=null)
    {
        $this->csv = new FileInputField(array(
            "name"     => "csv",
            "id"       => "csv",
            "label"    => "Drop csv here",
            "class"    => "form-control",
            "required" => True ));

        $this->images = new FileInputField(array(
            "label"    => "DropImages Here",
            "name"     => "images[]",
            "id"       => "img",
            "class"    => "form-control",
            "multiple" => True,
            ));
        $this->images->add_validators(new \Maksoft\Form\Validators\NotBiggerThan(4194304)); // 4MB
        #$this->images->add_validators(new \Jokuf\Form\Validators\FileExtensionMatch("jpg", "png", "jpeg"));

        $this->action = new HiddenField(array(
            "value" => "insert_product"
        ));
        $this->submit = new SubmitButton(array("class"=>"btn btn-success"));
        parent::__construct($form_data);
    }


    public function parse_csv($gate, $siteID)
    {
        $csv_products = self::csv_to_assoc_array($_FILES["csv"]["tmp_name"]);
        foreach($csv_products as $product){
           $p = (object) $product;
           $images = explode(",", strtolower(trim($p->img))); 
           foreach($images as $image){
               if(!empty($image)){
                   $this->storage["match"][$image] = $p->p_Id;
               }
           }
           $this->storage["products"][$p->p_Id] = new Product($gate, $product, $siteID);

        }
        $files = $this->images->reArrayFiles($_FILES["images"]);
        foreach($files as $image){
            $i = (object) $image;
            $i->name = strtolower(trim($i->name));
            if(array_key_exists(trim($i->name), $this->storage["match"])){
                $p_id = $this->storage["match"][$i->name];
                $product = $this->storage["products"][$p_id];
                $im = new Image($gate, $siteID);
                $im->setName($i->name);
                $im->setPostImage($image);
                $product->add_image($im);
                $this->storage["products"][$p_id] = $product;
            }
        }
    }

    public function save($gate, $siteID)
    {
        $this->parse_csv($gate, $siteID);
        echo "<table border=\"1px\">
                <tr>
                    <th>status</th>
                    <th>id</th>
                    <th>msg</th>
                    <th>exception</th>
                </tr>";
        foreach($this->storage["products"] as $product){
            echo "<tr>";
            try{
                $product->save();
                echo "<td><font color=\"green\"><b>Success</b></font></td>";
                echo "<td>".$product->get_sku()."</td>";
                echo "<td>".implode('<br>', $product->msg)."<br></td>";
                echo "<td></td>";
            } catch(\Exception $e){
                echo "<td><font color=\"red\"><b>error</b></font></td>";
                echo "<td>".$product->get_sku()."</td>";
                echo "<td>".implode('<br>', $product->msg)."<br></td>";
                echo "<td>".$e->getMessage()."</td>";
            }
            echo "</tr>";
            //sleep(5);
        }
        echo "</table>";
    }


    public static function csv_to_assoc_array($csv_file)
    {
        $rows = array_map("str_getcsv", file($csv_file));
        $header = array_shift($rows);
        $csv = array();
        foreach ($rows as $row) {
          $csv[] = array_combine($header, $row);
        }
        return $csv;
    }
}
