<?php
namespace Maksoft\Core;


class MysqlDb extends \PDO
{
    public $db;
    private $dsn, $db_user, $db_pass;
    public function __construct($settings) {
        $options = array(
                \PDO::ATTR_ERRMODE    => \PDO::ERRMODE_SILENT,
                \PDO::ATTR_PERSISTENT => false,
                \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
        );
        try{
            $this->db_user = $settings["DATABASE"]["DB_USER"];
            $this->db_pass = $settings["DATABASE"]["DB_PASS"];
            parent::__construct("mysql:host=" . $settings["DATABASE"]["DB_HOST"] .
                                ";dbname=" . $settings["DATABASE"]["DB_NAME"],
                                $this->db_user,
                                $this->db_pass,
                                $options);
            $this->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
        }   catch (Exception $e){
            echo '*';
        }
    }
} 


?>
