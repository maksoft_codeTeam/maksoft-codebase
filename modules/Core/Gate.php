<?php
namespace Maksoft\Core;
use Maksoft\Core\MysqlDb;


class Gate{
    protected $db;

    public function  __construct(MysqlDb $db)
    {
        $this->db = $db;
    }  

    public function getPage($n)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM pages p
            LEFT JOIN images im on p.imageNo = im.imageID
            WHERE p.n=:n LIMIT 1");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $row;
    }

    public function getImage($byImageDir, $andImageName)
    {
        $sql = "SELECT *, COUNT(*) > 0 as not_empty FROM images WHERE image_dir=:image_dir AND imageName = :image_name";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":image_dir", $byImageDir);
        $stmt->bindValue(":image_name", $andImageName);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }


    public function getEditPage($n, $SiteID)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM pages p
            LEFT JOIN images im on p.imageNo = im.imageID
            WHERE p.n=:n
            AND p.SiteID=:SiteID
            LIMIT 1");
        $stmt->bindValue(":n", $n); $stmt->bindValue(":SiteID", $SiteID); $stmt->execute();
        $row = $stmt->fetch();
        return $row;
    }

    public function getPageByPHPcode($needle, $SiteID)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM pages p
            LEFT JOIN images im on p.imageNo = im.imageID
            LEFT JOIN prices ON prices.price_n = p.n
            WHERE  `PHPcode` LIKE  :code
            AND p.SiteID=:SiteID");
        $stmt->bindValue(":code", "%\"p_Id\":\"$needle\"%");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $row;
    }
    
    
    public function get_price($n)
    {
        $sql = "SELECT * FROM prices WHERE price_n=:price_n";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":price_n", $n);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getPriceByCode($code, $siteID)
    {
        $sql = "SELECT * FROM prices WHERE price_code = :code AND price_SiteID = :siteID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":code", $code);
        $stmt->bindValue(":siteID", $siteID);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function update_price($n, $price, $code, $description, $moq=1)
    {
        $sql = "
            UPDATE prices 
            SET
                price_pcs = :pcs,
                price_value = :price,
                price_code = :code,
                price_currency = :currency,
                price_description = :description,
                price_changed = :updated_at
            WHERE price_n = :n
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":pcs", $moq);
        $stmt->bindValue(":price", $price);
        $stmt->bindValue(":code", $code);
        $stmt->bindValue(":currency", 2);
        $stmt->bindValue(":description", $description);
        $stmt->bindValue(":updated_at", date('Y-m-d g:h:i', time()));
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function set_price($n, $site_id, $description, $code, $moq, $qty, $price, $currency=2)
    {
        $sql = "
            INSERT INTO `prices`(`price_n`, `price_SiteID`, `price_description`, `price_code`, `price_pcs`, `price_qty`, `price_value`, `price_currency`, `price_changed`)
            VALUES (
                :price_n,
                :price_SiteID,
                :description,
                :code,
                :price_pcs,
                :qty,
                :price_value,
                :currency,
                :updated_at
            )
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":price_n", $n);
        $stmt->bindValue(":price_SiteID", $site_id);
        $stmt->bindValue(":description", $description);
        $stmt->bindValue(":code", $code);
        $stmt->bindValue(":price_pcs", $moq);
        $stmt->bindValue(":qty", $qty);
        $stmt->bindValue(":currency", $currency);
        $stmt->bindValue(":price_value", $price);
        $stmt->bindValue(":updated_at", date('Y-m-d g:h:i', time()));
        $stmt->execute();
        return $this->db->lastInsertId();
    }
    public function login($username, $pass, $SiteID)
    {   
        $select = "
            SELECT users.*, sl.slID, SiteAccess.* FROM users 
            LEFT JOIN sl on sl.userID=users.ID
            JOIN SiteAccess ON SiteAccess.userID=users.ID
            WHERE username=:username
            AND pass=:pass 
            AND SiteAccess.SiteID=:SiteID
            LIMIT 1";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":username", $username);
        $stmt->bindValue(":pass", $pass);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }


    public function mail_queue() {
        $this->db_query("INSERT INTO mails_outbox
                         SET mail_to=:mail_to , mail_subject =:mail_subject,
                             mail_ctype = :mail_ctype, mail_reply_to = :mail_reply_to,
                             mail_text = :mail_text");
        $stmt->bindValue(":mail_to", $this->mail_to);
        $stmt->bindValue(":mail_subject", $this->mail_subject);
        $stmt->bindValue(":mail_ctype", $this->mail_ctype);
        $stmt->bindValue(":mail_reply_to", $this->mail_from);
        $stmt->bindValue(":mail_text", $this->mail_text);
        $stmt->execute();
   }


    public function facebook_register(array $kwargs)
    {
        $sql = "
            INSERT INTO social_accounts 
            (name, first_name, last_name, email, id )
            VALUES
            (:name, :first_name, :last_name, :email, :id)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":name", $kwargs['name']);
        $stmt->bindValue(":first_name", $kwargs['first_name']);
        $stmt->bindValue(":last_name", $kwargs['last_name']);
        $stmt->bindValue(":email", $kwargs['email']);
        $stmt->bindValue(":id", $kwargs['id']);
        try{
            $stmt->execute();
            return true;
        }catch (\PDOException $e){
            return false;
        }
    }


    public function get_reply_to($EMail) 
    {
        $stmt = $this->db->prepare(
            "SELECT sl.EMail, sl.FullName FROM Firmi, sl
             WHERE Firmi.EMail LIKE :EMail
             AND sl<255 AND sl.slID=Firmi.sl LIMIT 1 ");
        $stmt->bindValue(":EMail", $EMail);
        $stmt->execute();
        $reply_to = $stmt->fetch(\PDO::FETCH_OBJ);
        $reply_to_str = $this->CyrLat($reply_to->FullName)."<".$reply_to->EMail.">";
        return $reply_to_str;
    }

    public function getPageKeywords($n, $SiteID)
    {

        $keywords_query = "SELECT DISTINCT keywords, n, SiteID
            FROM keywords WHERE SiteID = :SiteID
            AND keywords != '' 
            AND n = :n
            ORDER BY enter_time ASC
            LIMIT 25";
        $stmt = $this->db->prepare($keywords_query);
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row =$stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getQuest($SiteID, $n)
    {
        $stmt = $this->db->prepare("
            SELECT *  FROM anketi 
            WHERE SiteID = :SiteID AND n = :n LIMIT 1");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function getPageSlugByN($pn, $SiteID)
    {
        $stmt = $this->db->prepare("SELECT * FROM page_slug WHERE n=:n AND SiteID=:sID");
        $stmt->bindValue(":n", $pn);
        $stmt->bindValue(":sID", $SiteID);
        if($stmt->execute()){
            $res = $stmt->fetch(\PDO::FETCH_OBJ);
            return $res->slug;
        }
        return false;
    }

    public function getPageNBySlug($slug, $SiteID)
    {
        $stmt = $this->db->prepare("SELECT * FROM page_slug WHERE slug=:slug AND SiteID=:Sid");
        $stmt->bindValue(":slug", $slug);
        $stmt->bindValue(":Sid", $SiteID);
        if($stmt->execute()){
            $res = $stmt->fetch(\PDO::FETCH_OBJ);
                return $res->n;
        }
        return $false;
    }

    public function getParentPageOfIndex($SiteID,  $condition,  $filter,  $sort_order)
    {
        $stmt = $this->db->prepare("
                SELECT * FROM pages p
                left join images im on p.imageNo = im.imageID
                WHERE p.SiteID = :SiteID :condition
                AND :filter ORDER by :sort_order");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":condition", $condition);
        $stmt->bindValue(":filter", $filter);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getParentPage( $n, $condition, $filter,  $sort_order, $user_access_level)
    {
        $query = "
            SELECT * FROM pages p 
            LEFT JOIN images im on p.imageNo = im.imageID
            WHERE p.ParentPage = :parent_page
            AND p.SecLevel <= :user_level
            :condition
            AND :filter ORDER by :sort_order";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":parent_page", $n);
        $stmt->bindValue(":condition", $condition);
        $stmt->bindValue(":filter", $filter);
        $stmt->bindValue(":user_level", $user_access_level);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }
    //return array of all page groups
    public function  getPageGroups( $n, $sort_order = "g.group_id ASC")
    {
        $stmt = $this->db->prepare("
            SELECT * FROM pages_to_groups ptg
            LEFT JOIN groups g on ptg.group_id = g.group_id
            WHERE ptg.n = :n ORDER by :sort_order");
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function createGroup($title, $site_id, $parent=0, $sort=0)
    {
        $sql = "INSERT INTO `groups`
                    (`parent_id`, `group_title`, `SiteID`, `group_date_added`, `sort_order`)
                VALUES
                    (:parent, :title, :site_id, :date, :sort)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":title", $title);
        $stmt->bindValue(":site_id", $site_id);
        $stmt->bindValue(":date", date("Y-m-d H:i:s", time()));
        $stmt->bindValue(":parent", $parent);
        $stmt->bindValue(":sort", $sort);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function getGroupByTitle($title, $site_id)
    {
        $sql = "
            SELECT * 
            FROM  `groups` 
            WHERE  `group_title` LIKE  :title
            AND  `SiteID` = :site_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":title", "%".$title."%");
        $stmt->bindValue(":site_id", $site_id);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }

    //return selected group info
    public function  get_pGroupInfo(integer $group_id, $return = NULL)
    {
        $stmt = $this->db->prepare("SELECT * FROM groups WHERE group_id = :group_id LIMIT 1");
        $stmt->bindValue(":group_id", $group_id);
        $stmt->execute();
        $gInfo = $stmt->fetch();
        if($return)
            return $gInfo[$return];
        else 
            return $gInfo;
    }

    //return all pages in selected group
    public function  getPageGroupContent($group_id, $SiteID, $userAccessLevel, $filter, $sort_order)
    {
        $sql = "SELECT * FROM pages p
                left join images im on p.imageNo = im.imageID, pages_to_groups ptg
                left join groups g on ptg.group_id = g.group_id
                WHERE p.n = ptg.n
                AND g.group_id = :group_id
                AND g.SiteID = :SiteID
                AND p.SecLevel <= :sec_lvl
                AND :filter
                ORDER by :sort_order";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":group_id", $group_id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":sec_lvl", $userAccessLevel);
        $stmt->bindValue(":filter", $filter);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getAlligns($allign)
    {
        $stmt = $this->db->prepare("SELECT * FROM alligns WHERE allignID = :allign LIMIT 1");
        $stmt->bindValue(":allign", $allign);
        $stmt->execute();
        $img_align = $stmt->fetch();
        return $img_align;
    }

    public function get_parentPage(integer $n)
    {
        $select = "SELECT ParentPage FROM pages WHERE n=:n";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function  getPagePrice($n, $condition="ASC")
    {
        $sort = array("ASC" => "ASC", "DESC" => "DESC");
        if(array_key_exists($condition, $sort)){
            $sort = $sort[$condition];
        }else{
            $sort = "ASC";
        }
        $page_prices_sql = "SELECT * FROM prices p
                            left join currencies c on p.price_currency = c.currency_id
                            WHERE p.price_n = :price_n
                            ORDER by p.price_value :condition";
        $stmt = $this->db->prepare("
            SELECT * FROM prices p
            LEFT JOIN currencies c on p.price_currency = c.currency_id
            WHERE p.price_n =:n ORDER by p.price_value ".$sort);
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function  getPageBanners($SiteID)
    {
        $query = "SELECT p.n, im.imageName, im.image_src, p.Name, p.title
                  FROM pages p
                  LEFT JOIN images im ON p.imageNo = im.imageID
                  WHERE p.image_allign = 21
                  AND im.image_src != ''
                  AND p.SiteID = :SiteID
                  ORDER by p.sort_n ASC";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getComment($comment_id, $filter)
    {
        $stmt = $this->db->prepare("SELECT * FROM comments WHERE comment_id = :comment_id AND :filter");
        $stmt->bindValue(':comment_id', $comment_id);
        $stmt->bindValue(':filter', $filter);
        $stmt->execute();
        $comment = $stmt->fetch();
        return $comment;
    }

    public function  getPageComments($n, $filter, $sort_order)
    {
        $stmt = $this->db->prepare("SELECT * FROM comments WHERE comment_n = :n
                                    AND comment_status>0
                                    AND :filter ORDER by :sort_order");
        $stmt->bindValue(':n', $n);
        $stmt->bindValue(':filter', $filter);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function  getPageOptions($n)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM page_options po
            LEFT JOIN options o on po.option_id = o.option_id WHERE po.n = :n");
        $stmt->bindValue(':n', $n);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function  getPageTemplates($templ, $SiteID)
    {
        $pt_sql = "
            SELECT * FROM pt_descriptions ptd
            WHERE ptd.SiteID = :SiteID 
            AND ptd.template_id = :templ";
        $stmt = $this->db->prepare($pt_sql);
        $stmt->bindValue(':SiteID', $SiteID);
        $stmt->bindValue(':templ', $templ);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function  getPageTemplate($n, $templ_id)
    {
        $pt_sql = "SELECT * FROM page_templates pt
                   left join pt_descriptions ptd on pt.pt_id = ptd.pt_id
                   WHERE pt.n = :n
                   AND ptd.template_id = :templ LIMIT 1";
        $stmt = $this->db->prepare($pt_sql);
        $stmt->bindValue(':n', $n);
        $stmt->bindValue(':templ', $templ_id);
        $stmt->execute();
        $pt = $stmt->fetch();
        return $pt;
    }

    public function moveToTrash($n)
    {
        $stmt = $this->db->preapre("UPDATE pages SET status = -1 WHERE n = :n LIMIT 1 ");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
    }

    public function deletePage($n)
    {
        $stmt = $this->db->prepare("DELETE FROM pages WHERE n = :n LIMIT 1");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        $stmt = $this->db->prepare("UPDATE pages SET an='0' WHERE pages.an=:n");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        //remove page from all groups
        $stmt = $this->db->prepare("DELETE FROM pages_to_groups WHERE n=:n");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
    }
    
    public function getPpagesNotEqualStatusZero($parent_page, $st)
    {
        $status = array(
            0 => ' != 0',
            1 => ' = 0'
        );
        $stmt = $this->db->prepare("SELECT * FROM pages WHERE ParentPage=:parent_page AND status :status");
        $stmt->bindValue(":parent_page", $parent_page);
        $stmt->bindValue(":status", $status[$st]);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }
    public function emptyPage($n)
    {
        $stmt = $this->db->prepare("
            UPDATE pages
            SET an = 0, tags = '', PHPvars = '',
            textStr = '', PHPcode = '', toplink = 0,
            show_link = 1, show_group_id = 0, show_link_order = 'sort_n ASC',
            show_link_cols = 1, make_links = 1, PageURL = '',
            imageNo = 0, imageWidth = '200', image_allign = 1,
            date_modified = 'now()'
            WHERE n = :n LIMIT 1 ");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
    }

    public function  restorePage($n)
    {
        $stmt = $this->db->prepare("UPDATE pages SET status = 1 WHERE n = :n LIMIT 1");
        $stmt->bindValue(":n", $n);
        return $stmt->execute();
    }

    public function  insertFile($file, $path, $dir_id)
    {
        $sql = "
            INSERT INTO images
            SET imageName = :file_title,
            image_src =  :image_src,
            image_dir = :image_dir";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":file_title", $file);
        $stmt->bindvalue(":image_src", $path);
        $stmt->bindValue(":image_dir", $dir_id);
        $stmt->execute();
        $imageID = $this->db->lastInsertId();
        return $imageID;
    }

    
    //return user access statistic or message

    public function addPreview(integer $n, $i=1)
    {
        if($i<0 or !is_numeric($i)){
            $i=1;
        }
        $stmt = $this->db->prepare("UPDATE pages SET preview=preview+:i WHERE n=':n");
        $stmt->bindValue(':i', (integer) $i);
        $stmt->bindValue(':n', $n);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function getMirrorPages($n, $an)
    {
        $stmt = $this->db->prepare("SELECT n, an FROM pages WHERE n=:n AND an=:an");
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":an", $an);
        $stmt->execute();
        $row = $stmt->fetchAll();
        return $row;
    }

    public function page_edit_log($no)
    {
        $stmt = $this->db->prepare("SELECT DISTINCT(uID) FROM page_edit_log WHERE n=:n "); 
        $stmt->bindValue(":n", $no);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getAllPages($SiteID)
    {
        $select = "
            SELECT 
            n, ParentPage, Name,title 
            FROM 
            pages 
            WHERE SiteID=:SiteID
            ORDER BY 
            ParentPage, Name ";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function updatePageEditLog($n, $user_id)
    {
        $insert = "INSERT INTO page_edit_log  SET n = :n, uID = :user_id"; 
        $stmt = $this->db->prepare($insert);
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->execute();
    }

    public function insertPage($kwargs)
    {
        $n = null;
        if(array_key_exists('n', $kwargs) && is_numeric($kwargs['n'])){
            $n = $kwargs['n'];
            echo 'update';
            return $this->updatePage($kwargs);
        }
        $insert = "
        INSERT INTO pages(
            an, ParentPage, Name, slug, title,
            tags, url_rules, PHPvars, textStr,
            PHPcode, toplink, show_link, show_group_id,
            show_link_order, show_link_cols, make_links,
            form, PageURL, imageWidth, image_allign,
            ankID, PrintVer, SiteID, date_added, author,
            preview, addtype, SecLevel, sort_n)
        VALUES (
            :an, :ParentPage, :Name, :slug,
            :title, :tags, :url_rules, :PHPvars,
            :textStr, :PHPcode, :toplink, :show_link,
            :show_group_id, :show_link_order, :show_link_cols,
            :make_links, :form, :PageURL, :imageWidth,
            :image_allign, :ankID, :PrintVer, :SiteID,
            :date_added, :author, :preview, :addtype,
            :SecLevel, :sort_n) ";
        $stmt = $this->db->prepare($insert);
        $stmt->bindValue(":an", 0);
        $stmt->bindValue(":ParentPage", $kwargs['ParentPage']);
        $stmt->bindValue(":Name", $kwargs['Name']);
        $stmt->bindValue(":slug", $kwargs['slug']);
        $stmt->bindValue(":title", $kwargs['title']);
        $stmt->bindValue(":tags", $kwargs['Name']);
        $stmt->bindValue(":url_rules", $kwargs['url_rules']);
        $stmt->bindValue(":PHPvars", '');
        $stmt->bindValue(":textStr", $kwargs['textStr']);
        $stmt->bindValue(":PHPcode", $kwargs['PHPcode']);
        $stmt->bindValue(":toplink", $kwargs['toplink']);
        $stmt->bindValue(":show_link", $kwargs['show_link']);
        $stmt->bindValue(":show_group_id", 0);
        $stmt->bindValue(":show_link_order", $kwargs['show_link_order']);
        $stmt->bindValue(":show_link_cols", $kwargs['show_link_cols']);
        $stmt->bindValue(":make_links", $kwargs['make_links']);
        $stmt->bindValue(":form", 0);
        $stmt->bindValue(":PageURL", $kwargs['PageURL']);
        $stmt->bindValue(":imageWidth", $kwargs['imageWidth']);
        $stmt->bindValue(":image_allign", $kwargs['image_allign']);
        $stmt->bindValue(":ankID", 0);
        $stmt->bindValue(":PrintVer", 1);
        $stmt->bindValue(":SiteID", $kwargs['SiteID']);
        $stmt->bindValue(":date_added", date("y-m-d h:i:s"));
        $stmt->bindValue(":author", $kwargs['author']);
        $stmt->bindValue(":preview", 0);
        $stmt->bindValue(":addtype", 0);
        $stmt->bindValue(":SecLevel", $kwargs['SecLevel']);
        $stmt->bindValue(":sort_n", 0);
        try{
            $stmt->execute();
            $n = $this->db->lastInsertId();
            $upd = "UPDATE pages SET sort_n=:n WHERE n=:n";
            $stmt = $this->db->prepare($upd);
            $stmt->bindValue(":n", $n);
            $stmt->execute();
            return $n;
        } catch (\PDOException $e){
            throw new \Exception("Invalid insert data! Please check inserted data [".$e->getMessage()."]");
        }
    }

    public function searchProduct($needle, $site_id){
        $sql = "SELECT * 
                FROM  `pages` 
                WHERE  `PHPcode` = :needle AND SiteID=:site_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":needle", $needle);
        $stmt->bindValue(":site_id", $site_id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function updatePage($data)
    {
        if(!array_key_exists('n', $data)){
            throw new \Exception("Invalid update data! Please check update data");
        }
        $update =  "UPDATE pages set ";
        foreach($data as $key=>$value){
            $update .= $key. ' = :'.$key.',';
        }
        $update = substr($update, 0, -1);
        $update .= " WHERE n = :n";
        $stmt = $this->db->prepare($update);
        foreach($data as $key=>$value){
            $stmt->bindValue(':'.$key, $value);
        }
        $stmt->execute();
    }

    public function setPageSortN($n)
    {
        $stmt = $this->db->prepare("UPDATE pages SET sort_n=:n WHERE n=:n");
        $stmt->bindValue(":n", $n);
        $stmt->execute();
    }

    public function setPageImage($n, $imageID)
    {
        $stmt = $this->db->prepare("UPDATE pages SET imageNo = :imageID WHERE n=:n");
        $stmt->execute(array(
                ":imageID" => $imageID,
                ":n"       => $n
                )
        );
    }

    public function replaceSlug($slug, $n, $SiteID)
    {
        $stmt = $this->db->prepare("REPLACE INTO page_slug SET slug = :slug,  n = :n, SiteID = :SiteID");  
        $stmt->execute(array(
            ":n" => $n,
            ":slug" => $slug,
            ":SiteID" => $SiteID
        ));
        return $this->db->lastInsertId();
    }

    public function replaceImage($img_name, $img_src, $img_dir)
    {
        $replace ="
            INSERT INTO images 
            (imageName, image_src, image_dir, image_date_added, image_date_modified)
            VALUES 
            (:img_name, :img_src, :img_dir, now(), now())";
        $stmt = $this->db->prepare($replace);
        $stmt->bindValue(":img_name", $img_name);
        $stmt->bindValue(":img_src", $img_src);
        $stmt->bindValue(":img_dir", $img_dir);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function deleteImage($imageID)
    {
        $stmt = $this->db->prepare("DELETE FROM images WHERE imageID=:imageID");
        $stmt->execute(array(":imageID"=>$imageID));
    }

    public function deletePageGroups($group_id)
    {
        $stmt = $this->db->prepare("DELETE FROM pages_to_groups WHERE id = :group_id");
        $stmt->execute(array(":group_id", $group_id));
        return $this->db->lastInsertId();
    }
    public function insertPageGroup($n,$group_id, $label="", $end_date="")
    {
        $check = "SELECT * FROM pages_to_groups WHERE n=:n and group_id=:group_id";
        $stmt = $this->db->prepare($check);
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":group_id", $group_id);
        $stmt->execute();
        if($row = $stmt->fetch(\PDO::FETCH_OBJ)){
            return false;
        }
        $insert = "
            INSERT INTO pages_to_groups
            SET n = :n, group_id = :group_id, label=:label, start_date = now(), end_date = :end_date";  
        $stmt = $this->db->prepare($insert);
        $stmt->execute(array(
            ":n" => $n,
            ":group_id" => $group_id, 
            ":label" => $label,
            ":end_date" => $end_date
        ));
        return $this->db->lastInsertId();
    }

    public function replacePageGroups($group_id, $label, $end_date)
    {
        $insert = "
            INSERT INTO pages_to_groups
            SET n = :n, group_id = :group_id, label=:label, start_date = now(), end_date = :end_date";  
        $stmt = $this->db->preapare($insert);
        $stmt->execute(array(
            ":n" => $n,
            ":group_id" => $group_id, 
            ":label" => $label,
            ":end_date" => $end_date
        ));
        return $this->db->lastInsertId();
    }

    public function replacePageTemplates($n, $pt_id)
    {
        $insert = "REPLACE INTO page_templates (n, pt_id) 
            VALUES (:n, :pt_id)";
        $stmt = $this->db->prepare($insert);
        $stmt->execute(array(
            ":n" => $n, 
            ":pt_id" => $pt_id
        ));
        return $this->db->lastInsertId();
    }

    public function deletePageTemplates($n)
    {
        $stmt = $this->db->prepare("DELETE FROM page_templates WHERE n = :n");
        $stmt->execute(array(":n"=>$n));
    }

    public function replacePagePrices($kwargs)
    {
        $price_query = "
            REPLACE INTO prices 
            SET 
                price_description= :descr, price_code=:code, price_n = :n, price_SiteID = :SiteID, 
                price_qty = :qty, price_pcs = :pcs, price_value = :value price_currency = :currency ";
        $stmt = $this->db->prepare($price_query);
        $stmt->execute($kwargs);
        return $this->db->lastInsertId();
    }

    public function getPageAuthor($user_id)
    {
        $stmt = $this->db->prepare("SELECT Name FROM users WHERE users.ID=:user_id");
        $stmt->bindValue(":user_id", $user_id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $res;
    }
        public function getData()
    {
        $stmt = $this->db->prepare(
            "SELECT * FROM Sites
             WHERE url LIKE :url
             ORDER BY SitesID ASC LIMIT 1");
        $stmt->bindValue(":url", "%".$_SERVER['SERVER_NAME']."%");
        $stmt->execute();
        $site = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $site;
    }

    public function getSessionUser()
    {
        $stmt = $this->db->prepare("SELECT data from session where id = :session_id");
        $stmt->bindValue(":session_id", session_id());
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function countSitePages($SiteID)
    {
        $stmt = $this->db->prepare("SELECT COUNT(n) FROM pages WHERE SiteID=:SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function get_site_permissions(integer $user_id, integer $SiteID)
    {
        $stmt = $this->db->prepare(
            "SELECT * FROM SiteAccess
             WHERE userID=:user
             AND SiteID=:SiteID  LIMIT 1");
        $stmt->bindValue(":user", $user_id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $permissions = $stmt->fetch();
        return $permissions;
    }

    public function isActiveCms($SiteID)
    {
        $stmt = $this->db->prepare(
            "SELECT  hostCMS.*,
             DATEDIFF(hostCMS.toDate, hostCMS.Date) as days_total,
             DATEDIFF(hostCMS.toDate, CURDATE()) as days_left
             FROM hostCMS
             WHERE SiteID=:siteID
             AND active='1'
             ORDER BY Date DESC LIMIT 1");
        $stmt->bindParam(":siteID", $SiteID, \PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $row;
    }

    public function getCurrentSiteArr(integer $SiteID)
    {
        $stmt = $this->db->prepare("SELECT * FROM Sites WHERE SitesID = :SiteID LIMIT 1");
        $stmt->bindParam(":SiteID", $SiteID, \PDO::PARAM_INT);
        $stmt->execute();
        $row=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $row;
    }

    public function getSiteConfiguration($SiteID)
    {
        $conf = array();
        $stmt = $this->db->prepare("SELECT * FROM configuration WHERE SiteID=:SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getSiteCss($cssID)
    {
        $stmt = $this->db->prepare("SELECT * FROM CSSs WHERE cssID = :css");
        $stmt->bindValue(":css", $cssID);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }
    //return site image directory path
    public function getSiteImageDir($id)
    {
      $stmt = $this->db->prepare("SELECT * FROM image_dirs WHERE ID = :id");
      $stmt->bindValue(":id", $id);
      $stmt->execute();
      $dir = $stmt->fetch(\PDO::FETCH_ASSOC);
      return $dir;
    }

    //return array of all image directories accessed by the selected site or a single dir
    public function getSiteImagesDirs($SiteId)
    {
        $stmt = $this->db->prepare("
                SELECT * FROM ImageAccess ia, image_dirs id
                WHERE ia.SiteID=:SiteID
                AND ia.imagesdir=id.ID
                ORDER by id.Name ASC");
        $stmt->bindValue(":SiteID", $SiteId);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    //return directory info
    public function get_sDirectoryInfo($id)
    {
      $stmt = $this->db->prepare("SELECT * FROM image_dirs WHERE ID=:id LIMIT 1");
      $stmt->bindValue(":id", $id);
      $stmt->execute();
      $dInfo = $stmt->fetch();
      return $dInfo;
    }

    //return number of files in selected directory
    public function getSiteImages($image_dir, $start=0, $stop=50)
    {
        $stmt = $this->db->prepare("SELECT * FROM images im 
                                    WHERE im.image_dir=:im_dir
                                    LIMIT :start , :stop");
            
        $stmt->bindValue(":im_dir", $image_dir);
        $stmt->bindParam(":start", $start, \PDO::PARAM_INT);
        $stmt->bindParam(":stop", $stop, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function get_page_image(integer $img_id)
    {
        $query = "SELECT * FROM pages p WHERE p.imageNo=:imageID  LIMIT 1";
        $this->db->prepare($query);
        $stmt->bindValue(":imageID", $img_id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    //return image info
    public function get_sImageInfo(integer $imageID)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM images im
            LEFT JOIN image_dirs imd on im.image_dir = imd.ID
            WHERE im.imageID=:image_id");
        $stmt->bindValue(":image_id", $imageID);
        $stmt->execute();
        $row = $stmt->fetch();
        return $row;
    }

    public function searchImages($SiteID, $needle)
    {
        $query_str = "
            SELECT ImageID, imageName, image_src, image_dir
            FROM images
            WHERE image_dir=:SiteID
            ORDER BY image_date_added DESC";
        $stmt = $this->db->prepare(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function searchPages($uLevel, $needle, $SiteID)
    {
        $search_query =
             "SELECT p.n, p.Name, p.title, p.SiteID, p.textStr, im.image_src
              FROM pages p  left join images im on p.imageNo = im.imageID
              WHERE p.SiteID=:SiteID
              AND p.SecLevel <= :u_level
              AND p.Name LIKE :Name)
              ORDER BY p.preview DESC, p.date_modified  DESC";
        $stmt = $this->db->prepare($search_query);
        $stmt->bindValue(":u_level", $uLevel);
        $stmt->bindValue(":Name", $needle);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    // get tags from all site pages
    // TODO yield
    public function getSiteTags($SiteID)
    {
        $tags_array = array();
        $stmt = $this->db->prepare(
            "SELECT * FROM pages
            WHERE SiteID = :SiteID
            AND tags !=''
            ORDER by RAND()");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }
    public function insert_stats($n, $SiteID, $user_ip, $entry)
    {
        $stmt = $this->db->prepare("INSERT INTO stats SET sesID=:sesID, n=:n, SiteID=:n, ip=:ip, entry=:entry");
        $stmt->bindValue(":sesID", session_id());
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":ip", $user_ip);
        $stmt->bindValue(":entry", $entry);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function insert_referer($stID, $referer_name, $url_name, $SiteID, $n)
    {
        $stmt = $this->db->prepare("
            INSERT INTO
                referers
            SET  
                stID=:stID ,
                referer=:referer, 
                url=:url,
                SiteID=:SiteID");
        $stmt->bindValue(":stID", $stID);
        $stmt->bindValue(":referer", $referer_name);
        $stmt->bindValue(":url", $url_name);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function getKeys($n, $time)
    {
        $select_query = "SELECT * FROM keywords WHERE n=:id AND enter_time=:enter_time ";
        $stmt = $this->db->prepare($select_query);
        $stmt->bindValue(':n', $n);
        $stmt->bindValue(':enter_time', $time);
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function setKeywords(
            $keywords,
            $search_engine,
            $search_engine_id,
            $n,
            $g_pos,
            $SiteID,
            $ip,
            $enter_time
        )
    {
        $stmt = $this->db->prepare(
              "INSERT INTO keywords
              SET
              keywords=:keywords,
              search_engine=:search_engine,
              search_engine_id=:search_engine_id,
              g_pos=:g_pos,
              n=:n,
              SiteID=:SiteID,
              ip=:ip,
              enter_time=:enter_time");
        $stmt->bindValue(":keywords", $keywords);
        $stmt->bindValue(":search_engine", $search_engine);
        $stmt->bindValue(":search_engine_id", $search_engine_id);
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":g_pos", $g_pos);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":ip", $ip);
        $stmt->bindValue(":enter_time", $enter_time);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function updateSiteKeywords($keywords, $ip)
    {
        $stmt = $this->db->prepare("
            UPDATE keywords SET repeated='1'
            WHERE keywords=:keywords AND ip<>:ip  LIMIT 5"
        );
        $stmt->bindValue(":keywords", $keywods);
        $stmt->bindValue(":ip", $ip);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function selectSEkeywords($keywords)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM keywords
            WHERE keywords=:keywords
            AND search_engine_id>0
            ");
        $stmt->bindValue(":keywords", $keywords);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        return $rows;
    }

    public function update_keywords_visits($visits, $SiteID, $keywords)
    {
        $stmt = $this->db->prepare("
            UPDATE keywords SET visits=:visits
            WHERE SiteID=:SiteID AND keywords=:keywords
            ORDER BY keysID DESC LIMIT 2  ");
        $stmt->bindValue(':visits', $key_count);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":keywords", $keywords);
        $stmt->execute();
    }


    //return array of all site requests
    //TODO split to 4 methods
    //readed
    //completed
    //deleted
    //junk
    function get_sRequests($SiteID, $r_status = 0, $filter = "1")
    {
        // $filter = "f.Data>DATE_SUB(CURDATE(),INTERVAL 12 HOUR)";
        if(!isset($SiteID)) $SiteID = $this->SiteID;
        if($r_status < 0) {
            $query = "SELECT * FROM forms f
                      LEFT JOIN pages p ON f.n=p.n
                      WHERE f.SiteID =:SiteID
                      AND " . $filter . "
                      ORDER by f.Data DESC LIMIT 0, 100";
            $stmt = $this->db->prepare($query);
            $stmt->bindValue(":SiteID", $this->SiteID);
        }else {
            $query = "SELECT * FROM forms f
                      LEFT JOIN pages p ON f.n=p.n
                      WHERE f.SiteID =:SiteID
                      AND f.ftype =:r_status
                      AND " . $filter . "
                      ORDER by f.Data DESC LIMIT 0, 100";
            $stmt = $this->db->prepare($query);
            $stmt->bindValue(":SiteID", $this->SiteID);
            $stmt->bindValue(":r_status", $r_status);
        }
        $stmt->execute();
        while($request = $stmt->fetch(\PDO::FETCH_ASSOC))
            $sRequests[] = $request;
        return $sRequests;
    }

    //return site cms hosting info array
    public function getCmsHosting($SiteID, $filter=1)
    {
        $query = "SELECT * FROM hostCMS WHERE SiteID = :SiteID AND :filter ORDER by Date DESC";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":SiteID", $this->SiteID);
        $stmt->bindValue(":filter", $filter);
        $stmt->execute();
        while($row = $stmt->fetch()){
            $sCMSHosting[] = $row;
        }
        return $sCMSHosting;
    }

    //return site versions
    public function getSiteVersions($SiteID)
    {
        $stmt = $this->db->prepare("SELECT * FROM versions v WHERE v.SiteID =:SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;

    }
   //return site language: BG=1, EN=2, RU=3, FR=4
    public function getSiteLanguage($sLanguage)
    {
        $stmt = $this->db->prepare("SELECT * FROM languages l WHERE l.language_id = :sLanguage LIMIT 1");
        $stmt->bindValue(":sLanguage", $sLanguage);
        $stmt->execute();
        $language = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $language;
    }

    //return aaray of all site groups
    public function getSiteGroups($SiteID, $filter = "1")
    {
        $stmt = $this->db->prepare("SELECT * FROM groups WHERE SiteID =:SiteID AND :filter");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":filter", $filter);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getPagesToGroups($group_id)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM pages_to_groups ptg
            WHERE ptg.group_id = :group_id"); //TODO outside while
        $stmt->bindValue(":group_id", $group_id);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    //return site options
    public function getSiteOptions($SiteID)
    {
        $sOptions = array();
        $stmt = $this->db->prepare("SELECT * FROM options o WHERE o.SiteID = :SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    //return all comments in site
    public function getSiteComments($SiteID)
    {
        $stmt = $this->db->prepare("SELECT * FROM pages p
                                    RIGHT JOIN comments c on c.comment_n = p.n
                                    WHERE p.SiteID =:SiteID
                                    ORDER by c.comment_date DESC");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

/* ###################### ADMINISTRATIVE FUNCTIONS: add / update / delete etc. ###################### */

//add or update confuguration value
    public function getConfType($conf_key, $conf_value, $SiteID, $templ_id)
    {
        $search_query = "SELECT * FROM configuration_types WHERE conf_type_key =:conf_key LIMIT 1";
        $stmt = $this->db->prepare($search_query);
        $stmt->bindValue(":conf_key", $conf_key);
        $stmt->execute();
        $configuration = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $configuration;
    }

    public function updateConfigValue($conf_key, $conv_value, $SiteID, $templ_id)
    {
            $query = "UPDATE configuration
                      SET conf_value = :conf_value, last_modified = now(), templ_id =:templ_id
                      WHERE conf_key = :conf_key
                      AND SiteID = :SiteID
                      AND templ_id=:templ_id";
            $stmt = $this->db->prepare($search_query);
            $stmt->bindValue(":conf_value", $conf_value);
            $stmt->bindValue(":templ_id", $templ_id);
            $stmt->bindValue(":conf_key", $conf_key);
            $stmt->bindValue(":SiteID", $SiteID);
            $stmt->execute();
            return $this->db->lastInsertId();
    }

    public function insertConfigValue(
        $conf_key, $conf_value, $conf_description, $conf_group_id, $SiteID, $templ_id)
    {
        $query = "INSERT INTO configuration
                  SET conf_title = '',
                  conf_key = :conf_key,
                  conf_value = :conf_value,
                  conf_description = :conf_description,
                  conf_group_id = :conf_group_id,
                  sort_order = 0,
                  last_modified = 'now()',
                  date_added = 'now()',
                  SiteID = :SiteID,
                  templ_id = :templ_id";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":conf_key", $conf_key);
        $stmt->bindValue(":conf_value", $conf_value);
        $stmt->bindValue(":conf_description", $configuration['conf_type_description']);
        $stmt->bindValue(":conf_group_id", $configuration['conf_group_id']);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":templ_id", $templ_id);
        $stmt->execute();
        return $this->db->lastInsertId();
    }
//delete all configurations filtered by templ_id and SiteID
    public function deleteConfigurations($templ_id, $SiteID)
    {
        $query = "DELETE FROM configuration WHERE SiteID=:SiteID AND templ_id = :templ_id";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":templ_id", $templ_id);
        $stmt->execute();
    }

    public function setStartPage()
    {
        $query="INSERT INTO pages SET ParentPage='0', Name='������' ";
        $this->db->exec($query);
        $StartPage = $this->db->lastInsertId();
        return $StartPage;
    }

    public function setImageDir($site_name)
    {
        $query="INSERT INTO image_dirs SET Name=:site_name ";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":site_name", $NewSiteName);
        $stmt->execute();
        $imagesdir = $this->db->lastInsertId();
        return $imagesdir;
    }

    public function siteCreate(
        $site_name, $start_page_id, $img_dir_id, $rub, $templ_id, $css, $language
    )
    {
        $query = "
             INSERT INTO Sites
             SET
             Name=:site_name,
             StartPage=:start_page,
             imagesdir=:images_dir,
             Rub=:rub,
             Template=:Template,
             css=:css,
             language_id=:language ";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":site_name", $site_name);
        $stmt->bindValue(":start_page", $start_page_id);
        $stmt->bindValue(":images_dir", $img_dir_id);
        $stmt->bindValue(":rub", $rub);
        $stmt->bindValue(":Template", $templ_id);
        $stmt->bindValue(":css", $css);
        $stmt->bindValue(":language", $language);
        $stmt->execute();
        $SiteID = $this->db->lastInsertId();
        return $SiteID;
    }

    public function updateImgDir($SiteID, $img_dir_id)
    {
        $settings = parse_ini_file(dirname( realpath( __DIR__ ) ).'/../../../settings.ini', true);
        $image_dir_path = $settings['PATHS']['img_dir'].DIRECTORY_SEPARATOR.$SiteID;
        $query="UPDATE image_dirs SET image_dir =:image_dir WHERE ID=:images_dir ";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":image_dir", $image_dir_path);
        $stmt->bindValue(":images_dir", $img_dir_id);
        $stmt->execute();
    }

    public function updateStartPage($SiteID, $start_page)
    {
        $query="UPDATE pages SET SiteID=:SiteID WHERE n=:start_page";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":start_page", $start_page);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function setSiteAccess($SiteID, $user_id)
    {
        $query="INSERT INTO SiteAccess SET SiteID=:SiteID, userID=:userID";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":userID", $user_id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function setImgAccess($SiteID, $img_dir_id)
    {
        $query="INSERT INTO ImageAccess SET SiteID=, imagesdir=:imagesdir ";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":imagesdir", $img_dir_id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function setInitPage($SiteID, $user_id, $init_page=154)
    {
        //update users
        $query="UPDATE users SET InitPage = :init_page, InitSiteID =:SiteID WHERE ID = :user_id ";
        //echo("$query");
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":init_page", $init_page);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        return $SiteID;
    }

    public function getCache($cvID)
    {
        $stmt=$this->db->prepare("SELECT * FROM cache_vars WHERE cvID=:cvID LIMIT 1 ");
        $stmt->bindValue(":cvID", $cvID);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }

    public function cachePut($val, $cvID){
        $stmt = $this->db->prepare("
            UPDATE cache_vars
            SET val =:val, gen_time = :gen_time
            WHERE cvID = :cvID");
        $stmt->bindValue(":val", serialize($val));
        $stmt->bindValue(":gen_time", microtime(true));
        $stmt->bindValue(":cvID", $cvID);
        $stmt->execute();
    }

    public function getSiteDataByUrl()
    {
        $stmt = $this->db->prepare(
            "SELECT *  FROM `Sites`
            WHERE `url` LIKE :url OR `primary_url` LIKE :url");
        $stmt->bindValue(":url", "%".$_SERVER['HTTP_HOST']."%");
        $stmt->execute();
        $row = $stmt->fetch();
        return $row;
    }
    
    public function getSiteAddons($SiteID)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM PageURLs 
            WHERE (SiteID=0 OR SiteID=:SiteID)
            ORDER BY NAME ASC");
        $stmt->execute(array("SiteID"=>$SiteID));
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getAdminMessages($msg_group){
        $stmt = $this->db->prepare("
            SELECT * FROM default_messages 
            LEFT JOIN messages ON messages.default_message_id = default_messages.id
            WHERE name = :name");
        $stmt->bindValue(":name", $msg_group);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $res;
        }
        return $tmp;
    }
    public function createdPagesByMonth($SiteID, $interval)
    {
        $stmt = $this->db->prepare("
            SELECT date_added , COUNT( n ) 
            FROM pages
            WHERE date_added >= NOW( ) - INTERVAL :interval MONTH
            AND SiteID = :SiteID
            GROUP BY MONTH( date_added )");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindParam(":interval", $interval);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    
    public function getOnlineUsers()
    {
        $stmt = $this->db->prepare("
            SELECT id 
            FROM  `session` 
            WHERE modified + lifetime < NOW( )
            AND name=:domain");
        $stmt->bindValue(":domain", $_SERVER['HTTP_HOST']);
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function siteViewsByMonth($SiteID, $interval)
    {
        $select = "
            SELECT  viewTime , COUNT( n ) 
            FROM stats
            WHERE viewTime >= NOW( ) - INTERVAL :interval MONTH 
            AND SiteID =:SiteID
            GROUP BY MONTH( viewTime ) 
            ORDER BY viewTime ASC";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":interval", $interval);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getSiteViewsByInterval($SiteID, $date1, $date2)
    {
        $select = "
            SELECT  viewTime , COUNT( n ) 
            FROM stats
            WHERE viewTime >= NOW( ) - INTERVAL :interval MONTH 
            AND SiteID =:SiteID
            GROUP BY MONTH( viewTime ) 
            ORDER BY viewTime ASC";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":interval", $interval);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch()){
            $tmp[] = array($row[0], $row[1]);
        }
        return $tmp;
    }
    
    public function getTopKeywords($SiteID, $interval, $limit)
    {
        $select = "
            SELECT * 
            FROM  `keywords` 
            WHERE  `SiteID` =:SiteID
            AND g_pos >0
            AND enter_time > NOW( ) - INTERVAL :interval MONTH 
            ORDER BY g_pos ASC 
            LIMIT :limit";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":interval", $interval);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    } 

    public function getKeywordPositions($SiteID, $keyword)
    {
        $select = "
            SELECT *  FROM `keywords`
            WHERE `keywords`
            LIKE :keyword
            AND SiteID=:SiteID
            ORDER BY g_pos ASC";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":keyword", $keyword);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getLastEditedPages($user_id, $SiteID)
    {
        $select = "
            SELECT DISTINCT pl.n, pl.pe_time, pages.Name, Sites.primary_url
            FROM  `page_edit_log` pl
            JOIN pages ON pages.n = pl.n
            LEFT JOIN Sites ON Sites.SitesID = pages.SiteID
            WHERE  `uID` = :user_id
            WHERE pages.SiteID=:SiteID
            GROUP BY pl.n
            ORDER BY pe_time DESC 
            LIMIT 0 , 20";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getSiteVisits($SiteID)
    {
        $stmt = $this->db->prepare("SELECT count(*) FROM `stats` WHERE SiteID=:SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function getNewestPages($SiteID, $limit)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM pages
            WHERE SiteID=:SiteID 
            ORDER BY date_added DESC
            LIMIT :limit");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getCountActivePages($SiteID)
    {
        $stmt = $this->db->prepare("
            SELECT COUNT(n) FROM pages
            WHERE SiteID=:SiteID 
            AND status=1 
            AND SecLevel=0");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }
    
    public function getRegisteredUsersCount($SiteID)
    {
        $stmt = $this->db->prepare("
            SELECT COUNT( userID ) 
            FROM  `SiteAccess` 
            WHERE SiteID=:SiteID");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $res = $stmt->fetch();
        return $res;
    }

    public function getLatestPages($SiteID)
    {
        $select = "
            SELECT *  FROM `pages` 
            WHERE `SiteID` = :SiteID
            ORDER BY date_modified DESC LIMIT 5";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getTopReferers($SiteID, $limit)
    {
        $select = "
            SELECT ref_count, referer
            FROM (

                SELECT DISTINCT ref_count, referer
                FROM referers
                WHERE SiteID =:SiteID
            ) AS c1
            GROUP BY referer
            ORDER BY ref_count DESC 
            LIMIT :limit";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }
    
    public function getTopViewedPages($SiteID, $limit)
    {
        $select = "
            SELECT * 
            FROM  `pages` 
            WHERE SiteID =:SiteID
            ORDER BY preview DESC 
            LIMIT :limit";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }
    public function get_tasks($to_user, $start_date="", $filter = 1)
    {
        if($start_date == ""){
            $stmt=$this->db->prepare("
                SELECT * FROM tasks WHERE
                to_user=:to_user
                AND completed='0'
                AND start_date<NOW()
                AND :filter 
                ORDER BY priority ASC,  start_date ASC  ");
            $stmt->bindValue(":to_user", $to_user);
            $stmt->bindValue(":filter", $filter);
        } else {
            $stmt=$this->db->prepare("
                SELECT * FROM tasks 
                WHERE to_user=:to_user 
                AND completed='0'
                AND start_date>=:start_date 
                AND :filter ORDER BY priority ASC,  start_date ASC ");
            $stmt->bindValue(":start_date", $start_date);
        }
        $stmt->bindValue(":to_user", $to_user);
        $stmt->bindValue(":filter", $filter);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getUserTasks($user_id)
    {
        $stmt=$this->db->prepare("
            SELECT * FROM tasks WHERE
            to_user=:to_user
            AND completed='0'
            AND start_date<NOW()
            ORDER BY priority ASC,  start_date ASC  ");
        $stmt->bindValue(":to_user", $user_id);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    public function getUpcomingTasks($user_id)
    {
        $stmt=$this->db->prepare("
                SELECT * FROM tasks 
                WHERE to_user=:to_user 
                AND completed='0'
                AND start_date>=NOW() 
                ORDER BY priority ASC,  start_date ASC ");
        $stmt->bindValue(":to_user", $user_id);
        $stmt->execute();

        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }

    //return e specific user task
    public function get_task($taskID)
    {
        $stmt=$this->db->prepare("SELECT * FROM tasks WHERE taskID=:taskID ");
        $stmt->bindValue(":taskID", $taskID);
        $stmt->execute();
        $row=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $row;
    }

    public function complete_task($taskID)
    {
        $row = $this->get_task($taskID);
        if($row->repeat_in>0) {
            $start_date = $this->add_date($row->start_date, $row->repeat_in, 0, 0);
            $stmt = $this->db->prepare("
                UPDATE tasks 
                SET start_date=:start_date
                WHERE taskID=:taskID ");
            $stmt->bindValue(":start_date", $start_date);
        } else {
            $stmt = $this->db->prepare("
                UPDATE tasks 
                SET completed ='1'
                WHERE taskID=:taskID ");
        }
        $stmt->bindValue(":taskID", $taskID);
        $stmt->execute();
    }

    //return all user tasks assigned to other users
    public function getAssignedTasks($from_user)
    {
        $stmt=$this->db->prepare("
            SELECT * FROM tasks
            WHERE from_user=:from_user
            AND from_user<>to_user
            AND completed='0'
            AND start_date<NOW()
            ORDER BY start_date DESC, priority ASC ");
        $stmt->bindValue(":from_user", $from_user);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        return $tmp;
    }
    
    //insert a new task assigned by the selected user
    public function write_task($taskID, $task_set)
    {
        if ($taskID>0) {
            $date_modified = date_time("Y-m-d H:i:s");
            $stmt = $this->db->prepare("
                UPDATE tasks
                SET :task_set ,
                date_modified= :date_modified
                WHERE taskID=:taskID");
            $stmt->bindValue(":taskID", $taskID);
        } else { //new task
            $date_added = date_time("Y-m-d H:i:s");
            $stmt = $this->db->prepare("
                INSERT INTO 
                tasks SET :task_set ,
                date_added=:date_modified");
        }
        $stmt->bindValue(":task_set", $task_set);
        $stmt->bindValue(":date_modified", $date_modified);
        $stmt->execute();
    }

    public function get_uSites($user_id, $filter=1)
    {
        $stmt = $this->db->prepare("
            SELECT * FROM SiteAccess sa
            RIGHT JOIN Sites s on sa.SiteID = s.SitesID
            WHERE sa.userID = :user_id AND :filter
            ORDER by s.Name ASC");
        $stmt->bindValue(":user_id", $user_id);
        $stmt->bindValue(":filter", $filter);
        $stmt->execute();
        $data = array();
        while($row = $stmt->fetch(\PDO::FETCH_OBJ)){
            $data[] = $row;
        }
        return $data;
    }

    public function editUserName($id, $name)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET Name=:name
            WHERE ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":name", $name);
        $stmt->execute();
    }

    public function editUserEmail($id, $email)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET EMail=:email
            WHERE ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":email", $email);
        $stmt->execute();
    }

    public function editUserPass($id, $pass)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET pass=:pass
            WHERE ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":pass", $pass);
        $stmt->execute();
    }

    public function editUserPhone($id, $phone)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET Phone=:phone
            WHERE ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":phone", $phone);
        $stmt->execute();
    }

    public function updateProfileInfo($id, $name, $email, $phone)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET
                EMail=:email,
                Name=:name,
                Phone=:phone
            WHERE 
                ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":name", $name);
        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":phone", $phone);
        $stmt->execute();
    }

    public function editUserPassword($id, $password)
    {
        $stmt = $this->db->prepare("
            UPDATE users 
            SET password=:password
            WHERE ID=:id"
        );  
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":password", $password);
        $stmt->execute();
    }

    public function getFirm($id)
    {
        $stmt = $this->db->prepare("SELECT * 
        FROM  `Firmi` WHERE ID=:id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $r = $stmt->fetch(\PDO::FETCH_OBJ);
        return $r;
    }

    public function getUsersByParentUserId($id)
    {
        $stmt = $this->db->prepare("
            SELECT * 
            FROM  `users` 
            WHERE  `ParentUserID` =:id ");
        $stmt->bindValue(":id", $id);
        $stmt->execute();

        $tmp = array();
        while($r = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $r;
        }

        return $tmp;
    }

    public function newUser($firm_id, $parent_user, $name,
                            $email, $phone, $username,
                            $pass, $initPage,
                            $siteID, $access_lvl)
    {
        $sql = "
            INSERT INTO `users` ( 
                `FirmID`, 
                `ParentUserID`,
                `Name`,
                `EMail`,
                `Phone`,
                `username`,
                `pass`,
                `InitPage`,
                `InitSiteID`,
                `AccessLevel`) 
            VALUES (
                :firm_id,
                :parent_user,
                :name,
                :email,
                :phone,
                :username,
                :pass,
                11,
                :siteID,
                :access_lvl
            )";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":firm_id", $firm_id);
        $stmt->bindValue(":parent_user", $parent_user);
        $stmt->bindValue(":name", $name);
        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":phone", $phone);
        $stmt->bindValue(":username", $username);
        $stmt->bindValue(":pass", $pass);
        $stmt->bindValue(":siteID", $siteID);
        $stmt->bindValue(":access_lvl", $access_lvl);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function updateFirm($id, $email, $phone, $fax, $mol, $address, $office, $dn, $bulstat)
    {
        $sql = "UPDATE Firmi 
                SET    
                   EMail = :email, 
                   Phone = :phone, 
                   Fax = :fax, 
                   MOL = :mol, 
                   Address = :address, 
                   Office = :office, 
                   dn = :dn, 
                   bulstat = :bulstat
                WHERE ID=:id
                LIMIT 1 "; 
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":phone", $phone);
        $stmt->bindValue(":fax", $fax);
        $stmt->bindValue(":mol", $mol);
        $stmt->bindValue(":address", $address);
        $stmt->bindValue(":office", $office);
        $stmt->bindValue(":dn", $dn);
        $stmt->bindValue(":bulstat", $bulstat);
        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
        return $stmt->execute();
    }
}
