<?php
require_once realpath(__DIR__.'/vendor').'/autoload.php';

use Webmozart\Assert\Assert;
use Maksoft\Modules\Reservation\Reserve;
$check = array('c', 't', 's');

if($_SERVER['REQUEST_METHOD'] == 'GET'){
    if(array_diff($check, array_keys($_GET))){
        throw new Exception('Некоректни данни!', 304);
    }
    Assert::stringNotEmpty($_GET['c'], 'c must not be empty! Got: %s');
    Assert::stringNotEmpty($_GET['t'], 't must not be empty! Got: %s');
    Assert::stringNotEmpty($_GET['s'], 's must not be empty! Got: %s');
    $reservation = new Reserve();
    echo $reservation->loadReservation($_GET['c'], $_GET['s'], $_GET['t'])->res_str;
}else{
    throw new Exception('Некоректни данни!', 304);
}
