<?php
require_once __DIR__.DIRECTORY_SEPARATOR.'base.php';
use PhpAmqpLib\Message\AMQPMessage;

$channel = $connection->channel();

$queue_name = 'topic_logs';
$topic = 'anonymous.info';
$channel->exchange_declare($queue_name, 'topic', false, false, false);

$routing_key = isset($argv[1]) && !empty($argv[1]) ? $argv[1] : $topic;
$msg = implode(' ', array_slice($argv, 2));
if(empty($msg)) $msg = "Hello World!";

$message = new AMQPMessage($msg);

$channel->basic_publish($message, $queue_name, $routing_key);

echo " [x] Sent ",$routing_key,':',$msg," \n";

$channel->close();
$connection->close();

?>
