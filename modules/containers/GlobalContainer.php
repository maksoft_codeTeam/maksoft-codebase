<?php
namespace Maksoft\Containers;

use \Pimple\ServiceProviderInterface;
use \Maksoft\Core\MysqlDb;
use \Maksoft\Gateway\Gateway;

require_once __DIR__.'/../../lib/lib_page.php';
require_once __DIR__.'/../../lib/lib_user.php';
require_once __DIR__.'/../../lib/lib_site.php';


class GlobalContainer implements ServiceProviderInterface 
{
    public function register(\Pimple\Container $pimple)
    {
        $pimple['db'] = function($c){
            $env = array();
            $env['DATABASE']['DB_HOST']= 'localhost';
            $env['DATABASE']['DB_NAME']= 'maksoft';
            $env['DATABASE']['DB_USER']= 'maksoft';
            $env['DATABASE']['DB_PASS']= 'mak211';
            $db = new MysqlDb($env);
            $db->exec("SET NAMES cp1251");
            return $db;
        };

        $pimple['gate'] = function($c) {
            return new GateWay($c['db']);
        };

        $pimple['email_settings'] = $pimple->factory(function ($c) {
            $db = $c['db'];
            $stmt = $db->prepare("SELECT * FROM maksoft.mail_settings WHERE incoming_server = 'server.maksoft.net' LIMIT 1");
            $stmt->execute();
            return $stmt->fetch(\PDO::FETCH_OBJ);
        });;

        $pimple['email'] = $pimple->factory(function($c) {
            $mail = new \PHPMailer;
            #$mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'server.maksoft.net';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->CharSet = "utf8";
            $mail->Username = 'cc@maksoft.bg';                 // SMTP username
            $mail->Password = 'maksoft@cc';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to
            $mail->isHTML(true); 
            $mail->isSMTP();
            return $mail;
        });

        $pimple['broker'] = $pimple->factory(function($c) {
            $broker = new \Jokuf\Event\worker\RabbitMQ();
            $broker->setHost('localhost');
            $broker->setUsername('guest');
            $broker->setPassword("mnk@22MNK");
            $broker->setPort(5672);
            $broker->connect();
            return $broker;
        });

        $pimple['o_page'] = $pimple->factory(function($c) {
            return new \page();
        });

        $pimple['o_user'] = $pimple->factory(function($c) {
            return new \user();
        });

        $pimple['o_site'] = $pimple->factory(function($c) {
            return new \site();
        });

        $pimple['tree'] = $pimple->factory(function($c) {
            $allPages = $c['gate']->page()->getSiteMapPages($c['o_page']->_site['SitesID']);
            return new Tree($allPages);
        });

    }
}
