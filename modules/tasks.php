<?php 
require_once __DIR__.'/vendor/autoload.php';

if(!isset($di_container) or !$di_container) { 
    $di_container = new \Pimple\Container();
    $di_container->register(new \Maksoft\Containers\GlobalContainer());
}


function say_hi($name) {
        return sprintf("Hello %s", $name);
}


function task2() {
        return implode('|', func_get_args());
}

function multiply($x=2){
        return $x * $x;
}

function add($x, $y)
{
    return $x+$y;
}


function send_email($sender , array $recipients, $subject, $content) 
{
    global $di_container;

    $email = $di_container['email'];
    $email->setFrom($sender['email'], $sender['name']);

    foreach($recipients as $recipient) {
        $email->addAddress($recipient['email'], $recipient['name']);     // Add a recipient
    }
    $email->addBcc($sender['email'], $sender['name']);
    $email->addBcc('cc@maksoft.bg', $subject);
    $email->Subject = $subject;
    $email->Body    = $content;
    if(!$email->send()) {
        throw new \Exception($email->ErrorInfo);
    }
    return True;
}

