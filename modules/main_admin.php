<?php
error_reporting(E_ERROR | E_PARSE);
$_ds = DIRECTORY_SEPARATOR;
$ds = DIRECTORY_SEPARATOR;
require __DIR__.$_ds.'vendor'.$_ds.'autoload.php';

use Maksoft\Modules\Reservation\HotelReservation;

$hotel = new HotelReservation(431);
$hotel->setLanguage(1);
$column_rigth = array();
#printf('<pre>%s</pre>', print_r($hotel->getHotel()));
$session = new Maksoft\Modules\lib\Session();
$_SESSION['booking']['language_id'] = $hotel->getLanguage();
$_SESSION['booking']['hotel_id'] = $hotel->getId();


#printf('<pre>%s</pre>', print_r($_POST));
#printf('<pre>%s%s</pre>', print_r($_POST), print_r($_SESSION['booking']));
#echo session_id();
#echo dirname(  __FILE__  ).'/settings.ini'; 


#echo __DIR__.$ds.'src'.$ds.'Templates'.$ds.'admin'.$ds.'hotel.php';
require_once __DIR__.$ds.'src'.$ds.'Templates'.$ds.'admin'.$ds.'hotel.php';


if($_SERVER['REQUEST_METHOD'] === 'POST'){
    if($_POST['cancel']){
        $hotel->cancelReservation($_POST['cancel']);
    } elseif($_POST['approve']){
        $hotel->approveReservation($_POST['approve']);
    }
}
