<?php 
namespace Maksoft\Sockets\Chat\obj;


class Conversation
{
    protected $structrue;
    protected $graph=array();

    public function __construct()
    {
        $this->graph = new \SplObjectStorage();

    }
    /*
        (bool) $sender - boolean value to check if $user is sending the message or receving
    */
    public function add(ConnectionInterface $user, $message, $sender)
    {
        if(in_array($user, $this->graph)){
            $this->graph[$user][] = array(time(), $message, $sender);
            return;
        }
        $this->graph[$user] = array();
        $this->graph[$user][] = array(time(), $message, $sender);
    }

    
}
