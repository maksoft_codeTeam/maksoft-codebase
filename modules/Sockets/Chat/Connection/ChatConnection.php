<?php


namespace Maksoft\Sockets\Chat\Connection;
use Ratchet\ConnectionInterface;


class ChatConnection
{
    /**
     * The ConnectionInterface instance
     *
     * @var ConnectionInterface
     */
    private $connection;

    private $c_name;

    private $c_id;

    private $c_avatar;

    private $conversations;


    public function __construct(ConnectionInterface $conn)
    {
        $this->connection = $conn;
        $this->conversations = new \SplObjectStorage();
    }

    public function init()
    {
        return $this->connection;
    }


    public function setName($name){
        $this->c_name = $name;
    }

    public function setId($id){
        $this->c_id = $id;
    }

    public function setAvatar($avatar)
    {
        $this->c_avatar = $avatar;
    }


    public function getAvatar()
    {
        return $this->c_avatar;
    }

    public function getName(){
        return $this->c_name;
    }

    public function getId()
    {
        return $this->c_id;
    }

}
