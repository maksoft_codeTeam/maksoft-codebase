<?php
session_start();
require_once __DIR__.'/../../../lib/lib_page.php';
require_once __DIR__.'/../..'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Maksoft\Sockets\Chat\Message;
$o_page = new page();
$messageInterface = new Message();
$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            $messageInterface
        )
    ),
    8180
);

$server->run();
