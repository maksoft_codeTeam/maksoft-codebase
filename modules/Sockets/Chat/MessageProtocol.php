<?php
namespace Maksoft\Sockets\Chat;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;


class MessageProtocol implements MessageComponentInterface {
    protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        $msgData = json_decode($msg);
        if(array
    }

    public function onClose(ConnectionInterface $conn) {
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }

    public function sendPrivateMessage($from , $toResourceId, $msg) {
        foreach ($this->clients as $client){
            if ($toResourceId == $client->resourceId) {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }
    }

    public function sendToMySelf($from, $msg) {
        foreach ($this->clients as $client){
            if ($from == $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }
    }

    public function sendToAll($from, $msg) {
        foreach ($this->clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }
    }
}
