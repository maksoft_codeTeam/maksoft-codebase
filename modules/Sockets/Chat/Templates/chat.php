<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.3/handlebars.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<link rel="stylesheet" href="modules/Sockets/Chat/Templates/assets/css/style.css">

<script id="users-template" type="text/x-handlebars-template">
    {{#each users}}
    {{# if id }}
    <div class="msg">
        <div id="{{id}}" class="avatar">
            <a href="#" onclick="new_tab({{id}}, &quot;{{name}}&quot;)"><img src="{{avatar}}">
                <span class="user"> {{name}} </span>
            </a>
        </div>
    </div>
    {{/if}}
    {{/each}}
</script>
<link rel="stylesheet" href="/modules/Chat/assets/css/normalize.min.css">
<link rel="stylesheet" href="/modules/Chat/assets/css/chat.css">
<!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="/modules/Chat/css/chat-ie.css">
<![endif]-->
	<div id="chat" class="sidebar">
		<ul id="tab-list">
			<li id="user-tab" class="tab">
				<div class="container">
					<div class="row row-header">
						<div class="header">
							<div class="content">Chat</div>
						</div>
					</div>
					<div class="row row-body">
						<div class="body">
							<div class="content">
								<ul id="user-list" class="users">
								</ul>
							</div>
						</div>
					</div>
					<div class="row row-footer">
						<div class="footer">
							<div class="content">
								<input type="text" class="form-control" placeholder="Search">
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<script src="/modules/Chat/assets/js/elastic.min.js"></script>
	<script src="/modules/Chat/assets/js/main.js"></script>
<script>

    var user;
    var messages = [];
    function hashCode (str){
        var hash = 0;
        if (str.length == 0) return hash;
        for (i = 0; i < str.length; i++) {
            char = str.charCodeAt(i);
            hash = ((hash<<5)-hash)+char;
            hash = hash & hash; // Convert to 32bit integer
        }
        return hash;
    }
    djb2Code = function(str){
        var hash = 5381;
        for (i = 0; i < str.length; i++) {
            char = str.charCodeAt(i);
            hash = ((hash << 5) + hash) + char; /* hash * 33 + c */
        }
        return hash;
    }

    var my_hash = djb2Code("<?php echo $o_page->_user['Name']. $o_page->_user['ID'];?>");

    var you_msg =  ' <li class="message you">'
                   + '     <div class="media left">'
                   + '         <div class="media-aside"><img class="avatar" src="{{avatar}}" alt="avatar"></div>'
                   + '         <div class="media-body">'
                   + '             <div class="bubble">'
                   + '                 {{text}}'
                   + '             </div>'
                   + '         </div>'
                   + '     </div>'
                   + ' </li> ';
    var recv_templ = Handlebars.compile(you_msg);


    function recv_msg(msg)
    {
        console.log('receive');
        new_tab(msg.from_id, msg.from_name);
        var id = msg.from_name + msg.from_id;
        var id = djb2Code(id);
        console.log('receive id here ' + id);
        templ = recv_templ({text: msg.text, avatar: msg.avatar});
        $("#"+id).find("ul[class=messages]").append(templ).fadeIn('slow');
        scrollDown(id);
    }

    //var messages_template = Handlebars.compile($('#messages-template').html());
    var users_template = Handlebars.compile($('#users-template').html());

    function updateMessages(msg){
        if(msg.action == 'private'){
            recv_msg(msg);
        }
        // if(msg.text){
        //     messages.push(msg);
        //     var messages_html = messages_template({'messages': messages});
        //     $('#messages').html(messages_html);
        //     $("#messages").animate({ scrollTop: $('#messages')[0].scrollHeight}, 1000);
        // }
        if(msg.action == 'connect'){
            var users_html = users_template({'users': msg.clients});
            console.log(msg);
            $('#user-list').html(users_html);
            $("#user-list").animate({ scrollTop: $('#users')[0].scrollHeight}, 1000);
        }
    }
    var user = '<?php echo $o_page->_user['Name'];?>';
    var conn = new WebSocket('ws://maksoft.net:8180');
    conn.onopen = function(e) {
        var msg = {
            'user': user,
            'action': 'all',
            'text': user + ' �� ������ � ����',
            'time': moment().format('hh:mm a'),
            'user_id': <?php echo $o_page->_user['ID']; ?>
        };
        updateMessages(msg);
        conn.send(JSON.stringify(msg));
        var user_data = {
            'action': 'user_data',
            'id': <?php echo $o_page->_user['ID'];?>,
            'name': '<?php echo $o_page->_user['Name'];?>'
        };
        conn.send(JSON.stringify(user_data));

        $('#user').val('');
    };

    conn.onmessage = function(e) {
        var msg = JSON.parse(e.data);
        updateMessages(msg);
    };


    $('#join-chat').click(function(){
        conn = new WebSocket('ws://maksoft.net:8180');
        $('#user-container').addClass('hidden');
        $('#main-container').removeClass('hidden');
        conn.onopen = function(e) {
            $('#user-container').addClass('hidden');
            $('#main-container').removeClass('hidden');
                
            var msg = {
                'user': user,
                'action': 'all',
                'text': user + ' �� ������ � ����',
                'time': moment().format('hh:mm a'),
                'user_id': <?php echo $o_page->_user['ID']; ?>
            };
            updateMessages(msg);
            conn.send(JSON.stringify(msg));
            var user_data = {
                'action': 'user_data',
                'id': <?php echo $o_page->_user['ID'];?>,
                'name': '<?php echo $o_page->_user['Name'];?>'
            };
            conn.send(JSON.stringify(user_data));

            $('#user').val('');
        };

    });

    var tab= '<li id="{{id}}" class="tab">'
            +'	<div class="container">'
            +'		<div class="row row-header">'
            +'			<div class="header">'
            +'				<div class="content name-title">{{name}}</div>'
            +'              <div class="close"><button>x</button></div>'
            +'			</div>'
            +'		</div>'
            +'		<div class="row row-body">'
            +'			<div class="body">'
            +'				<div class="content">'
            +'					<ul class="messages">'
            +'					</ul>'
            +'				</div>'
            +'			</div>'
            +'		</div>'
            +'		<div class="row row-footer">'
            +'			<div class="footer">'
            +'				<div class="content">'
            +'					<textarea rows="1" class="form-control"></textarea>'
            +'				</div>'
            +'			</div>'
            +'		</div>'
            +'	</div>'
            +'</li>';
    var tab_templ = Handlebars.compile(tab);


    String.prototype.hashCode = function(){
        var hash = 0;
        if (this.length == 0) return hash;
        for (i = 0; i < this.length; i++) {
            char = this.charCodeAt(i);
            hash = ((hash<<5)-hash)+char;
            hash = hash & hash; // Convert to 32bit integer
        }
        return hash;
    }

    function new_tab(u_id, u_name){
        console.log(u_name + u_id);
        var id = djb2Code(u_name+u_id);
        console.log('new tab id -> ' + id);

        var tab = $("#"+id); 

        console.log(tab.is(':visible'));
        if(!$("#"+id).is(':visible')){
            tab.show();
        }

        if(tab.length){
            return;     
        }

        var template = tab_templ({name: u_name, id: id });
        $("#tab-list").append(template).fadeIn();
        var textarea = $("#"+id).find("textarea");
        var b = $("#"+id).find("button");
        b.on("click", function(){ $("#"+id).hide();});
        textarea.on("keydown", function(event){
            if(event.keyCode == 13){
                event.preventDefault();
                console.log('enter');
                send_msg(u_name, u_id, textarea.val());
                textarea.val('');
            }
        });
    }

    var me_msg = "<li class=\"message me no-avatar\">"
                +" <div class=\"media right\">"   
                +"    <div class=\"media-body\">"
                +"        <div class=\"bubble\">"
                +"            {{text}}"
                +"        </div>"
                +"    </div>"
                +"</div> </li> ";
    var send_templ  = Handlebars.compile(me_msg);


    function scrollDown(id){

        var height = 0;
        $("#" + id + " li").each(function(i, value){
            height += parseInt($(this).height());
        });

        height += '';

        $("#" + id +" div[class=content]").animate({scrollTop: height});
        console.log('scroll?');
        console.log(height);
    }

    console.log(height);

    function send_msg(u_name, u_id, msg){
        var id = u_name + u_id;
        var messages = $("#"+djb2Code(id)).find("ul[class=messages]");
        var template = send_templ({text:msg});
        messages.append(template).fadeIn('slow');
        scrollDown(djb2Code(id));
        var msg = {
            'from_name': "<?=$o_page->_user['Name'];?>",
            'from_id': <?=$o_page->_user['ID'];?>, 
            'user': u_name,
            'action': 'private',
            'text': msg,
            'time': moment().format('hh:mm a'),
            'user_id': u_id
        };
        conn.send(JSON.stringify(msg));
    }

    function close(el){
        $(el).hide();
    }


//
//    var user;
//    var messages = [];
//    var my_hash = djb2Code("<?php echo $o_page->_user['Name']. $o_page->_user['ID'];?>");
//    var you_msg =  ' <li class="message you">'
//    + '     <div class="media left">'
//    + '         <div class="media-aside"><img class="avatar" src="{{avatar}}" alt="avatar"></div>'
//    + '         <div class="media-body">'
//    + '             <div class="bubble">'
//    + '                 {{text}}'
//    + '             </div>'
//    + '         </div>'
//    + '     </div>'
//    + ' </li> ';
//    var recv_templ = Handlebars.compile(you_msg);
//
//    //var messages_template = Handlebars.compile($('#messages-template').html());
//    var users_template = Handlebars.compile($('#users-template').html());
//
//    function setupWebSocket(){
//        this.hash = function(str){
//            var hash = 5381;
//            for (i = 0; i < str.length; i++) {
//                char = str.charCodeAt(i);
//                hash = ((hash << 5) + hash) + char;
//                /* hash * 33 + c */
//            }
//            return hash;
//        }
//        this.conn = new WebSocket('ws://maksoft.net:8180');
//        this.conn.onopen = function(e) {
//            var msg = {
//                'user': user,
//                'action': 'all',
//                'text': user + ' successfully connected',
//                'time': moment().format('hh:mm a'),
//                'user_id': <?php echo $o_page->_user['ID']; ?>
//            };
//                this.update(msg);
//                this.conn.send(JSON.stringify(msg));
//                var user_data = {
//                'action': 'user_data',
//                'id': <?php echo $o_page->_user['ID'];?>,
//                'name': '<?php echo $o_page->_user['Name'];?>'
//            };
//            this.conn.send(JSON.stringify(user_data));
//            $('#user').val('');
//
//        };
//        conn.onmessage = function(e) {
//            var msg = JSON.parse(e.data);
//            updateMessages(msg);
//        };
//        this.conn.onclose = function(){
//            setTimeout(setupWebSocket, 1000);
//        };
//        this.scrollDown = function (id){
//            var height = 0;
//            $("#" + id + " li").each(function(i, value){
//                height += parseInt($(this).height());
//            }
//                                    );
//            height += '';
//            $("#" + id +" div[class=content]").animate({
//                scrollTop: height});
//        }
//        this.update = function(msg) {
//            if (msg.action == 'private') {
//                this.recv_msg(msg);
//            }
//            if (msg.action == 'connect') {
//                var users_html = users_template({
//                    'users': msg.clients
//                });
//                $('#user-list').html(users_html);
//                $("#user-list").animate({
//                    scrollTop: $('#users')[0].scrollHeight
//                }, 1000);
//            }
//        }
//        this.send_msg = function (u_name, u_id, msg){
//            var id = u_name + u_id;
//            var messages = $("#"+this.hash(id)).find("ul[class=messages]");
//            var template = send_templ({
//                text:msg}
//                                     );
//            messages.append(template).fadeIn('slow');
//            this.scrollDown(this.hash(id));
//            var msg = {
//                'from_name': "<?=$o_page->_user['Name'];?>",
//                'from_id': <?=$o_page->_user['ID'];?>, 
//                'user': u_name,
//                'action': 'private',
//                'text': msg,
//                'time': moment().format('hh:mm a'),
//                'user_id': u_id
//            };
//            this.conn.send(JSON.stringify(msg));
//        }
//        this.new_tab = function(u_id, u_name){
//            console.log(u_name + u_id);
//            var id = this.djb2Code(u_name+u_id);
//            var tab = $("#"+id);
//            if(!$("#"+id).is(':visible')){
//                tab.show();
//            }
//            if(tab.length){
//                return;
//            }
//            var template = tab_templ({
//                name: u_name, id: id }
//                                    );
//            $("#tab-list").append(template).fadeIn();
//            var textarea = $("#"+id).find("textarea");
//            var b = $("#"+id).find("button");
//            b.on("click", function(){
//                $("#"+id).hide();
//            }
//                );
//            textarea.on("keydown", function(event){
//                if(event.keyCode == 13){
//                    event.preventDefault();
//                    console.log('enter');
//                    send_msg(u_name, u_id, textarea.val());
//                    textarea.val('');
//                }
//            });
//        }
//        this.recv_msg = function(msg)
//        {
//            console.log('receive');
//            this.new_tab(msg.from_id, msg.from_name);
//            var id = msg.from_name + msg.from_id;
//            var id = this.hash(id);
//            templ = this.recv_templ({
//                text: msg.text, avatar: msg.avatar}
//                              );
//            $("#"+id).find("ul[class=messages]").append(templ).fadeIn('slow');
//            this.scrollDown(id);
//        }
//    }
//    
//    
//    
//    var user = '<?php echo $o_page->_user['Name'];?>';
//    var tab= '<li id="{{id}}" class="tab">'
//    +'  <div class="container">'
//    +'      <div class="row row-header">'
//    +'          <div class="header">'
//    +'              <div class="content name-title">{{name}}</div>'
//    +'              <div class="close"><button>x</button></div>'
//    +'          </div>'
//    +'      </div>'
//    +'      <div class="row row-body">'
//    +'          <div class="body">'
//    +'              <div class="content">'
//    +'                  <ul class="messages">'
//    +'                  </ul>'
//    +'              </div>'
//    +'          </div>'
//    +'      </div>'
//    +'      <div class="row row-footer">'
//    +'          <div class="footer">'
//    +'              <div class="content">'
//    +'                  <textarea rows="1" class="form-control"></textarea>'
//    +'              </div>'
//    +'          </div>'
//    +'      </div>'
//    +'  </div>'
//    +'</li>';
//    var tab_templ = Handlebars.compile(tab);
//    var me_msg = "<li class=\"message me no-avatar\">"
//    +" <div class=\"media right\">"   
//    +"    <div class=\"media-body\">"
//    +"        <div class=\"bubble\">"
//    +"            {{text}}"
//    +"        </div>"
//    +"    </div>"
//    +"</div> </li> ";
//    var send_templ  = Handlebars.compile(me_msg);
//    function close(el){
//        $(el).hide();
//    }
//    var c = setupWebSocket();
//    c.conn();

</script>

