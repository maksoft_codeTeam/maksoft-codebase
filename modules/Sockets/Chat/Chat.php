<?php
namespace Maksoft\Sockets\Chat;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;


class Chat implements MessageComponentInterface {
    protected $clients;
    protected $u_id=false;

    public function __construct() {
        # $this->clients = new \SplObjectStorage;
        $this->clients = array();
    }


    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        # $this->clients->attach($conn);
        $this->clients[$conn->resourceId] = array(
                'connection'=>$conn,
                'resourceId' => $conn->resourceId,
                "messages" => array(),
        );
        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');
        $data = json_decode($msg);
        switch($data->action){
            case 'all':
                foreach ($this->clients as $client) {
                    if ($from !== $client['connection']) {
                        // The sender is not the receiver, send to each client connected

                        $client['connection']->send(json_encode($data));
                    }
                }
                break;

            case 'private':
                foreach($this->clients as $client) {
                    if($data->user_id == $client['id']){
                        echo $client['id'];
                        $data->avatar = $client['avatar'];
                        $client['connection']->send(json_encode($data));
                    }
                }
                break;

            case 'user_data':
                $this->clients[$from->resourceId]['name'] = $data->name;
                $this->clients[$from->resourceId]['id'] = $data->id;
                break;
        }
        $this->eventconnect($from);
    }

    private function eventconnect(ConnectionInterface $from)
    {
        $avatar = 'http://api.adorable.io/avatars/150/' . rand(100000,999999) . '.png';
        if(empty($this->clients[$from->resourceId]['avatar'])){
            $this->clients[$from->resourceId]['avatar'] = $avatar;
        }
        $send_data = array(
            'action' => 'connect',
            'clients' => $this->clients,
        );
        $this->sendMessageToAll($send_data);
    }

    private function sendMessageToAll($msg)
    {
        if(is_object($msg) || is_array($msg)) {
            $msg = json_encode($msg);
        }
        foreach ($this->clients as $client) {
            $client['connection']->send($msg);
            echo PHP_EOL.'Send message to '. $client['connection']->resourceId.PHP_EOL;
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        unset($this->clients[$conn->resourceId]);
        $send_data = array(
            'action' => 'connect',
            'clients' => $this->clients,
        );
        $this->sendMessageToAll($send_data);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }
    
    public function setName($name){


    }

    public function setId($id){

    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}
