<?php
namespace Maksoft\Interfaces;

interface FormObserver 
{
    function update(FormSubject $subject);
}
