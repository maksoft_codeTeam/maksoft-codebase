<?php
namespace Maksoft\Interfaces;

interface FormSubject 
{
    function attach(\SplObserver $observer);

    function detach(\SplObserver $observer);

    function notify();
    
    function toEmail();

    function fromEmail();
    
    function page();

    function site();

    function subject();

    function content();
}
