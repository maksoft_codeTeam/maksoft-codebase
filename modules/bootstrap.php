<?php

// bootstrap.php
require_once "vendor/autoload.php";

use Bus\settings\Base;

require_once "vendor/autoload.php";

use Bus\settings\Base as BaseSettings;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Form\Forms;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;

$config = BaseSettings::init('bus_schedule', 'jokuf', '7BcpHAFB', 'localhost', 3306);
$config->setTwigPath(realpath(__DIR__.'/../forms/global/bus_tickets/twig/templates/'));
$config->setTwigCachePath(realpath(__DIR__.'/forms/global/bus_tickets/twig/cache/'));


$defaultFormTheme = 'form_div_layout.html.twig';

$vendorDir = realpath(__DIR__.'/vendor');
// the path to TwigBridge library so Twig can locate the
// form_div_layout.html.twig file
$appVariableReflection = new \ReflectionClass('\Symfony\Bridge\Twig\AppVariable');
$vendorTwigBridgeDir = dirname($appVariableReflection->getFileName());
// the path to your other templates
$viewsDir = $config->getTwigPath();

$twig = new Twig_Environment(new Twig_Loader_Filesystem(array(
    $viewsDir,
    $vendorTwigBridgeDir.'/Resources/views/Form',
)));
$formEngine = new TwigRendererEngine(array($defaultFormTheme), $twig);
$twig->addRuntimeLoader(new \Twig_FactoryRuntimeLoader(array(
   'Symfony\Bridge\Twig\Form\TwigRenderer' => function () use ($formEngine) {
        return new TwigRenderer($formEngine);
    },
)));

// ... (see the previous CSRF Protection section for more information)

// add the FormExtension to Twig PHP > 5.3
#$twig->addExtension(new FormExtension());

// create the Translator
$translator = new Translator('en');
// somehow load some translations into it
$translator->addLoader('xlf', new XliffFileLoader());
$translator->addResource(
    'xlf',
    __DIR__.'/translations/messages.en.xlf',
    'en'
);

// add the TranslationExtension (gives us trans and transChoice filters)
$twig->addExtension(new TranslationExtension($translator));


$formFactory = Forms::createFormFactoryBuilder()
    ->addExtension(new HttpFoundationExtension())
    ->getFormFactory();

$em = $config->getEntityManager();
