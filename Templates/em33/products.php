<?php
	//load subpages
	$query = mysqli_query("SELECT * FROM pages p LEFT JOIN images im on p.imageNo = im.imageID WHERE ParentPage = '$n' AND SiteID='$SiteID' ORDER by sort_n DESC");
	$subpages = array();
	$i=0;
	while($subpage = mysqli_fetch_object($query))
		{
			if(!file_exists($subpage->image_src))
				$subpage->image_src = DIR_TEMPLATE . "images/no_photo.jpg";
			$subpages[$i]['img'] = "<a href=\"page.php?n=$subpage->n&SiteID=$subpage->SiteID\"><img src=\"img_preview.php?image_file=$subpage->image_src&img_width=180\" class=border_image></a>";
			$subpages[$i]['Name'] = "<a href=\"page.php?n=$subpage->n&SiteID=$subpage->SiteID\" class=\"color_link\">".$subpage->Name."</a><br>";
			$subpages[$i]['text'] = crop_text($subpage->textStr);
			$subpages[$i]['link'] = "<a href=\"page.php?n=$subpage->n&SiteID=$subpage->SiteID\"><img src=\"Templates/em33/images/button_more.jpg\" border=0 vspace=5></a>";
			$i++;
		}
?>
<table width="820" border="0" cellspacing="2" cellpadding="0" style="border-bottom: 1px solid #599516; margin-bottom: 15px;">
  <tr align="center" valign="middle">
    <td width="25%"><?php echo $subpages[0]['img']; ?></td>
    <td width="25%"><?php echo $subpages[1]['img']; ?></td>
    <td width="25%"><?php echo $subpages[2]['img']; ?></td>
	<td width="25%"><?php echo $subpages[3]['img']; ?></td>
  </tr>
  <tr valign="top">
    <td style="border-right: 1px solid #599516; padding: 5px"><?php echo $subpages[0]['Name'] . $subpages[0]['text']; ?></td>
    <td style="border-right: 1px solid #599516; padding: 5px"><?php echo $subpages[1]['Name'] . $subpages[1]['text']; ?></td>
	<td style="border-right: 1px solid #599516; padding: 5px"><?php echo $subpages[2]['Name'] . $subpages[2]['text']; ?></td>
	<td style="padding: 5px"><?php echo $subpages[3]['Name'] . $subpages[3]['text']; ?></td>
  </tr>
  <tr align="center" valign="middle">
    <td><?php echo $subpages[0]['link']; ?></td>
	<td><?php echo $subpages[1]['link']; ?></td>
	<td><?php echo $subpages[2]['link']; ?></td>
	<td><?php echo $subpages[3]['link']; ?></td>
  </tr>
</table>