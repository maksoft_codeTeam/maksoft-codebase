<?php
	$news = $o_page->get_pSubpages($o_site->_site['news']);
	if(!DEMO_MODE && count($news) > 0)
		{
			?>
			<!-- NEWS //-->
			<h2 class="title"><?=$o_page->get_pTitle("strict", $o_site->_site['news'])?></h2>
			<ul class="blog">
				<?php
				for($i=0; $i<count($news); $i++)
					{	
						?>
						<li class="span4">
							<?php $o_page->print_pImage(145, "", "", $news[$i]['n'])?>
							<p class=""><?=cut_text($news[$i]['textStr'], 120, "...", "<b><strong>")?> <a href="<?=$o_page->get_pLink($news[$i]['n'])?>" title="" class="next_link"><?=$o_site->get_sMoreText()?></a></p>
						</li>
						<?php
					}
				echo LABEL_VIEW_ALL_NEWS . " ";
				$o_page->print_pName(true, $o_site->_site['news']);
				?>
			</ul>
			
			<?
		}
	else include_once TEMPLATE_DEMO . "box_news.html";
?>