<?php
	$top_offers = $o_page->get_pGroupContent($n_offers_group);
	
	if(!DEMO_MODE && count($top_offers) > 0)
	{
		$top_offers = $o_page->get_pGroupContent($n_offers_group);
		
?>
<!-- TOP OFFERS //-->
<h2 class="title"><?=$o_page->get_pName($n_offers)?></h2>
<div class="post_blog">

    <div class="hover_img">
        <a href="<?=$o_page->get_pLink($top_offers[0]['n'])?>" class="">
            <?php $o_page->print_pImage(375, "", TEMPLATE_IMAGES_DEMO . "post_blog.jpg", $top_offers[0]['n'])?>
        </a>
        <span class="portfolio_link3"><a href="<?=$o_page->get_pLink($top_offers[0]['n'])?>" ></a></span>
    </div>
    <span class="welcome-title"><?=$o_page->get_pName($top_offers[0]['n'])?></span>
    <p class="pad25"><?=strip_tags(crop_text($top_offers[0]['textStr']))?>
        <a href="<?=$o_page->get_pLink($top_offers[0]['n'])?>" class=""><?=$o_site->get_sMoreText()?></a>
    </p>  
</div>
<div class="clear"></div>
<?php
	}
	else include_once TEMPLATE_DEMO . "box_welcome.html";
?>