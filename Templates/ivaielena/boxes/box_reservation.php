<form method="post" id="short-reservation-form" action="<?=$o_page->get_pLink($n_reservation)?>">
<div id="reservation" class="short">
	<h2 class="title3"><?=TITLE_RESERVATION?></h2>
	<!-- SECTION 1 //-->
       <label for="date_from"><?=LABEL_DATE_ARIVAL?> *</label>
        <input type="text" name="date_from" id="date_from" value="">
        <label for="date_to"><?=LABEL_DATE_DEPARTURE?> *</label>
        <input type="text" name="date_to" id="date_to">
        <label for="room_type"><?=LABEL_ROOM_TYPE?></label>
        <select name="room_type" id="room_type">
          <?php
            $type_keys = array_keys($room_types);
            for($i=0; $i<count($room_types); $i++)
            {
            ?>
            <option value="<?=$type_keys[$i]?>"><?=$room_types[$type_keys[$i]]?></option>
            <?
            }
            ?>
        </select>
	<br clear="all"><br clear="all">
    <input type="reset" class="btn dark_btn" value="<?=BUTTON_CLEAR?>"><input type="button" class="btn btn-success" value="<?=BUTTON_SUBMIT?>" id="book-next">
	<br clear="all">
    <div class="alert">
      <?=MESSAGE_MANDANTORY_RESERVATION_FIELDS?>
    </div>
</div>
</form>
