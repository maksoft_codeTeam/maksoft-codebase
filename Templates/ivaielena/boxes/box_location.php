<?php
	$location = $o_page->get_page($n_info_map);
	if(!DEMO_MODE && $n_info_map)
	{
?>
<!-- LOCATION //-->
    <h2 class="title"><?=$o_page->get_pName($n_info_map)?></h2>
    <? $o_page->print_pImage(570, "", "", $n_info_map)?>
    <div class="location-info">
        <a href="<?=$o_page->get_pLink($n_info_map)?>" title="<?=$o_page->get_pTitle("strict", $n_info_map)?>"><img src="<?=TEMPLATE_IMAGES?>hotel.jpg" class="img-circle img-polaroid" alt="<?=$o_page->get_pTitle("strict", $n_info_map)?>"></a>
        <div class="info">
            <?=$o_page->get_pText($n_info_map)?>
        </div>
    </div>
<?
	}
	else include_once TEMPLATE_DEMO . "box_location.html";
?>