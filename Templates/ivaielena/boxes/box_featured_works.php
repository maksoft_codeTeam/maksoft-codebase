<?php
	$items = $o_page->get_pSubpages($n_featured_works);
	if(!DEMO_MODE && count($items) > 0)
		{
			?>
			<!-- FEATURED WORKS //-->
            <h2 class="title"><?=$o_page->get_pName($n_featured_works)?></h2>
            <ul id="mycarousel" class="jcarousel-skin-tango">
                 <?php
				 	for($i=0; $i<count($items); $i++)
						{
							 ?>
							 <li>
								<div class="view hover_img">
									<a href="<?=$items[$i]['image_src']?>" rel="prettyPhoto[portfolio1]">
										<img src="<?=$items[$i]['image_src']?>" alt="" />
										<!--
                                        <div class="mask mask-1"></div>
										<div class="mask mask-2"></div>
                                        //-->
									</a>  
									<div class="content">
										<h2 class=""><?=$o_page->get_pName($items[$i]['n'])?></h2>
										<div class="links_g">
											<a href="<?=$items[$i]['image_src']?>" class="portfolio_zoom1" rel="prettyPhoto[portfolio2]"></a>
											<a href="<?=$o_page->get_pLink($items[$i]['n'])?>" class="portfolio_link1"></a>
										</div>
									</div>                                  
								</div>
							</li>
							<?php
						}
				?>
            </ul> 
			<!-- FEATURED WORKS //-->
			<?
		}
	else include_once TEMPLATE_DEMO . "box_featured_works.html";
?>