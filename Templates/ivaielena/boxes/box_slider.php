<?php
	//get weather
	$wwo->get_weather();
	$today = getdate(); 
	$sun_info = date_sun_info(time(), $wwo->latitude, $wwo->longitude);
	$sunrise = getdate($sun_info['sunrise']);
	$sunset = getdate($sun_info['sunset']);
	//echo "---".$sunrise['hours']."---".$sunset['hours']."---".$today['hours']; 
	
	$day_night = "day_sm"; 
	if($today['hours']<$sunrise['hours']) $day_night = "night_sm"; 
	if($today['hours']>$sunset['hours']) $day_night = "night_sm"; 
	
	//$banners = $o_page->get_pBanners();
	$ses = ""; 
	$mesec = date("n"); 
	
	$ses = "summer/"; 
	if( ($mesec <= 4) || ($mesec >= 10) ) $ses = "winter/"; 
	


	
	$banners = array(
		array("image_src"=>TEMPLATE_IMAGES . "slider/".$ses."1.jpg"),
		array("image_src"=>TEMPLATE_IMAGES . "slider/".$ses."2.jpg"),
		array("image_src"=>TEMPLATE_IMAGES . "slider/".$ses."3.jpg"),
		array("image_src"=>TEMPLATE_IMAGES . "slider/".$ses."4.jpg")
	);
	if(count($banners)>0 && !DEMO_MODE)
	{
		?>
		<!--slider-->
		<div id="main_slider">
			<div class="camera_wrap" id="camera_wrap_1">
            	<?
				for($i=0; $i<count($banners); $i++)
					{
						?>
						<div data-src="<?=$banners[$i]['image_src']?>">
							<!--
                            <div class="camera_caption fadeIn">								
                                <h2>lorem ipsum &amp; dolor amet!</h2>
								<div class="slide_descr">Integer nec mauris at odio facilisis mattis venenatis.</div>                                
							</div>
                            //-->
						</div>                        
						<?
					}
				?>
            </div><!-- #camera_wrap_1 -->
            <div class="span4 offset8" id="box-reservation">
            	<?php 
					include_once TEMPLATE_DIR . "boxes/box_reservation.php"; 
					?>
            </div>
            <div class="clear"></div>	
            <div class="weather info">
            	<div class="span8">
                	<span class="weather-icon"><img src="http://cdn.worldweatheronline.net/images/weather/small/<?php echo $wwo->weather->current_condition->weatherCode."_$day_night"; ?>.png"></span>
                    <span class="degree"><strong><?=$wwo->weather->current_condition->temp_C?></strong> C&deg;</span>
                    <span class="city"><?=sprintf(LABEL_WEATHER_IN, $wwo->city)?></span>
                </div>
               <!--  
			   <div class="span4 buttons"><a href="#">����� ��������</a>   |   <a href="#">������ �� ����</a></div>
			   --> 
            </div>
			<div class="clear"></div>	
		</div>        
		<!--//slider-->
		<?
	}
	else include_once TEMPLATE_DIR_DEMO . "box_slider.html"
?>