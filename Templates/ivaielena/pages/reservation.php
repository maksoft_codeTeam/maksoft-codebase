    <!--page_container-->
    <div class="page_container">
    	<div class="wrap">
        	<div class="container">
                <div class="page_full white_bg drop-shadow">
                    <div class="breadcrumb">
                        <div class="container">
						<?=$o_page->print_pName("true", $o_page->_page['ParentPage'])?><span>/</span><?=$o_page->print_pName()?>
                        </div>
                    </div>
                    <div class="container">
                        <div class="page_in">
                        	<div class="container">
                                <section>
                                    <div class="row">
                                        <div class="span12">
                                            <h2 class="title"><?=$o_page->print_pTitle("strict")?></h2>
                                            <?php $o_page->print_pImage();?>
                                            <form method="post" id="short-reservation-form" action="form-cms.php">
                                            <div id="reservation" class="full">
                                                <!-- SECTION 1 //-->
                                                <div class="section1">
                                                    <div class="coll">
                                                        <label for="date_from"><?=LABEL_DATE_ARIVAL?> *</label>
                                                        <input type="text" name="date_from" id="date_from" value="<?=$_POST['date_from']?>">
                                                        <label for="date_to"><?=LABEL_DATE_DEPARTURE?> *</label>
                                                        <input type="text" name="date_to" id="date_to" value="<?=$_POST['date_to']?>">
                                                    </div>
                                                
                                                    <div class="coll">
                                                        <div class="scoll c1">
                                                        <label for="room_type"><?=LABEL_ROOM_TYPE?></label>
                                                        <select name="room_type" id="room_type">
                                                          <option> - </option>
                                                          <?php
                                                            $type_keys = array_keys($room_types);
                                                            for($i=0; $i<count($room_types); $i++)
                                                            {
                                                                $selected = "";
                                                                if($_POST['room_type'] == $type_keys[$i])
                                                                    $selected  = "selected";
                                                            ?>
                                                            <option value="<?=$type_keys[$i]?>" <?=$selected?>><?=$room_types[$type_keys[$i]]?></option>
                                                            <?
                                                            }
                                                            ?>
                                                        </select>
                                                        </div>
                                                        <div class="scoll c2">
                                                        <label for="count_people"><?=LABEL_ADULTS?></label>
                                                        <select name="count_people" id="count_people">
                                                          <option value="1">1</option>
                                                          <option value="2">2</option>
                                                          <option value="3">3</option>
                                                          <option value="4">4</option>
                                                          <option value="5">5</option>
                                                          <option value="5+">5+</option>
                                                        </select>
                                                        </div>
                                                        <br clear="all">
                                                        <div class="scoll c1">
                                                        <label for="count_kids"><?=LABEL_KIDS1?></label>
                                                        <select name="kids_under6">
                                                          <option value="0"></option>
                                                          <option value="1">1</option>
                                                          <option value="2">2</option>
                                                          <option value="3">3</option>
                                                        </select>
                                                        </div>
                                                        <div class="scoll c2">
                                                        <label for="count_kids"><?=LABEL_KIDS2?></label>
                                                        <select name="kids_above6">
                                                          <option value="0"></option>
                                                          <option value="1">1</option>
                                                          <option value="2">2</option>
                                                          <option value="3">3</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                
                                                    <div class="coll">
                                                        <label for="comments"><?=LABEL_COMMENTS?></label>
                                                        <textarea name="comments"></textarea>
                                                        <button class="btn btn-small btn-danger" type="button" id="book-next"><?=BUTTON_NEXT?> <i class="icon-chevron-right icon-white"></i></button>
                                                    </div>
                                                </div>
                                                <!-- SECTION 2 //-->
                                                <div class="section2">
                                                    <div class="coll2">
                                                        <label for="name"><?=LABEL_NAME?> *</label>
                                                        <input type="text" name="name" id="name" value="">
                                                        <div class="scoll c1">
                                                        <label for="email"><?=LABEL_EMAIL?> *</label>
                                                        <input type="text" name="EMail" id="email">
                                                        </div>
                                                        <div class="scoll c2">
                                                            <label for="phone"><?=LABEL_PHONE?> *</label>
                                                            <input type="text" name="phone" id="phone" value="">
                                                        </div>
                                                    </div>
                                                    <div class="coll">
                                                        <label for="code"><?=LABEL_CODE?></label>
                                                        <div class="scoll c1">
														<?php
                                                             $code = rand(0, 65535);   
                                                            
                                                             echo("
                                                             <img src=\"http://www.maksoft.net/gen.php?code=$code\" width=\"100%\" style=\"margin-bottom: 17px;\"><br>
                                                             <input type=\"hidden\" name=\"code\" value=\"$code\">
                                                             "); 
                                                        ?>
                                                        </div>
                                                        <div class="scoll c2">
	                                                        <input name="codestr" type="text" id="code" style="width: 125px;">
                                                        </div>
                                                        <br clear="all">
                                                        <button class="btn btn-small btn-danger" type="button" id="book-now"><i class="icon-star icon-white"></i> <?=BUTTON_BOOK_NOW?></button>
                                                    </div>
                                                        <br clear="all">
                                                        <div class="alert">
                                                          <?=MESSAGE_MANDANTORY_RESERVATION_FIELDS?>
                                                        </div>
                                                    <input type="hidden" name="SiteID" value="<?=$o_page->SiteID?>">
                                                    <input type="hidden" name="n" value="<?=$o_page->n?>">
                                                    <input name="ToMail" type="hidden" id="ToMail" value="<?=$o_site->get_sEmail()?>">        
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                     </div>
                                     <div class="row">
                                        <div class="span12">
                							<?=$o_page->get_pText()?>
                                        </div>                                  
                                    </div>
                                </section>
                            </div>
                            <br clear="all">
                            <?php $o_page->print_pNavigation()?>                        
                        </div>
                    </div>
                </div>
            </div>        	
        </div>
    </div>
    <!--//page_container-->