    <!--page_container-->
    <div class="page_container">
    	<div class="wrap">
        	<div class="container">
                <div class="page_full white_bg drop-shadow">
                    <div class="breadcrumb">
                        <div class="container">
						<?=$o_page->print_pName("true", $o_page->_page['ParentPage'])?><span>/</span><?=$o_page->print_pName()?>
                        </div>
                    </div>
                    <div class="container">
                        <div class="page_in">
                        	<div class="container">
                                <section>
                                    <div class="row">
                                        <div class="span6">
                                            <h2 class="title"><?=$o_page->print_pTitle("strict")?></h2>
                                            <div id="map">
                                            	<iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps?q=41.643366,24.690955&amp;num=1&amp;ie=UTF8&amp;ll=41.643326,24.694777&amp;spn=0.032614,0.066047&amp;t=m&amp;z=14&amp;output=embed"></iframe>                              
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <div class="contact_form">  
                                                <?php
												if($sent)
												{
												?>
                                                <div class="alert alert-success">
                                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                  <strong>Message:</strong> Your message has been sent successfully!
                                                </div>
                                                <?php
												}
												?>
                                                <div id="fields">
                                                <form id="ajax-contact-form" action="form-cms.php" method="post">
                                                    <input class="span6" type="text" name="name" value="" placeholder="<?=LABEL_NAME?>" />
                                                    <input class="span6" type="text" name="EMail" value="" placeholder="<?=LABEL_EMAIL?>" />
                                                    <input class="span6" type="text" name="subject" value="" placeholder="<?=LABEL_SUBJECT?>" />
                                                    <textarea id="message2" name="message" class="span6" placeholder="<?=LABEL_MESSAGE?>"></textarea>
                                                    <div class="clear"></div>
                                                    <br>
                                                    <input type="reset" class="btn" value="<?=BUTTON_CLEAR?>" />
                                                    <input type="submit" class="btn dark_btn" value="<?=BUTTON_SUBMIT?>" />
                                                    <div class="clear"></div>
                                                    <br>
                                                        
                                                        <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
                                                        <input name="SiteID" type="hidden" id="SiteID" value="<?=$o_site->SiteID?>">
                                                        <input name="ToMail" type="hidden" id="ToMail" value="<?=$o_site->_site['EMail']?>"> 
                                                        <input name="conf_page" type="hidden" id="conf_page " value="<?=$o_page->get_pLink()?>&sent=1">
                                                </form>
                                                </div>
                                            </div>                   
                                        </div>

                                        <div class="clear"></div>
                                        <div class="span3 contact_text" style="border-top: 1px solid #2f6346"><?=$o_page->get_pText()?></div>
                                        <div class="span3" style="border-top: 1px solid #2f6346"></div>
                                        <div class="span6" style="border-top: 1px solid #2f6346"><br><?=$o_page->get_pText($n_about)?></div>                                        
                                                        	
                                    </div>
                                </section>
                            </div>
                            <br clear="all">
                            <?php $o_page->print_pNavigation()?>                        
                        </div>
                    </div>
                </div>
            </div>        	
        </div>
    </div>
    <!--//page_container-->