<!--page_container-->
<div class="page_container">
    <div class="wrap">
        <div class="container">
            <div class="page_full white_bg drop-shadow">
				<?php include_once TEMPLATE_DIR . "boxes/box_slider.php";?>
                                    
				<?php //include_once TEMPLATE_DIR . "boxes/box_planing.php";?>
                
                <!--text block-->
                <div class="block text_block">
                    <div class="container">
                        <div class="row">
                            <div class="span4">
							<?php include_once TEMPLATE_DIR. "boxes/box_welcome.php"; ?>
                            </div>
                            <div class="span4">
                            <?php include_once TEMPLATE_DIR . "boxes/box_about.php"; ?>                         
                            </div>
                            <div class="span4">
                            <?php include_once TEMPLATE_DIR . "boxes/box_news.php"; ?>
                            </div> 
                                        
                        </div>
                    </div>
                </div>
                <!--text block-->
                <div class="container"><div class="border"></div><div class="clear"></div></div>             
                <!--featured works-->
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="span6">
                            <?php include_once TEMPLATE_DIR . "boxes/box_featured_works.php";?>                           
                            </div>
                            <div class="span6">
                            	<h2 class="title"><?=$o_page->get_pName($n_info_map2)?></h2>
                                <? $o_page->print_pImage(570, "", "", $n_info_map2)?>
                            </div>
                                            
                        </div>   
                    </div>
                </div>        
                <!--//featured works-->
                <div class="container"><div class="border"></div><div class="clear"></div></div>   
                <!--featured works-->
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="span6">
                            <?php include_once TEMPLATE_DIR . "boxes/box_latest_posts.php";?>                           
                            </div>
                            <div class="span6">
                            <?php include_once TEMPLATE_DIR . "boxes/box_location.php";?>
                            </div>
                                            
                        </div>   
                    </div>
                </div>        
                <!--//featured works-->                                  
          </div>
        <br clear="all">
        <?php $o_page->print_pNavigation()?>   
        </div>    
    </div>        
</div>
<!--//page_container-->