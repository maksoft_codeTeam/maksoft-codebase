<!--footer-->
<div id="footer">
    <div class="wrap">
        <div class="container">
            <div class="page_full white_bg drop-shadow round_footer">
                <div class="container">
                    <div class="row">
                        <div class="span3">
						<?php include_once TEMPLATE_DIR . "boxes/box_booking.php";?>                    
                        </div>
                        <div class="span3">
						<?php include_once TEMPLATE_DIR . "boxes/box_comments.php"; ?>
                        </div> 
                        <div class="span3">
                            <h2 class="title3"><span><?=TITLE_CONTACTS?></span></h2>
                            <form id="contact_form" method="post" action="form-cms.php">
                                <input class="span3" type="text" name="name" id="name" value="Name" onFocus="if (this.value == 'Name') this.value = '';" onBlur="if (this.value == '') this.value = 'Name';" />
                                <input class="span3" type="text" name="email" id="email" value="Email" onFocus="if (this.value == 'Email') this.value = '';" onBlur="if (this.value == '') this.value = 'Email';" />
                                <textarea name="message" id="message" class="span3" onFocus="if (this.value == '<?=LABEL_MESSAGE?>...') this.value = '';" onBlur="if (this.value == '') this.value = '<?=LABEL_MESSAGE?>...';" ><?=LABEL_MESSAGE?>...</textarea>
                                <div class="clear"></div>
                                <input type="reset" class="btn dark_btn" value="<?=BUTTON_CLEAR?>" />
                                <input type="submit" class="btn btn-success" value="<?=BUTTON_SUBMIT?>" />
                                <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
                                <input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
                                <input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$Site->EMail"); ?>"> 
                                <input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$Site->StartPage&SiteID=$SiteID&sent=1"); ?>">
                                <input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
                                <input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">
                                <div class="clear"></div>
                            </form>                 
                        </div>

                        <div class="span3">
                        <?php
                            $categories = $o_page->get_pGroupContent($n_gallery);
                        ?>
                            <h2 class="title3"><span><?=TITLE_GALLERY?></span></h2>
                            <div class="flickrs">
                                <div class="FlickrImages">
                                    <ul>
                                    <?
                                    $counter = 0;
                                    for($j=0; $j<count($categories); $j++)
                                    {
                                        
                                       //$gallery = $o_page->get_pSubpages($categories[$j]['n'], "p.sort_n ASC", "im.image_src !=''");
                                       $gallery = $o_page->get_pSubpages($categories[$j]['n'], "RAND()", "im.image_src !=''");
                                        for($i=0; $i<count($gallery); $i++)
                                            {
                                                if($counter<12)
                                                {
                                                ?>
                                                    <li><a href="<?=HTTP_SERVER . "/".$gallery[$i]['image_src']?>" rel="prettyPhoto[smallGallery]"><img src="<?=HTTP_SERVER . "/".$gallery[$i]['image_src']?>" width="63px" alt="" /></a></li>        
                                                <?
                                                }
                                                $counter++;	
                                            }
                                    }
                                    ?>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                            </div>
        	
                    </div>
                </div>
            </div>
        </div>            
    </div>        
    
    <div class="footer_bottom">
        <div class="wrap">
            <div class="container">
                <div class="page_full">
                    <div class="fleft">
                        <div class="copyright"><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="????????, SEO ???????????" target="_blank">Netservice</a> |  <a href="#" class="">Privacy Policy</a></div>                        
                    </div>
                    <div class="fright">
                        <div class="foot_menu">
						<?php print_menu($o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC"), array('menu_depth'=>0, 'home'=>true))?>
                        </div>
                    </div>
                    
                </div>	
            </div>
        </div>
    </div>
</div>
<!--//footer-->    

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.mobile.customized.min.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/camera.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/superfish.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.tweet.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.ui.totop.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/myscript.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">

	//no conflict with Prototype Lib
	var $ = jQuery.noConflict();

	function check_reservation(step)
		{
			//return true;
			$('#date_from').removeClass("error");
			$('#date_to').removeClass("error");
			$('#name').removeClass("error");
			$('#phone').removeClass("error");
			$('#email').removeClass("error");
			if(step == 1)
				{
					if($('#date_from').val() != "" && $('#date_to').val() != "")
						return true;
					else 
					{
						if($('#date_from').val() == "") $('#date_from').addClass("error");
						if($('#date_to').val() == "") $('#date_to').addClass("error"); 
						return false;
					}
				}
			if(step == 2)
				{
					if($('#name').val() != "" && $('#phone').val() != "" && $('#email').val() != "")
						return true;
					else 
					{
						if($('#name').val() == "") $('#name').addClass("error");
						if($('#phone').val() == "") $('#phone').addClass("error"); 
						if($('#email').val() == "") $('#email').addClass("error"); 
						return false;
					}

				}
			//full reservation form
			if(step == 3)
				{
					if($('#date_from').val() != "" && $('#date_to').val() != "" && $('#name').val() != "" && $('#phone').val() != "" && $('#email').val() != "")
						return true;
					else 
					{
						if($('#date_from').val() == "") $('#date_from').addClass("error");
						if($('#date_to').val() == "") $('#date_to').addClass("error"); 
						if($('#name').val() == "") $('#name').addClass("error");
						if($('#phone').val() == "") $('#phone').addClass("error"); 
						if($('#email').val() == "") $('#email').addClass("error"); 
						return false;
					}

				}

		}	
    $(document).ready(function(){	
        //Slider
        $('#camera_wrap_1').camera();
        
        //Hover_img
        $(".hover_img").live('mouseover',function(){
                var info=$(this).find("img");
                info.stop().animate({opacity:0.6},500);
                $(".preloader").css({'background':'none'});
            }
        );
        $(".hover_img").live('mouseout',function(){
                var info=$(this).find("img");
                info.stop().animate({opacity:1},500);
                $(".preloader").css({'background':'none'});
            }
        );	
        
        //Featured works & latest posts
        $('#mycarousel, #mycarousel2').jcarousel();		

		//RESERVATION FORM
		$('#book-next').click(function(){
			if(check_reservation(1))
			{
				$('#short-reservation-form').submit();
			}
			else 
			{
				$('.alert').fadeIn();
			}
			})

		$('.full #book-now').click(function(){
			if(check_reservation(3))
				{
					$('#short-reservation-form').submit();
				}
			else $('.alert').fadeIn();

			})
			
		$('#book-back').click(function(){
			$('.section1').toggle();
			$('.section2').toggle();
			})

		//RESREVATION FORM DATE PICKER

		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		 
		var checkin = $('#date_from').datepicker({
		  onRender: function(date) {
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  if (ev.date.valueOf() > checkout.date.valueOf()) {
			var newDate = new Date(ev.date)
			newDate.setDate(newDate.getDate() + 1);
			checkout.setValue(newDate);
		  }
		  checkin.hide();
		  $('#date_to')[0].focus();
		}).data('datepicker');
		var checkout = $('#date_to').datepicker({
		  onRender: function(date) {
			return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  checkout.hide();
		}).data('datepicker');
													
    });		
</script>