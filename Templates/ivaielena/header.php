<!DOCTYPE html>
<head>
<meta charset="windows-1251">
<title><?=$Title?></title>
<?php
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
		if($o_page->_site['language_id'] == 1)  {
			include_once TEMPLATE_DIR."languages/bg.php";
			echo  '<html lang="bg">'; 
		}		
		if($o_page->_site['language_id'] == 2)  {
			include_once TEMPLATE_DIR."languages/en.php";
			echo  '<html lang="en">'; 
		}
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<link href="<?=ASSETS_DIR?>css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" id="camera-css"  href="<?=ASSETS_DIR?>css/camera.css" type="text/css" media="all">
<link href="<?=ASSETS_DIR?>css/bootstrap.css" rel="stylesheet">
<link href="<?=ASSETS_DIR?>css/theme.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>css/skins/tango/skin.css" />
<link href="<?=ASSETS_DIR?>css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?=ASSETS_DIR?>css/bootstrap-datepicker.css" rel="stylesheet">

<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->    
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>
<body>

	<!--header-->
    <div class="header">
    	<div class="header_in">
        	<div class="wrap">
                <div class="container">
                    <div class="page_full">
                        <div class="header_top">
                            <div class="container">
                                <div class="row">
                                    <div class="span4">
                                        <div class="logo"><a href="<?=$o_page->get_pLink($o_site->get_sStartPage())?>"><img src="<?=$tmpl_config['default_logo']?>" alt="<?=$o_site->_site['Title']?>" width="<?=$tmpl_config['default_logo_width']?>" /></a></a><?=$o_site->print_sVersions()?></div>
                                        <div class="clear"></div>     
                                    </div>
                                    <div class="span4">
                                    	<a href="<?=$o_page->get_pLink($o_site->get_sStartPage())?>"><img src="<?=TEMPLATE_IMAGES?>phone-call.png" alt=""/></a>
                                    </div>
                                    <div class="span4">
                                        <div class="follow_us">
                                            <ul>
                                                <li><a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" class="facebook">Facebook</a></li>
                                                <li><a href="#" class="twitter">Twitter</a></li>
                                                <li><a href="#" class="vimeo">Vimeo</a></li>
                                                <li><a href="#" class="delicious">Delicious</a></li>
                                                <li><a href="#" class="rss">rss</a></li>
                                            </ul>
                                        </div>
                                        <div class="phone">GPS: <a href="https://www.google.com/maps/place/41%C2%B038'36.3%22N+24%C2%B041'27.9%22E/@41.6436175,24.6906976,19z/data=!4m2!3m1!1s0x0:0x0" target="_blank">41.64342, 24.691091</a></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="top_menu">
                        	<div class="fright">
                            	<div class="top_search">
                                    <form action="#" method="post">
                                        <input name="name" type="text" value="Search the Site..." onfocus="if (this.value == 'Search the Site...') this.value = '';" onblur="if (this.value == '') this.value = 'Search the Site...';" />
                                        <input type="submit" class="search_btn" value=" " />
                                        <div class="clear"></div>
                                    </form> 
                                </div>
                            </div>
                            <div class="fleft">
                                <!--menu-->
                                <nav id="main_menu">
                                    <div class="menu_wrap">
                                    <?php print_menu($o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 5"), array('menu_depth'=>1, 'params'=>"class=\"nav sf-menu\"", 'home'=>true))?>
                                  	<span class="partner-link">
									<a href="https://www.facebook.com/BoutiqueHotelIvaElena?fref=nf"><img src="<?=TEMPLATE_IMAGES?>facebook-stick.png" alt="" class="fbstick"></a>
									</span>
                                    <div class="clear"></div>
                                    </div>
                                </nav>                        
                                <!--//menu --> 
                            </div>
                            <div class="clear"></div>                       
                        </div>
                    </div>
                </div>
        	</div>           
        </div>    
    </div>
    <!--//header-->    