<?php echo "<?xml version=\"1.0\" encoding=\"windows-1251\"?".">"; ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<title><?php echo("$Title"); ?></title>
<?php
include "Templates/meta_tags.php"; 
?>
<link href="http://www.maksoft.net/Templates/metamorph_greenisland/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>

<div id="main_bg">
<div id="main">
<!-- header begins -->
<div id="header">
    <div id="logo">
      <h2><?php $o_page->print_pTitle(); ?></h2>
    </div>
    <div id="buttons">
          <div class="but1"  ><a href="#" class="but"  title="">Home</a></div>
          <div class="but2" ><a href="#" class="but" title="">Blog</a></div>
          <div class="but3" ><a href="#"  class="but" title="">Gallery</a></div>
          <div class="but4" ><a href="#"  class="but" title="">About us</a></div>
          <div class="but5" ><a href="#" class="but" title="">Contact us</a></div>
    </div>
</div>
<!-- header ends -->
    <!-- content begins -->
    
    	<div id="content">
        	<div id="right">
			<?php
			 $o_page->print_pTags(); 
			?>


            	
            	<h1>Metamorphosis Design</h1>
                <div class="right_b"><span>Proin at dolor quis diam feugiat euismod. Aliquam erat volutpat. </span><br /><br />
                    Mauris metus justo, accumsan dignissim elementum sit amet, posuere et nibh. Suspend- isse ultricies nunc consectetur ipsum dapibus in tempus odio consequat. Nulla non eros et nibh <br />
                    <div class="read"><a href="#">read more</a></div><br />
              	</div>
                <div class="right_b"><span>Proin at dolor quis diam feugiat euismod. Aliquam erat volutpat. </span><br /><br />
                    Mauris metus justo, accumsan dignissim elementum sit amet, posuere et nibh. Suspend- isse ultricies nunc consectetur ipsum dapibus in tempus odio consequat. Nulla non eros et nibh <br />
                    <div class="read"><a href="#">read more</a></div><br />
              	</div>
                <div class="right_b"><span>Proin at dolor quis diam feugiat euismod. Aliquam erat volutpat. </span><br /><br />
                    Mauris metus justo, accumsan dignissim elementum sit amet, posuere et nibh. Suspend- isse ultricies nunc consectetur ipsum dapibus in tempus odio consequat. Nulla non eros et nibh <br />
                    <div class="read"><a href="#">read more</a></div><br />
              	</div>
              <br />
              
           	</div>  
            <div id="left">
           	  <div class="tit_bot">
			  <?php
			  			 $o_page->print_pContent();
						 $o_page->print_pSubContent(); 
						 $o_page->print_pPHPcode(); 
						 $o_page->print_pNavigation(); 
				?>
       		    <h1>Metamorphosis Design</h1>
                  	<div class="text">
                    	<img src="images/img.jpg" width="120" height="80" class="img" alt="" /><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </span> <br /><br />
                    	Duis rhoncus nibh et felis facilisis vel consequat ipsum cursus. Etiam consequat turpis at metus euismod sit amet euismod elit malesuada. Maecenas vestibulum nisl quis sem dapi- bus consectetur. Phasellus mattis urna eu eros laoreet condimentum. Cras lectus felis... <br />
                    	<div class="read"><a href="#">read more</a></div><br />
           	  	</div>
                    <div class="text">
                    	<img src="images/img2.jpg" width="120" height="80" class="img" alt="" /><span>Integer sit amet nulla eu lectus pulvinar volutpat. Phasellus congue leo eget lacus posuere hendrerit. </span> <br />
                    	<br />
                    	Donec id massa purus, ac porta mi. Sed facilisis vestibulum risus ut dapibus. Integer cursus aliquam pellentesque. Vestibulum vestibulum iaculis nibh ut ultricies. Fusce vel sem sit amet metus ornare congue. Nulla laoreet luctus mauris at pharetra. Suspendisse id nulla elit. <br />
                    	<div class="read"><a href="#">read more</a></div><br />
           	  	</div>
              	</div>
								
								<div class="tit_bot">
           		  <h1>Web Design Starter's Guide</h1>
                  	<div class="text"><ol>
				<li><a href="http://www.metamorphozis.com/free_templates/free_templates.php">More Free Website Templates</a></li>
				<li><a href="http://www.metamorphozis.com/shop/flash_templates.php">Flash Templates</a></li>
                <li><a href="http://www.metamorphozis.com/website_hosting/index.php">Top Hosting Providers</a></li>
                <li><a href="http://www.metamorphozis.com/contact/contact.php">Support For Free Website Templates</a></li>
             
		  </ol>
                    	<div class="read"><a href="#">read more</a></div><br />
           	  	  </div>
                    
           	  </div>
								
                <div class="tit_bot">
           		  <h1>free website templates</h1>
                  	<div class="text"><span>Phasellus tellus lorem, vulputate sed tincidunt non, lacinia et odio. Nulla lacinia egestas tortor sed aliquam. Vestibulum felis ligula, lacinia ut malesuada et, rhoncus eget nisl. Fusce venenatis condimentum dolor vitae dictum. </span> <br />
                  	  <br />
                    	Pellentesque egestas lacus at dui vulputate posuere tempus nunc vestibulum. In nisl elit, iaculis at volutpat non, mattis sit amet magna. Quisque aliquam eleifend turpis vel viverra. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo placerat hendrerit. Nam rhoncus auctor massa, aliquam porttitor enim commodo consequat. Sed justo <br />
                    	<div class="read"><a href="#">read more</a></div><br />
           	  	  </div>
                    
           	  </div>
            </div>
            <br />
            <div style="clear: both"><img src="images/spaser.gif" alt="" width="1" height="1" /></div>
         <!-- footer begins -->
            <div id="footer">
          Copyright  2010. Designed by <a href="http://www.metamorphozis.com/" title="Flash Templates">Flash Templates</a><br />
                <a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a> | <a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional"><abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a> | <a href="http://jigsaw.w3.org/css-validator/check/referer" title="This page validates as CSS"><abbr title="Cascading Style Sheets">CSS</abbr></a></div>
        <!-- footer ends -->
        </div>
    <!-- content ends -->
</div>
</div>
</body>
</html>
