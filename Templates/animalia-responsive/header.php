<!DOCTYPE html>
<html lang="<?php echo($Site->language_key); ?>">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251"/>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		<?=$Title?>
	</title>
	<?php
	require_once "lib/lib_page.php";
	require_once "lib/Database.class.php";
	//include meta tags
	include( "Templates/meta_tags.php" );
	$o_site->print_sConfigurations();
	$o_site = new site();
	$db = new Database();
	$o_page->setDatabase( $db );
	$o_user = new user( $user->username, $user->pass );
	?>

	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/reset.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/style.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/custom.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/responsive.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/woocommerce.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/pe-icon-7-stroke.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/stroke-gap-icons-style.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/plugins/shortcodes/css/animations.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/plugins/shortcodes/css/shortcodes.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/plugins/revslider/css/settings.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/plugins/vc_addons/assets/min-css/ultimate.min.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/plugins/vc_addons/assets/css/icons.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/plugins/js_composer/assets/css/js_composer.min.css' type='text/css' media='all'/>
    <link href="https://fonts.googleapis.com/css?family=Merriweather|Neucha" rel="stylesheet">
    
	<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery/jquery.js'></script>
	<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/revslider/js/jquery.themepunch.tools.min.js'></script>
	<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/revslider/js/jquery.themepunch.revolution.min.js'></script>
	<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery/ui/core.min.js'></script>
	<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/vc_addons/assets/min-js/ultimate.min.js'></script>
	<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/modernizr.custom.js'></script>
	<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/slider-settings.js'></script>

</head>

<body class="layout-wide split-header fullwidth-header sticky-header standard-header woo-type6 page-with-slider">

	<div class="wrapper">
		<div class="inner-wrapper">
			<div id="header-wrapper" class="">

				<header id="header">
<div class="header-top">
		<div class="container">
			<div class="row">

				<div class="header-top-left wpb_column vc_column_container vc_col-sm-6">
					<ul class="social-icons float-left">
						<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
						<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
						</li>
					</ul>
				</div>

				<div class="header-top-right wpb_column vc_column_container vc_col-sm-6">
					<ul class="user-links float-right">
						<li><a href="#" class="top-header-link"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>����</a>
						</li>
						<li><a href="#" class="top-header-link"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>�����������</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
					<div id="main-header-wrapper" class="main-header-wrapper">
						<div class="container">

							<div class="main-header">
								<div id="logo"> <a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>" title="��������">
				<img class="normal_logo" src="<?=TEMPLATE_DIR?>assets/img/logo_animalia_x2.png" alt="��������" title="��������" />
				<img class="retina_logo" src="<?=TEMPLATE_DIR?>assets/img/logo_animalia_x2.png" alt="��������" title="��������" style="width:120px; height:auto;"/>
			</a>
								
								</div>
								<div id="menu-wrapper" class="menu-wrapper">
									<div class="dt-menu-toggle" id="dt-menu-toggle">
										���� <span class="dt-menu-toggle-icon"></span>
									</div>
									<nav id="main-menu">
										<ul id="menu-main-menu" class="menu menu-left">
											<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-0 menu-item-simple-parent "><a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>"><i class='fa fa-home'></i>������</a>
											</li>
											<?php
											if ( !isset( $fr_links ) )$fr_links = 3;
											$fr_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $fr_links" );

											for ( $i = 0; $i < count( $fr_links ); $i++ ) {
												$subpages = $o_page->get_pSubpages( $fr_links[ $i ][ 'n' ] );
												?>
											<li <?php echo ((count($subpages)> 0) ? "class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-0 menu-item-simple-parent \"" : "") ?>>
												<a href="<?php echo $o_page->get_pLink($fr_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <i class='fa fa-paw'></i><?php echo $fr_links[$i]['Name'] ?></a>
												<?php echo ((count($subpages) > 0) ? "<ul class=\"sub-menu \">" : "</li>") ?>
												<?php
												if ( count( $subpages ) > 0 ) {


													for ( $j = 0; $j < count( $subpages ); $j++ )
														echo "<li class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1\"><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\">" . $subpages[ $j ][ 'Name' ] . "</a></li>";
													?>
										</ul><a class="dt-menu-expand" href="#">+</a>
										</li>
										<?php 
} 
}
?>

										</ul>
										<ul id="menu-secondary-menu" class="menu menu-right">

											<?php
											if ( !isset( $se_links ) )$se_links = 5;
											$se_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $se_links" );

											for ( $i = 0; $i < count( $se_links ); $i++ ) {
												$subpages = $o_page->get_pSubpages( $se_links[ $i ][ 'n' ] );
												?>
											<li <?php echo ((count($subpages)> 0) ? "class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-0 menu-item-simple-parent \"" : "") ?>>
												<a href="<?php echo $o_page->get_pLink($se_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <i class='fa fa-paw'></i><?php echo $se_links[$i]['Name'] ?></a>

										</li>
										<?php 

}
?>

										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>