<footer id="footer">
	<div class="footer-widgets dt-sc-dark-bg">
		<div class="container">
			<div class='column dt-sc-three-sixth first'>
				<aside id="text-4" class="widget widget_text">
					<h3 class="widgettitle"> <span class="fa fa-paw"></span>�� ��������</h3>
					<div class="textwidget">
						<p>��� ��� ����� �� ���� �� ����� � ��������� �� ������� �������. ���������� ����� ����� �� ������ ������ ���������� �������������, �� ����� ��� ������������ ������������� �� ��������.</p>
						<div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span>
						</div>
						<ul class='dt-sc-sociable '>
							<li> <a class='fa fa-facebook' title='Facebook' href='#'> </a> </li>
							<li> <a class='fa fa-twitter' title='Twitter' href='#'> </a> </li>
							<li> <a class='fa fa-google-plus' title='Google-plus' href='#'> </a> </li>
						</ul>
					</div>
				</aside>
			</div>
			<div class='column dt-sc-one-sixth '>
				<aside id="text-5" class="widget widget_text">
					<h3 class="widgettitle"> <span class="fa fa-paw"></span>���������</h3>
					<div class="textwidget">
						<ul class='dt-sc-fancy-list  yellow  circle-bullet'>
						<?php

						if (!isset($footer_links)) $footer_links = 5;
						$footer_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $footer_links");

						for ($i = 0; $i < count($footer_links); $i++) {
							$subpages = $o_page->get_pSubpages($footer_links[$i]['n']);
						?>
												<li><a href="<?php echo $o_page->get_pLink($footer_links[$i]['n']) ?>"><?php echo $o_page->get_pName($footer_links[$i]['n']) ?></a></li>

						<?php  
						}
						?>	
						</ul>
					</div>
				</aside>
			</div>
			<div class='column dt-sc-one-sixth '>
				<aside id="text-6" class="widget widget_text">
					<h3 class="widgettitle"> <span class="fa fa-paw"></span>�����������</h3>
					<div class="textwidget">
						<ul class='dt-sc-fancy-list  yellow  circle-bullet'>
							<li><a href="#">����</a>
							</li>
							<li><a href="#">�����������</a>
							</li>
							<li><a href="#">��������� ������</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
			<div class='column dt-sc-one-sixth '>
				<aside id="text-7" class="widget widget_text">
                <div class="logo_footer">
                <a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>" title="��������">
				<img src="<?=TEMPLATE_DIR?>assets/img/logo_animalia_x2.png" alt="��������" title="��������" />
				</a>
				<div class="phone-number">
					<i class="fa fa-phone-square" aria-hidden="true"></i> <a href="tel:070029920">0700 299 20</a>
				</div>
				</div>
				</aside>
			</div>
		</div>
	</div>
	<div class="footer-copyright dt-sc-dark-bg">
		<div class="container">
			<div class="wpb_column vc_column_container vc_col-sm-6">
				<div class="vc_column-inner ">
					<div class="wpb_wrapper">

						� 2009 - 2017  ��������

					</div>
				</div>
			</div>
			<div class="alignright wpb_column vc_column_container vc_col-sm-6">
				<div class="vc_column-inner ">
					<div class="wpb_wrapper">

						<ul id="menu-footer-menu" class="menu-links ">
							<li id="menu-item-1638" class="menu-item menu-item-type-custom menu-item-object-custom webdesign">��� ������ � ��������� �� <a href="http://www.maksoft.net">�������</a>
							</li>
						</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

            </div> 
            </div>

<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Chewy%3Aregular&amp;ver=4.6.3' type='text/css' media='all'/>
<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Caveat%3Aregular%2C700&amp;ver=4.6.3' type='text/css' media='all'/>
<link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/plugins/js_composer/assets/css/js_composer_tta.min.css' type='text/css' media='all'/>
<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Abril+Fatface%3Aregular&amp;ver=4.6.3' type='text/css' media='all'/>

<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/shortcodes/js/jquery.tabs.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/shortcodes/js/jquery.tipTip.minified.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/shortcodes/js/jquery.inview.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/shortcodes/js/jquery.animateNumber.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/shortcodes/js/jquery.donutchart.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/shortcodes/js/jquery.scrollto.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/shortcodes/js/shortcodes.js'></script>

<script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/ie/html5shiv.min.js?ver=3.7.2'></script>
<![endif]-->
<!--[if lt IE 8]>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/ie/excanvas.js?ver=2.0'></script>
<![endif]-->
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.ui.totop.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.plugins.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.visualNav.min.js'></script>

<script type='text/javascript'>
/* <![CDATA[ */
var dttheme_urls = {"theme_base_url":"http:\/\/wedesignthemes.com\/themes\/petsworld\/wp-content\/themes\/petworld","framework_base_url":"http:\/\/wedesignthemes.com\/themes\/petsworld\/wp-content\/themes\/petworld\/framework\/","ajaxurl":"http:\/\/wedesignthemes.com\/themes\/petsworld\/wp-admin\/admin-ajax.php","url":"http:\/\/wedesignthemes.com\/themes\/petsworld","stickynav":"enable","stickyele":".main-header-wrapper","isRTL":"","loadingbar":"disable","nicescroll":"disable"};
/* ]]>*/
</script>

<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/custom.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/js_composer/assets/lib/vc_accordion/vc-accordion.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/js_composer/assets/lib/vc-tta-autoplay/vc-tta-autoplay.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/js_composer/assets/lib/vc_tabs/vc-tabs.min.js'></script>

</body> 
</html>