		<?php include TEMPLATE_DIR . "inc/slider.php"; ?>

	</header> 
</div>

<div id="main">
	<div class="container">
		<section id="primary" class="content-full-width">
			<div class="page type-page status-publish hentry">

				<?php include TEMPLATE_DIR . "inc/categories.php"; ?>

				<?php include TEMPLATE_DIR . "inc/top-products.php"; ?>

				<?php include TEMPLATE_DIR . "inc/our-mission.php"; ?>

				<?php include TEMPLATE_DIR . "inc/boxes.php"; ?>

			</div>
		</section>
	</div>
</div>