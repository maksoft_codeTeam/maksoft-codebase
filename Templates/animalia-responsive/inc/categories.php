<div class="vc_row wpb_row vc_row-fluid zindextop">
					<div class="wpb_column vc_column_container vc_col-sm-6">
						<div class="vc_column-inner ">
							<div class="wpb_wrapper">
								<div class='dt-sc-icon-box type1 '>
									<div class="icon-wrapper"><img width="180" height="290" src="<?=TEMPLATE_DIR?>uploads/2016/10/pet3.png" class="attachment-full" alt="<?=$o_page->get_pName(45288);?>"/>
									</div>
									<div class="icon-content">
										<h4 style='color:#a3ad55'><?=$o_page->get_pName(45288);?></h4>
										<div class="dt-sc-icon-box-content">
										  <?php 
											$dogs = $o_page->get_pSubpages(45288);

												for($i=0; $i<count($dogs); $i++)
													{
										  ?>
											<a href="<?=$o_page->get_pLink($dogs[$i]['n'])?>" class="dogsbg"><?=$dogs[$i]['Name']?></a>
										  <?php } ?> 

										</div>
									</div>
								</div>
								<div class='dt-sc-icon-box type1 '>
									<div class="icon-wrapper"><img class="box120" src="<?=TEMPLATE_DIR?>uploads/2016/10/pet1.png" class="attachment-full" alt="<?=$o_page->get_pName(45290);?>"/>
									</div>
									<div class="icon-content">
										<h4 style='color:#79b3ce'><?=$o_page->get_pName(45290);?></h4>
										<div class="dt-sc-icon-box-content">
										  <?php 
											$birds = $o_page->get_pSubpages(45290);

												for($i=0; $i<count($birds); $i++)
													{
										  ?>
											<a href="<?=$o_page->get_pLink($birds[$i]['n'])?>" class="birdsbg"><?=$birds[$i]['Name']?></a>
										  <?php } ?> 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="wpb_column vc_column_container vc_col-sm-6">
						<div class="vc_column-inner ">
							<div class="wpb_wrapper">
								<div class='dt-sc-icon-box type1 '>
									<div class="icon-wrapper"><img width="180" height="290" src="<?=TEMPLATE_DIR?>assets/img/category/cat.png" class="attachment-full" alt="<?=$o_page->get_pName(45289);?>"/>
									</div>
									<div class="icon-content">
										<h4 style='color:#a38262'><?=$o_page->get_pName(45289);?></h4>
										<div class="dt-sc-icon-box-content">
										  <?php 
											$cats = $o_page->get_pSubpages(45289);

												for($i=0; $i<count($cats); $i++)
													{
										  ?>
											<a href="<?=$o_page->get_pLink($cats[$i]['n'])?>" class="catsbg"><?=$cats[$i]['Name']?></a>
										  <?php } ?> 
										</div>
									</div>
								</div>
								<div class='dt-sc-icon-box type1 '>
									<div class="icon-wrapper"><img class="box120" src="<?=TEMPLATE_DIR?>uploads/2016/10/pet4.png" class="attachment-full" alt="<?=$o_page->get_pName(45291);?>"/>
									</div>
									<div class="icon-content">
										<h4 style='color:#a36eb9'><?=$o_page->get_pName(45291);?></h4>
										<div class="dt-sc-icon-box-content">
										  <?php 
											$gri = $o_page->get_pSubpages(45291);

												for($i=0; $i<count($gri); $i++)
													{
										  ?>
											<a href="<?=$o_page->get_pLink($gri[$i]['n'])?>" class="gribg"><?=$gri[$i]['Name']?></a>
										  <?php } ?> 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner ">
							<div class="wpb_wrapper">
								<div class='dt-sc-icon-box type1 '>
									<div class="icon-wrapper"><img class="box120" src="<?=TEMPLATE_DIR?>assets/img/category/aqua.png" class="attachment-full" alt="<?=$o_page->get_pName(60166);?>"/>
									</div>
									<div class="icon-content">
										<h4 style='color:#4dc0f5'><?=$o_page->get_pName(60166);?></h4>
										<div class="dt-sc-icon-box-content">
										  <?php 
											$aqua = $o_page->get_pSubpages(60166);

												for($i=0; $i<count($aqua); $i++)
													{
										  ?>
											<a href="<?=$o_page->get_pLink($aqua[$i]['n'])?>" class="aquabg"><?=$aqua[$i]['Name']?></a>
										  <?php } ?> 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>