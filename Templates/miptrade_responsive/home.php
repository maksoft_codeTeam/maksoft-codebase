	
<div class="col-md-12">
        <div class="carousel slide" id="myCarousel">
          <div class="carousel-inner">
				<?php
				$i=0;
				foreach($products as $product){
					$i++;
					$src=$product['image_src'];
					
					?>
					<div class="item <?php if($i==1){echo "active";}?>">
					  <a href="<?=$product['page_link'];?>" title="<?=$product['title'];?>"><?=$o_page->print_timthumb($src, "", "400", "280");?></a>
					  <p class="carousel-caption"><?=$product['title'];?></p>
					</div>
					<?php
				}
				?>
          </div>
          <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>
        </div>
		<script>
		jQuery(document).ready(function(){
			jQuery('#myCarousel, #myCarousel2').carousel({
			  interval: 3000
			});			
		})
		</script>
</div>
	<div class="row margin0" id="maincontent">
	<hr>
		<div class="text-center">
			<?php include DIR_MODULES . "listing/list_languages.php";?>
		</div>
	<hr>
		<div class="row padding10 margin0" id="parralaxed">
			<p><?php echo crop_text($o_page->get_pText(21843));?></p>
			<hr>
		</div>
		<div class="row margin0">
			<div class="carousel slide" id="myCarousel2">
			  <div class="carousel-inner">
			  <?php
				$i=0;
				foreach($home_gallery as $element){
					$i++;
					$src=$element['image_src'];
					
					?>
					<div class="item <?php if($i==1){echo "active";}?>">
					  <a href="<?=$element['page_link'];?>" title="<?=$element['title'];?>"><?=$o_page->print_timthumb($src, "", "400", "280");?></a>
					  <p class="carousel-caption"><?=$element['title'];?></p>
					</div>
					<?php
					
				}
				?>
			  </div>
			  <a class="left carousel-control" href="#myCarousel2" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
			  <a class="right carousel-control" href="#myCarousel2" data-slide="next"><i class="fa fa-chevron-right"></i></a>
			</div>
		</div>
		<hr>
		<div class="row margin0">
		<script src="http://www.jqueryscript.net/demo/Responsive-jQuery-News-Ticker-Plugin-with-Bootstrap-3-Bootstrap-News-Box/scripts/jquery.bootstrap.newsbox.min.js"></script>
        <div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="fa fa-list-alt"></span><b> <?=EVENTS;?></b>
			</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12">
							<ul class="demo1 padding0" style="overflow-y: hidden; height: 211px;">
							<?php
							foreach($news as $post){
								?>
								<li style="" class="news-item">
									<table cellpadding="4">
										<tbody>
											<tr>
												<td>
													<a href="<?=$post['page_link'];?>">
														<strong>
															<?=$post['Name'];?>
														</strong>
													</a>
													<p><?=substr(strip_tags(crop_text($post['textStr'])), 0, 80);?></p>
												</td>
											</tr>
										</tbody>
									</table>
								</li>
								
								<?php
							}
							?>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel-footer">
				
				</div>
			</div>
		</div>
		<script>
		$(function () {
			$(".demo1").bootstrapNews({
				newsPerPage: 3,
				autoplay: true,
				pauseOnHover:true,
				direction: 'down',
				newsTickerInterval: 4000,
				onToDo: function () {
					//console.log(this);
				}
			});
        });
		</script>
		</div>
		<div class="row margin0">
				<?php 
				$o_page->print_pContent();
				//print php code
				//$o_page->print_pSubContent(NULL, 1, true);
				
				eval($o_page->get_pPHPcode());
				$o_page->print_pURL();
				
				if ($user->AccessLevel >= $row->SecLevel){
					include_once("$row->PageURL");
				}
				
				?>
		</div>