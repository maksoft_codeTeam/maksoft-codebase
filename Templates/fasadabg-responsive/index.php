<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo("$Title"); ?></title>

    <?php include "config.php"; ?>

    <!-- Bootstrap Core CSS -->
    <link href="<?=DIR_TEMPLATE?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=DIR_TEMPLATE?>assets/css/style.css" rel="stylesheet">
    <link href="<?=DIR_TEMPLATE?>assets/css/base_style.css" rel="stylesheet">
    <link href="<?=DIR_TEMPLATE?>assets/css/layout.css" rel="stylesheet">

    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
        
<div id="site_container">
 <div id="page_bg">
 
  <div class="container">
        <div class="row">
        
    <div id="header">
        	<?php include DIR_TEMPLATE."header.php"?>
    </div>
    
	<div id="page_container">
        <div class="main-content">
        <?php
			if($o_page->_page['n'] == $o_site->_site['StartPage'])
				include DIR_TEMPLATE . "home.php";
			elseif($o_page->_page['SiteID'] == $o_site->_site['SitesID'] )
				include DIR_TEMPLATE . "main.php";
			else
				include DIR_TEMPLATE . "admin.php";
		?>


        </div>
    </div>
    <div class="main-shadow"></div>
    
      </div>
    </div>
</div>
</div>

    <div id="footer">
    	<div class="footer-content" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
		<?php
			include DIR_TEMPLATE . "footer.php";
		?>
        <div id="navbar"><?=$o_page->print_pNavigation()?></div>
        <br clear="all">
        <div class="copyrights"><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a></div>
        </div> 
    </div>

    <!-- jQuery -->
    <script src="<?=DIR_TEMPLATE?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=DIR_TEMPLATE?>assets/js/bootstrap.min.js"></script>
    
    <!-- ������������ ��������� -->
        <script type="text/javascript">
    $(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');
});
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/bg_BG/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<?php 
if (strlen($_uacct)>0) {
 echo("
	<script src=\"http://www.google-analytics.com/urchin.js\" type=\"text/javascript\">
	</script>
	<script type=\"text/javascript\">
	_uacct = \"$_uacct\";
	 urchinTracker();
	</script>
 "); 
}
?>

</body>
</html>