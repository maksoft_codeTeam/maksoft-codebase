<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title><?php echo("$Title");?></title>
<link rel="shortcut icon" href="http://www.maksoft.net/Templates/parnasse/images/favicon.ico" type="image/x-icon">
<?php

include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/parnasse/");
define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/".DIR_TEMPLATE."images/");
define("DIR_MODULES","web/admin/modules");

$o_page->print_sConfigurations();

//load CSS style
echo "<link href='http://www.maksoft.net/".DIR_TEMPLATE."layout.css' rel='stylesheet' type='text/css'>";
echo "<link href='http://www.maksoft.net/".DIR_TEMPLATE."base_style.css' rel='stylesheet' type='text/css'>";

//short navigation
$short_navigation = "";
$b_page = $o_page->get_page($o_page->get_pParent());
$short_navigation = "<a href=\"".$b_page['page_link']."\">".$b_page['Name'] . "</a> / " . $o_page->get_pName();

switch($language)
	{
		case "EN" : { include DIR_TEMPLATE."languages/english.php"; break;}
		case "RU" : { include DIR_TEMPLATE."languages/russian.php"; break;}
		case "BG" : 
		default :
		{ include DIR_TEMPLATE."languages/bulgarian.php"; break;}
	}
?>
<script language="JavaScript" type="text/javascript" src="lib/png/pngfix_tilebg.js"></script>
<!--[if lte IE 6]>
<link href='http://www.maksoft.net/Templates/parnasse/ie6_style.css' rel='stylesheet' type='text/css'>
<![endif]-->
</head>
<body>
<div align="center">
<div id="site_container">
	<div id="header">
	<?php
		// including header file
		include DIR_TEMPLATE."/header.php";
	?>
	</div>
	<?php
		//enable tag search / tag addresses
        if(strlen($search_tag)>2 || strlen($search)>2)
			{
				include_once "main.php";
			}
		elseif($o_page->get_pSiteID() == $o_site->get_sID())
			{
				if($o_page->n == $o_site->get_sStartPage())
					include_once "home.php";
				else
					include_once "main.php";
			}
		else
			include_once "admin.php";
	?>


	<div id="footer">
	<?php
		include "footer.php";	
	?>
	</div>
	<?php echo $o_page->print_pNavigation(); ?>
</div>
</div>
</body>
</html>
