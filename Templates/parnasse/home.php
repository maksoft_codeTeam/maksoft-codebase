<?php
	$page_acc = $o_page->get_page(N_ACCOMMODATION);
	$page_services = $o_page->get_page(N_SERVICES);
	$page_about = $o_page->get_page(N_ABOUT);
	
	$page_link1 = $o_page->get_page(N_LINK1);
	$page_link2 = $o_page->get_page(N_LINK2);
	$page_link3 = $o_page->get_page(N_LINK3);
	
	$link1 = $o_page->get_pLink(N_LINK1);
	$link2 = $o_page->get_pLink(N_LINK2);
	$link3 = $o_page->get_pLink(N_LINK3);
	$linkacc = $o_page->get_pLink(N_ACCOMMODATION);
	$linkservices = $o_page->get_pLink(N_SERVICES);
	$linkabout = $o_page->get_pLink(N_ABOUT);
	
	
	$string_text = strip_tags($page_link1['textStr']); 
	$letter1 = $string_text[0];
	
	$string_text = strip_tags($page_link2['textStr']); 
	$letter2 = $string_text[0];

	$string_text = strip_tags($page_link3['textStr']);  
	$letter3 = $string_text[0];
	
?>
<div id="top_links">
	<table cellpadding="0" cellspacing="0" border="0" width="900">
		<tr><td width="300px" align="center"><a href="<?=$link1?>"><?=$page_link1['Name']?></a>
			<td width="300px" align="center" style="border-left: 1px dotted #baae9a; border-right: 1px dotted #baae9a;"><a href="<?=$link2?>"><?=$page_link2['Name']?></a>
			<td width="300px" align="center"><a href="<?=$link3?>"><?=$page_link3['Name']?></a>
		</tr>
	</table>
</div>
<div id="home">
<br>
	<table cellpadding="5" cellspacing="0" border="0" width="906">
		<tr><td align="left"><img src="Templates/parnasse/images/home_img1.jpg" alt="">
			<td align="center"><img src="Templates/parnasse/images/home_img2.jpg" alt="">
			<td align="right"><img src="Templates/parnasse/images/home_img3.jpg" alt="">
		</tr>
		<tr>
			<td valign="top"><div class="preview"><div class="letter"><?=$letter1?>

</div><hr><?php echo strip_tags(crop_text($page_link1['textStr']), "<b><strong>"); ?></div><a class="next_link" href="<?=$link1?>"><?=$Site->MoreText?></a>
			<td valign="top"><div class="preview"><div class="letter"><?=$letter2?></div><hr><?php echo strip_tags(crop_text($page_link2['textStr']), "<b><strong>"); ?></div><a class="next_link" href="<?=$link2?>"><?=$Site->MoreText?></a>
			<td valign="top"><div class="preview"><div class="letter"><?=$letter3?></div><hr><?php echo strip_tags(crop_text($page_link3['textStr']), "<b><strong>"); ?></div><a class="next_link" href="<?=$link3?>"><?=$Site->MoreText?></a>
		</tr>
	</table>

	<?php
		include "box_front_links.php";
	?>
	<!--
	<div id="pageContent">
	<?php
		//echo $o_page->get_pText();
		//$o_page->print_pContent();
		//$o_page->print_pSubContent();
		//eval($o_page->get_pInfo("PHPcode"));
		?>
		<br style="clear: both;">
		<?php
		if ($user->AccessLevel >= $row->SecLevel)
			if (!(empty($row->PageURL)))
				include_once("$row->PageURL");
	?>
	</div>
	//-->
</div>