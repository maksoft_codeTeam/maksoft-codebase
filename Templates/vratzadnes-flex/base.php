<!DOCTYPE html>
<html lang="<?php echo($Site->language_key); ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title><?=$Title?></title>
    <?php
    require_once '/hosting/maksoft/maksoft/lib/lib_page.php';
    require_once '/hosting/maksoft/maksoft/lib/Database.class.php';
    //include meta tags
    include( __DIR__."/../meta_tags.php" );
    $o_site->print_sConfigurations();
    ?>
	<script type="text/javascript" async="" src="<?php echo STATIC_URL;?>js/recaptcha__bg.js"></script>
	<link rel="stylesheet" id="mvp-score-style-css" href="<?php echo STATIC_URL;?>css/score-style.css" type="text/css" media="all">
	<link rel="stylesheet" id="woocommerce-layout-css" href="<?php echo STATIC_URL;?>css/woocommerce-layout.css" type="text/css" media="all">
	<link rel="stylesheet" id="woocommerce-smallscreen-css" href="<?php echo STATIC_URL;?>css/woocommerce-smallscreen.css" type="text/css" media="only screen and (max-width: 768px)">
	<link rel="stylesheet" id="woocommerce-general-css" href="<?php echo STATIC_URL;?>css/woocommerce.css" type="text/css" media="all">
	<link rel="stylesheet" id="dashicons-css" href="<?php echo STATIC_URL;?>css/dashicons.min.css" type="text/css" media="all">
	<link rel="stylesheet" id="reviewer-public-css" href="<?php echo STATIC_URL;?>css/reviewer-public.css" type="text/css" media="all">
	<link rel="stylesheet" id="mvp-reset-css" href="<?php echo STATIC_URL;?>css/reset.css" type="text/css" media="all">
	<link rel="stylesheet" id="mvp-style-css" href="<?php echo STATIC_URL;?>css/style.css" type="text/css" media="all">
	<!--[if lt IE 10]>
	<link rel='stylesheet' id='mvp-iecss-css'  href='http://www.mvpthemes.com/flexmag/wp-content/themes/flex-mag/css/iecss.css?ver=4.7.3' type='text/css' media='all' />
	<![endif]-->
	<link rel="stylesheet" id="mvp-media-queries-css" href="<?php echo STATIC_URL;?>/css/media-queries.css" type="text/css" media="all">
	<link rel="stylesheet" id="mvp-media-queries-css" href="<?php echo STATIC_URL;?>/css/custom.css" type="text/css" media="all">
	<script type="text/javascript" src="<?php echo STATIC_URL;?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo STATIC_URL;?>js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="/Templates/vratzadnes/autobahn-js/lib/autobahn.js"></script>
</head>

<body class="home page-template page-template-page-home page-template-page-home-php page page-id-179 custom-background" style="transform: none;" cz-shortcut-listen="true">
    <div id="site" class="left relative" style="transform: none;">
        <?php require_once __DIR__.'/templates/head_main.php';?>
            <div id="body-main-wrap" class="left relative" style="transform: none;">
                <div class="body-main-out relative" style="transform: none;">
                    <div class="body-main-in" style="transform: none;">
<?php
    if($o_page->_page['SiteID'] != $o_site->_site['SitesID'] )
        include __DIR__."/admin.php";   
    elseif($o_page->_page['n'] == $o_site->_site['StartPage'] && strlen($search_tag)<3 && strlen($search)<3)
        include __DIR__."/templates/homepage.php";  
    else {
        if($pTemplate['pt_url'])
            include $pTemplate['pt_url'];
        else
            if($subpages_length > 5){
                require_once __DIR__."/templates/category.php";
            } else {
                require_once __DIR__."/templates/article.php";
            }
        include TEMPLATE_DIR . "main.php";}
?>
                    </div>
                <!--body-main-in-->
                </div>
                <?php require_once __DIR__.'/templates/footer.php'; ?>
            <!--body-main-out-->
           </div>
    </div>

    <div class="fly-to-top back-to-top">
    </div>

    <div class="fly-fade">

    </div>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery("#post-right-col,#post-sidebar-wrap,.home-mid-col,.home-right-col,#arch-right-col").theiaStickySidebar({
                "containerSelector": "",
                "additionalMarginTop": "65",
                "additionalMarginBottom": "15",
                "updateSidebarHeight": false,
                "minWidth": "767",
                "sidebarBehavior": "modern"
            });
        });
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
           var $ = jQuery;
           var source   = $("#footer-live-articles").html();
           var template = Handlebars.compile(source);
           var conn = new ab.Session('ws://79.124.31.189:8802',
                function() {
                       conn.subscribe('<?=$o_page->_site['primary_url'];?>-live', function(topic, article) {
                       if($('#1487688597266321564 li').length > 5){
                           $('#1487688597266321564 li:first-child').remove();
                       }
                       jQuery("#1487688597266321564").append(template({article:article}));
                    });
                },
                function() {
                    console.warn('WebSocket connection closed');
                },
                {'skipSubprotocolCheck': true}
            );

            // Back to Top Button
            var duration = 500;
            $('.back-to-top').click(function(event) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: 0
                }, duration);
                return false;
            });

            // Main Menu Dropdown Toggle
            $('.menu-item-has-children a').click(function(event) {
                event.stopPropagation();
                location.href = this.href;
            });

            $('.menu-item-has-children').click(function() {
                $(this).addClass('toggled');
                if ($('.menu-item-has-children').hasClass('toggled')) {
                    $(this).children('ul').toggle();
                    $('.fly-nav-menu').getNiceScroll().resize();
                }
                $(this).toggleClass('tog-minus');
                return false;
            });

            // Main Menu Scroll
            $(window).load(function() {
                $('.fly-nav-menu').niceScroll({
                    cursorcolor: "#888",
                    cursorwidth: 7,
                    cursorborder: 0,
                    zindex: 999999
                });
            });

            // Infinite Scroll
            $('.infinite-content').infinitescroll({
                navSelector: ".nav-links",
                nextSelector: ".nav-links a:first",
                itemSelector: ".infinite-post",
                loading: {
                    msgText: "Loading more posts...",
                    finishedMsg: "Sorry, no more posts"
                },
                errorCallback: function() {
                    $(".inf-more-but").css("display", "none")
                }
            });
            $(window).unbind('.infscr');
            $(".inf-more-but").click(function() {
                $('.infinite-content').infinitescroll('retrieve');
                return false;
            });
            $(window).load(function() {
                if ($('.nav-links a').length) {
                    $('.inf-more-but').css('display', 'inline-block');
                } else {
                    $('.inf-more-but').css('display', 'none');
                }
            });

            $(window).load(function() {
                // The slider being synced must be initialized first
                $('.post-gallery-bot').flexslider({
                    animation: "slide",
                    controlNav: false,
                    animationLoop: true,
                    slideshow: false,
                    itemWidth: 80,
                    itemMargin: 10,
                    asNavFor: '.post-gallery-top'
                });

                $('.post-gallery-top').flexslider({
                    animation: "fade",
                    controlNav: false,
                    animationLoop: true,
                    slideshow: false,
                    prevText: "&lt;",
                    nextText: "&gt;",
                    sync: ".post-gallery-bot"
                });
            });

        });
    </script>

    <script id="single_article_most_popular" type="text/x-handlebars-template">
    <div class="feat-widget-wrap left relative">
        <a href="{{article.page_link}}">
            <div class="feat-widget-img left relative">
                <img width="300" height="180" src="{{article.image_src}}" class="reg-img wp-post-image" alt="" sizes="(max-width: 300px) 100vw, 300px">
                <img width="80" height="80" src="{{article.image_src}}" class="mob-img wp-post-image" alt="" sizes="(max-width: 80px) 100vw, 80px">
                <div class="feat-info-wrap">
                    <div class="feat-info-views">
                        <i class="fa fa-eye"></i> <span class="feat-info-text">{{article.preview}}</span>
                    </div>
                    <!--feat-info-comm-->
                </div>
                <!--feat-info-wrap-->
            </div>
            <!--feat-widget-img-->
            <div class="feat-widget-text">
                <span class="side-list-cat">{{article.parent.Name}}</span>
                <h2>{{article.Name}}</h2>
            </div>
            <!--feat-widget-text-->
        </a>
    </div>

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.min.js" integrity="sha256-1O3BtOwnPyyRzOszK6P+gqaRoXHV6JXj8HkjZmPYhCI=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/comment-reply.min.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/score-script.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/theia-sticky-sidebar.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/add-to-cart.min.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/jquery.blockUI.min.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/js.cookie.min.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/woocommerce.min.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/cart-fragments.min.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/api.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/jquery.nouislider.all.min.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/jquery.knob.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/reviewer.public.min.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/reviewer-widget-users-reviews.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/jquery.infinitescroll.min.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/autoloadpost.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/retina.min.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/wp-embed.min.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/main.js"></script>
    <script type="text/javascript" src="<?php echo STATIC_URL;?>js/jquery.imagemapster.min.js"></script>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/bg_BG/sdk.js#xfbml=1&version=v2.8&appId=1231753050227827";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

jQuery(document).ready(function(){
   get("<?=ENV::MOST_VISITED?>")
    .then(JSON.parse)
    .then(function(articles){
        print_articles(articles, '#single_article_most_popular', "#most_popular", <?=ENV::BANNER_TOPNEWS300x250R?>);
    });
});
    </script>

</body>

</html>
