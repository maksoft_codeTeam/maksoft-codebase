<?php


interface Env
{
    const BASE_PATH = '/Templates/vratzadnes-flex';

    const ASSETS = '/Templates/vratzadnes-flex/assets';

    const MAIN_MENU = 3437;

    const MUNCIPALITY_MENU = 3464;

    const MUNCIPALITY_NEWS_LIMIT = 6;

    const TOP_NEWS = 542;

    const TOP_NEWS_LIMIT = 4;

    const MAXIMUM_ARTICLES_PER_MENU_ITEM = 5;

    const BASE_DIR = '/hosting/maksoft/maksoft/Templates/vratzadnes-responsive/';

    const MENU_MAX_ITEMS = 11;

    const TMPL_PATH = '/hosting/maksoft/maksoft/Templates/vratzadnes-responsive/templates/';

    const NEWEST_PAGES = 'http://vratzadnes.com/api/?command=newest_pages&SiteID=922&n=13321&limit=10';

    const MOST_VISITED = 'http://vratzadnes.com/api/?command=most_visited&SiteID=922&n=13321&limit=10&parent_pages=190803, 195395, 190805, 196326, 190115, 190807, 193122, 190804, 190808, 195393, 193123';

    const BANNER_RIGHT_BIG = 95;

    const BANNER_TOP_SMALL = 97;

    const BANNER_MIDDLE_SMALL = 105;

    const BANNER_BOTTOM_SMALL = 101;

    const BANNER_BOTTOM_BIG = 103;

    const BANNER_TOPNEWS300x250L = 126;
    const BANNER_TOPNEWS300x250R = 127;

    const PAGINATION_MAX_PAGES = 20;

    const MAXIMUM_CHARACTERS = 150; // Използва се от cut_text
}
