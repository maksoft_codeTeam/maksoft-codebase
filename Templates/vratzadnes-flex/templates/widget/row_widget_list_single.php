<li>
    <a href="<?=$article['page_link'];?>" rel="bookmark">
        <div class="row-widget-img left relative">
            <img width="300" height="180" src="<?=$article['image_src'];?>" class="reg-img wp-post-image" alt="<?=$article['Name'];?>" sizes="(max-width: 300px) 100vw, 300px"> 
            <img width="80" height="80" src="<?=$article['image_src'];?>" class="mob-img wp-post-image" alt="<?=$article['Name'];?>"  sizes="(max-width: 80px) 100vw, 80px">
            <div class="feat-info-wrap">
                <div class="feat-info-views">
                    <i class="fa fa-eye fa-2"></i> <span class="feat-info-text"><?=$article['preview'];?></span>
                </div>
                <!--feat-info-views-->
            </div>
            <!--feat-info-wrap-->
        </div>
        <!--row-widget-img-->
        <div class="row-widget-text left relative">
            <span class="side-list-cat"><?=$o_page->get_pInfo("Name", $article['ParentPage']);?></span>
            <p><?=$article['Name'];?></p>
        </div>
        <!--row-widget-text-->
    </a>
</li>
