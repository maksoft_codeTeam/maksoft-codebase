<!--<div class="feat-widget-cont left relative">
    <div class="feat-widget-in left relative">
        <div class="feat-widget-wrap left relative">
            <a href="http://www.mvpthemes.com/flexmag/8-fall-outfit-ideas-brought-to-you-by-celebs/" rel="bookmark">
                <div class="feat-widget-img left relative">
                    <img width="1000" height="600" src="./Flex Mag - Powerfully Simple WordPress News &amp; Magazine Theme_files/friends-fashion.jpg" class="widget-img-main wp-post-image" alt="" srcset="http://www.mvpthemes.com/flexmag/wp-content/uploads/2015/08/friends-fashion.jpg 1000w, http://www.mvpthemes.com/flexmag/wp-content/uploads/2015/08/friends-fashion-300x180.jpg 300w, http://www.mvpthemes.com/flexmag/wp-content/uploads/2015/08/friends-fashion-450x270.jpg 450w" sizes="(max-width: 1000px) 100vw, 1000px"> <img width="450" height="270" src="./Flex Mag - Powerfully Simple WordPress News &amp; Magazine Theme_files/friends-fashion-450x270.jpg" class="widget-img-side wp-post-image" alt="" srcset="http://www.mvpthemes.com/flexmag/wp-content/uploads/2015/08/friends-fashion-450x270.jpg 450w, http://www.mvpthemes.com/flexmag/wp-content/uploads/2015/08/friends-fashion-300x180.jpg 300w, http://www.mvpthemes.com/flexmag/wp-content/uploads/2015/08/friends-fashion.jpg 1000w" sizes="(max-width: 450px) 100vw, 450px">
                    <div class="feat-info-wrap">
                        <div class="feat-info-views">
                            <i class="fa fa-eye fa-2"></i> <span class="feat-info-text">22.5K</span>
                        </div>

                        <div class="feat-info-comm">
                            <i class="fa fa-comment"></i> <span class="feat-info-text">7</span>
                        </div>

                    </div>

                </div>

                <div class="feat-widget-text left relative">
                    <span class="side-list-cat">Fashion</span>
                    <h2>8 Fall outfit ideas brought to you by celebs</h2>
                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt.</p>
                </div>
   
            </a>
        </div>

    </div>

</div>
-->
<div class="feat-widget-cont left relative">
    <div class="home-title-wrap left relative"> <h3 class="side-list-title">�������</h3></div>
    <div class="feat-widget-in left relative">
        <img id="beatles" src="<?=STATIC_URL?>img/map-vratza.jpg" 
            style="width:100%;height:auto;" usemap="#beatles-map">
        <map name="beatles-map">
            <area shape="rect" data-name="paul,all" coords="36,46,121,131" href="1" />
            <area shape="rect" data-name="ringo,all" coords="113,76,198,161" href="2" />
            <area shape="rect" data-name="john,all" coords="192,50,277,135" href="3" />
            <area shape="rect" data-name="george,all" coords="262,60,347,145" href="4" />
        </map>
        <div style="width:390px; height: 120px; font-size: 12px; ">
            <div id="beatles-caption" style="clear:both;border: 1px solid black; width: 400px; padding: 6px; display:none;">
                <div id="beatles-caption-header" style="font-style: italic; font-weight: bold; margin-bottom: 12px;"></div>
                <div id="beatles-caption-text"></div>
            </div>
        </div>
    </div>
</div>

<script>
// javascript

// Set up some options objects: 'single_opts' for when a single area is selected, which will show just a border
// 'all_opts' for when all are highlighted, to use a different effect - shaded white with a white border
// 'initial_opts' for general options that apply to the whole mapster. 'initial_opts' also includes callbacks
// onMouseover and onMouseout, which are fired when an area is entered or left. We will use these to show or
// remove the captions, and also set a flag to let the other code know if we're currently in an area.

jQuery(document).ready(function(){
var $ = jQuery;
var inArea,
    map = $('#beatles'),
    captions = {
        paul: ["Paul McCartney - Bass Guitar and Vocals",
            "Paul McCartney's song, Yesterday, recently voted the most popular song "
                + "of the century by a BBC poll, was initially composed without lyrics. "
                + "Paul used the working title 'scrambled eggs' before coming up with the final words."],
        ringo: ["Ringo Starr - Drums",
            "Dear Prudence was written by John and Paul about Mia Farrow's sister, Prudence, "
            + "when she wouldn't come out and play with Mia and the Beatles at a religious retreat "
            + "in India."],
        john: ["John Lennon - Guitar and Vocals",
            "In 1962, The Beatles won the Mersyside Newspaper's biggest band in Liverpool "
            + "contest principally because they called in posing as different people and voted "
            + "for themselves numerous times."],
        george: ["George Harrison - Lead Guitar and Vocals",
            "The Beatles' last public concert was held in San Francisco's Candlestick "
            + "Park on August 29, 1966."]
    },
    single_opts = {
        fillColor: '000000',
        fillOpacity: 0,
        stroke: true,
        strokeColor: 'ff0000',
        strokeWidth: 2
    },
    all_opts = {
        fillColor: 'ffffff',
        fillOpacity: 0.6,
        stroke: true,
        strokeWidth: 2,
        strokeColor: 'ffffff'
    },
    initial_opts = {
        mapKey: 'data-name',
        isSelectable: false,
        onMouseover: function (data) {
            inArea = true;
            jQuery('#beatles-caption-header').text(captions[data.key][0]);
            jQuery('#beatles-caption-text').text(captions[data.key][1]);
            jQuery('#beatles-caption').show();
        },
        onMouseout: function (data) {
            inArea = false;
            jQuery('#beatles-caption').hide();
        }
    };
    opts = jQuery.extend({}, all_opts, initial_opts, single_opts);


    // Bind to the image 'mouseover' and 'mouseout' events to activate or deactivate ALL the areas, like the
    // original demo. Check whether an area has been activated with "inArea" - IE<9 fires "onmouseover" 
    // again for the image when entering an area, so all areas would stay highlighted when entering
    // a specific area in those browsers otherwise. It makes no difference for other browsers.

    map.mapster('unbind')
        .mapster(opts)
        .bind('mouseover', function () {
            if (!inArea) {
                map.mapster('set_options', all_opts)
                    .mapster('set', true, 'all')
                    .mapster('set_options', single_opts);
            }
        }).bind('mouseout', function () {
            if (!inArea) {
                map.mapster('set', false, 'all');
            }
        });
});
</script>
