<li>
    <a href="http://www.mvpthemes.com/flexmag/starbucks-just-made-a-major-change-to-its-pumpkin-spice-latte/" rel="bookmark">
        <div class="side-list-out">
            <div class="side-list-img left relative">
                <img width="80" height="80" src="./Flex Mag - Powerfully Simple WordPress News &amp; Magazine Theme_files/latte-80x80.jpg" class="attachment-mvp-small-thumb size-mvp-small-thumb wp-post-image" alt="" srcset="http://www.mvpthemes.com/flexmag/wp-content/uploads/2015/08/latte-80x80.jpg 80w, http://www.mvpthemes.com/flexmag/wp-content/uploads/2015/08/latte-150x150.jpg 150w, http://www.mvpthemes.com/flexmag/wp-content/uploads/2015/08/latte-180x180.jpg 180w, http://www.mvpthemes.com/flexmag/wp-content/uploads/2015/08/latte-600x600.jpg 600w" sizes="(max-width: 80px) 100vw, 80px"> </div>
            <!--side-list-img-->
            <div class="side-list-in">
                <div class="side-list-text left relative">
                    <span class="side-list-cat">Business</span>
                    <p>Starbucks just made a major change to its Pumpkin Spice Latte</p>
                </div>
                <!--side-list-text-->
            </div>
            <!--side-list-in-->
        </div>
        <!--side-list-out-->
    </a>
</li>
