<div class="feat-wide5-main left relative">
    <a href="<?php echo $article['page_link'];?>" rel="bookmark">
        <div class="feat-wide5-img left relative">
            <img width="1600" height="960" src="<?php echo $article['image_src'];?>" class="unlazy reg-img wp-post-image" alt=""  sizes="(max-width: 1600px) 100vw, 1600px"> 
            <img width="450" height="270" ssrc="<?php echo $article['image_src'];?>" class="unlazy mob-img wp-post-image" alt=""  sizes="(max-width: 450px) 100vw, 450px">
         </div>
        <!--feat-wide5-img-->
        <div class="feat-wide5-text">
        <span class="feat-cat"><?=$o_page->get_pInfo("Name", $article['ParentPage']);?></span>
            <h2><?php echo $article['Name'];?></h2>
        </div>
        <!--feat-wide5-text-->
        <div class="feat-info-wrap">
            <div class="feat-info-views">
            <i class="fa fa-eye fa-2"></i> <span class="feat-info-text"><?php echo $article['preview'];?></span>
            </div>
            <!--feat-info-views-->
            <div class="feat-info-comm">
                <i class="fa fa-comment"></i> <span class="feat-info-text">18</span>
            </div>
            <!--feat-info-comm-->
        </div>
        <!--feat-info-wrap-->
    </a>
</div>
