<div id="mvp_tagrow_widget-2" class="home-widget left relative mvp_tagrow_widget">
    <div class="row-widget-wrap left relative">
        <ul class="row-widget-list">
           <?php $limiter = 0; ?>
           <?php while($article = array_shift($articles)){
                if($limiter == 3) {break;}
                include __DIR__.'/../widget/row_widget_list_single.php';
                $limiter++;
            } ?>
        </ul>
    </div>
    <!--row-widget-wrap-->
</div>
