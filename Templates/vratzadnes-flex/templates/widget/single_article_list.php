<script id="single_article_latest" type="text/x-handlebars-template">
<li class="infinite-post">
    <a href="{{article.page_link}}" rel="bookmark">
        <div class="blog-widget-img left relative">
            <img width="300" height="180" src="{{article.image_src}}" class="reg-img wp-post-image" alt="" sizes="(max-width: 300px) 100vw, 300px">
            <img width="80" height="80" src="{{article.image_src}}" class="mob-img wp-post-image" alt="" sizes="(max-width: 80px) 100vw, 80px">
            <div class="feat-info-wrap">
                <div class="feat-info-views">
                    <i class="fa fa-eye"></i> <span class="feat-info-text">{{article.preview}}</span>
                </div>
                <!--feat-info-views-->
            </div>
            <!--feat-info-wrap-->
        </div>
        <!--blog-widget-img-->
        <div class="blog-widget-text left relative">
            <span class="side-list-cat">{{article.parent.Name}}</span>
            <h2>{{article.Name}}</h2>
                <p>{{article.textStr_part}}</p>
        </div>
        <!--blog-widget-text-->
    </a>
</li>
</script>
