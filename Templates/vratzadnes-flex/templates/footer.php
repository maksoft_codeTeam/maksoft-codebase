<footer id="foot-wrap" class="left relative">
    <div id="foot-top-wrap" class="left relative">
        <div class="body-main-out relative">
            <div class="body-main-in">
                <div id="foot-widget-wrap" class="left relative">
                    <div class="foot-widget left relative">
                        <div class="foot-logo left realtive">
                            <img src="http://www.vratzadnes.com/Templates/vratzadnes-flex/assets/img/logo-vratzadnes.png" alt="#" data-rjs="2">
                        </div>
                        <!--foot-logo-->
                        <div class="foot-info-text left relative">
                            �����
                        </div>
                        <!--footer-info-text-->
                        <div class="foot-soc left relative">

                            <ul class="foot-soc-list relative">
                    <?php
                            $s_configurations = $o_page->get_sConfigurations();
                            foreach ($s_configurations as $conf) {
                                switch($conf['conf_key']){
                                case "CMS_FACEBOOK_PAGE_LINK":
                                    ?> 
                                    <li class="foot-soc-fb">
                                        <a target="_blank" title="Facebook" href="<?=$conf['conf_value']?>">
                                            <i class="fa fa-facebook-square fa-2"></i>
                                        </a>
                                    </li>
                                    <?php break;
                                case "CMS_TWITTER_PAGE_LINK":
                                    ?> 
                                    <li class="foot-soc-twit">
                                        <a target="_blank" title="Twitter" href="<?=$conf['conf_value']?>">
                                            <i class="fa fa-twitter-square fa-2"></i>
                                        </a>
                                    </li>
                                    <?php break;
                                case "CMS_LINKEDIN_PAGE_LINK":
                                    ?> 
                                        <li class="linkedin"><a target="_blank" title="LinkedIN" href="<?=$conf['conf_value']?>">
                                            <i class="fa fa-linkedin-square fa-2"></i>
                                        </a></li>
                                    <?php break;
                                }
                            }
                    ?>
                                <li class="foot-soc-rss">
                                    <a href="/rss.php" target="_blank"><i class="fa fa-rss-square fa-2"></i></a>
                                </li>
                            </ul>
                        </div>
                        <!--foot-soc-->
                    </div>
                    <!--foot-widget-->
                    <div id="mvp_catlist_widget-4" class="foot-widget left relative mvp_catlist_widget">
                        <h3 class="foot-head">����� �� ���� � �������:</h3>
                        <?php $article = $o_page->get_page(19369199);?>
                        <div class="blog-widget-wrap left relative">
                            <ul id="1487688597266321564" class="blog-widget-list left relative">
                            </ul>
                        </div>
                        <!--blog-widget-wrap-->
                    </div>
                    <div id="mvp_tags_widget-2" class="foot-widget left relative mvp_tags_widget">
                        <h3 class="foot-head">������� ��� <?= $wwo->city;?></h3>
                        <div class="tag-cloud left relative">
                            <span class="weather-icon">
                                <img style="width: 64px!important; height:64px!important;" src="http://cdn.worldweatheronline.net/images/weather/small/<?php echo $wwo->weather->current_condition->weatherCode."_$day_night"; ?>.png">
                            </span>
                            <span class="degree"><strong><?=$wwo->weather->current_condition->temp_C?></strong> C&deg;</span>
                        </div>

                    </div>
                </div>
                <!--foot-widget-wrap-->
            </div>
            <!--body-main-in-->
        </div>
        <!--body-main-out-->
    </div>
    <!--foot-top-->
    <div id="foot-bot-wrap" class="left relative">
        <div class="body-main-out relative">
            <div class="body-main-in">
                <div id="foot-bot" class="left relative">
                    <div class="foot-menu relative">
                        <div class="menu-footer-menu-container">
                            <ul id="menu-footer-menu" class="menu">
                                <li id="menu-item-1817" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1817">
                                    <a href="/#tab-col1">������</a>
                                </li>
                                <li id="menu-item-1819" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1819"><a href="#">link2</a></li>
                                <li id="menu-item-1820" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1820"><a href="#">link3</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--foot-menu-->
                    <div class="foot-copy relative">
                        <p>Copyright � 2017 Flex Mag Theme. Theme by MVP Themes, powered by Wordpress.</p>
                    </div>
                    <!--foot-copy-->
                </div>
                <!--foot-bot-->
            </div>
            <!--body-main-in-->
        </div>
        <!--body-main-out-->
    </div>
    <!--foot-bot-->
</footer>

<script id="footer-live-articles" type="text/x-handlebars-template">
<li>
 <a href="{{article.link}}" rel="bookmark">
        <div class="blog-widget-img left relative">
        {{#if article.image_src }}
        <img width="300" height="180" src="/img_preview.php?image_file={{article.image_src}}&img_width=180px" class="widget-img-main wp-post-image" alt="{{article.image_name}}" >
        {{/if}}
            <div class="feat-info-wrap">
                <div class="feat-info-views">
                    <i class="fa fa-eye"></i> <span class="feat-info-text">{{article.preview}}</span>
                </div>
                <!--feat-info-views-->
            </div>
            <!--feat-info-wrap-->
        </div>
        <!--blog-widget-img-->
        <div class="blog-widget-text left relative">
        <span class="side-list-cat">live</span>
        <h2>{{article.Name}}</h2>
        </div>
        <!--blog-widget-text-->
    </a>
</li>
</script>
