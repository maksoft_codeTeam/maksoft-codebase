
<?php 
#include __DIR__.'/helpers/top_news.php';
define("LIMIT", ENV::PAGINATION_MAX_PAGES);
if(!isset($_GET['pag']) or !is_numeric($_GET['pag']) or $_GET['pag'] < 1){
    define("PAG", 1);
} else {
    define("PAG", $_GET['pag']);
}

$articles = $o_page->get_pSubpages($o_page->_page['n'], null, 1, false, array(((PAG * LIMIT) - LIMIT), LIMIT));

$right_coloumn_news_count = 4;

function build_url($n, $SiteID, $pagination){
    parse_str($_SERVER['QUERY_STRING'], $url);
    $url['n'] = $n;
    $url['SiteID'] = $SiteID;
    $url['pag'] = $pagination;
    return '/page.php?'.http_build_query($url);
}

?>

<div id="body-main-cont" class="left relative" style="transform: none;">
    <div id="leader-wrap" class="left relative">
        <div style="width: 90%; height: 30px; background: #ddd; text-align: center; display: inline-block; color: #bbb; font-family: 'Open Sans', sans-serif; font-weight: 700; font-size: 1.2rem; white-space: nowrap; padding: 30px 5% 0; overflow: hidden;">RESPONSIVE LEADERBOARD AD AREA</div>
    </div>
<div id="feat-top-wrap" class="left relative">
<?php
$i = 0;
while ($article = array_pop($articles)) {
    if($i == 4){
        break;
    }
    include __DIR__.'/widget/single_top_article.php';
    $i++;
}
?>
    <!--feat-top2-right-wrap-->
</div>
    <!--leader-wrap-->
    <div id="home-main-wrap" class="left relative" style="transform: none;">
        <div class="home-wrap-out1" style="transform: none;">
            <div class="home-wrap-in1">
                <div id="home-left-wrap" class="left relative">
                    <div id="home-left-col" class="relative">
                        <h1 class="cat-head"><?=$o_page->_page['Name'];?></h1>
                        <div id="home-mid-wrap" class="left relative">
                            <div id="archive-list-wrap" class="left relative">
                                <ul class="archive-col-list left relative infinite-content">
                                   <?php
                                         $i = 0;
                                         $articles_count = count($articles);
                                         $place_single_banner = true;
                                         if($articles_count % 2 == 0){
                                            $articles_count -= 1;
                                         }
                                         while($article = array_shift($articles)){
                                             if( $i % ($articles_count/3) == 0 and $i > 3 and $place_single_banner){
                                                echo tmpl::place_banner(ENV::BANNER_MIDDLE_SMALL);
                                                echo '<br>';
                                                $place_single_banner=false;
                                             }
                                    ?>
                                    <li class="infinite-post">
                                        <a href="<?=$article['page_link'];?>" rel="bookmark" title="<?php echo $article['Name'];?>">
                                            <div class="archive-list-img left relative">
                                                <img width="300" height="180" src="<?=$article['image_src'];?>" class="reg-img wp-post-image" alt="" sizes="(max-width: 300px) 100vw, 300px">
                                                <img width="80" height="80" src="<?=$article['image_src'];?>" class="mob-img wp-post-image" alt="" sizes="(max-width: 80px) 100vw, 80px">
                                                <div class="feat-info-wrap">
                                                    <div class="feat-info-views">
                                                        <i class="fa fa-eye fa-2"></i> <span class="feat-info-text"><?=$article['preview'];?></span>
                                                    </div>
                                                    <!--feat-info-views-->
                                                </div>
                                                <!--feat-info-wrap-->
                                            </div>
                                            <!--blog-widget-img-->
                                            <div class="archibe-list-in">
                                                <div class="blog-widget-text left relative">
                                                    <span class="side-list-cat"><?=$parent_name?></span>
                                                    <h2><?=$article['Name'];?></h2>
                                                        <p><?=cut_text($article['textStr'], 60);?></p>
                                                </div>
                                            </div>
                                            <!--blog-widget-text-->
                                        </a>
                                    </li>
                                   <?php
                                      $i++; 
                                    } ?>
                                </ul>
                                <?=tmpl::place_banner(ENV::BANNER_MIDDLE_SMALL); ?> </div>
                                <div class="nav-links">
                                    <div class="pagination">
                    <?php $max_pages = round($subpages_length / LIMIT); ?>
                                        <span>Page <?=PAG?> �� <?=$max_pages;?></span>
                    <?php
                    $subpages_length =(int) round( $subpages_length / LIMIT);
                    for($i=1; $i< $subpages_length; $i++){
                       $cls = '';
                       if(PAG == $i) { 
                            echo '<span class="current">'.PAG.'</span>';
                            continue;
                       }
                       echo "<a href=\"".build_url($n, $SiteID, $i)."\" class=\"inactive\">$i</a>";
                    }
                    ?>
                                    </div>
                                </div>
                                <!--nav-links-->
                            </div>
                            <!--archive-list-wrap-->
                        </div>
                        <!--home-mid-wrap-->
                    </div>
                    <!--home-left-col-->
                </div>
                <!--home-left-wrap-->
            </div>
            <!--home-wrap-in1-->
            <div id="arch-right-col" class="relative" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                <?php include __DIR__.'/helpers/most_popular.php'; ?>
            </div>
            <!--home-right-col-->
        </div>
        <!--home-wrap-out1-->
    </div>
    <!--home-main-wrap-->
    <!--foot-ad-wrap-->
</div>
