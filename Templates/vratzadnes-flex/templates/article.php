<?php 
$link = $o_page->get_pLink();
$parent_name = $o_page->get_pInfo("Name", $o_page->_page['ParentPage']);
?>
<div class="post-wrap-in1" style="transform: none;">
<?php include __DIR__.'/helpers/responsive_leaderboard.php';?>
<div id="post-left-col" class="relative" style="transform: none;">
<hr style="height: 0" class="post-divider" data-title="<?=$o_page->_page['Name'];?>" data-url="<?=$link;?>">
    <article id="post-area" class="post-83 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-android tag-google tag-marshmallow tag-phones tag-tech" style="transform: none;">
        <div id="content-area" itemprop="articleBody" class="post-83 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-android tag-google tag-marshmallow tag-phones tag-tech" style="transform: none;">
            <div class="post-cont-out" style="transform: none;">
                <div class="post-cont-in">
                    <div id="content-main" class="left relative">

                        <header id="post-header">
                            <a class="post-cat-link" href="http://www.mvpthemes.com/flexmag/category/tech/"><span class="post-head-cat"><?=$parent_name;?></span></a>
                            <h1 class="post-title entry-title left" itemprop="name headline"><?=$o_page->_page['Name'];?></h1>
                            <div id="post-info-wrap" class="left relative">
                                <div class="post-info-out">
                                    <div class="post-info-in">
                                        <div class="post-info-right left relative">
                                            <!--post-info-name-->
                                            <div class="post-info-date left relative">
                                                <span class="post-info-text">Posted on</span> <span class="post-date updated"><time class="post-date updated" itemprop="datePublished" datetime="<?=$o_page->_page['date_added'];?>"><?=$o_page->_page['date_added'];?></time></span>
                                                <meta itemprop="dateModified" content="<?=$o_page->_page['date_added'];?>">
                                            </div>
                                            <!--post-info-date-->
                                        </div>
                                        <!--post-info-right-->
                                    </div>
                                    <!--post-info-in-->
                                </div>
                                <!--post-info-out-->
                            </div>
                            <!--post-info-wrap-->
                        </header>
                        <!--post-header-->
                        <div id="post-feat-img" class="left relative" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
                        <img width="1000" height="600" src="<?=$o_page->_page['image_src'];?>" class="attachment- size- wp-post-image" alt="" sizes="(max-width: 1000px) 100vw, 1000px">
                        <meta itemprop="url" content="<?=$o_page->_page['image_src'];?>">
                            <meta itemprop="width" content="1000">
                            <meta itemprop="height" content="600">
                            <div class="post-feat-text">
                                <span class="post-excerpt left"><p><?=$o_page->_page['Name'];?></p> </span>
                                <span class="feat-caption" style="margin-right:2%;"><?=$o_page->_site['primary_url'];?></span>
                            </div>
                            <!--post-feat-text-->
                        </div>
                        <!--post-feat-img-->

                        <section class="social-sharing-top">
                        <a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?=$link?>&amp;t=<?=$o_page->_page['Name'];?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook">
                                <div class="facebook-share"><span class="fb-but1"><i class="fa fa-facebook fa-2"></i></span><span class="social-text">Share</span></div>
                            </a>
                            <a href="#" onclick="window.open('http://twitter.com/share?text=<?=$o_page->_page['Name'];?> -&amp;url=<?=$link?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post">
                                <div class="twitter-share"><span class="twitter-but1"><i class="fa fa-twitter fa-2"></i></span><span class="social-text">Tweet</span></div>
                            </a>
                            <a href="whatsapp://send?text=<?=$o_page->_page['Name'];?> <?=$link?>">
                                <div class="whatsapp-share"><span class="whatsapp-but1"><i class="fa fa-whatsapp fa-2"></i></span><span class="social-text">Share</span></div>
                            </a>
                            <a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?=$link?>&amp;media=<?=$o_page->_page['image_src'];?>&amp;description=<?=$o_page->_page['Name'];?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post">
                                <div class="pinterest-share"><span class="pinterest-but1"><i class="fa fa-pinterest-p fa-2"></i></span><span class="social-text">Share</span></div>
                            </a>
                            <a href="mailto:?subject=<?=$o_page->_page['Name'];?>&amp;BODY=I found this article interesting and thought of sharing it with you. Check it out: <?=$link?>">
                                <div class="email-share"><span class="email-but"><i class="fa fa-envelope fa-2"></i></span><span class="social-text">Email</span></div>
                            </a>
                            <a href="<?=$link?>#respond">
                                <div class="social-comments comment-click-83"><i class="fa fa-commenting fa-2"></i><span class="social-text-com">Comments</span></div>
                            </a>
                        </section>
                        <!--social-sharing-top-->
                        <p><?=$o_page->_page['textStr'];?></p>
                        <div class="mvp-org-wrap" itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">
                            <div class="mvp-org-logo" itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
                                <img src="http://www.mvpthemes.com/flexmag/wp-content/themes/flex-mag/images/logos/logo-large.png" alt="Flex Mag">
                                <meta itemprop="url" content="http://www.mvpthemes.com/flexmag/wp-content/themes/flex-mag/images/logos/logo-large.png">
                            </div>
                        </div>
                        <!--mvp-org-wrap-->
                        <div class="posts-nav-link">
                        </div>
                        <?php if($related_items = $o_page->get_related_links()){;?>
                        <!--posts-nav-link-->
                        <div class="post-tags">
                            <span class="post-tags-header">Related Items:</span>
                            <span itemprop="keywords">
                                <?php
                                foreach ($related_items as $tag){
                                    echo "<a href=\"".$tag['link']."\">".$tag['name']."</a>";
                                }
                                ?>
                            </span>
                        </div>
                        <?php } ?>
                        <!--post-tags-->
                        <div class="social-sharing-bot">
                        <a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?=$link?>&amp;t=<?=$o_page->_page['Name'];?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook">
                                <div class="facebook-share"><span class="fb-but1"><i class="fa fa-facebook fa-2"></i></span><span class="social-text">Share</span></div>
                            </a>
                            <a href="#" onclick="window.open('http://twitter.com/share?text=<?=$o_page->_page['Name'];?> -&amp;url=<?=$link?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post">
                                <div class="twitter-share"><span class="twitter-but1"><i class="fa fa-twitter fa-2"></i></span><span class="social-text">Tweet</span></div>
                            </a>
                            <a href="whatsapp://send?text=<?=$o_page->_page['Name'];?> <?=$link?>">
                                <div class="whatsapp-share"><span class="whatsapp-but1"><i class="fa fa-whatsapp fa-2"></i></span><span class="social-text">Share</span></div>
                            </a>
                            <a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?=$link?>&amp;media=<?=$o_page->_page['image_src'];?>&amp;description=<?=$o_page->_page['Name'];?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post">
                                <div class="pinterest-share"><span class="pinterest-but1"><i class="fa fa-pinterest-p fa-2"></i></span><span class="social-text">Share</span></div>
                            </a>
                            <a href="mailto:?subject=<?=$o_page->_page['Name'];?>&amp;BODY=I found this article interesting and thought of sharing it with you. Check it out: <?=$link?>">
                                <div class="email-share"><span class="email-but"><i class="fa fa-envelope fa-2"></i></span><span class="social-text">Email</span></div>
                            </a>
                        </div>
                        <!--social-sharing-bot-->
                        <div class="mvp-related-posts left relative">
                        <h4 class="post-header"><span class="post-header">��� �� <?=$parent_name;?></span></h4>
                            <ul>
                                <?php
                                    $limiter = 0;
                                    $articles = $o_page->get_pSubPages($o_page->_page['ParentPage']);
                                    while($article = array_shift($articles)){
                                        if($limiter == 3) {break;}
                                        include __DIR__.'/widget/row_widget_list_single.php';
                                        $limiter++;
                                    }
                                ?>
                            </ul>
                        </div>
                            <div class="fb-comments" data-href="http://www.vratzadnes.com/page.php?n=<?=$o_page->_page['n'];?>" data-numposts="5"></div>
                            <meta property="fb:app_id" content="1231753050227827" />
                    </div>
                    <!--content-main-->
                </div>
                <!--post-cont-in-->
                <div id="post-sidebar-wrap" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                    <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static;"><img src="<?=STATIC_URL?>img/flex300x600.jpg"></div>
                </div>
                <!--post-sidebar-wrap-->
            </div>
            <!--post-cont-out-->
        </div>
        <!--content-area-->
    </article>
</div>
</div>
<div id="post-right-col" class="relative" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
<?php include __DIR__.'/helpers/most_popular.php'; ?>
</div>
