<div id="head-main-wrap" class="left relative">
    <div id="head-main-top" class="left relative">
    </div>
    <!--head-main-top-->
    <div id="main-nav-wrap" class="">
        <div class="nav-out">
            <div class="nav-in">
                <div id="main-nav-cont" class="left" itemscope="" itemtype="http://schema.org/Organization">
                    <div class="nav-logo-out">
                        <div class="nav-left-wrap left relative">
                            <div class="fly-but-wrap left relative">
                            </div><!--fly-but-wrap-->
                            <div class="nav-logo left">
                                <h1>
                                    <a itemprop="url" href="/">
                                        <img itemprop="logo" src="<?php echo STATIC_URL;?>img/logo-vratzadnes.png" alt="����� ����">
                                    </a>
                                </h1>
                            </div><!--nav-logo-->
                        </div>
                        <div class="nav-logo-in">
                            <div class="nav-menu-out">
                                <div class="nav-menu-in">
                                    <nav class="main-menu-wrap left">
                                        <div class="menu-main-menu-container">
                                            <ul id="menu-main-menu-1" class="menu">
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-165">
                                                    <a href="/">Home</a>
                                                </li>
                                                <?php foreach($menu_pages as $menu_item) { ?>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10">
                                                <a href="<?php echo $menu_item['page_link']?>"><?php echo $menu_item['Name'];?></a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                                <!--nav-menu-in-->
                                <div class="nav-right-wrap relative">
                                    <div class="nav-search-wrap left relative">
                                        <span class="nav-search-but left"><i class="fa fa-search fa-2"></i></span>
                                        <div class="search-fly-wrap">
                                            <form method="get" id="searchform" action="http://www.mvpthemes.com/flexmag/">
                                                <input type="text" name="s" id="s" value="Type search term and press enter" onfocus="if (this.value == &quot;Type; search; term; and; press; enter&quot;) { this.value =; &quot;&quot; }" onblur="if (this.value == &quot;&quot;) { this.value =; &quot;Type; search; term; and; press; enter&quot; }">
                                                <input type="hidden" id="searchsubmit" value="Search">
                                            </form>
                                        </div>
                                        <!--search-fly-wrap-->
                                    </div>
                                    <!--nav-search-wrap-->
                                    <a href="http://www.facebook.com/envato" target="_blank">
                                        <span class="nav-soc-but"><i class="fa fa-facebook fa-2"></i></span>
                                    </a>
                                </div>
                                <!--nav-right-wrap-->
                            </div>
                            <!--nav-menu-out-->
                        </div>
                        <!--nav-logo-in-->
                    </div>
                    <!--nav-logo-out-->
                </div>
                <!--main-nav-cont-->
            </div>
            <!--nav-in-->
        </div>
        <!--nav-out-->
    </div>
    <!--main-nav-wrap-->
</div>
