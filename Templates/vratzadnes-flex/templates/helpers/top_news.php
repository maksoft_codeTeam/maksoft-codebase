<?php
$article = array_shift($articles);
?>
<div id="home-feat-wrap" class="left relative">
    <div class="home-feat-main left relative">
        <a href="<?=$article['page_link'];?>" rel="bookmark">
            <div id="home-feat-img" class="left relative">
                <img width="1000" height="600" src="<?=$article['image_src'];?>" class="reg-img wp-post-image" alt="" sizes="(max-width: 1000px) 100vw, 1000px">
                <img width="450" height="270" src="<?=$article['image_src'];?>" class="mob-img wp-post-image" alt="" sizes="(max-width: 450px) 100vw, 450px">
            </div>
            <!--home-feat-img-->
            <div id="home-feat-text">
                <span class="feat-cat"><?=$o_page->get_pInfo("Name", $article['ParentPage']);?></span>
                <h2><?=$article['Name'];?></h2>
                <p><?=cut_text($article['textStr'], 50)?></p>
            </div>
            <!--home-feat-text-->
            <div class="feat-info-wrap">
                <div class="feat-info-views">
                    <i class="fa fa-eye fa-2"></i> <span class="feat-info-text"><?=$article['preview'];?></span>
                </div>
                <!--feat-info-comm-->
            </div>
            <!--feat-info-wrap-->
        </a>
    </div>
    <!--home-feat-main-->
    <div class="feat-title-wrap">
        <h3 class="home-feat-title"><?=$group_info['group_title'];?></h3>
    </div>
    <!--feat-title-wrap-->
</div>
