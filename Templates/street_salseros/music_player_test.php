<?php
	require_once "lib/media/flowplayer/config.php";
?>
<script type="text/javascript" src="<?=FLOWPLAYER_LIBRARY_PATH?>"></script>

<table align="center" id="video_preview" border="0">
<td class="video_left">
<td class="content" align="center">

<img src="<?=DIR_TEMPLATE_IMAGES?>music_player_splash.jpg" width="580">
<!-- install flowplayer into container -->
<div id="audio" style="display:block;width:560px;height:30px; margin: -40px 10px 0 10px; position: relative;"></div>
<h1><?=str_replace(".mp3", "", str_replace("%20", " ", $music_files[$media_id]))?></h1>
<br clear="all">
<div class="clips petrol">
<?php
	for($i=0; $i<count($music_files); $i++)
			echo "<a href=\"page.php?n=$n&SiteID=$SiteID&media_id=".$i."\">".str_replace("%20", " ", $music_files[$i])."</a>";
?>	
</div>
<td class="video_right">
</table>
<table align="center">
<tr><td>
<div class="playlist">
<?php
	
	//read media directory
	$media_root_directory = "/hosting/maksoft/streetsalseros/music/";
	

	if(!isset($media_id)) $media_id = 0;
	if(!isset($category)) $category = 0;
	
	$media_dirs = array("Cuban Salsa", "Salsa in Line", "TOP 3 November 2010", "TOP 3 December 2010");
	$content = "";
	$subcontent = "";
	for($i=0; $i<count($media_dirs); $i++)
		{
			if($category == $i)
				{
					$content = "<a class=\"title selected\" href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;category=$i\">".$media_dirs[$i]."</a>";
					$clips = read_dir($media_root_directory.$media_dirs[$category], "mp3");
					$content.= "<div class=\"clips\">";
					for($j=0; $j<count($clips); $j++)
					{
						if($media_id == $j)
							$class = "playing";
						else $class = "";
						$content.= "<a href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;category=$i&amp;media_id=$j\" class=\"".$class."\">".($j+1).". ".str_replace("%20", " ", str_replace(".mp3", "", $clips[$j]))."</a>";
					}
					$content.= "</div>";
				}
			else $subcontent.= "<a class=\"title\" href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;category=$i\">".$media_dirs[$i]."</a>";
		}
	echo $content . $subcontent;
?>		
</div>
</table>
<script>
// install flowplayer into container
// setup player 
$f("player", "http://releases.flowplayer.org/swf/flowplayer-3.2.5.swf", {

	// common clip: these properties are common to each clip in the playlist
	clip: { 
		// by default clip lasts 5 seconds
		duration: 5		
	},
	
	// playlist with four entries
	playlist: [
		
		// JPG image
		'/img/tutorial/tap-splash.jpg', 
		
		// SWF file
		{url: 'http://releases.flowplayer.org/swf/clock.swf', scaling: 'fit'},
		
		// video
		'http://blip.tv/file/get/KimAronson-TwentySeconds58192.flv',
		
		// another image. works as splash for the audio clip
		{url: '/img/demos/national.jpg', duration: 0},
		
		// audio, requires audio plugin. custom duration
		{url: 'http://releases.flowplayer.org/data/fake_empire.mp3', duration: 25}		

	],
	
	// show playlist buttons in controlbar
	plugins:  {
		controls: {
			playlist: true,
			
			// use tube skin with a different background color
			url: 'flowplayer.controls-tube-3.2.3.swf', 
			backgroundColor: '#aedaff'
		} 
	}	
});
</script>