<?php
	require_once "lib/video/flowplayer/config.php";
?>
<script type="text/javascript" src="<?=FLOWPLAYER_LIBRARY_PATH?>"></script>

<script type="text/javascript" src="<?=FLOWPLAYER_DIR_PLUGINS?>flowplayer.playlist-3.0.8.js"></script>
<link href="<?=FLOWPLAYER_DIR_PLUGINS?>playlist.css" rel="stylesheet">
<div class="clips petrol" style="float:left">
	
	<!-- single playlist entry -->
	<a href="5th_FantaSalsaFestival_SS_team.flv" class="first">
		Palm trees and the Sun
		<span>HTTP streaming</span>
		<em>0:20 min</em>
	</a>
	
	<a href="4th_FantaSalsaFestival_SS_team.flv">
		Happy feet inside a car
		<span>HTTP streaming</span>
		<em>0:20 min</em>	
	</a>
	
	<a href="anounce.flv">
		Park side life
		<span>HTTP streaming</span>
		<em>0:20 min</em>	
	</a>
	
</div>
		
<!-- player container and a splash image (play button) -->
<a	 class="player plain"
	 href="http://images.streetsalseros.com/video/anounce.flv"  
	 style="display:block;width:400px;height:300px"  
	 id="player"> 
</a> 

<!-- let rest of the page float normally -->
<br clear="all"/>
<script>
	$j(function() {
	
	// setup player without "internal" playlists
	$f("player", "<?=FLOWPLAYER_VERSION_PATH?>", {
		clip: {baseUrl: 'http://images.streetsalseros.com/video'} 
		
		
	// use playlist plugin. again loop is true
	}).playlist("div.petrol", {loop:true});
	
});
</script>
