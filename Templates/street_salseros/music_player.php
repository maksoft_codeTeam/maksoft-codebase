<?php
	require_once "lib/media/flowplayer/config.php";
?>
<script type="text/javascript" src="<?=FLOWPLAYER_LIBRARY_PATH?>"></script>

<table align="center" id="video_preview" border="0">
<td class="video_left">
<td class="content" align="center">

<img src="<?=DIR_TEMPLATE_IMAGES?>music_player_splash.jpg" width="580">
<!-- install flowplayer into container -->
<div id="audio" style="display:block;width:560px;height:30px; margin: -40px 10px 0 10px; position: relative;"></div>
<h1><?=str_replace(".mp3", "", str_replace("%20", " ", $music_files[$media_id]))?></h1>
<br clear="all">
<div class="clips petrol">
<?php
	for($i=0; $i<count($music_files); $i++)
			echo "<a href=\"page.php?n=$n&SiteID=$SiteID&media_id=".$i."\">".str_replace("%20", " ", $music_files[$i])."</a>";
?>	
</div>
<td class="video_right">
</table>
<table align="center">
<tr><td>
<div class="playlist">
<?php
	
	//read media directory
	$media_root_directory = "/hosting/maksoft/streetsalseros/music/";
	

	if(!isset($media_id)) $media_id = 0;
	if(!isset($category)) $category = 0;
	
	//$media_dirs = array("Cuban Salsa", "Salsa in Line", "TOP 3 November 2010", "TOP 3 December 2010", "For Beginers/Bachata", "For Beginers/Merengue", "For Beginers/Salsa");
	
	$media_dirs = array(
		array("dir"=>"cuban_salsa", "title"=>"Cuban Salsa"),
		array("dir"=>"salsa_in_line", "title"=>"Salsa in Line"),
		array("dir"=>"top_3_november_2010", "title"=>"Top 3 November 2010"),
		array("dir"=>"top_3_december_2010", "title"=>"Top 3 December 2010"),
		array("dir"=>"for_beginers/bachata", "title"=>"Beginners - Bachata"),
		array("dir"=>"for_beginers/merengue", "title"=>"Beginners - Merengue"),
		array("dir"=>"for_beginers/salsa_counting", "title"=>"Beginners - Salsa counting")
		);
				
	$content = "";
	$subcontent = "";
	for($i=0; $i<count($media_dirs); $i++)
		{
			if($category == $i)
				{
					$content = "<a class=\"title selected\" href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;category=$i\">".$media_dirs[$i]['title']."</a>";
					$clips = read_dir($media_root_directory.$media_dirs[$category]['dir'], "mp3");
					$content.= "<div class=\"clips\">";
					for($j=0; $j<count($clips); $j++)
					{
						if($media_id == $j)
							$class = "playing";
						else $class = "";
						$content.= "<a href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;category=$i&amp;media_id=$j\" class=\"".$class."\">".($j+1).". ".str_replace("%20", " ", str_replace(".mp3", "", $clips[$j]))."</a>";
					}
					$content.= "</div>";
				}
			else $subcontent.= "<a class=\"title\" href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;category=$i\">".$media_dirs[$i]['title']."</a>";
		}
	echo $content . $subcontent;
?>		
</div>
</table>
<script>
// install flowplayer into container
$f("audio", "<?=FLOWPLAYER_VERSION_PATH?>", {

	plugins: {
				controls:	{
								// tooltips configuration
								tooltips: {
									
									// enable english tooltips on all buttons
									buttons: true,
									
									// customized texts for buttons
									play: '�����',
									pause: '�����',
									fullscreen: '��� �����',
									mute: '���� �����'
								},
								
								// background color for all tooltips
								tooltipColor: '#891fa3',
								
								// text color
								tooltipTextColor: '#FFFFFF', 

								fullscreen: false,
								height: 30,
								autoHide: false,
								playlist: true
							},
				audio: 		{
								url: '<?=FLOWPLAYER_PLUGIN_AUDIO?>'
							}

	},
	clip:	{
					
				autoPlay: true,
				onBeforeBegin: function() 
					{
						$f("audio").close();
					},
				url: 'http://images.streetsalseros.com/music/<?=$media_dirs[$category]['dir']."/".$clips[$media_id]?>'
	}
	
});
</script>