<?php
	require_once "lib/media/flowplayer/config.php";
	if(!isset($video_file))
		$video_file = "http://images.streetsalseros.com/video/anounce.flv";
?>
<script type="text/javascript" src="<?=FLOWPLAYER_LIBRARY_PATH?>"></script>

<script type="text/javascript" src="<?=FLOWPLAYER_DIR_PLUGINS?>flowplayer.playlist-3.0.8.js"></script>
<link href="<?=FLOWPLAYER_DIR_PLUGINS?>playlist.cs" rel="stylesheet">
<table align="center" id="video_preview" border="0">
<td class="video_left">
<td class="content">
<!-- player container and a splash image (play button) -->
<a	 class="player plain"
	 href="<?=$video_file?>"  
	 style="display:block;width:500px;height:375px; padding: 0; margin: 0"  
	 id="player"> 
</a> 
<td class="video_right">
</table>
<table align="center">
<tr><td>
<div class="playlist">
<?php
	//read clips directory
	$clips_root_directory = "/hosting/maksoft/streetsalseros/video/lesons/";
	
	if(!isset($category)) $category = 0;
	
	$clip_dirs = array("bachata", "cuban", "la", "merengue", "salsa");
	$content = "";
	$subcontent = "";
	for($i=0; $i<count($clip_dirs); $i++)
		{
			if($category == $i)
				{
					$content = "<a class=\"title selected\" href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;category=$i\">".$clip_dirs[$i]."</a>";
					$clips = read_dir($clips_root_directory.$clip_dirs[$category], "flv");
					$content.= "<div class=\"clips\">";
					for($j=0; $j<count($clips); $j++)
						$content.= "<a href=\"".$clips[$j]."\">".($j+1).". ".str_replace("_", " ", str_replace(".flv", "", $clips[$j]))."</a>";
					$content.= "</div>";
				}
			else $subcontent.= "<a class=\"title\" href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;category=$i\">".$clip_dirs[$i]."</a>";
		}
	echo $content . $subcontent;
?>		
</div>
</table>
<script>
	$j(function() {
	
	// setup player without "internal" playlists
	$f("player", "<?=FLOWPLAYER_VERSION_PATH?>", {

		plugins:  {
			controls: {
				
				// tooltips configuration
				tooltips: {
					
					// enable english tooltips on all buttons
					buttons: true,
					
					// customized texts for buttons
					play: '�����',
					pause: '�����',
					fullscreen: '��� �����',
					mute: '���� �����'
				},
				
				// background color for all tooltips
				tooltipColor: '#891fa3',
				
				// text color
				tooltipTextColor: '#FFFFFF' 
			}
		}, 

		clip: {
				baseUrl: 'http://images.streetsalseros.com/video/lesons/<?=$clip_dirs[$category]?>',
				autoPlay: true, 
				autoBuffering: true
			} 
		
		
	// use playlist plugin. again loop is true
	}).playlist("div.clips", {
		loop:false,
		playOnClick: true
		});
	
});
</script>
<!-- let rest of the page float normally -->
<br clear="all"/>