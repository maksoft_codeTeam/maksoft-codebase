<?php
	//template base/tmpl_001 configurations
	
	define("TEMPLATE_NAME", "maksoftbg_responsive");
	define("TEMPLATE_DIR", "Templates/".TEMPLATE_NAME."/");
	define("TEMPLATE_IMAGES", "https://maksoft.net/".TEMPLATE_DIR."images/");
	define("ASSETS_DIR", "/".TEMPLATE_DIR."");
	define("SITE_URL", "".$o_page->scheme.$o_page->_site['primary_url']."");

	//show what options are available for config
	
	$tmpl_config_support = array(
		"change_theme"=>true,					// full theme support
		"change_layout"=>true,					// layout support
		"change_banner"=>true,				// banner support
		//"change_css"=>true,						// css support
		"change_background"=>true,		// background support
		"change_fontsize"=>true,				// fontsize support
		"change_max_width"=>true,			// site width support
		"change_menu_width"=>false,		// site menu width support
		"change_main_width"=>true,		// page content width support
		"change_logo"=>true,						// logo support
		"column_left" => true, 						// column left support
		"column_right" => true,					// column right support
		"drop_down_menu"=> true			// drop-down menu support
	);
	
	//define some default values
	$tmpl_config = array(
		"default_max_width"=> ($o_site->get_sConfigValue('CMS_MAX_WIDTH'))? $o_site->get_sConfigValue('CMS_MAX_WIDTH') : "960",
		"default_main_width"=>660,
		"default_menu_width"=>180,
		"default_banner_width" => ($o_site->get_sConfigValue('CMS_MAX_WIDTH'))? $o_site->get_sConfigValue('CMS_MAX_WIDTH') : "960",
		"default_banner" => TEMPLATE_IMAGES."banner.jpg",
		"default_logo" => TEMPLATE_DIR."images/logo.png",
		"default_logo_width" =>($o_site->get_sConfigValue('TMPL_LOGO_SIZE'))? $o_site->get_sConfigValue('TMPL_LOGO_SIZE') : "180",
		"menu_drop_down" => TEMPLATE_DIR . "menu_drop_down.php",
		"column_left" => array(
								TEMPLATE_DIR . "menu.php", 
								TEMPLATE_DIR . "boxes/second_links.php",
								/* TEMPLATE_DIR . "boxes/sites.php", */
								TEMPLATE_DIR . "boxes/banner.php",
								TEMPLATE_DIR . "boxes/contact_form.php"
								),
		"column_right" => array(
								
								//TEMPLATE_DIR . "boxes/last_visited.php", 
								//TEMPLATE_DIR . "boxes/shopping_cart.php", 
								//TEMPLATE_DIR . "boxes/banners.php",
								//TEMPLATE_DIR . "boxes/news.php",
								//TEMPLATE_DIR . "boxes/second_links.php",
								//TEMPLATE_DIR . "boxes/sites.php"
								//"web/forms/maksoft/faqs2.php"
								TEMPLATE_DIR . "menu.php"
								),
		"footer" => array(
								TEMPLATE_DIR . "boxes/news.php"
							),		
		"date_format"=> "d-m-Y"
	);
	
	//overwrite column_right
	/*
	if(isset($column_right))
		$tmpl_config['column_right']	 = $column_right;
	*/
	if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->get_sImageDIr()."/".basename($o_site->_site['Logo']);
		
function current_season() {
       // Locate the icons
       $icons = array(
               "spring" => "". TEMPLATE_IMAGES ."maksoftbg-logo.png",
               "summer" => "". TEMPLATE_IMAGES ."maksoftbg-logo.png",
               "autumn" => "". TEMPLATE_IMAGES ."maksoftbg-christmas.png",
               "winter" => "". TEMPLATE_IMAGES ."maksoftbg-logo.png"
       );

       // What is today's date - number
       $day = date("z");

       //  Days of spring
       $spring_starts = date("z", strtotime("March 21"));
       $spring_ends   = date("z", strtotime("June 20"));

       //  Days of summer
       $summer_starts = date("z", strtotime("June 21"));
       $summer_ends   = date("z", strtotime("September 22"));

       //  Days of autumn
       $autumn_starts = date("z", strtotime("December 01"));
       $autumn_ends   = date("z", strtotime("December 31"));

       //  If $day is between the days of spring, summer, autumn, and winter
       if( $day >= $spring_starts && $day <= $spring_ends ) :
               $season = "spring";
       elseif( $day >= $summer_starts && $day <= $summer_ends ) :
               $season = "summer";
       elseif( $day >= $autumn_starts && $day <= $autumn_ends ) :
               $season = "autumn";
       else :
               $season = "winter";
       endif;

       $image_path = $icons[$season];

       echo $image_path;
}		

?>