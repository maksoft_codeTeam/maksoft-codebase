<!DOCTYPE HTML>
<html lang="<?php echo($Site->language_key); ?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
	<title><?=$Title?></title>
    <?php
		//include template configurations
		include("Templates/maksoftbg_responsive/configure.php");		
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel='shortcut icon' type='image/x-icon' href='<?=ASSETS_DIR?>images/favicon.ico' />
	<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_DIR?>layout.css" id="layout-style" rel="stylesheet" type="text/css" />
    <link href="/css/admin_classes.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_DIR?>base_style.css" id="base-style" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="/lib/bootstrap/js/bootstrap.min.js"></script>
    <script language="javascript" type="text/javascript" src="<?=ASSETS_DIR?>effects.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js"></script>

    <?php 
    /* if curdate == 01-12-20XX  and not bigger than 07-01-20XX => start snow */
    if (date('d-m-Y')== date('01-12-Y') and date('d-m-Y') < date("07-01-Y")){
	 echo '  <script src="/Templates/maksoft/snowfall.jquery.js"></script>
             <script language="JavaScript" type="text/javascript">
             $j(document).ready(function(){
                 $j(document).snowfall();
                 $j(".sPage a.title").before("<div class=\"snow\"></div>");
             })
             </script>
           ';
    } ?>
	<?php
		
		if(defined("TMPL_BODY_BACKGROUND") || defined("TMPL_LOGO_POSITION"))
		{
			?>


			<style media="all">
			<?
			//CUSTOM TEMPLATE BACKGROUND
			if(defined("TMPL_BODY_BACKGROUND")) 
			{
			?>
				body{background: url(<?=TMPL_BODY_BACKGROUND?>) 50% 0% fixed !important;}
				#header, #footer {background: transparent !important; box-shadow: none !important;}
			<?
			}
			
			//LOGO POSITION
			if(defined("TMPL_LOGO_POSITION"))
			{
				$logo_margin = explode("x", TMPL_LOGO_POSITION);
			?>
				#header .logo {margin: <?=$logo_margin[1]?>px <?=$logo_margin[0]?>px}
			<?
			}
			
			if(defined("CSS_FONT_SIZE"))
			{
			?>
				#pageContent, #pageContent p {font-size: <?=CSS_FONT_SIZE?>em}
			<?
			}
			
			?>
            </style>
			<?
		}

	//get site news
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 5");
	
	//get most viewed pages
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5", "p.n != '".$o_site->_site['StartPage']."'");

	//get last added pages
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5", "p.preview>30");
	
	//get last sites
	$sites = $o_page->get_pSubpages(267, "p.date_added DESC LIMIT 4");
	
	//home news from group Home News
	$home_news = $o_page->get_pGroupContent(623, NULL, "RAND()");
	
	if(defined('GOOGLE_TRACKING_CODE'))   { include_once("web/admin/modules/google/analitycs.php"); }
	if( (defined('GOOGLE_SITE_VERIF')) && ($o_page->n == $o_page->_site['StartPage']) )   { include_once("web/admin/modules/google/wmt.php"); }
?>
</head>
<body>
<?php
 include_once("Templates/header_inc.php");
 
 include_once("web/admin/modules/google/analitycs.php");
 include_once("web/admin/modules/social/fb_app_init.php");
?>
<div id="site_container">
    <div id="header">
        	<?php include TEMPLATE_DIR."header.php"?>
    </div>
	<div id="page_container" class="shadow" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
        <div class="main-content">
        <?php
			/*
			if($user->ID == 196)
			{
			*/
			// Page Template
			$pTemplate = $o_page->get_pTemplate();
			if($pTemplate['pt_url'])
				include $pTemplate['pt_url'];
			else
				include TEMPLATE_DIR . "main.php";
			/*
			}
			else
				include TEMPLATE_DIR."main.php";
			*/
		?>
        </div>
    </div>
    <div id="footer">
    	<div class="footer-content" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
		<?php
			include TEMPLATE_DIR . "footer.php";
		?>
        <br clear="all">
        <div class="copyrights"><?=$o_site->get_sCopyright() . " " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="<?=$o_page->scheme?>www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a></div>
        </div> 
    </div>
</div>

<?php include "Templates/footer_inc.php"; ?>
<!--<script type="text/javascript" src="web/assets/auto-search/js/search_autocmpl.js"></script> -->
<?php
    if($_SESSION['reload']){
        $_SESSION['reload'] = false;
        ?> <script> document.location = "page.php?n=13202&SiteID=<?=$SiteID?>";</script> <?
    }
?>
</body>
</html>
