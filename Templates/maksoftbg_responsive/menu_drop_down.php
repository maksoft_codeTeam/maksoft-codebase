<?php
	if(!isset($drop_links)) $drop_links = 2;
	$dd_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $drop_links");
	if(count($dd_links) > 0)
		{
			?>
			<link href="/lib/menu/ddsmoothmenu/ddsmoothmenu.css" rel="stylesheet" type="text/css" />
			<link href="/lib/menu/ddsmoothmenu/ddsmoothmenu-v.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="/lib/menu/ddsmoothmenu/ddsmoothmenu.js"></script>
			<script type="text/javascript">
			ddsmoothmenu.init({
				mainmenuid: "top_menu",
				orientation: 'h',
				classname: 'ddsmoothmenu',
				//customtheme: ["#1c5a80", "#18374a"],
				contentsource: "markup"
			})
			</script>
			<div class="top-menu-container">
                <div id="top_menu"class="ddsmoothmenu rounded" style="display: block; height: 40px; clear:both;" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
					<div class="menuhider">���� <div class="pull-right"><div class="menu-bars"></div></div></div>
                <ul><li><?=$o_page->print_pName(true, $o_site->_site['StartPage'])?></li></ul>
				<?php print_menu($dd_links, array('menu_depth'=>1)); ?>
                </div>
			</div>
			<script type="text/javascript">
			$j(document).ready(function(){
				$j(".menuhider").on("click", function(){
					var theclass = $j(this).find(".pull-right").find("div").attr("class");
					if(theclass=="menu-bars"){
						$j(".menuhider .menu-bars").removeAttr("class").addClass("menu-close");
						$j("#top_menu>ul").show();
					}else{
						$j(".menuhider .menu-close").removeAttr("class").addClass("menu-bars");
						$j("#top_menu>ul").hide();
					}
				});
				
			});
			</script>
			<?
		}
?>