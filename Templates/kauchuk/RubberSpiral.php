<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>

<body>
<TABLE width="669" border=1>
  <!--DWLayoutTable-->
  <TBODY>

  <tr bgcolor="#006633"><TD colSpan=6> <DIV align=center><FONT color=#ffffff size=+2><B>RUBBER 
          - TEXTILE SPIRAL HOSES<br>
          </B></FONT><font color="#FFFFFF"><strong>MANDREL MADE</strong></font><FONT color=#ffffff size=+2><B><br>
          </B></FONT></DIV></TD>
  </TR>
    <TR> 
      <TD width="16" rowSpan=4 bgColor=#007ABE><DIV align=center>
        <p><font color=#000000><b><font 
    color=#ffffff>W</font></b></font></p>
        <p><font color=#000000><b><font 
    color=#ffffff>A</font></b></font></p>
        <p><font color=#000000><b><font 
    color=#ffffff>T</font></b></font></p>
        <p><font color=#000000><b><font 
    color=#ffffff>E</font></b></font></p>
        <p><font color=#000000><b><font 
    color=#ffffff>R</font></b></font></p>
      </DIV>
      </TD>
      <TD colSpan=2 bgColor=#233271><B><FONT color=#ffffff>RW-4 Suction for Water</FONT></B></TD>
      <TD width="14" rowSpan=4 bgColor=#DF0029><DIV align=center></DIV>
        <DIV align=center>
          <p><font color=#000000><b><font 
    color=#FFFFFF>F</font></b></font></p>
          <p><font color=#FFFFFF><b>O</b></font></p>
          <p><font color=#FFFFFF><b>O</b></font></p>
          <p><font color=#FFFFFF><b>D</b></font></p>
      </DIV></TD>
      <TD bgColor=#999999 colSpan=2> <div align="left"><B><font color="#FFFFFF">RF-2 Suction for Foodstuffs</font></B></div></TD>
  </TR>
    <TR> 
      <TD width="170" bgColor=#ffffff> <DIV align=center><IMG height=60 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG4.jpg" 
      width=170></DIV></TD>
      <TD width="110" bgColor=#D7D7D7> <DIV align=center><strong>airtight for water suction</strong></DIV></TD>
      <TD width="170" bgColor=#ffffff> <DIV align=center><IMG height=57 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG14.jpg" 
      width=170></DIV></TD>
      <TD width="149" bgColor=#cccccc> <DIV align=center><strong>for milk, beer, wine, 
          ethyl alcohol and other liquid foods </strong></DIV>
      <DIV align=center></DIV></TD>
    </TR>
    <TR> 
      <TD colSpan=2 bgColor=#233271><FONT color=#ffffff><B>RW-5 Suction-Discherge 
        for Water</B></FONT></TD>
      <TD bgColor=#BC0021 colSpan=2><div align="left"><B><FONT color=#FFFFFF>RF-3 Suction-Discharge 
      for Foodstuffs</FONT></B></div></TD>
    </TR>
    <TR> 
      <TD bgColor=#ffffff> <DIV align=center><img height=56 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG11.jpg" 
      width=170></DIV></TD>
      <TD bgColor=#D7D7D7> <DIV align=center><strong>suction
      - discharge of water</strong></DIV></TD>
      <TD bgColor=#ffffff> <DIV align=center><img src="/Templates/kauchuk/4erven_spirala%20copy.jpg" width="176" height="74"></DIV></TD>
      <TD bgColor=#cccccc> <DIV align=center><strong>for milk, beer, wine, 
          ethyl alcohol and other liquid foods </strong></DIV>
      <DIV align=center></DIV></TD>
    </TR>
    <TR> 
      <TD bgColor=#F7E400 rowSpan=2><DIV align=center>
        <p><font color=#333300><strong>A</strong></font></p>
        <p><strong><font color="#333300">I</font></strong></p>
        <p><strong><font color="#333300">R</font></strong></p>
      </DIV></TD>
      <TD colSpan=2 bgColor=#F3C200><strong>RA-2 Foamy-Vortex Dust Catchers</strong></TD>
      <TD bgColor=#EE9B00 rowSpan=2><DIV align=center>
        <p><strong>P</strong></p>
        <p><strong>M</strong></p>
      </DIV></TD>
      <TD bgColor=#ff9900 colSpan=2><div align="left"><B>PM-2 Suction-Discharge for Powdered and 
        Abrasive Materials</B></div></TD>
  </TR>
    <TR> 
      <TD bgColor=#ffffff><DIV align=center><IMG height=58 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG16.jpg" 
      width=170></DIV></TD>
      <TD bgColor=#D7D7D7><DIV align=center><strong>for completing of mashines 
      and equipment</strong></DIV></TD>
      <TD bgColor=#ffffff> <DIV align=center><IMG height=58 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG10.jpg" 
      width=170></DIV></TD>
      <TD bgColor=#cccccc> <DIV align=center><strong>suction- discharge of 
      powdered materials and their water suspensions concrete, cement, sand</strong></DIV></TD>
    </TR>
    <TR> 
      <TD bgColor=#6F067B rowSpan=4><DIV align=center>
        <p><b><font color=#ffffff>A</font></b></p>
        <p><font color="#ffffff"><b>C</b></font></p>
        <p><font color="#ffffff"><b>I</b></font></p>
        <p><font color="#ffffff"><b>D</b></font></p>
      </DIV>
      </TD>
      <TD colSpan=2 bgColor=#53035C><FONT color=#ffffff><B>RK-2 Suction for Acids 
        and Alkali</B></FONT></TD>
      <TD bgColor=#7D7D7D rowSpan=6><DIV align=center></DIV>
        <DIV align=center>
          <p><font color="#ffffff"><strong>O</strong></font></p>
          <p><strong><font color="#ffffff">I</font></strong></p>
          <p><strong><font color="#ffffff">L</font></strong></p>
      </DIV></TD>
      <TD bgColor=#474747 colSpan=2><FONT color=#ffffff><B>RO-4 Suction-Discharge 
        for Petroleum and Mineral Oils</B></FONT></TD>
  </TR>
    <TR> 
      <TD bgColor=#ffffff> <DIV align=center><IMG height=60 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG4.jpg" 
      width=170></DIV></TD>
      <TD bgColor=#D7D7D7> <DIV align=center><strong>for strong and weak acids 
          and alcali solutions</strong></DIV>
      <DIV align=center></DIV></TD>
      <TD bgColor=#ffffff> <DIV align=center><IMG height=58 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG9.jpg" 
      width=170></DIV></TD>
      <TD bgColor=#D7D7D7> <DIV align=center><strong>suction of liquids on 
      petroleum basis</strong></DIV></TD>
    </TR>
    <TR> 
      <TD colSpan=2 bgColor=#53035C><FONT color=#ffffff><B>RK-3 Suction-Discharge 
        for Acids and Alkali</B></FONT></TD>
      <TD bgColor=#474747 colSpan=2><FONT color=#ffffff><B>RO-5 Suction for Bensine, 
        Kerosine, Gas Oil</B></FONT></TD>
    </TR>
    <TR> 
      <TD bgColor=#ffffff> <DIV align=center><IMG height=58 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG9.jpg" 
      width=170></DIV></TD>
      <TD bgColor=#D7D7D7> <DIV align=center><strong>for strong and weak acids 
          and alcali solutions</strong></DIV>
      <DIV align=center></DIV></TD>
      <TD bgColor=#ffffff> <DIV align=center><IMG height=56 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG8.jpg" 
      width=170></DIV></TD>
      <TD bgColor=#D7D7D7> <DIV align=center><strong>suction of bensine, kerosine 
      and gas oil with antistatic metal wire</strong></DIV></TD>
    </TR>
    <TR> 
      <TD bgColor=#7D7D7D rowSpan=2><p><font color="#ffffff"><strong><br>
        O</strong></font></p>
        <p><strong><font color="#ffffff">I</font></strong></p>
        <p><strong><font color="#ffffff">L</font></strong></p>
        <DIV align=center></DIV>
      <p><font color="#ffffff"><strong> </strong></font></p></TD>
      <TD colSpan=2 bgColor=#474747><FONT color=#ffffff><strong>RO-3 Suction for Petroleum 
        and Mineral Oils</strong></FONT></TD>
      <TD bgColor=#474747 colSpan=2><FONT color=#ffffff><strong>RO-6 Suction-Discharge 
        for Bensine, Kerosine, Gas Oil</strong></FONT></TD>
  </TR>
    <TR> 
      <TD bgColor=#ffffff> <DIV align=center><IMG height=60 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG4.jpg" 
      width=170></DIV></TD>
      <TD bgColor=#D7D7D7> <DIV align=center><strong>suction of liquids on 
      petroleum basis</strong></DIV></TD>
      <TD bgColor=#ffffff> <DIV align=center><IMG height=56 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG8.jpg" 
      width=170></DIV></TD>
      <TD bgColor=#D7D7D7> <DIV align=center><strong>suction-discharge of 
      bensine, kerosine and gas oil with antistatic metal wire</strong></DIV></TD>
    </TR>
    <TR bgcolor="#006633">
      <TD colspan="6" valign="top"><div align="left"><font color="#FFFFFF" size="1"><strong><font color="#FFFFFF">APLICATION:
                IN ALL INDUSTRIAL BRANCHES, AGRICULTURE,CONSTRUCTION WORKS, TRANSPORT
                AND DAILY NEEDS.</font><br> 
        </strong></font></div></TD>
    </TR>
</TABLE>
</body>
</html>
