<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>

<body>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="828" border=1>
  <!--DWLayoutTable-->
  <tbody>
  <tr bgcolor="#006633"><td colspan=6>
      <div align="center"><font color=#ffffff size=+2><b>RUBBER - TEXTILE SMOOTH
            HOSES</b></font> <font color="#FFFFFF"><strong> <br>
        FLEXIBLE MADE <br>
        MANDREL MADE</strong></font><font color=#ffffff size=+2><b><br>
      </b></font></div>
    </td>
  </tr>
  <tr>
    <td width="16" rowspan=6 valign="top" bgcolor=#007ABE><div align=center>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><br>
        <font color=#000000><b><font 
    color=#ffffff>W</font></b></font></p>
      <p><font color=#000000><b><font 
    color=#ffffff>A</font></b></font></p>
        <p><font color=#000000><b><font 
    color=#ffffff>T</font></b></font></p>
        <p><font color=#000000><b><font 
    color=#ffffff>E</font></b></font></p>
        <p><font color=#000000><b><font 
    color=#ffffff>R</font></b></font></p>
      </div>
    </td>
    <td colspan=2 bgcolor=#233271><b><font color=#ffffff><strong>RW-1
            Water /Schlauch/</strong></font></b></td>
    <td width="16" rowspan=6 valign="top" bgcolor=#00B2EA><div align=center></div>
        <div align=center>
          <p><font color=#000000><strong><font size="1">G</font></strong></font></p>
          <p><font size="1"><strong><font color=#000000>A</font></strong></font></p>
          <p><font size="1"><strong><font color=#000000>S</font></strong></font></p>
          <p><font size="1"><strong><font color=#000000>W</font></strong></font></p>
          <p><font size="1"><strong><font color=#000000>E</font></strong></font></p>
          <p><font size="1"><strong><font color=#000000>L</font></strong></font></p>
          <p><font size="1"><strong><font color=#000000>D</font></strong></font></p>
          <p><font size="1"><strong><font color=#000000>I</font></strong></font></p>
          <p><font size="1"><strong><font color=#000000>N</font></strong></font></p>
          <p><font size="1"><strong><font color=#000000>G</font></strong></font></p>
        </div>
    </td>
    <td bgcolor=#007ABE colspan=2>
      <div align="left"><b><font color="#FFFFFF">RG-2 (1) Gas Welding and Metal
            Cutting</font></b></div>
    </td>
  </tr>
  <tr>
    <td width="172" bgcolor=#ffffff>
      <div align=center><img height=52 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG13.jpg" 
      width=170></div>
    </td>
    <td width="185" bgcolor=#D7D7D7>
      <div align=center><strong>for watering gardens, black</strong></div>
    </td>
    <td width="170" bgcolor=#ffffff>
      <div align=center><img height=52 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/I1.jpg" 
      width=170></div>
    </td>
    <td width="229" bgcolor=#cccccc>
      <div align=center><strong>for oxygen - blue coloured,
          black - with colour strip blue</strong></div>
      <div align=center></div>
    </td>
  </tr>
  <tr>
    <td colspan=2 bgcolor=#233271><font color=#ffffff><b><font color=#ffffff>RW-2
            Water</font></b></font></td>
    <td bgcolor=#007ABE colspan=2><div align="left"><b><font color="#FFFFFF">RG-2
            (2) Gas Welding and Metal Cutting</font></b></div>
    </td>
  </tr>
  <tr>
    <td bgcolor=#ffffff>
      <div align=center><img height=51 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG7.jpg" 
      width=170></div>
    </td>
    <td bgcolor=#D7D7D7>
      <div align=center><strong>for water under pressure, black/coloured</strong></div>
    </td>
    <td bgcolor=#ffffff>
      <div align=center><img src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/rg22.jpg" 
      width=170></div>
    </td>
    <td bgcolor=#cccccc>
      <div align=center><strong>for acetylene- red coloured, black - with colour
          strip </strong></div>
      <div align=center></div>
    </td>
  </tr>
  <tr>
    <td colspan=2 bgcolor=#233271><b><font color=#ffffff>RW-3 (1,2)
          Saturated Steam and Hot Water</font></b></td>
    <td bgcolor=#007ABE colspan=2><div align="left"><b><font color="#FFFFFF">RG-2
            (3) Gas Welding and Metal Cutting</font></b></div>
    </td>
  </tr>
  <tr>
    <td height="143" valign="top" bgcolor=#ffffff><div align=center>      <br>
      <br>
      <img height=51 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG3.jpg" 
      width=170><br>
      <br>
      <br>
    </div>
    </td>
    <td valign="top" bgcolor=#D7D7D7><div align=center>
      <p><strong><br>
        for saturated steam with </strong><strong>t
              +130�C<br>
            </strong><strong>and t +150�C<br>
                </strong><strong>for water with <br>
          </strong><strong>t to +100�C</strong></p>
      </div>
    </td>
    <td bgcolor=#ffffff><div align=center><img src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/orang.jpg" width="170" height="51"></div>
    </td>
    <td bgcolor=#cccccc><div align="center"><strong>for propane - butane - orange
          coloured, black with colour strip for hydrogen and natural gas - black</strong></div></td>
  </tr>
  <tr>
    <td bgcolor=#F7E400 rowspan=4><div align=center>
        <p><font color=#333300><strong>A</strong></font></p>
        <p><strong><font color="#333300">I</font></strong></p>
        <p><strong><font color="#333300">R</font></strong></p>
      </div>
    </td>
    <td colspan=2 bgcolor=#F3C200><p><font color=#ffffff><b> RA-1
            (1) Air<br>
      </b></font></p>
    </td>
    <td rowspan=4 valign="top" bgcolor=#EE9B00><div align=center></div>
        <div align=center>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p><strong>P</strong></p>
          <p><strong>M</strong></p>
        </div>
    </td>
    <td bgcolor=#EE9B00 colspan=2><font color=#ffffff><b>PM-1 (1) Powdered and
          Abrasive Materials</b></font></td>
  </tr>
  <tr>
    <td valign="top" bgcolor=#ffffff>
      <div align=center>
        <p>        <img height=51 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG7.jpg" 
      width=170></p>
      </div>
    </td>
    <td bgcolor=#D7D7D7>
      <div align=center><strong>for air under pressure, black/coloured</strong></div>
      <div align=center></div>
    </td>
    <td bgcolor=#ffffff>
      <div align=center><img height=51 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG3.jpg" 
      width=170></div>
    </td>
    <td bgcolor=#D7D7D7>
      <div align=center><strong>for powdered materials and their water suspensions
          - concrete, cement, sand</strong></div>
    </td>
  </tr>
  <tr>
    <td colspan=2 bgcolor=#F3C200><font color=#ffffff><b>RA-1 (2)
          Air<br>
    </b></font></td>
    <td bgcolor=#EE9B00 colspan=2><font color=#ffffff><b>PM-1 (2) Powdered and
          Abrasive Materials</b></font></td>
  </tr>
  <tr>
    <td bgcolor=#ffffff>
      <div align=center><img height=51 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG3.jpg" 
      width=170></div>
    </td>
    <td bgcolor=#D7D7D7>
      <div align=center><strong>for air under pressure, black/coloured</strong></div>
      <div align=center></div>
    </td>
    <td bgcolor=#ffffff>
      <div align=center><img height=53 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG17.jpg" 
      width=170></div>
    </td>
    <td bgcolor=#D7D7D7>
      <div align=center><strong>for powdered materials and their water suspensions
          - concrete, cement, sand</strong></div>
    </td>
  </tr>
  <tr>
    <td rowspan=2 valign="top" bgcolor=#DF0029><p><font color=#000000><b><font 
    color=#FFFFFF>F</font></b></font></p>      <p align="center"><font color=#FFFFFF><b>O</b></font></p>      <p align="center"><font color=#FFFFFF><b>O</b></font></p>      <p align="center"><font color=#FFFFFF><b>D</b></font></p>
    </td>
    <td colspan=2 bgcolor=#BC0021><font color=#FFFFFF><b>RF-1 Foodstuffs</b></font><font color=#ffffff>&nbsp;</font></td>
    <td rowspan=4 valign="top" bgcolor=#7D7D7D><p>&nbsp;</p>      <p>&nbsp;</p>      <p>&nbsp;</p>      <p>&nbsp;</p>      <p><font color="#ffffff"><strong>O</strong></font></p>      <p align="center"><strong><font color="#ffffff">I</font></strong></p>      <p align="center"><strong><font color="#ffffff">L</font></strong></p>
    </td>
    <td bgcolor=#474747 colspan=2><font color=#ffffff><b><font color=#ffffff>RO-1
            Petroleum and Mineral Oils</font></b></font></td>
  </tr>
  <tr>
    <td valign="top" bgcolor=#ffffff><br>    
    <img height=51 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG6.jpg" 
      width=170></td>
    <td valign="top" bgcolor=#D7D7D7><div align=center><strong><br>
      for milk, beer, wine, ethyl
          alcohol and other liquid foods </strong></div>
    </td>
    <td valign="top" bgcolor=#ffffff>      <br>      <img height=51 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG7.jpg" 
      width=170><br>
    <br></td>
    <td valign="top" bgcolor=#D7D7D7><div align=center><strong><br>
      for liquids on
          petroleum basis, black</strong></div>
    </td>
  </tr>
  <tr>
    <td rowspan=2 align="center" valign="middle" bgcolor=#6F067B><p align="center"><b><font color=#ffffff>A</font></b></p>      <p align="center"><font color="#ffffff"><b>C</b></font></p>      <p align="center"><font color="#ffffff"><b>I</b></font></p>      <p align="center"><font color="#ffffff"><b>D</b></font></p>
    </td>
    <td colspan="2" valign="top" bgcolor=#53035C><p><b><font color=#ffffff> RK-1
            Acids And Alkali<br>
      </font></b></p>
    </td>
    <td colspan="2" valign="top" bgcolor=#474747><p><font color=#ffffff><b>        RO-2 Bensine, Kerosine, Gas Oil</b></font></p>
    </td>
  </tr>
  <tr>
    <td valign="top"><p><br>
      <img height=51 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG3.jpg" 
      width=170></p>
    </td>
    <td valign="top" bgcolor=#D7D7D7><div align="center"><strong><br>
      for strong and weak acids and alkali solutions</strong></div>
    </td>
    <td valign="top">        <br>      <img height=54 src="/Templates/kauchuk/KAUCHUK_files/r_hoses_files/IMG18.jpg" 
      width=170><br>
        <br>
    </td>
    <td valign="top" bgcolor=#D7D7D7><div align="center"><strong><br>
      for bensine, kerosine and gas oil, with antistatic metal
            wire</strong> </div>
    </td>
  </tr>
  <tr bgcolor="#006633">
    <td height="44" colspan="6" valign="top"><strong><font color="#FFFFFF">APLICATION:
          IN ALL INDUSTRIAL BRANCHES, AGRICULTURE,CONSTRUCTION WORKS, TRANSPORT
      AND DAILY NEEDS.</font></strong></td>
    </tr>
</table>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
