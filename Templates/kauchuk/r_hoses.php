<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0035)http://www.kauchuk.com/r_hoses.html -->
<HTML><HEAD><TITLE>KAUCHUK</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1251">
<META content="MSHTML 5.50.4134.600" name=GENERATOR></HEAD>
<BODY bgColor=#cccfff><TR><TD>
<P align=center><FONT color=#003399></FONT>&nbsp;</P>
<TABLE width="75%" align=center border=1>
  <TBODY>
  <TR>
    <TD width="12%">
      <DIV align=center><B><A 
      href="http://www.kauchuk.com/INDEX.html">Home</A></B></DIV></TD>
    <TD width="22%">
      <DIV align=center><B><A href="http://www.kauchuk.com/tyres.html">Bicycle 
      Tyres</A></B></DIV></TD>
    <TD width="21%">
      <DIV align=center><A href="http://www.kauchuk.com/belts.html"><B>Conveyor 
      Belts</B></A></DIV></TD>
    <TD width="26%">
      <DIV align=center><B><A href="http://www.kauchuk.com/about.html">About 
      Us</A></B></DIV></TD></TR></TBODY></TABLE>
<DIV align=center></DIV></TD><TD></TD><TD></TD></TR>
<P align=center><FONT color=#003399 size=6></FONT>&nbsp;</P>
<P align=center>&nbsp;</P>
<TABLE borderColor=#333399 width="75%" align=center border=3>
  <TBODY>
  <TR borderColor=#333399>
    <TD>
      <DIV align=center><FONT face="Times New Roman, Times, serif" color=#333399 
      size=7>RUBBER HOSES</FONT></DIV></TD></TR></TBODY></TABLE>
<P align=center>&nbsp;</P>
<DIV>
<DIV align=center>
<TABLE width="75%" border=1>
  <TBODY>
  <TR>
    <TD width="15%" bgColor=#3333cc rowSpan=7>
      <H1><FONT color=#000000><B><FONT 
    color=#ffffff>WATER</FONT></B></FONT></H1></TD>
    <TD borderColor=#006633 bgColor=#006633 colSpan=5>
      <DIV align=center><FONT color=#ffffff size=+2><B>RUBBER - TEXTILE SMOOTH 
      HOSES</B></FONT></DIV></TD></TR>
  <TR>
    <TD bgColor=#3030d0 colSpan=2><FONT color=#ffffff><B>RW-1 Water 
      /Schlauch/</B></FONT></TD>
    <TD width="19%" bgColor=#ffff00 rowSpan=2>
      <H1 align=center><B><FONT color=#000000>AIR</FONT></B></H1></TD>
    <TD bgColor=#ffff00 colSpan=2><B>RA-1 (2) Air</B></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><img height=52 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG13.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for watering gardens, black</TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=51 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG3.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for air under pressure, 
  black/coloured</TD></TR>
  <TR>
    <TD bgColor=#3333cc colSpan=2><B><FONT color=#ffffff>RW-2 
    Water</FONT></B></TD>
    <TD width="19%" bgColor=#0099ff rowSpan=6>
      <DIV align=center>
      <H1><FONT color=#000000>GAS WELDING</FONT></H1></DIV></TD>
    <TD bgColor=#0099ff colSpan=2><B>RG-2 (1) Gas Welding and Metal 
    Cutting</B></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=51 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG7.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for water under pressure, black/colored</TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=52 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/I1.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for oxygen - blue colored for acetylene - 
      red, black - with colour strip blue/red</TD></TR>
  <TR>
    <TD bgColor=#3333cc colSpan=2><B><FONT color=#ffffff>RW-3 (1,2) Saturated 
      Steam and Hot Water</FONT></B></TD>
    <TD bgColor=#0099ff colSpan=2><B>RG-2 (2) Gas Welding and Metal Cutting - 
      TWIN</B></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=51 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG3.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <P>for saturated stream with </P>
      <P>t +130 C</P>
      <P>and t +150 C</P>
      <P>for water with </P>
      <P>t to +100C</P></TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center>
      <P>&nbsp;</P>
      <P><IMG height=104 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/twin.jpg" width=170></P></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for oxygen and acetylene- blue/red colored 
      black - with colour strip </TD></TR>
  <TR>
    <TD width="15%" bgColor=#ffff00 rowSpan=2>
      <DIV align=center>
      <H1><B><FONT color=#000000>AIR</FONT></B></H1></DIV></TD>
    <TD bgColor=#ffff00 colSpan=2><B>RA-1 (1) Air</B></TD>
    <TD bgColor=#0099ff colSpan=2><B>RG-2 (3) Gas Welding and Metal 
    Cutting</B></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=51 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG7.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for air under pressure, black/coloured</TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=51 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG7.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for propane- butane - orange coloured, 
      black with colour strip for hydrogen and natural gas - black</TD></TR>
  <TR>
    <TD width="15%" bgColor=#ff0000 rowSpan=2>
      <DIV align=center>
      <H1>FOOD</H1></DIV></TD>
    <TD bgColor=#ff0000 colSpan=2><B>RF-1 Foodstuffs</B></TD>
    <TD width="19%" bgColor=#999999 rowSpan=8>
      <DIV align=center>
      <H1><FONT color=#ffffff>OIL</FONT></H1></DIV>
      <DIV align=center></DIV></TD>
    <TD bgColor=#999999 colSpan=2><B><FONT color=#ffffff>RO-1 Petroleum and 
      Mineral Oils</FONT></B></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=51 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG6.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for milk, beer, wine, ethyl alcohol and 
      other liquid foods</TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=51 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG7.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for liquids on petroleum basis, 
black</TD></TR>
  <TR>
    <TD width="15%" bgColor=#9933cc rowSpan=2>
      <DIV align=center>
      <H1><FONT color=#ffffff>ACID</FONT></H1></DIV></TD>
    <TD bgColor=#9933cc colSpan=2><B><FONT color=#ffffff>RK-1 Acids And 
      Alkali</FONT></B></TD>
    <TD bgColor=#999999 colSpan=2><FONT color=#ffffff><B>RO-2 Bensine, 
      Kerosine, Gas Oil</B></FONT></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=51 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG3.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for strong and weak acids and alkali 
      solutions</TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=54 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG18.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for bemsine, kerosine and gas oil, with 
      antistatic metal wire </TD></TR>
  <TR>
    <TD width="15%" bgColor=#ff9900 rowSpan=4>
      <DIV align=center>
      <H1><FONT color=#000000>PM</FONT></H1></DIV></TD>
    <TD bgColor=#ff9900 colSpan=2><B>PM-1 (1) Powdered and Abrasive 
      Materials</B></TD>
    <TD bgColor=#999999 colSpan=2><B><FONT color=#ffffff>HP-1 High 
      Pressure</FONT></B></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=51 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG3.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for powdered materials and their water 
      suspensions - concrete, cement, sand</TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=53 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG20.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>with one metal braiding - liquids under 
      pressure, for hydrolic systems </TD></TR>
  <TR>
    <TD bgColor=#ff9900 colSpan=2><B>PM-1 (2) Powdered and Abrasive 
      Materials</B></TD>
    <TD bgColor=#999999 colSpan=2><FONT color=#ffffff><B>HP-2 High 
      Pressure</B></FONT></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=53 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG17.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>for powdered materials and their water 
      suspensions - concrete, cement, sand</TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=53 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG19.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>with two metal braidings - for hydrolic 
      systems</TD></TR>
  <TR>
    <TD bgColor=#006633 colSpan=6>
      <DIV align=center><FONT color=#ffffff size=+2><B>RUBBER - TEXTILE SPIRAL 
      HOSES</B></FONT></DIV></TD></TR>
  <TR>
    <TD width="15%" bgColor=#003399 rowSpan=4>
      <DIV align=center></DIV>
      <DIV align=center>
      <H1><FONT color=#000000><B><FONT 
      color=#ffffff>WATER</FONT></B></FONT></H1></DIV></TD>
    <TD bgColor=#003399 colSpan=2><B><FONT color=#ffffff>RW-4 Suction for 
      Water</FONT></B></TD>
    <TD width="19%" bgColor=#ff0000 rowSpan=4>
      <DIV align=center></DIV>
      <DIV align=center>
      <H1>FOOD</H1></DIV></TD>
    <TD bgColor=#ff0000 colSpan=2>
      <DIV align=left><B>RF-2 Suction for Foodstuffs</B></DIV></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=60 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG4.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <DIV align=center>airtight for water suction</DIV></TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=57 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG14.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <DIV align=center>for milk, beer, wine, ethyl alcohol and other liquid 
      foods </DIV>
      <DIV align=center></DIV></TD></TR>
  <TR>
    <TD bgColor=#003399 colSpan=2 height=62><FONT color=#ffffff><B>RW-5 
      Suction-Discherge for Water</B></FONT></TD>
    <TD bgColor=#ff0000 colSpan=2 height=62><B><FONT color=#000000>RF-3 
      Suction-Discharge for Foodstuffs</FONT></B></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff height=62>
      <DIV align=center><IMG height=56 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG11.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc height=62>
      <DIV align=center>suction- discharge of water</DIV></TD>
    <TD width="21%" bgColor=#ffffff height=62>
      <DIV align=center><IMG height=56 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG11.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <DIV align=center>for milk, beer, wine, ethyl alcohol and other liquid 
      foods </DIV>
      <DIV align=center></DIV></TD></TR>
  <TR>
    <TD width="15%" bgColor=#ffff00 rowSpan=2>
      <DIV align=center>
      <H1>AIR</H1></DIV></TD>
    <TD bgColor=#ffff00 colSpan=2><B>RA-2 Foamy-Vortex Dust Catchers</B></TD>
    <TD width="19%" bgColor=#ff9900 rowSpan=2>
      <DIV align=center>
      <H1>PM</H1></DIV></TD>
    <TD bgColor=#ff9900 colSpan=2><B>PM-2 Suction-Discharge for Powdered and 
      Abrasive Materials</B></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=50 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG16.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <DIV align=center>for completing of mashines and equipment</DIV></TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=58 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG10.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <DIV align=center>suction- discharge of powdered materials and their water 
      suspensions concrete, cement, sand</DIV></TD></TR>
  <TR>
    <TD width="15%" bgColor=#9900cc rowSpan=4>
      <DIV align=center></DIV>
      <DIV align=center>
      <H1><FONT color=#ffffff>ACID</FONT></H1></DIV></TD>
    <TD bgColor=#9900cc colSpan=2><FONT color=#ffffff><B>RK-2 Suction for 
      Acids and Alkali</B></FONT></TD>
    <TD width="19%" bgColor=#999999 rowSpan=6>
      <DIV align=center></DIV>
      <DIV align=center></DIV>
      <DIV align=center>
      <H1><FONT color=#ffffff>OIL</FONT></H1></DIV></TD>
    <TD bgColor=#999999 colSpan=2><FONT color=#ffffff><B>RO-4 
      Suction-Discharge for Petroleum and Mineral Oils</B></FONT></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=60 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG4.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <DIV align=center>for strong and weak acids and alcali solutions</DIV>
      <DIV align=center></DIV></TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=58 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG9.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <DIV align=center>suction of liquids on petroleum basis</DIV></TD></TR>
  <TR>
    <TD bgColor=#9900cc colSpan=2><FONT color=#ffffff><B>RK-3 
      Suction-Discharge for Acids and Alkali</B></FONT></TD>
    <TD bgColor=#999999 colSpan=2><FONT color=#ffffff><B>RO-5 Suction for 
      Bensine, Kerosine, Gas Oil</B></FONT></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=58 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG9.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <DIV align=center>for strong and weak acids and alcali solutions</DIV>
      <DIV align=center></DIV></TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=56 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG8.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <DIV align=center>suction of bensine, kerosine and gas oil with antistatic 
      metal wire</DIV></TD></TR>
  <TR>
    <TD width="15%" bgColor=#999999 rowSpan=2>
      <DIV align=center>
      <H1><FONT color=#ffffff>OIL</FONT></H1></DIV></TD>
    <TD bgColor=#999999 colSpan=2><FONT color=#ffffff>RO-3 Suction for 
      Petroleum and Mineral Oils</FONT></TD>
    <TD bgColor=#999999 colSpan=2><FONT color=#ffffff>RO-6 Suction-Discharge 
      for Bensine, Kerosine, Gas Oil</FONT></TD></TR>
  <TR>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=60 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG4.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <DIV align=center>suction of liquids on petroleum basis</DIV></TD>
    <TD width="21%" bgColor=#ffffff>
      <DIV align=center><IMG height=56 src="file://///Mak1/d/Kauchuk/markuchi/r_hoses_files/IMG8.jpg" 
      width=170></DIV></TD>
    <TD width="12%" bgColor=#cccccc>
      <DIV align=center>suction-discharge of bensine, kerosine and gas oil with 
      antistatic metal wire</DIV></TD></TR></TBODY></TABLE>
<P>&nbsp;</P>
<P>&nbsp;</P>
<H1><FONT color=#333399>SMOOTH HOSES</FONT></H1>
<TABLE height=600 cellSpacing=1 cellPadding=7 width=800 align=center border=1>
  <TBODY>
  <TR>
    <TD vAlign=center width="8%" height=21 rowSpan=2>
      <P><FONT size=1><FONT size=2><FONT size=2><B><B><FONT 
      size=2></FONT></B></B></FONT></FONT></FONT></P></TD>
    <TD vAlign=center width="7%" bgColor=#666666 height=21 rowSpan=2>
      <P align=center><B><FONT color=#ffffff size=2>TYPE </FONT></B></P></TD>
    <TD vAlign=center bgColor=#666666 colSpan=3 height=21 rowSpan=2>
      <P align=center><B><FONT color=#ffffff size=2>INSIDE DIAMETER (mm) 
      </FONT></B></P></TD>
    <TD vAlign=center bgColor=#666666 colSpan=3 height=21 rowSpan=2>
      <P align=center><B><FONT color=#ffffff size=2>W.P. (bar) 
</FONT></B></P></TD>
    <TD vAlign=center width="9%" bgColor=#666666 height=21 rowSpan=2>
      <P align=center><B><FONT color=#ffffff size=2>W.T.</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=2>(<FONT face=Symbol>�</FONT> 
      C) </FONT></B></P></TD>
    <TD vAlign=center bgColor=#666666 colSpan=3 height=21 rowSpan=2>
      <P align=center><B><FONT color=#ffffff size=2>B.P. (bar) 
</FONT></B></P></TD>
    <TD vAlign=center bgColor=#666666 colSpan=2 height=21>
      <P align=center><B><FONT color=#ffffff size=2>LENGTH </FONT></B></P></TD></TR>
  <TR>
    <TD vAlign=top width="7%" bgColor=#666666 height=16>
      <P align=center><FONT color=#ffffff size=1><B>flex.</B> </FONT></P></TD>
    <TD vAlign=top width="7%" bgColor=#666666 height=16>
      <P align=center><FONT color=#ffffff size=1><B>mand.</B> </FONT></P></TD></TR>
  <TR>
    <TD vAlign=center width="8%" bgColor=#333399 height=80>
      <P align=center><FONT size=2><B><FONT color=#ffffff>WATER</FONT> 
      </B></FONT></P></TD>
    <TD vAlign=center width="7%" bgColor=#333399 height=80>
      <P><B><FONT color=#ffffff size=1>RW-1</FONT></B></P>
      <P><B><FONT color=#ffffff size=1>RW-2</FONT></B></P>
      <P><B><FONT color=#ffffff size=1>RW-31</FONT></B></P>
      <P><B><FONT color=#ffffff size=1>RW-32 </FONT></B></P></TD>
    <TD vAlign=center bgColor=#333399 colSpan=3 height=80>
      <P><B><FONT color=#ffffff size=1>8, 10, 13, 16, 19, 25, 32, 38, 
      50</FONT></B></P>
      <P><B><FONT color=#ffffff size=1>6, 8, 10, 13, 16, 19, 25, 32, 38, 45, 50, 
      54, 60, 70, 83, 90, 100, 115, 142, 150, 160, 200</FONT></B></P>
      <P><B><FONT color=#ffffff size=1>13, 16, 19, 25, 32, 38, 50</FONT></B></P>
      <P><B><FONT color=#ffffff size=1>16, 19, 25, 32, 38, 50 </FONT></B></P></TD>
    <TD vAlign=center width="5%" bgColor=#333399 height=80>
      <P align=center><B><FONT color=#ffffff size=1>1,6</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>-</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>3</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>10 </FONT></B></P></TD>
    <TD vAlign=center width="5%" bgColor=#333399 height=80>
      <P align=center><B><FONT color=#ffffff size=1>-</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>6,3</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>5</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="4%" bgColor=#333399 height=80>
      <P align=center><B><FONT color=#ffffff size=1>-</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>-</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>-</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="9%" bgColor=#333399 height=80>
      <P align=center><B><FONT color=#ffffff size=1>-20/+50</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>-20+50</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>+130/150</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>to +100 </FONT></B></P></TD>
    <TD vAlign=center width="6%" bgColor=#333399 height=80>
      <P align=center><B><FONT color=#ffffff size=1>-</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>-</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>15</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>50 </FONT></B></P></TD>
    <TD vAlign=center width="6%" bgColor=#333399 height=80>
      <P align=center><B><FONT color=#ffffff size=1>-</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>12,6</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>50</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="4%" bgColor=#333399 height=80>
      <P align=center><B><FONT color=#ffffff size=1>-</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>-</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>-</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="7%" bgColor=#333399 height=80>
      <P><B><FONT color=#ffffff size=1>to 50</FONT></B></P>
      <P><B><FONT color=#ffffff size=1>to 50</FONT></B></P>
      <P><B><FONT color=#ffffff size=1>to 50</FONT></B></P>
      <P><B><FONT color=#ffffff size=1>to 50 </FONT></B></P></TD>
    <TD vAlign=center width="7%" bgColor=#333399 height=80>
      <P align=center><B><FONT color=#ffffff size=1>20, 40</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>20, 40</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>20, 40</FONT></B></P>
      <P align=center><B><FONT color=#ffffff size=1>20, 40 </FONT></B></P></TD></TR>
  <TR>
    <TD vAlign=center width="8%" bgColor=#ffff33 height=19>
      <P align=center><FONT size=2><B>AIR </B></FONT></P></TD>
    <TD vAlign=center width="7%" bgColor=#ffff33 height=19>
      <P><B><FONT size=1>RA-1,2 </FONT></B></P></TD>
    <TD vAlign=center bgColor=#ffff33 colSpan=3 height=19>
      <P><B><FONT size=1>6, 8, 10, 13, 16, 19, 25, 32, 38, 50 </FONT></B></P></TD>
    <TD vAlign=center width="5%" bgColor=#ffff33 height=19>
      <P align=center><B><FONT size=1>10 </FONT></B></P></TD>
    <TD vAlign=center width="5%" bgColor=#ffff33 height=19>
      <P align=center><B><FONT size=1>20 </FONT></B></P></TD>
    <TD vAlign=center width="4%" bgColor=#ffff33 height=19>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="9%" bgColor=#ffff33 height=19>
      <P align=center><B><FONT size=1>-20/+50 </FONT></B></P></TD>
    <TD vAlign=center width="6%" bgColor=#ffff33 height=19>
      <P align=center><B><FONT size=1>40 </FONT></B></P></TD>
    <TD vAlign=center width="6%" bgColor=#ffff33 height=19>
      <P align=center><B><FONT size=1>60 </FONT></B></P></TD>
    <TD vAlign=center width="4%" bgColor=#ffff33 height=19>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="7%" bgColor=#ffff33 height=19>
      <P align=center><B><FONT size=1>to 50 </FONT></B></P></TD>
    <TD vAlign=center width="7%" bgColor=#ffff33 height=19>
      <P align=center><B><FONT size=1>20, 40 </FONT></B></P></TD></TR>
  <TR>
    <TD vAlign=center width="8%" bgColor=#6699ff height=54>
      <P align=center><FONT size=2><B>GAS </B></FONT></P></TD>
    <TD vAlign=center width="7%" bgColor=#6699ff height=54>
      <P><B><FONT size=1>RG-1</FONT></B></P>
      <P><B><FONT size=1>RG-2</FONT></B></P>
      <P><B><FONT size=1>RG-3 </FONT></B></P></TD>
    <TD vAlign=center bgColor=#6699ff colSpan=3 height=54>
      <P><B><FONT size=1>6, 8, 10, 13, 16</FONT></B></P>
      <P><B><FONT size=1>6, 8, 10, 13, 16</FONT></B></P>
      <P><B><FONT size=1>5, 6, 8, 10, 13 </FONT></B></P></TD>
    <TD vAlign=center width="5%" bgColor=#6699ff height=54>
      <P align=center><B><FONT size=1>10</FONT></B></P>
      <P align=center><B><FONT size=1>10</FONT></B></P>
      <P align=center><B><FONT size=1>6,3 </FONT></B></P></TD>
    <TD vAlign=center width="5%" bgColor=#6699ff height=54>
      <P align=center><B><FONT size=1>20</FONT></B></P>
      <P align=center><B><FONT size=1>20</FONT></B></P>
      <P align=center><B><FONT size=1>10 </FONT></B></P></TD>
    <TD vAlign=center width="4%" bgColor=#6699ff height=54>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="9%" bgColor=#6699ff height=54>
      <P align=center><B><FONT size=1>-20/+50</FONT></B></P>
      <P align=center><B><FONT size=1>-20/+50</FONT></B></P>
      <P align=center><B><FONT size=1>-20/+50 </FONT></B></P></TD>
    <TD vAlign=center width="6%" bgColor=#6699ff height=54>
      <P align=center><B><FONT size=1>30</FONT></B></P>
      <P align=center><B><FONT size=1>30</FONT></B></P>
      <P align=center><B><FONT size=1>18,9 </FONT></B></P></TD>
    <TD vAlign=center width="6%" bgColor=#6699ff height=54>
      <P align=center><B><FONT size=1>60</FONT></B></P>
      <P align=center><B><FONT size=1>60</FONT></B></P>
      <P align=center><B><FONT size=1>30 </FONT></B></P></TD>
    <TD vAlign=center width="4%" bgColor=#6699ff height=54>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="7%" bgColor=#6699ff height=54>
      <P><B><FONT size=1>to 50</FONT></B></P>
      <P align=center><B><FONT size=1>to 50</FONT></B></P>
      <P align=center><B><FONT size=1>to 50 </FONT></B></P></TD>
    <TD vAlign=center width="7%" bgColor=#6699ff height=54>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD></TR>
  <TR>
    <TD vAlign=center width="8%" bgColor=#ff6666 height=65>
      <P align=center><FONT size=2><B>FOOD </B></FONT></P></TD>
    <TD vAlign=center width="7%" bgColor=#ff6666 height=65>
      <P><B><FONT size=1>RF-1 </FONT></B></P></TD>
    <TD vAlign=center bgColor=#ff6666 colSpan=3 height=65>
      <P><B><FONT size=1>13, 16, 19, 25, 32, 38, 50, 60, 70, 83, 90, 100, 125 
      </FONT></B></P></TD>
    <TD vAlign=center width="5%" bgColor=#ff6666 height=65>
      <P align=center><B><FONT size=1>6,3 </FONT></B></P></TD>
    <TD vAlign=center width="5%" bgColor=#ff6666 height=65>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="4%" bgColor=#ff6666 height=65>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="9%" bgColor=#ff6666 height=65>
      <P align=center><B><FONT size=1>-20/+50 </FONT></B></P></TD>
    <TD vAlign=center width="6%" bgColor=#ff6666 height=65>
      <P align=center><B><FONT size=1>18,9 </FONT></B></P></TD>
    <TD vAlign=center width="6%" bgColor=#ff6666 height=65>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="4%" bgColor=#ff6666 height=65>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="7%" bgColor=#ff6666 height=65>
      <P align=center><B><FONT size=1>to 50 </FONT></B></P></TD>
    <TD vAlign=center width="7%" bgColor=#ff6666 height=65>
      <P align=center><B><FONT size=1>20, 40 </FONT></B></P></TD></TR>
  <TR bgColor=#cc66cc>
    <TD vAlign=center width="8%" height=56>
      <P align=center><FONT size=2><B>ACID </B></FONT></P></TD>
    <TD vAlign=center width="7%" height=56>
      <P><B><FONT size=1>RK-1 </FONT></B></P></TD>
    <TD vAlign=center colSpan=3 height=56>
      <P><B><FONT size=1>19, 25, 32, 38, 50, 60, 70, 75, 83, 90, 100, 110, 125, 
      135, 150, 200 </FONT></B></P></TD>
    <TD vAlign=center width="5%" height=56>
      <P align=center><B><FONT size=1>6,3 </FONT></B></P></TD>
    <TD vAlign=center width="5%" height=56>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="4%" height=56>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="9%" height=56>
      <P align=center><B><FONT size=1>-20/+50 </FONT></B></P></TD>
    <TD vAlign=center width="6%" height=56>
      <P align=center><B><FONT size=1>18,9 </FONT></B></P></TD>
    <TD vAlign=center width="6%" height=56>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="4%" height=56>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="7%" height=56>
      <P align=center><B><FONT size=1>to 50 </FONT></B></P></TD>
    <TD vAlign=center width="7%" height=56>
      <P align=center><B><FONT size=1>20, 40 </FONT></B></P></TD></TR>
  <TR bgColor=#ff9900>
    <TD vAlign=center width="8%" height=45>
      <P align=center><FONT size=2><B>PM </B></FONT></P></TD>
    <TD vAlign=center width="7%" height=45>
      <P><B><FONT size=1>PM-1 (1)</FONT></B></P>
      <P><B><FONT size=1>PM-1 (2) </FONT></B></P></TD>
    <TD vAlign=center colSpan=3 height=45>
      <P><B><FONT size=1>19, 25, 32, 38, 50, 60, 70, 75, 83, 90, 100, 100, 125, 
      150, 200</FONT></B></P>
      <P><B><FONT size=1>19, 25, 32, 38, 50, 60, 70, 75, 83, 90, 100, 100, 125, 
      150, 200 </FONT></B></P></TD>
    <TD vAlign=center width="5%" height=45>
      <P align=center><B><FONT size=1>6,3</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="5%" height=45>
      <P align=center><B><FONT size=1>10</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="4%" height=45>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>16 </FONT></B></P></TD>
    <TD vAlign=center width="9%" height=45>
      <P align=center><B><FONT size=1>-20/+50</FONT></B></P>
      <P align=center><B><FONT size=1>-20/+50 </FONT></B></P></TD>
    <TD vAlign=center width="6%" height=45>
      <P align=center><B><FONT size=1>18,9</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="6%" height=45>
      <P align=center><B><FONT size=1>30</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="4%" height=45>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>48 </FONT></B></P></TD>
    <TD vAlign=center width="7%" height=45>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="7%" height=45>
      <P align=center><B><FONT size=1>20, 40</FONT></B></P>
      <P align=center><B><FONT size=1>20, 40 </FONT></B></P></TD></TR>
  <TR bgColor=#cccccc>
    <TD vAlign=center height=55 rowSpan=4>
      <P>&nbsp;</P>
      <P align=center><FONT size=2><B>OILS </B></FONT></P></TD>
    <TD vAlign=center width="7%" height=37>
      <P><B><FONT size=1>RO-1</FONT></B></P>
      <P><B><FONT size=1>RO-2 </FONT></B></P></TD>
    <TD vAlign=center colSpan=3 height=37>
      <P><B><FONT size=1>6, 8, 10, 13, 16, 19, 25, 32, 38, 50, 60, 70, 83, 90, 
      100, 125, 150, 200</FONT></B></P>
      <P><B><FONT size=1>19, 25, 32, 38, 50, 54, 60, 70, 75, 83, 90, 100, 110, 
      125, 150, 200 </FONT></B></P></TD>
    <TD vAlign=center width="5%" height=37>
      <P align=center><B><FONT size=1>5</FONT></B></P>
      <P align=center><B><FONT size=1>5 </FONT></B></P></TD>
    <TD vAlign=center width="5%" height=37>
      <P align=center><B><FONT size=1>6,3</FONT></B></P>
      <P align=center><B><FONT size=1>6,3 </FONT></B></P></TD>
    <TD vAlign=center width="4%" height=37>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="9%" height=37>
      <P align=center><B><FONT size=1>-25/+60</FONT></B></P>
      <P align=center><B><FONT size=1>-25/+60 </FONT></B></P></TD>
    <TD vAlign=center width="6%" height=37>
      <P align=center><B><FONT size=1>15</FONT></B></P>
      <P align=center><B><FONT size=1>15 </FONT></B></P></TD>
    <TD vAlign=center width="6%" height=37>
      <P align=center><B><FONT size=1>18,9</FONT></B></P>
      <P align=center><B><FONT size=1>18,9 </FONT></B></P></TD>
    <TD vAlign=center width="4%" height=37>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=center width="7%" height=37>
      <P align=center><B><FONT size=1>to 50</FONT></B></P>
      <P align=center><B><FONT size=1>to 50 </FONT></B></P></TD>
    <TD vAlign=top width="7%" height=37>
      <P align=center><B><FONT size=1>20, 40</FONT></B></P>
      <P align=center><B><FONT size=1>20, 40 </FONT></B></P></TD></TR>
  <TR>
    <TD vAlign=top width="7%" bgColor=#cccccc height=18 rowSpan=2>
      <P><B><FONT size=1></FONT></B>&nbsp;</P>
      <P><B><FONT size=1>HP-1 </FONT></B></P></TD>
    <TD vAlign=top width="5%" bgColor=#cccccc height=18>
      <P><B><FONT size=1>I.D. </FONT></B></P></TD>
    <TD vAlign=top width="6%" bgColor=#cccccc height=18>
      <P><B><FONT size=1>O.D. mm </FONT></B></P></TD>
    <TD vAlign=top width="21%" bgColor=#cccccc height=18 rowSpan=3>
      <P align=center><B><FONT size=1>-</FONT></B></P></TD>
    <TD vAlign=bottom width="5%" bgColor=#cccccc height=18 rowSpan=2>
      <P align=center><B><FONT size=1>225</FONT></B></P>
      <P align=center><B><FONT size=1>215</FONT></B></P>
      <P align=center><B><FONT size=1>180</FONT></B></P>
      <P align=center><B><FONT size=1>160</FONT></B></P>
      <P align=center><B><FONT size=1>130</FONT></B></P>
      <P align=center><B><FONT size=1>105 </FONT></B></P></TD>
    <TD vAlign=bottom width="5%" bgColor=#cccccc height=18 rowSpan=2>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=bottom width="4%" bgColor=#cccccc height=18 rowSpan=2>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=bottom width="9%" bgColor=#cccccc height=18 rowSpan=2>
      <P align=center><B><FONT size=1>-25/+50</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=bottom width="6%" bgColor=#cccccc height=18 rowSpan=2>
      <P align=center><B><FONT size=1>900</FONT></B></P>
      <P align=center><B><FONT size=1>850</FONT></B></P>
      <P align=center><B><FONT size=1>720</FONT></B></P>
      <P align=center><B><FONT size=1>640</FONT></B></P>
      <P align=center><B><FONT size=1>520</FONT></B></P>
      <P align=center><B><FONT size=1>420 </FONT></B></P></TD>
    <TD vAlign=bottom width="6%" bgColor=#cccccc height=18 rowSpan=2>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=bottom width="4%" bgColor=#cccccc height=18 rowSpan=2>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=bottom width="7%" bgColor=#cccccc height=18 rowSpan=2>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=top width="7%" bgColor=#cccccc height=18 rowSpan=2>
      <H1>&nbsp;</H1>
      <H1><B><FONT size=1>to 10 </FONT></B></H1></TD></TR>
  <TR>
    <TD vAlign=top width="5%" bgColor=#cccccc height=93>
      <P><B><FONT size=1>6,3</FONT></B></P>
      <P><B><FONT size=1>8</FONT></B></P>
      <P><B><FONT size=1>10</FONT></B></P>
      <P><B><FONT size=1>12,5</FONT></B></P>
      <P><B><FONT size=1>16</FONT></B></P>
      <P><B><FONT size=1>19 </FONT></B></P></TD>
    <TD vAlign=top width="6%" bgColor=#cccccc height=93>
      <P><B><FONT size=1>15</FONT></B></P>
      <P><B><FONT size=1>17</FONT></B></P>
      <P><B><FONT size=1>19,4</FONT></B></P>
      <P><B><FONT size=1>23</FONT></B></P>
      <P><B><FONT size=1>29</FONT></B></P>
      <P><B><FONT size=1>34 </FONT></B></P></TD></TR>
  <TR>
    <TD vAlign=top width="7%" bgColor=#cccccc height=58>
      <P><B><FONT size=1>HP-2 </FONT></B></P></TD>
    <TD vAlign=top width="5%" bgColor=#cccccc height=58>
      <P><B><FONT size=1>6,3</FONT></B></P>
      <P><B><FONT size=1>8</FONT></B></P>
      <P><B><FONT size=1>10</FONT></B></P>
      <P><B><FONT size=1>12,5</FONT></B></P>
      <P><B><FONT size=1>16 </FONT></B></P></TD>
    <TD vAlign=top width="6%" bgColor=#cccccc height=58>
      <P><B><FONT size=1>17</FONT></B></P>
      <P><B><FONT size=1>19,4</FONT></B></P>
      <P><B><FONT size=1>21,4</FONT></B></P>
      <P><B><FONT size=1>25</FONT></B></P>
      <P><B><FONT size=1>28 </FONT></B></P></TD>
    <TD vAlign=top width="5%" bgColor=#cccccc height=58>
      <P align=center><B><FONT size=1>400</FONT></B></P>
      <P align=center><B><FONT size=1>350</FONT></B></P>
      <P align=center><B><FONT size=1>310</FONT></B></P>
      <P align=center><B><FONT size=1>275</FONT></B></P>
      <P align=center><B><FONT size=1>250 </FONT></B></P></TD>
    <TD vAlign=top width="5%" bgColor=#cccccc height=58>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=top width="4%" bgColor=#cccccc height=58>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=top width="9%" bgColor=#cccccc height=58>
      <P align=center><B><FONT size=1>-25/+50</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>-</FONT></B></P>
      <P align=center><B><FONT size=1>- </FONT></B></P></TD>
    <TD vAlign=top width="6%" bgColor=#cccccc height=58>
      <P align=center><B><FONT size=1>1600</FONT></B></P>
      <P align=center><B><FONT size=1>1400</FONT></B></P>
      <P align=center><B><FONT size=1>1320</FONT></B></P>
      <P align=center><B><FONT size=1>1100</FONT></B></P>
      <P align=center><B><FONT size=1>1000 </FONT></B></P></TD>
    <TD vAlign=top width="6%" bgColor=#cccccc height=58>
      <P align=center><B><FONT size=1>-</FONT></B></P></TD>
    <TD vAlign=top width="4%" bgColor=#cccccc height=58>
      <P align=center><B><FONT size=1>-</FONT></B></P></TD>
    <TD vAlign=top width="7%" bgColor=#cccccc height=58>
      <P align=center><B><FONT size=1>-</FONT></B></P></TD>
    <TD vAlign=top width="7%" bgColor=#cccccc height=58>
      <P align=center><B><FONT size=1>to 10 </FONT></B></P></TD></TR></TBODY></TABLE>
<P>&nbsp;</P>
<TABLE width="75%" border=0>
  <TBODY>
  <TR bgColor=#006633>
    <TD colSpan=2><B></B>
      <DIV align=right><FONT color=#ffffff><B>MANUFACTURING LENGTHS</B> 
      </FONT></DIV>
      <DIV align=right><FONT color=#ffffff><B>I.D. 6 mm � 19 mm are manufactured 
      in coils � to 100 l. m</B></FONT></DIV>
      <DIV align=right><FONT color=#ffffff><B>I.D. 19 mm � 50 mm are 
      manufactured in length - to 40 l. m</B></FONT></DIV><B></B>
      <DIV align=right><FONT color=#ffffff><B>I.D. 50 mm � 200 mm are 
      manufactured in length up to 8 l. m</B></FONT></DIV></TD></TR></TBODY></TABLE>
<P>&nbsp;</P>
<P>&nbsp;</P>
<H1><FONT color=#333399>SPIRAL HOSES</FONT></H1>
<TABLE cellSpacing=1 cellPadding=7 width=800 border=1>
  <TBODY>
  <TR>
    <TD vAlign=top width="10%" height=42>
      <P></P></TD>
    <TD vAlign=center width="8%" bgColor=#666666 height=42><B>
      <P align=center><FONT color=#ffffff>TYPE</FONT> </P></B></TD>
    <TD vAlign=center width="30%" bgColor=#666666 height=42><B>
      <P align=center><FONT color=#ffffff>INSIDE DIAMETER (mm)</FONT> 
</P></B></TD>
    <TD vAlign=center bgColor=#666666 colSpan=2 height=42><B>
      <P align=center><FONT color=#ffffff>W.P. (bar)</FONT> </P></B></TD>
    <TD vAlign=top width="8%" bgColor=#666666 height=42><B>
      <P align=center><FONT color=#ffffff>W.T.</FONT></P>
      <P align=center><FONT color=#ffffff>(<FONT face=Symbol>�</FONT> C)</FONT> 
      </P></B></TD>
    <TD vAlign=center bgColor=#666666 colSpan=2 height=42><B>
      <P align=center><FONT color=#ffffff>B.P. (bar)</FONT> </P></B></TD>
    <TD vAlign=center width="18%" bgColor=#666666 height=42>
      <P align=center><FONT size=1><B><FONT color=#ffffff size=2>VACUUM 
      (bar)</FONT></B> </FONT></P></TD></TR>
  <TR bgColor=#333399>
    <TD vAlign=center width="10%" height=80><B>
      <P align=center><FONT color=#ffffff>WATER</FONT> </P></B></TD>
    <TD vAlign=center width="8%" height=80><B><FONT size=2>
      <P><FONT color=#ffffff>RW-4</FONT></P>
      <P><FONT color=#ffffff>RW-5</FONT> </P></FONT></B></TD>
    <TD vAlign=top width="30%" height=80><B><FONT size=2>
      <P><FONT color=#ffffff>16, 20, 25, 32, 38, 44, 50, 54, 60, 70, 75, 83, 90, 
      102, 110, 115, 125, 142, 150, 160, 200</FONT></P>
      <P><FONT color=#ffffff>50, 54, 60, 70, 83, 90, 100, 110, 125, 142, 150, 
      160, 200</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=80><B><FONT size=2>
      <P align=center><FONT color=#ffffff>-</FONT></P>
      <P align=center></P>
      <P align=center><FONT color=#ffffff>6,3</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=80><B><FONT size=2>
      <P align=center><FONT color=#ffffff>-</FONT></P>
      <P align=center></P>
      <P align=center><FONT color=#ffffff>-</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=80><B><FONT size=2>
      <P align=center><FONT color=#ffffff>-20/+50</FONT></P>
      <P align=center></P>
      <P align=center><FONT color=#ffffff>-20/+50</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=80><B><FONT size=2>
      <P align=center><FONT color=#ffffff>-</FONT></P>
      <P align=center></P>
      <P align=center><FONT color=#ffffff>12,6</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=80><B><FONT size=2>
      <P align=center><FONT color=#ffffff>-</FONT></P>
      <P align=center></P>
      <P align=center><FONT color=#ffffff>-</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="18%" height=80>
      <P align=center><FONT size=2><B><FONT 
      color=#ffffff>0.8</FONT></B></FONT></P>
      <P align=center><B><FONT size=2>
      <P align=center></P></FONT></B>
      <P align=center><FONT color=#ffffff size=2><B>0.8</B> </FONT></P></TD></TR>
  <TR bgColor=#ffff33>
    <TD vAlign=center width="10%" height=47><B>
      <P align=center>AIR </P></B></TD>
    <TD vAlign=center width="8%" height=47><B><FONT size=2>
      <P>RA-4 </P></FONT></B></TD>
    <TD vAlign=center width="30%" height=47><B><FONT size=2>
      <P>25, 38, 50, 54, 60, 70, 75, 83, 90, 100, 125, 142, 150, 160, 200 
      </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=47><B><FONT size=2>
      <P align=center>1 </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=47><B><FONT size=2>
      <P align=center>- </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=47><B><FONT size=2>
      <P align=center>-20/+50 </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=47><B><FONT size=2>
      <P align=center>- </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=47><B><FONT size=2>
      <P align=center>- </P></FONT></B></TD>
    <TD vAlign=center width="18%" height=47>
      <P align=center><FONT size=2><B>0.8</B> </FONT></P></TD></TR>
  <TR bgColor=#ff0033>
    <TD vAlign=center width="10%" height=64><B>
      <P align=center>FOOD </P></B></TD>
    <TD vAlign=center width="8%" height=64><B><FONT size=2>
      <P>RF-2</P>
      <P>RF-3 </P></FONT></B></TD>
    <TD vAlign=center width="30%" height=64><B><FONT size=2>
      <P>32, 38, 50, 54, 60, 70, 83, 90, 100, 110, 125</P>
      <P>20. 25, 30, 32, 38, 50, 54, 60, 70, 75, 83, 90, 100, 110, 125, 150, 200 
      </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=64><B><FONT size=2>
      <P align=center>-</P>
      <P align=center></P>
      <P align=center>6,3 </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=64><B><FONT size=2>
      <P align=center>-</P>
      <P align=center></P>
      <P align=center>- </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=64><B><FONT size=2>
      <P align=center>-20/+50</P>
      <P align=center></P>
      <P align=center>-20/+50 </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=64><B><FONT size=2>
      <P align=center>-</P>
      <P align=center></P>
      <P align=center>12,6 </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=64><B><FONT size=2>
      <P align=center>-</P>
      <P align=center></P>
      <P align=center>- </P></FONT></B></TD>
    <TD vAlign=center width="18%" height=64>
      <P align=center><B><FONT size=2>
      <P align=center></P></FONT></B>
      <P align=center><FONT size=2><B>0.8</B></FONT></P>
      <P align=center><B><FONT size=2>
      <P align=center></P></FONT></B>
      <P align=center><FONT size=2><B>0.8</B></FONT></P>
      <P align=center><B><FONT size=2>
      <P align=center></P></FONT></B></TD></TR>
  <TR bgColor=#cc66cc>
    <TD vAlign=center width="10%" height=56><B>
      <P align=center><FONT color=#ffffff>ACID</FONT> </P></B></TD>
    <TD vAlign=center width="8%" height=56><B><FONT size=2>
      <P><FONT color=#ffffff>RK-2</FONT></P>
      <P><FONT color=#ffffff>RK-3</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="30%" height=56><B><FONT size=2>
      <P><FONT color=#ffffff>50, 54, 60, 70, 75, 83, 90, 100, 110, 125, 150, 
      200</FONT></P>
      <P><FONT color=#ffffff>25, 32, 38, 50, 54, 63, 70, 75, 83, 90, 100, 125, 
      150, 200</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=56><B><FONT size=2>
      <P align=center><FONT color=#ffffff>-</FONT></P>
      <P align=center></P>
      <P align=center><FONT color=#ffffff>6,3</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=56><B><FONT size=2>
      <P align=center><FONT color=#ffffff>-</FONT></P>
      <P align=center></P>
      <P align=center><FONT color=#ffffff>-</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=56><B><FONT size=2>
      <P align=center><FONT color=#ffffff>-20/+50</FONT></P>
      <P align=center></P>
      <P align=center><FONT color=#ffffff>-20/+50</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=56><B><FONT size=2>
      <P align=center><FONT color=#ffffff>-</FONT></P>
      <P align=center></P>
      <P align=center><FONT color=#ffffff>18,9</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=56><B><FONT size=2>
      <P align=center><FONT color=#ffffff>-</FONT></P>
      <P align=center></P>
      <P align=center><FONT color=#ffffff>-</FONT> </P></FONT></B></TD>
    <TD vAlign=center width="18%" height=56>
      <P align=center>
      <P align=center>
      <P align=center><B><FONT size=2>
      <P align=center></P></FONT></B>
      <P align=center><FONT size=2><B><FONT 
      color=#ffffff>0.8</FONT></B></FONT></P>
      <P align=center><B><FONT size=2>
      <P align=center></P></FONT></B>
      <P align=center><FONT color=#ffffff size=2><B>0.8</B></FONT></P></TD></TR>
  <TR bgColor=#ff9900>
    <TD vAlign=center width="10%" height=45><B>
      <P align=center>PM </P></B></TD>
    <TD vAlign=center width="8%" height=45><B><FONT size=2>
      <P>PM-2 </P></FONT></B></TD>
    <TD vAlign=center width="30%" height=45><B><FONT size=2>
      <P>50, 54, 60, 70, 75, 83, 90, 100, 110, 125, 142, 150, 160, 200 
      </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=45><B><FONT size=2>
      <P align=center>6,3 </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=45><B><FONT size=2>
      <P align=center>- </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=45><B><FONT size=2>
      <P align=center>-20/+50 </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=45><B><FONT size=2>
      <P align=center>18,9 </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=45><B><FONT size=2>
      <P align=center>- </P></FONT></B></TD>
    <TD vAlign=center width="18%" height=45>
      <P align=center><FONT size=2><B>0.8</B> </FONT></P></TD></TR>
  <TR bgColor=#cccccc>
    <TD vAlign=center width="10%" height=70><B>
      <P align=center>OILS </P></B></TD>
    <TD vAlign=center width="8%" height=70><B><FONT size=2>
      <P>RO-3</P>
      <P>RO-4</P>
      <P>RO-5</P>
      <P>RO-6 </P></FONT></B></TD>
    <TD vAlign=center width="30%" height=70><B><FONT size=2>
      <P>50, 54, 60, 70, 75, 83, 90, 100, 110, 125, 142, 150, 160, 200</P>
      <P>20, 25, 32, 38, 50, 54, 60, 70, 75, 83, 90, 100, 110, 125, 150, 200</P>
      <P>50, 54, 60, 70, 75, 83, 90, 100, 110, 125, 150, 200</P>
      <P>20, 25, 32, 38, 50, 60, 70, 75, 83, 90, 100, 110, 125, 150, 200 
      </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=70><B><FONT size=2>
      <P align=center>-</P>
      <P align=center>5</P>
      <P align=center>-</P>
      <P align=center>5 </P></FONT></B></TD>
    <TD vAlign=center width="5%" height=70><B><FONT size=2>
      <P align=center>-</P>
      <P align=center>6,3</P>
      <P align=center>-</P>
      <P align=center>6,3 </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=70><B><FONT size=2>
      <P align=center>-25/+60</P>
      <P align=center>-25/+60</P>
      <P align=center>-25/+60</P>
      <P align=center>-25/+60 </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=70><B><FONT size=2>
      <P align=center>-</P>
      <P align=center>15</P>
      <P align=center>-</P>
      <P align=center>15 </P></FONT></B></TD>
    <TD vAlign=center width="8%" height=70><B><FONT size=2>
      <P align=center>-</P>
      <P align=center>18,9</P>
      <P align=center>-</P>
      <P align=center>18,9 </P></FONT></B></TD>
    <TD vAlign=center width="18%" height=70>
      <P align=center><FONT size=2><B>0.8</B></FONT></P>
      <P align=center><B><FONT size=2>
      <P align=center></P></FONT></B>
      <P align=center><FONT size=2><B>0.8 </B></FONT>
      <P align=center><FONT size=2><B>0.8</B> </FONT>
      <P align=center><FONT size=2><B>0.8</B></FONT> </P></TD></TR></TBODY></TABLE>
<P>&nbsp;</P>
<P>&nbsp;</P>
<TABLE width="75%" align=center border=1>
  <TBODY>
  <TR>
    <TD width="12%">
      <DIV align=center><B><A 
      href="http://www.kauchuk.com/INDEX.html">Home</A></B></DIV></TD>
    <TD width="22%">
      <DIV align=center><B><A href="http://www.kauchuk.com/tyres.html">Bicycle 
      Tyres</A></B></DIV></TD>
    <TD width="21%">
      <DIV align=center><A href="http://www.kauchuk.com/belts.html"><B>Conveyor 
      Belts</B></A></DIV></TD>
    <TD width="26%">
      <DIV align=center><B><A href="http://www.kauchuk.com/about.html">About 
      Us</A></B></DIV></TD></TR></TBODY></TABLE>
<P>&nbsp;</P>
<P>&nbsp;</P></DIV></DIV></BODY></HTML>
