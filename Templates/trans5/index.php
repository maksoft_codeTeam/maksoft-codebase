<!DOCTYPE html>
<html lang="<?php echo($Site->language_key); ?>"><head>
        <title><?=$Title?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php
		//include template configurations
		include("Templates/trans5/configure.php");
        $o_site->print_sConfigurations();
    	//include meta tags
		include("Templates/meta_tags.php");
	    ?>

        <!-- Stylesheets -->
        <link rel="stylesheet" href="<?=TEMPLATE_DIR?>assets/css/bootstrap.css"/><!-- bootstrap grid -->
        <link rel="stylesheet" href="<?=TEMPLATE_DIR?>assets/css/animate.css"/><!-- used for animations -->
        <link rel="stylesheet" href="<?=TEMPLATE_DIR?>assets/masterslider/style/masterslider.css" /><!-- Master slider css -->
        <link rel="stylesheet" href="<?=TEMPLATE_DIR?>assets/masterslider/skins/default/style.css" /><!-- Master slider default skin -->
        <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/owl-carousel/owl.carousel.css'/><!-- Client carousel -->
        <link rel="stylesheet" href="<?=TEMPLATE_DIR?>assets/css/style.css"/><!-- template styles -->
        <link rel="stylesheet" href="<?=TEMPLATE_DIR?>assets/css/color-orange.css"/><!-- template main color -->
        <link rel="stylesheet" href="<?=TEMPLATE_DIR?>assets/css/retina.css"/><!-- retina ready styles -->
        <link rel="stylesheet" href="<?=TEMPLATE_DIR?>assets/css/responsive.css"/><!-- responsive styles -->

        <!-- Google Web fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,800,700,600' rel='stylesheet' type='text/css'>

        <!-- Font icons -->

    <?php
        	//get site news
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 5");
	
	//get most viewed pages
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5", "p.n != '".$o_site->_site['StartPage']."'");

	//get last added pages
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5", "p.preview>30");
	
	if(defined('GOOGLE_TRACKING_CODE'))   { include_once("web/admin/modules/google/analitycs.php"); }
	 include_once("web/admin/modules/counter/tyxo.php"); 
	if( (defined('GOOGLE_SITE_VERIF')) && ($o_page->n == $o_page->_site['StartPage']) )   { include_once("web/admin/modules/google/wmt.php"); }
	?>
    </head>

    <body>
<?php
    include_once("web/admin/modules/google/analitycs.php");
    include_once("web/admin/modules/social/fb_app_init.php");
	
	include TEMPLATE_DIR . "header.php";
   
    if(isset($_SESSION['error_msg'])){
        $msg = $_SESSION['error_msg'];
        unset($_SESSION['error_msg']);
        echo '<div class="alert-message">'.$msg.'</div>';
    }
	// Page Template
	$pTemplate = $o_page->get_pTemplate();
	if($o_page->_page['SiteID'] != $o_site->_site['SitesID'] )
		include TEMPLATE_DIR."admin.php";	
	elseif($pTemplate['pt_url'])
		include $pTemplate['pt_url'];
	elseif($o_page->_page['n'] == $o_site->_site['StartPage'] && strlen($search_tag)<3 && strlen($search)<3)
		include TEMPLATE_DIR . "home.php";
	else
		include TEMPLATE_DIR . "main.php";
		
	include TEMPLATE_DIR . "footer.php";
	
?>

        <script src="<?=TEMPLATE_DIR?>assets/js/jquery-2.1.4.min.js"></script><!-- jQuery library -->
        <script src="/Templates/ra/js/bootstrap.min.js"></script>

        
        <script src="<?=TEMPLATE_DIR?>assets/js/jquery.srcipts.min.js"></script><!-- modernizr, retina, stellar for parallax -->  
        <script src="<?=TEMPLATE_DIR?>assets/owl-carousel/owl.carousel.min.js"></script><!-- Carousels script -->
        <script src="<?=TEMPLATE_DIR?>assets/masterslider/masterslider.min.js"></script><!-- Master slider main js -->
        <script src="<?=TEMPLATE_DIR?>assets/js/jquery.dlmenu.min.js"></script><!-- for responsive menu -->
        <script src="<?=TEMPLATE_DIR?>assets/js/include.js"></script><!-- custom js functions -->
        <script src="<?=TEMPLATE_DIR?>assets/js/breakingNews.js"></script><!-- breaking news -->

        <script>
            /* <![CDATA[ */
            jQuery(document).ready(function ($) {
                'use strict';
                // MASTER SLIDER START
                var slider = new MasterSlider();
                slider.setup('masterslider', {
                    width: 1140, // slider standard width
                    height: 333, // slider standard height
                    space: 0,
                    speed: 50,
                    layout: "fullwidth",
                    centerControls: false,
                    loop: true,
                    autoplay: true
                            // more slider options goes here...
                            // check slider options section in documentation for more options.
                });
                // adds Arrows navigation control to the slider.
                slider.control('arrows');

                $('#client-carousel').owlCarousel({
                    items: 6,
                    loop: true,
                    margin: 30,
                    responsiveClass: true,
                    mouseDrag: true,
                    dots: false,
                    responsive: {
                        0: {
                            items: 2,
                            nav: true,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true
                        },
                        600: {
                            items: 3,
                            nav: true,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true
                        },
                        1000: {
                            items: 6,
                            nav: true,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true,
                            mouseDrag: true
                        }
                    }
                });
            });
            /* ]]> */
        </script>
        <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/bg_BG/sdk.js#xfbml=1&version=v2.5&appId=1650640101821444";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>
	$(window).load(function(e) {		
		$("#bn10").breakingNews({
			effect		:"slide-v",
			autoplay	:true,
			timer		:3000,
			border      :true,
			color		:"red"
		});
    });
 </script>
      
    </body>
</html>
