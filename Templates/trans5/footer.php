        <div id="footer-wrapper" class="footer-dark">
            <footer id="footer">
                <div class="container">
                    <div class="row">
                    <?php
if(($o_site->_site['news'] > 0) && ($o_site->_site['news'] <> $o_site->_site['StartPage']) && (count($news)>0) ) {
?>
                        <ul class="col-md-3 col-sm-6 footer-widget-container clearfix">
                            <!-- .widget-pages start -->
                            <li class="widget widget_pages">
                                <div class="title">
                                    <h3>������</h3>
                                </div>

                                <ul>
            <?php
				for($i=0; $i<count($news); $i++)
					{
					echo "<li>";
					$o_page->print_pName(true, $news[$i]['n']);
					echo "</li>";
					}
			?>
                                </ul>
                            </li><!-- .widget-pages end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->
                        <?php
} // page news is not set
?>

                        <ul class="col-md-3 col-sm-6 footer-widget-container">
                            <!-- .widget-pages start -->
                            <li class="widget widget_pages">
                                <div class="title">
                                    <h3>���-���������� ��������</h3>
                                </div>

                                <ul>
            <?php
				for($i=0; $i<count($most_viewed_pages); $i++)
					{
					  echo "<li>";
					  $o_page->print_pName(true, $most_viewed_pages[$i]['n']);
					  echo "</li>";
					}
			?>
                                </ul>
                            </li><!-- .widget-pages end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->

                        <ul class="col-md-3 col-sm-6 footer-widget-container">
                            <!-- .widget-pages start -->
                            <li class="widget widget_pages">
                                <div class="title">
                                    <h3>���� � �����</h3>
                                </div>

                                <ul>
			<?php
				for($i=0; $i<count($last_added_pages); $i++)
					{
					echo "<li>";
					$o_page->print_pName(true, $last_added_pages[$i]['n']);
					echo "</li>";
					}
			?>
                                </ul>
                            </li><!-- .widget-pages end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->

                        <ul class="col-md-3 col-sm-6 footer-widget-container">
                            <li class="widget widget-text">
                                <div class="title">
                                    <h3>��������</h3>
                                </div>
                                <address>
                                ���������� �������
                                </address>
                                <span class="text-big">
                                0700 47 505
                                </span>
                                <br />
                                <address>
                                ������� ���� 3 �
                                </address>
                                <span class="text-big">
                                02/8 133 143
                                </span>

                                <br /><br />

                                <a href="mailto:">office@trans5.bg</a>
                                <br />
                                <ul class="footer-social-icons">
                                <?php if(defined('CMS_FACEBOOK_PAGE_LINK') || $user->AccessLevel >0) { ?> <li><a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" target="_blank" class="fa fa-facebook"></a></li> <?php }?>
                                <li><a href="http://<?=$o_site->get_sURL()?>/rss.php" class="fa fa-rss"></a></li>
                                </ul><!-- .footer-social-icons end -->
                            </li><!-- .widget.widget-text end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->
                    </div><!-- .row end -->
                </div><!-- .container end -->
            </footer><!-- #footer end -->
            
          
<div class="container">
<div class="paymentlogos">  
 <?php 
if(VISA_MC_ICONS == 1) 
{
include "web/admin/modules/payments/payment_logos.php";
}
?>
</div>
</div>

            <div class="copyright-container">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p>� <?=date("Y")?></p>
                        </div><!-- .col-md-6 end -->

                        <div class="col-md-6">
                            <p class="align-right">������ � ���������� �� <a href="http://www.maksoft.net">Maksoft.net</a></p>
                        </div><!-- .col-md-6 end -->
                    </div><!-- .row end -->
                </div><!-- .container end -->
            </div><!-- .copyright-container end -->

            <a href="#" class="scroll-up">Scroll</a>
        </div><!-- #footer-wrapper end -->