<?php
    if(!isset($after_head)) {
        $after_head = '';
    }
    if(!isset($imageHeaderWidth)) {
        $imageHeaderWidth = '';
    }
?>
<html>
<head>
<?php echo("$after_head"); ?>
<title><?php echo("$Title"); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<?php

//includes the meta tags of the site
 include("Templates/meta_tags.php"); 
$o_site->print_sConfigurations();
 if($row->SiteID == $SiteID)
{
	
	?>
	<script type="text/javascript" src="/lib/lightbox/prototype.js"></script>
	<script type="text/javascript" src="/lib/lightbox/scriptaculous.js?load=effects"></script>
	<script type="text/javascript" src="/lib/lightbox/lightbox.js"></script>
	<link rel="stylesheet" href="/lib/lightbox/lightbox.css" type="text/css" media="screen">
	<?php
}
 //set the drop-down menu category : only sites with toplink=drop_links will be shown in the menu
 $drop_links = 2;
?>

<script language="JavaScript">
<!--
function getElement(id) { 

	return document.getElementById ? document.getElementById(id) : 
						document.all ? document.all(id) : null; 
} 


function show_hide(id) {

	obj = getElement(id);
	if (obj.style.display == 'none') {
		obj.style.display = '';
	}
	else {
		obj.style.display = 'none';
	}
}

function redirect(url)
{
if(url!="") document.location = url;
}


function startTime()
{
var today=new Date()
var h=today.getHours()
var m=today.getMinutes()
var s=today.getSeconds()
// add a zero in front of numbers<10
m=checkTime(m)
s=checkTime(s)
document.getElementById('txt').innerHTML=h+":"+m+":"+s
t=setTimeout('startTime()',500)
}

function checkTime(i)
{
if (i<10) 
  {i="0" + i}
  return i
}

//-->
</script>

<?php

//split the text by paragraphs and select paragraph longer than 5 symbols
//example: <p>par1</p><p>paragraph5</p> will return <p>paragraph5</p>  
// THIS FUNCTION IS COMMENTED BY EMO AFTER SERVER HACK IN ORDER TO FIX GEOSVIAT.COM WEB SITE
//function crop_text($text_str='')
//{
//	$split_text = spliti("</P>", $text_str);
//	$i=0;
//	
//			for($i=0; $i<=15; $i++)
//			{
//				if(strlen($split_text[$i]) > 5) 
//					{
//					$j=$i;
//					break;
//					}
//			}
//			
//	return $split_text[$j]."<br>"; 		
//}

//default CSS
echo "<link href='/css/main/main.css' rel='stylesheet' type='text/css'>";

define ("DIR_TEMPLATE","Templates/main");
define ("DIR_TEMPLATE_IMAGES","/Templates/main");
$MAX_WIDTH = 950;


if(defined('GOOGLE_TRACKING_CODE'))   { include_once("web/admin/modules/google/analitycs.php"); }
if( (defined('GOOGLE_SITE_VERIF')) && ($o_page->n == $o_page->_site['StartPage']) )   { include_once("web/admin/modules/google/wmt.php"); }

?>
</head>
<script src="https://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-4074238-2";
urchinTracker();
</script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9&appId=152835228556640";
      fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<table border="0" align="left" width="<?=$MAX_WIDTH?>

" height="100%" cellpadding="0" cellspacing="0" style="padding-top: 0px;">
<tr><td colspan="3" valign="top" height="1">
	<?php
	// including header file
	include DIR_TEMPLATE."/header.php";
	?>
</tr>
<tr>
	<td width="175" valign="top" class="t4">
	<?php
	// including main menu
	if(file_exists(DIR_TEMPLATE."/column_left.php")) include DIR_TEMPLATE."/column_left.php";
	?>
	<td valign="top" align="left" width="<?=$MAIN_WIDTH?>">
	<!-- main content //-->
		<?php
		// including main content
		
		if(file_exists(DIR_TEMPLATE."/main.php")) include_once DIR_TEMPLATE."/main.php";
		?>

	<!-- end main content //-->
	<td width="1" valign="top" class="t4">
		<?php
		// including right menu if $show_right_menu=true
		//if($show_right_menu==true)
			if(file_exists(DIR_TEMPLATE."/column_right.php")) include_once DIR_TEMPLATE."/column_right.php";
		?>
</tr>
<tr><td colspan="3" valign="top">
		<?php
		// including footer
		if(file_exists(DIR_TEMPLATE."/footer.php")) include_once DIR_TEMPLATE."/footer.php";
		?>
</tr>
</table>
</body>
</html>

