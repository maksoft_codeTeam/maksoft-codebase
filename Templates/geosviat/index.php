<!DOCTYPE HTML>
<?php
define("TEMPLATE_NAME", "geosviat");
define("TEMPLATE_DIR", "Templates/".TEMPLATE_NAME."/");
define("TEMPLATE_IMAGES", "http://www.maksoft.net/".TEMPLATE_DIR."images/");
define("ASSETS_DIR", "/Templates/".TEMPLATE_NAME."/");
//include template configurations
include(TEMPLATE_DIR."config.php");

?>
<html lang="bg">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
		<title><?=$Title?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<?php
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
		?>
		<link href="<?=ASSETS_DIR;?>css/bootstrap.min.css" rel="stylesheet">
		<link href="<?=ASSETS_DIR;?>css/styles.css" rel="stylesheet">
		<link href="<?=ASSETS_DIR;?>wowslider/style.css" rel="stylesheet">
		<link href="<?=ASSETS_DIR;?>css/lightbox.css" rel="stylesheet">
		<link href="<?=ASSETS_DIR;?>rhinoslider/css/rhinoslider-1.05.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
		<script src="<?=ASSETS_DIR;?>js/bootstrap.min.js"></script>
		<script src="<?=ASSETS_DIR;?>js/script.js"></script>
		<script src="<?=ASSETS_DIR;?>js/lightbox.js"></script>
		<script src="<?=ASSETS_DIR;?>js/wowslider.js"></script>
		<script src="<?=ASSETS_DIR;?>rhinoslider/js/easing.js"></script>
		<script src="<?=ASSETS_DIR;?>rhinoslider/js/mousewheel.js"></script>
		<script src="<?=ASSETS_DIR;?>rhinoslider/js/rhinoslider-1.05.js"></script>
		
		
		<script>
		jQuery(document).ready(function(){
			jQuery("a[rel='lightbox[gallery]']").removeAttr("rel").attr("data-lightbox","gallery");
		});
		</script>

	</head>
	<body>
    <div class="container">
		<nav class="navbar navbar-default navbar-static-top hidden-xs margin-bottom-0" role="navigation">
			<div class="navbar-header">
				<a class="navbar-brand" rel="home" href="#"><i class="fa fa-home"></i> ������</a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".trat">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse trat">
				<ul class="nav navbar-nav">
				<?php
				$SitesID = ($o_page->_site["SitesID"]);
				$query = $o_site->db_query("SELECT * FROM pages WHERE SiteID = $SitesID AND toplink = 2 AND status = 1 ORDER BY sort_n"); 
				while($result = mysqli_fetch_array($query)){
					$slug = $result["slug"];
					if($slug==''){
						$slug = "?n=".$result["n"];
					}
					echo "<li><a href='http://".$o_site->_site["primary_url"].$slug."'>".$result["Name"]."</a></li>";
				}
				?>
				</ul>
					<div class="col-sm-3 col-md-3 pull-right">
					  <form class="navbar-form" role="search">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="�������..." name="srch-term" id="srch-term">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
							</div>
						</div>
					  </form>
					</div>
					<div class="col-sm-2 col-md-2 pull-right padding-tb-5">
						<a class="margin-top-2 btn btn-success" href="http://agent.geosviat.bg/" target="_blank"><i class="fa fa-user"></i>&nbsp; ���� �� ������</a>
					</div>
			</div>
		</nav>
			<nav class="navbar navbar-default visible-xs" id="onlymobilemenu" role="navigation">
				<div class="navbar-header">
					<a class="navbar-brand" rel="home" href="#"><i class="fa fa-home"></i> ������</a>
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".tratatat">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse tratatat">
					<ul class="nav navbar-nav">
					<?php
					
					$groupContent = $o_page->get_pGroupContent($leftSideBar);
					$i=0;
					foreach($groupContent as $page){
						$slug=$page["slug"];
						if($slug==""){
							$slug="?n=".$page["n"];
						}
						$nn = $page["n"];
						if(($o_page->get_pSubpagesCount($nn))>0){
							?>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-expanded="false"><?=$page["Name"];?> <span class="caret"></span></a>
								<ul class="dropdown-menu">
							
								<li>
									<a href="http://<?=$o_site->_site["primary_url"].$slug;?>"><?=$page["Name"];?></a>
								</li>
								<li class="divider"></li>
							<?php
							$subs = $o_page->get_pSubpages($nn);
							foreach($subs as $sub){
								$subslug=$sub["slug"];
								if($subslug==""){
									$subslug="?n=".$sub["n"];
								}
								?>
									<li>
										<a href="http://<?=$o_site->_site["primary_url"].$subslug;?>"><?=$sub["Name"];?></a>
									</li>
								<?php
							}
							?>
								</ul>
							</li>
							<?php
						}
					}
					?>
					</ul>
					<div class="col-sm-3 col-md-3 pull-right">
					  <form class="navbar-form" role="search">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="�������..." name="srch-term" id="srch-term">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
							</div>
						</div>
					  </form>
					</div>
					<div class="col-sm-2 col-md-2 pull-right padding-tb-5">
						<a class="margin-top-2 btn btn-success" href="http://agent.geosviat.bg/" target="_blank"><i class="fa fa-user"></i>&nbsp; ���� �� ������</a>
					</div>
				</div>
			</nav>
         <div class="smalllogo"> 
         <img src="<?=ASSETS_DIR;?>images/logosmall.png" alt="" border="0" /> 
        </div>
		
        		<div class="row margin0" id="headslider">
                   
	<?php include('banners.php'); ?>

		</div>
		<div class="row divider margin0">
		</div>
			<ol class="breadcrumb margin-bottom-5">
				<?php $o_page->print_pNavigation();?>
			</ol>
			<div class="col-xs-12 col-sm-3 hidden-xs" id="left_sidebar">
				<?php
				$groupContent = $o_page->get_pGroupContent($leftSideBar);
				$i=0;
				foreach($groupContent as $page){
					
					$slug=$page["slug"];
					if($slug==""){
						$slug="?n=".$page["n"];
					}
					?>
					<div class="bs-component">
					  <div class="list-group padding-left-10 padding-top-4">
						<a href="http://<?=$o_site->_site["primary_url"].$slug;?>" class="list-group-item active">
						  <i class="fa fa-asterisk"></i> <?=$page["Name"];?>
						</a>
						
						<?php
						$nn = $page["n"];
						$subs = $o_page->get_pSubpages($nn);
						foreach($subs as $sub){
							$trat = $o_page->get_page($sub["n"]);
							$subslug=$sub["slug"];
							if($subslug==""){
								$subslug="?n=".$sub["n"];
							}
							echo '<a style="background:url('.$backgr.') white;" href="http://'.$o_site->_site["primary_url"].$slug.'" class="list-group-item"><i class="fa fa-angle-right"></i> '.$sub["Name"].'</a>';
						}
						?>
					  </div>
					</div>
					<?php
				}
				?>
			</div>
			<div class="col-xs-12 col-sm-7 padding0">
			<?php
			if($o_page->_page['SiteID'] == $o_site->_site['SitesID'] ){
				if($o_page->_page['n'] == $o_site->_site['StartPage']){
					include_once TEMPLATE_DIR."home.php";
				}else{
					include_once TEMPLATE_DIR."main.php";
				}
			}else{
				include_once TEMPLATE_DIR."admin.php";
			}
			?>
			</div>
			<div class="col-xs-12 col-sm-3" id="right_sidebar">
				<?php
				$groupContent = $o_page->get_pGroupContent($rightSideBar);
				$i=0;
				foreach($groupContent as $page){
					$slug=$page["slug"];
					if($slug==""){
						$slug="?n=".$page["n"];
					}
					?>
					<div class="bs-component">
					  <div class="list-group padding-right-5 padding-top-4">
						<a href="http://<?=$o_site->_site["primary_url"].$slug;?>" class="list-group-item active">
						  <i class="fa fa-asterisk"></i> <?=$page["Name"];?>
						</a>
						
						<?php
						$nn = $page["n"];
						$subs = $o_page->get_pSubpages($nn);
						foreach($subs as $sub){
							$subslug=$sub["slug"];
							if($subslug==""){
								$subslug="?n=".$sub["n"];
							}
							echo '<a href="http://'.$o_site->_site["primary_url"].$slug.'" class="list-group-item"><i class="fa fa-angle-right"></i> '.$sub["Name"].'</a>';
						}
						?>
					  </div>
					</div>
					<?php
				}
				?>
					<div class="bs-component">
						<div class="panel panel-primary">
							<div class="panel-heading">
							  <h3 class="panel-title"><i class="fa fa-user"></i> ����</h3>
							</div>
							<div class="panel-body">
							  
								<div class="form-login">
									<form name="login" method="post" action="http://www.maksoft.net/web/admin/login.php" >
									<input type="text" name="username" id="userName" class="form-control input-sm chat-input" placeholder="����������" />
									</br>
									<input type="password" name="pass" id="userPassword" class="form-control input-sm chat-input" placeholder="������" />
									</br>
									<div class="wrapper">
									<span class="group-btn">   
										<input name="n" type="hidden" id="n" value="<?=$n;?>">
										<input name="SiteID" type="hidden" id="SiteID" value="<?=$SiteID;?>">
										<button type="submit" class="btn btn-primary" name="Submit"><i class="fa fa-sign-in"></i> ����</button>
									</span>
									</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
				<div class="row margin0">
				
					<div class="thumbnail center well well-small text-center">
						<h3>��������</h3>
						
						<p>���������� �� �� ����� ������������� �������</p>
						
						<form action="http://geosviat.us3.list-manage.com/subscribe/post?u=b1877676dbb558f8afae58aea&amp;id=73cc2e70f3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
							<div class="input-prepend"><span class="add-on"><i class="icon-envelope"></i></span>
								 <input value="" name="EMAIL" class="form-control input-sm" id="mce-EMAIL" type="email" placeholder="����� �-����"/></br>
								<input value="" name="FNAME" class="form-control input-sm" id="mce-FNAME" type="text" placeholder="���"/></br>
								<input value="" name="LNAME" class="form-control input-sm" id="mce-LNAME" type="text" placeholder="�������"/>
							</div>
							<br />
							<input value="���������" name="subscribe" id="mc-embedded-subscribe" class="btn btn-large" type="submit">
					  </form>
					</div>  
				</div>
			</div>
			<?php
			include_once TEMPLATE_DIR."footer.php";
			?>
		</div>
	</body>
</html>