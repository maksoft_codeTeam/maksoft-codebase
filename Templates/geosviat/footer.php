<div class="row margin0">
	<div class="col-xs-12">
		<hr/>
	</div>
</div>
<footer>
	<a id="back-to-top" href="#" class="btn btn-default btn-sm back-to-top" role="button" data-original-title="" title="" style="display: inline;"><span class="fa fa-chevron-up"></span></a>
	<div class="row margin0">
			<div class="col-xs-12">
				<div class="col-xs-12 col-sm-6">
				<?php
				$SitesID = ($o_page->_site["SitesID"]);
				$query = $o_site->db_query("SELECT * FROM pages WHERE SiteID = $SitesID AND toplink = 2 AND status = 1 ORDER BY sort_n"); 
				while($result = mysqli_fetch_array($query)){
					$slug = $result["slug"];
					if($slug==''){
						$slug = "?n=".$result["n"];
					}
					echo "<a href='http://".$o_site->_site["primary_url"].$slug."'>".$result["Name"]."</a> ";
				}
				?>
				</div>
				<div class="col-xs-12 col-sm-6">
					<p class="muted pull-right">&copy; &nbsp;<?php echo date("Y"); ?>&nbsp;<?php echo("$Site->Title"); ?></p>
				</div>
			</div>
	</div>
</footer>

<!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_kit_size_32 a2a_floating_style a2a_vertical_style" style="right:0px; top:230px;">
    <a class="a2a_button_facebook"></a>
    <a class="a2a_button_twitter"></a>
    <a class="a2a_button_google_plus"></a>
    <a class="a2a_button_pinterest"></a>
    <a class="a2a_button_email"></a>
    <a class="a2a_button_print"></a>

</div>

<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
<style type="text/css">
/* Hide AddToAny vertical share bar when screen is less than 980 pixels wide */
@media screen and (max-width: 980px) {
    .a2a_floating_style.a2a_vertical_style { display: none; }
}
</style>