<html>
<head>
<title><?php echo("$Title");?></title>
<?php
include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/aris/");
define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/Templates/aris/images/");
define("DIR_MODULES","web/admin/modules/");

if(!isset($MAX_HEIGHT))
	$MAX_HEIGHT = "100%";
if(!isset($MAIN_WIDTH))
	$MAIN_WIDTH = "590";
if(!isset($MAX_WIDTH))
	$MAX_WIDTH = "820";
if(!isset($MENU_WIDTH))
	$MENU_WIDTH = "220";
if(!isset($PAGE_ALIGN))
	$PAGE_ALIGN = "center";

//load CSS style
echo "<link href='http://www.maksoft.net/css/aris/base_style.css' rel='stylesheet' type='text/css'>";
if($row->n == 11 || $row->ParentPage == 11 || $row->SiteID == 1)
	echo "<link href='http://www.maksoft.net/css/aris/base_style_admin.css' rel='stylesheet' type='text/css'>";
?>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>
<body>
<div align="center">
<div id="main_container">
	<div id="header">
				<?php
				// including header file
				include DIR_TEMPLATE."header.php";
				?>	
	</div>

	<div id="main_menu">
			<?php
				include(DIR_TEMPLATE."column_left.php") 
			?>
	</div>
	<div id="page_title"><div class="head_text"><?=$row->Name?></div></div>	
	<div id="page_content" align="center"><center>
			<?php
					if($n == $Site->StartPage)
						include(DIR_TEMPLATE."home.php");
					else
						include(DIR_TEMPLATE."main.php");
			?>
	</center>	
	</div>
	<div id="footer">
	<?php
			echo $nav_bar;
	?>
	</div>
</div>
</div>
</body>
</html>