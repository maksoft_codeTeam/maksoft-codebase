<div id="slider2" class="sliderwrapper" style="display: block;">
	<div style="width: 750px;">
	<div class="contentdiv"><img src="Templates/ecoconcept/images/home_gallery/5.jpg" width="750px"></div>
	</div>
	<div id="paginate-slider2" class="pagination">
	<a href="#" class="prev" style="margin: 0 0 0 20px;"><img src="Templates/dogramata/images/arrow_left.jpg" border="0" alt=""></a> <a href="#" class="toc">01.</a> <a href="#" class="toc anotherclass">02.</a> <a href="#" class="toc">03.</a> <a href="#" class="toc">04.</a> <a href="#" class="toc">05.</a> <a href="#" class="next"><img src="Templates/dogramata/images/arrow_right.jpg" border="0" alt=""></a>
	</div>
</div>
<script type="text/javascript">

featuredcontentslider.init({
	id: "slider2",  //id of main slider DIV
	contentsource: ["inline", ""],  //Valid values: ["inline", ""] or ["ajax", "path_to_file"]
	toc: "markup",  //Valid values: "#increment", "markup", ["label1", "label2", etc]
	nextprev: ["Previous", "Next"],  //labels for "prev" and "next" links. Set to "" to hide.
	revealtype: "click", //Behavior of pagination links to reveal the slides: "click" or "mouseover"
	enablefade: [true, 0.1],  //[true/false, fadedegree]
	autorotate: [true, 3000],  //[true/false, pausetime]
	onChange: function(previndex, curindex){  //event handler fired whenever script changes slide
		//previndex holds index of last slide viewed b4 current (1=1st slide, 2nd=2nd etc)
		//curindex holds index of currently shown slide (1=1st slide, 2nd=2nd etc)
	}
})

</script>   