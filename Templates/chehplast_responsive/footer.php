<a id="back-to-top" href="#" class="btn btn-primary btn-sm back-to-top" role="button"><span class="glyphicon glyphicon-chevron-up"></span></a>
<hr/>
<footer>
	<div class="row margin0">
		<script src="http://www.jqueryscript.net/demo/Responsive-jQuery-News-Ticker-Plugin-with-Bootstrap-3-Bootstrap-News-Box/scripts/jquery.bootstrap.newsbox.min.js"></script>
        <div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="glyphicon glyphicon-list-alt"></span><b> <?=EVENTS;?></b>
			</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12">
							<ul class="demo1" style="overflow-y: hidden; height: 211px;">
							<?php
							foreach($news as $post){
								?>
								<li style="" class="news-item">
									<table cellpadding="4">
										<tbody>
											<tr>
												<td>
													<a href="<?=$post['page_link'];?>">
														<strong>
															<?=$post['Name'];?>
														</strong>
													</a>
													<p><?=substr(strip_tags(crop_text($post['textStr'])), 0, 80);?></p>
												</td>
											</tr>
										</tbody>
									</table>
								</li>
								
								<?php
							}
							?>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel-footer">
				
				</div>
			</div>
		</div>
		<script>
		$(function () {
			$(".demo1").bootstrapNews({
				newsPerPage: 3,
				autoplay: true,
				pauseOnHover:true,
				direction: 'down',
				newsTickerInterval: 4000,
				onToDo: function () {
					//console.log(this);
				}
			});
        });
		</script>
	</div>
    	<div class="row margin0">
        <div class="col-xs-12">
    		<div class="thumbnail center well well-small text-center">
                <h2>����������</h2>
                <a href="https://excellent-sme-bulgaria.safesigned.com/cheh-plast"><img src="Templates/chehplast2/images/sme.png" alt="������������ �������� �� ���������� " /></a>

            </div>    
        </div>
	</div>
	<div class="row margin0">
        <div class="col-xs-12">
    		<div class="thumbnail center well well-small text-center">
                <h2><img src="Templates/chehplast_responsive/images/logo.png" width="30"> <?=NEWSLETTER;?></h2>
                
                <p><?=NEWSLETTERINFO;?></p>
                
                <form action="" method="post">
                    <div class="input-prepend"><span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" id="" name="" placeholder="�����@email.com">
                    </div>
                    <br />
                    <input type="submit" value="<?=SUBSCRIBE;?>" class="btn btn-large" />
              </form>
            </div>    
        </div>
	</div>
	<div class="row margin0">
		<div class="carousel slide media-carousel" id="footerlogos">
			<div class="carousel-inner">
				<div class="item text-center active"><center><a href="http://www.rehau.bg" target="_blank" title="������� �����"><img src="<?=TEMPLATE_IMAGES?>logo_rehau.jpg" alt="������� Rehau">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.solid55.com" target="_blank"><img src="<?=TEMPLATE_IMAGES?>logo_solid.jpg" alt="">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.madeinetem.com" target="_blank"><img src="<?=TEMPLATE_IMAGES?>logo_etem.jpg" alt="">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.schueco.com" target="_blank"><img src="<?=TEMPLATE_IMAGES?>logo_schuco.jpg" alt="">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.alumil.com" target="_blank"  title="������� Alumil"><img src="<?=TEMPLATE_IMAGES?>logo_alumil.jpg" alt="������� ������">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.geze.de" target="_blank"><img src="<?=TEMPLATE_IMAGES?>logo_geze.jpg" alt="">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.hormann.bg" target="_blank" title="������� ����� Hormann"><img src="<?=TEMPLATE_IMAGES?>logo_horman.jpg" alt="�������">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.dorma.com/bg" target="_blank"><img src="<?=TEMPLATE_IMAGES?>logo_dorma.jpg" alt="">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.reynaers.com" target="_blank"><img src="<?=TEMPLATE_IMAGES?>logo_reynaers.jpg" alt="">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.nevapv.cz" target="_blank"><img src="<?=TEMPLATE_IMAGES?>logo_neva.jpg" alt="">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.quanex.com/Products/3/ProductLine/Insulating-Glass-Systems/29/" target="_blank"><img src="<?=TEMPLATE_IMAGES?>logo_truseal.jpg" alt="">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.saav-bg.com" target="_blank" title="������� ����"><img src="<?=TEMPLATE_IMAGES?>logo_CAAB.jpg" alt="������� ����" height="45">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.atrea.cz" target="_blank"><img src="<?=TEMPLATE_IMAGES?>logo_atrea.jpg" alt="">
				</a></center></div>
				<div class="item text-center"><center><a  href="http://www.bulwindoors.org" target="_blank" title="��������� ��������� ����� � ��������"><img src="<?=TEMPLATE_IMAGES?>logo_sdrujenie.jpg" alt="��������� ��������� ����� � ��������" height="45">
				</a></center></div>
			</div>
			
				<a data-slide="prev" href="#footerlogos" class="left carousel-control">�</a>
				<a data-slide="next" href="#footerlogos" class="right carousel-control">�</a>
		</div>
		<script>
		$(document).ready(function(){
			$('#footerlogos').carousel({
				interval:500
			});
		});
		</script>
	</div>
	<hr/>
	<div class="row margin0">
		<div class="col-xs-12">
		<ul class="nav nav-pills text-center">
		<?php
		foreach($bottom_menu as $link){
			$o_page->print_pName(true, $link['n']); echo " / ";
		}
		?>
		</ul>
		</div>
	</div>
	<div class="text-center center-block">
				<p class="txt-railway">- <a href="http://<?=$o_site->_site['primary_url'];?>"><?=$o_site->_site['Name'];?></a> -</p>
					<?php if(defined('CMS_FACEBOOK_PAGE_LINK')) { ?><a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-facebook-square fa-3x social-fb"></i></a> <?php }?>
					<?php if(defined('CMS_GPLUS_PAGE_LINK')) { ?><a href="<?=(defined("CMS_GPLUS_PAGE_LINK")) ? CMS_GPLUS_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-google-plus-square fa-3x social-fb"></i></a> <?php }?>
					<?php if(defined('CMS_TWITTER_PAGE_LINK')) { ?><a href="<?=(defined("CMS_TWITTER_PAGE_LINK")) ? CMS_TWITTER_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-twitter-square fa-3x social-tw"></i></a> <?php }?>
					<?php if(defined('CMS_PINTEREST_PAGE_LINK')) { ?><a href="<?=(defined("CMS_PINTEREST_PAGE_LINK")) ? CMS_PINTEREST_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-pinterest-square fa-3x social-tw"></i></a> <?php }?>
					<?php if(defined('CMS_LINKEDIN_PAGE_LINK')) { ?><a href="<?=(defined("CMS_LINKEDIN_PAGE_LINK")) ? CMS_LINKEDIN_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-linkedin-square fa-3x social-tw"></i></a> <?php }?>
					<?php if(defined('CMS_BLOG_PAGE_LINK')) { ?><a href="<?=(defined("CMS_BLOG_PAGE_LINK")) ? CMS_BLOG_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-twitter-square fa-3x social-tw"></i></a> <?php }?>
	</div>
    <hr>
	<script  type="text/javascript">
            <!-- NACHALO NA TYXO.BG BROYACH -->
            d=document;
            d.write('<a href="http://www.tyxo.bg/?86158" title="Tyxo.bg counter" target=" blank"><img width="65" border="0" alt="Tyxo.bg counter"');
            d.write(' src="http://cnt.tyxo.bg/86158?rnd='+Math.round(Math.random()*2147483647));
            d.write('&sp='+screen.width+'x'+screen.height+'&r='+escape(d.referrer)+'" /><\/a>');
            //-->
    </script>
    <noscript><a href="http://www.tyxo.bg/?86158" title="Tyxo.bg counter" target=" blank"><img src="http://cnt.tyxo.bg/86158" width="65" border="0" alt="Tyxo.bg counter" /></a></noscript>
    <!-- KRAI NA TYXO.BG BROYACH -->
		
	<?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="" target="_blank">Netservice</a>
</footer>