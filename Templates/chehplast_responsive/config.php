<?php
	
	//show what options are available for config
	$tmpl_config_support = array(
		"change_theme"=>true,					// full theme support
		"change_layout"=>true,					// layout support
		"change_banner"=>true,				// banner support
		"change_css"=>true,						// css support
		"change_background"=>true,		// background support
		"change_fontsize"=>true,				// fontsize support
		"change_max_width"=>true,			// site width support
		"change_menu_width"=>false,		// site menu width support
		"change_main_width"=>true,		// page content width support
		"change_logo"=>true,						// logo support
		"column_left" => true, 						// column left support
		"column_right" => true,					// column right support
		"drop_down_menu"=> true			// drop-down menu support
	);
	//define some default values
	$tmpl_config = array(
		"default_banner" => TEMPLATE_IMAGES."banner.jpg",
		"default_logo" => TEMPLATE_DIR."images/logo.png",	
		"date_format"=> "d-m-Y",
		"homepage"=> "56837"
	);
	$homepageboxes = array(
		"first" => array("n" => $n_first, "img" => "front_photo_01.jpg"),
		"second" => array("n" => $n_second, "img" => "front_photo_02.jpg"),
		"third" => array("n" => $n_third, "img" => "front_photo_03.jpg"),
		"fourth" => array("n" => $n_fourth, "img" => "front_photo_04.jpg"),
	);
	
	//page about
	$p_about = $o_page->get_page($n_about);

	//news
	$news = $o_page->get_pSubpages($o_site->get_sNewsPage());

	//promo products
	$promotions = $o_page->get_pSubpages($n_promotions);

	//video
	$video_pages = $o_page->get_pSubpages($n_video);

	$bottom_menu = $o_page->get_pSubpages(0, "p.sort_n ASC", "p.toplink = 5");

	//home page titles
	$h_titles = $o_page->get_pSubpages($n_products, "p.sort_n ASC LIMIT 4");

	//page newsletter
	$p_newsletter = $o_page->get_page($n_newsletter);
	/* if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->get_sImageDIr()."/".basename($o_site->_site['Logo']); */

?>