<?php
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__. '/../../lib/messages.php';
require_once __DIR__.'/config.php';
$em = $entityManager = $settings->getEntityManager();

$request = Request::createFromGlobals();

$route = new Maksoft\Rent\Bus\Controller\Router($request, $twig, $em);

$logged = $route->is_logged();



Message\display();

if($route->is_logged()){
    $route->add("/", "Maksoft\Rent\Bus\View\B2B\IndexLoggedView");
    $route->add("/?profile", "Maksoft\Rent\Bus\View\B2B\EditProfileView");
    $route->add("/?add-vehicle", "Maksoft\Rent\Bus\View\B2B\AddVehicleView");
    $route->add("/?list-vehicles", "Maksoft\Rent\Bus\View\B2B\ListVehicleView");
    $route->add("/?orders", "Maksoft\Rent\Bus\View\B2B\OrdersView");
    $route->add("/?offers", "Maksoft\Rent\Bus\View\B2B\OfferView");
} else {
    $route->add("/", "Maksoft\Rent\Bus\View\B2B\IndexView");
}


$response =  $route->render();
if($logged != $route->is_logged()){
    header("Location: /");
}
?>
<?php

echo $response;
