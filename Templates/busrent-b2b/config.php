<?php

define('PREFIX', 'busr');
define('SESSION_USER', 'user');
define('SESSION_RENT', 'rent');
define('SESSION_EXPIRE', '1800'); # 30 min
define('SESSION_EXPIRE_PREFIX', 'expire_at'); # 30 min
define('SESSION_REAUTH', 'reauth'); # 30 min
define('SESSION_ORDER', 'order');
define('SESSION_ENQ', 'enquiries');
define('MAX_REQUESTS', 3);
define('PAGE_OFFERS', 19359428);
define('SUCCESS', 'success');
define('ERROR', 'error');
define('INFO', 'info');
define('WARNING', 'warning');
define('LOGIN_PAGE', 19371329);
