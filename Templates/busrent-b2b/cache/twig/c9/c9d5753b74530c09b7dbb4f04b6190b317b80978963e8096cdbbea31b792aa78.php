<?php

/* helpers/error_success_message.html */
class __TwigTemplate_f3adb8ea1cb4800ad51b1bae42de597372f147d6d6480109483729cfe8dfc473 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["msg"])) { $_msg_ = $context["msg"]; } else { $_msg_ = null; }
        if ((($this->getAttribute($_msg_, "status", array()) == "error") || ($this->getAttribute($_msg_, "code", array()) == "error"))) {
            // line 2
            echo "<h5><p class=\"bg-danger\"> ";
            if (isset($context["msg"])) { $_msg_ = $context["msg"]; } else { $_msg_ = null; }
            echo $this->getAttribute($_msg_, "text", array());
            echo "  </p> </h5>
";
        } else {
            // line 4
            echo "<h5><p class=\"bg-success\"> ";
            if (isset($context["msg"])) { $_msg_ = $context["msg"]; } else { $_msg_ = null; }
            echo $this->getAttribute($_msg_, "text", array());
            echo "  </p> </h5>
";
        }
    }

    public function getTemplateName()
    {
        return "helpers/error_success_message.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "helpers/error_success_message.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/helpers/error_success_message.html");
    }
}
