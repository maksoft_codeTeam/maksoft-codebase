<?php

/* login.html */
class __TwigTemplate_2bcfc12976abd1d789a1f560f5892e38d0cae30919fcc1b54dfed23e5320e158 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    
<head>
        <title>Matrix Admin</title><meta charset=\"cp1251\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
        <link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap.min.css\" />
        <link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap-responsive.min.css\" />
        <link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/matrix-login.css\" />
        <link href=\"Templates/busrent-b2b/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div id=\"loginbox\">            
            <div class=\"widget-box\">
                <div class=\"widget-content\">
                    <div class=\"alert alert-";
        // line 17
        if (isset($context["message"])) { $_message_ = $context["message"]; } else { $_message_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_message_, "code", array()), "html", null, true);
        echo "\">
                        <button class=\"close\" data-dismiss=\"alert\">x</button>
                        <strong>";
        // line 19
        if (isset($context["messages"])) { $_messages_ = $context["messages"]; } else { $_messages_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_messages_, "code", array()), "html", null, true);
        echo "</strong> <p></p>";
        if (isset($context["messages"])) { $_messages_ = $context["messages"]; } else { $_messages_ = null; }
        echo $this->getAttribute($_messages_, "text", array());
        echo "</div>
                </div>
            </div>
            ";
        // line 22
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "start", array());
        echo "
                 <div class=\"control-group normal_text\"> <h3><img src=\"Templates/busrent/assets/images/logo-busrent.png\" alt=\"Logo\" /></h3></div>
                <div class=\"control-group\">
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-envelope\"> </i></span>";
        // line 27
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "email", array());
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"control-group\">
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_ly\"><i class=\"icon-lock\"></i></span>";
        // line 34
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "password", array());
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"form-actions\">
                    <span class=\"pull-left\"><a href=\"#\" class=\"flip-link btn btn-info\" id=\"to-recover\">�����������</a></span>
                    <span class=\"pull-right\">";
        // line 40
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "submit", array());
        echo "</span>
                </div>
            ";
        // line 42
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "action", array());
        echo "
            ";
        // line 43
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "end", array());
        echo "
            ";
        // line 44
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "start", array());
        echo "
                <p class=\"normal_text\">
                �������� �������� ������� � ��� ���������� �� �� ������ � ���.
                </p>
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-user\"></i></span>";
        // line 50
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "name", array());
        echo "
                        </div>
                    </div>
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-envelope\"></i></span>";
        // line 55
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "vat", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-tasks\"></i></span>";
        // line 61
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "license", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-user-md\"></i></span>";
        // line 67
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "owner", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-globe\"></i></span>";
        // line 73
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "city", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-screenshot\"></i></span>";
        // line 79
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "address", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-phone\"></i></span>";
        // line 85
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "phone", array());
        echo "
                        </div>
                    </div>
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-envelope\"></i></span>";
        // line 90
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "email", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_ly\"><i class=\"icon-lock\"></i></span>";
        // line 96
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "password", array());
        echo "
                        </div>
                    </div>
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_ly\"><i class=\"icon-lock\"></i></span>";
        // line 101
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "password_repeated", array());
        echo "
                        </div>
                    </div>
                    ";
        // line 104
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "action", array());
        echo "
               
                <div class=\"form-actions\">
                    <span class=\"pull-left\"><a href=\"#\" class=\"flip-link btn btn-success\" id=\"to-login\">&laquo; �����</a></span>
                    <span class=\"pull-right\">";
        // line 108
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "submit", array());
        echo "</span>
                </div>
                ";
        // line 110
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "end", array());
        echo "
        </div>
        
        <script src=\"Templates/busrent-b2b/assets/js/jquery.min.js\"></script>  
        <script src=\"Templates/busrent-b2b/assets/js/matrix.login.js\"></script> 
    </body>

</html>

";
    }

    public function getTemplateName()
    {
        return "login.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 110,  211 => 108,  203 => 104,  196 => 101,  187 => 96,  177 => 90,  168 => 85,  158 => 79,  148 => 73,  138 => 67,  128 => 61,  118 => 55,  109 => 50,  99 => 44,  94 => 43,  89 => 42,  83 => 40,  73 => 34,  62 => 27,  53 => 22,  43 => 19,  37 => 17,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "login.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/login.html");
    }
}
