<?php

/* index.html */
class __TwigTemplate_7c1ede134108a8b544d85e51c39dc3a30233be7b1c8dfd7e242e8fffa802c2e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html", "index.html", 1);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_style($context, array $blocks = array())
    {
        // line 3
        echo "<link rel=\"stylesheet\" href=\"Templates/maksoft/v2/assets/fullcalendar/fullcalendar.css\">
<link rel=\"stylesheet\" href=\"Templates/maksoft/v2/assets/fullcalendar/scheduler.css\">
<link rel=\"stylesheet\" href=\"Templates/maksoft/v2/assets/fullcalendar/fullcalendar.print.css\" media=\"print\">
";
    }

    // line 8
    public function block_content($context, array $blocks = array())
    {
        // line 9
        echo "<!--Action boxes-->
  <div class=\"container-fluid\">
    <div class=\"quick-actions_homepage\">
      <ul class=\"quick-actions\">
        <li class=\"bg_lb\"> <a href=\"/?profile\"> <i class=\"icon-user\"></i> ������ </a> </li>
        <li class=\"bg_lg span3\"> <a href=\"/?orders\"> <i class=\"icon-tasks\"></i>
                <span class=\"label label-important\">";
        // line 15
        if (isset($context["orders"])) { $_orders_ = $context["orders"]; } else { $_orders_ = null; }
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $_orders_), "html", null, true);
        echo "</span> ����������</a> </li>
        <li class=\"bg_ly\"> <a href=\"/?offers\"> <i class=\"fa fa-first-order\" style=\"margin-bottom: 5%;display:block;font-size: 2em;\" aria-hidden=\"true\"></i><span class=\"label label-success\">";
        // line 16
        if (isset($context["company"])) { $_company_ = $context["company"]; } else { $_company_ = null; }
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute($_company_, "getOffers", array())), "html", null, true);
        echo "</span> ����� ������</a> </li>
        <li class=\"bg_lo\"> <a href=\"/?feedback\"> <i class=\"icon-comments\"></i> ������</a> </li>
        <li class=\"bg_ls\"> <a href=\"/?list-vehicles\"> 
            <i class=\"fa fa-bus\" style=\"margin-bottom: 5%;display:block;font-size: 2em;\" aria-hidden=\"true\"></i><span class=\"label label-important\">";
        // line 19
        if (isset($context["vehicles"])) { $_vehicles_ = $context["vehicles"]; } else { $_vehicles_ = null; }
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $_vehicles_), "html", null, true);
        echo "</span> ��������</a> </li>
        <li class=\"bg_lo span3\"> <a href=\"/?add-vehicle\"> <i class=\"icon-plus-sign\"></i> ������ �������� ��������</a> </li>
      </ul>
    </div>
    <div class=\"row-fluid\">
        <div id=\"calendar\" class=\"fc\"></div>
    </div>
  </div>
  <div class=\"row-fluid\">
      <div class=\"span6\">
        <div class=\"widget-box\">
          <div class=\"widget-title bg_ly\" data-toggle=\"collapse\" href=\"#collapseG2\"><span class=\"icon\"><i class=\"icon-chevron-down\"></i></span>
            <h5>�������� ��������</h5>
          </div>
          <div class=\"widget-content nopadding collapse in\" id=\"collapseG2\">
            <ul class=\"recent-posts\">
                ";
        // line 35
        if (isset($context["vehicles"])) { $_vehicles_ = $context["vehicles"]; } else { $_vehicles_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_vehicles_);
        foreach ($context['_seq'] as $context["_key"] => $context["vehicle"]) {
            // line 36
            echo "              <li>
                <div class=\"user-thumb\"> 
                    ";
            // line 38
            if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute($_vehicle_, "getImages", array()), 0, 1));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 39
                echo "                    <img width=\"40\" height=\"40\" alt=\"User\" src=\"";
                if (isset($context["image"])) { $_image_ = $context["image"]; } else { $_image_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_image_, "getPath", array()), "html", null, true);
                echo "\"> 
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 41
            echo "                </div>
                <div class=\"article-post\"><b> <span class=\"user-info\"> ";
            // line 42
            if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
            echo $this->getAttribute($this->getAttribute($_vehicle_, "getMake", array()), "getName", array());
            echo " ";
            if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
            echo $this->getAttribute($this->getAttribute($_vehicle_, "getModel", array()), "getname", array());
            echo " - ";
            if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_vehicle_, "getSeats", array()), "html", null, true);
            echo " ������</span></b>
                    <p></p>
                    ";
            // line 44
            if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($_vehicle_, "getExtras", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["extra"]) {
                // line 45
                echo "                        <span class=\"label label-info\">";
                if (isset($context["extra"])) { $_extra_ = $context["extra"]; } else { $_extra_ = null; }
                echo $this->getAttribute($_extra_, "getName", array());
                echo "</span> 
                        ";
                // line 46
                if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
                if ( !$this->getAttribute($_loop_, "last", array())) {
                    // line 47
                    echo "                            |
                        ";
                }
                // line 48
                echo "  
                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extra'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "                </div>
              </li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vehicle'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "            </ul>
          </div>
        </div>
      </div>
      <div class=\"span6\">
        <div class=\"widget-box\">
          <div class=\"widget-title\"> <span class=\"icon\"><i class=\"icon-ok\"></i></span>
            <h5>������ ������</h5>
          </div>
          <div class=\"widget-content\">
            ";
        // line 63
        if (isset($context["offers"])) { $_offers_ = $context["offers"]; } else { $_offers_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_offers_);
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["offer"]) {
            // line 64
            echo "            <div class=\"new-update clearfix\"><i class=\"icon-ok-sign\"></i>
                <div class=\"update-done\"><a title=\"\" href=\"#\"><strong>��:
                            ";
            // line 66
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            echo $this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getFromCity", array());
            echo " ��: ";
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            echo $this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getDestination", array());
            echo "</strong></a>
                    <span>���� �� ����������: ";
            // line 67
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "order", array()), "getDeparature", array()), "m/d/Y"), "html", null, true);
            echo "</span>
                    <br>
                    ";
            // line 69
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            if ($this->getAttribute($this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "order", array()), "getTwoWay", array())) {
                // line 70
                echo "                    <span class=\"label label-success\">����������</span>
                    <br>
                    <span>���� �� �������: ";
                // line 72
                if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "order", array()), "getArrival", array()), "m/d/Y"), "html", null, true);
                echo "</span>
                    ";
            }
            // line 74
            echo "                    <span style='color:green'><b>";
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_offer_, "getPrice", array()), "html", null, true);
            echo "��.</b></span>
                </div>
              <div class=\"update-date\"><span class=\"update-day\">";
            // line 76
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getDeparature", array()), "m"), "html", null, true);
            echo "</span>";
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            if (isset($context["getDeparature"])) { $_getDeparature_ = $context["getDeparature"]; } else { $_getDeparature_ = null; }
            echo twig_escape_filter($this->env, range($this->getAttribute($_offer_, "getOrder", array()), twig_date_format_filter($this->env, $_getDeparature_, "M")), "html", null, true);
            echo "</div>
            </div>
            ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 79
            echo "                ��� ��� ������ ������ ������
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['offer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 81
        echo "          </div>
        </div>
    </div>
        <div class=\"widget-box\">
          <div class=\"widget-title bg_lo\" data-toggle=\"collapse\" href=\"#collapseG3\"> <span class=\"icon\"> <i class=\"icon-chevron-down\"></i> </span>
            <h5>������� ����������</h5>
          </div>
          <div class=\"widget-content nopadding updates collapse in\" id=\"collapseG3\">
              ";
        // line 89
        if (isset($context["orders"])) { $_orders_ = $context["orders"]; } else { $_orders_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_orders_);
        foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
            // line 90
            echo "            <div class=\"new-update clearfix\"><i class=\"icon-ok-sign\"></i>
                <div class=\"update-done\"><a title=\"\" href=\"#\"><strong>��: ";
            // line 91
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo $this->getAttribute($_order_, "getFromCity", array());
            echo " ��: ";
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo $this->getAttribute($_order_, "getDestination", array());
            echo "</strong></a>
                    <span>���� �� ����������: ";
            // line 92
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_order_, "getDeparature", array()), "m/d/Y"), "html", null, true);
            echo "</span>
                    <br>
                    ";
            // line 94
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            if ($this->getAttribute($_order_, "getTwoWay", array())) {
                // line 95
                echo "                    <span class=\"label label-success\">����������</span>
                    <br>
                    <span>���� �� �������: ";
                // line 97
                if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_order_, "getArrival", array()), "m/d/Y"), "html", null, true);
                echo "</span>
                    ";
            }
            // line 99
            echo "                    <a class=\"btn btn-success\" href='/?orders'> ������� ������ </a>
                </div>
              <div class=\"update-date\"><span class=\"update-day\">";
            // line 101
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_order_, "getDeparature", array()), "m"), "html", null, true);
            echo "</span>";
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_order_, "getDeparature", array()), "M"), "html", null, true);
            echo "</div>
            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 104
        echo "          </div>
        </div>
      </div>
";
    }

    // line 109
    public function block_script($context, array $blocks = array())
    {
        // line 110
        echo "<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/lib/jquery.min.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/lib/jquery-ui.min.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/lib/moment.min.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/locale/bg.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/fullcalendar.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/gcal.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/js/tablesorter.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  324 => 110,  321 => 109,  314 => 104,  301 => 101,  297 => 99,  291 => 97,  287 => 95,  284 => 94,  278 => 92,  270 => 91,  267 => 90,  262 => 89,  252 => 81,  245 => 79,  232 => 76,  225 => 74,  219 => 72,  215 => 70,  212 => 69,  206 => 67,  198 => 66,  194 => 64,  188 => 63,  176 => 53,  168 => 50,  153 => 48,  149 => 47,  146 => 46,  140 => 45,  122 => 44,  110 => 42,  107 => 41,  97 => 39,  92 => 38,  88 => 36,  83 => 35,  63 => 19,  56 => 16,  51 => 15,  43 => 9,  40 => 8,  33 => 3,  30 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "index.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/index.html");
    }
}
