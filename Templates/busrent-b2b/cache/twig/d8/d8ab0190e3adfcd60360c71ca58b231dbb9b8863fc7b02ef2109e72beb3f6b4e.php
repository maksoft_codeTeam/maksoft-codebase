<?php

/* list-orders.html */
class __TwigTemplate_931cb7c7e770ae27d113291bc8913831c8beadb2c8c98597ffa4ad2a24b76c84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html", "list-orders.html", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container-fluid\">
    <div class=\"row-fluid\">
        <!-- START FORM -->
        <div class=\"span1\"></div>
        <div class=\"span10\">

        ";
        // line 10
        if (isset($context["msg"])) { $_msg_ = $context["msg"]; } else { $_msg_ = null; }
        echo $this->getAttribute($_msg_, "text", array());
        echo "
            ";
        // line 11
        if (isset($context["orders"])) { $_orders_ = $context["orders"]; } else { $_orders_ = null; }
        if ((twig_length_filter($this->env, $_orders_) > 0)) {
            // line 12
            echo "                ";
            if (isset($context["orders"])) { $_orders_ = $context["orders"]; } else { $_orders_ = null; }
            $this->loadTemplate("order-template.html", "list-orders.html", 12)->display(array_merge($context, array("orders" => $_orders_)));
            // line 13
            echo "            ";
        } else {
            // line 14
            echo "                ���� ������� �������
            ";
        }
        // line 16
        echo "
        </div>
        <div class=\"span1\"></div>
    </div>
</div>
";
    }

    // line 24
    public function block_script($context, array $blocks = array())
    {
        // line 25
        echo "<script async defer
    src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDX6iHHXUbcJjQi961C5jjWTD6nyU2jc3Q\">
 </script>
<script src=\"Templates/busrent-b2b/assets/js/jquery.wizard.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.wizard.js\"></script>
</body>
<script>
\$.each(\$(\".order-detail\"), function(){
    var from_city  = \$(this).find(\".from-city\").text();
    var to_city  = \$(this).find(\".to-city\").text();
    //var url = \"https://maps.googleapis.com/maps/api/distancematrix/json?eunits=imperial&origins=\"+from_city+\"&destinations=\"+to_city+\"&key=AIzaSyDX6iHHXUbcJjQi961C5jjWTD6nyU2jc3Q\";
    var url = \"/Templates/busrent-b2b/api.php?action=distance&destination=\"+to_city+\"&origin=\"+from_city;
    var table = \$(this);
    \$.ajax({
        url: \"Templates/busrent-b2b/api.php\",
        type: \"get\",
        data: {action: \"distance\", destination: to_city, origin: from_city},
        dataType: \"json\",
        success: function(response){
            if(response.status == \"OK\"){
                table.append(\"<tr><td>����������:</td><td>\"+ response.rows[0].elements[0].distance.text + \"</td></tr>\");
                table.append(\"<tr><td>�����:</td><td>\"+ response.rows[0].elements[0].duration.text + \"</td></tr>\");
            }
            console.log(response);
        },
        error: function(xhr){
            console.log(xhr);
        }
    });
});
</script>
";
    }

    public function getTemplateName()
    {
        return "list-orders.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 25,  68 => 24,  59 => 16,  55 => 14,  52 => 13,  48 => 12,  45 => 11,  40 => 10,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "list-orders.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/list-orders.html");
    }
}
