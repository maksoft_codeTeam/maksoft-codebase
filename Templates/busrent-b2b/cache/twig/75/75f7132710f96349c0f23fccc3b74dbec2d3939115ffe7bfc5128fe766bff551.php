<?php

/* list-vehicle.html */
class __TwigTemplate_803614ade817b36c2b3a1f69df8bc1d44f5355947e71a485d65d8d13a40be405 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html", "list-vehicle.html", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"container-fluid\">
    <div class=\"row-fluid\">
        <div class=\"span12\">
        ";
        // line 6
        if (isset($context["vehicles"])) { $_vehicles_ = $context["vehicles"]; } else { $_vehicles_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_vehicles_);
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["vehicle"]) {
            // line 7
            echo "            ";
            if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
            if ((($this->getAttribute($_loop_, "index", array()) % 1) == 0)) {
                // line 8
                echo "                <div class=\"row-fluid\">
            ";
            }
            // line 10
            echo "
            ";
            // line 11
            if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
            $this->loadTemplate("vehicle-template2.html", "list-vehicle.html", 11)->display(array_merge($context, array("vehicle" => $_vehicle_)));
            // line 12
            echo "
            ";
            // line 13
            if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
            if (((($this->getAttribute($_loop_, "index", array()) % 1) == 0) || ($this->getAttribute($_loop_, "last", array()) &&  !$this->getAttribute($_loop_, "first", array())))) {
                // line 14
                echo "                </div>
            ";
            }
            // line 16
            echo "        ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 17
            echo "        <h3>��� ��� ������ �������� �������� ��������</h3>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vehicle'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "        </div>
    </div>
    
</div>
";
    }

    public function getTemplateName()
    {
        return "list-vehicle.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 19,  92 => 17,  79 => 16,  75 => 14,  72 => 13,  69 => 12,  66 => 11,  63 => 10,  59 => 8,  55 => 7,  36 => 6,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "list-vehicle.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/list-vehicle.html");
    }
}
