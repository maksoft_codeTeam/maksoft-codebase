<?php

/* add-vehicle.html */
class __TwigTemplate_a2230bd978944a99f733acadefc182b67fe448ed506fa1d1d6a3720dff93f5d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html", "add-vehicle.html", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"container-fluid\">
    <div class=\"row-fluid\">
        <!-- START FORM -->
        <div class=\"span6\">
            <div class=\"widget-box\">
                <div class=\"widget-title\"> <span class=\"icon\"> <i class=\"icon-align-justify\"></i> </span>
                  <h5>";
        // line 11
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "title", array());
        echo "</h5>
                </div>
                <div class=\"widget-content nopadding\">
                    <form class=\"form-horizontal\" enctype=\"multipart/form-data\" name=\"upload\" method=\"post\">
                        <div class=\"control-group\">
                          <label class=\"control-label\">��� :</label>
                          <div class=\"controls\">
                            <select name=\"type\" onclick=\"renderMakes()\" onchange=\"renderMakes()\">
                                <option value=\"3\">��������</option>
                                <option value=\"2\">A������</option>
                            </select>
                          </div>
                        </div>
                        <div class=\"control-group\">
                          <label class=\"control-label\">����� :</label>
                          <div class=\"controls\">
                            <select id=\"make\" name=\"make\" onclick=\"renderModels(this.value)\"> </select>
                          </div>
                        </div>
                        <div class=\"control-group\">
                          <label class=\"control-label\">����� :</label>
                          <div id=\"model-div\" class=\"controls\">
                            <select id=\"model\" name=\"model\"> </select>
                          </div>
                        </div>
                        <div class=\"control-group\">
                          <label class=\"control-label\">";
        // line 37
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($this->getAttribute($_vehicle_, "fuel", array()), "label", array());
        echo "</label>
                          <div id=\"model-div\" class=\"controls\">
                            <select name=\"fuel\">
                                <option value=\"������\">������</option>
                                <option value=\"�����\" selected>�����</option>
                                <option value=\"�����\">�����</option>
                                <option value=\"�����\">�����</option>
                            </select>
                          </div>
                        </div>
                        <div class=\"control-group\">
                          <label class=\"control-label\">";
        // line 48
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($this->getAttribute($_vehicle_, "manufactured", array()), "label", array());
        echo "</label>
                          <div id=\"model-div\" class=\"controls\">
                            ";
        // line 50
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "manufactured", array());
        echo "
                          </div>
                        </div>
                        <div class=\"control-group\">
                          <label class=\"control-label\">";
        // line 54
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($this->getAttribute($_vehicle_, "seats", array()), "label", array());
        echo " :</label>
                          <div class=\"controls\">
                             ";
        // line 56
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "seats", array());
        echo "
                          </div>
                        </div>
                        <div class=\"control-group\">
                          <label class=\"control-label\">������ :</label>
                            <div class=\"controls\">
                            ";
        // line 62
        if (isset($context["extras"])) { $_extras_ = $context["extras"]; } else { $_extras_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_extras_);
        foreach ($context['_seq'] as $context["category"] => $context["col"]) {
            // line 63
            echo "                                <div class=\"span6\">
                                    <h5> ";
            // line 64
            if (isset($context["category"])) { $_category_ = $context["category"]; } else { $_category_ = null; }
            echo $_category_;
            echo "</h5>
                                    <div class=\"row\">
                                    ";
            // line 66
            if (isset($context["col"])) { $_col_ = $context["col"]; } else { $_col_ = null; }
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($_col_);
            foreach ($context['_seq'] as $context["_key"] => $context["extra"]) {
                // line 67
                echo "                                        <div class=\"span12 checkbox\">
                                            <label>
                                                <input style=\"width: 1em;height: 1em;\" name=\"extras[]\" type=\"checkbox\" value=\"";
                // line 69
                if (isset($context["extra"])) { $_extra_ = $context["extra"]; } else { $_extra_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_extra_, "getId", array()), "html", null, true);
                echo "\">";
                if (isset($context["extra"])) { $_extra_ = $context["extra"]; } else { $_extra_ = null; }
                echo $this->getAttribute($_extra_, "getName", array());
                echo "
                                            </label>
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extra'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "                                    </div>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['category'], $context['col'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "                            </div>
                        </div>
                        <div class=\"control-group\">
                          <label class=\"control-label\">������:</label>
                          <div class=\"controls\">
                            <input type=\"file\" name=\"image[]\" multiple>
                            ";
        // line 82
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "action", array());
        echo "
                          </div>
                        </div>
                        <div class=\"form-actions\">
                          <button type=\"submit\" class=\"btn btn-success\">Save</button>
                        </div>
                        ";
        // line 88
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "end", array());
        echo "
                    </div>
                </div>
          </div>

          </div>
        <!-- END FORM -->
    </div>
</div>

";
    }

    // line 100
    public function block_script($context, array $blocks = array())
    {
        // line 101
        echo "<script>
function renderMakes()
{
    var type = \$(\"select[name=type]\").val()
    var url = \"/Templates/busrent-b2b/api.php?action=make&type=\" + type
    \$.getJSON(url, function(result) {
        var options = \$(\"#make\");
        //don't forget error handling!
        options.find('option')
               .remove()
               .end();
        var first = 0;
        \$.each(result, function(id, name) {
            if(first == 0){
                first = id;
            }
            options.append(\$(\"<option />\").val(id).text(name));
        });
        renderModels(first);
    });

}

function renderModels(model)
{
    var url = \"/Templates/busrent-b2b/api.php?action=model&make=\" + model
    \$.getJSON(url, function(result) {
        if(!result || result.length == 0 || result.code == \"error\"){
            \$(\"#model-div\").html('<input name=\"model\" type=\"text\" placeholder=\"��� �� ������: \">');
        } else {
            \$(\"#model-div\").html('<select id=\"model\" name=\"model\"></select>');
            var options = \$(\"#model\");
            //don't forget error handling!
            options.find('option')
                   .remove()
                   .end();
            \$.each(result, function(id, name) {
                options.append(\$(\"<option />\").val(id).text(name));
            });
        }
    });
}


renderMakes();
</script>

";
    }

    public function getTemplateName()
    {
        return "add-vehicle.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 101,  194 => 100,  178 => 88,  168 => 82,  160 => 76,  152 => 73,  138 => 69,  134 => 67,  129 => 66,  123 => 64,  120 => 63,  115 => 62,  105 => 56,  99 => 54,  91 => 50,  85 => 48,  70 => 37,  40 => 11,  32 => 5,  29 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "add-vehicle.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/add-vehicle.html");
    }
}
