<?php

/* base.html */
class __TwigTemplate_7de803cbcd61022aec7b034f5f80036672dc317939465bf15cdcd26cf46e04c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
<title>Matrix Admin</title>
<meta charset=\"cp1251\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap.min.css\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap-responsive.min.css\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/fullcalendar.css\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/matrix-style.css\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/matrix-media.css\" />
<link href=\"Templates/busrent-b2b/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/jquery.gritter.css\" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

";
        // line 18
        $this->loadTemplate("top-header.html", "base.html", 18)->display($context);
        // line 19
        echo "
";
        // line 20
        $this->loadTemplate("sidebar.html", "base.html", 20)->display($context);
        // line 21
        echo "
<!--main-container-part-->
<div id=\"content\">
<!--breadcrumbs-->
  <div id=\"content-header\">
    <div id=\"breadcrumb\"> <a href=\"index.html\" title=\"Go to Home\" class=\"tip-bottom\"><i class=\"icon-home\"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

";
        // line 30
        $this->displayBlock('content', $context, $blocks);
        // line 33
        echo "
";
        // line 34
        $this->displayBlock('footer', $context, $blocks);
        // line 43
        echo "
<script src=\"Templates/busrent-b2b/assets/js/excanvas.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.ui.custom.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/bootstrap.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.flot.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.flot.resize.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.peity.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/fullcalendar.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.dashboard.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.gritter.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.interface.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.chat.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.validate.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.form_validation.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.wizard.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.uniform.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/select2.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.popover.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.dataTables.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.tables.js\"></script> 
";
        // line 65
        $this->displayBlock('script', $context, $blocks);
        // line 68
        echo "
<script type=\"text/javascript\">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != \"\") {
      
          // if url is \"-\", it is this page -- reset the menu:
          if (newURL == \"-\" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>

";
    }

    // line 30
    public function block_content($context, array $blocks = array())
    {
        // line 31
        echo "
";
    }

    // line 34
    public function block_footer($context, array $blocks = array())
    {
        echo " 

<!--Footer-part-->
<div class=\"row-fluid\">
  <div id=\"footer\" class=\"span12\"> 2013 &copy; Matrix Admin. Brought to you by <a href=\"http://themedesigner.in\">Themedesigner.in</a> </div>
</div>
<!--end-Footer-part-->

";
    }

    // line 65
    public function block_script($context, array $blocks = array())
    {
        // line 66
        echo "
";
    }

    public function getTemplateName()
    {
        return "base.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 66,  146 => 65,  132 => 34,  127 => 31,  124 => 30,  92 => 68,  90 => 65,  66 => 43,  64 => 34,  61 => 33,  59 => 30,  48 => 21,  46 => 20,  43 => 19,  41 => 18,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/base.html");
    }
}
