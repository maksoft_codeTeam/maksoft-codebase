<?php

/* edit-profile.html */
class __TwigTemplate_303e93e22f97fa6db0ff81750d0813bea7b87bb48f013f19c50b7cbb110c0ac0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html", "edit-profile.html", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    
<div class=\"container-fluid\">
    <div class=\"row-fluid\">
        <div class=\"span6\">
            <h3>";
        // line 8
        if (isset($context["edit"])) { $_edit_ = $context["edit"]; } else { $_edit_ = null; }
        echo $this->getAttribute($_edit_, "title", array());
        echo "</h3>
            ";
        // line 9
        if (isset($context["edit"])) { $_edit_ = $context["edit"]; } else { $_edit_ = null; }
        echo $_edit_;
        echo "
        </div>
        <div class=\"span6\">
            <h3>";
        // line 12
        if (isset($context["password"])) { $_password_ = $context["password"]; } else { $_password_ = null; }
        echo $this->getAttribute($_password_, "title", array());
        echo "</h3>
            ";
        // line 13
        if (isset($context["password"])) { $_password_ = $context["password"]; } else { $_password_ = null; }
        echo $_password_;
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "edit-profile.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 13,  49 => 12,  42 => 9,  37 => 8,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "edit-profile.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/edit-profile.html");
    }
}
