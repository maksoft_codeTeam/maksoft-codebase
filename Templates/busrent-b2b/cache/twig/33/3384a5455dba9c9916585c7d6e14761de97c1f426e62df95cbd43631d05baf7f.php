<?php

/* helpers/input-field.html */
class __TwigTemplate_21025bb51a1f88aa1ddaf70ac972970573d69762cc97351763dbefd72d63bc91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"control-group\">
    <div class=\"controls\">
      <label class=\"control-label\">";
        // line 3
        if (isset($context["input"])) { $_input_ = $context["input"]; } else { $_input_ = null; }
        echo $this->getAttribute($_input_, "label", array());
        echo "</label>
      <div id=\"model-div\" class=\"controls\">
        ";
        // line 5
        if (isset($context["input"])) { $_input_ = $context["input"]; } else { $_input_ = null; }
        echo $_input_;
        echo "
      </div>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "helpers/input-field.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 5,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "helpers/input-field.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/helpers/input-field.html");
    }
}
