<?php

/* index.html */
class __TwigTemplate_38f6a96a6e73b96d13e8beb02eb8b29116fdaa578956f6394bee7b53e342ba0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html", "index.html", 1);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_style($context, array $blocks = array())
    {
        // line 3
        echo "<link rel=\"stylesheet\" href=\"Templates/maksoft/v2/assets/fullcalendar/fullcalendar.css\">
<link rel=\"stylesheet\" href=\"Templates/maksoft/v2/assets/fullcalendar/scheduler.css\">
<link rel=\"stylesheet\" href=\"Templates/maksoft/v2/assets/fullcalendar/fullcalendar.print.css\" media=\"print\">
";
    }

    // line 8
    public function block_content($context, array $blocks = array())
    {
        // line 9
        echo "<!--Action boxes-->
  <div class=\"container-fluid\">
    <div class=\"quick-actions_homepage\">
      <ul class=\"quick-actions\">
        <li class=\"bg_lb\"> <a href=\"/?profile\"> <i class=\"icon-user\"></i> ������ </a> </li>
        <li class=\"bg_lg span3\"> <a href=\"/?orders\"> <i class=\"icon-tasks\"></i>
                <span class=\"label label-important\">";
        // line 15
        if (isset($context["orders"])) { $_orders_ = $context["orders"]; } else { $_orders_ = null; }
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $_orders_), "html", null, true);
        echo "</span> ����������</a> </li>
        <li class=\"bg_ly\"> <a href=\"/?offers\"> <i class=\"fa fa-first-order\" style=\"margin-bottom: 5%;display:block;font-size: 2em;\" aria-hidden=\"true\"></i><span class=\"label label-success\">";
        // line 16
        if (isset($context["company"])) { $_company_ = $context["company"]; } else { $_company_ = null; }
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute($_company_, "getOffers", array())), "html", null, true);
        echo "</span> ����� ������</a> </li>
        <li class=\"bg_lo\"> <a href=\"/?feedback\"> <i class=\"icon-comments\"></i> ������</a> </li>
        <li class=\"bg_ls\"> <a href=\"/?list-vehicles\"> 
            <i class=\"fa fa-bus\" style=\"margin-bottom: 5%;display:block;font-size: 2em;\" aria-hidden=\"true\"></i><span class=\"label label-important\">";
        // line 19
        if (isset($context["vehicles"])) { $_vehicles_ = $context["vehicles"]; } else { $_vehicles_ = null; }
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $_vehicles_), "html", null, true);
        echo "</span> ��������</a> </li>
        <li class=\"bg_lo span3\"> <a href=\"/?add-vehicle\"> <i class=\"icon-plus-sign\"></i> ������ �������� ��������</a> </li>
      </ul>
    </div>
    <div class=\"row-fluid\">
        <div id=\"calendar\" class=\"fc\"></div>
    </div>
  </div>
<!--End-Action boxes-->    
";
        // line 80
        echo " 
    </div>
";
    }

    // line 84
    public function block_script($context, array $blocks = array())
    {
        // line 85
        echo "<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/lib/jquery.min.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/lib/jquery-ui.min.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/lib/moment.min.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/locale/bg.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/fullcalendar.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/gcal.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/js/tablesorter.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 85,  82 => 84,  76 => 80,  63 => 19,  56 => 16,  51 => 15,  43 => 9,  40 => 8,  33 => 3,  30 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "index.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/index.html");
    }
}
