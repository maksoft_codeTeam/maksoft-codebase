<?php

/* order-template.html */
class __TwigTemplate_2a202e66cb729ce83db5e53ba5b54385d57f52964532d171f850c47487d9e311 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["orders"])) { $_orders_ = $context["orders"]; } else { $_orders_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_orders_);
        foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
            // line 2
            echo "        <h6> N: ";
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo $this->getAttribute($_order_, "getId", array());
            echo " </h6>
        <hr>
        <div class=\"row-fluid\">
            <div class=\"span5\">
                <img src=\"https://i.stack.imgur.com/IUVm4.png\">
            </div>
            <div class=\"span7\">
                <div class=\"widget-content nopadding\">
                    <table class=\"table table-bordered order-detail\">
                      <thead>
                        <tr>
                          <th>Browser</th>
                          <th>Visits</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td> ���� �� ����������: </td>
                            <td> ";
            // line 20
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_order_, "getDeparature", array()), "html", null, true);
            echo " </td>
                        </tr>
                        ";
            // line 22
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            if (($this->getAttribute($_order_, "getTwoWay", array()) == 1)) {
                // line 23
                echo "                            <tr>
                                <td> ���� �� �������: </td>
                                <td> ";
                // line 25
                if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_order_, "getArrival", array()), "html", null, true);
                echo " </td>
                            </tr>
                        ";
            }
            // line 28
            echo "                        <tr>
                          <td>�������� ��:</td>
                          <td class=\"from-city\">";
            // line 30
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo $this->getAttribute($_order_, "getFromCity", array());
            echo "</td>
                        </tr>
                        <tr>
                          <td>���������� �:</td>
                          <td class=\"to-city\">";
            // line 34
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo $this->getAttribute($_order_, "getDestination", array());
            echo "</td>
                        </tr>
                        <tr>
                          <td>���������:</td>
                          <td>";
            // line 38
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_order_, "getPersons", array()), "html", null, true);
            echo "</td>
                        </tr>
                        <tr>
                          <td>����:</td>
                          <td>";
            // line 42
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_order_, "getChildrens", array()), "html", null, true);
            echo "</td>
                        </tr>
                        <tr>
                          <td>���������� ��������</td>
                          <td>
                          ";
            // line 47
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            if (($this->getAttribute($_order_, "getTwoWay", array()) == 1)) {
                // line 48
                echo "                          <span class=\"label label-success\">��</span>
                          ";
            } else {
                // line 50
                echo "                          <span class=\"label label-important\">��</span>
                          ";
            }
            // line 52
            echo "                          </td>
                        </tr>
                        <tr>
                        <td>������� � ���</td>
                        <td>
                        <span class=\"label label-success\">
                            ";
            // line 58
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_order_, "getDays", array()), "html", null, true);
            echo "
                        </span>
                        </td>
                        </tr>
                        <tr>
                            <td>��������� ������:</td>
                            <td>
                            ";
            // line 65
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            if (isset($context["session"])) { $_session_ = $context["session"]; } else { $_session_ = null; }
            if ((twig_length_filter($this->env, $this->getAttribute($_order_, "getOffer", array(0 => $this->getAttribute($_session_, "id", array())), "method")) == 0)) {
                // line 66
                echo "                            <span class=\"label label-info\"> �� </span>
                            ";
            } else {
                // line 68
                echo "                            <span class=\"label label-error\"> �� </span>
                            ";
            }
            // line 70
            echo "                            </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
            </div>
        </div>
        <div class=\"span12\">
        <h5>������ ������: </h5>
        ";
            // line 79
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($_order_, "getExtras", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["extra"]) {
                // line 80
                echo "            <span class=\"label label-info\">";
                if (isset($context["extra"])) { $_extra_ = $context["extra"]; } else { $_extra_ = null; }
                echo $this->getAttribute($_extra_, "getName", array());
                echo "</span> | 
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extra'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 82
            echo "        </div>
        <div class=\"span12\">
        ";
            // line 84
            if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
            if (isset($context["session"])) { $_session_ = $context["session"]; } else { $_session_ = null; }
            if ((twig_length_filter($this->env, $this->getAttribute($_order_, "getOffer", array(0 => $this->getAttribute($_session_, "id", array())), "method")) == 0)) {
                // line 85
                echo "            <div class=\"accordion\" id=\"collapse-group\">
              <div class=\"accordion-group widget-box\">
                <div class=\"accordion-heading\">
                  <div class=\"widget-title\"> <a data-parent=\"#collapse-group\" href=\"#collapseGOne\" data-toggle=\"collapse\"> <span class=\"icon\"><i class=\"icon-ok\"></i></span>
                    <h5>������� ������ �����������</h5>
                    </a> </div>
                </div>
                <div class=\"collapse in accordion-body\" id=\"collapseGOne\">
                  <div class=\"widget-content\">
                    ";
                // line 94
                if (isset($context["createOffer"])) { $_createOffer_ = $context["createOffer"]; } else { $_createOffer_ = null; }
                echo $this->getAttribute($_createOffer_, "start", array());
                echo "
                    <input type=\"hidden\" name=\"order_id\" value=\"";
                // line 95
                if (isset($context["order"])) { $_order_ = $context["order"]; } else { $_order_ = null; }
                echo $this->getAttribute($_order_, "getId", array());
                echo "\">
                    <select name=\"vehicles[]\" multiple>
                    ";
                // line 97
                if (isset($context["vehicles"])) { $_vehicles_ = $context["vehicles"]; } else { $_vehicles_ = null; }
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($_vehicles_);
                foreach ($context['_seq'] as $context["_key"] => $context["vehicle"]) {
                    // line 98
                    echo "                      <option value=\"";
                    if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_vehicle_, "getId", array()), "html", null, true);
                    echo "\">";
                    if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
                    echo $this->getAttribute($this->getAttribute($_vehicle_, "getMake", array()), "getName", array());
                    echo " - ";
                    if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
                    echo $this->getAttribute($_vehicle_, "getModel", array());
                    echo " [";
                    if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
                    echo $this->getAttribute($_vehicle_, "getSeats", array());
                    echo " ������]</option>  

                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vehicle'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 101
                echo "                    </select>
                    ";
                // line 102
                if (isset($context["createOffer"])) { $_createOffer_ = $context["createOffer"]; } else { $_createOffer_ = null; }
                echo $this->getAttribute($_createOffer_, "cost", array());
                echo " ��.
                    ";
                // line 103
                if (isset($context["createOffer"])) { $_createOffer_ = $context["createOffer"]; } else { $_createOffer_ = null; }
                echo $this->getAttribute($_createOffer_, "action", array());
                echo "
                    ";
                // line 104
                if (isset($context["createOffer"])) { $_createOffer_ = $context["createOffer"]; } else { $_createOffer_ = null; }
                echo $this->getAttribute($_createOffer_, "submit", array());
                echo "
                    ";
                // line 105
                if (isset($context["createOffer"])) { $_createOffer_ = $context["createOffer"]; } else { $_createOffer_ = null; }
                echo $this->getAttribute($_createOffer_, "end", array());
                echo "
                  </div>
                </div>
              </div>
              <div class=\"accordion-group widget-box\">
                <div class=\"accordion-heading\">
                  <div class=\"widget-title\"> <a data-parent=\"#collapse-group\" href=\"#collapseGTwo\" data-toggle=\"collapse\"> <span class=\"icon\"><i class=\"icon-circle-arrow-right\"></i></span>
                    <h5>������</h5>
                    </a> </div>
                </div>
                <div class=\"collapse accordion-body\" id=\"collapseGTwo\">
                  <div class=\"widget-content\"> Another is open </div>
                </div>
              </div>
            </div>
        ";
            } else {
                // line 121
                echo "        <hr> <b>���� ��� ��������� ������ �� ���� ���������</b>
        ";
            }
            // line 123
            echo "        </div>
        <hr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "order-template.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  269 => 123,  265 => 121,  245 => 105,  240 => 104,  235 => 103,  230 => 102,  227 => 101,  207 => 98,  202 => 97,  196 => 95,  191 => 94,  180 => 85,  176 => 84,  172 => 82,  162 => 80,  157 => 79,  146 => 70,  142 => 68,  138 => 66,  134 => 65,  123 => 58,  115 => 52,  111 => 50,  107 => 48,  104 => 47,  95 => 42,  87 => 38,  79 => 34,  71 => 30,  67 => 28,  60 => 25,  56 => 23,  53 => 22,  47 => 20,  24 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "order-template.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/order-template.html");
    }
}
