<?php

/* list-offers.html */
class __TwigTemplate_f8169a1d81a2b84ae57d491c5e2f3a1ff1361eda22087e442af20ea368ad1d0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html", "list-offers.html", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"widget-title\">
    <span class=\"icon\"> <i class=\"icon-list\"></i> </span>
    <h5> ������ �� �������� </h5>
</div>

<div class=\"widget-content\">
    <table class=\"table table-responsive table-bordered\">
        <thead>
            <th>N:</th>
            <th>����:</th>
            <th>��:</th>
            <th>��:</th>
            <th>���� 1: </th>
            <th>���� 2: </th>
            <th> ������ </th>
        </head>
    ";
        // line 20
        if (isset($context["offers"])) { $_offers_ = $context["offers"]; } else { $_offers_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_offers_);
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["offer"]) {
            // line 21
            echo "        ";
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            $this->loadTemplate("offer.html", "list-offers.html", 21)->display(array_merge($context, array("offer" => $_offer_)));
            // line 22
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['offer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "    </table>


";
    }

    public function getTemplateName()
    {
        return "list-offers.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 23,  71 => 22,  67 => 21,  49 => 20,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "list-offers.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/list-offers.html");
    }
}
