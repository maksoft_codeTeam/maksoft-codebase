<?php

/* vehicle-template.html */
class __TwigTemplate_3ea02975a3ddd83cc365601bbe1e8a5868b3aad9a001eb07c454631115df7e21 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "        <h6> N: ";
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "getId", array());
        echo " </h6>
        <hr>
        <div class=\"row-fluid\">
            <div class=\"span4\">
                <div class=\"widget-content nopadding\">
                    <table class=\"table table-bordered\">
                      <thead>
                      </thead>
                      <tbody>
                        <tr>
                          <td>N</td>
                          <td>";
        // line 12
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_vehicle_, "getId", array()), "html", null, true);
        echo "</td>
                        </tr>
                        <tr>
                          <td>���</td>
                          <td>";
        // line 16
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($this->getAttribute($_vehicle_, "getType", array()), "getName", array());
        echo "</td>
                        </tr>
                        <tr>
                          <td>�����</td>
                          <td>";
        // line 20
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($this->getAttribute($_vehicle_, "getMake", array()), "getName", array());
        echo "</td>
                        </tr>
                        <tr>
                          <td>�����</td>
                          <td class=\"center\"> ";
        // line 24
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "getModel", array());
        echo "</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
            </div>
            <div class=\"span4\">
            <h5>������: </h5>
            ";
        // line 32
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($_vehicle_, "getExtras", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["extra"]) {
            // line 33
            echo "                <span class=\"label label-info\">";
            if (isset($context["extra"])) { $_extra_ = $context["extra"]; } else { $_extra_ = null; }
            echo $this->getAttribute($_extra_, "getName", array());
            echo "</span> | 
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extra'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "
            </div>
        </div>
        <div class=\"row-fluid\">
            <div class=\"span12\">
                <ul class=\"thumbnails\">
                ";
        // line 41
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($_vehicle_, "getImages", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 42
            echo "                    <li class=\"span1\"> <a> <img src=\"";
            if (isset($context["image"])) { $_image_ = $context["image"]; } else { $_image_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_image_, "getPath", array()), "html", null, true);
            echo "\" alt=\"\"> </a>
                        <div class=\"actions\"> <a class=\"lightbox_trigger\" href=\"";
            // line 43
            if (isset($context["image"])) { $_image_ = $context["image"]; } else { $_image_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_image_, "getPath", array()), "html", null, true);
            echo "\"><i class=\"icon-search\"></i></a> </div>
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "                </ul>
            </div>
        </div>
        <div class=\"row-fluid\">
            <div class=\"span12\">
                <div class=\"accordion\" id=\"collapse-group\">
                  <div class=\"accordion-group widget-box\">
                    <div class=\"accordion-heading\">
                      <div class=\"widget-title\"> <a data-parent=\"#collapse-group\" href=\"#collapseGOne\" data-toggle=\"collapse\"> <span class=\"icon\"><i class=\"icon-ok\"></i></span>
                        <h5>���������</h5>
                        </a> </div>
                    </div>
                    <div class=\"collapse in accordion-body\" id=\"collapseGOne\">
                      <div class=\"widget-content\">
                        <button class=\"btn btn-primary\"> ���������� </button>
                        <button class=\"btn btn-danger\"> ������ </button>
                      </div>
                    </div>
                  </div>
            </div>
        <hr>
        </div>
";
    }

    public function getTemplateName()
    {
        return "vehicle-template.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 46,  105 => 43,  99 => 42,  94 => 41,  86 => 35,  76 => 33,  71 => 32,  59 => 24,  51 => 20,  43 => 16,  35 => 12,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "vehicle-template.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/vehicle-template.html");
    }
}
