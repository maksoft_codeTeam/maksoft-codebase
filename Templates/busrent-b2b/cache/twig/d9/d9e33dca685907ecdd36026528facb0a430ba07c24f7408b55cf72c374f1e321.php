<?php

/* top-header.html */
class __TwigTemplate_e19b6319d93685ef544b6388b7a1f3fa36798ecf2f4827052e8ba553335dc2c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Header-part-->
<div id=\"header\">
  <h1><a href=\"/\">Busrent</a></h1>
</div>
<!--close-Header-part--> 
<!--top-Header-menu-->
<div id=\"user-nav\" class=\"navbar navbar-inverse\">
  <ul class=\"nav\">
    <li  class=\"dropdown\" id=\"profile-messages\" ><a title=\"\" href=\"#\" data-toggle=\"dropdown\" data-target=\"#profile-messages\" class=\"dropdown-toggle\"><i class=\"icon icon-user\"></i>  <span class=\"text\">";
        // line 9
        if (isset($context["session"])) { $_session_ = $context["session"]; } else { $_session_ = null; }
        echo $this->getAttribute($_session_, "company", array());
        echo "</span><b class=\"caret\"></b></a>
      <ul class=\"dropdown-menu\">
        <li><a href=\"/?profile\"><i class=\"icon-user\"></i>������</a></li>
        <li class=\"divider\"></li>
        <li><a href=\"#\"><i class=\"icon-check\"></i> My Tasks</a></li>
        <li class=\"divider\"></li>
        <li>
            ";
        // line 16
        if (isset($context["logout"])) { $_logout_ = $context["logout"]; } else { $_logout_ = null; }
        echo $this->getAttribute($_logout_, "start", array());
        echo "
            ";
        // line 17
        if (isset($context["logout"])) { $_logout_ = $context["logout"]; } else { $_logout_ = null; }
        echo $this->getAttribute($_logout_, "action", array());
        echo "
            <button style=\"border:none; background:none!important;font:inherit;padding:0!important;cursor: pointer;\" type=\"submit\" name=\"submit\"><i class=\"icon icon-share-alt\"></i>  <span class=\"text\"> Logout</span></button>
            ";
        // line 19
        if (isset($context["logout"])) { $_logout_ = $context["logout"]; } else { $_logout_ = null; }
        echo $this->getAttribute($_logout_, "end", array());
        echo "
        </li>
      </ul>
    </li>
    <li class=\"dropdown\" id=\"menu-messages\"><a href=\"#\" data-toggle=\"dropdown\" data-target=\"#menu-messages\" class=\"dropdown-toggle\"><i class=\"icon icon-envelope\"></i> <span class=\"text\">Messages</span> <span class=\"label label-important\">5</span> <b class=\"caret\"></b></a>
      <ul class=\"dropdown-menu\">
        <li><a class=\"sAdd\" title=\"\" href=\"#\"><i class=\"icon-plus\"></i> new message</a></li>
        <li class=\"divider\"></li>
        <li><a class=\"sInbox\" title=\"\" href=\"#\"><i class=\"icon-envelope\"></i> inbox</a></li>
        <li class=\"divider\"></li>
        <li><a class=\"sOutbox\" title=\"\" href=\"#\"><i class=\"icon-arrow-up\"></i> outbox</a></li>
        <li class=\"divider\"></li>
        <li><a class=\"sTrash\" title=\"\" href=\"#\"><i class=\"icon-trash\"></i> trash</a></li>
      </ul>
    </li>
    <li class=\"\"><a title=\"\" href=\"#\"><i class=\"icon icon-cog\"></i> <span class=\"text\">Settings</span></a></li>
    <li class=\"\">
            ";
        // line 36
        if (isset($context["logout"])) { $_logout_ = $context["logout"]; } else { $_logout_ = null; }
        echo $this->getAttribute($_logout_, "start", array());
        echo "
            ";
        // line 37
        if (isset($context["logout"])) { $_logout_ = $context["logout"]; } else { $_logout_ = null; }
        echo $this->getAttribute($_logout_, "action", array());
        echo "
            <button style=\"border:none; background:none!important;font:inherit;padding:0!important;cursor: pointer;font-size:11px;color:#999;position:relative!important;\" type=\"submit\" name=\"submit\"><i class=\"icon icon-share-alt\"></i>  <span class=\"text\"> Logout</span></button>
            ";
        // line 39
        if (isset($context["logout"])) { $_logout_ = $context["logout"]; } else { $_logout_ = null; }
        echo $this->getAttribute($_logout_, "end", array());
        echo "
  </ul>
</div>
<!--close-top-Header-menu-->
";
    }

    public function getTemplateName()
    {
        return "top-header.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 39,  77 => 37,  72 => 36,  51 => 19,  45 => 17,  40 => 16,  29 => 9,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "top-header.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/top-header.html");
    }
}
