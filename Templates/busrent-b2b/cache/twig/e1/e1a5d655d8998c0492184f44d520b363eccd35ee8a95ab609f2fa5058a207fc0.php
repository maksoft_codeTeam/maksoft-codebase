<?php

/* vehicle-template2.html */
class __TwigTemplate_1362fb191d78399afa184266dbf80d62cae2cd2d73c2226312c692f7e9dce713 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"col-md-6 col-offset-1\" >
    <h6> N: ";
        // line 2
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "getId", array());
        echo " </h6>
    <hr>
    <div class=\"row\">
        <div class=\"col-md-3\">
            ";
        // line 6
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($_vehicle_, "getImages", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 7
            echo "                ";
            if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
            if ($this->getAttribute($_loop_, "first", array())) {
                // line 8
                echo "                    <div class=\"col-md-8\">
                        <img src=\"";
                // line 9
                if (isset($context["image"])) { $_image_ = $context["image"]; } else { $_image_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_image_, "getPath", array()), "html", null, true);
                echo "\" alt=\"\">
                    </div>
                ";
            } else {
                // line 12
                echo "                    <div class=\"col-md-4\">
                        <img src=\"";
                // line 13
                if (isset($context["image"])) { $_image_ = $context["image"]; } else { $_image_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_image_, "getPath", array()), "html", null, true);
                echo "\" alt=\"\">
                    </div>
                ";
            }
            // line 16
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "        </div>            
        <div class=\"col-md-9\">
            <div class=\"col-md-12\">
            <h4> <strong> ";
        // line 20
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($this->getAttribute($_vehicle_, "getType", array()), "getName", array());
        echo " ";
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($this->getAttribute($_vehicle_, "getMake", array()), "getName", array());
        echo "</strong> - ";
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "getModel", array());
        echo " </h4>
            <hr>
            </div>
            <div class=\"col-md-12\">
                ������ �� ������������: ";
        // line 24
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_vehicle_, "getManufacturedDate", array()), "Y"), "html", null, true);
        echo "
            </div>
            <div class=\"col-md-12\">
                ������: ";
        // line 27
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "getFuel", array());
        echo "
            </div>
            <div class=\"col-md-12\">
                ���� �����: <b>";
        // line 30
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_vehicle_, "getSeats", array()), "html", null, true);
        echo "</b>
            </div>
            <div class=\"col-md-12\">
                <h5>������: </h5>
                ";
        // line 34
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($_vehicle_, "getExtras", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["extra"]) {
            // line 35
            echo "                    <span class=\"label label-info\">";
            if (isset($context["extra"])) { $_extra_ = $context["extra"]; } else { $_extra_ = null; }
            echo $this->getAttribute($_extra_, "getName", array());
            echo "</span> | 
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extra'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "            </div>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"accordion\" id=\"collapse-group\">
              <div class=\"accordion-group widget-box\">
                <div class=\"accordion-heading\">
                  <div class=\"widget-title\"> <a data-parent=\"#collapse-group\" href=\"#collapseGOne\" data-toggle=\"collapse\">
                    <h5>��������</h5>
                    </a> </div>
                </div>
                <div class=\"collapse in accordion-body\" id=\"collapseGOne\">
                  <div class=\"widget-content\">
                    ";
        // line 51
        if (isset($context["delete"])) { $_delete_ = $context["delete"]; } else { $_delete_ = null; }
        echo $this->getAttribute($_delete_, "start", array());
        echo "
                    ";
        // line 52
        if (isset($context["delete"])) { $_delete_ = $context["delete"]; } else { $_delete_ = null; }
        echo $this->getAttribute($_delete_, "action", array());
        echo "
                    <input type='hidden' name='vehicle_id' value='";
        // line 53
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_vehicle_, "getId", array()), "html", null, true);
        echo "'>
                    ";
        // line 54
        if (isset($context["delete"])) { $_delete_ = $context["delete"]; } else { $_delete_ = null; }
        echo $this->getAttribute($_delete_, "submit", array());
        echo "
                    ";
        // line 55
        if (isset($context["delete"])) { $_delete_ = $context["delete"]; } else { $_delete_ = null; }
        echo $this->getAttribute($_delete_, "end", array());
        echo "
                  </div>
                </div>
              </div>
        </div>
    <hr>
    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "vehicle-template2.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 55,  173 => 54,  168 => 53,  163 => 52,  158 => 51,  142 => 37,  132 => 35,  127 => 34,  119 => 30,  112 => 27,  105 => 24,  91 => 20,  86 => 17,  72 => 16,  65 => 13,  62 => 12,  55 => 9,  52 => 8,  48 => 7,  30 => 6,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "vehicle-template2.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/vehicle-template2.html");
    }
}
