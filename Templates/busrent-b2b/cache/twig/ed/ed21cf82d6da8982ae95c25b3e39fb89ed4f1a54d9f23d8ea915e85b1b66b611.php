<?php

/* login2.html */
class __TwigTemplate_074673ec5900d8f61840f69bddfd9aefc9e44cf3b5bb35971a3f051d4748e1da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    
<head>
        <title>Matrix Admin</title><meta charset=\"cp1251\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
        <link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap.min.css\" />
        <link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap-responsive.min.css\" />
        <link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/matrix-login.css\" />
        <link href=\"Templates/busrent-b2b/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div id=\"loginbox\">            
            ";
        // line 15
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "start", array());
        echo "
                 <div class=\"control-group normal_text\"> <h3><img src=\"Templates/busrent/assets/images/logo-busrent.png\" alt=\"Logo\" /></h3></div>
                <div class=\"control-group\">
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-envelope\"> </i></span>";
        // line 20
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "email", array());
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"control-group\">
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_ly\"><i class=\"icon-lock\"></i></span>";
        // line 27
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "password", array());
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"form-actions\">
                    <span class=\"pull-left\"><a href=\"#\" class=\"flip-link btn btn-info\" id=\"to-recover\">�����������</a></span>
                    <span class=\"pull-right\">";
        // line 33
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "submit", array());
        echo "</span>
                </div>
            ";
        // line 35
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "action", array());
        echo "
            ";
        // line 36
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "end", array());
        echo "
            ";
        // line 37
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "start", array());
        echo "
                <p class=\"normal_text\">
                �������� �������� ������� � ��� ���������� �� �� ������ � ���.
                </p>
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span>";
        // line 43
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "name", array());
        echo "
                        </div>
                    </div>
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span>";
        // line 48
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "vat", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span>";
        // line 54
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "license", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span>";
        // line 60
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "owner", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span>";
        // line 66
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "city", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span>";
        // line 72
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "address", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span>";
        // line 78
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "phone", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span>";
        // line 84
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "email", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span>";
        // line 90
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "password", array());
        echo "
                        </div>
                    </div>
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span>";
        // line 95
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "password_repeated", array());
        echo "
                        </div>
                    </div>
                    ";
        // line 98
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "action", array());
        echo "
               
                <div class=\"form-actions\">
                    <span class=\"pull-left\"><a href=\"#\" class=\"flip-link btn btn-success\" id=\"to-login\">&laquo; �����</a></span>
                    <span class=\"pull-right\">";
        // line 102
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "submit", array());
        echo "</span>
                </div>
                ";
        // line 104
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "end", array());
        echo "
        </div>
        
        <script src=\"Templates/busrent-b2b/assets/js/jquery.min.js\"></script>  
        <script src=\"Templates/busrent-b2b/assets/js/matrix.login.js\"></script> 
    </body>

</html>

";
    }

    public function getTemplateName()
    {
        return "login2.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 104,  194 => 102,  186 => 98,  179 => 95,  170 => 90,  160 => 84,  150 => 78,  140 => 72,  130 => 66,  120 => 60,  110 => 54,  100 => 48,  91 => 43,  81 => 37,  76 => 36,  71 => 35,  65 => 33,  55 => 27,  44 => 20,  35 => 15,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "login2.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/login2.html");
    }
}
