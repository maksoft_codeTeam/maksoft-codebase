<?php

/* base2.html */
class __TwigTemplate_11aff1ea07bae63c4f97c547ea53ab1cc9690679c73c48f9d206a8db856b96b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
<title>Matrix Admin</title>
<meta charset=\"cp1251\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap.min.css\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap-responsive.min.css\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/fullcalendar.css\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/matrix-style.css\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/matrix-media.css\" />
<script src=\"https://use.fontawesome.com/4044117fc8.js\"></script>
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/jquery.gritter.css\" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

";
        // line 18
        $this->loadTemplate("top-header.html", "base2.html", 18)->display($context);
        // line 19
        echo "
";
        // line 20
        $this->loadTemplate("sidebar.html", "base2.html", 20)->display($context);
        // line 21
        echo "
<!--main-container-part-->
<div id=\"content\">
<!--breadcrumbs-->
  <div id=\"content-header\">
    <div id=\"breadcrumb\"> <a href=\"index.html\" title=\"Go to Home\" class=\"tip-bottom\"><i class=\"icon-home\"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

";
        // line 30
        $this->displayBlock('content', $context, $blocks);
        // line 33
        echo "
";
        // line 34
        $this->displayBlock('footer', $context, $blocks);
        // line 43
        echo "
<script src=\"Templates/busrent-b2b/assets/js/excanvas.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.ui.custom.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/bootstrap.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.flot.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.flot.resize.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.peity.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/fullcalendar.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.dashboard.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.gritter.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.interface.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.chat.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.validate.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.form_validation.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.wizard.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.uniform.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/select2.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.popover.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.dataTables.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.tables.js\"></script> 

<script type=\"text/javascript\">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != \"\") {
      
          // if url is \"-\", it is this page -- reset the menu:
          if (newURL == \"-\" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>

";
    }

    // line 30
    public function block_content($context, array $blocks = array())
    {
        // line 31
        echo "
";
    }

    // line 34
    public function block_footer($context, array $blocks = array())
    {
        echo " 

<!--Footer-part-->
<div class=\"row-fluid\">
  <div id=\"footer\" class=\"span12\"> 2013 &copy; Matrix Admin. Brought to you by <a href=\"http://themedesigner.in\">Themedesigner.in</a> </div>
</div>
<!--end-Footer-part-->

";
    }

    public function getTemplateName()
    {
        return "base2.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 34,  122 => 31,  119 => 30,  65 => 43,  63 => 34,  60 => 33,  58 => 30,  47 => 21,  45 => 20,  42 => 19,  40 => 18,  21 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base2.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/base2.html");
    }
}
