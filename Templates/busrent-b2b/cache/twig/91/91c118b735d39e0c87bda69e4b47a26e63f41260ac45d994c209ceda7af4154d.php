<?php

/* sidebar.html */
class __TwigTemplate_9cebab2bcc302cd7bd018c2337d8fb60e53cd08a3f525aa9eb1f88330d5f9379 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--sidebar-menu-->
<div id=\"sidebar\"><a href=\"/\" class=\"visible-phone\"><i class=\"icon icon-home\"></i> ����</a>
  <ul>
    <li class=\"active\"><a href=\"/\"><i class=\"icon icon-home\"></i> <span>������</span></a> </li>
    <li><a href=\"/?profile\"><i class=\"icon icon-user\"></i> <span>������</span></a> </li>
    <li> <a href=\"/?orders\"><i class=\"icon icon-signal\"></i> <span>����������</span></a> </li>
    <li> <a href=\"/?offers\"><i class=\"icon icon-inbox\"></i> <span>������</span></a> </li>
    <li class=\"submenu\"> <a href=\"#\"><i class=\"icon icon-th-list\"></i> <span>��������</span> <span class=\"label label-important\">3</span></a>
      <ul>
        <li><a href=\"?list-vehicles\">������</a></li>
        <li><a href=\"/?add-vehicle\">��������</a></li>
      </ul>
    </li>
    <li class=\"content\"> <span>�������</span>
      <div class=\"progress progress-mini progress-danger active progress-striped\">
        <div style=\"width: 77%;\" class=\"bar\"></div>
      </div>
      <span class=\"percent\">77%</span>
      <div class=\"stat\">21419.94 / 14000 MB</div>
    </li>
    <li class=\"content\"> <span>Disk Space Usage</span>
      <div class=\"progress progress-mini active progress-striped\">
        <div style=\"width: 87%;\" class=\"bar\"></div>
      </div>
      <span class=\"percent\">87%</span>
      <div class=\"stat\">604.44 / 4000 MB</div>
    </li>
  </ul>
</div>
<!--sidebar-menu-->
";
    }

    public function getTemplateName()
    {
        return "sidebar.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "sidebar.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/sidebar.html");
    }
}
