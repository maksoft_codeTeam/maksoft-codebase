<?php

/* login.html */
class __TwigTemplate_60ae62dd0bec322f7ee863feeab6425f2ad79c5880453b33bdc861cca0c7498a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    
<head>
        <title>Busrent</title><meta charset=\"cp1251\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
        <link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap.min.css\" />
        <link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap-responsive.min.css\" />
        <link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/matrix-login.css\" />
        <link href=\"Templates/busrent-b2b/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div id=\"loginbox\">            
            <div class=\"widget-box\">
                ";
        // line 16
        if (isset($context["messages"])) { $_messages_ = $context["messages"]; } else { $_messages_ = null; }
        if ($this->getAttribute($_messages_, "text", array())) {
            // line 17
            echo "                <div class=\"widget-content\">
                    <div class=\"alert alert-";
            // line 18
            if (isset($context["message"])) { $_message_ = $context["message"]; } else { $_message_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_message_, "code", array()), "html", null, true);
            echo "\">
                        <button class=\"close\" data-dismiss=\"alert\">x</button>
                        <p></p>";
            // line 20
            if (isset($context["messages"])) { $_messages_ = $context["messages"]; } else { $_messages_ = null; }
            echo $this->getAttribute($_messages_, "text", array());
            echo "</div>
                </div>
                ";
        }
        // line 23
        echo "            </div>
            ";
        // line 24
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "start", array());
        echo "
                 <div class=\"control-group normal_text\"> <h3><img src=\"Templates/busrent/assets/images/logo-busrent.png\" alt=\"Logo\" /></h3></div>
                <div class=\"control-group\">
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-envelope\"> </i></span>";
        // line 29
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "email", array());
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"control-group\">
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_ly\"><i class=\"icon-lock\"></i></span>";
        // line 36
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "password", array());
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"form-actions\">
                    <span class=\"pull-left\"><a href=\"#\" class=\"flip-link btn btn-info\" id=\"to-recover\">�����������</a></span>
                    <span class=\"pull-right\">";
        // line 42
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "submit", array());
        echo "</span>
                </div>
            ";
        // line 44
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "action", array());
        echo "
            ";
        // line 45
        if (isset($context["login"])) { $_login_ = $context["login"]; } else { $_login_ = null; }
        echo $this->getAttribute($_login_, "end", array());
        echo "
            ";
        // line 46
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "start", array());
        echo "
                <p class=\"normal_text\">
                �������� �������� ������� � ��� ���������� �� �� ������ � ���.
                </p>
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-user\"></i></span>";
        // line 52
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "name", array());
        echo "
                        </div>
                    </div>
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-envelope\"></i></span>";
        // line 57
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "vat", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-tasks\"></i></span>";
        // line 63
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "license", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-user-md\"></i></span>";
        // line 69
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "owner", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-globe\"></i></span>";
        // line 75
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "city", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-screenshot\"></i></span>";
        // line 81
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "address", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-phone\"></i></span>";
        // line 87
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "phone", array());
        echo "
                        </div>
                    </div>
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-envelope\"></i></span>";
        // line 92
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "email", array());
        echo "
                        </div>
                    </div>

                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_ly\"><i class=\"icon-lock\"></i></span>";
        // line 98
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "password", array());
        echo "
                        </div>
                    </div>
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_ly\"><i class=\"icon-lock\"></i></span>";
        // line 103
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "password_repeated", array());
        echo "
                        </div>
                    </div>
                    ";
        // line 106
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "action", array());
        echo "
               
                <div class=\"form-actions\">
                    <span class=\"pull-left\"><a href=\"#\" class=\"flip-link btn btn-success\" id=\"to-login\">&laquo; �����</a></span>
                    <span class=\"pull-right\">";
        // line 110
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "submit", array());
        echo "</span>
                </div>
                ";
        // line 112
        if (isset($context["register"])) { $_register_ = $context["register"]; } else { $_register_ = null; }
        echo $this->getAttribute($_register_, "end", array());
        echo "
        </div>
        
        <script src=\"Templates/busrent-b2b/assets/js/jquery.min.js\"></script>  
        <script src=\"Templates/busrent-b2b/assets/js/matrix.login.js\"></script> 
    </body>

</html>

";
    }

    public function getTemplateName()
    {
        return "login.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 112,  216 => 110,  208 => 106,  201 => 103,  192 => 98,  182 => 92,  173 => 87,  163 => 81,  153 => 75,  143 => 69,  133 => 63,  123 => 57,  114 => 52,  104 => 46,  99 => 45,  94 => 44,  88 => 42,  78 => 36,  67 => 29,  58 => 24,  55 => 23,  48 => 20,  42 => 18,  39 => 17,  36 => 16,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "login.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/login.html");
    }
}
