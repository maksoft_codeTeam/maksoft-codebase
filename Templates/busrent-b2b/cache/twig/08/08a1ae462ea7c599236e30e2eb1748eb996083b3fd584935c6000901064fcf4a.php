<?php

/* add-vehicle.html */
class __TwigTemplate_8f9930ed1616458bd4a899d65276be7e80025e076201e6a99ab2caf6c3593418 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html", "add-vehicle.html", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"container-fluid\">
    <div class=\"row-fluid\">
        <!-- START FORM -->
        <div class=\"span6\">
      <div class=\"widget-box\">
        <div class=\"widget-title\"> <span class=\"icon\"> <i class=\"icon-align-justify\"></i> </span>
          <h5>";
        // line 11
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "title", array());
        echo "</h5>
        </div>
        <div class=\"widget-content nopadding\">
            ";
        // line 14
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "start", array());
        echo "
            <div class=\"control-group\">
              <label class=\"control-label\">��� :</label>
              <div class=\"controls\">
                <select name=\"vehicle-type\" onclick=\"renderMakes()\" onchange=\"renderMakes()\">
                    <option value=\"3\">��������</option>
                    <option value=\"2\">A������</option>
                </select>
              </div>
            </div>
            <div class=\"control-group\">
              <label class=\"control-label\">����� :</label>
              <div class=\"controls\">
                <select id=\"make\" name=\"make\" onclick=\"renderModels(this.value)\"> </select>
              </div>
            </div>
            <div class=\"control-group\">
              <label class=\"control-label\">����� :</label>
              <div id=\"model-div\" class=\"controls\">
                <select id=\"model\" name=\"model\"> </select>
              </div>
            </div>
            <div class=\"control-group\">
              <label class=\"control-label\">";
        // line 37
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($this->getAttribute($_vehicle_, "seats", array()), "label", array());
        echo " :</label>
              <div class=\"controls\">
                 ";
        // line 39
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "seats", array());
        echo "
              </div>
            </div>
            <div class=\"control-group\">
              <label class=\"control-label\">������ :</label>
              <div class=\"controls\">
                ";
        // line 45
        if (isset($context["extras"])) { $_extras_ = $context["extras"]; } else { $_extras_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_extras_);
        foreach ($context['_seq'] as $context["category"] => $context["col"]) {
            // line 46
            echo "                <div class=\"span6\">
                    <h5> ";
            // line 47
            if (isset($context["category"])) { $_category_ = $context["category"]; } else { $_category_ = null; }
            echo $_category_;
            echo "</h5>
                    <div class=\"row\">
                    ";
            // line 49
            if (isset($context["col"])) { $_col_ = $context["col"]; } else { $_col_ = null; }
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($_col_);
            foreach ($context['_seq'] as $context["_key"] => $context["extra"]) {
                // line 50
                echo "                        <div class=\"span12 checkbox\">
                            <label>
                                <input style=\"width: 1em;height: 1em;\" name=\"extras[]\" type=\"checkbox\" value=\"";
                // line 52
                if (isset($context["extra"])) { $_extra_ = $context["extra"]; } else { $_extra_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_extra_, "getId", array()), "html", null, true);
                echo "\">";
                if (isset($context["extra"])) { $_extra_ = $context["extra"]; } else { $_extra_ = null; }
                echo $this->getAttribute($_extra_, "getName", array());
                echo "
                            </label>
                        </div>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extra'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "                </div>
            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['category'], $context['col'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "            </div>
            <div class=\"control-group\">
              <label class=\"control-label\">������:</label>
              <div class=\"controls\">
                ";
        // line 63
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "images", array());
        echo "
                ";
        // line 64
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "action", array());
        echo "
              </div>
            </div>
            <div class=\"form-actions\">
              <button type=\"submit\" class=\"btn btn-success\">Save</button>
            </div>
        </div>
      </div>
    </div>
        <!-- END FORM -->

    ";
        // line 75
        if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
        echo $this->getAttribute($_vehicle_, "end", array());
        echo "
</div>

";
    }

    // line 80
    public function block_script($context, array $blocks = array())
    {
        // line 81
        echo "<script>
function renderMakes()
{
    var type = \$(\"select[name=vehicle-type]\").val()
    var url = \"/Templates/busrent-b2b/api.php?action=make&type=\" + type
    \$.getJSON(url, function(result) {
        var options = \$(\"#make\");
        //don't forget error handling!
        options.find('option')
               .remove()
               .end();
        var first = 0;
        \$.each(result, function(id, name) {
            if(first == 0){
                first = id;
            }
            options.append(\$(\"<option />\").val(id).text(name));
        });
        renderModels(first);
    });

}

function renderModels(model)
{
    var url = \"/Templates/busrent-b2b/api.php?action=model&make=\" + model
    \$.getJSON(url, function(result) {
        if(!result || result.length == 0 || result.code == \"error\"){
            \$(\"#model-div\").html('<input name=\"model\" type=\"text\" placeholder=\"��� �� ������: \">');
        } else {
            \$(\"#model-div\").html('<select id=\"model\" name=\"model\"></select>');
            var options = \$(\"#model\");
            //don't forget error handling!
            options.find('option')
                   .remove()
                   .end();
            \$.each(result, function(id, name) {
                options.append(\$(\"<option />\").val(id).text(name));
            });
        }
    });
}


renderMakes();
</script>

";
    }

    public function getTemplateName()
    {
        return "add-vehicle.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 81,  170 => 80,  161 => 75,  146 => 64,  141 => 63,  135 => 59,  127 => 56,  113 => 52,  109 => 50,  104 => 49,  98 => 47,  95 => 46,  90 => 45,  80 => 39,  74 => 37,  47 => 14,  40 => 11,  32 => 5,  29 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "add-vehicle.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/add-vehicle.html");
    }
}
