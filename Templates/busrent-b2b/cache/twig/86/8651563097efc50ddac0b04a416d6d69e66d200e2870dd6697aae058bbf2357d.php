<?php

/* base.html */
class __TwigTemplate_491e5ce97bd0d6dd3e25dd357eed28c1d616b9440b0b15b9fb14d8a2014d598f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"bg\">
<head>
<title>BUSRent �������������</title>
<meta charset=\"cp1251\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap.min.css\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/bootstrap-responsive.min.css\" />
<!-- Latest compiled and minified CSS -->
<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/fullcalendar.css\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/matrix-style.css\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/matrix-media.css\" />
<link href=\"Templates/busrent-b2b/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />
<link rel=\"stylesheet\" href=\"Templates/busrent-b2b/assets/css/jquery.gritter.css\" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
";
        // line 17
        $this->displayBlock('style', $context, $blocks);
        // line 20
        echo "</head>
<body>

";
        // line 23
        $this->loadTemplate("top-header.html", "base.html", 23)->display($context);
        // line 24
        echo "
";
        // line 25
        $this->loadTemplate("sidebar.html", "base.html", 25)->display($context);
        // line 26
        echo "
<!--main-container-part-->
<div id=\"content\">
<!--breadcrumbs-->
  <div id=\"content-header\">
    <div id=\"breadcrumb\"> <a href=\"/\" title=\"������\" class=\"tip-bottom\"><i class=\"icon-home\"></i> ������</a></div>
  </div>
<!--End-breadcrumbs-->

";
        // line 35
        $this->displayBlock('content', $context, $blocks);
        // line 38
        echo "
";
        // line 39
        $this->displayBlock('footer', $context, $blocks);
        // line 48
        echo "
<script src=\"Templates/busrent-b2b/assets/js/excanvas.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.ui.custom.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/bootstrap.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.flot.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.flot.resize.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.peity.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/fullcalendar.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.dashboard.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.gritter.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.interface.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.chat.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.validate.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.form_validation.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.wizard.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.uniform.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/select2.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.popover.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/jquery.dataTables.min.js\"></script> 
<script src=\"Templates/busrent-b2b/assets/js/matrix.tables.js\"></script> 
<script src=\"https://use.fontawesome.com/4044117fc8.js\"></script>
";
        // line 71
        $this->displayBlock('script', $context, $blocks);
        // line 74
        echo "
<script type=\"text/javascript\">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != \"\") {
      
          // if url is \"-\", it is this page -- reset the menu:
          if (newURL == \"-\" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>

";
    }

    // line 17
    public function block_style($context, array $blocks = array())
    {
        // line 18
        echo "
";
    }

    // line 35
    public function block_content($context, array $blocks = array())
    {
        // line 36
        echo "
";
    }

    // line 39
    public function block_footer($context, array $blocks = array())
    {
        echo " 

<!--Footer-part-->
<div class=\"row-fluid\">
  <div id=\"footer\" class=\"span12\">";
        // line 43
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " &copy; Busrent Admin. </div>
</div>
<!--end-Footer-part-->

";
    }

    // line 71
    public function block_script($context, array $blocks = array())
    {
        // line 72
        echo "
";
    }

    public function getTemplateName()
    {
        return "base.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 72,  165 => 71,  156 => 43,  148 => 39,  143 => 36,  140 => 35,  135 => 18,  132 => 17,  100 => 74,  98 => 71,  73 => 48,  71 => 39,  68 => 38,  66 => 35,  55 => 26,  53 => 25,  50 => 24,  48 => 23,  43 => 20,  41 => 17,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/base.html");
    }
}
