<?php

/* sidebar.html */
class __TwigTemplate_7b1be9d6710d7eae7595194a3a1a7d9cf3da68efa2808c413146e716dd9a2591 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--sidebar-menu-->
<div id=\"sidebar\"><a href=\"#\" class=\"visible-phone\"><i class=\"icon icon-home\"></i> Dashboard</a>
  <ul>
    <li class=\"active\"><a href=\"/?profile\"><i class=\"icon icon-home\"></i> <span>������</span></a> </li>
    <li> <a href=\"#\"><i class=\"icon icon-signal\"></i> <span>����������</span></a> </li>
    <li> <a href=\"widgets.html\"><i class=\"icon icon-inbox\"></i> <span>�������</span></a> </li>
    <li><a href=\"tables.html\"><i class=\"icon icon-th\"></i> <span>������ � �������</span></a></li>
    <li><a href=\"grid.html\"><i class=\"icon icon-fullscreen\"></i> <span>Full width</span></a></li>
    <li class=\"submenu\"> <a href=\"#\"><i class=\"icon icon-th-list\"></i> <span>��������</span> <span class=\"label label-important\">3</span></a>
      <ul>
        <li><a href=\"#\">�������� ��������</a></li>
        <li><a href=\"/?add-vehicle\">��������</a></li>
        <li><a href=\"#\">Form with Wizard</a></li>
      </ul>
    </li>
    <li><a href=\"buttons.html\"><i class=\"icon icon-tint\"></i> <span>Buttons &amp; icons</span></a></li>
    <li><a href=\"interface.html\"><i class=\"icon icon-pencil\"></i> <span>Eelements</span></a></li>
    <li class=\"submenu\"> <a href=\"#\"><i class=\"icon icon-file\"></i> <span>Addons</span> <span class=\"label label-important\">5</span></a>
      <ul>
        <li><a href=\"index2.html\">Dashboard2</a></li>
        <li><a href=\"gallery.html\">Gallery</a></li>
        <li><a href=\"calendar.html\">Calendar</a></li>
        <li><a href=\"invoice.html\">Invoice</a></li>
        <li><a href=\"chat.html\">Chat option</a></li>
      </ul>
    </li>
    <li class=\"submenu\"> <a href=\"#\"><i class=\"icon icon-info-sign\"></i> <span>Error</span> <span class=\"label label-important\">4</span></a>
      <ul>
        <li><a href=\"error403.html\">Error 403</a></li>
        <li><a href=\"error404.html\">Error 404</a></li>
        <li><a href=\"error405.html\">Error 405</a></li>
        <li><a href=\"error500.html\">Error 500</a></li>
      </ul>
    </li>
    <li class=\"content\"> <span>Monthly Bandwidth Transfer</span>
      <div class=\"progress progress-mini progress-danger active progress-striped\">
        <div style=\"width: 77%;\" class=\"bar\"></div>
      </div>
      <span class=\"percent\">77%</span>
      <div class=\"stat\">21419.94 / 14000 MB</div>
    </li>
    <li class=\"content\"> <span>Disk Space Usage</span>
      <div class=\"progress progress-mini active progress-striped\">
        <div style=\"width: 87%;\" class=\"bar\"></div>
      </div>
      <span class=\"percent\">87%</span>
      <div class=\"stat\">604.44 / 4000 MB</div>
    </li>
  </ul>
</div>
<!--sidebar-menu-->
";
    }

    public function getTemplateName()
    {
        return "sidebar.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "sidebar.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/sidebar.html");
    }
}
