<?php

/* offer.html */
class __TwigTemplate_7c73bf88f0244181b4c68c5eb9705a56c2cf5b8bf6c25806f4520e40178d4407 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<tr>
    <td>";
        // line 2
        if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_offer_, "getId", array()), "html", null, true);
        echo "</td>

    <td>
        <span class=\"label label-info\"> ";
        // line 5
        if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
        echo $this->getAttribute($_offer_, "getCost", array());
        echo " ��.</span>
    
    </td>
    
    <td>";
        // line 9
        if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
        echo $this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getFromCity", array());
        echo "</td>
    <td>";
        // line 10
        if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
        echo $this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getDestination", array());
        echo "</td>
    ";
        // line 11
        if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
        if (($this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getTwoWay", array()) == 1)) {
            // line 12
            echo "    <td>";
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            echo $this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getDeparature", array());
            echo "</td>
    ";
        } else {
            // line 14
            echo "    <td> Na </td>
    ";
        }
        // line 16
        echo "    <td>";
        if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
        echo $this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getArrival", array());
        echo "</td>
    <td>
        ";
        // line 18
        if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
        if (($this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getTwoWay", array()) == 0)) {
            // line 19
            echo "            <span class=\"label label-important\">�����������</span>
        ";
        } else {
            // line 21
            echo "            <span class=\"label label-success\">����������</span>
        ";
        }
        // line 23
        echo "        |
        ";
        // line 24
        if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
        if (($this->getAttribute($_offer_, "getStatus", array()) == 1)) {
            // line 25
            echo "        <span class=\"label label-success\">������</span>
        ";
        } else {
            // line 27
            echo "        <span class=\"label label-important\">��������</span>
        ";
        }
        // line 29
        echo "        |
        ";
        // line 30
        if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
        if ($this->getAttribute($_offer_, "getStatus", array())) {
            // line 31
            echo "        <span><b>";
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            echo $this->getAttribute($this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getClient", array()), "getName", array());
            echo " ";
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            echo $this->getAttribute($this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getClient", array()), "getLastName", array());
            echo "</b> |</span>
                <span class=\"label label-info\"><i class=\"icon-phone\"></i> <a style=\"color: white;\" href=\"tel:";
            // line 32
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            echo $this->getAttribute($this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getClient", array()), "getPhone", array());
            echo "\">";
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            echo $this->getAttribute($this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getClient", array()), "getPhone", array());
            echo "</a></span> | <span class=\"label label-info\"><i class=\"icon-envelope\"></i> <a style=\"color:white\" href=\"mailto:";
            if (isset($context["offer"])) { $_offer_ = $context["offer"]; } else { $_offer_ = null; }
            echo $this->getAttribute($this->getAttribute($this->getAttribute($_offer_, "getOrder", array()), "getClient", array()), "getEmail", array());
            echo "?Subject=������� �� ���� ���������\" target=\"_top\"> ����� </a></span>
        ";
        }
        // line 34
        echo "    </td>
</tr>
";
    }

    public function getTemplateName()
    {
        return "offer.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 34,  108 => 32,  99 => 31,  96 => 30,  93 => 29,  89 => 27,  85 => 25,  82 => 24,  79 => 23,  75 => 21,  71 => 19,  68 => 18,  61 => 16,  57 => 14,  50 => 12,  47 => 11,  42 => 10,  37 => 9,  29 => 5,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "offer.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/offer.html");
    }
}
