<?php

/* index.html */
class __TwigTemplate_bc52f21c8a8092955fd4190ac0932fac3917206798f6d3fdf3f80fad78782d7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html", "index.html", 1);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_style($context, array $blocks = array())
    {
        // line 3
        echo "<link rel=\"stylesheet\" href=\"Templates/maksoft/v2/assets/fullcalendar/fullcalendar.css\">
<link rel=\"stylesheet\" href=\"Templates/maksoft/v2/assets/fullcalendar/scheduler.css\">
<link rel=\"stylesheet\" href=\"Templates/maksoft/v2/assets/fullcalendar/fullcalendar.print.css\" media=\"print\">
";
    }

    // line 8
    public function block_content($context, array $blocks = array())
    {
        // line 9
        echo "<!--Action boxes-->
  <div class=\"container-fluid\">
    <div class=\"quick-actions_homepage\">
      <ul class=\"quick-actions\">
        <li class=\"bg_lb\"> <a href=\"/?profile\"> <i class=\"icon-user\"></i> ������ </a> </li>
        <li class=\"bg_lg span3\"> <a href=\"/?orders\"> <i class=\"icon-tasks\"></i>
                <span class=\"label label-important\">";
        // line 15
        if (isset($context["orders"])) { $_orders_ = $context["orders"]; } else { $_orders_ = null; }
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $_orders_), "html", null, true);
        echo "</span> ����������</a> </li>
        <li class=\"bg_ly\"> <a href=\"/?offers\"> <i class=\"fa fa-first-order\" style=\"margin-bottom: 5%;display:block;font-size: 2em;\" aria-hidden=\"true\"></i><span class=\"label label-success\">";
        // line 16
        if (isset($context["company"])) { $_company_ = $context["company"]; } else { $_company_ = null; }
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute($_company_, "getOffers", array())), "html", null, true);
        echo "</span> ����� ������</a> </li>
        <li class=\"bg_lo\"> <a href=\"/?feedback\"> <i class=\"icon-comments\"></i> ������</a> </li>
        <li class=\"bg_ls\"> <a href=\"/?list-vehicles\"> 
            <i class=\"fa fa-bus\" style=\"margin-bottom: 5%;display:block;font-size: 2em;\" aria-hidden=\"true\"></i><span class=\"label label-important\">";
        // line 19
        if (isset($context["vehicles"])) { $_vehicles_ = $context["vehicles"]; } else { $_vehicles_ = null; }
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $_vehicles_), "html", null, true);
        echo "</span> ��������</a> </li>
        <li class=\"bg_lo span3\"> <a href=\"/?add-vehicle\"> <i class=\"icon-plus-sign\"></i> ������ �������� ��������</a> </li>
      </ul>
    </div>
    <div class=\"row-fluid\">
        <div id=\"calendar\" class=\"fc\"></div>
    </div>
  </div>
  <div class=\"row-fluid\">
      <div class=\"span6\">
        <div class=\"widget-box\">
          <div class=\"widget-title bg_ly\" data-toggle=\"collapse\" href=\"#collapseG2\"><span class=\"icon\"><i class=\"icon-chevron-down\"></i></span>
            <h5>�������� ��������</h5>
          </div>
          <div class=\"widget-content nopadding collapse in\" id=\"collapseG2\">
            <ul class=\"recent-posts\">
                ";
        // line 35
        if (isset($context["vehicles"])) { $_vehicles_ = $context["vehicles"]; } else { $_vehicles_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_vehicles_);
        foreach ($context['_seq'] as $context["_key"] => $context["vehicle"]) {
            // line 36
            echo "              <li>
                <div class=\"user-thumb\"> 
                    ";
            // line 38
            if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute($_vehicle_, "getImages", array()), 0, 1));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 39
                echo "                    <img width=\"40\" height=\"40\" alt=\"User\" src=\"";
                if (isset($context["image"])) { $_image_ = $context["image"]; } else { $_image_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_image_, "getPath", array()), "html", null, true);
                echo "\"> 
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 41
            echo "                </div>
                <div class=\"article-post\"><b> <span class=\"user-info\"> ";
            // line 42
            if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
            echo $this->getAttribute($this->getAttribute($_vehicle_, "getMake", array()), "getName", array());
            echo " ";
            if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
            echo $this->getAttribute($this->getAttribute($_vehicle_, "getModel", array()), "getname", array());
            echo " - ";
            if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_vehicle_, "getSeats", array()), "html", null, true);
            echo " ������</span></b>
                    <p></p>
                    ";
            // line 44
            if (isset($context["vehicle"])) { $_vehicle_ = $context["vehicle"]; } else { $_vehicle_ = null; }
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($_vehicle_, "getExtras", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["extra"]) {
                // line 45
                echo "                        <span class=\"label label-info\">";
                if (isset($context["extra"])) { $_extra_ = $context["extra"]; } else { $_extra_ = null; }
                echo $this->getAttribute($_extra_, "getName", array());
                echo "</span> 
                        ";
                // line 46
                if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
                if ( !$this->getAttribute($_loop_, "last", array())) {
                    // line 47
                    echo "                            |
                        ";
                }
                // line 48
                echo "  
                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extra'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "                </div>
              </li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vehicle'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "            </ul>
          </div>
        </div>
      </div>
      <div class=\"span6\">
        <div class=\"widget-box\">
          <div class=\"widget-title\"> <span class=\"icon\"><i class=\"icon-ok\"></i></span>
            <h5>Progress Box</h5>
          </div>
          <div class=\"widget-content\">
            <ul class=\"unstyled\">
              <li> <span class=\"icon24 icomoon-icon-arrow-up-2 green\"></span> 81% Clicks <span class=\"pull-right strong\">567</span>
                <div class=\"progress progress-striped \">
                  <div style=\"width: 81%;\" class=\"bar\"></div>
                </div>
              </li>
            </ul>
          </div>
        </div>
    </div>
        <div class=\"widget-box\">
          <div class=\"widget-title bg_lo\" data-toggle=\"collapse\" href=\"#collapseG3\"> <span class=\"icon\"> <i class=\"icon-chevron-down\"></i> </span>
            <h5>News updates</h5>
          </div>
          <div class=\"widget-content nopadding updates collapse in\" id=\"collapseG3\">
            <div class=\"new-update clearfix\"><i class=\"icon-ok-sign\"></i>
              <div class=\"update-done\"><a title=\"\" href=\"#\"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</strong></a> <span>dolor sit amet, consectetur adipiscing eli</span> </div>
              <div class=\"update-date\"><span class=\"update-day\">20</span>jan</div>
            </div>
            <div class=\"new-update clearfix\"> <i class=\"icon-gift\"></i> <span class=\"update-notice\"> <a title=\"\" href=\"#\"><strong>Congratulation Maruti, Happy Birthday </strong></a> <span>many many happy returns of the day</span> </span> <span class=\"update-date\"><span class=\"update-day\">11</span>jan</span> </div>
            <div class=\"new-update clearfix\"> <i class=\"icon-move\"></i> <span class=\"update-alert\"> <a title=\"\" href=\"#\"><strong>Maruti is a Responsive Admin theme</strong></a> <span>But already everything was solved. It will ...</span> </span> <span class=\"update-date\"><span class=\"update-day\">07</span>Jan</span> </div>
            <div class=\"new-update clearfix\"> <i class=\"icon-leaf\"></i> <span class=\"update-done\"> <a title=\"\" href=\"#\"><strong>Envato approved Maruti Admin template</strong></a> <span>i am very happy to approved by TF</span> </span> <span class=\"update-date\"><span class=\"update-day\">05</span>jan</span> </div>
            <div class=\"new-update clearfix\"> <i class=\"icon-question-sign\"></i> <span class=\"update-notice\"> <a title=\"\" href=\"#\"><strong>I am alwayse here if you have any question</strong></a> <span>we glad that you choose our template</span> </span> <span class=\"update-date\"><span class=\"update-day\">01</span>jan</span> </div>
          </div>
        </div>
      </div>
";
    }

    // line 91
    public function block_script($context, array $blocks = array())
    {
        // line 92
        echo "<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/lib/jquery.min.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/lib/jquery-ui.min.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/lib/moment.min.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/locale/bg.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/fullcalendar.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/fullcalendar/gcal.js\"></script>
<script  type=\"text/javascript\" src=\"Templates/maksoft/v2/assets/js/tablesorter.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 92,  216 => 91,  176 => 53,  168 => 50,  153 => 48,  149 => 47,  146 => 46,  140 => 45,  122 => 44,  110 => 42,  107 => 41,  97 => 39,  92 => 38,  88 => 36,  83 => 35,  63 => 19,  56 => 16,  51 => 15,  43 => 9,  40 => 8,  33 => 3,  30 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "index.html", "/hosting/maksoft/maksoft/Templates/busrent-b2b/template/index.html");
    }
}
