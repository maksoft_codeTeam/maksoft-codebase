<?php
require_once "/hosting/maksoft/maksoft/Templates/busrent/bus_rent/vendor/autoload.php";
require_once __DIR__.'/config.php';
$settings = new \Maksoft\Rent\Bus\Settings\Bootstrap();
$settings->setMode("production");
$settings->setCacheDir("/hosting/maksoft/maksoft/Templates/busrent-b2b/cache/twig");
$settings->setTwigTemplateDir('/hosting/maksoft/maksoft/Templates/busrent-b2b/template');
$settings->enableAutoReload();
$settings->initialize();
$twig = $settings->getTwig();
$em = $entityManager = $settings->getEntityManager();
switch ($_GET['action']){
    case "make":
        if(isset($_GET['type']) and is_numeric($_GET['type'])){
            try{
                $makeRepository =  $em->getRepository('\Maksoft\Rent\Bus\Make');
                $makes = $makeRepository->findByType($_GET['type']);
                $tmp = array();
                foreach($makes as $make){
                    $tmp[$make->getId()] = $make->getName();
                }
                exit(json_encode($tmp));
            } catch( \Exception $e){
                exit(array("code"=>"error"));
            }
        }
    break;
    case "model":
        if(isset($_GET['make']) and is_numeric($_GET['make'])){
            try{
                $modelRepository =  $em->getRepository('\Maksoft\Rent\Bus\Model');
                $models = $modelRepository->findBy(array('make' => $_GET['make']));
                $tmp = array();
                foreach($models as $model){
                    $tmp[$model->getName()] = $model->getName();
                }
                exit(json_encode($tmp));
            } catch( \Exception $e){
                exit(json_encode(array("code"=>"error")));
            }
        }
        break;
    case "distance":
        if(isset($_GET['origin'], $_GET['destination'])){
            $url = array();
            $url['units'] = 'metric';
            $url['origins'] = $_GET['origin'];
            $url['destinations'] = $_GET['destination'];
            $url['key'] = "AIzaSyDX6iHHXUbcJjQi961C5jjWTD6nyU2jc3Q";
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?".http_build_query($url);

            exit(file_get_contents($url));
        }
    default:
        exit(json_encode(array("code"=>"error")));
}

exit(json_encode(array("code"=>"error")));
?>
