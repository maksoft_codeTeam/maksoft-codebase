<div id="sidebar">
	<div id="languages">
    <?php $o_site->print_sVersions()?>
    </div>
	<?php print_menu($o_page->get_pSubpages($o_site->StartPage), array("params"=>"id=\"navigation\""));?>
    <div class="location">
 	<?php
    	include TEMPLATE_DIR . "boxes/box_location.php";
	?>
    </div>
    <br>
    <div class="polls">
 	<?php
    	include TEMPLATE_DIR . "boxes/box_poll.php";
	?>
    </div>
    <br>
	<div class="gallery">
 	<?php
    	include TEMPLATE_DIR . "boxes/box_gallery.php";
	?>
    </div>
</div>