<script type="text/javascript" src="http://www.maksoft.net/lib/jquery/easyslider/easySlider1.7.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR;?>/scripts/bjqs-1.3.min.js"></script>
<?php
	//Home box LEFT
	$box_left_items = $o_page->get_pGroupContent($n_home_group_left);
	shuffle($box_left_items);
	$left_item = $box_left_items[0];
	
	//Home box RIGHT
	$box_right_items = $o_page->get_pGroupContent($n_home_group_right);
	shuffle($box_right_items);
	$right_item = $box_right_items[0];
	
	//home page post
	$home_page_post_n = $o_page->_site['news'];

	//get weather
	$wwo->get_weather();
	$today = getdate(); 
	$sun_info = date_sun_info(time(), $wwo->latitude, $wwo->longitude);
	$sunrise = getdate($sun_info['sunrise']);
	$sunset = getdate($sun_info['sunset']);
	
	//page famous persons
	$famous_persons = $o_page->get_pSubpages($n_famous_persons);
	shuffle($famous_persons);
		
	//page comments
	$comments = $o_site->get_sComments();
?>
		<div id="main">
			<div class="block first">
				<h5><?=$left_item['Name']?></h5>
				<p><?=cut_text(strip_tags($left_item['textStr']),180)?></p>
                <p><br><a href="<?=$o_page->get_pLink($left_item['n'])?>" class="link-more"><?=$o_site->get_sMoreText()?></a></p>
			</div>
			<div class="block">
				<h5><?=$right_item['Name']?></h5>
				<p><?=cut_text(strip_tags($right_item['textStr']), 180)?></p>
                <p><br><a href="<?=$o_page->get_pLink($right_item['n'])?>" class="link-more"><?=$o_site->get_sMoreText()?></a></p>
			</div>
			<div class="clearboth"></div>
			<!-- <div id="clickTicker">
					<?php
						$pageTicker = $o_page->get_page($n_pageticker);
						echo $pageTicker["textStr"];
					?>
			</div> -->
				
				<div id="sliders">
				  <a href="" class="control_next">>></a>
				  <a href="" class="control_prev"><<</a>
				  
					<?php
						$pageTicker = $o_page->get_page($n_pageticker);
						echo $pageTicker["textStr"];
					?>
				</div>
				<script>
				jQuery(document).ready(function($) {
					setInterval(function() {
						moveRight();
					}, 3000);
				  var slideCount = $('#sliders ul li').length;
				  var slideWidth = $('#sliders ul li').width();
				  var slideHeight = $('#sliders ul li').height();
				  var sliderUlWidth = slideCount * slideWidth;

				  $('#sliders').css({
					width: slideWidth,
					height: slideHeight
				  });

				  $('#sliders ul').css({
					width: sliderUlWidth,
					marginLeft: -slideWidth
				  });

				  $('#sliders ul li:last-child').prependTo('#sliders ul');

				  function moveLeft() {
					$('#sliders ul').animate({
					  left: +slideWidth
					}, 200, function() {
					  $('#sliders ul li:last-child').prependTo('#sliders ul');
					  $('#sliders ul').css('left', '');
					});
				  };

				  function moveRight() {
					$('#sliders ul').animate({
					  left: -slideWidth
					}, 200, function() {
					  $('#sliders ul li:first-child').appendTo('#sliders ul');
					  $('#sliders ul').css('left', '');
					});
				  };

				  $('a.control_prev').click(function(event) {
						event.preventDefault();
						moveLeft();
				  });

				  $('a.control_next').click(function(event) {
						event.preventDefault();
						moveRight();
				  });

				});
				</script>

			<div class="seeall">
				<a href="https://julian1951.wordpress.com" target="_blank">
					��� ������...
				</a>
			</div>
				<?php
					$loc_array=array(
						$wwo->weather->latitude,
						$wwo->weather->longitude
					);
					$marine=$wwo->get_marine($loc_array);
					$watertemp=$marine->weather->hourly->waterTemp_C;
				?>
			
			<script>
			$j(document).ready(function(){
				var now=parseInt(<?=$wwo->weather->current_condition->temp_C?>);
				var max=parseInt(<?=$wwo->weather->weather->tempMaxC?>);
				var min=parseInt(<?=$wwo->weather->weather->tempMinC?>);
				
				function init(){
					
					$j(".texttobechanged").html("������������� � �������");
					$j(".tochangecolor").attr("style", "transition:5s; -webkit-transition:5s; -moz-transition:5s;");
					$j(".tochangecolor").attr("style", "color:#EA4700;");
					$j({someValue: now}).animate({someValue: max}, {
						duration: 5000,
						easing:'swing', // can be anything
						step: function() { // called on every step
							// Update the element's text with rounded-up value:
							$j('#temperature').text(commaSeparateNumber(Math.round(this.someValue)));
						},
						complete: function(){
							$j(".texttobechanged").html("������������ ����������� ����");
							setTimeout(function(){
									$j(".tochangecolor").attr("style", "transition:0s; -moz-transition:0s;");
									setTimeout(function(){
										$j(".tochangecolor").attr("style", "color:#797979;");
										init();
									}, 1000);
							}, 6000);
						}
				  });
				}
				
				init();

				 function commaSeparateNumber(val){
					while (/(\d+)(\d{3})/.test(val.toString())){
					  val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
					}
					return val;
				  }
				  
			});
			</script>
			<section class="rw-wrapper">
				<h2 class="rw-sentence"><?=$o_page->get_pName($home_page_post_n)?></h2>
				<div class="weather-info">
					<div class="tochangecolor texttobechanged"><?=LABEL_TEMPNOW;?></div>
					<div class="temp tochangecolor"><strong id="temperature"><?=$wwo->weather->current_condition->temp_C?></strong>  C&deg; <br><br><small><?=LABEL_AIR?></small></div>
					<div class="temp watter"><strong><?=$watertemp?></strong>  C&deg; <br><br><small><?=LABEL_WATER?></small></div>
				</div>
					<div class="rw-words rw-words-1">
						<span><?=LABEL_WEATHERNOW;?> <strong><?=$wwo->get_condition($wwo->weather->current_condition->weatherCode)?></strong></span>
						<span><?=LABEL_TEMPNOWNOCAP;?> <strong><?=$wwo->weather->current_condition->temp_C?> C&deg;</strong></span>
						<span><?=LABEL_MAXTEMPTODAY;?> <strong><?=$wwo->weather->weather->tempMaxC?> C&deg;</strong></span>
						<span><?=LABEL_MINTEMPTODAY;?> <strong><?=$wwo->weather->weather->tempMinC?> C&deg;</strong></span>
						<span><?=LABEL_WINDSPEEDNOW;?> <strong><?=$wwo->weather->current_condition->windspeedKmph;?> km/h</strong></span>
						<span><?=LABEL_TEMPWATERTODAY;?> <strong><?=$watertemp;?> C&deg;</strong></span>
					</div>
			</section>
			<!-- -->
			<div class="post">
				<?php
                	$o_page->print_pImage(580, "", "", $home_page_post_n);
					$wwo->parse(); 
					//$o_page->print_pContent(); 
				?>
                <br />
				<?php 
				 $h_pages = $o_page->get_pGroupContent( $home_group_id); 
				 foreach($h_pages as $h_page) {
				  echo('
							  	<p class="style1">'.$o_page->get_pTitle("strict", $h_page['n']).'</p><br>
				  				<p>'.crop_text($o_page->get_pText($h_page['n'])).'</p>
								<p class="more"><a href="'.$o_page->get_pLink($h_page['n']).'"class="link-more"><'.$o_site->get_sMoreText().'></a></p>
					'); 
				 }
				?>
			</div>
			<div class="small_post style2">
				<h3><?=$o_page->get_pName($n_famous_persons)?> </h3>
				<div class="about" id="slider">
                	<ul class="bjqs">
					<?php
						for($i=0; $i<count($famous_persons); $i++)
						//for($i=0; $i<=1; $i++)
							{
								$title=$o_page->get_pName($famous_persons[$i]['n']);
								?>
								<li>
								<?php $o_page->print_pImage(500, "title=\"$title\"", "", $famous_persons[$i]['n'])?>
								<h4><?=$title?></h4>
								<!--<a href="<?=$o_page->get_pLink($famous_persons[$i]['n'])?>" class="link-more"><?=$o_site->get_sMoreText()?></a>//-->
								</li>        
                                <?
							}
					?>
                    </ul>

				</div>
			</div>
			<div class="small_post">
				<h3><?=$o_page->get_pName($n_comments)?></h3>

				<div class="comments" id="slider2">
                	<ul class="bjqs">
					<?php
						for($i=0; $i<count($comments); $i++)
							{
								
								?>
                                    <li>
                                    <div class="comment">
                                        <p><?=$comments[$i]['comment_text']?></p>
                                        <div class="comments-arrow"></div>	
                                        <span class="autor"><?=$comments[$i]['comment_author']?></span> <span class="date"><?=$comments[$i]['comment_date']?></span>
                                    </div>
                                    </li>
                                <?
								//if(($i%2 == 0) && $i>0) echo "</li><li>";									
							}
					?>
                    </li>
                    </ul>
				</div>
            </div>
		</div>
<?php
	include TEMPLATE_DIR . "column_right.php";
?>
	<script type="text/javascript">
		$j(document).ready(function(){
			
			$j('#slider').bjqs({
				'height' : 200,
				'width' : 250,
				animduration : 200,
				'responsive' : true,
				animspeed  : 1500,
				animtype : 'slide',
				automatic : true,
				showcontrols : true,
				nexttext : '>>',
				prevtext : '<<',
				hoverpause : true,
				usecaptions : true,
			});		
			$j("#slider2").bjqs({
				'height' : 300,
				'width' : 250,
				animduration : 200,
				'responsive' : true,
				animspeed  : 3000,
				animtype : 'slide',
				automatic : true,
				showcontrols : true,
				nexttext : '>>',
				prevtext : '<<',
				hoverpause : true,
				usecaptions : true,
			});
			/* var width = $j("#clickTicker").width();
			$j("#clickTicker").bjqs({
				'height' : 50,
				'width' : width,
				animduration : 200,
				'responsive' : true,
				animspeed  : 3500,
				animtype : 'slide',
				automatic : true,
				showcontrols : true,
				nexttext : '&gt;&gt;',
				prevtext : '&lt;&lt;',
				hoverpause : true,
				showmarkers : false,
				usecaptions : false,
			});
			
			$j( window ).resize(function() {
				var width = $j("#clickTicker").width();
				
				$j("#clickTicker").bjqs({
					'height' : 50,
					'width' : width,
					animduration : 200,
					'responsive' : true,
					animspeed  : 2000,
					animtype : 'slide',
					automatic : true,
					showcontrols : true,
					nexttext : '&gt;&gt;',
					prevtext : '&lt;&lt;',
					hoverpause : true,
					showmarkers : false,
					usecaptions : false,
				});
			}); */
			
		});	
	</script>