<?php		
/*
##########################################################
#	module name: 	Standart Main Menu 
#	description:		show all pages defined as subpages of home page
							select strart-menu page, show / skip pages 
							show / hide / expand submenus
#	directory:			web/admin/modules/menu/
#	author:				M.Petrov, petrovm@abv.bg
##########################################################
*/

//select the parent page of the folowing menu items (n=$Site->StartPage)
if(!isset($menu_parent_item))
	$menu_parent_item = $Site->StartPage;

//show / hide items numbering
if($view_numbering == true)
	$start_number = 1;
	
//show / hide submenu bullet
if(!isset($submenu_bullet))
	$submenu_bullet = " &raquo; ";

//show page with selected status (0-default, 1-toplink, 2-drop_down, 3-main_link, 4-vip_link, 5-second_link)
if(!isset($view_menu_item))
	{
		$view_menu_item = 0;
		if(isset($skip_menu_item)) 
			//skip selected pages
			$skip_menu_item_sql = " AND toplink!=$skip_menu_item ";
		else $skip_menu_item_sql = " ";
	}
else 
{
	unset($skip_menu_item);
	//view selected pages	
	$view_menu_item_sql = "(toplink = '". $view_menu_item."' OR toplink = 4) ";
}
//view all pages
if($view_menu_item == "all")  $view_menu_item_sql = "1"; 

		 //reading main menu		  
		 $res = mysqli_query("SELECT * FROM pages WHERE SiteID = $SiteID AND  $view_menu_item_sql $skip_menu_item_sql ORDER by sort_n ASC"); 
		 while ($menu_row = mysqli_fetch_object($res)) 
		 {
			if($n == $menu_row->n)
				$content ='<a href="/page.php?n='.$menu_row->n.'&SiteID='.$SiteID.'&view_id='.$menu_row->n.'" class="menu_button" style="display: block; font-size: 12px; font-weight: bold;">'.$menu_row->Name.' + </a>';
			else $content ='<a href="/page.php?n='.$menu_row->n.'&SiteID='.$SiteID.'&view_id='.$menu_row->n.'" class="menu_button" style="display: block;">'.$menu_row->Name.' + </a>';
			
			echo $content;
		 } 
?>