<?php
	//define some pages
	
		
	//gallery
	$n_gallery = 219242;
	
	//page contacs
	$n_contacts = 219144;
	
	/*
	//home page
	$n_home_group_left = 669;
	$n_home_group_right = 670;
		
	$home_group_id = 713; 	
	
	$n_famous_persons = 218799;

	
	//about page
	$n_about = "";
	
	//page comments
	$n_comments = 218802;
	*/
	
	define("LABEL_GALLERY", "GALLERY");
	define("LABEL_POLL", "RATING");
	define("LABEL_LINKS", "Follow Us");
	define("LABEL_WEATHER_INFO", "Weather in Keramoti");
	define("LABEL_LOCATION", "Location");
	
	define("LABEL_AIR", "air");
	define("LABEL_WATER", "water");
	
	
	define("LABEL_SUCCESS_RESERV_FORM", "<strong>Success!</strong> Your reservation is successfully sent!");
	define("LABEL_RESERVATION", "Reservation");
	define("LABEL_NAME", "Name");
	define("LABEL_YOURNAME", "Your Name");
	define("LABEL_FILLNAME", "Fill in your name! The field is empty.");
	define("LABEL_MIDDLENAME", "Middlename");
	define("LABEL_YOUR_MIDDLENAME", "Your middlename");
	define("LABEL_FILLMIDDLENAME", "Fill in your middlename! The field is empty.");
	define("LABEL_FAMILY", "Surename");
	define("LABEL_YOUR_FAMILY", "Your Surename");
	define("LABEL_FILLFAMILY", "Fill in your surename! The field is empty.");
	define("LABEL_EGN", "ID");
	define("LABEL_YOUREGN", "Your ID");
	define("LABEL_FILLEGN", "Fill in your ID! The field is empty.");
	define("LABEL_ADDRESS", "Address");
	define("LABEL_FILLADDRESS", "Fill in your address! The field is empty.");
	define("LABEL_YOURPHONE", "Your phone");
	define("LABEL_PHONE", "Phone");
	define("LABEL_FILLPHONE", "Fill in your phone! The field is empty.");
	define("LABEL_FILLMAIL", "Fill in your email! The field is empty.");
	define("LABEL_ARRIVAL", "Arrival date");
	define("LABEL_FILLARRIVAL", "Choose your arrival date! The field is empty.");
	define("LABEL_DEPARTURE", "Departure date");
	define("LABEL_FILLDEPARTURE", "Choose your departure date! The field is empty.");
	define("LABEL_PICKVILLA", "Choose a villa");
	define("LABEL_FILLPICKVILLA", "Choose a villa! The field is empty.");
	define("VILLA_BELLEVUE", "Villa \"Bellevue\"");
	define("VILLA_EXCELSIOR", "Villa \"Excelsior\"");
	define("DUPLEX_ONEFAMILY", "Duplex (one family)");
	define("LABEL_GUESTNUMBER", "Number of guests");
	define("LABEL_FILLGUESTNUMBER", "Fill in number of guests! The field is empty.");
	define("LABEL_ADDITIONALINFO", "Additional information");
	define("LABEL_FILLADDITIONALINFO", "Please, write your message here...");
	define("LABEL_SECCODE", "Security code");
	define("LABEL_FILLSECCODE", "Fill in security code! The field is empty.");
	define("LABEL_SUBMIT", "Submit");
?>