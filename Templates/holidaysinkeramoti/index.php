<?php
	define("TEMPLATE_DIR", "Templates/holidaysinkeramoti/");
	define("TEMPLATE_IMAGES", TEMPLATE_DIR . "images/");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><?php echo("$Title");?></title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<?php
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();

		if($o_page->_site['SitesID'] == 952)
			include_once TEMPLATE_DIR . "languages/en.php";
		else
			include_once TEMPLATE_DIR . "languages/bg.php";

?>
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>style.css" />
<link rel="stylesheet" type="text/css" href="http://www.maksoft.net/css/admin_classes.css" />
<?php
	if(defined('GOOGLE_TRACKING_CODE'))   { include_once("web/admin/modules/google/analitycs.php"); }
	if( (defined('GOOGLE_SITE_VERIF')) && ($o_page->n == $o_page->_site['StartPage']) )   { include_once("web/admin/modules/google/wmt.php"); }
	// <meta name="google-site-verification" content="rDVixnX1fc_yRiI8zns8gdXn6pYhkq9qO8niyrJLFQY" />
?>
</head>
<body>
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript" src="https://d31qbv1cthcecs.cloudfront.net/atrk.js"></script><script type="text/javascript">_atrk_opts = { atrk_acct: "DJbKf1a8Md00iJ", domain:"holidaysinkeramoti.com"}; atrk ();</script><noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=DJbKf1a8Md00iJ" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->
<?php 
if( ($o_page->n <> $o_page->_site['StartPage'])  && ($o_page->_page['ParentPage'] <> $o_page->_site['StartPage']) ) {
?>
<!-- Start of Zopim Live Chat Script -->
<script type="text/javascript">
document.write(unescape("%3Cscript src='" + document.location.protocol + "//zopim.com/?ID9A2DGUxYoybgPNUHwFJwCe6lrnHDOE' charset='utf-8' type='text/javascript'%3E%3C/script%3E"));
</script>
<!-- End of Zopim Live Chat Script -->
<?php
} // edn Zopim
?>
<script type="text/javascript">
$zopim.livechat.setLanguage('en');
</script>

<div id="header">
        <?php 
            //print_menu($o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 4"), array('menu_depth'=>0, 'params'=>"id=\"menu\"", 'home'=>true))
            include TEMPLATE_DIR . "menu_drop_down.php";
        ?>
	<div class="container">
        <div id="logo"><a href="<?=$o_page->get_pLink($o_site->StartPage)?>"><img src="<?=TEMPLATE_IMAGES?>holidaysinkeramoti-logo-icons.png" alt=""></a></div>
	</div>
</div>
<div id="site_container">
        <div id="content" class="<?=($o_page->n == $o_site->StartPage) ? "home":""?>">
            <?php
				// Page Template
				$pTemplate = $o_page->get_pTemplate();
                
				if($o_page->_page['SiteID'] != $o_site->_site['SitesID'] )
					include TEMPLATE_DIR."admin.php";	
				elseif($pTemplate['pt_url'])
					include $pTemplate['pt_url'];
				elseif(($o_page->n == $o_site->StartPage)  ) { // begin home, tag and search
						//enable tag search / tag addresses
							if(strlen($search_tag)>2)
								{
									$o_page->print_search($o_site->do_search(iconv("UTF-8","CP1251",$search_tag), 10)); 
									$keyword = iconv("UTF-8","CP1251",$search_tag); 
									//include("selected_sites.php"); 
								}
							//enable site search
							elseif(strlen($search)>2)
								$o_page->print_search($o_site->do_search($search, 10)); 
							else
								{
			                   if($o_site->_site['language_id'] == 1) include TEMPLATE_DIR . "home.php";
							   }
				 } // home and tag
                else 
					include TEMPLATE_DIR."main.php";
            ?>
        <br clear="all">
        <div class="main-footer">
        	<div class="small-post weather-box">
            	<h2 class="title"><?=LABEL_WEATHER_INFO?></h2>
				<?php			
				include("wwo/wwo_forecast.php"); 
				?>
            </div>
            <div class="small-post">
	            <h2 class="title"><?=$o_page->get_pName($n_contacts)?></h2>
                <?=$o_page->get_pText($n_contacts)?>
            </div>
            <div class="small-post style3">
            	<h2 class="title"><?=LABEL_LINKS?></h2>
                <ul class="social-links">
                    <li><a href="http://<?=$o_site->get_sURL()?>/rss.php" class="icon icon-rss"></a>
					<?php if(defined('CMS_FACEBOOK_PAGE_LINK') || $user->AccessLevel >0) { ?> <li><a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" target="_blank" class="icon icon-facebook"></a> <?php }?>
                    <?php if(defined('CMS_TWITTER_PAGE_LINK') || $user->AccessLevel >0) { ?> <li><a href="<?=(defined("CMS_TWITTER_PAGE_LINK")) ? CMS_TWITTER_PAGE_LINK:"#"?>" target="_blank" class="icon icon-twitter">Twitter</a> <?php }?>
					<?php if(defined('CMS_GPLUS_PAGE_LINK') || $user->AccessLevel >0) { ?> <li><a href="<?=(defined("CMS_GPLUS_PAGE_LINK")) ? CMS_GPLUS_PAGE_LINK:"#"?>" rel="publisher" target="_blank" class="icon icon-gplus"></a> <?php }?>
                    <!--                 
                    <?php if(defined('CMS_PINTEREST_PAGE_LINK') || $user->AccessLevel >0) { ?> <li><a href="<?=(defined("CMS_PINTEREST_PAGE_LINK")) ? CMS_PINTEREST_PAGE_LINK:"#"?>" target="_blank" class="icon icon-pinterest"></a> <?php }?>
                    <?php if(defined('CMS_LINKEDIN_PAGE_LINK') || $user->AccessLevel >0) { ?> <li><a href="<?=(defined("CMS_LINKEDIN_PAGE_LINK")) ? CMS_LINKEDIN_PAGE_LINK:"#"?>" target="_blank" class="icon icon-linkedin"></a> <?php }?>
                    <?php if(defined('CMS_BLOG_PAGE_LINK') || $user->AccessLevel >0) { ?> <li><a href="<?=(defined("CMS_BLOG_PAGE_LINK")) ? CMS_BLOG_PAGE_LINK:"#"?>" target="_blank" class="icon icon-blog"></a> <?php }?>
               		//-->
                </ul>                
				<?php //include DIR_WS_MODULES . "social/social_share.php"; ?>                
                <br><img src="<?=TEMPLATE_IMAGES?>footer-bg.png" class="footer-bg">
            </div>
        </div>
        </div>
        <div id="footer">
            <p>All Rights Reserved, 2009-<?php echo date("Y")." ".$o_page->_site['copyright']; ?></p>
            <hr>
            <div id="navbar"><?=$o_page->print_pNavigation()?></div>
        </div>
</div>
</body>
</html>
