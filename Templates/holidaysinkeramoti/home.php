<script type="text/javascript" src="http://www.maksoft.net/lib/jquery/easyslider/easySlider1.7.js"></script>
<?php
	//Home box LEFT
	$box_left_items = $o_page->get_pGroupContent($n_home_group_left);
	shuffle($box_left_items);
	$left_item = $box_left_items[0];
	
	//Home box RIGHT
	$box_right_items = $o_page->get_pGroupContent($n_home_group_right);
	shuffle($box_right_items);
	$right_item = $box_right_items[0];
	
	//home page post
	$home_page_post_n = $o_page->_site['news'];

	//get weather
	$wwo->get_weather();
	$today = getdate(); 
	$sun_info = date_sun_info(time(), $wwo->latitude, $wwo->longitude);
	$sunrise = getdate($sun_info['sunrise']);
	$sunset = getdate($sun_info['sunset']);
	
	//page famous persons
	$famous_persons = $o_page->get_pSubpages($n_famous_persons);
	shuffle($famous_persons);
	
	//page comments
	$comments = $o_site->get_sComments();
?>
		<div id="main">
			<div class="block first">
				<h5><?=$left_item['Name']?></h5>
				<p><?=cut_text(strip_tags($left_item['textStr']),180)?></p>
                <p><br><a href="<?=$o_page->get_pLink($left_item['n'])?>" class="link-more"><?=$o_site->get_sMoreText()?></a></p>
			</div>
			<div class="block">
				<h5><?=$right_item['Name']?></h5>
				<p><?=cut_text(strip_tags($right_item['textStr']), 180)?></p>
                <p><br><a href="<?=$o_page->get_pLink($right_item['n'])?>" class="link-more"><?=$o_site->get_sMoreText()?></a></p>
			</div>
			<h2><?=$o_page->get_pName($home_page_post_n)?></h2>
            <div class="weather-info">
            	<div class="temp"><strong><?=$wwo->weather->current_condition->temp_C?></strong>  C&deg; <br><br><small><?=LABEL_AIR?></small></div>
                <div class="temp watter"><strong><?=$wwo->marine->weather->hourly->waterTemp_C?></strong>  C&deg; <br><br><small><?=LABEL_WATER?></small></div>
            </div>
			<div class="post">
				<?php
                	$o_page->print_pImage(580, "", "", $home_page_post_n);
					$wwo->parse(); 
					//$o_page->print_pContent(); 
				?>
                <br />
				<?php 
				 $h_pages = $o_page->get_pGroupContent( $home_group_id); 
				 foreach($h_pages as $h_page) {
				  echo('
							  	<p class="style1">'.$o_page->get_pTitle("strict", $h_page['n']).'</p><br>
				  				<p>'.crop_text($o_page->get_pText($h_page['n'])).'</p>
								<p class="more"><a href="'.$o_page->get_pLink($h_page['n']).'"class="link-more"><'.$o_site->get_sMoreText().'></a></p>
					'); 
				 }
				?>
			</div>
			<div class="small_post style2">
				<h3><?=$o_page->get_pName($n_famous_persons)?> </h3>
				<div class="about" id="slider">
                	<ul>
					<?php
						for($i=0; $i<count($famous_persons); $i++)
						//for($i=0; $i<=1; $i++)
							{
								?>
								<li>
								<?php $o_page->print_pImage(262, "", "", $famous_persons[$i]['n'])?>
								<h4><?=$o_page->get_pName($famous_persons[$i]['n'])?></h4>
								<!--<a href="<?=$o_page->get_pLink($famous_persons[$i]['n'])?>" class="link-more"><?=$o_site->get_sMoreText()?></a>//-->
								</li>        
                                <?
							}
					?>
                    </ul>

				</div>
			</div>
			<div class="small_post">
				<h3><?=$o_page->get_pName($n_comments)?></h3>

				<div class="comments" id="slider2">
                	<ul>
					<?php
						for($i=0; $i<count($comments); $i++)
							{
								
								?>
                                    <li>
                                    <div class="comment">
                                        <p><?=$comments[$i]['comment_text']?></p>
                                        <div class="comments-arrow"></div>	
                                        <span class="autor"><?=$comments[$i]['comment_author']?></span> <span class="date"><?=$comments[$i]['comment_date']?></span>
                                    </div>
                                    </li>
                                <?
								//if(($i%2 == 0) && $i>0) echo "</li><li>";									
							}
					?>
                    </li>
                    </ul>
				</div>
            </div>
		</div>
<?php
	include TEMPLATE_DIR . "column_right.php";
?>
	<script type="text/javascript">
		$j(document).ready(function(){	
			$j("#slider").easySlider({
				auto: true, 
				controlsShow: false,
				pause: 3000,
				continuous: true
			});
			$j("#slider2").easySlider({
				auto: true, 
				continuous: true,
				vertical: true,
				controlsShow: false,
				pause: 10000
			});

		});	
	</script>