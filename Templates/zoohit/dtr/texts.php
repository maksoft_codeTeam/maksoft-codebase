<?php 
session_start();
header("Content-type: text/css");
header("Expires: ".gmdate("D, d M Y H:i:s", (time()+900)) . " GMT"); 

if(isset($_SESSION['pcdtr']['stylestr'])) echo "\n/* Generated images: */\n".$_SESSION['pcdtr']['stylestr']; 
else die("\n\n/* PLEASE ENABLE COOKIES! */");
?>


/* Default style - Generally no need to change */

.dtr{
overflow:hidden;
margin:0;
height:1%;
}
.dtr a{
display:block;
float:left;
overflow:hidden;
cursor: pointer
}
.dtr a:hover span{
background-position:left bottom;
}
.dtr span{
display:block;
float:left;
overflow:hidden;
text-indent:-1000px;
background-repeat:no-repeat;
background-position:left top;
}

@media print{
.dtr span{
background-image:none;
display:inline;
float:none
}
}


