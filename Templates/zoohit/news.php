<?php
			$max_news = 12;
			$news_per_page = 4;
			$row_news_res = mysqli_query("SELECT * FROM pages WHERE ParentPage = 35336 ORDER by date_added DESC LIMIT $max_news");

			//output the news subpages content
			$i=0;
			$j=0;
			$page_content = "";
			echo "<script type=\"text/javascript\">var pausecontent=new Array();";

			while($row_news = mysqli_fetch_object($row_news_res))
				{	
					$news_text = str_replace(chr(39), " ", crop_text($row_news->textStr));
					$news_text = str_replace(chr(34), " ", $news_text);
					
					if($i>=$news_per_page)
						{
							//$page_content.= '<span class="date">'.date( "d.m.Y", strtotime($row_news->date_added)).'</span><br><a href="page.php?n='.$row_news->n.'&SiteID='.$SiteID.'" align="right" class="main_link"><span class="title" align="left">'.$row_news->Name.'</span></a><span class="preview"><a href="page.php?n='.$row_news->n.'&SiteID='.$SiteID.'" align="right">'.crop_text($row_news->textStr).'</span></a><br><br>';
							$i=1;
							$j++;
							//$page_content= '<span class="date">'.date( "d.m.Y", strtotime($row_news->date_added)).'</span><br><a href="page.php?n='.$row_news->n.'&SiteID='.$SiteID.'" align="right" class="main_link"><span class="title" align="left">'.$row_news->Name.'</span></a><span class="preview"><a href="page.php?n='.$row_news->n.'&SiteID='.$SiteID.'" align="right">'.$news_text.'</span></a><br><br>';
							$page_content= '<a href="page.php?n='.$row_news->n.'&SiteID='.$SiteID.'" align="right" class="main_link"><span class="title" align="left">'.$row_news->Name.'</span></a><span class="preview"><a href="page.php?n='.$row_news->n.'&SiteID='.$SiteID.'" align="right">'.$news_text.'</span></a><br><br>';							echo "pausecontent[".$j."]='".$page_content."';";
							//$page_content = "";
						}
					else
						{
							//$page_content.= '<span class="date">'.date( "d.m.Y", strtotime($row_news->date_added)).'</span><br><a href="page.php?n='.$row_news->n.'&SiteID='.$SiteID.'" align="right" class="main_link"><span class="title" align="left">'.$row_news->Name.'</span></a><span class="preview"><a href="page.php?n='.$row_news->n.'&SiteID='.$SiteID.'" align="right">'.$news_text.'</span></a><br><br>';
							$page_content.= '<a href="page.php?n='.$row_news->n.'&SiteID='.$SiteID.'" align="right" class="main_link"><span class="title" align="left">'.$row_news->Name.'</span></a><span class="preview"><a href="page.php?n='.$row_news->n.'&SiteID='.$SiteID.'" align="right">'.$news_text.'</span></a><br><br>';
							echo "pausecontent[$j]='".$page_content."';";
							$i++;
						}
				}

				echo "</script>";
?>
<script type="text/javascript">

/***********************************************
* Pausing up-down scroller- � Dynamic Drive (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit http://www.dynamicdrive.com/ for this script and 100s more.
***********************************************/

function pausescroller(content, divId, divClass, delay){
this.content=content //message array content
this.tickerid=divId //ID of ticker div to display information
this.delay=delay //Delay between msg change, in miliseconds.
this.mouseoverBol=1 //Boolean to indicate whether mouse is currently over scroller (and pause it if it is)
this.hiddendivpointer=1 //index of message array for hidden div
document.write('<div id="'+divId+'" class="'+divClass+'" style="position: relative; overflow: hidden"><div class="innerDiv" style="position: absolute; width: 100%" id="'+divId+'1">'+content[0]+'</div><div class="innerDiv" style="position: absolute; width: 100%; visibility: hidden" id="'+divId+'2">'+content[1]+'</div></div>')
var scrollerinstance=this
if (window.addEventListener) //run onload in DOM2 browsers
window.addEventListener("load", function(){scrollerinstance.initialize()}, false)
else if (window.attachEvent) //run onload in IE5.5+
window.attachEvent("onload", function(){scrollerinstance.initialize()})
else if (document.getElementById) //if legacy DOM browsers, just start scroller after 0.5 sec
setTimeout(function(){scrollerinstance.initialize()}, 500)
}

// -------------------------------------------------------------------
// initialize()- Initialize scroller method.
// -Get div objects, set initial positions, start up down animation
// -------------------------------------------------------------------

pausescroller.prototype.initialize=function(){
this.tickerdiv=document.getElementById(this.tickerid)
this.visiblediv=document.getElementById(this.tickerid+"1")
this.hiddendiv=document.getElementById(this.tickerid+"2")
this.visibledivtop=parseInt(pausescroller.getCSSpadding(this.tickerdiv))
//set width of inner DIVs to outer DIV's width minus padding (padding assumed to be top padding x 2)
this.visiblediv.style.width=this.hiddendiv.style.width=this.tickerdiv.offsetWidth-(this.visibledivtop*2)+"px"
this.getinline(this.visiblediv, this.hiddendiv)
this.hiddendiv.style.visibility="visible"
var scrollerinstance=this
document.getElementById(this.tickerid).onmouseover=function(){scrollerinstance.mouseoverBol=1}
document.getElementById(this.tickerid).onmouseout=function(){scrollerinstance.mouseoverBol=0}
if (window.attachEvent) //Clean up loose references in IE
window.attachEvent("onunload", function(){scrollerinstance.tickerdiv.onmouseover=scrollerinstance.tickerdiv.onmouseout=null})
setTimeout(function(){scrollerinstance.animateup()}, this.delay)
}


// -------------------------------------------------------------------
// animateup()- Move the two inner divs of the scroller up and in sync
// -------------------------------------------------------------------

pausescroller.prototype.animateup=function(){
var scrollerinstance=this
if (parseInt(this.hiddendiv.style.top)>(this.visibledivtop+5)){
this.visiblediv.style.top=parseInt(this.visiblediv.style.top)-5+"px"
this.hiddendiv.style.top=parseInt(this.hiddendiv.style.top)-5+"px"
setTimeout(function(){scrollerinstance.animateup()}, 20)
}
else{
this.getinline(this.hiddendiv, this.visiblediv)
this.swapdivs()
setTimeout(function(){scrollerinstance.setmessage()}, this.delay)
}
}

// -------------------------------------------------------------------
// swapdivs()- Swap between which is the visible and which is the hidden div
// -------------------------------------------------------------------

pausescroller.prototype.swapdivs=function(){
var tempcontainer=this.visiblediv
this.visiblediv=this.hiddendiv
this.hiddendiv=tempcontainer
}

pausescroller.prototype.getinline=function(div1, div2){
div1.style.top=this.visibledivtop+"px"
div2.style.top=Math.max(div1.parentNode.offsetHeight, div1.offsetHeight)+"px"
}

// -------------------------------------------------------------------
// setmessage()- Populate the hidden div with the next message before it's visible
// -------------------------------------------------------------------

pausescroller.prototype.setmessage=function(){
var scrollerinstance=this
if (this.mouseoverBol==1) //if mouse is currently over scoller, do nothing (pause it)
setTimeout(function(){scrollerinstance.setmessage()}, 100)
else{
var i=this.hiddendivpointer
var ceiling=this.content.length
this.hiddendivpointer=(i+1>ceiling-1)? 0 : i+1
this.hiddendiv.innerHTML=this.content[this.hiddendivpointer]
this.animateup()
}
}

pausescroller.getCSSpadding=function(tickerobj){ //get CSS padding value, if any
if (tickerobj.currentStyle)
return tickerobj.currentStyle["paddingTop"]
else if (window.getComputedStyle) //if DOM2
return window.getComputedStyle(tickerobj, "").getPropertyValue("padding-top")
else
return 0
}
</script>
<script type="text/javascript">

//new pausescroller(name_of_message_array, CSS_ID, CSS_classname, pause_in_miliseconds)

new pausescroller(pausecontent, "news", "", 20000)

</script>