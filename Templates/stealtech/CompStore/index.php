<!DOCTYPE html>
<html lang="<?php echo($Site->language_key); ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<title><?=$Title?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="viewport" content="width=device-width,minimum-scale=1,maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<?php
	// saved from url=(0050)http://livedemo00.template-help.com/magento_47676/
	//include template configurations
	include("Templates/stealtech/CompStore/configure.php");		
	//include meta tags
	include("Templates/meta_tags.php");
	$o_site->print_sConfigurations();
?>
<script async="" defer="true" src="/Templates/stealtech/CompStore/inc/visits"></script>
<script type="text/javascript" async="" src="/Templates/stealtech/CompStore/inc/ga.js"></script>
<script async="" src="/Templates/stealtech/CompStore/inc/cloudflare.min.js"></script>
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok2v=88e434a982/"},atok:"03e33451287dea82005ed8f728a35e00",petok:"9908ebd7aa8b20211b9efb1c87b36d605b23a3ce-1412601207-86400",zone:"template-help.com",rocket:"0",apps:{"abetterbrowser":{"ie":"7"},"ga_key":{"ua":"UA-7078796-5","ga_bs":"2"}}}];!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="//ajax.cloudflare.com/cdn-cgi/nexp/dok2v=97fb4d042e/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
//]]>
</script>
<link rel="icon" href="http://livedemo00.template-help.com/magento_47676/skin/frontend/default/theme566/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="http://livedemo00.template-help.com/magento_47676/skin/frontend/default/theme566/favicon.ico" type="image/x-icon">
<link href="/Templates/stealtech/CompStore/inc/css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/jquery-1.7.min.js"></script><style type="text/css">.cf-hidden { display: none; } .cf-invisible { visibility: hidden; }</style>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/superfish.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/scripts.js"></script>
<!--[if lt IE 7]>
<script type="text/javascript">
//<![CDATA[
    var BLANK_URL = 'http://livedemo00.template-help.com/magento_47676/js/blank.html';
    var BLANK_IMG = 'http://livedemo00.template-help.com/magento_47676/js/spacer.gif';
//]]>
</script>
<![endif]-->
<!--[if lt IE 9]>
<div style=' clear: both; text-align:center; position: relative;'>
 <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<!--[if lt IE 9]>
	<style>
	body {
		min-width: 960px !important;
	}
	</style>
<![endif]-->
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/font-awesome.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/jquery.bxslider.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/photoswipe.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/bootstrap.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/extra_style.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/styles.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/responsive.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/superfish.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/camera.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/widgets.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/cloud-zoom.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/catalogsale.css" media="all">
<link rel="stylesheet" type="text/css" href="/Templates/stealtech/CompStore/inc/print.css" media="print">
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/prototype.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/ccard.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/validation.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/builder.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/effects.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/dragdrop.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/controls.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/slider.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/js.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/form.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/translate.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/cookies.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/cloud-zoom.1.0.2.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/jquery.mobile.customized.min.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/bootstrap.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/jquery.carouFredSel-6.2.1.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/jquery.touchSwipe.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/carousel.js"></script>
<script type="text/javascript" src="/Templates/stealtech/CompStore/inc/msrp.js"></script>
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="http://livedemo00.template-help.com/magento_47676/skin/frontend/default/theme566/css/styles-ie.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="http://livedemo00.template-help.com/magento_47676/js/lib/ds-sleight.js"></script>
<script type="text/javascript" src="http://livedemo00.template-help.com/magento_47676/skin/frontend/base/default/js/ie6.js"></script>
<![endif]-->
<script type="text/javascript">
//<![CDATA[
Mage.Cookies.path     = '/magento_47676';
Mage.Cookies.domain   = '.livedemo00.template-help.com';
//]]>
</script>
<script type="text/javascript">//<![CDATA[
        var Translator = new Translate([]);
        //]]></script><script type="text/javascript">
/* <![CDATA[ */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-7078796-5']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

(function(b){(function(a){"__CF"in b&&"DJS"in b.__CF?b.__CF.DJS.push(a):"addEventListener"in b?b.addEventListener("load",a,!1):b.attachEvent("onload",a)})(function(){"FB"in b&&"Event"in FB&&"subscribe"in FB.Event&&(FB.Event.subscribe("edge.create",function(a){_gaq.push(["_trackSocial","facebook","like",a])}),FB.Event.subscribe("edge.remove",function(a){_gaq.push(["_trackSocial","facebook","unlike",a])}),FB.Event.subscribe("message.send",function(a){_gaq.push(["_trackSocial","facebook","send",a])}));"twttr"in b&&"events"in twttr&&"bind"in twttr.events&&twttr.events.bind("tweet",function(a){if(a){var b;if(a.target&&a.target.nodeName=="IFRAME")a:{if(a=a.target.src){a=a.split("#")[0].match(/[^?=&]+=([^&]*)?/g);b=0;for(var c;c=a[b];++b)if(c.indexOf("url")===0){b=unescape(c.split("=")[1]);break a}}b=void 0}_gaq.push(["_trackSocial","twitter","tweet",b])}})})})(window);
/* ]]> */
</script>
<script src="/Templates/stealtech/CompStore/inc/application2.js" async="" defer="" data-cfasync="false"></script><style type="text/css">.olark-key,#hbl_code,#olark-data{display: none !important;}</style><link id="habla_style_div" type="text/css" rel="stylesheet" href="/Templates/stealtech/CompStore/inc/bc25e8d10f4b6653d4ecdb6dbca0328f.css"><style type="text/css">@media print {#habla_beta_container_do_not_rely_on_div_classes_or_names {display: none !important}}</style></head>
<body class="ps-static  cms-index-index cms-home"><div id="olark" style="display: none;"><olark><iframe frameborder="0" id="olark-loader"></iframe></olark></div>
<div class="wrapper ps-static en-lang-class">
<noscript>
&lt;div class="global-site-notice noscript"&gt;
&lt;div class="notice-inner"&gt;
&lt;p&gt;
&lt;strong&gt;JavaScript seems to be disabled in your browser.&lt;/strong&gt;&lt;br/&gt;
You must have JavaScript enabled in your browser to utilize the functionality of this website. &lt;/p&gt;
&lt;/div&gt;
&lt;/div&gt;
</noscript>
<div class="page">
<div class="shadow"></div>
<div class="swipe-left"></div>
<div class="swipe" style="height: 866px; left: -237px;">
<div class="swipe-menu">
<a href="/Templates/stealtech/CompStore/inc/Home page.htm" title="Home" class="home-link">Home</a>
<ul class="links">
<li class="first"><a href="http://livedemo00.template-help.com/magento_47676/customer/account/" title="My Account">My Account</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/wishlist/" title="My Wishlist">My Wishlist</a></li>
<li class="top-car"><a href="http://livedemo00.template-help.com/magento_47676/checkout/cart/" title="My Cart" class="top-link-cart">My Cart</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/checkout/" title="Checkout" class="top-link-checkout">Checkout</a></li>
<li class=" last"><a href="http://livedemo00.template-help.com/magento_47676/customer/account/login/" title="Log In">Log In</a></li>
</ul>
<div class="currency-switch switch-show">
<div class="currency-title">
<span class="label">Currency:</span><strong class="current">USD</strong>
</div>
<ul class="currency-dropdown"><li><a href="http://livedemo00.template-help.com/magento_47676/directory/currency/switch/currency/GBP/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/"><span>British Pound Sterling -</span> GBP</a></li><li><a href="http://livedemo00.template-help.com/magento_47676/directory/currency/switch/currency/EUR/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/"><span>Euro -</span> EUR</a></li><li><a href="http://livedemo00.template-help.com/magento_47676/directory/currency/switch/currency/USD/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/" class="selected"><span>US Dollar -</span> USD</a></li></ul>
</div>
 
<div class="language-list switch-show">
<div class="language-title"><span class="label">Your Language:</span> <strong>en</strong></div>
<ul>
<li>
<a class="selected" href="http://livedemo00.template-help.com/magento_47676/?___store=english&___from_store=english" title="en_US">   <strong>en</strong></a>
</li>
<li>
<a href="http://livedemo00.template-help.com/magento_47676/?___store=german&___from_store=english" title="de_DE">   <strong>de</strong></a>
</li>
<li>
<a href="http://livedemo00.template-help.com/magento_47676/?___store=spanish&___from_store=english" title="es_ES">   <strong>es</strong></a>
</li>
<li>
<a href="http://livedemo00.template-help.com/magento_47676/?___store=russian&___from_store=english" title="ru_RU">   <strong>ru</strong></a>
</li>
</ul>
</div>
<div class="footer-links-menu">
<ul>
<li><a href="http://livedemo00.template-help.com/magento_47676/about-magento-demo-store">About Us</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/customer-service">Customer Service</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/template-settings">Template Settings</a></li>
<li class="last privacy"><a href="http://livedemo00.template-help.com/magento_47676/privacy-policy-cookie-restriction-mode">Privacy Policy</a></li>
</ul>
<ul class="links-2">
<li class="first"><a href="http://livedemo00.template-help.com/magento_47676/catalog/seo_sitemap/product/">Product Sitemap</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/catalog/seo_sitemap/category/">Category Sitemap</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/">Advanced Search</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/sales/guest/form/">Orders and Returns</a></li>
</ul> </div>
</div>
</div>
<div class="top-icon-menu">
<div class="swipe-control"><i class="icon-reorder"></i></div>
<div class="top-search"><i class="icon-search"></i></div>
<span class="clear"></span>
</div>

<div id="header">
<?php include TEMPLATE_DIR."header.php"?>
</div>

<div class="nav-container">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="sf-menu-block">
<div id="menu-icon" class="menu-icon-style">Categories</div>
 
<ul class="sf-menu-phone">
<li class="level0 nav-1 first level-top"><a href="http://livedemo00.template-help.com/magento_47676/accessories.html" class="level-top"><span>Accessories</span></a></li><li class="level0 nav-2 level-top"><a href="http://livedemo00.template-help.com/magento_47676/cpus.html" class="level-top"><span>CPUs</span></a></li><li class="level0 nav-3 level-top"><a href="http://livedemo00.template-help.com/magento_47676/hard-drives.html" class="level-top"><span>Hard Drives</span></a></li><li class="level0 nav-4 level-top"><a href="http://livedemo00.template-help.com/magento_47676/keyboards-mice.html" class="level-top"><span>Keyboards / Mice</span></a></li><li class="level0 nav-5 last level-top parent"><a href="http://livedemo00.template-help.com/magento_47676/monitors.html" class="level-top"><span>monitors</span></a><ul class="level0"><li class="level1 nav-5-1 first"><a href="http://livedemo00.template-help.com/magento_47676/monitors/entertainment-networking.html"><span>A�er</span></a></li><li class="level1 nav-5-2 parent"><a href="http://livedemo00.template-help.com/magento_47676/monitors/dell.html"><span>Dell</span></a><ul class="level1"><li class="level2 nav-5-2-1 first last"><a href="http://livedemo00.template-help.com/magento_47676/monitors/dell/test-category.html"><span>Test Category</span></a></li></ul><strong></strong></li><li class="level1 nav-5-3 last"><a href="http://livedemo00.template-help.com/magento_47676/monitors/samsung.html"><span>Samsung</span></a></li></ul><strong></strong></li> </ul>
</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
<div class="main-container col2-left-layout">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="main">
<div class="row">
<div class="col-main col-xs-12 col-sm-9">
<div class="padding-s">
<div class="std"><div class="clear"></div>
<script src="/Templates/stealtech/CompStore/inc/camera.js"> </script>
<script>
        jQuery(function(){
            jQuery('#camera_wrap').camera({
                alignmen: 'topCenter',
                height: '40.43%',
                minHeight: '100px',
                loader : false,
                navigation: true,
                fx: 'simpleFade',
                navigationHover:false,       
                thumbnails: false,
                playPause: false,
                pagination:false,
            });
        });
        </script>
<div class="fluid_container_wrap">
<div class="fluid_container">
<div class="camera_wrap camera_orange_skin" id="camera_wrap" style="display: block; height: 351px;"><div class="camera_fakehover"><div class="camera_src camerastarted camerasliding">
<div data-src="http://livedemo00.template-help.com/magento_47676/skin/frontend/default/theme566/images/camera/slides/slide1.jpg">

</div>
<div data-src="http://livedemo00.template-help.com/magento_47676/skin/frontend/default/theme566/images/camera/slides/slide2.jpg">

</div>
<div data-src="http://livedemo00.template-help.com/magento_47676/skin/frontend/default/theme566/images/camera/slides/slide3.jpg">

</div>
</div><div class="camera_target"><div class="cameraCont"><div class="cameraSlide cameraSlide_0 cameracurrent" style="visibility: visible; display: block; z-index: 999;"><img src="/Templates/stealtech/CompStore/inc/slide1.jpg" class="imgLoaded" data-alignment="" data-portrait="" width="868" height="351" style="visibility: visible; height: 351px; margin-left: 0px; margin-top: 0px; position: absolute; width: 868px;"><div class="camerarelative" style="width: 868px; height: 351px;"></div></div><div class="cameraSlide cameraSlide_1 cameranext" style="display: none; z-index: 1;"><img src="/Templates/stealtech/CompStore/inc/slide2.jpg" class="imgLoaded" data-alignment="" data-portrait="" width="868" height="351" style="visibility: visible; height: 351px; margin-top: 0px; position: absolute; margin-left: 0px; width: 868px;"><div class="camerarelative" style="width: 868px; height: 351px;"></div></div><div class="cameraSlide cameraSlide_2 cameranext" style="display: none; z-index: 1;"><img src="/Templates/stealtech/CompStore/inc/slide3.jpg" class="imgLoaded" data-alignment="" data-portrait="" width="868" height="351" style="visibility: visible; height: 351px; margin-left: 0px; margin-top: 0px; position: absolute; width: 868px;"><div class="camerarelative" style="width: 868px; height: 351px;"></div></div><div class="cameraSlide cameraSlide_3 cameranext" style="z-index: 1; display: none;"><div class="camerarelative" style="width: 868px; height: 351px;"></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 351.013108683981px; left: 0px; top: 0px; width: 868.013108683981px; margin-left: 0px; margin-top: 0px; opacity: 0.0131086839807539;"><div class="cameraSlide cameraSlide_1 cameranext" style="z-index: 1; height: 351px; margin-left: 0px; margin-top: 0px; width: 868px;"><img src="/Templates/stealtech/CompStore/inc/slide2.jpg" class="imgLoaded" data-alignment="" data-portrait="" width="868" height="351" style="visibility: visible; height: 351px; margin-top: 0px; position: absolute; margin-left: 0px; width: 868px;"><div class="camerarelative" style="width: 868px; height: 351px;"></div></div></div></div></div><div class="camera_overlayer"></div><div class="camera_target_content"><div class="cameraContents"><div class="cameraContent cameracurrent" style="display: block; opacity: 0.135669444444445;"><div class="camera_caption fadeFromLeft" style="visibility: visible; opacity: 1; left: 0px; right: auto;"><div>
<div class="lof_camera_title">iPad Air</div>
<div class="lof_camera_desc">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod</div>
<a href="http://livedemo00.template-help.com/magento_47676/monitors/entertainment-networking.html/">Shop now!</a>
</div></div></div><div class="cameraContent" style="display: none;"><div class="camera_caption fadeFromLeft" style="visibility: hidden; opacity: 1; left: 0px; right: auto;"><div>
<div class="lof_camera_title">iPod touch</div>
<div class="lof_camera_desc">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod</div>
<a class="color-1" href="http://livedemo00.template-help.com/magento_47676/monitors/dell.html/">Shop now!</a>
</div></div></div><div class="cameraContent" style="display: none;"><div class="camera_caption fadeFromLeft" style="visibility: hidden; opacity: 1; left: 0px; right: auto;"><div>
<div class="lof_camera_title">Cocoon Grid IT</div>
<div class="lof_camera_desc">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod</div>
<a class="color-2" href="http://livedemo00.template-help.com/magento_47676/monitors/samsung.html/">Shop now!</a>
</div></div></div></div></div><div class="camera_bar" style="display: none; top: auto; height: 7px;"><span class="camera_bar_cont" style="opacity: 0.8; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; background-color: rgb(34, 34, 34);"><span id="pie_0" style="opacity: 0; position: absolute; left: 0px; right: 0px; top: 2px; bottom: 2px; display: block; background-color: rgb(238, 238, 238);"></span></span></div><div class="camera_prev"><span></span></div><div class="camera_next"><span></span></div></div><div class="camera_loader" style="display: none; visibility: visible;"></div></div>
</div>
</div>
<?php
	//enable tag search / tag addresses
	if(strlen($search_tag)>2)
		{
			$o_page->print_search($o_site->do_search(iconv("UTF-8","CP1251",$search_tag), 10)); 
			$keyword = iconv("UTF-8","CP1251",$search_tag); 
		}
	//enable site search
	elseif(strlen($search)>2)
		$o_page->print_search($o_site->do_search($search, 10)); 
	else
		{	

			//show scrolling welcome message
			if($welcome_message)
				{
					?>
					<div id="welcome_message" style="display: block; width: 600px; overflow:hidden;"><?=$welcome_message?></div>
					<br clear="all">                    
					<?	
				}
			//print page content
			$o_page->print_pContent();
			
			echo "<br clear=\"all\">";
			if(!defined(CMS_MAIN_WIDTH) || CMS_MAIN_WIDTH > $tmpl_config['default_main_width'])
				$cms_args = array("CMS_MAIN_WIDTH"=>$tmpl_config['default_main_width']);
			//print page subcontent
			$o_page->print_pSubContent(NULL, 1, true, $cms_args);
								
			//print php code
			eval($o_page->get_pInfo("PHPcode"));
			
			if ($user->AccessLevel >= $row->SecLevel)
				include_once("$row->PageURL");
				
			// likes
			include_once("web/admin/modules/social/like.php"); 

			
		} // do_search				
?>
<div id="navbar"><?=$o_page->print_pNavigation();?></div>
<ul class="banner-block">
<li>
<div class="banner-block-cen">
<img src="/Templates/stealtech/CompStore/inc/banners-1.jpg" alt="">
<div class="banner-block-content">
<h1>Laptops</h1>
<p>Ipsum dolor sit amet conse ctetur adipisicing eli eiusmod.</p>
<button type="button" title="Shop now!" class="button color-1" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/cpus.html/&#39;)"><span><span>Shop now!</span></span></button>
</div>
</div>
</li>
<li>
<div class="banner-block-cen">
<img src="/Templates/stealtech/CompStore/inc/banners-2.jpg" alt="">
<div class="banner-block-content">
<h1>Desktops</h1>
<p>Ipsum dolor sit amet conse ctetur adipisic ing eli eiusmod.</p>
<button type="button" title="Shop now!" class="button color-2" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/monitors/dell.html/&#39;)"><span><span>Shop now!</span></span></button>
</div>
</div>
</li>
<li>
<div class="banner-block-cen">
<img src="/Templates/stealtech/CompStore/inc/banners-3.jpg" alt="">
<div class="banner-block-content">
<h1>Monitors</h1>
<p>Ipsum dolor sit amet conse ctetur adipisi cing eli eiusmod.</p>
<button type="button" title="Shop now!" class="button color-3" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/monitors/samsung.html/&#39;)"><span><span>Shop now!</span></span></button>
</div>
</div>
</li>
<li>
<div class="banner-block-cen">
<img src="/Templates/stealtech/CompStore/inc/banners-4.jpg" alt="">
<div class="banner-block-content">
<h1>Tablets</h1>
<p>Ipsum dolor sit amet conse ctetur adipisicing eli eiusmod.</p>
<button type="button" title="Shop now!" class="button color-4" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/home-appliances/kitchen-appliances.html/&#39;)"><span><span>Shop now!</span></span></button>
</div>
</div>
</li>
<li>
<div class="banner-block-cen">
<img src="/Templates/stealtech/CompStore/inc/banners-5.jpg" alt="">
<div class="banner-block-content">
<h1>Keyboards &amp; Mice</h1>
<p>Ipsum dolor sit amet conse ctetur adipisicing eli eiusmod.</p>
<button type="button" title="Shop now!" class="button color-5" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/keyboards-mice-2.html/&#39;)"><span><span>Shop now!</span></span></button>
</div>
</div>
</li>
<li>
<div class="banner-block-cen">
<img src="/Templates/stealtech/CompStore/inc/banners-6.jpg" alt="">
<div class="banner-block-content">
<h1>Accessories</h1>
<p>Ipsum dolor sit amet conse ctetur adipisicing eli eiusmod.</p>
<button type="button" title="Shop now!" class="button color-6" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/accessories.html/&#39;)"><span><span>Shop now!</span></span></button>
</div>
</div>
</li>
</ul></div><div class="page-title category-title">
<h1>New Products</h1>
</div>
<ul class="products-grid row">
<li class="item col-xs-4 first">
<div class="wrapper-hover">
<a href="http://livedemo00.template-help.com/magento_47676/mx610-left-hand-laser-cordless-mouse.html" title="Dell UltraSharp U2711" class="product-image"><img src="/Templates/stealtech/CompStore/inc/dell_ultrasharp_u2711_2.png" alt="Dell UltraSharp U2711"></a>
<div class="product-shop">
<div class="price-box">
<span class="regular-price" id="product-price-36-new">
<span class="price">$550.00</span> </span>
</div>
<h3 class="product-name"><a href="http://livedemo00.template-help.com/magento_47676/mx610-left-hand-laser-cordless-mouse.html" title="Dell UltraSharp U2711">Dell UltraSharp U2711</a></h3>
<div class="desc_grid">Cordless, Forward/Back Buttons, Hyper-Fast Scrolling, Laser Sensor...I...</div>
<div class="actions">
<button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/checkout/cart/add/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/product/36/form_key/KcsyOETxYMbFIu4U/&#39;)"><span><span>Add to Cart</span></span></button>
<button type="button" title="Details" class="button btn-details" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/mx610-left-hand-laser-cordless-mouse.html&#39;)"><span><span>Details</span></span></button>
 
</div>
</div>
<div class="label-product">
<span class="new">New</span> </div>
<div class="clear"></div>
</div>
</li>
<li class="item col-xs-4">
<div class="wrapper-hover">
<a href="http://livedemo00.template-help.com/magento_47676/auzentech-vr-fidelity-usb-speakerphone.html" title="WD My Book Live Duo" class="product-image"><img src="/Templates/stealtech/CompStore/inc/wd_my_book_live_duo_2.png" alt="WD My Book Live Duo"></a>
<div class="product-shop">
<div class="price-box">
<span class="regular-price" id="product-price-18-new">
<span class="price">$220.00</span> </span>
</div>
<h3 class="product-name"><a href="http://livedemo00.template-help.com/magento_47676/auzentech-vr-fidelity-usb-speakerphone.html" title="WD My Book Live Duo">WD My Book Live Duo</a></h3>
<div class="desc_grid">It seems that computer is such a revolutionary invention that we could...</div>
<div class="actions">
<button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/checkout/cart/add/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/product/18/form_key/KcsyOETxYMbFIu4U/&#39;)"><span><span>Add to Cart</span></span></button>
<button type="button" title="Details" class="button btn-details" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/auzentech-vr-fidelity-usb-speakerphone.html&#39;)"><span><span>Details</span></span></button>
 
</div>
</div>
<div class="label-product">
<span class="new">New</span> </div>
<div class="clear"></div>
</div>
</li>
<li class="item col-xs-4 last">
<div class="wrapper-hover">
<a href="http://livedemo00.template-help.com/magento_47676/apple-airport-extreme-base-station-gigabit-mb053ll.html" title="Logitech Gaming Keyboard G103" class="product-image"><img src="/Templates/stealtech/CompStore/inc/logitech_gaming_keyboard_g103_2.png" alt="Logitech Gaming Keyboard G103"></a>
<div class="product-shop">
<div class="price-box">
<span class="regular-price" id="product-price-22-new">
<span class="price">$128.99</span> </span>
</div>
<h3 class="product-name"><a href="http://livedemo00.template-help.com/magento_47676/apple-airport-extreme-base-station-gigabit-mb053ll.html" title="Logitech Gaming Keyboard G103">Logitech Gaming Keyboard G103</a></h3>
<div class="desc_grid">We are living in the epoch of great technical progress and we are sure...</div>
<div class="actions">
<button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/checkout/cart/add/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/product/22/form_key/KcsyOETxYMbFIu4U/&#39;)"><span><span>Add to Cart</span></span></button>
<button type="button" title="Details" class="button btn-details" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/apple-airport-extreme-base-station-gigabit-mb053ll.html&#39;)"><span><span>Details</span></span></button>
 
</div>
</div>
<div class="label-product">
<span class="new">New</span> </div>
<div class="clear"></div>
</div>
</li>
</ul>
<a class="banner-home" href="http://livedemo00.template-help.com/magento_47676/accessories.html/"><img src="/Templates/stealtech/CompStore/inc/banners-7.gif" alt=""></a>
<div class="widget widget-catalogsale-products">
<div class="page-title category-title">
<h1>Special products</h1>
</div>
<ul class="products-grid row" id="widget-catalogsale-products-ed21f511b0019779c44a8d72e98cc7bd">
<li class="item col-xs-12 col-sm-4 odd">
<div class="wrapper-hover">
<a class="product-image" href="http://livedemo00.template-help.com/magento_47676/seagate-maxtor-central-axis-1-tb-network-storage-server-stm310005caa00g-rk.html" title="M3200� Modern 2.1 Multimedia Speaker System "><img src="/Templates/stealtech/CompStore/inc/m3200_modern_2.1_multimedia_speaker_system_1.png" alt="M3200� Modern 2.1 Multimedia Speaker System "></a>
<div class="product-shop">
<div class="price-box">
<p class="old-price">
<span class="price-label">Regular Price:</span>
<span class="price" id="old-price-4-widget-catalogsale-ed21f511b0019779c44a8d72e98cc7bd">
$400.00 </span>
</p>
<p class="special-price">
<span class="price-label">Special Price</span>
<span class="price" id="product-price-4-widget-catalogsale-ed21f511b0019779c44a8d72e98cc7bd">
$350.00 </span>
</p>
</div>
<p class="product-name"><a href="http://livedemo00.template-help.com/magento_47676/seagate-maxtor-central-axis-1-tb-network-storage-server-stm310005caa00g-rk.html" title="M3200� Modern 2.1 Multimedia Speaker System )">M3200� Modern 2.1 Multimedia Speaker System </a></p>
<div class="desc_grid">We are living in the epoch of great technical progress and we are sure...</div>
<div class="actions">
<button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/checkout/cart/add/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/product/4/form_key/KcsyOETxYMbFIu4U/&#39;)"><span><span>Add to Cart</span></span></button>
<button type="button" title="Details" class="button btn-details" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/seagate-maxtor-central-axis-1-tb-network-storage-server-stm310005caa00g-rk.html&#39;)"><span><span>Details</span></span></button>
 
</div>
</div>
<div class="label-product">
<span class="sale">Sale</span> </div>
<div class="clear"></div>
</div>
</li>
<li class="item col-xs-12 col-sm-4 even">
<div class="wrapper-hover">
<a class="product-image" href="http://livedemo00.template-help.com/magento_47676/qwerty-usb-white-keyboard-kit-51.html" title="Samsung S27A950D "><img src="/Templates/stealtech/CompStore/inc/samsung_s27a950d_1.png" alt="Samsung S27A950D "></a>
<div class="product-shop">
<div class="price-box">
<p class="old-price">
<span class="price-label">Regular Price:</span>
<span class="price" id="old-price-41-widget-catalogsale-ed21f511b0019779c44a8d72e98cc7bd">
$420.00 </span>
</p>
<p class="special-price">
<span class="price-label">Special Price</span>
<span class="price" id="product-price-41-widget-catalogsale-ed21f511b0019779c44a8d72e98cc7bd">
$400.00 </span>
</p>
</div>
<p class="product-name"><a href="http://livedemo00.template-help.com/magento_47676/qwerty-usb-white-keyboard-kit-51.html" title="Samsung S27A950D )">Samsung S27A950D </a></p>
<div class="desc_grid">The Keyboard is a necessary and often overlooked component for an enjo...</div>
<div class="actions">
<button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/checkout/cart/add/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/product/41/form_key/KcsyOETxYMbFIu4U/&#39;)"><span><span>Add to Cart</span></span></button>
<button type="button" title="Details" class="button btn-details" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/qwerty-usb-white-keyboard-kit-51.html&#39;)"><span><span>Details</span></span></button>
 
</div>
</div>
<div class="label-product">
<span class="sale">Sale</span> </div>
<div class="clear"></div>
</div>
</li>
<li class="item col-xs-12 col-sm-4 last odd">
<div class="wrapper-hover">
<a class="product-image" href="http://livedemo00.template-help.com/magento_47676/sony-nas-cz1-network-audio-player-and-shelf-system.html" title="Seagate Backup Plus 1 TB"><img src="/Templates/stealtech/CompStore/inc/seagate_backup_plus_1_tb_2.png" alt="Seagate Backup Plus 1 TB"></a>
<div class="product-shop">
<div class="price-box">
<p class="old-price">
<span class="price-label">Regular Price:</span>
<span class="price" id="old-price-13-widget-catalogsale-ed21f511b0019779c44a8d72e98cc7bd">
$200.00 </span>
</p>
<p class="special-price">
<span class="price-label">Special Price</span>
<span class="price" id="product-price-13-widget-catalogsale-ed21f511b0019779c44a8d72e98cc7bd">
$180.00 </span>
</p>
</div>
<p class="product-name"><a href="http://livedemo00.template-help.com/magento_47676/sony-nas-cz1-network-audio-player-and-shelf-system.html" title="Seagate Backup Plus 1 TB)">Seagate Backup Plus 1 TB</a></p>
<div class="desc_grid">We are living in the epoch of great technical progress and we are sure...</div>
<div class="actions">
<button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/checkout/cart/add/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/product/13/form_key/KcsyOETxYMbFIu4U/&#39;)"><span><span>Add to Cart</span></span></button>
<button type="button" title="Details" class="button btn-details" onclick="setLocation(&#39;http://livedemo00.template-help.com/magento_47676/sony-nas-cz1-network-audio-player-and-shelf-system.html&#39;)"><span><span>Details</span></span></button>
 
</div>
</div>
<div class="label-product">
<span class="sale">Sale</span> </div>
<div class="clear"></div>
</div>
</li>
</ul>
<script type="text/javascript">decorateList('widget-catalogsale-products-ed21f511b0019779c44a8d72e98cc7bd', 'none-recursive')</script>
</div>
<div id="map-popup" class="map-popup" style="display:none;">
<a href="http://livedemo00.template-help.com/magento_47676/#" class="map-popup-close" id="map-popup-close">x</a>
<div class="map-popup-arrow"></div>
<div class="map-popup-heading"><h2 id="map-popup-heading"></h2></div>
<div class="map-popup-content" id="map-popup-content">
<div class="map-popup-msrp" id="map-popup-msrp-box"><strong>Price:</strong> <span style="text-decoration:line-through;" id="map-popup-msrp"></span></div>
<div class="map-popup-price" id="map-popup-price-box"><strong>Actual Price:</strong> <span id="map-popup-price"></span></div>
<div class="map-popup-checkout">
<form action="" method="POST" id="product_addtocart_form_from_popup">
<input type="hidden" name="product" class="product_id" value="" id="map-popup-product-id">
<div class="additional-addtocart-box">
</div>
<button type="button" title="Add to Cart" class="button btn-cart" id="map-popup-button"><span><span>Add to Cart</span></span></button>
</form>
</div>
<script type="text/javascript">
        //<![CDATA[
            document.observe("dom:loaded", Catalog.Map.bindProductForm);
        //]]>
        </script>
</div>
<div class="map-popup-text" id="map-popup-text">Our price is lower than the manufacturer's "minimum advertised price." As a result, we cannot show you the price in catalog or the product page. <br><br> You have no obligation to purchase the product once you know the price. You can simply remove the item from your cart.</div>
<div class="map-popup-text" id="map-popup-text-what-this">Our price is lower than the manufacturer's "minimum advertised price." As a result, we cannot show you the price in catalog or the product page. <br><br> You have no obligation to purchase the product once you know the price. You can simply remove the item from your cart.</div>
</div>
</div>
</div>
<div class="col-left sidebar col-xs-12 col-sm-3"><div class="nav-container block first">
<div class="menu-icon-style">Categories</div>
<ul class="sf-menu-phone item">
<li class="level0 nav-1 first level-top"><a href="http://livedemo00.template-help.com/magento_47676/accessories.html" class="level-top"><span>Accessories</span></a></li><li class="level0 nav-2 level-top"><a href="http://livedemo00.template-help.com/magento_47676/cpus.html" class="level-top"><span>CPUs</span></a></li><li class="level0 nav-3 level-top"><a href="http://livedemo00.template-help.com/magento_47676/hard-drives.html" class="level-top"><span>Hard Drives</span></a></li><li class="level0 nav-4 level-top"><a href="http://livedemo00.template-help.com/magento_47676/keyboards-mice.html" class="level-top"><span>Keyboards / Mice</span></a></li><li class="level0 nav-5 last level-top parent"><a href="http://livedemo00.template-help.com/magento_47676/monitors.html" class="level-top"><span>monitors</span></a><ul class="level0"><li class="level1 nav-5-1 first"><a href="http://livedemo00.template-help.com/magento_47676/monitors/entertainment-networking.html"><span>A�er</span></a></li><li class="level1 nav-5-2 parent"><a href="http://livedemo00.template-help.com/magento_47676/monitors/dell.html"><span>Dell</span></a><ul class="level1"><li class="level2 nav-5-2-1 first last"><a href="http://livedemo00.template-help.com/magento_47676/monitors/dell/test-category.html"><span>Test Category</span></a></li></ul><strong></strong></li><li class="level1 nav-5-3 last"><a href="http://livedemo00.template-help.com/magento_47676/monitors/samsung.html"><span>Samsung</span></a></li></ul><strong></strong></li> </ul>
</div>
<div class="block block-list block-manufacturer">
<div class="block-title"><strong><span>Brands</span></strong><span class="toggle"></span></div><div class="block-content"><ul><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=24">Apple</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=25">D-Link</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=22">Epson</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=21">HP</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=31">Iomega</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=26">Linksys</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=20">MvixBOX</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=33">NAS</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=27">RangeMax</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=18">Roku</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=19">Seagate</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=15">Sony</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=23">TiVo</a></li><li class="item"><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/result/?manufacturer=13">Toshiba</a></li></ul></div></div>
<div class="block block-tags last_block">
<div class="block-title">
<strong><span>Popular Tags</span></strong>
<span class="toggle"></span></div>
<div class="block-content">
<ul class="tags-list">
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/5/" style="font-size:121.66666666667%;">Antivirus</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/4/" style="font-size:110%;">Apple</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/17/" style="font-size:75%;">AppleGames</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/7/" style="font-size:110%;">Business</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/1/" style="font-size:98.333333333333%;">Custom</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/9/" style="font-size:133.33333333333%;">Databases</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/10/" style="font-size:121.66666666667%;">Desktop</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/14/" style="font-size:110%;">Education</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/16/" style="font-size:121.66666666667%;">Games</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/8/" style="font-size:110%;">Productivity</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/12/" style="font-size:98.333333333333%;">Publishing</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/15/" style="font-size:98.333333333333%;">Reference</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/6/" style="font-size:110%;">Security</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/3/" style="font-size:133.33333333333%;">Software</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/13/" style="font-size:121.66666666667%;">Tools</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/11/" style="font-size:145%;">Web</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/tag/product/list/tagId/2/" style="font-size:121.66666666667%;">Whitebox</a></li>
</ul>
<div class="actions">
<a href="http://livedemo00.template-help.com/magento_47676/tag/list/">View All Tags</a>
</div>
</div>
</div>
<a class="banner_sidebar" href="http://livedemo00.template-help.com/magento_47676/cpus.html/">
<img src="/Templates/stealtech/CompStore/inc/col_right_callout.gif" alt="">
</a></div>
</div>
</div>
</div>
</div>
</div>
</div>

<div id="footer">
	<div class="footer-content" >
	<?php
		include TEMPLATE_DIR . "footer.php";
	?>
	<br clear="all">
	<div class="copyrights"><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a></div>
	</div> 
</div>

<script data-cfasync="false" type="text/javascript">/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('7830-582-10-3714');/*]]>*/</script><noscript>&lt;a href="https://www.olark.com/site/7830-582-10-3714/contact" title="Contact us" target="_blank"&gt;Questions? Feedback?&lt;/a&gt; powered by &lt;a href="//www.olark.com?welcome" title="Olark live chat software"&gt;Olark live chat software&lt;/a&gt;</noscript>
 
<script type="text/javascript">/* CloudFlare analytics upgrade */
</script>


<span style="display: none;"><iframe style="display: none !important" id="_olark_framesocket93785" src="/Templates/stealtech/CompStore/inc/storage.htm" onload="try{window._olark_framesocket93785()}catch(e){_olark_framesocket93785()}"></iframe></span><div id="habla_beta_container_do_not_rely_on_div_classes_or_names" class="habla-browser-chrome habla-desktop olrk-noquirks"><div id="hbl_operator_state_div" class="olrk-away"><div id="hbl_region" class="olrk-normal"><div id="habla_window_state_div" class=" olrk-state-compressed"><div id="habla_window_div" style="margin: 0px 20px; bottom: 0px; right: 0px; display: none; position: fixed;" class="habla_window_div_base hbl_pal_main_width olrk-fixed-bottom olrk-fixed-right "><div id="habla_compressed_div" style="display: block;"></div><div id="habla_panel_div" class="habla_panel_border hbl_pal_main_bg hbl_pal_panel_border hbl_pal_main_font_family hbl_pal_main_font_size " style="display: block;"><div id="habla_both_div" style="display: block;"><div id="habla_topbar_div" class="habla_topbar_div_normal hbl_pal_title_fg hbl_pal_title_bg habla_topbar_div_compressed "><a id="habla_sizebutton_a" class="habla_button habla_button_a_normal hbl_pal_header_font_size hbl_pal_main_font_family hbl_pal_button_bg hbl_pal_button_fg ">^</a><a id="habla_oplink_a" class="habla_oplink_a_normal hbl_pal_header_font_size hbl_pal_title_fg ">Contact us!</a></div></div><div id="habla_expanded_div" style="display: none;"><div id="habla_middle_div"><div id="habla_middle_wrapper_div"><div id="habla_conversation_div" class="hbl_panel habla_conversation_div hbl_pal_main_height hbl_pal_control_border hbl_pal_main_fg hbl_pal_main_bg   habla_conversation_message_on " style="height: 155px; overflow: hidden; display: none;"><span id="hbl_body_message">Questions? We'd love to chat!</span></div><div id="habla_offline_message_sent_div" class="hbl_panel habla_offline_message_sent_div hbl_pal_main_height hbl_pal_control_border hbl_pal_main_fg " style="display: none;">Thanks for your message!  We'll get back to you shortly.</div><div id="habla_offline_message_div" class="hbl_panel habla_offline_message_div hbl_pal_control_border hbl_pal_main_fg " style="display: block;"><span id="habla_offline_message_span">Live help is displayed for demo purposes only. To add it to your store please refer to the template documentation or <a href="http://www.olark.com/?r=ad8fbsj2">Olark Live chat official website. </a></span><div class="hbl_txt_wrapper"><textarea id="habla_name_input" name="habla_name_input" class="habla_wcsend_field habla_wcsend_input_pre habla_wcsend_input_normal hbl_pal_input_pre_fg hbl_pal_main_font_family hbl_pal_input_font_size hbl_pal_control_border " placeholder="click here and type your Name" style="height: 20px; display: block;"></textarea></div><div class="hbl_txt_wrapper"><textarea id="habla_offline_email_input" name="habla_offline_email_input" class="habla_wcsend_field habla_wcsend_input_pre habla_wcsend_input_normal hbl_pal_input_pre_fg hbl_pal_main_font_family hbl_pal_input_font_size hbl_pal_control_border " placeholder="click here and type your Email" style="height: 20px; display: block;"></textarea></div><div class="hbl_txt_wrapper"><textarea id="habla_offline_body_input" name="habla_offline_body_input" class="habla_wcsend_field habla_wcsend_input_pre habla_wcsend_input_normal hbl_pal_input_pre_fg hbl_pal_main_font_family hbl_pal_input_font_size hbl_pal_control_border " placeholder="We&#39;re not around but we still want to hear from you!  Leave us a note:" style="height: 70px; display: block;"></textarea></div><span id="habla_offline_error_span" class="habla_offline_error_span "></span><input id="habla_offline_submit_input" name="habla_offline_submit_input" type="submit" class="habla_offline_submit_input hbl_pal_offline_submit_fg hbl_pal_control_border hbl_pal_offline_submit_bg " value="Send" style="display: block;"><div id="habla_offline_clear_div" class="clear_style "></div></div><div id="habla_pre_chat_div" class="hbl_panel habla_pre_chat_div hbl_pal_main_height hbl_pal_main_fg " style="display: none; height: 155px;"><span id="habla_pre_chat_span">Hi, I am around, click 'start chatting' to contact me.</span><div class="hbl_txt_wrapper"><textarea id="habla_pre_chat_name_input" name="habla_pre_chat_name_input" class="habla_wcsend_field habla_wcsend_input_pre habla_wcsend_input_normal hbl_pal_input_pre_fg hbl_pal_main_font_family hbl_pal_input_font_size hbl_pal_control_border " placeholder="click here and type your Name" style="height: 20px; line-height: 20px;"></textarea></div><div class="hbl_txt_wrapper"><textarea id="habla_pre_chat_email_input" name="habla_pre_chat_email_input" class="habla_wcsend_field habla_wcsend_input_pre habla_wcsend_input_normal hbl_pal_input_pre_fg hbl_pal_main_font_family hbl_pal_input_font_size hbl_pal_control_border " placeholder="click here and type your Email" style="height: 20px; line-height: 20px;"></textarea></div><div class="hbl_txt_wrapper" style="display: none;"><textarea id="habla_pre_chat_phone_input" name="habla_pre_chat_phone_input" class="habla_wcsend_field habla_wcsend_input_pre habla_wcsend_input_normal hbl_pal_input_pre_fg hbl_pal_main_font_family hbl_pal_input_font_size hbl_pal_control_border " placeholder="click here and type your Phone" style="height: 20px; line-height: 20px; display: none;"></textarea></div><span id="habla_pre_chat_error_span"></span><input id="habla_pre_chat_submit_input" name="habla_pre_chat_submit_input" type="submit" class="habla_pre_chat_form_field habla_offline_submit_input hbl_pal_offline_submit_fg hbl_pal_control_border hbl_pal_offline_submit_bg " value="Click here to start chatting"><div id="habla_pre_chat_clear_div" class="clear_style "></div></div></div><form id="habla_chatform_form" action="http://livedemo00.template-help.com/magento_47676/#" method="GET" autocomplete="off" class="habla_chatform_form " style="display: none;"><div id="habla_input_div" class="habla_input_div "><div class="hbl_txt_wrapper"><textarea id="habla_wcsend_input" name="habla_wcsend_input" size="undefined" class="habla_wcsend_field habla_wcsend_input_pre habla_wcsend_input_normal hbl_pal_input_pre_fg hbl_pal_main_font_family hbl_pal_input_font_size hbl_pal_control_border " placeholder="Type here and hit enter to chat" disabled="" style="line-height: 20px; height: 20px; display: none;"></textarea></div></div></form></div><div style="font-family: helvetica, sans-serif; text-align: center; text-transform: uppercase; font-size: 9px; letter-spacing: 2px; font-weight: bold; padding: 3px 0px 5px !important; color: rgb(170, 170, 170) !important; clear: both;"><span style="display:none"><a id="hblink9" style="font-family: helvetica, sans-serif; text-transform: uppercase; font-size: 9px !important; letter-spacing: 2px; font-weight: bold; color: #e75917 !important;"></a>http://www.olark.com</span>Powered By <a style="font-family: helvetica, sans-serif; text-transform: uppercase; font-size: 9px !important; letter-spacing: 2px; font-weight: bold; color: #e75917 !important;" href="http://www.olark.com/welcome/?rid=7830-582-10-3714&rid=7830-582-10-3714&powered_f=1&utm_medium=widget&utm_campaign=powered_by_free&utm_source=7830-582-10-3714" id="hblink99" target="_blank" rel="nofollow">Olark</a></div></div></div><div id="habla_closed_div" style="display: none;"></div></div></div></div></div></div></body>
</html>