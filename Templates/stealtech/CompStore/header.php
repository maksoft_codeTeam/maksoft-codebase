<div class="header-container">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="header">
<div class="header-buttons">
<div class="header-button currency-list">
<a title="Currency" href="http://livedemo00.template-help.com/magento_47676/#">$</a>
<ul>
<li>
<a href="http://livedemo00.template-help.com/magento_47676/directory/currency/switch/currency/GBP/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/" title="GBP">British Pound Sterling - GBP</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/magento_47676/directory/currency/switch/currency/EUR/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/" title="EUR">Euro - EUR</a>
</li>
<li>
<a class="selected" href="http://livedemo00.template-help.com/magento_47676/directory/currency/switch/currency/USD/uenc/aHR0cDovL2xpdmVkZW1vMDAudGVtcGxhdGUtaGVscC5jb20vbWFnZW50b180NzY3Ni8,/" title="USD">US Dollar - USD</a>
</li>
</ul>
</div>
<div class="header-button lang-list">
<a title="Language" href="http://livedemo00.template-help.com/magento_47676/#">en</a>
<ul>
<li>
<a class="selected" href="http://livedemo00.template-help.com/magento_47676/?___store=english&___from_store=english" title="en_US">English</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/magento_47676/?___store=german&___from_store=english" title="de_DE">German</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/magento_47676/?___store=spanish&___from_store=english" title="es_ES">Spanish</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/magento_47676/?___store=russian&___from_store=english" title="ru_RU">Russian</a>
</li>
</ul>
</div>
</div>
<h1 class="logo"><strong>Gadget online</strong><a href="/Templates/stealtech/CompStore/inc/Home page.htm" title="Gadget online" class="logo"><img src="/Templates/stealtech/CompStore/inc/logo.gif" alt="Gadget online"></a></h1>
<div class="block-cart-header">
<h3>Cart:</h3>
<div class="block-content">
<div class="empty">
<div>0 item(s) - <span class="price">$0.00</span></div>
<div class="cart-content">
You have no items in your shopping cart. </div>
</div>
<p class="mini-cart"><strong>0</strong></p>
</div>
</div>
<div class="f-right">
<p class="welcome-msg">Welcome to our online store! </p>
<div class="clear"></div>
<form id="search_mini_form" action="http://livedemo00.template-help.com/magento_47676/catalogsearch/result/" method="get">
<div class="form-search">
<label for="search">Search:</label>
<input id="search" type="text" name="q" value="" class="input-text" autocomplete="off">
<button type="submit" title="Search" class="button"><span><span>Search</span></span></button>
<div id="search_autocomplete" class="search-autocomplete" style="display: none;"></div>
 
<script type="text/javascript">
        //<![CDATA[
            var searchForm = new Varien.searchForm('search_mini_form', 'search', '');
            searchForm.initAutocomplete('http://livedemo00.template-help.com/magento_47676/searchautocomplete/suggest/result/', 'search_autocomplete');
        //]]>
        </script>
</div>
</form>
<style>.ajaxsearch{border:solid #CCCCCC 1px}.ajaxsearch .suggest{background:#0A263D;color:#FFF}.ajaxsearch .suggest .amount{color:#FF0000}.ajaxsearch .preview{background:#ffffff}.ajaxsearch .preview a{color:#00B2CA}.ajaxsearch .preview .description{color:#0A263D}.ajaxsearch .preview img{float:left;border:solid 1px #CCC}.header .form-search .ajaxsearch li.selected{background-color:#fcfcfc}</style> <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="quick-access">
<a href="/Templates/stealtech/CompStore/inc/Home page.htm" title="" class="home-link-access"></a>
<ul class="links">
<li class="first"><a href="http://livedemo00.template-help.com/magento_47676/customer/account/" title="My Account">My Account</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/wishlist/" title="My Wishlist">My Wishlist</a></li>
<li class="top-car"><a href="http://livedemo00.template-help.com/magento_47676/checkout/cart/" title="My Cart" class="top-link-cart">My Cart</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/checkout/" title="Checkout" class="top-link-checkout">Checkout</a></li>
<li class=" last"><a href="http://livedemo00.template-help.com/magento_47676/customer/account/login/" title="Log In">Log In</a></li>
</ul>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
