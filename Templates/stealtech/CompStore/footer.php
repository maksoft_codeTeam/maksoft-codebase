<div class="footer-container">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="footer">
<p id="back-top" style="display: block;"><a href="http://livedemo00.template-help.com/magento_47676/#top"><span></span></a> </p>
<div class="footer-cols-wrapper">
<div class="footer-col footer-col-ex">
<h4>Information<span class="toggle"></span></h4>
<div class="footer-col-content">
<ul>
<li><a href="http://livedemo00.template-help.com/magento_47676/about-magento-demo-store">About Us</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/customer-service">Customer Service</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/template-settings">Template Settings</a></li>
<li class="last privacy"><a href="http://livedemo00.template-help.com/magento_47676/privacy-policy-cookie-restriction-mode">Privacy Policy</a></li>
</ul> <ul class="links">
<li class="first"><a href="http://livedemo00.template-help.com/magento_47676/catalog/seo_sitemap/category/" title="Site Map">Site Map</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/term/popular/" title="Search Terms">Search Terms</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/catalogsearch/advanced/" title="Advanced Search">Advanced Search</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/sales/guest/form/" title="Orders and Returns">Orders and Returns</a></li>
<li class=" last"><a href="http://livedemo00.template-help.com/magento_47676/contacts/" title="Contact Us">Contact Us</a></li>
</ul>
</div>
</div>
<div class="footer-col footer-col-ex">
<h4>Why buy from us<span class="toggle"></span></h4>
<div class="footer-col-content">
<ul>
<li><a href="http://livedemo00.template-help.com/magento_47676/#">Shipping &amp; Returns</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/#">Secure Shopping</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/#">International Shipping</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/#">Affiliates</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/#">Group Sales</a></li>
</ul>
</div>
</div>
<div class="footer-col footer-col-ex">
<h4>My account<span class="toggle"></span></h4>
<div class="footer-col-content">
<ul>
<li><a href="http://livedemo00.template-help.com/magento_47676/customer/account/login/">Sign In</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/checkout/cart/">View Cart</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/wishlist/">My Wishlist</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/#">Track My Order</a></li>
<li><a href="http://livedemo00.template-help.com/magento_47676/#">Help</a></li>
</ul>
</div>
</div> <div class="footer-col last footer-col-ex">
<div class="block block-subscribe">
<div class="block-title">
<strong><span>Newsletter</span></strong>
</div>
<form action="http://livedemo00.template-help.com/magento_47676/newsletter/subscriber/new/" method="post" id="newsletter-validate-detail">
<div class="block-content">
<div class="form-subscribe-header">
<label for="newsletter">Sign Up for Our Newsletter:</label>
</div>
<div class="input-box">
<input type="text" name="email" id="newsletter" title="Sign up for our newsletter" class="input-text required-entry validate-email form-control">
</div>
<div class="actions">
<button type="submit" title="Subscribe" class="button"><span><span>Go!</span></span></button>
</div>
</div>
</form>
<script type="text/javascript">
    //<![CDATA[
        var newsletterSubscriberFormDetail = new VarienForm('newsletter-validate-detail');
    //]]>
    </script>
</div>
<div class="clear"></div>
<div class="paypal-logo">
<a href="http://livedemo00.template-help.com/magento_47676/#" title="Additional Options" onclick="javascript:window.open(&#39;https://www.paypal.com/us/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside&#39;,&#39;paypal&#39;,&#39;width=600,height=350,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes&#39;); return false;"><img src="/Templates/stealtech/CompStore/inc/bnr_nowAccepting_150x60.gif" alt="Additional Options" title="Additional Options"></a>
</div>
</div>
<div class="clear"></div>
</div>
<address><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="????????, SEO ???????????" target="_blank">Netservice</a></address>
<div class="clear"></div>
  <div class="clear"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
