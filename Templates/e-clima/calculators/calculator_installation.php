<script language="javascript" type="text/javascript">
	$j(document).ready(function(){
		
		$j('#room_lenght').change(function(){
			 //calculate();
			 })
			 
		$j('#room_width').change(function(){
			 //calculate();
			 })
		
		$j('#room_height').change(function(){
			 //calculate();
			 })
			 
		$j('#button_calculate').click(function(){
			calculate();
			})	 		
		})
		
	function calculate()
		{
			var La = parseFloat($j('#La').val());
			var Lb = parseFloat($j('#Lb').val());
			var Lc = parseFloat($j('#Lc').val());
			var Ld = parseFloat($j('#Ld').val());
			var Ne = parseInt($j('#Ne').val());
			var Lf = parseFloat($j('#Lf').val());
			var Ng = parseInt($j('#Ng').val());
			
			var Lextra = 0;
			var price = 0;
			
			var C = 25;
			var D = 12;
			var E = 15;
			var F = 4;
			var G = 45;
			var H = 27.50;
			
			Lextra = (La+Lb-3);	
			if(Lextra<0) Lextra = 0;
			price = Math.round((Lextra*H+Lc*C+Ld*D+Ne*E+Lf*F+Ng*G)*100)/100;
			
			$j('#price_text span').text(price);
			$j('#price').val(price);
			$j('#calculations').show();
			/*
			if(lenght >0 && width > 0 && height >0)
				{
					$j('#calculations').show();
					$j('#error_message').hide();
					$j('input').removeClass("error");
				}
			else
				{
					if(lenght == "") $j('#room_lenght').addClass("error");
					if(width == "") $j('#room_width').addClass("error");
					if(height == "") $j('#room_height').addClass("error");
					$j('#error_message').html("���������� ������ �� ������������");
					$j('#error_message').show();
				}
			*/
		}
</script>
<div id="calculator">
	<fieldset><legend>����� �� �������</legend>
    	<table cellpadding="0" border="0" cellspacing="1">
        	<tr>
            	<td class="short">������������� ������� ����� ��������-������ ����
                <td><div class="text_field short" title="�������� ������������ ����� ���������� � �������� ���� �� ����������� (������������� �� ���� ��������)">
                        <input name="La" type="text" id="La" value="0">[m]
                    </div>
            </tr>
            <tr>        
                <td class="short">������������� ����������� ����� ��������-������ ����
                <td><div class="text_field short" title="�������� ��������� ��� ���������� ����� �������� � �������� ���� (������������� �� ���� ��������)">
                        <input name="Lb" type="text" id="Lb" value="0">[m]
                    </div>
            </tr>
            <tr>
            	<td class=" short">��������� �� ������� ����� � �������� �����
                <td><div class="text_field short" title="�������� ������� (� �����) �� ����� �� ����� �����, ����� ������� �� ���� ������� � ������������ ������� �� ����� (���������������� �� ��������� �� � �������� � ������)">
                        <input name="Lc" type="text" id="Lc" value="0">[m]
                    </div>
            </tr>
           <tr>
            	<td class="short">�������� �� ������� ����� � PVC ����� (���. ������)
                <td><div class="text_field short" title="�������� ������� (� �����) �� ����� �� ����� �����, ����� ������� �� ���� �������� � PVC ����� (PVC ������ �� ������ �������). � ������ �� �������� - �����, ����� �� ������, ������, ��������� �� ������ � �����)">
                        <input name="Ld" type="text" id="Ld" value="0">[m]
                    </div>
            </tr>
            <tr>
            	<td class="short">����������� ���� ������ �� ���� �����
                <td><div class="text_field short" title="�������� ���� �� ������� ���� ����� ������ �� �� ������� (� �������� ������ �������  ����� ����� ��������� ��������� ���� ����� ���������)">
                        <input name="Ne" type="text" id="Ne" value="0">[m]
                    </div>
            </tr>
            <tr>
            	<td class="short">�������� �� ��������� �����
                <td><div class="text_field short" title="�������� ������� (� �����) �� ����������� ����� �� ���������, ����� ������� �� ���� ������ � ������������ ������� �� ����� (���������������� �� ��������� �� � �������� � ������)">
                        <input name="Lf" type="text" id="Lf" value="0">[m]
                    </div>
            </tr>
            <tr>
            	<td class="short">������ �� ��� �����
                <td><div class="text_field short" style="text-align:left"  title="������ �� ��� ����� �� ������ ������ ��� ����������� �� ��������� �� ������� �� 1 ����  - ����� �����, �������������� ������, ������ �� �������� ����; 2 ���� - �� ������� ������ �������� � ������ ����">
                        <select name="Ng" id="Ng">
                        	<option value="2">��</option>
                            <option value="1" selected>��</option>
                        </select>
                    </div>
            </tr>
       </table>
            <a href="javascript: void()" id="button_calculate" class="rounded">�������</a>
   	<div id="error_message" class="message_error" style="display:none; clear:both;"></div>
   	</fieldset>
	<fieldset id="calculations" style="display:none"><legend>����������</legend>
            <div id="price_text" class="text_field long"><label for="price">����:</label><b><span>0</span></b> ��.<input type="hidden" name="price" id="price"></div>
     </fieldset>
</div>