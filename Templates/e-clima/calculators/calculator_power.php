<script language="javascript" type="text/javascript">
	$j(document).ready(function(){
		
		$j('#room_lenght').change(function(){
			 //calculate_volume();
			 })
			 
		$j('#room_width').change(function(){
			 //calculate_volume();
			 })
		
		$j('#room_height').change(function(){
			 //calculate_volume();
			 })
			 
		$j('#button_calculate').click(function(){
			calculate_volume();
			})	 		

		$j('#button_power_calculate_request').click(function(){
			$j("#power_request").submit();
			})
		$j('#reset_calculation_data').click(function(){
			$j('#calculations').toggle();
			$j('#calculation_data').toggle();			
			})
		})
		
	function calculate_volume()
		{
			var lenght = $j('#room_lenght').val();
			var width = $j('#room_width').val();
			var height = $j('#room_height').val();
			volume = Math.round(lenght * width *height*100)/100;
			hCapacity = (volume * 50) / 1000;
			cCapacity = (volume * 40) / 1000;
			$j('#volume_text span').text(volume);
			$j('#heat_capacity_text span').text(hCapacity);
			$j('#cool_capacity_text span').text(cCapacity);

			$j('#volume').val(volume);
			$j('#heat_capacity').val(hCapacity);
			$j('#cool_capacity').val(cCapacity);
			
			if(lenght >0 && width > 0 && height >0)
				{
					//$j('#calculations').show();
					$j('#error_message').hide();
					$j('input').removeClass("error");
					$j('#calculations').toggle();
					$j('#calculation_data').toggle();
				}
			else
				{
					if(lenght == "") $j('#room_lenght').addClass("error");
					if(width == "") $j('#room_width').addClass("error");
					if(height == "") $j('#room_height').addClass("error");
					$j
					$j('#error_message').html("���������� ������ �� ������������");
					$j('#error_message').show();
				}
		}
		
</script>
<div id="calculator">
	<fieldset class="rounded" id="calculation_data"><legend>����� �� �����������</legend>
    <form>
            <div class="text_field short">
                <label for="room_lenght">�������</label>
                <input type="text" name="room_lenght" id="room_lenght">[m]
            </div>
            <div class="text_field short">
                <label for="room_width">������</label>
                <input type="text" name="room_width" id="room_width">[m]
            </div>
            <div class="text_field short">
                <label for="room_height">��������</label>
                <input type="text" name="room_height" id="room_height">[m]
            </div>
            <br clear="all">
            <hr>
            <a href="javascript: void()" id="button_calculate" class="rounded">�������</a>
   	<div id="error_message" class="message_error" style="display:none; clear:both;"></div>
   	</form>
    </fieldset>
	<fieldset class="rounded" id="calculations" style="display:none;"><legend>����������</legend>
            <a href="javascript: void(0)" class="next_link" id="reset_calculation_data">����� ���� �������</a>
            <br clear="all"><br clear="all">
            <div id="volume_text" class="text_field long"><label for="volume">�������� ����:</label><b><span>0</span></b> m<sup>3</sup><input type="hidden" name="volume" id="volume"></div>
            <form name="request" id="power_request" method="get" action="page.php">
                <input type="hidden" name="n" value="176191">
                <input type="hidden" name="SiteID" value="<?=$SiteID?>">
            	<div id="heat_capacity_text" class="text_field long"><label for="heat_capacity">��������� �������� �������:</label><b><span>0</span></b> [kW]<input type="hidden" name="hCapacity" id="heat_capacity"></div>
            	<div id="cool_capacity_text" class="text_field long"><label for="cool_capacity">��������� ������� �������:</label><b><span>0</span></b> [kW]<input type="hidden" name="cCapacity" id="cool_capacity"></div>
                <hr>
                <a href="javascript: void(0);" class="button_submit rounded" id="button_power_calculate_request">���������� ��������</a>
            </form>
     
     </fieldset>
</div>