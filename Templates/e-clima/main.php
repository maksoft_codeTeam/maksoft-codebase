<?php
	//page options
	$pOptions = $o_page->get_pOptions();
?>
<script language="javascript" type="text/javascript">
	$j(document).ready(function(){
		
		$j('#box_search .content').hide();
		$j('#extra_content').hide();
		
		$j('#box_search_toggle, #top_menu_search').click(function(){
			$j('#box_search .content').toggle(100);
			$j('#extra_content').toggle(100);
			$j('#box_search_toggle').toggleClass("up");
			
			if($j('#extra_content').is(":visible"))
				{
					$j('#extra_content input').attr("disabled", "");
					$j('#extra_content select').attr("disabled", "");
				}
			
			})
		//show-hide single product request form
		$j('#button_request').click(function(){
			$j('#single-product-request').slideToggle()
		});
		})
</script>
<div id="main">
    <div class="column left">
    	<?php include DIR_TEMPLATE . "column_left.php"; ?>
    </div>
    <div class="shadow left"></div>
    <div id="pageContent">
    <div class="bg rounded border">
		<?php include DIR_TEMPLATE . "boxes/box_search.php"; ?>


        <div class="pContent">
        <h1 class="head_text"><?=$o_page->get_pTitle("strict")?></h1>
        <?php
            if($o_page->get_pImage())
			{
				$o_page->print_pImage(NULL, "class=\"main_image\" align=\"left\"");
			
			if(count($pOptions) > 0)
				{
					echo "<ul class=\"pOptions\">";
					for($i=0; $i<count($pOptions); $i++)
						echo "<li><span class=\"option_label\">".$pOptions[$i]['option_title']."</span><span class=\"option_value ".$pOptions[$i]['option_name']."\">".$pOptions[$i]['po_value']." ".$pOptions[$i]['option_unit']."</span></li>";
					echo "</ul><br clear=\"all\">";	
					}
			}
			$o_page->print_pText();
			//$o_page->print_pContent();
            $o_page->print_pSubContent();
			//single request form
			if(count($pOptions) > 0)
				{
					echo "<br clear=\"all\">";
					include_once "web/forms/e-clima/product_request.php";
				}
			
			eval($o_page->get_pPHPcode());
			include $o_page->get_pURL();
        ?>
        <br clear="all">
        </div>
        </div>
  
        <?php
			//other links
			include "web/admin/pageURLs/other_links.php"; 
		?>
        <div id="nav_links"><?php $o_page->print_pNavigation(); ?></div>
    </div>
    <div class="shadow right"></div>
    <div class="column right">
    	<?php include DIR_TEMPLATE . "column_right.php"; ?>
    </div>
</div>