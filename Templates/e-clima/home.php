<script language="javascript" type="text/javascript">
	$j(document).ready(function(){
		
		$j('#box_search_toggle').click(function(){
			$j('#extra_content').toggle();
			$j('#box_promo').toggle();
			$j('#box_search_toggle').toggleClass("up");
			
			$j('#advanced_search').toggle();
		
			if($j('#extra_content').is(":visible"))
				{
					$j('#extra_content input').attr("disabled", "");
					$j('#extra_content select').attr("disabled", "");
				}
				
			})
			
			$j('#advanced_search').click(function(){
					$j('#advanced_search').toggle();
					$j('#extra_content').toggle();
					$j('#box_promo').toggle();
					$j('#box_search_toggle').toggleClass("up");
					if($j('#extra_content').is(":visible"))
						{
							$j('#extra_content input').prop("disabled", false);
							$j('#extra_content select').prop("disabled", false);
						}
				});
		})
</script>
<div id="home">
	<div class="column left">
        <?php include DIR_TEMPLATE . "column_left.php"; ?>
    </div>
    <div class="shadow left"></div>
    <div id="pageContent">
    	<div class="bg rounded border">
        <?php include DIR_TEMPLATE . "boxes/box_search.php"; ?>


        <!--<h1 class="head_text"><?=$o_page->get_pTitle("strict")?></h1>//-->
        <?php
            $o_page->print_pContent();
            //$o_page->print_pSubContent();
            eval($o_page->get_pPHPcode());
          
        ?>
        </div>
        <div id="box_promo" class="rounded border">
        	<?php include DIR_TEMPLATE . "boxes/box_promo.php"; ?>
        </div>
        <?php
			//other links
			include "web/admin/pageURLs/other_links.php"; 
		?>
        <div id="nav_links"><?php $o_page->print_pNavigation(); ?></div>
    </div>
    <div class="shadow right"></div>
    <div class="column right">
    <?php include DIR_TEMPLATE . "column_right.php"; ?>
    </div>
</div>