<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title><?php echo("$Title");?></title>
<?php
//page_options
$labels = array(
						"price"=>array("����", "��."), 
						"model"=>array("�����", ""), 
						"power"=>array("�������", "kW"), 
						"compressor_type"=>array("��� ���������", ""),
						);
								
$filter_values = array(
						"price"=> array("0 - 499", "500 - 899", "900 - 1299", "1300 - 1899", "> 1900 "),
						"model"=> array("Fujitsu", "Daikin", "Toshiba", "Panasonic", "Mitsubishi"),
						"power"=> array("< 2.50", "2.50 - 5.00", "5.00 - 9.00", "9.00"),
						"compressor_type"=> array("7660", "8660", "9660", "9800")
						); 

//EXTRA FILTER
//product_categories
$p_categories = array("- ������ ��������� -", "��������� 1", "��������� 2");

//product_types
$p_types = array("- ������ ������ -", "������ ���", "����� ���", "������� ���");

//product_classes
$p_classes = array("- ������ ������� -", "�", "��", "���");

//product_condition
$p_conditions = array("���", "����� ��������");

//payment types
$p_payments = array("����������", "������", "���", "�� ������ ���");

$labels_extra = array(
						"category"=>array("���������", ""),
						"type"=>array("��� �� ���������", ""),
						"class"=>array("�������� ����", ""),
						"condition"=>array("���������", ""),
						"payment"=>array("����� �� �������", "")
						
						);
$filter_values_extra = array(
						"category"	=>$p_categories,
						"type"		=>$p_types,
						"class"		=>$p_classes,
						"condition"	=>$p_conditions,
						"payment"	=>$p_payments
						);

//$product_filter = array();
if(isset($_GET['price']))
	$product_filter['price'] = $_GET['price'];

if(isset($_GET['model']))
	$product_filter['model'] = $_GET['model'];
	
if(isset($_GET['power']))
	$product_filter['power'] = $_GET['power'];
	
if(isset($_GET['compressor_type']))
	$product_filter['compressor_type'] = $_GET['compressor_type'];	
	
session_register("product_filter", $product_filter);

if($clear_filter)
	session_unregister("product_filter");


include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/e-clima/");
define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/Templates/e-clima/images/");
//define("DIR_STYLE","css/���_��_���������/base_style.css");
define("DIR_MODULES","web/admin/modules");

$o_site->print_sConfigurations();

//top menu buttons
$top_menu = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 4", "p.toplink=0");

//latest news
$news = $o_page->get_pSubpages($o_site->_site['news'], "p.sort_n ASC LIMIT 5");

//vip links (promo box)
$vip_links = $o_page->get_pSubpages(0, "p.sort_n ASC LIMIT 3", "p.toplink=4 AND im.image_src != '' ");

//page conditions
$p_conditions = $o_page->get_page(176285);

//page calculators
$p_calculators[0] = $o_page->get_page(176287);
$p_calculators[1] = $o_page->get_page(176288);

//products
$product_models = $o_page->get_pSubpages(176197, "p.sort_n ASC LIMIT 5");

?>
<script language="javascript" type="text/javascript" src="lib/jquery/easyslider/easyslider1.7.js"></script>
<link href="http://www.maksoft.net/Templates/e-clima/layout.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/e-clima/base_style.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/e-clima/custom_form.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/e-clima/easyslider.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="http://www.maksoft.net/Templates/e-clima/images/favicon.ico" type="image/x-icon">
</head>
<body>
<div id="site_container">
    <div id="page_container">
        <div id="header">
            <?php include "header.php"; ?>
        </div>
        <?php
            if($o_page->_page['SiteID'] == 1)
                include "admin.php";
            else
            if($o_page->n == $o_site->get_sStartPage())
                include "home.php";
            else include "main.php";
        ?>


    </div>
    <br clear="all">
    <div id="footer">
          <?php include "footer.php"; ?>
    </div>
</div>
</body>
</html>
