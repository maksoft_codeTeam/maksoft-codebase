<div id="special_menu">
    <a href="<?=$p_conditions['page_link']?>" class="rounded"><p class="condition"><?=$p_conditions['Name']?></p></a>
    <a href="<?=$p_calculators[0]['page_link']?>" class="rounded"><p class="calculator"><?=$p_calculators[0]['Name']?></p></a>
    <a href="<?=$p_calculators[1]['page_link']?>" class="rounded"><p class="calculator"><?=$p_calculators[1]['Name']?></p></a>
</div>
<?php
	if($o_page->n == $o_site->_site['StartPage'])
		{
			//print news
			?>
			<div id="box_news">
            	<img src="<?=DIR_TEMPLATE_IMAGES?>label_news.png" class="label" alt="">
                <?php
					echo "<p>";
					$o_page->print_pImage(140, "", "", $news[0]['n']);
					$o_page->print_pName(true, $news[0]['n']);
					echo cut_text(strip_tags($news[0]['textStr']));
					echo "</p>";
				?>
			</div>
			<?php
		}
		else
		{
			//print menu	
			?>
			<div id="menu">
				<?php
					//print parent page name
					if($o_page->_page['ParentPage'] != $o_site->_site['StartPage'])
						{
							echo "<h2>";
							$o_page->print_pName(false, $o_page->_page['ParentPage']);
							echo "</h2>";
							echo "<img src=\"".DIR_TEMPLATE_IMAGES."gradient.jpg\">";
						}
						
					$links = $o_page->get_pSubpages($o_page->_page['ParentPage']);
					echo "<br>";
					for($i=0; $i<count($links); $i++)
						{
							$class = "";
							if($o_page->n == $links[$i]['n']) $class = " selected";
							echo "<a href=\"".$links[$i]['page_link']."\" title=\"".$links[$i]['title']."\" class=\"button rounded$class\">".$links[$i]['Name']."</a>";
						}
					echo "<br>";
				?>
			</div>
			<?php
		}
?>