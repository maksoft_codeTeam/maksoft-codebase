<?php
//get tags from all subpages or all site pages
function getTags($n = NULL)
{
	global $o_site;

	$tags_array = array();
	
	if($n == NULL) 
			$tags_query = mysqli_query("SELECT * FROM pages WHERE SiteID = '".$o_site->SiteID."' AND tags !='' ORDER by RAND()");
	else
			$tags_query = mysqli_query("SELECT * FROM pages WHERE ParentPage = '".$n."' AND SiteID = '".$o_site->SiteID."' AND tags !='' ORDER by RAND()");
			
	while($tag_links = mysqli_fetch_array($tags_query))
		{
			$split_tags = explode(",", $tag_links['tags']);
			$tags_array = array_merge($tags_array, $split_tags);
		}
		$tags_array = array_unique($tags_array);
		$tags_array_compact = array();
		for($i=0; $i<count($tags_array); $i++)
				if(!empty($tags_array[$i]))
						array_push($tags_array_compact, $tags_array[$i]);
		return $tags_array_compact;
}
?>