<?php
define("BASE_URL", "https://maksoft-webservices.herokuapp.com/rent/vehicle/api");


function post($request, $url, $data, $headers=array())
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0';

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $response = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close ($ch);
    return (object) array(
        'data' => json_decode($response),
        'status' => $status
    );
}


function get_token($request, $data, $headers=array()){
    $url = BASE_URL."/auth/";
    return post($request, $url, $data, $headers=array());
}

function register($request, $data, $headers=array())
{
    $url = BASE_URL.'/client/';
    return post($request, $url, $data, $headers=array());
}

function save_trip($request, $data, $headers=array())
{
    $url = BASE_URL.'/trips/';
    return post($request, $url, $data, $headers=array());
}
