<?php
namespace Maksoft\Rent\Bus\Form;

use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\PasswordField;
use \Maksoft\Form\Fields\PhoneField;
use \Maksoft\Form\Fields\SubmitButton;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\DateField;
use Maksoft\Rent\Bus\Settings\Base as BaseSettings;
use Maksoft\Rent\Bus\Settings\Base;

class DeleteOrder extends DivForm 
{
    private $_em;
    public function __construct($em, $post_data=null)
    {
        $this->_em = $em;

        $this->order_id = HiddenField::init();
        $this->order_id->add_validator(new \Maksoft\Form\Validators\Integerish());

        $this->action = HiddenField::init()
            ->add("value", "delete");

        $this->submit = SubmitButton::init()
            ->add("value", "Изпрати");
        parent::__construct($post_data);
    }

    public function save()
    {
        $orderRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Order');
        $id = $_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT]['id'];
        try{
            $order = $orderRepository->findOneBy(array("id" => $this->order_id->value, "client_id" => $id));
            $this->_em->remove($order);
            $this->_em->flush();
            return array('text' => '������� �������� ��������� � ����� '.$this->order_id->value.'!', 'code' => 'success');
        } catch (\Exception $e) { 
        }
        return array('text' => '������ ����� �� �������� ���� �������!', 'code' => 'error');

    }
}
