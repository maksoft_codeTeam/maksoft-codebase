<?php
namespace Maksoft\Rent\Bus\Form;
use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\SubmitButton;
use \Maksoft\Form\Validators\EmailValidator;

class CheckIfRegistered extends DivForm
{
    public function __construct($em, $form_data=null)
    {
        $this->_em = $em;
        $this->email = EmailField::init()
            ->add("required", True)
            ->add_validator(EmailValidator::init());

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "check_registration");

        $this->submit = SubmitButton::init()
            ->add("style", "margin-top:2%")
            ->add("class" , "btn waves-effect waves-light");
        
        parent::__construct($form_data);
    }

    public function save()
    {
        $clientRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Client');
        if($client = $clientRepository->findOneByEmail($this->email->value)){
            $this->_em->detach($client);
            return  array("text" => "Съществуващ потребител", "code" => "success");
        }
        unset($this->_em);
        return  array("text" => "Няма регистриран потребител с имейл адрес ".$this->email->value, "code" => "error");
    }
}
