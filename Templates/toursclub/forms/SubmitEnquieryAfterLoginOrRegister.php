<?php
namespace Maksoft\Rent\Bus\Form;
use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\SubmitButton;
use Maksoft\Rent\Bus\Settings\Base as BaseSettings;


class SubmitEnquieryAfterLoginOrRegister extends DivForm
{
    private $_em;
    public function __construct($em, $post_data=null)
    {
        $this->_em = $em;

        $this->submit = SubmitButton::init()
            ->add("value", "��������� �����������")
            ->add("style", "margin-top:1%")
            ->add("class" , "btn waves-effect waves-light");

        $this->action = HiddenField::init()
            ->add("value", "complete_order_after");

        parent::__construct($post_data);
    }

    private function days($d1, $d2)
    {
        return floor((strtotime($d1) - strtotime($d2)) /(60*60*24));
    }

    public function save()
    {
        $enq_data =  $_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT];
        $extrasRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Extras');
        $extras = $extrasRepository->findBy(array("id" =>$enq_data['extras']));
        $clientRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Client');
        $client = $clientRepository->findOneById($_SESSION['bus']['id']);
        $order = new \Maksoft\Rent\Bus\Order();
        $order->setClient($client);
        foreach($extras as $extra){
            $order->addExtra($extra);
        }
        $order->setFromCity($enq_data['from_city']);
        $order->setDestination($enq_data['to_city']);
        $order->setTwoWay(0);
        $order->setDeparature($enq_data['from_date']);
        $order->setArrival($enq_data['to_date']);
        $order->setPersons($enq_data['persons']);
        $order->setChildrens($enq_data['childrens']);
        $order->setDays($this->days($enq_data['to_date'], $enq_data['from_date']));
        $order->setCompleted(False);
        $this->_em->persist($order);
        $this->_em->flush();
        $id = $order->getId();
        $msg = \Maksoft\Rent\Message\Message::init("successfullOrderEmail", "email/successfull_order.html", $id, 30);
        $sender = new \Maksoft\Rent\Message\Pusher();
        $sender->connect();
        $sender->publish($msg);
        unset($_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT]);
        return array('text' => '������ ��������� � ���������. ���� ������� ������ �� �������� ����� � ����������. ���������� ��!', 'code' => 'success');
    }
}
