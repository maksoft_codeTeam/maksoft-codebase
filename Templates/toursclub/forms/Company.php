<?php
namespace Maksoft\Rent\Bus\Form;
use Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\HiddenField;
use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\PhoneField;
use Maksoft\Form\Fields\PasswordField;
use Maksoft\Form\Fields\EmailField;
use Maksoft\Form\Fields\SubmitButton;


class Company extends DivForm
{
    private $_em;

    public function __construct($em, $post_data=null)
    {
        $this->_em = $em;

        $class = "form-control";

        $this->name = TextField::init()
            ->add("name", "name")
            ->add("class", $class)
            ->add("label", "��� �� �����:")
            ->add("required", True);

        $this->city = TextField::init()
            ->add("name" , "city")
            ->add("class" , $class)
            ->add("label" , "����")
            ->add("required" , True);

        $this->address = TextField::init()
            ->add("name" , "address")
            ->add("class" , $class)
            ->add("label" , "�����:")
            ->add("required" , True);

        $this->owner = TextField::init()
            ->add("name" , "owner")
            ->add("class" , $class)
            ->add("label" , "���")
            ->add("required" , True);

        $this->phone = PhoneField::init()
            ->add("name" , "phone")
            ->add("class" , $class)
            ->add("label" , "�������")
            ->add("required" , True);

        $this->email = EmailField::init()
            ->add("name" , "email")
            ->add("class" , $class)
            ->add("label" , "����� �� �������")
            ->add("required" , True);

        $this->vat = TextField::init()
            ->add("name" , "vat")
            ->add("class" , $class)
            ->add("label" , "���")
            ->add("required" , True);

        $this->license = TextField::init()
            ->add("name" , "license")
            ->add("class" , $class)
            ->add("label" , "������")
            ->add("required" , True);

        $this->password = PasswordField::init() 
            ->add("class" , $class)
            ->add("label", "������")
            ->add("required", True);

        $password_max_lenght = 15;
        $maxLenghtValidator = new \Maksoft\Form\Validators\MaxLength($password_max_lenght);
        $maxLenghtValidator->setMsg( sprintf("������������ ������� �� �������� �� ������ �� ���������  %s �������.", $password_max_lenght + 1));
        $this->password->add_validators( $maxLenghtValidator );

        $password_min_lenght = 5;

        $minLenghtValidator = new \Maksoft\Form\Validators\MinLength($password_min_lenght);
        $minLenghtValidator->setMsg(sprintf("����������� ������� �� �������� ������ �� ���� ������� %s �������.", $password_min_lenght + 1));
        $this->password->add_validators( $minLenghtValidator );

        $hasDigitValidator = new \Maksoft\Form\Validators\HasDigit() ;
        $hasDigitValidator->setMsg("�������� ������ �� ������� �����") ;
        $this->password->add_validators( $hasDigitValidator );

        $hasUpperCaseValidator = new \Maksoft\Form\Validators\HasUpperCase() ;
        $hasUpperCaseValidator ->setMsg("�������� ������ �� ��� ���� ���� ������ �����!");
        $this->password->add_validators( $hasUpperCaseValidator );

        $this->password_repeated = PasswordField::init() 
            ->add("class" , $class)
            ->add("label", "��������� ��������")
            ->add("required", True);

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "company_register");

        $this->submit = SubmitButton::init()
            ->add("style", "margin-top: 3%;")
            ->add("class", "btn btn-primary btn-lg btn-block")
            ->add("value", "�����������");
        parent::__construct($post_data);
    }

    private function checkEntityManager()
    {
        if (!$this->_em->isOpen()) {
            $this->_em = $this->_em->create(
                $this->_em->getConnection(),
                $this->_em->getConfiguration()
            );
        }
    }

    protected function validate_password()
    {
        if($this->password->value === $this->password_repeated->value){
            return True;                        
        }
        throw new \Exception("�������� �� ��������! ����, �������� ������.");
    }

    public function save()
    {
        $this->checkEntityManager();
        $company = new \Maksoft\Rent\Bus\Company();
        $company->setName($this->name->value);
        $company->setCity($this->city->value);
        $company->setAddress($this->address->value);
        $company->setOwner($this->owner->value);
        $company->setPhone($this->phone->value);
        $company->setEmail($this->email->value);
        $company->setVat($this->vat->value);
        $company->setLicense($this->license->value);
        $company->setPassword($this->password->value);
        $company->createdAt();
        try { 
            $this->_em->persist($company);
            $this->_em->flush();
            $id = $company->getId();
            $msg = \Maksoft\Rent\Message\Message::init("successfullRegisterEmail", "email/successfull_registration.html", $id, 30);
            $sender = new \Maksoft\Rent\Message\Pusher();
            $sender->connect();
            $sender->publish($msg);
            $logged_at = time();
            return array(
                "text" => "������� ����������� ".$company->getName(),
                 "code" => "success",
                 "session" => array(
                    "name" => "company",
                    "id" => $company->getId(),
                    "email" => $company->getEmail(),
                    "logged_at" => $logged_at,
                    "expire" => $logged_at + 3600,
            ));
        } catch (\Exception $e){
            return array("text" => "���� ��� ���������� �������� � ���� ������/���", "code" => "error");
        }
    }
}
