<?php
namespace Maksoft\Rent\Bus\Form;
use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\SubmitButton;
use \Maksoft\Form\Validators\EmailValidator;

use Maksoft\Rent\Bus\Settings\Base as BaseSettings;


class ShowOrder extends DivForm
{
    public function __construct($em, $form_data=null)
    {
        $this->_em = $em;

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "order-details");

        $this->order_id = HiddenField::init()
            ->add("name", "order_id");

        $this->submit = SubmitButton::init()
            ->add("style", "margin-top:2%")
            ->add("class" , "btn waves-effect waves-light");
        
        parent::__construct($form_data);
    }

    protected function validate_order_id()
    {
        $orderRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Order');
        $order_exist = $orderRepository->findOneBy(array("client_id" => $_SESSION['bus']['id'], "id" => $this->order_id->value));
        if($order_exist){
            return True;
        }
        throw new \Exception("���� ������ �������");
    }

    public function save()
    {
        $_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_ORDER]['id'] = $this->order_id->value;
        return array('text' => '�������� ���������� �� ������� N:'.$this->order_id->value, 'code' => 'success');
    }
}
