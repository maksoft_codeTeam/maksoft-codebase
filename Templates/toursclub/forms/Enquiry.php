<?php
namespace Tours\Forms;
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Field;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\DivForm;



class Enquiry extends DivForm
{
    public function __construct($post_data=null)
    {
        $this->from_city = Text::init()
            ->add("class" , "validate")
            ->add("placeholder", "sliven")
            ->add("label" , "from city:")
            ->add("required", True);

        $this->to_city = Text::init()
            ->add("class" , "validate")
            ->add("label" , "to_city:")
            ->add("placeholder", "sofia")
            ->add("required", True);

        $this->from_date = Text::init()
            ->add("class" , "datepicker")
            ->add("label", "deparature date:")
            ->add("value" , date('Y-m-d', time()));

        $this->to_date = Text::init()
            ->add("class" , "datepicker")
            ->add("label", "arrival date:")
            ->add("value" , date('Y-m-d', time()));

        $this->persons = Integer::init()
            ->add("class" , "validate")
            ->add("value" , 1)
            ->add("label", "how many persons:")
            ->add("placeholder" , 1);

        $this->childrens = Integer::init()
            ->add("value" , 1)
            ->add("label", "how many childrens:")
            ->add("class" , "validate");

        $this->twoway = Radio::init()
            ->add("label", "is two way trip")
            ->add("value", false)
            ->add("data", array(true=>'yes', false=>'no'))
            ->add("class" , "validate");

        $this->extras = Checkbox::init()
            ->add("data", range(1,10))
            ->add("class" , "validate");

        $this->submit = Submit::init()
            ->add("class" , "btn waves-effect waves-light");

        $this->action = Hidden::init()
            ->add("value", "complete_order");

        parent::__construct($post_data);
    }
}
