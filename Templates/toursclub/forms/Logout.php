<?php
namespace Maksoft\Rent\Bus\Form;

use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\PasswordField;
use \Maksoft\Form\Fields\PhoneField;
use \Maksoft\Form\Fields\SubmitButton;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\DateField;

use Maksoft\Rent\Bus\Settings\Base as BaseSettings;


class Logout extends DivForm 
{
    private $_em;

    public function __construct($em, $post_data=null)
    {
        $this->_em = $em;

        $this->action = HiddenField::init()
            ->add("value", "logout");

        $this->submit = SubmitButton::init()
            ->add("value", "�����")
            ->add("style", "margin-top:1%")
            ->add("class" , "btn waves-effect waves-light");

        parent::__construct($post_data);
    }

    public function save()
    {
        unset($_SESSION[BaseSettings::APP_PREFIX]);
        return array('text' => '��� ��������� �������', 'code' => 'success');
    }
}
