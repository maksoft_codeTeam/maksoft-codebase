<?php
namespace Maksoft\Rent\Bus\Form;
use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\SubmitButton;
use \Maksoft\Form\Validators\EmailValidator;


use \Jokuf\Flash\SuccessMessage;
use \Jokuf\Flash\Notifier;
use \Jokuf\Flash\Type;
use Maksoft\Rent\Bus\Settings\Base as BaseSettings;


class BackToAllOrders extends DivForm
{
    public function __construct($em, $form_data=null)
    {
        $this->_em = $em;

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "viewAllorders");

        $this->submit = SubmitButton::init()
            ->add("style", "margin-top:2%")
            ->add("value", "Изпрати")
            ->add("class" , "btn waves-effect waves-light");
        
        parent::__construct($form_data);
    }

    public function save()
    {
        $_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_ORDER]['id'] = null;
        $notifier = Notifier::init();
        $notifier->add(new SuccessMessage("������ ����������"));
        return array('text' => '������ ����������', 'code' => 'success');
    }
}
