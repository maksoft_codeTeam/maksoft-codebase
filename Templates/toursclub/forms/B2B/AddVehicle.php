<?php
namespace Maksoft\Rent\Bus\Form\B2B;
use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\PhoneField;
use \Maksoft\Form\Fields\PasswordField;
use \Maksoft\Form\Fields\SelectField;
use \Maksoft\Form\Fields\DateField;
use \Maksoft\Form\Fields\FileInputField;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\SubmitButton;


class AddVehicle extends DivForm
{
    private $_em, $_company, $_type;
    private $_savePath = '/hosting/maksoft/maksoft/Templates/busrent-b2b/assets/img/vehicles/';
    public $title = "�������� �� �������� ��������:";

    public function __construct($company_id, $em, $post_data=null)
    {
        $this->_em = $em;
        $this->_company = $company_id;
        $this->make = TextField::init();
        $this->type = TextField::init();
        $this->model = TextField::init();
        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "vehicle");

        $this->extras = TextField::init()
            ->add("name" , "extras[]")
            ->add("class" , "validate");

        $this->seats = TextField::init()
            ->add("label", "���� �����")
            ->add("class" , "validate");

        $this->submit = SubmitButton::init()
            ->add("class", "btn btn-info")
            ->add("value", "������");
        $this->manufactured = DateField::init()
            ->add("label", "���� �� ������������:");

        $this->image = new FileInputField(array(
            "label"    => "DropImages Here",
            "name"     => "images[]",
            "id"       => "img",
            "multiple" => True,
            ));
        $options = array("�����"=>"�����", "������" => "3");
        $this->fuel = SelectField::init()
            ->add("class", "form-control")
            ->add("label", "������:")
            ->add("option", $options);

        #$this->images->add_validators(new \Maksoft\Form\Validators\NotBiggerThan(4194304)); // 4MB
        #$this->images->add_validators(new \Maksoft\Form\Validators\FileTypeMatch("image/jpeg"));
        #$this->images->add_validators(new \Maksoft\Form\Validators\FileExtensionMatch("jpg", "JPG", ".jpg"));

        parent::__construct($post_data);
    }

    private function checkEntityManager()
    {
        if (!$this->_em->isOpen()) {
            $this->_em = $this->_em->create(
                $this->_em->getConnection(),
                $this->_em->getConfiguration()
            );
        }
    }

    public function save()
    {
        $extrasRepository  = $this->_em->getRepository('\Maksoft\Rent\Bus\Extras');
        $makeRepository    = $this->_em->getRepository('\Maksoft\Rent\Bus\Make');
        $typeRepository    = $this->_em->getRepository('\Maksoft\Rent\Bus\Type');
        $companyRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Company');
        $extras  = $extrasRepository
                            ->findBy(array("id" => $this->extras->value));
        $make    = $makeRepository
                            ->findOneById($this->make->value);
        $company = $companyRepository
                            ->findOneById($_SESSION['bus']['id']);
        $type = $typeRepository
                            ->findOneById($this->type->value);

        $vehicle = new \Maksoft\Rent\Bus\Vehicle();
        $vehicle->setMake($make);
        $vehicle->setModel($this->model->value);
        $vehicle->setType($type);
        $vehicle->setCompany($company);
        $vehicle->setSeats($this->seats->value);
        $vehicle->setFuel($this->fuel->value);
        $vehicle->setManufacturedDate($this->manufacture->value);
        foreach($extras as $extra){
            $vehicle->addExtra($extra);
        }

        $this->_em->persist($vehicle);

        $this->_em->flush();
        $vehicle_id = $vehicle->getId();
        if($images = $this->image->reArrayFiles($_FILES['image'])){
            $ds = DIRECTORY_SEPARATOR;
            $path = $this->_savePath.$ds.$_SESSION['bus']['id'];
            if(!file_exists($path)){
                mkdir($path, 0755, true); 
            }
            foreach($images as $image) {
                $this->saveImage($image, $vehicle, $path);
            }
        }
    }

    private function createOptions(array $ofOjects)
    {
        $tmp = array();
        foreach($ofObjects as $object) {
            $tmp[$object->getId()] = $object->getName();
        }
        return $tmp;
    }

    public function saveImage($image, $vehicle, $path)
    {
        $status = False;
        $ds = DIRECTORY_SEPARATOR;
        $full_path = $path.$ds.$image['name'];
        if(!file_exists($full_path)){
            $status = move_uploaded_file($image['tmp_name'], $full_path);
        } elseif(file_exists($full_path)){
            $status = True;
        }
        if($status){
            $im = new \Maksoft\Rent\Bus\Images();
            $im->setName($image['name']);
            $im->setExtension($image['type']);
            $im->setSize($image['size']);
            $im->setPath("Templates/busrent-b2b/assets/img/vehicles/".$ds.$_SESSION['bus']['id'].$ds.$image['name']);
            $im->setVehicle($vehicle);
            $this->_em->persist($im);
            $this->_em->flush();
        }
    }
}
