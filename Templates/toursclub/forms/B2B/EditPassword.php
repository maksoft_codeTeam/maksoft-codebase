<?php
namespace Maksoft\Rent\Bus\Form\B2B;
use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\PhoneField;
use \Maksoft\Form\Fields\PasswordField;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\SubmitButton;


class EditPassword extends DivForm
{
    private $_em, $_company;
    public $title = "����� �� ������:";

    public function __construct($em, $post_data=null)
    {
        $this->_em = $em;
        $class = "form-control";
        $companyRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Company');
        $this->_company = $companyRepository->findOneBy(array("id" => $_SESSION['bus']['id']));
        $this->password = PasswordField::init() 
            ->add("class" , $class)
            ->add("placeholder", "���� ������")
            ->add("required", True);

        $password_max_lenght = 15;
        $maxLenghtValidator = new \Maksoft\Form\Validators\MaxLength($password_max_lenght);
        $maxLenghtValidator->setMsg( sprintf("������������ ������� �� �������� �� ������ �� ���������  %s �������.", $password_max_lenght + 1));
        $this->password->add_validators( $maxLenghtValidator );

        $password_min_lenght = 5;

        $minLenghtValidator = new \Maksoft\Form\Validators\MinLength($password_min_lenght);
        $minLenghtValidator->setMsg(sprintf("����������� ������� �� �������� ������ �� ���� ������� %s �������.", $password_min_lenght + 1));
        $this->password->add_validators( $minLenghtValidator );

        $hasDigitValidator = new \Maksoft\Form\Validators\HasDigit() ;
        $hasDigitValidator->setMsg("�������� ������ �� ������� �����") ;
        $this->password->add_validators( $hasDigitValidator );

        $hasUpperCaseValidator = new \Maksoft\Form\Validators\HasUpperCase() ;
        $hasUpperCaseValidator ->setMsg("�������� ������ �� ��� ���� ���� ������ �����!");
        $this->password->add_validators( $hasUpperCaseValidator );

        $this->password_repeated = PasswordField::init() 
            ->add("class" , $class)
            ->add("placeholder", "��������� ��������")
            ->add("required", True);

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "password");

        $this->submit = SubmitButton::init()
            ->add("class", "btn btn-info")
            ->add("value", "������� ��������");
        parent::__construct($post_data);
    }

    private function checkEntityManager()
    {
        if (!$this->_em->isOpen()) {
            $this->_em = $this->_em->create(
                $this->_em->getConnection(),
                $this->_em->getConfiguration()
            );
        }
    }

    protected function validate_password()
    {
        if($this->password->value === $this->password_repeated->value){
            return True;                        
        }
        throw new \Exception("�������� �� ��������! ����, �������� ������.");
    }

    public function save()
    {
        $this->checkEntityManager();
        $this->_company->setPassword($this->password->value);
        try { 
            $this->_em->persist($this->_company);
            return array(
                "text" => "������� ����������� ".$company->getName(),
                 "code" => "success"
            );
        } catch (\Exception $e){
            return array("text" => "���� ��� ���������� �������� � ���� ������/���", "code" => "error");
        }
    }
}
