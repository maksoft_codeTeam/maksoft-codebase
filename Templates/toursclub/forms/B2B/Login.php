<?php
namespace Maksoft\Rent\Bus\Form\B2B;

use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\PasswordField;
use \Maksoft\Form\Fields\PhoneField;
use \Maksoft\Form\Fields\SubmitButton;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\DateField;


class Login extends DivForm 
{
    private $_em;
    public function __construct($em, $post_data=null)
    {
        $this->_em = $em;

        $this->email = EmailField::init() 
            ->add("class" , "validate")
            ->add("placeholder", "����� �����:")
            ->add("required", True);

        $this->password = PasswordField::init() 
            ->add("class" , "validate")
            ->add("placeholder", "������:")
            ->add("required", True);

        $this->submit = SubmitButton::init()
            ->add("value", "����")
            ->add("class" , "btn btn-success");

        $this->action = HiddenField::init()
            ->add("value", "login");

        parent::__construct($post_data);
    }

    public function save()
    {
        $companyRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Company');
        if(!$company = $companyRepository->findOneByEmail($this->email->value)){
            throw new \Exception("���� ��� ������������ �������� � ����� ����� �����.");
        }
        if($company->equal($this->password->value)){
            $this->_em->detach($company);
            $logged_at = time();
            $_SESSION['bus'] = array( "id" => $company->getId(),
                                      "company" => $company->getName(),
                                      "email" => $company->getEmail(),
                                      "logged_at" => $logged_at,
                                      "expire" => $logged_at + 3600,
                                );
            return array("text" => "����� ����� ".$company->getName(),
                         "code" => "success",);
        }

        throw new \Exception("������ ������, �������� ���.");
    }
}
