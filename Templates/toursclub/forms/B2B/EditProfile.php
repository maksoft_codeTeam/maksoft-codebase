<?php
namespace Maksoft\Rent\Bus\Form\B2B;
use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\PhoneField;
use \Maksoft\Form\Fields\PasswordField;
use \Maksoft\Form\Fields\EmailField;
use \Maksoft\Form\Fields\SubmitButton;


class EditProfile extends DivForm
{
    private $_em, $_company;
    public $title = "������� �� �����:";

    public function __construct($em, $post_data=null)
    {
        $this->_em = $em;
        $class = "form-control";
        $companyRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Company');
        $this->_company = $companyRepository->findOneBy(array("id" => $_SESSION['bus']['id']));

        $this->city = TextField::init()
            ->add("name" , "city")
            ->add("class" , $class)
            ->add("label" , "����")
            ->add("value" , $this->_company->getCity())
            ->add("required" , True);

        $this->address = TextField::init()
            ->add("name" , "address")
            ->add("class" , $class)
            ->add("value" , $this->_company->getAddress())
            ->add("label" , "�����:")
            ->add("required" , True);

        $this->owner = TextField::init()
            ->add("name" , "owner")
            ->add("class" , $class)
            ->add("value" , $this->_company->getOwner())
            ->add("label" , "���")
            ->add("required" , True);

        $this->phone = PhoneField::init()
            ->add("name" , "phone")
            ->add("class" , $class)
            ->add("value" , $this->_company->getPhone())
            ->add("label" , "�������")
            ->add("required" , True);

        $this->phone2 = PhoneField::init()
            ->add("name" , "phone2")
            ->add("class" , $class)
            ->add("value" , $this->_company->getPhone2())
            ->add("label" , "�������");

        $this->email = EmailField::init()
            ->add("name" , "email")
            ->add("class" , $class)
            ->add("value" , $this->_company->getEmail())
            ->add("label" , "����� �� �������")
            ->add("required" , True);

        $this->email2 = EmailField::init()
            ->add("name" , "email2")
            ->add("class" , $class)
            ->add("value" , $this->_company->getEmail2())
            ->add("label" , "����� �� �������") ;

        $this->vat = TextField::init()
            ->add("name" , "vat")
            ->add("class" , $class)
            ->add("label" , "���")
            ->add("value" , $this->_company->getVat())
            ->add("required" , True);

        $this->fax = TextField::init()
            ->add("name" , "vat")
            ->add("class" , $class)
            ->add("label" , "����")
            ->add("value" , $this->_company->getFax())
            ->add("required" , True);

        $this->website = TextField::init()
            ->add("name" , "website")
            ->add("class" , $class)
            ->add("label" , "�������")
            ->add("value" , $this->_company->getFax())
            ->add("required" , True);


        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "edit");

        $this->submit = SubmitButton::init()
            ->add("class", "btn btn-info")
            ->add("value", "������");
        parent::__construct($post_data);
    }

    private function checkEntityManager()
    {
        if (!$this->_em->isOpen()) {
            $this->_em = $this->_em->create(
                $this->_em->getConnection(),
                $this->_em->getConfiguration()
            );
        }
    }


    public function save()
    {
        $this->checkEntityManager();
        $this->_company->setName($this->name->value);
        $this->_company->setCity($this->city->value);
        $this->_company->setAddress($this->address->value);
        $this->_company->setOwner($this->owner->value);
        $this->_company->setPhone($this->phone->value);
        $this->_company->setPhone2($this->phone->value);
        $this->_company->setEmail($this->email->value);
        $this->_company->setEmail2($this->email->value);
        $this->_company->setVat($this->vat->value);
        $this->_company->setLicense($this->license->value);
        $this->_company->createdAt();
        $this->_em->persist($this->_company);
    }
}
