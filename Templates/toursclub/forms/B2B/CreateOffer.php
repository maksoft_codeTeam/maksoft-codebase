<?php
namespace Maksoft\Rent\Bus\Form\B2B;
use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\PhoneField;
use \Maksoft\Form\Fields\PasswordField;
use \Maksoft\Form\Fields\IntegerField;
use \Maksoft\Form\Fields\SelectField;;
use \Maksoft\Form\Fields\SubmitButton;


class CreateOffer extends DivForm
{
    private $_em, $_company;
    public $title = "������ �����������:";

    public function __construct($em, $post_data=null)
    {
        $this->_em = $em;
        $class = "";
        $companyRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Company');
        $this->_company = $companyRepository->findOneBy(array("id" => $_SESSION['bus']['id']));


        $this->order_id = HiddenField::init();
            //->add_validator(new \Maksoft\Form\Validators\Integerish());

        $vehicleRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Vehicle');
        $vehicles = $vehicleRepository->findBy(array("company" => $_SESSION['bus']['id']));
        $options = array();
        foreach($vehicles as $vehicle){
            $options[$vehicle->getId()] = $vehicle->getMake()->getName() . ' - ' . $vehicle->getModel(). ' - ' . $vehicle->getSeats() . ' �����';
        }
        $this->vehicles = SelectField::init()
            ->add("name", "vehicles[]")
            ->add("options", $options)
            ->add("multiple", True)
            ->add("required", True);

        $this->cost = IntegerField::init()
            ->add("value", 1)
            ->add("required", True);
        $this->cost->setStep(1);
        $this->cost->setMin(0);

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "createOffer");

        $this->submit = SubmitButton::init()
            ->add("class", "btn btn-info")
            ->add("value", "��������");
        parent::__construct($post_data);
    }

    private function checkEntityManager()
    {
        if (!$this->_em->isOpen()) {
            $this->_em = $this->_em->create(
                $this->_em->getConnection(),
                $this->_em->getConfiguration()
            );
        }
    }

    public function save()
    {
        $this->checkEntityManager();
        try{
            $orderRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Order');
            $order = $orderRepository->findOneBy(array("id" => $this->order_id->value));
            $vehicleRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Vehicle');
            $vehicles = $vehicleRepository->findBy(array("id" => $this->vehicles->value));
            $offer = new \Maksoft\Rent\Bus\Offer;
            $offer->setOrder($order);
            $offer->setCost($this->cost->value);
            $offer->setCompany($this->_company);
            $offer->setStatus(0);
            foreach($vehicles as $vehicle) {
                $offer->addVehicle($vehicle);
            }
            $this->_em->persist($offer);
            $this->_em->flush();
            return array(
                "text" => "�������� � ��������. ���������� ��!",
                 "code" => "success"
            );
        } catch (\Exception $e){
            return array(
                "text" => "",
                "code" => "error"
            );
        }
    }
}
