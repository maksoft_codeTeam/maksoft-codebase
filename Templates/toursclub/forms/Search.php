<?php
namespace Maksoft\Rent\Bus\Form;

use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\IntegerField;
use \Maksoft\Form\Fields\SubmitButton;
use \Maksoft\Form\Fields\DateField;


use Maksoft\Rent\Bus\Settings\Base as BaseSettings;


class Search extends DivForm
{
    private $_em;

    public function __construct($em, $post_data=null)
    {
        $this->_em = $em;

        $this->from_city = TextField::init()
            ->add("name" , "from_city")
            ->add("class" , "validate")
            ->add("id" , "from_city")
            ->add("placeholder", "�����")
            ->add("label" , "�������� ��:")
            ->add("required", True);

        $this->to_city = TextField::init()
            ->add("name" , "to_city")
            ->add("id", "to_city")
            ->add("class" , "validate")
            ->add("label" , "����������:")
            ->add("placeholder", "�����")
            ->add("required", True);

        $this->from_date = TextField::init()
            ->add("name" , "from_date")
            ->add("label", "���� �� ��������:")
            ->add("class" , "datepicker")
            ->add("value" , date('Y-m-d', time()));

        $this->to_date = TextField::init()
            ->add("name" , "to_date")
            ->add("class" , "datepicker")
            ->add("label", "���� �� ����������:")
            ->add("value" , date('Y-m-d', time()));

        $this->persons = IntegerField::init()
            ->add("name" , "persons")
            ->add("class" , "validate")
            ->add("label", "���������:")
            ->add("value" , 1)
            ->add("step", 1)
            ->add("min", 1)
            ->add("placeholder" , 1);

        $this->childrens = IntegerField::init()
            ->add("value" , 1)
            ->add("name" , "childrens")
            ->add("step", 1)
            ->add("min", 1)
            ->add("label", "����:")
            ->add("class" , "validate");

        $this->twoway = TextField::init()
            ->add("name" , "twoway")
            ->add("label", "���������� ��������")
            ->add("class" , "validate");

        $this->submit = SubmitButton::init()
            ->add("name" , "search")
            ->add("class" , "btn waves-effect waves-light");

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "search");

        $this->extras = TextField::init()
            ->add("name" , "extras[]")
            ->add("class" , "validate");


        parent::__construct($post_data);
    }

    #public function validate_twoway()
    #{
    #    if($this->twoway->value === "on"){
    #        $this->twoway->value = True;
    #    } else {
    #        $this->twoway->value = False;
    #    }
    #    return True;
    #}

    public function save()
    {
        $postData = $this->getCleanedData();
        $_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT] = $postData;
        return "ads";
    }
}
