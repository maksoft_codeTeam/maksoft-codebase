<?php
namespace Tours\Forms;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Password;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class Login extends Bootstrap 
{
    public function __construct($post_data=null)
    {
        $this->email = Email::init() 
            ->add("label", "email")
            ->add("class" , "validate")
            ->add("required", True);

        $this->password = Password::init() 
            ->add("label", "pass")
            ->add("class" , "validate")
            ->add("data-validate", "min, max, required")
            ->add("required", True);

        $this->submit = Submit::init()
            ->add("value", "login")
            ->add("style", "margin-top:1%")
            ->add("class" , "btn waves-effect waves-light");

        $this->action = Hidden::init()
            ->add("value", "login");

        parent::__construct($post_data);
    }
    
    public function validate_password()
    {
        echo '<br><b>check user exist</b>';
    }
}
