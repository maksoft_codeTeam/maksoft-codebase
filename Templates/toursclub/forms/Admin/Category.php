<?php
namespace Maksoft\Rent\Bus\Form\Admin;

use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\SelectField;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\SubmitButton;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Validators\NotEmpty;
use \Maksoft\Form\Validators\MaxLength;
use \Maksoft\Form\Validators\MinLength;


class Category extends DivForm
{
    private $_em;
    private $_language;

    public function __construct($language, $em, $post_data=array())
    {
        $this->_em = $em;
        $this->_language = $language;
        $class = "form-control";

        $this->name = TextField::init()
            ->add("label", "��� �� �����������:")
            ->add("class", $class);
        $this->name->add_validator(new NotEmpty());
        $this->name->add_validator(new MinLength(3));
        $this->name->add_validator(new MaxLength(30));

        $this->icon = TextField::init()
            ->add("label", "fa-icon:")
            ->add("name" , "icon")
            ->add("class", $class);

        $this->submit = SubmitButton::init()
            ->add("class", "btn btn-primary")
            ->add("label", "<hr>")
            ->add("value", "������");

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "insert_category");

        parent::__construct($post_data);
    }

    public function save()
    {
        $languageRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Language');
        $language = $languageRepository->findOneById($this->_language);
        $category = new \Maksoft\Rent\Bus\Category();
        $category->setName($this->name->value);
        $category->setIcon($this->icon->value);
        $category->setLanguage($language);
        $this->_em->persist($category);
        $this->_em->flush();
    }
}
