<?php
namespace Maksoft\Rent\Bus\Form\Admin;

use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\SelectField;
use \Maksoft\Form\Fields\SubmitButton;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Validators\NotEmpty;
use \Maksoft\Form\Validators\MaxLength;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Validators\MinLength;


class VehicleType extends DivForm
{
    private $_em;
    private $_language;

    public function __construct($language, $em, $post_data=array())
    {
        $this->_em = $em;
        $this->_language = $language;
        $class = "form-control";

        $this->name = TextField::init()
            ->add("label", "��� �������� ��������:")
            ->add("name" , "name")
            ->add("class", $class)
            ->add_validator(new NotEmpty())
            ->add_validator(new MinLength(3))
            ->add_validator(new MaxLength(30));

        $this->icon = TextField::init()
            ->add("label", "�����(fa-icon):")
            ->add("name" , "icon")
            ->add("class", $class);

        $this->submit = SubmitButton::init()
            ->add("class", "btn btn-primary")
            ->add("label", "<hr>")
            ->add("value", "������");

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "type_insert");

        parent::__construct($post_data);
    }

    public function save()
    {
        $languageRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Language');
        $language = $languageRepository->findOneById($this->_language);
        $vehicle_type = new \Maksoft\Rent\Bus\Type();
        $vehicle_type->setName($this->name->value);
        $vehicle_type->setIcon($this->icon->value);
        $vehicle_type->setLanguage($language);
        $this->_em->persist($vehicle_type);
        $this->_em->flush();
    }
}

