<?php
namespace Maksoft\Rent\Bus\Form\Admin;

use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\SelectField;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\SubmitButton;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Validators\NotEmpty;
use \Maksoft\Form\Validators\MaxLength;
use \Maksoft\Form\Validators\MinLength;


class Extra extends DivForm
{
    private $_em;

    public function __construct($em, $post_data=array())
    {
        $this->_em = $em;
        $class = "form-control";
        $typeRepository = $em->getRepository('\Maksoft\Rent\Bus\Type');
        $types = $typeRepository->findBy(array("language" => 38));
        $options = array();
        foreach($types as $type){
            $options[$type->getId()] = $type->getName();
        }

        $this->type = SelectField::init()
            ->add("options", $options)
            ->add("name"   , "category")
            ->add("class"  , $class)
            ->add("label"  , "Тип превозно средство:");

        $categoryRepository = $em->getRepository('\Maksoft\Rent\Bus\Category');
        $categories = $categoryRepository->findBy(array("language" => 38));

        $options = array();
        foreach($categories as $category){
            $options[$category->getId()] = $category->getName();
        }

        $this->category = SelectField::init()
            ->add("options", $options)
            ->add("name"   , "category")
            ->add("class"  , $class)
            ->add("label"  , "Категория:");

        $this->name = TextField::init()
            ->add("label", "Име на екстрата:")
            ->add("name" , "name")
            ->add("class", $class)
            ->add_validator(new NotEmpty())
            ->add_validator(new MinLength(3))
            ->add_validator(new MaxLength(30));

        $this->icon = TextField::init()
            ->add("label", "fa-icon:")
            ->add("name" , "icon")
            ->add("class", $class);

        $this->submit = SubmitButton::init()
            ->add("class", "btn btn-primary")
            ->add("label", "<hr>")
            ->add("value", "Запази");

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "insert_extra");

        parent::__construct($post_data);
    }

    public function save()
    {
        $categoryRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Category');
        $category = $categoryRepository->findOneById($this->category->value);
        $typeRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Type');
        $type = $typeRepository->findOneById($this->type->value);
        $extra = new \Maksoft\Rent\Bus\Extras();
        $extra->setName($this->name->value);
        $extra->setIcon($this->icon->value);
        $extra->setCategory($category);
        $extra->addType($type);
        $this->_em->persist($extra);
        $this->_em->flush();
    }
}
