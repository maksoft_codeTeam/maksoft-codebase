<?php
namespace Maksoft\Rent\Bus\Form\Admin;

use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\SelectField;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\SubmitButton;
use \Maksoft\Form\Fields\TextField;
use \Maksoft\Form\Validators\NotEmpty;
use \Maksoft\Form\Validators\MaxLength;
use \Maksoft\Form\Validators\MinLength;


class ExtraDetail extends DivForm
{
    private $_em;
    private $_type;
    private $_language;
    private $_category;

    public function __construct($language, $vehicleType, $category, $em, $post_data=array())
    {
        $this->_em = $em;
        $this->_language = $language;
        $this->_type = $vehicleType;
        $this->_category = $category;
        $class = "form-control";
        $options = array();
        $bg_language = 34;

        $this->name = TextField::init()
            ->add("label", "��� �� ��������:")
            ->add("name" , "name")
            ->add("class", $class)
            ->add_validator(new NotEmpty())
            ->add_validator(new MinLength(1))
            ->add_validator(new MaxLength(30));

        $this->icon = TextField::init()
            ->add("label", "fa-icon:")
            ->add("name" , "icon")
            ->add("class", $class);

        $this->submit = SubmitButton::init()
            ->add("class", "btn btn-primary")
            ->add("label", "<hr>")
            ->add("value", "������");

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "insert_extra");

        parent::__construct($post_data);
    }

    public function save()
    {
        $categoryRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Category');
        $category = $categoryRepository->findOneById($this->_category);
        $typeRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Type');
        $type = $typeRepository->findOneById($this->_type);
        $extra = new \Maksoft\Rent\Bus\Extras();
        $extra->setName($this->name->value);
        $extra->setIcon($this->icon->value);
        $extra->setCategory($category);
        $extra->setType($type);
        $this->_em->persist($extra);
        $this->_em->flush();
    }
}
