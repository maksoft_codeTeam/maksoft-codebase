<?php
namespace Tours\Forms;
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Field;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Password;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\MaxLength;
use Jokuf\Form\Validators\MinLength;
use Jokuf\Form\Validators\HasDigit;
use Jokuf\Form\Validators\HasUpperCase;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class Client extends Bootstrap 
{
    public $_header = "register";
    public function __construct($post_data)
    {
        $this->_header = $header;

        $this->first_name = Text::init()
            ->add("label", "first name")
            ->add("class" , "validate")
            ->add("required", True);

        $this->last_name = Text::init()
            ->add("label", "last name")
            ->add("class" , "validate")
            ->add("required", True);

        $this->phone = Phone::init() 
            ->add("label", "phone")
            ->add("class" , "validate")
            ->add("required", True);

        $this->email = Email::init() 
            ->add("label", "email")
            ->add("class" , "validate")
            ->add("required", True);

        $this->password = Password::init() 
            ->add("label", "password")
            ->add("data-validate", "required,rangeVal(5, 15)")
            ->add("class" , "validate")
            ->add("required", True);

        $this->password->add_validator(new MaxLength(15));

        $this->password->add_validator(new MinLength(5));

        $this->password->add_validator(new HasDigit());

        $this->password->add_validator(new HasUpperCase());

        $this->repeated_password = Password::init() 
            ->add("label", "repeat password")
            ->add("class" , "validate")
            ->add("required", True);

        $this->submit = Submit::init()
            ->add("value", "register")
            ->add("class" , "btn waves-effect waves-light");

        $this->action = Hidden::init()
            ->add("value", "client");

        parent::__construct($post_data);
    }
}
