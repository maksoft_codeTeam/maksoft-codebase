<?php
namespace Maksoft\Rent\Bus\Form;

use \Maksoft\Form\Forms\DivForm;
use \Maksoft\Form\Fields\HiddenField;
use \Maksoft\Form\Fields\Checkbox;
use \Maksoft\Form\Fields\SubmitButton;


class Extras extends DivForm
{
    public function __construct($em, $vehicle_type=1, $post=null)
    {
        $type =  $em->getRepository('\Maksoft\Rent\Bus\Type');
        $type = $type->findOneById($vehicle_type);
        $extras = array();
        foreach($type->getExtras() as $extra){
            $category = $extra->getCategory();
            $extras[$category->getName()][] = $extra;

            $this->{ 'extra-'.$extra->getId() } = Checkbox::init()
                                                    ->add("value", $extra->getId())
                                                    ->add("label", $extra->getName());
        }

        $this->action = HiddenField::init()
            ->add("name", "action")
            ->add("value", "insert_extras");

        $this->submit = SubmitButton::init(array());
    }
}
