<?php

function is_authenticated_hook()
{
    return check_permissions(array(
        is_cookie_exist('TOKEN'),
        is_cookie_exist(COOKIE_NAME),
        is_cookie_expire(COOKIE_NAME)
    ));
}

function is_cookie_exist($cookie_name)
{
    return isset($_COOKIE[$cookie_name]);
}

function is_cookie_expire($name)
{
    if(!is_cookie_exist($name)){
        return false;
    }
    return $_COOKIE[$name] < time();
}


function check_permissions(array $permissions){
    if(count(array_unique($permissions)) === 1)  
        return current($permissions);
    return false;
}

