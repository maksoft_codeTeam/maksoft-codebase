{{form.start|raw}}
<div class="row">
    <div class="col-md-12">
        <div class="form-elements ui-widget" style="display:inline">
            <label for="twoway" >{{form.twoway.label|raw}}</label>
            <div class="form-item ui-widget">
              <input style="width: 1em; height: 1em;" type="radio" name="{{form.twoway.name|raw}}" onclick="show()" value="true">Да
              <input style="width: 1em; height: 1em;" type="radio" name="{{form.twoway.name|raw}}" onclick="hide()" value="false" checked>Не
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-elements ui-widget">
          <label for="from_city" >{{form.from_city.label|raw}}</label>
          <div class="form-item ui-widget"><i class="awe-icon awe-icon-marker-1"></i>
            {{form.from_city|raw}}
          </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-elements ui-widget">
          <label>{{form.to_city.label|raw}}</label>
          <div class="form-item"><i class="awe-icon awe-icon-marker-1"></i>
            {{form.to_city|raw}}
          </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6" id="from_date">
        <div class="form-elements">
          <label>{{form.from_date.label|raw}} </label>
          <div class="form-item"><i class="awe-icon awe-icon-calendar"></i>
            {{form.from_date|raw}}
          </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3" id="to_date" style="display:none;">
        <div class="form-elements">
          <label>{{form.to_date.label|raw}}</label>
          <div class="form-item"><i class="awe-icon awe-icon-calendar"></i>
            {{form.to_date|raw}}
          </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="form-elements">
          <label>{{form.persons.label|raw}}</label>
          <div class="form-item">
            {{form.persons|raw}}
          </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="form-elements">
          <label>{{form.childrens.label|raw}}</label>
          <div class="form-item">
            {{form.childrens|raw}}
          </div>
        </div>
    </div>
    <div class="col-md-12">
    <hr>
        {% for category, col in extras %}
        <div class="col-sm-4 col-md-3">
            <h5> {{category|raw}}</h5>
            <div class="row">
            {% for extra in col %}
                <div class="col-sm-12 checkbox">
                    <label>
                        <input style="width: 1em;height: 1em;" name="extras[]" type="checkbox" value="{{extra.getId}}">{{extra.getName|raw}}
                    </label>
                </div>
            {% endfor %}
            </div>
        </div>
        {% endfor %}
    </div>
    <div class="col-md-12 form-actions">
      <input type="submit" value="Поискай ОФЕРТА">
    </div>
    {{form.action|raw}}
    {{form.end|raw}}
</div>
