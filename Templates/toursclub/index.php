<?php
define("DS", DIRECTORY_SEPARATOR);
define("BASE_DIR", realpath(__DIR__.DS.'..'.DS.'..'));
define("COOKIE_NAME", 'auth');
define("CURRENT_TIME", time()); 
define("JWT_EXPIRE_TIME", 15*60);
define("EXPIRE_DATE", CURRENT_TIME + JWT_EXPIRE_TIME);


require_once BASE_DIR.DS.'modules'.DS.'vendor'.DS.'autoload.php';
require_once __DIR__.DS.'forms'.DS.'Client.php';
require_once __DIR__.DS.'forms'.DS.'Login.php';
require_once __DIR__.DS.'forms'.DS.'Enquiry.php';
require_once __DIR__.DS.'views.php'; 
require_once __DIR__.DS.'hooks.php'; 
require_once __DIR__.DS.'helpers.php'; 
require_once __DIR__.DS.'HttpResponseCode.php'; 
session_start();

define("INDEX_VIEW", 'index_view');

define("LOGIN_VIEW", 'login_view');

define("REGISTER_VIEW", 'register_view');

define("LOGIN_OR_REGISTER", 'login_or_register');

define("DETAIL_VIEW", 'index_view');

if(!isset($_SESSION['client'])){
    $_SESSION['client'] = array();
}
if(!isset($_SESSION['action'])){
    $_SESSION['action'] = INDEX_VIEW;
}

debugly($_SESSION);

$request = new \Curl\Curl();

#$response = get_token($request, array("email" => "iordanov_@mail.bg", "password" => "8802285760Admin00"));


function debugly($msg)
{
    echo  '<pre><code>';
    print_r(json_decode(json_encode($msg)));
    echo '</code></pre>';
}

function login_form()
{
    debugly("login_view");
    return new Tours\Forms\Login($_POST);
}

function register_form()
{
    debugly("register_view");
    return new Tours\Forms\Client($_POST);
}

function trip_form()
{
    debugly("enquery_view");
    return new Tours\Forms\Enquiry($_POST);
}

function check_if_user_is_registered($username, $email)
{

}


function register_user($email, $first_name, $last_name, $phone, $password)
{

}

function get_request_instance()
{
    return new \Curl\Curl();
}

function authenticate($email, $password)
{
    $response = get_token(get_request_instance(), array("email" => $email, "password" => $password));
    if($response->status == Response::HTTP_OK){
        set_authentication_cookies($response->data->token);
        $_SESSION['action'] = INDEX_VIEW;
    }
}

function set_authentication_cookies($token)
{
    set_cookie(COOKIE_NAME, EXPIPRE_DATE);
    set_cookie('TOKEN', $token);
}


function is_token_valid()
{
    return isset($_COOKIE[COOKIE_NAME]); 
}

function is_token_exist()
{
    return isset($_SESSION['token']);
}

function get_session_token()
{
    return $_SESSION['token'];
}

function refresh_token_view()
{
    debugly("refresh token");
}

function check_jwt_token()
{
    if(is_token_exist()){
        if(is_token_valid()){
            return get_session_token();
        }

        refresh_token_view();
    }
}

function set_cookie($name, $value)
{
    setcookie($name, $value, EXPIRE_DATE, '/', null, false, true);
}
unset($_SESSION['action']);

$render = function(){
    switch(get_current_page()){
        case INDEX_VIEW:
            if(!is_query_already_set()){
                set_index_page();
            }
            if(isset($_SESSION['trip'])){

                if(is_authenticated_hook()){
                    $response = save_trip(
                                    get_request_instance(),
                                    $_SESSION['trip'],
                                    get_authentication_headers()
                                );
                                
                } else {
                    $_SESSION['action'] = LOGIN_OR_REGISTER; 
                }
            }
        case LOGIN_VIEW:
            if(is_authenticated_hook()){
                set_index_page();
            }
    }
    return $_SESSION['action']();
};
echo $render();

function get_authentication_headers(){
    return array("Authorization: JWT ".$_COOKIE['TOKEN']);
}

function is_query_already_set()
{
    return isset($_SESSION['trip']);
}

function get_current_page()
{
    return isset($_SESSION['action']) ? $_SESSION['action'] : set_index_page();
}

function set_index_page()
{
    $_SESSION['action'] = INDEX_VIEW;
    return $_SESSION['action'];
}

#if(Response::isError($response->status)){
#    // redirect to login or register page
#} else {
#    //put token in cookie
#    $_COOKIE[COOKIE_NAME]['token'] = $response->data->token;
#    // submit enquiery
#    $form_data = $_SESSION['trip'];
#    $response = save_trip($request, $form_data, array("Authorization: JWT ".$_COOKIE[COOKIE_NAME]['token']));
#    if($response->status == Response::HTTP_CREATED){
#        unset($_SESSION['trip']);
#        //other stuff
#    } else {
#        //do something
#    }
#    // message to user
#}
