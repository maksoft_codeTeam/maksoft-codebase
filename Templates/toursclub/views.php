<?php

function index_view()
{
    $trip_form = trip_form();
    switch($_SERVER['REQUEST_METHOD']){
        case "POST":
            try {
                $trip_form->is_valid();
                $_SESSION['trip'] = $trip_form->clean_data();
                debugly($_SESSION);
            } catch (Exception $e) {
                debugly($e->getMessage());
            }
            $_SESSION['action'] = LOGIN_OR_REGISTER;
            break;
        default:
            debugly("Current page is ".$_SERVER['REQUEST_URI']);
            debugly("Request method is: ".$_SERVER['REQUEST_METHOD']);
            debugly("user flow:");
            debugly("get request");
            debugly("after get request to index user must see enquiery form");
            debugly("if user submit form and form is valid user must login or register");
    }
    echo $trip_form;
}

function register_view()
{
    $auth_header = "Authorization: JWT ".$_COOKIE['token'];
    $form = register_form();
    try{
        $form->is_valid();
        $request = register(get_request_instance(), $form->clean_data(), array($auth_header)); 
        debugly($request->status);
        switch($request->status){
            case Response::HTTP_CREATED:
                authenticate($form->email->value, $form->password->value);
                set_index_page();
                break;
            case Response::HTTP_UNAUTHORIZED:
                print_errors($request->response);
                break;
            default:
                print_errors($request->response);
                debugly($request->status);
        }
    } catch(Exception $e){
        debugly($e);
    }

    debugly($request);
    print_errors($request->response);
    echo $form;
}

function print_errors($response)
{
    foreach($response as $field)
    {
        foreach($field as $err){
            echo $err.'<br>';
        }
    }
}

function login_or_register()
{
    debugly('login or register view');   
    debugly('side by side');   
    echo register_view();
    echo login_view();
}

function login_view()
{
    $form = login_form();
    try{
        $form = submit_form(login_form());
        $response = get_token($form->email->value, $form->password->value);
        if(Response::isError($response->status)){
            $_SESSION['action'] = REGISTER_VIEW;
            return;
        }

        set_authentication_cookies($response->data->token);
        $_SESSION['action'] = INDEX_VIEW;
    } catch (Exception $e) {
        debugly($e);
    }
    echo $form;
}


function submit_form($form)
{
    if($_SERVER['REQUEST_METHOD'] === 'POST' and isset($_POST['action']) and $_POST['action'] == $form->action->value){
        $form->is_valid();
        return $form;
    }
    return $form;
}
