<div class="clearfix"></div>
<section class="banner-wrap">
    <div class="banner">
        
        <div class="rev_slider_wrapper">            
            <!-- START REVOLUTION SLIDER 5.0 auto mode -->
            <div id="rev_slider" class="rev_slider"  data-version="5.0">
                <ul>    
                    
                    <?php
					    $n_sliders = 19342434;
                        $sliders = $o_page->get_pSubpages($n_sliders);
                        
                            for($i=0; $i<count($sliders); $i++)
                                {
                                    ?>
                            
					<li data-transition="fade"> <img src="<?=$o_page->get_pImage($sliders[$i]['n'])?>" alt="<?=$o_page->get_pName($sliders[$i]['n'])?>"> </li>      
                                
                                                                <?							
							}
					?>


                </ul>               
            </div><!-- END REVOLUTION SLIDER -->
        </div><!-- END REVOLUTION SLIDER WRAPPER -->    

        <script>
        /******************************************
        -   PREPARE PLACEHOLDER FOR SLIDER  -
        ******************************************/
        
        var revapi;
        jQuery(document).ready(function() {     
            revapi = jQuery("#rev_slider").revolution({
                sliderType:"standard",
                sliderLayout:"fullwidth",
                delay:9000,
                navigation: {
                    arrows:{enable:true}                
                },          
                gridwidth:1170,
                gridheight:645      
            });     
        }); /*ready*/
        </script>       
        
        <!-- END REVOLUTION SLIDER --> 
        
        <!-- Content End --> 
        
    </div>
</section>
<section class="message-wrap">
    <div class="container">
        <div class="row">
            <h2 class="col-lg-9 col-md-8 col-sm-12 col-xs-12 xs-padding-left-15">������ ��������� �� ����������� � <span class="alternate-font">���������� ���� ���������</span></h2>
            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 xs-padding-right-15"> <a href="<?=$o_page->get_pLink(19338090)?>" class="default-btn pull-right action_button lg-button">���������� ������ ������!</a> </div>
        </div>
    </div>
    <div class="message-shadow"></div>
</section>
<!--message-wrap ends-->
<section class="content">
    <div class="container">
        <div class="inner-page homepage margin-bottom-none">
            <section class="car-block-wrap padding-bottom-40">
                <div class="container">
                    <div class="row">
                      <?php 
                        $top3index = $o_page->get_pGroupContent($n_top3index);
                        
                            for($i=0; $i<count($top3index); $i++)
                                {
                                    ?>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 margin-bottom-none">
                            <div class="flip margin-bottom-30">
                                <div class="card">
                                    <div class="face front">
                                <a href="<?=$o_page->get_pLink($top3index[$i]['n'])?>">
                                <img class="img-responsive" src="<?=$o_page->get_pImage($top3index[$i]['n'])?>" alt="">
                                </a>
                                </div>
                                <div class="face back">
                                <div class='hover_title'><?php echo $top3index[$i]['title'] ?></div>
                                        <a href="<?=$o_page->get_pLink($top3index[$i]['n'])?>"><i class="fa fa-link button_icon"></i></a> <a href="<?=$o_page->get_pImage($top3index[$i]['n'])?>" class="fancybox"><i class="fa fa-arrows-alt button_icon"></i></a> </div>
                                </div>
                            </div>
                            <h4><a href="<?=$o_page->get_pLink($top3index[$i]['n'])?>"><?php echo $top3index[$i]['title'] ?></a></h4>
							<p class="margin-bottom-none"><?=cut_text(strip_tags($top3index[$i]['textStr']), 200)?> <small><a href="<?=$o_page->get_pLink($top3index[$i]['n'])?>">[��� ���]</a></small></p>
                        </div>
                                        
                                
                                                                <?							
							}
					?>
                    
                    </div>
                </div>
            </section>
            
            <!--car-block-wrap ends-->
            <div class="row parallax_parent design_2 margin-bottom-40 margin-top-30">
                <div class="parallax_scroll clearfix" data-velocity="-.5" data-offset="-200" data-image="<?=TEMPLATE_DIR?>images/parallax1.jpg">
                    <div class="overlay">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 padding-left-none xs-margin-bottom-none xs-padding-top-30 scroll_effect bounceInLeft"> <span class="align-center"><img src="<?=TEMPLATE_DIR?>images/picts/coin.svg" class="svg-pict" alt=""></span>
                                    <h3>��������� ������� �������</h3>
                                </div>
<!--                                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 xs-margin-bottom-none xs-padding-top-30 scroll_effect bounceInLeft" data-wow-delay=".2s"> <span class="align-center"><i class="fa fa-6x fa-road"></i></span>
                                    <h3>�������� ������ � ���������� ������� �� �������� ������</h3>
                                </div>-->
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 xs-margin-bottom-none xs-padding-top-30 scroll_effect bounceInRight" data-wow-delay=".2s"> <span class="align-center"><img src="<?=TEMPLATE_DIR?>images/picts/speedometer.svg" class="svg-pict" alt=""></span>
                                    <h3>������� � ���������</h3>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 xs-margin-bottom-none xs-padding-top-30 padding-right-none scroll_effect bounceInRight"> <span class="align-center"><img src="<?=TEMPLATE_DIR?>images/picts/family-sofa.svg" class="svg-pict" alt=""></span>
                                    <h3>������ ���� �� �������</h3>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 xs-margin-bottom-none xs-padding-top-30 padding-right-none scroll_effect bounceInRight"> <span class="align-center"><img src="<?=TEMPLATE_DIR?>images/picts/earth.svg" class="svg-pict" alt=""></span>
                                    <h3>����� ������ �����</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--car-info-wrap ends-->
            <section class="welcome-wrap padding-top-30 sm-horizontal-15">
                
                <div class="recent-vehicles-wrap margin-top-30 sm-padding-left-none padding-bottom-40">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 recent-vehicles padding-left-none">
                            <h5 class="margin-top-none">�������� ������</h5>
                            <p>����������� � ������� ��������, ���������� �� Eldrive � ��������</p>
                            <div class="arrow3 clearfix margin-top-15 xs-margin-bottom-25" id="slideControls3"><span class="prev-btn"></span><span class="next-btn"></span></div>
                        </div>
                        <div class="col-md-10 col-sm-8 padding-right-none xs-padding-left-none">
                            <div id="actual-models" class="carasouel-slider3">
                                   
                        <?php 
                        $models = $o_page->get_pGroupContent($n_models);
                        
                            for($i=0; $i<count($models); $i++)
                                {
                                    ?>
                                    
                                <div class="slide">
                                    <div class="car-block">
                                        <div class="img-flex"> <a href="<?=$o_page->get_pLink($models[$i]['n'])?>"><span class="align-center"><i class="fa fa-3x fa-plus-square-o"></i></span></a> <img src="<?=$o_page->get_pImage($models[$i]['n'])?>" alt="<?=$o_page->get_pName($models[$i]['n'])?>" class="img-responsive"> </div>
                                        <div class="car-block-bottom">
                                            <h6><strong><?=$o_page->get_pName($models[$i]['n'])?></strong></h6>
                                            <!--<h5>�� 720 ��. �� �����</h5>-->
                                        </div>
                                    </div>
                                </div>
                                
                                <?php } ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Footer Map -->
                <div class='fullwidth_element_parent margin-top-30 padding-bottom-40'>

                    <iframe src="https://map.eldrive.eu" class="fullwidth_element" style="height: 390px;" scrolling="no"></iframe>

               <script>
				$('.fullwidth_element_parent')
    .click(function(){
            $(this).find('iframe').addClass('clicked')})
    .mouseleave(function(){
            $(this).find('iframe').removeClass('clicked')});   
			   </script>
                </div>
                
                <div class="car-rate-block clearfix margin-top-30 padding-bottom-40">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none padding-left-none scroll_effect bounceInLeft">
                        <div class="small-block clearfix">
                            <span class="align-center">
                            <img src="<?=TEMPLATE_DIR?>images/picts/payment-method.svg" class="svg-pict" alt="">
                            </span>
                            <p class="margin-top-15 text-center bold-text pictext">���-�������� ������� �� �������, ����� ���������� ��� ��������� �������</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none scroll_effect bounceInLeft" data-wow-delay=".2s">
                        <div class="small-block clearfix">
                            <span class="align-center">
                            <img src="<?=TEMPLATE_DIR?>images/picts/giving-a-presentation-card.svg" class="svg-pict" alt="">
                            </span>
                            <p class="margin-top-15 text-center bold-text pictext">������������ ���� RFID ����� �������� ��� ���������� ������ � ���������</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padding-left-none padding-right-none hours_operation">
                        <div class="small-block clearfix">
                            <span class="align-center">
                            <img src="<?=TEMPLATE_DIR?>images/picts/worlwide-transmission.svg" class="svg-pict" alt="">
                            </span>
                            <p class="margin-top-15 text-center bold-text pictext">��� 20 000 ���������� � �������� �����</p>
                        </div>
                    </div>
                </div>
            
        </div>
    </div>
</section>

<section class="welcome-wrap padding-top-30 sm-horizontal-15">
                <div class="recent-vehicles-wrap margin-top-30 sm-padding-left-none padding-bottom-40">
                    <div class="row">
<div class="row parallax_parent margin-top-30">
                <div class="parallax_scroll clearfix" data-velocity="-.5" data-offset="-300" data-no-repeat="true" data-image="<?=TEMPLATE_DIR?>images/parallax2.jpg">
                    <div class="overlay">
                        <div class="container">
                            
                            <div class="row">
                                
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padding-left-none margin-vertical-60">
                                    <img src="<?=TEMPLATE_DIR?>images/picts/co2-inside-cloud.svg" class="svg-pict" alt="">
                                    
                                    <span class="animate_number margin-vertical-15">
                                        <span class="number"><?=$broiach_1?></span>
                                    </span>
                                    
                                    ��������� �������� ���������� �������
                                </div>
                                
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-vertical-60">
                                    <img src="<?=TEMPLATE_DIR?>images/picts/pine.svg" class="svg-pict" alt="">
                                    
                                    <span class="animate_number margin-vertical-15">
                                        <span class="number"><?=$broiach_2?></span>
                                    </span>
                                    
                                    �������� ������� ���� ���������� �� �������� ������
                                </div>
                                
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-vertical-60">
                                    <img src="<?=TEMPLATE_DIR?>images/picts/car-electric-station.svg" class="svg-pict" alt="">
                                    
                                    <span class="animate_number margin-vertical-15">
                                        <span class="number"><?=$broiach_3?></span>
                                    </span>
                                    
                                    ����� �� ��������� � �������� 
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <div class="car-rate-block clearfix margin-top-30 padding-bottom-40">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 featured-brand-slider padding-left-none padding-right-none margin-top-30">
                    <div class="featured-brand" style="max-width: 1080px;margin: 0px auto;">

                        <h3 class="margin-bottom-20">������� � ���������</h3>
                        <div class="arrow2" id="slideControls"><span class="prev-btn"></span><span class="next-btn"></span></div>

                        <div id="featured-brand-slide" class="carasouel-slider featured_slider" >
                           
                            <?php 
                        $logos = $o_page->get_pSubpages($n_logos);
                        
                            for($i=0; $i<count($logos); $i++)
                                {
                                    ?>
                                    <div class="slide scroll_effect fadeInUp"><a href="#"><img src="<?=$o_page->get_pImage($logos[$i]['n'])?>" alt="<?=$o_page->get_pName($logos[$i]['n'])?>"></a></div>
                                    <?php } ?>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
                </div>
                </div>
                </div>
            </section>