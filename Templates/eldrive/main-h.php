<div class="clearfix"></div>
<section id="secondary-banner" class="main-slider"><!--for other images just change the class name of this section block like, class="dynamic-image-2" and add css for the changed class-->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <h2><?=$o_page->get_pTitle("strict")?></h2>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 ">
                <ul class="breadcrumb">
                    <li><?="".$nav_bar.""?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--secondary-banner ends-->
<div class="message-shadow"></div>
<div class="clearfix"></div>
<section class="content">
    <div class="container">
        <div class="inner-page full-width row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-left-none padding-right-none">