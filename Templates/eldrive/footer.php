<!--Footer Start-->
<footer class="design_2">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padding-left-none md-padding-left-none sm-padding-left-15 xs-padding-left-15">
                            <h4>�������� ������</h4>                
                
                        <?php 
				        $n_news = "19358592"; 
                        $news = $o_page->get_pSubpages($n_news, $sort_order="p.date_modified ASC");
				
                            $i =0;
                            while($news){
                                if($i===2){
                                    break;
                                }
                                 $page = array_pop($news);
								 $date = new DateTime($page['date_added']);
                                    ?>

                <div class="side-blog">
                    <a href="<?=$o_page->get_pLink($page['n'])?>">
                        <img src="<?=$o_page->get_pImage($page['n'])?>" class="alignleft" width="120px" height="auto" alt="<?=$page['Name']?>"><?=$page['Name']?>
                    </a>
					<span class="data-news"><?=$date->format('d-m-Y')?></span>
                    <p><?=cut_text(strip_tags($page['textStr']), 60)?></p>
                </div>
                <? 
                 $i++;
                } ?>

                </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 contact-with-us">
                            <h4>������ � ���</h4>
 <?php include TEMPLATE_DIR . "forms/contact-footer.php"; ?>
                
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padding-right-none md-padding-right-none sm-padding-right-15 xs-padding-right-15">
               <h4>��������</h4>
                <div class="footer-contact">
                    <ul>
                        <li><i class="fa fa-map-marker"></i> <strong>�����:</strong> <?php echo $o_page->_site['SAddress'];?></li><br/>
                        <li><i class="fa fa-phone"></i> �������� <strong>����</strong></li>
						<li class="sphone"><?=$phone_1?></li>
                        <li class="sphone"><?=$phone_2?></li>
						<li><i class="fa fa-phone"></i> �������� <strong>����������</strong></li>
                        <li class="sphone"><?=$phone_3?></li>
                        <li class="sphone"><?=$phone_4?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="clearfix"></div>
<section class="copyright-wrap padding-bottom-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="logo-footer margin-bottom-20 md-margin-bottom-20 sm-margin-bottom-10 xs-margin-bottom-20">
                <img alt="logo" src="<?=ASSETS_DIR?>images/logo-white.fw.png">
                </div>
                <p>&copy; 2008-<?php echo date("Y"); ?> Eldrive. ������ ����� ��������.</p>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <ul class="social margin-bottom-25 md-margin-bottom-25 sm-margin-bottom-20 xs-margin-bottom-20 xs-padding-top-10 clearfix">
                    <li><a class="sc-1" href="https://www.facebook.com/eldrive.eu/"></a></li>
                    <li><a class="sc-5" href="https://www.linkedin.com/company/eldrive?trk=company_logo"></a></li>
                    
                </ul>
                
                <ul class="f-nav">
<?php

if (!isset($footer_links)) $footer_links = 5;
$footer_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $footer_links");

for ($i = 0; $i < count($footer_links); $i++) {
echo "<li><a href=\"" . $o_page->get_pLink($footer_links[$i]['n']) . "\">" . $footer_links[$i]['Name'] . "</a></li>";
}
?>
                </ul>
                
            </div>
        </div>
    </div>
</section>

<div class="back_to_top"> <img src="<?=ASSETS_DIR?>images/arrow-up.png" alt="scroll up" /> </div>
<!-- Bootstrap core JavaScript --> 
<!--<script src="<?=ASSETS_DIR?>js/retina.js"></script> -->

<script type="text/javascript" src="<?=ASSETS_DIR?>js/jquery.parallax.js"></script> 
<script type="text/javascript" src="<?=ASSETS_DIR?>js/jquery.inview.min.js"></script> 
<script type="text/javascript" src="<?=ASSETS_DIR?>js/main.js"></script> 
<script type="text/javascript" src="<?=ASSETS_DIR?>js/jquery.fancybox.js"></script> 
<script type="text/javascript" src="<?=ASSETS_DIR?>js/modernizr.custom.js"></script> 
<script defer src="<?=ASSETS_DIR?>js/jquery.flexslider.js"></script> 
<script src="<?=ASSETS_DIR?>js/jquery.bxslider.js" type="text/javascript"></script> 
<script src="<?=ASSETS_DIR?>js/jquery.selectbox-0.2.js" type="text/javascript"></script> 
<script type="text/javascript" src="<?=ASSETS_DIR?>js/jquery.mousewheel.js"></script> 
<script type="text/javascript" src="<?=ASSETS_DIR?>js/jquery.easing.js"></script>   
<script>
jQuery(document).ready( function($){
	$('#featured-brand-slide').bxSlider({
	  auto: true,
	  controls: false,
	  minSlides: 6,
	  maxSlides: 6,
	  slideWidth: 180,
	  slideMargin: 10,
      moveSlides: 6,
	  speed: 2000,
	  pause: 1000,
	  autoHover: true
	});
	$('#actual-models').bxSlider({
	  auto: true,
	  controls: false,
	  minSlides: 5,
	  maxSlides: 5,
	  slideWidth: 180,
	  slideMargin: 10,
      moveSlides: 1,
	  speed: 2000,
	  pause: 1000,
      autoHover: true
	});
});
</script>

<?php include( "Templates/footer_inc.php" ); ?>
</body>
</html>
