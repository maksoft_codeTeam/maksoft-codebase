<!doctype html>
<!--[if IE 7 ]> <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]> <html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="<?php echo($Site->language_key); ?>">
<!--<![endif]--><head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title><?=$Title?></title>
<?php
require_once "lib/lib_page.php";
require_once "lib/Database.class.php";
	    //include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
		$o_site = new site();
$db = new Database();
$o_page->setDatabase($db);
$o_user = new user($user->username, $user->pass);
	?>
<!-- Bootstrap core CSS -->
<link href="<?=ASSETS_DIR?>css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="<?=ASSETS_DIR?>js/html5shiv.js"></script>
      <script src="<?=ASSETS_DIR?>js/respond.min.js"></script>
    <![endif]-->

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Yellowtail%7COpen%20Sans%3A400%2C300%2C600%2C700%2C800" media="screen" />
<!-- Custom styles for this template -->
<link rel="stylesheet" href="<?=ASSETS_DIR?>css/flexslider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?=ASSETS_DIR?>css/jquery.bxslider.css" type="text/css" media="screen" />
<link href="<?=ASSETS_DIR?>css/jquery.fancybox.css" rel="stylesheet">
<link href="<?=ASSETS_DIR?>css/jquery.selectbox.css" rel="stylesheet">
<link href="<?=ASSETS_DIR?>css/style.css" rel="stylesheet" media="all">
<link href="<?=ASSETS_DIR?>css/mobile.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>css/settings.css" media="screen" />
<link href="<?=ASSETS_DIR?>css/animate.min.css" rel="stylesheet" media="all">
<link href="<?=ASSETS_DIR?>css/ts.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="<?=ASSETS_DIR?>js/jquery.min.js"></script>
<script src="<?=ASSETS_DIR?>js/bootstrap.min.js"></script>


<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key&amp;sensor=false"></script>-->

<script type="text/javascript" src="<?=ASSETS_DIR?>js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="<?=ASSETS_DIR?>js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>js/wow.min.js"></script>

<style>
.main-slider {
	background: url(<?=$o_page->get_pImage(19342856)?>) top center no-repeat;
}
</style>

</head>

<body>
<!--Header Start-->
<header  data-spy="affix" data-offset-top="1" class="clearfix">
    <section class="toolbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 left_bar">
                    <ul class="left-none">
                        <!--<li><a href="#"><i class="fa fa-globe"></i> English</a></li>-->
 <!--                       <li><i class="fa fa-search"></i>
                            <input type="search" placeholder="�������.." class="search_box">
                        </li>-->
                    </ul>
                </div>
                <div class="col-lg-6 ">
                    <ul class="right-none pull-right company_info">
						<li><a href="tel:<?php echo $o_page->_site['SPhone'];?>"><i class="fa fa-phone"></i> <?php echo $o_page->_site['SPhone'];?></a></li>
						<li><a href="tel:359895557516"><i class="fa fa-phone"></i> +359 895 557 516</a></li>
                        <li class="address"><a href="<?php echo $o_page->get_pLink(19338096) ?>"><i class="fa fa-map-marker"></i> <?php echo $o_page->_site['SAddress'];?></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="toolbar_shadow"></div>
    </section>
    <div class="bottom-header" >
        <div class="container">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid"> 
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <a class="navbar-brand" href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>"><img alt="logo" src="<?=ASSETS_DIR?>images/logo-white.fw.png"></a> </div>
                    
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav pull-right">

<?php

if (!isset($dd_links)) $dd_links = 2;
$dd_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $dd_links");

for ($i = 0; $i < count($dd_links); $i++) {
    $subpages = $o_page->get_pSubpages($dd_links[$i]['n']);
?>
<li <?php echo ((count($subpages) > 0) ? "class=\"dropdown\"" : "") ?>>
<a href="<?php echo $o_page->get_pLink($dd_links[$i]['n']) ?>" <?php echo ((count($subpages) > 0) ? "class=\"dropdown-toggle\" data-toggle=\"dropdown\"" : "") ?>>
            <?php echo $dd_links[$i]['Name'] ?></a><?php echo ((count($subpages) > 0) ? "<ul class=\"dropdown-menu\">" : "</li>") ?> <?php
    if (count($subpages) > 0) {


        for ($j = 0; $j < count($subpages); $j++)
            echo "<li><a href=\"" . $o_page->get_pLink($subpages[$j]['n']) . "\">" . $subpages[$j]['Name'] . "</a></li>";
?>
          </ul></li>
<?php } ?>

<?php 
 }

?>
					<li><a href="https://portal.eldrive.eu" target="_blank">������������� ������</a></li>
</ul>  
                    </div>
                    <!-- /.navbar-collapse --> 
                </div>
                <!-- /.container-fluid --> 
            </nav>
        </div>
        <div class="header_shadow"></div>
    </div>
</header>
<!--Header End-->

