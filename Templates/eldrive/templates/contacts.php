<section class="content">
    <div class="container">
        <div class="inner-page">
            <div class="col-md-12 padding-none"> 
                <!--CONTACT INFORMATION-->
                <div class="row contacts margin-top-25"> 
                    <!--LEFT INFORMATION-->
                    <div class="col-md-6 left-information">
                        <div class="contact_information information_head clearfix">
                            <h3 class="margin-bottom-25 margin-top-none">��������</h3>
                            <div class="address clearfix margin-right-25 padding-bottom-40">
                                <div class="icon_address">
                                    <p><i class="fa fa-map-marker"></i><strong>�����:</strong></p>
                                </div>
                                <div class="contact_address">
                                    <p class="margin-bottom-none"><?php echo $o_page->_site['SAddress'];?></p>
                                </div>
                            </div>
                            <div class="address clearfix address_details margin-right-25 padding-bottom-40">
                                <ul class="margin-bottom-none">
									<li><i class="fa fa-phone"></i><strong>�������:</strong> <span><a href="tel:"><?php echo $o_page->_site['SPhone'];?></a></span></li>
                                    <li><i class="fa fa-envelope-o"></i><strong>Email:</strong> <a href="mailto:<?php echo $o_page->_site['EMail'];?>"><?php echo $o_page->_site['EMail'];?></a></li>
                                </ul>
                            </div>
                        </div>
                <!--MAP-->
                <div class="find_map row clearfix">
                    <h2 class="margin-bottom-25 margin-top-none">���� �� �� ��������</h2>
                    <div class="map margin-vertical-30" style="height:350px;">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d509.49449792164353!2d23.3684414!3d42.6214277!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa8133d4439315%3A0x559ca924b70a63aa!2zTGluZG5lciBCdWxnYXJpYSBMdGQuICjQm9C40L3QtNC90LXRgCDQkdGK0LvQs9Cw0YDQuNGPINCV0J7QntCUKQ!5e1!3m2!1sbg!2sbg!4v1502355519131" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <!--MAP--> 
                    </div>
                    <!--LEFT INFORMATION--> 
                    
                    <!--RIGHT INFORMATION-->
                    <div class="col-md-5 col-lg-offset-1 col-md-offset-1 padding-right-none xs-padding-left-none sm-padding-left-none xs-margin-top-30">
                        <div class="contact_wrapper information_head">
                            <h3 class="margin-bottom-25 margin-top-none">��������� ���������</h3>
                            <div class="form_contact margin-bottom-20">
                                <div id="result"></div>
                                <?php include TEMPLATE_DIR . "forms/contact.php"; ?>
                            </div>
                        </div>
                    </div>
                    <!--RIGHT INFORMATION--> 
                    
                </div>
                <!---CONTACT INFORMATION--> 
                
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!--container ends--> 
</section>