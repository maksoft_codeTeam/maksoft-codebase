<div class="box" id="menu">
	<div class="box-title"></div>
	<div class="box-content">
	<?php 
        $menu_links = $o_page->get_pSubpages($o_site->_site['StartPage']);
        for($i=0; $i<count($menu_links); $i++)
            echo "<a href=\"".$o_page->get_pLink($menu_links[$i]['n'])."\" title=\"".$menu_links[$i]['title']."\" class=\"".(($o_page->n == $menu_links[$i]['n'])? "selected":"")."\"><div class=\"bullet\"></div>".$menu_links[$i]['Name']."</a>";
        
    ?>
	</div>
    <div class="box-footer"></div>
</div>