<link rel="stylesheet" href="http://lib.maksoft.net/jquery/nivo-slider/nivo-slider.css" type="text/css" media="screen" />
<script type="text/javascript" src="http://lib.maksoft.net/jquery/nivo-slider/jquery.nivo.slider.js"></script>
<script language="javascript" type="application/javascript"> 
	$j(document).ready(function(){
		$j(".tab-content").hide();
		$j("#schedule-1").show();
		$j(".tab").each(function(){
			$j(this).hover(function(i){
				$j(".tab-content").hide();
				$j(".tab").removeClass("selected");
				$j(this).addClass("selected");
				$j("#"+$j(this).attr("rel")).toggle();
				})
			})
		//home banner slider	
		$j('#banner').nivoSlider({
			  effect: 'sliceDown', // Specify sets like: 'fold,fade,sliceDown'
			  slices: 15, // For slice animations
			  boxCols: 8, // For box animations
			  boxRows: 4, // For box animations
			  animSpeed: 500, // Slide transition speed
			  pauseTime: 5000, // How long each slide will show
			  startSlide: 0, // Set starting Slide (0 index)
			  directionNav: false, // Next & Prev navigation
			  controlNav: true, // 1,2,3... navigation
			  controlNavThumbs: false // Use thumbnails for Control Nav

		  });

		})
</script>
<div class="slider-content">
    <div class="banner-content">
    <div id="banner"> 
		<a href="#"><img src="<?=TEMPLATE_IMAGES?>_banner.jpg"></a>
        <a href="#"><img src="<?=TEMPLATE_IMAGES?>_banner2.jpg"></a>
        <a href="#"><img src="<?=TEMPLATE_IMAGES?>_banner.jpg"></a>
        <a href="#"><img src="<?=TEMPLATE_IMAGES?>_banner2.jpg"></a>
	</div>
    <a href="<?=$o_page->get_pLink(188245)?>

" title="<?=$o_page->get_pTitle("strict", 188245)?>" class="btn_learn_more"><img src="<?=TEMPLATE_IMAGES?>btn_learn_more.png"></a>
    </div>
</div>
<div class="shadow top"></div>
<div id="page_container">
	<?php
		$p_services = new page(188244);
		$p_prices = new page(190001);
		$p_about = new page(188241);
		$p_schedule = new page(188245);
	?>
    <div id="home">
		<div class="box left">
		<h2><?=$p_services->print_pName()?></h2>
		<?=$p_services->print_pImage(200, "class=\"main_image align-top-left\"") . $p_services->get_pText()?>
        </div>
        <div class="box right">
        <h2><?=$p_prices->print_pName()?></h2>
        <table class="content" cellspacing="10">
        	<tr>
            	<td align="center" height="145px"><img src="<?=TEMPLATE_IMAGES?>double-room.jpg">
                <td align="center" height="145px"><img src="<?=TEMPLATE_IMAGES?>single-room.jpg">
            <tr>
            <tr>
            	<td valign="top"><span class="price">68,<sup>00</sup></span>������ ����<br> �� ��� <br><span>����� �����</span>
                <td valign="top"><span class="price">90,<sup>00</sup></span>�������������<br> ���� �� ��� <br><span>����� �����</span>
            <tr>
        </table>
        </div>
    </div>
</div>
<div class="shadow bottom"></div>
<br clear="all">
<div id="box-about">
	<div class="content">
        <div class="box left">
            <h2 class="title gradient-green"><?=$p_about->print_pTitle("strict")?></h2>
            <p><a href="#" class="main_image"><?=$p_about->print_pImage(267)?></a><?=$p_about->get_pText()?></p>
        </div>
        <div class="box schedule right">
         <div class="title gradient-green">
				<a href="#schedule" rel="schedule-1" class="tab selected">����� �����</a>
                <a href="#schedule" rel="schedule-2" class="tab">�������� �����</a>
                <a href="#schedule" rel="schedule-3" class="tab">����� �����</a>
         </div>
         <div id="schedule-1" class="tab-content">
         	<ul>
                <li>04.02.13 �� 13.02.13</li>
                <li>18.02.13 �� 27.02.13</li>
                <li>04.03.13 �� 13.03.13</li>
                <li>18.03.13 �� 27.03.13</li>
            </ul>
         </div>
         <div id="schedule-2" class="tab-content">
             <ul>
                <li>01.04.13 �� 10.04.13</li>
                <li>15.04.13 �� 24.04.13</li>
                <li>29.04.13 �� 08.05.13</li>
                <li>13.05.13 �� 22.05.13</li>
                <li>27.05.13 �� 05.06.13</li>
            </ul>
        </div>
        <div id="schedule-3" class="tab-content">
        	<ul>
            	<li>10.06.13 �� 19.06.13</li>
            	<li>24.06.13 �� 03.07.13</li>
            	<li>08.07.13 �� 17.07.13</li>
            	<li>22.07.13 �� 31.07.13</li>
            	<li>05.08.13 �� 14.08.13</li>
            	<li>19.08.13 �� 28.08.13</li>
            </ul>
        </div>
        <h2>������ �����</h2>
            <ul>
            <li>������� �� ��� - ������</li>
            <li>�������</li>
            <li>�������������(�����)/<wbr>����������</wbr></li>
            <li>�������� �������</li>
            <li>�������������(�����)/<wbr>����������</wbr></li>
            <li>����</li>
            </ul>
            <ul>
            <li>�������� �����</li>
            <li>�������������(�����)</li>
            <li>���������� �������</li>
            <li>������������(�������� ��� ����)</li>
            <li>������</li>
            <li>�������� �����</li>
            <li>���</li>
            </ul>	
        </div>
	</div>
</div> 
<br clear="all">   