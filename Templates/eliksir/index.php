<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
	<title><?=$Title?></title>
    <?php
		//include template configurations
		include("Templates/eliksir/configure.php");		
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
    <link href="<?=HTTP_SERVER ."/". TEMPLATE_DIR?>layout.css" id="layout-style" rel="stylesheet" type="text/css" />
    <link href="<?=HTTP_SERVER?>/css/admin_classes.css" rel="stylesheet" type="text/css" />
    <link href="<?=HTTP_SERVER ."/".TEMPLATE_DIR?>base_style.css" id="base-style" rel="stylesheet" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <script language="javascript" type="text/javascript" src="<?=HTTP_SERVER ."/".TEMPLATE_DIR?>effects.js"></script>

</head>
<?php
	//get site news
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 5");
	
	//get most viewed pages
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5");

	//get last added pages
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5");
	
	//top menu
	$top_menu = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC");
?>
<body>
<div id="site_container">
    <div id="header">
        	<?php include TEMPLATE_DIR."header.php"?>
    </div>
    <div style="padding-top:110px">
        <?php
			if(strlen($search_tag)>2 || strlen($search)>2)
				include TEMPLATE_DIR . "main.php";
			else
			if($o_page->_page['n'] == $o_site->_site['StartPage'])
				include TEMPLATE_DIR . "home.php";
			elseif($o_page->n == 190003)
				include TEMPLATE_DIR . "pages/contacts.php";
			elseif($o_page->_page['SiteID'] == $o_site->_site['SitesID'])
				include TEMPLATE_DIR . "main.php";
			else
				include TEMPLATE_DIR . "admin.php";
		?>


    </div>
    <div id="footer">
    	<div class="footer-content">
		<?php
			include TEMPLATE_DIR . "footer.php";
		?>
        </div>
    </div>
    <?php
	if($o_page->_page['n'] != $o_site->_site['StartPage'] || $user->AccessLevel >0)
		{
    ?>
	<div id="navbar">
		<div class="navbar-content"><?=$o_page->print_pNavigation()?></div>
	</div>
    <?
		}
	?>
    <div class="copyrights">
        <div class="copyrights-content">
        <?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a>, web design & development: <a href="http://www.pmd-studio.com" target="_blank" title="Web design"><b>pmd</b>studio</a></div></div>
        </div>
    </div>
</div>
</body>
</html>