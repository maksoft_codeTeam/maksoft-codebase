<!DOCTYPE HTML>
<html lang="<?php echo($Site->language_key); ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<title><?=$Title?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
		//include template configurations
		include("Templates/base/tmpl_001/configure.php");		
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
		<link href="Templates/hebarcup/assets/css/landpage.css" rel="stylesheet" type="text/css">
		<link href="Templates/hebarcup/assets/css/jquery.countdown.css" rel="stylesheet" type="text/css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
		<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
		<script src="Templates/hebarcup/assets/js/jquery.countdown.min.js"></script>
	</head>
	<body>
		<div id="outer">
			<a href="">
				<div class="MEINCONT">
					
				</div>
			</a>
			<div class="side_information">
			<?php
			$language_id = $o_page->_site['language_id'];
			$days; $hours; $minutes; $seconds; $overtext;
			switch($language_id){
				case 1:
					$days="���";
					$hours="����";
					$minutes="������";
					$seconds="�������";
					$overtext="�� �������� �������";
					break;
				case 2:
					$days="Days";
					$hours="Hours";
					$minutes="Minutes";
					$seconds="Seconds";
					$overtext="Remaining time to go";
					break;
			}
			?>
			<div class="languageflags">
			<?php
			$langs = $o_page->print_sVersions();
			//var_dump($langs);
			?>
			</div>
				<div id="clock">
					<h2 class="overtext"><?=$overtext;?></h2>
					<ul id="example">
					  <li><span class="days">00</span><p class=""><?=$days;?></p></li>
						<li class="seperator">:</li>
						<li><span class="hours">00</span><p class=""><?=$hours;?></p></li>
						<li class="seperator">:</li>
						<li><span class="minutes">00</span><p class=""><?=$minutes;?></p></li>
						<li class="seperator">:</li>
						<li><span class="seconds">00</span><p class=""><?=$seconds;?></p></li>
					</ul>

					<script type="text/javascript">
						$('#example').countdown({
							date: '05/15/2015 23:59:59'
						});
					</script>
				</div>
				<div id="menu_page">
					<ul id="sitelinks">
					<?php
					//var_dump($o_page->n);
					$subpagesArray = $o_page->get_pSubpages($o_page->n);
					foreach($subpagesArray as $subpage){
						$title = $subpage['title'];
						$page_link = $subpage['page_link'];
						/* var_dump($title);
						var_dump($page_link); */
						?>
						<li><a href="<?=$page_link;?>"><?=$title;?></a></li>
						<?php
					}
					?>
					</ul>
				</div>
				<div class="impulsteamlogo">
					<a href="#">
						<img src="/Templates/hebarcup/assets/images/impuls-team.png"/>
					</a>
				</div>
				<div id="imagesHolder">
					<div class="smalllogo">
						<img src="/Templates/hebarcup/assets/images/logo-one.png" height="150"/>
					</div>
					<div class="smalllogo">
						<img src="/Templates/hebarcup/assets/images/logo-three.png" width="160"/>
					</div>
					<div class="smalllogo">
						<img src="/Templates/hebarcup/assets/images/logo-two.png" width="200"/>
					</div>
				
				</div>
				<div id="hitcounter">
					<h3>
					<?php
					 echo $o_page->print_pVisits();
					/*  $thisfolder = getcwd();
					 $thisfolder.="/Templates/hebarcup/";
					 $folders = scandir($thisfolder);
					 //var_dump($folders);
					 $File = $thisfolder."hitcounter.txt"; 
					 //This is the text file we keep our count in, that we just made
					//echo $File;
					 $handle = fopen($File, 'r+') ; 
					 //Here we set the file, and the permissions to read plus write
					 $data = fread($handle, filesize($File)) ; 
					 //echo $data;
					 $count = intval($data) + 1;
					 $handle = fopen($File, 'w+') ; 
					 //echo $data;
					 //@ftruncate($handle, 0);
					 fwrite($handle, $count);
					
					 //Actully get the count from the file

					 //Add the new visitor to the count

					 //echo $File;
					 switch($language_id){
						case 1:
							$visitorText="��������� �� ���������� : <strong>".$count."</strong>";
							break;
						case 2:
							$visitorText="Times page is visited : <strong>".$count."</strong>";
							break;
					}
					 print $visitorText;  */
					?>
					</h3>
				</div>
			</div>
		</div>
	</body>
</head>