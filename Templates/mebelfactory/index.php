<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<?php
include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/mebelfactory/");
define("DIR_TEMPLATE_IMAGES", "Templates/mebelfactory/images/");
define("DIR_MODULES","web/admin/modules/");

$o_site->print_sConfigurations();

//define some pages
$vip_menu = $o_page->get_pSubpages(0, "p.date_added DESC", "p.toplink=4");
$second_links = $o_page->get_pSubpages(0, "sort_n ASC", "(p.toplink=5 OR p.toplink=1)");
$news = $o_page->get_pSubpages($o_site->get_sNewsPage(), "p.date_added DESC", "p.toplink=0");

$page_orders = $o_page->get_page(132885);
$page_news = $o_page->get_page($o_site->get_sNewsPage());

?>
<title><?php echo("$Title");?></title>
<link href="http://www.maksoft.net/Templates/mebelfactory/layout.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/mebelfactory/base_style.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/lib/datepicker/datepicker.css" rel="stylesheet" type="text/css">
<!--[if IE]>
<link href='http://www.maksoft.net/Templates/mebelfactory/ie_fix.css' rel='stylesheet' type='text/css'>
<![endif]-->
<script src="http://www.maksoft.net/lib/jquery/jquery-accordion/jquery.accordion.js" language="JavaScript" type="text/javascript"></script>
<script src="http://www.maksoft.net/lib/jquery/jquery.easing.1.3.js" language="JavaScript" type="text/javascript"></script>
<script src="http://www.maksoft.net/lib/jquery/jquery.tagcloud.min.js" language="JavaScript" type="text/javascript"></script>
<script src="http://www.maksoft.net/lib/jquery/roundabout/jquery.roundabout-shapes.min.js" language="JavaScript" type="text/javascript"></script>
<script src="http://www.maksoft.net/lib/jquery/roundabout/jquery.roundabout.min.js" language="JavaScript" type="text/javascript"></script>
<script src="http://www.maksoft.net/Templates/mebelfactory/effects.js" language="JavaScript" type="text/javascript"></script>
<script src="http://www.maksoft.net/lib/datepicker/datepicker.js" language="JavaScript" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>
<body>
<div align="center" class="bg">
	<div id="page_container" align="center">
		<div id="header">
        	<a href="http://<?=$o_site->get_sURL()?>" title="<?=$o_site->get_sTitle()?>" class="logo" style="display:block; height: 155px; width: 340px; float:left"></a>
            <a href="page.php?n=138718&SiteID=857" title="������ �� �������" class="button_request" style="width: 165px; height:85px; margin:40px 0 0 180px; float:left"></a>
            <br clear="all">
			<div id="top_menu">
			<?php
				$top_menu = $o_page->get_pSubpages(0, "sort_n ASC", "p.toplink=1");
				$bullet = "";
				for($i=0; $i<count($top_menu); $i++)
					{	
						$class = "";
						if(($o_page->n == $top_menu[$i]['n'] || ($o_page->get_pParent() == $top_menu[$i]['n'] && $o_page->get_pParent() != $o_site->get_sStartPage())) && $o_page->n != $o_site->get_sStartPage()) $class = "selected";
						echo "<a href=\"".$top_menu[$i]['page_link']."\" title=\"".$top_menu[$i]['Name']."\" class=\"".$class."\">".$top_menu[$i]['Name']."</a>";
					}
			?>
			</div>
		</div>
        <div id="nav_links"><?=($user->AccessLevel> 0) ? $o_page->get_pNavigation():""?>

</div>
		<div id="<?=$o_page->get_pSiteID() == $o_site->SiteID ? "main":"admin"?>">
				<div class="content">
					<?php
						if($o_page->n == $o_site->get_sStartPage())
							include "home.php";
						else
							include "main.php";
					?>
				</div>
		<div id="footer"><?php include "footer.php";?></div>
        <div class="credits"><?=$o_site->get_sCopyright().", ".date("Y").", hosting & cms: <a href=\"http://www.maksoft.net\" title=\"�������\" target=\"_blank\">netservice</a> / web design: <a href=\"http://www.pmd-studio.com\" title=\"Web design\"><b>pmd</b>studio</a>"?></div>
        </div>
		</div>
	</div>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19463072-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
