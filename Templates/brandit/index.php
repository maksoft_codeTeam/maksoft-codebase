<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
require_once "lib/lib_page.php";

include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/brandit/");
define("DIR_TEMPLATE_IMAGES", DIR_TEMPLATE."images/");
define("DIR_MODULES","web/admin/modules/");
define("ASSETS_DIR", "/Templates/brandit/");

$o_site->print_sConfigurations();

//promo products
$promo_products = $o_page->get_pGroupContent(389, "", "p.sort_n ASC LIMIT 5", 1);

//news
$news = $o_page->get_pSubpages($o_site->get_sNewsPage(), "p.date_added DESC LIMIT 2");

//main menu, products page
$menu = $o_page->get_pSubpages($n_products_cat, "p.sort_n ASC");

//bottom menu, products page
$bottom_menu = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC", "p.toplink != 3");

//home photo gallery
$home_gallery = $o_page->get_pSubpages(0, "RAND() LIMIT 6", "im.image_src != ''");

//partners list
$partners = $o_page->get_page($n_partners);
$partners_list = $o_page->get_pSubpages($partners['n']);

//products list
$n_products = 161045;
$products = $o_page->get_pSubpages($n_products);

$p_categories = $o_page->get_pGroupContent(273);

//latest products
$latest_products = $o_page->get_pGroupContent(3255);
//$latest_products = $o_page->get_pSubpages($n_products, "RAND() LIMIT 10", "p.date_added <= DATE_ADD(CURDATE(), INTERVAL 360 DAY)");

//define some pages
	$n_contacts = 169288;
	$n_cart = 13202;
	$n_sitemap = 170695;
	
?>
<title><?php echo("$Title");?></title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
<link href="<?=ASSETS_DIR?>layout.css" rel="stylesheet" type="text/css">
<link href="<?=ASSETS_DIR?>base_style.css" rel="stylesheet" type="text/css">
<link href="<?=ASSETS_DIR?>easyslider.css" rel="stylesheet" type="text/css">
<link href="<?=ASSETS_DIR?>sitemap.css" rel="stylesheet" type="text/css">
<link href="<?=ASSETS_DIR?>responsive.css" rel="stylesheet" type="text/css">
<link href="<?=ASSETS_DIR?>jScrollPane.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
	<link href="http://www.maksoft.net/Templates/brandit/ie7hacks.css" rel="stylesheet" type="text/css">
<![endif]-->
<script language="JavaScript" src="/lib/jquery/jScrollPane/jScrollPane.js" type="text/javascript"></script>
<script language="JavaScript" src="/lib/jquery/easyslider/easySlider1.7.js" type="text/javascript"></script>
<script language="JavaScript" src="<?=ASSETS_DIR?>effects.js" type="text/javascript"></script>
</head>
<body>
<div class="bg">
<div id="desktop">
	<div id="page_container">
		<div id="header">
            <div id="icons"><div class="content"><a href="<?=$o_page->get_pLink($n_contacts)?>" class="contacts"></a><a href="<?=$o_page->get_pLink($n_cart)?>" class="cart"><?php if(count($_SESSION['cart_items'])>0) echo "<div class=\"items_count\">".count($_SESSION['cart_items'])."</div>";?>

</a><a href="<?=$o_page->get_pLink($n_sitemap)?>" class="sitemap"></a></div></div>
        	<img src="/<?=DIR_TEMPLATE_IMAGES?>header_bg.png" class="logo">
            <!--<a href="http://maksoft.net/download/BrandIt_2012_low.pdf" target="_blank" class="download"></a>//-->
		</div>
		<div style="display: table; background: url(https://www.maksoft.net/Templates/brandit/images/bg_content.png) 50% 10px no-repeat;">
            <div id="column_left">
                <div id="menu">
                <?php 
                    //expand all submenus
                    $view_submenu = "expand";
                    //expand selected pages
                    $view_submenu_array = array(169287);
                    $view_menu_item = 3;
					$submenu_bullet = "";
                    include DIR_MODULES."menu/menu_standart.php";
                ?>
                <form method="get" class="search" action="page.php">
                	<input type="hidden" name="n" value="170689">
                    <input type="hidden" name="SiteID" value="<?=$SiteID?>">
                	<input type="text" name="search" id="search" value="<?=$o_site->get_sSearchText()?>">
                </form>
                </div>
                <div id="box_news">
                	<h3>News</h3>
                    <?php
						for($i=0; $i<count($news); $i++)
							echo "<div class=\"news\"><a href=\"".$news[$i]['page_link']."\" class=\"title\">".$news[$i]['Name']."</a>".crop_text($news[$i]['textStr'])."<a href=\"".$news[$i]['page_link']."\" class=\"next_link\">read more</a></div>";
					?>
                </div>
                <div id="social_links"><div class="sl_content"><a href="https://www.twitter.com/BranditOnline" target="_blank" class="tweeter"></a><a href="/rss.php" target="_blank" title="Brandit RSS Feed" class="rss"></a><a href="https://www.facebook.com/Maksoft.Net" target="_blank" class="facebook"></a></div></div>
            </div>
            <div id="main" class="rounded">
                <div id="top_menu">
                <a href="<?=$o_page->get_pLink($o_page->get_pParent())?>"></a><?=$o_page->print_pNavigation("short", $o_page->n, "")?>
                </div>
				<?php
				    if($o_page->n == $o_site->get_sStartPage() && strlen($_GET['search_tag'])<=2)
                        include "home.php";
                    else
                        include "main.php";
					
					include "box_tags.php";
                ?>
            	<div id="footer">
                <?php
					if($user->AccessLevel > 0)
						{
							?>
								<div id="nav_links"><?=$o_page->get_pNavigation()?></div>
							<?
						}
				?>
                	<div class="content">
                    	<div class="coll">
                        	<h3>products</h3>
                            <ul>
                            <?
                            	$coll_links = $o_page->get_pSubpages(169287);
								for($i=0; $i<count($coll_links); $i++)
									{
										echo "<li>";
										$o_page->print_pName(true, $coll_links[$i]['n']);
										echo "</li>";
									}
							?>
                            </ul>
                    	</div>
                        <div class="coll">
                        	<h3>web services</h3>
                            <ul>
                                <li><a href="#">Web SEO services</a>
                                <li><a href="#">Hosting + CMS</a>
                                <li><a href="#">Web design</a>
                            </ul>
                    	</div>
                        <div class="coll">
                        	<h3>external links</h3>
                            <ul>
                                <li><a href="#">Free ads publications</a>
                                <li><a href="#">Producers, importers, companies</a>
                                <li><a href="#">Print & prepress</a>
                            </ul>
                    	</div>
                        <div class="coll">
                        	<h3>sponsored links</h3>
                            <ul>
                                <li><a href="#">www.maksoft.net</a>
                                <li><a href="#">www.prn.bg</a>
                                <li><a href="#">www.ad-bg.net</a>
                            </ul>
                    	</div>
                    </div>
                    <hr style="margin: 0 20px 0 20px;">
                	<div class="bottom_menu">
                    	
					<?php
						for($i=0; $i<count($bottom_menu); $i++)
							{
								if($i>0) echo "&nbsp;&nbsp;|&nbsp;&nbsp; ";
							
								$o_page->print_pLink($bottom_menu[$i]['n']);
							}
					?>
				</div>
                <!--<div class="credits"><a href="http://www.pmd-studio.com" target="_blank" title="�������� & ��� ������"><b>pmd</b>studio</a> / <a href="http://www.maksoft.net" target="_blank" title="CMS, SEO, hosting & �������"><b>maksoft</b>.net</a></div>//-->
                <a class="copyrights" href="https://maksoft.net/page.php?n=38146&SiteID=1" target="_blank"></a>
                </div>
            </div>
            <br clear="all"><div style="width: 750px; text-align:left; float:right; margin: 20px 0 0 0"><?=$o_page->get_sCopyright() ." - ".date("Y")?></div>
    </div>
</div>
</div>
<div id="mobile">
<link href="/Templates/brandit-2016/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<?php
     include DIR_TEMPLATE . "mobile/mainmenu.php";
	$pTemplate = $o_page->get_pTemplate();
	if($o_page->_page['SiteID'] != $o_site->_site['SitesID'] )
		include DIR_TEMPLATE."mobile/admin.php";	
	elseif($pTemplate['pt_url'])
		include $pTemplate['pt_url'];
	elseif($o_page->_page['n'] == $o_site->_site['StartPage'] && strlen($search_tag)<3 && strlen($search)<3)
		include DIR_TEMPLATE . "mobile/home.php";
	else
		include DIR_TEMPLATE . "mobile/main.php";
	?>
</div>
<?php include "Templates/footer_inc.php"; ?>
</body>
</html>