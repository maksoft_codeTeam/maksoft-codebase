<?php
$homepage=$o_site->StartPage;
$subPs = $o_page->get_pSubpages($homepage);

?>
<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img src="<?=ASSETS_DIR?>images/logo-small.png" alt="Brandit" /></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <?php
				print_menu($subPs, array('menu_depth'=>0));
			?>
        </div><!--/.nav-collapse -->
		<script>
        var $ = jQuery;
		$(document).ready(function(){
			$("#navbar>ul").addClass("nav navbar-nav");
			<?php
			if(isset($_SESSION["user"])){
				?>
			$("#navbar>ul").append('\
			<li class="active">\
				<a href="http://maksoft.net/web/admin/logout.php">\
				  <span class="glyphicon glyphicon-user"></span> <?=$_SESSION["user"]->Name;?> | <i class="glyphicon glyphicon-log-out"></i> Logout\
				</a>\
            </li>');
				<?php
			}else{
				?>
			$("#navbar>ul").append('\
			<li>\
				<a class="loginer" data-toggle="modal" data-target="#login">\
				  <span class="glyphicon glyphicon-user"></span> Login\
				</a>\
            </li>');
				<?php
			}
			?>
		});
		</script>
      </div>
</nav>
<div class="container">
