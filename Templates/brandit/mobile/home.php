	<!-- 
Latest compiled and minified jQuery -->


	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->


<div id="home">
<div id="promo">
	<div id="box_promo" class="box"><h2>Promo products <?php if($user->AccessLevel >2) echo "[<a href=\"page.php?n=63931&SiteID=888&group_id=389\">+</a>]";?></h2>
    	<ul>
			<?php
				for($i=0; $i<count($promo_products); $i++)
					{
						$pPrice = 0;
						$pPrices = array();
						$pPrices = $o_page->get_pPrice($promo_products[$i]['n']);

						//$percents = number_format((($pPriceMAX[0]['price_value']/$pPriceMAX[0]['pcs'])-($pPriceMIN[0]['price_value']/$pPriceMIN[0]['pcs']))/($pPriceMIN[0]['price_value']/$pPriceMIN[0]['pcs']), 2, ".", "")*100;
						//$pPrice = $pPrices[0]['price_value'];
						
						$pPriceMIN = $pPrices[0]['price_value_single'];
						$pPriceMAX = $pPrices[0]['price_value_single'];
						
						//loop trough the prices to find the minimal & maximal price
						for($j=0; $j<count($pPrices); $j++)
							{
								$pPriceMAX = ($pPrices[$j]['price_value_single'] > $pPriceMAX) ? $pPrices[$j]['price_value_single'] : $pPriceMAX;
								$pPriceMIN = ($pPrices[$j]['price_value_single'] < $pPriceMIN) ? $pPrices[$j]['price_value_single'] : $pPriceMIN;
								
							}
						
						$percents = number_format(($pPriceMIN - $pPriceMAX)/$pPriceMAX, 2, ".", "")*100;
						
						$pPriceMIN = number_format($pPriceMIN, 2, '.', '');	
						$pPriceMAX = number_format($pPriceMAX, 2, '.', '');	
							
						$cart_link = "";
						$price_text = "";
						$currency_text = "";
						$percents_text = "";
						$special_price_class = "";
						if($percents != 0)
							{
								$percents_text = "<div class=\"discount\">".$percents."%</div>";
								$special_price_class = "special";
							}
						
						//	view currency
						if(SHOPPING_CART_VIEW_CURRENCY)
							$currency_text = "&nbsp;".$pPrices[0]['currency_string'];
							
						if(count($pPrices) == 1)
							{
								$cart_link = "<a href=\"".$o_page->get_pLink(13202)."&amp;action=insert&amp;pid=".$pPrices[0]['id']."\" class=\"add_to_cart\"></a>";
								$price_text = "<div class=\"price\">".$pPriceMIN.$currency_text."</div>";
							}
						if(count($pPrices) > 1)
							{
								$cart_link = "<a href=\"".$promo_products[$i]['page_link']."\" class=\"add_to_cart\"></a>";
								$price_text = "<div class=\"price ".$special_price_class."\">".$pPriceMIN.$currency_text."</div>";
							}
						echo "<li>".$percents_text."<div class=\"promo_info\">".$cart_link."<div class=\"title\">".$promo_products[$i]["Name"]."</div>".$price_text."</div><a href=\"".$promo_products[$i]['page_link']."\"><img src=\"img_preview.php?image_file=".$promo_products[$i]["image_src"]."&amp;img_width=355&amp;ratio=strict\"></a></li>";
					}
			?>


        </ul>
    </div>

</div>
<h1 class="head_text">Product categories</h1>
<a href="<?=$o_page->get_pLink($o_page->get_pParent())?>" title="<?=$o_page->get_pName($o_page->get_pParent())?>" class="button_up"></a>
<div id="pageContent">
	<?php
		//$o_page->print_pContent();
		//$o_page->print_pSubContent();
	?>
    <div class="box_categories">
        <?php
            
			//product categories
            $categories = $o_page->get_pGroupContent(273);
    
            for($i=0; $i<count($categories); $i++)
                {
                    if($categories[$i]['show_group_id']==0)
						$sublinks = $o_page->get_pSubpages($categories[$i]['n'], "p.date_added DESC LIMIT 5");
                    else
						$sublinks = $o_page->get_pGroupContent($categories[$i]['show_group_id'], "p.date_added DESC LIMIT 5");
						
					$sublinks_content = "<div class=\"sublinks\">";
                    for($j=0; $j<count($sublinks); $j++)
                        {
                            if($j>0) $sublinks_content.= ", ";
                            $sublinks_content.= "<a href=\"".$sublinks[$j]['page_link']."\">".$sublinks[$j]['Name']."</a>";
                        }
                    $sublinks_content.= "</div>";
                    echo "<div class=\"category\"><a href=\"".$categories[$i]['page_link']."\" class=\"title\">".$categories[$i]['Name']."</a>".$sublinks_content."</div>";
                }
			echo "<div class=\"category\"><a href=\"#\" class=\"more\">more products</a></div>";
        ?>
    </div>
	<div class="shadow"></div>
    <div class="box_howto">
   	  <h2><?=$o_page->print_pName(true, 170662)?></h2>
        <a href="<?=$o_page->get_pLink(170664)?>" title="<?=$o_page->get_pName(170664)?>"><img src="<?=DIR_TEMPLATE_IMAGES?>_img1.jpg" style="margin: 0 0 0 0;"></a><a href="<?=$o_page->get_pLink(170721)?>" title="<?=$o_page->get_pName(170721)?>"><img src="<?=DIR_TEMPLATE_IMAGES?>_img2.jpg"></a><a href="<?=$o_page->get_pLink(170722)?>" title="<?=$o_page->get_pName(170722)?>"><img src="<?=DIR_TEMPLATE_IMAGES?>_img3.jpg"></a><a href="<?=$o_page->get_pLink(170723)?>" title="<?=$o_page->get_pName(170723)?>"><img src="<?=DIR_TEMPLATE_IMAGES?>_img4.jpg"></a>
    </div>
</div>
</div>
