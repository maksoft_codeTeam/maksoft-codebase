
<?php
	//get subpages
	$group_id = $o_page->get_pInfo("show_group_id");
	
	if(isset($group_id) && !empty($group_id))
		$products = $o_page->get_pGroupContent();
	else 
		$products = $o_page->get_pSubpages();
	
	for($i=0; $i<count($products); $i++)
		{
			$s_group_id = $o_page->get_pInfo("show_group_id", $products[$i]['n']);
			if(isset($s_group_id) && !empty($s_group_id))
				$s_pages = $o_page->get_pGroupContent($s_group_id);
			else
				$s_pages = $o_page->get_pSubpages($products[$i]['n']);
			
			$class = " pr".$i;
		?>
			<table class="product_preview<?=$class?>" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td rowspan="2" width="180" align="left" valign="top">
					<div class="img">
					<div class="num">.0<?=$i+1?></div>
					<?php $o_page->print_pImage("150", "", "", $products[$i]['n']); ?>
					</div>
					<td valign="middle" height="40px"><h2><?=$products[$i]['Name']?></h2>
				<tr>
					<td valign="top" height="80px">
						<ul>
						<?
							for($j=0; $j<count($s_pages); $j++)
								echo "<li><a href=\"".$s_pages[$j]['page_link']."\">".$s_pages[$j]['Name']."</a></li>";	
						?>
						</ul>
				<tr>
					<td colspan="2" height="30px"><a href="<?=$products[$i]['page_link']?>" class="next_link"></a>
			</table>
		<?
		}
?>
