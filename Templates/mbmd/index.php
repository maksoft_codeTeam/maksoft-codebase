<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<?php
require_once "lib/lib_page.php";

include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/mbmd/");
define("DIR_TEMPLATE_IMAGES", "Templates/mbmd/images/");
define("DIR_MODULES","web/admin/modules/");

$o_site->print_sConfigurations();

//define some pages
$second_links = $o_page->get_pSubpages(0, "sort_n ASC", "(p.toplink=5 OR p.toplink=1)");

$page_services = $o_page->get_page(143371);
$page_news = $o_page->get_page($o_site->get_sNewsPage());

$news = $o_page->get_pSubpages($page_news['n'], "p.date_added DESC");
$services = $o_page->get_pSubpages($page_services['n']);


?>
<title><?php echo("$Title");?></title>
<link href="http://www.maksoft.net/Templates/mbmd/layout.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/mbmd/base_style.css" rel="stylesheet" type="text/css">
<!--[if IE]>
<link href='http://www.maksoft.net/Templates/mbmd/ie_fix.css' rel='stylesheet' type='text/css'>
<![endif]-->
<link href="http://www.maksoft.net/Templates/mbmd/contentslider.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="lib/contentslider/contentslider.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>
<body>
<div align="center" class="bg">
	<div id="page_container" align="center">
		<div id="header">
		<?php include "header.php"; ?>
		</div>
		<div id="<?=$o_page->get_pSiteID() == $o_site->SiteID ? "main":"admin"?>">
				<div id="nav_links"><?=$o_page->print_pNavigation()?></div>
				<div class="content">
					<?php
						if($o_page->n == $o_site->get_sStartPage())
							include "home.php";
						else
							include "main.php";
					?>
				</div>
		<div id="footer"><?php include "footer.php"; ?></div>
        <div class="credits"><?=$o_site->get_sCopyright().", ".date("Y").", hosting & cms: <a href=\"http://www.maksoft.net\" title=\"�������\" target=\"_blank\">netservice</a> / web design: <a href=\"http://www.pmd-studio.com\" title=\"Web design\"><b>pmd</b>studio</a>"?></div>
        </div>
		</div>
	</div>
</div>
</body>
</html>
