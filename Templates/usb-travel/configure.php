<?php
	//template usbtravel configurations
	
	define("TEMPLATE_DEFAULT_BANNER", "");
	define("TEMPLATE_DEFAULT_IMAGE_DIR", "");
	define("TEMPLATE_DIR", "Templates/usb-travel/");
	define("TEMPLATE_IMAGES", "http://www.maksoft.net/Templates/usb-travel/images/");

	//show what options are available for config
	
	$tmpl_config_support = array(
		"change_theme"=>false,					// full theme support
		"change_layout"=>false,					// layout support
		"change_banner"=>false,					// banner support
		"change_css"=>false,					// css support
		"change_background"=>false,				// background support
		"change_fontsize"=>false,				// fontsize support
		"change_max_width"=>false,				// site width support
		"change_menu_width"=>false,				// site menu width support
		"change_main_width"=>false,				// page content width support
		"change_logo"=>true,					// logo support
		"column_left" => false,					// column left support
		"column_right" => false,				// column right support
		"drop_down_menu"=> false				// drop-down menu support
	);
	
	//define some default values
	$tmpl_config = array(
		"default_max_width"=> (defined('CMS_MAX_WIDTH'))? CMS_MAX_WIDTH : "960",
		"default_main_width"=>"650",
		"default_menu_width"=>"180",
		"default_banner_width" => (defined('CMS_MAX_WIDTH'))? CMS_MAX_WIDTH : "960",
		"default_banner" => TEMPLATE_IMAGES."banner.jpg",
		"default_logo" => TEMPLATE_IMAGES."logo.png",
		"default_logo_width" =>($o_site->get_sConfigValue('TMPL_LOGO_SIZE'))? $o_site->get_sConfigValue('TMPL_LOGO_SIZE') : "180",
		"column_left" => array(
								TEMPLATE_DIR . "menu_dynamic.php"
								),
		"footer" => array(
								TEMPLATE_DIR . "boxes/news.php"
							),		
		"date_format"=> "d-m-Y"
	);
	
	if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->get_sImageDIr().$o_site->_site['Logo'];
	
?>