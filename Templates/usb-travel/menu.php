<div id="menu">
	<div class="box-title"></div>
	<?
		if($o_page->_page['n'] != $o_site->StartPage)
			{
				echo "<div class=\"back\">";
				$o_page->print_pName("true", $o_site->StartPage);
				echo "</div>";
			}	
	?>
	<div class="box-content">
	<?php 
        $menu_links = $o_page->get_pSubpages($o_site->_site['StartPage'], "", "p.status >= 0");
        $parent_pages = $o_page->get_pParentPages();
		//print_menu($menu_links);
		for($i=0; $i<count($menu_links); $i++)
            echo "<a href=\"".$o_page->get_pLink($menu_links[$i]['n'])."\" title=\"".$menu_links[$i]['title']."\" class=\"".(($o_page->n == $menu_links[$i]['n'] || in_array($menu_links[$i]['n'], $parent_pages))? "selected":"")."\"><div class=\"bullet\"></div>".$menu_links[$i]['Name']."</a>";
        
    ?>
	</div>
    <div class="box-footer"></div>
    <?php include TEMPLATE_DIR . "boxes/social_links.php"; ?>
</div>