<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?=$Title?></title>
    <?php
	
	$o_page->get_sMoreText('� ��� ���');
	
		//include template configurations
		include("Templates/usb-travel/configure.php");		
    	//include meta tags
        require_once("lib/Database.class.php");
		include("Templates/meta_tags.php");
        $pdo = new Database();

		switch($language_id)
			{
				case 2: { $TMPL_LANGUAGE = "english"; break; }
				case 3: { $TMPL_LANGUAGE = "russian"; break; }
				case 4: { $TMPL_LANGUAGE = "french"; break; }
				case 1:
				default:{ $TMPL_LANGUAGE = "bulgarian"; break; }
			}
			
			
		define("DIR_TMPL_LANGUAGE", TEMPLATE_DIR."languages/".$TMPL_LANGUAGE."/");

		$o_site->print_sConfigurations();

	?>
    <link href="http://www.maksoft.net/<?=TEMPLATE_DIR?>layout.css" id="layout-style" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/<?=TEMPLATE_DIR?>responsive.css" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/css/admin_classes.css" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/hotels/base_style.css" rel="stylesheet" type="text/css" />
	<link href="<?=(($Site->css_url != "") ? $Site->css_url:$Template->css_url)?>" id="base-style" rel="stylesheet" type="text/css" />
	<?		
	if($o_page->_page['SiteID'] == $o_site->SiteID)
	{
		?>
	<script language="javascript" type="text/javascript" src="http://www.maksoft.net/<?=TEMPLATE_DIR?>effects.js"></script>
    	<?
	}
	?>
<!--    <script language="javascript" type="text/javascript" src="http://www.maksoft.net/<?=TEMPLATE_DIR?>script.js"></script>-->
		
		<!-- Stylesheets -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/slidebars/slidebars.min.css">
		<link rel="stylesheet" href="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/css/style.css">
		
		<!-- Shims -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<?php
	//top menu
	$top_menu = $o_page->get_pSubpages(0, "p.sort_n ASC LIMIT 5", "p.toplink = 1");
	
	//home resorts
	$home_resorts = $o_page->get_pSubpages($n_resorts, "p.sort_n ASC LIMIT 3");
	
	//vip links on home page
	$vip_links = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 4");
	
?>
<body id="top">
<?php include_once("Templates/header_inc.php"); ?>
		<!-- Navbar -->
		<nav class="navbar navbar-default navbar-fixed-top sb-slide hidden-lg hidden-md hidden-sm" role="navigation">
			<!-- Left Control -->
			<div class="sb-toggle-left navbar-left">
				<div class="navicon-line"></div>
				<div class="navicon-line"></div>
				<div class="navicon-line"></div>
			</div><!-- /.sb-control-left -->
			
			<!-- Right Control -->
			<div class="sb-toggle-right navbar-right">
				<i class="fa fa-search fa-2x icon-search"></i>
			</div><!-- /.sb-control-right -->
			
			<div class="container">
				<!-- Logo -->
				<div id="logo" class="navbar-left">
					<a href="http://usb-travel.com"><img src="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/images/logo.png" alt="USB-Travel" width="118" height="40"></a>
				</div><!-- /#logo -->
				
<!--			
				<ul class="nav navbar-nav navbar-right">
                <li></li>
				</ul>-->
			</div>
		</nav>

		<!-- Site -->
		<div id="sb-site">
			<div class="container mobile-site">
    <div id="header">
        <div class="header-content">
        	<?php include TEMPLATE_DIR."header.php"?>
        </div>
    </div>
	<div id="page_container" class="shadow">
        <div class="main-content">
        <?php
			if($o_page->_page['n'] == $o_site->_site['StartPage'])
				{
					if($o_site->SiteID == 917 || $o_site->SiteID == 926)
						include TEMPLATE_DIR."home-kids.php";
					else
						include TEMPLATE_DIR."home.php";
				}
			elseif($o_page->_page['SiteID'] != 1)
				include TEMPLATE_DIR."main.php";
			else
				include TEMPLATE_DIR."admin.php";
		?>
<?php

?>
<?php

?>
<?php

?>
        </div>
    </div>
    <div id="mobile-footer">
 <div class="col-lg-1 col-offset-6 centered">
<?php include TEMPLATE_DIR . "boxes/social_links.php"; ?>
</div>
</div>
    <div id="footer">
    	<div class="footer-content">
		<?php
			include TEMPLATE_DIR . "footer.php";
		?>
        </div>
        <div class="copyrights"><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a></div>
    </div>
</div>
            
        </div>   
		<!-- Slidebars -->
		<div class="sb-slidebar sb-left">
	<nav>
		<ul class="sb-menu">
			<li><img src="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/images/logo.png" alt="USB-Travel" width="118" height="40"></li>
            <div class="lang-icons"><?php $o_site->print_sVersions(); ?></div>
          <li class="sb-close"><a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>"><i class="fa fa-home fa-2x"></i></a></li>
            	<?php 
    $menu_links = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC", "p.toplink = 3");
	for($i=0; $i<count($menu_links); $i++)
		echo "<li class=\"sb-site\"><a href=\"".$o_page->get_pLink($menu_links[$i]['n'])."\" title=\"".$menu_links[$i]['title']."\" class=\"".(($o_page->n == $menu_links[$i]['n'])? "selected":"")."\"><div class=\"bullet\"></div>".$menu_links[$i]['Name']."</a></li>";
        
    ?>
		</ul>
	</nav>
</div><!-- /.sb-left --><div class="sb-slidebar sb-right sb-style-overlay">
	<aside id="about-me">
    <div class="search-icons">
        <form name="search" method="get" class="search-form">
        <input type="hidden" name="n" value="<?=$o_site->StartPage?>">
        <input type="hidden" name="SiteID" value="<?=$o_site->SiteID?>">
        <input type="text" value="" placeholder="<?=$o_site->get_sSearchText()?>" name="search">
        <input type="hidden" id="search_text" value="<?=$o_site->get_sSearchText()?>">
        <button type="submit" class="btn"><i class="fa fa-search fa-2x"></i></button>
        </form>
    </div>
	</aside>
</div><!-- /.sb-right -->
		<!-- jQuery -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

		<!-- Bootstrap -->
		<script src="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/bootstrap/js/bootstrap.min.js"></script>
		
		<!-- Slidebars -->
		<script src="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/slidebars/slidebars.min.js"></script>
		<script>
			(function($) {
				$(document).ready(function() {
					// Initiate Slidebars
					$.slidebars();
				});
			}) (jQuery);
		</script>

        <script>
		var headertext = [],
headers = document.querySelectorAll("#miyazaki th"),
tablerows = document.querySelectorAll("#miyazaki th"),
tablebody = document.querySelector("#miyazaki tbody");

for(var i = 0; i < headers.length; i++) {
  var current = headers[i];
  headertext.push(current.textContent.replace(/\r?\n|\r/,""));
} 
for (var i = 0, row; row = tablebody.rows[i]; i++) {
  for (var j = 0, col; col = row.cells[j]; j++) {
    col.setAttribute("data-th", headertext[j]);
  } 
}

		</script>
<?php include "Templates/footer_inc.php"; ?> 
</body>
</html>
