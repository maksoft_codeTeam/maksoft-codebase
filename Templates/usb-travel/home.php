<div id="column_left"><?php include TEMPLATE_DIR . "column_left.php"; ?></div>
<div id="pageContent" class="home">
    <div class="page-content">
    <h1 class="title"><?=$o_page->get_pTitle()?>
</h1>
    <?php
		//enable tag search / tag addresses
		$o_page->print_pContent();
        if(strlen($search_tag)>2)
			{
				$o_page->print_search($o_site->do_search(iconv("UTF-8","CP1251",$search_tag), 10)); 
				$keyword = $search_tag; 
				include("selected_sites.php"); 
			}
		//enable site search
		elseif(strlen($search)>2)
			$o_page->print_search($o_site->do_search($search, 10)); 
		else
			{
				include TEMPLATE_DIR . "boxes/top_offers.php";
				include TEMPLATE_DIR . "boxes/select_hotels.php";
			}
    ?>
    </div>
	<div id="navbar"><?=$o_page->print_pNavigation()?></div>
</div>
<div id="column_right"><?php include TEMPLATE_DIR . "column_right.php"; ?></div>