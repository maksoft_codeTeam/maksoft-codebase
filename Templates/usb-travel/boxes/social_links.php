<div id="box_social_links">
	<br clear="all">
    <?php if(defined('CMS_VKONTAKTE_PAGE_LINK') || $user->AccessLevel >0) { ?>
    <a href="<?=(defined("CMS_VKONTAKTE_PAGE_LINK")) ? CMS_VKONTAKTE_PAGE_LINK:"#"?>" class="vkontakte"></a>
    <?php }?>
    <?php if(defined('CMS_GPLUS_PAGE_LINK') || $user->AccessLevel >0) { ?>
    <a href="<?=(defined("CMS_GPLUS_PAGE_LINK")) ? CMS_GPLUS_PAGE_LINK:"#"?>" class="google"></a>
    <?php }?>
    <?php if(defined('CMS_FACEBOOK_PAGE_LINK') || $user->AccessLevel >0) { ?>
    <a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" class="facebook"></a>
    <?php }?>
    
    <br clear="all">
</div>
