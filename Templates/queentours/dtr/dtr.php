<?php 
/* 
    PCDTR - PHP+CSS Dynamic Text Replacement
    Revised by Joao Makray <joaomak.net> 
    updated by Frits Jan van Kempen <fritsjan.nl>
    Version 1.6
*/

session_start();
ob_start('DTR_init');

function DTR_init($buffer){
	$DTR = new DTR();
	return $DTR->getHeadings($buffer);
}


class DTR
{
  var $dtr_dir = "dtr"; //relative to root dir.
  var $heading_css = 'Templates/queentours/dtr/headings.css'; // relative to dtr dir
  var $cache_dir = 'cache'; // relative to image.php in dtr dir
  var $letterWrap = 0;
  var $dtr_tags = array('h10'); 
  
  function DTR(){
  	$this->dtr_style = '';
  	$this->id=0;
	$this->image_url = 'head_text.php';
	$this->heading_css = $this->dtr_dir . '/' . $this->heading_css;
  }
  function splitTag($text, $tag, $specific='', $innerTag=''){
  	$inline='span';
  	$text=trim($text);
  	if(!$text)return '';
  	$font_file = '';
	$font_size = 22;
	if(isset($_SESSION['pcdtr'][$tag.$specific.$innerTag]))$text_info = $_SESSION['pcdtr'][$tag.$specific.$innerTag];
	else if(isset($_SESSION['pcdtr'][$tag.$innerTag])) $text_info = $_SESSION['pcdtr'][$tag.$innerTag];
	else $text_info = $_SESSION['pcdtr'][$tag];
	if(isset($text_info['font-size'])) $font_size = $text_info['font-size'];
	if(isset($text_info['font-family'])) $font_file = $this->dtr_dir.'/'.$text_info['font-family'];
	if(!file_exists($font_file)) $font_file.='.ttf';
	if(!file_exists($font_file)) $font_file=str_replace('.ttf','.otf',$font_file);
	if(!file_exists($font_file)) return false;
	if(isset($text_info['text-transform']) && $text_info['text-transform']=='uppercase') $text=strtoupper($text); 
	if(isset($text_info['text-transform']) && $text_info['text-transform']=='lowercase') $text=strtolower($text); 
  	$wrap=wordwrap($text, $this->letterWrap, '<|>');
	$ww=explode('<|>',$wrap);
	$out='';
	foreach($ww as $n=>$v){
			$out.='<'.$inline.' id="'.$tag.$innerTag.$this->id.'">'.$v.'</'.$inline.'> ';
			$image=$this->image_url.'?text='.urlencode(html_entity_decode($v)).'&tag='.$tag.$specific;
			if($innerTag)$image.= '&it='.substr($innerTag,1);
			$gis=imagettfbbox($font_size,0,$font_file,html_entity_decode($v).' ');
			$w=abs($gis[4]-$gis[6]);
			$gis=imagettfbbox($font_size,0,$font_file,'ÁIJQÇjgx|/@#()');
			$h=abs($gis[3]-$gis[5]);
			$this->dtr_style.="\n". $specific.' #'.$tag.$innerTag.$this->id.'{background-image:url('.$image.');width:'.$w.'px;height:'.$h.'px}';
			$this->id++;
	}
	return $out;
  }
  function replaceHeading($tag, $buffer){	
	$ex=explode('<'.$tag, $buffer);
	$page=array_shift($ex);
	if (count($ex)>0) {
		foreach($ex as $v){
			$ex2=explode('</'.$tag.'>',$v);
			$pos = strpos($ex2[0], '>');
			$tagattrs = substr($ex2[0], 0, $pos);
			$txtNode = substr($ex2[0], $pos+1);
			$innerSplit=explode('<', $txtNode);
			$classpos = strpos($tagattrs, 'class=');
			$specific='';
			if ($classpos === false) $tagattrs.=' class="dtr"';
			else {	
				$classendpos = strpos($tagattrs, '"', $classpos+7);
				$className= trim(substr($tagattrs,$classpos+7, ($classendpos-($classpos+7))));
				if(isset($_SESSION['pcdtr'][$tag.'.'.$className]))$specific='.'.$className;
				$tagattrs = substr($tagattrs,0,$classpos+7) .'dtr '.substr($tagattrs,$classpos+7);
			}	
			$inner ='';
			foreach($innerSplit as $icount=>$innerstr){ 
				$tmp=explode('>',$innerstr);
				if(count($tmp)>1 && substr($innerstr,0,1)!='/'){
					$attrs=explode(' ',trim($tmp[0]));
					$innerTag=array_shift($attrs);
					$innerHTML=$this->splitTag($tmp[1], $tag, $specific, '-'.$innerTag);
					if($innerHTML===false)return $buffer;
					$inner .= '<'.$tmp[0].'>'.trim($innerHTML).'</'.$innerTag.'>';
				}else $inner .= $this->splitTag(array_pop($tmp), $tag, $specific);
			}
			$page .= '<'.$tag.$tagattrs.'>'.$inner.'</'.$tag.'>';
			$page .= $ex2[1];
		}
	}
	return $page;
  }
  function readCSS(){ 	
  		if (is_readable($this->heading_css)) {
			$style_array = file($this->heading_css);
			if (is_array($style_array)) {
				$curr =false;
				$_SESSION['pcdtr']=array();
				$_SESSION['pcdtr']['cache_dir']=$this->cache_dir;
				foreach ($style_array as $k => $prop) {
					$selector=trim(str_replace('{', '', $prop));
					if (in_array(substr($selector,0,2), $this->dtr_tags)) {
						$curr = str_replace(' ','-',$selector);
						// subtags cascade
						$subtags=explode('-',$curr);
						array_pop($subtags);
						if(count($subtags)>0)$subtag=array_pop($subtags);
						if(isset($subtag)){
							if(isset($_SESSION['pcdtr'][$subtag])) $_SESSION['pcdtr'][$curr]=$_SESSION['pcdtr'][$subtag];
						}
	
						if(!isset($_SESSION['pcdtr'][$curr])) $_SESSION['pcdtr'][$curr]=array();
					}else if($selector=='}'){
						$curr=false;
					} else {
						$dets = explode(':', $prop);
						if ($curr && isset($dets[0]) && isset($dets[1])) {
							$property=trim($dets[0]);
							$value=trim(str_replace(';', '', $dets[1]));
							
							if($property=='font-size')settype($value, "integer");
							if($property=='font-family'){
								$fontList=explode(',',$value);
								$value=array_shift($fontList);
							}
							$_SESSION['pcdtr'][$curr][$property] = trim($value);
						}
					}
				}
			}
		}else die($this->heading_css.' not readable');
	
  }
  function getHeadings($buffer) { 
  	// uncomment the line below to read the CSS file once per session
	//if (!is_array($_SESSION['pcdtr']) || $_GET['debug'])
		$this->readCSS();
		
	$html=$buffer;
	foreach($this->dtr_tags as $tag){
		if(array_key_exists($tag, $_SESSION['pcdtr']))$html = $this->replaceHeading($tag, $html);
	}	
	$_SESSION['pcdtr']['stylestr']=$this->dtr_style;
	return $html;
  }

}//end class

?>
