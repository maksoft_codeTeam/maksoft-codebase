<?php
	//read categories
	$select_cities = "<select name=\"city\" onChange=\"this.form.submit()\" style=\"width: 200px;\"><option value=\"\" selected>" . FORM_SELECT_CITY . "</option>";
	$cities_array = array();
	if($row->an > 0)
		$n = $row->an;
		
	$sub_query = mysqli_query("SELECT * FROM pages WHERE ParentPage = '".$n."' ORDER by sort_n ASC");
	
	while($subpage = mysqli_fetch_object($sub_query))
		$cities_array[] = $subpage->n;
		
	$offer_condition = "";	
	for($i=0; $i<count($cities_array); $i++)
		{
			$offer_condition.= "ParentPage = '".$cities_array[$i]."' OR ";
			$p_city = get_page($cities_array[$i]);
			
			if($city == $p_city->n) $selected = "selected";
			else $selected = "";
			
			$select_cities.= "<option value=\"".$p_city->n."\" $selected>".$p_city->Name."</option>";
		}
	$select_cities.="</select>";
	if(isset($city) && !empty($city))
		$offer_condition = "ParentPage = '".$city."' ";
	else $offer_condition.= "ParentPage = '".$cities_array[0]."' ";
	
	$sort_name = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;order=nameDESC\" title=\"".TEXT_SORT_BY_NAME.", ".TEXT_DESC."\">".TEXT_SORT_BY_NAME."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_down.gif\" border=\"0\" align=\"middle\">";
	$sort_date = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;order=dateDESC\" title=\"".TEXT_SORT_BY_DATE.", ".TEXT_DESC."\">".TEXT_SORT_BY_DATE."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_down.gif\" border=\"0\" align=\"middle\">";

	switch($order)
		{
			case "nameASC": {$sort_name = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;order=nameDESC\" title=\"".TEXT_SORT_BY_NAME.", ".TEXT_DESC."\">".TEXT_SORT_BY_NAME."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_down.gif\" border=\"0\" align=\"middle\">"; $order_by = "p.Name ASC"; break;}
			case "nameDESC": {$sort_name = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;order=nameASC\" title=\"".TEXT_SORT_BY_NAME.", ".TEXT_ASC."\">".TEXT_SORT_BY_NAME."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_up.gif\" border=\"0\" align=\"middle\">"; $order_by = "p.Name DESC"; break;}
			case "dateASC": {$sort_date = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;order=dateDESC\" title=\"".TEXT_SORT_BY_DATE.", ".TEXT_DESC."\">".TEXT_SORT_BY_DATE."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_down.gif\" border=\"0\" align=\"middle\">"; $order_by = "p.date_added ASC"; break;}
			case "dateDESC": {$sort_date = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID&amp;order=dateASC\" title=\"".TEXT_SORT_BY_DATE.", ".TEXT_ASC."\">".TEXT_SORT_BY_DATE."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_up.gif\" border=\"0\" align=\"middle\">"; $order_by = "p.date_added DESC"; break;}
			default:
				{
					$order_by = "p.Name ASC";
				}
		}	
?>
<form name="city" method="get" style="display: block; margin: 0px; clear: both;">
	<table width="740" cellpadding="0" cellspacing="0" border="0" style="background-image: url(Templates/queentours/images/head_text_bg2.jpg); background-repeat: no-repeat;">
	<tr><td width="530"><div style="display: block; height: 62px; line-height: 62px; margin: 0px;"><?=output_head_text(BOX_HEADING_ALL_OFFERS)?></div>
	<td width="210px;" align="center" valign="middle">
	<input type="hidden" name="n" value="<?=$n?>">
	<input type="hidden" name="SiteID" value="<?=$SiteID?>">
	<?php echo $select_cities; ?>
	</table>
</form>
<table style="background-color: #f0f0f0; display: block; clear: both;" width="740" cellpadding="0" cellspacing="0" border="0">
<tr><td align="center">
<?php
	
	$query_pages = mysqli_query("SELECT * FROM pages p left join images im on p.imageNo = im.imageID WHERE SiteID = '".$Site->SitesID."' AND ($offer_condition) ORDER by $order_by");
	$count_offers = mysqli_num_rows($query_pages);
	$pages = round($count_offers / 5);
	if($pages < ($count_offers / 5))
		$pages++;
			
	//selected page 
	if(!isset($p) || $p<=0 || $p > $count_offers)
		$p = 1;
	else $p = (int)$p;
	
	$page_range = 10;
		
	$start = ($p-1)*5;
	
	//read all offers
	$offers = array();
	$query = mysqli_query("SELECT * FROM pages p left join images im on p.imageNo = im.imageID WHERE SiteID = '".$Site->SitesID."' AND ($offer_condition) ORDER by $order_by LIMIT $start, 5");

	$i = 0;
	$o_counter = 1;
	if($pages > $page_range)
		{
		
			//pages start / end
			if(!isset($ps) || $ps<=0 || $ps> $pages)
					$ps = 1;
			else
					$ps = (int)$ps;
			
			$pe = $ps + $page_range;
			if($pe > $pages)
				$pe = $ps;
				
			//prev page range = page_start - page_range	
			$ppr = $ps-$page_range;
			if($ppr <= 0)
				$ppr = $ps;
			
			if($ps > $page_range)	
				$prev_button = "<a href=\"page.php?n=$n&SiteID=$SiteID&city=$city&p=1&ps=1\" class=\"first_page\"></a><a href=\"page.php?n=$n&SiteID=$SiteID&city=$city&p=".$ppr."&ps=".$ppr."\" class=\"prev_page\" title=\"".sprintf(TEXT_PREV_OFFERS, $page_range*5)."\"></a>";
			
			if($pages > ($ps+$page_range))	
				$next_button = "<a href=\"page.php?n=$n&SiteID=$SiteID&city=$city&p=".$pe."&ps=".$pe."\" class=\"next_page\" title=\"".sprintf(TEXT_NEXT_OFFERS, $page_range*5)."\"></a><a href=\"page.php?n=$n&SiteID=$SiteID&city=$city&p=".($pages-$pages%10)."&ps=".($pages-$pages%10)."\" class=\"last_page\"></a>";
		}
			
	if(mysqli_num_rows($query) > 0)
		{

				while($offer = mysqli_fetch_array($query))
					{
						$offer_content = "";
						if($i==1)
							{
								$table_id = "offer_preview2";
								$mask_class = "mask2";	
								$i = 0;
							}
						else
							{
								$table_id = "offer_preview";
								$mask_class = "mask1";	
								$i++;
							}
							
						$offer_content.="<div id=\"".$table_id."\" style=\"height: 185px; \"><table cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"700\" border=\"0\">";
						$offer_content.="<tr><td width=\"165px\" height=\"165px\" align=\"left\" valign=\'top\" rowspan=\"3\">".img_mask($offer['image_src'], 140, $mask_class, "page.php?n=".$offer['n']."&SiteID=".$SiteID, "title=\"".$offer['Name']."\"")."<td class=\"title\" height=\"50\" valign=\"middle\"><div style=\"float: right; clear: both;\">".stars($offer['Name'])."</div><a href=\"page.php?n=".$offer['n']."&SiteID=".$offer['SiteID']."\" title=\"".$offer['Name']."\"><span style=\"color: #ff0097; padding: 5px;\">".($o_counter + ($p-1)*5) .".</span>&nbsp;".$offer['Name'] . "</a>";
						$offer_content.="<tr><td class=\"text\" valign=\"top\" height=\"105\">".strip_tags(crop_text($offer['textStr']), "<b>, <B>, <strong>, <br>, <BR>") . "<br><div class=\"date\">".date(DATE_SHORT_FORMAT, strtotime($offer['date_added']))."</div>";
						$offer_content.="<tr><td align=\"right\" height=\"10\"><div style=\"width: 90px; height: 20px; margin-right: 30px; text-align: center;\"><a href=\"page.php?n=".$offer['n']."&SiteID=".$offer['SiteID']."\" class=\"next_link\" title=\"".$Site->MoreText." : ".$offer['Name']."\">".$Site->MoreText."</a></div>";
						$offer_content.="</table></div>";
						echo $offer_content;
						$o_counter ++;
					}
						echo "<br><div style=\"display: block; text-align: center;\">".sprintf(TEXT_COUNT_OFFERS, $count_offers)."</div>";
		}
	else echo "<br><br>Nothing found !";
	
	$pagination = "<div id=\"pagination\" style=\"margin-bottom: 10px;\">";
	//$pagination.= "<div style=\"float: left; margin-left: 25px; display: block; width: 120px;\">".sprintf(TEXT_COUNT_OFFERS, $count_offers)."</div>";
	//$pagination.= "<div style=\"float: right; margin-right: 25px; display: block; width: 120px;\"></div>";
	$pagination.= $prev_button;
	for($i=1; $i<=$pages; $i++)
	{
		if($i>= $ps && $i<($ps+$page_range))
		{
			if($p==$i)
				$pagination.="<a href=\"page.php?n=$n&SiteID=$SiteID&city=$city&p=$i&ps=$ps\" class=\"page_selected\">".$i."</a> ";
			else
				$pagination.="<a href=\"page.php?n=$n&SiteID=$SiteID&city=$city&p=$i&ps=$ps\" class=\"page\">".$i."</a> ";
		}
	}
	$pagination.= $next_button;
	$pagination.="</div>";
	
?>
<tr><td style="background-image: url(Templates/queentours/images/table_bottom.jpg); height: 50px;" align="center">
<?php printf(TEXT_SORT_BY, "$sort_name / $sort_date "); ?>
</table>
<center>
<?php
	if($pages>1) echo $pagination;
?>
</center><br clear="all">