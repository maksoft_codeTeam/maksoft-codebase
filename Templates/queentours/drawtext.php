<?php

/**
* 
* PHP Text Image Generator
* Version 0.1
* Tuesday, April 5, 2005
*
* Copyright (C) 2005 Matsuda Shota
* http://sgss.mdl-web.com/
* sgss@mac.com
*
* ------------------------------------------------------------------------
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
* ------------------------------------------------------------------------
*
*/

/*
2005-4-23
	　- Recording note started.
*/


define('WIDTH_TRIMMING', 0);
define('FULL_TRIMMING', 1);


if(isset($_GET['text']) && isset($_GET['font']) && isset($_GET['size'])) {
	$_GET['text'] = urldecode(html_entity_decode($_GET['text']));
	$_GET['font'] = urldecode(html_entity_decode($_GET['font']));
}
else {
	trigger_error("Insatisfied Parameters", E_USER_ERROR);
}


if(!isset($_GET['color']))
	$_GET['color'] = "000000";
if(!isset($_GET['bgcolor']))
	$_GET['bgcolor'] = "ffffff";
if(!isset($_GET['trim']))
	$_GET['trim'] = WIDTH_TRIMMING;
if(!isset($_GET['trans']))
	$_GET['trans'] = 1;
if(!isset($_GET['lh']))
	$_GET['lh'] = 1;
if(!isset($_GET['bl']))
	$_GET['bl'] = 0;
if(!isset($_GET['alpha']))
	$_GET['alpha'] = 100;
if(!isset($_GET['type']))
	$_GET['type'] = "gif";
if(!isset($_GET['pallete']))
	$_GET['pallete'] = 32;
if(!isset($_GET['quality']))
	$_GET['quality'] = 80;
	

$canvas = @imagecreatetruecolor(
	$_GET['size'] * (strlen($_GET['text']) + 1),
	($_GET['trim'] == WIDTH_TRIMMING)? $_GET['size'] * $_GET['lh'] : $_GET['size'] * 2
)
	or die("Cannot create image.");
$font = @imagepsloadfont($_GET['font'])
	or die("Cannot load the font.");
	
/*
create colors
*/
$fore = imagecolorallocate(
	$canvas, 
	hexdec(substr($_GET['color'], 0, 2)), 
	hexdec(substr($_GET['color'], 2, 2)), 
	hexdec(substr($_GET['color'], 4, 2))
);
$trans = imagecolorallocatealpha(
	$canvas, 
	hexdec(substr($_GET['color'], 0, 2)), 
	hexdec(substr($_GET['color'], 2, 2)), 
	hexdec(substr($_GET['color'], 4, 2)),
	127
);
$back = imagecolorallocate(
	$canvas, 
	hexdec(substr($_GET['bgcolor'], 0, 2)), 
	hexdec(substr($_GET['bgcolor'], 2, 2)), 
	hexdec(substr($_GET['bgcolor'], 4, 2))
);

imagefill($canvas, 0, 0, $back);

/*
draw text
*/
imagepstext(
	$canvas, //image
	$_GET['text'], //text
	$font, //font
	$_GET['size'], //size
	$fore, //foreground
	$trans, //background
	$_GET['size'], //x
	($_GET['lh'] - $_GET['bl']) * $_GET['size'], //y
	0, //space
	0, //tightness
	0.0, //angle
	16 //antialias steps
);


/*
adjust size
*/
$position = array(
	'ty' => 0, 
	'by' => imagesy($canvas), 
	'lx' => 0, 
	'rx' => imagesx($canvas)
);

// from right to left
$x = imagesx($canvas) - 1;
$cc = imagecolorat($canvas, $x, 0);
while($x > 0) {
	$y = 0;
	while($y < imagesy($canvas) - 1) {

		if($cc != imagecolorat($canvas, $x, $y)) {
			break;
		}
		$y ++;
	}
	if($y != imagesy($canvas) - 1) {
		break;
	}
	$x --;
}
$position['rx'] = $x + 1;

// from left to right
$x = 0;
$cc = imagecolorat($canvas, $x, 0);
while($x < imagesx($canvas) - 1) {
	$y = 0;
	while($y < imagesy($canvas) - 1) {
		if($cc != imagecolorat($canvas, $x, $y)) {
			break;
		}
		$y ++;
	}
	if($y != imagesy($canvas) - 1) {
		break;
	}
	$x ++;
}
$position['lx'] = $x;


if($_GET['trim'] == FULL_TRIMMING) {
	// from top to bottom
	$y = 0;
	$cc = imagecolorat($canvas, 0, $y);
	while($y < imagesy($canvas) - 1) {
		$x = 0;
		while($x < imagesx($canvas) - 1) {
			if($cc != imagecolorat($canvas, $x, $y)) {
				break;
			}
			$x ++;
		}
		if($x != imagesx($canvas) - 1) {
			break;
		}
		$y ++;
	}
	$position['ty'] = $y;
	
	// from bottom to top
	$y = imagesy($canvas) - 1;
	$cc = imagecolorat($canvas, 0, $y);
	while($y > 0) {
		$x = 0;
		while($x < imagesx($canvas) - 1) {
			if($cc != imagecolorat($canvas, $x, $y)) {
				break;
			}
			$x ++;
		}
		if($x != imagesx($canvas) - 1) {
			break;
		}
		$y --;
	}
	$position['by'] = $y + 1;
}


// create position-adjusted image
$canvas_adjusted = imagecreatetruecolor(
	$position['rx'] - $position['lx'],
	$position['by'] - $position['ty']
);

imagecopy(
	$canvas_adjusted, $canvas, 0, 0, 
	$position['lx'], 
	$position['ty'],
	$position['rx'] - $position['lx'],
	$position['by'] - $position['ty']
);



/*
alpha process
*/
$completedimg = @imagecreatetruecolor(imagesx($canvas_adjusted), imagesy($canvas_adjusted))
	or die("Cannot create image.");
$temporaryimg = @imagecreatetruecolor(imagesx($canvas_adjusted), imagesy($canvas_adjusted))
	or die("Cannot create image.");
$samplingimg = @imagecreatetruecolor(imagesx($canvas_adjusted) + 1, imagesy($canvas_adjusted) + 1)
	or die("Cannot create image.");


$color = imagecolorallocate(
	$completedimg, 
	hexdec(substr($_GET['bgcolor'], 0, 2)), 
	hexdec(substr($_GET['bgcolor'], 2, 2)), 
	hexdec(substr($_GET['bgcolor'], 4, 2))
);

imagefill($completedimg, 0, 0, $color);
imagefill($temporaryimg, 0, 0, $color);
imagefill($samplingimg, 0, 0, $color);

// merge
imagecopy($temporaryimg, $canvas_adjusted, 0, 0, 0, 0, imagesx($canvas_adjusted), imagesy($canvas_adjusted));
imagecopymerge($completedimg, $temporaryimg, 0, 0, 0, 0, imagesx($canvas_adjusted), imagesy($canvas_adjusted), $_GET['alpha']);
imagecopy($samplingimg, $completedimg, 1, 1, 0, 0, imagesx($canvas_adjusted), imagesy($canvas_adjusted));


/*
output
*/
switch($_GET['type']) {
	case "jpeg":
		// output
		header("Content-type: image/jpeg");
		imagejpeg($completedimg, null, $_GET['quality']);
		break;
	
	case "png":
		// color conversion
		imagetruecolortopalette($completedimg, false, $_GET['pallete']);
		
		if($_GET['trans'] == 1) {
			imagetruecolortopalette($samplingimg, false, $_GET['pallete']);
			$trans = imagecolorat($samplingimg, 0, 0);
			imagecolortransparent($completedimg, $trans);
		}
		// output
		header("Content-type: image/png");
		imagegif($completedimg);
		break;
		
	case "gif":
	default:
		// color conversion
		imagetruecolortopalette($completedimg, false, $_GET['pallete']);
		
		if($_GET['trans'] == 1) {
			imagetruecolortopalette($samplingimg, false, $_GET['pallete']);
			$trans = imagecolorat($samplingimg, 0, 0);
			imagecolortransparent($completedimg, $trans);
		}
		
		// output
		header("Content-type: image/gif");
		imagegif($completedimg);
}


// release memories
imagedestroy($completedimg);
imagedestroy($temporaryimg);
imagedestroy($samplingimg);
imagepsfreefont($font);
imagedestroy($canvas);
imagedestroy($canvas_adjusted);

?>
