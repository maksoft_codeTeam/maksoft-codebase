<div id="reviews">
<?php
	$rp = get_page(N_REVIEWS);
	echo "<a href=\"page.php?n=$rp->n&SiteID=$SiteID\" class=\"title\">".$rp->Name."</a>";
	echo "<img src=\"".DIR_TEMPLATE_IMAGES."shadow.png\">";
	
	$revs_query = mysqli_query("SELECT * FROM pages WHERE ParentPage = '".$rp->n."' ORDER by date_added DESC LIMIT 2");
	if(mysqli_num_rows($revs_query) > 0)
	{
	while($review = mysqli_fetch_object($revs_query))
		{
			echo "<div class=\"box\">";
			echo TEXT_FROM . "<span class=\"title\">".$review->Name."</span>";
			echo "<span class=\"date\">".date("d-m-Y", strtotime($review->date_added))."</span>";
			echo strip_tags(crop_text($review->textStr));
			echo "<a href=\"page.php?n=$review->n&SiteID=$SiteID\" class=\"next_link\">".$Site->MoreText."</a>";
			echo "</div>";
		}
			echo TEXT_WRITE_REVIEW;
	}
	else echo TEXT_NO_REVIEWS;
?>
</div>