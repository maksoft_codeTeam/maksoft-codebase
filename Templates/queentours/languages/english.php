<?php
	//configuration
	define("DATE_SHORT_FORMAT","Y.m.d");
	define("DATE_LONG_FORMAT","Y M d, h:m");
	
	//head title
	define("BOX_HEADING_HOT_OFFERS", "Hot offers");
	define("BOX_HEADING_TOP_OFFERS", "Top offers");
	define("BOX_HEADING_RECOMMENDED_OFFERS", "Recommended offers");
	define("BOX_HEADING_ALL_OFFERS", "All offers");
	define("BOX_HEADING_DESTINATIONS", "Destination");
	define("BOX_HEADING_MOST_VISITED", "Most visited");
	define("BOX_HEADING_OTHER_LINKS", "Other links");
	define("BOX_HEADING_CONTACTS", "Contacts");
	define('BOX_HEADING_REQUEST_RESERVATION', "Let me reserve ... ");
	define('BOX_HEADING_AIR_TICKETS', "Air tikets");
	define('BOX_HEADING_ADVANCED_SEARCH', "Advanced search");
	define("BOX_HEADING_SEND_TO_FRIEND", "Tell a friend about: %s");
	define("BOX_HEADING_RENTACAR", "Rent-a-car");
	
	//titles
	define("TITLE_SEARCH", "Search");
	
	//content
	define("N_CATALOG", 48358);
	define("N_SEARCH_ADVANCED", 49797);
	define("N_SEARCH_RESULTS", 49803);
	define("N_REVIEWS", 48433);
	define("N_CONTACTS", 48232);
	//defined in Site configurations
	if(!defined(N_MOST_VISITED) || N_MOST_VISITED == 0)	
		define("N_MOST_VISITED", 57832);
	define("N_RESERVATIONS", 55739);
	define("N_SEND_TO_FRIEND", "55741");

	//countries goup ID
	define("COUNTRIES_GROUP_ID", "5");
	
	//text
	define("TEXT_NAVIGATION", "You are here %s");
	define("TEXT_NO_REVIEWS", "<div class=\"box\">No reviews yet. Be the first one posted a review <a href=\"page.php?n=".N_CONTACTS."\">here</a></div>");
	define("TEXT_FROM", "from: ");
	define("TEXT_MORE", "More from; ");
	define("TEXT_WRITE_REVIEW", "<a href=\"page.php?n=".N_CONTACTS."&SiteID=".$SiteID."\" style=\"display: block; margin-top: 20px; margin-bottom: 10px;\">+ write review</a>");
	define("TEXT_COUNT_OFFERS", "total <b>%s</b> offers");
	define("TEXT_PREV_OFFERS", "previous %s offers");
	define("TEXT_NEXT_OFFERS", "next %s offers");
	define("TEXT_PAGE_VISITED", "page visited <b>%s</b> time(s)");
	define("TEXT_NIGHTS", " nights");
	define("TEXT_SORT_BY", "sort by: %s");
	define("TEXT_SORT_BY_NAME","name");
	define("TEXT_SORT_BY_DATE","date");
	define("TEXT_ASC","ascending");
	define("TEXT_DESC","descending");
				
	//messages
	define("MESSAGE_FORM_FIELDS_REQUIRED", "<div class=\"message_warning\">All fileds marked with * are reuired. Please fill them correctly.</div>");
	define("MESSAGE_SELECT_OFFER", "You must select offer ! Please take a look at the site and choose offer.<br>%s");
	define("MESSAGE_SEND_TO_FRIEND", "Hi, look what i've found: %s \nVisit the link and enjoy !");
	define("MESSAGE_SEND_TO_FRIEND_SUBJECT", "Recomended from: ");
	define("MESSAGE_SENT", "Message sent. Thank you !");
	
	//form
	define("FORM_SELECT_COUNTRY", " - select destination - ");
	define("FORM_SELECT_CITY", " - select city - ");
	define("FORM_SELECT_CATEGORY", " - select category - ");
	define("FORM_SELECT_ROOM_TYPE","<select name=\"room_type\"><option value=\"single\">single</option><option value=\"double\">double</option><option value=\"triple\">triple</option><option value=\"suit\">suit</option><option value=\"appartment\">appartment</option><option value=\"family\">family</option><option value=\"other\">other</option></select>");
	
	define("FORM_BUTTON_SEARCH", "Search");
	define("FORM_BUTTON_SEARCH_MORE", "Search more");
	define("FORM_BUTTON_SEND", "Send");
		
	define("FORM_LABEL_COUNTRY", "country:");
	define("FORM_LABEL_HOTEL", "hotel:");
	define("FORM_LABEL_GALLERY", "gallery:");
	define("FORM_LABEL_DATE", "date :");
	define("FORM_LABEL_DATE_ARRIVE", "date, <small>accommodation</small>:");
	define("FORM_LABEL_DATE_LEAVE", "date, <small>leaving</small>:");
	define("FORM_LABEL_ROOM_TYPE", "room, <small>type</small>:");
	define("FORM_LABEL_ROOM_COUNT", "room, <small>count</small>:");
	define("FORM_LABEL_PEOPLE", "number of <small>adults</small>:");
	define("FORM_LABEL_PEOPLE", "number of <small>kids</small>:");
	define("FORM_LABEL_NAME", "name");
	define("FORM_LABEL_SENDER_NAME", "your name: *");
	define("FORM_LABEL_FAMILY", "family");
	define("FORM_LABEL_NAME_FAMILY", "".FORM_LABEL_NAME.", ".FORM_LABEL_FAMILY.":");
	define("FORM_LABEL_PHONE", "phone:");
	define("FORM_LABEL_EMAIL", "e-mail:");
	define("FORM_LABEL_SENDER_EMAIL", 'your e-mail:');
	define("FORM_LABEL_RECEIVER_EMAIL", "receiver e-mail : *");
	define("FORM_LABEL_ALTERNATIVE", "I wish an alternative:");
	define("FORM_LABEL_YES", "YES");
	define("FORM_LABEL_NO", "No");
	define("FORM_LABEL_COMMENTS", "Comments:");
	define("FORM_LABEL_RESORT", "resort (<small>city</small>):");
	define("FORM_LABEL_HOTEL_CATEGORY", "category:");
	define("FORM_LABEL_SLECT_ARRIVE_DATE", "Select arrival date !");
	define("FORM_LABEL_SLECT_LEAVE_DATE", "Select date of leaving !");
	define("FORM_LABEL_COUNT_NIGHTS", "number of <small>nights</small>:");
	define("FORM_LABEL_YEARS", "years:");
	define("FORM_LABEL_PICKUP_LOCATION", "pickup location:");
	define("FORM_LABEL_PICKUP_DATE", "pickup date:");
	define("FORM_LABEL_SLECT_PICKUP_DATE", "Select pickup date");	
	define("FORM_LABEL_RETURN_LOCATION", "return location:");
	define("FORM_LABEL_RETURN_DATE", "return date:");
	define("FORM_LABEL_SLECT_RETURN_DATE", "Select return date");
	define("FORM_LABEL_BIRTHDAY", "Kid birthday");
				
	define("FORM_MESSAGE_VALID_EMAIL", "enter a valid e-mail address !");
	define("FORM_MESSAGE_MUST_CONTAIN_NUMBERS", "must contain only numbers !");
	define("FORM_MESSAGE_MUST_CONTAIN_NUMBER_BETWEEN", "must me number between");
	define("FORM_MESSAGE_REQUESTED_FIELD", "is reuired field !");
	define("FORM_MESSAGE_ALL_REQUESTED_FIELD", "All fields marked with * are required :");		
?>