<?php
	//configuration
	define("DATE_SHORT_FORMAT","d.m.Y");
	define("DATE_LONG_FORMAT","d M Y, h:m");
	
	//head title
	define("BOX_HEADING_HOT_OFFERS", "������ ������");
	define("BOX_HEADING_TOP_OFFERS", "��� ������");
	define("BOX_HEADING_RECOMMENDED_OFFERS", "����������� ������");
	define("BOX_HEADING_ALL_OFFERS", "������ ������");
	define("BOX_HEADING_DESTINATIONS", "����������");
	define("BOX_HEADING_MOST_VISITED", "���-����������");
	define("BOX_HEADING_OTHER_LINKS", "���...");
	define("BOX_HEADING_CONTACTS", "��������");
	define('BOX_HEADING_REQUEST_RESERVATION', "����� �� ���������� ... ");
	define('BOX_HEADING_AIR_TICKETS', "��������� ������");
	define('BOX_HEADING_ADVANCED_SEARCH', "��������� �������");
	define("BOX_HEADING_SEND_TO_FRIEND", "���� �� ������� ��: %s");
	define("BOX_HEADING_RENTACAR", "Rent-a-car");
	
	//titles
	define("TITLE_SEARCH", "�������");

	//content
	define("N_CATALOG", 49830);
	define("N_SEARCH_ADVANCED", 49849);
	define("N_SEARCH_RESULTS", 49852);
	define("N_REVIEWS", 49905);
	define("N_CONTACTS", 46912);
	//defined in Site configurations
	if(!defined(N_MOST_VISITED) || N_MOST_VISITED == 0)
		define("N_MOST_VISITED", 49498);
	define("N_RESERVATIONS", 52505);
	define("N_SEND_TO_FRIEND", "55736");
	
	//countries goup ID
	define("COUNTRIES_GROUP_ID", "3");
	
	//text
	define("TEXT_NAVIGATION", "��� ��� ��� %s");
	define("TEXT_NO_REVIEWS", "<div class=\"box\">���� ������. ���� �� �������� ������ ����� <a href=\"page.php?n=".N_CONTACTS."\">���</a></div>");
	define("TEXT_FROM", "��: ");
	define("TEXT_MORE", "��� ��: ");
	define("TEXT_WRITE_REVIEW", "<a href=\"page.php?n=".N_CONTACTS."&SiteID=".$SiteID."\" style=\"display: block; margin-top: 20px; margin-bottom: 10px;\">+ �������� �����</a>");
	define("TEXT_COUNT_OFFERS", "���� <b>%s</b> ������");
	define("TEXT_PREV_OFFERS", "���������� %s ������");
	define("TEXT_NEXT_OFFERS", "���������� %s ������");
	define("TEXT_PAGE_VISITED", "���� �������� � �������� <b>%s</b> ���(�)");
	define("TEXT_NIGHTS", " �������");
	define("TEXT_SORT_BY", "�������� ��: %s");
	define("TEXT_SORT_BY_NAME","���");
	define("TEXT_SORT_BY_DATE","����");
	define("TEXT_ASC","���������");
	define("TEXT_DESC","���������");
	
	//messages
	define("MESSAGE_FORM_FIELDS_REQUIRED", "<div class=\"message_warning\">�������� ���������� � * �� ������������. ���� �� �� ��������� ��������.<br>��� ����� ������� ������ �� �� ������ �� �����: %s</div>");
	define("MESSAGE_SELECT_OFFER", "�� ��� ������� ������ ! ���� ���������� ������ ����� � ������� ������.<br>%s");
	define("MESSAGE_SEND_TO_FRIEND", "�������, ��� ����� ������: %s \n������ �� �o ����������� !");
	define("MESSAGE_SEND_TO_FRIEND_SUBJECT", "��������� ��: ");
	define("MESSAGE_SENT", "����������� � ��������� �������. ���������� �� !");
	
	//form
	define("FORM_SELECT_COUNTRY", " - �������� ����������� - ");
	define("FORM_SELECT_CITY", " - �������� ����������� - ");
	define("FORM_SELECT_CATEGORY", " - �������� ��������� - ");
	define("FORM_SELECT_ROOM_TYPE","<select name=\"room_type\"><option value=\"single\">��������</option><option value=\"double\">������</option><option value=\"triple\">������</option><option value=\"suit\">�����</option><option value=\"appartment\">����������</option><option value=\"family\">�������</option><option value=\"other\">�����</option></select>");	
	
	define("FORM_BUTTON_SEARCH", "�������");
	define("FORM_BUTTON_SEARCH_MORE", "���� �������");
	define("FORM_BUTTON_SEND", "�������");
	
	define("FORM_LABEL_COUNTRY", "�������:");
	define("FORM_LABEL_HOTEL", "�����:");
	define("FORM_LABEL_GALLERY", "�������:");
	define("FORM_LABEL_DATE", "����:");
	define("FORM_LABEL_DATE_ARRIVE", "����, <small>�����������</small>:");
	define("FORM_LABEL_DATE_LEAVE", "����, <small>���������</small>:");
	define("FORM_LABEL_ROOM_TYPE", "����, <small>���</small>:");
	define("FORM_LABEL_ROOM_COUNT", "����, <small>����</small>:");
	define("FORM_LABEL_PEOPLE", "���� <small>���������</small>:");
	define("FORM_LABEL_KIDS", "���� <small>����</small>:");
	define("FORM_LABEL_NAME", "���");
	define("FORM_LABEL_SENDER_NAME", "������ ���: *");
	define("FORM_LABEL_FAMILY", "�������");
	define("FORM_LABEL_NAME_FAMILY", "".FORM_LABEL_NAME.", ".FORM_LABEL_FAMILY.":");
	define("FORM_LABEL_PHONE", "�������:");
	define("FORM_LABEL_EMAIL", "e-mail:");
	define("FORM_LABEL_SENDER_EMAIL", '����� e-mail:');
	define("FORM_LABEL_RECEIVER_EMAIL", "e-mail �� ����������: *");
	define("FORM_LABEL_ALTERNATIVE", "����� �����������:");
	define("FORM_LABEL_YES", "��");
	define("FORM_LABEL_NO", "��");
	define("FORM_LABEL_COMMENTS", "���������:");
	define("FORM_LABEL_RESORT", "������ (<small>����</small>):");
	define("FORM_LABEL_HOTEL_CATEGORY", "���������:");
	define("FORM_LABEL_SLECT_ARRIVE_DATE", "�������� ���� �� ����������� !");
	define("FORM_LABEL_SLECT_LEAVE_DATE", "�������� ���� �� ��������� !");
	define("FORM_LABEL_COUNT_NIGHTS", "���� <small>�������</small>:");
	define("FORM_LABEL_YEARS", "������:");
	define("FORM_LABEL_PICKUP_LOCATION", "����� �� �������:");
	define("FORM_LABEL_PICKUP_DATE", "���� �� �������:");
	define("FORM_LABEL_SLECT_PICKUP_DATE", "�������� ���� �� �������");
	define("FORM_LABEL_RETURN_LOCATION", "����� �� �������:");
	define("FORM_LABEL_RETURN_DATE", "���� �� �������:");
	define("FORM_LABEL_SLECT_RETURN_DATE", "�������� ���� �� �������");
	define("FORM_LABEL_BIRTHDAY", "�������� ����");
	
	define("FORM_MESSAGE_VALID_EMAIL", "�������� ������� e-mail ����� !");
	define("FORM_MESSAGE_MUST_CONTAIN_NUMBERS", "������ �� � �������� �� ����� !");
	define("FORM_MESSAGE_MUST_CONTAIN_NUMBER_BETWEEN", "������ �� ����� �����");
	define("FORM_MESSAGE_REQUESTED_FIELD", "� ������������ ���� !");
	define("FORM_MESSAGE_ALL_REQUESTED_FIELD", "������������ ��� * ������ �� ������������! ���� ��������� ��:");
?>