<?php
function img_mask($src, $img_width=140, $class="mask2", $url=NULL, $params="")
{
	if(file_exists($src) && is_file($src))
		{
			//get the image size
			$img_size = getimagesize($src);
			$preview_width =  $img_size[0];
			$preview_height =  $img_size[1];
			
			if($preview_width > $preview_height)
				$width = $preview_width * $img_width/$preview_height;
			else
				$width = $preview_height * $img_width/$preview_width;
			//echo "<img src=\"img_preview.php?image_file=http://www.maksoft.net/$row->image_src&img_width=$width\">";
				//echo $row->image_src;
				if($url)
					return "<div id=\"img_preview\"><a class=\"".$class."\" href=\"".$url."\" $params></a><div class='content' style=\"background-image: url('img_preview.php?image_file=http://www.maksoft.net/$src&img_width=$width');\"></div></div>";
				else
					return "<div id=\"img_preview\"><a class=\"".$class."\" href=\"#\" $params></a><div class='content' style=\"background-image: url('img_preview.php?image_file=http://www.maksoft.net/$src&img_width=$width');\"></div></div>";
		}
		else
			{
				if($url)
					return "<div id=\"img_preview\"><a class=\"".$class."\" href=\"".$url."\" $params></a><div class='content' style=\"background-image: url('img_preview.php?image_file=http://www.maksoft.net/Templates/queentours/images/no_image.jpg&img_width=$img_width');\"></div></div>";
				else
					return "<div id=\"img_preview\"><a class=\"".$class."\" href=\"#\" $params></a><div class='content' style=\"background-image: url('img_preview.php?image_file=http://www.maksoft.net/Templates/queentours/images/no_image.jpg&img_width=$img_width');\"></div></div>";
				
			}
}

function stars($title)
{
		//stars indicator
	 $paterns = array("2*", "3*", "4*", "5*", "6*", "2 *", "3 *", "4 *", "5 *", "6 *", "2+*", "3+*", "4+*", "2+ *", "3+ *", "4+ *", "2*+", "3*+", "4*+", "2* +", "3* +", "4* +");
	 $stars = "";
	 $stars_img = "";
	 for($i=0; $i<count($paterns); $i++)
	 	{
			$tmp_string = strstr($title, $paterns[$i]);
			if(!empty($tmp_string))
				{
					$stars = str_replace("*", "", $tmp_string);
					$stars = str_replace(" ", "", $stars);
				}
		}
	
	if($stars != "")
		$stars_img = "<img src=\"Templates/queentours/images/".$stars."stars.png\" alt=\"$stars stars hotel\">";
	return $stars_img;
}

//check if page is offer: 1. page has subpages 2. all subpages has images 3. show only subpages image
function page_is_offer($n)
{
	$search_query = mysqli_query("SELECT * FROM pages p, images im WHERE p.ParentPage = '".$n."' AND p.imageNo = im.imageID");
	$p = get_page($n);
	if($p->show_link == 9 && mysqli_num_rows($search_query) > 0)
	return true;
}
?>