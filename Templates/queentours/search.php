<?php
$search_condition = " 1 ";
$hotel_category_condition = "";
$keys = array_keys($hotel_category);

for($i=0; $i<count($keys); $i++)
	{
			
			if($i>0) $hotel_category_condition.= " OR ";
			$hotel_category_condition.= "p.Name LIKE ('%".$hotel_category[$keys[$i]]."%')";
			$hotel_category_url.= "&amp;hotel_category[$i]=".$hotel_category[$keys[$i]];
	}



	$search_url = "&amp;do_search=$do_search&amp;country=$country&amp;resort=$resort&amp;hotel_name=$hotel_name&amp;$hotel_category_url";
	
	$sort_name = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID".$search_url."&amp;order=nameDESC\" title=\"".TEXT_SORT_BY_NAME.", ".TEXT_DESC."\">".TEXT_SORT_BY_NAME."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_down.gif\" border=\"0\" align=\"middle\">";
	$sort_date = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID".$search_url."&amp;order=dateDESC\" title=\"".TEXT_SORT_BY_DATE.", ".TEXT_DESC."\">".TEXT_SORT_BY_DATE."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_down.gif\" border=\"0\" align=\"middle\">";
	
	switch($order)
		{
			case "nameASC": {$sort_name = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID".$search_url."&amp;order=nameDESC\" title=\"".TEXT_SORT_BY_NAME.", ".TEXT_DESC."\">".TEXT_SORT_BY_NAME."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_down.gif\" border=\"0\" align=\"middle\">"; $order_by = "p.Name ASC"; break;}
			case "nameDESC": {$sort_name = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID".$search_url."&amp;order=nameASC\" title=\"".TEXT_SORT_BY_NAME.", ".TEXT_ASC."\">".TEXT_SORT_BY_NAME."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_up.gif\" border=\"0\" align=\"middle\">"; $order_by = "p.Name DESC"; break;}
			case "dateASC": {$sort_date = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID".$search_url."&amp;order=dateDESC\" title=\"".TEXT_SORT_BY_DATE.", ".TEXT_DESC."\">".TEXT_SORT_BY_DATE."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_down.gif\" border=\"0\" align=\"middle\">"; $order_by = "p.date_added ASC"; break;}
			case "dateDESC": {$sort_date = "<a href=\"page.php?n=$n&amp;SiteID=$SiteID".$search_url."&amp;order=dateASC\" title=\"".TEXT_SORT_BY_DATE.", ".TEXT_ASC."\">".TEXT_SORT_BY_DATE."</a>&nbsp;<img src=\"".DIR_TEMPLATE_IMAGES."arrow_up.gif\" border=\"0\" align=\"middle\">"; $order_by = "p.date_added DESC"; break;}
			default:
				{
					$order_by = "p.Name ASC";
				}
		}
			
if(empty($resort) && $country == "0" && count($hotel_category) == 0 && empty($search) && empty($hotel_name))
	{
		mk_output_message("error", "Nothing selected !");
	}
else
	{
		if(!empty($resort))
			{
				
				$search_condition.= " AND p.Name LIKE '%".$resort."%'";
				
				$query_resort = mysqli_query("SELECT * FROM pages p WHERE  $search_condition AND p.SiteID = '$SiteID' AND p.SecLevel <= '".$user->ReadLevel."' ORDER by $order_by"); 
				while($resort = mysqli_fetch_object($query_resort))
					{
						
						$search_condition = "p.ParentPage = '".$resort->n."'";
						
						if(!empty($hotel_name))
							$search_condition.= " AND p.Name LIKE '%$hotel_name%'";
						
						if(count($hotel_category) > 0)
							$search_condition.= " AND ($hotel_category_condition)";
				
						
						$query_text = "SELECT * FROM pages p left join images im on p.imageNo = im.imageID WHERE ".$search_condition." AND SiteID = '$SiteID' AND p.SecLevel <= '".$user->ReadLevel."' ORDER by $order_by"; 
						
						$search_result = mysqli_query($query_text);  
						$sr_text = "<h1 class=\"title\"><a href=\"page.php?n=$resort->n\">".$resort->Name."</a></h1>";
						$res_counter = 1;
						while($result = mysqli_fetch_object($search_result))
							{
								$sr_text.="<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" height=\"150\"  width=\"350\" style=\"float: left;\"><tr>";
								$sr_text.= "<td rowspan=2 align=\"center\" valign=\"top\" width=\"150\">".img_mask($result->image_src, 140, "mask2", "page.php?n=".$result->n."&SiteID=".$SiteID, "title=\"".$result->Name."\"");
								$sr_text.= "<td style=\"border-bottom: 1px solid #cccccc\" height=\"30px\" class=\"title\"> ".($res_counter++). ". <a href=\"page.php?n=".$result->n."&SiteID=".$SiteID."\">".$result->Name."</a>";
								$sr_text.= "<tr><td style=\"border-top: 1px solid #FFFFFF\"  width=\"350\" valign=\"top\">".crop_text($result->textStr);
								$sr_text.="</table>";
							}
						$sr_text.= "<br style=\"clear: both\">";
					}
		
				
			}	
		
		else
		{
				if(!empty($hotel_name))
					$search_condition.= " AND p.Name LIKE '%$hotel_name%' ";
		
				if(!empty($country))
					{
						//search for mirror page, if found $country get the original value 
						$country_page = get_page($country);
						if($country_page->an > 0) $country = $country_page->an;
							
						$search_condition.= " AND ((p.n='".$country."') OR (p.ParentPage='".$country."') OR (p.textStr LIKE '%$country_page->Name%') OR (p.Name LIKE '%$country_page->Name%')) ";
					}		
				
				if(count($hotel_category) > 0)
					$search_condition.= " AND ($hotel_category_condition) ";
		
		
				if(!empty($search))
					$search_condition.= " AND ((Name LIKE '%$search%') OR  (textStr LIKE '%$search%'))";
		
				$query_text = "SELECT * FROM pages p left join images im on p.imageNo = im.imageID WHERE ".$search_condition." AND SiteID = '$SiteID' AND p.SecLevel <= '".$user->ReadLevel."' ORDER by $order_by"; 
				
				$search_result = mysqli_query("$query_text");  
				
				 if (mysqli_num_rows($search_result) > 0) 
					{
						$sr_text = "<h1 class=\"title\">��������� �� ���������: </h1>";
						$res_counter = 1;
						  //$sr_text.= "<ul style=\"1\">"; 
						  while ($result = mysqli_fetch_object($search_result) )
							{
								   //$sr_text.= "<li><a href=/page.php?n=$matched_page->n&SiteID=$SiteID>$matched_page->Name</a>"; 
								$sr_text.="<table cellpadding=\"10\" cellspacing=\"0\" border=\"0\" height=\"150\"  width=\"350\" style=\"float: left;\"><tr>";
								$sr_text.= "<td rowspan=2 align=\"center\" valign=\"top\" width=\"150\">".img_mask($result->image_src, 140, "mask2", "page.php?n=".$result->n."&SiteID=".$SiteID, "title=\"".$result->Name."\"");
								$sr_text.= "<td style=\"border-bottom: 1px solid #cccccc\" height=\"30px\" class=\"title\"> ".($res_counter++). ". <a href=\"page.php?n=".$result->n."&SiteID=".$SiteID."\">".$result->Name."</a>";
								$sr_text.= "<tr><td style=\"border-top: 1px solid #FFFFFF\"  width=\"350\" valign=\"top\">".crop_text($result->textStr);
								$sr_text.="</table>";					 
							 }
					
						  //$sr_text.= "</ul>"; 
					 } 
				 else 
					 mk_output_message("warning", "<br>��������� ���� <b>$search</b> �� � ������� � �����! <br>");  
		 }
		
		print($sr_text); 
	}
echo "<hr style=\"clear: both;\">";
printf("<div align=\"center\">".TEXT_SORT_BY."</div><hr>", "$sort_name $sort_date ");
echo "<div style=\"text-align: center; width: 100%; background-color: #FFF; clear: both;\" class=\"contact_table\"><a href=\"page.php?n=".N_SEARCH_ADVANCED."&SIteID=$SiteID\" class=\"button_submit\" style=\"margin: 10px;\">".FORM_BUTTON_SEARCH_MORE."</a></div>";

?>