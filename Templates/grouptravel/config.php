<?php
date_default_timezone_set( 'Europe/Sofia' );
require_once __DIR__ . '/../../modules/bootstrap.php';
require_once __DIR__ . '/../../global/recaptcha/init.php';
require_once __DIR__.'/../../global/bus_tickets/repository.php';
require_once __DIR__.'/helpers.php';

$custom_js = array();


define("MAX_RESERVED_TIME", 600);
define( "TEMPLATE_NAME", "grouptravel" );
define( "TEMPLATE_DIR", "Templates/" . TEMPLATE_NAME . "/" );
define( "TEMPLATE_IMAGES", "http://www.maksoft.net/" . TEMPLATE_DIR . "images/" );
define( "ASSETS_DIR", "/" . TEMPLATE_DIR . "" );
define( "SCHEDULE_PAGE" , 19373572);

$search_tag = '';
$search = '';

if(isset($_GET['search_tag'])){
    $search_tag = $_GET['search_tag'];
}

if(isset($_GET['search'])){
    $search = $_GET['search'];
}

if(!defined('COMPANY_ID')) {
    echo '���� ����� �� � ���������, ���� �������� �� � ������� �� ���������';
}

$tmpl_config = array(
    "default_banner" => ASSETS_DIR."banner.jpg",
    "default_logo" => ASSETS_DIR."assets/images/default-logo.png",
);

$mail = new \PHPMailer;
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'server.maksoft.net';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->CharSet = "UTF-8";
$mail->Username = 'cc@maksoft.bg';                 // SMTP username
$mail->Password = 'maksoft@cc';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to
$mail->isHTML(true);                                  // Set email format to HTML

$text = array(
    'title'       => '���������� �� �� ����������� �������',
    'website'     => array(
        'link'   => $o_site->_site['primary_url'],
        'name'   => $o_site->_site['Name'],
        'logo'   => 'http://www.grouptravel.bg/Templates/grouptravel/assets/images/logo-grouptravel.png',
        'banner' => 'http://www.grouptravel.bg/Templates/grouptravel/assets/images/default-slider.jpg'
    ),
    'body'        => array(
        'title'   => '���������� �� ����������:',
        'content' => '
            <ul>
                <li>����������� ������������ ���������� � ����� �� GroupTravel.bg, �� ������� �� �������� ��� ��� ������������ � ���� /����/ �� ���������� ������� 45 ������ ����� ��������� �� ��������.</li>
                <li>��������� ������������ ���������� �� ������ �� �������� �� �������, �� ��-���� �� ��� ���� 
                  ����� ��������� �� ����������� ������. 
                  ����������� ������ � ����� ���������� ����������, ������ �� �������� �� ���.: +359 2 418 00 77 � ���� ����� ����������.
                  ������������ �� �� ������� ���� ��� �������� �� �����.</li>
              </ul>'
    ),
    'ticket'     => array(
        'link' => 'http://www.grouptravel.bg/Templates/grouptravel/templates/busticket/printable_busticket.php?s=2&r=7&p=2',
        'name' => '��������� �����',
    ),
    'passengers' => array(),
    'buyer'      => array(),
    'about'      => array(
        'link' => 'http://www.grouptravel.bg/page.php?n=19367143&SiteID=1042',
        'name' => '�� ���'
    ),
    'route' => new StdClass(),
    'contact'    => array(
        'link' => 'http://www.grouptravel.bg/page.php?n=19367290&SiteID=1042',
        'name' => '��������'
    ),
    'facebook' => 'https://www.facebook.com/GTravel.Info/'
);
