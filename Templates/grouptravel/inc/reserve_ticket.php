<?php
use Jokuf\Form\Validators\NotEmpty;
require_once __DIR__.'/../../../global/bus_tickets/forms/AddPassenger.php';
require_once __DIR__.'/../../../modules/tasks.php';

$passengers = $request->request->get('passengers', false);
$post_data = $request->request->all();

$buyer = $request->request->get('payer', false);
$payer->is_valid();
$u = session_id();


if(!$passengers):
    $log->error("$u �� � ������ �����");
    $sticky_message('����, ����� �������� �����', 'error');
endif;

if(!$buyer):
    $log->error("$u �� � ��������� ������� �� �������");
    $sticky_message('����, ��������� ������� �� ������', 'error');
endif;

$add_js = function($code) use (&$custom_js) {
    $custom_js[] = $code;
};

$build_selector = function($parent, $child, $num) {
    return "{$parent}{$num}-{$child}";
};

$write_field_error = function($selector, $msg) use ($add_js) {
    $add_js('var node= document.createElement("SPAN");');
    $add_js('node.appendChild(document.createTextNode("'.$msg.'"));');
    $add_js("document.getElementById('$selector').parentElement.appendChild(node);");
    $add_js($code); 
};
$validator = NotEmpty::init(true);

$fields_valid = array();
$is_valid = function($name, $value, $i) use ($validator, $write_field_error, $build_selector, &$fields_valid) {
    $field_errors = array(
        'phone' => '�� ��� ������ ��������� �����',
        'email' => '������ ��� ��������� ���������� ����',
        'firstName' => '���� �������� ���',
        'lastName' => '���� �������� �������',
        'tariff' => '���� �������� ��� �����',
    );
    $warning = '* ';
    if($validator($value)) {
        $fields_valid[] = true;
        return true;
    }

    $write_field_error($build_selector('passengers', $name, $i), array_key_exists($name, $field_errors) ? $warning.$field_errors[$name] : '');
    $fields_valid[] = false;
};

$i = 1;
foreach($passengers as $passenger) {
    if(!is_array($passenger) ) {
        continue;
    }

    foreach($passenger as $name=>$value) {
        $is_valid($name, $value, $i);
    }

    $i++;
}
if(count(array_unique($fields_valid)) !== 1){
    throw new Exception('�� ��� ��������� ������ ������');
}

$payer = add_buyer($em, $buyer['email'], $buyer['firstName'], $buyer['lastName'], '', $buyer['phone']); 
if(!$payer) {
    throw new Exception('������� �� ������� � ���������');
}

$tickets = 0;
$total = 0;
$_passengers = array();
$recipients = array(
    array(
        'email' => $buyer['email'],
        'name'  => $buyer['firstName'] . ' ' . $buyer['lastName']
    ),
);

foreach($passengers as $passenger) {
    try {
        $p = add_passenger($em, $passenger['email'], $passenger['firstName'], $passenger['lastName'], '', $passenger['phone']); 
    } catch(\Exception $e) {
        echo $e->getMessage();
        $p = $payer;
    }
    $r_seat = $em->getRepository('Bus\models\ReservedSeats')->findOneById($passenger['seat_id']);
    if(!$r_seat) {
        throw new Exception('�������, ���� ���������� ����������');
    }

    if($r_seat->getSold()) {
        throw new Exception('�������� �� �� �������� ����� �� ���� ���������/����������� �������');
    }

    $seat = $r_seat->getSeat();
    $route = get_route_by_id($passenger['route']);
    $pr  = new EmptyObject();
    foreach($route->getPrices() as $price) {
        if($price->getType()->getId() == $passenger['tariff']) {
            $pr = $price;
        }
    }

    if($pr instanceof EmptyObject) {
        throw new Exception('�������� �� �� �������� ����� �� ���� ���������/����������� �������');
    }
    

    $passenger['seat_number'] = $seat->getNumber();
    $vehicle = $seat->getVehicle();
    $r_seat->setSold(true);
    $r_seat->setAutoRemove(false);
    $em->persist($r_seat);
    $em->flush();
    try {
        add_ticket($em, $payer, $vehicle, $seat, $pr->getPrice(), $schedule, $route, $p); 
        $recpients[] = array('name' => $passenger['firstName'] . ' ' . $passenger['lastName'], 'email' => $passenger['email']);
        $total += $pr->getPrice();
        $_passengers[] = $passenger;
    } catch (\Exception $e) {
        throw new Exception('������� ������� ������, ���� �������� ������.');
    }
    $tickets++;
}
$ticket_str = $ticket > 1 ? '������' : '�����';
$add_js("toastr.success('������������! ������� ��������� $tickets ������ � ���� �������� $total BGN. �� ������������� �� ��� ����� �� �������� �������� ���������� ������� ����������.' );");

$order_successfull = true;
ob_start();
$text['passengers'] = $_passengers;
$text['buyer'] = $buyer;
$text['route'] = $route;
$text['total_sum'] = $total;
$text['ticket']['link'] = 'http://www.grouptravel.bg/Templates/grouptravel/templates/busticket/printable_busticket.php?s='.$schedule->getId().'&r='.$route->getId().'&p='.$payer->getId();
$log->info("$u ������� �������� $tickets ������ �� �������� $total", $text);

require_once __DIR__ .'/../../../global/bus_tickets/templates/order_accepted.php';

$email_content = ob_get_contents();

ob_clean();

$sender = array('email' => $o_site->_site['EMail'], 'name' => $o_site->_site['Name']);

echo $email_content;

$broker = $di_container['broker'];

$_ = function ($e) {
    return iconv('cp1251', 'utf8', $e);
};

$broker->publish(
    Maksoft\Task\SendEmail::delay($sender, $recipients, $_($text['title']), $_($email_content))
);
