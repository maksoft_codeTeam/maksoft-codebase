<?php
use Doctrine\Common\Collections\Criteria;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
require_once __DIR__ .'/../../../global/bus_tickets/forms/AddPayer.php';
$log = new Logger('schedule');
$log->pushHandler(new StreamHandler(__DIR__.'/../logs/user_flow.log', Logger::DEBUG));
$payer = new AddPayer($em, $request->request->get('payer', array()));
$order_successfull = false;

if(!isset($_GET['pickup1'], $_GET['dropoff1'], $_GET['deparature_date'])) {
    return;
}

$msg = '';
$user_date = date('d.m.Y', strtotime($_GET['deparature_date']));
?>
<style>
a.list-group-item {
    height:auto;
    min-height:220px;
}
a.list-group-item.active small {
    color:#fff;
}
.stars {
    margin:20px auto 1px;    
}
</style>
<?php 
?>

<?php $route = get_object($get_course(COMPANY_ID, $_GET['pickup1'], $_GET['dropoff1'], $user_date)); ?>
    <div class="bs-callout bs-callout-gt">
<?php
$criteria = 'eq';
if($route instanceof EmptyObject) {
    $route = get_object($get_course_relative(COMPANY_ID, $_GET['pickup1'], $_GET['dropoff1'], $user_date));
    $criteria = 'gt';
    $msg = '�� ���� ���� ���� �������� �������. <br> ���������� ��������� �� �� ���������� ���-������ ����.';
    if($route instanceof EmptyObject or !$route instanceof \Bus\models\Route) {
        $msg = '�� ��������� �� ��� ���� �� ����� �� ����� �������� ������� �������. <br>';
    }
}
$schedule = $get_schedule_relative($user_date, $criteria, $route);

echo $msg;
if(!$schedule) {
    return;
}
        ?>
</div>
        <?php
        
if($_SERVER['REQUEST_METHOD'] == "POST") {
    try {
        require_once __DIR__ .'/reserve_ticket.php';
    } catch( \Exception $e) {
        $log->error(session_id(). ' '. $e->getMessage());
        $add_js("toastr.error('{$e->getMessage()}');");
    }
}
if(!$order_successfull) {
    include __DIR__.'/../templates/single_route.php';
}
?>
