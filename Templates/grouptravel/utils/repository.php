<?php
require_once __DIR__.'/../../../lib/lib_functions.php';
if(!defined('MAX_RESERVED_TIME')) {
    define("MAX_RESERVED_TIME", 600);
}

use Doctrine\Common\Collections\Criteria;

class EmptyObject
{
    protected $data = array();
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }

        return null;
    }

    /**  As of PHP 5.1.0  */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /**  As of PHP 5.1.0  */
    public function __unset($name)
    {
        unset($this->data[$name]);
    }
    public function __call($name, $arguments)
    {
    }

    /**  As of PHP 5.3.0  */
    public static function __callStatic($name, $arguments)
    {
    }
}

$money = function($price) {
    $fmt = '%n ';
    return iconv('utf8', 'cp1251', (string) money_format($fmt, $price)) . "\n";
};

$format_time = function($minutes) {
    $hours = floor($minutes / 60); // Get the number of whole hours
    $minutes = $minutes % 60;
    return sprintf("%s %s %s", $hours, $hours < 2 ? " ���": " ����", $minutes > 0 ? " � $minutes ������" : '');
};

function get_object($obj) {
    return $obj ? $obj : new EmptyObject; 
}


function my_json_encode($in) { 
  $_escape = function ($str) { 
    return addcslashes($str, "\v\t\n\r\f\"\\/"); 
  }; 
  $out = ""; 
  if (is_object($in)) { 
    $class_vars = get_object_vars(($in)); 
    $arr = array(); 
    foreach ($class_vars as $key => $val) { 
      $arr[$key] = "\"{$_escape($key)}\":\"{$val}\""; 
    } 
    $val = implode(',', $arr); 
    $out .= "{{$val}}"; 
  }elseif (is_array($in)) { 
    $obj = false; 
    $arr = array(); 
    foreach($in AS $key => $val) { 
      if(!is_numeric($key)) { 
        $obj = true; 
      } 
      $arr[$key] = my_json_encode($val); 
    } 
    if($obj) { 
      foreach($arr AS $key => $val) { 
        $arr[$key] = "\"{$_escape($key)}\":{$val}"; 
      } 
      $val = implode(',', $arr); 
      $out .= "{{$val}}"; 
    }else { 
      $val = implode(',', $arr); 
      $out .= "[{$val}]"; 
    } 
  }elseif (is_bool($in)) { 
    $out .= $in ? 'true' : 'false'; 
  }elseif (is_null($in)) { 
    $out .= 'null'; 
  }elseif (is_string($in)) { 
    $out .= "\"{$_escape($in)}\""; 
  }else { 
    $out .= $in; 
  } 
  return "{$out}"; 
} 

$week_days = array(
    'en' => array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'),
    'bg' => array('����������', '�������', '�����', '���������', '�����', '������', '������'),
);
$day_name = function($day, $local='bg') use ($week_days){
    if(!isset($week_days[$local])) {
        $local = 'bg';
    }

    return $week_days[$local][$day];
};

function getDayName($day, $local='bg') {
    return jddayofweek($day);
}

function getWeekday($date) {
    return date('w', strtotime($date)) -1 ;
}

$get_station = function($station_id) use ($em) {
    return $em->getRepository('Bus\models\Station')->findOneById($station_id);
};


$directions_from = function ($company_id) use ($em) {
    return $em->createQueryBuilder()->select('r')
       ->from('Bus\models\Route', 'r')
       ->join('Bus\models\Schedule', 'sh', 'WITH', 'sh.start_date <= :today')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->setParameter('today', new DateTime())
       ->setParameter('company_id', $company_id)
       ->groupBy('r.startStation')
       ->getQuery()
       ->getResult();
};

$directions_to = function($company_id, $city_id) use ($em) {
    return $em->createQueryBuilder()->select('r')
       ->from('Bus\models\Route', 'r')
       ->join('Bus\models\Schedule', 'sh', 'WITH', 'sh.start_date <= :today')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->where('r.startStation = :start_station')
       ->setParameter('start_station', $city_id)
       ->setParameter('today', new DateTime())
       ->setParameter('company_id', $company_id)
       ->getQuery()
       ->getResult();
};
$get_course = function($company_id, $city_from, $city_to, $date) use ($em) {
    $date = date('Y-m-d', strtotime($date));
    return $em->createQueryBuilder()->select('r')
       ->from('Bus\models\Route', 'r')
       ->join('Bus\models\Schedule', 'sh', 'WITH', 'sh.start_date = :today')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->where('r.startStation = :start_station')
       ->andWhere('r.endStation = :end_station')
       ->andWhere('r.day_of_week = :day_of_week')
       ->setParameter('start_station', $city_from)
       ->setParameter('end_station', $city_to)
       ->setParameter('today', $date)
       ->setParameter('company_id', $company_id)
       ->setParameter('day_of_week', getWeekDay($date))
       ->getQuery()
       ->getOneOrNullResult();
};


$get_course_relative = function($company_id, $city_from, $city_to, $date) use ($em) {
    $date = date('Y-m-d', strtotime($date));
    return $em->createQueryBuilder()->select('r')
       ->from('Bus\models\Route', 'r')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->where('r.startStation = :start_station')
       ->andWhere('r.endStation = :end_station')
       ->setParameter('start_station', $city_from)
       ->setParameter('end_station', $city_to)
       ->setParameter('company_id', $company_id)
       ->getQuery()
       ->setMaxResults(1)
       ->getOneOrNullResult();
};

$get_route = function($from_city, $to_city, $direction_id) use ($em) {
    return $em->getRepository('Bus\models\Route')->findOneBy(array('startStation' => $from_city, 'endStation' => $to_city, 'direction' => $direction_id));
};

$get_schedule = function($route_id, $direction_id, $date, $criteria) use ($em) {
    $date = date('Y-m-d', strtotime($date));
    return $em->createQueryBuilder()->select('s')
        ->from('Bus\models\Schedule', 's')
        ->join('Bus\models\Direction', 'd', 'WITH', 'd.id = s.direction')
        ->join('Bus\models\Route', 'r')
        ->Where('r.id = :route_id')
        ->AndWhere("s.start_date $criteria :date")
        ->setParameter('route_id', $route_id)
        ->setParameter('date', $date)
        ->getQuery()
        ->setMaxResults(1)
        ->getOneOrNullResult();
};
$get_vehicles = function($route_id) use ($em) {
    return $em->createQueryBuilder()->select(array('v', 's'))
        ->from('Bus\models\Vehicle', 'v')
        ->from('Bus\models\Schedule', 'v')
        ->join('Bus\models\Direction', 'd', 'WITH', 'd.id = s.direction')
        ->join('Bus\models\Route', 'r')
        ->Where('r.id = :route_id')
        ->setParameter('route_id', $route_id)
        ->getQuery()
        ->getSingleResult();
};


$clean_tickets = function() use ($em) {
    $ticketsRepository = $em->getRepository('Bus\models\ReservedSeats');
    $criteria = Criteria::create()
        ->where(Criteria::expr()->lte("created", new \Datetime(date('Y-m-d H:i:s', time()- MAX_RESERVED_TIME))))
        ->andWhere(Criteria::expr()->eq("sold", false))
        ->andWhere(Criteria::expr()->eq("auto_remove", true))
    ;
    $tickets = $ticketsRepository->matching($criteria);

    foreach($tickets as $ticket) {
        $em->remove($ticket);
    }

    $em->flush();
};

$reserve_ticket = function($kwargs) use($em, $clean_tickets){
    //First remove trashy reservations 
    $clean_tickets();

    //Try to get reserved seat
    $reserve = $em->getRepository('Bus\models\ReservedSeats')
                  ->findOneBy(array(
                      'seat' => $kwargs['seat'],
                      'schedule' => $kwargs['schedule'],
                      'identifier' => session_id(),
                    ));
    // If there is reservation we delete it
    if($reserve){
        $em->remove($reserve);
        $em->flush();
        return array('number' => $reserve->getSeat()->getNumber(), 'id' => $reserve->getId());
    }

    // Trying to insert new reservation if other user is already reserved a seat throws Exception
    $seat = $em->getRepository('Bus\models\Seat')->findOneById($kwargs['seat']);
    $schedule = $em->getRepository('Bus\models\Schedule')->findOneById($kwargs['schedule']);
    $reserve = new Bus\models\ReservedSeats();
    $reserve->setSeat($seat);
    $reserve->setSchedule($schedule);
    $reserve->setIdentifier(session_id());
    $em->persist($reserve);
    $em->flush();
    return array('reserved' => true, 'number' => $seat->getNumber(), 'id' => $reserve->getId());

    

};


/*
$reserve_ticket = function($kwargs) use($em, $clean_tickets) {
    $ticket = $em->getRepository('Bus\models\Ticket')
                  ->findOneBy(array(
                      'vehicle' => $kwargs['vehicle'],
                      'client' => $kwargs['client'],
                      'seat' => $kwargs['seat'],
                      'schedule' => $kwargs['schedule'],
                      'route' => $kwargs['route'],
                    ));
    if($ticket) {
        $payload = array('reserved' => false, 'number' => $ticket->getSeat()->getNumber(), 'id' => $ticket->getId());
        $em->remove($ticket);
        $em->flush();
        return $payload;
    }
    $ticket = new Bus\models\Ticket();
    $ticket->setReserved(true);
    $ticket->setVehicle(get_vehicle($kwargs['vehicle']));
    $ticket->setBuyer(get_client($kwargs['client']));
    $ticket->setSeat(get_seat($kwargs['seat']));
    $ticket->setPrice(0);
    $ticket->setCreated(new DateTime());
    $ticket->setUpdated(new DateTime());
    $ticket->setSchedule(get_schedule_by_id($kwargs['schedule']));
    $ticket->setRoute(get_route_by_id($kwargs['route']));
    $em->persist($ticket);
    $em->flush();
    return array(
        #'reserved' => $ticket->getReserved(),
        'number' => $ticket->getSeat()->getNumber(), 
        'id' => $ticket->getId()
    );
};
 */

function get_schedule_by_id ($schedule_id) {
    global $em;
    return $em->getRepository('Bus\models\Schedule')->findOneById($schedule_id);
}

function get_vehicle ($vehicle_id) {
    global $em;
    return $em->getRepository('Bus\models\Vehicle')->findOneById($vehicle_id);
}

function get_seat($seat_id) {
    global $em;
    return $em->getRepository('Bus\models\Seat')->findOneById($seat_id);
}

function get_client ($client_id) {
    global $em;
    return $em->getRepository('Bus\models\Client')->findOneById($client_id);
}

function get_route_by_id($route_id) {
    global $em;
    return $em->getRepository('Bus\models\Route')->findOneById($route_id);
}

$get_stops_between = function($from_city, $to_city, $company_id) use($em) {
    return $em->createQueryBuilder()->select('r')
       ->from('Bus\models\Route', 'r')
       ->join('Bus\models\Direction', 'd', 'WITH', 'd.id = r.parent')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->where('r.startStation = :start_station')
       ->andWhere('r.endStation = :end_station')
       ->setParameter('start_station', $from_city)
       ->setParameter('end_station', $to_city)
       ->setParameter('company_id', $company_id)
       ->getQuery()
       ->getResult();
};

$is_free = function ($seat_id, $schedule_id, $vehicle_id) use($em){
    $res = $em->createQueryBuilder()->select('t')
        ->from('Bus\models\Ticket', 't')
        ->where('t.schedule = :schedule_id')
        ->andWhere('t.seat = :seat_id')
        #->andWhere('t.reserved = 1')
        ->andWhere('t.vehicle = :vehicle_id')
        ->setParameter('schedule_id', $schedule_id)
        ->setParameter('seat_id', $seat_id)
        ->setParameter('vehicle_id', $vehicle_id)
        ->getQuery()
        ->setMaxResults(1)
        ->getOneOrNullResult();
    return $res ? false : true;

};

$get_stops_between_dev = function($from_city, $to_city, $company_id) use($em) {
    return $em->createQueryBuilder()->select('t')
       ->from('Bus\models\Stop', 't')
       ->join('Bus\models\Route', 'r', 'WITH', 'r.id = t.route')
       ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
       ->where('r.startStation = :start_station')
       ->andWhere('r.endStation <= :end_station')
       ->setParameter('start_station', $from_city)
       ->setParameter('end_station', $to_city)
       ->setParameter('company_id', $company_id)
       ->getQuery()
       ->getResult();
};
