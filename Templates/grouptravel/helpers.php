<?php
function build_email_content($text) {
    ob_start();
    require_once __DIR__ .'/../../global/bus_tickets/templates/order_accepted.php';
    $email_content = ob_get_contents();
    ob_clean();
    return $email_content;
}

$_ = function ($e) {
    return iconv('cp1251', 'utf8', $e);
};
