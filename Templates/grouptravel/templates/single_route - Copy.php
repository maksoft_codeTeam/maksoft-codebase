<link rel='stylesheet' href='<?=ASSETS_DIR?>assets/css/jquery.seat-charts.css' type='text/css' media='all' />
<?php
use Doctrine\Common\Collections\Criteria;


$from_name = $route->getStartStation()->getCountry(). ', ' .$route->getStartStation()->getName();
$to_name = $route->getEndStation()->getCountry(). ', ' .$route->getEndStation()->getName();

// �� ������������  � PHP ��������� ������� �� 0(������), �� �� ����� � ���������� :) � ��� ����� ����� :)
$dayOfWeek +=1; 

$dateOfWeek = date('w', strtotime($schedule->getStartDate()));
$schedule_time = $schedule->getStartDate()->format('H:i');
$direction = $schedule->getDirection();
$day = ucfirst($week_days['bg'][$dayOfWeek]);
$day = in_array(substr($day, -1), array('�', '�')) ? "����� $day" : "����� $day";

$vehicles = $schedule->getVehicles();
$seat_matrix = array('vehicles' => array());
$schedule_id = $schedule->getId();

$is_free = function($seat) use($schedule_id) {
    return $seat->getSchedule()->getId() == $schedule;
};

foreach($vehicles as $vehicle) {
    foreach($vehicle->getSeats() as $seat) {
        $row = $seat->getRow();
        $col = $seat->getCol();
        $number = $seat->getNumber();
        if(!array_key_exists($row, $seat_matrix)) {
            $seat_matrix[$row] = array();
        }
        if($seat->isBlocked()) {
            $available = 'taken';
        } else {
            $available = count($seat->getReservations()) ? 'taken' : 'free';
        }

        $seat_matrix[$row][$col] = array(
            'id' => $seat->getId(),
            'number'=> $number,
            'available' => $available
        );
    }
}

?>

<?php /*?><?php if($_SESSION['user']->AccessLevel > 0): ?>
<pre>
<code> <?php #var_dump($_POST);?></code>
</pre>
<?php endif;?><?php */?>
                       
<?php 
    $course_info = $get_course_info($direction, $route);
    $time_to_course = $course_info['time']['to_course'];
    $travel_time = $course_info['time']['duration'];
    $deparature = $schedule->getStartDate()->modify("+$time_to_course minutes");
    $arrival = clone $deparature;
    $arrival->add(new DateInterval('PT' . $travel_time  . 'M'));
    $arrival_date = $arrival->format('�������� � H:i �� d-m-Y');
?>
                        
<div class="destination-info">
    <div class="row">
        <div class="col-md-6 first-col no-padmob">
            <div class="col-md-3 famob">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
            </div>
            <div class="col-md-9 no-padmob">
                �������� ��:<br/>
                <span>
                    <?=$from_name?>
                </span><br/> ���������� �:<br/>
                <span>
                <?=$to_name?>
                </span><br/> ����������:
                <br/>
                <span>
                <?=$course_info['distance']?> ��.</span>
            </div>
        </div>
        <div class="col-md-6 no-padmob">
            <div class="col-md-3 famob">
                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
            </div>
            <div class="col-md-9 no-padmob">
                T�������:<br/>
                <span>
                    <?=$deparature->format('� H:i �. �� d-m-Y');?>
                </span><br/> ��������:<br/>
                <span>
                    <?=$arrival->format('� H:i �. �� d-m-Y');?>
                </span><br/> �����������:
                <br/>
                <span>
                    <?=$format_time($travel_time)?>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
                <?php 
                    foreach($route->getPrices() as $price):
                          $pr = number_format($price->getPrice(), 2);
                ?>
                ���� �� <strong><?=$price->getType()->getDescription()?></strong>: <br/>
                <span class="price">
                    <?=$pr > 0 ? $pr . ' ��.' : '' ;?>
                </span>
                <?php 
                    endforeach; 
                ?>
        </div>
    </div>
</div>

<!--        <div class="row">
        <?php foreach($seat_matrix as $row): ?>
            <div class="row" style='margin-bottom: 5%;margin-left:-1%;margin-right:-1%'>
            <?php foreach($row as $seat): ?>
                <div class="col-xs-2" style='width:20%'>
                    <div class="seat <?php echo $seat['available'];?> text-center"
                         style="width:50px;" 
                         data-route="<?php echo $route->getId();?>"
                         data-seat="<?php echo $seat['id'];?>"
                         data-vehicle="<?php echo $vehicle->getId()?>"
                         data-client="1"
                         data-schedule="<?php echo $schedule->getId()?>"


                    >
                    <strong><?=$seat['number']?></strong>
                    </div> 
                </div>
            <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
        </div>-->
        
        <div class="bs-title">
            <span>�������� �����</span>
        </div>
        <div class="row">
            
            <div class="col-md-6">
                <div id="seat-map">
                    <div class="front-indicator">�������</div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="booking-details">
                    <h2>�������</h2>
                    
                    <h3> ������� ����� (<span id="counter">0</span>):</h3>
                    <ul id="selected-seats"></ul>
                    
                    ����: <strong><span id="total">0</span> ��.</strong>
                    
                    <div id="legend"></div>
                </div>
            </div>
</div>
       
        <div class="bs-title">
            <span>��������� ����� �� ���������</span>
        </div>

        <form method="POST">
        <div id='passengers'>
        <?php $i=1;?>
        <?php foreach($get_reserved_tickets(array('schedule' => $schedule_id)) as $seat): ?>
            <?php include __DIR__.'/passenger_form.php';?>
            <?php $i++;?>
        <?php endforeach; ?>
        </div>
        <div class='checkout row'>
            <div class="col-md-12">
                <div class="well">
                    <h4>������</h4>
                    <fieldset id="private-fieldset">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="payer-firstname" class="control-label">���</label>
                                    <input id="first_name" name="payer[firstName]" type="text" placeholder="" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="payer-lastname" class="control-label">�������</label>
                                    <input type="text" maxlength="25" id="payer-lastname" name="payer[lastName]" value="">
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset id="common-fieldset">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="payer-phone" class="control-label" data-trigger="hover" data-toggle="popover" data-content="����� �� ������� �� ������ � ������� , ������������ �������, ��� �������� ����, �� ����� �� ������� ���� �� �� ������ ������ ����������, � ������ �� ������� � �� �  ������������ ������������� �������� �� �� ���������. " data-original-title="" title="">
                                        <span>�������</span>
                                        <i class="fa fa-info-circle"></i>
                                    </label>
                                    <div class="intl-tel-input allow-dropdown">
                                        <div class="flag-container">
                                            <div class="selected-flag" tabindex="0">
                                                <div class="iti-flag gb"></div>
                                                <div class="iti-arrow"></div>
                                            </div>
                                        </div>
                                        <input type="tel" maxlength="15" id="payer-phone" name="payer[phone]" value="" autocomplete="off">
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="payer-email" class="control-label" data-trigger="hover" data-toggle="popover" data-content="���������� ���� �� �������. ���������� �� ��������� �� ���������� ������,�� ��������� �� ���������� �� ��������� � �� ���������� �� ������ ��� ������� � �� � ������������ �������������." data-original-title="" title="">
                                        <span>EMAIL</span>
                                        <i class="fa fa-info-circle"></i>
                                    </label>
                                    <input id="email" name="payer[email]" type="text" placeholder="example@example.com" required="">

                                </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-12">
                                <?php require_once __DIR__ .'/../../../web/forms/form_captcha.php';?>
                              </div>
                            </div>
                        <input type='hidden' name='deparature_date' value="<?php #echo $deparature->format('d-m-Y H:i')?>">
                        <input type='hidden' name='total_sum'>
                        <input type='hidden' name='priceId'>
                        <input type='hidden' name='price'>
                        <input type='hidden' name='route_name' value="<?=$from_name . ' '. $to_name?>">
                        <input name="ConfText" type="hidden" id="ConfText" value="����������� �� � ����������!"> 
                        <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
                        <input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
                        <input name="ToMail" type="hidden" id="ToMail" value="<?=$o_site->_site['EMail']; ?>"> 
                        <input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$Site->StartPage&SiteID=$SiteID&sent=1"); ?>">
                        <input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
                        <input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">
                        <div class="verify-hidden">
                            <input name="verify" type="text">
                        </div>
                            <div class="col-md-6">
                                <p></p>
                                <button type="submit" name="Submit" class="btn btn-primary btn-lg" > ����</button>
                            </div>
                        </div>
                    
                    </fieldset>
                </div>
            </div>
        </div>
    </form>

<?php require_once __DIR__.'/../templates/js_templates.php'; ?>

<script>
    jQuery('#submit').on('click', function (e) {
        var button = jQuery(this) // Button that triggered the modal
        const data = button.data();
        jQuery("input[name=price]").val(data.price);
        jQuery("input[name=deparature_date]").val(data.deparature_date);
        jQuery("input[name=priceId]").val(data.priceId);
        jQuery("input[name=route_name]").val(data.route_name);
        jQuery("input[name=total_sum]").val(data.price);
        jQuery("#total").text(data.price + ' ��.');
        jQuery("#myModalLabel").html("���� ����� �� ����: <br>" + data.route_name);
        jQuery("input[name=qty]").on('change', function(el) {
            var input = jQuery(this);
            jQuery("input[name=total_sum]").val(data.price);
            const price = data.price * input.val();
            jQuery("#total").text(price + ' ��.');
        });
    });
jQuery('.seat').on('click', function(el) {

});
jQuery(document).ready(function(){
    jQuery('[data-toggle="popover"]').popover(); 
});

function passenger_info(seat){
    const source = jQuery("#passenger-info").html(); 
    var template = Handlebars.compile(source);
    context = {
        n: jQuery("#passengers").find('fieldset').length + 1,
        price: 2,
        currency: 'bgn',
        seat_id: seat.id
    };
    return template(context);
}

function update_price(el) {
    var select = jQuery(el);
    
    var option= select.find(':selected');
    var data = option.data()
    console.log(data.num);
    if(data.price) {
        price = data.price + ' ��.';
    } else {
        price = '';
    }
    jQuery("#info-price" +select.data('num')).text(price);
    jQuery("#passengers" + data.num + "-" + "ticket_price").val(price);
    jQuery("#passengers" + data.num + "-" + "ticket_type").val(option.val());
}
</script>

<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery.seat-charts.js'></script>

<script>
            var firstSeatLabel = 1;
        
            jQuery(document).ready(function(){
                var $cart = jQuery('#selected-seats'),
                    $counter = jQuery('#counter'),
                    $total = jQuery('#total'),
                    sc = jQuery('#seat-map').seatCharts({
                    map: [
                        'nn_ne',
                        'ee_ee',
                        'ee_ee',
                        'ee_ee',
                        'ee___',
                        'ee_ee',
                        'ee_ee',
                        'ee_ee',
                        'eeeee',
                    ],
                    seats: {
                        e: {
                            price   : 40,
                            classes : 'economy-class', //your custom CSS class
                            category: '�������� �����'
                        }           
                    
                    },
                    naming : {
                        top : false,
                        getLabel : function (character, row, column) {
                            return firstSeatLabel++;
                        },
                    },
                    legend : {
                        node : jQuery('#legend'),
                        items : [
                            [ 'e', 'available',   '�������� �����'],
                            [ 'f', 'unavailable', '����� �����'],
                            [ 'n', 'notactive',   '��������� �����' ]
                        ]                   
                    },
                    click: function () {
                        console.log('click');
                        var seat = jQuery(this);
                        payload = seat.data();
                        payload['command'] = 'reserve_ticket';
                        console.log(payload);
                        jQuery.ajax({
                            url: '<?=$o_page->scheme?><?=$_SERVER['HTTP_HOST']?>/forms/gtravel/api.php',
                            type: 'get',
                            data: payload,
                            success: function(resp) {
                                if(resp.reserved) {
                                    toastr.success('������������ ����o N:' + resp.number);
                                    jQuery("#passengers").append(passenger_info(resp));
                                    jQuery('[data-toggle="popover"]').popover(); 
                                } else {
                                    toastr.success('����������� ����o N:'+resp.number);
                                    jQuery('#passengers').children().last().remove();
                                }
                                if(seat.hasClass('free')) {
                                    seat.removeClass('free').addClass('taken');
                                } else {
                                    seat.removeClass('taken').addClass('free');
                                }
                            }, 
                            error: function() {
                                toastr.error('������� � ����������� �� ����.');
                            }
                        });
    
                        if (this.status() == 'available') {
                            //let's create a new <li> which we'll add to the cart items
                            jQuery('<li>- ����� # '+this.settings.label+': <b>'+this.data().price+' ��.</b> <a href="#" class="cancel-cart-item">[������]</a></li>')
                                .attr('id', 'cart-item-'+this.settings.id)
                                .data('seatId', this.settings.id)
                                .appendTo($cart);
                            
                            /*
                             * Lets update the counter and total
                             *
                             * .find function will not find the current seat, because it will change its stauts only after return
                             * 'selected'. This is why we have to add 1 to the length and the current seat price to the total.
                             */
                            $counter.text(sc.find('selected').length+1);
                            $total.text(recalculateTotal(sc)+this.data().price);
<?php /*?>                          <?php 
                                foreach($seat_matrix as $row):
                                foreach($row as $seat): 
                            ?>
                            return 'selected <?php echo $seat['available'];?>';
                            <?php 
                                endforeach;
                                endforeach; 
                            ?><?php */?>
                            return 'selected';
                        } else if (this.status() == 'selected') {
                            //update the counter
                            $counter.text(sc.find('selected').length-1);
                            //and total
                            $total.text(recalculateTotal(sc)-this.data().price);
                        
                            //remove the item from our cart
                            jQuery('#cart-item-'+this.settings.id).remove();
                        
                            //seat has been vacated
                            return 'available';
                        } else if (this.status() == 'unavailable') {
                            //seat has been already booked
                            return 'unavailable';
                        } else {
                            return this.style();
                        }
                    }
                });

                //this will handle "[cancel]" link clicks
                jQuery('#selected-seats').on('click', '.cancel-cart-item', function () {
                    //let's just trigger Click event on the appropriate seat, so we don't have to repeat the logic here
                    sc.get($(this).parents('li:first').data('seatId')).click();
                });

                //let's pretend some seats have already been booked
                sc.get(['4_1', '7_1', '7_2']).status('unavailable');
                sc.get(['1_1', '1_2', '1_3']).status('notactive');
        
        });

        function recalculateTotal(sc) {
            var total = 0;
        
            //basically find every selected seat and sum its price
            sc.find('selected').each(function () {
                total += this.data().price;
            });
            
            return total;
        }
        
        </script>
