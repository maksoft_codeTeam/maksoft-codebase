<div id="home">
<div id="column_left"><?php include TEMPLATE_DIR . "column_left.php"; ?></div>
<div id="pageContent">
    <div class="page-content">
     <ul id="sdt_menu" class="sdt_menu">
	<?php
    	$products = $o_page->get_pSubpages(182046, "p.sort_n ASC LIMIT 4");
		for($i=0; $i<count($products); $i++)
			{
				echo "<li><a href=\"".$o_page->get_pLink($products[$i]['n'])."\" title=\"".$products[$i]['title']."\">";
				echo "<div class=\"img_wrap\">";
				$o_page->print_pImage(300, "", NULL, $products[$i]['n']);
				echo "</div>";
				echo '
				   <span class="sdt_active"></span>
					<span class="sdt_wrap">
						<span class="sdt_link">'.$products[$i]['Name'].'</span>
						<span class="sdt_descr">'.$o_page->get_pName($products[$i]['ParentPage']).'</span>
					</span>
				';			
				echo "</a>";
				echo "<div class=\"sdt_box\">".$products[$i]['textStr']."</div>";
				echo "</li>";
			}
    ?>


    </ul>
    </div>
</div>
<div id="column_right"><a class="box arrow right" href="#"></a></div>
</div>
       <!-- The JavaScript -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
		<script type="text/javascript" src="http://mpetrov.com/includes/SlideDownBoxMenu/jquery.easing.1.3.js"></script>
        <script type="text/javascript">
            $(function() {
				/**
				* for each menu element, on mouseenter, 
				* we enlarge the image, and show both sdt_active span and 
				* sdt_wrap span. If the element has a sub menu (sdt_box),
				* then we slide it - if the element is the last one in the menu
				* we slide it to the left, otherwise to the right
				*/
                $('#sdt_menu > li').bind('mouseenter',function(){
					var $elem = $(this);
					$elem.find('.img_wrap')
						 .stop(true)
						 .animate({
							'width':'180px',
							'height':'300px',
							'left':'0px',
							'top': '0px'
						 },400,'easeOutBack')
						  .andSelf()
						  .find('img')
						 .stop(true)
						 .animate({
							'top': '0px'
						 },400,'easeOutBack')
						 .andSelf()
						 .find('.sdt_wrap')
					     .stop(true)
						 .animate({'top':'-60px'},500,'easeOutBack')
						 .andSelf()
						 .find('.sdt_active')
					     .stop(true)
						 .animate({'height':'180px'},300,function(){
						var $sub_menu = $elem.find('.sdt_box');
						if($sub_menu.length){
							var left = '180px';
							if($elem.parent().children().length == $elem.index()+1)
								left = '-300px';
							$sub_menu.show().animate({'left':left},200);
						}	
					});
				}).bind('mouseleave',function(){
					var $elem = $(this);
					var $sub_menu = $elem.find('.sdt_box');
					if($sub_menu.length)
						$sub_menu.hide().css('left','0px');
					
					$elem.find('.sdt_active')
						 .stop(true)
						 .animate({'height':'0px'},300)
						 .andSelf().find('.img_wrap')
						 .stop(true)
						 .animate({
							'width':'180px',
							'height':'180px',
							'left':'0px',
							'top': '0px'
							},400)
						.andSelf()
						  .find('img')
						 .stop(true)
						 .animate({
							'top': '-20px'
						 },400,'easeOutBack')
						 .andSelf()
						 .find('.sdt_wrap')
						 .stop(true)
						 .animate({'top':'0px'},500);
				});
            });
        </script>