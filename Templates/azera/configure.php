<?php
	//template base/tmpl_001 configurations
	
	define("TEMPLATE_DEFAULT_BANNER", "");
	define("TEMPLATE_DEFAULT_IMAGE_DIR", "");
	define("TEMPLATE_DIR", "Templates/azera/");
	define("TEMPLATE_IMAGES", "http://www.maksoft.net/Templates/azera/images/");

	$tmpl_config = array(
		"change_banner"=>true,
		"change_css"=>true,
		"default_max_width"=> (defined('CMS_MAX_WIDTH'))? CMS_MAX_WIDTH : "960",
		"default_main_width"=>"500",
		"default_menu_width"=>"180",
		"default_banner_width" => (defined('CMS_MAX_WIDTH'))? CMS_MAX_WIDTH : "960",
		"default_banner" => TEMPLATE_IMAGES."banner.jpg",
		"default_logo" => TEMPLATE_IMAGES."logo.png",
		"default_logo_width" => "180",
		"column_left" => TEMPLATE_DIR . "menu.php",
		"box_login" => TEMPLATE_DIR . "login.php",
		"box_social_links" => TEMPLATE_DIR . "social_links.php",
		"date_format"=> "d-m-Y"
	);
	
	if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->_site['Logo'];
	
?>