<?php
//read all products in selected categories and subcategories
$cats_array = array(45288, 45289, 45290, 45291);
$products_array = array();

//loop trough the main categories
for($i=0; $i<count($cats_array); $i++)
	{	
		$tmp = new page($cats_array[$i]);
		$tmp_array = $tmp->get_pSubpages();
		
		//loop trough the subcategories
		for($j=0; $j<count($tmp_array); $j++)
			{
				$tmp2 = new page($tmp_array[$j]['n']);
				$tmp2_array = $tmp2->get_pSubpages($tmp2->n, "date_added DESC");
				
				//loop trough the products
				for($k=0; $k<count($tmp2_array); $k++)
					{
						$t_array['n'] = $tmp2_array[$k]['n'];
						$t_array['date'] = $tmp2_array[$k]['date_added'];
						array_push($products_array, $t_array);
					}
			}
	}

//echo count($products_array);
?>

<div id="products_listing">
<?php
// Obtain a list of columns
foreach ($products_array as $key => $row)
{
    $n[$key]  = $row['n'];
    $date[$key] = $row['date'];
}
// Sort the data with date descending
array_multisort($date, SORT_DESC, $products_array);

$limit = count($products_array);
if($limit > 30) $limit = 30;
	
for($i=0; $i<$limit; $i++)
{
	$f_page = new page($products_array[$i]['n']);
	?>
		<div class="product">
		<div class="title"><?=$f_page->print_pName()?></div>
		<a href="<?=$f_page->get_pLink()?>" style="display: block; width: 118px; height: 118px; background-color: #FFFFFF; float: left;" class="border_image"><?=$f_page->print_pImage("118", "border=0")?></a>
		<div class="text"><?=$f_page->print_pText()?></div>
		<?php
		//generate button_more (shopping cart button)
			if($o_user->get_uLevel() > 0 && count($f_page->get_pPrice()) > 0)
				$button_more = "Templates/animalia/images/shopping_cart.jpg";
			else
				$button_more = "Templates/animalia/images/button_more_hover.jpg";
		?>
		<a href="<?=$f_page->get_pLink()?>" ><img src="<?=$button_more?>" border="0" style="margin: 0px; float: right;"></a>
		</div>
	<?php
	
}
?>
</div>