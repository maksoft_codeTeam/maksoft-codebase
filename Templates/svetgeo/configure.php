<?php
	define("TEMPLATE_DIR", "Templates/svetgeo/");
	define("TEMPLATE_IMAGES", "http://www.maksoft.net/Templates/svetgeo/images/");

	//define some default values
	$tmpl_config = array(
		"default_max_width"=> (defined('CMS_MAX_WIDTH'))? CMS_MAX_WIDTH : "960",
		"default_main_width"=>"500",
		"default_menu_width"=>"180",
		"default_banner_width" => (defined('CMS_MAX_WIDTH'))? CMS_MAX_WIDTH : "960",
		"default_banner" => TEMPLATE_IMAGES."banner.jpg",
		"default_logo" => TEMPLATE_DIR."images/svetgeo_logo.png",
		"default_logo_width" =>"240",
		"menu_drop_down" => TEMPLATE_DIR . "menu_drop_down.php",
		"column_left" => array(
								TEMPLATE_DIR . "menu.php"
								),
		"column_right" => array(
								TEMPLATE_DIR . "boxes/news.php",
								TEMPLATE_DIR . "boxes/social_links.php"
								),
		"footer" => array(
								TEMPLATE_DIR . "boxes/news.php"
							),		
		"date_format"=> "d-m-Y"
	);
	
	if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->get_sImageDIr()."/".basename($o_site->_site['Logo']);
	
?>