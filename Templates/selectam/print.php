<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title><?php echo("$Title");?></title>
<?php
require_once "lib/lib_page.php";

include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/selectam/");
define("DIR_TEMPLATE_IMAGES","http://wwww.maksoft.net/Templates/selectam/images/");
define("DIR_MODULES","web/admin/modules");

$o_site->print_sConfigurations();

?>
<link href='http://www.maksoft.net/Templates/selectam/layout.css' rel='stylesheet' type='text/css'>
<link href='http://www.maksoft.net/Templates/selectam/base_style.css' rel='stylesheet' type='text/css'>
<link href='http://www.maksoft.net/Templates/selectam/poll/poll.css' rel='stylesheet' type='text/css'>
<style type="text/css">
	#poll {display: block; clear: both; margin: 0 0 100px 0;}
	#poll ol.questions {list-style: decimal-leading-zero; }
	#poll ol.questions li {font-size: 14px; font-weight:bold; margin: 10px 0 10px 0; background:#f3f3f3; padding: 5px; border: 1px solid #FFF; cursor:pointer;}
	#poll ol.questions li:hover {border: 1px solid #CCC}
	#poll ol.questions li:nth-child(odd) {background: #FFF;}
	
	#poll ol.answers {list-style: upper-latin; display: block;}
	#poll ol.answers li{font-size: 12px; font-weight:normal; font-style:italic; margin: 2px 0 2px 0; background:none; padding:0px; border: 0px solid #f3f3f3}
	#poll ol.answers li:hover {border: 0px solid #f3f3f3}
	#poll ol.answers li:nth-child(odd) {background: none;}
	
	.poll_results {border: 1px solid #f68b1f; margin: 20px auto 20px auto; }
	.poll_results th {background: #f68b1f; color: #FFF; text-align: center;}
	.poll_results td {background: #fef4e9; color: #58595b; text-align:center; font-style:italic;}
	.poll_results td.result {background: #808184; color: #FFF;}
	.poll_results td b {font-style:normal;}
	.poll_results td:hover {background-color: #FFF; color: #58595b; cursor: pointer;}
	
	#poll .submit {display: block; float: right; width: 200px; line-height:35px; text-align:center; height: 35px; border: none; background: #f68b1f; color: #FFF; cursor:pointer;}
	#poll .reset {display: block; float: left; width: 150px; height: 25px; line-height: 25px; text-align:center; margin: 5px 0 5px 0; border: none; background: #d6d6d6; color: #000; cursor:pointer;}
	#poll .submit:hover, #poll .reset:hover {background: #f6f6f6; color: #000;}
	#poll p strong {color: #f68c1f}
	#poll span.special_text {font-weight:normal; font-style:italic; font-size:12px}
</style>
</head>
<body style="background: #FFF">
<div id="print_container">
	<div id="page_container">
	<div id="pageContent" style="width:800px;">
    	<img src="<?=DIR_TEMPLATE_IMAGES?>blank_top_line.jpg" alt="">
        <h1><?=$o_page->get_pInfo("title")?></h1>
        <div class="content" style="padding: 0 20px 20px 20px; overflow:hidden">
        <?php
            //eval($o_page->get_pInfo("PHPcode"));
            $o_page->print_pContent();
            //$o_page->print_pSubContent();
        ?>
        <br style="clear: both;">
        <?php include $o_page->get_pURL();?>
        </div>
        <img src="<?=DIR_TEMPLATE_IMAGES?>blank_bottom_line.jpg" alt="">
    </div>
    </div>
</div>
</body>
</html>