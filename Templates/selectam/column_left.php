<?php
	if($o_page->_page['ParentPage'] != $o_page->_site['StartPage'])
		$menu = $o_page->get_pSubpages($o_page->_page['ParentPage'], "p.sort_n ASC", "p.toplink != 1 AND p.toplink != 5");
	else
		$menu = $o_page->get_pSubpages();
?>
<h3><?=$o_page->get_pName(($o_page->_page['ParentPage'] != $o_page->_site['StartPage']) ? $o_page->_page['ParentPage'] : $o_page->_page['n'])?></h3>
<div id="menu">
	<?php
    	for($i=0; $i<count($menu); $i++)
			{
				$class = "";
				$submenu_str = "";
				if($o_page->n == $menu[$i]['n'])
					{
						$class = "selected";
						$submenu = $o_page->get_pSubpages($menu[$i]['n']);
						if(count($submenu) > 0)
							{
								$submenu_str.= "<div class=\"submenu\">";
								for($j=0; $j<count($submenu); $j++)
									$submenu_str.= "<a href=\"".$submenu[$j]['page_link']."\" title=\"".$submenu[$j]['title']."\">".$submenu[$j]['Name']."</a>";
								$submenu_str.= "</div>";
							}
					}
				echo "<a href=\"".$menu[$i]['page_link']."\" class=\"".$class."\">".$menu[$i]['Name']."</a>";
				echo $submenu_str;
			//$o_page->print_pName(true, $menu[$i]['n']);
			}
	?>
</div>