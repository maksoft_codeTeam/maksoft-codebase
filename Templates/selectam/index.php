<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title><?php echo("$Title");?></title>
<?php
include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/selectam/");
define("DIR_TEMPLATE_IMAGES","http://wwww.maksoft.net/Templates/selectam/images/");
define("DIR_MODULES","web/admin/modules");

$o_site->print_sConfigurations();

//top menu
$top_menu = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC", "p.toplink=1");

//top navigation menu (second links)
$top_nav_menu = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC", "p.toplink=5");

?>
<link href='http://www.maksoft.net/Templates/selectam/layout.css' rel='stylesheet' type='text/css' media="screen">
<link href='http://www.maksoft.net/Templates/selectam/base_style.css' rel='stylesheet' type='text/css'>
<link href='http://www.maksoft.net/Templates/selectam/poll/poll.css' rel='stylesheet' type='text/css' media="screen">
<!-- Include Print CSS -->
<link href="http://www.maksoft.net/Templates/selectam/print.css" rel="stylesheet" type="text/css" media="print" />
<script language="javascript" type="text/javascript">
	
	jQuery(function($){
		$('#search').click(function(){
			$('#search_form').submit();
			})
		
		$('.column').height($('#pageContent').height() - 200);
		$('#pageContent').height($('.column').height() + 190)
		
		$('#search_field').focus(function(){
				$(this).val("");
			})
	})
</script>
</head>
<body>
<div id="site_container">
	<div id="top_nav_menu">
    <a href="http://www.ccbank.bg" title="CCB" target="_blank" style="float: left; text-decoration:underline; margin: 0 0 0 250px">www.ccbank.bg</a>
    <div id="box_search">
    <form method="get" action="page.php" id="search_form">
    <input type="hidden" name="n" value="36274">
    <input type="hidden" name="SiteID" value="<?=$o_site->SiteID?>">
    <div class="text_field"><input type="text" name="search" id="search_field" size="28" value="<?=$o_site->_site['SearchText']?>"><a href="javascript: void(0)" id="search"><img src="<?=DIR_TEMPLATE_IMAGES?>btn_search.jpg" style="float: right;"></a></div>
    </form>
    </div>
	<div class="nav_menu">
	<?php
    	for($i=0; $i<count($top_nav_menu); $i++)
			{
				$class = "";
				if($o_page->n == $top_nav_menu[$i]['n']) $class = "selected";
				echo "<a href=\"".$top_nav_menu[$i]['page_link']."\" class=\"".$class."\">".$top_nav_menu[$i]['Name']."</a>";	
				//$o_page->print_pName(true, $top_nav_menu[$i]['n']);
			}
	?>


    </div>
    </div>
    <div id="page_container" class="shadow">
    	<a href="http://<?=$o_site->_site['primary_url']?>" id="logo"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_sam.jpg" class="shadow" border="0" alt=""></a>
        <div id="header">
            <?php include "header.php"; ?>
        </div>
        <?php
            if($o_page->_page['SiteID'] == 1)
                include "admin.php";
            else
            if($o_page->n == $o_site->get_sStartPage())
                include "home.php";
            else include "main.php";
        ?>
        <div id="footer">
              <?php if($o_page->_page['SiteID'] != 1) include "footer.php"; ?>
        </div>
    </div>
    <br clear="all">
</div>
</body>
</html>
