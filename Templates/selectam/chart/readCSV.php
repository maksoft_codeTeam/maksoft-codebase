<?php
//if($_POST['action'] == "insert_fund_file" && $upload_status == 1)
if($_POST['action'] == "insert_fund_file")
{
	$i=0;
	//$csv_data_separator = ",";
	$csv_data_separator = $_POST['csv_data_separator'];
	
	if (($handle = fopen($csv_dir.$csv_file_name , "r")) !== FALSE)
	{
		while (($data = fgetcsv($handle, 1000, $csv_data_separator)) !== FALSE) {
			$num = count($data);
			for ($c=0; $c < $num; $c++) {
				$data_str = $data[$c];
				$data_str = str_replace(",", ".", $data_str);
				$data_str = str_replace("%", "", $data_str);
				$data_str = str_replace("/", "-", $data_str);
				$data_str = str_replace("#DIV/0!", "0", $data_str);
				
				//leave only "0-9" "." "-"
				$pattern = '/[^0-9.-]*/';
				$data_str =  preg_replace($pattern, '', $data_str);
				
				//each row from the CSV file is an array element
				$data_array[$i][] = $data_str;
			}
			$i++;
		}
		fclose($handle);
	}
	
	//get data template keys for every fund
	$data_fields = array_keys($data_template);
	//data insert counter
	$di_counter = 0;
	//skip the first row of the CSV file 
	for($i=1; $i<count($data_array); $i++)
		{
			$set = "";
			for($j=0; $j<count($data_fields); $j++)
				{
					if($j>0)
						$set.=", ";

					//first data_array element is date
					if($j==0)
						$data_array[$i][$j] = date("Y-m-d", strtotime($data_array[$i][$j]));
					
					$set.= $data_fields[$j]." = '".$data_array[$i][$j]."'";
				}
			//echo $set."<hr>";
			$query = "INSERT INTO ".$selected_fund['fund_table']." SET $set";
			//echo $query."<hr>";
			if(mysqli_query($query)) $di_counter++;
		}
		
	if($di_counter >0)
		mk_output_message("normal", "<b>".$di_counter."</b> row(s) inserted");
	else
		mk_output_message("warning", "No rows inserted !");
}
?>