<?php
	//Fund name			: �� �����
	//data structure	: name = > title
	//data rows			: 17
	
	$data_template = array(
		"OnDate" => "Date", 
		"EURRate" => "Euro",
		"RateLess" => "Rate / less than 100k",
		"RateMore" => "Rate / more than 100k",
		"redemption" => "Redemption",
		"Volume" => "Volume",
		"RateChange" => "Change",
		"EUR" => "EUR",
		"BGN" => "BGN",
		"StDev" => "StDev",
		"VaR" => "VaR (20, 99%)",
		"L" => "L",
		"FromBonds1" => "From Bonds (1.65%)",
		"FromBonds2" => "From Bonds (0.80%)",
		"FromKDPelikan1" => "from KD Pelikan(1%)",
		"FromKDPelikan2" => "from KD Pelikan(0%)",	
		"Investors" => "NumOfActiveInvestors"
		);
	
	//chart data view	
	$chart_data = array("OnDate", "redemption");
?>