<?php
require_once __DIR__."/bus_rent/vendor/autoload.php";
require_once __DIR__."/bus_rent/bootstrap.php";
require_once __DIR__.'/../../lib/HttpResponseCode.php';
session_start();
$forms = array();
$message = array("message"=>"", "code"=> "");
$search = new \Maksoft\Rent\Bus\Form\Search($entityManager, $_POST);
$search->setId("search");
$order = new \Maksoft\Rent\Bus\Form\Enquiry($entityManager, $_POST);
$order->setId("search");
$client = new \Maksoft\Rent\Bus\Form\Client($entityManager, $_POST);
$registration = new \Maksoft\Rent\Bus\Form\CheckIfRegistered($entityManager, $_POST);
$login = new \Maksoft\Rent\Bus\Form\Login($entityManager, $_POST);

$logout = new \Maksoft\Rent\Bus\Form\Logout($entityManager, $_POST);

$forms[$search->action->value] = $search;
$forms[$client->action->value] = $client;
$forms[$registration->action->value] = $registration;
$forms[$login->action->value] = $login;
$forms[$order->action->value] = $order;
$forms[$logout->action->value] = $logout;

if($_SERVER["REQUEST_METHOD"] === "POST" and isset($_POST['action'])){
    if(array_key_exists($_POST['action'], $forms)){
        $form = $forms[$_POST['action']];
            try{
                $form->is_valid();
                $msg = $form->save();
                if(is_array($msg)){
                    if(array_key_exists('session', $msg)){
                        $_SESSION['bus'] = $msg['session'];
                        unset($msg['session']);
                    }
                    $message = $msg;
                } else {
                    $message["text"] = "������� ���������";
                    $message["code"] = "success";
                }
                header(Response::status(CODE::HTTP_CREATED));
            } catch (\Exception $e){
                $message['text'] =  $e->getMessage();
                $message['status'] = 'error';
                header(Response::status(CODE::HTTP_UNAUTHORIZED));
            }
        exit(json_encode($message));
    }
    header(Response::status(CODE::HTTP_UNAUTHORIZED));
    exit(json_encode(array("text"=>"unknown form [".$_POST['action']."]", "code" => "error")));
} elseif( $_SERVER["REQUEST_METHOD"] === "POST" and isset($_POST['is_logged'])){
    if(isset($_SESSION['bus'], $_SESSION['bus']['expire'], $_SESSION['bus']['logged_at']) and $_SESSION['bus']['expire'] > $_SESSIOn['bus']['logged_at']){
        $message['text'] = "���� ��� �������";
        $message['code'] = "success";
        header(Response::status(CODE::HTTP_ACCEPTED));
        exit(json_encode($message));
    }
    $message['text'] = '�� ��� ������ ��� ������ ������.';
    $message['code'] = 'error';
    header(Response::status(CODE::HTTP_UNAUTHORIZED));
    exit(json_encode($message));
}

header(Response::status(CODE::HTTP_METHOD_NOT_ALLOWED));
exit(json_encode(array("text"=>"GET method is not supported", "code" => "error")));
