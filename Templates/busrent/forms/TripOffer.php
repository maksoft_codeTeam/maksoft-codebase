<?php
use \Jokuf\Form\DivForm;
use \Jokuf\Form\Field\Text;
use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Field\Integer;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Field\Date;

use Maksoft\Rent\Bus\Settings\Base as BaseSettings;


class TripOffer extends DivForm
{
    private $_em;

    public function __construct($em, $post_data=null)
    {
        $this->_em = $em;

        $this->from_city = Text::init()
            ->add("class" , "validate")
            ->add("id" , "from_city")
            ->add("placeholder", "�����")
            ->add("label" , "�������� ��:")
            ->add("required", True);

        $this->to_city = Text::init()
            ->add("id", "to_city")
            ->add("class" , "validate")
            ->add("label" , "����������:")
            ->add("placeholder", "�����")
            ->add("required", True);

        $this->from_date = Text::init()
            ->add("class" , "datepicker")
            ->add("label", "���� �� ��������:")
            ->add("value" , date('Y-m-d', time()));

        $this->to_date = Text::init()
            ->add("class" , "datepicker")
            ->add("label", "���� �� ����������:")
            ->add("value" , date('Y-m-d', time()));

        $this->persons = Integer::init()
            ->add("class" , "validate")
            ->add("value" , 1)
            ->add("label", "���������:")
            ->add("placeholder" , 1);
        $this->persons->setStep(1);
        $this->persons->setMin(1);

        $this->childrens = Integer::init()
            ->add("value" , 1)
            ->add("label", "����:")
            ->add("class" , "validate");


        $this->childrens->setStep(1);
        $this->childrens->setMin(0);

        $this->twoway = Text::init()
            ->add("label", "���������� ��������")
            ->add("class" , "validate");

        $this->extras = Text::init()
            ->add("class" , "validate");

        $this->submit = Submit::init()
            ->add("class" , "btn waves-effect waves-light");

        $this->action = Hidden::init()
            ->add("value", "complete_order");

        parent::__construct($post_data);
    }

    #public function validate_twoway()
    #{
    #    if($this->twoway->value === "on"){
    #        $this->twoway->value = True;
    #    } else {
    #        $this->twoway->value = False;
    #    }
    #    return True;
    #}

    private function days()
    {
        $datediff = strtotime($_SESSION['rent']['to_date']) - strtotime($_SESSION['rent']['from_date']);
        return floor($datediff/(60*60*24));
    }


    public function can_send_request()
    {
        if(!isset($_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_ENQ])){
            $_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_ENQ] = 0;
        }

        return $_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_ENQ] < BaseSettings::APP_MAX_REQUESTS;
    }

    public function save()
    {
        if (!$this->can_send_request()) {
            throw new \Exception('���������� ��� ������������ �������� ���� ����������. ���� �������� ������ ����.'); 
        }

        $_SESSION[BaseSettings::APP_PREFIX] = array(BaseSettings::APP_SESSION_RENT => $this->clean_data());

        $extrasRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Extras');
        $extras = $extrasRepository->findBy(array("id" => $_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT]['extras']));
        $clientRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Client');
        $client = $clientRepository->findOneById($_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT]['id']);
        $order = new \Maksoft\Rent\Bus\Order();
        $order->setClient($client);
        foreach($extras as $extra){
            $order->addExtra($extra);
        }
        $order->setFromCity($_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT]['from_city']);
        $order->setDestination($_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT]['to_city']);
        $order->setTwoWay(0);
        $order->setDeparature($_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT]['from_date']);
        $order->setArrival($_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT]['to_date']);
        $order->setPersons($_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT]['persons']);
        $order->setChildrens($_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT]['childrens']);
        $order->setDays($this->days());
        $order->setCompleted(False);
        $this->_em->persist($order);
        $this->_em->flush();
        $id = $order->getId();
        $msg = \Maksoft\Rent\Message\Message::init("successfullOrderEmail", "email/successfull_order.html", $id, 30);
        $sender = new \Maksoft\Rent\Message\Pusher();
        $sender->connect();
        $sender->publish($msg);
        unset($_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_RENT]);
        if(!isset($_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_ENQ])){
            $_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_ENQ] = 1;
        } else {
            $_SESSION[BaseSettings::APP_PREFIX][BaseSettings::APP_SESSION_ENQ] += 1;
        }
        return array('text' => '������ ��������� ���� �������� �������. ���� ������� ������ �� �������� �����', 'code' => 'success');
    }
}
