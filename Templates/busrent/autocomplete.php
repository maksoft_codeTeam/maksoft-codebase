<?php

$language = "bg";
$auth_key = "AIzaSyChotvE05caXFYMTT9TsdnxScAd_tJkCus";
$input = $_GET['needle'];
$url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&types=(cities)&language=$language&key=$auth_key";
$response = file_get_contents($url);
header('Content-Type: application/json', 'charset=cp1251');
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
json_last_error(); 
echo json_encode($response);
