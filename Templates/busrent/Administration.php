<?php
$languageRepository = $em->getRepository('\Maksoft\Rent\Bus\Language');
$languages = $languageRepository->findAll();
$vehicleTypeForm = new \Maksoft\Rent\Bus\Form\Admin\VehicleType($_GET['language'], $em, $_POST);
$extrasForm = new \Maksoft\Rent\Bus\Form\Admin\ExtraDetail($_GET['language'], $_GET['vehicleType'], $_GET['category'], $em, $_POST);
$categoryForm = new \Maksoft\Rent\Bus\Form\Admin\Category($_GET['language'], $em, $_POST);
if($_SERVER["REQUEST_METHOD"] === "POST" and isset($_POST["action"])){
    switch($_POST['action']){
        case "insert_category":
            $categoryForm->is_valid();
            $categoryForm->save();
            break;
        case "insert_extra":
            $extrasForm->is_valid();
            break;
        case "type_insert":
            $vehicleTypeForm->is_valid();
            $vehicleTypeForm->save();
            break;
    }
}
?>

<div id="select_language" class="row text-center">
    <div class="col-md-12">
        <p>���� �������� ����:</p>

        <select id="#language_id" name="language">
<?php 
    $l_id = 38;
    if(isset($_GET['language']) and is_numeric($_GET['language'])){
        $l_id = $_GET["language"];
    }
    ?>
    <?php
    foreach($languages as $language){
?>
        <option value="<?=$language->getId();?>" <?php if($language->getId() == $l_id){ ?> selected <?php }?>><?=$language->getCountry()?></option>
<?php } ?>
        </select>
        <input type="submit" value="��������" onclick="setGetParameter('language', jQuery('select[name=language]').val())">
    </div>
</div>

<?php
if(isset($_GET['language']) and !isset($_GET['vehicleType'])){
    $types = $em->getRepository('\Maksoft\Rent\Bus\Type');
    $types = $types->findAll();

    ?> <div class="row"> <?
    echo "<h3> ������ �������� ��������: </h3>";
    foreach($types as $type){ ?>
        <div class="col-md-1">
            <button value="<?=$type->getId()?>" onclick="setGetParameter('vehicleType', $(this).val())">
                <?=$type->getName()?>
            </button>
        </div>
    <?php } ?>
    </div>
    <div class="row text-center">
        <p>�������� �� �������� ��������:</p>
        <div class="col-md-12">
            <?=$vehicleTypeForm?>
        </div>
    </div>
<? } else if(isset($_GET['language'], $_GET['vehicleType'])){
    $type = $em->getRepository('\Maksoft\Rent\Bus\Type');
    $type = $type->findOneBy(array("id"=>$_GET['vehicleType']));
    echo "<h4> �������� �� ������ �� �������� ��������:</h4> <h3>".$type->getName()."</h3>";
    $category = $em->getRepository('\Maksoft\Rent\Bus\Category');
    $categories = $category->findBy(array("language" => $_GET['language']));

    echo "<h3> �������� �� ��������� </h3>";
    foreach($categories as $category){?>
        <div class="col-md-1">
            <button value="<?=$category->getId()?>" onclick="setGetParameter('category', $(this).val())"> <?=$category->getName()?> </button>
        </div>
    <?}?>
    <div class="row text-center">
        <div class="col-md-12">
            <?=$categoryForm?>
        </div>
    </div>
    <div class="row text-center">
        <?php 
        if(isset($_GET['category']) and is_numeric($_GET['category'])){
            $category = $em->getRepository('\Maksoft\Rent\Bus\Category');
            $category = $category->findOneBy(array("id" => $_GET['category']));
            echo '<p>�������� �� ������ ��� ��������� <b>'.$category->getName().'</b></p>';
            ?>
            <div class="col-md-12">
                <?=$extrasForm?>
            </div>
        <?php } ?>
    </div>
    <?php
}


?>



<script>
function setGetParameter(paramName, paramValue)
{
    var url = window.location.href;
    var hash = location.hash;
    url = url.replace(hash, '');
    if (url.indexOf(paramName + "=") >= 0)
    {
        var prefix = url.substring(0, url.indexOf(paramName));
        var suffix = url.substring(url.indexOf(paramName));
        suffix = suffix.substring(suffix.indexOf("=") + 1);
        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
        url = prefix + paramName + "=" + paramValue + suffix;
    }
    else
    {
    if (url.indexOf("?") < 0)
        url += "?" + paramName + "=" + paramValue;
    else
        url += "&" + paramName + "=" + paramValue;
    }
    window.location.href = url + hash;
}

</script>
