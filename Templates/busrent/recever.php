<?php

require_once __DIR__ . '/bus_rent/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;

$durable = true;
$acknowledgment = false;
$queue_name = 'busrent';

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'mnk@22MNK');

$channel = $connection->channel();

$channel->queue_declare($queue_name, false, $durable, false, false);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$callback = function($msg){
  echo " [x] Received ", $msg->body, "\n";
  sleep(substr_count($msg->body, '.'));
  echo " [x] Done", "\n";
  $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

/*
 * This tells RabbitMQ not to give more than one message to a worker at a time.
 * Or, in other words, don't dispatch a new message to a worker until it has processed and acknowledged the previous one.
 * Instead, it will dispatch it to the next worker that is not still busy.
 */
$channel->basic_qos(null, 1, null);

$channel->basic_consume($queue_name, '', false, $acknowledgment, false, false, $callback);

while(count($channel->callbacks)) {
        $channel->wait();
};

$channel->close();
$connection->close();
?>
