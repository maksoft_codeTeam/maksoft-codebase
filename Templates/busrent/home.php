<?php
require_once __DIR__.'/forms/TripOffer.php';
$form = new TripOffer($em, $_POST);

?>
  <section class="hero-section">
    <div id="slider-revolution">
      <ul>
        <li data-slotamount="7" data-masterspeed="500" data-title="Slide title 1"><img src="<?=TEMPLATE_DIR?>assets/images/bg/1.jpg" data-bgposition="left center" data-duration="14000" data-bgpositionend="right center" alt="">
          <div class="tp-caption sfb fadeout slider-caption slider-caption-1" data-x="center" data-y="180" data-speed="700" data-easing="easeOutBack" data-start="2000">���-������� ������</div>
          <a href="#" class="tp-caption sfb fadeout awe-btn awe-btn-style3 awe-btn-slider" data-x="center" data-y="280" data-easing="easeOutBack" data-speed="700" data-start="2200">������� ������</a></li>
        <li data-slotamount="7" data-masterspeed="500" data-title="Slide title 2"><img src="<?=TEMPLATE_DIR?>assets/images/bg/2.jpg" data-bgposition="left center" data-duration="14000" data-bgpositionend="right center" alt="">
          <div class="tp-caption sft fadeout slider-caption-sub slider-caption-sub-2" data-x="center" data-y="120" data-speed="700" data-start="1500" data-easing="easeOutBack">������� ���������� ������ �� ���������</div>
          <div class="tp-caption sft fadeout slider-caption slider-caption-2" data-x="center" data-y="160" data-speed="700" data-easing="easeOutBack" data-start="2000">������� � ���</div>
          <a href="#" class="tp-caption sft fadeout awe-btn awe-btn-style3 awe-btn-slider" data-x="center" data-y="270" data-easing="easeOutBack" data-speed="700" data-start="2200">������� ������</a></li>
        <li data-slotamount="7" data-masterspeed="500" data-title="Slide title 3"><img src="<?=TEMPLATE_DIR?>assets/images/bg/3.jpg" data-bgposition="left center" data-duration="14000" data-bgpositionend="right center" alt="">
          <div class="tp-caption lfl fadeout slider-caption slider-caption-3" data-x="center" data-y="160" data-speed="700" data-easing="easeOutBack" data-start="1500">����� ������</div>
          <div href="#" class="tp-caption lfr fadeout slider-caption-sub slider-caption-sub-3" data-x="center" data-y="265" data-easing="easeOutBack" data-speed="700" data-start="2000">������� ����� ���� �� �����</div>
        </li>
      </ul>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="awe-search-tabs tabs">

        <ul>
          <li><a href="#awe-search-tabs-1"><i class="awe-icon awe-icon-bus"></i></a></li>
          <li><a href="#awe-search-tabs-2"><i class="awe-icon awe-icon-briefcase"></i></a></li>
        </ul>

        <div class="awe-search-tabs__content tabs__content">


          <div id="awe-search-tabs-1" class="search-bus">

            <h2>����� �������</h2>
            <?=$form?>
            <form>

              <div class="form-group">

                <div class="form-elements ui-widget">
                  <label for="from_city" >�������� ��</label>
                  <div class="form-item ui-widget"><i class="awe-icon awe-icon-marker-1"></i>
                    <input id="from_city" type="text" placeholder="����, �������...">
                  </div>
                </div>
                <div class="form-elements">
                  <label>���������� �</label>
                  <div class="form-item"><i class="awe-icon awe-icon-marker-1"></i>
                    <input id="to_city" type="text" placeholder="����, �������...">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-elements">
                  <label>���� �� ��������</label>
                  <div class="form-item"><i class="awe-icon awe-icon-calendar"></i>
                    <input name="from_date" id="#from_date" type="text" class="awe-calendar" value="����">
                  </div>
                </div>
                <div class="form-elements">
                  <label>���� �� �������</label>
                  <div class="form-item"><i class="awe-icon awe-icon-calendar"></i>
                    <input type="text" class="awe-calendar" value="����" name="to_date" id="to_date">
                  </div>
                </div>
                <div class="form-elements">
                  <label>���������</label>
                  <div class="form-item">
                    <input type="number"  value="1" step="1" name="persons" id="persons">
                  </div>
                </div>
                <div class="form-elements">
                  <label>����</label>
                  <div class="form-item">
                    <input type="number"  value="0" step="1" name="childrens" id="childrens">
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="������� ������">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
 
 <script>
var $ = jQuery;
$(document).ready(function(){
    $("#from_city").keyup(function(){
        return auto_complete($(this));
    });

    $("#to_city").keyup(function(){
         auto_complete($(this));
    });
});

function auto_complete(el) {
    var url = "/Templates/busrent/autocomplete.php";
    var el = el;
    if(el.val().length > 1){
        $.ajax({
            url: url, 
            type: "GET",   
            dataType: 'json',
            data: {needle: el.val()},
            cache: true,
            success: function(response){                          
                create_predictions(JSON.parse(response), "#"+el.attr('id'));
            }           
        });
    }
}

function create_predictions(data, selector) {
    var tags = [];
    var predictions = data.predictions;
    for (i in predictions) {
        var prediction = predictions[i];
        tags.push({
            label: prediction.description,
            value: prediction.description
        });
    }
    $(selector).autocomplete({
        source: tags
    });
}
 </script>
