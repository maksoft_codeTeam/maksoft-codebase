       <div class="sections">
        <section class="your-destinations-section-demo">
            <div class="container">
       <div class="destination-grid-content">
                        <div class="row">
                            <div class="awe-masonry">
                                <div class="awe-masonry__item">
                                    <a href="#">
                                        <div class="image-wrap image-cover"><img src="http://www.ipa.co.uk/write/images/IPA%20map.JPG" alt=""></div>
                                    </a>
                                    <div class="item-title">
                                        <h2><a href="#">���</a></h2>
                                        <div class="item-cat">
                                            <ul>
                                                <li><a href="#">������</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-available"><span class="count">35</span> ���������� ������</div>
                                </div>
                                <div class="awe-masonry__item">
                                    <a href="#">
                                        <div class="image-wrap image-cover"><img src="http://www.ipa.co.uk/write/images/IPA%20map.JPG" alt=""></div>
                                    </a>
                                    <div class="item-title">
                                        <h2><a href="#">�����</a></h2>
                                        <div class="item-cat">
                                            <ul>
                                                <li><a href="#">��������</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-available"><span class="count">4</span> ���������� ������</div>
                                </div>
                                <div class="awe-masonry__item">
                                    <a href="#">
                                        <div class="image-wrap image-cover"><img src="http://www.ipa.co.uk/write/images/IPA%20map.JPG" alt=""></div>
                                    </a>
                                    <div class="item-title">
                                        <h2><a href="#">�����</a></h2>
                                        <div class="item-cat">
                                            <ul>
                                                <li><a href="#">�������</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-available"><span class="count">0</span> ���������� ������</div>
                                </div>
                                
                                                                <div class="awe-masonry__item">
                                    <a href="#">
                                        <div class="image-wrap image-cover"><img src="http://www.ipa.co.uk/write/images/IPA%20map.JPG" alt=""></div>
                                    </a>
                                    <div class="item-title">
                                        <h2><a href="#">�����</a></h2>
                                        <div class="item-cat">
                                            <ul>
                                                <li><a href="#">�������</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-available"><span class="count">0</span> ���������� ������</div>
                                </div>
                                                                <div class="awe-masonry__item">
                                    <a href="#">
                                        <div class="image-wrap image-cover"><img src="http://www.ipa.co.uk/write/images/IPA%20map.JPG" alt=""></div>
                                    </a>
                                    <div class="item-title">
                                        <h2><a href="#">�����</a></h2>
                                        <div class="item-cat">
                                            <ul>
                                                <li><a href="#">�������</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-available"><span class="count">0</span> ���������� ������</div>
                                </div>
                               
                              
                            </div>
                        </div>

                    </div>
			</div>
</section>
       
        <section class="product-detail">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="product-detail__info">
                            <div class="product-title">
                                <h2>Qatar : Hanoi - NYC</h2></div>
                            <div class="product-address"><span>9579 Wishing Mount, Wynot, ND, US. | +1-888-8765-1234</span></div>
                            <div class="product-email"><i class="fa fa-envelope"></i> <a href="#">Send Email Inquiry</a></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="product-tabs tabs">
                            <ul>
                                <li><a href="#tabs-1">Initiative</a></li>
                                <li><a href="#tabs-2">Services on-flight</a></li>
                                <li><a href="#tabs-3">Good to know</a></li>
                                <li><a href="#tabs-4">Review &amp; rating</a></li>
                            </ul>
                            <div class="product-tabs__content">
                                <div id="tabs-1">
                                    <div class="initiative">
                                        <div class="initiative__item">
                                            <div class="initiative-top">
                                                <div class="title">
                                                    <div class="from-to"><span class="from">Ha Noi</span> <i class="awe-icon awe-icon-arrow-right"></i> <span class="to">New York</span></div>
                                                    <div class="time">Thursday 12 Feb 2015 | Total time: 33h 30m</div>
                                                </div>
                                                <div class="price"><span class="amount">$1200</span> <a href="sale-flight-popup.html" class="initiative-choose-other-open">Choose other</a></div>
                                            </div>
                                            <table class="initiative-table">
                                                <tbody>
                                                    <tr>
                                                        <th>
                                                            <div class="item-thumb">
                                                                <div class="image-thumb"><img src="images/flight/4.jpg" alt=""></div>
                                                                <div class="text"><span>Quatar Airway</span>
                                                                    <p>QR-829</p><span>Economy</span></div>
                                                            </div>
                                                        </th>
                                                        <td>
                                                            <div class="item-body">
                                                                <div class="item-from">
                                                                    <h3>HAN</h3><span class="time">14:15</span> <span class="date">Thu, 12 Feb, 2015</span>
                                                                    <p class="desc">John F Kennedy, New York</p>
                                                                </div>
                                                                <div class="item-time"><i class="fa fa-clock-o"></i> <span>10h 25m</span></div>
                                                                <div class="item-to">
                                                                    <h3>DOH</h3><span class="time">23:10</span> <span class="date">Thu, 12 Feb, 2015</span>
                                                                    <p class="desc">Doha, Doha</p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>
                                                            <div class="item-thumb">
                                                                <div class="image-thumb"><img src="images/flight/4.jpg" alt=""></div>
                                                                <div class="text"><span>Quatar Airway</span>
                                                                    <p>QR-829</p><span>Economy</span></div>
                                                            </div>
                                                        </th>
                                                        <td>
                                                            <div class="item-body">
                                                                <div class="item-from">
                                                                    <h3>HAN</h3><span class="time">14:15</span> <span class="date">Thu, 12 Feb, 2015</span>
                                                                    <p class="desc">John F Kennedy, New York</p>
                                                                </div>
                                                                <div class="item-time"><i class="fa fa-clock-o"></i> <span>10h 25m</span></div>
                                                                <div class="item-to">
                                                                    <h3>DOH</h3><span class="time">23:10</span> <span class="date">Thu, 12 Feb, 2015</span>
                                                                    <p class="desc">Doha, Doha</p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="initiative__item">
                                            <div class="initiative-top">
                                                <div class="title">
                                                    <div class="from-to"><span class="from">Ha Noi</span> <i class="awe-icon awe-icon-arrow-right"></i> <span class="to">New York</span></div>
                                                    <div class="time">Thursday 12 Feb 2015 | Total time: 33h 30m</div>
                                                </div>
                                                <div class="price"><span class="amount">$1200</span> <a href="sale-flight-popup.html" class="initiative-choose-other-open">Choose other</a></div>
                                            </div>
                                            <table class="initiative-table">
                                                <tbody>
                                                    <tr>
                                                        <th>
                                                            <div class="item-thumb">
                                                                <div class="image-thumb"><img src="images/flight/4.jpg" alt=""></div>
                                                                <div class="text"><span>Quatar Airway</span>
                                                                    <p>QR-829</p><span>Economy</span></div>
                                                            </div>
                                                        </th>
                                                        <td>
                                                            <div class="item-body">
                                                                <div class="item-from">
                                                                    <h3>HAN</h3><span class="time">14:15</span> <span class="date">Thu, 12 Feb, 2015</span>
                                                                    <p class="desc">John F Kennedy, New York</p>
                                                                </div>
                                                                <div class="item-time"><i class="fa fa-clock-o"></i> <span>10h 25m</span></div>
                                                                <div class="item-to">
                                                                    <h3>DOH</h3><span class="time">23:10</span> <span class="date">Thu, 12 Feb, 2015</span>
                                                                    <p class="desc">Doha, Doha</p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>
                                                            <div class="item-thumb">
                                                                <div class="image-thumb"><img src="images/flight/4.jpg" alt=""></div>
                                                                <div class="text"><span>Quatar Airway</span>
                                                                    <p>QR-829</p><span>Economy</span></div>
                                                            </div>
                                                        </th>
                                                        <td>
                                                            <div class="item-body">
                                                                <div class="item-from">
                                                                    <h3>HAN</h3><span class="time">14:15</span> <span class="date">Thu, 12 Feb, 2015</span>
                                                                    <p class="desc">John F Kennedy, New York</p>
                                                                </div>
                                                                <div class="item-time"><i class="fa fa-clock-o"></i> <span>10h 25m</span></div>
                                                                <div class="item-to">
                                                                    <h3>DOH</h3><span class="time">23:10</span> <span class="date">Thu, 12 Feb, 2015</span>
                                                                    <p class="desc">Doha, Doha</p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="tabs-2">
                                    <div class="services-on-flight">
                                        <div class="item">
                                            <label>
                                                <input type="checkbox" name="serviceitem"> <i class="awe-icon awe-icon-check"></i> <span>Breakfast , lunch, dinner</span></label>
                                        </div>
                                        <div class="item">
                                            <label>
                                                <input type="checkbox" name="serviceitem"> <i class="awe-icon awe-icon-check"></i> <span>Breakfast , lunch, dinner</span></label>
                                        </div>
                                        <div class="item">
                                            <label>
                                                <input type="checkbox" name="serviceitem"> <i class="awe-icon awe-icon-check"></i> <span>Breakfast , lunch, dinner</span></label>
                                        </div>
                                        <div class="item">
                                            <label>
                                                <input type="checkbox" name="serviceitem"> <i class="awe-icon awe-icon-check"></i> <span>Breakfast , lunch, dinner</span></label>
                                        </div>
                                        <div class="item">
                                            <label>
                                                <input type="checkbox" name="serviceitem"> <i class="awe-icon awe-icon-check"></i> <span>Breakfast , lunch, dinner</span></label>
                                        </div>
                                        <div class="item">
                                            <label>
                                                <input type="checkbox" name="serviceitem"> <i class="awe-icon awe-icon-check"></i> <span>Breakfast , lunch, dinner</span></label>
                                        </div>
                                    </div>
                                </div>
                                <div id="tabs-3">
                                    <table class="good-to-know-table">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    <p>Check in</p>
                                                </th>
                                                <td>
                                                    <p>From 15:00 hours</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Check out</p>
                                                </th>
                                                <td>
                                                    <p>Until 11:00 hours</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Cancellation / prepayment</p>
                                                </th>
                                                <td>
                                                    <p>Cancellation and prepayment policies vary according to room type. Please check the room conditions when selecting your room above.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Children and extra beds</p>
                                                </th>
                                                <td>
                                                    <p>The maximum number of children�s cots/cribs in a room is 1.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Internet</p>
                                                </th>
                                                <td>
                                                    <p>free! WiFi is available in all areas and is free of charge.</p>
                                                    <p><span class="light">Free</span>children under 2 years stay free of charge when using existing beds.</p>
                                                    <p><span class="light">Free</span>children under 2 years stay free of charge when using existing beds.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Pets</p>
                                                </th>
                                                <td>
                                                    <p>Pets are allowed. Charges may be applicable.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Groups</p>
                                                </th>
                                                <td>
                                                    <p>When booking for more than 11 persons, different policies and additional supplements may apply.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Accepted cards for payment</p>
                                                </th>
                                                <td>
                                                    <p><img src="images/paypal2.png" alt=""></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="tabs-4">
                                    <div id="reviews">
                                        <div class="rating-info">
                                            <div class="average-rating-review good"><span class="count">7.5</span> <em>Average rating</em> <span>Good</span></div>
                                            <ul class="rating-review">
                                                <li><em>Facility</em> <span>7.5</span></li>
                                                <li><em>Human</em> <span>9.0</span></li>
                                                <li><em>Service</em> <span>9.5</span></li>
                                                <li><em>Interesting</em> <span>8.7</span></li>
                                            </ul><a href="#" class="write-review">Write a review</a></div>
                                        <div id="add_review">
                                            <h3 class="comment-reply-title">Add a review</h3>
                                            <form>
                                                <div class="comment-form-author">
                                                    <label for="author">Name <span class="required">*</span></label>
                                                    <input id="author" type="text">
                                                </div>
                                                <div class="comment-form-email">
                                                    <label for="email">Email <span class="required">*</span></label>
                                                    <input id="email" type="text">
                                                </div>
                                                <div class="comment-form-rating">
                                                    <h4>Your Rating</h4>
                                                    <div class="comment-form-rating__content">
                                                        <div class="item facility">
                                                            <label>Facility</label>
                                                            <select class="awe-select">
                                                                <option>5.0</option>
                                                                <option>6.5</option>
                                                                <option>7.5</option>
                                                                <option>8.5</option>
                                                                <option>9.0</option>
                                                                <option>10</option>
                                                            </select>
                                                        </div>
                                                        <div class="item human">
                                                            <label>Human</label>
                                                            <select class="awe-select">
                                                                <option>5.0</option>
                                                                <option>6.5</option>
                                                                <option>7.5</option>
                                                                <option>8.5</option>
                                                                <option>9.0</option>
                                                                <option>10</option>
                                                            </select>
                                                        </div>
                                                        <div class="item service">
                                                            <label>Service</label>
                                                            <select class="awe-select">
                                                                <option>5.0</option>
                                                                <option>6.5</option>
                                                                <option>7.5</option>
                                                                <option>8.5</option>
                                                                <option>9.0</option>
                                                                <option>10</option>
                                                            </select>
                                                        </div>
                                                        <div class="item interesting">
                                                            <label>Interesting</label>
                                                            <select class="awe-select">
                                                                <option>5.0</option>
                                                                <option>6.5</option>
                                                                <option>7.5</option>
                                                                <option>8.5</option>
                                                                <option>9.0</option>
                                                                <option>10</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="comment-form-comment">
                                                    <label for="comment">Your Review</label>
                                                    <textarea id="comment"></textarea>
                                                </div>
                                                <div class="form-submit">
                                                    <input type="submit" class="submit" value="Submit">
                                                </div>
                                            </form>
                                        </div>
                                        <div id="comments">
                                            <ol class="commentlist">
                                                <li>
                                                    <div class="comment-box">
                                                        <div class="avatar"><img src="images/img/demo-thumb.jpg" alt=""></div>
                                                        <div class="comment-body">
                                                            <p class="meta"><strong>Nguyen Gallahendahry</strong> <span class="time">December 10, 2012</span></p>
                                                            <div class="description">
                                                                <p>Takes me back to my youth. I love the design of this soda machine. A bit pricy though..!</p>
                                                            </div>
                                                            <div class="rating-info">
                                                                <div class="average-rating-review good"><span class="count">7.5</span> <em>Average rating</em> <span>Good</span></div>
                                                                <ul class="rating-review">
                                                                    <li><em>Facility</em> <span>7.5</span></li>
                                                                    <li><em>Human</em> <span>9.0</span></li>
                                                                    <li><em>Service</em> <span>9.5</span></li>
                                                                    <li><em>Interesting</em> <span>8.7</span></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="comment-box">
                                                        <div class="avatar"><img src="images/img/demo-thumb.jpg" alt=""></div>
                                                        <div class="comment-body">
                                                            <p class="meta"><strong>James Bond not 007</strong> <span class="time">December 10, 2012</span></p>
                                                            <div class="description">
                                                                <p>Takes me back to my youth. I love the design of this soda machine. A bit pricy though..!</p>
                                                            </div>
                                                            <div class="rating-info">
                                                                <div class="average-rating-review good"><span class="count">7.5</span> <em>Average rating</em> <span>Good</span></div>
                                                                <ul class="rating-review">
                                                                    <li><em>Facility</em> <span>7.5</span></li>
                                                                    <li><em>Human</em> <span>9.0</span></li>
                                                                    <li><em>Service</em> <span>9.5</span></li>
                                                                    <li><em>Interesting</em> <span>8.7</span></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="comment-box">
                                                        <div class="avatar"><img src="images/img/demo-thumb.jpg" alt=""></div>
                                                        <div class="comment-body">
                                                            <p class="meta"><strong>Bratt not Pitt</strong> <span class="time">December 10, 2012</span></p>
                                                            <div class="description">
                                                                <p>Takes me back to my youth. I love the design of this soda machine. A bit pricy though..!</p>
                                                            </div>
                                                            <div class="rating-info">
                                                                <div class="average-rating-review fine"><span class="count">5.0</span> <em>Average rating</em> <span>Fine</span></div>
                                                                <ul class="rating-review">
                                                                    <li><em>Facility</em> <span>7.5</span></li>
                                                                    <li><em>Human</em> <span>9.0</span></li>
                                                                    <li><em>Service</em> <span>9.5</span></li>
                                                                    <li><em>Interesting</em> <span>8.7</span></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="detail-sidebar">
                            <div class="call-to-book"><i class="awe-icon awe-icon-phone"></i> <em>Call to book</em> <span>+1-888-8765-1234</span></div>
                            <div class="booking-info">
                                <h3>Booking info</h3>
                                <div class="form-group">
                                    <div class="form-elements form-adult">
                                        <label>Adult</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>0</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div><span>12 yo and above</span></div>
                                    <div class="form-elements form-kids">
                                        <label>Kids</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>0</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div><span>11 and under</span></div>
                                </div>
                                <div class="form-baggage-weight">
                                    <label>Extra baggage weight / person</label>
                                    <div class="form-item">
                                        <select class="awe-select">
                                            <option>15 kg - $20</option>
                                            <option>15 kg - $20</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <select class="awe-select">
                                            <option>25 kg - $40</option>
                                            <option>25 kg - $40</option>
                                        </select>
                                    </div><span>Cabin 7kg/person for free</span></div>
                                <div class="price"><em>Total for this booking</em> <span class="amount">$5,923</span></div>
                                <div class="form-submit">
                                    <div class="add-to-cart">
                                        <button type="submit"><i class="awe-icon awe-icon-cart"></i>Add this to Cart</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
                <section class="product-detail">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="product-detail__info">
                            <div class="product-title">
                                <h2>Hotel Carlifornia</h2>
                                <div class="hotel-star"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                            </div>
                            <div class="product-address"><span>9579 Wishing Mount, Wynot, ND, US. | +1-888-8765-1234</span></div>
                            <div class="product-email"><i class="fa fa-envelope"></i> <a href="#">Send Email Inquiry</a></div>
                            <div class="rating-trip-reviews">
                                <div class="item good"><span class="count">7.5</span>
                                    <h6>Average rating</h6>
                                    <p>Good</p>
                                </div>
                                <div class="item">
                                    <h6>TripAdvisor ></h6><img src="images/trips.png" alt=""></div>
                                <div class="item">
                                    <h6>Reviews</h6>
                                    <p>No review yet</p>
                                </div>
                            </div>
                            <div class="product-descriptions">
                                <p>Situated along the famous Hung Vuong Street, Souvenir Nha Trang Hotel is within a convenient 2-minute walk to the beautiful Nha Trang Beach. Free Wi-Fi access is available in the entire property and complimentary parking is provided on site.
                                    <br>
                                    <br>Fitted with tiled flooring, air-conditioned rooms are furnished with a wardrobe, a flat-screen cable TV, minibar and seating area. En suite bathrooms are equipped with a hairdryer, shower facilities, slippers and free toiletries.
                                    <br>
                                    <br>Souvenir Nha Trang Hotel operates a 24-hour front desk that can assist with luggage storage, currency exchange and laundry services. Guests may rent a bicycle/car to explore the area, while the tour desk can organise sightseeing and travel arrangements.</p>
                            </div>
                            <div class="property-highlights">
                                <h3>Property highlights</h3>
                                <div class="property-highlights__content">
                                    <div class="item"><i class="awe-icon awe-icon-unlock"></i> <span>Room service</span></div>
                                    <div class="item"><i class="awe-icon awe-icon-beds"></i> <span>Bunkbed available</span></div>
                                    <div class="item"><i class="awe-icon awe-icon-beds"></i> <span>Bunkbed available</span></div>
                                    <div class="item"><i class="awe-icon awe-icon-laundry"></i> <span>Laundry</span></div>
                                    <div class="item"><i class="awe-icon awe-icon-shower"></i> <span>Shower</span></div>
                                    <div class="item"><i class="awe-icon awe-icon-shower"></i> <span>Shower</span></div>
                                    <div class="item"><i class="awe-icon awe-icon-pool"></i> <span>Outside Pool</span></div>
                                    <div class="item"><i class="awe-icon awe-icon-meal"></i> <span>Room meal service</span></div>
                                    <div class="item"><i class="awe-icon awe-icon-meal"></i> <span>Room meal service</span></div>
                                    <div class="item"><i class="awe-icon awe-icon-key"></i> <span>High security</span></div>
                                    <div class="item"><i class="awe-icon awe-icon-tv"></i> <span>TV in room</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="product-detail__gallery">
                            <div class="product-slider-wrapper">
                                <div class="product-slider">
                                    <div class="item"><img src="images/img/1.jpg" alt=""></div>
                                    <div class="item"><img src="images/img/2.jpg" alt=""></div>
                                    <div class="item"><img src="images/img/3.jpg" alt=""></div>
                                    <div class="item"><img src="images/img/4.jpg" alt=""></div>
                                    <div class="item"><img src="images/img/5.jpg" alt=""></div>
                                    <div class="item"><img src="images/img/6.jpg" alt=""></div>
                                    <div class="item"><img src="images/img/7.jpg" alt=""></div>
                                    <div class="item"><img src="images/img/8.jpg" alt=""></div>
                                    <div class="item"><img src="images/img/9.jpg" alt=""></div>
                                </div>
                                <div class="product-slider-thumb-row">
                                    <div class="product-slider-thumb">
                                        <div class="item"><img src="images/img/demo-thumb-1.jpg" alt=""></div>
                                        <div class="item"><img src="images/img/demo-thumb-2.jpg" alt=""></div>
                                        <div class="item"><img src="images/img/demo-thumb-3.jpg" alt=""></div>
                                        <div class="item"><img src="images/img/demo-thumb-4.jpg" alt=""></div>
                                        <div class="item"><img src="images/img/demo-thumb-5.jpg" alt=""></div>
                                        <div class="item"><img src="images/img/demo-thumb-6.html" alt=""></div>
                                        <div class="item"><img src="images/img/demo-thumb-7.html" alt=""></div>
                                        <div class="item"><img src="images/img/demo-thumb-8.html" alt=""></div>
                                        <div class="item"><img src="images/img/demo-thumb-9.html" alt=""></div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-map">
                                <div data-latlong="21.036697, 105.834871"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="product-tabs tabs">
                            <ul>
                                <li><a href="#tabs-1">Room detail</a></li>
                                <li><a href="#tabs-2">Facilities &amp; freebies</a></li>
                                <li><a href="#tabs-3">Good to know</a></li>
                                <li><a href="#tabs-4">Review &amp; rating</a></li>
                            </ul>
                            <div class="product-tabs__content">
                                <div id="tabs-1">
                                    <div class="check-availability">
                                        <form>
                                            <div class="form-group">
                                                <div class="form-elements form-checkin">
                                                    <label>Check in</label>
                                                    <div class="form-item"><i class="awe-icon awe-icon-calendar"></i>
                                                        <input type="text" class="awe-calendar" value="Date">
                                                    </div>
                                                </div>
                                                <div class="form-elements form-checkout">
                                                    <label>Check out</label>
                                                    <div class="form-item"><i class="awe-icon awe-icon-calendar"></i>
                                                        <input type="text" class="awe-calendar" value="Date">
                                                    </div>
                                                </div>
                                                <div class="form-elements form-adult">
                                                    <label>Adult</label>
                                                    <div class="form-item">
                                                        <select class="awe-select">
                                                            <option>0</option>
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                        </select>
                                                    </div><span>12 yo and above</span></div>
                                                <div class="form-elements form-kids">
                                                    <label>Kids</label>
                                                    <div class="form-item">
                                                        <select class="awe-select">
                                                            <option>0</option>
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                        </select>
                                                    </div><span>0-11 yo</span></div>
                                                <div class="form-actions">
                                                    <input type="submit" value="CHECK AVAILABILITY" class="awe-btn awe-btn-style3">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <table class="room-type-table">
                                        <thead>
                                            <tr>
                                                <th class="room-type">Room type</th>
                                                <th class="room-people">Max</th>
                                                <th class="room-condition">Condition</th>
                                                <th class="room-price">Today price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="room-type">
                                                    <div class="room-thumb"><img src="images/thumb/1.jpg" alt=""></div>
                                                    <div class="room-title">
                                                        <h4>Standard Double Room</h4></div>
                                                    <div class="room-descriptions">
                                                        <p>Prices are per room
                                                            <br>Included: 5 % service charge, 10 % VAT</p>
                                                    </div>
                                                    <div class="room-type-footer"><i class="awe-icon awe-icon-gym"></i> <i class="awe-icon awe-icon-car"></i> <i class="awe-icon awe-icon-food"></i> <i class="awe-icon awe-icon-level"></i> <i class="awe-icon awe-icon-wifi"></i></div>
                                                </td>
                                                <td class="room-people"><i class="awe-icon awe-icon-users"></i></td>
                                                <td class="room-condition">
                                                    <ul class="list-condition">
                                                        <li>Non-refundable</li>
                                                        <li>Breakfast $ 190 per guest</li>
                                                    </ul>
                                                </td>
                                                <td class="room-price">
                                                    <div class="price"><span class="amount">$59</span> <em>2 room available</em> <a href="room-type-popup.html" class="full-price-open-popup">Full price</a></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="room-type">
                                                    <div class="room-thumb"><img src="images/thumb/2.jpg" alt=""></div>
                                                    <div class="room-title">
                                                        <h4>Standard Double Room</h4></div>
                                                    <div class="room-descriptions">
                                                        <p>Prices are per room
                                                            <br>Included: 5 % service charge, 10 % VAT</p>
                                                    </div>
                                                    <div class="room-type-footer"><i class="awe-icon awe-icon-gym"></i> <i class="awe-icon awe-icon-car"></i> <i class="awe-icon awe-icon-food"></i> <i class="awe-icon awe-icon-level"></i> <i class="awe-icon awe-icon-wifi"></i></div>
                                                </td>
                                                <td class="room-people"><i class="awe-icon awe-icon-user"></i></td>
                                                <td class="room-condition">
                                                    <ul class="list-condition">
                                                        <li>Non-refundable</li>
                                                        <li>Breakfast $ 190 per guest</li>
                                                    </ul>
                                                </td>
                                                <td class="room-price">
                                                    <div class="price"><span class="amount">$59</span> <em>2 room available</em> <a href="room-type-popup.html" class="full-price-open-popup">Full price</a></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="room-type">
                                                    <div class="room-thumb"><img src="images/thumb/3.jpg" alt=""></div>
                                                    <div class="room-title">
                                                        <h4>Victoria Suite</h4></div>
                                                    <div class="room-descriptions">
                                                        <p>Prices are per room
                                                            <br>Included: 5 % service charge, 10 % VAT</p>
                                                    </div>
                                                    <div class="room-type-footer"><i class="awe-icon awe-icon-gym"></i> <i class="awe-icon awe-icon-car"></i> <i class="awe-icon awe-icon-food"></i> <i class="awe-icon awe-icon-level"></i> <i class="awe-icon awe-icon-wifi"></i></div>
                                                </td>
                                                <td class="room-people"><i class="awe-icon awe-icon-users"></i></td>
                                                <td class="room-condition">
                                                    <ul class="list-condition">
                                                        <li>Non-refundable</li>
                                                        <li>Breakfast $ 190 per guest</li>
                                                    </ul>
                                                </td>
                                                <td class="room-price">
                                                    <div class="price"><span class="amount">$59</span> <em>2 room available</em> <a href="room-type-popup.html" class="full-price-open-popup">Full price</a></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="room-type">
                                                    <div class="room-thumb"><img src="images/thumb/4.jpg" alt=""></div>
                                                    <div class="room-title">
                                                        <h4>Standard Double Room</h4></div>
                                                    <div class="room-descriptions">
                                                        <p>Prices are per room
                                                            <br>Included: 5 % service charge, 10 % VAT</p>
                                                    </div>
                                                    <div class="room-type-footer"><i class="awe-icon awe-icon-gym"></i> <i class="awe-icon awe-icon-car"></i> <i class="awe-icon awe-icon-food"></i> <i class="awe-icon awe-icon-level"></i> <i class="awe-icon awe-icon-wifi"></i></div>
                                                </td>
                                                <td class="room-people"><i class="awe-icon awe-icon-user"></i></td>
                                                <td class="room-condition">
                                                    <ul class="list-condition">
                                                        <li>Non-refundable</li>
                                                        <li>Breakfast $ 190 per guest</li>
                                                    </ul>
                                                </td>
                                                <td class="room-price">
                                                    <div class="price"><span class="amount">$59</span> <em>unavailable</em></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="room-type">
                                                    <div class="room-thumb"><img src="images/thumb/4.jpg" alt=""></div>
                                                    <div class="room-title">
                                                        <h4>Standard Double Room</h4></div>
                                                    <div class="room-descriptions">
                                                        <p>Prices are per room
                                                            <br>Included: 5 % service charge, 10 % VAT</p>
                                                    </div>
                                                    <div class="room-type-footer"><i class="awe-icon awe-icon-gym"></i> <i class="awe-icon awe-icon-car"></i> <i class="awe-icon awe-icon-food"></i> <i class="awe-icon awe-icon-level"></i> <i class="awe-icon awe-icon-wifi"></i></div>
                                                </td>
                                                <td class="room-people"><i class="awe-icon awe-icon-user"></i></td>
                                                <td class="room-condition">
                                                    <ul class="list-condition">
                                                        <li>Non-refundable</li>
                                                        <li>Breakfast $ 190 per guest</li>
                                                    </ul>
                                                </td>
                                                <td class="room-price">
                                                    <div class="price"><span class="amount">$59</span> <em>unavailable</em></div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="tabs-2">
                                    <table class="facilities-freebies-table">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    <p>View</p>
                                                </th>
                                                <td>
                                                    <p>City view <em>Beach ( link or gallery)</em></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Activities</p>
                                                </th>
                                                <td>
                                                    <p>Billiards</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Media &amp; Technology</p>
                                                </th>
                                                <td>
                                                    <p>Flat-screen TV, Telephone</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Food &amp; Drink</p>
                                                </th>
                                                <td>
                                                    <p>Bar, Snack bar</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Internet</p>
                                                </th>
                                                <td>
                                                    <p>free! WiFi is available in all areas and is free of charge.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Parking</p>
                                                </th>
                                                <td>
                                                    <p>No parking available.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Services</p>
                                                </th>
                                                <td>
                                                    <p>Packed lunches, Vending machine (drinks), Vending machine (snacks), 24-hour front desk, Ticket service, Luggage storage, Lockers, Daily maid service, Business centre, Fax/photocopying, Wake Up Service/Alarm Clock</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>General</p>
                                                </th>
                                                <td>
                                                    <p>Newspapers, Safety deposit box, Non-smoking rooms, Family rooms, Lift, Soundproof rooms, Heating, Non-smoking throughout, Hardwood/Parquet floors, Soundproofing</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Languages spoken</p>
                                                </th>
                                                <td>
                                                    <p>English, German</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="tabs-3">
                                    <table class="good-to-know-table">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    <p>Check in</p>
                                                </th>
                                                <td>
                                                    <p>From 15:00 hours</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Check out</p>
                                                </th>
                                                <td>
                                                    <p>Until 11:00 hours</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Cancellation / prepayment</p>
                                                </th>
                                                <td>
                                                    <p>Cancellation and prepayment policies vary according to room type. Please check the room conditions when selecting your room above.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Children and extra beds</p>
                                                </th>
                                                <td>
                                                    <p>The maximum number of children�s cots/cribs in a room is 1.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Internet</p>
                                                </th>
                                                <td>
                                                    <p>free! WiFi is available in all areas and is free of charge.</p>
                                                    <p><span class="light">Free</span>children under 2 years stay free of charge when using existing beds.</p>
                                                    <p><span class="light">Free</span>children under 2 years stay free of charge when using existing beds.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Pets</p>
                                                </th>
                                                <td>
                                                    <p>Pets are allowed. Charges may be applicable.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Groups</p>
                                                </th>
                                                <td>
                                                    <p>When booking for more than 11 persons, different policies and additional supplements may apply.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p>Accepted cards for payment</p>
                                                </th>
                                                <td>
                                                    <p><img src="images/paypal2.png" alt=""></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="tabs-4">
                                    <div id="reviews">
                                        <div class="rating-info">
                                            <div class="average-rating-review good"><span class="count">7.5</span> <em>Average rating</em> <span>Good</span></div>
                                            <ul class="rating-review">
                                                <li><em>Facility</em> <span>7.5</span></li>
                                                <li><em>Human</em> <span>9.0</span></li>
                                                <li><em>Service</em> <span>9.5</span></li>
                                                <li><em>Interesting</em> <span>8.7</span></li>
                                            </ul><a href="#" class="write-review">Write a review</a></div>
                                        <div id="add_review">
                                            <h3 class="comment-reply-title">Add a review</h3>
                                            <form>
                                                <div class="comment-form-author">
                                                    <label for="author">Name <span class="required">*</span></label>
                                                    <input id="author" type="text">
                                                </div>
                                                <div class="comment-form-email">
                                                    <label for="email">Email <span class="required">*</span></label>
                                                    <input id="email" type="text">
                                                </div>
                                                <div class="comment-form-rating">
                                                    <h4>Your Rating</h4>
                                                    <div class="comment-form-rating__content">
                                                        <div class="item facility">
                                                            <label>Facility</label>
                                                            <select class="awe-select">
                                                                <option>5.0</option>
                                                                <option>6.5</option>
                                                                <option>7.5</option>
                                                                <option>8.5</option>
                                                                <option>9.0</option>
                                                                <option>10</option>
                                                            </select>
                                                        </div>
                                                        <div class="item human">
                                                            <label>Human</label>
                                                            <select class="awe-select">
                                                                <option>5.0</option>
                                                                <option>6.5</option>
                                                                <option>7.5</option>
                                                                <option>8.5</option>
                                                                <option>9.0</option>
                                                                <option>10</option>
                                                            </select>
                                                        </div>
                                                        <div class="item service">
                                                            <label>Service</label>
                                                            <select class="awe-select">
                                                                <option>5.0</option>
                                                                <option>6.5</option>
                                                                <option>7.5</option>
                                                                <option>8.5</option>
                                                                <option>9.0</option>
                                                                <option>10</option>
                                                            </select>
                                                        </div>
                                                        <div class="item interesting">
                                                            <label>Interesting</label>
                                                            <select class="awe-select">
                                                                <option>5.0</option>
                                                                <option>6.5</option>
                                                                <option>7.5</option>
                                                                <option>8.5</option>
                                                                <option>9.0</option>
                                                                <option>10</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="comment-form-comment">
                                                    <label for="comment">Your Review</label>
                                                    <textarea id="comment"></textarea>
                                                </div>
                                                <div class="form-submit">
                                                    <input type="submit" class="submit" value="Submit">
                                                </div>
                                            </form>
                                        </div>
                                        <div id="comments">
                                            <ol class="commentlist">
                                                <li>
                                                    <div class="comment-box">
                                                        <div class="avatar"><img src="images/img/demo-thumb.jpg" alt=""></div>
                                                        <div class="comment-body">
                                                            <p class="meta"><strong>Nguyen Gallahendahry</strong> <span class="time">December 10, 2012</span></p>
                                                            <div class="description">
                                                                <p>Takes me back to my youth. I love the design of this soda machine. A bit pricy though..!</p>
                                                            </div>
                                                            <div class="rating-info">
                                                                <div class="average-rating-review good"><span class="count">7.5</span> <em>Average rating</em> <span>Good</span></div>
                                                                <ul class="rating-review">
                                                                    <li><em>Facility</em> <span>7.5</span></li>
                                                                    <li><em>Human</em> <span>9.0</span></li>
                                                                    <li><em>Service</em> <span>9.5</span></li>
                                                                    <li><em>Interesting</em> <span>8.7</span></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="comment-box">
                                                        <div class="avatar"><img src="images/img/demo-thumb.jpg" alt=""></div>
                                                        <div class="comment-body">
                                                            <p class="meta"><strong>James Bond not 007</strong> <span class="time">December 10, 2012</span></p>
                                                            <div class="description">
                                                                <p>Takes me back to my youth. I love the design of this soda machine. A bit pricy though..!</p>
                                                            </div>
                                                            <div class="rating-info">
                                                                <div class="average-rating-review good"><span class="count">7.5</span> <em>Average rating</em> <span>Good</span></div>
                                                                <ul class="rating-review">
                                                                    <li><em>Facility</em> <span>7.5</span></li>
                                                                    <li><em>Human</em> <span>9.0</span></li>
                                                                    <li><em>Service</em> <span>9.5</span></li>
                                                                    <li><em>Interesting</em> <span>8.7</span></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="comment-box">
                                                        <div class="avatar"><img src="images/img/demo-thumb.jpg" alt=""></div>
                                                        <div class="comment-body">
                                                            <p class="meta"><strong>Bratt not Pitt</strong> <span class="time">December 10, 2012</span></p>
                                                            <div class="description">
                                                                <p>Takes me back to my youth. I love the design of this soda machine. A bit pricy though..!</p>
                                                            </div>
                                                            <div class="rating-info">
                                                                <div class="average-rating-review fine"><span class="count">5.0</span> <em>Average rating</em> <span>Fine</span></div>
                                                                <ul class="rating-review">
                                                                    <li><em>Facility</em> <span>7.5</span></li>
                                                                    <li><em>Human</em> <span>9.0</span></li>
                                                                    <li><em>Service</em> <span>9.5</span></li>
                                                                    <li><em>Interesting</em> <span>8.7</span></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="detail-sidebar">
                            <div class="call-to-book"><i class="awe-icon awe-icon-phone"></i> <em>Call to book</em> <span>+1-888-8765-1234</span></div>
                            <div class="booking-info">
                                <h3>Booking info</h3>
                                <div class="form-group">
                                    <div class="form-elements form-checkin">
                                        <label>Check in</label>
                                        <div class="form-item">
                                            <input type="text" class="awe-calendar" value="Date">
                                        </div>
                                    </div>
                                    <div class="form-elements form-checkout">
                                        <label>Check out</label>
                                        <div class="form-item">
                                            <input type="text" class="awe-calendar" value="Date">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-elements form-room">
                                    <label>Room detail</label>
                                    <div class="form-group">
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>R-type</option>
                                                <option>Std - $59/n</option>
                                            </select>
                                        </div>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>Number</option>
                                                <option>1</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>R-type</option>
                                                <option>Std - $59/n</option>
                                            </select>
                                        </div>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>Number</option>
                                                <option>1</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="add-room-type"><a href="#"><i class="awe-icon awe-icon-plus"></i>Add More Room type</a></div>
                                </div>
                                <div class="price"><em>Total for this booking</em> <span class="amount">$5,923</span></div>
                                <div class="form-submit">
                                    <div class="add-to-cart">
                                        <button type="submit"><i class="awe-icon awe-icon-cart"></i>Add this to Cart</button>
                                    </div>
                                </div>
                            </div>
                            <div class="booking-info">
                                <h3>Booking info</h3>
                                <div class="form-group">
                                    <div class="form-elements form-checkin">
                                        <label>Check in</label>
                                        <div class="form-item">
                                            <input type="text" class="awe-calendar" value="Date">
                                        </div>
                                    </div>
                                    <div class="form-elements form-checkout">
                                        <label>Check out</label>
                                        <div class="form-item">
                                            <input type="text" class="awe-calendar" value="Date">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-elements form-room">
                                    <label>Room detail</label>
                                    <div class="form-group">
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>R-type</option>
                                                <option>Std - $59/n</option>
                                            </select>
                                        </div>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>Number</option>
                                                <option>1</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>R-type</option>
                                                <option>Std - $59/n</option>
                                            </select>
                                        </div>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>Number</option>
                                                <option>1</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="add-room-type"><a href="#"><i class="awe-icon awe-icon-plus"></i>Add More Room type</a></div>
                                </div>
                                <div class="price"><em>Total for this booking</em> <span class="amount">$5,923</span>
                                    <div class="cart-added"><i class="awe-icon awe-icon-check"></i> Added</div>
                                </div>
                                <div class="reset"><a href="#">Reset</a></div>
                                <div class="form-submit">
                                    <div class="add-to-cart">
                                        <button type="submit"><i class="awe-icon awe-icon-cart"></i>Add this to Cart</button>
                                    </div>
                                    <div class="view-cart"><a href="#">View cart</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</div>
<!--
  <section class="pt-80 pb-80 bg-border">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h1>H1 Heading 1</h1>
          <p>Donec pharetra ipsum sed lacinia malesuada. Integer tristique sapien et ligula porttitor egestas. Nam nec mauris volutpat, euismod massa ac, fermentum eros. Sed in lorem sed metus dapibus malesuada sit amet eget justo. In sollicitudin aliquam diam eget maximus. Sed auctor ultrices mi quis mattis. Nam porta iaculis eros, eu suscipit arcu rutrum sed. Curabitur at urna efficitur, malesuada mi non, auctor dui.</p>
          <h2>H2 Heading 2</h2>
          <p>Donec pharetra ipsum sed lacinia malesuada. Integer tristique sapien et ligula porttitor egestas. Nam nec mauris volutpat, euismod massa ac, fermentum eros. Sed in lorem sed metus dapibus malesuada sit amet eget justo. In sollicitudin aliquam diam eget maximus. Sed auctor ultrices mi quis mattis. Nam porta iaculis eros, eu suscipit arcu rutrum sed. Curabitur at urna efficitur, malesuada mi non, auctor dui.</p>
          <h3>H3 Heading 3</h3>
          <p>Donec pharetra ipsum sed lacinia malesuada. Integer tristique sapien et ligula porttitor egestas. Nam nec mauris volutpat, euismod massa ac, fermentum eros. Sed in lorem sed metus dapibus malesuada sit amet eget justo. In sollicitudin aliquam diam eget maximus. Sed auctor ultrices mi quis mattis. Nam porta iaculis eros, eu suscipit arcu rutrum sed. Curabitur at urna efficitur, malesuada mi non, auctor dui.</p>
          <h4>H4 Heading 4</h4>
          <p>Donec pharetra ipsum sed lacinia malesuada. Integer tristique sapien et ligula porttitor egestas. Nam nec mauris volutpat, euismod massa ac, fermentum eros. Sed in lorem sed metus dapibus malesuada sit amet eget justo. In sollicitudin aliquam diam eget maximus. Sed auctor ultrices mi quis mattis. Nam porta iaculis eros, eu suscipit arcu rutrum sed. Curabitur at urna efficitur, malesuada mi non, auctor dui.</p>
          <h5>H5 Heading 5</h5>
          <p>Donec pharetra ipsum sed lacinia malesuada. Integer tristique sapien et ligula porttitor egestas. Nam nec mauris volutpat, euismod massa ac, fermentum eros. Sed in lorem sed metus dapibus malesuada sit amet eget justo. In sollicitudin aliquam diam eget maximus. Sed auctor ultrices mi quis mattis. Nam porta iaculis eros, eu suscipit arcu rutrum sed. Curabitur at urna efficitur, malesuada mi non, auctor dui.</p>
          <h6>H6 Heading 6</h6>
          <p>Donec pharetra ipsum sed lacinia malesuada. Integer tristique sapien et ligula porttitor egestas. Nam nec mauris volutpat, euismod massa ac, fermentum eros. Sed in lorem sed metus dapibus malesuada sit amet eget justo. In sollicitudin aliquam diam eget maximus. Sed auctor ultrices mi quis mattis. Nam porta iaculis eros, eu suscipit arcu rutrum sed. Curabitur at urna efficitur, malesuada mi non, auctor dui.</p>
        </div>
      </div>
    </div>
  </section>
  <section class="pt-80 pb-80 bg-border">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <p>Fusce congue <del>convallis malesuada</del>. Etiam ut rutrum augue.
            <mark>Duis consequat nibh vitae</mark>
            odio dapibus blandit. <a href="#">Phasellus elementum nibh</a> sit amet pharetra tincidunt. Morbi dapibus vehicula diam a accumsan. Vestibulum consectetur est aliquet, <ins>rutrum diam et, luctus leo.</ins> Quisque semper volutpat pellentesque. Cras pretium nisl a sem convallis, id fringilla nisl lobortis. Proin eu sapien augue. Sed congue nunc non massa dignissim, vel ullamcorper dui fermentum. Suspendisse in gravida augue. <abbr>Sed congue nunc non massa dignissim, vel ullamcorper dui fermentum.</abbr> Suspendisse in gravida augue. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus non efficitur ex. Aliquam ultricies tincidunt eleifend. pre, <dfn>porttitor iaculis felis</dfn>. Cras sed felis vel purus interdum semper.</p>
        </div>
      </div>
    </div>
  </section>
  <section class="pt-80 pb-80 bg-border">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h5 class="text-uppercase">Blockquote</h5>
          <blockquote>
            <p>Donec pharetra ipsum sed lacinia malesuada. Integer tristique sapien et ligula porttitor egestas. Nam nec mauris volutpat, euismod massa ac, fermentum eros. Sed in lorem sed metus dapibus malesuada sit amet eget justo. In sollicitudin aliquam diam eget maximus. Sed auctor ultrices mi quis mattis. Nam porta iaculis eros, eu suscipit arcu rutrum sed. Curabitur at urna efficitur, malesuada mi non, auctor dui.</p>
            <footer>Awethemes</footer>
          </blockquote>
        </div>
      </div>
    </div>
  </section>
  <section class="pt-80 pb-80 bg-border">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h5 class="text-uppercase">Preformated Text</h5>
          <pre>Nunc nec blandit turpis. Nulla pharetra sit amet mauris ac facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac vestibulum magna. Praesent sed neque ac tellus ullamcorper malesuada non a diam. Nulla metus lectus, imperdiet at quam ac</pre>
        </div>
      </div>
    </div>
  </section>
  <section class="pt-80 pb-80 bg-border">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h5 class="text-uppercase">Dropcap</h5>
          <p><span class="dropcap">N</span>unc nec blandit turpis. Nulla pharetra sit amet mauris ac facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac vestibulum magna. Praesent sed neque ac tellus ullamcorper malesuada non a diam. Nulla metus lectus, imperdiet at quam ac, fringilla pharetra enim. Donec faucibus sollicitudin mi. Phasellus tempus diam a dolor lacinia, vitae ultricies sem eleifend. Mauris mollis dolor vel metus aliquam, lobortis congue leo rhoncus. Ut vel ligula pulvinar, tempor tortor at, tempus diam. Fusce in leo sed enim dictum pellentesque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed metus ex, semper eu magna at, pharetra lacinia odio.</p>
        </div>
      </div>
    </div>
  </section>
  <section class="pt-80 pb-80 bg-border">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h5 class="text-uppercase">Ordered Lists</h5>
          <ol class="list">
            <li>Nulla pharetra sit amet mauris ac facilisis. Lorem ipsum dolor.</li>
            <li>Nulla pharetra sit amet mauris ac facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
            <li>Nulla pharetra sit amet mauris ac facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
            <li>Nulla pharetra sit amet mauris ac facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
          </ol>
        </div>
        <div class="col-md-6">
          <h5 class="text-uppercase">Unordered Lists</h5>
          <ul class="list">
            <li>Nulla pharetra sit amet mauris ac facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
            <li>Nulla pharetra sit amet mauris ac facilisis. Lorem ipsum dolor.</li>
            <li>Nulla pharetra sit amet mauris ac facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
            <li>Nulla pharetra sit amet mauris ac facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section class="pt-80 pb-80 bg-border">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h5 class="text-uppercase">Tabs</h5>
          <div class="tabs">
            <ul>
              <li><a href="#tabs-1">Tabs 1</a></li>
              <li><a href="#tabs-2">Tabs 2</a></li>
              <li><a href="#tabs-3">Tabs 3</a></li>
            </ul>
            <div class="tabs-content">
              <div id="tabs-1">
                <p>Donec auctor sem eros, feugiat consectetur diam varius sit amet. Ut dapibus ante vel consectetur aliquam. Etiam lacus dolor, porta non tortor ac, elementum pretium nisl. Donec pharetra purus bibendum arcu dictum rutrum. Donec consectetur tortor at ligula luctus imperdiet. Phasellus vel nisi ligula. Duis accumsan, quam id rutrum aliquam, lorem eros consequat lectus, non fringilla risus mauris vel ex. Aliquam sollicitudin ipsum ex, sit amet eleifend arcu consectetur eu. Ut nec pulvinar quam, non cursus justo. Nunc at facilisis turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer quis eros ultrices magna porttitor faucibus at eu dolor.</p>
              </div>
              <div id="tabs-2">
                <p>Donec auctor sem eros, feugiat consectetur diam varius sit amet. Ut dapibus ante vel consectetur aliquam. Etiam lacus dolor, porta non tortor ac, elementum pretium nisl. Donec pharetra purus bibendum arcu dictum rutrum. Donec consectetur tortor at ligula luctus imperdiet. Phasellus vel nisi ligula. Duis accumsan, quam id rutrum aliquam, lorem eros consequat lectus, non fringilla risus mauris vel ex. Aliquam sollicitudin ipsum ex, sit amet eleifend arcu consectetur eu. Ut nec pulvinar quam, non cursus justo. Nunc at facilisis turpis.</p>
              </div>
              <div id="tabs-3">
                <p>Donec auctor sem eros, feugiat consectetur diam varius sit amet. Ut dapibus ante vel consectetur aliquam. Etiam lacus dolor, porta non tortor ac, elementum pretium nisl. Donec pharetra purus bibendum arcu dictum rutrum. Donec consectetur tortor at ligula luctus imperdiet. Phasellus vel nisi ligula. Duis accumsan, quam id rutrum aliquam, lorem eros consequat lectus, non fringilla risus mauris vel ex. Aliquam sollicitudin ipsum ex, sit amet eleifend arcu consectetur eu. Ut nec pulvinar quam, non cursus justo. Nunc at facilisis turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer quis eros ultrices magna porttitor faucibus at eu dolor.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <h5 class="text-uppercase">Accordion</h5>
          <div class="accordion">
            <h3>Day 1 : Downtown Tour</h3>
            <div>
              <p>Get on and go places! If you want to hit the most-visited spots in New York City, this is the one: CitySights NY�s Downtown Tour. You�ll get easy access to the most popular sites in town: the Statue of Liberty, the Empire State Building and �Ground Zero,� where the World Trade Center once stood and where a stunning new skyscraper soars today.</p>
            </div>
            <h3>Day 2 : Uptown Treasures &amp; Harlem Tour</h3>
            <div>
              <p>Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.</p>
            </div>
            <h3>Day 3 : Chinatown</h3>
            <div>
              <p>Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</p>
              <ul>
                <li>List item one</li>
                <li>List item two</li>
                <li>List item three</li>
              </ul>
            </div>
            <h3>Day 4 : Parks &amp; Bridges</h3>
            <div>
              <p>Cras dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia mauris vel est.</p>
              <p>Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="pt-80 pb-80 bg-border">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h5 class="text-uppercase">Map</h5>
          <div class="map-demo">
            <div data-latlong="21.036697, 105.834871"></div>
          </div>
        </div>
        <div class="col-md-6">
          <h5 class="text-uppercase">Contact Form</h5>
          <form class="contact-form" action="http://envato.megadrupal.com/html/gofar/processContact.php" method="post">
            <div class="form-item">
              <input type="text" value="Your Name *" name="name">
            </div>
            <div class="form-item">
              <input type="email" value="Your Email *" name="email">
            </div>
            <div class="form-textarea-wrapper">
              <textarea name="message">Company name</textarea>
            </div>
            <div class="form-actions">
              <input type="submit" value="Send" class="submit-contact">
            </div>
            <div id="contact-content"></div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class="masonry-section-demo">
    <div class="container">
      <div class="destination-grid-content">
        <div class="section-title">
          <h5 class="text-uppercase">Gallery</h5>
        </div>
        <div class="row">
          <div class="awe-masonry">
            <div class="awe-masonry__item"><a href="#">
              <div class="image-wrap image-cover"><img src="images/img/1.jpg" alt=""></div>
              </a>
              <div class="item-title">
                <h2><a href="#">Florenze</a></h2>
                <div class="item-cat">
                  <ul>
                    <li><a href="#">Italy</a></li>
                  </ul>
                </div>
              </div>
              <div class="item-available"><span class="count">845</span> available hotel</div>
            </div>
            <div class="awe-masonry__item"><a href="#">
              <div class="image-wrap image-cover"><img src="images/img/2.jpg" alt=""></div>
              </a>
              <div class="item-title">
                <h2><a href="#">Toluca</a></h2>
                <div class="item-cat">
                  <ul>
                    <li><a href="#">USA</a></li>
                  </ul>
                </div>
              </div>
              <div class="item-available"><span class="count">132</span> available hotel</div>
            </div>
            <div class="awe-masonry__item"><a href="#">
              <div class="image-wrap image-cover"><img src="images/img/3.jpg" alt=""></div>
              </a>
              <div class="item-title">
                <h2><a href="#">Venice</a></h2>
                <div class="item-cat">

                  <ul>
                    <li><a href="#">Italy</a></li>
                  </ul>
                </div>
              </div>
              <div class="item-available"><span class="count">2341</span> available hotel</div>
            </div>
            <div class="awe-masonry__item"><a href="#">
              <div class="image-wrap image-cover"><img src="images/img/4.jpg" alt=""></div>
              </a>
              <div class="item-title">
                <h2><a href="#">Ohio</a></h2>
                <div class="item-cat">
                  <ul>
                    <li><a href="#">USA</a></li>
                  </ul>
                </div>
              </div>
              <div class="item-available"><span class="count">2531</span> available hotel</div>
            </div>
            <div class="awe-masonry__item"><a href="#">
              <div class="image-wrap image-cover"><img src="images/img/5.jpg" alt=""></div>
              </a>
              <div class="item-title">
                <h2><a href="#">Venice</a></h2>
                <div class="item-cat">
                  <ul>
                    <li><a href="#">Italy</a></li>
                  </ul>
                </div>
              </div>
              <div class="item-available"><span class="count">2531</span> available hotel</div>
            </div>
            <div class="awe-masonry__item"><a href="#">
              <div class="image-wrap image-cover"><img src="images/img/6.jpg" alt=""></div>
              </a>
              <div class="item-title">
                <h2><a href="#">Mandives</a></h2>
                <div class="item-cat">
                  <ul>
                    <li><a href="#">Mandives</a></li>
                  </ul>
                </div>
              </div>
              <div class="item-available"><span class="count">2531</span> available hotel</div>
            </div>
            <div class="awe-masonry__item"><a href="#">
              <div class="image-wrap image-cover"><img src="images/img/7.jpg" alt=""></div>
              </a>
              <div class="item-title">
                <h2><a href="#">Istanbul</a></h2>
                <div class="item-cat">
                  <ul>
                    <li><a href="#">Turkey</a></li>
                  </ul>
                </div>
              </div>
              <div class="item-available"><span class="count">2531</span> available hotel</div>
            </div>
            <div class="awe-masonry__item"><a href="#">
              <div class="image-wrap image-cover"><img src="images/img/8.jpg" alt=""></div>
              </a>
              <div class="item-title">
                <h2><a href="#">Bali</a></h2>
                <div class="item-cat">
                  <ul>
                    <li><a href="#">Thailand</a></li>
                  </ul>
                </div>
              </div>
              <div class="item-available"><span class="count">2531</span> available hotel</div>
            </div>
            <div class="awe-masonry__item"><a href="#">
              <div class="image-wrap image-cover"><img src="images/img/9.jpg" alt=""></div>
              </a>
              <div class="item-title">
                <h2><a href="#">Phu Quoc</a></h2>
                <div class="item-cat">
                  <ul>
                    <li><a href="#">Vietnam</a></li>
                  </ul>
                </div>
              </div>
              <div class="item-available"><span class="count">2531</span> available hotel</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  -->
