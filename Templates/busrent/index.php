<?php
use Symfony\Component\HttpFoundation\Request;
define( "TEMPLATE_NAME", "busrent" );
define( "TEMPLATE_DIR", "Templates/" . TEMPLATE_NAME . "/" );
define( "TEMPLATE_IMAGES", "/" . TEMPLATE_DIR . "images/" );
$o_page->debug($_SESSION);

$em = $entityManager = $settings->getEntityManager();

$request = Request::createFromGlobals();
$route = new Maksoft\Rent\Bus\Controller\Router($request, $twig, $em, $o_page);

$pTemplate = $o_page->get_pTemplate( $n, 289 );
$logged = $route->is_logged();

include TEMPLATE_DIR . "header.php";

if ( $o_page->_page[ 'SiteID' ] != $o_site->_site[ 'SitesID' ] ){
    # тук трябва да се Inkludne admin.php
	include TEMPLATE_DIR . "main.php";
}elseif ( $o_page->_page[ 'n' ] == $o_site->_site[ 'StartPage' ] && strlen( $search_tag ) < 3 && strlen( $search ) < 3 ){
    require_once __DIR__.'/home.php';
}else {
	if ( $pTemplate[ 'pt_url' ] )
		include $pTemplate[ 'pt_url' ];
	else
        echo $response;
		include TEMPLATE_DIR . "main.php";
}

if($user->ID > 0){ ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center text-sm-left">
                <ol class="breadcrumb">
                    <li><?="".$nav_bar.""?></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<?php } ?>
  <br><br>  <br><br>  <br><br>
<?php include TEMPLATE_DIR . "footer.php"; ?>
