<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="cp1251">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<title><?=$Title?></title>
<?php
require_once "lib/lib_page.php";
require_once "lib/Database.class.php";
require_once "Templates/meta_tags.php";

$o_site->print_sConfigurations();

?>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="google-site-verification" content="L8LPKQKr2d813bXlOczGg_ZZtPXlORpuc3CfP4uNZ6s" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:700,600,400,300" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Oswald:400" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>assets/css/lib/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>assets/css/lib/awe-booking-font.css">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>assets/css/lib/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>assets/css/lib/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>assets/css/lib/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>assets/revslider/css/settings.css">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>assets/css/demo.css">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>assets/Gallery/css/blueimp-gallery.min.css">
<link id="colorreplace" rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>assets/css/colors/blue.css">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->

</head>
<!--[if IE 7]><body class="ie7 lt-ie8 lt-ie9 lt-ie10"><![endif]--><!--[if IE 8]><body class="ie8 lt-ie9 lt-ie10"><![endif]--><!--[if IE 9]><body class="ie9 lt-ie10"><![endif]--><!--[if (gt IE 9)|!(IE)]><!-->
<body>
<!--<![endif]-->
<div id="page-wrap">
  <div class="preloader"></div>
  <header id="header-page">
    <div class="header-page__inner">
      <div class="container">
        <div class="logo"><a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>"><img src="<?=TEMPLATE_DIR?>assets/images/logo-busrent.png" width="150px" alt=""></a></div>
        <nav class="navigation awe-navigation" data-responsive="1200">
          <ul class="menu-list">
            <li class="menu-item-has-children current-menu-parent"><a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>">������</a>

            </li>
<?php

if (!isset($dd_links)) {
    $dd_links = 2;
}

$dd_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $dd_links");

for ($i = 0; $i < count($dd_links); $i++) {
    $subpages = $o_page->get_pSubpages($dd_links[$i]['n']);
?>
<li <?php echo ((count($subpages) > 0) ? "class=\"menu-item-has-children current-menu-parent\"" : "") ?>>
    <a href="<?php echo $o_page->get_pLink($dd_links[$i]['n']) ?>" <?php echo ((count($subpages) > 0) ? "" : "") ?>>
            <?php echo $dd_links[$i]['Name'] ?></a><?php echo ((count($subpages) > 0) ? "<ul class=\"sub-menu\">" : "</li>") ?> <?php
    if (count($subpages) > 0) {
        for ($j = 0; $j < count($subpages); $j++)
            echo "<li><a href=\"" . $o_page->get_pLink($subpages[$j]['n']) . "\">" . $subpages[$j]['Name'] . "</a></li>";
            echo '</ul></li>';
    }
}
?>
          </ul>
        </nav>
        <div class="search-box"><span class="searchtoggle"><i class="awe-icon awe-icon-search"></i></span>
          <form class="form-search">
            <div class="form-item">
              <input type="text" value="����� � ����� &amp; ������� Enter">
            </div>
          </form>
        </div>
        <a class="toggle-menu-responsive" href="#">
        <div class="hamburger"><span class="item item-1"></span> <span class="item item-2"></span> <span class="item item-3"></span></div>
        </a></div>
    </div>
  </header>
