<?php
require_once __DIR__."/bus_rent/vendor/autoload.php";
#use PhpAmqpLib\Connection\AMQPStreamConnection;
#use PhpAmqpLib\Message\AMQPMessage;
#$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'mnk@22MNK');
#$channel = $connection->channel();
#$durable = True;
#$queue_name = "busrent";
#$channel->queue_declare($queue_name, false, $durable, false, false);
#$data = implode(' ', array_slice($argv, 1));
#if(empty($data)) $data = "Hello World!";
#$data = array(
#    "cmd" => "register_confirmation",
#    "id" => 1  
#);
#$msg = new AMQPMessage($data,  array("delivery_mode" => 2)); //persistent message
#$channel->basic_publish($msg, '', $queue_name);
#
#echo " [x] Sent ", $data, "\n";
#
#$channel->close();
#$connection->close();

#$settings = new \Maksoft\Rent\Bus\Settings\Bootstrap();
#$settings->setMode("production");
#$settings->setCacheDir("/hosting/maksoft/maksoft/Templates/busrent/cache/twig");
#$settings->setTwigTemplateDir('/hosting/maksoft/maksoft/Templates/busrent/template');
#$settings->enableAutoReload();
#$settings->initialize();
#$twig = $settings->getTwig();
#$entityManager = $settings->getEntityManager();

$msg = \Maksoft\Rent\Message\Message::init("successfullRegisterEmail", "email/base.html", 3, 30);
$sender = new \Maksoft\Rent\Message\Pusher();
$sender->connect();
$sender->publish($msg);
