 <footer id="footer-page">
    <div class="container">
      <div class="row">
        <div class="col-md-3">

            <div class="widget_background">
              <div class="widget_background__half">
                <div class="bg"></div>
              </div>
              <div class="widget_background__half">
                <div class="bg"></div>
              </div>
            </div>
            <div class="logo"><img src="<?=TEMPLATE_DIR?>assets/images/logo-busrent.png" width="200px" alt="BUSRENT" /></div>
            <div class="widget_content">
              <br />
              <p><?=$o_page->_site['SAddress']?></p>
              <p><a href="tel:<?=$o_page->_site['SPhone']?>"><b><?=$o_page->_site['SPhone']?></a></p>
              </div>
        
        </div>
        <div class="col-md-2">
          <div class="widget widget_about_us">
            <h3>�� ���</h3>
            <div class="widget_content">
              <p>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="widget widget_categories">
            <h3>���� ��</h3>
            <ul>
              <li><a href="#">��������</a></li>
              <li><a href="#">���������</a></li>
              <li><a href="#">�����</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-2">
          <div class="widget widget_recent_entries">
            <h3>���������</h3>
            <ul>
            <?php
                foreach($o_page->get_pSubPages($o_page->_site['StartPage']) as $page){
                    if($page['SecLevel'] == 0 and $page["toplink"] == 2){
                        echo "<li><a href=\"".$o_page->get_pLink($page["n"])."\">".$page["Name"]."</a></li>";
                    }
                }
            ?>
            </ul>
            <?php
            ?>
          </div>
        </div>
        <div class="col-md-3">
          <div class="widget widget_follow_us">
            <div class="widget_content">
              <p>�� ��������� ������, ������� �� ��:</p>
              <span class="phone"><a href="tel:<?=$o_page->_site['SPhone']?>"><?=$o_page->_site['SPhone']?></a></span>
              <div class="awe-social">
                <a href="<?=$o_page->get_sConfigValue("CMS_TWITTER_PAGE_LINK"); ?>"><i class="fa fa-twitter"></i></a>
                <a href="<?=$o_page->get_sConfigValue("CMS_PINTEREST_PAGE_LINK"); ?>"><i class="fa fa-pinterest"></i></a>
                <a href="<?=$o_page->get_sConfigValue("CMS_FACEBOOK_PAGE_LINK"); ?>"><i class="fa fa-facebook"></i></a>
                <a href="<?=$o_page->get_sConfigValue("CMS_LINKEDIN_PAGE_LINK"); ?>"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright">
        <p>� <?=date('Y', time());?> BUSRENT.</p>
      </div>
    </div>
  </footer>
</div>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/js/lib/jquery-1.11.2.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/js/lib/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/js/lib/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/js/lib/jquery.owl.carousel.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/js/lib/theia-sticky-sidebar.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/js/lib/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/js/lib/jquery-ui.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/js/scripts.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/js/custom.js"></script>
<!--
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/Gallery/js/blueimp-gallery.min.js"></script>
-->
<script type="text/javascript" src="http://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/revslider/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>assets/revslider/js/jquery.themepunch.tools.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
<script type="text/javascript">
<?php
if ($route->is_logged()){
?>
$(".menu-list li:last").remove();
$(".menu-list").append("<li><a href=\"/page.php?n=19358929\"> ����� ����������</a></li>");
$(".menu-list").append("<li><a href=\"/\" onclick=\"return logout()\"><i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i></a></li>");



function logout()
{
  $.ajax({
    type: "POST",
    url: "/Templates/busrent/api.php",
    data: {action: "logout"},
    success: function(data){
        var response = JSON.parse(data);
        if(response.code == "success"){
        } else {
            return false;
        }
    }
  });
}
<?php } ?>
   if($('#slider-revolution').length) {
    $('#slider-revolution').show().revolution({
        ottedOverlay:"none",
        delay:10000,
        startwidth:1600,
        startheight:450,
        hideThumbs:200,

        thumbWidth:100,
        thumbHeight:50,
        thumbAmount:5,
        
                                
        simplifyAll:"off",

        navigationType:"none",
        navigationArrows:"solo",
        navigationStyle:"preview4",

        touchenabled:"on",
        onHoverStop:"on",
        nextSlideOnWindowFocus:"off",

        swipe_threshold: 0.7,
        swipe_min_touches: 1,
        drag_block_vertical: false,
        
        parallax:"mouse",
        parallaxBgFreeze:"on",
        parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                                
                                
        keyboardNavigation:"off",

        navigationHAlign:"center",
        navigationVAlign:"bottom",
        navigationHOffset:0,
        navigationVOffset:20,

        soloArrowLeftHalign:"left",
        soloArrowLeftValign:"center",
        soloArrowLeftHOffset:20,
        soloArrowLeftVOffset:0,

        soloArrowRightHalign:"right",
        soloArrowRightValign:"center",
        soloArrowRightHOffset:20,
        soloArrowRightVOffset:0,

        shadow:0,
        fullWidth:"on",
        fullScreen:"off",

        spinner:"spinner2",
                                
        stopLoop:"off",
        stopAfterLoops:-1,
        stopAtSlide:-1,

        shuffle:"off",

        autoHeight:"off",
        forceFullWidth:"off",
        
        
        
        hideThumbsOnMobile:"off",
        hideNavDelayOnMobile:1500,
        hideBulletsOnMobile:"off",
        hideArrowsOnMobile:"off",
        hideThumbsUnderResolution:0,

        hideSliderAtLimit:0,
        hideCaptionAtLimit:0,
        hideAllCaptionAtLilmit:0,
        startWithSlide:0
    });
}
function hide(){
    $('#to_date').hide();
    $('#from_date').attr('class' , 'col-xs-12 col-md-6');
}
function show(){
    $('#from_date').attr('class', 'col-xs-6 col-md-3');
    $('#to_date').show();
}
$(window).on("load", function(){
    $.each($('input[class="datepicker"]'), function(i, el){
        $(el).datepicker({ dateFormat: 'yy-mm-dd' });
    });
});

</script>
	
</body>
</html>
