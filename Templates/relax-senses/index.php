<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
	<title><?=$Title?></title>
    <?php
		//include template configurations
		include("configure.php");		
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
    <link href="http://www.maksoft.net/<?=TEMPLATE_DIR?>layout.css" id="layout-style" rel="stylesheet" type="text/css" />
    <link href="http://maksoft.net/css/admin_classes.css" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/<?=TEMPLATE_DIR?>base_style.css" id="base-style" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript" src="<?=TEMPLATE_DIR?>effects.js"></script>
</head>
<?php
	//get site news
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 5");
	
	//get most viewed pages
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5");

	//get last added pages
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5", "p.preview>30");
	
	//about page
	$n_about = 199755;
	$n_contacts = 198398;
?>
<body>
<?php
if(defined('FB_COMMENTS_APP_ID')) {
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/bg_BG/all.js#xfbml=1&appId=<?=FB_COMMENTS_APP_ID?>

";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php
} // end fb
?>

<div id="site_container">
    <div id="header">
        	<?php include TEMPLATE_DIR."header.php"?>
    </div>
	<div id="page_container" class="shadow" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
        <div class="main-content">
        <?php
			if($o_page->_page['n'] == $o_site->_site['StartPage'])
				include TEMPLATE_DIR."home.php";
			elseif($o_page->_page['SiteID'] == $o_site->_site['SitesID'] )
				include TEMPLATE_DIR."main.php";
			else
				include TEMPLATE_DIR."admin.php";
		?>
        </div>
    </div>
    <div id="footer">
    	<div class="footer-content" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
		<?php
			include TEMPLATE_DIR . "footer.php";
		?>
        <div class="social-links">
        	<a href="https://www.facebook.com/pages/%D0%A2%D0%B5%D1%80%D0%B0%D0%BF%D0%B8%D0%B8-%D0%B7%D0%B0-%D0%BB%D0%B8%D1%86%D0%B5-%D0%B8-%D1%82%D1%8F%D0%BB%D0%BE-%D1%81-%D0%B5%D1%81%D1%82%D0%B5%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%B0-%D0%BA%D0%BE%D0%B7%D0%BC%D0%B5%D1%82%D0%B8%D0%BA%D0%B0-%D0%BD%D0%B0-Alqvimia/110216832394740" class="icon facebook" target="_blank"></a>
            <a href="https://twitter.com/RelaxSenses" class="icon twitter" target="_blank"></a>
            <a href="https://plus.google.com/116761901104295568925/posts" class="icon gplus" target="_blank"></a>
            <a href="https://previous.delicious.com/v2/rss/relaxsenses" class="icon rss" target="_blank"></a>
        </div>
        <br clear="all">
        <div class="copyrights"><?=$o_site->get_sCopyright() . date("Y")?>, �������� <a href="http://www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a></div>
        </div> 
    </div>
</div>
</body>
</html>