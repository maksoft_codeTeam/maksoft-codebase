<!--page_container-->
<div class="page_container">
    <!--MAIN CONTENT AREA-->
    <div class="wrap">
        <div class="container">
            <div class="page_full white_bg drop-shadow">
                <div class="breadcrumb">
                    <div class="container">
                        <div class="breadcrumb_title"><?=$o_page->print_pTitle("strict")?></div>
                        <?=$o_page->print_pName(false, $o_page->_page['ParentPage'])?><span>/</span><?=$o_page->print_pName()?>
                    </div>
                </div>
                <div class="container">
                    <div class="page_in" id="pageContent">
                        <div class="container inner_content">
						<?php
                          //enable tag search / tag addresses
                          if(strlen($search_tag)>2)
                              {
                                  $o_page->print_search($o_site->do_search(iconv("UTF-8","CP1251",$search_tag), 10)); 				
                              }
                          //enable site search
                          elseif(strlen($search)>2)
                              $o_page->print_search($o_site->do_search($search, 10)); 
                          else
                              {	
                                    $o_page->print_pContent();
                                    $cms_args = array("CMS_MAIN_WIDTH" => "100%");
                                    $o_page->print_pSubContent();
                                    eval($o_page->get_pInfo("PHPcode"));
                                    if ($user->AccessLevel >= $row->SecLevel)
                                        include_once("$row->PageURL");
                              }
                        ?>

                        </div>
                        	<br clear="all">
      					<?php $o_page->print_pNavigation()?>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <!--/MAIN CONTENT AREA-->    	
</div>
<!--//page_container-->