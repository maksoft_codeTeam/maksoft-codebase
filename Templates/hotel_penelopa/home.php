	<!--page_container-->
    <div class="page_container">
        
        <!--slider-->
		<?php include TEMPLATE_DIR . "boxes/box_banner_slider.php"; ?>
        <!--//slider-->
               
        <!--planning-->
		<?php include TEMPLATE_DIR . "boxes/box_planning.php"; ?>
        <!--//planning-->
        <!--Welcome-->
        <div class="separator"></div>
        <div class="wrap welcome">
        	<div class="container">
                    <div class="span2"></div><div class="span7" id="hometext"><h2><?=HEADING_WELCOME?></h2></div><div class="span3"></div>
             </div>
        </div>        
        <!-- <div class="wrap">  
            <div class="container welcome_block">
            <?=HEADING_WELCOME_TEXT?>
            </div>
        </div> -->
        <div class="separator"></div>
        <!--//Welcome-->

        <!--featured works-->
        <?php include TEMPLATE_DIR . "boxes/box_featured_works.php"; ?>
        <!--//featured works-->
        
		<!-- middle content -->
        <?php /* <div class="wrap block">
            <div class="container">
                
                <div class="row">
                    <div class="span6">
                    <?php //include TEMPLATE_DIR . "boxes/box_about.php"; ?>
                    <?php include TEMPLATE_DIR . "boxes/box_reservation.php"; ?>
                    </div>
                    <div class="span6">
                    <?php //include TEMPLATE_DIR . "boxes/box_accordion.php"; ?>
                    <?php include TEMPLATE_DIR . "boxes/box_comments.php"; ?>
                    </div>
        
                    
        
                </div>                
            </div>
        </div> */ ?>
		<!-- //middle content -->
        
		<!-- Latest posts -->
		<?php include TEMPLATE_DIR . "boxes/box_latest_posts.php"; ?>
		<!-- //Latest posts -->
		
        <div class="wrap navbar">
            <div class="container">
            <?php $o_page->print_pNavigation()?>
            </div>
        </div>		
       
    </div>
    <!--//page_container-->