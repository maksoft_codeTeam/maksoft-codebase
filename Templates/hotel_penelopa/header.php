<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="windows-1251">
<title><?=$Title?></title>
<?php
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<link href="<?=TEMPLATE_PATH?>css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" id="camera-css"  href="<?=TEMPLATE_PATH?>css/camera.css" type="text/css" media="all">
<link href="<?=TEMPLATE_PATH?>css/bootstrap.css" rel="stylesheet">
<link href="<?=TEMPLATE_PATH?>css/theme.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_PATH?>css/skins/tango/skin.css" />
<link href="<?=TEMPLATE_PATH?>css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?=TEMPLATE_PATH?>css/bootstrap-datepicker.css" rel="stylesheet">

<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->    
</head>
<body>

	<!--header-->
    <div class="header" <?=($o_page->n != $o_site->get_sStartPage() || strlen($search_tag)>2 || strlen($search)>2)?"style=\"position:static;\"":""?>>
    	<div class="wrap">
        	<div class="navbar navbar_ clearfix">
				<div class="socials">
					<div>
						<a href="http://www.facebook.com/Penelopepalace" target="_blank">
							<img src="<?=$o_site->get_sImageDIr()."/"."facebook.png";?>"/>
						</a>
						<a href="http://www.tripadvisor.ru/Hotel_Review-g663130-d2578855-Reviews-Penelope_Palace-Pomorie_Burgas_Province.html" target="_blank">
							<img src="<?=$o_site->get_sImageDIr()."/"."social_tripadvisor.png";?>" width="22"/>
						</a>
					</div>
					<div>
						<a href="http://www.penelopa.bg/page.php?n=19336306">
							<img src="<?=TEMPLATE_PATH?>img/book-now.png" alt="���������� ������"  width="110"/>
						</a>
					</div>
				</div>
			  <div class="logo">
				<a href="<?=$o_page->get_pLink($o_site->get_sStartPage())?>">
					<img src="<?=TEMPLATE_PATH?>img/logo.png" alt="<?=$o_site->_site['Title']?>" width="<?=$tmpl_config['default_logo_width']?>" />
				</a>
			  </div>
              <div class="menu_wrapper navbar navbar_ clearfix">
              	  <div id="languages"><?php //$o_page->print_sVersions();?>
					<ul id="langs">
<!--						<li><a href="" class="russ">RU</a></li>-->
						<li><a href="" class="bggg">BG</a></li>
<!--						<li><a href="" class="ennn">EN</a></li>-->
					</ul>
				  </div>
                  <!--
				  <div class="social-links">
                  	<a href="www.facebook.com/Penelopepalace" target="_blank" class="icon facebook"></a>
                    <a href="http://www.youtube.com/channel/UCKBvR4ioBdcGeXhOrRBTysQ" target="_blank" class="icon youtube"></a>
                    <a href="http://www.tripadvisor.com/Hotel_Review-g295392-d678236-Reviews-Hotel_Plaza_Varna-Varna_Varna_Province.html" target="_blank" class="icon tripadvisor"></a>
                  </div> -->
                  <nav id="main_menu">
                      <div class="menu_wrap">
                       <?php print_menu($o_page->get_pSubpages($o_site->_site['StartPage'], $o_page->get_pSubPagesOrder($o_site->_site['StartPage'])." LIMIT 9"), array('menu_depth'=>1, 'params'=>"class=\"nav sf-menu\"", 'home'=>true))?>
                      </div>
                  </nav>
              </div>
              <div class="clear"></div>
            </div>
        </div>    
    </div>
<!--//header-->