<?php
	$comments = $o_site->get_sComments();
	if(count($comments) > 0 && !DEMO_MODE)
	{
?>
<h2 class="title3"><span><a href="<?=$o_page->get_pLink($n_comments);?>"><?=LABEL_COMMENTS?></a></span></h2>
<div id="box-comments">
	<div class="span3" style="margin-right:30px;">
    <p class="quote">
	<?=mb_substr($comments[0]['comment_text'], 0, 100)?>...
    <br><br><span class="author"><?=$comments[0]['comment_author']?>,</span> <span class="city"><?=$comments[0]['comment_date_added']?></span>
    </p>
    </div>
    <div class="span3">
    <p class="quote">
    <?=mb_substr($comments[1]['comment_text'], 0, 100)?>...
    <br><br><span class="author"><?=$comments[1]['comment_author']?>,</span> <span class="city"><?=$comments[1]['comment_date_added']?></span>
    </p>    
    </div>
</div>
<?php
	}
	else include TEMPLATE_DIR . "demo/box_comments.html";
?>