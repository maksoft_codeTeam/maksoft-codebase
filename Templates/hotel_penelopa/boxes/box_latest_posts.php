<?php
	//$n_latest_posts is defined in languages/.php;
	
	$items = $o_page->get_pSubpages($n_latest_posts, "p.date_modified DESC LIMIT 8", "p.imageNo > 0");
	if(count($items) > 0 && !DEMO_MODE)
		{
			?>
            <div class="wrap block carousel_block" id="latest-posts">
               <div class="container">
                  <h2 class="upper title3"><span><?=$o_page->get_pName($n_latest_posts)?></span></h2>
                    <div class="blog_post_block">
                        <ul id="mycarousel" class="jcarousel-skin-tango">
                        <?php
                            for($i=0; $count=count($items), $i<$count; $i++)
                                {
                                    ?>
                                    <li class="recent_blog">
                                        <div class="recent_image" style="width:270px; height: 190px; overflow:hidden;">
                                        <? $o_page->print_pImage(270, "", "", $items[$i]['n'])?>
                                        </div>
                                        <div class="recent_text" style="height: 150px;">
                                            <? $o_page->print_pName(true, $items[$i]['n'], "class=\"latest_post_text1\"")?>
                                            <p><? $o_page->print_pName(true, $items[$i]['ParentPage'], "class=\"latest_post_text2\"")?></p>
                                            <?=cut_text(strip_tags($items[$i]['textStr']))?>
                                            <a class="read_more" href="<?=$o_page->get_pLink($items[$i]['n'])?>"><?=$o_site->_site['MoreText']?></a>
                                            <div class="clear"></div>
                                        </div>
                                    </li>                                                              
                                    <?	
                                }      
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
            <?
		}
	else include TEMPLATE_DIR . "demo/box_latest_posts.html";
?>