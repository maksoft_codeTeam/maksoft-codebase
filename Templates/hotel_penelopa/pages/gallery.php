	<link href="<?=TEMPLATE_DIR?>js/google-code-prettify/prettify.css" rel="stylesheet">
    <!--page_container-->
    <div class="page_container">
    	<div class="breadcrumb">
        	<div class="wrap">
                <div class="container">
                    <?=$o_page->print_pName("true", $o_page->_page['ParentPage'])?><span>/</span><?=$o_page->print_pName()?>
                </div>
            </div> 
        </div>
    	<!--MAIN CONTENT AREA-->
        <?php
			$categories = $o_page->get_pSubpages();
		?>
        <div class="wrap">
            <div class="container inner_content">
                <div id="options">                                           
                    <ul id="filters" class="option-set" data-option-key="filter">
                    <li><a href="#filter" data-option-value="*" class="btn dark_btn selected"><?=LABEL_ALL?></a></li>
                    <?php
						for($i=0; $c=count($categories), $i<$c; $i++)
							echo '<li><a href="#filter" data-option-value=".category-'.$categories[$i]['n'].'" class="btn dark_btn">'.$categories[$i]['Name'].'</a></li>';
					?>
                    </ul>                                           
                    <div class="clear"></div>
                </div>
                <div class="row">
                    <!-- portfolio_block -->
                    <div class="projects">                                  

                        <?php

							for($i=0; $c=count($categories), $i<$c; $i++)
								{
									$subpages = $o_page->get_pSubpages($categories[$i]['n']);
									
									for($j=0; $c2=count($subpages), $j<$c2; $j++)
									{
										?>
										<div class="span3 element category-<?=$categories[$i]['n']?>" data-category="category01">
											<div class="hover_img" style="height: 180px; overflow:hidden;">
												<a href="<?=$o_page->get_pLink($subpages[$j]['n'])?>"><img src="<?=$o_page->get_pImage($subpages[$j]['n'])?>" /></a>
												<span class="portfolio_zoom"><a href="<?=$o_page->get_pImage($subpages[$j]['n'])?>" rel="prettyPhoto[portfolio1]"></a></span>
																										  
											</div> 
											<div class="item_description">
												<h6><a href="<?=$o_page->get_pLink($subpages[$j]['ParentPage'])?>" title="<?=$o_page->get_pTitle("strict", $subpages[$j]['n'])?>"><?php $o_page->print_pName(false, $subpages[$j]['n']); ?></a></h6>
												<!--<div class="descr"><?=$o_page->get_pText($subpages[$j]['n'])?></div>//-->
                                                <div class="descr"><?=sprintf(TEXT_PAGE_VISITS, $subpages[$j]['preview'])?></div>
											</div>                                    
										</div>
										<?php
									}
								}
						?>                        
                        <div class="clear"></div>
                    </div>   
                    <!-- //portfolio_block --> 
                    <div class="span12" id="pageContent">
                    <?php
		                 $o_page->print_pContent();
            		     $o_page->print_pSubContent();
					?>
                    </div>  
                </div>
              <br clear="all">
              <?php $o_page->print_pNavigation()?>

            </div>
        </div>
    <!--//MAIN CONTENT AREA-->
    	
    </div>
    <!--//page_container-->
	
	<script type="text/javascript" language="javascript">
        var $ = jQuery.noConflict();
    </script>
	<script src="<?=TEMPLATE_DIR?>js/google-code-prettify/prettify.js"></script>
    <script src="<?=TEMPLATE_DIR?>js/jquery.isotope.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/sorting.js"></script>
    <script type="text/javascript" src="<?=TEMPLATE_DIR?>js/jquery.preloader.js"></script>
	<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/myscript.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){	
			//prettyPhoto
			$("a[rel^='prettyPhoto']").prettyPhoto();
			
			//Image hover
			$(".hover_img").live('mouseover',function(){
					var info=$(this).find("img");
					info.stop().animate({opacity:0.1},500);
					$(".preloader").css({'background':'none'});
				}
			);
			$(".hover_img").live('mouseout',function(){
					var info=$(this).find("img");
					info.stop().animate({opacity:1},500);
					$(".preloader").css({'background':'none'});
				}
			);	
			// Preloader
			$(".projects .element").preloader();
			
			
							
		});
	</script>
    <script src="<?=TEMPLATE_DIR?>js/application.js"></script>