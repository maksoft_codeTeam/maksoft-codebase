	<link href="<?=TEMPLATE_DIR?>js/google-code-prettify/prettify.css" rel="stylesheet">
    <!--page_container-->
    <div class="page_container">
    	<div class="breadcrumb">
        	<div class="wrap">
                <div class="container">
                    <?=$o_page->print_pName("true", $o_page->_page['ParentPage'])?><span>/</span><?=$o_page->print_pName()?>
                </div>
            </div> 
        </div>
    	<!--MAIN CONTENT AREA-->
        <?php
			$categories = $o_page->get_pSubpages();
		?>
        <div class="wrap">
            <div class="container inner_content">
                <div class="row">
                    <!-- portfolio_block -->
                    <div class="projects">                                  

                        <?php

							for($i=0; $c=count($categories), $i<$c; $i++)
								{
										?>
										<div class="span3 element">
											<div class="hover_img" style="height: 180px; overflow:hidden;">
												<a href="<?=$o_page->get_pLink($categories[$i]['n'])?>"><img src="<?=$o_page->get_pImage($categories[$i]['n'])?>" /></a>
												<span class="portfolio_zoom"><a href="<?=$o_page->get_pImage($categories[$i]['n'])?>" rel="prettyPhoto[portfolio1]"></a></span>
																										  
											</div> 
											<div class="item_description">
												<h6><?php $o_page->print_pName("true", $categories[$i]['n']); ?>></h6>
												<div class="descr">Et dicta essent vis, sed vitae dictas vulputate ea, ex zril quaeque quo.</div>
											</div>                                    
										</div>
										<?php
								}
						?>                        
                        <div class="clear"></div>
                    </div>   
                    <!-- //portfolio_block --> 
                    <div class="span8" id="pageContent">
                    <?php
		                 $o_page->print_pContent();
            		     $o_page->print_pSubContent();
					?>
                    </div>  
                </div>
            </div>
        </div>
    <!--//MAIN CONTENT AREA-->
    	
    </div>
    <!--//page_container-->
	
	<script type="text/javascript" language="javascript">
        var $ = jQuery.noConflict();
    </script>
	<script src="<?=TEMPLATE_DIR?>js/google-code-prettify/prettify.js"></script>
    <script src="<?=TEMPLATE_DIR?>js/jquery.isotope.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/sorting.js"></script>
    <script type="text/javascript" src="<?=TEMPLATE_DIR?>js/jquery.preloader.js"></script>
	<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/myscript.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){	
			//prettyPhoto
			$("a[rel^='prettyPhoto']").prettyPhoto();
			
			//Image hover
			$(".hover_img").live('mouseover',function(){
					var info=$(this).find("img");
					info.stop().animate({opacity:0.1},500);
					$(".preloader").css({'background':'none'});
				}
			);
			$(".hover_img").live('mouseout',function(){
					var info=$(this).find("img");
					info.stop().animate({opacity:1},500);
					$(".preloader").css({'background':'none'});
				}
			);	
			// Preloader
			$(".projects .element").preloader();
			
			
							
		});
	</script>
    <script src="<?=TEMPLATE_DIR?>js/application.js"></script>