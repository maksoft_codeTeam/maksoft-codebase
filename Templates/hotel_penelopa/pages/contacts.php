    <!--page_container-->
    <div class="page_container">
    	<div class="breadcrumb">
        	<div class="wrap">
            	<div class="container">
                    <?=$o_page->print_pName("true", $o_page->_page['ParentPage'])?><span>/</span><?=$o_page->print_pName()?>
                </div>
            </div>
        </div>
    	<div class="wrap">
        	<div class="container" id="pageContent">
                <section>
                	<div class="row">
                    	<div class="span8">
                        	<h1 class="title"><?=$o_page->print_pTitle("strict")?></h1>
                        </div>
                    	<div class="span6">
                            <div id="map"><iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1469.3462313918565!2d27.602781583843242!3d42.56182269199773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xd3378b8ee5def9e5!2sHotel+Penelope+Palace!5e0!3m2!1sen!2sbg!4v1440265238136"></iframe>
							</div>
                        </div>
                    	<div class="span6">
                            <div class="contact_form">
                                                <?php
												if($sent)
												{
												?>
                                                <div class="alert">
                                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                  <strong>Message:</strong> Your message has been sent successfully!
                                                </div>
                                                <?php
												}
												?>
                                                <div id="fields">
                                                <form id="ajax-contact-form" action="form-cms.php" method="post">
                                                    <input class="span6" type="text" name="name" value="" placeholder="<?=LABEL_NAME?>" />
                                                    <input class="span6" type="text" name="EMail" value="" placeholder="<?=LABEL_EMAIL?>" />
                                                    <input class="span6" type="text" name="subject" value="" placeholder="<?=LABEL_SUBJECT?>" />
                                                    <textarea id="message2" name="message" class="span6" placeholder="<?=LABEL_MESSAGE?>"></textarea>
                                                    <div class="clear"></div>
                                                    <br>
                                                    <input type="reset" class="btn goldhover" value="<?=BUTTON_CLEAR?>" />
                                                    <input type="submit" class="btn goldhover" value="<?=BUTTON_SUBMIT?>" />
                                                    <div class="clear"></div>
                                                    <br>
                                                        
                                                        <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
                                                        <input name="SiteID" type="hidden" id="SiteID" value="<?=$o_site->SiteID?>">
                                                        <input name="ToMail" type="hidden" id="ToMail" value="<?=$o_site->_site['EMail']?>"> 
                                                        <input name="conf_page" type="hidden" id="conf_page " value="<?=$o_page->get_pLink()?>&sent=1">
                                                </form>
                                 </div> 
                            </div>
                        </div>                	
                        <div class="clear"></div>
                        <div class="span3 contact_text" style="border-top: 1px solid #FFF"><?=$o_page->get_pText()?></div>
                        <div class="span3" style="border-top: 1px solid #FFF"></div>
                        <div class="span6" style="border-top: 1px solid #FFF"><br><?=$o_page->get_pText($n_about)?></div>
                        
            <!-- middle content -->
            <br clear="all"><br clear="all">
            <div class="row border">
                <div class="span6">
                <?php 
				//	include TEMPLATE_DIR . "boxes/box_reservation.php"; 
				?>
                </div>
                <div class="span6">
                <?php 
				// include TEMPLATE_DIR . "boxes/box_comments.php"; 
				?>
                </div>
            </div>                
            <!-- //middle content -->
                                    
                	</div>
                </section>
              <br clear="all">
              <?php $o_page->print_pNavigation()?>

            </div>
        </div>
    </div>
    <!--//page_container-->