<!--page_container-->
<div class="page_container">
    <div class="breadcrumb">
        <div class="wrap">
            <div class="container">
                <?=$o_page->print_pName("true", $o_page->_page['ParentPage'])?><span>/</span><?=$o_page->print_pName()?>
            </div>
        </div> 
    </div>
    <!--MAIN CONTENT AREA-->
    <div class="wrap">
        <div class="container">
        	<h1 class="title"><?=$o_page->print_pTitle("strict")?></h1>
            <div class="row pad25">
                <div class="span8">               	
                    <div id="portfolio_carousel" class="carousel slide">
                            <div class="carousel-inner">
                            <?php
								if($o_page->get_pImage())
									{
										?>
										<div class="item active">
										<?php $o_page->print_pImage(800)?>   
										</div>
										<?	
									}
								$subpages = $o_page->get_pSubpages($o_page->n, "p.sort_n ASC", "p.imageNo > 0");
                            	for($i=0; $c=count($subpages), $i<$c; $i++)
									{
										?>
                                          <div class="item <?=(($i==0 && !$o_page->get_pImage())? "active":"")?>">
                                          <?php $o_page->print_pImage(800, "", "", $subpages[$i]['n'])?>    
                                          </div>                                       
										<?php
											
									}
							?>
                            </div>
                            <a class="left carousel-control" href="#portfolio_carousel" data-slide="prev"></a>
                            <a class="right carousel-control" href="#portfolio_carousel" data-slide="next"></a>
                      </div>
                </div>
                <div class="span4">
                <?php $o_page->print_pText($n_about_rooms);?>
                </div>
            </div>
            <a class="btn dark_btn marg20" href="<?=$o_page->get_pLink($o_page->_page['ParentPage'])?>"><?=BUTTON_BACK_TO?> <?=$o_page->get_pName($o_page->_page['ParentPage'])?></a>
            <div class="row pad25">
            	<div class="span6"></div>
                <div class="span6"></div>
            </div>

            <!-- middle content -->
            <br clear="all"><br clear="all">
            <div class="row border">
                <div class="span6">
                <?php include TEMPLATE_DIR . "boxes/box_reservation.php"; ?>
                </div>
                <div class="span6">
                <?php include TEMPLATE_DIR . "boxes/box_comments.php"; ?>
                </div>
            </div>                
            <!-- //middle content -->

            <br clear="all">
            <?php $o_page->print_pNavigation()?>            
        </div>
    </div>
<!--//MAIN CONTENT AREA-->
    
</div>
<!--//page_container-->