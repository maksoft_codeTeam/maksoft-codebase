<?php
	define("TEMPLATE_NAME", "hotel_penelopa");
	define("TEMPLATE_DIR", "Templates/".TEMPLATE_NAME."/");
	define("TEMPLATE_PATH", "http://".$_SERVER['SERVER_NAME']."/Templates/".TEMPLATE_NAME."/");
	define("TEMPLATE_IMAGES", "http://www.maksoft.net/".TEMPLATE_DIR."img/");
	define("TEMPLATE_DEMO", "Templates/_test/".TEMPLATE_NAME."/demo/");
	define("TEMPLATE_IMAGES_DEMO", "http://www.maksoft.net/".TEMPLATE_DIR."demo/img/");
	if($user->AccessLevel > 0)
		define("DEMO_MODE", false);
	else
		define("DEMO_MODE", false);

	//show what options are available for config
	/*
	$tmpl_config_support = array(
		"change_theme"=>true,					// full theme support
		"change_layout"=>true,					// layout support
		"change_banner"=>true,				// banner support
		"change_css"=>true,						// css support
		"change_background"=>true,		// background support
		"change_fontsize"=>true,				// fontsize support
		"change_max_width"=>true,			// site width support
		"change_menu_width"=>false,		// site menu width support
		"change_main_width"=>true,		// page content width support
		"change_logo"=>true,						// logo support
		"column_left" => true, 						// column left support
		"column_right" => true,					// column right support
		"drop_down_menu"=> true			// drop-down menu support
	);
	*/
	//define some default values
	$tmpl_config = array(
		"default_logo" => TEMPLATE_IMAGES_DEMO."logo.png",
		"default_logo_width" =>($o_site->get_sConfigValue('TMPL_LOGO_SIZE'))? $o_site->get_sConfigValue('TMPL_LOGO_SIZE') : "140",
		"default_main_width" => "100%",
		"footer" => array(
								TEMPLATE_DIR . "boxes/news.php"
							),		
		"date_format"=> "d-m-Y"
	);
	
	$tmpl_page_templates = array(
	"About"		=> TEMPLATE_DIR . "about.html",
	"Portfolio"	=> TEMPLATE_DIR . "portfolio_3columns.html",
	"Contacts" 	=> TEMPLATE_DIR . "contacts.html",
	"Blog"		=> TEMPLATE_DIR . "blog.html",
	"Post"		=> TEMPLATE_DIR . "blog_post.html",
	"Single"	=> TEMPLATE_DIR . "single_portfolio.html"
	);

	if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->get_sImageDIr()."/".basename($o_site->_site['Logo']);
	
	//define("TMPL_TEXT_WELCOME", "���� ������ �� ������� ������ ������ ��������?");
	//define("TMPL_TEXT_WELCOME_CONTENT", "*     *     *<br><span>Hotel Plaza - ����������� ������� �� ������ � �������</span>");
	//define("TMPL_TEXT_WELCOME", "Where you can do business while relaxing?");
	//define("TMPL_TEXT_WELCOME_CONTENT", "*     *     *<br><span>Hotel Plaza - preferred place for business and pleasure</span>");
?>