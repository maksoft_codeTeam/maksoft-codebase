<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<?php
require_once "lib/lib_page.php";

include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/miptrade/");
define("DIR_TEMPLATE_IMAGES", "Templates/miptrade/images/");
define("DIR_MODULES","web/admin/modules/");

$o_site->print_sConfigurations();

//vip pages
$vip_menu = $o_page->get_pSubpages(0, "p.sort_n ASC LIMIT 5", "p.toplink = 4");

//news
$news = $o_page->get_pSubpages($o_site->get_sNewsPage(), "p.date_added DESC LIMIT 3");

//main menu, products page
$menu = $o_page->get_pSubpages($n_products_cat, "p.sort_n ASC");

//bottom menu, products page
$bottom_menu = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC");

//home photo gallery
$home_gallery = $o_page->get_pSubpages(0, "RAND() LIMIT 6", "im.image_src != ''");

//partners list
$partners = $o_page->get_page($n_partners);
$partners_list = $o_page->get_pSubpages($partners['n']);

//products list
$products = $o_page->get_pSubpages($n_products);

?>
<title><?php echo("$Title");?></title>
<link href="http://www.maksoft.net/Templates/miptrade/ddsmoothmenu.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/miptrade/ddsmoothmenu-v.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/miptrade/easyslider.css" rel="stylesheet" type="text/css">
<script src="http://www.maksoft.net/Templates/miptrade/ddsmoothmenu.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "menu", //menu DIV id
	orientation: 'v', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu-v', //class added to menu's outer DIV
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>
<script type="text/javascript" src="http://www.maksoft.net/lib/jquery/easyslider/easySlider1.7.js"></script>
<script type="text/javascript">
	$j(document).ready(function(){	
		$j("#slider").easySlider({
			auto: true,
			speed: 500,
			prevText: '',
			nextText: '',
			continuous: true
		});
		
		$j("#slider2").easySlider({
			auto: true,
			speed: 2000,
			pause: 5000,
			controlsShow: false,
			prevText: '',
			nextText: '',
			continuous: true
		});


	});	
</script>
<link href="http://www.maksoft.net/Templates/miptrade/layout.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/miptrade/base_style.css" rel="stylesheet" type="text/css">

</head>
<body>
<div align="center" class="bg">
	<center>
	<div id="page_container" align="center">
		<div id="header">
			<?php include "header.php"; ?>
		</div>
		<?php
			if($o_page->n == $o_site->StartPage)
			{
			?>


			<div id="banner">
				<div class="mask_left"></div>
				<div class="mask_right"></div>
				<?php include DIR_TEMPLATE . "banner.php"; ?>
			</div>
			<div style="height: 224px;"></div>
			<?php
			}
			?>
		
		<div id="<?=$o_page->n == $o_site->StartPage ? "home":"main"?>">
			<?php
				if($o_page->n == $o_site->get_sStartPage())
					include "home.php";
				else
					include "main.php";
			?>

		</div>
		</div>
	</div>
	<div id="footer">
		<center>
			<div class="content">
				<div class="bottom_menu">
					<?php
						for($i=0; $i<count($bottom_menu); $i++)
							{
								if($i>0) echo "&nbsp;&nbsp;/&nbsp;&nbsp; ";
								//echo "<a href=\"".$bottom_menu[$i]['page_link']."\" title=\"".$bottom_menu[$i]['title']."\">".$bottom_menu[$i]['Name']."</a>";
								$o_page->print_pLink($bottom_menu[$i]['n']);
							}
					?>
				</div>
				<div class="credits"><a href="http://www.pmd-studio.com" target="_blank" title="�������� & ��� ������"><b>pmd</b>studio</a> / <a href="http://www.maksoft.net" target="_blank" title="CMS, SEO, hosting & �������"><b>maksoft</b>.net</a></div>
				<br clear="all">
				<div class="copyrights"><?=$o_site->get_sCopyright()?></div>
			</div>
		</center>
	</div>
	<center><div id="nav_links"><?=$o_page->get_pNavigation()?></div></center>
	</center><br><br>
</div>
</body>
</html>