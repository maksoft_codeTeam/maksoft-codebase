<script id='passenger-info'type="text/x-handlebars-template" >
<fieldset class="passenger-fieldset">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <div class="row">
                    <div class="col-md-6">
                        <h4> ����� {{number}}</h4>
                    </div>
                    <div class="col-md-6">
                        <h4 class="text-right">
                            <span id='info-price{{n}}' class="badge pr" style='display:none'>
                                <span class="price"></span>
                            </span>                                      
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" data-trigger="hover" data-toggle="popover" data-content="��������� ��������� ������ �� ����������� �� ��������� � ����������� �� �������." for="passengers{{n}}-tariff">
                                <span>������</span>
                                <i class="fa fa-info-circle"></i>
                            </label>
                            <input type='hidden' id='passengers{{n}}-route' value='<?=$route->getId();?>' name='passengers[{{n}}][route]'>
                            <select onclick="update_price(this)" class="form-control" data-num="{{n}}" id="passengers{{n}}-tariff" name="passengers[{{n}}][tariff]">
                                <option selected value disabled> �������� ����� </option>
                                <?php foreach($route->getPrices() as $price):?>
                                <option  data-num="{{n}}" data-price="<?=number_format($price->getPrice(), 2)?>" value="<?=$price->getType()->getId()?>"><?=$price->getType()->getDescription()?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="passengers{{n}}-firstname">���</label>
                            <input type="text" class="form-control passenger-firstname" maxlength="25" id="passengers{{n}}-firstName" name="passengers[{{n}}][firstName]" value="">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="passengers{{n}}-lastname">�������</label>
                            <input type="text" class="form-control passenger-lastname" maxlength="25" id="passengers{{n}}-lastName" name="passengers[{{n}}][lastName]" value="">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" data-trigger="hover" data-toggle="popover" data-content="����� �� ������� �� ������ � �������,������������ �������, ��� �������� ����, �� ����� �� ������� ���� �� �� ������ ������ ����������, � ������ �� ������� � �� � ������������ ������������� �������� � ������������ �� �����. " for="passengers{{n}}[phone]">
                                <span>���. �������</span>
                                <i class="fa fa-info-circle"></i>
                            </label>
                            <div class="intl-tel-input allow-dropdown">
                                <div class="flag-container">
                                    <div class="selected-flag" tabindex="0" title="United Kingdom: +44">
                                        <div class="iti-flag gb"></div>
                                        <div class="iti-arrow"></div>
                                    </div>
                                </div>
                                <input type="tel" class="form-control passenger-phone" maxlength="15" id="passengers{{n}}-phone" name="passengers[{{n}}][phone]" value="" autocomplete="off">
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" data-trigger="hover" data-toggle="popover" data-content="Email �� �������. �������� �� �� ��������� �� ������, ��������� �� �������� ���������� � �������� � ������ �� ��������� �� ������������" for="passengers{{n}}[email]">
                                <span>���������� ����.</span>
                                <i class="fa fa-info-circle"></i>
                            </label>
                            <input type="email" class="form-control passenger-email" maxlength="45" id="passengers{{n}}-email" placeholder="����������" name="passengers[{{n}}][email]" value="">
                            <input type="hidden" id='passengers{{n}}-ticket_price' name="passengers[{{n}}][ticket_price]" value="">
                            <input type="hidden" id='passengers{{n}}-ticket_type' name="passengers[{{n}}][ticket_type]" value="">
                            <input type="hidden" id='passengers{{n}}-seat_id' name="passengers[{{n}}][seat_id]" value="{{seat_id}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>
</script>
