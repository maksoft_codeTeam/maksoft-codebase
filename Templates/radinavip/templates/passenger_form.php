<fieldset class="passenger-fieldset">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <div class="row">
                    <div class="col-md-6">
                    <h4> ����� <?=$seat->getSeat()->getNumber()?></h4>
                    </div>
                    <div class="col-md-6">
                        <h4 class="text-right">
                            <span id='info-price<?=$i?>' class="badge pr" style='display:none'>
                                <span class="price"></span>
                            </span> 
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" data-trigger="hover" data-toggle="popover" data-content="��������� ��������� ������ �� ����������� �� ��������� � ����������� �� �������." for="passengers<?=$i?>-tariff">
                                <span>������</span>
                                <i class="fa fa-info-circle"></i>
                            </label>
                            <input type='hidden' id='passengers<?=$i?>-route' value='<?=$route->getId();?>' name='passengers[<?=$i?>][route]'>
                            <select onclick="update_price(this)" class="form-control" data-num="<?=$i?>" id="passengers<?=$i?>-tariff" name="passengers[<?=$i?>][tariff]">
                                <option selected value disabled> �������� ����� </option>
                                <?php foreach($route->getPrices() as $price):?>
                                <option  data-num="<?=$i?>" data-price="<?=number_format($price->getPrice(), 2)?>" value="<?=$price->getType()->getId()?>"><?=trim($price->getType()->getDescription())?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="passengers<?=$i?>-firstname">���</label>
                            <input type="text" class="form-control passenger-firstname" maxlength="25" id="passengers<?=$i?>-firstName" name="passengers[<?=$i?>][firstName]" value="">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="passengers<?=$i?>-lastname">�������</label>
                            <input type="text" class="form-control passenger-lastname" maxlength="25" id="passengers<?=$i?>-lastName" name="passengers[<?=$i?>][lastName]" value="">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" data-trigger="hover" data-toggle="popover" data-content="����� �� ������� �� ������ � �������,������������ �������, ��� �������� ����, �� ����� �� ������� ���� �� �� ������ ������ ����������, � ������ �� ������� � �� � ������������ ������������� �������� � ������������ �� �����. " for="passengers<?=$i?>[phone]">
                                <span>���. �������</span>
                                <i class="fa fa-info-circle"></i>
                            </label>
                            <div class="intl-tel-input allow-dropdown">
                                <div class="flag-container">
                                    <div class="selected-flag" tabindex="0" title="United Kingdom: +44">
                                        <div class="iti-flag gb"></div>
                                        <div class="iti-arrow"></div>
                                    </div>
                                </div>
                                <input type="tel" class="form-control passenger-phone" maxlength="15" id="passengers<?=$i?>-phone" name="passengers[<?=$i?>][phone]" value="" autocomplete="off">
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" data-trigger="hover" data-toggle="popover" data-content="Email �� �������. �������� �� �� ��������� �� ������, ��������� �� �������� ���������� � �������� � ������ �� ��������� �� ������������" for="passengers<?=$i?>[email]">
                                <span>���������� ����.</span>
                                <i class="fa fa-info-circle"></i>
                            </label>
                            <input type="email" class="form-control passenger-email" maxlength="45" id="passengers<?=$i?>-email" placeholder="����������" name="passengers[<?=$i?>][email]" value="">
                            <input type="hidden" id='passengers<?=$i?>-ticket_price' name="passengers[<?=$i?>][ticket_price]" value="">
                            <input type="hidden" id='passengers<?=$i?>-ticket_type' name="passengers[<?=$i?>][ticket_type]" value="">
                            <input type="hidden" id='passengers<?=$i?>-seat_id' name="passengers[<?=$i?>][seat_id]" value="<?=$seat->getId()?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>
