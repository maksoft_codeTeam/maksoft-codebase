<?php
error_reporting(E_ALL);
ini_set("display_errors", 1); 

use Symfony\Component\HttpFoundation\Request;
use jokuf\ticksys\Config;


require_once __DIR__ . '/config.php';
require_once TEMPLATE_DIR . "header.php";
require_once __DIR__.'/../../global/ticksys/forms/SearchCourseForm.php';


$request = Request::createFromGlobals();

$search_form = new SearchCourseForm($request->request->all());

$pTemplate = $o_page->get_pTemplate( $n, 299 );

$config = new Config(
    $_SESSION['TICKSYS_SETTINGS']['TICKSYS_API_PASS'], 
    $_SESSION['TICKSYS_SETTINGS']['TICKSYS_COMPANY_ID']
);


switch(True){
    case $o_page->_page[ 'SiteID' ] != $o_site->_site[ 'SitesID' ]:
        include TEMPLATE_DIR . "admin.php";
        break; 
    case ($o_page->_page[ 'n' ] == $o_site->_site[ 'StartPage' ] && (!isset($_GET['search_tag']) and !isset($_GET['search']))):
        include TEMPLATE_DIR . "home.php";
        break;
    default:
        echo '<main class="main" role="main">';
        include TEMPLATE_DIR . "main-header.php";
        if ( $pTemplate[ 'pt_url' ] )
            include $pTemplate[ 'pt_url' ];
        else
            include TEMPLATE_DIR . "main.php";
        echo '</main>';
}

include TEMPLATE_DIR . "footer.php";

?>
