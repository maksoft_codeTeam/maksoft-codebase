<?php
?>

<aside id="right-sidebar" class="right-sidebar one-fourth sidebar right widget-area" role="complementary">
    <ul>
        <li class="widget widget-sidebar transfers_advanced_search_widget">
            <!-- Advanced search -->
            <div class="advanced-search-right color" id="booking">
                <div class="wrap">
                    <form role="form" method="POST" action='/global/ticksys/callback.php'>
                        <input type='hidden' name='n' value='19375395'>
                        <input type='hidden' name='SiteID' value='<?=$o_page->_page['SiteID']?>'>
                        <!-- Row -->
                        <div class="f-row">
                            <div class="form-group datepicker one-third">
                                <label for="departure-date">���� � ��� �� ��������</label>
                                <input type="text" id="<?=$search_form->for_date->name?>" name="<?=$search_form->for_date->name?>" value='<?=date('d.m.Y', time());?>'>
                            </div>
                            <div class="form-group select one-third">
                                <label>�������� ��</label>
                                    <select id="<?=$search_form->from_city->name?>" name="<?=$search_form->from_city->name?>" onchange='getArrivalCities(this)'>
                                        <option value="">�������� ����� �� ��������</option>
                                    <?php foreach($directions as $country=>$dir): ?>
                                        <optgroup label="<?=$country?>">
                                        <?=implode('', $directions[$country])?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                    </select>
                            </div>
                            <div class="form-group select one-third">
                                <label>���������� �</label>
                                    <select id="<?=$search_form->to_city->name?>" name="<?=$search_form->to_city->name?>">
                                        <option value="">�������� ����� �� ����������</option>
                                    </select>
                            </div>
                        </div>
                        <div class="f-row">
                            <div class="form-group right">
                                <button type="submit" class="btn large black">�������</button>
                            </div>
                        </div>
                        <?=$search_form->action?>
                        <!--// Row -->
                    </form>
                </div>
            </div>
            <!-- // Advanced search -->
        </li>
        <li class="widget widget-sidebar transfers_contact_widget">
            <article class="transfers_contact_widget one-fourth">
                <h4>����� ����� �� �����?</h4>
                <p>�������� �� � ��� ��� ����� ����� �� ����� ��� ���������� �� ������ ��� ���� ����� �������� � ������ ������ ��� �����������.</p>
                <p class="contact-data">
                    <span class="icon icon-themeenergy_call"></span> <?php echo $o_page->_site['SPhone'];?> </p>
            </article>
        </li>

    </ul>
</aside>

<script>
jQuery(document).ready(function(){
    get('/api/?command=get_cities&n=1') 
        .then(JSON.parse) 
        .then(function(cities) {
                console.log(cities);
            populate_select_by('<?=$search_form->from_city->name?>', cities);
        });

    jQuery("#<?=$search_form->for_date->name?>").datepicker({
        minDate: 0, 
        defaultDate: new Date(),
        showButtonPanel : true,
        dateFormat      : "d.m.yy",
        changeMonth: true,
        firstDay: 1,
        changeYear: true,
    });
});
</script>
