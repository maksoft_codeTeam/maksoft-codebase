<!-- ����� -->
<footer class="footer black" role="contentinfo">
    <div class="wrap">
        <div id="footer-sidebar" class="footer-sidebar widget-area clearfix row" role="complementary">
            <ul>
                <li class="widget widget-sidebar transfers_about_widget">
                    <article class="about_widget clearfix one-half">
                        <h6><?=$o_site->_site['Title']?></h6>
                        <p><?=$o_site->_site['Description']?></p>
                    </article>
                </li>
                <li class="widget widget-sidebar transfers_contact_widget">
                    <article class="transfers_contact_widget one-fourth">
                        <h6>����� ����� �� �����?</h6>
                        <p>�������� �� � ���:</p>       
                        <?php if($o_page->_site['SPhone']) { ?>
                        <p class="contact-data">
                            <i class="fa fa-phone icon" aria-hidden="true"></i> <a href="tel:<?php echo $o_page->_site['SPhone'];?>"><?php echo $o_page->_site['SPhone'];?></a>
                        </p>
                        <?php }?>
                                
                        <p class="contact-data">
                            <i class="fa fa-envelope icon" aria-hidden="true"></i> <a href="mailto:<?php echo $o_page->_site['EMail'];?>"><?php echo $o_page->_site['EMail'];?></a>
                        </p>
                        
                    </article>
                </li>
                <li class="widget widget-sidebar transfers_social_widget">
                    <article class="one-fourth">
                        <h6>��������� ��</h6>
                        <ul class="social">
                                <!-- ������� -->
                                <?php if(defined('CMS_FACEBOOK_PAGE_LINK') || $user->AccessLevel >0) { ?> <li><a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" target="_blank"><span class="fa fa-fw fa-facebook"></span></a></li> <?php }?>
                                
                                <!-- ������� -->
                                <?php if(defined('CMS_GPLUS_PAGE_LINK') || $user->AccessLevel >0) { ?> <li><a href="<?=(defined("CMS_GPLUS_PAGE_LINK")) ? CMS_GPLUS_PAGE_LINK:"#"?>" target="_blank"><span class="fa fa-fw fa-google-plus"></a></li> <?php }?>
                                
                                <!-- RSS -->
                                <li class="rss"><a href="/rss.php" class="fa fa-rss"></a></li>
                            
                        </ul>
                    </article>
                </li>
            </ul>
        </div>
        <!-- #secondary -->
        <div class="copy">
			<p><?php include("Templates/copyrights_links.php"); ?></p>
            <!--����� ��������� (�� ��� �� ����������: ����� ������) -->
            <nav class="foot-nav">
                <ul id="menu-footer" class="">
                        <?php

                        if ( !isset( $footer_links ) )$footer_links = 5;
                        $footer_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $footer_links" );

                        for ( $i = 0; $i < count( $footer_links ); $i++ ) {
                            $subpages = $o_page->get_pSubpages( $footer_links[ $i ][ 'n' ] );
                            ?>
                        <li>
                            <a href="<?php echo $o_page->get_pLink($footer_links[$i]['n']) ?>">
                                <?php echo $o_page->get_pName($footer_links[$i]['n']) ?>
                            </a>
                        </li>

                        <?php  
                        }
                        ?>
                </ul>
            </nav>
            <!--//����� ���������-->
        </div>
    </div>
</footer>
<!-- //����� -->

<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/core.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/widget.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/mouse.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/slider.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/button.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/datepicker.min.js'></script>

<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/ticketform/jquery.validate.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/effect.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery.uniform.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/respond.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery.slicknav.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/scripts.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/custom.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.min.js" integrity="sha256-1O3BtOwnPyyRzOszK6P+gqaRoXHV6JXj8HkjZmPYhCI=" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<?php include( "Templates/footer_inc.php" ); ?>
<script>
jQuery(document).ready(function(){
<?php echo implode(PHP_EOL, $custom_js);?>
});
</script>

</body> 
</html>
