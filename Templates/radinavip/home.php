<!-- Main -->
<main class="main " role="main">

    <!-- Intro -->
    <div class="intro" style="background-image:url(<?=ASSETS_DIR?>assets/images/default-slider.jpg);">
            <div class="wrap">
                <div class="advanced-search color-home-search" id="booking">
                    <div class="wrap">
                        <form role="form" method="post" action='/global/ticksys/callback.php'>
                            <!-- Row -->
                            <input type='hidden' name='n' value='19375395'>
                            <input type='hidden' name='SiteID' value='<?=$o_page->_page['SiteID']?>'>
                            <div class="f-row">
                                <div class="form-group datepicker one-fifth">
                                    <label for="departure-date">���� � ��� �� ��������</label>
                                    <input type="text" id="<?=$search_form->for_date->name?>" name="<?=$search_form->for_date->name?>" value='<?=date('d.m.Y', time());?>'>
                                </div>
                                <div class="form-group select one-third">
                                    <label>�������� ��</label>
                                        <select id="<?=$search_form->from_city->name?>" class='form-control select2' name="<?=$search_form->from_city->name?>" onchange='getArrivalCities(this)'>
                                            <option value="">�������� ����� �� ��������</option>
                                        </select>
                                </div>
                                <div class="form-group select one-third">
                                    <label>���������� �</label>
                                        <select id="<?=$search_form->to_city->name?>" name="<?=$search_form->to_city->name?>" class='form-control select2'>
                                            <option value="">�������� ����� �� ����������</option>
                                        </select>
                                </div>
                                <?=$search_form->action?>
                                <div class="form-group search-button">
                                    <label>&nbsp;</label>
                                    <button type="submit" class="btn large black search-home-button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>

    <div class="wrap">
        <div class="row">
            <!--- ���������� �� ������� �������� -->
            <article class="full-width content textongrey destination type-destination status-publish has-post-thumbnail hentry entry-content">
                <div class="entry-content">
                <?php
                    $o_page->print_pContent();
                ?>
                <div class="clearfix"></div>
                <?php
                    $o_page->print_pSubContent();

                    eval( $o_page->get_pPHPcode() );

                    if ( $user->AccessLevel >= $row->SecLevel )
                        include_once( "$row->PageURL" );
                ?>
                </div>
            </article>
            <!--- // ���������� �� ������� �������� -->
        </div>
    </div>
</main>
<script>
jQuery(document).ready(function(){
    get('/api/?command=get_cities&n=1') 
        .then(JSON.parse) 
        .then(function(cities) {
            populate_select_by('<?=$search_form->from_city->name?>', cities);
        });

    jQuery("#<?=$search_form->for_date->name?>").datepicker({
        minDate: 0, 
        defaultDate: new Date(),
        showButtonPanel : true,
        dateFormat      : "d.m.yy",
        changeMonth: true,
        firstDay: 1,
        changeYear: true,
    });
});
</script>
