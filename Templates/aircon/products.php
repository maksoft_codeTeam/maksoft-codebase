<?php
	$p_products = $o_page->get_pSubpages($o_page->n, $o_page->get_pSubPagesOrder());
	for($i=0; $i<count($p_products); $i++)
		{
			if(empty($p_products[$i]['image_src'])) $p_products[$i]['image_src'] = DIR_TEMPLATE . "images/nopicture.jpg";
		?>
			<table border="0" cellpadding="0" cellspacing="0" class="product_preview">
				<tr>
					<td rowspan="2" width="180px" class="border_image" align="center" valign="middle"><a href="<?=$p_products[$i]['page_link']?>" title="<?=$p_products[$i]['Name']?>"><img src="img_preview.php?image_file=<?=$p_products[$i]['image_src']?>&amp;img_width=175" border="0"></a>
					<td width="10px">
					<td height="50px" valign="middle" class="border head_text"><a href="<?=$p_products[$i]['page_link']?>" title="<?=$p_products[$i]['Name']?>"><?=$p_products[$i]['Name']?></a>
				<tr>
					<td width="10px">
					<td valign="top" class="border" height="130px"><img src="<?=DIR_TEMPLATE_IMAGES?>shadow.jpg"><br style="clear:both"><?=strip_tags(crop_text($p_products[$i]['textStr']),"<p> <br> <b> <strong> <span>")?>
				<tr>
					<td height="30px" valign="top"><img src="<?=DIR_TEMPLATE_IMAGES?>shadow.jpg">
					<td width="10px">
					<td align="right" class="border"><a href="<?=$p_products[$i]['page_link']?>" class="next_link">(<?=$o_site->get_sMoreText()?>)</a>
			</table>		
		<?php
		}
?>