<?php
	if(!isset($drop_links)) $drop_links = 1;
	$dd_links = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n", "p.toplink = $drop_links");
	if(count($dd_links) > 0)
		{
			?>
			<link href="<?=LIB_SERVER?>/menu/ddsmoothmenu/ddsmoothmenu.css" rel="stylesheet" type="text/css" />
			<link href="<?=LIB_SERVER?>/menu/ddsmoothmenu/ddsmoothmenu-v.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="<?=LIB_SERVER?>/menu/ddsmoothmenu/ddsmoothmenu.js"></script>
			<script type="text/javascript">
			ddsmoothmenu.init({
				mainmenuid: "top_menu",
				orientation: 'h',
				classname: 'ddsmoothmenu',
				//customtheme: ["#f2f2f2", "#000000"],
				contentsource: "markup"
			})
			</script>
			<div  class="top-menu-container">
                <div id="top_menu" class="ddsmoothmenu">
                <?php print_menu($dd_links, array('menu_depth'=>2)); ?>
                </div>
			</div>
			<?
		}
?>