<div id="testimonials">
<?php 
	$subpages = $o_page->get_pSubpages();
	for($i=0; $i<count($subpages); $i++)
		{
			echo "<div class=\"item round\">";
			echo "<div class=\"quotes\"></div>";
			echo "<h2 class=\"head_text\"><a href=\"".$subpages[$i]['page_link']."\">".$subpages[$i]['Name'].", ".($subpages[$i]['title'])."</a></h2>";
			echo "<div class=\"content\">".strip_tags($subpages[$i]['textStr'])."<a href=\"".$subpages[$i]['page_link']."\" class=\"next_link\">view more</a></div>";
			echo "</div>";	
		}
?>
</div>