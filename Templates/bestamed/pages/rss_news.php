<?php
$rss_feeds = array(
array(
	"Австрия", 
	"Федерална служба за безопасност в здравеопазването", 
	"Austria", 
	"Bundesamt für Sicherheit im Gesundheitswesen", 
	"www.ages.at", 
	"http://www.ages.at/?eID=teaser_rss_feed&newsSite=28477"),
array(
	"Белгия", 
	"Генерална дирекция по лекарствата ", 
	"Belgium", 
	"Direction générale Médicaments", 
	"www.fagg-afmps.be", 
	""),
array(
	"България", 
	"Изпълнителна Агенция по Лекарствата", 
	"Bulgaria", 
	"Bulgarian Drug Agency", 
	"www.bda.bg", 
	""),
	
array(
	"Великобритания ", 
	"Регулаторна агенция за лекарства и здравни продукти ", 
	"UK", 
	"Medicines and Healthcare products Regulatory Agency", 
	"www.mhra.gov.uk", 
	""),
	/*
array(
	"Великобритания (вет) ", 
	"Дирекция ветеринарни лекарства", 
	"UK (vet) ", 
	"Veterinary Medicines Directorate", 
	"www.vmd.gov.uk", 
	""),
	*/
array(
	"Германия", 
	"Федерално министерство на здравеопазването и социалната сигурност", 
	"Germany", 
	"Bundesministerium für Gesundheit und Soziale Sicherung", 
	"www.bmg.bund.de", 
	""),
array(
	"Германия", 
	"Федералният институт за лекарствата и медицинските изделия", 
	"Germany", 
	"Bundesinstitut für Arzneimittel und Medizinprodukte", 
	"www.bfarm.de", 
	""),
array(
	"Германия", 
	"Паул Ерлих Институт", 
	"Germany", 
	"Paul-Ehrlich-Institut", 
	"www.pei.de", 
	""),
array(
	"Германия (вет) ", 
	"Федерално министерство на защита на потребителите, по прехрана и земеделие", 
	"Germany (vet)", 
	"Bundesministerium für Verbraucherschutz, Ernahrung und Landwirtschaft", 
	"www.bmvel.bund.de", 
	"http://www.bmel.de/SiteGlobals/Functions/RSSFeed/EN/RSSNewsfeed_NeueBeitraege.xml?nn=313112"),
array(
	"Германия (вет) ", 
	"Федерално министерство за защита на потребителите и безопасност на храните (Ветеринарни лекарствени продукти)", 
	"Germany (vet)", 
	"Bundesministerium für Verbraucherschutz und Lebensmittelsicherheit  (Veterinary medicinal products)", 
	"www.bvl.bund.de", 
	"http://www.bvl.bund.de/SiteGlobals/Functions/RSSFeed/RSSGeneratorPressemitteilungen.xml?nn=2854462"),
array(
	"Германия", 
	"Централен офис на страните за здравословни условия на лекарства и медицински изделия", 
	"Germany", 
	"Zentralstelle der Länder für Gesundheitsschutz bei Arzneimitteln und Medizinprodukten", 
	"www.zlg.nrw.de", 
	""),
array(
	"Гърция", 
	"Националната организация по лекарствата", 
	"Greece", 
	"National Organisation for Medicines", 
	"www.eof.gr", 
	""),
array(
	"Дания ", 
	"Датската агенция за лекарствата", 
	"Denmark", 
	"Lægemiddelstyrelsen", 
	"www.dkma.dk", 
	""),
array(
	"Естония", 
	"Държавна агенция по лекарствата", 
	"Estonia", 
	"State Agency of Medicines", 
	"www.sam.ee", 
	""),
array(
	"Ирландия ", 
	"Ирландски борд по лекарствата", 
	"Ireland", 
	"Irish Medicines Board", 
	"www.imb.ie", 
	""),
array(
	"Исландия ", 
	"Лекарства", 
	"Iceland", 
	"Lyfjastofnun", 
	"www.lyfjastofnun.is", 
	""),
array(
	"Испания", 
	"Испанската агенция по лекарствата", 
	"Spain", 
	"Agencia española del medicamento", 
	"www.agemed.es", 
	""),
array(
	"Италия", 
	"Министерство на здравеопазването", 
	"Italy", 
	"Ministero della Salute", 
	"www.ministerosalute.it", 
	""),
array(
	"Кипър", 
	"Министерство на здравеопазването00, Министерство на земеделието", 
	"Cyprus", 
	"Ministry of Health, Ministry of Agriculture", 
	"www.pio.gov.cy", 
	""),
array(
	"Латвия", 
	"Хранителната и ветеринарна служба", 
	"Latvia", 
	"Food and Veterinary Service", 
	"http://vmvt.lt/en/links/", 
	""),
array(
	"Литва", 
	"Държавна агенция за контрол на лекарствата", 
	"Lithuania", 
	"State Medicines Control Agency", 
	"www.vvkt.lt", 
	""),
array(
	"Литва", 
	"Членка на борда на храните и ветеринарна служба", 
	"Lithuania", 
	"State Food and Veterinary Service", 
	"www.vet.lt", 
	""),
array(
	"Лихтенщайн ", 
	"Лихтенщайн Националната администрация, Офис на контрола върху храните и ветеринарните служби, Контролен орган по лекарствата", 
	"Liechtenstein ", 
	"Liechtensteinische Landesverwaltung, Amt für Lebensmittelkontrolle und Veterinarwesen, Kontrollstelle für Arzneimittel", 
	"www.llv.li", 
	""),
array(
	"Люксембург", 
	"Министерство на здравеопазването", 
	"Luxembourg", 
	"Ministère de la Santé Division de la Pharmacie et des Médicaments", 
	"www.etat.lu/MS",
	""),
array(
	"Малта", 
	"Лекарствена правителствена агенция", 
	"Malta", 
	"Medicines Authority", 
	"www.medicinesauthority.gov.mt/", 
	""),
array(
	"Норвегия", 
	"Норвежката агенция по лекарствата", 
	"Norway", 
	"Statens Legemiddelverk", 
	"www.legemiddelverket.no", 
	""),
array(
	"Полша", 
	"Служба за лекарствените продукти", 
	"Poland", 
	"Office for Medicinal Products", 
	"http://www.urpl.gov.pl", 
	""),
array(
	"Португалия", 
	"Португалия", 
	"Portugal", 
	"Instituto Nacional da Farmacia e do Medicamento", 
	"www.infarmed.pt", 
	""),
array(
	"Румъния", 
	"Национална агенция по лекарствата, Национална Санитарни ветеринарна агенция", 
	"Romania", 
	"National Medicines Agency, National Sanitary Veterinary Agency", 
	"www.anm.ro/anmdm/", 
	""),
array(
	"Словакия", 
	"Държавния институт за контрол на наркотиците, Институт за контрол на състоянието на ветеринарните биологични продукти и лекарства", 
	"Slovakia", 
	"State Institute for Drug Control, Institute for State Control of Veterinary Biologicals and  Medicaments", 
	"www.sukl.sk", 
	""),
array(
	"Словения ", 
	"Агенция на Република Словения за лекарствени продукти и медицински изделия", 
	"Slovenia", 
	"Agency of the Republic of Slovenia for Medicinal Products and  Medical Devices", 
	"www.jazmp.si", 
	""),
array(
	"Унгария", 
	"Национален институт по фармация, Институт за ветеринарномедицинските продукти", 
	"Hungary ", 
	"National Institute of Pharmacy, Institute for Veterinary Medicinal Products", 
	"www.ogyi.hu", 
	""),
array(
	"Финландия ", 
	"Национална агенция по медицински продукти", 
	"Finland", 
	"Lääkelaitos", 
	"www.fimea.fi", 
	""),
array(
	"Франция", 
	"Френската агенция за безопасност на здравни продукти", 
	"France", 
	"Agence Française de Sécurité Sanitaire des Produits de Santé", 
	"www.ansm.sante.fr", 
	""),
	/*
array(
	"Франция (вет) ", 
	"Френска агенция за безопасност на храните", 
	"France (vet)", 
	"Agence Française de Sécurité Sanitaire des Aliments", 
	"www.anmv.afssa.fr", 
	""),
	*/
array(
	"Холандия", 
	"Държавен надзор на общественото здраве, Инспекторат по здравеопазването", 
	"Netherlands", 
	"Staatstoezicht op de volksgezondheid Inspectie voor de Gezondheidszorg", 
	"www.igz.nl", 
	""),
array(
	"Холандия", 
	"Борд по оценка на лекарствените продукти", 
	"Netherlands", 
	"College ter Beoordeling van Geneesmiddelen ", 
	"http://www.cbg-meb.nl", 
	""),
array(
	"Чехия ", 
	"Институт за контрол на наркотиците", 
	"Czech Republic", 
	"State Institut for Drug Control", 
	"www.sukl.cz", 
	""),
array(
	"Чехия ", 
	"Институт за контрол на държавата над Ветеринарни биологини продукти и лекарства", 
	"Czech Republic", 
	"Institute for the State Control of Veterinary Biologicals and Medicaments", 
	"www.uskvbl.cz", 
	""),
array(
	"Швеция", 
	"Агенция по медицински продукти ", 
	"Sweden", 
	"Läkemedelsverket", 
	"www.lakemedelsverket.se", 
	""),
);
	
?>
<div id="rss_news">
<?php

	$offset = 0;
	if($site_version == "en") $offset = 2;
	$counter = 1;
	$news_content = "";
	/*
	if(isset($nid) && $rss_feeds[$nid][5] != "")
		{
			$url = $rss_feeds[$nid][5];
			$rss = simplexml_load_file($url);
			echo "<div class=\"item round\"><h2 class=\"head_text\">".$rss_feeds[$nid][$offset]."</h2><p>".$rss_feeds[$nid][$offset+1]."</p></div>";
			foreach ($rss->channel->item as $item) 
				{
					if($counter>20) break;
					$news_title = iconv('UTF-8','CP1251', $item->title);
					//$news_title = $item->title;
					//$news_description = strip_tags(mb_convert_encoding($item->description, 'auto'), "<a><p>");
					$news_description = strip_tags($item->description, "<a><p>");
					$news_image = "";
					if(strlen($news_description) > 10)
					{
					$news_content = 
					"<div class=\"news round\">
					<h2 class=\"news_title\"><a href=\"".$item->link."\">".$news_title."</a></h2><span class=\"date\">".$item->pubDate."</span>
					<div class=\"content\">".$news_description."<a href=\"".$item->link."\" class=\"next_link\" target=\"_blank\">".TEXT_READ_MORE."</a></div>
					</div>".$news_content;	
					$counter++;			
					}
				}
			
			echo $news_content;			
		}
	else
*/
	for($i=0; $i<count($rss_feeds); $i++)
		{
			
			$country = $rss_feeds[$i][$offset];
			$title = $rss_feeds[$i][$offset+1];
			
			if($site_version == "bg")
				{
					$country = iconv("UTF-8", "CP1251", $rss_feeds[$i][$offset]);
					$title = iconv("UTF-8", "CP1251", $rss_feeds[$i][$offset+1]);
				}
			$url = str_replace("http://", "", $rss_feeds[$i][4]);

			echo "<div class=\"item round\">";
			
			echo "<h2 class=\"head_text\">".$country."</h2>";
			//if($rss_feeds[$i][5] != "")
				//echo "<div class=\"content\">".$title."<a href=\"".$o_page->get_pLink()."&nid=$i\" class=\"next_link\">".TEXT_READ_NEWS."</a></div>";
			//else
				echo "<div class=\"content\">".$title."<a href=\"http://".$url."\" target=\"_blank\" class=\"next_link\">".TEXT_READ_MORE."</a></div>";
			echo "</div>";
				
			//echo "<li><a href=\"".$item->link."\" target=\"blank\">".$news_title."</a><br><span class=\"date\">".$item->pubDate."</span><br>".$news_image . $news_description."</li><br clear=\"all\">";
		}
?>
</div>