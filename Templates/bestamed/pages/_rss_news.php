<?php
	$rss_urls = array(
		216668 => "http://www.ema.europa.eu/ema/pages/rss/news.xml",
		217172 => "http://www.ages.at/?id=22485&feedUid=1&type=69862938",
		//216669 => "http://www.fagg-afmps.be/en/news/rss.jsp",
		216670 => "http://www.almp.hr/rss/novosti_eng.php",
		216671 => "http://www.sukl.eu/rss/en/23"	,
		217161 => "http://sundhedsstyrelsen.dk/en/Feeds/rss-feed",
		217162 => "http://www.ravimiamet.ee/en/rss",
		//217163 => "http://www.bfarm.de/SiteGlobals/Functions/RSSFeed/DE/Pressemitteilungen/RSSNewsfeed.xml?nn=3495390",
		//217164 => "http://www.ogyi.hu/modules/rss/rss.php?language=en",
		217167 => "http://www.imb.ie/rss.ashx?c=News",
		217168 => "https://www.agenziafarmaco.gov.it/services/rss/stampati-aggiornati.xml",
		217169 => "http://www.igz.nl/rss.aspx",
		//217170 => "http://www.infarmed.pt/rss/alertas.php",
		217171 => "http://www.lakemedelsverket.se/Nyheter-i-RSS-format/Medical-Products-Agency-Sweden---NEWS/"
	);
	
	$url_keys = array_keys($rss_urls);
?>
<div id="rss_news">
<?php
	$counter = 1;
	$news_content = "";
	if(in_array($o_page->n, $url_keys))
		{
			$url = $rss_urls[$o_page->n];
			$rss = simplexml_load_file($url);
			
			foreach ($rss->channel->item as $item) 
				{
					if($counter>20) break;
					//$news_title = iconv('UTF-8','CP1251', $item->title);
					$news_title = $item->title;
					//$news_description = strip_tags(iconv('UTF-8','CP1251', $item->description), "<a><p>");
					$news_description = strip_tags($item->description, "<a><p>");
					$news_image = "";
					if(strlen($news_description) > 10)
					{
					$news_content = 
					"<div class=\"news round\">
					<h2 class=\"news_title\"><a href=\"".$item->link."\">".$news_title."</a></h2><span class=\"date\">".$item->pubDate."</span>
					<div class=\"content\">".$news_description."<a href=\"".$item->link."\" class=\"next_link\" target=\"_blank\">read the news</a></div>
					</div>".$news_content;	
					$counter++;			
					}
				}
			
			echo $news_content;			
		}
	else
	for($i=0; $i<count($url_keys); $i++)
		{
			$url = $rss_urls[$url_keys[$i]];
			$rss = simplexml_load_file($url);				
			$item = $rss->channel->item;
			//$news_description = strip_tags(iconv('UTF-8','CP1251', $item->description), "<a><p>");
			$news_description = strip_tags($item->description, "<a><p>");
			//$news_title = iconv('UTF-8','CP1251', $item->title);
			$news_title = $item->title;
			if(strlen($news_description) < 5) $news_description = $news_title;

			echo "<div class=\"item round\">";
			echo "<div class=\"counter\">".($i+1)."</div>";
			echo "<h2 class=\"head_text\"><a href=\"".$o_page->get_pLink($url_keys[$i])."\">".$o_page->get_pName($url_keys[$i])."</a></h2><span class=\"date\">".$item->pubDate."</span>";
			echo "<div class=\"content\">".strip_tags(cut_text($news_description, 200))."<a href=\"".$o_page->get_pLink($url_keys[$i])."\" class=\"next_link\">view more news</a></div>";
			echo "</div>";
				
			//echo "<li><a href=\"".$item->link."\" target=\"blank\">".$news_title."</a><br><span class=\"date\">".$item->pubDate."</span><br>".$news_image . $news_description."</li><br clear=\"all\">";
		}
?>
</div>