<style>
#column_right {
	display: none;
}
#pageContent {
	width: 100%;
}
</style>
<div id="team">
<div class="coll">
<?php 
	//$subpages = $o_page->get_pSubpages(182355);
	$subpages = $o_page->get_pGroupContent($team_groups[0]);
	for($i=0; $i<count($subpages); $i++)
		{
			$class = "";
			if($subpages[$i]['SecLevel'] > 0)
				$class = "hidden";
			
			if($i== (count($subpages)-1))
				echo "<div class=\"tmember round\"></div>";	
			echo "<div class=\"tmember round $class\">";
			$o_page->print_pImage("130", "class=\"avatar\"", "", $subpages[$i]['n']);
			echo "<div style=\"float: left; width: 270px;\"><h2 class=\"head_text\"><a href=\"".$subpages[$i]['page_link']."\">".$subpages[$i]['Name']."</a></h2><span class=\"position\">".$subpages[$i]['title']."</span>";
			echo "<div class=\"content\">".$subpages[$i]['textStr']."<a href=\"".$subpages[$i]['page_link']."\" class=\"next_link\">".$o_site->get_sMoreText()."</a></div>";
			echo "</div></div>";
			if(($i+1)%2==0) echo "<br clear=\"all\">";
		}
?>
</div>

<div class="coll">
<?php 
	//$subpages = $o_page->get_pSubpages(182355);
	$subpages = $o_page->get_pGroupContent($team_groups[1]);
	for($i=0; $i<count($subpages); $i++)
		{
			$class = "";
			if($subpages[$i]['SecLevel'] > 0)
				$class = "hidden";
			
			if($i== (count($subpages)-1))
				echo "<div class=\"tmember round\"></div>";	
			echo "<div class=\"tmember round $class\">";
			$o_page->print_pImage("130", "class=\"avatar\"", "", $subpages[$i]['n']);
			echo "<div style=\"float: left; width: 270px;\"><h2 class=\"head_text\"><a href=\"".$subpages[$i]['page_link']."\">".$subpages[$i]['Name']."</a></h2><span class=\"position\">".$subpages[$i]['title']."</span>";
			echo "<div class=\"content\">".$subpages[$i]['textStr']."<a href=\"".$subpages[$i]['page_link']."\" class=\"next_link\">".$o_site->get_sMoreText()."</a></div>";
			echo "</div></div>";
			if(($i+1)%2==0) echo "<br clear=\"all\">";
		}
?>
</div>
</div>
<br clear="all">
<a name="forms"></a>
<div class="section special">
<?php
	if(isset($n_pharmacy_forms))
		{
			$o_page->print_pText($n_pharmacy_forms);
			//echo "<h2 class=\"title\">";
			//$o_page->print_pName(false, $n_pharmacy_forms);
			//echo "</h2><hr>";
			$o_page->print_pSubContent($n_pharmacy_forms, 1, true, array("CMS_MAIN_WIDTH"=>800));
			
		}
?>
</div>
