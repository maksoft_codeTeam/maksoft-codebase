<?php

require_once "modules/vendor/autoload.php";

use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\EmailField;
use Maksoft\Form\Fields\TextAreaField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Fields\FileInputField;
use Maksoft\Form\Exceptions\ValidationError;
use Maksoft\Form\Forms\DivForm;


class CareerForm extends DivForm
{

    public function __construct($form_data){
        $this->name = new TextField(array(
            "label"=>"*Name",
            "placeholder" => "Name",
            "name" => "name",
            "class"=>'form-control',
            "required" => True,
        ));

        $this->surname = new TextField(array(
            "label"=>"Surname",
            "name" => "surname",
            "placeholder" => "Surname",
            "class"=>'form-control',
        ));

        $this->phone = new TextField(array(
            "label"=>"*Phone",
            "name" => "phone",
            "class"=>'form-control',
            "placeholder" => "phone",
            "required" => True,
        ));

        $this->email = new EmailField(array(
            "label"=>"*Email",
            "name" => "email",
            "placeholder" => "Email",
            "class"=>'form-control',
            "required" => True,
        ));

        $this->city = new TextField(array(
            "label"=>"City",
            "name" => "name",
            "class"=>'form-control',
            "required" => True,
        ));

        $this->upload_cv = new FileInputField(array(
            "label"=>"Upload CV",
            "name" => "name",
            "class"=>'form-control',
        ));
        $this->submit = new SubmitButton(array("class"=>"btn btn-default"));
        parent::__construct($form_data);
    }


}
