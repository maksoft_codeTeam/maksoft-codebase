<?php
	$h_services = $o_page->get_pSubpages(182009);
?>
<div id="home">
<div id="banner">
	<a href="<?=$o_page->get_pLink(182008)?>" title="<?=$o_page->get_pName(182008)?>" class="arrow"><img src="<?=TEMPLATE_IMAGES?>arrow-big.png" alt=""></a>
	<div class="info">
        <h1>Trust is everything</h1>
        <p><big>Bestamed is a partner you can trust</big><br><br>Our purpose is to provide strategic solutions and services for companies operating in pharmacy channel. Give us your trust and we will deliver the best possible results.</p>
    	<a href="<?=$o_page->get_pLink(182008)?>" title="<?=$o_page->get_pName(182008)?>" class="link_more round">learn more</a>
    </div>
    <link rel="stylesheet" href="http://lib.maksoft.net/jquery/nivo-slider/themes/light/light.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="http://lib.maksoft.net/jquery/nivo-slider/nivo-slider.css" type="text/css" media="screen" />
    <div class="nivoSlider round2" id="banner-slider">
    <a href="<?=$o_page->get_pLink(182008)?>" title="<?=$o_page->get_pName(182008)?>"><img src="<?=TEMPLATE_IMAGES?>banners/banner_00.jpg" alt=""  class="round2"></a>
    <a href="<?=$o_page->get_pLink(182008)?>" title="<?=$o_page->get_pName(182008)?>"><img src="<?=TEMPLATE_IMAGES?>banners/banner_01.jpg" alt=""  class="round2"></a>
    <a href="<?=$o_page->get_pLink(182008)?>" title="<?=$o_page->get_pName(182008)?>"><img src="<?=TEMPLATE_IMAGES?>banners/banner_02.jpg" alt=""  class="round2"></a>
    <a href="<?=$o_page->get_pLink(182008)?>" title="<?=$o_page->get_pName(182008)?>"><img src="<?=TEMPLATE_IMAGES?>banners/banner_03.jpg" alt=""  class="round2"></a>
    <a href="<?=$o_page->get_pLink(182008)?>" title="<?=$o_page->get_pName(182008)?>"><img src="<?=TEMPLATE_IMAGES?>banners/banner_04.jpg" alt=""  class="round2"></a>
    </div>
    <script type="text/javascript" src="http://lib.maksoft.net/jquery/nivo-slider/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $j(window).load(function() {
      $j('#banner-slider').nivoSlider({
            effect: 'fade', // Specify sets like: 'fold,fade,sliceDown'
            slices: 15, // For slice animations
            boxCols: 8, // For box animations
            boxRows: 4, // For box animations
            animSpeed: 500, // Slide transition speed
            pauseTime: 5000, // How long each slide will show
            startSlide: 0, // Set starting Slide (0 index)
            directionNav: false, // Next & Prev navigation
            controlNav: false, // 1,2,3... navigation
            controlNavThumbs: false, // Use thumbnails for Control Nav
            pauseOnHover: false, // Stop animation while hovering
            manualAdvance: false, // Force manual transitions
            prevText: 'Prev', // Prev directionNav text
            nextText: 'Next', // Next directionNav text
            randomStart: false, // Start on a random slide
            beforeChange: function(){}, // Triggers before a slide transition
            afterChange: function(){}, // Triggers after a slide transition
            slideshowEnd: function(){}, // Triggers after all slides have been shown
            lastSlide: function(){}, // Triggers when last slide is shown
            afterLoad: function(){} // Triggers when slider has loaded
        });
    });
    </script>             
    <!--
    <div>
    <a href="<?=$o_page->get_pLink(182008)?>" title="<?=$o_page->get_pName(182008)?>"><img src="<?=TEMPLATE_IMAGES?>banners/banner_00.jpg" alt=""  class="round2"></a>
    </div>
    //-->
    
</div>
<div id="pageContent">
 	<div class="coll round" style="background: url(<?=TEMPLATE_IMAGES?>img_001.jpg) 50% 100% no-repeat;">
    	<div class="content">
        <h3 class="c1"><?=$h_services[0]['Name']?></h3>
        <?=crop_text($h_services[0]['textStr'])?>
        </div>
        <a href="<?=$o_page->get_pLink($h_services[0]['n'])?>" class="link_more">learn more</a>
    </div>
    <div class="coll middle round" style="background: url(<?=TEMPLATE_IMAGES?>img_002.jpg) 50% 100% no-repeat;">
       	<div class="content">
        <h3 class="c2"><?=$h_services[1]['Name']?></h3>
        <?=crop_text($h_services[1]['textStr'])?>
        </div>
        <a href="<?=$o_page->get_pLink($h_services[1]['n'])?>" class="link_more">learn more</a>
    </div>
    <div class="coll round" style="background: url(<?=TEMPLATE_IMAGES?>img_003.jpg) 50% 100% no-repeat;">
        <div class="content">
        <h3 class="c3"><?=$h_services[2]['Name']?></h3>
        <?=crop_text($h_services[2]['textStr'])?>
        </div>
        <a href="<?=$o_page->get_pLink($h_services[2]['n'])?>" class="link_more">learn more</a>
    </div>
 	<br clear="all">   
	<?php
    ?>
 </div>
</div>