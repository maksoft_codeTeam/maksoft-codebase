<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php
		if($o_page->n == 234232)
			echo '<meta http-equiv="Content-Type" content="text/html; charset='.HTML_CHARSET.'" />';
		else
			echo '<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />'; 
	?>
<title><?=$Title?></title>
    <?php
		
    	//include meta tags
		include("Templates/meta_tags.php");
		//include template configurations
		include("Templates/bestamed/configure.php");
		$o_site->print_sConfigurations();
		$site_version = $o_site->get_slanguage();
		
		if($site_version == "bg") include_once TEMPLATE_DIR . "languages/bulgarian.php";
		else include_once TEMPLATE_DIR . "languages/english.php"; 
		/*
		if($user->AccessLevel > 0)
			{
				?>
					<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
				<?
			}
		else
			{
				?>
					<meta http-equiv="Content-Type" content="text/html; charset=<?=HTML_CHARSET?>" />				
				<?
			}
			*/
     ?>
	<!--<meta http-equiv="Content-Type" content="text/html; charset=<?=HTML_CHARSET?>" />//-->
    <link href="http://www.maksoft.net/Templates/bestamed/layout.css" id="layout-style" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/Templates/bestamed/base_style.css" id="base-style" rel="stylesheet" type="text/css" />
    <!--<link href="http://www.maksoft.net/lib/font-awesome/font-awesome.min.css" rel="stylesheet" type="text/css" />//-->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="http://www.maksoft.net/css/admin_classes.css" rel="stylesheet" type="text/css" />
    <!--<link href='http://www.maksoft.net/lib/font_face/open_sans/open_sans.css'>//-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <!--[if IE]>
    <link href='http://www.maksoft.net/Templates/bestamed/ie_style.css' rel='stylesheet' type='text/css'>
    <![endif]-->
    <link rel="shortcut icon" href="<?=TEMPLATE_IMAGES?>favicon.ico" type="image/x-icon">
    <script language="javascript" type="text/javascript" src="<?=TEMPLATE_DIR?>effects.js"></script>
<style type="text/css">
body,td,th {
	font-family: "Open Sans", sans-serif;
}
h1,h2,h3,h4,h5,h6 {
	font-family: "Open Sans", sans-serif;
}
</style>
</head>
<?php
	//top menu
	//$top_menu = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 5", "p.toplink = 1");
	$top_menu = $o_page->get_pSubpages($o_site->StartPage, "p.sort_n ASC LIMIT 7");
	
	//get site news
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 5");
	
	//get most viewed pages
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5");

	//get last added pages
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5");
	
	//team members
	$team = $o_page->get_pSubpages($n_team, "p.sort_n ASC LIMIT 5");
	
	//testimonials
	$testimonials = $o_page->get_pSubpages(182017, "RAND() LIMIT 1");
	
?>


<body class="<?=(($o_page->n == $o_site->StartPage) ? "hm":"")?>">
<div id="site_container">
    <div id="header">
        <div class="header-content" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
        	<?php include TEMPLATE_DIR."header.php"?>
        </div>
    </div>
	<div id="page_container" class="shadow" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
        <div class="main-content">
        <?php
			if($o_page->_page['SiteID'] == $o_site->_site['SitesID'] )
				if($o_page->n == $o_site->StartPage)
					include TEMPLATE_DIR."home.php";
				else
					include TEMPLATE_DIR."main.php";
			else
				include TEMPLATE_DIR."admin.php";
		?>
        </div>
    </div>
    <div id="footer">
    	<div class="footer-content" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
		<?php
			//include TEMPLATE_DIR . "footer.php";
			/* new footer with RINA logo */
			include TEMPLATE_DIR . "footer2.php";
		?>
        <br clear="all">
        <!--<div class="social-links">Follow us on: <a href="#" class="link fb"><span class="icon"></span>Facebook</a><a href="#" class="link tw"><span class="icon"></span>Twitter</a><a href="#" class="link rss"><span class="icon"></span>RSS</a>//-->
          <div class="copyrights"><div class="copyrights-content"><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a></div></div>
        <div class="social-links">web design & development: <a href="http://www.pmd-studio.com" target="_blank" title="Web design"><b>pmd</b>studio</a>
        </div>
        </div>
        <?php
			if($user->AccessLevel > 0)
			{
			?>
        <div id="navbar"><?=$o_page->print_pNavigation();?></div>
        	<?
			}
			?>
        
        
    </div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48819223-1', 'bestamed.com');
  ga('send', 'pageview');

</script>
</body>
</html>