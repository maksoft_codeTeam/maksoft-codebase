<?php
$n_icepower = 229218;

	define("HTML_CHARSET", "UTF-8");
	define("TEXT_READ_NEWS", "read news");
	define("TEXT_READ_MORE", "read more");
	
	/* FORM CAREERS */
		define("FORM_TITLE", "Apply for this position");
		define("LABEL_NAME", "Name");
		define("LABEL_SURNAME", "Surname");
		define("LABEL_PHONE", "Phone");
		define("LABEL_EMAIL", "E-mail");
		define("LABEL_CITY", "City");
		define("LABEL_UPLOAD_CV", "Upload CV");
		define("LABEL_APPLY", "Apply");
		define("LABEL_COPY_TEXT", "I want to receive a copy of E-mail from this application.");
		define("LABEL_CODE", "Security code");
		
		define("MESSAGE_CAREERS_SENT", "Your message has been sent ! Thank you.");
		
	/* MEDICAL FORM 01 */
	
		define("F1_TITLE_01","You fill in this message as");
		define("F1_TITLE_02","Reporter's data");
		define("F1_TITLE_03","Patient's data");
		define("F1_TITLE_04","Information about the suspected medicine");
		define("F1_TITLE_05","Information about suspected adverse reaction");
		
		/*section 1*/
		
		define("F1_S1_LABEL_01", "Patient (user)");
		define("F1_S1_LABEL_02", "Parent");
		define("F1_S1_LABEL_03", "Other non-medical staff");
		
		/*section 2*/
		
		define("F1_S2_LABEL_01", "Name");
		define("F1_S2_LABEL_02", "Telephone");
		define("F1_S2_LABEL_03", "Address");
		define("F1_S2_LABEL_04", "E-mail");
		
		define("F1_S2_TEXT_01", "<p class=\"help-block\">Please provide your contact details by filling in at least one of the following fields so that your message to accepted as valid; preferably - phone number.</p>");
		
		/*section 3*/
		
		define("F1_S3_LABEL_01", "Patient (initials)");
		define("F1_S3_LABEL_02", "Age");
		define("F1_S3_LABEL_03", "Weight");
		define("F1_S3_LABEL_04", "Height");
		define("F1_S3_LABEL_05", "Sex");
		define("F1_S3_LABEL_06", "Male");
		define("F1_S3_LABEL_07", "Female");
		
		/*section 4*/
		
		define("F1_S4_LABEL_01", "Pharmaceutical product's name");
		define("F1_S4_LABEL_02", "1. Pharmaceutical form (e.g. tablets), quantity (e.g. tablets of 250 mg)");
		define("F1_S4_LABEL_03", "2. Pharmaceutical company - holder of authorization");
		define("F1_S4_LABEL_04", "3. Batch number");
		define("F1_S4_LABEL_05", "4. For what purpose was prescribed the medicine?");
		define("F1_S4_LABEL_06", "5. How did you take the medicine? When, how many times and how many times a day / hour?");
		define("F1_S4_LABEL_07", "6. First administration");
		define("F1_S4_LABEL_08", "7. Last administration");
		define("F1_S4_LABEL_09", "Additional information about the medicine");
		define("F1_S4_LABEL_10", "Other concomitant medicine while the adverse reaction was experienced");
		
		/*section 5*/
		
		define("F1_S5_LABEL_00", "Description of reaction");
		define("F1_S5_LABEL_01", "1. When the adverse reaction did started and how long was it experienced?");
		define("F1_S5_LABEL_02", "2. Were any measure to treat the reaction undertaken and what were they?");
		define("F1_S5_LABEL_03", "3. What is the outcome of the adverse reaction?");
			define("F1_S5_LABEL_03_OP01", "recovery");
			define("F1_S5_LABEL_03_OP02", "recovery with consequences");
			define("F1_S5_LABEL_03_OP03", "undergoing recovery");
			define("F1_S5_LABEL_03_OP04", "continuing adverse reaction");
			define("F1_S5_LABEL_03_OP05", "the adverse reaction caused. death");
			define("F1_S5_LABEL_03_OP06", "unknown");
			define("F1_S5_LABEL_03_OP07", "other");
			
		define("F1_S5_LABEL_04", "4. Please describe to what extent the adverse reaction affected your daily routine");
			define("F1_S5_LABEL_04_OP01", "cause some discomfort");
			define("F1_S5_LABEL_04_OP02", "caused disability");
			define("F1_S5_LABEL_04_OP03", "caused hospitalization");
			define("F1_S5_LABEL_04_OP04", "caused life-saving actions");
			define("F1_S5_LABEL_04_OP05", "caused invalidity");
			define("F1_S5_LABEL_04_OP06", "other");
			
		define("F1_S5_LABEL_05", "5. Possible reason for the adverse reaction");
			define("F1_S5_LABEL_05_OP01", "Yes");
			define("F1_S5_LABEL_05_OP02", "No");
			
		define("F1_S5_LABEL_06", "6. Additional information");
		define("F1_S5_LABEL_07", "7. Contact date with practising doctor");
		define("F1_S5_LABEL_08", "Anti-spam code");
		
		define("F1_S5_TEXT_01", "<p class=\"help-block\">Do you admit that the experienced adverse reaction was caused by a non-intentional mistake at the time of prescription, distribution at the pharmacy, preparation or administration of the pharmaceutical product and it so, please explain?</p>");
		define("F1_S5_TEXT_02", "<p class=\"help-block\">Please, explain the reason in your opinion, which resulted in the experienced adverse reaction non-intentional mistake at the time of prescription, distribution at the pharmacy, preparation or administration of the pharmaceutical product.</p>");
		define("F1_S5_TEXT_03", "<p class=\"help-block\">Results from tests, other diseases, allergies, smoking, abuse with medicinal products, alcohol, drugs, pregnancy, breastfeeding, etc.</p>");
		define("F1_S5_TEXT_04", "<p class=\"help-block\">If you agree that we may search for additional information on your case by your treatment doctor, please, fill in his contact data</p>");
		define("F1_S5_TEXT_05", "<p class=\"help-block\">Fill in generated symbols.<br>Code is lower and upper case sensitive.</p>");		

		define("BUTTON_SEND", "Send message");

	
	/* MEDICAL FORM 02 */

		define("F2_TITLE_01","Patient information");
		define("F2_TITLE_02","Adverse reactions (AR) ");
		define("F2_TITLE_03","Suspected medicinal product");
		define("F2_TITLE_04","Other medicinal product");
		define("F2_TITLE_05","Results, condition");
		define("F2_TITLE_06","Comment (history data, allergies, treatment of AR)");
		define("F2_TITLE_07","Reporter's information");
	
		/*section 1*/
		
		define("F2_S1_LABEL_01", "Patient (initials)");
		define("F2_S1_LABEL_02", "Age");
		define("F2_S1_LABEL_03", "Sex");
			define("F2_S1_LABEL_03_OP1", "Male");
			define("F2_S1_LABEL_03_OP2", "Female");
			
		/*section 2*/
		
		define("F2_S2_LABEL_01", "Short description");
		define("F2_S2_LABEL_02", "Duration (from - to)");
	
		
		/*section 3*/
		
		define("F2_S3_LABEL_01", "Trade name");
		define("F2_S3_LABEL_02", "Med. Form/ dosage unit");
		define("F2_S3_LABEL_03", "Daily dosage");
		define("F2_S3_LABEL_04", "Method of administration");
		define("F2_S3_LABEL_05", "Duration of administration");
		define("F2_S3_LABEL_06", "Indications");
		define("F2_S3_LABEL_07", "Product");
		define("F2_S3_LABEL_08", "from");
		define("F2_S3_LABEL_09", "to");
		
		/*section 4*/
		
		define("F2_S4_LABEL_01", "Trade name");
		define("F2_S4_LABEL_02", "Med. Form/ dosage unit");
		define("F2_S4_LABEL_03", "Daily dosage");
		define("F2_S4_LABEL_04", "Method of administration");
		define("F2_S4_LABEL_05", "Duration of administration");
		define("F2_S4_LABEL_06", "Indications");
		define("F2_S4_LABEL_07", "Product");
		define("F2_S4_LABEL_08", "from");
		define("F2_S4_LABEL_09", "to");
		
		/*section 5*/
		
		define("F2_S5_LABEL_01", "The suspected medicinal product");
			define("F2_S5_LABEL_01_OP1", "was discontinued");
			define("F2_S5_LABEL_01_OP2", "treatment continues");
			define("F2_S5_LABEL_01_OP3", "reduced dosage");
			define("F2_S5_LABEL_01_OP4", "unknown");
			
		define("F2_S5_LABEL_02", "Has the patient used this medicine before?");
			define("F2_S5_LABEL_02_OP1", "yes");
			define("F2_S5_LABEL_02_OP2", "no");
			define("F2_S5_LABEL_02_OP3", "unknown");
			
		define("F2_S5_LABEL_03", "AR has resulted in");
			define("F2_S5_LABEL_03_OP1", "hospitalization");
			define("F2_S5_LABEL_03_OP2", "prolonged hospitalization");
			define("F2_S5_LABEL_03_OP3", "life-endangering situation");
			define("F2_S5_LABEL_03_OP4", "congenital anomalies");
			define("F2_S5_LABEL_03_OP5", "significant/ permanent impairments");
			define("F2_S5_LABEL_03_OP6", "other of medical significance / requiring intervention");
			define("F2_S5_LABEL_03_OP7", "none of the above");
			
		define("F2_S5_LABEL_04", "AR outcome");
			define("F2_S5_LABEL_04_OP1", "healed without consequences");
			define("F2_S5_LABEL_04_OP2", "AR was treated");
			define("F2_S5_LABEL_04_OP3", "AR has not been treated");
			define("F2_S5_LABEL_04_OP4", "healed with consequences");
			define("F2_S5_LABEL_04_OP5", "treatment of AR still continues");
			define("F2_S5_LABEL_04_OP6", "unknown");
			define("F2_S5_LABEL_04_OP7", "death - date");
			
		/*section 6*/
		
		define("F2_S6_LABEL_01", "");
			define("F2_S6_LABEL_01_OP1", "hypersensitivity");
			define("F2_S6_LABEL_01_OP2", "drugs");
			define("F2_S6_LABEL_01_OP3", "pregnancy");
			define("F2_S6_LABEL_01_OP4", "smoking");
			define("F2_S6_LABEL_01_OP5", "alcohol");
			
		define("F2_S6_LABEL_02", "Connection between suspected medical product and adverse reaction");
			define("F2_S6_LABEL_02_OP1", "certain");
			define("F2_S6_LABEL_02_OP2", "uncertain");
			define("F2_S6_LABEL_02_OP3", "possible");
			define("F2_S6_LABEL_02_OP4", "impossible");
			define("F2_S6_LABEL_02_OP5", "conditional");
			define("F2_S6_LABEL_02_OP6", "unconditional");
			
		define("F2_S6_LABEL_03", "Additional information: <small>(if proposed boxes are not sufficient)</small>");	

		/*section 7*/
		
		define("F2_S7_LABEL_01", "Reporter's name");
		define("F2_S7_LABEL_02", "Specialty");
		define("F2_S7_LABEL_03", "Address");
		define("F2_S7_LABEL_04", "Telephone");
		define("F2_S7_LABEL_05", "Date");
		define("F2_S7_LABEL_06", "Enter the generated symbols. <br> Code is sensitive to uppercase and lowercase letters.");
		define("F2_S7_LABEL_07", "Anti-spam code");
		
		define("BUTTON_SEND2", "Send message");			
?>