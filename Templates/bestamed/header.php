<div class="banner">
	<a href="http://<?=$o_site->_site['primary_url']?>" title="<?=$o_page->get_pTitle("strict", $o_site->_site['StartPage'])?>"><img src="<?=$tmpl_config['default_banner']?> "class="banner-image" alt=""></a>
	<div class="languages">
		<?php $o_site->print_sVersions(); ?>
	</div>
</div>
<div id="top_menu" class="round">
	<?
		//$o_page->print_pName(true, $o_site->_site['StartPage']);
		for($i=0; $i<count($top_menu); $i++)
			$o_page->print_pName(true, $top_menu[$i]['n']);
	?>

	<a href="javascript: void(0)" class="search" id="open-search"><span class="fa fa-search"></span></a>
</div>
<div id="search" class="home-search round">
	<form method="get">
		<input name="SiteID" type="hidden" id="SiteID" value="<?=$o_site->SiteID?>">
		<input name="n" type="hidden" id="n" value="<?=(($n_search)? $n_search: $o_page->n)?>">		
		<input type="text" placeholder="<?=$o_site->get_sSearchText()?>" value="" name="search">
	</form>
</div>