<?php
	$links = $o_page->get_pSubpages($n_about);
	if(!DEMO_MODE && count($links)>0)
	{
?>
<!-- ABOUT US //-->
<h2 class="title"><?=$o_page->print_pLink($n_about)?></h2>
<div class="accordion">
    <?php
		for($i=0; $i<count($links); $i++)
			{
				?>
				<h3>
					<span>
						<p class="">
							<?=$o_page->get_pName($links[$i]['n'])?>
						</p> 
					</span>
				</h3>
				<div class="accord_cont">
					<?=cut_text(strip_tags($links[$i]['textStr']))?>
                    <a href="<?=$o_page->get_pLink($links[$i]['n'])?>"><?=$o_site->get_sMoreText()?></a>
				</div>
				<?php
			}
	?>
</div>  
<div class="clear"></div>  
<?
	}
	else include_once TEMPLATE_DEMO . "box_about.html";
?>