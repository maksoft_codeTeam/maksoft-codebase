<?php
	$location = $o_page->get_page($n_info_map);
	if(!DEMO_MODE && $n_info_map)
	{
?>
<!-- LOCATION //-->
    <h2 class="title"><?=$o_page->print_pLink($n_info_map);?></h2>
    <? $o_page->print_pImage(570, "", "", $n_info_map)?>
    <div class="location-info">
        <div class="info">
            <p>
				<?php
					$subpages = $o_page->get_pSubpages($n_info_map);
					$psubpageN = $subpages[0]["n"];
					?>
					<div class='bolded'>
						<strong>
							<?php
							 $o_page->print_pLink($subpages[0]["n"])
							?>
						</strong>
						<?=$o_page->print_pContent($psubpageN);?>
					</div>
			</p>
        </div>
    </div>
<?
	}
	else include_once TEMPLATE_DEMO . "box_location.html";
?>