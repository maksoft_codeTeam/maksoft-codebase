<?php
	$items = $o_page->get_pSubpages($n_latest_posts);
	if(!DEMO_MODE && count($items) > 0)
		{
			?>
			<!-- FEATURED WORKS //-->
            <h2 class="title"><?=$o_page->print_pLink($n_latest_posts);?></h2>
            <ul id="mycarousel" class="jcarousel-skin-tango">
                 <?php
				 	for($i=0; $i<count($items); $i++)
						{
							 ?>
                                <li class="recent_blog">
                                    <div class="img"><? $o_page->print_pImage(270, "", "", $items[$i]['n'])?></div>
                                    <div class="recent_text" style="height: 150px;">
                                        <? $o_page->print_pName(true, $items[$i]['n'], "class=\"latest_post_text1\"")?>
                                        <p><?=date($tmpl_config['date_format'], strtotime($items[$i]['date_modified']))?>   /  <? $o_page->print_pName(true, $items[$i]['ParentPage'], "class=\"latest_post_text2\"")?></p>
										<?=cut_text(strip_tags($items[$i]['textStr']))?>
                                        <a class="read_more" href="<?=$o_page->get_pLink($items[$i]['n'])?>"><?=$o_site->_site['MoreText']?></a>
                                        <div class="clear"></div>
                                    </div>
                                </li>
							<?php
						}
				?>
            </ul> 
			<!-- FEATURED WORKS //-->
			<?
		}
	else include_once TEMPLATE_DEMO . "box_latest_posts.html";
?>