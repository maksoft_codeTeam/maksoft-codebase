<?php
	define("TEMPLATE_NAME", "hotel_zamuka");
	define("TEMPLATE_DIR", "Templates/".TEMPLATE_NAME."/");
	define("TEMPLATE_PATH", "http://".$_SERVER['SERVER_NAME']."/Templates/".TEMPLATE_NAME."/");
	define("TEMPLATE_IMAGES", "http://www.maksoft.net/".TEMPLATE_DIR."img/");
	define("TEMPLATE_DEMO", "Templates/".TEMPLATE_NAME."/demo/");
	define("TEMPLATE_IMAGES_DEMO", "http://www.maksoft.net/".TEMPLATE_DIR."demo/img/");
	if($user->AccessLevel > 0)
		define("DEMO_MODE", false);
	else
		define("DEMO_MODE", false);

	//define some default values
	$tmpl_config = array(
		"default_logo" => TEMPLATE_IMAGES."logo.png",
		"default_logo_width" =>($o_site->get_sConfigValue('TMPL_LOGO_SIZE'))? $o_site->get_sConfigValue('TMPL_LOGO_SIZE') : "120",
		"default_main_width" => "100%",
		"footer" => array(
								TEMPLATE_DIR . "boxes/news.php"
							),		
		"date_format"=> "d.m.Y"
	);
	//$n_offers_group=230055;
	if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->get_sImageDIr()."/".basename($o_site->_site['Logo']);
?>