<!DOCTYPE html>
<?php
		if($o_page->_site['language_id'] == 1)  {
			include_once TEMPLATE_DIR."languages/bg.php";
			echo  '<html lang="bg">'; 
		}		
		if($o_page->_site['language_id'] == 2)  {
			include_once TEMPLATE_DIR."languages/en.php";
			echo  '<html lang="en">'; 
		}
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title><?=$Title?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<link href="<?=TEMPLATE_PATH?>css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" id="camera-css"  href="<?=TEMPLATE_PATH?>css/camera.css" type="text/css" media="all">
<link href="<?=TEMPLATE_PATH?>css/bootstrap.css" rel="stylesheet">
<link href="<?=TEMPLATE_PATH?>css/theme.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_PATH?>css/skins/tango/skin.css" />
<link href="<?=TEMPLATE_PATH?>css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?=TEMPLATE_PATH?>css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->    

</head>
<body>

	<!--header-->
    <div class="header">
    	<div class="header_in">
        	<div class="wrap">
                <div class="container">
                    <div class="page_full">
                        <div class="header_top drop-shadow">
                            <div class="container">
                                <div class="row">
                                    <div class="span2">
                                        <div class="logo">
											<a href="<?=$o_page->get_pLink($o_site->get_sStartPage())?>">
												<img src="<?=$tmpl_config['default_logo']?>" alt="<?=$o_site->_site['Title']?>" width="<?=$tmpl_config['default_logo_width']?>" />
											</a>
										</div>
										<div class="clear"></div>     
                                    </div>
									<div class="span7 margin-left-0 margin-right-20">
										<h1 class="lobster">
											<?=$o_site->_site['Title']?>
										</h1>
                                        <div class="clear"></div>
									</div>
                                    <div class="span3 followlinks">
										<ul>
											<li>
												<a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>">
													<i class="fa fa-facebook"></i>
												</a>
											</li>
											<li>
												<a href="skype:bg-thecastle?call"><?php //(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>
													<i class="fa fa-skype"></i>
												</a>
											</li>
											<li>
												<a href="#">
													<i class="fa fa-rss"></i>
												</a>
											</li>
										</ul>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="top_menu">
                        	<div class="fright">
                            	<div class="top_search">
                                    <form action="#" method="post">
                                        <input name="name" type="text" value="����� � �����" onfocus="if (this.value == '����� � �����') this.value = '';" onblur="if (this.value == '') this.value = '����� � �����';" />
                                        <input type="submit" class="search_btn" value=" " />
                                        <div class="clear"></div>
                                    </form> 
                                </div>
                            </div>
                            <div class="fleft">
                                <!--menu-->
								<?php
								//$arraymenu = get_menu($o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 5"), array('menu_depth'=>1));
								//var_dump($arraymenu);
								?>
                                <nav id="main_menu">
                                    <div class="menu_wrap">
                                    <?php print_menu($o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 7"),
										array(
											'menu_depth'=>1,
											'params'=>"class=\"nav sf-menu\"",
											'home'=>true
											)
										)?>
                                        <div class="clear"></div>
                                    </div>
                                 </nav>                        
                                <!--//menu --> 
								<script>
								jQuery(document).ready(function(){
									jQuery('.nav.sf-menu li').has('ul').addClass('sub-menu');
								})
								</script>
                            </div>
                            <div class="clear"></div>                       
                        </div>
                    </div>
                </div>
        	</div>           
        </div>    
    </div>
    <!--//header-->   
