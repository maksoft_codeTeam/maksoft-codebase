
<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.11.1.js"></script>
<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/2.2.1/js/bootstrap.min.js"></script>
<script src="http://www.maksoft.net/web/forms/holidaysInKeramotiReservForm/js/validator.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style>
	.span6 input, .span2 input, .span1 input, .span2 select, .span3 select, .span4 select, .span6 textarea{
		width:95%;
	}
	label{
		font-weight:600;
	}
	.help-block.with-errors{
		color:red;
		font-weight:bold;
	}
</style>
<script>
			  $(function() {
				$( "#arrival, #departure" ).datepicker({
					showAnim: "slideDown",
					dateFormat: "dd-mm-yy",
					dayNamesMin: [ "��", "��", "��", "��", "��", "��", "��" ],
					firstDay: 1,
					monthNames: [ "������", "��������", "����", "�����", "���", "���", "���", "������", "���������", "��������", "�������", "��������" ]
					}
				);
			  });
</script>
	<!--page_container-->
    <div class="page_container">
    	<div class="wrap">
        	<div class="container">
                <div class="page_full white_bg drop-shadow">
                    <div class="breadcrumb">
                        <div class="container">
						<?=$o_page->print_pName("true", $o_page->_page['ParentPage'])?><span>/</span><?=$o_page->print_pName()?>
                        </div>
                    </div>
                    <div class="container">
                        <div class="page_in">
                        	<div class="container">
                                <section>
                                    <div class="row">
                                        <div class="span12">
                                            <h2 class="title"><?=$o_page->print_pTitle("strict")?></h2>
												<div class="row">
													<div class="span8">
													<?php
														if($sent == 1) mk_output_message('normal','
															<div class="alert alert-success alert-dismissible" role="alert">
															  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															  <strong>'.TEXT_SUCCESS.'</strong> '.TEXT_SUCCESS_RESERVATION.'
															</div>');
													?>
													<form id="reservation_hotel_zamuka" class="form-horizontal" role="form" data-toggle="validator"  name="reservation_hotel_zamuka" method="post" action="/form-cms.php?<?php echo("n=$n&SiteID=$SiteID"); ?>">
														<fieldset>

															<!-- Form Name -->
															<legend></legend>

															<!-- Appended Input-->
															<div class="row">
																<div class="form-group">
																  <label class="span4 control-label" for="Name"><?=TEXT_NAMES;?></label>
																  <div class="span6">
																	<div class="input-group">
																	  <input id="Name" name="Name" class="form-control" placeholder="<?=TEXT_NAMES;?>" type="text" required data-error="<?=TEXT_PLEASE_FILL_NAMES;?>">
																	  <span class="input-group-addon">*</span>
																	</div>
																		<div class="help-block with-errors"></div>
																  </div>
																</div>
															</div>
															<br>
															<!-- Appended Input-->
															<div class="row">
																<div class="form-group">
																  <label class="span4 control-label" for="Firma"><?=TEXT_FIRM;?></label>
																  <div class="span6">
																	<div class="input-group">
																	  <input id="Firma" name="Firma" class="form-control" placeholder="<?=TEXT_FIRM;?>" type="text">
																	</div>
																  </div>
																</div>
															</div>
															<br>
															<!-- Text input-->
															<div class="row">
																<div class="form-group">
																  <label class="span4 control-label" for="bulstat"><?=TEXT_BULSTAT;?></label>  
																  <div class="span6">
																  <input id="bulstat" name="bulstat" type="text" placeholder="<?=TEXT_BULSTAT;?>" class="form-control input-md">
																	
																  </div>
																</div>
															</div>
															<br>
															<!-- Appended Input-->
															<div class="row">
																<div class="form-group">
																  <label class="span4 control-label" for="Phone"><?=TEXT_PHONE;?></label>
																  <div class="span6">
																	<div class="input-group">
																	  <input id="Phone" name="Phone" class="form-control" placeholder="<?=TEXT_PHONE;?>" type="text" required data-error="<?=TEXT_FILL_PHONE;?>">
																	  <span class="input-group-addon">*</span>
																	</div>
																		<div class="help-block with-errors"></div>
																  </div>
																</div>
															</div>
															<br>
															<!-- Text input-->
															<div class="row">
																<div class="form-group">
																  <label class="span4 control-label" for="Address"><?=TEXT_ADDRESS;?></label>  
																  <div class="span6">
																  <input id="Address" name="Address" type="text" placeholder="<?=TEXT_ADDRESS;?>" class="form-control input-md">
																	
																  </div>
																</div>
															</div>
															<br>
															<!-- Text input-->
															<div class="row">
																<div class="form-group">
																
																  <label class="span4 control-label" for="EMail">E-mail</label>  
																  <div class="span6">
																  <div class="input-group">
																  <input id="EMail" name="EMail" type="email" placeholder="E-mail" class="form-control input-md" required data-error="<?=TEXT_INVALID_EMAIL;?>">
																  <span class="input-group-addon">*</span>
																  </div>
																  <div class="help-block with-errors"></div>
																  </div>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="form-group">
																  <label class="span4 control-label" for="arrival"><?=TEXT_ARRIVALDATE;?></label>
																  <div class="span6">
																		<input id="arrival" name="arrival" type="text" placeholder="<?=TEXT_ARRIVALDATE;?>" class="form-control">
																  </div>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="form-group">
																  <label class="span4 control-label" for="departure"><?=TEXT_DEPARTUREDATE;?></label>
																  <div class="span6">
																		<input id="departure" name="departure" type="text" placeholder="<?=TEXT_DEPARTUREDATE;?>" class="form-control">
																  </div>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="span4">
																	<div class="form-group">
																	  <label class="control-label" for="nights"><?=TEXT_NIGHTS;?></label>
																	  <div class="span2">
																			<input id="nights" name="nights" type="text" placeholder="" class="form-control">
																	  </div>
																	</div>
																</div>
																<div class="span4">
																	<div class="form-group">
																	  <label class="control-label" for="rooms"><?=TEXT_ROOMS;?></label>
																	  <div class="span2">
																			<input id="rooms" name="rooms" type="text" placeholder="" class="form-control">
																	  </div>
																	</div>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="span3">
																	<div class="form-group">
																	  <label class="control-label" for="adults"><?=TEXT_ADULTS;?></label>
																	  <div class="span1">
																			<input id="adults" name="adults" type="text" placeholder="" class="form-control">
																	  </div>
																	</div>
																</div>
																<div class="span5">
																	<div class="form-group">
																	  <label class="control-label" for="roomtype"><?=TEXT_ROOMTYPE;?></label>
																	  <div class="span3">
																			<select>
																				<?php
																				$type_keys = array_keys($room_types);
																				for($i=0; $i<count($room_types); $i++)
																				{
																				?>
																				<option value="<?=$type_keys[$i]?>"><?=$room_types[$type_keys[$i]]?></option>
																				<?
																				}
																				?>
																			</select>
																	  </div>
																	</div>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="span4">
																	<div class="form-group">
																	  <label class="control-label" for="childrenunder3">���� ���� �� 3 �� 12�.</label>
																	  <div class="span2">
																			<input id="childrenunder3" name="childrenunder3" type="text" placeholder="" class="form-control">
																	  </div>
																	</div>
																</div>
																<div class="span4">
																	<div class="form-group">
																	  <label class="control-label" for="childrenabove3">���� ���� �� 3�.</label>
																	  <div class="span2">
																			<input id="childrenabove3" name="childrenabove3" type="text" placeholder="" class="form-control">
																	  </div>
																	</div>
																</div>
															</div>
															<br>
															<div class="row">
															
																<!-- Message body -->
																<div class="form-group">
																  <label class="control-label span4" for="Zapitvane">������������ ����������</label>
																  <div class="span6">
																	<textarea class="form-control span6" id="Zapitvane" name="Zapitvane" placeholder="" rows="5"></textarea>
																  </div>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="form-group">
																  <label class="span4 control-label" for="codestr">��� �� ���������</label>
																  <div class="span6">
																	<?php
																		$code = rand(0, 65535);   
																		
																		echo("
																		<img src=\"http://www.maksoft.net/gen.php?code=$code\" align=\"middle\" vspace=5 hspace=5>
																		<input type=\"hidden\" name=\"code\" value=\"$code\">
																		"); 
																	?>
																	<div class="input-group">
																		<input name="codestr" type="text" id="codestr" class="form-control" required>
																		<span class="input-group-addon">*</span>
																	</div>
																		<div class="help-block with-errors"></div>
																  </div>
																</div>
															</div>
															<br>
															<div class="row">
															<input name="ConfText" type="hidden" id="ConfText" value="����������� �� � ����������!"> 
															<input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
															<input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
															<input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$Site->EMail"); ?>"> 
															<input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$Site->StartPage&SiteID=$SiteID&sent=1"); ?>">
															<input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
															<input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">
															<input type="submit" class="btn-sendit pull-right margin-right-50" value="�������">
															<input type="reset" class="btn-clearit pull-right margin-right-10" value="�������">
															</div>
														</fieldset>
													</form>

													</div>
													<div class="span4">
														<?php $o_page->print_pImage();?>
													</div>
												</div>
                                            
                                        </div>
                                     </div>
                                     <div class="row">
                                        <div class="span12">
                							<?=$o_page->get_pText()?>
                                        </div>                                  
                                    </div>
                                </section>
                            </div>
                            <br clear="all">
                            <?php $o_page->print_pNavigation()?>                        
                        </div>
                    </div>
                </div>
            </div>        	
        </div>
    </div>
    <!--//page_container-->