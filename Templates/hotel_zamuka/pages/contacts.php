    <!--page_container-->
    <div class="page_container">
    	<div class="wrap">
        	<div class="container">
                <div class="page_full white_bg drop-shadow">
                    <div class="breadcrumb">
                        <div class="container">
						<?=$o_page->print_pName("true", $o_page->_page['ParentPage'])?><span>/</span><?=$o_page->print_pName()?>
                        </div>
                    </div>
                    <div class="container">
                        <div class="page_in">
                        	<div class="container">
                                <section>
                                    <div class="row">
                                        <div class="span6">
                                            <h2 class="title"><?=$o_page->print_pTitle("strict")?></h2>
                                            <div id="map">
                                            	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d23655.847245388715!2d27.84455!3d42.17209!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf2e958f5814895e4!2z0KXQntCi0JXQmyDQl9CQ0JzQqtCa0JA!5e0!3m2!1sbg!2sbg!4v1427122529033" width="600" height="400" frameborder="0" style="border:0"></iframe>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <div class="contact_form">  
                                                <?php
												if($sent)
												{
												?>
                                                <div class="alert alert-success">
                                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                  <strong>Message:</strong> Your message has been sent successfully!
                                                </div>
                                                <?php
												}
												?>
                                                <div id="fields">
                                                <form id="ajax-contact-form" action="form-cms.php" method="post">
                                                    <input class="span6" type="text" name="name" value="" placeholder="<?=LABEL_NAME?>" />
                                                    <input class="span6" type="text" name="EMail" value="" placeholder="<?=LABEL_EMAIL?>" />
                                                    <input class="span6" type="text" name="subject" value="" placeholder="<?=LABEL_SUBJECT?>" />
                                                    <textarea id="message2" name="message" class="span6" placeholder="<?=LABEL_MESSAGE?>"></textarea>
                                                    <div class="clear"></div>
                                                    <br>
                                                    <input type="reset" class="btn-clearit" value="<?=BUTTON_CLEAR?>" />
                                                    <input type="submit" class="btn-sendit" value="<?=BUTTON_SUBMIT?>" />
                                                    <div class="clear"></div>
                                                    <br>
                                                        
                                                        <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
                                                        <input name="SiteID" type="hidden" id="SiteID" value="<?=$o_site->SiteID?>">
                                                        <input name="ToMail" type="hidden" id="ToMail" value="<?=$o_site->_site['EMail']?>"> 
                                                        <input name="conf_page" type="hidden" id="conf_page " value="<?=$o_page->get_pLink()?>&sent=1">
                                                </form>
                                                </div>
                                            </div>                   
                                        </div>
                                        <div class="clear"></div>
										<?=$o_page->get_pText()?>
										<!--
                                        <div class="clear"></div>
                                        <div class="span3 contact_text" style="border-top: 1px solid #2f6346"><?=$o_page->get_pText()?></div>
                                        <div class="span3" style="border-top: 1px solid #2f6346"></div>
                                        <div class="span6" style="border-top: 1px solid #2f6346"><br><?=$o_page->get_pText($n_about)?></div>
										-->                                        
                                    </div>
                                </section>
                            </div>
                            <br clear="all">
                            <?php $o_page->print_pNavigation()?>                        
                        </div>
                    </div>
                </div>
            </div>        	
        </div>
    </div>
    <!--//page_container-->