<?php
	//define some pages
	
	//terms & conditions page
	$n_terms = 229437;
	
	//home welcome box - top offers
	$n_offers = 230055;
	$n_offers_group = 763;
	
	//about page
	$n_about = 229414;
	
	//landmarks page
	$n_latest_posts = 229416;

	//info map (CONTACTS)
	$n_info_map= 229420;
	
	//info map2 (SPA & WELLNESS)
	$n_info_map2= 229415;

	//room gallery (LUX APPARTMENTS)
	$n_featured_works = 231855;

	//footer gallery Group
	$n_gallery = 765;
	
	$n_reservation = 232236;
	
	
	$n_commentSection = 232595;
		
	//reservation form
	define("LABEL_WEATHER_IN", "������� � %s");
	define("LABEL_VIEW_ALL_NEWS", "��� ������");

	define("TITLE_RESERVATION", "����������");
	define("LABEL_DATE_ARIVAL", "����������");
	define("LABEL_DATE_DEPARTURE", "����������");
	define("LABEL_ROOM_TYPE", "��� ����");
	define("LABEL_ADULTS", "���������");
	define("LABEL_KIDS1", "���� 0-6");
	define("LABEL_KIDS2", "���� 7-12");
	define("LABEL_COMMENTS", "���������");	
	define("LABEL_NAME", "������ ���");
	define("LABEL_EMAIL", "����� e-mail");
	define("LABEL_PHONE", "�������");
	define("LABEL_CODE", "���");
	define("LABEL_ALL", "������");
	define("LABEL_SUBJECT", "����");
	
	define("LABEL_POLL", "������");
			
	define("MESSAGE_MANDANTORY_RESERVATION_FIELDS","<strong>��������!</strong> ������ ������ � * �� ������������!");
	
	define("BUTTON_NEXT", "������");
	define("BUTTON_BACK", "�����");
	define("BUTTON_BOOK_NOW", "����������");
	define("BUTTON_CLEAR", "�������");
	define("BUTTON_SUBMIT", "�������");
	define("BUTTON_BACK_TO", "����� ���");
	define("BUTTON_", "");
	
	define("TITLE_REQUEST_TRANSFER", "����� ��������");
	define("TITLE_VISA_INVITATION", "������ �� ����");
	
	$room_types = array(
	"" => "- �������� ���� -",
	"vip" => "��� ����������", 
	"studio" => "������", 
	"standart-room" => "���������� ����", 
	"economy-room" => "���������� ����"
	);
	
	//contact form
	define("LABEL_MESSAGE", "���������");
	
	//footer content
	define("TITLE_NEWS", "������");
	define("TITLE_COMMENTS", "������");
	define("TITLE_CONTACTS", "������ ��");
	define("TITLE_CONTACT", "��������");
	define("TITLE_GALLERY", "�������");
	
	
	define("TEXT_SUCCESS", "�����!");
	define("TEXT_SUCCESS_RESERVATION", "������ ������ � ��������� �������!");
	define("TEXT_NAMES", "��� � �������");
	define("TEXT_PLEASE_FILL_NAMES", "����, ��������� ��� � �������");
	define("TEXT_FIRM", "�����");
	define("TEXT_FILL_FIRM", "����, ��������� �����");
	define("TEXT_BULSTAT", "�������");
	define("TEXT_PHONE", "�������");
	define("TEXT_FILL_PHONE", "����, ��������� �������");
	define("TEXT_ADDRESS", "����� �� ��������������");
	define("TEXT_ARRIVALDATE", "���� �� ����������");
	define("TEXT_DEPARTUREDATE", "���� �� ����������");
	define("TEXT_NIGHTS", "���� �������");
	define("TEXT_ROOMS", "���� ����");
	define("TEXT_ADULTS", "���� ���������");
	define("TEXT_ROOMTYPE", "��� ����");
	define("TEXT_INVALID_EMAIL", "��������� EMail");

	/*
	define("TEXT_NEWSLETTER_CONTENT", "<p>���������� ����� �� ������ �� �������, �������� � �����������. �������� �� �� ����� ��������!</p>");
	define("TEXT_NEWSLETTER_CONTENT2", "<em>�������� ���, �� �� �������� �� ���� ������� �� ��������� ��������� ��������� �� <a href=\"#\">����� �����</a></em>");
	define("TEXT_PAGE_VISITS","<b>%d</b> ������������");
	define("TEXT_VIEW_ALL", "��� ������");
	
	//home content
	define("HEADING_WELCOME", "���� ������ �� ������� ������ � �� ��������?");
	define("HEADING_WELCOME_TEXT", "*     *     *<br><span>����� ����� - �������������� ����� �� ������ � <br>�����������</span>");
	define("TITLE_COMMENTS", "���������");
	define("TITLE_FEATURED_WORKS", "������� ����");
	
	define("LABEL_PART_OF", "���� ��");
	*/
?>