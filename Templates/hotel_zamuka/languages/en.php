<?php
	//define some pages
	
	//terms & conditions page
	$n_terms = 229437;
	
	//home welcome box - top offers
	$n_offers = 218328;
	$n_offers_group = 673;

	//about page
	$n_about = 218340;
	
	//landmarks page
	$n_latest_posts = 218348;

	//info map (CONTACTS)
	$n_info_map= 218351;
	
	//info map2 (SPA & WELLNESS)
	$n_info_map2= 216146;

	//room gallery (LUX APPARTMENTS)
	$n_featured_works = 206208;

	//footer gallery Group
	$n_gallery = 719;
		
	//reservation form
	define("LABEL_WEATHER_IN", "Weather in %s");
	define("LABEL_VIEW_ALL_NEWS", "View All");

	define("TITLE_RESERVATION", "Reservation");
	define("LABEL_DATE_ARIVAL", "arrival");
	define("LABEL_DATE_DEPARTURE", "departure");
	define("LABEL_ROOM_TYPE", "room type");
	define("LABEL_ADULTS", "adults");
	define("LABEL_KIDS1", "children 0-6");
	define("LABEL_KIDS2", "children 7-12");
	define("LABEL_COMMENTS", "Comments");	
	define("LABEL_NAME", "name");
	define("LABEL_EMAIL", "e-mail");
	define("LABEL_PHONE", "phone");
	define("LABEL_CODE", "post code");
	define("LABEL_ALL", "all");
	define("LABEL_SUBJECT", "subject");

	define("LABEL_POLL", "Poll");
			
	define("MESSAGE_MANDANTORY_RESERVATION_FIELDS","<strong>Important!</strong> All fields marked with  * are mandatory!");
	
	define("BUTTON_NEXT", "<?=$o_page->_site['forward']?>");
	define("BUTTON_BACK", "<?=$o_page->_site['Back']?>");
	define("BUTTON_BOOK_NOW", "Book");
	define("BUTTON_CLEAR", "Clear");
	define("BUTTON_SUBMIT", "Send");
	define("BUTTON_BACK_TO", "<?=$o_page->_site['Back']?>");
	define("BUTTON_", "");
	
	define("TITLE_REQUEST_TRANSFER", "Request transfer");
	define("TITLE_VISA_INVITATION", "Visa invitation");
	
	$room_types = array(
	"vip" => "Vip Apartment", 
	"studio" => "Studio", 
	"standart-room" => "Standart Room", 
	"economy-room" => "Economy Room"
	);
	
	//contact form
	define("LABEL_MESSAGE", "Message");
	
	//footer content
	define("TITLE_NEWS", "News");
	define("TITLE_COMMENTS", "COMMENTS");
	define("TITLE_CONTACTS", "CONTACTS");
	define("TITLE_GALLERY", "GALLERY");

	
	define("TEXT_SUCCESS", "�����!");
	define("TEXT_SUCCESS_RESERVATION", "������ ������ � ��������� �������!");
	define("TEXT_NAMES", "��� � �������");
	define("TEXT_PLEASE_FILL_NAMES", "����, ��������� ��� � �������");
	define("TEXT_FIRM", "�����");
	define("TEXT_FILL_FIRM", "����, ��������� �����");
	define("TEXT_BULSTAT", "�������");
	define("TEXT_PHONE", "�������");
	define("TEXT_FILL_PHONE", "����, ��������� �������");
	define("TEXT_ADDRESS", "����� �� ��������������");
	define("TEXT_ARRIVALDATE", "���� �� ����������");
	define("TEXT_NIGHTS", "���� �������");
	define("TEXT_ROOMS", "���� ����");
	define("TEXT_ADULTS", "���� ���������");
	define("TEXT_ROOMTYPE", "��� ����");

	/*
	define("TEXT_NEWSLETTER_CONTENT", "<p>���������� ����� �� ������ �� �������, �������� � �����������. �������� �� �� ����� ��������!</p>");
	define("TEXT_NEWSLETTER_CONTENT2", "<em>�������� ���, �� �� �������� �� ���� ������� �� ��������� ��������� ��������� �� <a href=\"#\">����� </a></em>");
	define("TEXT_PAGE_VISITS","<b>%d</b> ������������");
	define("TEXT_VIEW_ALL", "��� ������");
	
	//home content
	define("HEADING_WELCOME", "���� ������ �� ������� ������ � �� ��������?");
	define("HEADING_WELCOME_TEXT", "*     *     *<br><span>����� ����� - �������������� ����� �� ������ � <br>�����������</span>");
	define("TITLE_COMMENTS", "���������");
	define("TITLE_FEATURED_WORKS", "Gallery Rooms");
	
	define("LABEL_PART_OF", "���� ��");
	*/
?>