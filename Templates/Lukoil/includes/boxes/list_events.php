<?php
	//edit players
	if($user->WriteLevel >= $page->AccessLevel && $user->WriteLevel > 0)
		include DIR_WS_ADMIN . "events.php";
		
	//show team info
	if(isset($_GET['show_team']))
	{
		$tid = (int)$show_team;
		include_once DIR_WS_BOXES ."team_info.php";
	}
	
	if(!isset($event_tournament)) $event_tournament = "NBL";
	if(!isset($event_status)) $event_status = 1;
 
  $info_box_contents = array();
  //$team = tep_get_team_info($tid);
  //$info_box_contents[] = array('text' => BOX_HEADING_EVENTS . $team['team_name']. " [ ". $event_tournament." ]");
  $tournament_title = tep_get_tournament_info($event_tournament, "tournament_title");
  $info_box_contents[] = array('text' => ($event_status == 1 ? BOX_HEADING_EVENTS_UPCOMMING : BOX_HEADING_EVENTS_PASSED));
  new infoBoxHeading($info_box_contents, true, false, false);	

	
	
	$order_sql = "";
	switch($_GET['sort'])
		{
			case "event_round":
				{	
					$order_sql = "CAST(event_round AS UNSIGNED)";
					break;
				}
			case "event_date":
			case "event_place":
			case "event_home_team":
			case "event_away_team":
				{	
					$order_sql = $_GET['sort'];
					break;
				}
			default: $order_sql = "CAST(event_round AS UNSIGNED)";
		}

	//AND (event_score1 = 0 AND event_score2 = 0)
	
	$row_events_res = mysqli_query("SELECT * FROM ".TABLE_EVENTS." WHERE (event_home_team = '$tid' OR event_away_team = '$tid')  AND event_tournament = '$event_tournament' AND event_language_id = '$language_id' AND event_status = '".(int)$event_status."'  ORDER by $order_sql ASC ");

	$info_box_contents = array();

	$info_box_contents[] = array(array('text'=>"<h2>".$tournament_title."</h2>", 'params'=>"colspan=9"), 'params'=>"class=t1 align=center");
	
	$i=1;
	if(mysqli_affected_rows() >0)
	{	

	$info_box_contents[] = 
	array(
	array('text'=>"<a href=\"$local_url&sort=event_round\">".BOX_EVENTS_TEXT_ROUND."</a>"),  
	array('text'=>"<a href=\"$local_url&sort=event_date\">".BOX_EVENTS_TEXT_DATE."</a>"),
	array('text'=>BOX_EVENTS_TEXT_TIME), 
	array('text'=>"<a href=\"$local_url&sort=event_home_team\">".BOX_EVENTS_TEXT_HOME_TEAM."</a>"), 
	array('text'=>"<a href=\"$local_url&sort=event_away_team\">".BOX_EVENTS_TEXT_AWAY_TEAM."</a>"), 
	array('text'=>"<a href=\"$local_url&sort=event_place\">".BOX_EVENTS_TEXT_HALL."</a>"),
	array('text'=>"&nbsp;"), 
	array('text'=>"&nbsp;"), 
	array('text'=>"&nbsp;"), 
	'params'=>"class=t1 align=center");

		while($row_events = mysqli_fetch_object($row_events_res))
			{	
	
					if($user->WriteLevel >= $page->AccessLevel && $user->WriteLevel > 0)
					{
						$edit_link = "<a href=\"".$local_url."&edit_id=".$row_events->event_id."\" class=button_submit>edit</a>";
						$add_link = "<a href=\"".$local_url."&edit_id=0\">+ add event</a>";
						$del_link = tep_draw_form("del","","POST") . tep_draw_hidden_field("del_id", $row_events->event_id) . tep_draw_input_field("form_sumbit", " delete ", "class=button_submit onClick=\"return confirm('Are you sure ?')\"", "submit") . "</form>";
					}
				else 	
					{
						$edit_link="&nbsp;";
						$add_link="&nbsp;";
						$del_link = "&nbsp;";
					}
						
				
				$home_team = tep_get_team_info($row_events->event_home_team);
				$away_team = tep_get_team_info($row_events->event_away_team);
				
				if($home_team['team_id']==1) $home_team['team_name'] = "<b>".$home_team['team_name']."</b>";
				if($away_team['team_id']==1) $away_team['team_name'] = "<b>".$away_team['team_name']."</b>";
				
				if(($row_events->event_score1 != 0) && ($row_events->event_score2 != 0))
					$stats_more = "<a href=\"$local_url&eid=$row_events->event_id\">".$Site->MoreText."</a>";
				else
					if($row_events->event_status == 1)
						$stats_more = "<img src=\"".DIR_TEMPLATE_IMAGES."live_on.jpg\" alt=\"&raquo; ".BOX_TEXT_LIVE.$row_events->event_tv.", \n".$row_events->event_date ."\n". $row_events->event_time."\">";
					else $stats_more="&nbsp;";
				
				$info_box_contents[] =
				array(
					array('text' => "&nbsp;".$row_events->event_round, 'params'=>"valign=\"middle\" "),
					array('text' => "&nbsp;".$row_events->event_date, 'params'=>"valign=\"middle\" "),
					array('text' => date("H:i", strtotime($row_events->event_time)), 'params'=>"valign=\"middle\" "),
					array('text' =>"<a href=\"$local_url&show_team=$row_events->event_home_team\">". $home_team['team_name']."</a>",'params'=>"valign=\"middle\" "),
					array('text' =>"<a href=\"$local_url&show_team=$row_events->event_away_team\">". $away_team['team_name']."</a>",'params'=>"valign=\"middle\" "),
					array('text' =>"&nbsp;".$row_events->event_place, 'params'=>"valign=\"middle\" "),
					array('text' =>"&nbsp;".$stats_more, 'params'=>"valign=\"middle\" "),
					array('text' =>$edit_link, 'params'=>"valign=\"middle\" "),
					array('text' =>$del_link, 'params'=>"valign=\"middle\" "),
					'params'=>"align=center"
				);
	
			}
	}
	else 
		{
			$info_box_contents[]=array('text'=>BOX_TEXT_NO_RECORDS, 'params'=>"colspan=9 align=center class=t1");
			if($user->WriteLevel >= $page->AccessLevel && $user->WriteLevel > 0) 
							$add_link = "<a href=\"".$local_url."&edit_id=0\">+ add event</a>";
					else 	
							$add_link="";
		}
								
	$info_box_contents[]=array(array('text'=>$add_link, 'params'=>"colspan=9 align=right class=t1"));	
	new contentBox($info_box_contents);
?>