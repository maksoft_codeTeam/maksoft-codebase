<?php

	if($user->WriteLevel >= $page->AccessLevel && $user->WriteLevel > 0)
		{
			//edit players
			include DIR_WS_ADMIN . "players.php";
			
			//edit teams
			//include DIR_WS_ADMIN . "teams.php";
		}
	
	$local_url="page.php?n=".$n."&SiteID=".$SiteID."";	
	$info_box_contents = array();
  	$info_box_contents[] = array('text' => BOX_HEADING_TEAM_ROSTER . " : " . tep_get_team_info($team_id, '', 'team_season'));
	new infoBoxHeading($info_box_contents, true, false, false);
	
	$contents = array();
	$contents[]=
		array(
			array('text'=>"<a href=\"".$local_url."&sort=player_number\">N:</a>"),
			array('text'=>"<a href=\"".$local_url."&sort=player_name\">".BOX_PLAYER_TEXT_NAME."</a>"),
			array('text'=>BOX_PLAYER_TEXT_BIRTHDATE),
			array('text'=>BOX_PLAYER_TEXT_POSITION),
			array('text'=>"<a href=\"".$local_url."&sort=player_height\">".BOX_PLAYER_TEXT_HEIGHT."</a>"),
			array('text'=>"<a href=\"".$local_url."&sort=player_birthplace\">".BOX_PLAYER_TEXT_CITY."</a>"),
			array('text'=>"&nbsp;"),
			array('text'=>"&nbsp;"),
			'params'=>"class=t1"
			);
			
	$order_sql = "";
	switch($_GET['sort'])
		{
			case "player_name":
			case "player_number":
			case "player_height":
			case "player_birthplace" : 
				{	
					$order_sql = $_GET['sort'];
					break;
				}
			default: $order_sql = "player_number";
		}
	
	$players_sql = mysqli_query("SELECT * FROM players WHERE player_team = '$team_id' AND player_language_id='$language_id' ORDER BY $order_sql ASC");
	if(mysqli_affected_rows() >0)
		{
			
			while($player = mysqli_fetch_object($players_sql))
			{
						$edit_link="&nbsp;";
						$add_link="&nbsp;";
						$add_team_link="&nbsp;";
						$del_link = "&nbsp;";
											
					if($user->WriteLevel >= $page->AccessLevel && $user->WriteLevel > 0)
					{
						$edit_link = "<a href=\"".$local_url."&edit_id=".$player->player_id."\" class=button_submit>edit</a>";
						$add_link = "<a href=\"".$local_url."&edit_id=0\">+ add player</a>";
						$add_team_link = "<a href=\"".$local_url."&edit_tid=0\">+ add team</a>";
						$del_link = tep_draw_form("del","","POST") . tep_draw_hidden_field("del_id", $player->player_id) . tep_draw_input_field("form_sumbit", " delete ", "class=button_submit onClick=\"return confirm('Are you sure ?')\"", "submit") . "</form>";
					}

				$contents[]=
				array(
					array('text'=>$player->player_number),
					array('text'=>"<a href=\"".$local_url."&pid=".$player->player_id."\" title=\"".$player->player_description."\">".$player->player_name."</a>"),
					array('text'=>$player->player_birthdate."&nbsp;"),
					array('text'=>$player->player_position."&nbsp;"),
					array('text'=>$player->player_height."&nbsp;"),
					array('text'=>$player->player_birthplace."&nbsp;"),
					array('text'=>$edit_link),
					array('text'=>$del_link)
				
					);
			}
		}
	else 
		{
			$contents[]=array('text'=>BOX_TEXT_NO_RECORDS, 'params'=>"colspan=7 align=center class=t1");
			if($user->WriteLevel >= $page->AccessLevel && $user->WriteLevel > 0) 
				{
							$add_link = "<a href=\"".$local_url."&edit_id=0\">+ add player</a>";
							$add_team_link = "<a href=\"".$local_url."&edit_tid=0\">+ add team</a>";
				}
			else 
				{		
							$add_link="&nbsp;";
							$add_team_link  = "$nbsp;";
				}
		}
				
	$contents[]=
	array(
	array('text'=>"&nbsp;"),
	array('text'=>$add_team_link, 'params'=>"align=left"),
	array('text'=>$add_link, 'params'=>"colspan=6"),
	'params'=>"align=right class=t1"
	);
	
	new contentBox($contents);
						
	mysql_select_db("maksoft");
	//$content_n = 19737; //BG
	//$content_n = 21836; //EN
	$content_n = N_COACHING_STAFF;
	include DIR_WS_BOXES . "list_page_content.php";
	
	mysql_select_db("basketball");
?>

