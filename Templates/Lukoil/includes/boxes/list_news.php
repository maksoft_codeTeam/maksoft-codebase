<?php
	//read news
	if(!tep_not_null($subcontent_n))
		$subcontent_n = $Site->news;
	
	if(!tep_not_null($subcontent_view))
		$subcontent_view = "normal";
	
	if(!tep_not_null($subcontent_limit))
		$subcontent_limit = 20;


	//news list visualization params
	if(!tep_not_null($subcontent_view_news_title))
		$subcontent_view_news_title = true;

	if(!tep_not_null($subcontent_view_news_paragraph))
		$subcontent_view_news_paragraph = false;

	if(!tep_not_null($subcontent_limit))
		$subcontent_view_news_content = false;
	
	if(!tep_not_null($subcontent_view_columns))
		$subcontent_view_columns = 1;
				
	$row_content_query = mysqli_query("SELECT Name FROM pages WHERE n = '".(int)$subcontent_n."' ");
	$row_content = mysqli_fetch_array($row_content_query);	
  
  $info_box_contents = array();
  $info_box_contents[] = array('text' => $row_content['Name']);
  if($subcontent_view == "normal")
  	new infoBoxHeading($info_box_contents, true, false, false);	
	

	$info_box_contents = array();
	
	if($subcontent_view == "simple")
		{
			$info_box_contents[0] = array('text' => "<b>".$row_content['Name']."</b>", 'params'=>"class=t1 colspan=2");
			$row=1;
		}
	else $row=0;
	
	$row_news_res = mysqli_query("SELECT * FROM pages WHERE pages.ParentPage = '$subcontent_n' ORDER by n DESC LIMIT $subcontent_limit");
	
	$i=1;
	$col = 0;
		while($row_news = mysqli_fetch_object($row_news_res))
		{	
			$content = "";
			if($subcontent_view_news_title)
				$content.= "<a href=\"page.php?n=$row_news->n&SiteID=$SiteID\" class=\"main_link\">". $row_news->Name."</a><br>";
			
			if($subcontent_view_news_paragraph)
				$content.= crop_text($row_news->textStr);			
			
			$info_box_contents[$row]= array(array('text'=>($i++)." .", 'params'=>"align=right width=30 valign=top"), array('text' =>$content,'params'=>"valign=\"top\" "));
			if($col<$subcontent_view_columns-1)
				$col ++;
			else 
			{
				$col = 0;
				$row++;	
			}	

		}
	
	new contentBox($info_box_contents);

unset($subcontent_n);
?>