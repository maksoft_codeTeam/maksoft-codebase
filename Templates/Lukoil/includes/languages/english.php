<?php

// Global entries for the <html> tag
define('HTML_PARAMS','dir="LTR" lang="??"');

// charset for web pages and emails
define('CHARSET', 'windows-1251');

//form variables
define('PULL_DOWN_DEFAULT', '---');

// specials box text in includes/boxes/login.php
define('BOX_HEADING_LOGIN', 'login');
define('BOX_TEXT_USERNAME', 'username:');
define('BOX_TEXT_PASSWORD', 'password:');
define('BOX_TEXT_SUBMIT', 'enter');

// specials box text in includes/boxes/search.php
define('BOX_HEADING_SEARCH', 'site search');
define('BOX_SEARCH_TEXT', 'Please enter search text...');

// specials box text in includes/boxes/news.php
define('BOX_HEADING_NEWS', 'hot news | <a href="page.php?n=21660&SiteID='.$SiteID.'">archive</a>');
define('BOX_HEADING_SECTIONS', 'sections ');


// specials box text in includes/boxes/sponsors.php
define('BOX_HEADING_SPONSOR', 'sponsors');

// specials box text in includes/boxes/calendar.php
define('BOX_HEADING_CALENDAR', '<a href="page.php?n=21702&SiteID='.$SiteID.'">Calendar</a>');

// specials box text in includes/boxes/calendar_full.php
define('BOX_HEADING_CALENDAR_FULL', 'Full calendar');

// specials box text in includes/boxes/calendar_upcomming.php
define('BOX_HEADING_CALENDAR_UPCOMMING', 'Upcomming events');

define('N_SCHEDULE_PAGE', 21668);
define('N_RESULTS_PAGE', 21735);
define('N_STANDING_PAGE', 22053);
define('BOX_TEXT_RESULTS', '<a href="page.php?n='.N_RESULTS_PAGE.'&SiteID='.$SiteID.'">results</?>');
define('BOX_TEXT_STANDING', '<a href="page.php?n='.N_STANDING_PAGE.'&SiteID='.$SiteID.'">standings</a>');
define('BOX_TEXT_SCHEDULE', '<a href="page.php?n='.N_SCHEDULE_PAGE.'&SiteID='.$SiteID.'">schedule</?>');

define('BOX_TEXT_TOURNAMENT_ULEB', 'ULEB Cup');
define('BOX_TEXT_TOURNAMENT_BG', 'BG');
define('BOX_TEXT_TOURNAMENT_BGCUP', 'BG Cup');

// specials box text in includes/boxes/list_events.php
define('BOX_HEADING_EVENTS_UPCOMMING', 'upcomming events: ');
define('BOX_HEADING_EVENTS_PASSED', 'passed events: ');
define('BOX_EVENTS_TEXT_ROUND', 'round');
define('BOX_EVENTS_TEXT_DATE', 'date');
define('BOX_EVENTS_TEXT_TIME', 'time');
define('BOX_EVENTS_TEXT_HOME_TEAM', 'home');
define('BOX_EVENTS_TEXT_AWAY_TEAM', 'away');
define('BOX_EVENTS_TEXT_HALL', 'hall');
define('BOX_HEADING_LAST_EVENT','last match');
define('BOX_HEADING_EVENT_INFO','match info');
define('BOX_LABEL_EVENT_PLACE', 'hall: ');
define('BOX_LABEL_EVENT_DESCRIPTION', 'description: ');
define('BOX_LABEL_EVENT_REFEREES', 'referees: ');
define('BOX_LABEL_EVENT_COMMISSIONER', 'commissioner: ');

// specials box text in admin/events.php
define('BOX_ADMIN_EVENT_LANGUAGE', 'language:<br>');
define('BOX_ADMIN_EVENT_TITLE', 'event title:<br>');
define('BOX_ADMIN_EVENT_HOME_TEAM', 'home team:<br>');
define('BOX_ADMIN_EVENT_AWAY_TEAM', 'away team:<br>');
define('BOX_ADMIN_EVENT_PLACE', 'event place:<br>');
define('BOX_ADMIN_EVENT_DATE', 'date (YYYY-MM-DD):<br>');
define('BOX_ADMIN_EVENT_TIME', 'time (HH:MM:SS):<br>');
define('BOX_ADMIN_EVENT_DESCRIPTION', 'description:<br>');
define('BOX_ADMIN_EVENT_TOURNAMENT', 'tournament (ULEB/ BG/ BGCUP):<br>');
define('BOX_ADMIN_EVENT_ROUND', 'event round:<br>');
define('BOX_ADMIN_EVENT_MATCH_NUMBER', 'matc N:<br>');
define('BOX_ADMIN_EVENT_SCORE1', 'score '.BOX_ADMIN_EVENT_HOME_TEAM);
define('BOX_ADMIN_EVENT_SCORE2', 'score '.BOX_ADMIN_EVENT_AWAY_TEAM);
define('BOX_ADMIN_EVENT_REFEREES', 'referees:<br>');
define('BOX_ADMIN_EVENT_COMMISSIONER', 'commissioner:<br>');
define('BOX_ADMIN_EVENT_TV', 'TV (BNT, bTV, Eurosport 2 ..):<br>');
define('BOX_ADMIN_EVENT_STATUS', 'status (0-stopped / 1-upcomming / 2-passed):<br>');
define('BOX_ADMIN_EVENT_URL', 'url (redirect):<br>');

// specials box text in includes/boxes/players_list.php
define('BOX_HEADING_TEAM_ROSTER', 'team rooster');
define('BOX_HEADING_TEAM_ROSTER_LANGUAGE', "<a href=\"page.php?n=19398&SiteID=450\" target=\"_blank\"><img src=\"".DIR_TEMPLATE_IMAGES."/flag_bg.jpg\" border=\"0\"></a>");
define('BOX_PLAYER_TEXT_NAME', 'name');
define('BOX_PLAYER_TEXT_BIRTHDATE', 'birthdate');
define('BOX_PLAYER_TEXT_HEIGHT', 'height');
define('BOX_PLAYER_TEXT_POSITION', 'position');
define('BOX_PLAYER_TEXT_CITY', 'city');
define('N_COACHING_STAFF', 21836);

// specials box text in includes/boxes/advertisements.php
define('BOX_HEADING_ADVERTISEMENT', 'advertisement');

// specials box text in includes/boxes/poll.php
define('BOX_HEADING_POLL', 'Poll');
define('BOX_TEXT_BUTTON_VOTE', 'vote');
define('BOX_TEXT_VOTE_CONFIRM', '<h1>Thank you for your vote!  </h1>');
define('BOX_TEXT_VOTE_ALERT', 'Make your choice, please!');


// languages box text in includes/boxes/languages.php
define('BOX_HEADING_LANGUAGES', 'languages');

define('BOX_HEADING_ULEB', 'ULEB');
define('BOX_HEADING_ULEB_RULES', 'ULEB rules');

define('BOX_HEADING_BG', 'BG');
define('BOX_HEADING_BG_RULES', 'BG rules');

// languages box text in includes/boxes/search.php
define('BOX_TEXT_ADVANCED_SEARCH', '<a href="page.php?n=21735&SiteID='.$SiteID.'">advanced search</a>');


define('BOX_TEXT_NO_RECORDS', '<h4>No records found</h4>');
define('BOX_TEXT_LIVE', 'Live: ');
define('BOX_TEXT_NO_IMAGES', 'No images found');
define('BOX_TEXT_NO_STATS', 'No stats found');

// specials box text in admin/players.php
define('BOX_LABEL_PLAYER_NUMBER', 'N:<br>');
define('BOX_LABEL_PLAYER_NAME', 'name/family:<br>');
define('BOX_LABEL_PLAYER_BIRTHDATE', 'birthdate:<br>');
define('BOX_LABEL_PLAYER_POSITION', 'position:<br>');
define('BOX_LABEL_PLAYER_HEIGHT', 'height:<br>');
define('BOX_LABEL_PLAYER_BIRTHPLACE', 'city:<br>');
define('BOX_LABEL_PLAYER_NATIONALITY', 'nationality:<br>');
define('BOX_LABEL_PLAYER_PREVIOUS_TEAM', 'previous team/career:<br>');
define('BOX_LABEL_PLAYER_YEARS_IN_TEAM', 'years in team:<br>');
define('BOX_LABEL_PLAYER_DESCRIPTION', 'description/biography:<br>');
define('BOX_LABEL_PLAYER_PHOTO', 'photo:');

// languages box text in includes/boxes/players_gallery.phpp
define('BOX_HEADING_PLAYERS_GALLERY', 'players');

// languages box text in includes/boxes/player_gallery.phpp
define('BOX_HEADING_PLAYER_GALLERY', 'player gallery');

// languages box text in includes/boxes/player_stats.phpp
define('BOX_HEADING_PLAYER_STATS', 'player stats');

// languages box text in includes/boxes/event_stats.phpp
define('BOX_HEADING_TEAM_LIST', 'team list');

//page options array
$page_options = array();
	
$page_options['e-mail'] = array('text'=>tep_image(DIR_TEMPLATE_IMAGES."button_email.jpg", "Send to friend"), 'url'=>"mailto:$Site->EMail?subject=Interesting site&body=I suggest you to visiti this address: $Site->url", 'alt'=>"Send to a friend");
$page_options['pdf'] = array('text'=>tep_image(DIR_TEMPLATE_IMAGES."button_pdf.jpg", "Download PDF"), 'url'=>"#", 'alt'=>"Download PDF");
$page_options['print'] = array('text'=>tep_image(DIR_TEMPLATE_IMAGES."button_print.jpg", "Print this page"), 'url'=>"page.php?n=$n&SiteID=$SiteID&templ=89", 'alt'=>"Print this page");

define('N_CONTENT_STANDINGS_BG', '22053');
define('N_CONTENT_TEAM_LEADERS', '21015');
?>
