<?php

// Global entries for the <html> tag
define('HTML_PARAMS','dir="LTR" lang="bg"');

// charset for web pages and emails
define('CHARSET', 'windows-1251');

//form variables
define('PULL_DOWN_DEFAULT', '---');


// specials box text in includes/boxes/login.php
define('BOX_HEADING_LOGIN', '����');
define('BOX_TEXT_USERNAME', '���:');
define('BOX_TEXT_PASSWORD', '������:');
define('BOX_TEXT_SUBMIT', '����');


// specials box text in includes/boxes/search.php
define('BOX_HEADING_SEARCH', '�������');
define('BOX_SEARCH_TEXT', '�������� ���� �� �������.');

// specials box text in includes/boxes/news.php
define('BOX_HEADING_NEWS', '������ | <a href="page.php?n=19797&SiteID='.$SiteID.'">�����</a>');
define('BOX_HEADING_SECTIONS', '������ ');

// specials box text in includes/boxes/sponsors.php
define('BOX_HEADING_SPONSOR', '��������');

// specials box text in includes/boxes/calendar.php
define('BOX_HEADING_CALENDAR', '<a href="page.php?n=19860&SiteID='.$SiteID.'">��������</�>');

// specials box text in includes/boxes/calendar_full.php
define('BOX_HEADING_CALENDAR_FULL', '����� ��������');

// specials box text in includes/boxes/calendar_upcomming.php
define('N_SCHEDULE_PAGE', 21479);
define('N_RESULTS_PAGE', 21757);
define('N_STANDING_PAGE', 22049);

define('BOX_HEADING_CALENDAR_UPCOMMING', '���������� ������');
define('BOX_TEXT_RESULTS', '<a href="page.php?n='.N_RESULTS_PAGE.'&SiteID='.$SiteID.'">���������</�>');
define('BOX_TEXT_STANDING', '<a href="page.php?n='.N_STANDING_PAGE.'&SiteID='.$SiteID.'">���������</�>');
define('BOX_TEXT_SCHEDULE', '<a href="page.php?n='.N_SCHEDULE_PAGE.'&SiteID='.$SiteID.'">��������</�>');

define('BOX_TEXT_TOURNAMENT_ULEB', '���� ���');
define('BOX_TEXT_TOURNAMENT_BG', 'BG');
define('BOX_TEXT_TOURNAMENT_BGCUP', 'BG ���');


// specials box text in includes/boxes/list_events.php
define('BOX_HEADING_EVENTS_UPCOMMING', '���������� �����: ');
define('BOX_HEADING_EVENTS_PASSED', '�������� �����: ');
define('BOX_EVENTS_TEXT_ROUND', '����');
define('BOX_EVENTS_TEXT_DATE', '����');
define('BOX_EVENTS_TEXT_TIME', '���');
define('BOX_EVENTS_TEXT_HOME_TEAM', '�������');
define('BOX_EVENTS_TEXT_AWAY_TEAM', '����');
define('BOX_EVENTS_TEXT_HALL', '����');
define('BOX_HEADING_LAST_EVENT','�������� ���������');
define('BOX_HEADING_EVENT_INFO','����������� �� ���');
define('BOX_LABEL_EVENT_PLACE', '����: ');
define('BOX_LABEL_EVENT_DESCRIPTION', '��������: ');
define('BOX_LABEL_EVENT_REFEREES', '�����: ');
define('BOX_LABEL_EVENT_COMMISSIONER', '�������: ');

// specials box text in admin/events.php
define('BOX_ADMIN_EVENT_LANGUAGE', '����:<br>');
define('BOX_ADMIN_EVENT_TITLE', '��������:<br>');
define('BOX_ADMIN_EVENT_HOME_TEAM', '�������:<br>');
define('BOX_ADMIN_EVENT_AWAY_TEAM', '����:<br>');
define('BOX_ADMIN_EVENT_PLACE', '����� �� �������:<br>');
define('BOX_ADMIN_EVENT_DATE', '���� (YYYY-MM-DD):<br>');
define('BOX_ADMIN_EVENT_TIME', '��� (HH:MM:SS):<br>');
define('BOX_ADMIN_EVENT_DESCRIPTION', '��������:<br>');
define('BOX_ADMIN_EVENT_TOURNAMENT', '������ (ULEB/ BG/ BGCUP):<br>');
define('BOX_ADMIN_EVENT_ROUND', '����:<br>');
define('BOX_ADMIN_EVENT_MATCH_NUMBER', '��� N:<br>');
define('BOX_ADMIN_EVENT_SCORE1', '�������� '.BOX_ADMIN_EVENT_HOME_TEAM);
define('BOX_ADMIN_EVENT_SCORE2', '�������� '.BOX_ADMIN_EVENT_AWAY_TEAM);
define('BOX_ADMIN_EVENT_REFEREES', '�����:<br>');
define('BOX_ADMIN_EVENT_COMMISSIONER', '�������:<br>');
define('BOX_ADMIN_EVENT_TV', 'TV (BNT, bTV, ��������� 2 ..):<br>');
define('BOX_ADMIN_EVENT_STATUS', '������ (0-������� / 1-�������� / 2-�����):<br>');
define('BOX_ADMIN_EVENT_URL', 'url (���������):<br>');
				
// specials box text in includes/boxes/players_list.php
define('BOX_HEADING_TEAM_ROSTER', '������ �� ������');
define('BOX_HEADING_TEAM_ROSTER_LANGUAGE', '<a href="page.php?n=21833&SiteID=483" target="_blank"><img src="'.DIR_TEMPLATE_IMAGES.'/flag_uk.jpg" border=0></a>');
define('BOX_PLAYER_TEXT_NAME', '���');
define('BOX_PLAYER_TEXT_BIRTHDATE', '�.����');
define('BOX_PLAYER_TEXT_POSITION', '�������');
define('BOX_PLAYER_TEXT_HEIGHT', '����');
define('BOX_PLAYER_TEXT_CITY', '����');
define('N_COACHING_STAFF', 19706);


// specials box text in includes/boxes/advertisements.php
define('BOX_HEADING_ADVERTISEMENT', '�������');

// specials box text in includes/boxes/poll.php
define('BOX_HEADING_POLL', '������');
define('BOX_TEXT_BUTTON_VOTE', '��������');
define('BOX_TEXT_VOTE_CONFIRM', '<h1>��� ���������� �������! </h1>');
define('BOX_TEXT_VOTE_ALERT', '�� �� ���� ����������� �� ������� ���� ��������� ����� �����');

// languages box text in includes/boxes/languages.php
define('BOX_HEADING_LANGUAGES', 'E����');


define('BOX_HEADING_ULEB', 'ULEB');
define('BOX_HEADING_ULEB_RULES', '������� �� ULEB');

define('BOX_HEADING_BG', 'BG');
define('BOX_HEADING_BG_RULES', '������� BG');


// languages box text in includes/boxes/search.php
define('BOX_TEXT_ADVANCED_SEARCH', '<a href="page.php?n=21734&SiteID='.$SiteID.'">�������� �������</a>');

define('BOX_TEXT_NO_RECORDS', '<h4>���� �������� ������</h4>');
define('BOX_TEXT_LIVE', '�� ����: ');
define('BOX_TEXT_NO_IMAGES', '���� �������� ������');
define('BOX_TEXT_NO_STATS', '���� �������� ����������');

// specials box text in admin/players.php
define('BOX_LABEL_PLAYER_NUMBER', 'N:<br>');
define('BOX_LABEL_PLAYER_NAME', '���/�������:<br>');
define('BOX_LABEL_PLAYER_BIRTHDATE', '�.����:<br>');
define('BOX_LABEL_PLAYER_POSITION', '����:<br>');
define('BOX_LABEL_PLAYER_HEIGHT', '����:<br>');
define('BOX_LABEL_PLAYER_BIRTHPLACE', '����:<br>');
define('BOX_LABEL_PLAYER_NATIONALITY', '������������:<br>');
define('BOX_LABEL_PLAYER_PREVIOUS_TEAM', '�������� �����/�������:<br>');
define('BOX_LABEL_PLAYER_YEARS_IN_TEAM', '������ � ������:<br>');
define('BOX_LABEL_PLAYER_DESCRIPTION', '��������/���������:<br>');
define('BOX_LABEL_PLAYER_PHOTO', '������:');

//admin/teams.php
define('BOX_LABEL_TEAM_SHORTNAME','������� ��� (Lat.): <br>');
define('BOX_LABEL_TEAM_NAME','��� �� ������:<br>');
define('BOX_LABEL_TEAM_COACH','��. �������:<br>');
define('BOX_LABEL_TEAM_URL','�����: <br>');
define('BOX_LABEL_TEAM_DESCRIPTION','������ ��������: <br>');
define('BOX_LABEL_TEAM_PHOTO','���� �� ������: (max 256 kB, JPG, GIF, PNG; �������)');

// languages box text in includes/boxes/play�rs_gallery.phpp
define('BOX_HEADING_PLAYERS_GALLERY', '������ ������');

// languages box text in includes/boxes/player_gallery.phpp
define('BOX_HEADING_PLAYER_GALLERY', '������� ������');

// languages box text in includes/boxes/player_stats.phpp
define('BOX_HEADING_PLAYER_STATS', '����������');

// languages box text in includes/boxes/event_stats.phpp
define('BOX_HEADING_TEAM_LIST', '������ �����');

//page options array
$page_options = array();
	
$page_options['e-mail'] = array('text'=>tep_image(DIR_TEMPLATE_IMAGES."button_email.jpg", "������� �� ������"), 'url'=>"mailto:$Site->EMail?subject=��������� ����&body=����������� �� �� ����� ���� ����: $Site->url", 'alt'=>"������� �� �������");
$page_options['pdf'] = array('text'=>tep_image(DIR_TEMPLATE_IMAGES."button_pdf.jpg", "������� PDF"), 'url'=>"#", 'alt'=>"��������� PDF");
$page_options['print'] = array('text'=>tep_image(DIR_TEMPLATE_IMAGES."button_print.jpg", "��������� ��������"), 'url'=>"page.php?n=$n&SiteID=$SiteID&templ=89", 'alt'=>"��������� ��������");

define('N_CONTENT_STANDINGS_BG', '22049');
define('N_CONTENT_TEAM_LEADERS', '21015');

?>
