<?php
// Define the webserver and path parameters
// * DIR_FS_* = Filesystem directories (local/physical)
// * DIR_WS_* = Webserver directories (virtual/URL)

  define('HTTP_SERVER', 'http://lukoil.net'); // eg, http://localhost - should not be empty for productive servers
  define('HTTPS_SERVER', ''); // eg, https://localhost - should not be empty for productive servers
  define('DIR_WS_IMAGES', 'web/images/lukoil/');
  define('DIR_WS_MEDIA', 'Templates/Lukoil/media/');
  define('DIR_WS_ADMIN', 'Templates/Lukoil/admin/');
  define('DIR_WS_HTTP_CATALOG','/');
  define('DIR_WS_PLAYERS', DIR_WS_IMAGES.'players/');
  define('DIR_WS_DOWNLOADS', DIR_TEMPLATE .'downloads/');
  define('DIR_WS_INCLUDES', DIR_TEMPLATE . 'includes/');
  define('DIR_WS_BOXES', DIR_WS_INCLUDES . 'boxes/');
  define('DIR_WS_FUNCTIONS', DIR_WS_INCLUDES . 'functions/');
  define('DIR_WS_CLASSES', DIR_WS_INCLUDES . 'classes/');
  define('DIR_WS_MODULES', DIR_WS_INCLUDES . 'modules/');
  define('DIR_WS_LANGUAGES', DIR_WS_INCLUDES . 'languages/');

// define our database connection
  define('DB_SERVER', 'localhost'); // eg, localhost - should not be empty for productive servers
  define('DB_SERVER_USERNAME', 'maksoft');
  define('DB_SERVER_PASSWORD', 'mak211');
  define('DB_DATABASE', 'basketball');
  define('USE_PCONNECT', 'false'); // use persistent connections?
?>