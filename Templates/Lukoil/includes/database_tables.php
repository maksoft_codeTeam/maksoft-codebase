<?php
// define the database table names used in the project
  define('TABLE_PLAYERS', 'players');
  define('TABLE_EVENTS', 'events');
  define('TABLE_NEWS', 'news');
  define('TABLE_TEAMS', 'teams');
  define('TABLE_LANGUAGES','languages');
  define('TABLE_TOURNAMENTS','tournaments');
?>
