<?php
define ("DIR_TEMPLATE","Templates/Lukoil/");
define("DIR_TEMPLATE_IMAGES","Templates/Lukoil/images/");
define("DIR_MODULES","web/admin/modules/");

// include server parameters
  require(DIR_TEMPLATE . '/includes/configure.php');

// define the project version
  define('PROJECT_VERSION', 'Lukoil Academic v.1');

// include the list of project filenames
  require(DIR_WS_INCLUDES . 'filenames.php');

// include the list of project database tables
require(DIR_WS_INCLUDES . 'database_tables.php');

// include the calendar class
require(DIR_WS_FUNCTIONS . 'calendar.php');
$cal = new Calendar;

// customization for the design layout
  define('BOX_WIDTH', 180); // how wide the boxes should be in pixels (default: 150)
  
// include the database functions
  require(DIR_WS_FUNCTIONS . 'database.php');


// define general functions used application-wide
  require(DIR_WS_FUNCTIONS . 'general.php');
  require(DIR_WS_FUNCTIONS . 'html_output.php');
  require(DIR_WS_FUNCTIONS . 'xml_parser.php');

// include upload class
  //include(DIR_WS_CLASSES . 'upload.php');
   
//set the language
if(isset($_GET['lng'])) $lang = $_GET['lng'];
if(!isset($_SESSION['lng'])) session_register('lng');
switch($lng)
{
case "EN": 
		{
		include DIR_WS_LANGUAGES."english.php";
		$language_directory = "english";
		$language_id = 2;
		$version_flash_banner = DIR_WS_MEDIA."LA_banner_EN";
		break;
		}

case "BG": 
		{
		include DIR_WS_LANGUAGES."bulgarian.php";
		$language_directory = "bulgarian";
		$language_id = 1;
		$version_flash_banner = DIR_WS_MEDIA."LA_banner";
		break;
		}
default: 
		{
			include DIR_WS_LANGUAGES."bulgarian.php";
			$language_directory = "bulgarian";
			$language_id = 1;
			$version_flash_banner = DIR_WS_MEDIA."LA_banner"; 
		}
}
	
// infobox
  //require(DIR_WS_CLASSES . 'boxes.php');

if(!isset($MAX_HEIGHT))
	$MAX_HEIGHT = "100%";
if(!isset($MAIN_WIDTH))
	$MAIN_WIDTH = "100%";
if(!isset($MAX_WIDTH))
	$MAX_WIDTH = "100%";
if(!isset($MENU_WIDTH))
	$MENU_WIDTH = "180";
if(!isset($PAGE_ALIGN))
	$PAGE_ALIGN = "left";
	
//load CSS style
if($Site->css!=0 && !empty($Site->cssDef))
	echo $Site->cssDef; 
else if($Site->Template != 89) echo "<link href='http://www.maksoft.net/css/Lukoil/base_style.css' rel='stylesheet' type='text/css'>";
//else echo "<link href='http://www.maksoft.net/css/Lukoil/printable_style.css' rel='stylesheet' type='text/css'>";

//team_id variable
	if(isset($team_id))
		$tid = (int)$team_id;
	else if(isset($_GET['tid']))
		$tid = (int)$_GET['tid'];

//local URL
	$local_url="page.php?n=".$n."&SiteID=".$SiteID."";


//include "js/editor/simple.php";
?>
