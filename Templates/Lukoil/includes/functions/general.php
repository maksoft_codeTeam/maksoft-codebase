<?php
/*
  $Id: general.php,v 1.231 2003/07/09 01:15:48 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

////
// Stop from parsing any further PHP code
  function tep_exit() {
   tep_session_close();
   exit();
  }

////
// Redirect to another page or site
  function tep_redirect($url) {
    if ( (ENABLE_SSL == true) && (getenv('HTTPS') == 'on') ) { // We are loading an SSL page
      if (substr($url, 0, strlen(HTTP_SERVER)) == HTTP_SERVER) { // NONSSL url
        $url = HTTPS_SERVER . substr($url, strlen(HTTP_SERVER)); // Change it to SSL
      }
    }

    header('Location: ' . $url);

    tep_exit();
  }

////
// Parse the data used in the html tags to ensure the tags will not break
  function tep_parse_input_field_data($data, $parse) {
    return strtr(trim($data), $parse);
  }

  function tep_output_string($string, $translate = false, $protected = false) {
    if ($protected == true) {
      return htmlspecialchars($string);
    } else {
      if ($translate == false) {
        return tep_parse_input_field_data($string, array('"' => '&quot;'));
      } else {
        return tep_parse_input_field_data($string, $translate);
      }
    }
  }

  function tep_output_string_protected($string) {
    return tep_output_string($string, false, true);
  }

  function tep_sanitize_string($string) {
    $string = ereg_replace(' +', ' ', trim($string));

    return preg_replace("/[<>]/", '_', $string);
  }


// Return a player info
// TABLES: players
  function tep_get_player_info($player_id) {
    global $languages_id;

    if (empty($language)) $language = $languages_id;

    //$player_query = tep_db_query("select * from " . TABLE_PLAYERS . " where player_id = '" . (int)$player_id . "' and language_id = '" . (int)$language . "'");
    $player_query = tep_db_query("select * from " . TABLE_PLAYERS . " where player_id = " . (int)$player_id . " ");    
	$player = tep_db_fetch_array($player_query);

    return $player;
  }

// Return a player info
// TEAMS: players
  function tep_get_team_info($team_id = NULL, $team_shortname= NULL, $return=NULL) {
    global $languages_id;

    if (empty($language)) $language = $languages_id;

	if(tep_not_null($team_id))
		$team_query = tep_db_query("select * from " . TABLE_TEAMS . " where team_id = " . (int)$team_id . " ");
	else
	if(tep_not_null($team_shortname))
		$team_query = tep_db_query("select * from " . TABLE_TEAMS . " where team_shortname = '".$team_shortname."' ");
		
	$team = tep_db_fetch_array($team_query);
	
	if(tep_not_null($return))
    	return $team[$return];
    else return $team;
  }


// Break a word in a string if it is longer than a specified length ($len)
  function tep_break_string($string, $len, $break_char = '-') {
    $l = 0;
    $output = '';
    for ($i=0, $n=strlen($string); $i<$n; $i++) {
      $char = substr($string, $i, 1);
      if ($char != ' ') {
        $l++;
      } else {
        $l = 0;
      }
      if ($l > $len) {
        $l = 1;
        $output .= $break_char;
      }
      $output .= $char;
    }

    return $output;
  }

////
// Return all HTTP GET variables, except those passed as a parameter
  function tep_get_all_get_params($exclude_array = '') {
    global $_GET;

    if (!is_array($exclude_array)) $exclude_array = array();

    $get_url = '';
    if (is_array($_GET) && (sizeof($_GET) > 0)) {
      reset($_GET);
      while (list($key, $value) = each($_GET)) {
        if ( (strlen($value) > 0) && ($key != tep_session_name()) && ($key != 'error') && (!in_array($key, $exclude_array)) && ($key != 'x') && ($key != 'y') ) {
          $get_url .= $key . '=' . rawurlencode(stripslashes($value)) . '&';
        }
      }
    }

    return $get_url;
  }
  
 
// Returns the clients browser
  function tep_browser_detect($component) {
    global $HTTP_USER_AGENT;

    return stristr($HTTP_USER_AGENT, $component);
  }



  function tep_row_number_format($number) {
    if ( ($number < 10) && (substr($number, 0, 1) != '0') ) $number = '0' . $number;

    return $number;
  }


// Check date
  function tep_checkdate($date_to_check, $format_string, &$date_array) {
    $separator_idx = -1;

    $separators = array('-', ' ', '/', '.');
    $month_abbr = array('jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec');
    $no_of_days = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    $format_string = strtolower($format_string);

    if (strlen($date_to_check) != strlen($format_string)) {
      return false;
    }

    $size = sizeof($separators);
    for ($i=0; $i<$size; $i++) {
      $pos_separator = strpos($date_to_check, $separators[$i]);
      if ($pos_separator != false) {
        $date_separator_idx = $i;
        break;
      }
    }

    for ($i=0; $i<$size; $i++) {
      $pos_separator = strpos($format_string, $separators[$i]);
      if ($pos_separator != false) {
        $format_separator_idx = $i;
        break;
      }
    }

    if ($date_separator_idx != $format_separator_idx) {
      return false;
    }

    if ($date_separator_idx != -1) {
      $format_string_array = explode( $separators[$date_separator_idx], $format_string );
      if (sizeof($format_string_array) != 3) {
        return false;
      }

      $date_to_check_array = explode( $separators[$date_separator_idx], $date_to_check );
      if (sizeof($date_to_check_array) != 3) {
        return false;
      }

      $size = sizeof($format_string_array);
      for ($i=0; $i<$size; $i++) {
        if ($format_string_array[$i] == 'mm' || $format_string_array[$i] == 'mmm') $month = $date_to_check_array[$i];
        if ($format_string_array[$i] == 'dd') $day = $date_to_check_array[$i];
        if ( ($format_string_array[$i] == 'yyyy') || ($format_string_array[$i] == 'aaaa') ) $year = $date_to_check_array[$i];
      }
    } else {
      if (strlen($format_string) == 8 || strlen($format_string) == 9) {
        $pos_month = strpos($format_string, 'mmm');
        if ($pos_month != false) {
          $month = substr( $date_to_check, $pos_month, 3 );
          $size = sizeof($month_abbr);
          for ($i=0; $i<$size; $i++) {
            if ($month == $month_abbr[$i]) {
              $month = $i;
              break;
            }
          }
        } else {
          $month = substr($date_to_check, strpos($format_string, 'mm'), 2);
        }
      } else {
        return false;
      }

      $day = substr($date_to_check, strpos($format_string, 'dd'), 2);
      $year = substr($date_to_check, strpos($format_string, 'yyyy'), 4);
    }

    if (strlen($year) != 4) {
      return false;
    }

    if (!settype($year, 'integer') || !settype($month, 'integer') || !settype($day, 'integer')) {
      return false;
    }

    if ($month > 12 || $month < 1) {
      return false;
    }

    if ($day < 1) {
      return false;
    }

    if (tep_is_leap_year($year)) {
      $no_of_days[1] = 29;
    }

    if ($day > $no_of_days[$month - 1]) {
      return false;
    }

    $date_array = array($year, $month, $day);

    return true;
  }

////
// Check if year is a leap year
  function tep_is_leap_year($year) {
    if ($year % 100 == 0) {
      if ($year % 400 == 0) return true;
    } else {
      if (($year % 4) == 0) return true;
    }

    return false;
  }


  function tep_create_random_value($length, $type = 'mixed') {
    if ( ($type != 'mixed') && ($type != 'chars') && ($type != 'digits')) return false;

    $rand_value = '';
    while (strlen($rand_value) < $length) {
      if ($type == 'digits') {
        $char = tep_rand(0,9);
      } else {
        $char = chr(tep_rand(0,255));
      }
      if ($type == 'mixed') {
        if (eregi('^[a-z0-9]$', $char)) $rand_value .= $char;
      } elseif ($type == 'chars') {
        if (eregi('^[a-z]$', $char)) $rand_value .= $char;
      } elseif ($type == 'digits') {
        if (ereg('^[0-9]$', $char)) $rand_value .= $char;
      }
    }

    return $rand_value;
  }


  function tep_array_to_string($array, $exclude = '', $equals = '=', $separator = '&') {
    if (!is_array($exclude)) $exclude = array();

    $get_string = '';
    if (sizeof($array) > 0) {
      while (list($key, $value) = each($array)) {
        if ( (!in_array($key, $exclude)) && ($key != 'x') && ($key != 'y') ) {
          $get_string .= $key . $equals . $value . $separator;
        }
      }
      $remove_chars = strlen($separator);
      $get_string = substr($get_string, 0, -$remove_chars);
    }

    return $get_string;
  }

  function tep_not_null($value) {
    if (is_array($value)) {
      if (sizeof($value) > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      if (($value != '') && (strtolower($value) != 'null') && (strlen(trim($value)) > 0)) {
        return true;
      } else {
        return false;
      }
    }
  }


// Return a random value
  function tep_rand($min = null, $max = null) {
    static $seeded;

    if (!isset($seeded)) {
      mt_srand((double)microtime()*1000000);
      $seeded = true;
    }

    if (isset($min) && isset($max)) {
      if ($min >= $max) {
        return $min;
      } else {
        return mt_rand($min, $max);
      }
    } else {
      return mt_rand();
    }
  }

  function tep_get_ip_address() {
    if (isset($_SERVER)) {
      if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
      } else {
        $ip = $_SERVER['REMOTE_ADDR'];
      }
    } else {
      if (getenv('HTTP_X_FORWARDED_FOR')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
      } elseif (getenv('HTTP_CLIENT_IP')) {
        $ip = getenv('HTTP_CLIENT_IP');
      } else {
        $ip = getenv('REMOTE_ADDR');
      }
    }

    return $ip;
  }
 
 
 // Return an event info
// TABLE: events
  function tep_get_event_info($event_id = NULL, $event_tournament= NULL) {
	mysql_select_db("basketball");
	
	if(tep_not_null($event_id))
		$event_query = mysqli_query("select * from " . TABLE_EVENTS . " where event_id = " . (int)$event_id . " ");
	else
	if(tep_not_null($event_tournament))
		$event_query = mysqli_query("select * from " . TABLE_EVENTS . " where event_tournament = '".$event_tournament."' ");
		
	$event = mysqli_fetch_array($event_query);
	//mysql_select_db("maksoft");
    return $event;
  }

 ////
// Returns an array with teams
// TABLES: teams
  function tep_get_teams($team_id = '', $team_language_id = NULL, $team_shortname = NULL) {
    $teams_array = array();
	
    if(tep_not_null($team_language_id))
			$teams = tep_db_query("select team_id, team_name, team_season from " . TABLE_TEAMS . " WHERE team_language_id = '$team_language_id' order by team_name ");
	else if(tep_not_null($team_shortname))
		$teams = tep_db_query("select team_id, team_name,  team_shortname, team_season from " . TABLE_TEAMS . " WHERE team_shortname = '$team_shortname' order by team_name ");
	else
		$teams = tep_db_query("select team_id, team_name, team_season from " . TABLE_TEAMS . " order by team_name ");
   
	  
	  while ($teams_values = tep_db_fetch_array($teams)) {
        $teams_array[] = array('team_id' => $teams_values['team_id'],
                                   'team_name' => $teams_values['team_name'] . " ".$teams_values['team_season']);
      }

    return $teams_array;
  }
  
////
// Returns an array with languages
// TABLES:languages
  function tep_get_languages($language_id = '', $language_key = '') {
    $languages_array = array();
    $languages = tep_db_query("select * from " . TABLE_LANGUAGES . " order by language_id ");
      while ($languages_values = tep_db_fetch_array($languages)) {
        $languages_array[] = 
		array(
		'language_id' => $languages_values['language_id'],
		'language_name' => $languages_values['language_name'],
		'language_key' => $languages_values['language_key']
		);
      }

    return $languages_array;
  } 

////
// Returns an array with tournaments
// TABLES:tournaments
  function tep_get_tournaments()
  {
    $tournament_array = array();
    $tournaments = tep_db_query("select * from " . TABLE_TOURNAMENTS . " WHERE tournament_status = 1 order by tournament_order ASC ");
      while ($tournaments_values = tep_db_fetch_array($tournaments)) {
        $tournaments_array[] = 
		array(
		'tournament_id' => $tournaments_values['tournament_id'],
		'tournament_title' => $tournaments_values['tournament_title'],
		'tournament_key' => $tournaments_values['tournament_key']
		);
      }

    return $tournaments_array;
  } 
  
 // Return a tournament info
// TABLE: tournament
  function tep_get_tournament_info($tournament_key, $return = "tournament_title")
  {
	mysql_select_db("basketball");
	$tournament_query = mysqli_query("select * from " . TABLE_TOURNAMENTS . " where tournament_key LIKE '" . $tournament_key . "' ");

	$tournament = mysqli_fetch_array($tournament_query);
    return $tournament[$return];
  }
  
?>