<?php

$edit_id = $_GET['edit_id'];
$edit_confirm = $_POST['edit_confirm'];

$del_id = $_POST['del_id'];
if(tep_not_null($del_id))
	tep_db_query("DELETE FROM " . TABLE_EVENTS . " WHERE event_id='$del_id'");

if(tep_not_null($edit_id))
	{
		if($edit_confirm)
			{
				$data = array(
				'event_title'=> tep_db_prepare_input($_POST['event_title']), 
				'event_language_id'=> tep_db_prepare_input($_POST['event_language_id']), 
				'event_home_team'=>tep_db_prepare_input($_POST['event_home_team']), 
				'event_away_team'=>tep_db_prepare_input($_POST['event_away_team']), 
				'event_place'=>tep_db_prepare_input($_POST['event_place']), 
				'event_date'=>tep_db_prepare_input($_POST['event_date']),
				'event_time'=>tep_db_prepare_input($_POST['event_time']),
				'event_description'=>tep_db_prepare_input($_POST['event_description']),
				'event_tournament'=>tep_db_prepare_input($_POST['event_tournament']),
				'event_round'=>tep_db_prepare_input($_POST['event_round']),
				'event_match_number'=>tep_db_prepare_input($_POST['event_match_number']),
				'event_score1'=>tep_db_prepare_input($_POST['event_score1']),
				'event_score2'=>tep_db_prepare_input($_POST['event_score2']),
				'event_referees'=>tep_db_prepare_input($_POST['event_referees']),
				'event_commissioner'=>tep_db_prepare_input($_POST['event_commissioner']),
				'event_tv'=>tep_db_prepare_input($_POST['event_tv']),
				'event_status'=>tep_db_prepare_input($_POST['event_status']),
				'event_url'=>tep_db_prepare_input($_POST['event_url'])
				);
				
				if($_POST['duplicate']) $edit_id = 0;
				
				if($edit_id != 0)
					tep_db_perform("events", $data,  'update', "event_id = '" . $edit_id . "'");
				else
				if($edit_id == 0)
					tep_db_perform("events", $data,  'insert');
					
			}

		$info_box_contents = array();
  		$info_box_contents[] = array('text' => "Edit/Add event [ " . $event_tournament . " ]");
 		new infoBoxHeading($info_box_contents, true, false, false);
		

		echo tep_draw_form("edit_event", "", 'post');
		//include "js/editor/default.php";
		//get event info
		$event = tep_get_event_info($edit_id);
		$tournament_labels = array(
											array('text'=>"Uleb", 'id'=>"ULEB"),
											array('text'=>"BG Championship", 'id'=>"BG"),
											array('text'=>"BG Cup", 'id'=>"BGCUP"),
											array('text'=>"���������� �����", 'id'=>"FRIENDLY"),
										);
		
		$contents = array();
		$contents[]= array('text'=>BOX_ADMIN_EVENT_LANGUAGE . tep_get_languages_list("event_language_id", $language_id, "disabled")  .  tep_draw_hidden_field("event_language_id", $language_id) /*. tep_draw_checkbox_field('duplicate', true, false) . " duplicate"*/);
		$contents[]= array('text'=>BOX_ADMIN_EVENT_TITLE . tep_draw_input_field("event_title", $event['event_title'], "size=100", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_HOME_TEAM . tep_get_teams_list("event_home_team", $event['event_home_team'], $language_id));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_AWAY_TEAM . tep_get_teams_list("event_away_team", $event['event_away_team'], $language_id));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_PLACE . tep_draw_input_field("event_place", $event['event_place'], "size=20", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_DATE . tep_draw_input_field("event_date", $event['event_date'], "size=10", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_TIME . tep_draw_input_field("event_time", $event['event_time'], "size=10", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_DESCRIPTION . "<textarea name=event_description rows=15 cols=50>".$event['event_description']."</textarea>"/*tep_draw_textarea_field("event_description", true, 44, 5, $event['event_description'], "id=\"text_editor\"")*/);
		
		//$contents[]= array('text'=>BOX_ADMIN_EVENT_TOURNAMENT . tep_draw_pull_down_menu("event_tournament", $tournament_labels, $event['event_tournament']));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_TOURNAMENT . tep_get_tournaments_list("event_tournament", $event['event_tournament']));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_ROUND . tep_draw_input_field("event_round", $event['event_round'], "size=2", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_MATCH_NUMBER . tep_draw_input_field("event_match_number", $event['event_match_number'], "size=2", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_SCORE1 . tep_draw_input_field("event_score1", $event['event_score1'], "size=3", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_SCORE2 . tep_draw_input_field("event_score2", $event['event_score2'], "size=3", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_REFEREES . tep_draw_input_field("event_referees", $event['event_referees'], "size=20", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_COMMISSIONER . tep_draw_input_field("event_commissioner", $event['event_commissioner'], "size=20", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_STATUS . tep_draw_input_field("event_status", $event['event_status'], "size=20", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_URL . tep_draw_input_field("event_url", $event['event_url'], "size=20", 'text', true));
		$contents[]= array('text'=>BOX_ADMIN_EVENT_TV . tep_draw_input_field("event_tv", $event['event_TV'], "size=20", 'text', true));
		$contents[]= array('text'=>tep_draw_input_field("form_sumbit", " OK ", "class=button_submit", "submit"), 'params'=>"align=center");
		
			
		new contentBox($contents);
		
		echo  tep_draw_hidden_field("edit_confirm", true);
		echo  tep_draw_hidden_field("edit_id", $edit_id);
		echo "</form>";


	}
	
?>
<br>