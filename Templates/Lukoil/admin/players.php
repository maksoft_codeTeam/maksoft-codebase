<?php

$edit_id = $_GET['edit_id'];
$edit_confirm = $_POST['edit_confirm'];

$del_id = $_POST['del_id'];
if(tep_not_null($del_id))
{
	//echo "DELETE FROM " . TABLE_PLAYERS . " WHERE player_id='$del_id' AND player_team = '$team_id' ";
	tep_db_query("DELETE FROM " . TABLE_PLAYERS . " WHERE player_id='$del_id' AND player_team = '$team_id' ");
	mk_output_message("normal", "Player deleted"); 
}

if(tep_not_null($edit_id))
	{

		if($edit_confirm)
			{
				$data = array(
				'player_number'=>tep_db_prepare_input($_POST['player_number']), 
				'player_language_id' => tep_db_prepare_input($_POST['player_language_id']), 
				'player_name'=>tep_db_prepare_input($_POST['player_name']), 
				'player_birthdate'=>tep_db_prepare_input($_POST['player_birthdate']), 
				'player_position'=>tep_db_prepare_input($_POST['player_position']), 
				'player_height'=>tep_db_prepare_input($_POST['player_height']),
				'player_birthplace'=>tep_db_prepare_input($_POST['player_birthplace']),
				'player_nationality'=>tep_db_prepare_input($_POST['player_nationality']),
				'player_previous_team'=>tep_db_prepare_input($_POST['player_previous_team']),
				'player_team'=>tep_db_prepare_input($_POST['player_team']),
				'player_years_in_team'=>tep_db_prepare_input($_POST['player_years_in_team']),
				'player_description'=>tep_db_prepare_input($_POST['player_description'])
				);
				
				if($_POST['duplicate']) $edit_id = 0;
				
				if($edit_id != 0)
					tep_db_perform("players", $data,  'update', "player_id = '" . $edit_id . "'");
				
				if($edit_id == 0)
					tep_db_perform("players", $data,  'insert');
					
			}

		$info_box_contents = array();
  		$info_box_contents[] = array('text' => "Edit/Add player info");
 		new infoBoxHeading($info_box_contents, true, false, false);
		
		echo tep_draw_form("edit_player", "", 'post');
		//get player info
		$player = tep_get_player_info($edit_id);
		$contents = array();		
		$contents[]=
		array(
			array('text'=>BOX_LABEL_PLAYER_NUMBER . tep_draw_input_field("player_number", $player['player_number'], "size=1", 'text', true)),
			array('text'=>BOX_LABEL_PLAYER_NAME . tep_draw_input_field("player_name", $player['player_name'], "size=20", 'text', true)),
			array('text'=>BOX_LABEL_PLAYER_BIRTHDATE . tep_draw_input_field("player_birthdate", $player['player_birthdate'], "size=12", 'text', true)),
			array('text'=>BOX_LABEL_PLAYER_POSITION . tep_draw_input_field("player_position", $player['player_position'], "size=5", 'text', true)),
			array('text'=>BOX_LABEL_PLAYER_HEIGHT . tep_draw_input_field("player_height", $player['player_height'], "size=3", 'text', true)),
			array('text'=>BOX_LABEL_PLAYER_BIRTHPLACE . tep_draw_input_field("player_birthplace", $player['player_birthplace'], "size=15", 'text', true)),
			array('text'=>BOX_LABEL_PLAYER_NATIONALITY . tep_draw_input_field("player_nationality", $player['player_nationality'], "size=15", 'text', true))
			);
	
		$contents[]=
		array(
			array('text'=>"&nbsp;"),
			array('text'=>BOX_LABEL_PLAYER_PREVIOUS_TEAM . tep_draw_input_field("player_previous_team", $player['player_previous_team'], "size=70", 'text', true), 'params'=>"colspan=3"),
			array('text'=>BOX_LABEL_PLAYER_YEARS_IN_TEAM . tep_draw_input_field("player_years_in_team", $player['player_years_in_team'], "size=1", 'text', true)),
			array('text'=>"&nbsp;",'params'=>"colspan=2")
			);
	
			
		$contents[]=
		array(
			array('text'=>"&raquo;"),		
			array('text'=>BOX_LABEL_PLAYER_DESCRIPTION . tep_draw_textarea_field("player_description", true, 44, 5, $player['player_description']), 'params'=>"colspan=4"),
			array('text'=>(tep_get_languages_list("player_language_id", tep_not_null($player['player_language_id']) ? $player['player_language_id'] : $language_id)) . "<br><br>" . tep_draw_checkbox_field('duplicate', true, false) . " duplicate", 'params'=>" align=left"),
			array('text'=>tep_draw_input_field("form_sumbit", " OK ", "class=button_submit", "submit"), 'params'=>"colspan=2 align=center")
			);
		
		$contents[]=
		array(
			array('text'=>""),		
			array('text'=>mk_output_message("warning","Player will be added to team_id: ".tep_get_team_info($team_id, "", "team_name") . " / " . tep_get_team_info($team_id, "", "team_season")), 'params'=>"colspan=5"),
			array('text'=>tep_draw_input_field("form_sumbit", " OK ", "class=button_submit", "submit"), 'params'=>"colspan=2 align=center")
			);	
		new contentBox($contents);
		
		echo  tep_draw_hidden_field("edit_confirm", true);
		echo  tep_draw_hidden_field("edit_id", $edit_id);
		echo  tep_draw_hidden_field("player_team", $team_id);
		echo "</form>";


	}
?>
<br>