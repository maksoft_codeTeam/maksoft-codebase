<?php

$edit_tid = $_GET['edit_tid'];
$edit_confirm = $_POST['edit_confirm'];

$del_tid = $_POST['del_tid'];
if(tep_not_null($del_tid))
	tep_db_query("DELETE FROM " . TABLE_TEAMS . " WHERE player_id='$del_tid'");


if(tep_not_null($edit_tid))
	{

		if($edit_confirm)
			{
				/*		
				//upload posted files
				$player_photo = new upload('player_photo');
				$player_photo->set_destination(DIR_WS_PLAYERS);
				if ($player_photo->parse() && $player_photo->save())
				 {
				  $player_photo_name = $player_photo->filename;
				} else {
				  $player_photo_name = (isset($_POST['player_previous_photo']) ? $_POST['player_previous_photo'] : '');
				}
				*/

				$team_logo_name = (isset($_POST['team_previous_logo']) ? $_POST['team_previous_logo'] : '');
				$data = array(
				'team_language_id' => tep_db_prepare_input($_POST['team_language_id']), 
				'team_shortname'=>tep_db_prepare_input($_POST['team_shortname']), 
				'team_name'=>tep_db_prepare_input($_POST['team_name']),
				'team_season'=>tep_db_prepare_input($_POST['team_season']), 
				'team_description'=>tep_db_prepare_input($_POST['team_description']), 
				'team_coach'=>tep_db_prepare_input($_POST['team_coach']),
				'team_url'=>tep_db_prepare_input($_POST['team_url'])
				);
				
				if($_POST['duplicate']) $edit_tid = 0;

				if($edit_tid != 0)
					tep_db_perform("teams", $data,  'update', "team_id = '" . $edit_tid . "'");
				
				if($edit_tid == 0)
					tep_db_perform("teams", $data,  'insert');

		
			}

		$info_box_contents = array();
  		$info_box_contents[] = array('text' => "Edit/Add team info");
 		new infoBoxHeading($info_box_contents, true, false, false);
		
		echo tep_draw_form("edit_team", "", 'post');
		//get player info
		$team = tep_get_team_info($edit_tid);
		$contents = array();		

		$contents[]= array('text'=>BOX_LABEL_TEAM_SHORTNAME . tep_draw_input_field("team_shortname", $team['team_shortname'], "size=50", 'text', true));
		$contents[]= array('text'=>BOX_LABEL_TEAM_NAME . tep_draw_input_field("team_name", $team['team_name'], "size=50", 'text', true));
		$contents[]= array('text'=>BOX_LABEL_TEAM_COACH . tep_draw_input_field("team_coach", $team['team_coach'], "size=50", 'text', true));
		$contents[]= array('text'=>BOX_LABEL_TEAM_URL . tep_draw_input_field("team_url", $team['team_url'], "size=50", 'text', true));
	
		$seasons_array = array(array("id"=>"2007/2008", "text"=>"����� 2007/2008"), array("id"=>"2008/2009", "text"=>"����� 2008/2009"), array("id"=>"2009/2010", "text"=>"����� 2009/2010"), array("id"=>"2010/2011", "text"=>"����� 2010/2011"), array("id"=>"2011/2012", "text"=>"����� 2011/2012"));
			
		$contents[]= array('text'=>BOX_LABEL_TEAM_SEASON . tep_draw_pull_down_menu("team_season", $seasons_array, '', '', false));
		$contents[]= array('text'=>BOX_LABEL_TEAM_DESCRIPTION . tep_draw_textarea_field("team_description", true, 44, 5, $player['team_description']), 'params'=>"colspan=4");
		$contents[]= array('text'=>(tep_get_languages_list("team_language_id", tep_not_null($team['team_language_id']) ? $team['team_language_id'] : $language_id)) . "<br><br>" . tep_draw_checkbox_field('duplicate', true, false) . " duplicate", 'params'=>" align=left");
		$contents[]= array('text'=>tep_draw_input_field("form_sumbit", " OK ", "class=button_submit", "submit"), 'params'=>"colspan=2 align=center");
		
		$label  = tep_not_null($team['team_logo']) ? "<em>( <a href=\"".DIR_WS_IMAGES.$team['team_logo']."\" target=\"_blank\" rel=\"lightbox\">".$team['team_logo']."</a> )</em><br>" : "<br>"; 
		
		$contents[]= array('text'=> BOX_LABEL_TEAM_PHOTO . $label . tep_draw_input_field("team_logo", $team['team_logo'],'size=70', 'file', true));
		$contents[]= array('text'=>tep_draw_hidden_field("team_previous_logo", $team['team_logo'], ""), 'params'=>" align=left");

		//echo "<br>delete image:".tep_draw_checkbox_field("product_image_delete", false, false, "onClick=\"alert('The small image will be deleted !')\" ") ;
		new contentBox($contents);

		echo tep_draw_hidden_field("team_previous_logo", $team['team_logo'], "");
		echo  tep_draw_hidden_field("edit_confirm", true);
		echo  tep_draw_hidden_field("edit_tid", $edit_tid);
		echo "</form>";


	}
?>
<br>