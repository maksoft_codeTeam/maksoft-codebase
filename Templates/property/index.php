<html>
<head>
<title><?php echo("$Title"); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<meta http-equiv="Pragma" content="no-cache">
<?php
// includes the meta tags of the site
include("Templates/meta_tags.php"); 

// include library functions
//include("lib/lib_functions.php");

//set the drop-down menu category : only sites with toplink=drop_links will be shown in the menu
//$drop_links = '';


//default CSS
echo "<link href='http://www.maksoft.net/css/property/property.css' rel='stylesheet' type='text/css'>";

define ("DIR_TEMPLATE","Templates/property");
define ("DIR_TEMPLATE_IMAGES","http://maksoft.net/Templates/property");
$MAX_WIDTH = 840;



?>
<SCRIPT LANGUAGE="JavaScript">
<!--
function menu_position()
{
var max_width = 840;
var main_width = 615;
var menu_width = 225;
var menu_top = 125;

w = document.body.clientWidth;
h = document.body.clientHeight;

//pos_left = parseInt(w/2 + main_width/2 - menu_width);
pos_left = parseInt((w - max_width)/2);

var menu = document.getElementById('menu');
//alert (w + ":" + h)
menu.style.left = pos_left;
menu.style.top = menu_top;
menu.style.visibility = 'visible';
}

function clear_text(form) 
				{
				 if ((form.Search.value == '<?=$Site->SearchText ?>') || (form.Search.value == 'search')) {form.Search.value = "";}
				}
				
function do_search(form) 
				{
				 if (form.Search.value != "") {form.submit();}
				}
				
//-->
</SCRIPT>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="t4" onLoad="menu_position">
<table border="0" align="center" width="<?=$MAX_WIDTH?>

" height="100%" cellpadding="0" cellspacing="0" style="padding-top: 0px; ">
<tr><td valign="top">
		<?php
			// including header file
			if(file_exists(DIR_TEMPLATE."/header.php")) include_once DIR_TEMPLATE."/header.php";
		?>
<tr><td valign="top" height="100%">
		<?php
			// including main content		
			if(file_exists(DIR_TEMPLATE."/main.php")) include_once DIR_TEMPLATE."/main.php";
		?>
<tr><td height="50" valign="top">
		<?php
			// including footer
			if(file_exists(DIR_TEMPLATE."/footer.php")) include_once DIR_TEMPLATE."/footer.php";
		?>
</tr>
</table>
</body>
</html>

