<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<?php

include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/penny_market/");
define("DIR_TEMPLATE_IMAGES", "Templates/penny_market/images/");
define("DIR_MODULES","web/admin/modules/");

$o_site->print_sConfigurations();

//define some pages
$n_promotions = "124252";
$n_services = "124253";
$n_careers = "124256";
$n_news = $o_site->get_sNewsPage();
$n_comments = "125746";
$n_current = "125747";
$n_search = "126233";
$n_game = "124525";
$n_markets = "126487";
$n_about_clients = "129543";

$page_promotions = $o_page->get_page($n_promotions);
$page_service = $o_page->get_page($n_services);
$page_careers = $o_page->get_page($n_careers);
$page_news = $o_page->get_page($n_news);
$page_comments = $o_page->get_page($n_comments);
$page_current = $o_page->get_page($n_current);
$page_game = $o_page->get_page($n_game);
$page_markets = $o_page->get_page($n_markets);
$page_about_clients = $o_page->get_page($n_about_clients);

if($user->AccessLevel == 0)
	$promotions = $o_page->get_pSubpages($n_promotions, "RAND() LIMIT 18");
else $promotions = $o_page->get_pSubpages($n_promotions, "RAND() LIMIT 3");

$news = $o_page->get_pSubpages($n_news, "p.date_added DESC LIMIT 3");
$markets_list = $o_page->get_pSubpages($n_markets, "p.Name ASC");

//load markets list
include "markets.php";

?>
<title><?php echo("$Title");?></title>
<link href="http://www.maksoft.net/Templates/penny_market/layout.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/penny_market/base_style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="lib/datepicker/datepicker.css" type="text/css">
<!--[if IE]>
<link href='http://www.maksoft.net/Templates/penny_market/ie_fix.css' rel='stylesheet' type='text/css'>
<![endif]-->
<script src="http://www.maksoft.net/Templates/penny_market/effects.js" language="JavaScript" type="text/javascript"></script>
<script src="http://www.maksoft.net/lib/jquery/tabcontent/tabcontent.js" language="JavaScript" type="text/javascript"></script>
<script src="http://www.maksoft.net/lib/jquery/easyslider/easySlider1.7.js" language="JavaScript" type="text/javascript"></script>
<script src="http://www.maksoft.net/lib/datepicker/datepicker.js" language="JavaScript" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>
<body>
<div align="center" class="bg">
	<div id="page_container" align="center">
		<div id="top_menu">
			<?php
				$top_menu = $o_page->get_pSubpages(0, "sort_n ASC", "p.toplink=1");
				$bullet = "";
				for($i=0; $i<count($top_menu); $i++)
					{	
						$class = "";
						if($o_page->n == $top_menu[$i]['n'] && $o_page->n != $o_site->get_sStartPage()) $class = "selected";
						echo "<a href=\"".$top_menu[$i]['page_link']."\" title=\"".$top_menu[$i]['Name']."\" class=\"".$class."\">".$top_menu[$i]['Name']."</a>";
					}
			?>
            <div class="list markets"><?php list_markets(); ?></div>
		</div>
	
		<div id="header">
			<?php include "header.php"; ?>


		</div>
		<div id="<?=$o_page->get_pSiteID() == $o_site->SiteID ? "main":"admin"?>">
				<div id="menu">
						<?php include "column_right.php"; ?>
				</div>
				<div class="content">
					<?php
						if($o_page->n == $o_site->get_sStartPage())
							include "home.php";
						else
							include "main.php";
					?>
				<div id="nav_links"><?=$o_page->get_pNavigation()?></div>
				</div>
		<div id="footer"><?php include "footer.php";?></div>
		</div>
		</div>
	</div>
</div>
<?php
	$site_url = $_SERVER['HTTP_HOST'];
	$site_url = str_replace("www.", "", $site_url);
	$site_url = str_replace("ww.", "", $site_url);
	
		
	if($site_url == "pennyjobsbg.com")
		$ga_id = "UA-21086986-1";
	else	
	if($site_url == "pennyproductsbg.com")
		$ga_id = "UA-21086991-1";	
	else
		$ga_id = "UA-19463072-1";

?>
		<script type="text/javascript">
		
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', '<?=$ga_id?>']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>
</body>
</html>
