<script type="text/javascript">
		$j(document).ready(function(){	
			$j("#box_promotions").easySlider({
				auto: true, 
				controlsShow: false,
				boxWidth: 780,
				boxHeight: 270,
				speed: 1000,
				pause: 3000,
				continuous: true
			});
		});	
</script>

<div id="box_promotions">
	<ul>
	<li class="first">
	<?php
		$li_counter = 1;
		for($i=0; $i<count($promotions); $i++)
			{
				$promo_text = strip_tags(crop_text($promotions[$i]['textStr']), "<li><LI><sup>");
				$promo_text = preg_replace('/\s\s+/', '', $promo_text);
				$promo_text = str_replace('<LI>', '<li>', $promo_text);
				$promo_text_array = explode("<li>", $promo_text);
				$promo_title = cut_text(strip_tags($promo_text_array[1]), 32);
				$promo_qty = strip_tags($promo_text_array[2]);
				$promo_price = str_replace("��.", "", strip_tags($promo_text_array[3],"<sup><SUP>"));
				$promo_price = str_replace("��.", "", $promo_price);
				$promo_price = str_replace("��", "", $promo_price);
				$promo_price = str_replace("��", "", $promo_price);
				if($li_counter >3)
					{
						$li_counter = 1;
						echo "<li>";
					}
				$li_counter ++;
				?>
					<img src="Templates/penny_market/images/ps_right.jpg" align="left" hspace="0" alt=""></a>
					<div class="promo">
							<div class="img_preview"><a href="<?=$promotions[$i]['page_link']?>" title="<?=$promotions[$i]['Name']?>"><img src="<?=$promotions[$i]['image_src']?>" hspace="0" alt=""></a></div>
							<div class="title"><div><?=$promo_title?><br><span class="qty"><?=$promo_qty?></span></div></div>
							<div class="price"><?=$promo_price?></div>
						</div>
					<img src="Templates/penny_market/images/ps_left.jpg" align="left" hspace="0" alt="">
				<?php
			}
	?>
	</ul>
</div>