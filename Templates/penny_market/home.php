<div style="clear: both">
	<div id="home_menu">
		<a href="javascript: void(0)" rel="box_promotions" title="<?=$page_promotions['Name']?>" class="promo"></a>
		<a href="javascript: void(0)" rel="box_service" title="<?=$page_service['Name']?>" class="service"></a>
		<a href="javascript: void(0)" rel="box_careers" title="<?=$page_careers['Name']?>" class="careers"></a>
	</div>
	<?php
		include "box_promotions.php";
		include "box_services.php";
		include "box_careers.php";
	?>


			<script type="text/javascript">
				var home_menu = new ddtabcontent("home_menu")
				home_menu.setpersist(true)
				home_menu.setselectedClassTarget("linkparent") //"link" or "linkparent"
				home_menu.init()
			</script>

</div>
<div style="clear: both">
	<div id="tab_menu">
		<a href="javascript: void(0)" rel="box_news" title="<?=$page_news['Name']?>" class="news"><?=$page_news['Name']?></a>
		<a href="javascript: void(0)" rel="box_comments" title="<?=$page_comments['Name']?>" class="comments"><?=$page_comments['Name']?></a>
		<a href="javascript: void(0)" rel="box_current" title="<?=$page_current['Name']?>" class="current"><?=$page_current['Name']?></a>
	</div>
	<div id="pageContent">
		<?php
			include "box_news.php";
			include "box_comments.php";
			include "box_current.php";
		?>
	
			<script type="text/javascript">
				var tab_menu = new ddtabcontent("tab_menu")
				tab_menu.setpersist(true)
				tab_menu.setselectedClassTarget("linkparent") //"link" or "linkparent"
				tab_menu.init()
			</script>
	</div>
	<div class="bottom_line"></div>
</div>