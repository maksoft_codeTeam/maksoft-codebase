<!DOCTYPE html>
<html lang="<?php echo($Site->language_key); ?>">
<head>
<meta http-equiv="content-type" content="text/html;charset=windows-1251" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title><?=$Title?></title>
	<?php
	require_once "lib/lib_page.php";
	require_once "lib/Database.class.php";
	//include meta tags
	include( "Templates/meta_tags.php" );
	$o_site->print_sConfigurations();

	$db = new Database();
	$o_page->setDatabase( $db );
	$o_user = new user( $user->username, $user->pass );
	?>
<link rel="icon" href="<?=ASSETS_DIR?>assets/images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="<?=ASSETS_DIR?>assets/images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/layout.css" media="all" />
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/jquery.mmenu.all.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/jquery-ui.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/owl.carousel.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/owl.theme.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/owl.transitions.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/jquery.pagepiling.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/jquery.multiscroll.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/swiper.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/nouislider.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/jcf.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/styles.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/revslider/rs/settings.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/revslider/dynamic.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/revslider/static.css" media="all" />
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/js/slider.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:normal,semibold,bold,italic,semibolditalic,bolditalic" />
</head>
<body class=" cms-index-index cms-home-page">

<div id="framework-global-notices">
  <noscript>
  <div class="global-site-notice noscript">
    <div class="container notice-inner">
      <p><strong>JavaScript seems to be disabled in your browser.</strong><br />
        You must have JavaScript enabled in your browser to utilize the functionality of this website.</p>
    </div>
  </div>
  </noscript>
</div>

<div id="framework-mobile-header" class="mm-fixed">
  <div class="mfb_row row">
    <div class="mfb_col col-lg-12 col-md-12 col-sm-12 col-xs-12 column">
      <div class="framework-table framework-mobile-header">
        <div class="framework-table-cell text-left">
          <ul class="menu-horizontal">
            <li> <a href="#mobile-menu" id="trigger-categories1" class="button-menu nav-on-left" > <i class="fa fa-bars" aria-hidden="true"></i> <i class="fa fa-times" aria-hidden="true"></i> </a> </li>
          </ul>
        </div>
        
        <div class="framework-table-cell text-center"> 
        <a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>">
        <img src="<?=ASSETS_DIR?>assets/images/ontariosport/default-logo_1.png" alt="" />
        </a>
        </div>
        
        <div class="framework-table-cell text-right">
          <ul class="menu-horizontal">
            <li> <a href="#mobile-search" class="button-search"> <i class="fa fa-search" aria-hidden="true"></i> </a> </li>
          </ul>
        </div>
      </div>
      <div id="mobile-menu">
        <ul>
        

							<?php
							if ( !isset( $mobile_links ) )$mobile_links = 3;
							$mobile_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $mobile_links" );

							for ( $i = 0; $i < count( $mobile_links ); $i++ ) {
								$subpages = $o_page->get_pSubpages( $mobile_links[ $i ][ 'n' ] );
							?>
							<li <?php echo ((count($subpages)> 0) ? "" : "") ?>>
							<a href="<?php echo $o_page->get_pLink($mobile_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <?php echo $mobile_links[$i]['Name'] ?></a>
								<?php echo ((count($subpages) > 0) ? "<ul>" : "</li>") ?>
								<?php
								if ( count( $subpages ) > 0 ) {
									for ( $j = 0; $j < count( $subpages ); $j++ )
										echo "<li><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\"><div class=\"child-title\">" . $subpages[ $j ][ 'Name' ] . "</div></a></li>";
									?>
						</ul>
						</li>
							<?php 
							} 
							}
							?>
							
        </ul>
      </div>
      <div id="mobile-filter"> </div>
      <div id="mobile-search">
        <div>
          <form id="mobile-search-mini-form-header" method="get" class="search-form" action="page.php">
          <input type="hidden" name="n" value="19369227">
           <input type="hidden" name="SiteID" value="<?php echo $o_site->SiteID?>">
            <div class="search-form"> <span class="search-submit-wrapper">
              <button type="submit" title="Search" class="btn btn-default button">�����</button>
              </span> <span class="search-input-wrapper">
              <input name="search" id="search" placeholder="�����..." class="input-text" maxlength="128">
              </span> </div>
            <div id="mobile-search-autocomplete-header" class="search-autocomplete"></div>
<!--            <script type="text/javascript">
                //<![CDATA[
                var searchFormMobile = new Varien.searchForm('mobile-search-mini-form-header', 'mobile-search-input-header', 'Search entire store here...');
                searchFormMobile.initAutocomplete('catalogsearch/ajax/suggest/index.html', 'mobile-search-autocomplete-header');
                //]]>
            </script>-->
          </form>
        </div>
      </div>

      <script>
    jQuery(document).ready(function ($) {

        $("#mobile-menu").mmenu({
            navbar: {
                title: "���������"
            }
        },
        {
            classNames: {
                fixedElements: {
                    fixed: "mm-fixed"
                }
            }
        });

        $("#mobile-filter").mmenu({
            navbar: {
                title: "Filter"
            }
        },
        {
            classNames: {
                fixedElements: {
                    fixed: "mm-fixed"
                }
            }
        });
        $("#mobile-search").mmenu({
            navbar: {
                title: "�����"
            }
        },
        {
            classNames: {
                fixedElements: {
                    fixed: "mm-fixed"
                }
            }
        });
        $("#mobile-cart").mmenu({
            navbar: {
                title: "Shopping Bag"
            }
        },
        {
            classNames: {
                fixedElements: {
                    fixed: "mm-fixed"
                }
            }
        });
    });
</script>
 </div>
  </div>
</div>

<div id="framework-desktop-header">
  <div class="mfb_row row">
    <div class="mfb_col col-lg-12 col-md-12 col-sm-12 col-xs-12 column mfb_ccss_var07-top-box">
      <div class="mfb_row row container">
       
        <div class="mfb_col col-lg-6 col-md-6 col-sm-6 col-xs-6 column mfb_ccss_var07-top-box-left-side">
        <a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>"> 
        <img src="<?=ASSETS_DIR?>assets/images/ontariosport/ontario-sport-logo.png" alt="Ontario Sport" /> 
        </a>
        </div>
        
        <div class="mfb_col col-lg-6 col-md-6 col-sm-6 col-xs-6 column mfb_ccss_var07-top-box-right-side">
          <div class="google-translate-container">
            <div id="google_translate_element"></div>
          </div>
          
<!--          <script type="text/javascript">function googleTranslateElementInit(){new google.translate.TranslateElement({pageLanguage:'bg',includedLanguages:'ar,az,be,bg,bs,ca,cs,cy,da,de,el,en,eo,es,et,eu,fr,ga,hr,hu,hy,it,iw,la,lt,lv,mk,mt,nl,no,pl,pt,ro,ru,sk,sl,sq,sr,sv,uk,uz',layout:google.translate.TranslateElement.InlineLayout.SIMPLE},'google_translate_element');}</script> <script type="text/javascript" src="../translate.google.com/translate_a/elementa0d8.html?cb=googleTranslateElementInit"></script>-->
          
          <ul class="MonsterMenuBuilder var07-top-links">
            
							<?php
							if ( !isset( $top_links ) )$top_links = 5;
							$top_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $top_links" );

							for ( $i = 0; $i < count( $top_links ); $i++ ) {
								$subpages = $o_page->get_pSubpages( $top_links[ $i ][ 'n' ] );
							?>
							<li class="item level-0 " <?php echo ((count($subpages)> 0) ? "" : "") ?>>
							<a href="<?php echo $o_page->get_pLink($top_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <?php echo $top_links[$i]['Name'] ?></a>
							</li>
							<?php 
							}
							?>
          </ul>
          
          <div class="var07-vertical-divider"></div>
          <ul class="MonsterMenuBuilder var07-top-social">
           
            <li class="item level-0 "><a href="https://www.facebook.com/www.ontariosport.net/?fref=ts"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li class="item level-0 "><a href="https://plus.google.com/108353686440072427722/about?gmbpt=true&amp;_ga=1.103257472.1656264047.1461766002"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
            <li class="item level-0 "><a href="https://www.instagram.com/ontariosport6228/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            
          </ul>
        </div>
      </div>
    </div>
  </div>
  
  <div class="mfb_row row">
    <div class="mfb_col col-lg-12 col-md-12 col-sm-12 col-xs-12 column mfb_ccss_.mfb_ccss_var07-header-main-box mfb_ccss_var07-header-main-box">
      <div class="mfb_row row container">
        <div class="mfb_col column mfb_ccss_megamenu col-lg-11 col-md-11 col-sm-11 col-xs-11" style="display: block;">
          <ul class="framework-megamenu-default framework-dropdown dropdown1">
           
							<?php
							if ( !isset( $head_links ) )$head_links = 3;
							$head_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $head_links" );

							for ( $i = 0; $i < count( $head_links ); $i++ ) {
								$subpages = $o_page->get_pSubpages( $head_links[ $i ][ 'n' ] );
							?>
							<li class="level0" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
							<a href="<?php echo $o_page->get_pLink($head_links[$i]['n']) ?>" class="level0 has-children" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <?php echo $head_links[$i]['Name'] ?></a>
								<?php echo ((count($subpages) > 0) ? "<ul class='row megamenu-dropdown'>" : "</li>") ?>
								<?php
								if ( count( $subpages ) > 0 ) {
									for ( $j = 0; $j < count( $subpages ); $j++ )
										echo "<li><div class=\"col-sm-12\"><ul class=\"level0\"><li class='level1 nav-1-1 first megamenu-col clear-left col-first'><div class=\"list-container\"><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\" class=\"subcategories-first level1 \"><span class=\"child-title\">" . $subpages[ $j ][ 'Name' ] . "</span></a></div></li></ul></div></li>";
									?>
						</ul>
						</li>
							<?php 
							} 
							}
							?>

          </ul>
        </div>
        <div class="mfb_col column mfb_ccss_var07-header-main-right-side col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
      </div>
    </div>
  </div>
</div>

<div id="framework-sticky-header">
  <div class="mfb_row row sticky mfb_ccss_var07-header-main-box">
   
    <div class="mfb_col column mfb_ccss_sticky-menu col-md-2 col-sm-2 col-xs-2 col-lg-2">
    <a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>">
    <img src="<?=ASSETS_DIR?>assets/images/ontariosport/ontario-sport-small-logo.png" alt="Ontario Sport" />
    </a>
    </div>
    
    <div class="mfb_col column col-md-10 col-sm-10 col-xs-10 mfb_ccss_megamenu col-lg-10">
      <ul class="framework-megamenu-default framework-dropdown dropdown1">
       
							<?php
							if ( !isset( $sticky_links ) )$sticky_links = 3;
							$sticky_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $sticky_links" );

							for ( $i = 0; $i < count( $sticky_links ); $i++ ) {
								$subpages = $o_page->get_pSubpages( $sticky_links[ $i ][ 'n' ] );
							?>
							<li class="level0" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
							<a href="<?php echo $o_page->get_pLink($sticky_links[$i]['n']) ?>" class="level0 has-children" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <?php echo $sticky_links[$i]['Name'] ?></a>
								<?php echo ((count($subpages) > 0) ? "<ul class='row megamenu-dropdown'>" : "</li>") ?>
								<?php
								if ( count( $subpages ) > 0 ) {
									for ( $j = 0; $j < count( $subpages ); $j++ )
										echo "<li><div class=\"col-sm-12\"><ul class=\"level0\"><li class='level1 nav-1-1 first megamenu-col clear-left col-first'><div class=\"list-container\"><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\" class=\"subcategories-first level1 \"><span class=\"child-title\">" . $subpages[ $j ][ 'Name' ] . "</span></a></div></li></ul></div></li>";
									?>
						</ul>
						</li>
							<?php 
							} 
							}
							?>
							
        
      </ul>
    </div>
  </div>
</div>