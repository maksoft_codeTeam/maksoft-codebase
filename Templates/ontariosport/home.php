
<div id="framework-content" class="content-container">
  <main class="themeengine-content">
    <div>
      <div class="row">
        <div class="col-sm-12">
          <div class="std">
            <div class="mfb_row row" style="display: block;">
              <div class="mfb_col col-lg-12 col-md-12 col-sm-12 col-xs-12 column" style="display: block;">
                <link href='http://fonts.googleapis.com/css?family=Dancing+Script:300,400,600,700,800|Open+Sans:300,400,600,700,800|Oswald:300,400,600,700,800' rel='stylesheet' type='text/css' />
                <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:596px;"> 
                  <!-- START REVOLUTION SLIDER  fullwidth mode -->
                  <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;max-height:596px;height:596px;">
                    <ul>
                      <!-- SLIDE  -->
                      <li data-transition="fade" data-slotamount="7" data-masterspeed="100" data-saveperformance="off" data-title="Slide"> 
                        <!-- MAIN IMAGE --> 
                        <img src="<?=ASSETS_DIR?>assets/images/sliders/OntarioSport-Slider-01.jpg"  alt="Slide"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        <!-- LAYERS --> 
                      </li>
                      <!-- SLIDE  -->
                      <li data-transition="fade" data-slotamount="7" data-masterspeed="100" data-saveperformance="off"  data-title="Slide"> 
                        <!-- MAIN IMAGE --> 
                        <img src="<?=ASSETS_DIR?>assets/images/sliders/OntarioSport-Slider-04.jpg"  alt="Slide"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        <!-- LAYERS --> 
                      </li>
                      <!-- SLIDE  -->
                      <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-saveperformance="off"  data-title="Slide"> 
                        <!-- MAIN IMAGE --> 
                        <img src="<?=ASSETS_DIR?>assets/images/sliders/OntarioSport-Slider-03.jpg"  alt="Slide"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        <!-- LAYERS --> 
                      </li>
                      <!-- SLIDE  -->
                      <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off"  data-title="Slide"> 
                        <!-- MAIN IMAGE --> 
                        <img src="<?=ASSETS_DIR?>assets/images/sliders/OntarioSport-Slider-07.jpg"  alt="Slide"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        <!-- LAYERS --> 
                      </li>
                      <!-- SLIDE  -->
                      <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off"  data-title="Slide"> 
                        <!-- MAIN IMAGE --> 
                        <img src="<?=ASSETS_DIR?>assets/images/sliders/OntarioSport-Slider-02.jpg"  alt="Slide"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        <!-- LAYERS --> 
                      </li>
                      <!-- SLIDE  -->
                      <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off"  data-title="Slide"> 
                        <!-- MAIN IMAGE --> 
                        <img src="<?=ASSETS_DIR?>assets/images/sliders/OntarioSport-Slider-06.jpg"  alt="Slide"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        <!-- LAYERS --> 
                      </li>
                      <!-- SLIDE  -->
                      <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off"  data-title="Slide"> 
                        <!-- MAIN IMAGE --> 
                        <img src="<?=ASSETS_DIR?>assets/images/sliders/OntarioSport-Slider-09.jpg"  alt="Slide"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        <!-- LAYERS --> 
                      </li>
                      <!-- SLIDE  -->
                      <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off"  data-title="Slide"> 
                        <!-- MAIN IMAGE --> 
                        <img src="<?=ASSETS_DIR?>assets/images/sliders/OntarioSport-Slider-010.jpg"  alt="Slide"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        <!-- LAYERS --> 
                      </li>
                    </ul>
                    <div class="tp-bannertimer"></div>
                  </div>
                  <div class="revsliderstyles">
                    <style>
</style>
                  </div>
                  <script type="text/javascript">

				/******************************************
					-	PREPARE PLACEHOLDER FOR SLIDER	-
				******************************************/
				
				(function(jQuery) {

				var setREVStartSize = function() {
					var	tpopt = new Object();
						tpopt.startwidth = 1920;
						tpopt.startheight = 596;
						tpopt.container = jQuery('#rev_slider_1_1');
						tpopt.fullScreen = "off";
						tpopt.forceFullWidth="off";

					tpopt.container.closest(".rev_slider_wrapper").css({height:tpopt.container.height()});tpopt.width=parseInt(tpopt.container.width(),0);tpopt.height=parseInt(tpopt.container.height(),0);tpopt.bw=tpopt.width/tpopt.startwidth;tpopt.bh=tpopt.height/tpopt.startheight;if(tpopt.bh>tpopt.bw)tpopt.bh=tpopt.bw;if(tpopt.bh<tpopt.bw)tpopt.bw=tpopt.bh;if(tpopt.bw<tpopt.bh)tpopt.bh=tpopt.bw;if(tpopt.bh>1){tpopt.bw=1;tpopt.bh=1}if(tpopt.bw>1){tpopt.bw=1;tpopt.bh=1}tpopt.height=Math.round(tpopt.startheight*(tpopt.width/tpopt.startwidth));if(tpopt.height>tpopt.startheight&&tpopt.autoHeight!="on")tpopt.height=tpopt.startheight;if(tpopt.fullScreen=="on"){tpopt.height=tpopt.bw*tpopt.startheight;var cow=tpopt.container.parent().width();var coh=jQuery(window).height();if(tpopt.fullScreenOffsetContainer!=undefined){try{var offcontainers=tpopt.fullScreenOffsetContainer.split(",");jQuery.each(offcontainers,function(e,t){coh=coh-jQuery(t).outerHeight(true);if(coh<tpopt.minFullScreenHeight)coh=tpopt.minFullScreenHeight})}catch(e){}}tpopt.container.parent().height(coh);tpopt.container.height(coh);tpopt.container.closest(".rev_slider_wrapper").height(coh);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);tpopt.container.css({height:"100%"});tpopt.height=coh;}else{tpopt.container.height(tpopt.height);tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);}
				};

				/* CALL PLACEHOLDER */
				setREVStartSize();


				var tpj=jQuery;
				tpj.noConflict();
				var revapi1;

				tpj(document).ready(function() {

                if(tpj('#rev_slider_1_1').revolution == undefined){
					revslider_showDoubleJqueryError('#rev_slider_1_1');
                }else{
				   revapi1 = tpj('#rev_slider_1_1').show().revolution(
                    {    
                        						dottedOverlay:"none",
						delay:3000,
						startwidth:1920,
						startheight:596,
						hideThumbs:200,

						thumbWidth:100,
						thumbHeight:50,
						thumbAmount:5,

						
						simplifyAll:"off",

						navigationType:"none",
						navigationArrows:"solo",
						navigationStyle:"preview1",

						touchenabled:"on",
						onHoverStop:"on",
						nextSlideOnWindowFocus:"off",

						swipe_threshold: 75,
						swipe_min_touches: 1,
						drag_block_vertical: false,
						
						
						
						keyboardNavigation:"off",

						navigationHAlign:"center",
						navigationVAlign:"bottom",
						navigationHOffset:0,
						navigationVOffset:20,

						soloArrowLeftHalign:"left",
						soloArrowLeftValign:"center",
						soloArrowLeftHOffset:20,
						soloArrowLeftVOffset:0,

						soloArrowRightHalign:"right",
						soloArrowRightValign:"center",
						soloArrowRightHOffset:20,
						soloArrowRightVOffset:0,

						shadow:0,
						fullWidth:"on",
						fullScreen:"off",

                                                spinner:"off",
                        
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,

						shuffle:"off",

						autoHeight:"off",
						forceFullWidth:"off",
						
						
						
						hideThumbsOnMobile:"off",
						hideNavDelayOnMobile:1500,
						hideBulletsOnMobile:"off",
						hideArrowsOnMobile:"off",
						hideThumbsUnderResolution:0,

												hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						startWithSlide:0					});



					                }
				});	/*ready*/

				})($nwd_jQuery);

			</script> 
                </div>
                <!-- END REVOLUTION SLIDER --></div>
            </div>
            <div class="mfb_row row mfb_ccss_var07-container" style="display: block;">
              <div class="mfb_col col-lg-12 col-md-12 col-sm-12 col-xs-12 column mfb_ccss_ mfb_ccss_main-header" style="display: block;">
                <div class="mg-main-header">
                  <h1 class="mg-title"><?=$o_page->get_pTitle();?></h1>
                  <div class="mg-title-divider"></div>
                  <?php $o_page->print_pContent(); ?>
                </div>
              </div>
            </div>
            
            <!--<div class="mfb_row row mfb_ccss_var07-container-imgs" style="display: block;">
              <div class="mfb_col col-lg-12 col-md-12 col-sm-12 col-xs-12 column" style="display: block;">
                <div class="mfb_row row container">
        
				<?php
				if ( !isset( $home_links ) )$home_links = 3;
				$home_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $home_links" );

				for ( $i = 0; $i < count( $home_links ); $i++ ) {
					$subpages = $o_page->get_pSubpages( $home_links[ $i ][ 'n' ] );
					?>
				<div class="col-md-2 mfb_col mfb_ccss_var07-clogic-services-box column col-sm-6">
					<div>
				  		<div class="mg-services-box">
							<div class="mg-service-thumb"> <i class="mg-service-icon"><img src="<?=$o_page->get_pImage($home_links[$i]['n'])?>" alt="" /></i>
							
							</div>
							<h2 class="mg-service-title">
								<a href="<?=$o_page->get_pLink($home_links[$i]['n'])?>" title="<?=$home_links[$i]['Name']?>"><?=$home_links[$i]['Name']?></a>
							</h2>	
							<p class="mg-service-content">
				<?php
				if ( count( $subpages ) > 0 ) {
					for ( $j = 0; $j < count( $subpages ); $j++ )
						echo "" . $subpages[ $j ][ 'Name' ] . ", ";
					?>
							</p>
                      </div>
                    </div>
                  </div>

			<?php 
							} 
							}
							?>

                </div>
              </div>
            </div>-->
            
            <div class="mfb_row row mfb_ccss_variant07-row-bkg-heading" style="display: block;">
              <div class="mfb_col column mfb_ccss_variant07-bkg-heading col-lg-6 col-xs-12 col-sm-12 col-md-12">
                <div class="background-heading" style="background: url(<?=ASSETS_DIR?>assets/images/image-football.jpg);"></div>
              </div>
              <div class="mfb_col column col-lg-6 mfb_ccss_hide-bootstrap-padding col-xs-12 col-sm-12 col-md-12">
                <div class="mfb_row row mfb_ccss_var7-home-main-text">
                  <div class="mfb_col column mfb_ccss_var7-right-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="home-text">
                      <h2 class="home-title-products">�������� ������</h2>
                      <p class="text-products">���� �� ��������� �������� �� "������� �����" � ������������� � ������������ �� �������� ������ - � ���������� � ���������� ������ ��������. ��������� ����������� ������� �����, ������������������ ����������, ������������� ��<strong> FIFA</strong> ������������� - ���� � ������ �������� �� ������ �� ���-����� ����. ����������� �� ������ ���� � ������. ���������� �������� �����, ���������� �������� � ������ ��������, ������������ �� <strong>FIFA � UEFA</strong>. ������, ���������� �������, ������� �� ����������.</p>
                      <div class="image-link"><img alt="" src="<?=ASSETS_DIR?>assets/images/logo_football.png" / class="logo-btn-box"> <a class="foot-link" href="<?=$o_page->get_pLink(38454);?>">+ �������� ������</a></div>
                    </div>
                  </div>
                  <div class="mfb_col col-lg-6 col-sm-6 col-xs-6 column mfb_ccss_var7-home-symbol-box col-md-8">
                    <div class="var7-home-symbol">"</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="mfb_row row mfb_ccss_variant07-row-bkg-heading" style="display: block;">
              <div class="mfb_col column col-lg-6 col-xs-12 col-sm-12 col-md-12">
                <div class="mfb_row row mfb_ccss_var7-home-main-text">
                  <div class="mfb_col column mfb_ccss_var7-right-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="home-text">
                      <h2 class="home-title-products">����� �������</h2>
                      <p class="text-products">"������� �����" e ���������� � ������������ �� ����� �������.  ������������ �������� �� ���-������ ������������ � ������������� <strong>ITF /ATP, WTA, ETA</strong> ����� ������� �� ����� ����. ����, ��������� ����, ���������� ����� � ������ ������ ����������, "<strong>������</strong>" �������� �� ����� �������. ���������� � ��������������, ���������� � ��������� �� ����� �������. �������� �����������-�������, �������, ���-�����. ������� �� ����������.</p>
                      <div class="image-link"><img alt="" src="<?=ASSETS_DIR?>assets/images/bwf_logo.png" / class="logo-btn-box">
                        <div class="image-link"><img alt="" src="<?=ASSETS_DIR?>assets/images/itf-logo.png" / class="logo-btn-box"> <a class="foot-link" href="<?=$o_page->get_pLink(38455);?>">+ ����� �������</a></div>
                      </div>
                    </div>
                  </div>
                  <div class="mfb_col col-lg-6 col-md-6 col-sm-6 col-xs-6 column mfb_ccss_home-symbol-box-right">
                    <div class="home-symbol-right">"</div>
                  </div>
                </div>
              </div>
              <div class="mfb_col column col-lg-6 mfb_ccss_variant07-bkg-heading col-xs-12 col-sm-12 col-md-12">
                <div class="background-heading" style="background: url(<?=ASSETS_DIR?>assets/images/tennis-court.jpg);"></div>
              </div>
            </div>
            <div class="mfb_row row mfb_ccss_variant07-row-bkg-heading">
              <div class="mfb_col column col-lg-6 mfb_ccss_variant07-bkg-heading col-md-12 col-xs-12 col-sm-12">
                <div class="background-heading" style="background: url(<?=ASSETS_DIR?>assets/images/multi-field.jpg);"></div>
              </div>
              <div class="mfb_col column col-lg-6 mfb_ccss_hide-bootstrap-padding col-md-12 col-xs-12 col-sm-12">
                <div class="mfb_row row mfb_ccss_var7-home-main-text">
                  <div class="mfb_col column mfb_ccss_var7-right-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="home-text">
                      <h2 class="home-title-products">����������������� ������</h2>
                      <p class="text-products">"������� �����" ���������, ��������� �������� ������� �������� � �������� �� ���������� ����� � ���������� �������� - <strong>���� ������, ���������, ��������, �����, ������� � ��</strong>., ����������������� ������, ��������������� �����, ������ ��������, ������� � ���� �� �����, ������ ����, ���������� ������.�������� �����������-�������, �������, ���-�����. ������� �� ����������, ������������.</p>
                      <div class="image-link"><img alt="" src="<?=ASSETS_DIR?>assets/images/fiba-logo.png" / class="logo-btn-box"> <a class="foot-link" href="<?=$o_page->get_pLink(38456);?>">+ ����������������� ������</a></div>
                    </div>
                  </div>
                  <div class="mfb_col col-lg-6 col-md-6 col-sm-6 col-xs-6 column mfb_ccss_home-symbol-box-blu">
                    <div class="home-symbol-left">"</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="mfb_row row container">
              <div class="mfb_col col-lg-12 col-md-12 col-sm-12 col-xs-12 column mfb_ccss_var07-certificate">
                <div class="iso-certificates">
                  <p>ISO �����������</p>
                </div>
                <div class="iso-logos">
                  <ul>
                    <li><img src="<?=ASSETS_DIR?>assets/images/logo-iso-01.png" alt="" /></li>
                    <li><img src="<?=ASSETS_DIR?>assets/images/logo-iso-0.png" alt="" /></li>
                    <li><img src="<?=ASSETS_DIR?>assets/images/logo-iso-02.png" alt="" /></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>

