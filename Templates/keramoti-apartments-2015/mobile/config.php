<?php
	//template base/tmpl_001 configurations
	
	define("TEMPLATE_NAME", "mobile");
	define("TEMPLATE_DIR", "Templates/keramoti-apartments-2015/".TEMPLATE_NAME."/");
	define("TEMPLATE_IMAGES", "http://www.maksoft.net/".TEMPLATE_DIR."images/");
	
	//show what options are available for config
	$tmpl_config_support = array(
		"change_theme"=>true,					// full theme support
		"change_layout"=>true,					// layout support
		"change_banner"=>true,				// banner support
		"change_css"=>true,						// css support
		"change_background"=>true,		// background support
		"change_fontsize"=>true,				// fontsize support
		"change_max_width"=>true,			// site width support
		"change_menu_width"=>false,		// site menu width support
		"change_main_width"=>true,		// page content width support
		"change_logo"=>true,						// logo support
		"column_left" => true, 						// column left support
		"column_right" => true,					// column right support
		"drop_down_menu"=> true			// drop-down menu support
	);
	//define some default values
	$tmpl_config = array(
		"default_banner" => TEMPLATE_IMAGES."banner.jpg",
		"default_logo" => TEMPLATE_DIR."images/logo.png",	
		"date_format"=> "d-m-Y"
	);
	
	/* if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->get_sImageDIr()."/".basename($o_site->_site['Logo']); */

?>