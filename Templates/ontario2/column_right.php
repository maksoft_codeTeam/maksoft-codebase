<?php
	$info_box_header = array();
	$news_page = get_page($Site->news);
	$info_box_header[] = array('text' => "<a href=\"page.php?n=$news_page->n&SiteID=$news_page->SiteID\">".$news_page->Name."</a>");
	new infoBoxHeading($info_box_header, false, false, false);
	
	//news products
	$news_products_sql = "SELECT * FROM pages WHERE ParentPage = '$Site->news' AND SiteID='$SiteID' ORDER by sort_n DESC";
	$news_result = mysqli_query($news_products_sql);

	while($news = mysqli_fetch_object($news_result))
	{
		
		$news_img = get_page($news->n);

		$info_box_contents = array();
		$info_box_contents[] = array(
													array('text' =>"<a href=\"page.php?n=".$news->n."&SiteID=$SiteID\"><img src=\"img_preview.php?image_file=".$news_img->image_src."&img_width=80\" border=0></a>",'align'=>"center", 'params'=>"width=80 valign=top"),
													array('text' =>"<small>".date("F j, Y", strtotime($news->date_added)) ."</small><br><a href=\"page.php?n=$news->n&SiteID=$news->SiteID\" class=\"title\">".$news->Name."</a></span><br>".strip_tags(crop_text($news->textStr), ""), 'params'=>"valign=top width=220")
												);
		new contentBox($info_box_contents);
	}

	$info_box_header = array();
	$news_archive = get_page(38527);
	$info_box_header[] = array('text' => "<a href=\"page.php?n=$news_archive->n&SiteID=$news_archive->SiteID\">".$news_archive->Name."</a>");
	new infoBoxHeading($info_box_header, false, false, false);
?>