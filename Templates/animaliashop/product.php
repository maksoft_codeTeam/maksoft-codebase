<?php
$price=$prices = array();
if (user::$uID > 0 ){
    $prices = $o_page->get_pPrice($o_page->_page[ 'n' ]);
    $price = array_shift($prices);
}

?>
<section class="product">
    <div class="col-md-12">
        <div class="row" style="margin-bottom: 1%;">
            <div class="col-sm-3">
                <a href="<?php echo $o_page->get_pLink($o_page->_page['ParentPage']);?>"><i class="fa fa-chevron-left" aria-hidden="true"></i> ����� </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="thumbnail relative">
                    <figure id="zoom-primary" class="zoom" data-mode="mouseover">
                        <?= $o_page->print_pImage(600, "class=\"img-responsive\"") ?>
                    </figure>
                </div>
                <!-- Thumbnails (required height:100px) -->
                <div data-for="zoom-primary" class="zoom-more owl-carousel owl-padding-3 featured" data-plugin-options='{"singleItem": false, "autoPlay": false, "navigation": true, "pagination": false}'>
                    <a class="thumbnail active" href="<?= $o_page->get_pImage() ?>">
                        <img src="<?= $o_page->get_pImage() ?>" height="100" alt="" />
                    </a>
                    <?php
                        $page_gallery = $o_page->get_pSubpages(NULL, "p.sort_n ASC", "im.image_src !=''");
                        if ($page_gallery) {
                          $sub_title   = '';
                          $sub_content = '';
                          for ($i = 0; $i < count($page_gallery); $i++) {
                            $sub_title .= '<h5>' . $page_gallery[$i]['Name'] . '</h5>';
                            $sub_content .= '<p id=' . $page_gallery[$i]['Name'] . '>' . $page_gallery[$i]['textStr'] . '</p>';
                            echo "<a href=\"" . $o_page->get_pImage($page_gallery[$i]['n']) . "\" class=\"thumbnail\">";
                            $o_page->print_pImage(100, "", "", $page_gallery[$i]['n']);
                            echo "</a>";
                          } //$i = 0; $i < count($page_gallery); $i++
                        } //$page_gallery
                        $availability = is_available($o_page->get_sLanguage(), available($price));
                        ?>
                </div>
            <!-- /Thumbnails -->
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="shop-item-price">
                    <?= $o_page->print_pName() ?>
                </div>
                <?php if (user::$uID > 0 ){ ?>
                <div id="display_price" class="pPrice"><?=number_format( $price[ "price_value" ], '2', '.', '' ) . ' ' . $price[ "currency_string" ]?>/��.
                    <span id="av-cls" class="pull-right text-<?=$availability['code'];?> thinst">
                        <i class="fa fa-check"></i><?=$availability['status'];?>
                    </span>
                </div>
                <?php } ?>
                <hr/>
                    <?php
                    $subpages = $o_page->get_pSubpages();
                    if ( !empty( $subpages ) ) {
                        foreach ( $subpages as $_page ) {
                            eval( $_page[ 'PHPcode' ] );
                            $product = json_decode( $product );
                            $colors_bar .= get_colors( $product, $colors, "" );
                        }
                    }
                    // PRICES ONLY IF USER IS LOGGED
                    if ( $user->AccessLevel > 0 and ( !empty($prices) or $price ) ) {
                        require_once __DIR__.'/templates/add_product.php';
                    }
                    ?>
                <p>
                    <?=$o_page->_page['textStr']?>
                </p>
                <?php  if (in_promotion($product->initial)) { ?>
                <p>
                    <span class="pull-left text-success"><i class="glyphicon glyphicon-star-empty"></i>��������:</span>
                </p>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
