<!--<div class="container">
				<div class="row">
					<ol>
						<?="".$nav_bar.""?>
					</ol>
				</div>
</div>-->

<?php
include DIR_PHP . "inc/bepartner.php";
include DIR_PHP . "inc/top-products.php"; 
include DIR_PHP . "inc/onlineshop.php";
?>
			<!-- FOOTER -->
			<footer id="footer">
				<div class="container">

					<div class="row margin-top-20 margin-bottom-20 size-13">

						<!-- col #1 -->
						<div class="col-md-4">

							<h4 class="letter-spacing-1">�� ��������</h4>
							<p>��� ��� ����� �� ���� �� ����� � ��������� �� ������� �������. ���������� ����� ����� �� ������ ������ ���������� �������������, �� ����� ��� ������������ ������������� �� ��������.</p>

<div class="clearfix">

								<a href="https://www.facebook.com/animalliyazoo/" class="social-icon social-icon-sm social-icon-border social-facebook pull-left" data-toggle="tooltip" data-placement="top" title="Facebook">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a>

								<a href="#" class="social-icon social-icon-sm social-icon-border social-gplus pull-left" data-toggle="tooltip" data-placement="top" title="Google plus">
									<i class="icon-gplus"></i>
									<i class="icon-gplus"></i>
								</a>

								<a href="/rss.php" class="social-icon social-icon-sm social-icon-border social-rss pull-left" data-toggle="tooltip" data-placement="top" title="Rss">
									<i class="icon-rss"></i>
									<i class="icon-rss"></i>
								</a>

							</div>
						</div>
						<!-- /col #1 -->

						<!-- col #2 -->
						<div class="col-md-8 fa-blue">

							<div class="row">

								<div class="col-md-4 hidden-sm hidden-xs">
                                
									<h4 class="letter-spacing-1">���������</h4>
									<ul class="list-unstyled footer-list half-paddings">
											<?php
											if ( !isset( $footer_links ) )$footer_links = 5;
											$footer_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $footer_links" );

											for ( $i = 0; $i < count( $footer_links ); $i++ ) {
												$subpages = $o_page->get_pSubpages( $footer_links[ $i ][ 'n' ] );
												?>
											<li <?php echo ((count($subpages)> 0) ? "" : "") ?>>
												<a class="block" href="<?php echo $o_page->get_pLink($footer_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <i class='fa fa-paw'></i> <?php echo $footer_links[$i]['Name'] ?></a>

										</li>
										<?php 
										}
										?>
											<?php
											if ( !isset( $footer_links2 ) )$footer_links2 = 4;
											$footer_links2 = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $footer_links2" );

											for ( $i = 0; $i < count( $footer_links2 ); $i++ ) {
												$subpages = $o_page->get_pSubpages( $footer_links2[ $i ][ 'n' ] );
												?>
											<li <?php echo ((count($subpages)> 0) ? "" : "") ?>>
												<a class="block" href="<?php echo $o_page->get_pLink($footer_links2[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <i class='fa fa-paw'></i> <?php echo $footer_links2[$i]['Name'] ?></a>

										</li>
										<?php 
										}
										?>
									</ul>
								</div>
                      

								<div class="col-md-4 hidden-sm hidden-xs">
                                     
									<h4 class="letter-spacing-1">�� ������� �� ����</h4>
									<ul class="list-unstyled footer-list half-paddings">
										<li><a href="#" class="block" data-toggle="modal" data-target="#LoginForm"><i class='fa fa-paw'></i> <?=LOGIN?></a></li>
            							<li><a href="#" class="block" data-toggle="modal" data-target="#RegistrationForm"><i class='fa fa-paw'></i> <?=REGISTER?></a></li>
									</ul>

								</div>

								<div class="col-md-4">
									<h4 class="letter-spacing-1">������ � ���</h4>
									<p><i class="fa fa-map-marker" aria-hidden="true"></i> ��.�����, ����� �. ���������, ����� - ��. "������� �����" 46 /����� ����, ��� �����/</p>
									<p><i class="fa fa-phone" aria-hidden="true"></i> <strong>0700 299 20</strong><br />
                                    <i class="fa fa-phone" aria-hidden="true"></i> <strong>052 / 50 60 88</strong></p>

								</div>

							</div>

						</div>
						<!-- /col #2 -->

					</div>

				</div>

				<div class="copyright">
					<div class="container">
						<ul class="pull-right nomargin list-inline mobile-block">
							<li>��� ������ � ���������: <a href="http://maksoft.net">�������</a></li>
						</ul>

						&copy; 2009-<?=date("Y")?> ��������
					</div>
				</div>

			</footer>
            
            	<!-- /FOOTER -->
            
<div id="RegistrationForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="RegistrationFormLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="RegistrationFormLabel"><i class="fa fa-users"></i> ����������� �� ��������</h4>
			</div>

			<!-- Modal Body -->
			<div class="modal-body">
           
           		<div class="alert alert-warning margin-bottom-30"><!-- WARNING -->
					��� �� ��� �������� ���� �� �������� ������ <a href="http://animaliaonline.com">������ �������</a>
				</div>
            <?php include __DIR__."/../../forms/animalia/registration.php"; ?>

			</div>

		</div>
	</div>
</div>	

<div id="LoginForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="LoginFormLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="LoginFormLabel"><i class="fa fa-sign-in" aria-hidden="true"></i> ���� �� ��������</h4>
			</div>

			<!-- Modal Body -->
			<div class="modal-body">
           		<div class="alert alert-warning margin-bottom-30"><!-- WARNING -->
					��� �� ��� �������� ���� �� �������� ������ <a href="http://animaliaonline.com">������ �������</a>
				</div>
            <?php include __DIR__."/../../forms/animalia/login.php"; ?>
            
			</div>

		</div>
	</div>
</div>	

<div id="ForgetPassForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ForgetPassFormLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="ForgetPassFormLabel">��������� ������</h4>
			</div>

			<!-- Modal Body -->
			<div class="modal-body">

            <?php include __DIR__."/../../forms/animalia/forgetpass.php"; ?>
            
			</div>

		</div>
	</div>
</div>	

<!--<div id="shopLoadModal" class="modal fade in" data-autoload="true" data-autoload-delay="2000" style="display: block; padding-right: 17px;">
				<div class="modal-dialog modal-full">
					<div class="modal-content" style="background-image:url('assets/images/misc/shop/shop_modal.jpg');">

						<div class="modal-header noborder">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>

						<div class="modal-body">

							<div class="block-content">

								<img src="assets/images/logo-footer-dark.png" alt="">
								<p class="size-13 margin-bottom-20 margin-top-30">��������.</p>

								<div class="inline-search clearfix margin-bottom-20">
									<form action="php/newsletter.php" method="post" class="validate nomargin" data-success="Subscribed! Thank you!" data-toastr-position="bottom-right" novalidate>

										<input placeholder="Email Address" id="shop_email" name="shop_email" class="serch-input required" type="search">
										<button type="submit">
											<i class="fa fa-check"></i>
										</button>
									<input name="is_ajax" value="true" type="hidden"></form>
								</div>

								<div class="size-11 text-left">
									<label class="checkbox pull-left">
										<input class="loadModalHide" type="checkbox">
										<i></i> <span class="weight-300">Don't show this popup again</span>
									</label>

								</div>

							</div>

						</div>

					</div>
				</div>
			</div>-->