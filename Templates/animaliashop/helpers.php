<?php
define("SUCCESS", "success");
define("ERROR", "error");
define("WARNING", "warning");
define("INFO", "info");

function is_available($lang, $available=1)
{
    $available = (int) $available;
    $availability = array(
        "bg" => array(
            1 => array("code" => "success", "status" => "� ���������"),
            0 => array("code" => "error", "status" => "��������"),
            3 => array("code" => "warning", "status"=> "�������� ��������")
        ),
        "en" => array(
            1 => array("code" => "success", "status" => "On stock"),
            0 => array("code" => "error", "status" => "Out of stock"),
            3 => array("code" => "warning", "status" => "Expected delivery")
        ),
    );
    if(!isset($availability[$lang]) or !isset($availability[$lang][$available])){
        return array("code" => "success", "status" => "� ���������");
    }

    return $availability[$lang][$available];
}

function in_promotion($product){
    if(!isset($product->in_promotion, $product->promo_from_date, $product->promo_end_date)){
        return false;
    }

    if((int) $product->in_promotion < 1){
        return false;
    }

    $now = new DateTime();
    $now->setTimezone(new DateTimeZone('Europe/Sofia'));
    try{
        $t1 = new DateTime($product->promo_from_date);
        $t2 = new DateTime($product->promo_end_date);
    } catch( Exception $e ) {
        return false;
    }

    if ($t1 > $now) {
        return false; 
    }

    if ($t2 < $now) {
        return false;
    }

    return true;
}

function add_msg($type, $msg)
{
    $_SESSION['system']['messages'][$type][] = $msg;
}

/*function msg($type, $text){
    return sprintf(' <div class="alert alert-%s alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            %s
        </div>', $type, $text);
}*/
	
function msg($type, $text){
    return sprintf(' <div class="btn btn-success toastr-notify top-absolute" data-progressBar="true" data-position="top-right" data-notifyType="success" data-message="%s">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            %s
        </div>', $type, $text);
}
$notifications = array();

function msg_notify($type, $text)
{
    global $notifications;
    $notifications[] = '_toastr("'.$text.'","top-right","'.$type.'",false);';
}

function get_colors($product, $colors , $selected = "checked"){
    $i = 0;
    $html = '<div class="pColors margin-top-30">';
    foreach ($product->color as $color) { 
        if(!$c = $colors[$color]){ //must be tested
            continue;
        }
        if($i > 0){ $selected = "";}
        $html .= '   
            <div class="btn-group pull-left product-opt-color">
                <input type="radio" name="color" data-sku="'.$product->p_Id.'" value="'.$c['name'].'"'.$selected.'>
                <span id="product-selected-color" class="tag shop-color" style="background-color:#'.$c['hex'].'" data-tooltip="'.$c['name'].'"></span> 
            </div>';
        $i++;
    }
    return $html."</div>";
}


function get_sku($price)
{
    $sku = 'an-'.$price['id'];
    if(!empty($price['code'])){
        $sku = $price['code'];
    }
    return $sku;
}

function get_description($title, $description)
{
    if(strlen($description) > 20){
        return $description;
    }
    $it_name = str_ireplace($title, '', $description);
    return sprintf("%s - %s", $title, $it_name);
}

function available($price)
{
    if(($price['pcs'] * $price['qty']) > 0){
        return 1;
    }
    return 0;
}
