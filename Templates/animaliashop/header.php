<!-- Top Bar -->
<div id="topBar" class="dark">
    <div class="container">
        <!-- right -->
        <ul class="top-links list-inline pull-right">
        <?php
		if($user->ID > 0) { ?>
            <li class="text-welcome hidden-xs"><?=HELLO?>, <strong><?php echo("$user->Name"); ?></strong></li>
            <?php if($user->WriteLevel > 0 and $user->AccessLevel > 1){ ?>
            <li>
				<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><i class="fa fa-th-list" aria-hidden="true"></i> ������� �� ��������</a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a tabindex="-1" href="<?=$o_page->get_pLink(19371171);?>"><i class="fa fa-circle-o-notch fa-spin fa-fw admin-new" aria-hidden="true"></i> <?=$o_page->get_pName(19371171);?></a>
                    </li>
                    <li>
                        <a tabindex="-1" href="<?=$o_page->get_pLink(19371172);?>"><i class="fa fa-check admin-check" aria-hidden="true"></i> <?=$o_page->get_pName(19371172);?></a>
                    </li>
                    <li>
                        <a tabindex="-1" href="<?=$o_page->get_pLink(19371173);?>"><i class="fa fa-ban admin-cancel" aria-hidden="true"></i> <?=$o_page->get_pName(19371173);?></a>
                    </li>
				</ul>
			</li>
           <?php } ?>
            <li>
                <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><i class="fa fa-cogs" aria-hidden="true"></i> <?=OPTIONS?></a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a tabindex="-1" href="page.php?n=19370920&SiteID=<?=$SiteID?>"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=ORDERS?></a>
                    </li>
                    <?php if($user->WriteLevel > 0 and $user->AccessLevel > 1){ ?>
                    <li class="divider"></li>
                    <li>
                        <a tabindex="-1" href="page.php?n=11&SiteID=<?=$SiteID?>"> <i class="fa fa-cog" aria-hidden="true"></i> <?=ADM?></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a tabindex="-1" href="page.php?n=19373642&SiteID=<?=$SiteID?>"> <i class="fa fa-cog" aria-hidden="true"></i> ��������</a>
                    </li>
                    <?php } ?>
                    <li class="divider"></li>
                    <li><a tabindex="-1" href="<?=DIR_TEMPLATE?>inc/logout.php"><i class="glyphicon glyphicon-off"></i> <?=LOGOUT?></a></li>
                </ul>
            </li>
        <?php } else { ?>
            <li style="border-left: rgba(255,255,255,0.1) 1px solid;"><a href="#" data-toggle="modal" data-target="#LoginForm"><?=LOGIN?></a></li>
            <li><a href="#" data-toggle="modal" data-target="#RegistrationForm"><?=REGISTER?></a></li>
        <?php } ?>
		</ul>
        <!-- left -->
        <ul class="top-links list-inline hidden-xs hidden-sm">
       
        <?php if($user->WriteLevel > 0 and $user->AccessLevel > 1){ ?>
        <?php } else { ?>
			<li><a href="tel:070029920"><i class="fa fa-phone" aria-hidden="true"></i> 0700 299 20</a></li>
			<li><a href="tel:052506088"><i class="fa fa-phone" aria-hidden="true"></i> 052 / 50 60 88</a></li>
			<li><a href="http://animaliaonline.com/" target="_blank" style="text-shadow: 0px 0px 2px rgb(0, 0, 0);">������ ������� &nbsp;&nbsp;<i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
			<?php } ?>
				<?php
				if ( !isset( $second_links ) )$second_links = 5;
				$second_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $second_links" );

				for ( $i = 0; $i < count( $second_links ); $i++ ) {
					$subpages = $o_page->get_pSubpages( $second_links[ $i ][ 'n' ] );
					?>
				<li>
					<a href="<?php echo $o_page->get_pLink($second_links[$i]['n']) ?>">
					<?php echo $second_links[$i]['Name'] ?>
					</a>
			    </li>
				<?php 
				}
				?>
        </ul>
        
		<button class="btn btn-mobile visible-xs visible-sm" data-toggle="collapse" data-target=".nav-head-collapse">
			<i class="fa fa-bars"></i>
		</button>
      
			<div class="navbar-collapse pull-left nav-head-collapse collapse">
				<nav class="nav-main">
					<ul id="topHead" class="nav nav-pills nav-main has-topBar visible-xs visible-sm">
						<li><a href="tel:070029920"><i class="fa fa-phone" aria-hidden="true"></i> 0700 299 20</a></li>
						<li><a href="tel:052506088"><i class="fa fa-phone" aria-hidden="true"></i> 052 / 50 60 88</a></li>
						<li><a href="http://animaliaonline.com/" target="_blank" style="text-shadow: 0px 0px 2px rgb(0, 0, 0);">������ ������� &nbsp;&nbsp;<i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
							<?php
							if ( !isset( $second_links2 ) )$second_links2 = 5;
							$second_links2 = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $second_links2" );

							for ( $i = 0; $i < count( $second_links2 ); $i++ ) {
								$subpages = $o_page->get_pSubpages( $second_links2[ $i ][ 'n' ] );
								?>
							<li>
								<a href="<?php echo $o_page->get_pLink($second_links2[$i]['n']) ?>">
								<?php echo $second_links2[$i]['Name'] ?>
								</a>
							</li>
							<?php 
							}
							?>
					</ul>
				</nav>
			</div>

    </div>
</div>

<div id="header" class="sticky clearfix">
    <!-- �������� -->
    <div class="search-box over-header">
        <a id="closeSearch" href="#" class="glyphicon glyphicon-remove"></a>

        <form method="get" class="search-form" action="page.php">
            <input type="hidden" name="n" value="45296">
            <input type="hidden" name="SiteID" value="<?php echo $o_site->SiteID?>">
            <input type="text" class="form-control" name="search" id="search" placeholder="<?=SEARCH_PLACEHOLDER?>" />
           
        </form>
    </div> 
    <!-- /�������� -->
    <!-- TOP NAV -->
    <header id="topNav" class="menu-strip">
        <div class="container">
            <!-- Mobile Menu -->
            <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <!-- ������ -->
            <ul class="pull-right nav nav-pills nav-second-main">
                <li class="search">
                    <a href="javascript:;">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
                <li class="social-link">
					<a href="https://www.facebook.com/animalliyazoo/"><i class="fa fa-facebook"></i></a>
				</li>
            <?php 
                if($user->ID > 0) {
                    include __DIR__.'/templates/widgets/cart_header.php';
                }
            ?>
            </ul>
            <!-- Logo -->
            <a class="logo pull-left" href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>">
                <img src="<?=DIR_TEMPLATE?>assets/images/logo_animalia_x2.png" alt="" />
            </a>

			<div class="navbar-collapse pull-right nav-main-collapse collapse">
				<nav class="nav-main">
					<ul id="topMain" class="nav nav-pills nav-main has-topBar">
											<?php
											if ( !isset( $menu_links ) )$menu_links = 3;
											$menu_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $menu_links" );

											for ( $i = 0; $i < count( $menu_links ); $i++ ) {
												$subpages = $o_page->get_pSubpages( $menu_links[ $i ][ 'n' ] );
												?>
											<li <?php echo ((count($subpages)> 0) ? "class=\"dropdown\"" : "") ?>>
												<a href="<?php echo $o_page->get_pLink($menu_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "class=\"dropdown-toggle\"" : "") ?>>
													<i class='fa fa-paw'></i> <?php echo $menu_links[$i]['Name'] ?></a>
                       
                        <?php echo ((count($subpages) > 0) ? "<ul class=\"dropdown-menu\">" : "</li>") ?> 
                        <?php
    						if (count($subpages) > 0) {

        					for ($j = 0; $j < count($subpages); $j++)
								echo "<li><a href=\"" . $o_page->get_pLink($subpages[$j]['n']) . "\">" . $subpages[$j]['Name'] . "</a></li>";
							?>
								</ul>
										</li>
										<?php 
										}
											}
										?>
										<li class="promo-btn"><a href="<?=$o_page->get_pLink(46531)?>"><i class='fa fa-percent blink_me'></i> <?=$o_page->get_pName(46531)?></a></li>
					</ul>
				</nav>
			</div>
        </div>
    </header>
</div>
