<?php
require_once __DIR__."/../../lib/Database.class.php";
require_once __DIR__."/../../modules/vendor/autoload.php";
require_once __DIR__."/../../forms/animalia/cart/Add.php";
require_once __DIR__."/../../forms/animalia/cart/Reset.php";
require_once __DIR__."/../../forms/animalia/cart/Edit.php";
require_once __DIR__."/../../forms/animalia/cart/Remove.php";
require_once __DIR__."/../../forms/animalia/cart/Purchase.php";
require_once __DIR__."/helpers.php";
require_once __DIR__."/DBCart.php";

define("DIR_PHP","Templates/animaliashop/");
define("DIR_TEMPLATE","/Templates/animaliashop/");
define("CART_PAGE_N", $o_site->get_sConfigValue("CART_PAGE_N"));
define("PROMO_PAGE_N", $o_site->get_sConfigValue("PROMO_PAGE_N"));
define("NEWPRODUCTS_PAGE_N", $o_site->get_sConfigValue("NEWPRODUCTS_PAGE_N"));

require_once __DIR__."/../ra/container.php";
$container = new \Pimple\Container();
$container->register(new RaMaksoftLoader());

$uri = $_SERVER['REQUEST_URI'];
$msg = "";

$o_page->setDatabase(new Database());

Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem(__DIR__.'/templates/');

$twig = new Twig_Environment($loader, array(
    'cache' => __DIR__.'/templates/cache/',
    'auto_reload' => true
    )
);
$_cart = new DBCart("BG", $container['gate']);
$payload = isset($_SESSION[\Maksoft\Cart\Cart::getName()]) ? $_SESSION[\Maksoft\Cart\Cart::getName()] : array();
$_cart->load($payload);

$form = new Add($_cart, $_POST);

$reset = new Reset($_cart);

$purchase = new Purchase($_cart, $o_page, $twig, $_POST);


$edit_item = new Edit($_cart, $_POST);

$o_site->print_sConfigurations();


$import = "";
if($o_page->_page['SiteID'] == $o_page->_site['SitesID']){
ob_start();
    eval($o_page->_page['PHPcode']);
    $import = ob_get_clean();
    if(isset($product)){
        $product = json_decode($product);
    }
}


$type = 'standart';


if($user->WriteLevel < 1 or $user->AccessLevel < 1){
$nav_bar = $o_page->get_pNavigation(false); 
}


if($o_page->_site['language_id'] == 1)
include_once DIR_PHP . "lang/bg.php";
else
include_once DIR_PHP . "lang/en.php";

$host = $_SERVER['SERVER_NAME']; 
if($host == 'animalia-bg.com' or $host == 'http://www.animalia-bg.com/' or $host == 'http://animalia-bg.com/' or $host == 'www.animalia-bg.com') {
header('Location: http://www.animalia.bg/page.php?n=' .$n. '&SiteID=' .$SiteID. '', true, 301);
}
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html lang="<?php echo($Site->language_key); ?>"> <!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html;" charset="windows-1251">
<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
<title><?php echo("$Title");?></title>
<?php
    require_once __DIR__."/../meta_tags.php"; 
?>
<link href="<?=DIR_TEMPLATE?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" type="text/css" />

<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="<?=DIR_TEMPLATE?>assets/css/essentials.css" rel="stylesheet" type="text/css" />
<link href="<?=DIR_TEMPLATE?>assets/css/layout.css" rel="stylesheet" type="text/css" />
<link href="<?=DIR_TEMPLATE?>assets/css/header-1.css" rel="stylesheet" type="text/css" />
<link href="<?=DIR_TEMPLATE?>assets/css/layout-shop.css" rel="stylesheet" type="text/css" />
<link href="<?=DIR_TEMPLATE?>assets/darktooltip/dist/darktooltip.css" rel="stylesheet" type="text/css" />
<link href="<?=DIR_TEMPLATE?>assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />

<!-- REVOLUTION SLIDER -->
<link href="<?=DIR_TEMPLATE?>assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
<link href="<?=DIR_TEMPLATE?>assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />

<!--<script async src="<?=DIR_TEMPLATE?>assets/js/handlebars.min.js"></script>-->
</head>
    <body class="smoothscroll enable-animation">
        <div id="wrapper">
        <?php
            $action = $_POST['action'];
            if($_SERVER["REQUEST_METHOD"] === "POST" && $action and user::$uID > 0){
                try{
                    switch($action){
                        case "add_product":
                            $form->is_valid();
                            $_cart = $form->addToOrder($o_page);
                            $_SESSION[\Maksoft\Cart\Cart::getName()] = $_cart->save();
                            msg_notify("success", "������� ��������� �������� � ���������.");
                            break;
                        case "remove_product":
                            $remove_item = new Remove($_cart, $_POST['sku'], $_POST);
                            $remove_item->is_valid();
                            $_cart = $remove_item->remove();
                            break;
                        case $edit_item->action->value:
                            $edit_item->is_valid();
                            $_cart = $edit_item->save();
                            msg_notify("success", "������� ���������� ������� #". $edit_item->sku->value);
                            break;
                        case "reset":
                            $reset->is_valid();
                            $_cart = $reset->reset();
                            msg_notify("success", "��������� � �������.");
                            break;
                        case "make_purchase":
                            $purchase->is_valid();
                            $purchase->save();
                            msg_notify("success", "������ ������� � �������� �������. ��� ���������� �� �� ������ � ��� ��� �����.");
                            $_cart = $reset->reset();
                    }

                    $_SESSION[\Maksoft\Cart\Cart::getName()] = $_cart->save();
                } catch (Exception $e){
                    msg_notify("error", $e->getMessage());
                    require_once __DIR__.'/templates/warning_message.php';
                }
            }
            require_once DIR_PHP . "header.php";
            $pTemplate = $o_page->get_pTemplate();
            if($o_page->_page['SiteID'] != $o_site->_site['SitesID'])
               	require_once DIR_PHP."admin.php";	
            elseif($pTemplate['pt_url'])
                require_once $pTemplate['pt_url'];
            elseif($o_page->_page['n'] == $o_site->_site['StartPage'] && strlen($search_tag)<3 && strlen($search)<3)
                require_once DIR_PHP . "home.php";
            else
                require_once DIR_PHP . "main.php";
                echo $import;
            require_once DIR_PHP . "footer.php";
        ?>
        </div>
        <a href="#" id="toTop"></a>
        
        <!--
        <div id="preloader">
            <div class="inner">
                      <div class="sk-double-bounce">
						<div class="sk-child sk-double-bounce1"></div>
						<div class="sk-child sk-double-bounce2"></div>
					  </div>
            </div>
        </div>
        -->
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript">
            var plugin_path = '<?=DIR_TEMPLATE?>assets/plugins/';
/*            jQuery(document).ready(function(){
                var $ = jQuery;
                $.each($("[type=submit]"), function() {
                    var el= this;
                    $(this).mouseover(function(){
                        $(this).addClass("animated flipInY");
                    });
                    $(this).mouseout(function(){
                         setTimeout(function() { 
                            $(el).removeClass("animated flipInY");
                       }, 2000);
                    });
                  }
                );
            });*/
        </script>
        <script type="text/javascript" src="<?=DIR_TEMPLATE?>assets/js/scripts.js"></script>
        <script type="text/javascript" src="<?=DIR_TEMPLATE?>assets/darktooltip/dist/jquery.darktooltip.js"></script>
        <script type="text/javascript" src="<?=DIR_TEMPLATE?>assets/js/view/settings.shop.js"></script>
        
        <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
        <script src="/Templates/ra/js/jquery-confirm.min.js"></script>
        <script src="web/assets/js/notify.min.js"></script>
        
		<!-- REVOLUTION SLIDER -->
		<script type="text/javascript" src="<?=DIR_TEMPLATE?>assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="<?=DIR_TEMPLATE?>assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="<?=DIR_TEMPLATE?>assets/js/view/settings.revolution_slider.js"></script>

        <script>
            jQuery(document).ready(function(){
                <?php echo implode('', $notifications);?>
            });
        </script>
        <?php include( "Templates/footer_inc.php" ); ?>
	</body>
</html>
<?php
#$_SESSION[\Maksoft\Cart\Cart::getName()] = $_cart->save();
?>

