<?php
$form->set_id( "add-product" );
foreach($form as $field){
    foreach($field->get_errors() as $err){
        msg_notify("error", $err);
    }
}
?>
<div class="shop-item-price">
    <label for="product" class="smallgray">�������� ������� *</label>
    <select  name="product" class="form-control pointer required" onchange="change_product(this)">
        <option data-available="<?=available($price);?>"  selected value="<?=number_format($price['price_value'], 2, '.', '')?>" data-sku="<?=get_sku($price)?>"><?=get_description($o_page->_page['Name'], $price['description'])?></option>
    <?php 
    while($pr = array_shift($prices)){
        $price_format = number_format($pr['price_value'], '2', '.','');
        echo '<option value="'.$price_format.'" data-available="'.available($pr).'" data-sku="'.get_sku($pr).'">'.get_description($o_page->_page['Name'], $pr['description']).'</option>';
    }
    echo '</select>';
?>
    <div class="row">
        <div class="btn-group pull-left product-opt-qty margin-left-15 add-product">
            <?=$form->start();?>
            <?=$form->qty;?>
            <input type="hidden" id='it_name' name="name" value="<?=get_description($o_page->_page['Name'], $price['description']);?>">
            <input type="hidden" name="unit" value="��.">
            <input type="hidden" id='it_price' name="price" value="<?=$price['price_value']?>">
            <input type="hidden" name="link" value="<?=$o_page->get_pLink($o_page->_page['n']);?>">
            <?php
            $form->sku->id = 'it_sku';
            $form->sku->value=get_sku($price);
            echo $form->sku;
            echo $form->action;
            if(boolval(available($price))){
                echo $form->submit;
            }
            echo $form->end(); //END form tag
            ?> 
        </div>
    </div>
</div>
<hr/>

<script>
function change_product(el){
    let opt = jQuery(el).find('option:selected');
    jQuery("#it_sku").attr('value', opt.data('sku'));
    jQuery("#it_price").attr('value', opt.val());
    jQuery("#it_name").attr('value', opt.text());
    jQuery("#display_price").text(opt.val()+' ��./��.');
    console.log(opt.data('available') == 1);
    if (opt.data('available') == 1){
        if(jQuery("#add-product input[name=submit]").length == 0){
            jQuery('<input />').attr('type', 'submit')
                               .attr('name', 'submit')
                               .attr('class', 'btn btn-primary product-add-cart noradius-left')
                               .attr('value', '+ ������')
                               .attr('onclick', 'populate_inputs()')
                               .appendTo("#add-product"); 
        }
        jQuery("#display_price").append('<span id="av-cls"></span>') 
        jQuery("#av-cls").attr('class', 'pull-right text-success thinst');
        jQuery("#av-cls").html('<i class="fa fa-check"></i> � ���������');
    } else {
        jQuery("#add-product input[name=submit]").remove()
        jQuery("#display_price").append('<span id="av-cls"></span>') 
        jQuery("#av-cls").attr('class', 'pull-right text-error thinst');
        jQuery("#av-cls").html('<i class="fa fa-check"></i> ��������');
    }
}
</script>
