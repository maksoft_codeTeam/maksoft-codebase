<?php

/* mail_template.html */
class __TwigTemplate_be47569891149363b9a12e19f6ae41b965ac4a0576b65ceca96773f855403e1e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<style type=\"text/css\">body,html,.body{background:#f3f3f3!important;}</style>
<container>
<spacer size=\"16\"></spacer>
<row>
<columns>
<h2 style=\"color:#004C86;\">���������� �� �� ����������� ������� ";
        // line 6
        if (isset($context["name"])) { $_name_ = $context["name"]; } else { $_name_ = null; }
        echo twig_escape_filter($this->env, $_name_, "html", null, true);
        echo "!</h2>
<p>������ ������� � ����� <b style=\"color:#004C86;\">";
        // line 7
        if (isset($context["order_id"])) { $_order_id_ = $context["order_id"]; } else { $_order_id_ = null; }
        echo twig_escape_filter($this->env, $_order_id_, "html", null, true);
        echo "</b> ���� ������������ ������� � � � ������ �� ���������.

<spacer size=\"16\"></spacer>
<callout class=\"secondary\">
<row>
<columns large=\"6\">
<p>
<strong style=\"color:#004C86;\">��� �� �����</strong><br/>
";
        // line 15
        if (isset($context["client_name"])) { $_client_name_ = $context["client_name"]; } else { $_client_name_ = null; }
        echo $_client_name_;
        echo "
</p>
<p>
<strong style=\"color:#004C86;\">����� �����</strong><br/>
";
        // line 19
        if (isset($context["email"])) { $_email_ = $context["email"]; } else { $_email_ = null; }
        echo twig_escape_filter($this->env, $_email_, "html", null, true);
        echo "
</p>
<p>
<strong style=\"color:#004C86;\">����� �� �������:</strong><br/>
";
        // line 23
        if (isset($context["order_id"])) { $_order_id_ = $context["order_id"]; } else { $_order_id_ = null; }
        echo twig_escape_filter($this->env, $_order_id_, "html", null, true);
        echo "
</p>
</columns>
<columns large=\"6\">
<p>
<strong style=\"color:#004C86;\">����� �� ��������:</strong><br/>
";
        // line 29
        if (isset($context["shipping"])) { $_shipping_ = $context["shipping"]; } else { $_shipping_ = null; }
        echo $_shipping_;
        echo "
</p>
<p>
<strong style=\"color:#004C86;\">����� �� ��������:</strong><br/>
";
        // line 33
        if (isset($context["address"])) { $_address_ = $context["address"]; } else { $_address_ = null; }
        echo $_address_;
        echo "
</p>
<p>
<strong style=\"color:#004C86;\">���������:</strong><br/>
";
        // line 37
        if (isset($context["notes"])) { $_notes_ = $context["notes"]; } else { $_notes_ = null; }
        echo $_notes_;
        echo "
</p>
</columns>
</row>
</callout>

<h4  style=\"color:#004C86;\">������� �� ���������</h4>
";
        // line 44
        if (isset($context["order_info"])) { $_order_info_ = $context["order_info"]; } else { $_order_info_ = null; }
        echo $_order_info_;
        echo "
<hr/>
<h4 style=\"color:#004C86;\">��������!</h4>
<p>��� ������ �� �� ������ � ���, � ������������ ���������� ������� ����������� ������� � ���������� �.</p>
</columns>
</row>
<row class=\"footer text-center\">
<columns large=\"3\">
<img src=\"http://maksoft.net/Templates/topexpress/assets/images/logo_topexpress.png\" alt=\"\">
</columns>
<columns large=\"3\">
<p style=\"color:#4EA57E;\">
��.�����, ��.����� ����� 93<br/>
���. 02/480-11-24<br/>
���. 02/480-11-25<br/>
�����: <a style=\"color:#4EA57E;\" href=\"mailto:topexpress2000@gmail.com\"><b>topexpress2000@gmail.com</b></a><br/>
�������: <a style=\"color:#4EA57E;\" href=\"http://topexpress2000.com/\"><b>http://topexpress2000.com/</b></a><br/>
</p>
</columns>
<columns large=\"3\">
</columns>
</row>
</container>

";
    }

    public function getTemplateName()
    {
        return "mail_template.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 44,  85 => 37,  77 => 33,  69 => 29,  59 => 23,  51 => 19,  43 => 15,  31 => 7,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "mail_template.html", "/hosting/maksoft/maksoft/Templates/animaliashop/templates/mail_template.html");
    }
}
