<?php include DIR_PHP . "main-h.php";?>
<section>
	<div class="container">
		<div class="row">

			<!-- RIGHT -->
			<div class="col-lg-9 col-md-9 col-sm-9 col-lg-push-3 col-md-push-3 col-sm-push-3">

				<?php
				if ( strlen( $search_tag ) > 2 ) {
					$o_page->print_search( $o_site->do_search( iconv( "UTF-8", "CP1251", $search_tag ), 10 ) );
					$keyword = iconv( "UTF-8", "CP1251", $search_tag );
				} elseif ( strlen( $search ) > 2 ) {
					#$o_page->print_search($o_site->do_search($search, 10)); 
					#if($user->AccessLevel > 1){
					$needle = $o_site->do_search( $search, 100 );
					if ( count( $needle ) == 0 ) {
						echo '<div class="results_wrong">���� �������� ��������� �� ���� <strong>' . $search . '</strong></div><br>';
						echo '<div class="quest_include"><div class="form_quest">��������� ���������, �� ���� ����� �������:</div>';
						echo '<div class="contact_form">';
						require_once 'web/forms/main_form.php';
						echo '</div></div>';
					} else {
						echo '<div class="results">��������� �� ��������� �� ���� <strong>' . $search . '</strong>:</div><br>';
						$i = 0;
						foreach ( $needle as $page ) {
							$_pPage = $o_page->get_page( $page[ 'ParentPage' ] );
							echo '
                    <div class="sPage" style="float: left; width: 23%;">

                    <div class="sPage-content border_image" style="margin: 1px;">

                    
                    <a href="' . $o_page->get_pLink( $page[ 'n' ] ) . '" class="img align-center" style="" title="' . $page[ "Name" ] . '">
                    <img src="' . $page[ 'image_src' ] . '" width="100%" align="" class="align-center" alt="' . $page[ 'Name' ] . '" border="0"></a>
                    </div>
                    </div>';
							$i++;
							if ( $i == 4 ) {
								echo '<div style="clear: both" class="sPage-separator"></div>';
								$i = 0;
							}
						}
					}

				} else {
					if ( $o_page->get_pImage() && $o_page->_page[ 'image_allign' ] == 9 ) {
						include "product.php";
					} else {
						$parent_page = $o_page->_page[ 'n' ];
						$order = $o_page->get_pSubPagesOrder( $parent_page );
						$group_id = $o_page->get_pInfo( "show_group_id", $parent_page );
						$subpages = $o_page->get_pSubpages( $parent_page, $order, 1, true );
						echo $o_page->_page[ 'textStr' ];
						eval( $o_page->get_pInfo( "PHPcode" ) );
						$cms_args = array( "CMS_MAIN_WIDTH" => "100%" );
						if ( $o_page->_page[ 'n' ] == 19342970 ) {
							require_once 'Templates/topexpress/templates/checkout.php';
						}
						#$o_page->print_pSubContent();
						$i = 0;
						$o_page->debug( $o_page->_page );
						define( 'MAX_TITLE_LENGTH', 50 );
						echo '<div class="sDelimeter"></div>';
						$max_columns = $o_page->_page[ 'show_link_cols' ];
						echo '<div class="row">';

						foreach ( $subpages as $page ) {
							$link = $o_page->get_pLink( $page[ 'n' ] );
							$img = "/img_preview.php?image_file=".$page[ 'image_src' ]."&img_width=232";
							$img_alt = $page[ 'imageName' ];
							if ( empty( $page['image_src'] ) ) {
								$img = '/web/admin/images/no_image.jpg';
							}
							$full_title = $page[ 'Name' ];
							$title = cut_text( $full_title, MAX_TITLE_LENGTH, '' );

							echo <<<HEREDOC
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="sPage-content">
                    <a href="$link" title="$full_title" class="title">
                        <div class="bullet1" style="float:left;"></div>
                        <div class="text title-trunc">$title</div>
                    </a><br style="clear: both;" class="br-style">
					<div class="imgcolumn">
                    <a href="$link" class="img align-center">
                        <img src="$img" class="height-fix" alt="$img_alt">
                    </a>
					</div>


                </div>
            </div>
HEREDOC;

						}
						echo '</div>';
						echo '<div class="sDelimeter"></div>';
						if ( $user->AccessLevel >= $row->SecLevel && $row->PageURL ) {
							include_once( "$row->PageURL" );
						}

					}
				}
				?>
			</div>

			<!-- LEFT -->
			<div class="col-lg-3 col-md-3 col-sm-3 col-lg-pull-9 col-md-pull-9 col-sm-pull-9">

				<?php include "inc/left-sidebar.php";?>

			</div>

		</div>

	</div>
</section>
