			<!-- REVOLUTION SLIDER -->
			<section id="slider" class="slider fullwidthbanner-container roundedcorners">
				<!--
					Navigation Styles:
					
						data-navigationStyle="" theme default navigation
						
						data-navigationStyle="preview1"
						data-navigationStyle="preview2"
						data-navigationStyle="preview3"
						data-navigationStyle="preview4"
						
					Bottom Shadows
						data-shadow="1"
						data-shadow="2"
						data-shadow="3"
						
					Slider Height (do not use on fullscreen mode)
						data-height="300"
						data-height="350"
						data-height="400"
						data-height="450"
						data-height="500"
						data-height="550"
						data-height="600"
						data-height="650"
						data-height="700"
						data-height="750"
						data-height="800"
				-->
				<div class="fullwidthbanner" data-height="350" data-navigationStyle="">
					<ul class="hide">
						
						<!-- SLIDE  -->
						<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Slide" style="background-color: #F6F6F6;">

							<img src="<?=DIR_TEMPLATE?>assets/images/slides/33.jpg" alt="Анималия" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat">

						</li>
											
						<!-- SLIDE  -->
						<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Slide" style="background-color: #F6F6F6;">

							<img src="<?=DIR_TEMPLATE?>assets/images/slides/465.jpg" alt="Анималия" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat">

						</li>
						
<!--						<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Slide" style="background-color: #F6F6F6;">

							<img src="<?=DIR_TEMPLATE?>assets/images/slides/slide_1.jpg" alt="Анималия" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat">

						</li>
						
						<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Slide" style="background-color: #F6F6F6;">

							<img src="<?=DIR_TEMPLATE?>assets/images/slides/slide_4.jpg" alt="Анималия" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat">

						</li>
						
						<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Slide" style="background-color: #F6F6F6;">

							<img src="<?=DIR_TEMPLATE?>assets/images/slides/slide_3.jpg" alt="Анималия" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat">

						</li>
						
						<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Slide" style="background-color: #F6F6F6;">

							<img src="<?=DIR_TEMPLATE?>assets/images/slides/slide_2.jpg" alt="Анималия" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat">

						</li>
						
						<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Slide" style="background-color: #F6F6F6;">

							<img src="<?=DIR_TEMPLATE?>assets/images/slides/slide_5.jpg" alt="Анималия" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat">

						</li>-->

					</ul>
					<div class="tp-bannertimer"></div>
				</div>
			</section>
			<!-- /REVOLUTION SLIDER -->