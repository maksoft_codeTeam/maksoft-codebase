<section class="featured pastelgreen">
	<div class="container">

		<h2 class="owl-featured noborder text-center"><strong><?=TOP?> <?=PRODUCTS?></strong></h2>
		<div class="owl-carousel featured nomargin owl-padding-10" data-plugin-options='{"singleItem": false, "items": "4", "stopOnHover":false, "autoPlay":3000, "autoHeight": false, "navigation": true, "pagination": false}'>

			<?php 
	                    $group_topproducts = "3602";
                        $topproducts = $o_page->get_pGroupContent($group_topproducts);
                        
                            for($i=0; $i<count($topproducts); $i++)
                                {
                                    ?>

			<div class="shop-item nomargin">

				<div class="thumbnail">
					<a class="shop-item-image" href="<?=$o_page->get_pLink($topproducts[$i]['n'])?>">
															<img class="img-responsive" src="/img_preview.php?image_file=<?=$topproducts[$i]['image_src']?>&img_width=200px" alt="<?=$topproducts[$i]['Name']?>" />
														</a>
				

				</div>

				<div class="shop-item-summary text-center">
					<h4><a href="<?=$o_page->get_pLink($topproducts[$i]['n'])?>"><?=$topproducts[$i]['Name']?></a></h4>
				</div>

				<div class="shop-item-buttons text-center">
                <?php if(user::$uID > 0 ){ ?>
					<span class="product-price">
						<?php
						if($prices = $o_page->get_pPrice($topproducts[$i]['n'])){
							$price = array_shift($prices);
							echo '<span class="price">'.number_format($price["price_value"], '2', '.', '') . ' ' . $price["currency_string"].'</span>';
						}
						?>
					</span>
                <?php } ?>
				</div>

			</div>

			<?							
					}
					?>

		</div>

	</div>
</section>
