<?php 
include DIR_PHP . "inc/slider.php"; 
/*include DIR_PHP . "inc/cats.php"; */
?>

<section>
    <div class="container">
        
        <div class="row">
        
            <!-- RIGHT -->
            <div class="col-lg-9 col-md-9 col-sm-9 col-lg-push-3 col-md-push-3 col-sm-push-3">
            <h4 class="h4-homepage">��� ����� ��������</h4>
            <div class="pContent-home">
            <?php 
            $o_page->print_pContent();
            ?>
			</div>
            <div class="heading-title heading-dotted">
                <h2 class="size-20">���� ��������</h2>
            </div>
            <div class="new-products-columns">
            <?php 
                $newest_products = $o_page->get_newest_products(365, $SiteID, 12);
                $o_page->debug($newest_products);
                $max_columns = $o_page->_page['show_link_cols'];
                define('MAX_TITLE_LENGTH', 50);
                $bootstrap_column = ceil(12 / $max_columns);
                $i=1;
                echo '<div class="row">';
                foreach($newest_products as $product){
                    $link = $o_page->get_pLink($product['n']);
                    $img = (!empty($product['image_src']) ? $product['image_src'] : '/web/admin/images/no_image.jpg') ;
                    $img_alt = (!empty($product['imageName']) ? $product['imageName'] : $product['Name']);
                    $full_title = $product['Name'];
                    $title = cut_text($full_title, MAX_TITLE_LENGTH,'');
                    echo <<<HEREDOC
<div class="col-xs-12 col-md-$bootstrap_column">
    <div class="sPage-content">
        <a href="$link" title="$title" class="title">
            <div class="bullet1" style="float:left;"></div>
            <div class="text title-trunc">$full_title</div>
        </a>
        <br style="clear: both;" class="br-style">
		<div class="imgcolumn-160">
        <a href="$link" class="img align-center">
            <img src="/img_preview.php?image_file=$img&img_width=232px" class="height-fix-160" alt="$img_alt" />
        </a>
		</div>
    </div>
</div>
HEREDOC;
                    if($max_columns % $i){
                        $i = 1;
                        echo '</div><div class="row">';
                    } else {
                        $i++;
                    }
                }
                echo '</div>';
            ?>
            </div>
            
            </div>

            <!-- LEFT -->
            <div class="col-lg-3 col-md-3 col-sm-3 col-lg-pull-9 col-md-pull-9 col-sm-pull-9">
                <?php include "inc/left-sidebar.php";?> 
            </div>
        
        </div>
        
    </div>
</section>
