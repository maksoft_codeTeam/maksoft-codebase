<?php
require_once __DIR__.'/../helpers.php';
require_once __DIR__ .'/../../../loader.php';
$db = $services['db'];
$o_page = $services['o_page'];

$sql = "SELECT * 
FROM  `pages` 
LEFT JOIN prices p ON p.price_n = pages.n
LEFT JOIN images i ON i.imageID = pages.imageNo
WHERE  `PHPcode` LIKE  '%,\"in_promotion\":\"1\"%'
AND  `SiteID` =:SiteID";


$stmt = $db->prepare($sql);
$stmt->bindValue(":SiteID", $o_page->_site['SitesID']);
$stmt->execute();
echo '<div class="row">';
echo '<ul class="shop-item-list row list-inline nomargin">';
while($page = $stmt->fetch(\PDO::FETCH_ASSOC)){
    if(!$page['price_value']){
        continue;
    }

    eval($page['PHPcode']);
    $p = json_decode($product);
    if(!in_promotion($p->initial)){
        continue;
    }

    $discounted_price = $page['price_value'] / (1+ $p->initial->promo_discount);
    
    if($page['price_value'] >= $discounted_price){
        continue;
    }

    $link = $o_page->get_pLink($page['n']);
    $img = $o_page->get_pImage($page['n']);
    $img_alt = $page['imageName'];
    if(empty($img)) {
        $img = '/web/admin/images/no_image.jpg';
    }
    $full_title = $page['Name'];
    $title = cut_text($full_title, MAX_TITLE_LENGTH,'');
?>
    <!-- ITEM -->
    <li class="col-lg-3 col-sm-4">
        <div class="shop-item">
            <div class="thumbnail">
                <!-- product image(s) -->
                <a class="shop-item-image" href="<?=$link?>">
                <img class="img-responsive" src="<?=$page['image_src']?>" alt="<?=$page['imageName'];?>">
                </a>
                <!-- /product image(s) -->

                <!-- product more info -->
                <div class="shop-item-info">
                    <span class="label label-success">��������</span>
                    <span class="label label-danger">-<?=$p->initial->promo_discount?>%</span>
                </div>
                <!-- /product more info -->
            </div>
            <div class="shop-item-counter">
                <div class="countdown is-countdown" data-from="<?=$p->initial->promo_from_date;?>" data-labels="years,months,weeks,days,hour,min,sec">                
                </div>
            </div>
            
            <div class="shop-item-summary text-center">

                <h3><?=$title?></h3>
                
                <?php 
                if($user->AccessLevel > 0){ ?>
                <!-- price -->
                <div class="shop-item-price">
                    <span class="line-through"><?=$page['price_value'];?> ��.</span>
                    <?php $o_page->debug($page['price_value'], $p->initial->promo_discount); ?>
                    <?=$discounted_price;?> ��.
                </div>
                <?php } ?>
                <!-- /price -->
            </div>
        </div>

    </li>
    <!-- /ITEM -->


<?php
}
echo '</ul></div>';

?>

