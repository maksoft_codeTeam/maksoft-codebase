<?php
use Maksoft\Cart\Item;

class DBCart extends \Maksoft\Cart\Cart
{
    protected $gate;
    protected $cart;

    protected $cart_id;

    public function __construct($currency, $gate)
    {
        $this->gate = $gate;
        if(user::$uID > 0){
            $this->cart = $this->gate->cart()->create_cart(session_id(), user::$uID, site::$sID);
            $this->cart_id = $this->cart->id;
        }

        return parent::__construct($currency);
    }

    public function save()
    {
        $tmp = array();

        foreach($this->items as $item) {
            $tmp[$item->getSku()] = $item->toArray();
        }

        $tmp['cart_id'] = $this->cart_id;

        return $tmp;
    }

    public function remove(Item $item)
    {
        if($item and array_key_exists($item->hash(), $this->items)){
            $this->gate->cart()->delete_item($this->cart_id, $item->getSku());
            unset($this->items[$item->hash()]);
            return True;
        }
        throw new \Exception("Cant Remove Item! Item doesnt exist!", 400);
    }    

    public function add(Item $item)
    {
        $other_sku = $item->getSku();
        if(empty($other_sku)){
            return;
        }
        foreach($this->items as $i){
            if($i->add($item)){
                $this->db_update_item($i, $this->cart_id);
                return;
            }
        }
        $this->items[$item->hash()] = $item;
        $this->db_insert_item($item, $this->cart_id);
    }

    public function checkout()
    {
        $this->gate->cart()->checkout($this->cart_id);
        $this->cart = $this->gate->cart()->create_cart(session_id(), user::$uID, site::$sID);
        $this->cart_id = $this->cart->id;
    }

    public function edit(Item $item)
    {
        if(array_key_exists($item->hash(), $this->items)){
            $this->items[$item->hash()] = $item;
            $this->db_update_item($item, $this->cart_id);
            return True;
        }
        throw new \Exception("Cant Edit Item that doesnt exist!", 400);
    }

    protected function db_insert_item($item, $cart_id)
    {
        $sku = $item->getSku();
        $price = $item->getPrice();
        $it_arr = $item->toArray();
        $it_arr['name'] = iconv('cp1251', 'utf8', $it_arr['name']);
        $json = json_encode($it_arr);
        $qty = $item->getQty();
        $SiteID = site::$sID; 
        $item_id = $this->gate->cart()->insert_item($cart_id, $sku, $qty, $price, $json, $SiteID);
        return $item->getSku();
    }

    protected function db_update_item($item, $cart_id)
    {
        $sku = $item->getSku();
        $price = $item->getPrice();
        $json = json_encode($item->toArray());
        $SiteID = site::$sID; 
        $qty = $item->getQty();
        $item_id = $this->gate->cart()->update_item($cart_id, $sku, $qty, $price, $json, $SiteID);
        return $item->getSku();
    }

    public function __destruct()
    {
        return parent::__destruct();
    }
} 
