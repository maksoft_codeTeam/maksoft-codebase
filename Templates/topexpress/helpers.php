<?php
$colors = array ( 1 => array ( 'id' => '1', 'name' => '����', 'hex' => 'fcfdfe', ), 2 => array ( 'id' => '2', 'name' => '����� ������', 'hex' => '5c433f', ), 3 => array ( 'id' => '3', 'name' => '��������', 'hex' => '944332', ), 4 => array ( 'id' => '4', 'name' => '����� ������', 'hex' => '38613f', ), 5 => array ( 'id' => '5', 'name' => '�������', 'hex' => 'dc0014', ), 6 => array ( 'id' => '6', 'name' => '������ ������', 'hex' => '98d414', ), 7 => array ( 'id' => '7', 'name' => '���������', 'hex' => 'f162d0', ), 8 => array ( 'id' => '8', 'name' => '������', 'hex' => '9d6dd9', ), 9 => array ( 'id' => '9', 'name' => '�����', 'hex' => '212121', ), 10 => array ( 'id' => '10', 'name' => '����', 'hex' => '999a99', ), 11 => array ( 'id' => '11', 'name' => '������', 'hex' => 'ffb1c9', ), 12 => array ( 'id' => '12', 'name' => '�����', 'hex' => 'ffea46', ), 13 => array ( 'id' => '13', 'name' => '�����', 'hex' => '66b9e1', ), 14 => array ( 'id' => '14', 'name' => '����', 'hex' => 'eee3cd', ), 15 => array ( 'id' => '15', 'name' => '����� �����', 'hex' => '0147c9', ), 16 => array ( 'id' => '16', 'name' => '��������', 'hex' => 'fd721f', ), 17 => array ( 'id' => '17', 'name' => '������', 'hex' => 'efbab3', ), 18 => array ( 'id' => '18', 'name' => '����� �����', 'hex' => 'cae8fb', ), 19 => array ( 'id' => '19', 'name' => '����� �����', 'hex' => 'f6f4b6', ), 20 => array ( 'id' => '20', 'name' => '����� ������', 'hex' => 'b1e2b2', ), 21 => array ( 'id' => '21', 'name' => '����� ������', 'hex' => '7b52a1', ), 22 => array ( 'id' => '22', 'name' => '����� ������', 'hex' => 'f169ac', ), 23 => array ( 'id' => '23', 'name' => '����� �������', 'hex' => 'b41720', ), 24 => array ( 'id' => '24', 'name' => '����� ������', 'hex' => 'dbd0e3', ), 25 => array ( 'id' => '25', 'name' => '�����', 'hex' => 'f4675d', ), 26 => array ( 'id' => '26', 'name' => '������� �����', 'hex' => 'e59800', ), 27 => array ( 'id' => '27', 'name' => '��������-������', 'hex' => 'ba6111', ), 28 => array ( 'id' => '28', 'name' => '2,', 'hex' => 'ffd997', ), 29 => array ( 'id' => '29', 'name' => '����� ��������', 'hex' => 'cd4623', ), 30 => array ( 'id' => '30', 'name' => '��������', 'hex' => 'b68f4c', ), );


function is_available($lang, $avalable=1)
{
    $lang = (string) $lang;
    $available = (int) $available;
    $availability = array(
        "bg" => array(
            1 => array("code" => "success", "status" => "� ���������"),
            2 => array("code" => "error", "status" => "��������"),
            3 => array("code" => "warning", "status"=> "�������� ��������")
        ),
        "en" => array(
            1 => array("code" => "success", "status" => "On stock"),
            2 => array("code" => "error", "status" => "Out of stock"),
            3 => array("code" => "warning", "status" => "Expected delivery")
        ),
    );
    if(array_key_exists($lang, $availability)){
        return array("code" => "success", "status" => "� ���������");
    }

    if(!isset($availability[$lang][$available])) {
        return array("code" => "success", "status" => "� ���������");
    }

    return $availability[$lang][$available];
}

function in_promotion($product){
    if(!isset($product->in_promotion, $product->promo_from_date, $product->promo_end_date)){
        return false;
    }

    if((int) $product->in_promotion < 1){
        return false;
    }

    $now = new DateTime();
    $now->setTimezone(new DateTimeZone('Europe/Sofia'));
    try{
        $t1 = new DateTime($product->promo_from_date);
        $t2 = new DateTime($product->promo_end_date);
    } catch( Exception $e ) {
        return false;
    }

    if ($t1 > $now) {
        return false; 
    }

    if ($t2 < $now) {
        return false;
    }

    return true;
}

function get_price($product, $price)
{
    if(!$product){
        return ( $price <= 0 ? 0 : $price);
    }

    if($price <= 0){
        return 0;
    }

    if(!in_promotion($product)){
        return $price;
    }

    #$discounted_price = $price / (1.00+ ($product->promo_discount/100));
    #if(user::$uID == 1424) {
    $discounted_price = $price - ($price * $product->promo_discount / 100) ;
    #k}
    return number_format($discounted_price, 2, '.', ' ');
}


function msg($type, $text){
    return sprintf(' <div class="alert alert-%s alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            %s
        </div>', $type, $text);
}


function get_colors($product, $colors , $selected = "checked"){
    $i = 0;
    $choices = array();
    $sku = $product->p_Id;
    $color_name = '����';
    foreach ($product->color as $color) { 
        if(!$c = $colors[$color]){ //must be tested
            continue;
        }
        if($i > 0){
            $selected = "";
        }
        $choices[] = get_color_tmpl($product->p_Id, $c['name'], $selected, $c['hex']);   
        $i++;
    }

    if(count($choices) == 0){
        return "<input style='display:none' type=\"radio\" name=\"color\" data-sku=\"$sku\" value=\"$color_name\" $selected>";
        $choices[] = get_color_tmpl($product->p_Id, $colors[1]['name'], $selected, $colors[1]['hex']);   
    }

    return '<div class="pColors margin-top-30">'.implode('', $choices).'</div>';
}


function get_color_tmpl($p_Id, $color_name, $selected, $hex){

    return '<div class="btn-group pull-left product-opt-color">
                <input type="radio" name="color" data-sku="'.$p_Id.'" value="'.$color_name.'"'.$selected.'>
                <span id="product-selected-color" class="tag shop-color" style="background-color:#'.$hex.'" data-tooltip="'.$color_name.'"></span> 
            </div>';
}


$get_prices = function() use($o_page) {
    
};
$custom_js = array();

$add_js = function($code) use (&$custom_js) {
    $custom_js[] = $code;
};
