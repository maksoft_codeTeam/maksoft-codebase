			<section>
				<div class="container">

					<h2 class="owl-featured noborder"><strong>ТОП</strong> ПРОДУКТИ</h2>
					<div id="top-products" class="owl-carousel featured nomargin owl-padding-10" data-plugin-options='{"singleItem": false, "items": "4", "stopOnHover":false, "autoHeight": false, "navigation": true, "pagination": false}'>
					</div>

				</div>
			</section>
			<!-- /FEATURED -->

						<!-- item -->
                        <script id="entry-template" type="text/x-handlebars-template">
						<div class="shop-item nomargin" id="top-{{n}}">
							<div class="thumbnail">
								<!-- product image(s) -->
								<a class="shop-item-image" href="{{link}}">
									<img class="img-responsive" src="{{image_src}}" alt="{{Name}}" height="220px" />
								</a>
							</div>
							<div class="shop-item-summary text-center">
								<h2>{{Name}}</h2>
							</div>
                            <!-- buttons -->
                            <div class="shop-item-buttons text-center">
                                <a class="btn btn-default" href="{{link}}"><i class="fa fa-info" aria-hidden="true"></i> Повече информация</a>
                            </div>
                            <!-- /buttons -->
						</div>
                        </script>
						<!-- /item -->
			<!-- /FEATURED -->
        <script>
                var $=jQuery;
                var source   = $("#entry-template").html();
                var template = Handlebars.compile(source);
                var url = "http://maksoft.net/api/?command=top_products&n=1&SiteID=1015";
                $.get(url, function(data){
                    var t = template;
                    var timer = 200; //2 sec
                    $.each(data, function(k, product){
                        var html = t(product);
                            $("#top-products").append(html);
                        setTimeout(function(){
                            $("#top-products").append(html);
                            $("#top-" + product.n).addClass("animated bounceInRight");

                        },timer+500);
                        setTimeout(function(){
                            $("#top-" + product.n).removeClass("animated bounceInRight");
                            //$("#top-" + product.n).addClass("animated pulse");
                        },timer+300);
                        timer += 800;
                    });
                });
        </script>
