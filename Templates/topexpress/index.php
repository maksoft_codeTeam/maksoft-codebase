<?php

//set error handler
#set_error_handler("xhandler");
#error_reporting(-1);
require_once __DIR__."/../../modules/vendor/autoload.php";
require_once __DIR__."/../../forms/topexpress/cart/Add.php";
require_once __DIR__."/../../forms/topexpress/cart/Reset.php";
require_once __DIR__."/../../forms/topexpress/cart/Remove.php";
require_once __DIR__."/../../forms/topexpress/cart/Purchase.php";
require_once __DIR__.'/../../forms/topexpress/ClientRegister.php';
require_once __DIR__."/../../lib/messages.php";
require_once __DIR__."/helpers.php";
require_once __DIR__."/../animaliashop/DBCart.php";
$uri = $_SERVER['REQUEST_URI'];
$msg = "";
define("DIR_PHP","Templates/topexpress/");
define("DIR_TEMPLATE","/Templates/topexpress/");
define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/Templates/topexpress/images/");
Twig_Autoloader::register();
require_once __DIR__."/../ra/container.php";

$loader = new Twig_Loader_Filesystem(__DIR__.'/templates/');
$twig = new Twig_Environment($loader, array(
    'cache' => __DIR__.'/templates/cache/',
    'auto_reload' => true
));

define("C_PREFIX", $o_site->_site['primary_url'].$o_page->_page['n']);

$detect = new Mobile_Detect;

if(!isset($_SESSION['isMobile']) or $_SESSION['isMobile'] !== $detect->isMobile()){
    $_SESSION['isMobile'] = $detect->isMobile();
}

if(!$_SESSION['isTablet'] or $_SESSION['isTablet'] !== $detect->isTablet()){
    $_SESSION['isTablet'] = $detect->isTablet();
}

if(!$_SESSION['isMobile']){
    $max_columns = $o_page->_page['show_link_cols'];
} else {
    if($_SESSION['isTablet']){
        $max_columns = 2;
    } else {
        $max_columns = 1;
    }
}

$container = new \Pimple\Container();
$container->register(new RaMaksoftLoader());
$_cart = new DBCart("BG", $container['gate']);
$_cart->load($_SESSION[\Maksoft\Cart\Cart::getName()]);
$o_site->print_sConfigurations();
$db = $di_container['db'];
$o_page->setDatabase($db);
$purchase = new Purchase($_cart, $o_page, $twig, $_POST);
$import = "";
if($o_page->_page['SiteID'] == $o_page->_site['SitesID']){
    ob_start();
    eval($o_page->_page['PHPcode']);
    $import = ob_get_clean();
}
if(isset($product)){
    $product = json_decode($product);
}
require_once __DIR__.'/../../forms/maksoft/inquire/index.php';
$registration_form = new ClientRegister($_POST, $o_page, $mail);
$registration_form->add_attr("class", "nomargin sky-form");
$err_msg = "";

$form = new Add($_cart, $product, $_POST);
$reset = new Reset($_cart);

$type = 'standart';


if($user->WriteLevel < 1 or $user->AccessLevel < 1){
    $nav_bar = $o_page->get_pNavigation(false); 
}


if($o_page->_site['language_id'] == 1)
    include_once DIR_PHP . "lang/bg.php";
else
    include_once DIR_PHP . "lang/en.php";
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html lang="bg-BG"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html;" charset="windows-1251">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
    <title><?php echo("$Title");?></title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
    <link href="<?=DIR_TEMPLATE?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" type="text/css" />
    
    <link href="<?=DIR_TEMPLATE?>assets/css/essentials.css" rel="stylesheet" type="text/css" />
    <link href="<?=DIR_TEMPLATE?>assets/css/layout.css" rel="stylesheet" type="text/css" />
    <link href="<?=DIR_TEMPLATE?>assets/css/header-1.css" rel="stylesheet" type="text/css" />
    <link href="<?=DIR_TEMPLATE?>assets/css/layout-shop.css" rel="stylesheet" type="text/css" />
    <link href="<?=DIR_TEMPLATE?>assets/darktooltip/dist/darktooltip.css" rel="stylesheet" type="text/css" />
    <link href="<?=DIR_TEMPLATE?>assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.min.js" integrity="sha256-1O3BtOwnPyyRzOszK6P+gqaRoXHV6JXj8HkjZmPYhCI=" crossorigin="anonymous"></script>
    <?php
        require_once __DIR__."/../meta_tags.php"; 
    ?>
<!--    <script async type="text/javascript" src="/web/assets/fresco-2.2.1/js/fresco.js"></script>
    <link async rel="stylesheet" type="text/css" href="/web/assets/fresco-2.2.1/css/fresco.css"/>-->
</head>
    <body class="smoothscroll enable-animation">
        <div id="wrapper">
        <?php
            Message\display();
            $action = $_POST['action'];
            if($_SERVER["REQUEST_METHOD"] === "POST" && $action){
                try{
                    switch($action){
                        case "add_product":
                            $form->is_valid();
                            if(mb_detect_encoding($form->name->value, 'UTF-8', true)) { 
                                $form->name->value = iconv('utf8', 'cp1251', $form->name->value);
                            }
                            $_cart = $form->addToOrder($o_page);
                            $_SESSION[\Maksoft\Cart\Cart::getName()] = $_cart->save();
                            $msg = msg("success", "������� ��������� �������� � ���������.");
                            break;
                        case "remove_product":
                            $remove_item = new Remove($_cart, $_POST['sku'], $_POST);
                            $remove_item->is_valid();
                            $_cart = $remove_item->remove();
                            break;
                        case "reset":
                            $reset->is_valid();
                            $_cart = $reset->reset();
                            $msg = msg("success", "��������� � �������.");
                            break;
                        case "make_purchase":
                            $purchase->is_valid();
                            $purchase->save();
                            $msg = msg("success", "������ ������� � �������� �������. ��� ���������� �� �� ������ � ��� ��� �����.");
                            $_cart = $reset->reset();
                        case $registration_form->action->value:
                            $registration_form->is_valid();
                            $registration_form_valid = $registration_form->save();
                            $msg = ' <div class="alert alert-theme-color margin-bottom-30"><!-- THEME COLOR -->
                                    <h4><strong>�������� �� � ������.</strong></h4>
                                    <p>�� �� ������� � ��� �� ������������.</p>
                                </div> ';
                            break;
                    }

                    $_SESSION[\Maksoft\Cart\Cart::getName()] = $_cart->save();
                } catch (Exception $e){
                    $message = msg("error", $e->getMessage());
                    require_once __DIR__.'/templates/warning_message.php';
                }
            }
            require_once DIR_PHP . "header.php";
            echo $msg;
            $pTemplate = $o_page->get_pTemplate();
            if($o_page->_page['SiteID'] != $o_site->_site['SitesID'])
                require_once DIR_PHP."admin.php";	
            elseif($pTemplate['pt_url'])
                require_once $pTemplate['pt_url'];
            elseif($o_page->_page['n'] == $o_site->_site['StartPage'] && strlen($search_tag)<3 && strlen($search)<3)
                require_once DIR_PHP . "home.php";
            else
                require_once DIR_PHP . "main.php";
                echo $import;
            require_once DIR_PHP . "footer.php";
        ?>
        </div>
        <a href="#" id="toTop"></a>
        <!--
        <div id="preloader">
            <div class="inner">
                <span class="loader"></span>
            </div>
        </div>
        -->
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript">
            function populate_inputs()
            {
                var sku = $('input[name=color]:checked', '#add-product').data('sku');
                $('input[name=sku]', '#add-product').val(sku);
                $.notify("�������� � �������", "success");
            }
            var plugin_path = '<?=DIR_TEMPLATE?>assets/plugins/';
            jQuery(document).ready(function(){
                var $ = jQuery;
                $.each($("[type=submit]"), function() {
                    var el= this;
                    $(this).mouseover(function(){
                        $(this).addClass("animated flipInY");
                    });
                    $(this).mouseout(function(){
                         setTimeout(function() { 
                            $(el).removeClass("animated flipInY");
                       }, 2000);
                    });
                  }
                );
            });
        </script>
        <script type="text/javascript" src="<?=DIR_TEMPLATE?>assets/js/scripts.js"></script>

        <script type="text/javascript" src="<?=DIR_TEMPLATE?>assets/darktooltip/dist/jquery.darktooltip.js"></script>
        <script type="text/javascript" src="<?=DIR_TEMPLATE?>assets/js/view/demo.shop.js"></script>
        <script
                  src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
                  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
                  crossorigin="anonymous"></script>
        <script src="/Templates/ra/js/jquery-confirm.min.js"></script>
        <script src="/web/assets/js/notify.min.js"></script>
        <script>
        jQuery(document).ready(function(){
        <?php echo implode(PHP_EOL, $custom_js);?>
        });
        </script>
	</body>
</html>
<?php
$_SESSION[\Maksoft\Cart\Cart::getName()] = $_cart->save();
?>

