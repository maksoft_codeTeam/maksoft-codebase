<div class="container">
	  <div class="row">
      <div class="col-lg-12">
<div class="breadcrumbs-home">
  <?="".$nav_bar.""?>
</div>
</div>
</div>
</div>
			<!-- FOOTER -->
			<footer id="footer">
				<div class="container">

					<div class="row margin-top-20 margin-bottom-20 size-13">

						<!-- col #1 -->
						<div class="col-md-4 col-sm-4">

							<!-- Footer Logo -->
							<img class="footer-logo" src="<?=DIR_TEMPLATE?>assets/images/logo_topexpress_light.png" width="140" alt="" />

<div class="clearfix">

								<a href="#" class="social-icon social-icon-sm social-icon-border social-facebook pull-left" data-toggle="tooltip" data-placement="top" title="Facebook">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a>

								<a href="#" class="social-icon social-icon-sm social-icon-border social-twitter pull-left" data-toggle="tooltip" data-placement="top" title="Twitter">
									<i class="icon-twitter"></i>
									<i class="icon-twitter"></i>
								</a>

								<a href="#" class="social-icon social-icon-sm social-icon-border social-gplus pull-left" data-toggle="tooltip" data-placement="top" title="Google plus">
									<i class="icon-gplus"></i>
									<i class="icon-gplus"></i>
								</a>

								<a href="#" class="social-icon social-icon-sm social-icon-border social-linkedin pull-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
									<i class="icon-linkedin"></i>
									<i class="icon-linkedin"></i>
								</a>

								<a href="#" class="social-icon social-icon-sm social-icon-border social-rss pull-left" data-toggle="tooltip" data-placement="top" title="Rss">
									<i class="icon-rss"></i>
									<i class="icon-rss"></i>
								</a>

							</div>
						</div>
						<!-- /col #1 -->

						<!-- col #2 -->
						<div class="col-md-8 col-sm-8">

							<div class="row">

								<div class="col-md-5 hidden-sm hidden-xs">
                                
									<h4 class="letter-spacing-1">�����������</h4>
									<ul class="list-unstyled footer-list half-paddings">
                                    <?php foreach($o_page->get_pSubPages(19337898) as $info_page) {?>
                                    <li><a class="block" href="<?=$info_page['page_link']?>"><i class="fa fa-angle-right"></i> <?=$info_page['Name'];?></a></li>
                                    <?php } ?>
									</ul>
								</div>
                      

								<div class="col-md-3 hidden-sm hidden-xs">
									<!--<h4 class="letter-spacing-1">�����������</h4>-->
<!--									<ul class="list-unstyled footer-list half-paddings noborder">
										<li><a class="block" href="#"><i class="fa fa-angle-right"></i> �� ���</a></li>
										<li><a class="block" href="#"><i class="fa fa-angle-right"></i> ��������</a></li>
										<li><a class="block" href="#"><i class="fa fa-angle-right"></i> ���� ��������</a></li>
										<li><a class="block" href="#"><i class="fa fa-angle-right"></i> ��������</a></li>
										<li><a class="block" href="#"><i class="fa fa-angle-right"></i> ��������</a></li>
                                     </ul>-->
                                     
                                     <?php 

                                     $attr['parent']['ul'][0] = 'class="list-unstyled footer-list half-paddings noborder"'; 
                                     $attr['parent']['a'][0] = 'class="block"';
                                     $attr['parent']['a'][1] = '<i class="fa fa-angle-right"></i>';
                                     echo $o_page->buildMenu($o_page->_site['StartPage'], $menu_data, 1, $attr['parent']); 									 
									 ?>

								</div>

								<div class="col-md-4">
									<h4 class="letter-spacing-1">������ � ���</h4>
                                    ��.�����, ��.�������<br>
                                    ��.&rdquo;����� �����&rdquo; �93<br>
                                    ���.: 02/ 480-11-24, 480-11-25<br>
                                    ����: 02/917-39-16<br><br>
                                     
                                    ����: <span style="color: #E7E7E7;"><strong>+359 (0)889 77 80 07</strong></span><br>
                                    T������: <span style="color: #E7E7E7;"><strong>+359 (0)898 62 04 78</strong></span><br>
                                    �������: <span style="color: #E7E7E7;"><strong>+359 (0)878 69 87 18</strong></span>

								</div>

							</div>

						</div>
						<!-- /col #2 -->

					</div>

				</div>

				<div class="copyright">
					<div class="container">
						<ul class="pull-right nomargin list-inline mobile-block">
							<li>��� ������ � ���������: <a href="http://maksoft.net">�������</a></li>
						</ul>

						&copy; 2015-2016 ��� ������� 2000 ��������
					</div>
				</div>

			</footer>
            
            	<!-- /FOOTER -->
            
<div id="RegistrationForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="RegistrationFormLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="RegistrationFormLabel"><i class="fa fa-users"></i> �����������</h4>
			</div>

			<!-- Modal Body -->
			<div class="modal-body">
            <?php include __DIR__."/../../forms/topexpress/registration.php"; ?>
            
<!--				<h4>Text in a modal</h4>
				<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>

				<h4>Popover in a modal</h4>
				<p>This <a href="#" data-toggle="popover" class="btn btn-default" title="A Title" data-content="And here's some amazing content. It's very engaging. right?">button</a> should trigger a popover on click.</p>

				<h4>Tooltips in a modal</h4>
				<p><a href="#" data-toggle="tooltip" title="Tooltip">This link</a> and <a href="#" data-toggle="tooltip" title="Tooltip">that link</a> should have tooltips on hover.</p>-->
			</div>

		</div>
	</div>
</div>	

<div id="LoginForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="LoginFormLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="LoginFormLabel"><i class="fa fa-sign-in" aria-hidden="true"></i> ����</h4>
			</div>

			<!-- Modal Body -->
			<div class="modal-body">
            <?php include __DIR__."/../../forms/topexpress/login.php"; ?>
            
<!--				<h4>Text in a modal</h4>
				<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>

				<h4>Popover in a modal</h4>
				<p>This <a href="#" data-toggle="popover" class="btn btn-default" title="A Title" data-content="And here's some amazing content. It's very engaging. right?">button</a> should trigger a popover on click.</p>

				<h4>Tooltips in a modal</h4>
				<p><a href="#" data-toggle="tooltip" title="Tooltip">This link</a> and <a href="#" data-toggle="tooltip" title="Tooltip">that link</a> should have tooltips on hover.</p>-->
			</div>

		</div>
	</div>
</div>	

