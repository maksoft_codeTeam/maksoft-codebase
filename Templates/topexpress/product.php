<section class="product">
    <div class="container">
        <div class="row" style="margin-bottom: 1%;">
            <div class="col-sm-3">
                <a href="<?php echo $o_page->get_pLink($o_page->_page['ParentPage']);?>"><i class="fa fa-chevron-left" aria-hidden="true"></i> ����� </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="thumbnail relative margin-bottom-3 height-padding">
                            <figure id="zoom-primary" class="zoom" data-mode="mouseover">
                                <?= $o_page->print_pImage(600, "class=\"img-responsive\"") ?>
                            </figure>
                               <figure class="zoom buttonzoom">
                                <a class="lightbox bottom-right" href="<?= $o_page->get_pImage() ?>" data-plugin-options='{"type":"image"}'><i class="glyphicon glyphicon-search"></i></a>
								</figure>
                        </div>
                        <!-- Thumbnails (required height:100px) -->
                        <div data-for="zoom-primary" class="zoom-more owl-carousel owl-padding-3 featured" data-plugin-options='{"singleItem": false, "autoPlay": false, "navigation": true, "pagination": false}'>
                            <a class="thumbnail active" href="<?= $o_page->get_pImage() ?>">
                                <img src="<?= $o_page->get_pImage() ?>" height="100" alt="" />
                            </a>
                                 <?php
$page_gallery = $o_page->get_pSubpages(NULL, "p.sort_n ASC", "im.image_src !=''");
if ($page_gallery) {
  $sub_title   = '';
  $sub_content = '';
  for ($i = 0; $i < count($page_gallery); $i++) {
    $sub_title .= '<h5>' . $page_gallery[$i]['Name'] . '</h5>';
    $sub_content .= '<p id=' . $page_gallery[$i]['Name'] . '>' . $page_gallery[$i]['textStr'] . '</p>';
    echo "<a href=\"" . $o_page->get_pImage($page_gallery[$i]['n']) . "\" class=\"thumbnail\">";
    $o_page->print_pImage(100, "", "", $page_gallery[$i]['n']);
    echo "</a>";
  } //$i = 0; $i < count($page_gallery); $i++
} //$page_gallery

$availability = is_available($o_page->get_sLanguage(), (int) $product->available);
?>
                        </div>
                                    <!-- /Thumbnails -->
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="shop-item-price">
                        <span class="pull-right text-<?=$availability['code'];?>">
                            <i class="fa fa-check"></i><?=$availability['status'];?>
                        </span>
                        <?= $o_page->print_pName() ?>
                        </div>
                        <hr />
                        <p> <?=$o_page->_page['textStr']?> </p>
                <?php  if (in_promotion($product->initial)) { ?>
                    <p>
                        <span class="pull-left text-success"><i class="glyphicon glyphicon-star-empty"></i>��������:</span>
                    </p>
                <?php } ?>
                        <hr />
                        <div class="shop-item-price">
                  <?php
// COLORS
$colors_bar = "";
if(!empty($product->color)){
    $colors_bar = get_colors($product, $colors);
}
$subpages = $o_page->get_pSubpages();
if(!empty($subpages)){
    foreach($subpages as $_page){
        eval($_page['PHPcode']);
        $product = json_decode($product);
        $colors_bar .= get_colors($product, $colors, "");
    }
}
// PRICES
if($user->AccessLevel > 0 and $prices = $o_page->get_pPrice($o_page->_page['n'])){
    $price = array_shift($prices);
    $promo_price = get_price($product->initial, $price['price_value']);
    if($promo_price < $price['price_value']){
        $price['price_value'] = $promo_price;
    }
    $form->setAction($uri);
    $form->setId("add-product");
    echo $form->start(); //form start here 
	echo '<div class="pPrice">'.number_format($price["price_value"], '2', '.', '') . ' ' . $price["currency_string"].'/��.</div>';
    echo $colors_bar;
    ?>
	    <div class="row">
		<div class="btn-group pull-left product-opt-qty margin-top-30 margin-left-20">
             <?=$form->qty;?>
		
       
        <input type="hidden" name="name" value="<?=$o_page->_page['Name'];?>">
        <input type="hidden" name="unit" value="��.">
        <input type="hidden" name="price" value="<?=$price['price_value']?>">
        <input type="hidden" name="link" value="<?=$o_page->get_pLink($o_page->_page['n']);?>">
<?php
        echo $form->sku;
        echo $form->action;
        echo $form->submit;
        echo $form->end(); //END form tag
        ?> </div></div> <?
} else {
    echo '<div class="pPrice">'.$colors_bar.'</div>';
}
?>
</div> 


                                    <!-- ������� -->
                                   
                                    <div class="margin-top-10 pull-left">
                                     <hr>
                                    <p class="margin-top-10 pull-left">
                                        ������� �:
                                        </p> 
                                        <a href="#" class="social-icon social-icon-sm social-icon-transparent social-facebook pull-left" data-toggle="tooltip" data-placement="top" title="Facebook">
                                            <i class="icon-facebook"></i>
                                            <i class="icon-facebook"></i>
                                        </a>

                                        <a href="#" class="social-icon social-icon-sm social-icon-transparent social-twitter pull-left" data-toggle="tooltip" data-placement="top" title="Twitter">
                                            <i class="icon-twitter"></i>
                                            <i class="icon-twitter"></i>
                                        </a>

                                        <a href="#" class="social-icon social-icon-sm social-icon-transparent social-gplus pull-left" data-toggle="tooltip" data-placement="top" title="Google plus">
                                            <i class="icon-gplus"></i>
                                            <i class="icon-gplus"></i>
                                        </a>

                                        <a href="#" class="social-icon social-icon-sm social-icon-transparent social-linkedin pull-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
                                            <i class="icon-linkedin"></i>
                                            <i class="icon-linkedin"></i>
                                        </a>
                                    </div>
                                    <!-- /Share -->
<!--<div class="margin-top-10 width-100">������� �:</div>-->
                                </div>
							</div>                           
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="margin-bottom-60">
                                <div class="owl-carousel featured"
                                     data-plugin-options='{"singleItem": true,
                                                           "stopOnHover":false,
                                                           "autoPlay":false, 
                                                           "autoHeight": false,
                                                           "navigation": true,
                                                           "pagination": false}'>
                                    <div>
                                        <ul id="top_products" class="list-unstyled nomargin nopadding text-left">
                                        </ul>
                                    </div><!-- /SLIDE 1 -->
                                </div>
                            </div>
                            <!-- /FEATURED -->
                        </div>
                    </div>
                </div>
            </section>
			<section class="same-products">
				<div class="container">
                    <div id="second_slider" class="col-md-12 list-unstyled nomargin nopadding">
                    </div>
				</div>
			</section>
			<!-- /FEATURED -->
            <!-- item -->
            <script id="product-slider-title" type="text/x-handlebars-template">
                <h2 class="owl-featured padding-bottom-20"><strong>{{group_title}}</strong></h2>
            </script>
            <script id="entry-template22" type="text/x-handlebars-template">
            <div class="col-md-2">
            <div class="shop-item nomargin" id="top-{{n}}">
                <div class="thumbnail featured clearfix pull-left">
                    <!-- product image(s) -->
                    <a class="shop-item-image" href="/page.php?n={{n}}&SiteID={{SiteID}}">
                        <img class="img-responsive" src="{{image_src}}" alt="{{Name}}" height="220px" />
                    </a>
                </div>
                <div class="block size-12 text-center">
                    <a href="/page.php?n={{n}}&SiteID={{SiteID}}">{{Name}}</a>
                </div>
            </div>
            </div>
            
            </script>
            <!-- /item -->
			<!-- /FEATURED -->
            <script id="entry-template" type="text/x-handlebars-template">
                <li id="top-{{n}}" class="clearfix">
                    <div class="thumbnail featured clearfix pull-left">
                        <a href="/page.php?n={{n}}&SiteID={{SiteID}}">
                            <img src="{{image_src}}" width="80" height="80" alt="{{Name}}">
                        </a>
                    </div>
                    <a class="block size-12" href="/page.php?n={{n}}&SiteID={{SiteID}}">{{Name}}</a>
<?php if($user->AccessLevel > 0 and $prices = $o_page->get_pPrice($o_page->_page['n'])){ ?>
                    {{#if price_value}}
                    <div class="size-18 text-left">{{price_value}} ��.</div>
                    {{/if}}
<?php } ?>
                </li>
            </script>
        <script>
jQuery(document).ready(function(){
    $ = jQuery;
    jQuery('.tag.shop-color').darkTooltip({
            animation: 'flipIn',
            theme: 'light'
    });
    setTimeout(function(){
        jQuery(".shop-item-price").addClass("animated pulse");
    }, 3000)
    jQuery(".shop-item-price").removeClass("animated pulse");
    var source   = jQuery("#entry-template").html();
    var template = Handlebars.compile(source);
    var source = jQuery("#entry-template22").html();
    var template2 = Handlebars.compile(source);
    var productSliderTemplate = jQuery("#product-slider-title").html();
    var sliderName = Handlebars.compile(productSliderTemplate);

    $.get("/api/?command=page_groups&n=<?=$o_page->_page['n']?>", function(data){
        $.each(data, function(i,g){
            if(i == 2){
                return false;
            }
            var url = "/api/?command=get_group_random&n="+g.group_id +"&limit=6";
            if(i % 2){
                print_products(url, template2, "#second_slider", "#top", "animated bounceInRight", 200, g.group_title, sliderName);
            } else {
                print_products(url, template, "#top_products", "#top", "animated zoomInDown", 200, g.group_title, sliderName);
            }
        });
    });

});

function print_products(url, template, selector, product_prefix, animation, timer, group_name, sliderName)
{
    $.get(url, function(data){
        var t = template;
        var s = selector;
        var c = animation;
        var p = product_prefix;
        var g = group_name;
        jQuery(s).append(jQuery(sliderName({group_title: g.toUpperCase()})));
        $.each(data, function(k, product){
            var html = jQuery(t(product));
            setTimeout(function(){
                html.addClass(c);
                jQuery(s).append(html);
            },timer+500);
            setTimeout(function(){
                jQuery(html).removeClass(c);
            },timer+300);
            timer += 800;
        });
    });
}
</script>
