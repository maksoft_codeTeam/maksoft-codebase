<!-- Top Bar -->
<div id="topBar" class="dark">
    <div class="container">
        <!-- right -->
        <ul class="top-links list-inline pull-right">
        <?php
		if($user->ID > 0) { ?>
            <li class="text-welcome hidden-xs"><?=HELLO?>, <strong><?php echo("$user->Name"); ?></strong></li>
            <li>
                <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><i class="fa fa-cogs" aria-hidden="true"></i> <?=OPTIONS?></a>
                <ul class="dropdown-menu pull-right">
                    <!--<li><a tabindex="-1" href="#"><i class="fa fa-user" aria-hidden="true"></i> ������</a></li>-->
                    <?php if($user->WriteLevel > 0 and $user->AccessLevel > 1){ ?>
                    <li>
                        <a tabindex="-1" href="page.php?n=11&SiteID=<?=$SiteID?>"> <i class="fa fa-cog" aria-hidden="true"></i> <?=ADM?></a>
                    </li>
                    <?php } ?>
                    <li>
                        <a tabindex="-1" href="page.php?n=19373087&SiteID=<?=$SiteID?>"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=ORDERS?></a>
                    </li>
                    <li class="divider"></li>
                    <li><a tabindex="-1" href="web/admin/logout.php"><i class="glyphicon glyphicon-off"></i> <?=LOGOUT?></a></li>
                </ul>
            </li>
        <?php } else { ?>
            <li><a href="#" data-toggle="modal" data-target="#LoginForm">&nbsp;&nbsp;<?=LOGIN?>&nbsp;&nbsp;</a></li>
            <li><a href="#" data-toggle="modal" data-target="#RegistrationForm"><?=REGISTER?></a></li>
        <?php } ?>
		</ul>
        <!-- left -->
        <ul class="top-links list-inline">
            <li>
                <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">
                <?php
					if($o_page->_site['language_id'] == 1) { 
		        ?>
                <img class="flag-lang" src="<?=DIR_TEMPLATE?>assets/images/flags/bg.png" width="16" height="11" alt="lang" /> ���������
                <?php } else { ?>
                <img class="flag-lang" src="<?=DIR_TEMPLATE?>assets/images/flags/us.png" width="16" height="11" alt="lang" /> English
                <?php } ?>
                </a>
                <ul class="dropdown-langs dropdown-menu">

                    <li>
                        <a tabindex="-1" href="page.php?n=19337894&SiteID=1015">
                        <img class="flag-lang" src="<?=DIR_TEMPLATE?>assets/images/flags/bg.png" width="16" height="11" alt="lang" />
							���������
                        </a>
                    </li>
                    <li class="divider"></li>
					<li><a tabindex="-1" href="page.php?n=19337505&SiteID=1014"><img class="flag-lang" src="<?=DIR_TEMPLATE?>assets/images/flags/us.png" width="16" height="11" alt="lang" /> English</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<div id="header" class="sticky clearfix shadow-after-3">
    <!-- �������� -->
    <div class="search-box over-header">
        <a id="closeSearch" href="#" class="glyphicon glyphicon-remove"></a>

        <form method="get" class="search-form" action="page.php">
            <input type="hidden" name="n" value="19337899">
            <input type="hidden" name="SiteID" value="<?php echo $o_site->SiteID?>">
            <input type="text" class="form-control" name="search" id="search" placeholder="<?=SEARCH_PLACEHOLDER?>" />
           
        </form>
    </div> 
    <div class='container'>
        <?=Message\display();?>
    </div>
    <!-- /�������� -->
    <!-- TOP NAV -->
    <header id="topNav">
        <div class="container">
            <!-- Mobile Menu -->
            <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <!-- ������ -->
            <ul class="pull-right nav nav-pills nav-second-main">
                <li class="search">
                    <a href="javascript:;">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
            <?php 
                if($user->WriteLevel > 0 or $user->AccessLevel > 0){
                    include __DIR__.'/templates/widgets/cart_header.php';
                }
            ?>
            </ul>
            <!-- Logo -->
            <a class="logo pull-left" href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>">
                <img src="<?=DIR_TEMPLATE?>assets/images/logo_topexpress.png" alt="" />
            </a>

			<div class="navbar-collapse pull-right nav-main-collapse collapse">
				<nav class="nav-main">
                     <?php 
                     $menu_data = $o_page->formatMenuData("". SITEID ."");
                     $attr = array(
                        'child' => array(
                            'li' => array('class="dropdown"', ''), 
                            'a'  => array('class="dropdown-toggle"', ''),
                            'ul' => array('class="dropdown-menu"', ''),
                            ),
                        'parent' => array(
                            'li' => array('', ''),
                            'a'  => array('class="dropdown-toggle"', ''),
                            'ul' => array('id="topMain" class="nav nav-pills nav-main"', ''),
                            )
                        );
                         echo $o_page->buildMenu($o_page->_site['StartPage'], $menu_data, 4, $attr['parent'], $attr['child']); 
                     ?>
				</nav>
			</div>
        </div>
    </header>
</div>
