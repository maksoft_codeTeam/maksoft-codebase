<?php
$sql = "/* topexpress new_products.php */
    SELECT pages.n, i.image_src, i.imageName, p.price_value FROM `pages`
    inner join images i on i.ImageID = pages.ImageNo
    left join prices p on p.price_n = pages.n
    where pages.PHPcode LIKE \"%p_Id%\"
    and pages.SiteID = :SiteID
    and pages.date_modified BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW()
    GROUP BY pages.n;
";
$item_type = '��� �������';

require_once __DIR__.'/base.php';

echo '<div class="row">';
echo '<ul class="shop-item-list row list-inline nomargin">';
while($page = $stmt->fetch(\PDO::FETCH_ASSOC)){
    eval($page['PHPcode']);
    $p = json_decode($product);
    $discounted_price = $page['price_value'] / (1+ $p->initial->promo_discount);
    $link = $o_page->get_pLink($page['n']);
    $img = $page['image_src'];
    $img_alt = $page['imageName'];
    if(empty($img)) {
        $img = '/web/admin/images/no_image.jpg';
    }
    $full_title = $page['Name'];
    $title = cut_text($full_title, MAX_TITLE_LENGTH,'');
    include __DIR__.'/single_product_tmpl.php';
}
echo '</ul></div>';
