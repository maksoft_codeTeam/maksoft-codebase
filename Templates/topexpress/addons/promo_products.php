<?php
$sql = "SELECT * 
FROM  `pages` 
INNER JOIN prices p ON p.price_n = pages.n
INNER JOIN images i ON i.imageID = pages.imageNo
WHERE  `PHPcode` LIKE  '%,\"in_promotion\":\"1\"%'
AND  `SiteID` =:SiteID";
$item_type = 'Промоция';

require_once __DIR__.'/base.php';

echo '<div class="row">';
echo '<ul class="shop-item-list row list-inline nomargin">';
while($page = $stmt->fetch(\PDO::FETCH_ASSOC)){
    eval($page['PHPcode']);
    $p = json_decode($product);
    if(!in_promotion($p->initial)){
        echo '*';
        continue;
    }

    $discounted_price = get_price($p->initial, $page['price_value']);
    
    if($page['price_value'] <= $discounted_price){
        continue;
    }

    $link = $o_page->get_pLink($page['n']);
    $img = $o_page->get_pImage($page['n']);
    $img_alt = $page['imageName'];
    if(empty($img)) {
        $img = '/web/admin/images/no_image.jpg';
    }
    $full_title = $page['Name'];
    $title = cut_text($full_title, MAX_TITLE_LENGTH,'');
    include __DIR__.'/single_product_tmpl.php';
}
echo '</ul></div>';
