<li class="col-lg-3 col-sm-4">
    <div class="shop-item">
        <div class="thumbnail">
            <!-- product image(s) -->
            <a class="shop-item-image" href="<?=$link?>">
            <img class="img-responsive" src="<?=$page['image_src']?>" alt="<?=$page['imageName'];?>">
            </a>
            <!-- /product image(s) -->

            <!-- product more info -->
            <div class="shop-item-info">
                <span class="label label-success"><?=$item_type?></span>
                <?php if($p->initial and $p->initial->promo_discount){
                        echo ' <span class="label label-danger">-'.$p->initial->promo_discount.'%</span>';
                    }
                ?>
            </div>
            <!-- /product more info -->
        </div>
        <div class="shop-item-counter">
            <div class="countdown is-countdown" data-from="<?=$p->initial->promo_from_date;?>" data-labels="years,months,weeks,days,hour,min,sec">                
            </div>
        </div>
        
        <div class="shop-item-summary text-center">

            <h3><?=$title?></h3>
            
            <?php 
            if($user->AccessLevel > 0 and isset($page['price_value']) and $page['price_value'] > 0){ ?>
            <div data-n="<?=$page['n']?>" class=""> </div>
            <?php } ?>
            <!-- /price -->
        </div>

    </div>
</li>
