			<section class="featured-grid">
				<div class="container">
					
					<div class="row">

						<div class="col-sm-8">
                            <a href="<?=$o_page->get_pLink(19337903)?>">
							<img src="<?=DIR_TEMPLATE?>assets/images/home/home-za-doma.jpg" alt="<?=$o_page->get_pName(19337903)?>" />
                            </a>
						</div>

						<div class="col-sm-4">
                        
                                <a href="<?=$o_page->get_pLink(19337904)?>">
								<img src="<?=DIR_TEMPLATE?>assets/images/home/home-za-gradinata.jpg" alt="<?=$o_page->get_pName(19337904)?>" />
                                </a>

                                <a href="<?=$o_page->get_pLink(19338245)?>">
								<img src="<?=DIR_TEMPLATE?>assets/images/home/home-za-baniata.jpg" style="margin-top: 12px;" alt="<?=$o_page->get_pName(19338245)?>" />
                                </a>
                                                          
							</div>
							
						</div>
						
					</div>

				</div>
			</section>

			<section class="featured">
				<div class="container">

					<h2 class="owl-featured noborder"><strong><?=TOP?> <?=PRODUCTS?></strong></h2>
					<div class="owl-carousel featured nomargin owl-padding-10" data-plugin-options='{"singleItem": false, "items": "4", "stopOnHover":false, "autoHeight": false, "navigation": true, "pagination": false}'>
						
					<?php 
	                    $n_topproducts = "3598";
                        $topproducts = $o_page->get_pGroupContent($n_topproducts);
                        
                            for($i=0; $i<count($topproducts); $i++)
                                {
                                    ?>
                                       
						<div class="shop-item nomargin">

							<div class="thumbnail">
								<a class="shop-item-image" href="<?=$o_page->get_pLink($topproducts[$i]['n'])?>">
									<img class="img-responsive" src="/img_preview.php?image_file=<?=$topproducts[$i]['image_src']?>&img_width=200px" alt="<?=$o_page->get_pName($topproducts[$i]['n'])?>" />
								</a>
							</div>
							
							<div class="shop-item-summary text-center">
								<h3 style="color:white;"><?=$o_page->get_pName($topproducts[$i]['n'])?></h3>
							</div>

								<div class="shop-item-buttons text-center">
									<a class="btn btn-default" href="<?=$o_page->get_pLink($topproducts[$i]['n'])?>"><i class="fa fa-info" aria-hidden="true"></i> Повече информация</a>
								</div>
								
						</div>                
                                
                    <?							
					}
					?>

					</div>

				</div>
			</section>
