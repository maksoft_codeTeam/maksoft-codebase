<?php
require_once __DIR__."/../../loader.php";
require_once __DIR__."/../../lib/messages.php";
require_once __DIR__."/../../forms/topexpress/cart/Add.php";
require_once __DIR__."/../../forms/topexpress/cart/Reset.php";
require_once __DIR__."/../../forms/topexpress/cart/Remove.php";
require_once __DIR__."/../../forms/topexpress/cart/Purchase.php";
require_once __DIR__.'/../../forms/topexpress/ClientRegister.php';
require_once __DIR__."/helpers.php";
require_once __DIR__."/../animaliashop/DBCart.php";




define("DIR_PHP","Templates/topexpress/");

define("DIR_TEMPLATE","/Templates/topexpress/");

define("DIR_TEMPLATE_IMAGES","/Templates/topexpress/images/");

define("C_PREFIX", $o_site->_site['primary_url'].$o_page->_page['n']);



$detect = new Mobile_Detect;

if(!isset($_SESSION['isMobile']) or $_SESSION['isMobile'] !== $detect->isMobile()){
    $_SESSION['isMobile'] = $detect->isMobile();
}

if(!$_SESSION['isTablet'] or $_SESSION['isTablet'] !== $detect->isTablet()){
    $_SESSION['isTablet'] = $detect->isTablet();
}

if(!$_SESSION['isMobile']){
    $max_columns = $o_page->_page['show_link_cols'];
} else {
    if($_SESSION['isTablet']){
        $max_columns = 2;
    } else {
        $max_columns = 1;
    }
}
