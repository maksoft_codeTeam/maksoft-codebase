<?php
require_once __DIR__. '/../../loader.php';
require_once __DIR__ .'/helpers.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
$request = Request::createFromGlobals();

$response = new JsonResponse();

$check = function($kwargs) use ($o_page) {
    $n = $kwargs['n'];
    $page = $o_page->get_page($n, 'object');
    return validate_page($page, $o_page);
};

$commands = array(
    'build' => $check,
);


function validate_page($page, $o_page) {
    if(empty($page->PHPcode) or $page->SiteID != 1015) {
        return array(1,2,333);
    }

    return build_json_response($page, $o_page);
}

function build_json_response($page, $o_page) {
    global $colors;
    eval($page->PHPcode);
    $product = json_decode($product);
    $availability = is_available($o_page->get_sLanguage(), (int) $product->available);
    $data = array();
    $data['availability'] = is_available($availability);
    $data['promotion'] = in_promotion($page->initial);
    $data['colors'] = get_colors($product, $colors, 'checked');
    $data['unit'] = 'бр,';
    $data['link'] = $o_page->get_pLink($page->n);
    $data['name'] = $page->Name;
    $data['product'] = $product;
    $data['sku'] = $product->p_Id;
    $data['image'] = $page->image_src;

    if(!$prices = $o_page->get_pPrice($page->n)){
        $prices = array();
    } 
    if($price = array_shift($prices)) {
        $data['price'] = get_price($product->initial, $price['price_value']);
        $data['original_price'] = $price['price_value'];
        $data['currency'] = $price['currency_string'];
        $data['min_qty'] = $price['pcs'];
    }

    $subpages = $o_page->get_pSubpages($page->n);
    if(!empty($subpages)){
        foreach($subpages as $_page){
            eval($_page['PHPcode']);
            $product = json_decode($product);
            $data['colors'] .= get_colors($product, $colors, "");
        }
    }
    return $data;
}


if(!isset($_GET['command']) or !array_key_exists($_GET['command'], $commands)) {
    $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
    $response->prepare($request);
    $response->send();
    return;
}

$cmd = $_GET['command'];

$data = json_encode($commands[$cmd]($_GET));

$response->setContent(iconv('cp1251', 'utf8', $data));

$response->prepare($request);

$response->send();
