<?php if($user->AccessLevel > 3){ ?>
<style>
	#slider:hover .hover-edit, .intro:hover .hover-edit {
		display:block;
	}
	#footer-sidebar .widget article:hover {
		border:1px dashed #fff;
	}
	#footer-sidebar .widget article:hover .hover-edit {
		display:block;
	}
	
</style>
<?php } ?>
<!-- ����� -->
<footer class="footer black" role="contentinfo">
	<div class="wrap">
		<div id="footer-sidebar" class="footer-sidebar widget-area clearfix row" role="complementary">
			<ul>
				<li class="widget widget-sidebar">
					<article class="about_widget clearfix one-half widget-rel">
						<div class="hover-edit"><a href="#" data-toggle="modal" data-target="#settingsModal"><i class="fa fa-cog fa-border" aria-hidden="true"></i></a></div>
						<h6><?=$o_site->_site['Title']?></h6>
						<p><?=$o_site->_site['Description']?></p>
					</article>
				</li>
				<li class="widget widget-sidebar">
					<article class="contact_widget one-fourth widget-rel">
						<div class="hover-edit"><a href="#" data-toggle="modal" data-target="#settingsModal"><i class="fa fa-cog fa-border" aria-hidden="true"></i></a></div>
						<h6>�� ��������</h6>   
						<?php if($o_page->_site['SAddress']) { ?>
						<p class="contact-data">
							<i class="fa fa-map-marker icon" aria-hidden="true"></i> <a href="https://www.google.bg/maps/place/<?php echo $o_page->_site['SAddress'];?>" target="_blank"><?php echo $o_page->_site['SAddress'];?></a>
						</p>
						<?php }?>  
						<?php if($o_page->_site['SPhone']) { ?>
						<p class="contact-data">
							<i class="fa fa-phone icon" aria-hidden="true"></i> <a href="tel:<?php echo $o_page->_site['SPhone'];?>"><?php echo $o_page->_site['SPhone'];?></a>
						</p>
						<?php }?>

					</article>
				</li>
				<li class="widget widget-sidebar">
					<article class="social_widget one-fourth widget-rel">
						<div class="hover-edit"><a href="#" data-toggle="modal" data-target="#socialsModal"><i class="fa fa-cog fa-border" aria-hidden="true"></i></a></div>
						<h6>��������� ��</h6>
						<ul class="social">
							<?php if(defined('CMS_FACEBOOK_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" target="_blank" class="fa fa-facebook"></a></li> <?php }?>
                                
							<?php if(defined('CMS_GPLUS_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_GPLUS_PAGE_LINK")) ? CMS_GPLUS_PAGE_LINK:"#"?>" target="_blank" class="fa fa-google-plus"></a></li> <?php }?>
                               
							<?php if(defined('CMS_LINKEDIN_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_LINKEDIN_PAGE_LINK")) ? CMS_LINKEDIN_PAGE_LINK:"#"?>" target="_blank" class="fa fa-linkedin"></a></li> <?php }?>
                              
							<?php if(defined('CMS_PINTEREST_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_PINTEREST_PAGE_LINK")) ? CMS_PINTEREST_PAGE_LINK:"#"?>" target="_blank" class="fa fa-pinterest"></a></li> <?php }?>
                               
							<?php if(defined('CMS_TWITTER_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_TWITTER_PAGE_LINK")) ? CMS_TWITTER_PAGE_LINK:"#"?>" target="_blank" class="fa fa-twitter"></a></li> <?php }?>   
                               
							<?php if(defined('CMS_BLOG_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_BLOG_PAGE_LINK")) ? CMS_BLOG_PAGE_LINK:"#"?>" target="_blank" class="fa fa-bold"></a></li> <?php }?>
                                
							<!-- RSS -->
							<li class="rss"><a href="/rss.php" class="fa fa-rss"></a></li>
							
						</ul>
					</article>
				</li>
			</ul>
		</div>
		<!-- #secondary -->
		<div class="copy">
			<p><?php include("Templates/copyrights_links.php"); ?></p>
			<!--����� ��������� (�� ��� �� ����������: ����� ������) -->
			<nav class="foot-nav">
				<ul id="menu-footer" class="">
					    <?php if(($o_site->_site['copyright_n'] > 0)) { ?><li><a href="<?=$o_page->get_pLink($o_site->_site['copyright_n']);?>"><?=$o_page->get_pName($o_site->_site['copyright_n']);?></a></li>
						<?php
						}
						if ( !isset( $footer_links ) )$footer_links = 5;
						$footer_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $footer_links" );

						for ( $i = 0; $i < count( $footer_links ); $i++ ) {
							$subpages = $o_page->get_pSubpages( $footer_links[ $i ][ 'n' ] );
							?>
						<li>
							<a href="<?php echo $o_page->get_pLink($footer_links[$i]['n']) ?>">
								<?php echo $o_page->get_pName($footer_links[$i]['n']) ?>
							</a>
						</li>

						<?php  
						}
						?>
				</ul>
			</nav>
			<!--//����� ���������-->
		</div>
	</div>
</footer>
<!-- //����� -->

<?php include TEMPLATE_DIR . "inc/modals.php"; ?>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<!--    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/core.min.js'></script>
<!--<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/widget.min.js'></script>-->
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/mouse.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/slider.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/button.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/datepicker.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/effect.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery.uniform.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/respond.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery.slicknav.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/scripts.js'></script>

<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/vendors.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/styleswitcher.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/colors.js'></script>

<!-- REVOLUTION SLIDER -->
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/plugins/slider.revolution/js/settings.revolution_slider.js"></script>
<!-- DATETIME PICKER -->
<script type='text/javascript' src='/global/Pikaday/pikaday.js'></script>


<?php if($user->AccessLevel > 3){ ?>
 <script type="text/javascript" src="<?=ASSETS_DIR?>assets/plugins/farbtastic/farbtastic.js"></script>
<script type="text/javascript">
  $(document).ready(function() {

    $('#colorpicker').farbtastic('#color');
	$('#colorpicker-cssname').farbtastic('#color-cssname');

  });
</script>
<?php } ?>

<?php include( "Templates/footer_inc.php" ); ?>
<script>
jQuery(document).ready(function(){
<?php echo implode(PHP_EOL, $custom_js);?>
});
</script>

</body> 
</html>
