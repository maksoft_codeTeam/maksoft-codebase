<!DOCTYPE html>
<html lang="<?php echo($Site->language_key); ?>">
<head>
<meta http-equiv="content-type" content="text/html;charset=windows-1251"/>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title><?=$Title?></title>
<?php
require_once "lib/lib_page.php";
require_once "lib/Database.class.php";
//include meta tags
include( "Templates/meta_tags.php" );
$o_site->print_sConfigurations();

$db = new Database();
$o_page->setDatabase( $db );
$o_user = new user( $user->username, $user->pass );
?>
<!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" type="text/css" />-->

<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link rel='stylesheet' href='<?=ASSETS_DIR?>assets/css/style.css' type='text/css' media='all'/>
<link rel='stylesheet' href='<?=ASSETS_DIR?>assets/css/jquery-ui.theme.min.css' type='text/css' media='all'/>
<link rel='stylesheet' href='/global/Pikaday/css/pikaday.css'></script>
<style>
	:root {
		--main-color:<?php if(defined("TMPL_CSS_NAME_COLOR")) {
			echo TMPL_CSS_NAME_COLOR;
		}
		else {
			echo "#000";
		}
		?>;
	}
</style>
<link rel='stylesheet' id='style-color-css' href='<?=ASSETS_DIR?>assets/css/color/theme-color.css' type='text/css' media='all'/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<?php if($user->AccessLevel > 3){ ?>
<link rel='stylesheet' href='<?=ASSETS_DIR?>assets/css/styler.css' type='text/css' media='all'/>
<link rel="stylesheet" href="<?=ASSETS_DIR?>assets/plugins/farbtastic/farbtastic.css" type="text/css"/>
<?php } ?>
<!-- REVOLUTION SLIDER -->
<link href="<?=ASSETS_DIR?>assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css"/>
<link href="<?=ASSETS_DIR?>assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css"/>
<!-- DATETIME PICKER -->
<link async rel="stylesheet" href='/global/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'>
<link async rel="stylesheet" href='/global/bootstrap-datetimepicker/datetimepicker.css'>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="<?=ASSETS_DIR?>assets/js/html5shiv.min.js"></script>
<script src="<?=ASSETS_DIR?>assets/js/respond.min.js"></script>
<![endif]-->
</head>

<body class="destination-template-default single single-destination">

	<!-- Preloader -->
	<div class="preloader">
		<div id="followingBallsG">
			<div id="followingBallsG_1" class="followingBallsG"></div>
			<div id="followingBallsG_2" class="followingBallsG"></div>
			<div id="followingBallsG_3" class="followingBallsG"></div>
			<div id="followingBallsG_4" class="followingBallsG"></div>
		</div>
	</div>
	<!-- //Preloader -->

	<?php 
	include TEMPLATE_DIR . "inc/custom-styles.php";
	include TEMPLATE_DIR . "inc/styleswitcher.php"; 
	include TEMPLATE_DIR . "inc/headline.php"; 
	?>

	<!-- �����-->
	<header id="header" class="header <?=confkey(" TMPL_HEADER_TYPE ");?>" role="banner">

		<div class="wrap">

			<!-- ���� -->
			<div class="logo">
				<?php
				if ( !empty( $o_site->_site[ 'Logo' ] ) ) {
					?>
				<a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>" title="<?=$o_site->_site['Title']?>"><img src="<?=$tmpl_config['default_logo']?>" class="site-logo"></a>
				<?
		} else { ?>
				<h3 class="h2-logo"><a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>" title="<?=$o_site->_site['Title']?>"><?=$o_site->_site['Name']?></a></h3>
				<?php } ?>
			</div>
			<!-- //���� -->

			<!--������� ���������-->
			<nav id="nav" class="main-nav">
				<ul id="menu-primary" class="">
				
					<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>">������</a>
						<?php

						if ( !isset( $primary_links ) )$primary_links = 2;
						$primary_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $primary_links" );

						for ( $i = 0; $i < count( $primary_links ); $i++ ) {
							$subpages = $o_page->get_pSubpages( $primary_links[ $i ][ 'n' ] );
							?>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
							<a href="<?php echo $o_page->get_pLink($primary_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <?php echo $primary_links[$i]['Name'] ?></a>
							<?php echo ((count($subpages) > 0) ? "<ul class='sub-menu'>" : "</li>") ?>
							<?php
							if ( count( $subpages ) > 0 ) {


								for ( $j = 0; $j < count( $subpages ); $j++ )
									echo "<li class='menu-item menu-item-type-post_type menu-item-object-destination'><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\">" . $subpages[ $j ][ 'Name' ] . "</a></li>";
								?>
				</ul>


				<?php 
	} 
}
?>
				<ul id="menu-haedline">
				<?php
					if ( !isset( $head2_links ) )$head2_links = 1;
					$head2_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $head2_links" );
					for ( $i = 0; $i < count( $head2_links ); $i++ ) {
					?>
					<li>
						<a href="<?php echo $o_page->get_pLink($head2_links[$i]['n']) ?>">
							<?php echo $o_page->get_pName($head2_links[$i]['n']) ?>
						</a>
					</li>
				<?php  
				}
				?>
				</ul>
				</ul>
			</nav>
			<!--//������� ���������-->
		</div>
	</header>
	<!-- //����� -->
