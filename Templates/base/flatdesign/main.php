    <div class="wrap">
        <div class="row">
        <?php
        if((defined("TMPL_CSS_LAYOUT") && TMPL_CSS_LAYOUT=="right-sidebar")){
            include TEMPLATE_DIR . "content.php";
            include TEMPLATE_DIR . "right-sidebar.php";
        }elseif((defined("TMPL_CSS_LAYOUT") && TMPL_CSS_LAYOUT=="two-sidebars")){
            include TEMPLATE_DIR . "left-sidebar.php";
            include TEMPLATE_DIR . "content.php";
            include TEMPLATE_DIR . "right-sidebar.php";
        }elseif((defined("TMPL_CSS_LAYOUT") && TMPL_CSS_LAYOUT=="no-sidebars")){
            include TEMPLATE_DIR . "content.php";
        }else{  
            include TEMPLATE_DIR . "left-sidebar.php";
            include TEMPLATE_DIR . "content.php";
        }
        ?>
        </div>
    </div>
