			<!-- REVOLUTION SLIDER -->
			<section id="slider" class="slider fullwidthbanner-container roundedcorners">
					<div class="hover-edit"><a href="#" data-toggle="modal" data-target="#sliderModal"><i class="fa fa-cog fa-border" aria-hidden="true"></i></a></div>
				<!--
					Navigation Styles:
					
						data-navigationStyle="" theme default navigation
						
						data-navigationStyle="preview1"
						data-navigationStyle="preview2"
						data-navigationStyle="preview3"
						data-navigationStyle="preview4"
						
					Bottom Shadows
						data-shadow="1"
						data-shadow="2"
						data-shadow="3"
						
					Slider Height (do not use on fullscreen mode)
						data-height="300"
						data-height="350"
						data-height="400"
						data-height="450"
						data-height="500"
						data-height="550"
						data-height="600"
						data-height="650"
						data-height="700"
						data-height="750"
						data-height="800"
				-->
				<div class="fullwidthbanner" data-height="<?php if(defined("TMPL_SLIDE_HEIGHT")) { echo TMPL_SLIDE_HEIGHT; } else { echo "450"; }?>" data-navigationStyle="">
	
					<ul class="hide">
					<?php for($i=0; $i<count($pBanners); $i++) { ?>
                   	   <!-- SLIDE  -->
	                   <li data-transition="<?php if(defined("TMPL_SLIDE_EFFECT")) { echo TMPL_SLIDE_EFFECT; } else { echo "fade"; }?>" data-slotamount="1" data-masterspeed="1500" data-delay="5000" data-saveperformance="off" data-title="<?=htmlspecialchars($o_page->get_pInfo("title", $pBanners[$i]['n']))?>" style="background-color: #F6F6F6;">
							<img src="<?=$pBanners[$i]['image_src']?>" alt="<?=htmlspecialchars($o_page->get_pInfo("title", $pBanners[$i]['n']))?>" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat">
						</li>
            		<?php } ?>
						
					</ul>
					<!--<div class="tp-bannertimer"></div>-->
				</div>
			</section>
			<!-- /REVOLUTION SLIDER -->