<?php if(defined("TMPL_BODY_BACKGROUND")) { ?>
<style>
body {
	background-image: url(<?=TMPL_BODY_BACKGROUND?>);
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
</style>
<? } ?>

<?php if(defined("TMPL_BACKGROUND_COLOR")) { ?>
<style>
body {
	background-color: <?=TMPL_BACKGROUND_COLOR?>;
}
</style>
<? } ?>