<?php
if ( !isset( $head_links ) )$head_links = 1; {
$head_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $head_links" );
?>
<div class="headline <?=confkey("TMPL_HEADER_TYPE");?>">
<div class="wrap">
<ul class="headinfo">
	<?php if(defined('CMS_FACEBOOK_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" target="_blank" class="fa fa-facebook"></a></li> <?php }?>

	<?php if(defined('CMS_GPLUS_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_GPLUS_PAGE_LINK")) ? CMS_GPLUS_PAGE_LINK:"#"?>" target="_blank" class="fa fa-google-plus"></a></li> <?php }?>

	<?php if(defined('CMS_LINKEDIN_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_LINKEDIN_PAGE_LINK")) ? CMS_LINKEDIN_PAGE_LINK:"#"?>" target="_blank" class="fa fa-linkedin"></a></li> <?php }?>

	<?php if(defined('CMS_PINTEREST_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_PINTEREST_PAGE_LINK")) ? CMS_PINTEREST_PAGE_LINK:"#"?>" target="_blank" class="fa fa-pinterest"></a></li> <?php }?>

	<?php if(defined('CMS_TWITTER_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_TWITTER_PAGE_LINK")) ? CMS_TWITTER_PAGE_LINK:"#"?>" target="_blank" class="fa fa-twitter"></a></li> <?php }?>   

	<?php if(defined('CMS_BLOG_PAGE_LINK') || $user->AccessLevel >15) { ?> <li><a href="<?=(defined("CMS_BLOG_PAGE_LINK")) ? CMS_BLOG_PAGE_LINK:"#"?>" target="_blank" class="fa fa-bold"></a></li> <?php }?>

	<?php if($o_page->_site['SPhone']) { ?>
	<li><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:<?php echo $o_page->_site['SPhone'];?>"><?php echo $o_page->_site['SPhone'];?></a></li>
	<?php }?>
</ul>
<ul class="headmenu">
<?php
for ( $i = 0; $i < count( $head_links ); $i++ ) {
	$subpages = $o_page->get_pSubpages( $head_links[ $i ][ 'n' ] );
?>
<li>
	<a href="<?php echo $o_page->get_pLink($head_links[$i]['n']) ?>">
		<?php echo $o_page->get_pName($head_links[$i]['n']) ?>
	</a>
</li>
<?php  
} echo "</ul></div></div>"; }
?>