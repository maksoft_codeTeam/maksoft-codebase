<?php
    function print_pSubContentBS($parent_page=NULL, $filter=1, $filter_by_tag = true, $cms_args = array(), $limit=false)
    {
        //array of global parameters overwriting some of the variables
        global $cms_args;
        
        if($parent_page==NULL) $parent_page = $this->n;
        
        $colls = $this->get_pSubPagesColls();
        if($cms_args['CMS_COLS'])
            $colls =    $cms_args['CMS_COLS'];
        
        $order = $this->get_pSubPagesOrder($parent_page);
        //output group content
        $group_id = $this->get_pInfo("show_group_id", $parent_page);
        //get_pGroupContent($group_id=NULL, $date_expire="0000-00-00", $sort_order= NULL, $filter=1)
        
        /*
        if(isset($group_id) && !empty($group_id))
            $subpages = $this->get_pGroupContent($group_id, date("Y-m-d"), $order, $filter);
        else
            $subpages = $this->get_pSubpages($parent_page, $order, $filter, $filter_by_tag);
        */
        
        $subpages = $this->get_pSubpages($parent_page, $order, $filter, $filter_by_tag, $limit);
        //add group content to subpages
        
        if(isset($group_id) && !empty($group_id))
            {
                $g_pages = array();
                //$s_pages = $this->get_pSubpages($parent_page, $order, $filter, $filter_by_tag);
                $g_order = $order;
                if($order == "p.sort_n ASC" || $order == "ASC" || $order == "p.sort_n DESC" || $order == "DESC")
                    $g_order = NULL;
                $g_pages = $this->get_pGroupContent($group_id, date("Y-m-d"), $g_order, $filter);

                global $user;
                if(count($subpages) == 0)
                    $subpages = $g_pages;
                else
                    if(count($g_pages) > 0)
                    //$subpages = array_merge($s_pages, $g_pages);
                    if($this->n == $this->_site['StartPage']) {
                        $subpages = $g_pages;
                    } else {
                        $subpages = array_merge($subpages, $g_pages);
                    }                        
                    //$subpages = array_merge($subpages, $g_pages);

/*                if(count($g_pages) > 0) {
                    if($this->n == $this->_site['StartPage']) {
                     $subpages = $g_pages;
                    } else {
                     $subpages = array_merge($subpages, $g_pages);
                    }

             }*/


                //place code here for sorting the entire array of pages + group content
                //By now the group pages are added after the subpages
            }
        
        if(isset($cms_args['subpages']))
            $subpages = $cms_args['subpages'];
        
        $MAIN_WIDTH_DIMMENSION = "px";  
        
        if(isset($cms_args['MAIN_WIDTH']))
            $MAIN_WIDTH = $cms_args['MAIN_WIDTH'];
            
    
        if(site::$sMAIN_WIDTH)
            $MAIN_WIDTH = site::$sMAIN_WIDTH;
        
        /* mpetrov correction: 3.05.2014 */
        if(isset($cms_args['CMS_MAIN_WIDTH']))
            $MAIN_WIDTH = $cms_args['CMS_MAIN_WIDTH'];

        if(!isset($MAIN_WIDTH))
            $MAIN_WIDTH = 450;
                    
        //looking for percentages
        if(strpos($MAIN_WIDTH, "%") !== false )
            {
                $MAIN_WIDTH_DIMMENSION = "%";
                $MAIN_WIDTH = str_replace("%", "", $MAIN_WIDTH);
            }
        
        //100% == 950px, 50% == 950*50/100 = 475px
        if($MAIN_WIDTH_DIMMENSION == "%")
            {
                //$MAIN_WIDTH = 950*$MAIN_WIDTH/100;
                //$MAIN_WIDTH_DIMMENSION = "px";
            }
            
        //standart colls margin is 2% peprcents from the computed width
        $width_margin = 1*$MAIN_WIDTH/100;
        // -25 M.Kamenov correction 15.06.2014
        $width = round($MAIN_WIDTH/$colls - 6*$width_margin, 0, PHP_ROUND_HALF_DOWN);  //2*width
        
        if($colls == 1)
            {
                //$width = 100; 
                //$MAIN_WIDTH_DIMMENSION = "%";
                //$width_margin = 0;
            }
            
        $width_percents = (100*$width/$MAIN_WIDTH);
        //if($this->n == 11 || $this->get_pParent() == 11) $width = (int)(450/$colls - 4);
        $image_width = $width/2;
        
        $colls_counter = 1;
        $content = "<div class=\"sDelimeter\"></div>";

        //temporary subpage title
        $str_title = "";
        //temporary subpage image
        $str_img = "";
        //temporary subpage full text
        $str_text = "";
        //temporary subpage 1-st paragraph
        $str_paragraph = "";
        //more link text
        $str_more = "";
        //subpage item
        $str_subpage = "";
        //subpage links
        $str_subpage_links = "";
        //alternative text
        $str_alt = $page->Name;
        //link to the subpage
        $str_link = "";
        //get the previous page image width in px
        $str_previous_img_info = array("width"=>"180", "height"=>"180", "align"=>"");
        //page price content
        $str_price = "";
        
        //read and analyze subpages content
        //collect information about subpages images, subpages text
        //cycle works only in edit mode
        if(is_array($subpages))
            {                   
            
                for($i=0; $i<count($subpages); $i++)
                    {
                        if($subpages[$i]["image_allign"] == 2 || $subpages[$i]["image_allign"] == 10)
                            {
                                $str_img_align = "left";
                                $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                                $str_previous_img_info["align"] = $str_img_align;
                            }
                        
                        if($subpages[$i]["image_allign"] == 3 || $subpages[$i]["image_allign"] == 11)
                            {
                                $str_img_align = "right";
                                $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                                $str_previous_img_info["align"] = $str_img_align;
                            }
                            
                        if(file_exists($subpages[$i]["image_src"]) && is_file($subpages[$i]["image_src"]) && user::$uID>0)
                            {
                                $str_previous_img_info["width"] = $image_width;
                                $str_previous_img_info["height"] = $image_width;
                            }
                        
                    }
            }

        //read subapages
        if(is_array($subpages))
            {                   
            
            for($i=0; $i<count($subpages); $i++)
                {
                        
                    //break when no links shown
                    if($this->get_pInfo("show_link") == 0) break;       
                    
                    //page date
                    if(($this->n == $this->get_sInfo("news") || $this->get_pParent() == $this->get_sInfo("news"))  && $this->get_sInfo("news") != site::$sStartPage)
                        $str_date = "<div class=\"date\">".date("d.m.Y", strtotime($subpages[$i]['date_added']))."</div>";
                    else $str_date ="";
                    
                    $hide_news_dates = $this->get_sConfigValue("CMS_HIDE_NEWS_DATES");
                    if($hide_news_dates) $str_date = "";
                    
                    //default variables
                    //$str_link = "page.php?n=".$subpages[$i]["n"]."&amp;SiteID=$this->SiteID";
                    $str_link = $subpages[$i]['page_link']; 
                    //$str_link = $this->get_pLink($subpages[$i]['n']); 
                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$width."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\">";

                    $str_img_align = "default";
                    
                    //label from pages_to_groups table
                    $str_label = "<span class=\"label\">".$subpages[$i]["label"]."</span>";
                    
                    $str_title = $subpages[$i]["Name"] . $str_date;
                    //$str_txt = "<div class=\"text\">".$subpages[$i]["textStr"]."</div>";
                    $str_txt = $str_label.$subpages[$i]["textStr"]; 
                    //$str_paragraph = "<div class=\"text\">".strip_tags(crop_text($subpages[$i]["textStr"]), "<P>, <p>, <B>, <b>, <U>, <u>, <LI>, <li>, <strong>, <STRONG>, <span>, <i>, <em>, <br>, <BR>")."</div>";
                    $str_paragraph = strip_tags(crop_text($subpages[$i]["textStr"]), "<P>, <p>, <B>, <b>, <U>, <u>, <LI>, <li>, <ul>, <UL>, <ol>, <OL><strong>, <STRONG>, <span>, <i>, <em>, <br>, <BR>");
                    $str_paragraph = $this->replace_shortcodes($str_paragraph);
                    //page prices, product availability etc.
                    $p_prices = $this->get_pPrice($subpages[$i]["n"], "ASC");
                    $str_price = "";
                    $str_availability = "";
                    $str_cart_options = "";
                    if(!empty($p_prices[0]['code']) && $this->get_sConfigValue("SHOPPING_CART_ACTIVATE"))
                        {
                            $str_price = "<span class=\"sPage-price\">".$p_prices[0]['price_value']."</span>";
                            $qty = $this->qty_calc($p_prices[0]['code']);
                            $item = $this->info($p_prices[0]['code']);
                            if($item->itGroup == 5 and $qty > 0 and $this->_site["SitesID"] == 2){
                                $qty = $this->qty_calc($this->info($item->itParentID));
                            };

                            $str_availability = $this->getAvailability($qty, $p_prices[0], $item);

                            if(!($this->is_product($p_prices[0]['code'])))   // service not product
                                $str_availability = "<span class=\"product-call\" title=\"Call to order\"></span>";
                            $price_pcs_str = "";
                            if($p_prices[0]['pcs']>1)  {
                                $price_pcs_str = $p_prices[0]['pcs'].' x ';
                                $unit_price = round($p_prices[0]['price_value']/$p_prices[0]['pcs'],3); 
                                $str_price = "<span class=\"sPage-price\">".$unit_price."</span>";
                            }
                            //page options for shopping
                            $str_cart_options = "<div class=\"page-cart-options2\">".$str_availability.$price_pcs_str.$str_price.((SHOPPING_CART_VIEW_CURRENCY) ? $p_prices[$i]['currency_string'] : "")."</div>";
                        }
                    
                    //read subpages of the current subpage
                    $str_subpage_links = "";
                    $ss_array = $this->get_pSubpages($subpages[$i]["n"], null, 1, false, $limit);
                    if(count($ss_array[0]) > 0 )
                    {
                        $str_subpage_links = "<div class=\"ssLinks\">";
                        for($j = 0; $j<count($ss_array); $j++)
                            {
                                //$str_subpage_links.= "<a href=\"page.php?n=".$ss_array[$j]["n"]."&amp;SiteID=".$this->SiteID."\" title=\"".$ss_array[$j]['title']."\">".$ss_array[$j]["Name"]."</a>"; !!!                     
                                $str_subpage_links.= "<a href=\"".$ss_array[$j]["page_link"]."\" title=\"".$ss_array[$j]['title']."\">".$ss_array[$j]["Name"]."</a>";                       
                            }
                        $str_subpage_links.= "</div>";
                    }
                    else $str_subpage_links = "";
                    //get page more link
                    $str_more = "<a href=\"".$str_link."\" class=\"next_link\" title=\"".$this->get_pInfo("title", $subpages[$i]["n"])."\"  rel=\"nofollow\">".$this->get_sMoreText()."</a>";                   
                    
                    //deafult TEXT and PARAGRAPH content                
                    if(strlen(strip_tags($subpages[$i]["textStr"])) < 5 && user::$uID>0)
                        { 
                            $str_txt = "<div style=\"width: 100%; display: block; background-color: Red; color: #FFFFFF; text-align: center;\"><br><i>page content empty</i><br><br></div>";
                            #$str_paragraph = $this->replace_shortcodes($str_txt);
                        }
                                    
                    //reduce image width depending on page width: default image_width = 1/3*page_width 
                    if(!defined("CMS_IMG_WIDTH_REDUCE"))
                        define("CMS_IMG_WIDTH_REDUCE", 3);

                    //define image crop ratio
                    if(!defined("CMS_IMAGE_CROP_RATIO"))
                        define("CMS_IMAGE_CROP_RATIO", 4/3);    
                    
                    //define automatic image crop
                    if(!defined("CMS_IMAGE_CROP"))
                        define("CMS_IMAGE_CROP", false);
                                            
                    //get subpage image alignment
                    $sp_img_align = $subpages[$i]["image_allign"];                      
                    
                    if($sp_img_align == 2 || $sp_img_align == 10)
                        {
                            $str_img_align = "left";
                            $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                            //$str_previous_img_info["align"] = $str_img_align;
                        }
                    else
                    if($sp_img_align == 3 || $sp_img_align == 11)
                        {
                            $str_img_align = "right";
                            $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                            //$str_previous_img_info["align"] = $str_img_align;
                        }
                    else
                        {
                            $image_width = $width-2*$width_margin;
                            //$str_img_align = "default";
                        }
                        
                    
                    
                    //do not reduce image variables if shown: only image, image + title or suppage text is empty (and subpage links are hidden)
                    if($this->get_pInfo("show_link") == 3 ||  $this->get_pInfo("show_link") == 9 || (strlen($subpages[$i]["textStr"]) <= 3 && $this->_page["show_link"] != 13)) 
                        $image_width = $width-2*$width_margin;
                    
                    //get real img width and reduce if necessary
                    $imgInfo = GetImageSize($subpages[$i]["image_src"]); 
                    //if($imgInfo[0] < $image_width) $image_width = $image_width/2;
                    if($imgInfo[0] < $image_width) $image_width = $imgInfo[0];

                    
                    if($subpages[$i]["image_allign"] == 2 || $subpages[$i]["image_allign"] == 10)
                        $align_class = "align-left";
            
                    if($subpages[$i]["image_allign"] == 3 || $subpages[$i]["image_allign"] == 11)
                        $align_class = "align-right";       
                    
                    if($this->_page['show_link'] == 3)
                        {
                            $align_class = "align-center";  
                            $str_img_align  = "";
                        }
                                        
                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                    
                    //mpetrov correction: 22.10.2014
                    if($MAIN_WIDTH_DIMMENSION == "%")
                        {
                            
                            $image_width = $MAIN_WIDTH;
                            $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/".$subpages[$i]["image_src"]."\" width=\"".$image_width.$MAIN_WIDTH_DIMMENSION."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                        }
                        
                    if($sp_img_align == 1)
                        $str_img.= "";
                        
                    if($sp_img_align == 9 || $sp_img_align == 12)
                        $str_img = "<center>".$str_img."</center>";
                        
                    if($this->get_pInfo("make_links") != 0)
                        {
                            //$str_title = "<a href=\"".$str_link."\" title=\"".$subpages[$i]['title']."\" class=\"title\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; margin: ".$width_margin."px; \"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\">";
                            $str_title = "<a href=\"".$str_link."\" title=\"".$subpages[$i]['title']."\" class=\"title\"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\" class=\"br-style\">";
                        }
                    else    
                            //$str_title = "<a href=\"#\" title=\"".$subpages[$i]['title']."\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; margin: ".$width_margin."px; \"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\">";
                            $str_title = "<a href=\"#\" title=\"".$subpages[$i]['title']."\" class=\"title\"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\" class=\"br-style\">";
                    
                    if($this->get_pInfo("make_links") == 1)
                        { 
                            
                            
                            $style = "";
                            if(CMS_IMAGE_CROP && $this->_page['SiteID'] == $subpages[$i]["SiteID"])
                                {
                                    $style = "display: block; width: ".$image_width."px; float: ".$str_img_align."; height: ".($image_width/CMS_IMAGE_CROP_RATIO)."px; overflow: hidden;";
                                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."&amp;ratio=exact\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                                }
                            
                            $str_img = "<a href=\"".$str_link."\" class=\"img ".$align_class."\" style=\"$style\" title=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\">".$str_img."</a>";
                            
                            $str_txt = "<a href=\"".$str_link."\" title=\"".$subpages[$i]["title"]."\">".$str_txt."</a>";
                            $str_paragraph = "<a href=\"".$str_link."\" >".$this->replace_shortcodes($str_paragraph)."</a>";
                        }
                        
                    //photo gallery
                    if($this->get_pInfo("make_links") == 4)
                        {
                                                        
                            $style = "";
                            if(CMS_IMAGE_CROP && $this->_page['SiteID'] == $subpages[$i]["SiteID"])
                                {
                                    $style = "display: inline-block; width: ".$image_width."px; height: ".($image_width/CMS_IMAGE_CROP_RATIO)."px; overflow: hidden;";
                                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."&amp;ratio=exact\" align=\"".$str_img_align."\"  class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"],ENT_QUOTES)."\" border=\"0\">";
                                }
                            $str_img = "<a href=\"".$subpages[$i]["image_src"]."\" 
                                            data-fresco-caption=\"".$this->get_pInfo("imageName", $subpages[$i]['n'])."\" 
                                            data-fresco-group=\"gallery\" 
                                            data-fresco-group-options=\"overflow: true, thumbnails: 'vertical', onClick: 'close', ui: 'outside'\"
                                            title=\"".$subpages[$i]["Name"] . " / ".$subpages[$i]["imageName"]."\" 
                                            class=\"fresco\" style=\"$style\">".$str_img.
                                        "</a>";

                            //if($this->get_pSubpagesCount($subpages[$i]['n'])==0)
                                                
                            //else
                                //$str_img = "<a href=\"".$this->get_pLink($subpages[$i]["n"])."\" title=\"".$subpages[$i]["Name"] . " / ".$subpages[$i]["imageName"]."\">".$str_img."</a>";                
                        }
                    //no photo file
                    if(!file_exists($subpages[$i]["image_src"]) && !is_file($subpages[$i]["image_src"]))
                        if(user::$uID>0)
                        {
                            //$str_img = "<table style=\"display: block; width: ".$str_previous_img_info["width"]."px; height: ".$str_previous_img_info["height"]."px; border: 1px solid #e7e7e7; text-align: center\" align=\"".$str_previous_img_info["align"]."\"><tr><td valign=\"middle\">no picture</table>";
                            $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=web/admin/images/no_image.jpg&amp;img_width=".$width."\" align=\"".$str_img_align."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\"  style=\"border: 1px solid #ebebeb\">";

                            //mpetrov correction: 22.10.2014
                            if($MAIN_WIDTH_DIMMENSION == "%")
                                $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/web/admin/images/no_image.jpg\" width=\"".$image_width.$MAIN_WIDTH_DIMMENSION."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" style=\"border: 1px solid #ebebeb\">";
                        
                        }
                        else 
                            $str_img = "";              
                    else 
                        {
                            
                            //$str_previous_img_info["width"] = $image_width;
                            //$str_previous_img_info["height"] = $image_width;

                        }
                    
                    if( ($this->_site['site_status']>0) && ($this->_page['SecLevel']==0) ) {
                        //$str_paragraph .= $this->get_top_word($str_paragraph); 
    
                    //if($this->n == 100306)  $str_paragraph = $str_paragraph."<p>***".$this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph)."***</p>"; 
                        $str_txt = $str_txt."&nbsp;".$this->get_seo_etiketi_link($this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph.' '.$str_txt.' '.$subpages[$i]['tags']))."&nbsp"; 
                        $str_paragraph = $str_paragraph."&nbsp;".$this->get_seo_etiketi_link($this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph))."&nbsp";
                        $str_paragraph = $this->replace_shortcodes($str_paragraph);
                        // $str_paragraph .= ' ***  '; 
                    }
                    
                    // MPETROV correction - 22.10.2014
                    
                    $col_class = "";
                    if(is_array($cms_args['CSS_FRAMEWORK']))
                        {
                            $framework = $cms_args['CSS_FRAMEWORK'];
                            if($framework['name'] == "bootstrap3")
                                {
                                    $col_class = " col-xs-".(int)($framework['max_cols']/$colls);   
                                }
                            if($framework['name'] == "bootstrap2")
                                {
                                    $col_class = " col-".(int)($framework['max_cols']/$colls);  
                                }                           
                        }   
                        
                    /*
                    switch($align)
                        {
                            case 1: 
                            case 2:
                            case 3:
                            case 9: { $content.=  $img . $this->get_pText($n);  break; }
                            case 10: 
                            case 11:
                            case 12: { $content.= $this->get_pText($n) . $img; break; }
                            default: $content.= $img . $this->get_pText($n);
                        }
                    */
                    //CONSTRUCT SUBPAGE CONTENT
                    
                    // 1- add links
                    //if($this->get_pInfo("show_link") == 1)
                    if($this->_page['show_link'] == 1)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title ."</div></div>";
                    
                    
                    // 2-   title + picture + text
                    if($this->_page['show_link'] == 2)
                        if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">". $str_title . "<div class=\"text\">".$str_txt ."</div>". $str_img ."</div>".$str_cart_options."</div>";
                        else
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">". $str_title . "<div class=\"text\">". $str_img . $str_txt ."</div></div>".$str_cart_options."</div>";
                    
                    // 3-   title + picture 
                    if($this->_page['show_link'] == 3)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."; margin: ".$width_margin."px;\"><div class=\"sPage-content\">" . $str_title . $str_img . "</div>".$str_cart_options."</div>";
                    
                    // 4-   title + picture + paragraph
                    if($this->_page['show_link'] == 4)
                        if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_paragraph ."</div>". $str_img . $str_cart_options . $str_more ."</div></div>";
                        else
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_img . $str_cart_options . $str_paragraph ."</div>". $str_more ."</div></div>";
                        
                    // 5-   picture + text
                    if($this->_page['show_link'] == 5)
                        if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_txt .  $str_img . "</div>".$str_cart_options."</div>";
                        else
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_img . $str_txt . "</div>".$str_cart_options."</div>";
                        
                    // 6-   text
                    if($this->_page['show_link'] == 6)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_txt . "</div></div>"; 
                    

                    // 7-   picture + paragraph
                    if($this->_page['show_link'] == 7)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">". $str_img . $str_cart_options . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more ."</div></div>";
                    
                    // 8-   paragraph
                    if($this->_page['show_link'] == 8)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more."</div></div>";
                    
                    // 9-   picture
                    if($this->_page['show_link'] == 9)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content border_image\" style=\"margin: ".$width_margin."px; \">" . $str_img . "</div>".$str_cart_options."</div>";
                        
                    // 10-      title+text
                    if($this->_page['show_link'] == 10)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">".$str_txt . "</div></div></div>";

                    // correction M Kamenov 2.06.2014 padding-bottom   MUST be set as $MAIN_PADDING_BOTTOM
                    //11-   title+ paragraph 
                    if($this->_page['show_link']  == 11)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."; padding-bottom:20px \"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more ."</div></div>";


                    //13-   title + picture + subpage links 
                    if($this->_page['show_link'] == 13)
                        $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_title . $str_img . $str_subpage_links . "</div>";

                    //14-   picture + subpage links 
                    if($this->_page['show_link']  == 14)
                        $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_img . $str_subpage_links . "</div>";

                    //15-   title+ subpage links 
                    if($this->_page['show_link']  == 15)
                        $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_title . $str_subpage_links . "</div>";


                    //end of the current row, new row
                    if(  ($colls_counter >= $colls) || ($i==count($subpages)-1)  )
                        {
                            $content.= "<div style=\"clear: both\" class=\"sPage-separator\"></div>";
                            //$content.= "<div class=\"sPage-separator\"></div>";
                            // Martin Kamenov 06.08.2014
                            $colls_counter = 0;
                        }
                    $colls_counter++;
                    
                }
                
                $content.= "<div class=\"sDelimeter\"></div>";          
                if(site::$uReadLevel >= $this->get_pInfo("SecLevel"))
                    if($cms_args['CSS_FRAMEWORK'] == "bootstrap3" || $cms_args['CSS_FRAMEWORK'] == "bootstrap2")
                        echo "<div class=\"row\" id=\"pageSubcontent\">".$content."</div>";
                    else
                        echo "<div class=\"row\">".$content."</div>";
                
                //CMS ADMIN BUTTONS
                //if($this->get_pInfo("show_link") != 0 && count($this->get_pSubpages()) > 0)
                    //$this->admin_buttons();
        }
        
    }
?>