<?php
$copyright_link = $o_page->get_pLink($o_page->_site['copyright_n']);
define("TERMS_LINK", $copyright_link);

$custom_js = array();

function confkey ($constant, $value='', $print=true) {
  if(!defined($constant)) {
    define($constant, $value);
  }
  if($print) { 
    echo constant($constant); 
    return; 
  }
  return constant($constant);
  
}


	//define some default values
	$tmpl_config = array(
		"default_banner" => ASSETS_DIR."assets/images/default-slider.jpg",
		"default_logo" => ASSETS_DIR."assets/images/default-logo.png",
		"background_image" =>($o_site->get_sConfigValue('TMPL_BODY_BACKGROUND')),
		"default_logo_width" =>($o_site->get_sConfigValue('TMPL_LOGO_SIZE'))? $o_site->get_sConfigValue('TMPL_LOGO_SIZE') : "180",
	);
	    if(!empty($o_site->_site['Logo']))
        $tmpl_config['default_logo'] = $o_site->get_sImageDIr()."/".basename($o_site->_site['Logo']);

    //overwrite column_right
    if(isset($column_right))
        $tmpl_config['column_right'] = $column_right;

    //overwrite column_right
    if(isset($column_left))
        $tmpl_config['column_left'] = $column_left;

?>
