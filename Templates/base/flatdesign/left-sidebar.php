<aside id="left-sidebar" class="left-sidebar one-fourth sidebar left widget-area" role="complementary">
	<ul>
		<?php 
			if(is_array($tmpl_config['column_left']))
				{
					for($icr=0; $icr<count($tmpl_config['column_left']); $icr++)
						include $tmpl_config['column_left'][$icr];
				}
			else
				include $tmpl_config['column_left'];
		?>
	</ul>
</aside>