<style>
	.spacecols { 
		margin-top:45px;
	}
	.text-we {
		font-size:17px;
		font-weight:500;
	}
	.width-we {
		width: 70px;
		height:auto;
	}
	.counter .stat-count {
		font-size: 40px;
    	font-weight: 500;
		margin-bottom:15px;
	}
	.counter {
		color:#fff;
		font-size: 18px;
	}
	.bg-second-color.bg-darker {
    	background-color: #004a66;
	}
	.main {
		padding:0;
	}
	.img-mas {
		padding:0;
	}
</style>
<div class="wrap">
	<div class="row">
		<?php
		include TEMPLATE_DIR . "content.php";
		?>
	</div>
</div>

<div class="widget-area clearfix">
	<div class="container">
	<h3>��� �� Espace.bg ����������� �� ������ �������</h3>
	<hr>
	<div class="spacecols"></div>
	<div class="row">
		<div class="col-md-6">
		  <div class="row">
			<div class="col-md-3">
				<img src="web/images/upload/1041/verification-of-delivery-list-clipboard-symbol.png" alt="������ ��������� �����" class="img-responsive width-we" />
			</div>
			<div class="col-md-6 text-we"> 
				������ ��������� �� PP & PVC ����� � ������ ��������!
			</div>
		  </div>
		</div>
		<div class="col-md-6">
		<div class="row">
			<div class="col-md-3">
				<img src="web/images/upload/1041/delivery-truck.png" alt="��������� ��������" class="img-responsive width-we" /> 
			</div>
			<div class="col-md-6 text-we"> 	
				����� � ��������� ��������� � ������� �� ��������
			</div>
			</div>
		</div>
	</div>
	<div class="spacecols"></div>
	<div class="row">
		<div class="col-md-6">
		  <div class="row">
			<div class="col-md-3">
				<img src="web/images/upload/1041/certification-award.png" alt="��� �������������" class="img-responsive width-we" />
			</div>
			<div class="col-md-6 text-we"> 
				���������� �� ��������� ������������ �� ��������� ��� EN 1401-1:2009 /  ��� EN 1329-1:2014
			</div>
		  </div>
		</div>
		<div class="col-md-6">
		<div class="row">
			<div class="col-md-3">
				<img src="web/images/upload/1041/24-years.png" alt="24 ������ Espace.bg" class="img-responsive width-we" />
			</div>
			<div class="col-md-6 text-we"> 	
				������ ������ ����������� ��������� � ���������� � ��������� �����
			</div>
			</div>
		</div>
	</div>
	<div class="spacecols"></div>
	</div>
	
<div id="block-counter" class="bg-second-color bg-darker block" style="padding:40px 0">

	<div class="container">
		<div class="row">
		
			<div class="col-md-4">
				<div class="counter">
					<div class="stat-count"><b class="timer" id="years" data-to="24" data-speed="2000"></b>+</div>
					<div class="counter-info">
						<div class="counter-title">������ ����</div>
						<div class="counter-details"></div>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="counter">
					<div class="stat-count"><b class="timer" id="modelspvc" data-to="38" data-speed="1900"></b>+</div>
					<div class="counter-info">
						<div class="counter-title">������ PVC & PP �����</div>
						<div class="counter-details"></div>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="counter">
					<div class="stat-count"><b class="timer" id="partners" data-to="1700" data-speed="1800"></b>+</div>
					<div class="counter-info">
						<div class="counter-title">��������� � EU</div>
						<div class="counter-details"></div>
					</div>
				</div>
			</div>

		</div>
	</div>

</div>

<div class="masonry-3-images">
<div class="row">
<div class="col-md-4 img-mas">
<img src="/web/images/upload/1041/slider2.jpg" alt="" class="img-responsive" />
</div>
<div class="col-md-4 img-mas">
<img src="/web/images/upload/1041/slider2.jpg" alt="" class="img-responsive" />
</div>
<div class="col-md-4 img-mas">
<img src="/web/images/upload/1041/slider2.jpg" alt="" class="img-responsive" />
</div>
</div>
</div>

</div>

</div>


<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery.countTo.js'></script>
<script type="text/javascript">
    jQuery(function ($) {

      // start all the timers https://github.com/mhuggins/jquery-countTo
      $('.timer').each(count);
		
      function count(options) {
        var $this = $(this);
        options = $.extend({}, options || {}, $this.data('countToOptions') || {});
        $this.countTo(options);
      }
    });
  </script>