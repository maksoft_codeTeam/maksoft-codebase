	<div class="advanced-search color" id="booking">
		<div class="wrap">
			<form role="form" action="#" method="get">
				<!-- Row -->
				<div class="f-row">
					<div class="form-group datepicker one-third">
						<label for="departure-date">���� � ��� �� ��������</label>
						<input type="text" class="departure-date hasDatepicker" id="departure-date">
						<input type="hidden" name="dep" id="dep" value="2017-03-30 14:23" style="cursor: pointer;">
						<script>
							window.datepickerDepartureDateValue = '2017-03-30 11:23';
						</script>
					</div>
					<div class="form-group select one-third">
						<label>�������� ��</label>
						<div class="selector" id="uniform-pickup1" style="width: 385px;"><span style="width: 383px; user-select: none;">�������� ����� �� ��������</span>
							<select id="pickup1" name="p1">
								<option value="">�������� ����� �� ��������</option>
								<optgroup label="������� 1">
									<option value="1">���� 1</option>
									<option value="2">���� 2</option>
									<option value="3">���� 3</option>
								</optgroup>
								<optgroup label="������� 2">
									<option value="4">���� 4</option>
									<option value="5">���� 5</option>
									<option value="6">���� 6</option>
								</optgroup>
							</select>
						</div>
					</div>
					<div class="form-group select one-third">
						<label>���������� �</label>
						<div class="selector" id="uniform-dropoff1" style="width: 415px;"><span style="width: 413px; user-select: none;">�������� ����� �� ����������</span>
							<select id="dropoff1" name="d1">
								<option value="">�������� ����� �� ����������</option>
								<optgroup label="������� 1">
									<option value="1">���� 1</option>
									<option value="2">���� 2</option>
									<option value="3">���� 3</option>
								</optgroup>
								<optgroup label="������� 2">
									<option value="4">���� 4</option>
									<option value="5">���� 5</option>
									<option value="6">���� 6</option>
								</optgroup>
							</select>
						</div>
					</div>
				</div>
				<!-- //Row -->
				<!-- Row -->
				<div class="f-row" style="display: none;">
					<div class="form-group datepicker one-third">
						<label for="return-date">���� � ��� �� �������</label>
						<input type="text" class="return-date hasDatepicker" id="return-date" disabled="">
						<input type="hidden" name="ret" id="ret" disabled="" value="" style="cursor: pointer;">
					</div>
					<div class="form-group select one-third">
						<label>�������� ��</label>
						<div class="selector" id="uniform-pickup2" style="width: 415px;"><span style="width: 413px; user-select: none;">�������� ����� �� ��������</span>
							<select id="pickup2" name="p2">
								<option value="">�������� ����� �� ��������</option>
								<optgroup label="������� 1">
									<option value="1">���� 1</option>
									<option value="2">���� 2</option>
									<option value="3">���� 3</option>
								</optgroup>
								<optgroup label="������� 2">
									<option value="4">���� 4</option>
									<option value="5">���� 5</option>
									<option value="6">���� 6</option>
								</optgroup>
							</select>
						</div>
					</div>
					<div class="form-group select one-third">
						<label>Drop off location</label>
						<div class="selector" id="uniform-dropoff2" style="width: 415px;"><span style="width: 413px; user-select: none;">Select drop-off location</span>
							<select id="dropoff2" name="d2">
								<option value="">Select drop-off location</option>
								<optgroup label="������� 1">
									<option value="1">���� 1</option>
									<option value="2">���� 2</option>
									<option value="3">���� 3</option>
								</optgroup>
								<optgroup label="������� 2">
									<option value="4">���� 4</option>
									<option value="5">���� 5</option>
									<option value="6">���� 6</option>
								</optgroup>
							</select>
						</div>
					</div>
				</div>
				<!-- Row -->
				<div class="f-row">
					<div class="form-group spinner">
						<label for="people">���� ������� <small>(����������� ����)</small>?</label>
						<input type="number" id="people" name="ppl" min="1" class="uniform-input number" value="">
					</div>
					<div class="form-group radios">
						<div>
							<div class="radio" id="uniform-return">
								<span>
									<div class="radio" id="uniform-return"><span><input type="radio" name="trip" id="return" value="2"></span>
									</div>
								</span>
							</div>
							<label for="return">����������</label>
						</div>
						<div>
							<div class="radio" id="uniform-oneway">
								<span class="checked">
									<div class="radio" id="uniform-oneway"><span class="checked"><input type="radio" name="trip" id="oneway" value="1" checked=""></span>
									</div>
								</span>
							</div>
							<label for="oneway">�����������</label>
						</div>
					</div>
					<div class="form-group right">
						<button type="submit" class="btn large black">������� �� ����������</button>
					</div>
				</div>
				<!--// Row -->
			</form>
		</div>
	</div>