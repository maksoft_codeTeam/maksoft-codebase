<!DOCTYPE html>
<html lang="<?php echo($Site->language_key); ?>">

<head>
	<meta http-equiv="content-type" content="text/html;charset=windows-1251" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title><?=$Title?></title>
	<?php
	require_once "lib/lib_page.php";
	require_once "lib/Database.class.php";
	//include meta tags
	include( "Templates/meta_tags.php" );
	$o_site->print_sConfigurations();

	$db = new Database();
	$o_page->setDatabase( $db );
	$o_user = new user( $user->username, $user->pass );
	?>

		<script type="text/javascript">
		window.themePath = '<?=TEMPLATE_DIR?>';
		window.siteUrl = '<?=$o_page->_site['primary_url']?>';
	
		window.currentUserId = 0;
		window.currentUserLogin = null;
		window.currentLanguage = 'en';
				
		window.datepickerDateFormat = "dd/mm/yy";
				window.datepickerAltFormat = "yy-mm-dd";
	</script>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<link rel='stylesheet' href='<?=ASSETS_DIR?>assets/css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' href='<?=ASSETS_DIR?>assets/css/jquery-ui.theme.min.css' type='text/css' media='all' />
	<link rel='stylesheet' href='<?=ASSETS_DIR?>assets/css/color/theme-yellow.css' type='text/css' media='all' />
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?=ASSETS_DIR?>assets/js/html5shiv.min.js"></script>
    <script src="<?=ASSETS_DIR?>assets/js/respond.min.js"></script>
    <![endif]-->
    
<script src="https://use.typekit.net/okc6biy.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

</head>  
<body class="destination-template-default single single-destination">
	
	<!-- Preloader -->
	<div class="preloader">
		<div id="followingBallsG">
			<div id="followingBallsG_1" class="followingBallsG"></div>
			<div id="followingBallsG_2" class="followingBallsG"></div>
			<div id="followingBallsG_3" class="followingBallsG"></div>
			<div id="followingBallsG_4" class="followingBallsG"></div>
		</div>
	</div>
	<!-- //Preloader -->
    
	    <!-- �����-->
<header class="header" role="banner">
	<div class="wrap">
	
		<!-- ���� -->
		<div class="logo">
			<a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>" title="<?=$o_site->_site['Title']?>"><img src="<?=ASSETS_DIR?>assets/images/logo-grouptravel.png" alt="<?=$o_site->_site['Title']?>" /></a>
		</div>
	    <!-- //���� -->
		
		<!--������� ���������-->
		<nav id="nav" class="main-nav">

			<ul id="menu-primary" class="">

				<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>">������</a>
					<?php

					if ( !isset( $primary_links ) )$primary_links = 3;
					$primary_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $primary_links" );

					for ( $i = 0; $i < count( $primary_links ); $i++ ) {
						$subpages = $o_page->get_pSubpages( $primary_links[ $i ][ 'n' ] );
						?>
					<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
						<a href="<?php echo $o_page->get_pLink($primary_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <?php echo $primary_links[$i]['Name'] ?></a>
						<?php echo ((count($subpages) > 0) ? "<ul class='sub-menu'>" : "</li>") ?>
						<?php
						if ( count( $subpages ) > 0 ) {


							for ( $j = 0; $j < count( $subpages ); $j++ )
								echo "<li class='menu-item menu-item-type-post_type menu-item-object-destination'><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\">" . $subpages[ $j ][ 'Name' ] . "</a></li>";
							?>
			</ul>
			</li>
			<?php 
	} 
}
?>
			</ul>
		</nav>
		<!--//������� ���������-->
	</div>
</header>
	<!-- //����� -->