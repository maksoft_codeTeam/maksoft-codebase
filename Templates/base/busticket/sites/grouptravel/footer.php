<!-- ����� -->
<footer class="footer black" role="contentinfo">
	<div class="wrap">
		<div id="footer-sidebar" class="footer-sidebar widget-area clearfix row" role="complementary">
			<ul>
				<li class="widget widget-sidebar transfers_about_widget">
					<article class="about_widget clearfix one-half">
						<h6><?=$o_site->_site['Title']?></h6>
						<p><?=$o_site->_site['Description']?></p>
					</article>
				</li>
				<li class="widget widget-sidebar transfers_contact_widget">
					<article class="transfers_contact_widget one-fourth">
						<h6>����� ����� �� �����?</h6>
						<p>�������� �� � ���:</p>       
						<?php if($o_page->_site['SPhone']) { ?>
						<p class="contact-data">
							<i class="fa fa-phone icon" aria-hidden="true"></i> <a href="tel:<?php echo $o_page->_site['SPhone'];?>"><?php echo $o_page->_site['SPhone'];?></a>
						</p>
						<?php }?>
                                
						<p class="contact-data">
							<i class="fa fa-envelope icon" aria-hidden="true"></i> <a href="mailto:<?php echo $o_page->_site['EMail'];?>"><?php echo $o_page->_site['EMail'];?></a>
						</p>
						
					</article>
				</li>
				<li class="widget widget-sidebar transfers_social_widget">
					<article class="one-fourth">
						<h6>��������� ��</h6>
						<ul class="social">
							    <!-- ������� -->
								<?php if(defined('CMS_FACEBOOK_PAGE_LINK') || $user->AccessLevel >0) { ?> <li><a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" target="_blank"><span class="fa fa-fw fa-facebook"></span></a></li> <?php }?>
                                
                                <!-- ������� -->
                                <?php if(defined('CMS_GPLUS_PAGE_LINK') || $user->AccessLevel >0) { ?> <li><a href="<?=(defined("CMS_GPLUS_PAGE_LINK")) ? CMS_GPLUS_PAGE_LINK:"#"?>" target="_blank"><span class="fa fa-fw fa-google-plus"></a></li> <?php }?>
                                
                                <!-- RSS -->
                                <li class="rss"><a href="/rss.php" class="fa fa-rss"></a></li>
							
						</ul>
					</article>
				</li>
			</ul>
		</div>
		<!-- #secondary -->
		<div class="copy">
			<p>� <?=$o_site->_site['Title']?> | ��� ������ � ��������� <a href="http://www.maksoft.net" title="�������">�������</a></p>
			<!--����� ��������� (�� ��� �� ����������: ����� ������) -->
			<nav class="foot-nav">
				<ul id="menu-footer" class="">
						<?php

						if ( !isset( $footer_links ) )$footer_links = 5;
						$footer_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $footer_links" );

						for ( $i = 0; $i < count( $footer_links ); $i++ ) {
							$subpages = $o_page->get_pSubpages( $footer_links[ $i ][ 'n' ] );
							?>
						<li>
							<a href="<?php echo $o_page->get_pLink($footer_links[$i]['n']) ?>">
								<?php echo $o_page->get_pName($footer_links[$i]['n']) ?>
							</a>
						</li>

						<?php  
						}
						?>
				</ul>
			</nav>
			<!--//����� ���������-->
		</div>
	</div>
</footer>
<!-- //����� -->

<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/core.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/widget.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/mouse.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/slider.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/button.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/datepicker.min.js'></script>

<script type='text/javascript'>
	jQuery( document ).ready( function ( jQuery ) {
		jQuery.datepicker.setDefaults( {
			"closeText": "Close",
			"currentText": "Today",
			"monthNames": [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
			"monthNamesShort": [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
			"nextText": "Next",
			"prevText": "Previous",
			"dayNames": [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
			"dayNamesShort": [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
			"dayNamesMin": [ "S", "M", "T", "W", "T", "F", "S" ],
			"dateFormat": "dd\/mm\/yy",
			"firstDay": 1,
			"isRTL": false
		} );
	} );
</script>

<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/ticketform/jquery.validate.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/ticketform/jquery-ui-sliderAccess.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/ticketform/jquery-ui-timepicker-addon.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/ticketform/search.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery/ui/effect.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery.uniform.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/respond.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/jquery.slicknav.min.js'></script>
<script type='text/javascript' src='<?=ASSETS_DIR?>assets/js/scripts.js'></script>

<?php include( "Templates/footer_inc.php" ); ?>

</body> 
</html>