<?php
require_once __DIR__ . '/../../../modules/vendor/autoload.php';
require_once __DIR__ . '/../../../global/recaptcha/init.php';
date_default_timezone_set( 'Europe/Sofia' );
$search_tag = '';

$search = '';

if(isset($_GET['search_tag'])){

    $search_tag = $_GET['search_tag'];
}

if(isset($_GET['search'])){
    $search = $_GET['search'];
}

define( "TEMPLATE_NAME", "busticket" );
define( "TEMPLATE_DIR", "Templates/base/" . TEMPLATE_NAME . "/" );
define( "TEMPLATE_IMAGES", "http://www.maksoft.net/" . TEMPLATE_DIR . "images/" );
define( "ASSETS_DIR", "/" . TEMPLATE_DIR . "" );

    //show what options are available for config
/*  $tmpl_config_support = array(
        "change_theme"=>true,                   // full theme support
        "change_layout"=>true,                  // layout support
        "change_banner"=>true,              // banner support
        "change_css"=>true,                     // css support
        "change_background"=>true,      // background support
        "change_logo"=>true                     // logo support
    );*/
    //define some default values
    $tmpl_config = array(
        "default_banner" => ASSETS_DIR."banner.jpg",
        "default_logo" => ASSETS_DIR."assets/images/default-logo.png",
    );

include TEMPLATE_DIR . "header.php";
$pTemplate = $o_page->get_pTemplate( $n, 296 );


switch(True){
    case $o_page->_page[ 'SiteID' ] != $o_site->_site[ 'SitesID' ]:
        include TEMPLATE_DIR . "admin.php";
        break; 
    case ($o_page->_page[ 'n' ] == $o_site->_site[ 'StartPage' ] && (!isset($_GET['search_tag']) and !isset($_GET['search']))):
        include TEMPLATE_DIR . "home.php";
        break;
    default:
        echo '<main class="main" role="main">';
        include TEMPLATE_DIR . "main-header.php";
        if ( $pTemplate[ 'pt_url' ] )
            include $pTemplate[ 'pt_url' ];
        else
            include TEMPLATE_DIR . "main.php";
        echo '</main>';
}

include TEMPLATE_DIR . "footer.php";

?>
