<?php
require_once __DIR__ . '/../../../modules/vendor/autoload.php';
require_once __DIR__ . '/../../../global/recaptcha/init.php';
date_default_timezone_set( 'Europe/Sofia' );
$search_tag = '';

$search = '';

if(isset($_GET['search_tag'])){

    $search_tag = $_GET['search_tag'];
}

if(isset($_GET['search'])){
    $search = $_GET['search'];
}

define( "TEMPLATE_NAME", "busticket" );
define( "TEMPLATE_DIR", "Templates/base/" . TEMPLATE_NAME . "/" );
define( "TEMPLATE_IMAGES", "http://www.maksoft.net/" . TEMPLATE_DIR . "images/" );
define( "ASSETS_DIR", "/" . TEMPLATE_DIR . "" );

	//show what options are available for config
	$tmpl_config_support = array(
		"change_theme"=>true,					// full theme support
		"change_layout"=>true,					// layout support
		"change_banner"=>true,				// banner support
		"change_css"=>true,						// css support
		"change_background"=>true,		// background support
		"change_logo"=>true						// logo support
	);
	//define some default values
	$tmpl_config = array(
		"default_banner" => ASSETS_DIR."banner.jpg",
		"default_logo" => ASSETS_DIR."assets/images/default-logo.png",
	);

?>