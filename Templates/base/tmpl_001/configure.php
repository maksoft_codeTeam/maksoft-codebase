<?php
    //template base/tmpl_001 configurations
    
    define("TEMPLATE_NAME", "tmpl_001");
	define("ASSETS_DIR", "/Templates/base/".TEMPLATE_NAME."/");
    define("TEMPLATE_DIR", "Templates/base/".TEMPLATE_NAME."/");
    define("TEMPLATE_IMAGES", "/".TEMPLATE_DIR."images/");

    //show what options are available for config
    $tmpl_config_support = array(
        "change_theme"=>true,                   // full theme support
        "change_layout"=>true,                  // layout support
        "change_banner"=>true,              // banner support
        "change_css"=>true,                     // css support
        "change_background"=>true,      // background support
        "change_fontsize"=>true,                // fontsize support
        "change_max_width"=>true,           // site width support
        "change_menu_width"=>false,     // site menu width support
        "change_main_width"=>true,      // page content width support
        "change_logo"=>true,                        // logo support
        "column_left" => true,                      // column left support
        "column_right" => true,                 // column right support
        "drop_down_menu"=> true         // drop-down menu support
    );
    //define some default values
    $tmpl_config = array(
        "default_max_width"=> ($o_site->get_sConfigValue('CMS_MAX_WIDTH'))? $o_site->get_sConfigValue('CMS_MAX_WIDTH') : "960",
        "default_main_width"=>($o_site->get_sConfigValue('CMS_MAIN_WIDTH'))? $o_site->get_sConfigValue('CMS_�AIN_WIDTH') : "500",
        "default_menu_width"=>($o_site->get_sConfigValue('CMS_MENU_WIDTH'))? $o_site->get_sConfigValue('CMS_�ENU_WIDTH') : "180",
        "default_banner_width" => ($o_site->get_sConfigValue('CMS_MAX_WIDTH'))? $o_site->get_sConfigValue('CMS_MAX_WIDTH') : "960",
        "default_banner" => ASSETS_DIR."images/banner.jpg",
        "default_logo" => ASSETS_DIR."images/logo.png",
        "default_logo_width" =>($o_site->get_sConfigValue('TMPL_LOGO_SIZE'))? $o_site->get_sConfigValue('TMPL_LOGO_SIZE') : "180",
        "menu_drop_down" => TEMPLATE_DIR . "menu_drop_down.php",
        "column_left" => array(
                                TEMPLATE_DIR . "menu.php", 
                                /*TEMPLATE_DIR . "login.php",*/ 
                                TEMPLATE_DIR . "social_links.php"
                                ),
        "column_right" => array(
                                TEMPLATE_DIR . "boxes/second_links.php",
                                TEMPLATE_DIR . "boxes/news.php",
                                TEMPLATE_DIR . "boxes/last_visited.php", 
                                TEMPLATE_DIR . "boxes/shopping_cart.php", 
                                TEMPLATE_DIR . "boxes/banners.php"
                                ),
        "footer" => array(
                                TEMPLATE_DIR . "boxes/news.php"
                            ),      
        "date_format"=> "d-m-Y"
    );
    
    //overwrite column_right
    if(isset($column_right))
        $tmpl_config['column_right']     = $column_right;

    if(!empty($o_site->_site['Logo']))
        $tmpl_config['default_logo'] = $o_site->get_sImageDIr()."/".basename($o_site->_site['Logo']);

    // LANGUAGE TEXTS
    $tmpl_config['append_to_body'] = '';

    $footer_top_pages = array("","���-���������� ��������", "Top Pages", "", "Pages les plus visit&#233;es");
    $footer_new_pages = array("","���� � �����", "New", "", "Nouveaut&#233;s dans  web site");
    $footer_contacts = array("","��������", "Contacts", "", "Contact");
    $footer_links = array("","������", "Links", "", "Links");
    $footer_address = array("","�����", "Address", "", "Address");
    $footer_phone = array("","�������", "Phone", "", "Phone");

?>
