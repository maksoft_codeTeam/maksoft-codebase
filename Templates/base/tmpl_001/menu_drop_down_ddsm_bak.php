<?php
	if(!isset($drop_links)) $drop_links = 2;
	$dd_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $drop_links");
	if(count($dd_links) > 0)
		{
			?>
			<link href="<?=LIB_SERVER?>/menu/ddsmoothmenu/ddsmoothmenu.css" rel="stylesheet" type="text/css" />
			<link href="<?=LIB_SERVER?>/menu/ddsmoothmenu/ddsmoothmenu-v.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="<?=LIB_SERVER?>/menu/ddsmoothmenu/ddsmoothmenu.js"></script>
			
			<link href="<?=LIB_SERVER?>/menu/cssmenu/styles.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="<?=LIB_SERVER?>/menu/cssmenu/script.js"></script>
			
			<script type="text/javascript">
			ddsmoothmenu.init({
				mainmenuid: "top_menu",
				orientation: 'h',
				classname: 'ddsmoothmenu',
				//customtheme: ["#1c5a80", "#18374a"],
				contentsource: "markup"
			})
			</script>
			<div  class="top-menu-container">
                <div id="top_menu" class="ddsmoothmenu" style="display: block; height: 30px; clear:both;" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
                <?php print_menu($dd_links, array('menu_depth'=>2)); ?>
                </div>
			</div>
			<?
		}
?>