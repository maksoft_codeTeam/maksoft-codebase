</div></div>


        <div id="footer-wrapper" class="footer-dark">
            <footer id="footer-new">
                <div class="container">
                    <div class="row">
                    <?php
if(($o_site->_site['news'] > 0) && ($o_site->_site['news'] <> $o_site->_site['StartPage']) && (count($news)>0) ) {
?>
                        <ul class="col-md-3 col-sm-6 footer-widget-container clearfix">
                            <!-- .widget-pages start -->
                            <li class="widget widget_pages">
                                <div class="title">
                                    <h3><?=$o_page->print_pName(false, $o_site->_site['news'])?></h3>
                                </div>

                                <ul>
            <?php
				for($i=0; $i<count($news); $i++)
					{
					echo "<li>";
					$o_page->print_pName(true, $news[$i]['n']);
					echo "</li>";
					}
			?>
                                </ul>
                            </li><!-- .widget-pages end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->
                        <?php
} else { // page news is not set
echo "<ul class=\"col-md-3 col-sm-6 footer-widget-container clearfix\"></ul>";
}
?>

                        <ul class="col-md-3 col-sm-6 footer-widget-container">
                            <!-- .widget-pages start -->
                            <li class="widget widget_pages">
                                <div class="title">
                                    <h3><?=$footer_top_pages[$o_page->_site['language_id']]; ?></h3>
                                </div>

                                <ul>
            <?php
				for($i=0; $i<count($most_viewed_pages); $i++)
					{
					  echo "<li>";
					  $o_page->print_pName(true, $most_viewed_pages[$i]['n']);
					  echo "</li>";
					}
			?>
                                </ul>
                            </li><!-- .widget-pages end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->

                        <ul class="col-md-3 col-sm-6 footer-widget-container">
                            <!-- .widget-pages start -->
                            <li class="widget widget_pages">
                                <div class="title">
                                    <h3><?=$footer_new_pages[$o_page->_site['language_id']]; ?></h3>
                                </div>

                                <ul>
			<?php
				for($i=0; $i<count($last_added_pages); $i++)
					{
					echo "<li>";
					$o_page->print_pName(true, $last_added_pages[$i]['n']);
					echo "</li>";
					}
			?>
                                </ul>
                            </li><!-- .widget-pages end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->

                        <ul class="col-md-3 col-sm-6 footer-widget-container">
                            <li class="widget widget-text">
                                <div class="title">
                                    <h3><?=$footer_contacts[$o_page->_site['language_id']]; ?></h3>
                                </div>
                                
<?php
if(isset($n_contacts)) {
?>
<span class="text-big">
       	<?php
				echo "<blockquote>".cut_text($o_page->get_pText($n_contacts), 128, "", "<br>")."</blockquote>";
			//else
			//	echo $o_site->_site["primary_url"];
		?>
</span>
<?php
} else {// $n_contacts not setu
?> 
                                <?php if($o_page->_site['SAddress']) { ?>
                                <address>
                                <?=$footer_address[$o_page->_site['language_id']]; ?>
                                </address>
                                <span class="text-big">
                                <?php echo $o_page->_site['SAddress'];?>
                                </span>
                                <?php }?>
                                
                                <br /><br />
                                
                                <?php if($o_page->_site['SPhone']) { ?>
                                <address>
                                <?=$footer_phone[$o_page->_site['language_id']]; ?>
                                </address>
                                <span class="text-big">
                                <?php echo $o_page->_site['SPhone'];?>
                                </span>
                                <?php 
								}
}
								?>
                                
                                <br /><br />

                                <ul class="footer-social-icons">
                                <?php if(defined('CMS_FACEBOOK_PAGE_LINK') || $user->AccessLevel >0) { ?> <li class="facebook"><a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" target="_blank" class="fa fa-facebook"></a></li> <?php }?>
                                <?php if(defined('CMS_GPLUS_PAGE_LINK') || $user->AccessLevel >0) { ?> <li class="google-plus"><a href="<?=(defined("CMS_GPLUS_PAGE_LINK")) ? CMS_GPLUS_PAGE_LINK:"#"?>" target="_blank" class="fa fa-google-plus"></a></li> <?php }?>
                                <li class="rss"><a href="/rss.php" class="fa fa-rss"></a></li>
                                <?php if($o_page->_site['SPhone']) { ?>
                                <li class="phone-call">
                                <a href="tel:<?php echo $o_page->_site['SPhone'];?>" class="fa fa-phone"></a>
                                </li>
                                <?php }?>
                                
                                </ul><!-- .footer-social-icons end -->
                            </li><!-- .widget.widget-text end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->
                    </div><!-- .row end -->
                </div><!-- .container end -->
            </footer><!-- #footer end -->
            
          
<div class="container">
<div class="paymentlogos">  
 <?php 
if(VISA_MC_ICONS == 1) 
{
include "web/admin/modules/payments/payment_logos.php";
}
?>
</div>
</div>

            <div class="copyright-container">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="align-left"><?=$o_page->get_sCopyright() ." - ".date("Y")?></p>
                        </div><!-- .col-md-6 end -->

                        <div class="col-md-6">
                            <p class="align-right">��� ������ � ��������� ���� <a href="https://maksoft.net" title="��� ������ � ��������� �� �������� �������" target="_blank"><strong>�������</strong></a></a>
<strong>���������</strong> & <a href="<?=$o_page->get_pLink(38146)?>" title="SEO ����������� � ��������� �� �������� �������"> <strong>Netservice</strong></a></p>
                        </div><!-- .col-md-6 end -->
                    </div><!-- .row end -->
                </div><!-- .container end -->
            </div><!-- .copyright-container end -->

        </div><!-- #footer-wrapper end -->
<?php
if(defined('FLOATING_BUTTON')){
        echo '<a style="display:none;" href="http://www.dedal95.com/231225.html" id="floater" class="btn btn-default btn-lg floater" role="button" data-toggle="tooltip" data-placement="left">
        ����� �����
        </a>';
        echo "
        <script>
        jQuery(document).ready(function(){
                 jQuery(window).scroll(function () {
                                if (jQuery(this).scrollTop() > 50) {
                                        jQuery('#floater').fadeIn();
                                } else {
                                        jQuery('#floater').fadeOut();
                                }
                        })
        })
        </script>
        ";
}
?>
	<script async src="/lib/jquery/jquery.marquee.min.js" type="text/javascript"></script>
    <script async src="/lib/jquery/jquery.pause.js" type="text/javascript"></script>
    <script async language="javascript" type="text/javascript" src="<?=TEMPLATE_DIR?>effects.js"></script>
    <!--<script async src="http://maksoft.net/css/base/bootstrap-3.3.1/dist/js/bootstrap.min.js"></script>-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
<script async type="text/javascript" src="web/assets/auto-search/js/search_autocmpl.js"></script>
<?php include "Templates/footer_inc.php"; ?>    
    
