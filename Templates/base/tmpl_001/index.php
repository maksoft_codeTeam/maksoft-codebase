<?php 
require_once __DIR__.'/../../../lib/messages.php';
?>
<!DOCTYPE HTML>
<html lang="<?php echo($Site->language_key); ?>"><head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<title><?=$Title?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
		//include template configurations
		include("Templates/base/tmpl_001/configure.php");
        $o_site->print_sConfigurations();
    	//include meta tags
		include("Templates/meta_tags.php");
	?>
    <link href="<?=(defined("TMPL_CSS_LAYOUT")) ? TMPL_CSS_LAYOUT : "/".TEMPLATE_DIR."layout.css"?>" id="layout-style" rel="stylesheet" type="text/css" />
    <link async href="/css/admin_classes.css" rel="stylesheet" type="text/css" />
    
    <!--<link href="<?=(($Template->default_css > 0) ? $Template->css_url:"")?>" id="base-style" rel="stylesheet" type="text/css" />//-->
    <link href="<?=(($o_site->get_sCSS() != "") ? $o_site->get_sCSS():$Template->css_url)?>" id="base-style" rel="stylesheet" type="text/css" />


	<!--<link href="/css/base/bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet" media="screen">-->
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


	<?php
	if( (defined('GOOGLE_SITE_VERIF')) && ($o_page->n == $o_page->_site['StartPage']) ) 
	{
	     include_once("web/admin/modules/google/wmt.php"); 
	}
		
		if(defined("TMPL_BODY_BACKGROUND") || defined("TMPL_LOGO_POSITION"))
		{
			?>
			<style media="all">
			<?
			//CUSTOM TEMPLATE BACKGROUND
			if(defined("TMPL_BODY_BACKGROUND")) 
			{
			if(filesize("TMPL_BODY_BACKGROUND")<5000)
             $b_params = " 50% 0% fixed !important ";
            else
			 $b_params = "  no-repeat center center fixed ";
/*			if(filezise(TMPL_BODY_BACKGROUND)<10000) {
                $b_params = " 50% 0% fixed !important ";
            } else {
                $b_params = "  no-repeat center center fixed ";
            }
			//echo("body{background: url(".TMPL_BODY_BACKGROUND.") 50% 0% fixed !important;}");
			*/
			?>
            body{background: url(<?=TMPL_BODY_BACKGROUND?>) <?=$b_params?>;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                }
				#header, #footer {background: transparent !important; box-shadow: none !important;}
				#page_container {padding: 15px 0 !important;}
			<?
			}
			
			//LOGO POSITION
			if(defined("TMPL_LOGO_POSITION"))
			{
				$logo_margin = explode("x", TMPL_LOGO_POSITION);
			?>
				#header .logo {margin: <?=$logo_margin[1]?>px <?=$logo_margin[0]?>px}
			<?
			}
			
			if(defined("CSS_FONT_SIZE"))
			{
			?>
				#pageContent, #pageContent p {font-size: <?=CSS_FONT_SIZE?>em}
			<?
			}
			
			?>
            </style>
			<?
		}

	//get site news
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 5");
	
	//get most viewed pages
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5", "p.n != '".$o_site->_site['StartPage']."'");

	//get last added pages
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5", "p.preview>30");
	
	if(defined('GOOGLE_TRACKING_CODE'))
	{ 
	    include_once("web/admin/modules/google/analitycs.php");
	}
?>
</head>
<body itemscope itemtype="//schema.org/WebPage">
<?php 
include ("Templates/header_inc.php");	
if($o_page->_page['SiteID'] == "1013"){
    include ("Templates/solarpro-sat/header.php");	
}
?>

<?php
 include_once("web/admin/modules/google/analitycs.php");
 include_once("web/admin/modules/social/fb_app_init.php");
?>
<div id="site_container">
    <div id="header">
  	<?php include TEMPLATE_DIR."header.php"?>
    </div>
	<div id="page_container" class="shadow" >
	<style>
	</style>
        <div class="main-content">
        <?=Message\display();?>

        <?php
			if($o_page->_page['SiteID'] == $o_site->_site['SitesID'] )
				include TEMPLATE_DIR."main.php";
			else
				include TEMPLATE_DIR."admin.php";
		?>
        </div>
    </div>
    
		<?php
		include TEMPLATE_DIR . "footer-new-design.php";
		?>

	
</div>
</body>
</html>
