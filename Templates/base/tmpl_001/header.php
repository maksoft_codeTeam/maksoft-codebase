<div class="header-content" >
<div class="top-menu-content" style="width:100%;" >
    <?php 
    if( ($o_page->n<>111) && ($o_page->n<>132) )    include $tmpl_config['menu_drop_down'];    // edit.php
    ?>
</div>
    <!-- CSS file -->
    <link rel="stylesheet" href="/web/assets/package/EasyAutocomplete/dist/easy-autocomplete.min.css"> 

    <!-- Additional CSS Themes file - not required-->
    <link rel="stylesheet" href="/web/assets/package/EasyAutocomplete/dist/easy-autocomplete.themes.min.css"> 
    <div id="box_search"><div class="box-search-content">

    <form method="get">
        <input type="hidden" name="n" value="<?=$o_page->n?>">
        <input type="hidden" name="SiteID" value="<?=$o_site->SiteID?>">
        <input type="text" class="search_field" id="search" name="search" placeholder="�������">
    </form>
    </div></div>
    <?php
        if((file_exists($tmpl_config['default_logo']) && !empty($o_site->_site['Logo'])) || $user->AccessLevel >0)
        {
            ?>
            <a class="logo" href="<?=$o_page->scheme.$o_site->get_sURL()?>" title="<?=$o_site->_site['Title']?>"><img src="<?=$tmpl_config['default_logo']?>" width="<?=$tmpl_config['default_logo_width']?>"></a>
            <?
        }
    ?>
    <!-- BANNER SLIDER //-->
    <?php
    if(!isset($banner_limit) or !$banner_limit){
        $banner_limit=false;
    }
    $pBanners = $o_page->get_pBanners(null , $banner_limit);
    if(count($pBanners)>1 && $o_page->orig_n == $o_site->_site['StartPage'])
        {
            
            ?>
            <link rel="stylesheet" href="/lib/jquery/nivo-slider/themes/light/light.css" type="text/css" media="screen" />
            <link rel="stylesheet" href="/lib/jquery/nivo-slider/nivo-slider.css" type="text/css" media="screen" />
            <div class="slider-wrapper theme-default">
            <div id="banner-slider" class="nivoSlider banner">  
            <?  for($i=0; $i<count($pBanners); $i++)
                    print "<a href=\"".$o_page->get_pLink($pBanners[$i]['n'])."\" title=\"".htmlspecialchars($o_page->get_pInfo("title", $pBanners[$i]['n']))."\"><img src=\"/img_preview.php?image_file=".$pBanners[$i]['image_src']."&amp;img_width=".$tmpl_config['default_banner_width']."&amp;ratio=strict\" $params alt=\"".htmlspecialchars($o_page->get_pInfo("title", $pBanners[$i]['n']))."\"></a>";
            ?>
            </div>
            </div>
              <script type="text/javascript" src="/lib/jquery/nivo-slider/jquery.nivo.slider.js"></script>
              <script type="text/javascript">
              $j(window).load(function() {
                  $j('#banner-slider').nivoSlider({
                        effect: 'sliceDown', // Specify sets like: 'fold,fade,sliceDown'
                        slices: 15, // For slice animations
                        boxCols: 8, // For box animations
                        boxRows: 4, // For box animations
                        animSpeed: 500, // Slide transition speed
                        pauseTime: 5000, // How long each slide will show
                        startSlide: 0, // Set starting Slide (0 index)
                        directionNav: false, // Next & Prev navigation
                        controlNav: false, // 1,2,3... navigation
                        controlNavThumbs: false, // Use thumbnails for Control Nav
                        pauseOnHover: false, // Stop animation while hovering
                        manualAdvance: false, // Force manual transitions
                        prevText: 'Prev', // Prev directionNav text
                        nextText: 'Next', // Next directionNav text
                        randomStart: false, // Start on a random slide
                        beforeChange: function(){}, // Triggers before a slide transition
                        afterChange: function(){}, // Triggers after a slide transition
                        slideshowEnd: function(){}, // Triggers after all slides have been shown
                        lastSlide: function(){}, // Triggers when last slide is shown
                        afterLoad: function(){} // Triggers when slider has loaded
                    });
              });
              </script> 
            <?
        }
        else
        {
    ?>
    <div class="banner"><?=$o_page->print_pBanner($o_page->n, $tmpl_config['default_banner_width'], "class=\"banner-image\"", $tmpl_config['default_banner'])?></div>
    <!-- BANNER SLIDER //-->
    <?
        }
        //read google_translate_array
        if(isset($google_translate_languages) and is_array($google_translate_languages) && count($google_translate_languages)>0) {
            ?>
            <div id="box_languages">
            <?
            for($i=0; $i<count($google_translate_languages); $i++)
            {
                if(isset($google_translate_languages[$i]['img']))
                    echo "<a href=\"".$google_translate_languages[$i]['url']."\" target=\"_blank\"><img src=".$google_translate_languages[$i]['img']." border=\"0\" alt=\"".$google_translate_languages[$i]['title']."\"></a>";
                else
                    echo "<a href=\"".$google_translate_languages[$i]['url']."\" target=\"_blank\" title=\"".$google_translate_languages[$i]['title']."\">".$google_translate_languages[$i]['key']."</a>";
            }
            ?>
            </div>
            <?
        }
    else    
    if(count($o_site->get_sVersions()) > 0)
    {
    ?>
    <div id="box_languages"><?=$o_site->print_sVersions()?></div>
    <?
    }
    ?>
    <div id="navbar"><?=$o_page->print_pNavigation();?></div>
</div>
