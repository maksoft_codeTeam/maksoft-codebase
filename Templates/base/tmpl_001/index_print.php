<!DOCTYPE HTML>
<html lang="<?php echo($Site->language_key); ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<title><?=$Title?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
		//include template configurations
		include("Templates/base/tmpl_001/configure.php");		
    	//include meta tags
		
		//$noindex = true; 
		//include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
    <link href="<?=(defined("TMPL_CSS_LAYOUT")) ? TMPL_CSS_LAYOUT : "http://www.maksoft.net/".TEMPLATE_DIR."layout.css"?>" id="layout-style" rel="stylesheet" type="text/css" />
    <link href="/css/admin_classes.css" rel="stylesheet" type="text/css" />
    
    <!--<link href="<?=(($Template->default_css > 0) ? $Template->css_url:"")?>" id="base-style" rel="stylesheet" type="text/css" />//-->
    <link href="<?=(($o_site->get_sCSS() != "") ? $o_site->get_sCSS():$Template->css_url)?>" id="base-style" rel="stylesheet" type="text/css" />
	<script src="/lib/jquery/jquery.marquee.min.js" type="text/javascript"></script>
    <script src="/lib/jquery/jquery.pause.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript" src="<?=TEMPLATE_DIR?>effects.js"></script>

	<link href="/css/base/bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <script src="/css/base/bootstrap-3.3.1/dist/js/bootstrap.min.js"></script>

	<?php
		
		if(defined("TMPL_BODY_BACKGROUND") || defined("TMPL_LOGO_POSITION"))
		{
			?>
			<style media="all">
			<?
			
			//LOGO POSITION
			if(defined("TMPL_LOGO_POSITION"))
			{
				$logo_margin = explode("x", TMPL_LOGO_POSITION);
			?>
				#header .logo {margin: <?=$logo_margin[1]?>px <?=$logo_margin[0]?>px}
			<?
			}
			
			if(defined("CSS_FONT_SIZE"))
			{
			?>
				#pageContent, #pageContent p {font-size: <?=CSS_FONT_SIZE?>em}
			<?
			}
			
			?>
            </style>
			<?
		}

	//get site news
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 5");
	
	//get most viewed pages
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5", "p.n != '".$o_site->_site['StartPage']."'");

	//get last added pages
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5", "p.preview>30");
	
	if(defined('GOOGLE_TRACKING_CODE'))   { include_once("web/admin/modules/google/analitycs.php"); }
	 include_once("web/admin/modules/counter/tyxo.php"); 
	if( (defined('GOOGLE_SITE_VERIF')) && ($o_page->n == $o_page->_site['StartPage']) )   { include_once("web/admin/modules/google/wmt.php"); }
?>
</head>
<body onload = "javascript:window.print()" >
<div id="pageContent">
    <div class="page-content">
   	<a href="<?=$o_page->get_pLink($o_page->get_pParent())?>" class="page-back" title="<?=$o_page->get_pName($o_page->_page['ParentPage'])?>"></a>
    <div id="box-font-size"><a href="javascript: void(0)" title="" id="inc-font">A+</a> / <a href="javascript: void(0)" id="dec-font" title="">A-</a></div>
    <h1 class="title"><?=$o_page->get_pTitle("strict")?></h1>
	<?php
		//enable tag search / tag addresses
        if(strlen($search_tag)>2)
			{
				$o_page->print_search($o_site->do_search(iconv("UTF-8","CP1251",$search_tag), 10)); 
				$keyword = iconv("UTF-8","CP1251",$search_tag); 
				//include("selected_sites.php"); 
			}
		//enable site search
		elseif(strlen($search)>2)
			$o_page->print_search($o_site->do_search($search, 10)); 
		else
			{	

				//show scrolling welcome message
				if($welcome_message)
					{
						?>
						<div id="welcome_message" style="display: block; width: 600px; overflow:hidden;"><?=$welcome_message?></div>
						<br clear="all">                    
						<?	
					}
				//print page content
				$o_page->print_pContent();
				
				echo "<br clear=\"all\">";
				if(!defined(CMS_MAIN_WIDTH) || CMS_MAIN_WIDTH > $tmpl_config['default_main_width'])
					$cms_args = array("CMS_MAIN_WIDTH"=>$tmpl_config['default_main_width']);
				//print page subcontent
				$o_page->print_pSubContent(NULL, 1, true, $cms_args);
				
				//print php code
				eval($o_page->get_pInfo("PHPcode"));
				
				if ($user->AccessLevel >= $row->SecLevel)
					include_once("$row->PageURL");
					
				// offers parser 
				include("selected_offers.php"); 


			}
			
			if(($Site->site_status>0) && ($row->preview>10) ) {
			 $Rub=""; 
			 $PRub=""; 
			 $keyword=""; 
			 //$keywords_list = $row->title; 
			 $keywords_list = $o_page->get_top_word(); 
			// if(strlen($keywords_list)<3) $keywords_list="$row->title"; 
			// if(strlen($keywords_list)<3) $keywords_list="$row->Name"; 
			 echo("<br><br>"); 
			 //$limit = 15; 
			 include("etiketi.php"); 
						
		}
				
    ?>
    </div>
</div>
</body>
</html>
