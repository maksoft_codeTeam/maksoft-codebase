<div class="box menu" id="menu">
    <div class="box-content">
    <?php 
    /*
        $menu_links = $o_page->get_pSubpages($o_site->_site['StartPage']);
        for($i=0; $i<count($menu_links); $i++)
            echo "<a href=\"".$o_page->get_pLink($menu_links[$i]['n'])."\" title=\"".$menu_links[$i]['title']."\" class=\"".(($o_page->n == $menu_links[$i]['n'])? "selected":"")."\"><div class=\"bullet\"></div>".$menu_links[$i]['Name']."</a>";
    */

//select the parent page of the folowing menu items (n=$Site->StartPage)
if(!isset($menu_parent_item))
    $menu_parent_item = $Site->StartPage;

//show / hide items numbering
if(isset($view_numbering) and $view_numbering == true)
    $start_number = 1;

//start button
if(isset($view_home) and $view_home == true)
    $start_button = '<a href="'.$o_page->get_pLink($o_site->_site['StartPage']).'" class="menu_button" style="display: block;">'.$o_site->_site['Home'].'</a>';
else $start_button = "";
    
//show / hide submenu bullet
if(!isset($menu_bullet))
    $menu_bullet = "";
    
//show / hide submenu bullet
if(!isset($submenu_bullet))
    $submenu_bullet = " &raquo; ";

//show page with selected status (0-default, 1-toplink, 2-drop_down, 3-main_link, 4-vip_link, 5-second_link)
if(!isset($view_menu_item))
    {
        $view_menu_item = 0;
        if(isset($skip_menu_item)) 
            //skip selected pages
            $skip_menu_item_sql = " AND toplink!=$skip_menu_item ";
        else $skip_menu_item_sql = " ";
    }
else 
{
    unset($skip_menu_item);
    //view selected pages   
    $view_menu_item_sql = "toplink = '". $view_menu_item."' ";
}

//show / hide submneu
if(!isset($view_submenu)) $view_submenu = "none";

//show / hide selected submenus - only when view_submenu = epxpand !!!
if(!isset($view_submenu_array)) $view_submenu_array = NULL;

//view all pages
if($view_menu_item == "all")  $view_menu_item_sql = "1"; 

         echo $start_button;
         if($menu_parent_item == 0)
            $parent_page_sql = "1";
        else $parent_page_sql = "ParentPage = $menu_parent_item";
        
        //reading main menu       
        $menu_links = $o_page->get_pSubpages($menu_parent_item, "p.sort_n ASC", "$view_menu_item_sql $skip_menu_item_sql");
        
        echo "<ul>";
        for($i=0; $i<count($menu_links); $i++)
            {
                echo "<li>";
                $params = "";
                if($menu_links[$i]['n'] == $o_page->n || in_array($menu_links[$i]['n'], $o_page->get_pParentPages($o_page->n)))
                    $params = "class=\"selected\"";
                $o_page->print_pName(true, $menu_links[$i]['n'], $params);  
                $submenu_links = $o_page->get_pSubpages($menu_links[$i]['n']);
                switch($view_submenu)
                {
                case "expand":{
                                if(count($view_submenu_array)>0 && !in_array($menu_links[$i]['n'], $view_submenu_array)) break;
                                
                                $params_array = array("params"=>"class=\"submenu\"");
                                print_menu($submenu_links, $params_array);
                                break;
                            }
                case "open":{
                                if($menu_links[$i]['n'] == $o_page->n || in_array($menu_links[$i]['n'], $o_page->get_pParentPages($o_page->n)))
                                    {
                                        $params_array = array("params"=>"class=\"submenu\"");
                                        print_menu($submenu_links, $params_array);
                                    }
                                break;
                            }
                case "none":
                default:    {
                                $submenu_display = "none"; break;
                            }
                }
                echo "</li>";
            }
        echo "</ul>";
    ?>
    </div>
</div>
