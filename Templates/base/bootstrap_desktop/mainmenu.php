<!-- style="width:<?=$tmpl_config['default_max_width'];?>px !important;" -->
<div class="container-fluid padding0" >
<?php
/* $homepage=$o_site->StartPage;
$subPs = $o_page->get_pSubpages($homepage); */
if(!isset($drop_links)) $drop_links = 2;
$dd_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $drop_links");
if(!$dd_links==''){
?>
<nav class="navbar navbar-default navbar-static-top margin-bottom-0">
    
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
		  <?php
		  
			if((file_exists($tmpl_config['default_logo']) && !empty($o_site->_site['Logo'])) || $user->AccessLevel >0)
			{
				?>
				<a class="navbar-brand logo padding0" href="http://<?=$o_site->get_sURL()?>" title="<?=$o_site->_site['Title']?>">
					<img class="pull-left" src="<?=HTTP_SERVER."/".$tmpl_config['default_logo']?>" height="50"> <!-- width="<?=$tmpl_config['default_logo_width']?>"> -->
					<span class="pull-left margin-top-15 margin-left-10"><?=$o_site->_site['Title'];?></span>
				</a>
				<?php
			}else{
			 ?>
			 <a class="navbar-brand logo padding0" href="http://<?=$o_site->get_sURL()?>" title="<?=$o_site->_site['Title']?>">
			 <span class="pull-left margin-top-15 margin-left-10"><?=$o_site->_site['Title']?></span>
			 </a>
			 <?php
			}
		  ?>
		  
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <?php
				print_menu($dd_links, array('menu_depth'=>0));
			?>
		<script>
		jQuery(document).ready(function(){
			jQuery("#navbar>ul").addClass("nav navbar-nav pull-right");
			jQuery("ul.navbar-nav>li>ul").addClass("dropdown-menu");
			jQuery("ul.navbar-nav>li").each(function(){
				if (jQuery(this).find("ul").length){
					jQuery(this).addClass("dropdown");
				}
			});
			jQuery("ul.navbar-nav>li.dropdown>a").addClass("dropdown-toggle").attr("data-toggle", "dropdown").attr("role", "button").attr("aria-expanded", "false").append("<span class='caret'></span>");
			jQuery(".nav.navbar-nav li").removeClass("over");
		});
		</script>
        </div><!--/.nav-collapse -->
      
</nav>
</div>
<?php
}
?>