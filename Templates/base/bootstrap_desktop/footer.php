<a id="back-to-top" href="#" class="btn btn-default btn-sm back-to-top" role="button"><span class="glyphicon glyphicon-chevron-up"></span></a>
<hr/>
<footer>
	<div class="panel panel-default">
<!--		<div class="panel-heading">
			- <a href="http://<?=$o_site->_site['primary_url'];?>"><?=$o_site->_site['Name'];?></a> -
		</div>-->
		<div class="panel-body">
			<div class="row">
				<?php
				foreach($tmpl_config["footer"] as $block){
					?>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<?php include $block; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<div class="panel-footer">
        <div class="container">
			<div class="row text-center">

					<?php include_once("Templates/copyrights_links.php"); ?>

			</div>
            </div>
		</div>
	</div>
</footer>


	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="<?=ASSETS_DIR;?>js/script.js"></script>
	<script src="<?=ASSETS_DIR;?>fws-slider2/js/css3-mediaqueries.js"></script>
	<script src="<?=ASSETS_DIR;?>fws-slider2/js/fwslider.js"></script>
<?php include "Templates/footer_inc.php"; ?>