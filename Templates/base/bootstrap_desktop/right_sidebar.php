<?php
	if((defined("TMPL_CSS_LAYOUT") && TMPL_CSS_LAYOUT=="both-sidebars") || (defined("TMPL_CSS_LAYOUT") && TMPL_CSS_LAYOUT=="right-sidebar")){
			?>
			<div class="col-xs-12 col-md-3">
			<?php

				if(is_array($tmpl_config['column_right']))
					{
						for($icr=0; $icr<count($tmpl_config['column_right']); $icr++)
							include $tmpl_config['column_right'][$icr];
					}
				else
					include $tmpl_config['column_right'];
			?>
			</div>
			<?php
	}else if(!defined("TMPL_CSS_LAYOUT")){
		?>
			<div class="col-xs-12 col-md-3">
			<?php

				if(is_array($tmpl_config['column_right']))
					{
						for($icr=0; $icr<count($tmpl_config['column_right']); $icr++)
							include $tmpl_config['column_right'][$icr];
					}
				else
					include $tmpl_config['column_right'];
			?>
			</div>
		<?php
	}
	?>