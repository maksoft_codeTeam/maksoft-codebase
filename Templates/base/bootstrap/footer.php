<a id="back-to-top" href="#" class="btn btn-default btn-sm back-to-top" role="button"><span class="glyphicon glyphicon-chevron-up"></span></a>
<hr/>
<footer>
	 <div class="text-center center-block">
				<p class="txt-railway">- <a href="http://<?=$o_site->_site['primary_url'];?>"><?=$o_site->_site['Name'];?></a> -</p>
					<?php if(defined('CMS_FACEBOOK_PAGE_LINK')) { ?><a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-facebook-square fa-3x social-fb"></i></a> <?php }?>
					<?php if(defined('CMS_GPLUS_PAGE_LINK')) { ?><a href="<?=(defined("CMS_GPLUS_PAGE_LINK")) ? CMS_GPLUS_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-google-plus-square fa-3x social-fb"></i></a> <?php }?>
					<?php if(defined('CMS_TWITTER_PAGE_LINK')) { ?><a href="<?=(defined("CMS_TWITTER_PAGE_LINK")) ? CMS_TWITTER_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-twitter-square fa-3x social-tw"></i></a> <?php }?>
					<?php if(defined('CMS_PINTEREST_PAGE_LINK')) { ?><a href="<?=(defined("CMS_PINTEREST_PAGE_LINK")) ? CMS_PINTEREST_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-pinterest-square fa-3x social-tw"></i></a> <?php }?>
					<?php if(defined('CMS_LINKEDIN_PAGE_LINK')) { ?><a href="<?=(defined("CMS_LINKEDIN_PAGE_LINK")) ? CMS_LINKEDIN_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-linkedin-square fa-3x social-tw"></i></a> <?php }?>
					<?php if(defined('CMS_BLOG_PAGE_LINK')) { ?><a href="<?=(defined("CMS_BLOG_PAGE_LINK")) ? CMS_BLOG_PAGE_LINK:"#"?>" target="_blank"><i id="social" class="fa fa-twitter-square fa-3x social-tw"></i></a> <?php }?>
	</div>
    <hr>
	<?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="" target="_blank">Netservice</a>
</footer>

<?php include "Templates/footer_inc.php"; ?>