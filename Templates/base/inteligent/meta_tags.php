<?php
//including function files
include_once("lib/lib_functions.php"); 
//DB functions
include_once("web/admin/query_set.php");

//Input/Output functions
include_once("web/admin/io.php");

// shortcodes manipulation
include_once("shortcodes/main/shortcodes.php");
if(is_object($wwo)) {
    include_once("shortcodes/wwo/shortcodes.php");
}

if(defined("SHORTCODES_FILE") && (file_exists("SHORTCODES_FILE"))) {
    include_once("SHORTCODES_FILE");
}

$noindex=false; 

// description
$description= $Site->Description;

// keywords

$row=get_page($n);
if(strlen($row->tags)<10) {
	$row->tags=$row->tags.$row->title;
}
 if ($row->tags<>'') {
	$row->tags=str_replace(" \,", "\,",  $row->tags); 
	$keywords = $o_page->replace_shortcodes($row->tags);
} 
/*
else {
$keywords=str_replace(" ,", ",", $Site->keywords);   
} 
*/

// if(strlen($Site->keywords)<10) { $Site->keywords=$Site->title; }

if($Site->site_status>=0) {
	//if($n!=$Site->StartPage) {
		$description = $o_page->get_pText();
		$description = str_replace(array("\n", "\t", "\r"), '', $description);
        $description= htmlspecialchars(strip_tags($row->textStr));
    //}

	if(strlen($search_tag)>5)  {
	  $keywords = iconv("UTF-8","CP1251",$search_tag); 
	  $description = iconv("UTF-8","CP1251",$search_tag); 
	  $taged_pages = $o_site->do_search(iconv("UTF-8","CP1251",$_GET['search_tag']));
	  if(count($taged_pages) > 0) {
	  
		  foreach($taged_pages as $taged_page) {
			//$description .= $taged_page['Name'].",".$taged_page['title'].",";
			
			$keywords = $keywords ." ".$taged_page['Name'].",".$taged_page['title'].",";
		 }
		}
	   else {
	   	$noindex = true; 
	   }
	 
	$description=  strip_tags(str_replace("\"", "", $description)); 
	if(strlen($description)<10) {$description = rtrim("$description $row->tags $Site->Description")." ... "; }
}
	$description .= $o_page->_page['comment_subject']; 
	//if(strlen($description)>160) $description = rtrim(substr($description, 0, 160));  
	if(strlen($description)>180) $description = cut_text($description, 180, "...");

    $description = $o_page->replace_shortcodes($description);

}
$date_expires = new DateTime();
$date_expires->add(new DateInterval('P0DT2H30M5S'));  // cashe the CONTENT for 2 HOURS    date format must be RFC1123

// author
if($o_page->_page['author']>0) {
	$author_q = $o_page->db_query("SELECT * FROM user_authorship WHERE uID = ".$o_page->_page['author']." AND (user_authorship.SiteID = 0 OR  user_authorship.SiteID = '".$o_page->SiteID."') ORDER BY uaID DESC LIMIT 1"); 
	$row_author = mysqli_fetch_object($author_q); 
}
?>

<meta http-equiv="Content-Language" content="<?php echo("$Site->language_key"); ?>" />
<meta http-equiv="CACHE-CONTROL" content="PUBLIC" />
<meta http-equiv="EXPIRES" content="<?php echo $date_expires->format("D, d M Y H:i:s O"); ?>" />
<?php if( ($o_page->get_uInfo("AccessLevel")==0) && (strlen($o_page->_site['primary_url'])>7) ) echo("<base href=\"http://".$o_page->_site['primary_url']."\" />"); ?> 
<meta name="resource-type" content="document" />
<meta name="copyright" content="Copyright (C) 2004-<?php echo date("Y"); ?>" />
<meta name="webmaster" content="<?php echo("$Template->webmaster");  ?>" />
<?php 
// IF NO CONTENT- canonical rel to PREVIOUS PAGE
if( (strlen($row->textStr)<256)  && (strlen($row->tags)<3)  && (strlen($row->title)<3)  && ($row->n <> $Site->StartPage)  && ($row->ParentPage <> $Site->StartPage) ) {
	echo('<link rel="canonical" href="'.$o_page->get_pLink($row->ParentPage).'" />'); 
}

$primary_server_name = preg_replace("/^w+\./", "",  $o_page->_site['primary_url']); 
$current_server_address = preg_replace("/\/$/", "", "http://".$_SERVER["HTTP_HOST"].iconv("UTF-8","CP1251",urldecode($_SERVER['REQUEST_URI'])) ); 

if($o_page->SiteID<>$o_page->_page['SiteID']) {
	$noindex=true;
	echo("<!--".$o_page->SiteID."<>".$o_page->_page['SiteID'].$o_page->get_pLink() ."<> http://".$_SERVER['HTTP_HOST'].iconv('UTF-8','CP1251',urldecode($_SERVER['REQUEST_URI']))."-->");
} else {
	if ( ($o_page->get_pLink() <> $current_server_address ) && (strlen($search_tag)<2) ) {
		echo('<link rel="canonical" href="'.$o_page->get_pLink($row->n).'" />'); 
	}
}

?>

<meta name="keywords" content="<?=strip_tags($keywords)?>" />
<meta name="description" content="<?=strip_tags($description) ?>" />
<meta name="coverage" content="Worldwide" />
<meta name="language" content="<?php echo("$Site->language_key"); ?>" />
<!-- Facebook  Open Graph Meta Data -->          
<meta property="og:title" content="<?php echo $o_page->_page['Name']." - ".$description; ?>" />
<meta property="og:type" content="article" />
<meta property="og:image" content="<?php echo "http://".$o_page->_site['primary_url']."/".$o_page->get_pImage($n);  ?>" />
<meta property="og:url" content="<?php echo $o_page->get_pLink($o_page->n, $o_page->SiteID, "", true); ?>" />
<meta property="og:description" content="<?php echo $description ?>" />
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="robots" content="index,follow,all,noarchive" />
<link rel="alternate" type="application/rss+xml" title="<?=$Site->title?>" href="http://<?=$o_page->_site['primary_url']?>/rss.php" />

<script type="text/javascript" src="http://www.maksoft.net/lib/lib_functions.js" ></script>


<link href="http://www.maksoft.net/css/admin_classes.css" rel="stylesheet" type="text/css" />
<?php if(defined('CMS_IMPORT_CSS') || $user->AccessLevel >0) { ?> <link href="<?=(defined("CMS_IMPORT_CSS")) ? CMS_IMPORT_CSS:"#"?>" rel="stylesheet" type="text/css" /> <?php }?>
