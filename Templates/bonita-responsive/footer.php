<div class="copyright">

&copy; 2008-<?php echo date("Y"); ?> Bonita Style Fashion

</div>
<div class="support">
<?=WEBDESIGN?>
</div>
 
    <!-- jQuery -->
   <script src="<?=ASSETS_DIR?>assets/js/jquery.js"></script>
   <script>jQuery.noConflict();</script> 
         
    <!-- Bootstrap Core JavaScript -->
    <!--<script src="<?=ASSETS_DIR?>assets/js/bootstrap.min.js"></script>-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="<?=ASSETS_DIR?>assets/js/classie.js"></script>
    <script src="<?=ASSETS_DIR?>assets/js/cbpAnimatedHeader.js"></script>
    
    <?php 
	if(in_array($o_page->n, array(19334745, 19334755, 19334923))){
		?>
    <!-- jQuery 1.7.2+ or Zepto.js 1.0+ -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="<?=ASSETS_DIR?>assets/js/jquery.magnific-popup.min.js"></script>
    
    <script type="text/javascript">
    $('.magnific').magnificPopup({
    delegate: 'a',
    type: 'image',
	  gallery:{
    enabled:true
  }
    });
    </script> 
    <?php
	}
    ?>
    
    
    <script src="<?=ASSETS_DIR?>assets/js/ddmenu.js"></script>
    <script src="<?=ASSETS_DIR?>assets/js/custom.js"></script>

<!--    <script src="<?=ASSETS_DIR?>assets/js/script.js"></script>-->
<!--    <script>jQuery.noConflict();</script>-->
<?php include( "Templates/footer_inc.php" ); ?>

</body>

</html>
