<?php
	define("TEMPLATE_NAME", "bonita-responsive");
	define("TEMPLATE_DIR", "Templates/".TEMPLATE_NAME."/");
	define("TEMPLATE_PATH", "http://".$_SERVER['SERVER_NAME']."/Templates/".TEMPLATE_NAME."/");
	define("ASSETS_DIR", "/Templates/".TEMPLATE_NAME."/");

	if($o_page->lang_id == 1) {
		include_once TEMPLATE_DIR . "lang/bg.php";
	}
	else
	{
		include_once TEMPLATE_DIR . "lang/en.php";
	}
$translation = array();
foreach(range(0,100) as $lang) {
    $translation[$lang] = array();
}
?>
