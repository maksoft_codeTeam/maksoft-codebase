<!DOCTYPE html>
<html lang="<?php echo($Site->language_key); ?>" class="responsive-layout product-page"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
    <link rel="icon" type="image/png" href="<?=ASSETS_DIR?>assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="mUrxWWbUBShCvuCJDUHpDgdE_hPawFj-Lj9Bc-oCHxE" />

    <title><?php echo $Title ?></title>
    
<?php

// include meta tags

include ("Templates/meta_tags.php");

// include('lib/test.php');

require_once ('lib/Database.class.php');

$db = new Database();
$o_page->setDatabase($db);
$menu_data = $o_page->formatMenuData($o_page->SiteID);

$o_site->print_sConfigurations();

if (($o_page->n) == "19330641") {
    $body_class = "index";
}
else {
    $body_class = "main";
    $main = "navbar-main";
}

?>

    <!-- Bootstrap Core CSS -->
    <!--<link href="<?=ASSETS_DIR?>assets/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link href="<?=ASSETS_DIR?>assets/css/style.css" rel="stylesheet">
    <link href="<?=ASSETS_DIR?>assets/css/ddmenu.css" rel="stylesheet">
    <link href="<?=ASSETS_DIR?>assets/css/magnific-popup.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=ASSETS_DIR?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Exo+2:500,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
     <!--[if IE]>
     <link rel='stylesheet' type='text/css' href='<?=ASSETS_DIR?>assets/css/ie.css'/>
	 <![endif]-->

</head>

<body id="page-top" class="<?php echo $body_class
?>">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top <?php echo $main ?>">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
            
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll fade-in logo" href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>"><img src="<?php echo ASSETS_DIR ?>assets/img/logo-bonita-250.png" class="bonita-logo" alt="������" /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <div id="ddmenu">
<ul>
                                                                                    
<?php

if (!isset($dd_links)) $dd_links = 2;
$dd_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $dd_links");

for ($i = 0; $i < count($dd_links); $i++) {
    $subpages = $o_page->get_pSubpages($dd_links[$i]['n']);
?>
<li class="full-width">
    <span class="top-heading">
        <a href="<?php echo $o_page->get_pLink($dd_links[$i]['n']) ?>" title="<?=stripslashes($o_page->get_pName($dd_links[$i]['n']))?>" class="nav navbar-nav cl-effect-13 margin-menu">
            <?php echo $dd_links[$i]['Name'] ?><?php echo ((count($subpages) > 0) ? "
                <i class=\"fa fa-angle-double-down\" aria-hidden=\"true\"></i>" : "") ?></a></span> <?php
    if (count($subpages) > 0) {
?>
            <div class="dropdown">

            
                <div class="dd-inner">

                        <?php
            #echo $o_page->buildMenu($dd_links[$i]['n'], $menu_data, 1);
        for ($j = 0; $j < count($subpages); $j++)
            echo "<ul class=\"column\"><li><h3><a href=\"" . $o_page->get_pLink($subpages[$j]['n']) . "\" title=\"" . $o_page->get_pName($subpages[$j]['n']) . "\">" . stripslashes($subpages[$j]['Name']) . "</a></h3></li></ul>";
?>
                </div>
            </div>
<?php }
 }

?>  
               </ul>
               </div>
    
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                    <a href="#page-top"></a>
                    </li>
                    <li>
                    


            <form method="get" class="search-form" action="page.php">
                    <input type="hidden" name="n" value="19330846">
                    <input type="hidden" name="SiteID" value="<?php echo $o_site->SiteID
?>">
                <div class="form-group has-feedback">
                    <label for="search" class="sr-only"><?=SEARCH?></label>
                    <input type="text" class="form-control" name="search" id="search" placeholder="<?=SEARCH?>..">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
            </form>


</li>
					<li><?=$o_page->print_sVersions_dev();?></li>
<li class="facebook"><a href="https://www.facebook.com/Bonita-Style-810873205657974/" class="fb"><i class="fa fa-facebook"></i></a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
</nav>
