<?php
  $body_class = "index";
?>
    
      <div class="container">
      <div class="row">
      <div id="portfolio">
                <div class="col-md-3 portfolio-item fade-in one">
                    <a href="<?=$o_page->get_pLink(19331102)?>" class="portfolio-link">
                        <img src="<?=$o_page->get_pImage(19331102)?>" class="img-responsive" alt="<?=$o_page->get_pName(19331102)?>">
                    </a>
                    <div class="portfolio-caption">
                        <h5><a href="<?=$o_page->get_pLink(19331102)?>" title="<?=$o_page->get_pName(19331102)?>"><?=$o_page->get_pName(19331102)?></a></h5>
                    </div>
                </div>
                <div class="col-md-3 portfolio-item fade-in two">
                    <a href="<?=$o_page->get_pLink(19333074)?>" class="portfolio-link">
                        <img src="<?=$o_page->get_pImage(19333074)?>" class="img-responsive" alt="<?=$o_page->get_pName(19333074)?>">
                    </a>
                    <div class="portfolio-caption">
                        <h5><a href="<?=$o_page->get_pLink(19333074)?>" title="<?=$o_page->get_pName(19333074)?>"><?=$o_page->get_pName(19333074)?></a></h5>
                    </div>
                </div>
                <div class="col-md-3 portfolio-item fade-in three">
                    <a href="<?=$o_page->get_pLink(19331104)?>" class="portfolio-link">
                        <img src="<?=$o_page->get_pImage(19331104)?>" class="img-responsive" alt="<?=$o_page->get_pName(19331104)?>">
                    </a>
                    <div class="portfolio-caption">
                        <h5><a href="<?=$o_page->get_pLink(19331104)?>" title="<?=$o_page->get_pName(19331104)?>"><?=$o_page->get_pName(19331104)?></a></h5>
              </div>
                </div>
                <div class="col-md-3 portfolio-item fade-in four">
                    <a href="<?=$o_page->get_pLink(19331105)?>" class="portfolio-link">
                        <img src="<?=$o_page->get_pImage(19331105)?>" class="img-responsive" alt="<?=$o_page->get_pName(19331105)?>">
                    </a>
                    <div class="portfolio-caption">
                        <h5><a href="<?=$o_page->get_pLink(19331105)?>" title="<?=$o_page->get_pName(19331105)?>"><?=$o_page->get_pName(19331105)?></a></h5>
                    </div>
                </div>
                
            </div>
        </div>
       <div class="row">
       <div id="portfolio">
       <div style="opacity: 0.7;">
       <marquee behavior="scroll" direction="left">
       <?=$o_page->print_pContent();?>
       </marquee>
       </div>
       </div>
       </div>
       </div>
