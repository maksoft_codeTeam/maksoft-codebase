<?php

$languages = array('bg' => 1, 'en' => 2);
define("LANGUAGE", $o_site->get_sLanguage());


	include "config.php";
	
	include TEMPLATE_DIR . "header.php";
	
	// Page Template
	$pTemplate = $o_page->get_pTemplate();
	if($o_page->_page['SiteID'] != $o_site->_site['SitesID'] )
		include TEMPLATE_DIR."admin.php";	
	elseif($pTemplate['pt_url'])
		include $pTemplate['pt_url'];
	elseif($o_page->_page['n'] == $o_site->_site['StartPage'] && strlen($search_tag)<3 && strlen($search)<3)
		include TEMPLATE_DIR . "home.php";
	else
		include TEMPLATE_DIR . "main.php";
		
	include TEMPLATE_DIR . "footer.php";
?>
