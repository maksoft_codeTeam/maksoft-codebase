<link rel="stylesheet" href="<?=ASSETS_DIR?>assets/journal/product.css"/> 
<meta property="og:image" content="<?php echo "http://".$o_page->_site['primary_url']."/".$o_page->get_pImage($n);  ?>" />

<script type="text/javascript" src="<?=ASSETS_DIR?>assets/journal/product.js"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'bg'}
</script>
<style>
h1.title {
display: none;
}
</style>
                            
<div class="extended-container">
<div id="container" class="j-container">

<div id="content">

<div class="product-info">

<div class="left">
<div class="image">

<a href="/<?=$o_page->get_pImage()?>">
<img src='/<?=$o_page->get_pImage()?>' id="image" data-largeimg="<?=$o_page->get_pImage()?>"> </a> 

</div>

<div class="gallery-text"><i class="fa fa-search-plus"></i> <span><?=SHOW_FULL_SIZE?></span></div>
<div id="product-gallery" class="image-additional journal-carousel">

<a href="/<?=$o_page->get_pImage()?>" id="<?=$o_page->_page['n'];?>" onclick='hide(this)'>
<img src="/img_preview.php?image_file=<?=$o_page->_page['image_src'];?>&img_width=150" data-largeimg="<?=$o_page->_page['image_src']?>">

</a>

    <?php
        $page_gallery = $o_page->get_pSubpages(NULL, "p.sort_n ASC", "im.image_src !=''");
        foreach($page_gallery as $img) {
                echo "<a href=\"/".$img['image_src']."\" id=\"".$img['n']."\" title=\"".$img['n']."\" onclick='hide(this)'>";
                echo "<img src=\"/img_preview.php?image_file={$img['image_src']}&img_width=150\">";
                echo "</a>";
            }
    ?>

</div> 

<div class="share">
<div class="fb-like" data-href="<?=$o_page->get_pLink()?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>

<!-- Place this tag where you want the +1 button to render. -->
<div class="gplus">
<div class="g-plusone" data-size="medium" data-href="<?=$o_page->get_pLink()?>"></div>
</div>

</div>

<script>(function(){var opts={itemsCustom:[[0,parseInt('4',10)],[470,parseInt('4',10)],[760,parseInt('4',10)],[980,parseInt('4',10)],[1100,parseInt('4',10)]],navigation:true,scrollPerPage:true,navigationText:false,stopOnHover:true,cssAnimation:false,paginationSpeed:300,margin:parseInt('15',10)};opts.autoPlay=parseInt('3000',10);opts.stopOnHover=true;jQuery("#product-gallery").owlCarousel(opts);$('#product-gallery .owl-buttons').addClass('side-buttons');})();</script> 

<div class="image-gallery" style="display: none !important;">

<a href="/<?=$o_page->get_pImage()?>" class="swipebox">
<img src="/<?=$o_page->get_pImage()?> id="image" data-rageimg="<?=$o_page->get_pImage()?>">

</a>

<?php
    $page_gallery = $o_page->get_pSubpages(NULL, "p.sort_n ASC", "im.image_src !=''");
    $sub_title = '';
    $sub_content = '';
    foreach($page_gallery as $img) {
            $sub_title .= '<h5>'.$img['Name'].'</h5>';
            $sub_content .= '<p id='.$img['Name'].'>'.stripslashes($img['textStr']).'</p>';
            echo "<a href=\"/".$o_page->get_pImage($img['n'])."\" class=\"swipebox\">";
            $o_page->print_pImage(150, "", "", $img['n']);
            ?>
            <img src="/img_preview.php?image_file=<?=$img['image_src']?>&width=150 id="image" data-rageimg="<?=$img['image_src']?>">
            <?php
            echo "</a>";
        }
?>
                                


</div></div>

<div class="right">
<div class="product-options">
<div class="description">

<span class="p-model"><?=PRODUCT_CODE?>:</span> 
<span id="product_code" class="p-model"><?=$o_page->print_pName()?></span>
<?php /*?><?=$sub_title;?><?php */?>
</div>

<div class="opisanie">
<h5><?=DESCRIPTION?>:</h5>
<p id="<?=$o_page->print_pName()?>"><?=$o_page->print_pText()?></p>
<?php /*?><?=$sub_content;?><?php */?>


</div>

<?php
if($o_page->_page['ParentPage'] != 19331201 && $o_page->_page['ParentPage'] != 19331203 && $o_page->_page['ParentPage'] != 19331202 && $o_page->_page['ParentPage'] != 19332351 )
{
    ?>
<div class="tags"><b><?=USEFULL_TIPS?></b>
<a href="<?=SIZE_CHART_URL?>"><i class="fa fa-download" aria-hidden="true"></i> <?=SIZE_CHART?></a>

<?php
if($o_page->_page['ParentPage'] != 19331199 && $o_page->_page['ParentPage'] != 19333838 && $o_page->_page['ParentPage'] != 19333861 && $o_page->_page['ParentPage'] != 19333932 && $o_page->_page['ParentPage'] != 19334147 && $o_page->_page['ParentPage'] != 19334148 && $o_page->_page['ParentPage'] != 19333482 && $o_page->_page['ParentPage'] != 19333534 && $o_page->_page['ParentPage'] != 19333542 && $o_page->_page['ParentPage'] != 19333581 && $o_page->_page['ParentPage'] != 19333674 && $o_page->_page['ParentPage'] != 19333698 && $o_page->_page['ParentPage'] != 19334145 && $o_page->_page['ParentPage'] != 19334146 )
{
?>
<a href="<?=TREATMENT_GUIDELINES_URL?>"><i class="fa fa-download" aria-hidden="true"></i> <?=TREATMENT_GUIDELINES?></a>
<?php } ?>
</div>
<?php } ?>

<?php include ("web/forms/bonita/zapitvane_form.php");?>

</div>
</div>
</div>
</div>
</div> 
</div>
</div>

<script type="text/javascript" src="<?=ASSETS_DIR?>assets/journal/journal.js"></script> 
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/<?=FB_LANG?>/sdk.js#xfbml=1&version=v2.5&appId=543199142507187";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
function hide(caller){
    $.get('/api/?command=get_page&n='+caller.id +'<?=isset($_GET['lang']) ? '&lang='.$_GET['lang'] : ''?> ', function(data, status){
                $('.opisanie').html("<p>"+data.textStr + "</p>");
                $("#product_code").html(data.Name);
                 });
};

</script>
