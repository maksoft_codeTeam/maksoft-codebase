<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title><?php echo("$Title");?></title>
<?php

include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/eurostandart/");
define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/Templates/eurostandart/images/");
define("DIR_MODULES","web/admin/modules/");

$o_site->print_sConfigurations();

//top menu
$top_menu = $o_page->get_pSubpages($o_site->get_sStartPage(), "sort_n ASC", "p.toplink = 1");

//news
$news = $o_page->get_pSubpages($o_site->get_sNewsPage(), "p.date_added DESC LIMIT 3");

//head_news
//$head_news = $o_page->get_pSubpages(157767, "p.date_added DESC LIMIT 5", "p.imageNo>0");
$head_news = $o_page->get_pGroupContent($o_page->get_pInfo("show_group_id", 157767));

//top news
$top_news = $o_page->get_pSubpages($o_site->get_sNewsPage(), "p.sort_n DESC LIMIT 15");

?>
<link href="http://www.maksoft.net/Templates/eurostandart/layout.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/eurostandart/base_style.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/eurostandart/contentslider.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/eurostandart/jScrollPane.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/eurostandart/ddsmoothmenu.css" rel="stylesheet" type="text/css">
<script src="http://www.maksoft.net/Templates/eurostandart/ddsmoothmenu.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "top_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	arrowimages: {},
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

function set_news_content(id)
	{
		var news_image = document.getElementById("img_"+id);
		var first_image = document.getElementById("img_0");
		first_image.style.display = "none";
		news_image.style.display = "block";

	}
function hide_news_content(id)
	{
		var news_image = document.getElementById("img_"+id);
		var first_image = document.getElementById("img_0");
		first_image.style.display = "block";
		news_image.style.display = "none";
	}
</script>

<script language="JavaScript" src="http://www.maksoft.net/lib/contentslider/contentslider.js" type="text/javascript"></script>
<script language="JavaScript" src="http://www.maksoft.net/lib/jquery/jScrollPane/jScrollPane.js" type="text/javascript"></script>
<script language="JavaScript" src="http://www.maksoft.net/Templates/eurostandart/effects.js" type="text/javascript"></script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23987855-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body class="bg">
<div align="center">
	<div id="page_container">
		<div id="header"><?php include "header.php"; ?></div>
        <?php
			if($o_page->n == $o_site->get_sStartPage())
				{
		?>


		<div id="banner" class="sliderwrapper">
            <div>
			<?php
				for($i=0; $i<count($head_news); $i++)
					//echo "<div class=\"contentdiv\"><a href=\"".$head_news[$i]['page_link']."\" title=\"\"><img src=\"".$head_news[$i]['image_src']."\" width=\"700\" alt=\"\"></a><div class=\"info\"><div class=\"content\"><h2>".$head_news[$i]['Name']."</h2>".strip_tags(crop_text($head_news[$i]['textStr']), "<p><b><em>")."</div><a href=\"".$head_news[$i]['page_link']."\" title=\"\" class=\"next_link\">��� ���</a></div></div>";
					echo "<div class=\"contentdiv\"><a href=\"".$head_news[$i]['page_link']."\" title=\"\"><img src=\"http://".$o_site->_site['primary_url']."/".$head_news[$i]['image_src']."\" alt=\"\"></a></div>";
			?>
			<!--
			<div class="contentdiv"><a href="<?=$o_page->get_pLink(150883)?>" title="<?=$o_page->get_pInfo("title", 150883)?>"><img src="Templates/eurostandart/images/banner.jpg" alt="<?=$o_page->get_pInfo("title", 150883)?>"></a></div>
            <div class="contentdiv"><a href="<?=$o_page->get_pLink(147953)?>" title="<?=$o_page->get_pInfo("title", 147953)?>"><img src="Templates/eurostandart/images/banner2.jpg" alt="<?=$o_page->get_pInfo("title", 150883)?>"></a></div>
            <div class="contentdiv"><a href="<?=$o_page->get_pLink(147952)?>" title="<?=$o_page->get_pInfo("title", 147952)?>"><img src="Templates/eurostandart/images/banner3.jpg" alt="<?=$o_page->get_pInfo("title", 150883)?>"></a></div>
            //-->
			</div>
            <div id="paginate-banner" class="pagination">
     		<? for($i=0; $i<count($head_news); $i++) echo '<a href="#" class="toc">'.($i+1).'</a>'; ?> 
        	</div>
        </div>
		<script type="text/javascript">
        
        featuredcontentslider.init({
            id: "banner",  //id of main slider DIV
            contentsource: ["inline", ""],  //Valid values: ["inline", ""] or ["ajax", "path_to_file"]
            toc: "markup",  //Valid values: "#increment", "markup", ["label1", "label2", etc]
            nextprev: ["Previous", "Next"],  //labels for "prev" and "next" links. Set to "" to hide.
            revealtype: "click", //Behavior of pagination links to reveal the slides: "click" or "mouseover"
            enablefade: [true, 0.1],  //[true/false, fadedegree]
            autorotate: [true, 3000]  //[true/false, pausetime]
        })
        
        </script> 		
		<?php
				}
		?>
		
		<div id="main">
			<div id="menu">
			<?php include "column_left.php"; ?>
			</div>
			<div class="content">
			<?php

			  if($o_page->n == $o_site->StartPage && strlen($_GET['search_tag'])<3)
				  {
					  include "home2.php";
				  }
			  else
				  include "main.php";
			?>
			</div>
		</div>
	</div>
	<br clear="all">
	<div id="footer"><?php include "footer.php"; ?></div>
	<div id="bottom"><hr>Eurostandart.org &copy; 2011 All Rights Reserved. / ������: <a href="http://www.pmd-studio.com" title="�������� � ��� ������"><b>pmd</b>studio</a></div>
</div>
<?php include "tracking_codes.php"; ?>
</body>
</html>
