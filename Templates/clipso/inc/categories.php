<div class="container">
<div class="row">
<?php 
	$n_categories = "169287";
	$categories = $o_page->get_pSubpages($n_categories);
	for($i=0; $i<count($categories); $i++) {
?>

<div class="col-md-6 featCat c-sm-quarter-2 c-xs-full floatLeft marginBtm10 posRelative">
	<picture>
		<source srcset="/<?=$o_page->get_pImage($categories[$i]['n'])?>">
		<img class="bkgdImage" src="/<?=$o_page->get_pImage($categories[$i]['n'])?>" alt="<?=$o_page->get_pName($categories[$i]['n'])?>">
	</picture>
	<div class="catText c-md-7 c-xs-half floatRight textCenter posAbsolute">
		<div class="centerThis">
			<h3 class="text18 textsemibold"><?=$o_page->get_pName($categories[$i]['n'])?></h3>
			<p class="text13 marginBtm10"><?=strip_tags($o_page->replace_shortcodes(cut_text($categories[$i]['textStr']), 20), "<p> <img>")?></p>
			<div class="btnMdWrapper layoutCenter">
				<a role="button" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" href="<?=$o_page->get_pLink($categories[$i]['n'])?>"><span class="button-shine"></span>Shop <?=$o_page->get_pName($categories[$i]['n'])?></a>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<img class="bkgdSlantRightSide" src="<?=TEMPLATE_DIR?>assets/img/categoryslant.svg" alt="">
</div>

<?php } ?>
	</div>
</div>
