<section class="mdl-shadow--4dp">
	<div class="container">
		<div class="row">

			<?php include TEMPLATE_DIR . "sidebar.php"; ?>

			<div class="col-md-9 col-sm-9">
				<div class="card-default mb-100 mdl-shadow--2dp">
					<div class="card-block">
						<h2 class="fs-25 mt-0">
										<span>
											<?=$o_page->get_pTitle("strict")?>
										</span>
									</h2>
					


						<p class="fs-16 fw-300">
							<?php
							//enable tag search / tag addresses
							if ( strlen( $search_tag ) > 2 ) {
								$o_page->print_search( $o_site->do_search( iconv( "UTF-8", "CP1251", $search_tag ), 10 ) );
								$keyword = $search_tag;
								include( "selected_sites.php" );
							}
							//enable site search
							elseif ( strlen( $search ) > 2 )
								$o_page->print_search( $o_site->do_search( iconv( "UTF-8", "CP1251", $search ), 10 ) );
							else {
								//print page content
								$o_page->print_pContent();
								echo "<br clear=\"all\">";
								$cms_args = array( "CMS_MAIN_WIDTH" => "100%" );
								//print page subcontent
								$o_page->print_pSubContent( NULL, 1, true, $cms_args );

								//print
								eval( $o_page->get_pPHPcode() );

								if ( $user->AccessLevel >= $row->SecLevel )
									include_once( "$row->PageURL" );
							}

							?>
						</p>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>