<?php
require_once __DIR__ .'/../fak/base.php';
//including function files
include_once("lib/lib_functions.php"); 
//DB functions
include_once("web/admin/query_set.php");

//Input/Output functions
include_once("web/admin/io.php");

// shortcodes manipulation
include_once("shortcodes/main/shortcodes.php");
if(isset($wwo) && is_object($wwo)) {
    include_once("shortcodes/wwo/shortcodes.php");
}

$o_page->_page['scope'] = "head";

if(defined("SHORTCODES_FILE") && (file_exists("SHORTCODES_FILE"))) {
    include_once("SHORTCODES_FILE");
}

$noindex=false;
if($o_page->_page['SecLevel']>0) $noindex=true;

$keywords = '';

function getplaintextintrofromhtml($html, $numchars) {
    // Remove the HTML tags
    $html = strip_tags($html);
    // Convert HTML entities to single characters
    $html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
    // Make the string the desired number of characters
    // Note that substr is not good as it counts by bytes and not characters
    $html = mb_substr($html, 0, $numchars, 'UTF-8');
    // Add an elipsis
    $html .= "...";
    return $html;
}

// description
$description= $Site->Description;

// keywords

$row=get_page($n);
if(strlen($row->tags)<10) {
	$row->tags=$row->tags.$row->title;
}
 if ($row->tags<>'') {
	$row->tags=str_replace(" \,", "\,",  $row->tags); 
	$keywords = $o_page->replace_shortcodes($row->tags);
} 
/*
else {
$keywords=str_replace(" ,", ",", $Site->keywords);   
} 
*/

// if(strlen($Site->keywords)<10) { $Site->keywords=$Site->title; }

if($Site->site_status>=0) {
	//if($n!=$Site->StartPage) {
		$description = $o_page->get_pText();
        $description = strip_shortcodes($description);
		$description = str_replace(array("\n", "\t", "\r"), '', $description);
        $description= htmlspecialchars(strip_tags($description));
    //}

	if(isset($search_tag) && strlen($search_tag)>5)  {
	  $keywords = iconv("UTF-8","CP1251",$search_tag); 
	  $description = iconv("UTF-8","CP1251",$search_tag); 
	  $taged_pages = $o_site->do_search(iconv("UTF-8","CP1251",$_GET['search_tag']));
	  if(count($taged_pages) > 0) {
	  
		  foreach($taged_pages as $taged_page) {
			//$description .= $taged_page['Name'].",".$taged_page['title'].",";
			
			$keywords = $keywords ." ".$taged_page['Name'].",".$taged_page['title'].",";
		 }
		}
	   else {
	   	$noindex = true; 
	   }
	 
	$description=  strip_tags(str_replace("\"", "", $description)); 
	if(strlen($description)<10) {$description = rtrim("$description $row->tags $Site->Description")." ... "; }
}
    if(array_key_exists('comment_subject', $o_page->_page)){
        $description .= $o_page->_page['comment_subject']; 

    }
	//if(strlen($description)>160) $description = rtrim(substr($description, 0, 160));  
	if(strlen($description)>180) $description = cut_text($description, 180, "...");

    $description = $o_page->replace_shortcodes($description);
    $description = strip_tags($description);
    $description = getplaintextintrofromhtml($description, 100);

}
$date_expires = new DateTime();
$date_expires->add(new DateInterval('P0DT2H30M5S'));  // cashe the CONTENT for 2 HOURS    date format must be RFC1123

// author
if($o_page->_page['author']>0) {
	$author_q = $o_page->db_query("SELECT * FROM user_authorship WHERE uID = ".$o_page->_page['author']." AND (user_authorship.SiteID = 0 OR  user_authorship.SiteID = '".$o_page->SiteID."') ORDER BY uaID DESC LIMIT 1"); 
	$row_author = mysqli_fetch_object($author_q); 
} else {
    $row_author = new stdClass();
}
$primary_url = $_SERVER['HTTP_HOST'];
?>
<meta http-equiv="Content-Language" content="<?php echo("$Site->language_key"); ?>" />
<meta http-equiv="CACHE-CONTROL" content="PUBLIC" />
<meta http-equiv="EXPIRES" content="<?php echo $date_expires->format("D, d M Y H:i:s O"); ?>" />
<?php if( ($o_page->get_uInfo("AccessLevel")==0) && (strlen($o_page->_site['primary_url'])>7) ) echo("<base href=\"".$o_page->scheme.$o_page->_site['primary_url']."\" />"); ?> 
<meta name="resource-type" content="document" />
<meta name="copyright" content="Copyright (C) 2004-<?php echo date("Y"); ?>" />
<meta name="webmaster" content="<?php echo("$Template->webmaster");  ?>" />
<?php 
// IF NO CONTENT- canonical rel to PREVIOUS PAGE
if( (strlen($row->textStr)<256)  && (strlen($row->tags)<3)  && (strlen($row->title)<3)  && ($row->n <> $Site->StartPage)  && ($row->ParentPage <> $Site->StartPage) ) {
	echo('<link rel="canonical" href="'.$o_page->get_pLink($row->ParentPage).'" />'); 
}

$primary_server_name = preg_replace("/^w+\./", "",  $o_page->_site['primary_url']); 
$current_server_address = preg_replace("/\/$/", "", $o_page->scheme.$_SERVER["HTTP_HOST"].iconv("UTF-8","CP1251",urldecode($_SERVER['REQUEST_URI'])) );
echo("<!-- ***  ".$o_page->get_pLink()."<> $current_server_address //-->");
// IF URL address diferrs NOindex
//if( (stristr($_SERVER['SERVER_NAME'], $primary_server_name) != $primary_server_name) && ($o_page->_site['primary_url']<>'') ) {
if($o_page->SiteID<>$o_page->_page['SiteID']) {
	$noindex=true;
	echo("<!--".$o_page->SiteID."<>".$o_page->_page['SiteID'].$o_page->get_pLink() ."<> ".$o_page->scheme.$_SERVER['HTTP_HOST'].iconv('UTF-8','CP1251',urldecode($_SERVER['REQUEST_URI']))."-->");
} else {
	if ( ($o_page->get_pLink() <> $current_server_address ) && (strlen($search_tag)<2)  ) {
		//if($o_page->n <> $Site->StartPage) $noindex = true;
		echo('<link rel="canonical" href="'.$o_page->get_pLink($row->n).'" />');
	}
	//echo "<!--".$o_page->_site['primary_url']."   ".$_SERVER['SERVER_NAME']."-->"; 
}

//if (($row->SecLevel>0) || ($row->SiteID != $SiteID) || !(eregi($o_page->_site['primary_url'],$_SERVER['SERVER_NAME']))   ) {
//		echo('<link async  rel="canonical" href="'.$o_page->get_pLink($row->n).'" >'); 
//}

//echo "<!-- ".$o_page->get_pLink($n, $SiteID, "")."<>".$current_server_address."  //-->"; 


if($noindex) {
//$o_page->pr--; 
?>
<meta name="distribution" content="iu" />
<meta name="robots" content="noindex,follow" />
<?php 
} else {
$noindex=false; 
?>
<meta name="keywords" content="<?=strip_tags($keywords)?>" />
<meta name="description" content="<?=strip_tags($description) ?>" />
<meta name="coverage" content="Worldwide" />
<meta name="language" content="<?php echo("$Site->language_key"); ?>" />
<!-- Facebook  Open Graph Meta Data -->          
<meta property="og:title" content="<?php echo $o_page->_page['Name']." - ".$description; ?>" />
<meta property="og:type" content="article" />
<meta property="og:image" content="<?php echo $o_page->scheme.$o_page->_site['primary_url']."/".$o_page->get_pImage($n);  ?>" />
<meta property="og:url" content="<?php echo $o_page->get_pLink($o_page->n, $o_page->SiteID, "", true); ?>" />
<meta property="og:description" content="<?php echo $description ?>" />
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="robots" content="index,follow,all,noarchive" />
<link  rel="alternate" type="application/rss+xml" title="<?=$o_page->_site['Title']?>" href="<?=$o_page->scheme.$o_page->_site['primary_url']?>/rss.php" />
<?php if(defined("FAVICON_SRC")) { 
	echo "<link rel='shortcut icon' type='image/x-icon' href='".FAVICON_SRC."' />";
} ?>
<?php 
// <meta name="classification" content="Regional, Europe, Bulgaria, Business and Economy" />
    if(isset($row_author->author_links)){
        echo $row_author->author_links; 
    }
}

//if($o_page->get_pLink($o_page->n) <> $_SERVER['SERVER_NAME'].$_SERVER['SERVER_URI']) 
//	if(strlen($o_page->get_pLink($o_page->n))>12)
//	echo('<link async  rel="canonical" href="'.$o_page->get_pLink($o_page->n).'" />'); 
// ne raboti adekvatno s tag adresi 
?>
<?php 
	 $mem = round((memory_get_peak_usage()/1024)/1024,2); 
	echo("<!--  SiteID=$SiteID  n=$n PR:".$o_page->pr." $mem Mb  ".$o_page->Rub."-->\r\n");

//	if($row->tags<>'') echo("<!-- $row->tags -->"); 
	// <link async  rel="shortcut icon" href="http://127.0.0.1/stable/images/favicon.ico" />
?>
<script type="text/javascript" src="/lib/lib_functions.js" ></script>
<?php
//load ajax library
//AJAX library does not work with the OLD TEXT editor
//&& $row->n != 132

if($row->ParentPage != 11 && $row->n != 111 && $row->n != 132 && (($o_page->_page['make_links'] == 4) || (stristr($o_page->_page['PHPcode'], "photos")>0) || (isset($hotel_id) and $hotel_id >0) )
)
{
    if($o_site->SiteID!="1007"){
	?>

    <!-- new gallery --!>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.0.0.min.js"></script>
    <script async type="text/javascript" src="/web/assets/fresco-2.2.1/js/fresco.js"></script>
    <link async  rel="stylesheet" type="text/css" href="/web/assets/fresco-2.2.1/css/fresco.css"/>
    <!-- end --!>
	<?php
		}
}
?>
<?php
if(isset($head_script) and $head_script<>"" and is_string($head_script)) {
 echo("$head_script"); 
}
?>
<!-- load FLASH library to prevent BORDER arround flash //-->
<script src="/lib/lib_flash.js" type="text/javascript"></script>
<!-- load jQuery library//-->
<!--<script async  src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.0/jquery.js"></script>//-->
<!--<script async type="text/javascript" language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>//-->
<?php
if(($o_site->SiteID)=="961"){
	if((($o_page->n)=="220915" && isset($_GET["KURS_ID"])) || (($o_page->n)=="220915" && isset($_GET["TWOWAY"]))){
		?>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<?php
	}else{
		?>
		<?php /*?><script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script><?php */?>
		
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"   integrity="sha256-SOuLUArmo4YXtXONKz+uxIGSKneCJG4x0nVcA0pFzV0="   crossorigin="anonymous"></script>
		<?php
		
	}
}else{
	?>
<!--        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"   integrity="sha256-SOuLUArmo4YXtXONKz+uxIGSKneCJG4x0nVcA0pFzV0="   crossorigin="anonymous"></script>
	<?php
}
?>


<script  type="text/javascript" src="/lib/jquery/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script  type="text/javascript" src="/lib/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/lib/jquery/jquery.cookie.js"></script>
<script type="text/javascript" >
	//no conflict with Prototype Lib
	var $j = jQuery.noConflict();
</script>
	<!--<link href="<?=$o_page->scheme;?>www.maksoft.net/css/admin_classes.css" rel="stylesheet" type="text/css" />-->
    <link  href="/css/admin_classes.css" rel="stylesheet" type="text/css" />
<?php
$googleVerify = include(__DIR__.'/../global/google/verify.php');
echo $googleVerify;
$googleAnalytics = include(__DIR__.'/../global/google/analytics.php');
echo $googleAnalytics;
$facebookPixelCode = include(__DIR__.'/../global/facebook/pixel.php');
echo $facebookPixelCode;
?>
    <link rel="stylesheet" type="text/css" href="/lib/jquery/fancybox/jquery.fancybox-1.3.4.css" media="screen"/>
<?php if(defined('CMS_IMPORT_CSS') || $user->AccessLevel >10) { ?> <link href="<?=(defined("CMS_IMPORT_CSS")) ? CMS_IMPORT_CSS:"#"?>" rel="stylesheet" type="text/css" /> <?php }?>
<!--
<script async language="javascript" type="text/javascript">
	$j(document).ready(function(){
			$j('#page_stats').load("http://www.maksoft.net/web/admin/includes/page_stats.php");
		})
</script>
<div id="page_stats"></div>
//-->			
<?php
  //show admin navigation
  $currentTempl = $o_site->_site["Template"];
	if( ($user->AccessLevel>=2) && ($o_page->_page['SiteID'] == $o_site->_site['SitesID']) && ($currentTempl!="257")) include "web/admin/admin_navigation.php";	
	//if(($user->ID==196 || $user->ID==1 || $user->ID==625 || $user->ID==67) && ($o_page->_page['SiteID'] == $o_site->_site['SitesID'])) include "web/admin/admin_navigation.php";

$o_page->_page['scope'] = "body";


?>
<?php /*?><link async  rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"><?php */?>
<link async rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link async rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
<script async src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "domain": "<?php echo $o_page->_site['primary_url'];?>",
  "palette": {
    "popup": {
      "background": "#edeff5",
      "text": "#838391"
    },
    "button": {
      "background": "#c4161c"
    }
  },
  "content": {
    "message": "���� ���� �������� \"���������\", �� �� \n������ ��������� �� ���.", /* ����� �� ��������, title */
    "dismiss": "�������", /* ���� �� ���������� ����� */
    "link": "������� ���", /* ������������ �� ������� ��� ��� ���� �� ���������� �����*/
    "href": "/page.php?n=19361847&SiteID=<?=$o_page->SiteID?>" /* �� ���� �� ������ */
  }
})});
</script>

<?php if(defined("SITE_MAINTENANCE")) { 
	include(__DIR__.'/../global/maintenance/sticky_bar.php');
} ?>
<link href="/web/assets/toastr/toastr.min.css" rel="stylesheet" type="text/css" />
