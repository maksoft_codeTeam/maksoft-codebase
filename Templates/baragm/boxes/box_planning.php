<?php

	$vip_links = $o_page->get_pGroupContent($group_vip_links);
	if(count($vip_links) > 0 && !DEMO_MODE)
		{
			?>
            <div class="wrap planning">
                <div class="container">
                    <div class="row">
                        <?php
                            for($i=0; $count=count($vip_links), $i<$count; $i++)
                                {
                                    ?>
                                    <div class="span3 button">
                                        <a href="<?=$o_page->get_pLink($vip_links[$i]['n'])?>" class="btn_hvr">
                                            <span class="star"></span>
                                            <h2><?=$vip_links[$i]['Name']?></h2><p><?=cut_text(strip_tags($vip_links[$i]['textStr']))?></p>
                                            <div class="clear"></div>
                                        </a>						
                                    </div>                                
                                    <?	
                                }      
                        ?>
                    </div>
                </div>
            </div>
            <?
		}
		
?>
