<?php
	//$n_featured_works is defined in languages/.php
	
	$items = $o_page->get_pSubpages($n_featured_works, "p.sort_n ASC LIMIT 20", "p.imageNo > 0");
	if(count($items) > 0 && !DEMO_MODE)
		{
			?>
            <div class="wrap block" id="featured-works">
                <div class="container">
                    <div class="row">
                        <div class="span12 title2">
                        <h2 class="title3"><span><?=$o_page->get_pName($n_featured_works)?></span></h2>
                      	<ul id="mycarousel" class="jcarousel-skin-tango featured-works">
                        <?php
                            for($i=0; $count=count($items), $i<$count; $i++)
                                {
                                    ?>
                                      <li>
                                          <div class="prbl_1">
                                              <div class="hover_img">	
                                                  <img src="<?=HTTP_SERVER?>/img_preview.php?image_file=<?=$items[$i]['image_src']?>&amp;ratio=strict" alt="">
                                              </div>
                                          </div>
                                      </li>                                                                
                                    <?	
                                }      
                        ?>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?
		}
	else include TEMPLATE_DIR . "demo/box_featured_works.html";
?>