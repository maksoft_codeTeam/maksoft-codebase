	<!--page_container-->
    <div class="page_container">
        
        <!--slider-->
		<?php include TEMPLATE_DIR . "boxes/box_banner_slider.php"; ?>
        <!--//slider-->
               
        <!--planning-->
		<?php include TEMPLATE_DIR . "boxes/box_planning.php"; ?>
        <!--//planning-->
  <!--MAIN CONTENT AREA-->
  <div class="wrap">
      <div class="container">
          <div class="row pad25">
              <div class="span12" id="pageContent">
              <h1 class="title"><?=$o_page->print_pTitle("strict")?></h1>
              <?php
				//enable tag search / tag addresses
				if(strlen($search_tag)>2)
					{
						$o_page->print_search($o_site->do_search(iconv("UTF-8","CP1251",$search_tag), 10)); 				
					}
				//enable site search
				elseif(strlen($search)>2)
					$o_page->print_search($o_site->do_search($search, 10)); 
				else
					{	
						  $o_page->print_pContent();
						  $cms_args = array("CMS_MAIN_WIDTH" => "100%");
						  eval($o_page->get_pInfo("PHPcode"));
						  if ($user->AccessLevel >= $row->SecLevel)
							  include_once("$row->PageURL");
					}
			  ?>
             
              </div>
          </div>
      <br clear="all">
      <?php $o_page->print_pNavigation()?>
      </div>
      
  </div>
<!--//MAIN CONTENT AREA-->
        <div class="separator"></div>
        <!--//Welcome-->

        <!--featured works-->
        <?php include TEMPLATE_DIR . "boxes/box_clients.php"; ?>
        <!--//featured works-->
		
        <div class="wrap navbar">
            <div class="container">
            <?php $o_page->print_pNavigation()?>
            </div>
        </div>		
       
    </div>
    <!--//page_container-->