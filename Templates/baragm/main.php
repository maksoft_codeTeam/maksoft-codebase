<!--page_container-->
<div class="page_container">
  <div class="breadcrumb">
      <div class="wrap">
          <div class="container">
              <?php $o_page->print_pNavigation()?>
          </div>
      </div> 
  </div>
  <!--MAIN CONTENT AREA-->
  <div class="wrap">
      <div class="container">
          <div class="row pad25">
              <div class="span12" id="pageContent">
              <h1 class="title"><?=$o_page->print_pTitle("strict")?></h1>
              <?php
				//enable tag search / tag addresses
				if(strlen($search_tag)>2)
					{
						$o_page->print_search($o_site->do_search(iconv("UTF-8","CP1251",$search_tag), 10)); 				
					}
				//enable site search
				elseif(strlen($search)>2)
					$o_page->print_search($o_site->do_search($search, 10)); 
				else
					{	
						  $o_page->print_pContent();
						  $cms_args = array("CMS_MAIN_WIDTH" => "100%");
						  $o_page->print_pSubContent();
						  eval($o_page->get_pInfo("PHPcode"));
						  if ($user->AccessLevel >= $row->SecLevel)
							  include_once("$row->PageURL");
					}
			  ?>
             
              </div>
          </div>
      </div>
      
  </div>
<!--//MAIN CONTENT AREA-->
  
</div>
<!--//page_container-->