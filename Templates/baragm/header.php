<!DOCTYPE html>
<html lang="<?php echo($Site->language_key); ?>">
<head>
<meta charset="windows-1251">
<title><?=$Title?></title>
<?php
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<link href="<?=TEMPLATE_PATH?>css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" id="camera-css"  href="<?=TEMPLATE_PATH?>css/camera.css" type="text/css" media="all">
<link href="<?=TEMPLATE_PATH?>css/bootstrap.css" rel="stylesheet">
<link href="<?=TEMPLATE_PATH?>css/theme.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_PATH?>css/skins/tango/skin.css" />
<link href="<?=TEMPLATE_PATH?>css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?=TEMPLATE_PATH?>css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="shortcut icon" type="image/x-icon" href="<?=TEMPLATE_PATH?>img/fav.ico">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->    
</head>
<body>

	<!--header-->
    <div class="header" <?=($o_page->n != $o_site->get_sStartPage() || strlen($search_tag)>2 || strlen($search)>2)?"style=\"position:static;\"":""?>>
    	<div class="wrap">
        	<div class="navbar navbar_ clearfix">
              <div class="logo"><a href="<?=$o_page->get_pLink($o_site->get_sStartPage())?>"><img src="<?=$tmpl_config['default_logo']?>" alt="<?=$o_site->_site['Title']?>" width="<?=$tmpl_config['default_logo_width']?>" /></a></div>
              <div class="menu_wrapper navbar navbar_ clearfix">
                            	  <!--<div id="languages"><? $o_page->print_sVersions();?></div>-->
                                  <div id="home"><a href="http://<?=$o_site->get_sURL()?>"><i class="fa fa-home fa-2"></i></a></div>
                 <div class="socicons">
                 <div class="langen"><? $o_page->print_sVersions();?></div>
                 <a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>" class="btn btn-social-icon btn-sm btn-facebook" target="_blank" onclick="_gaq.push(['_trackEvent', 'btn-social-icon', 'click', 'btn-sm']);"><i class="fa fa-facebook"></i></a>
                  </div>
                  <nav id="main_menu">
                  <?php
	if(!isset($drop_links)) $drop_links = 2;
	$dd_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $drop_links");
	if(count($dd_links) > 0)
		{
			?>

                      <div class="menu_wrap">
                       <?php print_menu($dd_links, array('menu_depth'=>1, 'params'=>"class=\"nav sf-menu\""))?>
                      </div>

                      			<?
		}
?>
                  </nav>
              </div>
              <div class="clear"></div>
            </div>
        </div>    
    </div>
<!--//header-->