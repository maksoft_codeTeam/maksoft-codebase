                <!-- FOOTER STARTS HERE -->
                <footer id="footer">
                    <div class="footer-top"></div>
                    <div class="footer-wrapper">
                        <div class="container">
                            <div class="row show-grid">
                                <div class="col-sm-12 col-sm-12 col-md-12">
                                    <div class="row show-grid">
                                        <!-- FOOTER: LOGO -->
                                        <div class="col-sm-4 col-md-4">
                                            <img alt="" class="footer-logo img-responsive" src="http://maksoft.net/Templates/aris-responsive/images/logo_aris.png" width="200px" />
                                            <!--<img alt="" class="footer-logo img-responsive" src="<?=DIR_TEMPLATE?>assets/img/SGS_ISO-9001_with_UKAS_TCL.png" width="200px" />-->

                                        </div>
                                        <!-- FOOTER: ABOUT US -->
                                        <div class="col-sm-4 col-md-4 footer-center">
                                            <h4 class="center-title">������ � ���</h4>
                                            <br />
                                            <!-- FOOTER: ADDRESS --> <address class="address">

                                                <p><i class="fa fa-map-marker" aria-hidden="true"></i>��������� ����</p>
                                                
                                                <p><i>&nbsp;</i>1632 �����</p>

                                                <p><i>&nbsp;</i>��.���������� �66</p>

                                                <p><i class="fa fa-phone" aria-hidden="true"></i>02/ 93 79 555/500; 089 996 1101; 089 850 9502</p>

                                                <p><i class="fa fa-print" aria-hidden="true"></i>����: 02/ 9379 505</p>
                                                
                                                <p><i class="fa fa-envelope" aria-hidden="true"></i>aris@aris-bg.com</p>

                                            </address>
                                        </div>
                                        <!-- FOOTER: NAVIGATION LINKS -->
                                        <div class="col-sm-4 col-md-4 footer-right">
                                            <h4 class="center-title">���������</h4>
                                            <ul class="footer-navigate">
                    <?php
						echo "".$top_links."";
					?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <div class="footer-bottom">
                        <div class="container">
                            <div class="row show-grid">
                                <!-- FOOTER: COPYRIGHT TEXT -->
                                <div class="col-sm-12 col-md-12">
                                    <p><?=$o_page->get_sCopyright()?> 
                                    <span class="copyrights">��� ������ � ��������� 
                                    <a href="http://maksoft.net" title="��� ������ � ��������� �������" target="_blank"><strong>�������</strong></a> 
                                    & 
                                    <a href="http://seo.maksoft.net" title="SEO ����������� �� Google">SEO</a> 
                                    <a href="<?=$o_page->get_pLink(38146)?>" title="SEO ����������� � ��������� �� �������� �������">
                                    <strong>NetService</strong>
                                    </a></span></p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- END FOOTER -->