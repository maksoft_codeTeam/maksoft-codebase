<section class="home-header">
	<img src="<?=TEMPLATE_DIR?>assets/img/about/home-header.png" alt=""/>
</section>
<section class="help-us-area pt-150 pb-80">
	<div class="container">
		<div class="row">
			<div id="container" class="clearfix masonry">
			
		<div class="row">
				<div class="imagebox photo col1 masonry-brick">
					<a href="<?=$o_page->get_pLink(19366106)?>" title="<?=$o_page->get_pName(19366106)?>" class="boxhref">
      		<img src="<?=TEMPLATE_DIR?>assets/img/homepage/boi-lakove-grundove.jpg" alt="<?=$o_page->get_pName(19366106)?>">
		  <div class="boxtitle"><?=$o_page->get_pName(19366106)?></div>
      </a>
				

				</div>
				<div class="imagebox photo col3 masonry-brick">
					<a href="<?=$o_page->get_pLink(19366107)?>" title="<?=$o_page->get_pName(19366107)?>" class="boxhref">
      <img src="<?=TEMPLATE_DIR?>assets/img/homepage/baicove.jpg" alt="<?=$o_page->get_pName(19366107)?>">
      <div class="boxtitle"><?=$o_page->get_pName(19366107)?></div>
      </a>
				
				</div>
				</div>
				
				<div class="row">
				<div class="imagebox photo col3 masonry-brick">
					<a href="<?=$o_page->get_pLink(19367235)?>" title="<?=$o_page->get_pName(19367235)?>" class="boxhref">
      <img src="<?=TEMPLATE_DIR?>assets/img/homepage/filters.jpg" alt="<?=$o_page->get_pName(19367235)?>">
      <div class="boxtitle"><?=$o_page->get_pName(19367235)?></div>
      </a>
				
				</div>
				<div class="imagebox photo col1 masonry-brick">
					<a href="<?=$o_page->get_pLink(19366108)?>" title="<?=$o_page->get_pName(19366108)?>" class="boxhref">
      <img src="<?=TEMPLATE_DIR?>assets/img/homepage/abrasives.jpg" alt="<?=$o_page->get_pName(19366108)?>">
      <div class="boxtitle"><?=$o_page->get_pName(19366108)?></div>
      </a>
				
				</div>
				</div>
				
				<div class="row">
				<div class="imagebox photo col1 masonry-brick">
					<a href="<?=$o_page->get_pLink(19366110)?>" title="<?=$o_page->get_pName(19366110)?>" class="boxhref">
      <img src="<?=TEMPLATE_DIR?>assets/img/homepage/machines.jpg" alt="<?=$o_page->get_pName(19366110)?>">
      <div class="boxtitle"><?=$o_page->get_pName(19366110)?></div>
      </a>
				
				</div>
				<div class="imagebox photo col3 masonry-brick">
					<a href="<?=$o_page->get_pLink(19366111)?>" title="<?=$o_page->get_pName(19366111)?>" class="boxhref">
      <img src="<?=TEMPLATE_DIR?>assets/img/homepage/cars.jpg" alt="<?=$o_page->get_pName(19366111)?>">
      <div class="boxtitle"><?=$o_page->get_pName(19366111)?></div>
      </a>
				
				</div>
				</div>

			</div>
		</div>
	</div>
</section>

<section class="pt-90">
	<div class="container">
		<div class="row">
<?php 
$o_page->print_pContent();

if($user->ID > 0) { echo "<div class=\"pt-60\">".$nav_bar."</div>"; }		 
?>
		</div>
	</div>
</section>

<div class="pt-90">
	<?php include TEMPLATE_DIR . "inc/our-partners.php"; ?>
</div>