            <!-- Start page title -->
            <section class="page-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="breadcumb-area">
                                <ol class="breadcrumb">
                                    <?="".$nav_bar.""?>
                                </ol>
                            </div>
                            <div class="page-title">
								<h1><span><?=$o_page->get_pTitle("strict")?></span></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End page title -->