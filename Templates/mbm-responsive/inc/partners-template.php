<section class="our-blog-area pt-90 pb-60">
		<div class="container">
			<div class="row">
				<div class="blog">
				
						  <?php 
						  $partners = $o_page->get_pSubpages(19363708);
							for($i=0; $i<count($partners); $i++)
							{
							?>
							<div class="col-md-4">
								<div class="column span-12" data-cols="1">
									<div class="ht-brand">
										<div class="single-brand">	
											<a href="<?=$o_page->get_pLink($partners[$i]['n'])?>">										
												<img width="100%" height="26" src="<?=$partners[$i]['image_src']?>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="<?=$o_page->get_pName($partners[$i]['n'])?>" />		
											</a>									
										</div>
									</div>
								</div>
							</div>
							<?																
							}
							?>

				</div>
			</div>
		</div>
</section>