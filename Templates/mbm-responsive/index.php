<?php
require_once __DIR__ . '/../../modules/vendor/autoload.php';
require_once __DIR__ . '/../../global/recaptcha/init.php';
date_default_timezone_set( 'Europe/Sofia' );

define( "TEMPLATE_NAME", "mbm-responsive" );
define( "TEMPLATE_DIR", "Templates/" . TEMPLATE_NAME . "/" );
define( "TEMPLATE_IMAGES", "http://www.maksoft.net/" . TEMPLATE_DIR . "images/" );

include TEMPLATE_DIR . "header.php";
$pTemplate = $o_page->get_pTemplate( $n, 294 );
if ( $o_page->_page[ 'SiteID' ] != $o_site->_site[ 'SitesID' ] )
	include TEMPLATE_DIR . "admin.php";
elseif ( $o_page->_page[ 'n' ] == $o_site->_site[ 'StartPage' ] && strlen( $search_tag ) < 3 && strlen( $search ) < 3 )
	include TEMPLATE_DIR . "home.php";
else {
	include TEMPLATE_DIR . "main-h.php";
	if ( $pTemplate[ 'pt_url' ] ) {
		include $pTemplate[ 'pt_url' ];

	} else {
		include TEMPLATE_DIR . "main.php";
	}
	if($o_page->_page['n']==19363708) {
	}else {
	include TEMPLATE_DIR . "inc/our-partners.php";
	}
}

include TEMPLATE_DIR . "footer.php";

?>