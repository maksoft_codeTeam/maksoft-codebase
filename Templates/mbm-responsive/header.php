<!doctype html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="<?php echo($Site->language_key); ?>" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="<?php echo($Site->language_key); ?>" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="<?php echo($Site->language_key); ?>" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="<?php echo($Site->language_key); ?>" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="<?php echo($Site->language_key); ?>">
<head>
<meta charset="windows-1251">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?=$Title?></title>
<?php
require_once "lib/lib_page.php";
require_once "lib/Database.class.php";
//include meta tags
include( "Templates/meta_tags.php" );
$o_site->print_sConfigurations();

$db = new Database();
$o_page->setDatabase( $db );
$o_user = new user( $user->username, $user->pass );

$linebottom = "<img src='".TEMPLATE_DIR."assets/img/title-bottom.png' alt='' />";
?>

<link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed" rel="stylesheet">
<link rel="stylesheet" href="<?=TEMPLATE_DIR?>assets/master.css">
<script src="<?=TEMPLATE_DIR?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
        
</head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		
		<!-- Start main area -->
        <div class="main-area">
        	<!-- Start header -->
        	<header>
        		<!-- Start header top -->
        		<div class="header-top">
        			<div class="container">
        				<div class="row">
        					<div class="col-sm-5 col-md-6">
	                            <div class="top-social">
	                                <nav>
	                                	<ul>
<!--		                                    <li><a href="#">��������� ��:</a></li>-->
		                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
		                                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
		                                    <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
		                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
		                                    <li><a href="mailto:mbm@mbm.bg"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
		                                </ul>
	                                </nav>
	                            </div>
	                        </div>
	                        <div class="col-sm-7 col-md-6">
	                            <div class="call-to-action">
	                                <nav>
	                                	<ul>
							<?php
							if ( !isset( $top_links ) )$top_links = 1;
							$top_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $top_links" );

							for ( $i = 0; $i < count( $top_links ); $i++ ) {
								$subpages = $o_page->get_pSubpages( $top_links[ $i ][ 'n' ] );
							?>
							<li class="top_links" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
							<a href="<?php echo $o_page->get_pLink($top_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <?php echo $top_links[$i]['Name'] ?></a>
							</li>
							<?php 
							}
							?>
		                                	<li><a href="tel:+359888809965"><i class="fa fa-phone" aria-hidden="true"></i> +359 888 809 965</a></li>
<!--		                                	<li><a href="#">BG <i class="fa fa-angle-down" aria-hidden="true"></i></a>
												<ul>
													<li><a href="#">ENGLISH</a></li>
												</ul>
		                                	</li>-->
		                                </ul>
	                                </nav>
	                            </div>
	                        </div>
        				</div>
        			</div>
        		</div>
        		<!-- End header top -->
        		<!-- Start main menu area -->
        		<div class="main-menu-area" id="sticker">
        			<div class="container">
        				<div class="row">
        					<div class="col-sm-2">
        						<div class="logo ptb-32">
        							<a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>">
        								<img src="<?=TEMPLATE_DIR?>assets/img/logo-zona.png" width="160px" alt="">
        							</a>
        						</div>
        					</div>
        					<div class="col-sm-10">
        						<div class="main-menu">
        							<nav>
        								<ul>
											<!--<li><a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>">������</a></li>-->
        									
							<?php
							if ( !isset( $dd_links ) )$dd_links = 3;
							$dd_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $dd_links" );

							for ( $i = 0; $i < count( $dd_links ); $i++ ) {
								$subpages = $o_page->get_pSubpages( $dd_links[ $i ][ 'n' ] );
								$menuicon="";
        						eval($dd_links[$i]["PHPvars"]);
							?>
							<li <?php echo ((count($subpages)> 0) ? "" : "") ?>>
							<img src="<?php echo $menuicon; ?>" alt="<?php echo $dd_links[$i]['Name'] ?>" /><a href="<?php echo $o_page->get_pLink($dd_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <?php echo $dd_links[$i]['Name'] ?></a>
								<?php /*echo ((count($subpages) > 0) ? "<ul class=\"drop-menu\">" : "</li>")*/ ?>
								<?php
								/*if ( count( $subpages ) > 0 ) {
									for ( $j = 0; $j < count( $subpages ); $j++ )
										echo "<li><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\">" . $subpages[ $j ][ 'Name' ] . "</a></li>";*/
									?>
						<!--</ul>-->
						</li>
							<?php 
								/*}*/
							}
							?>
        									
        								</ul>
        							</nav>
        						</div>
<!--        						<div class="donate-button ptb-32">
        							<a class="waves-effect waves-light" href="#zapitvane" data-toggle="modal">��������� ���������</a>
        						</div>-->
        					</div>
        				</div>
        			</div>
        		</div>
        		<!-- End main menu area -->
                <!-- Start mobile menu -->
                <div class="mobile-menu-area">
                    <div class="container">
                        <div class="row">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul class="nav">
                                    
							<?php
							if ( !isset( $mobile_links ) )$mobile_links = 3;
							$mobile_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $mobile_links" );

							for ( $i = 0; $i < count( $mobile_links ); $i++ ) {
								$subpages = $o_page->get_pSubpages( $mobile_links[ $i ][ 'n' ] );
							?>
							<li <?php echo ((count($subpages)> 0) ? "" : "") ?>>
							<a href="<?php echo $o_page->get_pLink($mobile_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <?php echo $mobile_links[$i]['Name'] ?></a>
								<?php echo ((count($subpages) > 0) ? "<ul>" : "</li>") ?>
								<?php
								if ( count( $subpages ) > 0 ) {
									for ( $j = 0; $j < count( $subpages ); $j++ )
										echo "<li><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\">" . $subpages[ $j ][ 'Name' ] . "</a></li>";
									?>
						</ul>
						</li>
							<?php 
							} 
							}
							?>
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End mobile menu -->
        	</header>
        	<!-- End header -->