<?php
//TITLES
define("TITLE_WELCOME", "Welcome");
define("TITLE_CONTACTS", "Contacts");

define("N_CONTACTS", 97017);
define("N_SITEMAP", 96995);
define("N_ABOUT", 97018);
define("N_ROOMS", 96636);
define("N_AMENITIES", 96472);
define("N_GALLERY", 96624);
define("N_SEND_PHOTO", 97289);

	//form 
	define("FORM_MESSAGE_VALID_EMAIL", "please enter valid e-mail address !");
	define("FORM_MESSAGE_MUST_CONTAIN_NUMBERS", "must contain numbers !");
	define("FORM_MESSAGE_MUST_CONTAIN_NUMBER_BETWEEN", "must contain numbers between");
	define("FORM_MESSAGE_REQUESTED_FIELD", "is a required field !");
	define("FORM_MESSAGE_ALL_REQUESTED_FIELD", "All fields marked with * are mandantory! Please fill them:");
	
	define("FORM_LABEL_NAME", "name");
	define("FORM_LABEL_EMAIL", "e-mail");
	define("FORM_LABEL_PHONE", "phone");
	define("FORM_LABEL_COMMENTS", "comments");
	define("FORM_LABEL_CODE", "secure code");
	
	define("FORM_BUTTON_SUBMIT", "send");
	
	//messages
	define("MESSAGE_FORM_FIELDS_REQUIRED", "<div class=\"message_warning\">All fields marked with * are mandantory! Please fill them correctly.<br>If you have questions write us : %s</div>");
	define("MESSAGE_SENT", "Your message has been sent. Thank you !");
?>