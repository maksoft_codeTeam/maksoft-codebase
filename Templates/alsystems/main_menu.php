<?php		
/*
##########################################################
#	module name: 	Standart Main Menu 
#	description:		show all pages defined as subpages of home page
							select strart-menu page, show / skip pages 
							show / hide / expand submenus
#	directory:			web/admin/modules/menu/
#	author:				M.Petrov, petrovm@abv.bg
##########################################################
*/

//select the parent page of the folowing menu items (n=$Site->StartPage)
if(!isset($menu_parent_item))
	$menu_parent_item = $Site->StartPage;

//show / hide items numbering
if($view_numbering == true)
	$start_number = 1;
	
//show / hide submenu bullet
if(!isset($submenu_bullet))
	$submenu_bullet = " &raquo; ";

//show page with selected status (0-default, 1-toplink, 2-drop_down, 3-main_link, 4-vip_link, 5-second_link)
if(!isset($view_menu_item))
	{
		$view_menu_item = 0;
		if(isset($skip_menu_item)) 
			//skip selected pages
			$skip_menu_item_sql = " AND toplink!=$skip_menu_item ";
		else $skip_menu_item_sql = " ";
	}
else 
{
	unset($skip_menu_item);
	//view selected pages	
	$view_menu_item_sql = "toplink = '". $view_menu_item."' ";
}
//view all pages
if($view_menu_item == "all")  $view_menu_item_sql = "1"; 

		 //reading main menu		  
		 $res = mysqli_query("SELECT * FROM pages WHERE ParentPage = $menu_parent_item AND  $view_menu_item_sql $skip_menu_item_sql ORDER by sort_n ASC"); 
		 while ($menu_row = mysqli_fetch_object($res)) 
		 {

			
			$content = "<a href=\"".$o_page->get_pLink($menu_row->n)."\" class=\"menu_button\" title=\"".cut_text(strip_tags($menu_row->textStr), 100)."\"><div class=\"number\">0".$start_number .".</div><div class=\"text\">". $menu_row->Name."</div></a>";
			
			if($row->n == $menu_row->n)
				$content = "<a href=\"".$o_page->get_pLink($menu_row->n)."\" class=\"menu_button\" title=\"".cut_text(strip_tags($menu_row->textStr), 100)."\"><div class=\"number\"><b>0".$start_number ."</b></div><div class=\"text\"><b>". $menu_row->Name."</b></div></a>";

			if($view_numbering)
					$start_number++;
				echo $content;
		 } 
?>