<?php
//return the file type
function FileBaseName($address,$ret)
{
	$path_parts = pathinfo($address);
	$fdir = $path_parts["dirname"];
	$fext = $path_parts["extension"];
	$fname = str_replace(".".$fext,"",$path_parts["basename"]);
	switch ($ret)
		{
			case 'dir': return $fdir; 
					  break;
			case 'name': return $fname; 
					  break;
			case 'ext': return $fext; 
					  break;
			default: return $address;
		}
}

//default domain
$default_domain_url = "http://www.maksoft.net";

//default img width
$default_img_width = 150;

//get the variables picture name and directory
@$pic_name = $_GET['pic_name'];
if(!isset($domain_url) || $domain_url =='')
	@$domain_url = $default_domain_url;
	
@$pic_dir = $domain_url."/".$_GET['pic_dir'];


$image_file = $pic_dir."/".$pic_name;
$image_extension = FileBaseName($image_file,'ext');

//detect the image type
switch($image_extension)
{
case 'jpg':
case 'jpeg':
case 'JPG':
case 'JPEG':
		{
		$img = imagecreatefromjpeg("$image_file");
		break;
		}
case 'gif':
case 'GIF':
		{
		if (ImageTypes() & IMG_GIF)
			$img = imagecreatefromgif("$image_file");
		else $img = imagecreatefromjpeg("$image_file");
		break;
		}

case 'png':
case 'PNG':
		{
		if (ImageTypes() & IMG_PNG)
			$img = imagecreatefrompng("$image_file");
		else $img = imagecreatefromjpeg("$image_file");	
		break;
		}
}

$image_width=imagesx($img); 
$image_height=imagesy($img);
$img_border = 2;
$border_bottom = 0; //botom offset

$new_image_max_size = $default_img_width+2*$img_border; // max image width

if($image_width > $image_height)
{
	$relation = $image_width/$new_image_max_size;
	$new_image_width = $new_image_max_size;
	$new_image_height = $image_height/$relation;
}
else
{
	$relation = $image_height/$new_image_max_size;
	$new_image_height = $new_image_max_size;
	$new_image_width = $image_width/$relation;
}

$new_image_width = $new_image_width+$img_border;
$new_image_height = $new_image_height+$img_border;

$new_image = imagecreatetruecolor($new_image_width, $new_image_height);
$img_border_color = imagecolorallocate($new_image, 255,255,255);
imagefill($new_image, 0, 0, $img_border_color); 


$dist_X = $img_border;
$dist_Y = $img_border;

imagecopyresampled($new_image, $img, $dist_X, $dist_Y, 0, 0, $new_image_width-(2*$img_border), $new_image_height-(2*$img_border), $image_width, $image_height);


//detect the image type
switch($image_extension)
{
case 'jpg':
case 'jpeg':
case 'JPG':
case 'JPEG':
		{
		Header('Content type: image/jpeg');
		imagejpeg($new_image, '', $new_image_max_size);
		break;
		}
case 'gif':
case 'GIF':
		{
		if (ImageTypes() & IMG_GIF)
			{
				Header('Content type: image/gif');
				imagegif($new_image, '', $new_image_max_size);
			}			
		else 
			{
				Header('Content type: image/jpeg');
				imagejpeg($new_image, '', $new_image_max_size);			
			}
		break;
		}

case 'png':
case 'PNG':
		{
		if (ImageTypes() & IMG_PNG)
			{
				Header('Content type: image/png');
				imagepng($new_image, '', $new_image_max_size);
			}			
		else 
			{
				Header('Content type: image/jpeg');
				imagejpeg($new_image, '', $new_image_max_size);			
			}	
		break;
		}
}

?>