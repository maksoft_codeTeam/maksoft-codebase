<?php
use \Jokuf\Form\DivForm;
use \Jokuf\Form\Field\Text;
use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Field\Integer;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Field\Date;


class TripOffer extends DivForm
{
    private $_em;
    public $submited = false;

    public function __construct($em, $post_data=null, $initial_data=array())
    {
        $this->_em = $em;

        $this->from_city = Text::init()
            ->add("class" , "validate")
            ->add("id" , "from_city")
            ->add("placeholder", "�����")
            ->add("label" , "�������� ��:")
            ->add("required", True);

        $this->to_city = Text::init()
            ->add("id", "to_city")
            ->add("class" , "validate")
            ->add("label" , "����������:")
            ->add("placeholder", "�����")
            ->add("required", True);

        $this->from_date = Text::init()
            ->add("class" , "datepicker")
            ->add("label", "���� �� ��������:")
            ->add("value" , date('Y-m-d', time()));

        $this->to_date = Text::init()
            ->add("class" , "datepicker")
            ->add("label", "���� �� ����������:")
            ->add("value" , date('Y-m-d', time()));

        $this->persons = Integer::init()
            ->add("class" , "validate")
            ->add("value" , 1)
            ->add("label", "���������:")
            ->add("placeholder" , 1);
        $this->persons->setStep(1);
        $this->persons->setMin(1);

        $this->childrens = Integer::init()
            ->add("value" , 1)
            ->add("label", "����:")
            ->add("class" , "validate");


        $this->childrens->setStep(1);
        $this->childrens->setMin(0);

        $this->twoway = Text::init()
            ->add("label", "���������� ��������")
            ->add("class" , "validate");

        $this->extras = Text::init()
            ->add("name", "extras[]")
            ->add("class" , "validate");

        $this->submit = Submit::init()
            ->add("class" , "btn waves-effect waves-light");

        $this->action = Hidden::init()
            ->add("value", "complete_order");

        parent::__construct($post_data, $_FILES, $initial_data);
    }

    public function is_valid()
    {
        $this->submited = true;
        parent::is_valid();
    }

    #public function validate_twoway()
    #{
    #    if($this->twoway->value === "on"){
    #        $this->twoway->value = True;
    #    } else {
    #        $this->twoway->value = False;
    #    }
    #    return True;
    #}

    private function days()
    {
        $datediff = strtotime($_SESSION[PREFIX]['to_date']) - strtotime($_SESSION[PREFIX]['from_date']);
        return floor($datediff/(60*60*24));
    }


    public function can_send_request()
    {
        if(!isset($_SESSION[PREFIX][SESSION_ENQ])){
            $_SESSION[PREFIX][SESSION_ENQ] = 0;
        }

        return $_SESSION[PREFIX][SESSION_ENQ] < MAX_REQUESTS;
    }

    public function save()
    {
        $extrasRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Extras');
        $extras = $extrasRepository->findBy(array("id" => $_SESSION[PREFIX][SESSION_RENT]['extras']));
        $clientRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Client');
        $client = $clientRepository->findOneById($_SESSION[PREFIX][SESSION_USER]['id']);
        if(!$client){
            throw new \Exception("���� ������ ��� �� �������������");
        }
        $order = new \Maksoft\Rent\Bus\Order();
        $order->setClient($client);
        foreach($extras as $extra){
            $order->addExtra($extra);
        }
        $order->setFromCity($_SESSION[PREFIX][SESSION_RENT]['from_city']);
        $order->setDestination($_SESSION[PREFIX][SESSION_RENT]['to_city']);
        $order->setTwoWay(0);
        $order->setDeparature(new \DateTime($_SESSION[PREFIX][SESSION_RENT]['from_date']));
        $order->setArrival(new \DateTime($_SESSION[PREFIX][SESSION_RENT]['to_date']));
        $order->setPersons($_SESSION[PREFIX][SESSION_RENT]['persons']);
        $order->setChildrens($_SESSION[PREFIX][SESSION_RENT]['childrens']);
        $order->setDays($this->days());
        $order->setCompleted(False);
        $this->_em->persist($order);
        $this->_em->flush();
        $id = $order->getId();
        $msg1 = \Maksoft\Rent\Message\Message::init("successfullOrderEmail", "email/successfull_order.html", $id, 10);
        $msg2 = \Maksoft\Rent\Message\Message::init("orderHasBeenPlacedEmail", "email/successfull_order.html", $id, 10);
        $sender = new \Maksoft\Rent\Message\Pusher();
        $sender->connect();
        $sender->publish($msg1);
        $sender->publish($msg2);
        unset($_SESSION[PREFIX][SESSION_RENT]);
        if(!isset($_SESSION[PREFIX][SESSION_ENQ])){
            $_SESSION[PREFIX][SESSION_ENQ] = 1;
        } else {
            $_SESSION[PREFIX][SESSION_ENQ] += 1;
        }
        Message\success('������ ��������� ���� �������� �������. ���� ������� ������ �� �������� �����');
    }
}
