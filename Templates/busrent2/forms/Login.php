<?php
use \Jokuf\Form\DivForm;
use \Jokuf\Form\Field\Email;
use \Jokuf\Form\Field\Password;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Validators\NotEmpty;
use Maksoft\Rent\Bus\Settings\Base as BaseSettings;


class Login extends DivForm 
{
    private $_em;
    private $_client;
    public function __construct($em, $post_data=null)
    {
        $this->_em = $em;

        $this->email = Email::init() 
            ->add("class" , "validate")
            ->add("required", True);

        $this->password = Password::init() 
            ->add("class" , "validate")
            ->add("data-validate", "min, max, required")
            ->add_validator(NotEmpty::init(true)->err_msg('�� ��� ������ ������'))
            ->add("required", True);

        $this->submit = Submit::init()
            ->add("value", "����")
            ->add("style", "margin-top:1%")
            ->add("class" , "btn waves-effect waves-light");

        $this->action = Hidden::init()
            ->add("value", "login");

        parent::__construct($post_data);
    }
    public function validate_password()
    {
        $clientRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Client');
        if(!$client = $clientRepository->findOneByEmail($this->email->value)){
            throw new \Exception('������������� ����������');
        }

        if($client->equal($this->password->value)){
            $this->_em->detach($client);
            $this->_client = $client;
            return True;
        }
        throw new \Exception('������ ������, ���� �������� ������');
    }

    public function save()
    {
        $logged_at = time();
        $_SESSION[PREFIX] = 
            array(SESSION_USER =>
                array(
                    "id" => $this->_client->getId(),
                    "first_name" => $this->_client->getFirstName(),
                    "last_name" => $this->_client->getLastName(),
                    "email" => $this->_client->getEmail(),
                    "logged_at" => $logged_at,
                    SESSION_EXPIRE_PREFIX => $logged_at + SESSION_EXPIRE,
                )
            );
        Message\success("����� ����� ".$this->_client->getFirstName());
    }
}
