<?php
require_once __DIR__.'/BaseActionForm.php';

class DeleteOrder extends BaseActionForm 
{
    public $submit_value = '������ �����������';
    public $submit_class = '';
    public $action_value = 'delete_offer';

    public function save()
    {
        $orderRepository = $this->em->getRepository('\Maksoft\Rent\Bus\Order');
        $id = $_SESSION[PREFIX][SESSION_USER]['id'];
        try{
            $order = $orderRepository->findOneBy(array("id" => $this->order_id->value, "client_id" => $id));
            $this->em->remove($order);
            $this->em->flush();
            Message\success('������� �������� ��������� �'.$this->order_id->value);
        } catch (\Exception $e) { 
            Message\error($e->getMessage());
        }
    }
}
