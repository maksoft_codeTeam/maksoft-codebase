<?php
use Jokuf\Form\DivForm;
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Password;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Date;

use Maksoft\Rent\Bus\Settings\Base as BaseSettings;


class Register extends DivForm 
{
    private $_em;
    public $_header = "register";
    public function __construct($em, $post_data=null, $header=false)
    {
        $this->_em = $em;
        if($header){
            $this->_header = $header;
        }

        $this->first_name = Text::init()
            ->add("class" , "validate")
            ->add("required", True);

        $this->last_name = Text::init()
            ->add("class" , "validate")
            ->add("required", True);

        $this->phone = Phone::init() 
            ->add("class" , "validate")
            ->add("required", True);

        $this->email = Email::init() 
            ->add("class" , "validate")
            ->add("required", True);

        $this->password = Password::init() 
            ->add("data-validate", "required,rangeVal(5, 15)")
            ->add("class" , "validate")
            ->add("required", True);

        $this->password->add_validator(new \Jokuf\Form\Validators\MaxLength(15));

        $this->password->add_validator(new \Jokuf\Form\Validators\MinLength(5));

        $this->password->add_validator(new \Jokuf\Form\Validators\HasDigit());

        $this->password->add_validator(new \Jokuf\Form\Validators\HasUpperCase());

        $this->repeated_password = Password::init() 
            ->add("class" , "validate")
            ->add("required", True);

        $this->submit = Submit::init()
            ->add("value", "�����������")
            ->add("class" , "btn waves-effect waves-light");

        $this->action = Hidden::init()
            ->add("value", "client");

        parent::__construct($post_data);
    }

    public function validate_password($field)
    {
        if ($field->value != $this->repeated_password->value) {
            throw new Exception('���������� ������ �� ��������'); 
        }
    }

    public function save()
    {
        $clientRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Client');

        if($client = $clientRepository->findOneByEmail($this->email->value)){
            throw new \Exception('���� ��� ���������� ����������� � ���� ����� �����');
        }

        $client = new \Maksoft\Rent\Bus\Client();
        $client->setFirstName($this->first_name->value);
        $client->setMiddleName("");
        $client->setLastName($this->last_name->value);
        $client->setEmail($this->email->value);
        $client->setPhone($this->phone->value);
        $client->setPassword($this->password->value);
        $client->createdAt();
        $this->_em->persist($client);
        $this->_em->flush();
        $id = $client->getId();
        $msg = \Maksoft\Rent\Message\Message::init("successfullRegisterEmail", "email/successfull_registration.html", $id, 30);
        $sender = new \Maksoft\Rent\Message\Pusher();
        $sender->connect();
        $sender->publish($msg);
        $logged_at = time();
        $_SESSION[PREFIX] = 
            array(SESSION_USER =>
                array(
                    "id" => $client->getId(),
                    "first_name" => $client->getFirstName(),
                    "last_name" => $client->getLastName(),
                    "email" => $client->getEmail(),
                    "logged_at" => $logged_at,
                    SESSION_EXPIRE_PREFIX => $logged_at + SESSION_EXPIRE,
                )
            );

        \Message\success('������ ����������� � �������. ���������� �� ����� � ������ ����������');
    }
}
