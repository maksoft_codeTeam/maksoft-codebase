<?php
require_once __DIR__.'/BaseActionForm.php';


class RejectCompanyOffer extends BaseActionForm
{
    public $submit_value = '������';
    public $submit_class = 'btn btn-warning';
    public $action_value = 'reject_offer';

    public function save()
    {
        $orderRepository = $this->em->getRepository('\Maksoft\Rent\Bus\Offer');
        $id = $_SESSION[PREFIX][SESSION_USER]['id'];
        try{
            $order = $orderRepository->findOneBy(array("id" => $this->order_id->value, "order" => $_GET['inquiry']));
            $order->setStatus(false);
            $this->em->persist($order);
            $this->em->flush();
            Message\info('��� ��������� ������ �'.$this->order_id->value);
        } catch (\Exception $e) { 
            Message\error($e->getMessage());
        }
    }
}
