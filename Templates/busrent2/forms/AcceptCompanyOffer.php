<?php
require_once __DIR__.'/BaseActionForm.php';


class AcceptCompanyOffer extends BaseActionForm
{
    public $submit_value = '������';
    public $submit_class = 'btn btn-success';
    public $action_value = 'accept_offer';

    public function save()
    {
        $offerRepository = $this->em->getRepository('\Maksoft\Rent\Bus\Offer');
        $orderRepository = $this->em->getRepository('\Maksoft\Rent\Bus\Order');
        $id = $_SESSION[PREFIX][SESSION_USER]['id'];
        try{
            $offer = $offerRepository->findOneBy(array("id" => $this->order_id->value, "order" => $_GET['inquiry']));
            $order = $orderRepository->findOneById($offer->getCompany()->getId());
            $order->setCompleted(true);
            $offer->setStatus(true);
            $this->em->persist($offer);
            $this->em->persist($order);
            $this->em->flush();
            Message\success('������� ������� ������ �'.$this->order_id->value. '. ���� ������� �� �������� ����� � ����������');
        } catch (\Exception $e) { 
            Message\error($e->getMessage());
        }
    }

}
