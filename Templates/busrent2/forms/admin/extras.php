<?php
use \Jokuf\Form\Bootstrap;
use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Field\Text;
use \Jokuf\Form\Validators\NotEmpty;
use \Jokuf\Form\Validators\MaxLength;
use \Jokuf\Form\Validators\MinLength;


class ExtraDetail extends Bootstrap
{
    private $_em;
    private $_type;
    private $_language;
    private $_category;

    public function __construct($vehicleType, $category, $em, $post_data=array())
    {
        $this->_em = $em;
        $this->_language = $language;
        $this->_type = $vehicleType;
        $this->_category = $category;
        $class = "form-control";
        $options = array();
        $bg_language = 34;

        $this->name = Text::init()
            ->add("label", "��� �� ��������:")
            ->add("class", $class)
            ->add_validator(MaxLength::init(30)->err_msg('������������ ������� � 30 �������'));

        $this->icon = Text::init()
            ->add("label", "fa-icon:")
            ->add("name" , "icon")
            ->add("class", $class);

        $this->submit = Submit::init()
            ->add("class", "btn btn-primary")
            ->add("label", "<hr>")
            ->add("value", "������");

        $this->action = Hidden::init()
            ->add("name", "action")
            ->add("value", "insert_extra");

        parent::__construct($post_data);
    }

    public function save()
    {
        $categoryRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Category');
        $category = $categoryRepository->findOneById($this->_category);
        $typeRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Type');
        $type = $typeRepository->findOneById($this->_type);
        $extra = new \Maksoft\Rent\Bus\Extras();
        $extra->setName($this->name->value);
        $extra->setIcon($this->icon->value);
        $extra->setCategory($category);
        $extra->setType($type);
        $this->_em->persist($extra);
        $this->_em->flush();
    }
}
