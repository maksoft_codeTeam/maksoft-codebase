<?php
use Jokuf\Form\Bootstrap;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Text;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\MaxLength;
use Jokuf\Form\Validators\MinLength;


class Category extends Bootstrap
{
    private $_em;
    private $_language;

    public function __construct($language, $em, $post_data=array())
    {
        $this->_em = $em;
        $this->_language = $language;
        $class = "form-control";

        $this->name = Text::init()
            ->add("label", "Име на категорията:")
            ->add("class", $class);
        $this->name->add_validator(new NotEmpty(true));
        $this->name->add_validator(new MinLength(3));
        $this->name->add_validator(new MaxLength(30));

        $this->icon = Text::init()
            ->add("label", "fa-icon:")
            ->add("class", $class);

        $this->submit = Submit::init()
            ->add("class", "btn btn-primary")
            ->add("label", "<hr>")
            ->add("value", "Запази");

        $this->action = Hidden::init()
            ->add("value", "insert_category");

        parent::__construct($post_data);
    }

    public function save()
    {
        $languageRepository = $this->_em->getRepository('\Maksoft\Rent\Bus\Language');
        $language = $languageRepository->findOneById($this->_language);
        $category = new \Maksoft\Rent\Bus\Category();
        $category->setName($this->name->value);
        $category->setIcon($this->icon->value);
        $category->setLanguage($language);
        $this->_em->persist($category);
        $this->_em->flush();
        Message\success('Категорията е добавена');
    }
}
