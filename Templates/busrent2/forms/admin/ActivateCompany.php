<?php
use \Jokuf\Form\Bootstrap;
use \Jokuf\Form\Field\Hidden;
use \Jokuf\Form\Field\Select;
use \Jokuf\Form\Field\Submit;
use \Jokuf\Form\Validators\NotEmpty;
use \Jokuf\Form\Field\Date;


class ActivateCompany extends Bootstrap
{
    protected $company;
    protected $em;

    public function __construct($post=array(), $company, $em) {
        $this->company = $company;
        $this->em = $em;
        $this->action = Hidden::init()->add('value', 'status');
        $this->status = Select::init()
            ->add('value', $this->company->active)
            ->add('class', 'form-control')
            ->add('data', array(0 => '���������', 1 => '�������'))
            ->add_validator(NotEmpty::init(true)->err_msg('��������� ������'));

        $this->submit = Submit::init()
            ->add("class" , "btn waves-effect waves-light");
        parent::__construct($post);
    }
    
    public function save()
    {
        $this->em->persist($this->company);
        $this->company->active = $this->status->value;
        $this->em->flush();
    }
}
