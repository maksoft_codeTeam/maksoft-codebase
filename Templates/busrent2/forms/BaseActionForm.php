<?php
use Jokuf\Form\Bootstrap;

use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;


abstract class BaseActionForm extends Bootstrap
{

    public $em;

    public $submit_value = '������';
    public $submit_class = 'btn btn-success';
    public $action_value = '';

    public function __construct($em, $post_data=null)
    {
        $this->em = $em;

        $this->order_id = Hidden::init();
        $this->order_id->add_validator(new Jokuf\Form\Validators\Integerish());

        $this->action = Hidden::init()
            ->add('value', $this->action_value);

        $this->submit = Submit::init()
            ->add("class", $this->submit_class)
            ->add('value', $this->submit_value);

        parent::__construct($post_data);
    }

}
