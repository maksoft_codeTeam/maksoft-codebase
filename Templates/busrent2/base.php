<!DOCTYPE html>
<html lang='en'>
    <head>
        <?php require_once TEMPLATE_PATH.'/meta.php';?>
    </head>
        <!--[if IE 7]>
            <body class='ie7 lt-ie8 lt-ie9 lt-ie10'>
        <![endif]-->
        <!--[if IE 8]>
            <body class='ie8 lt-ie9 lt-ie10'>
        <![endif]-->
        <!--[if IE 9]>
            <body class='ie9 lt-ie10'>
        <![endif]-->
        <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <div id='page-wrap'>
            <?php require_once TEMPLATE_PATH.'/header.php';?>
            <?php require_once TEMPLATE_PATH.'/sliders.php';?>
            <section>
              <div class='container'>
                <?php 
                switch(True) {
                    case $o_page->_page['SiteID'] != $o_site->_site['SitesID']:
                        require_once __DIR__.'/admin.php';   
                        break;
                    case $o_page->_page['n'] == $o_site->_site['StartPage'] && strlen($search_tag)<3 && strlen($search)<3:
                        require_once TEMPLATE_PATH.'/pages/homepage.php';
                        break;
                    case $pTemplate['pt_url']:
                        include $pTemplate['pt_url'];
                    default:
                        if($subpages_len > 5){
                            require_once TEMPLATE_PATH.'/pages/category.php';
                        } else {
                            require_once TEMPLATE_PATH.'/pages/article.php';
                        }
                }
                ?>
              </div>
            </section>
            <?php require_once TEMPLATE_PATH.'/footer.php';?>
            <?php require_once TEMPLATE_PATH.'/scripts.php'; ?>
    </body>
</html>
