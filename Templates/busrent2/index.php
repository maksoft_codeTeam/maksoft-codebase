<?php
ini_set('display_errors',1);
ini_set('display_startup_errors', 1);
require_once __DIR__.'/config/base.php';
require_once __DIR__.'/../../modules/vendor/autoload.php';
require_once __DIR__.'/helpers.php';
require_once __DIR__.'/../../lib/messages.php';


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
$base = __DIR__;
$b_user = get_user();

if(is_authenticated()) {
    define('AUTHENTICATED' , true);
} else {
    define('AUTHENTICATED' , false);
}

define('STATIC_URL', str_replace($_SERVER['DOCUMENT_ROOT'],'', $base).'/assets/'); 
define('ROOT_DIR', realpath($base.'/../../'));
define('BASE_DIR', $base);
define('TEMPLATE_PATH', realpath($base.'/templates/'));

$request = Request::createFromGlobals();

// Doctrine entity manager
$em = $settings->getEntityManager();

$subpages_len = $o_page->count_subpages($o_page->_page['n']);

require_once __DIR__.'/base.php';
