<?php

function submit($form, $redirectUrl=null, $msec=3000)
{
    try {
        $form->is_valid();
        $form->save();
    } catch (Exception $e) {
        \Message\warning($e->getMessage());
        return;
    }
    if($redirectUrl){
        echo '<script>
                setTimeout(function() { 
                window.location.href = "'.$redirectUrl.'"; 
                 }, '.$msec.');</script>';
    }
}

function process_order_data($data)
{
    $tmp = array();
    foreach($data as $k=>$v)
    {
        if(is_array($v)){
            $tmp[$k] = array();

            foreach ($v as $_k => $_v){
                $tmp[$k][$_v] = $_v;
            }
            continue;
        }
        $tmp[$k] = $v;
    }
    return $tmp;
}

function msg_notify($type, $text)
{
    return 'jQuery.notify(\''.$text.'\', \''.$type.'\');';
}

function process_extras($em, $type=2)
{
    $type = $em->getRepository('\Maksoft\Rent\Bus\Type')
               ->findOneById($type);

    if(!$type){
        return array('type' => '', 'extras' => '');
    }

    $extras = array();

    foreach($type->getExtras() as $extra){
        $category = $extra->getCategory();
        $extras[$category->getName()][] = $extra;
    }

    return array('type' => $type, 'extras' => $extras);
}

function save_to_session($post_data)
{
    if(!isset($_SESSION[PREFIX])){
        $_SESSION[PREFIX] = array();
    }
    $_SESSION[PREFIX][SESSION_RENT] = $post_data;
}


function get_user()
{
    return is_authenticated() ? $_SESSION[PREFIX][SESSION_USER] : array();
}


function is_authenticated()
{
    if (!isset($_SESSION[PREFIX][SESSION_USER]) 
        or 
        !isset($_SESSION[PREFIX][SESSION_USER][SESSION_EXPIRE_PREFIX])) {
        $_SESSION[PREFIX][SESSION_USER][SESSION_EXPIRE_PREFIX] = 0;
        return false;
    }
    $expire = $_SESSION[PREFIX][SESSION_USER][SESSION_EXPIRE_PREFIX];
    if ($expire < time()) {
        add_status_expired();
        return false;
    }

    update_expire_time();
    return true;
}

function add_status_expired()
{
    $_SESSION[PREFIX][SESSION_USER][SESSION_REAUTH] = true;
}

function update_expire_time()
{
    $_SESSION[PREFIX][SESSION_USER][SESSION_EXPIRE_PREFIX] = time() + SESSION_EXPIRE;
    $_SESSION[PREFIX][SESSION_USER][SESSION_REAUTH] = false;
}

function login()
{

}

function register()
{

}



function build_attributes($attributes=array()) {
    $attrs = array();
    array_walk($array, function($key, $val) use(&$attrs) {
            $attrs[] = sprintf("%s=\"%s\"", $key, $val);
    });

    return sprintf(" %s ", implode(" ", $attrs));
}

function build_href($link, $name, $attributes=array()) {
    $attributes = build_attributes($attributes);
    return "<a href=\"$link\" $attributes>$name</a>";
}

function build_div($class='', $inner_html='') {
    return sprintf("<div class=\"%s\">%s</div>", $class, $inner_html);
}

function get_query_url()
{
    parse_str($_SERVER['QUERY_STRING'], $url);
    return $url;
}

function print_language($carry, $item) 
{
    static $url;
    if(empty($url)) {
        $url = get_query_url();
    }

    $url['language'] = $item->getId();
    $link = '/page.php?'.http_build_query($url);
    $carry .= build_div('col-md-3', build_href($link, $item->getCountry()));  
    return $carry;
}


function print_categories($carry, $item) {
    foreach($item->extras as $extra) {
        echo "<br><b>".$extra->getId()."</b>";
    }
    $carry .= build_div('col-md-4', $item->getName());  
    return $carry;
}


function print_extras($extras) {
    static $url;
    if(empty($url)) {
        $url = get_query_url();
    }

    return implode(' ', array_map(function($item) use ($url) {
        $url['language'] = $item->getId();
        $link = '/page.php?'.http_build_query($url);
        return build_div('col-md-3', build_href($link, $item->getName()));  
    }, $extras));

}
