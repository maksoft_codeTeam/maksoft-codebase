<?=$form->start()?>
<div class="row">
    <div class="col-md-12">
        <div class="form-elements ui-widget" style="display:inline">
        <label for="twoway" ><?=$form->twoway->label?></label>
            <div class="form-item ui-widget">
            <input style="width: 1em; height: 1em;" type="radio" name="<?=$form->twoway->name?>" onclick="show()" value="true">��
            <input style="width: 1em; height: 1em;" type="radio" name="<?=$form->twoway->name?>" onclick="hide()" value="false" checked>��
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-elements ui-widget">
        <label for="from_city" ><?=$form->from_city->label?></label>
          <div class="form-item ui-widget"><i class="awe-icon awe-icon-marker-1"></i>
<?=$form->from_city?>
          </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-elements ui-widget">
        <label><?=$form->to_city->label?></label>
          <div class="form-item"><i class="awe-icon awe-icon-marker-1"></i>
<?=$form->to_city?>
          </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6" id="from_date">
        <div class="form-elements">
        <label><?=$form->from_date->label?> </label>
          <div class="form-item"><i class="awe-icon awe-icon-calendar"></i>
<?=$form->from_date?>
          </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3" id="to_date" style="display:none;">
        <div class="form-elements">
        <label><?=$form->to_date->label?></label>
          <div class="form-item"><i class="awe-icon awe-icon-calendar"></i>
<?=$form->to_date?>
          </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="form-elements">
        <label><?=$form->persons->label?></label>
          <div class="form-item">
<?=$form->persons?>
          </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="form-elements">
        <label><?=$form->childrens->label?></label>
          <div class="form-item">
<?=$form->childrens?>
          </div>
        </div>
    </div>
    <div class="col-md-12">
    <hr>
        <?php foreach ($extras as $category=>$col) { ?>
        <div class="col-sm-4 col-md-3">
        <h5> <?=$category?></h5>
            <div class="row">
            <?php foreach($col as $extra) { ?>
                <div class="col-sm-12 checkbox">
                    <label>
                    <input style="width: 1em;height: 1em;" 
                           name="extras[]"
                           type="checkbox"
                           value="<?=$extra->getId()?>"
                           <?php if(array_key_exists($extra->getId(), $checked_extras)) { echo 'checked'; }?>
                    ><?=$extra->getName()?>
                    </label>
                </div>
            <?php } ?>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="col-md-12 form-actions">
      <input type="submit" value="������� ������">
    </div>
<?=$form->action?>
<?=$form->end()?>
</div>
