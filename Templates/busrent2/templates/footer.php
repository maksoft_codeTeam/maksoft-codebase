 <footer id="footer-page">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
            <div class="widget_background">
              <div class="widget_background__half">
                <div class="bg"></div>
              </div>
              <div class="widget_background__half">
                <div class="bg"></div>
              </div>
            </div>
            <div class="logo"><img src="<?=STATIC_URL?>/images/logo-busrent.png" width="200px" alt="BUSRENT" /></div>
            <div class="widget_content">
              <br />
              <p><?=$o_page->_site['SAddress']?></p>
              <p><a href="tel:<?=$o_page->_site['SPhone']?>"><b><?=$o_page->_site['SPhone']?></b></a></p>
            </div>
        </div>
        <div class="col-md-2">
          <div class="widget widget_about_us">
            <h3>�� ���</h3>
            <div class="widget_content">
              <p>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="widget widget_categories">
            <h3>���� ��</h3>
            <ul>
              <li><a href="#">��������</a></li>
              <li><a href="#">���������</a></li>
              <li><a href="#">�����</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-2">
          <div class="widget widget_recent_entries">
            <h3>���������</h3>
            <ul>
            <?php
                foreach($o_page->get_pSubPages($o_page->_site['StartPage']) as $page){
                    if($page['SecLevel'] == 0 and $page["toplink"] == 2){
                        echo "<li><a href=\"".$page['page_link']."\">".$page["Name"]."</a></li>";
                    }
                }
            ?>
            </ul>
            <?php
            ?>
          </div>
        </div>
        <div class="col-md-3">
          <div class="widget widget_follow_us">
            <div class="widget_content">
              <p>�� ��������� ������, ������� �� ��:</p>
              <span class="phone"><a href="tel:<?=$o_page->_site['SPhone']?>"><?=$o_page->_site['SPhone']?></a></span>
              <div class="awe-social">
                <a href="<?=$o_page->get_sConfigValue("CMS_TWITTER_PAGE_LINK"); ?>"><i class="fa fa-twitter"></i></a>
                <a href="<?=$o_page->get_sConfigValue("CMS_PINTEREST_PAGE_LINK"); ?>"><i class="fa fa-pinterest"></i></a>
                <a href="<?=$o_page->get_sConfigValue("CMS_FACEBOOK_PAGE_LINK"); ?>"><i class="fa fa-facebook"></i></a>
                <a href="<?=$o_page->get_sConfigValue("CMS_LINKEDIN_PAGE_LINK"); ?>"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright">
        <p>� <?=date('Y', time());?> BUSRENT.</p>
      </div>
    </div>
  </footer>
