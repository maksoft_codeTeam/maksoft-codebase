<meta charset="cp1251">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<title><?=$Title?></title>
<?php
require_once "Templates/meta.php";
$o_site->print_sConfigurations();
?>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="google-site-verification" content="L8LPKQKr2d813bXlOczGg_ZZtPXlORpuc3CfP4uNZ6s" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:700,600,400,300" rel="stylesheet" type="text/css">
<link href="//fonts.googleapis.com/css?family=Oswald:400" rel="stylesheet" type="text/css">
<link href="//fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<link rel="stylesheet" type="text/css" href="<?=STATIC_URL?>css/lib/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=STATIC_URL?>css/lib/awe-booking-font.css">
<link rel="stylesheet" type="text/css" href="<?=STATIC_URL?>css/lib/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?=STATIC_URL?>css/lib/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="<?=STATIC_URL?>css/style.css">
<link id="colorreplace" rel="stylesheet" type="text/css" href="<?=STATIC_URL?>/css/colors/blue.css">
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
