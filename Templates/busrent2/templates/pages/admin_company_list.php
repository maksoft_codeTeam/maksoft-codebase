<?php
$companyRepository = $em->getRepository('\Maksoft\Rent\Bus\Company');

if(isset($_GET['company_id']) and is_numeric($_GET['company_id']) and $_GET['company_id'] > 0) {
    $company = $companyRepository->findOneById($_GET['company_id']);
    if(!$company) { 
        echo '<h2>���� ������ ��������</h2>';
    } else {
        require_once __DIR__ .'/admin_company_detail.php';
        return;
    }
}

$companies = $companyRepository->findAll();

echo '<table class="table table-responsive">';
echo <<<DOC
        <th>N</th>
        <th>���</th>
        <th>���</th>
        <th>E-mail</th>
        <th>�������</th>
        <th>������</th>
        <th>������</th>
        <th>��������</th>
DOC;
$i = 1;
$link = array('n' => $n, 'SiteID' => $SiteID);
foreach ($companies as $company) {
    $active = $company->active ? '�������' : '���������';
    $link['company_id'] = $company->getId();
    $detail_link = http_build_query($link);
    echo <<<HERE
    <tr>
        <td>$i. </td>
        <td> {$company->getName()}</td>
        <td> {$company->getOwner()}</td>
        <td> {$company->getEmail()}</td>
        <td> {$company->getPhone()}</td>
        <td> {$company->getLicense()}</td>
        <td> {$active} </td>
        <td> <a href="/page.php?{$detail_link}" class="btn btn-success"> ������� </a> </td>
    </tr>
HERE;
    $i++;
}

echo '</table>';
