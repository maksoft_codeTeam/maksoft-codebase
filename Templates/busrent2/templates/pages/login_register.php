<?php

if(is_authenticated()){
    $redirect_url = $o_page->get_pLink($o_site->_site['StartPage']);
    Message\info('���� ��� ������ ��� ������ ������');
    echo '<script>window.location = "'.$redirect_url.'";</script>';
}

require_once BASE_DIR.'/forms/Login.php';
require_once BASE_DIR.'/forms/Register.php';

$login_form = new Login($em, $_POST);
$register_form = new Register($em, $_POST);

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action'])) {
    switch($_POST['action']) {
        case $login_form->action->value:
            $response = submit($login_form, '/', 10);
            break;
        case $register_form->action->value:
            $response = submit($register_form, '/', 10);
            break;
        default:
            break;
    }
}
?>
<div class="row">
    <?php Message\display();?>
    <div class="col-md-10 col-md-offset-1 text-center">
        <h2> �������, ����� �� ��� ������ ���� ������ </h2>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6">
        <?php require_once TEMPLATE_PATH.'/forms/login.php'; ?>
    </div>
    <div class="col-md-6">
        <?php require_once TEMPLATE_PATH.'/forms/register.php'; ?>
    </div>
</div>
