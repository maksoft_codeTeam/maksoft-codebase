<?php
if (!is_authenticated()) {
    return '';
}

$ordersRepository = $em->getRepository('\Maksoft\Rent\Bus\Order');

if(isset($_GET['inquiry']) and is_numeric($_GET['inquiry'])) {
    $order = $ordersRepository->findOneBy(array('client_id' => $_SESSION[PREFIX][SESSION_USER]['id'], 
                                                'id' => $_GET['inquiry']));
    require_once BASE_DIR.'/forms/DeleteOrder.php';

    $delete = new DeleteOrder($em, $_POST); 
    $template = 'offer_detail.php';
    if($_SERVER['REQUEST_METHOD'] == 'POST' and $_POST['action'] == $delete->action->value){
        submit($delete);
        echo '<script>window.location = "'.$o_page->get_pLink(PAGE_OFFERS).'";</script>';
    }
} else {
    $orders = $ordersRepository->findBy(array("client_id"=>$_SESSION[PREFIX][SESSION_USER]['id']));
    $template = 'offer_list.php';
}

?>
<div class="row">
    <?=Message\display();?>
    <h2><?=$o_page->_page['Name'];?></h2>
    <?php require_once __DIR__.'/../'.$template;?>
</div>

<div class="row">
    <div class="col-md-10 col-offset-md-1">
        <?php $o_page->print_pContent();?>
    </div>
    <div class="col-md-10 col-offset-md-1">
        <?php $o_page->print_pSubcontent();?>
    </div>
</div>

