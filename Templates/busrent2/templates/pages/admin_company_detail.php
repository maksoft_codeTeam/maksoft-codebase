<?php
require_once __DIR__.'/../../forms/admin/ActivateCompany.php';

$status_change_form = new ActivateCompany($_POST, $company, $em);

$status_change_form->add_class('div', 'col-md-12');

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        $status_change_form->is_valid();
        $status_change_form->save();
    } catch (Exception $e) { }
}

?>
<hr>
<div class="container bootstrap snippet">
    <div class="row">
        <div class="col-sm-10">
            <h1><?=$company->getName();?></h1>
        </div>
        <div class="col-sm-2">
            <?php if( $image = array_shift($company->getLogos())) { ?>
                <a href="#" class="pull-right">
                <img title="profile image" class="img-circle img-responsive" src="<?=$image->path?>">
                </a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3"><!--left col-->
              
          <ul class="list-group">
            <li class="list-group-item text-right"><span class="pull-left"><strong>���:</strong></span> <br><?=$company->getOwner();?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>������:</strong></span><br><?=$company->getLicense()?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>���:</strong></span><br><?=$company->getVat()?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>�������:</strong></span><br><?=$company->getPhone()?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>E-mail:</strong></span><br><?=$company->getEmail()?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>�����������:</strong></span><br> <?=$company->created->format('d-m-Y H:i');?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>�������� �������:</strong></span><br><?=$company->updated->format('d-m-Y H:i')?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>������:</strong></span><br><?=$company->active ? '<span  class="text-success">���������</span>' : '<span  class="text-danger"> �����������</span>'?></li>
          </ul> 
          <div class="panel panel-default">
            <div class="panel-heading">������� <i class="fa fa-link fa-1x"></i></div>
            <div class="panel-body">
                <?php if($company->getWebsite()) { ?>
                <a href="<?=$company->getWebsite();?>"><?=$company->getWebsite();?></a>
                <?php } else { echo '���� �������'; } ?>
            </div>
          </div>
          
          
          <ul class="list-group">
            <li class="list-group-item text-muted">������� �� ������ </li>
            <li class="list-group-item text-right">
                <?=$status_change_form;?>
            </li>
          </ul> 
        </div><!--/col-3-->
        <div class="col-sm-9">
          
          <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#home" data-toggle="tab">��������� ������</a></li>
            <li><a href="#vehicles" data-toggle="tab">��������</a></li>
            <li><a href="#settings" data-toggle="tab">Settings</a></li>
          </ul>
              
          <div class="tab-content">
            <div class="tab-pane active" id="home">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>N</th>
                      <th>��</th>
                      <th>��</th>
                      <th>����</th>
                      <th>������ </th>
                      <th>����� ������ </th>
                    </tr>
                  </thead>
                  <tbody id="items">
                <?php 
                    $offers = $company->getOffers();
                    if(!count($offers)) {
                        echo '<tr><td colspan="6" style="text-align:center;">���� ������</td></tr>';
                    }
                    foreach($offers as $offer) { ?>
                        <tr>
                            <td><?=$offer->getId()?></td>
                            <td><?=$offer->getOrder()->getFromCity()?></td>
                            <td><?=$offer->getOrder()->getDestination()?></td>
                            <td><?=$offer->getPrice()?> ��.</td>
                            <td><?=$offer->getStatus() ? '<span  class="text-success">������</span>' : '<span  class="text-danger">��������</span>'?></td>
                            <td>
                                <ul>
                                    <li><?=$offer->getOrder()->getClient()->getFirstName();?> <?=$offer->getOrder()->getClient()->getLastName();?></li>
                                    <li><?=$offer->getOrder()->getClient()->getEmail();?></li>
                                    <li><?=$offer->getOrder()->getClient()->getPhone();?></li>
                                </ul>
                            </td>
                        </tr>

                    <?php } ?>
                  </tbody>
                </table>
                <hr>
                <div class="row">
                  <div class="col-md-4 col-md-offset-4 text-center">
                    <ul class="pagination" id="myPager"></ul>
                  </div>
                </div>
              </div><!--/table-resp-->
              
              <hr>
              
             </div><!--/tab-pane-->
             <div class="tab-pane" id="vehicles">
                    <div class="row">

<?php
$vehicles = $company->getVehicles();
if(!count($vehicles) ) {
    echo '��� ��� ���� �������� �������� ��������'; 
}
?>

<div class='col-md-6'>
<div class="widget-content nopadding">
    <table class="table table-bordered">
      <thead>
          <th>N</th>
          <th>���</th>
          <th>�����</th>
          <th>�����</th>
          <th>���� �����</th>
          <th>������</th>
          <th>���� �� ������������</th>
          <th>������</th>
      </thead>
      <tbody>
<?php $i = 1;?>
<?php foreach($vehicles as $vehicle) { ?>
        <tr>
          <td> <?=$i?></td>
          <td><?=$vehicle->getType()->getName()?></td>
          <td><?=$vehicle->getMake()->getName()?></td>
          <td class="center"> <?=$vehicle->getModel()?></td>
          <td><?=$vehicle->getSeats()?></td>
          <td><?=$vehicle->getFuel()?></td>
          <td><?=$vehicle->getManufacturedDate()->format('Y')?></td>
          <td>
            <ul>
                <?php echo array_reduce($vehicle->getExtras(), function($carry, $item) {
                    $carry .= '<li>'.$item->getName().'['.$item->getCategory()->getName() .']</li>';
                    return $carry;
                }); ?>
            <ul>
          </td>
        </tr>
    <?php $i++;?>
<?php } ?>
      </tbody>
    </table>
  </div>
</div>
                    </div>
              </div>
             <div class="tab-pane" id="settings">
                    4
                  <hr>
              </div>
              </div><!--/tab-pane-->
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
