<?php
$companyRepository = $em->getRepository('\Maksoft\Rent\Bus\Company');
$companies = $companyRepository->findByActive(true);
$i = 0;
define('MAX_TITLE_LENGTH', 50);
echo '<div class="sDelimeter"></div>';
$max_columns = $o_page->_page['show_link_cols'];
$bootstrap_column = ceil(12 / $max_columns);
echo '<div class="row">';
foreach($companies as $company) 
{
    $link = '#';
    $img = $company->getLogos()->first();
    $img = (!empty($img) ? $img->getPath() : '/web/admin/images/no_image.jpg') ;
    $img_alt = $company->getName();
    $full_title = $title = $company->getName();

    echo <<<HEREDOC
    <div class="col-xs-12 col-sm-6 col-md-$bootstrap_column">
        <div class="sPage-content">
                <div class="bullet1" style="float:left;"></div>
                <div class="text title-trunc">$title</div>
            <br style="clear: both;" class="br-style">
            <img src="$img" width="100%" align="" class="align-center img-circle" alt="$img_alt" border="0">
        </div>
    </div>
HEREDOC;

    if($max_columns % $i){
        $i = 0;
        echo '</div><div class="row">';
    } else {
        $i++;
    }
}

?>
</div>

