<?php
require_once __DIR__ .'/../../forms/admin/categories.php';
require_once __DIR__ .'/../../forms/admin/extras.php';
$languageRepository = $em->getRepository('\Maksoft\Rent\Bus\Language');
if(isset($_GET['language'])) {
    $language = $languageRepository->findOneBy(array('id' => $_GET['language']));
    $languages = array();
} else { 
    $languages = $languageRepository->findAll();
}
$vehicleTypeForm = new \Maksoft\Rent\Bus\Form\Admin\VehicleType($_GET['language'], $em, $_POST);
$extrasForm = new ExtraDetail($_GET['vehicleType'], $_GET['category'], $em, $_POST);
$categoryForm = new \Maksoft\Rent\Bus\Form\Admin\Category($_GET['language'], $em, $_POST);
if($_SERVER["REQUEST_METHOD"] === "POST" and isset($_POST["action"])){
    try {

            switch($_POST['action']){
                case "insert_category":
                    $categoryForm->is_valid();
                    $categoryForm->save();
                    break;
                case "insert_extra":
                    $extrasForm->is_valid();
                    $extrasForm->save();
                    break;
                case "type_insert":
                    $vehicleTypeForm->is_valid();
                    $vehicleTypeForm->save();
                    break;
            }
    } catch (Exception $e) {
        Message\error($e->getMessage());
    }
}
?>

<div id="select_language" class="row text-center">
<?php 
Message\display();
if (!empty($languages)) {
    include __DIR__.'/../blocks/language_admin_choice.php';
} else {
    echo '������ ���� - '.$language->getCountry();
}
?>
</div>
<?php
switch(true) { 
    case isset($_GET['language'], $_GET['vehicleType'], $_GET['category']):
        $category = $em->getRepository('\Maksoft\Rent\Bus\Category');
        $category = $category->findOneBy(array("id" => $_GET['category']));
        ?>
        <div class="row text-center">
            <?php echo '<p>�������� �� ������ ��� ��������� <b>'.$category->getName().'</b></p>'; ?>
            <div class="col-md-8 col-md-offset-2">
                <?=$extrasForm?>
            </div>
        </div>
        <?php
        break;
    case isset($_GET['language'], $_GET['vehicleType']):
        $type = $em->getRepository('\Maksoft\Rent\Bus\Type');
        $type = $type->findOneBy(array("id"=>$_GET['vehicleType']));
        echo "<h4> �������� �� ������ �� �������� ��������:</h4> <h3>".$type->getName()."</h3>";
        $category = $em->getRepository('\Maksoft\Rent\Bus\Category');
        $categories = $category->findBy(array("language" => $_GET['language']));

        echo "<h3> �������� �� ��������� </h3>";
        foreach($categories as $category){?>
            <div class="col-md-1">
                <button value="<?=$category->getId()?>" onclick="setGetParameter('category', $(this).val())"> <?=$category->getName()?> </button>
            </div>
        <?}?>
        <div class="row text-center">
            <div class="col-md-8 col-md-offset-2">
                <?=$categoryForm?>
            </div>
        </div>
        <?php
        break;
    case isset($_GET['language']) and !isset($_GET['vehicleType']):
        $types = $em->getRepository('\Maksoft\Rent\Bus\Type');
        $types = $types->findAll();

        ?> <div class="row"> <?
        echo "<h3> ������ �������� ��������: </h3>";
        foreach($types as $type){ ?>
            <div class="col-md-1">
                <button class='button btn-info' value="<?=$type->getId()?>" onclick="setGetParameter('vehicleType', $(this).val())">
                    <?=$type->getName()?>
                </button>
            </div>
        <?php } ?>
        </div>
        <div class="row text-center">
            <p>�������� �� �������� ��������:</p>
            <div class="col-md-8 col-md-offset-2">
                <?=$vehicleTypeForm?>
            </div>
        </div>
        <?php 
        break;
}


?>



<script>
function setGetParameter(paramName, paramValue)
{
    var url = window.location.href;
    var hash = location.hash;
    url = url.replace(hash, '');
    if (url.indexOf(paramName + "=") >= 0)
    {
        var prefix = url.substring(0, url.indexOf(paramName));
        var suffix = url.substring(url.indexOf(paramName));
        suffix = suffix.substring(suffix.indexOf("=") + 1);
        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
        url = prefix + paramName + "=" + paramValue + suffix;
    }
    else
    {
    if (url.indexOf("?") < 0)
        url += "?" + paramName + "=" + paramValue;
    else
        url += "&" + paramName + "=" + paramValue;
    }
    window.location.href = url + hash;
}

</script>
