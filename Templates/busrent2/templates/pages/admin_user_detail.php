<hr>
<div class="container bootstrap snippet">
    <div class="row">
        <div class="col-sm-10">
            <h1><?=$client->getFullName();?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3"><!--left col-->
              
          <ul class="list-group">
            <li class="list-group-item text-right"><span class="pull-left"><strong>�������:</strong></span><br><?=$client->getPhone()?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>E-mail:</strong></span><br><?=$client->getEmail()?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>�����������:</strong></span><br> <?=$client->created->format('d-m-Y H:i');?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>�������� �������:</strong></span><br><?=$client->updated->format('d-m-Y H:i')?></li>
          </ul> 
        </div><!--/col-3-->
        <div class="col-sm-9">
          
          <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#home" data-toggle="tab">����������</a></li>
          </ul>
              
          <div class="tab-content">
            <div class="tab-pane active" id="home">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>N</th>
                      <th>��</th>
                      <th>��</th>
                      <th>����������</th>
                    </tr>
                  </thead>
                  <tbody id="items">
                <?php 
                    $offers = $client->getOffers();
                    if(!count($offers)) {
                        echo '<tr><td colspan="6" style="text-align:center;">���� ������</td></tr>';
                    }
                    foreach($offers as $offer) { ?>
                        <tr>
                            <td><?=$offer->getId()?></td>
                            <td><?=$offer->getFromCity()?></td>
                            <td><?=$offer->getDestination()?></td>
                            <td>
                                ���� �� ����������: <br>
                                <?=$offer->getDeparature()->format('d-m-Y')?>
                                 <br>���� ���������: <br>
                                <?=$offer->getPersons()?>
                                 <br>���� ����: <br>
                                <?=$offer->getChildrens()?>
                                <?php if ($offer->getTwoWay()) { ?>
                                 <br>������� ����������: <br>
                                <?=$offer->getArrival()->format('d-m-Y')?>
                                 <br>�������: <br>
                                <?=$offer->getDays();?>
                                <?php } ?>
                                 <br>���������/���������:
                                <?=$offer->getCompleted() ? '<span  class="text-success">������ ������</span>' : '<span  class="text-danger">��� ��� �� � ������ ������</span>'?>
                            </td>
                        </tr>

                    <?php } ?>
                  </tbody>
                </table>
                <hr>
                <div class="row">
                  <div class="col-md-4 col-md-offset-4 text-center">
                    <ul class="pagination" id="myPager"></ul>
                  </div>
                </div>
              </div><!--/table-resp-->
              
              <hr>
              
             </div><!--/tab-pane-->
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
