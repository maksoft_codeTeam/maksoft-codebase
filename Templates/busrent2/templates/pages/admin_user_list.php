<?php
$clientRepository = $em->getRepository('\Maksoft\Rent\Bus\Client');

if(isset($_GET['user_id']) and is_numeric($_GET['user_id']) and $_GET['user_id'] > 0) {
    $client = $clientRepository->findOneById($_GET['user_id']);
    if(!$client) { 
        echo '<h2>���� ������ ��������</h2>';
    } else {
        require_once __DIR__ .'/admin_user_detail.php';
        return;
    }
}

$clients = $clientRepository->findAll();

echo '<table class="table table-responsive">';
echo <<<DOC
        <th>N</th>
        <th>���</th>
        <th>E-mail</th>
        <th>�������</th>
        <th>���� �� �����������</th>
        <th>��������</th>
DOC;
$i = 1;
$link = array('n' => $n, 'SiteID' => $SiteID);
foreach ($clients as $client) {
    $link['user_id'] = $client->getId();
    $detail_link = http_build_query($link);
    echo <<<HERE
    <tr>
        <td>$i. </td>
        <td> {$client->getFirstName()} {$client->getMiddleName()} {$client->getLastName()}</td>
        <td> {$client->getEmail()}</td>
        <td> {$client->getPhone()}</td>
        <td> {$client->created->format('d-m-Y')}</td>
        <td> <a href="/page.php?{$detail_link}" class="btn btn-success"> ������� </a> </td>
    </tr>
HERE;
    $i++;
}

echo '</table>';
