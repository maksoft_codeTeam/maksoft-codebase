<?php
require_once __DIR__.'/../../forms/TripOffer.php';
if(is_authenticated()){
    $form = new TripOffer($em, $_POST, process_order_data($_SESSION[PREFIX][SESSION_RENT]));
} else {
    $form = new TripOffer($em, $_POST);
}

if(isset($_SESSION[PREFIX][SESSION_RENT]['extras'])){
    $checked_extras = process_order_data($_SESSION[PREFIX][SESSION_RENT]); 
    $checked_extras = $checked_extras['extras'];
}

$extras= process_extras($em);
$extras = $extras['extras'];
$template = __DIR__.'/../forms/offer.php';
if($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        $form->is_valid();
        $_SESSION[PREFIX][SESSION_RENT] = $form->clean_data();
        if(is_authenticated()) {
            Message\success('������ ��������� � �������� �������. � ������� �� ������� ������ �� �������� �����');
            $form->save();
            echo '<script>window.location = \''.$o_page->get_pLink(PAGE_OFFERS).'\';</script>';
        } else {
            $template = TEMPLATE_PATH.'/pages/login_register.php';
        }
    } catch (\Exception $e) {
        Message\warning($e->getMessage());
    }
}

?>
<div class='awe-search-tabs tabs'>
  <ul>
    <li><a href='#awe-search-tabs-1'><i class='awe-icon awe-icon-bus'></i></a></li>
    <li><a href='#awe-search-tabs-2'><i class='awe-icon awe-icon-briefcase'></i></a></li>
  </ul>
  <div class='awe-search-tabs__content tabs__content'>
    <?php Message\display();?>
    <div id='awe-search-tabs-1' class='search-bus'>
      <h2><?=$o_page->_page['Title'];?></h2>
        <?php require_once $template; ?>
    </div>
    <div id='awe-search-tabs-2' class='search-bus'>
        <?php require_once __DIR__.'/list_companies.php';; ?>
    </div>
  </div>
</div>
 <script>
    window.onload = function() {
        jQuery('#from_city').keyup(function(){
            return auto_complete(jQuery(this));
        });

        jQuery('#to_city').keyup(function(){
             auto_complete(jQuery(this));
        });
    };
 </script>
