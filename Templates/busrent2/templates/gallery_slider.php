<div class='product-detail__gallery'>
    <div class='product-slider-wrapper'>
        <div class='product-slider owl-carousel owl-theme' style='opacity: 1; display: block;'>
            <div class='owl-wrapper-outer'>
                <div class='owl-wrapper-outer'>
                    <?php foreach ($images as $image) { ?>
                        <div class='owl-item'>
                        <div class='item'><img src='<?=$image->getPath()?>' alt=''></div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class='owl-controls clickable'>
                <div class='owl-buttons'>
                    <div class='owl-prev'><i class='fa fa-caret-left'></i></div>
                    <div class='owl-next'><i class='fa fa-caret-right'></i></div>
                </div>
            </div>
        </div>
        <div class='product-slider-thumb-row'>
            <div class='product-slider-thumb owl-carousel owl-theme' style='opacity: 1; display: block;'>
                <div class='owl-wrapper-outer'>
                    <div class='owl-wrapper' >
                    <?php foreach ($images as $image) { ?>
                        <div class='owl-item synced'>
                            <div class='item'><img src='<?=$image->getPath()?>' alt=''></div>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='product-map'>
        <div data-latlong='21.036697, 105.834871'></div>
    </div>
</div>
