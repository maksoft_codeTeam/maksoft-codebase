<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?=STATIC_URL?>js/lib/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>js/lib/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>js/lib/jquery.owl.carousel.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>js/lib/theia-sticky-sidebar.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>js/lib/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>js/lib/jquery-ui.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>js/scripts.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>js/custom.js"></script>
<script async type="text/javascript" src="//rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>revslider/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>revslider/js/jquery.themepunch.tools.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
        jQuery.each(jQuery('input[class="datepicker"]'), function(i, el){
            jQuery(el).datepicker({ dateFormat: 'yy-mm-dd' });
        });
        jQuery('#slider-revolution').show().revolution({
            ottedOverlay:"none",
            delay:10000,
            startwidth:1600,
            startheight:450,
            hideThumbs:200,

            thumbWidth:100,
            thumbHeight:50,
            thumbAmount:5,
            simplifyAll:"off",
            navigationType:"none",
            navigationArrows:"solo",
            navigationStyle:"preview4",
            touchenabled:"on",
            onHoverStop:"on",
            nextSlideOnWindowFocus:"off",
            swipe_threshold: 0.7,
            swipe_min_touches: 1,
            drag_block_vertical: false,
            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
            keyboardNavigation:"off",
            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:20,
            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:20,
            soloArrowLeftVOffset:0,
            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:20,
            soloArrowRightVOffset:0,
            shadow:0,
            fullWidth:"on",
            fullScreen:"off",
            spinner:"spinner2",
            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,
            shuffle:"off",
            autoHeight:"off",
            forceFullWidth:"off",
            hideThumbsOnMobile:"off",
            hideNavDelayOnMobile:1500,
            hideBulletsOnMobile:"off",
            hideArrowsOnMobile:"off",
            hideThumbsUnderResolution:0,
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            startWithSlide:0
        });
        <?php 
        foreach ($_SESSION['system']['messages'] as $status_code => $message_bag) {
            while($message = array_shift($_SESSION['system']['messages'][$status_code])){
                echo msg_notify($status_code, $message);
                unset($_SESSION['system']['messages'][$status_code]);
            }
        }
        ?>
});
</script>
