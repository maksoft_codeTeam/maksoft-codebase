<?php
require_once BASE_DIR.'/forms/AcceptCompanyOffer.php';
require_once BASE_DIR.'/forms/RejectCompanyOffer.php';

$offers= $order->getOffers();
$accept_offer = new AcceptCompanyOffer($em, $_POST);
$reject_offer = new RejectCompanyOffer($em, $_POST);
if($_SERVER['REQUEST_METHOD'] === 'POST'){
    switch ($_POST['action']) {
    case $accept_offer->action->value:
        submit($accept_offer);
        break;
    case $reject_offer->action->value:
        submit($reject_offer);
        break;
    }
}

?>
<a href="<?=$o_page->get_pLink(PAGE_OFFERS);?>" class="tp-caption sfb fadeout awe-btn awe-btn-style3 awe-btn-slider start">�����</a>
<section class="product-detail" style="transform: none;">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <tbody>
                    <th>�������� ��:</th>
                    <th>���������� �:</th>
                    <th>���� �� ����������:</th>
                    <th>���� �� ����������:</th>
                    <th>���� ���������:</th>
                    <th>���� ����:</th>
                    <th>������� � ���:</th>
                    <th>������</th>
                    <th>������</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tbody>
                    <tr>
                        <td><?=$order->getFromCity()?></td>
                        <td><?=$order->getDestination()?></td>
                        <td><?=$order->getDeparature()->format('d-m-Y')?></td> 
                        <td>
                            <?php 
                            if ($order->getTwoWay() > 0) { 
                                echo $order->getArrival()->format('d-m-Y');
                            } else {
                                echo 'Na';
                            }
                            ?>
                        </td>
                        <td><?=$order->getPersons()?></td>
                        <td><?=$order->getChildrens()?> </td>
                        <td><?=$order->getDays()?></td>
                        <td><?=$order->getCompleted() == 0 ? '� ������ �� ���������' : '����������'?></td>
                        <td>
                            <ul>
                        <?php foreach($order->getExtras() as $extra) {?>
                            <li><?=$extra->getName()?></li>
                        <?php } ?>
                            </ul>
                        </td>
                        <td>
                            <?=$delete->start()?>
                            <input type="hidden" value="<?=$order->getId()?>" name="order_id">
                            <?=$delete->action?>
                            <?=$delete->submit?>
                            <?=$delete->end()?>
                        </td>
                        <td> <?php if($offers == 0){ echo '���� ������'; }?> </td>
                    </tr>
                </table>
                <?php foreach($offers as $offer) { ?>
                    <?php
                        $company = $offer->getCompany();
                    ?>
                    <?php include __DIR__.'/company_single_offer.php'; ?>
                    <hr align="left" color="yellow" width="150px" size="20px">
                <?php } ?>
            </div>
            </div>
    </div>
    </section>
