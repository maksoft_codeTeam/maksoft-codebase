<?php
if (!isset($dd_links)) {
  $dd_links = 2;
}
$dd_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $dd_links");
?>
<ul class="menu-list">
  <li class="menu-item-has-children current-menu-parent">
      <a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>">������</a>
  </li>
<?php
foreach ($dd_links as $page) { 
    if(AUTHENTICATED and $page['n'] == LOGIN_PAGE) {
        continue;
    }

    if(!AUTHENTICATED and $page['n'] == PAGE_OFFERS) {
        continue;
    }
    $subpages = $o_page->get_pSubpages($page['n']);
    $has_children_class =((count($subpages) > 0) ? "class=\"menu-item-has-children current-menu-parent\"" : ""); 
?>
    <li <?=$has_chilren_class?>>
        <a href="<?=$page['page_link'];?>"><?=$page['Name'];?></a>
        <?php if(count($subpages)>0) { ?>
            <ul class="sub-menu">
            <?php echo array_reduce($subpages, function($carry, $item) {
                    $carry .= sprintf('<li><a href="%s">%s</a></li>', $item['page_link'], $item['Name']);
                    return $carry;
                }); ?>
            </ul>
        <?php } ?>
    </li>
<?php } ?>
    <?php
    if(AUTHENTICATED){
        $user_name = $b_user['first_name'] . ' ' .$b_user['last_name'];
        echo '<li><a href="#">���������, '.$user_name.'</a></li>';
        echo '<li><a href="/Templates/busrent2/logout.php">�����</a></li>';
    }
    ?>
</ul>
