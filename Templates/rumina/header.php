<!DOCTYPE html>
<html lang="bg" class="responsive-layout product-page">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?=$Title?></title>
    
<?php
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();

if(($o_page->n)=="19330641"){
	 $body_class = "index";
} else {
 $body_class = "main";
 $main = "navbar-main";
}

?>

    <!-- Bootstrap Core CSS -->
    <link href="<?=TEMPLATE_PATH?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=TEMPLATE_PATH?>assets/css/style.css" rel="stylesheet">
    <link href="<?=TEMPLATE_PATH?>assets/css/figure.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=TEMPLATE_PATH?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Philosopher:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/masterslider/css/masterslider.main.css' type='text/css' media='all' />
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="<?=$body_class?>">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top <?=$main?>">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
            
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll fade-in logo" href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>"><img src="<?=TEMPLATE_PATH?>assets/img/rumina_logo.png" class="rumina-logo" alt="Rumina Men's & lady's fashion" /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           

    <?php
	if(!isset($drop_links)) $drop_links = 2;
	$dd_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $drop_links");
	if(count($dd_links) > 0)
		{
          print_menu($dd_links, array('menu_depth'=>1, 'params'=>"class=\"nav navbar-nav cl-effect-4 margin-menu\""));
		}
    ?>
    
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                    <a href="#page-top"></a>
                    </li>

<li class="facebook"><a href="https://www.facebook.com/Rumina96/?fref=ts" class="fb"><i class="fa fa-facebook"></i></a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
</nav>
