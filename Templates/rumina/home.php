<?php
  $body_class = "index";
?>
      
      <div class="gdlr-full-size-wrapper gdlr-show-all"  style="padding-top: 70px;"  ><div class="gdlr-master-slider-item gdlr-slider-item gdlr-item"  style="margin-bottom: 0px;"  >
		<!-- MasterSlider -->
		<div id="P_MS5628cf5090bb7" class="master-slider-parent ms-parent-id-1"  >
				
			
			<!-- MasterSlider Main -->
			<div id="MS5628cf5090bb7" class="master-slider ms-skin-default" >
            
            
            				<div  class="ms-slide" data-delay="7" data-fill-mode="fill"  >
					<img src="<?=TEMPLATE_PATH?>assets/img/slides/slider-1.jpg" alt="" title="" data-src="<?=TEMPLATE_PATH?>assets/img/slides/slider-1.jpg" />

				</div>
                
                				<div  class="ms-slide" data-delay="7" data-fill-mode="fill"  >
					<img src="<?=TEMPLATE_PATH?>assets/img/slides/slider-2.jpg" alt="" title="" data-src="<?=TEMPLATE_PATH?>assets/img/slides/slider-2.jpg" />

				</div>
                				<div  class="ms-slide" data-delay="7" data-fill-mode="fill"  >
					<img src="<?=TEMPLATE_PATH?>assets/img/slides/slider-3.jpg" alt="" title="" data-src="<?=TEMPLATE_PATH?>assets/img/slides/slider-3.jpg" />

				</div>		
               		 				 
               
			</div>
			<!-- END MasterSlider Main -->

			 
		</div>
		<!-- END MasterSlider --> 
		
		<script>
		(function ( $ ) {
			"use strict";

			$(function () {
				var masterslider_0bb7 = new MasterSlider();

				// slider controls
				masterslider_0bb7.control(''     ,{ autohide:true, overVideo:true  });                    
				masterslider_0bb7.control('bullets'    ,{ autohide:false, overVideo:true, dir:'h', align:'bottom', space:8 , margin:25  });
				// slider setup
				masterslider_0bb7.setup("MS5628cf5090bb7", {
						width           : 1140,
						height          : 580,
						minHeight       : 0,
						space           : 0,
						start           : 1,
						grabCursor      : true,
						swipe           : false,
						mouse           : true,
						keyboard        : false,
						layout          : "fullwidth", 
						wheel           : false,
						autoplay        : true,
						instantStartLayers:false,
						loop            : true,
						shuffle         : false,
						preload         : 0,
						heightLimit     : true,
						autoHeight      : false,
						smoothHeight    : true,
						endPause        : false,
						overPause       : true,
						fillMode        : "fill", 
						centerControls  : true,
						startOnAppear   : false,
						layersMode      : "center", 
						autofillTarget  : "", 
						hideLayers      : false, 
						fullscreenMargin: 0,
						speed           : 20, 
						dir             : "h", 
						parallaxMode    : 'swipe',
						view            : "fadeFlow"
				});
				

								MSScrollParallax.setup( masterslider_0bb7, 30, 50, true );
				$("head").append( "<link rel='stylesheet' id='ms-fonts'  href='//fonts.googleapis.com/css?family=Open+Sans:300,700|Merriweather:regular' type='text/css' media='all' />" );

				window.masterslider_instances = window.masterslider_instances || [];
				window.masterslider_instances.push( masterslider_0bb7 );
			 });
			
		})(jQuery);
		</script> 
		
</div><div class="clear"></div><div class="clear"></div></div><div class="clear"></div>

<div class="col-md-12 no-padding">
<div class="col-md-3 no-padding">
        <div id="box" class="burst-circle teal">
            <div class="caption"></div>
            <a href="<?=$o_page->get_pLink(19336496)?>"><img src="<?=TEMPLATE_PATH?>assets/img/cats/bulchenski-rokli.jpg" class="img-responsive" alt="<?=$o_page->get_pName(19336496)?>" /></�>
            <h1>������</h1>
        </div>
        <div class="cat-title"><p><a href="<?=$o_page->get_pLink(19336496)?>"><?=$o_page->get_pName(19336496)?></a></p></div>        
</div>

<div class="col-md-3 no-padding">
        <div id="box" class="burst-circle teal">
            <div class="caption"></div>
            <a href="<?=$o_page->get_pLink(19336599)?>"><img src="<?=TEMPLATE_PATH?>assets/img/cats/damska-moda.jpg" class="img-responsive" alt="<?=$o_page->get_pName(19336599)?>" /></�>
            <h1>������</h1>
        </div>
        <div class="cat-title"><p><a href="<?=$o_page->get_pLink(19336599)?>"><?=$o_page->get_pName(19336599)?></a></p></div>   
</div>
<div class="col-md-3 no-padding">
        <div id="box" class="burst-circle teal">
            <div class="caption"></div>
            <a href="<?=$o_page->get_pLink(19336600)?>"><img src="<?=TEMPLATE_PATH?>assets/img/cats/myjka-moda.jpg" class="img-responsive" alt="<?=$o_page->get_pName(19336600)?>" /></�>
            <h1>������</h1>
        </div>
        <div class="cat-title"><p><a href="<?=$o_page->get_pLink(19336600)?>"><?=$o_page->get_pName(19336600)?></a></p></div>   
</div>
<div class="col-md-3 no-padding">
        <div id="box" class="burst-circle teal">
            <div class="caption"></div>
            <a href="<?=$o_page->get_pLink(19336601)?>"><img src="<?=TEMPLATE_PATH?>assets/img/cats/detska-moda.jpg" class="img-responsive" alt="<?=$o_page->get_pName(19336601)?>" /></�>
            <h1>������</h1>
        </div>
        <div class="cat-title"><p><a href="<?=$o_page->get_pLink(19336601)?>"><?=$o_page->get_pName(19336601)?></a></p></div> 
</div>
</div>
<div class="marq-content">
       <marquee behavior="scroll" direction="left">
       <?=$o_page->print_pContent();?>
       </marquee>
</div>
