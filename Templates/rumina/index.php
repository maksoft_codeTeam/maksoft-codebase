<?php
			
	include "config.php";
	
	include TEMPLATE_DIR_PHP . "header.php";
	
	// Page Template
	$pTemplate = $o_page->get_pTemplate();
	if($o_page->_page['SiteID'] != $o_site->_site['SitesID'] )
		include TEMPLATE_DIR_PHP."admin.php";	
	elseif($pTemplate['pt_url'])
		include $pTemplate['pt_url'];
	elseif($o_page->_page['n'] == $o_site->_site['StartPage'] && strlen($search_tag)<3 && strlen($search)<3)
		include TEMPLATE_DIR_PHP . "home.php";
	else
		include TEMPLATE_DIR_PHP . "main.php";
		
	include TEMPLATE_DIR_PHP . "footer.php";

?>