            <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" 
  role="button" title="����� �� ������" data-toggle="tooltip" data-placement="top">
  <i class="fa fa-chevron-up" aria-hidden="true"></i>
</a>

<div class="copyright">

<?php include("Templates/copyrights_links.php"); ?>

</div>
 
    <!-- jQuery -->
   <script src="<?=TEMPLATE_PATH?>assets/js/jquery.js"></script>
   <script>jQuery.noConflict();</script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=TEMPLATE_PATH?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript">
    $(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');
});
</script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="<?=TEMPLATE_PATH?>assets/js/classie.js"></script>
    <script src="<?=TEMPLATE_PATH?>assets/js/cbpAnimatedHeader.js"></script>

<script type='text/javascript' src='https://maksoft.net/Templates/einstein-house/assets/plugins/masterslider/public/assets/js/masterslider.min9066.js?ver=2.14.2'></script>
<script>
var $ = jQuery;
jQuery(document).ready(function(){
<?php echo implode(PHP_EOL, $custom_js);?>
});
</script>
<?php include "Templates/footer_inc.php"; ?>
</body>

</html>
