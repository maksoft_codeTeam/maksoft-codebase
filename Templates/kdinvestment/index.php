<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML lang=sr xml:lang="sr" xmlns="http://www.w3.org/1999/xhtml">
<HEAD><TITLE>
<?php echo("$Title");?>
</TITLE>
<?php
include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/kdinvestment/");
define("DIR_TEMPLATE_IMAGES","Templates/kdinvestment/images/");

define("DIR_MODULES","web/admin/modules/");
?>
<LINK href="http://www.kd-group.com/design/images/favicon.ico" rel="shortcut icon">
<LINK href="http://www.kd-group.com/design/images/favicon.ico" rel=icon>

<SCRIPT src="<?=DIR_TEMPLATE?>kd.js" defer type=text/javascript></SCRIPT>
<SCRIPT src="<?=DIR_TEMPLATE?>scripts.js" defer type=text/javascript></SCRIPT>
<SCRIPT src="<?=DIR_TEMPLATE?>animator.js" defer type=text/javascript></SCRIPT>
<?php
if($row->SiteID != $SiteID)
	{
		?>
			<LINK media=all href="<?=DIR_TEMPLATE?>layout_admin.css" type=text/css rel=stylesheet>
		<?
	}
else
	{
	?>
			<LINK media=screen href="<?=DIR_TEMPLATE?>layout.css" type=text/css rel=stylesheet>
	<?php
	}
?>

<LINK media=print href="<?=DIR_TEMPLATE?>layoutprint.css" type=text/css rel=stylesheet>

<?
if($row->n == $Site->StartPage)
	{
		?>
			<LINK media=all href="<?=DIR_TEMPLATE?>layoutintro.css" type=text/css rel=stylesheet>
		
		<?
	}
else
	{
		?>
            <!--[if lte IE 6.0]>
            <link href='http://www.maksoft.net/Templates/kdinvestment/layout_ie6.css' rel='stylesheet' type='text/css'>
            <![endif]-->
		<?
		}
?>
<SCRIPT type=text/javascript>
window.onload = function() {repositionpagelayout('contdiv', 'rightdiv'); repositionsubmeni(); if (window.graphinitmarkers) {graphinitmarkers(graphmarkerxmlfn);}};
</SCRIPT>

<META content="MSHTML 6.00.5730.11" name=GENERATOR><meta http-equiv="Content-Type" content="text/html; charset=windows-1251"></HEAD>
<BODY><A name=top></A>

<div class="pageholder">
	<div id="smkd" class="smkd" onmouseover="smkdstophidetimer()" onmouseout="smkdstarthidetimer();">
	<div style="width:480px;height:580px;">
		<div class="twocolfloat">
		<h3>Investicijski skladi</h3>
		<a href="http://www.kd-group.bg" target="_blank" rel="nofollow">KD Investments EAD, Sofija</a>
		<a href="http://www.kd-group.hr" target="_blank" rel="nofollow">KD Investments d. o. o., Zagreb</a>
		<a href="http://www.kd-group.com" target="_blank" rel="nofollow">KD Investments Sp. Z. o. o., Varsava</a>
		<a href="http://www.kd-group.com" target="_blank" rel="nofollow">KD Investments a. d., Beograd</a>
		<a href="http://www.kd-group.ro" target="_blank" rel="nofollow">KD Investments S. A., Bukaresta</a>
		<a href="http://www.kd-group.sk" target="_blank" rel="nofolow">KD Investments, sprav. spol. a. s., Bratislava</a>
		<a href="http://www.kd-skladi.si" target="_blank" rel="nofolow">KD Skladi d. o. o., Ljubljana</a>
		<a href="http://www.kd-group.si" target="_blank" rel="nofolow">KD ID d. d., Ljubljana</a>
		<a href="http://www.financna-tocka.si" target="_blank" rel="nofolow">KD Financna tocka d. o. o., Ljubljana</a>
		<h3>Individualno upravljanje in borzno posredovanje</h3>
		<a href="http://www.kd-group.bg" target="_blank" rel="nofolow">KD Securities EAD, Sofija</a>
		<a href="http://www.kd-group.hr" target="_blank" rel="nofolow">KD upravljanje imovinom d. o. o., Zagreb</a>
		<a href="http://www.kd-group.ro" target="_blank" rel="nofolow">KD Capital Management S. A., Bukaresta</a>
		<a href="http://www.kd-bpd.si" target="_blank" rel="nofolow">KD BPD, d. o. o., Ljubljana</a>
		<h3>Zivljenjska zavarovanja</h3>
		<a href="http://www.kd-life.bg" target="_blank" rel="nofolow">KD Life AD, Sofija</a>
		<a href="http://www.kd-group.ro" target="_blank" rel="nofolow">KD Life Asigurari, S. A., Bukaresta</a>
		<a href="http://www.slovenica-life.sk" target="_blank" rel="nofolow">KD LIFE plc, Bratislava</a>
		<a href="http://www.kd-zivljenje.si" target="_blank" rel="nofolow">KD Zivljenje, zavarovalnica, d. d.</a>
		<a href="http://www.financna-tocka.si" target="_blank" rel="nofolow">KD Financna tocka d. o. o., Ljubljana</a>
		<h3>Nezivljenjska zavarovanja</h3>
		<a href="http://www.adriatic-slovenica.si" target="_blank" rel="nofolow">Adriatic Slovenica d. d., Koper</a>
		<a href="http://www.coris.si" target="_blank" rel="nofolow">Assistance CORIS d. o. o., Ljubljana</a>
		<h3>Banke</h3>
		<a href="http://www.dbs.si" target="_blank" rel="nofolow">Dezelna banka Slovenije d. d., Ljubljana</a>
		<h3>Izobrazevanje</h3>
		<a href="http://www.gea-college.si" target="_blank" rel="nofolow">GEA College d. d., Ljubljana</a>
		</div>
		<div class="twocolfloat">
		<h3>Nepremicnine</h3>
		<a href="http://www.kd-group.com" target="_blank" rel="nofolow">KD Kvart d. o. o., Ljubljana</a>
		<a href="http://www.kd-group.com" target="_blank" rel="nofolow">R.E. Invest d. o. o., Ljubljana</a>
		<h3>Mediji</h3>
		<a href="http://www.kmeckiglas.com" target="_blank" rel="nofolow">CZD Kmecki glas d. o. o., Ljubljana</a>
		<a href="http://www.radio-kranj.si" target="_blank" rel="nofolow">Radio Kranj d. o. o., Kranj</a>
		<h3>Privatizacijski skladi na Balkanu</h3>
		<a href="http://www.abds.com.ba" target="_blank" rel="nofolow">ABDS d. d., Sarajevo</a>
		<a href="http://www.vibfond.com" target="_blank" rel="nofolow">VIB a. d., Banja Luka</a>
		<a href="http://www.kd-group.com" target="_blank" rel="nofolow">KD MONT A. D., Podgorica</a>
		<h3>Navtika</h3>
		<a href="http://www.seaway.si" target="_blank" rel="nofolow">Seaway Group d. o. o., Begunje</a>
		<h3>Turizem</h3>
		<a href="http://www.vogel.si" target="_blank" rel="nofolow">Zicnice Vogel Bohinj d. d., Bohinjsko Jezero</a>
		<h3>Ostalo</h3>
		<a href="http://www.evolve.si" target="_blank" rel="nofolow">Evolve d. o. o., Ljubljana</a>
		<a href="http://www.kdplus.si" target="_blank" rel="nofolow">KD Mark, d. o. o., Ljubljana</a>
		<a href="http://www.kd-group.com" target="_blank" rel="nofolow">Concorde PS, d. o. o.</a>
		<a href="http://www.semenarna.si" target="_blank" rel="nofolow">Semenarna Ljubljana d. d., Ljubljana</a>
		<a href="http://www.vrtnarstvo-celje.si" target="_blank" rel="nofolow">Vrtnarstvo Celje d. o. o., Celje</a>
		<a href="http://www.kd-group.com" target="_blank" rel="nofolow">KD Kapital d. o. o., Ljubljana</a>
		</div>
	</div>
	<!--[if lte IE 6.5]><![endif]-->
	</div>


<DIV class="prehead">
		<A href="http://www.kd-group.com/" target=_blank>www.kd-group.com</A>
<?php
	//versions
		$v_query_str = "SELECT * FROM Sites, versions WHERE Sites.SitesID = '$SiteID' AND Sites.SitesID = versions.SiteID"; 
		//echo("$v_query_str"); 
		$versions_query = mysqli_query("$v_query_str"); 
		while ($ver = mysqli_fetch_object($versions_query)) 
		 echo "<A style=\"PADDING-RIGHT: 0px; PADDING-LEFT: 5px; MARGIN-LEFT: 25px; BORDER-LEFT: #6d6e70 1px solid; TEXT-DECORATION: none\" href=\"page.php?n=$ver->n&SiteID=$ver->verSiteID\">$ver->version</A>";
?>


		<DIV class=spletnamestakd>
		<A id=smkdbtn onMouseOver="startopensmkd(false);"  onclick="startopensmkd(true); return false;" onmouseout=smkdstarthidetimer(); href="http://www.kd-investments.co.yu/#">KD �� �����</A>
		</DIV>
</DIV>

<div class=head>
<A id=logooutput href="http://<?=$Site->url?>"><h1 class="logo"></h1></A>

<DIV class=toolbar>
<DIV class=meni><A href="page.php?n=35344&SiteID=554">��������</A>
<A style="BORDER-RIGHT-WIDTH: 0px" href="page.php?n=36270&SiteID=554">����� �� �����</A> </DIV>
<DIV class=search>
<FORM style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-TOP: 0px" action="/web/search.php" method=post>
			<input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
			<input name="res_no" type="hidden" id="res_no" value="36274">
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TBODY>
  <TR>
    <TD style="PADDING-RIGHT: 5px; VERTICAL-ALIGN: middle" align=right width="100%">
	<INPUT class=searchinputbox id=searchinputfield name=Search> </TD>
    <TD style="VERTICAL-ALIGN: middle; BACKGROUND-COLOR: #6e6f71"><INPUT class=submitbtn type=submit value="GO"> 
    </TD></TR></TBODY></TABLE></FORM>
 </DIV></DIV></DIV>
<DIV class="prehead"></div>
<DIV class=bodyholder>
<DIV class=mainmeniholder id=mainmeniholder>
<DIV class=mainmeni>
<?php
//top buttons
	$res = mysqli_query("SELECT * FROM pages WHERE SiteID = $SiteID AND ParentPage = $Site->StartPage AND toplink = 1 ORDER by sort_n ASC"); 
	$top_links = "<a href='page.php?n=".$Site->StartPage."&SiteID=".$SiteID."'>".$Site->Home."</a>";
	$i=0;
	 while ($top_links_row = mysqli_fetch_object($res)) 
			{
			
				if($n == $top_links_row->n) $class = "class=selected";
					else $class = "";
				$top_links.= "<a href='page.php?n=".$top_links_row->n."&SiteID=".$SiteID."' $class>".$top_links_row->Name."</a>";
				$i++;
			}
	echo "<div id=\"top_links\">".$top_links."</div>";
?>
</DIV>
<DIV class=pagenav>
<?php 
	echo "<a href=\"page.php?n=$Site->StartPage&SiteID=$SiteID\">".$Site->Home ."</a> / <a href=\"page.php?n=$row->n&SiteID=$SiteID\">". $row->Name."</a>"; 
?>
<DIV class=subpagetoolbar>
<a class=tafbtn  href="mailto:kds.office@kd-group.bg?subject=KD%20Securities%20�������&body=����%20��%20��%20��������%20www.kd-securities.bg/page.php?n=<?=$n?>"><IMG src="<?=DIR_TEMPLATE_IMAGES?>ico_prijatelju.gif" border=0></a>
<a class=tafbtn  href="page.php?n=35344&SiteID=554"><IMG src="<?=DIR_TEMPLATE_IMAGES?>ico_komentar.gif" border=0></a>
<a class=tafbtn  href="#" onclick="javascript:window.print()"><IMG src="<?=DIR_TEMPLATE_IMAGES?>ico_natisni.gif" border=0></a> 
</DIV>
</DIV>
</DIV>
<DIV class=body>
<DIV class=cont id=contdiv>
<?php
	if($row->n == $Site->StartPage)
		include "header.php";

	if($row->n != $Site->StartPage && $row->SiteID == $SiteID)
		include "column_left.php";
?>
<DIV class=intromeniholder style="text-align: left;">
<?php
	if($row->n == $Site->StartPage)
		include DIR_TEMPLATE . "home.php";
	else 
		{
		echo "<div style=\"height: auto; min-height: 700px; width: 550px; margin-left: 70px; text-align: left;\"><div class=\"title\"><h1>".$row->Name."</h1></div>".$str;
	
		  if ($user->AccessLevel >= $row->SecLevel)
			{
				if (!(empty($row->PageURL)))
							include_once("$row->PageURL");
	
			}
			//include DIR_TEMPLATE . "other_pages.php";
			eval("$row->PHPcode"); 
			echo "</div>";
			}	


	if($row->SiteID == $SiteID)
	{
		?>
		<DIV class=right id=rightdiv style="display: none;">
			<?php
				$query = mysqli_query("SELECT * FROM pages WHERE toplink = 4 AND SIteID = '$SiteID' ORDER by sort_n ASC");
				while($vip_link = mysqli_fetch_object($query))
					echo "<DIV class=orange style=\"margin: 2px; padding: 3px;\"><A href=\"page.php?n=$vip_link->n&SiteID=$vip_link->SiteID\">".$vip_link->Name."</A></DIV>";
				echo "<br>";
				$query = mysqli_query("SELECT * FROM pages WHERE toplink = 5 AND SIteID = '$SiteID' ORDER by sort_n ASC");
				while($second_link = mysqli_fetch_object($query))
					echo "<DIV class=gray style=\"margin: 2px; padding: 3px;\"><A href=\"page.php?n=$second_link->n&SiteID=$second_link->SiteID\">".$second_link->Name."</A></DIV>";
			?>
			
		</DIV>
		<?
	}
?>
</DIV>
</DIV>
<DIV style="CLEAR: both; HEIGHT: 1px"></DIV>
</DIV>
<DIV class="navbar" style="margin-bottom: 5px;">
<?php
	echo "<center>".$nav_bar."</center>";
?>
</DIV>
<DIV class=footer>
<DIV class=copyright>
<?php
	echo $Site->copyright;
?>
<A href="http://www.maksoft.net">hosting & CMS</A> 
</DIV></DIV></DIV></DIV>
<SCRIPT type=text/javascript>
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</SCRIPT>

<SCRIPT type=text/javascript>
var pageTracker = _gat._getTracker("UA-5455098-1");
pageTracker._trackPageview();
</SCRIPT>
</BODY></HTML>
