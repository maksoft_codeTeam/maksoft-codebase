<link rel="stylesheet" href="http://www.maksoft.net/lib/jquery-ui/themes/base/jquery.ui.all.css">
<link rel="stylesheet" href="lib/jquery-ui/theme/jquery.ui.all.css">
<style>
	img.ui-datepicker-trigger{cursor: pointer;}
</style>
<!--Load the AJAX API-->
<script src="https://www.google.com/jsapi"></script>
<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.core.js"></script>
<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.widget.js"></script>
<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.datepicker.js"></script>
<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.tabs.js"></script>

<script>
	$j(document).ready(function(){
		$j("#tabs").tabs();
		})
</script>
<?php
if($o_page->get_uLevel() >= $o_page->get_pInfo("SecLevel"))
{
	$today = date("m-d");
	if(empty($from_date)) $from_date = (date("Y")-1) ."-". $today;
	if(empty($to_date)) $to_date = date("Y-m-d");

	if(!isset($fund_id)) die("No fund selected!");
	mysql_select_db("kd");
	
	$fund_sql = "SELECT * FROM funds WHERE fund_id = '".$fund_id."'";
	$fund_result = mysqli_query($fund_sql);
	$selected_fund = mysqli_fetch_array($fund_result);
	
	include "templates/".$selected_fund['fund_file'];
	
	//insert fund data
	if($_POST['action'] == "insert_fund_data")
		{
			$fund_table = $selected_fund['fund_table'];
			$insert_data_set = "";
			$data_counter = 0;
			while(list($var, $value) = each ($_POST))
				{
					if($var != "action")
						{
							$value = str_replace(",", ".", $value);
							$value = str_replace(" ", "", $value);
							$value = str_replace("%", "", $value);
							if($var == "OnDate") $value = date("Y-m-d", strtotime($value));
							if($data_counter>0) $insert_data_set.=", ";
							$insert_data_set.= $var."='".$value."'";
						}
					$data_counter++;
				}
			//echo $insert_data_set;
			
			//print_r($_POST);
			$insert_data_sql = "INSERT INTO ".$fund_table. " SET ".$insert_data_set;
			mysqli_query($insert_data_sql);
		}
	
	//read CSV file
	if($_POST['action'] == "insert_fund_file")
		{
			//upload file
			//print_r($_FILES);
			$csv_dir = "/hosting/maksoft/maksoft/Templates/kdinvestment/chart/data/";
			$upload_status = file_upload($_FILES['fund_file_data']['name'], $_FILES['fund_file_data']['tmp_name'], $csv_dir, $_FILES['fund_file_data']['type'], $_FILES['fund_file_data']['size'], false, 262144);
			//echo $upload_status . $_FILES['fund_file_data']['type']; 
			if($upload_status == 101)
				{
					$csv_file_name = $_FILES['fund_file_data']['name'];
					//echo $csv_file_name . " <b>uploaded</b>";
					include "readCSV.php";	
					//delete uploaded file
					unlink($csv_dir.$csv_file_name);
				}
			else mk_output_message("error", "������ <b>".$upload_status."</b> ��� ������� �� ����� ! ������� �� �� �������� !");
		}
		
	//delete rate
	if(isset($del_id))
		{
			$delete_sql = "DELETE FROM ".$selected_fund['fund_table']. " WHERE rate_id='".$del_id."'";
			mysqli_query($delete_sql);
			if(mysqli_affected_rows()>0)
				mk_output_message("normal", "Rate deleted !");
				
		}
?>


<script type="text/javascript">

// Load the Visualization API and the piechart package.
google.load('visualization', '1', {'packages':['corechart']});
  
// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawChart);
  
function drawChart() {
  var jsonData = $j.ajax({
	  url: "Templates/kdinvestment/chart/getData.php?from_date=<?=$from_date?>&to_date=<?=$to_date?>&fund_id=<?=$fund_id?>",
	  dataType:"json",
	  async: false
	  }).responseText;
	  
  // Create our data table out of JSON data loaded from server.
  var data = new google.visualization.DataTable(jsonData);

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
  chart.draw(data, 
  	{
		title: "<?=$selected_fund['fund_title']?>",
		lineWidth	: 1,
		hAxis		: {title: ""},
		vAxis		: {title: ""},
		width		: 770, 
		height		: 340
	});
}
</script>
<script>
	$j(document).ready(function() {
		var dates = $j("#from_date, #to_date").datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 3,
			dateFormat: 'yy-m-d',
			showOn: "button",
			buttonImage: "lib/jquery-ui/demos/images/calendar.gif",
			buttonImageOnly: true,
			onSelect: function( selectedDate ) {
				var option = this.id == "from_date" ? "minDate" : "maxDate",
					instance = $j( this ).data( "datepicker" ),
					date = $j.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );

			}

		});
		
	$j("input[name='OnDate']").attr("readonly","readonly");
	$j("input[name='OnDate']").datepicker({
		dateFormat: 'yy-m-d'
		});

	});

	</script>

<!--Div that will hold the chart-->
<fieldset style="width:770px"><br>
<div id="chart_div"></div>
<form method="get">
	<input type="hidden" name="n" value="<?=$n?>">
    <input type="hidden" name="SiteID" value="<?=$SiteID?>">
<br>
<table class="border_table" cellspacing="0">
    <thead><th width="150px"><input name="from_date" type="text" id="from_date" value="<?=$from_date?>" readonly="readonly"><th width="150px"><input name="to_date" type="text" id="to_date" value="<?=$to_date?>" readonly="readonly"><th><input type="submit" value="���������"></thead>
</table>
</form>
</firldset>
<?php
if($user->AccessLevel >= 2)
	{
		//print_r($data_template);
		include "chart_admin.php";
	}
	
	mysql_select_db("maksoft");
}
?>