<?php

$subpages = $o_page->get_pSubPages();

function print_prices($o_page, $subpages){
    $last_title = '';
    foreach($subpages as $price_type){
        if($prices = $o_page->get_pPrice($price_type["n"])){
			echo '<h2 class="table-title">'.$price_type['Name'].'</h2>';
            echo '<div class="text-desc-second">'.trim($price_type['textStr']).'</div>';
            echo format_prices($prices);
        } else {
			echo '<h1 class="general-title">'.$price_type['Name'].'</h1><hr>';
            echo '<div class="text-desc">'.trim($price_type['textStr']).'</div>';
            print_prices($o_page, $o_page->get_pSubPages($price_type['n']));
        }
        $last_title = '<h2>'.$price_type['Name'].'</h2><br>'; 
    }
}



function format_prices($prices){
    $tmp = array();
    foreach($prices as $price){
        $tmp[] = format_price($price);
    }
    return '<table id="list-prices">'.implode('', $tmp).'</table><br>';
}

function format_price($price){
    $price_f = number_format($price['price_value'], 0, '.', '');
    return <<<HEREDOC
    <tr>
        <td>{$price['description']}</td>
<!--        <td>{$price['qty']} ��. x </td>-->
        <td>&nbsp;{$price_f} {$price['currency_string']} </td>
    </tr>
HEREDOC;
}




print_prices($o_page, $subpages);
