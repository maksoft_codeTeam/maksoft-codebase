<?php
require_once __DIR__.'/../../../modules/vendor/autoload.php';
require_once __DIR__.'/../../../forms/persona/BuyVoucher.php';
$form = new BuyVoucher($_POST);
$recaptcha = new \ReCaptcha\ReCaptcha($secretKey);
$form_valid = false;
$err_msg = "";
if(isset($_POST['action']) and $_SERVER['REQUEST_METHOD'] === "POST"){
    if($_POST['action'] == $form->action->value){
        $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
        if ($resp->isSuccess()) {
            try{
                $form->is_valid();
                $form_valid = $form->save($o_page);
            } catch( Exception $e){
                $err_msg = "���� �� �������� � ���������! ����, �������� ������. ";
            }
        } else {
            $err_msg = "<strong>Note:</strong> Error code <tt>missing-input-response</tt> may mean the user just didn't complete the reCAPTCHA.</p>
        <p><a href="/">Try again</a></p>";
        }
    }
}
?>
<style>
	.sub-pages-2-columns {
		display: none;
	}
</style>
<div class="container">
 	<div class="row">
     <div class="col-sm-5 form-col">
        <?php 
		echo $form->start();
		if($form_valid){ ?>
        <div class="alert alert-success alert-message fadeInUp animated">
            <div class="complete-icon">
                <img src="Templates/persona/assets/img/formcheck.png" alt="�������">
            </div> ������� ���������� ������ ������! � ���-������ ���� �� �� ������� � ��� �� �� ������� ��������� �� ������ ������.
        </div>
        <?php } ?>
        <?php if(!empty($err_msg)){?>
            <div class="alert alert-warning alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $err_msg; ?>
            </div>

        <?php } ?>
    <?php
            /*
             *  Foreach ���� ������ ������ ��� �������...
             *  ����� �� �� ������ � �� ������� echo $field->holder_name //���� 
             *  �� ������� ����� ������ �� �����
             */
             foreach($form as $field){
                 echo $field->get_label(); // Label
                 echo $field;// Samoto input pole
                 
                /*
                 * ��� ���������� ���� ��� ���������� ������ �� ����� ���� � 
                 * ��� ��� �� �������� ���� ������
                 */
                $errors = $field->getErrors();
                if(!empty($errors)){
                    echo "<ul>";
                    foreach($errors as $error){
                        echo "<li>".$error."</li>";
                    }
                    echo "</ul>";
                }
            } ?>

    <div id="RecaptchaField1" class="margintop15"></div>
    <input type="submit" value="�������" class="btn btn-lg btn-primary margintop15" name="submit">
    <?php echo $form->end();?>
	</div>
	<div class="col-sm-5 form-col-2">
	<img src="<?=TEMPLATE_DIR?>assets/img/rsz_vaucher_na2.png" alt="��������� �������" />
	</div>
</div>
</div>

