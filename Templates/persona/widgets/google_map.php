<script src="<?=TEMPLATE_DIR?>assets/js/modernizr.js"></script>

<div class="googlemap">
		<div id="map" class="map desk-three-forth"></div>
		<div class="map-legend desk-one-forth">
			<div class="location location-1">
				<div class="location--inner">
					<img src="<?=TEMPLATE_DIR?>assets/img/persona.svg" width="60px" alt="Persona">
					<p>&nbsp;</p>
					<p>��. �����, �.�. ���� ����, ��. 124</p>
					<h4 class="worktime">������� �����:</h4>
					<p>9:00 - 21:00</p>
					<p>������ � ������:</p>
					<p>10:00 - 18:00</p>
				</div>
			</div>
		</div>
		<script>
			var map,
				desktopScreen = Modernizr.mq( "only screen and (min-width:1024px)" ),
				zoom = desktopScreen ? 15 : 14,
				scrollable = draggable = !Modernizr.hiddenscroll || desktopScreen,
				isIE11 = !!(navigator.userAgent.match(/Trident/) && navigator.userAgent.match(/rv[ :]11/));

			function initMap() {
				var myLatLng = {lat: 42.6680534, lng: 23.312816};
				map = new google.maps.Map(document.getElementById('map'), {
					zoom: zoom,
					center: myLatLng,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					scrollwheel: false,
					draggable: draggable,
					styles: [{"stylers": [{ "saturation": -100 }]}],
				});

				var locations = [
					{
						title: 'Persona',
						position: {lat: 42.6680534, lng: 23.312816},
						icon: {
							url: isIE11 ? "<?=TEMPLATE_DIR?>assets/img/persona.png" : "<?=TEMPLATE_DIR?>assets/img/persona.png",
							scaledSize: new google.maps.Size(64, 64)
						}

					}					
				];
				
				locations.forEach( function( element, index ){	
					var marker = new google.maps.Marker({
						position: element.position,
						map: map,
						title: element.title,
						icon: element.icon,
					});
				});	
			}

		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9WXKAl4Y-_Cj3EloiYYFzQdKEwe1-PNA&callback=initMap" async defer></script>
		<!-- replace the above with <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap" async defer></script> -->
</div>