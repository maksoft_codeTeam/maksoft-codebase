<?php
require_once __DIR__ . '/../../modules/vendor/autoload.php';
require_once __DIR__ . '/../../global/recaptcha/init.php';
date_default_timezone_set( 'Europe/Sofia' );
$search_tag = '';

$search = '';

if(isset($_GET['search_tag'])){

    $search_tag = iconv('utf8', 'cp1251', $_GET['search_tag']);
}

if(isset($_GET['search'])){
    $search = $_GET['search'];
}

define( "TEMPLATE_NAME", "persona" );
define( "TEMPLATE_DIR", "Templates/" . TEMPLATE_NAME . "/" );
define( "TEMPLATE_IMAGES", "http://www.maksoft.net/" . TEMPLATE_DIR . "images/" );
define( "ASSETS_DIR", "/" . TEMPLATE_DIR . "" );

include TEMPLATE_DIR . "header.php";
$pTemplate = $o_page->get_pTemplate( $n, 290 );


switch(True){
    case $o_page->_page[ 'SiteID' ] != $o_site->_site[ 'SitesID' ]:
        include TEMPLATE_DIR . "admin.php";
        break; 
    case ($o_page->_page[ 'n' ] == $o_site->_site[ 'StartPage' ] && (!isset($_GET['search_tag']) and !isset($_GET['search']))):
        include TEMPLATE_DIR . "home.php";
        break;
    default:
        include TEMPLATE_DIR . "main-h.php";
        if ( $pTemplate[ 'pt_url' ] )
            include $pTemplate[ 'pt_url' ];
        else
            include TEMPLATE_DIR . "main.php";
}

include TEMPLATE_DIR . "footer.php";

?>
