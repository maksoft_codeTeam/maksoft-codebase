<?php 
include TEMPLATE_DIR . "widgets/hour_form.php"; 
include TEMPLATE_DIR . "widgets/google_map.php";
?> 

<!-- Footer Area -->
<div class="footer-area">
	<!-- Footer Top -->
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<!-- Footer Widget About -->
				<div class="footer-widget footer-widget-about col-md-3 col-sm-5">
					<img src="<?=ASSETS_DIR?>assets/img/logo_persona_bw.svg" alt=""/>
					<p>2 ������ ������ � ���������!</p>
					<div class="footer-social">
						<a href="https://www.facebook.com/persona.bg/"><i class="mo-facebook"></i></a>
						<a href="#"><i class="mo-google-plus"></i></a>
						<a href="https://www.youtube.com/channel/UCpVX2T8JnYb-gCKmGfFJDQw"><i class="mo-youtube"></i></a>
						<a href="https://foursquare.com/user/123092603"><i class="fa fa-foursquare" aria-hidden="true"></i></a>
						<a href="https://www.instagram.com/persona.bg/"><i class="mo-instagram"></i></a>
					</div>
				</div>
				<!-- Footer Widget Address -->
				<div class="footer-widget footer-widget-collection col-md-4 col-sm-5">
					<h3>�������� �� Instagram</h3>

					<div id="instafeed"></div>

				</div>
				<div class="footer-widget footer-widget-address col-md-2 col-sm-5">
					<h3>���������</h3>
					<ul>
						<?php

						if ( !isset( $footer_links ) )$footer_links = 5;
						$footer_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $footer_links" );

						for ( $i = 0; $i < count( $footer_links ); $i++ ) {
							$subpages = $o_page->get_pSubpages( $footer_links[ $i ][ 'n' ] );
							?>
						<li>
							<a href="<?php echo $o_page->get_pLink($footer_links[$i]['n']) ?>">
								<?php echo $o_page->get_pName($footer_links[$i]['n']) ?>
							</a>
						</li>

						<?php  
}
?>
					</ul>

				</div>
				<!-- Footer Widget Shop -->
				<div class="footer-widget footer-widget-shop col-md-3 col-sm-5">
					<h3>�� ��������</h3>
					<ul>
						<li>��. �����</li>
						<li>�.�. ���� ����, ��. 124</li>
						<li>(�� ������������� ���������, ����� �� ��.�����)</li>
						<li class="phone-ft"><a href="tel:+35928221067">+359 2 822 10 67</a>
						</li>
						<li class="phone-ft"><a href="tel:+359898356190">+359 898 356 190</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer Bottom -->
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<!-- Footer Copyright -->
				<div class="copyright col-sm-6 text-left">
					<p>
						<?=$o_page->get_sCopyright() ." - ".date("Y")?>
				</div>
				<!-- Footer Payment -->
				<div class="payment col-sm-6 text-right">
					��� ������ � ��������� �� <a href="http://www.maksoft.net" style="color:rgba(255, 255, 255, 0.64);" title="��� ������ � ��������� �� �������">�������</a>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- jQuery 1.12.4
============================================ -->
<!--<script src="<?=ASSETS_DIR?>assets/js/vendor/jquery-1.12.3.min.js"></script>-->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

<!-- Bootstrap JS
============================================ -->
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/js/bootstrap.min.js"></script>
<!-- MeanMenu JS
============================================ -->
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/js/jquery.meanmenu.min.js"></script>

<script type="text/javascript" src="<?=ASSETS_DIR?>assets/lib/slick/slick.min.js" charset="utf-8"></script>

<script src="<?=ASSETS_DIR?>assets/js/fb-live-chat.min.js"></script>
<?php if($user->AccessLevel <1) { ?>
<script type="text/javascript">
	fbLiveChat.init({
		sdk_locale: 'bg_BG',
		facebook_page: 'persona.bg', // required
		position: 'right',
		header_text: '����� �������?',
		header_background_color: 'rgb(176, 47, 98)',
		show_close_btn: true,
		animation_speed: 400,
		auto_show_delay: 15000
	});
</script>
<?php } ?>

<script type="text/javascript">	
	$( document ).on( 'ready', function () {
		$( ".center" ).slick( {
			autoplay: true,
			autoplaySpeed: 2000,
			dots: true,
			infinite: true,
			centerMode: true,
			slidesToShow: 2,
			slidesToScroll: 1,
			responsive: [ {
					breakpoint: 1000,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}, {
					breakpoint: 767,
					settings: "unslick"
				}

			]
		} );
	} );
</script>


<script type="text/javascript" src="<?=ASSETS_DIR?>assets/lib/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/lib/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?=ASSETS_DIR?>assets/lib/rs-plugin/rs.home.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/js/jquery.scrollup.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/js/instafeed.min.js"></script>
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/js/datepicker-bg.js"></script>

<script type="text/javascript">
	jQuery( document ).ready( function ( $ ) {
		$( "#datepicker" ).datepicker();
		$.datepicker.setDefaults( $.datepicker.regional[ 'bg' ] );
	} );
	var feed = new Instafeed( {
		get: 'user',
		userId: '1649717875',
		accessToken: '1649717875.1677ed0.b4a7a1f0a4f045039f183c47e3438694',
		limit: '8',
		template: '<a href="{{link}}" target="_blank"><img src="{{image}}" width="90px" height="auto" style="padding:5px;-webkit-border-radius:17px;-moz-border-radius:17px;border-radius:17px;" /></a>'

	} );

	feed.run();
</script>
<!-- Tree View JS
============================================ -->
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/js/jquery.treeview.js"></script>
<!-- Nice Scroll JS
============================================ -->
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/js/jquery.nicescroll.min.js"></script>
<!-- WOW JS
============================================ -->
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/js/wow.min.js"></script>
<!-- Google Map APi
============================================ -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuU_0_uLMnFM-2oWod_fzC0atPZj7dHlU"></script> -->
<script>
	new WOW().init();

	function onloadCallback() {
		/* Place your recaptcha rendering code here */
		if ( $( "#RecaptchaField1" ).length ) {
			grecaptcha.render( 'RecaptchaField1', {
				'sitekey': '<?php echo $secret;?>'
			} );
		}
		grecaptcha.render( 'RecaptchaField2', {
			'sitekey': '<?php echo $secret;?>'
		} );
	}
</script>
	<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>&render=explicit&onload=onloadCallback">
</script>
<script type="text/javascript" src="<?=ASSETS_DIR?>assets/js/main.min.js"></script>

<?php include( "Templates/footer_inc.php" ); ?>
</body>
</html>