<div class="home-slider-wrapper">
	<div id="revslider1" class="rev_slider">
		<ul>
			<li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off"  data-title="Slide1">
				 <!-- MAIN BACKGROUND IMAGE -->
				 <img src="<?=TEMPLATE_DIR?>assets/img/home-1-slider/bg-1.jpg"  alt="">
				 <!-- LAYER NR. 1 -->
				 <div class="tp-caption sfb layer1"				
					data-x="350" 
					data-y="50"
					data-speed="800"
					data-start="1000"
					data-easing="easeInOutExpo"
					data-elementdelay="0.1"
					data-endelementdelay="0.1"
					data-endspeed="300"><img src="<?=TEMPLATE_DIR?>assets/img/home-1-slider/layer1.png"  alt=""></div>
				 <div class="tp-caption sfb layer4" 							
					data-x="270" 
					data-y="310" 
					data-speed="800"
					data-start="2500"
					data-easing="easeInOutExpo"
					data-elementdelay="0.1"
					data-endelementdelay="0.1"
					data-endspeed="300"><img src="<?=TEMPLATE_DIR?>assets/img/home-1-slider/layer4.png"  alt=""></div>
				 <!-- LAYER NR. 5 -->
				 <div class="tp-caption sfb layer5" 
					data-x="300" 
					data-y="450" 
					data-speed="800"
					data-start="3000"
					data-easing="easeInOutExpo"
					data-elementdelay="0.1"
					data-endelementdelay="0.1"
					data-endspeed="300"
					style="
					  color: #fff;
					  font-family: 'Didact Gothic', sans-serif;
					  font-size: 35px;
					  font-style: italic;
					  font-weight: 700;
					  letter-spacing: -1.5px;
					  line-height: 62px;
					">2 ������ ������ � ���������!</div>
				 <!-- LAYER NR. 6 -->
				 <a href="<?=$o_page->get_pLink(19342802)?>" class="shop-now tp-caption sfb layer6"
					data-x="355" 
					data-y="606" 
					data-speed="800"
					data-start="3500"
					data-easing="easeInOutExpo"
					data-elementdelay="0.1"
					data-endelementdelay="0.1"
					data-endspeed="300"
					style="
					  border: 2px solid #fff;
					  color: #fff;
					  display: block;
					  font-size: 18px;
					  line-height: 46px;
					  padding: 0 40px;
					  text-align: center;
					  text-transform: uppercase;
					">������ �������</a>
			</li>
		</ul>				
	</div>
</div>

<div class="two-column-promo">
	<div class="container">
		<div class="row">
			<div class="two-column-promo-container col-md-12 fadeInLeft animated">
				<div class="sin-promo sin-promo-hover col-md-6 fix">
					
					<a href="<?=$o_page->get_pLink(19341102)?>" class="image"><img src="<?=TEMPLATE_DIR?>assets/img/2-column-promo/olesia.jpg" alt="" /></a>
					<div class="promo-title promo-title-left-bottom">
						<h1>���������<br> ������������</h1>
					</div>
				</div>
				<div class="sin-promo sin-promo-right col-md-6 fix">
					<img src="<?=TEMPLATE_DIR?>assets/img/2-column-promo/column2.png" alt="�-� ����� ��������" />
					<div class="promo-title column-title promo-title-right">
						<h3><strong>�-� ����� �������� �� �� ������� �� ������ ����!</strong></h3>
						<p>�-� ����� �������� ��� ��� 8-������� ���� ��� ����� ��, ����� � ��������� �� ����� �� �������� �� � ������. ���� �� ���������, � ����� � �������� ��� � ��������� ���� ����, ��������� ���� �������, ������ �������� � �������� ���� �� ���������� �� ����� �� ����������� � �����������</p>
					</div>
					<div class="links promo-title-right">
						<a href="<?=$o_page->get_pLink(19341102)?>">������� ��� <i class="mo-arrow-right"></i></a>
					</div>
				</div>
			</div>
		</div>
			
		<div class="row">
			<div class="two-column-promo-container col-md-12 column-2">
				<div class="sin-promo sin-promo-hover col-md-6 fix">
					
					<a href="<?=$o_page->get_pLink(19340971)?>" class="image"><img src="<?=TEMPLATE_DIR?>assets/img/2-column-promo/face.jpg" alt="���� - �������" /></a>
					<div class="promo-title promo-title-left-bottom">
						<h1>����</h1>
					</div>
				</div>
				<div class="sin-promo sin-promo-right col-md-6 fix">
					<img src="<?=TEMPLATE_DIR?>assets/img/2-column-promo/column-face.png" alt="����" />
					<div class="promo-title column-title">
					<div class="col-md-12 padding-left-30">
					
      				<?php 
                    $face = $o_page->get_pGroupContent(3426);
						for($i=0; $i<count($face); $i++)
							{
					?>
					<div class="col-md-6"><a href="<?=$o_page->get_pLink($face[$i]['n'])?>" class="column-links"><i class="mo-arrow-right"></i> <?=$o_page->get_pName($face[$i]['n'])?></a></div>
					<?php } ?>
					
					

					</div>

						</div>
					</div>
				</div>
		</div>
		  
		  <div class="row">
			<div class="two-column-promo-container col-md-12 column-3">
				<div class="sin-promo sin-promo-hover col-md-6 fix">
					
					<a href="<?=$o_page->get_pLink(19340972)?>" class="image"><img src="<?=TEMPLATE_DIR?>assets/img/2-column-promo/body.jpg" alt="���� - �������" /></a>
					<div class="promo-title promo-title-left-bottom">
						<h1>����</h1>
					</div>
				</div>
				<div class="sin-promo sin-promo-right col-md-6 fix">
					<img src="<?=TEMPLATE_DIR?>assets/img/2-column-promo/column-body.png" alt="����" />
					<div class="promo-title column-title">
					<div class="col-md-12 padding-left-30">
      				<?php 
                    $body = $o_page->get_pGroupContent(3427);
						for($i=0; $i<count($body); $i++)
							{
					?>
					<div class="col-md-6"><a href="<?=$o_page->get_pLink($body[$i]['n'])?>" class="column-links"><i class="mo-arrow-right"></i> <?=$o_page->get_pName($body[$i]['n'])?></a></div>
					<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>

<div class="promo-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="subscribe-container">

					<div class="subscribe-text fix">
						<h2>�������� ��������</h2>
						<p>��� ����� ������ �������</p>
					</div>
			
				</div>
				
	<section class="center slider">
      <?php 
      $promo = $o_page->get_pSubpages(19341115);
		for($i=0; $i<count($promo); $i++)
		{
		?>
    <div class="promo">
     <a href="<?=$o_page->get_pLink($promo[$i]['n'])?>">
      <img src="/img_preview.php?image_file=<?=$promo[$i]['image_src']?>&img_width=400" width="auto" height="248" />
		<div class="promos-title"><h3><?=$o_page->get_pName($promo[$i]['n'])?></h3></div>
	 </a>
    </div>
    	<?																
		}
		?>
  </section>
			</div>
		</div>
	</div>
</div>

<div class="whyus">
	<div class="container">
	    <div class="row">
	    <div class="col-sm-12 text-center">

					<div class="whyus-title fix">
						<h2>���� �� �������� ���?</h2>
						<span class="downline"></span>
					</div>
         	
          	<div class="cols-why">
                <div class="row">
                	<div class="col-md-6">
						<h3>������ ��� ����������</h3>
						<div class="iconwhy">
							<img src="<?=TEMPLATE_DIR?>assets/img/why-icons/inno.svg" class="img-whyus" alt="������ ��� ����������" />
						</div>
						<p>� ������ �� �������� Persona  �� �������� ���-������ ���������� �� ����� ��� ����������, ������������ �������� �� ������, ����� � ���������� �� ������ ���� �������, �����, ����������, ������� ��������� � ������ �����������.</p>
					</div>
                	<div class="col-md-6">
						<h3>������ ��� �������������������</h3>
						<div class="iconwhy">
							<img src="<?=TEMPLATE_DIR?>assets/img/why-icons/highly.svg" class="img-whyus" alt="������ ��� �������������������" />
						</div>
						<p>� ������ �� �������� Persona ������� ���� �� ���-������� ����������� � ��������  - ��������� � ���������������. ���� �� ����� �� � � �-� ����� ��������, ����� ����������, ����� ��������� ��� 70 ������� �� ������������ � ������������.</p>
					</div>
                	<div class="col-md-6">
						<h3>������ ��� ��������</h3>
						<div class="iconwhy">
							<img src="<?=TEMPLATE_DIR?>assets/img/why-icons/trust.svg" class="img-whyus" alt="������ ��� ��������" />
						</div>
						<p>��� ��� ��������, �� ����� ���������� ������ �� �� ��������. ������ ������ �� ����� � ������������ �������� �� �������� � � �������� ����� ���������. ������� �� ������ ��� �������������������. ������ ����������� ���������� ���� ������ �������  ������� �� ���������� �� ��������������.</p>
					</div>
                	<div class="col-md-6">
						<h3>������ ����� ��������� ��</h3>
						<div class="iconwhy">
							<img src="<?=TEMPLATE_DIR?>assets/img/why-icons/care.svg" class="img-whyus" alt="������ ����� ��������� ��" />
						</div>
						<p>��� ������� �� ���. ��� ����� ������ ����� � �� ������� �� �� ����������. ����� ������ ������� � �� ������� �� �� ������������. �� ��� ��� ��� �������. �� �������� �� ������� ������������ ��������� �������� � ����� �������� � ��������.</p>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>

<!-- Subscribe Area
============================================ -->
<!--<div class="subscribe-area">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="subscribe-container">

					<div class="subscribe-text fix">
						<h2>���� ���������� �� ������ ���� ������!</h2>
						<p>���������� �� �� ����� �������, �� �� ������ ���������� �� ��������</p>
					</div>

					<div class="subscribe-form fix">
						<form action="#" id="subscribe-form">
							<input type="text" placeholder="����� email �����" />
							<input type="submit" value="�������� ��" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>-->

<!-- Blog
============================================ -->
<!--<div class="blog-area">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-title text-center"><h1>��������� �� ���</h1></div>
			</div>
		</div>
		<div class="row">
			<div class="blog-slider home1-blog-slider">
			
				<div class="blog-item">
					<div class="blog-image col-lg-6 col-sm-5">
						<a href="#"><img src="<?=TEMPLATE_DIR?>assets/img/client.png" alt="blog" /></a>
					</div>
					<div class="blog-content col-lg-6 col-sm-7">
						<span class="blog-date">26 ������, 2016</span>
						<p>����� ��� ���������� �� ������������ �� ������������� �� ������� �� ��������. ��� ������ ���������� � ������� ��������� ���������� �� ��������� ���� ������. ����������� ����������� ���� ����� ������� � �� ���������� ���������� ���������� ������� ����������, ���������� � ������ �� ������. </p>
						<span>��</span><a href="#" class="blog-author"> <h5>������� ���������</h5></a>
					</div>
				</div>
				
				<div class="blog-item">
					<div class="blog-image col-lg-6 col-sm-5">
						<a href="#"><img src="<?=TEMPLATE_DIR?>assets/img/client.png" alt="blog" /></a>
					</div>
					<div class="blog-content col-lg-6 col-sm-7">
						<span class="blog-date">26 ������, 2016</span>
						<p>����� ��� ���������� �� ������������ �� ������������� �� ������� �� ��������. ��� ������ ���������� � ������� ��������� ���������� �� ��������� ���� ������. ����������� ����������� ���� ����� ������� � �� ���������� ���������� ���������� ������� ����������, ���������� � ������ �� ������. </p>
						<span>��</span><a href="#" class="blog-author"> <h5>������� ���������</h5></a>
					</div>
				</div>
				
				<div class="blog-item">
					<div class="blog-image col-lg-6 col-sm-5">
						<a href="#"><img src="<?=TEMPLATE_DIR?>assets/img/client.png" alt="blog" /></a>
					</div>
					<div class="blog-content col-lg-6 col-sm-7">
						<span class="blog-date">26 ������, 2016</span>
						<p>����� ��� ���������� �� ������������ �� ������������� �� ������� �� ��������. ��� ������ ���������� � ������� ��������� ���������� �� ��������� ���� ������. ����������� ����������� ���� ����� ������� � �� ���������� ���������� ���������� ������� ����������, ���������� � ������ �� ������. </p>
						<span>��</span><a href="#" class="blog-author"> <h5>������� ���������</h5></a>
					</div>
				</div>
			
			</div>
		</div>
	</div>
</div>-->