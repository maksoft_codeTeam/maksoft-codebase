<!doctype html>
<html class="no-js" lang="<?php echo($Site->language_key); ?>">

<head>
	<meta http-equiv="Content-Type" content="text/html;" charset="windows-1251">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?=$Title?>
	</title>
	<?php
	require_once "lib/lib_page.php";
	require_once "lib/Database.class.php";
	//include meta tags
	include( "Templates/meta_tags.php" );
	$o_site->print_sConfigurations();

	$db = new Database();
	$o_page->setDatabase( $db );
	$o_user = new user( $user->username, $user->pass );
	?>
	<link rel="shortcut icon" href="<?=ASSETS_DIR?>assets/img/favicon/favicon.ico" type="image/x-icon"/>
	<link rel="apple-touch-icon" href="<?=ASSETS_DIR?>assets/img/favicon/apple-touch-icon.png"/>
	<link rel="apple-touch-icon" sizes="57x57" href="<?=ASSETS_DIR?>assets/img/favicon/apple-touch-icon-57x57.png"/>
	<link rel="apple-touch-icon" sizes="72x72" href="<?=ASSETS_DIR?>assets/img/favicon/apple-touch-icon-72x72.png"/>
	<link rel="apple-touch-icon" sizes="76x76" href="<?=ASSETS_DIR?>assets/img/favicon/apple-touch-icon-76x76.png"/>
	<link rel="apple-touch-icon" sizes="114x114" href="<?=ASSETS_DIR?>assets/img/favicon/apple-touch-icon-114x114.png"/>
	<link rel="apple-touch-icon" sizes="120x120" href="<?=ASSETS_DIR?>assets/img/favicon/apple-touch-icon-120x120.png"/>
	<link rel="apple-touch-icon" sizes="144x144" href="<?=ASSETS_DIR?>assets/img/favicon/apple-touch-icon-144x144.png"/>
	<link rel="apple-touch-icon" sizes="152x152" href="<?=ASSETS_DIR?>assets/img/favicon/apple-touch-icon-152x152.png"/>
	<link rel="apple-touch-icon" sizes="180x180" href="<?=ASSETS_DIR?>assets/img/favicon/apple-touch-icon-180x180.png"/>

	<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/custom-style.css"/>
	<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/css/animate.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/lib/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/lib/slick/slick-theme.css" charset="utf-8"/>
	<link rel="stylesheet" type="text/css" href="<?=ASSETS_DIR?>assets/lib/rs-plugin/css/settings.min.css" media="screen"/>

	<script src="<?=ASSETS_DIR?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script src="https://use.typekit.net/cit3sya.js"></script>
	<script>
		try {
			Typekit.load( {
				async: true
			} );
		} catch ( e ) {}
	</script>

</head>

<body>

	<!-- Header Top
============================================ -->
	<div class="header-top">
		<div class="container">
			<div class="row">

				<div class="header-top-left col-sm-6">
					<ul class="header-login-reg float-left">
						<li><a href="tel:<?php echo $o_page->_site['SPhone'];?>" class="top-header-link"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> <?php echo $o_page->_site['SPhone'];?></a>
						</li>
					</ul>
				</div>

				<div class="header-top-right col-sm-6">
					<ul class="language-currency float-right">
						<li><a href="https://www.facebook.com/persona.bg/"><i class="mo-facebook"></i></a>
						</li>
						<li><a href="#"><i class="mo-google-plus"></i></a>
						</li>
						<li><a href="https://www.youtube.com/channel/UCpVX2T8JnYb-gCKmGfFJDQw"><i class="mo-youtube"></i></a>
						</li>
						<li><a href="https://foursquare.com/user/123092603"><i class="fa fa-foursquare" aria-hidden="true"></i></a>
						</li>
						<li><a href="https://www.instagram.com/persona.bg/"><i class="mo-instagram"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Header Bottom
============================================ -->
	<div class="header-bottom">
		<div class="container">
			<div class="row">
				<!-- Header Logo -->
				<div class="logo col-md-2 col-sm-4 col-xs-5"><a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>"><img src="<?=ASSETS_DIR?>assets/img/persona_logo.svg" alt="Persona.bg" /></a>
				</div>
				<!-- Main Menu -->
				<div class="main-menu col-md-9 hidden-sm hidden-xs">
					<nav>
						<ul>
							<li><a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>"><span class="glyphicon glyphicon-home"></span></a>
							</li>

							<?php

							if ( !isset( $dd_links ) )$dd_links = 2;
							$dd_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $dd_links" );

							for ( $i = 0; $i < count( $dd_links ); $i++ ) {
								$subpages = $o_page->get_pSubpages( $dd_links[ $i ][ 'n' ] );
								?>
							<li <?php echo ((count($subpages)> 0) ? "" : "") ?>>
								<a href="<?php echo $o_page->get_pLink($dd_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <?php echo $dd_links[$i]['Name'] ?></a>
								<?php echo ((count($subpages) > 0) ? "<ul class=\"sub-menu\">" : "</li>") ?>
								<?php
								if ( count( $subpages ) > 0 ) {


									for ( $j = 0; $j < count( $subpages ); $j++ )
										echo "<li><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\">" . $subpages[ $j ][ 'Name' ] . "</a></li>";
									?>
						</ul>
						</li>
						<?php 
	} 
}
?>

						</ul>
					</nav>
				</div>
				<!-- Header Search -->
				<div class="search-cart col-md-1 col-md-offset-0 col-sm-5 col-sm-offset-3 col-xs-7">

					<!-- �������� -->
					<div class="header-search float-right">
						<button class="search-btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
						<form method="get" class="search-form" action="page.php">
							<input type="text" name="search" id="search" placeholder="�������..."/>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Mobile Menu
============================================ -->
	<div class="mobile-menu hidden-lg hidden-md fix">
		<nav>
			<ul>
				<li><a href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>">������</a>
					<?php

					if ( !isset( $mobile_links ) )$mobile_links = 2;
					$mobile_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $mobile_links" );

					for ( $i = 0; $i < count( $mobile_links ); $i++ ) {
						$subpages = $o_page->get_pSubpages( $mobile_links[ $i ][ 'n' ] );
						?>
					<li <?php echo ((count($subpages)> 0) ? "" : "") ?>>
						<a href="<?php echo $o_page->get_pLink($mobile_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
            <?php echo $mobile_links[$i]['Name'] ?></a>
						<?php echo ((count($subpages) > 0) ? "<ul>" : "</li>") ?>
						<?php
						if ( count( $subpages ) > 0 ) {


							for ( $j = 0; $j < count( $subpages ); $j++ )
								echo "<li><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\">" . $subpages[ $j ][ 'Name' ] . "</a></li>";
							?>
			</ul>
			</li>
			<?php 
	} 
}
?>
			</ul>
		</nav>
	</div>