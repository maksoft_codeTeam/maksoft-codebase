<div class="blog-page">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="sin-blog-post">
					<div class="blog-details">
						<?php
						if ( $sent == 1 ) {
							echo "<div class=\"alert alert-success alert-message fadeInUp animated\"><div class=\"complete-icon\"><img src=\"" . TEMPLATE_DIR . "assets/img/formcheck.png\" alt=\"�������\" /></div> ������� ���������� ������ ������! � ���-������ ���� �� �� ������� � ��� �� �� ������� ����� ���.</div>";
						}
						?>
						<?php
						//enable tag search / tag addresses
						if ( strlen( $search_tag ) > 2 ) {
							/*$o_page->print_search( $o_site->do_search( iconv( "UTF-8", "CP1251", $search_tag ), 10 ) );*/
							$keyword = $o_site->do_search( iconv( "UTF-8", "CP1251", $search_tag ), 10 );
							if ( count( $keyword ) == 0 ) {
								echo '<div class="results_wrong"><h4>���� �������� ��������� �� ���� <strong>' . $search_tag . '</strong></h4></div><br>';
								echo '<div class="quest_include"><div class="form_quest">��������� ���������, �� ���� ����� �������:</div>';
								echo '<div class="contact_form">';
								require_once 'web/forms/main_form.php';
								echo '</div></div>';
							} else {
								echo '<div class="results"><h4>��������� �� ��������� �� ��� <strong>' . $search_tag . '</strong>:</h4></div><br>
		<div class="blog-page pages sub-pages-2-columns">
	<div class="container">
		<div class="row">
		';
								$i = 0;
								foreach ( $keyword as $page ) {
									$_pPage = $o_page->get_page( $page[ 'ParentPage' ] );
									echo '
			<div class="col-sm-5 margin-top-30">
				<div class="sin-blog">
			       
			       <div class="col-md-12 bg-persona">
			       <h2 class="title-subpage"><a href="' . $o_page->get_pLink( $page[ 'n' ] ) . '">' . $page[ "Name" ] . '</a></h2>
					</div>
				   <div class="col-md-6 fix subpage-image">
					<div class="blog-image">
						<img src="' . $page[ 'image_src' ] . '" height="232px" alt="' . $page[ "Name" ] . '" />
						<a href="' . $o_page->get_pLink( $page[ 'n' ] ) . '">������</a>
					</div>
					</div>
					<div class="col-md-6 fix bg-white">
					<div class="subpage-details">

						<p>' . cut_text( strip_tags( $page[ 'textStr' ], 150 ) ) . '</p>
						
						<div class="links promo-title-right bottom fix subpage-more">
							<a href="' . $o_page->get_pLink( $page[ 'n' ] ) . '">��� ������ <i class="mo-arrow-right"></i></a>
						</div>
						
					</div>
					</div>
				</div>
			</div>
				';
									$i++;
									if ( $i == 4 ) {
										echo '<div style="clear: both" class="sPage-separator"></div>';
										$i = 0;
									}
								}
								echo '</div></div></div>';
							
							}
						} elseif ( strlen( $search ) > 2 ) {
							#$o_page->print_search($o_site->do_search($search, 10)); 
							#if($user->AccessLevel > 1){
							$needle = $o_site->do_search( $search, 100 );
							if ( count( $needle ) == 0 ) {
								echo '<div class="results_wrong"><h4>���� �������� ��������� �� ���� <strong>' . $search . '</strong></h4></div><br>';
								echo '<div class="quest_include"><div class="form_quest">��������� ���������, �� ���� ����� �������:</div>';
								echo '<div class="contact_form">';
								require_once 'web/forms/main_form.php';
								echo '</div></div>';
							} else {
								echo '<div class="results"><h4>��������� �� ��������� �� ���� <strong>' . $search . '</strong>:</h4></div><br>
		<div class="blog-page pages sub-pages-2-columns">
	<div class="container">
		<div class="row">
		';
								$i = 0;
								foreach ( $needle as $page ) {
									$_pPage = $o_page->get_page( $page[ 'ParentPage' ] );
									echo '
										<div class="col-sm-5 margin-top-30">
											<div class="sin-blog">

											   <div class="col-md-12 bg-persona">
											   <h2 class="title-subpage"><a href="' . $o_page->get_pLink( $page[ 'n' ] ) . '">' . $page[ "Name" ] . '</a></h2>
												</div>
											   <div class="col-md-6 fix subpage-image">
												<div class="blog-image">
													<img src="' . $page[ 'image_src' ] . '" height="232px" alt="' . $page[ "Name" ] . '" />
													<a href="' . $o_page->get_pLink( $page[ 'n' ] ) . '">������</a>
												</div>
												</div>
												<div class="col-md-6 fix bg-white">
												<div class="subpage-details">

													<p>' . cut_text( strip_tags( $page[ 'textStr' ], 150 ) ) . '</p>

													<div class="links promo-title-right bottom fix subpage-more">
														<a href="' . $o_page->get_pLink( $page[ 'n' ] ) . '">��� ������ <i class="mo-arrow-right"></i></a>
													</div>

												</div>
												</div>
											</div>
										</div>
											';
									$i++;
									if ( $i == 4 ) {
										echo '<div style="clear: both" class="sPage-separator"></div>';
										$i = 0;
									}
								}
								echo '</div></div></div>';
							}


						} else {
							//print page content
							echo "<div class='fadeInDownBig animated'>";
							$o_page->print_pContent();
							echo "</div>";
							echo "<div class='clearfix'></div>";

							//				echo "<br clear=\"all\">";
							//				if(!defined(CMS_MAIN_WIDTH) || CMS_MAIN_WIDTH > $tmpl_sonfig['default_main_width'])
							//					$cms_args = array("MAIN_WIDTH"=>640);
							//				//print page subcontent
							//				$o_page->print_pSubContent(NULL, 1, true, $cms_args);

							//print
							if ( $o_page->_page[ 'show_link' ] == 9 and $o_page->_page[ 'make_links' ] == 4 ) {
								$n_n = $o_page->_page[ 'n' ];
								$p_images = $o_page->get_pSubpages( $n_n, "p.sort_n ASC", "p.imageNo>0" );
								foreach ( $p_images as $p_key => $p_val ) {

									$g_img .= "<div class=\"sPage\" style=\"float: left; width: 100%\"><div class=\"sPage-content border_image gallery_persona\" style=\"margin: 2px; \"><img src='" . $p_images[ $p_key ][ 'image_src' ] . "' class='img-responsive' alt='" . $p_images[ $p_key ][ 'imageName' ] . "'></div></div>
									";
								}
								echo $g_img;

							} else {
								include TEMPLATE_DIR . "subpages.php";
							}

							eval( $o_page->get_pPHPcode() );

							if ( $user->AccessLevel >= $row->SecLevel and file_exists($row->PageURL) ) {
								include_once( "$row->PageURL" );
                            }
						}

						?>

						<div class="breadcrumb-mobile" style="display:none;">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center text-sm-left">
										<ol class="breadcrumb">
											<li>
												<?="".$nav_bar.""?>
											</li>
										</ol>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
