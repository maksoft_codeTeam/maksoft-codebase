<?php
use Maksoft\Admin\Auth\Login;
use Maksoft\Admin\Auth\FailedAttempts;
$login = new Login($gate);
$login->attach( new FailedAttempts($gate));

$block = $gate->auth()->isBlocked($client_ip, get_class($login));
if($block){
$time = date("H:i d-m-Y", $block->block_until);
echo <<<HERE
<div class="alert alert-danger">
<strong>�������� ������!</strong> ���������� 5 �������� ����� �� ������� � ������ �������. ��� ��� �������� �� {$time}.

</div>
HERE;
    exit();
}
?>

<?php
$redirect = function($site){
    return "<script>window.open(\"http://{$site["primary_url"]}/page.php?n={$site["StartPage"]}&SiteID={$site["SitesID"]}\",'_self');</script>";
};
if($_SERVER["REQUEST_METHOD"] === "POST"){
    try{
        $form = new \Maksoft\Admin\Auth\LoginForm($_POST);
        $form->is_valid();
        $login->init($form->_cleaned_data["username"], $form->_cleaned_data["password"], $client_ip);
        //TODO add login timeout function
        echo $redirect($o_page->_site);
    } catch (Exception $e){ ?>
             <div class="alert alert-danger" role="alert">
                   <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                   <span class="sr-only">Error:</span>
                   <?php echo $e->getMessage(); ?>
             </div>
    <?}
} elseif (isset($_SESSION["user"]) && is_object($_SESSION["user"])){
    echo $redirect($o_page->_site);
}else {
    $form = new \Maksoft\Admin\Auth\LoginForm();
}
echo $form;

?>
</body>
</html>
