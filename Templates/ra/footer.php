<!--

<div id="message" style="z-index: 100;">

        <div id="inner-message" class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            �������� �������,����� �� �� ��������, �� ������ ���������� ����������, ���������� �� ���� ���������� ���� �� ���� �������� �� 16:00 �� 19:00�. .
            ���������� �� �� ���������� - ��� ������� �� ���!
        </div>

</div>
-->

<style>
	#message {
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
}
#inner-message {
    margin: 0 auto;
	background-color: #AF2729;
	color: #ffffff;
	border-radius: 0px;
}
	</style>
	
<?php 
date_default_timezone_set('Europe/Sofia');
$curr_time= date("Gi", time());
$open = 900;
$end = 1800;
$account = $router->getAccount();
$day = date("D", time());

if ($account) {?>
<link rel="stylesheet" href="<?=ASSETS_DIR?>css/account.css">

<div class="cd-cart-container">
	<a href="#0" class="cd-cart-trigger">
		Cart
		<ul class="count <?php if (($curr_time > $open  and $curr_time < $end ) and !in_array($day, array("Sat", "Sun"))) { echo "online"; } ?>"> <!-- cart items count -->

		</ul> <!-- .count -->
	</a>

	<div class="cd-cart">
		<div class="wrapper">
			<header>
				<h2>������ ������ ��������</h2>
			</header>
			
			<div class="body">
            <h3><i class="fa fa-address-card" aria-hidden="true"></i> <?=$account->FullName;?></h3>
			</div>

			<div class="footer-buttons">
                <a class="checkout" href="mailto:<?=$account->EMail;?>"><em><i class="fa fa-envelope" aria-hidden="true"></i> <?=$account->EMail;?></em></a>
				<a class="checkout phone-<?php if ($curr_time > $open  and $curr_time < $end) { echo "online"; } else { echo "offline"; }?>" href="tel:<?=$account->Phone;?>"><em><i class="fa fa-phone" aria-hidden="true"></i> <?=$account->Phone;?></em></a>
			</div>
		</div>
	</div> <!-- .cd-cart -->
</div> <!-- cd-cart-container -->
<?php } ?>
        <?php
		if($_SESSION["user"]->WriteLevel > 2) { ?>
            <ol class="breadcrumb">
              <li><?="".$nav_bar.""?></li>
            </ol>
        <?php } ?>

    <script src="<?=ASSETS_DIR?>js/bootstrap.min.js"></script>
    <script src="<?=ASSETS_DIR?>js/bootstrap-dropdown.js"></script>
	<script src="<?=ASSETS_DIR?>js/custom.js"></script>
	<script src="<?=ASSETS_DIR?>js/notification.js"></script>
	<script src="web/assets/js/notify.min.js"></script>
	<script src="<?=ASSETS_DIR?>js/jquery-confirm.min.js"></script>
    <script src="<?=ASSETS_DIR?>js/account-help.js"></script>
<script>
var $ = jQuery;
    $('.dropdown-toggle').dropdownHover();
jQuery(document).ready(function($) {

    $('[data-toggle="tooltip"]').tooltip(); 
    <?php
    if($o_page->_user["ID"] == 1424){ ?>

    OneSignal.push(function(){
        OneSignal.isPushNotificationsEnabled(function(isEnabled) {
            if (isEnabled)
                console.log("Push notifications are enabled!");
            else
                OneSignal.registerForPushNotifications({
                    modalPrompt: true
                });
            });
        OneSignal.sendTags({
            user_id: <?=$o_page->_user["ID"];?>,
            firm_id: <?=$client;?>
        }, function(tagsSent){
            console.log(tagsSent);
        });
    });
    <?php } ?>
});
</script>
  </body
</html>
