<?php
date_default_timezone_set('Europe/Sofia');
require_once "modules/vendor/autoload.php";
require_once "modules/settings.php";
require_once __DIR__."/container.php";
#require_once __DIR__."/helpers.php";
#session_start();

define("TEMPLATE_NAME", "ra");
define("TEMPLATE_DIR", "Templates/".TEMPLATE_NAME."/");
define("TEMPLATE_IMAGES", "http://www.maksoft.net/".TEMPLATE_DIR."images/");
define("ASSETS_DIR", "/Templates/".TEMPLATE_NAME."/");
define("ONESIGNAL_APP_KEY", "e992cf74-4dc5-4b1b-9347-f086e1c3e7f5");

$container = new \Pimple\Container();
$container->register(new RaMaksoftLoader());

$gate = $container["gate"];
$client_ip = \Maksoft\Admin\FormCMS\Save::client_ip();
$_c = new \Maksoft\Cart\Cart("BG");
try{
    if(!isset($_SESSION['cart'])){ $_SESSION['cart'] = array();}
    $_c->load($_SESSION['cart']);
} catch(Exception $e){
}

$twig = $container["twig"];

if(!isset($discounts) and !is_array($discounts)){
    $discounts = array();
}

$router = new \Maksoft\Admin\Ra\Router($gate, $o_page, $twig, $_c);
$router->setDiscounts($discounts);
$router->setGroupDiscounts($group_discounts);
$router->setInfoGroup(5, 3296);
$router->setInfoGroup(8, 3394);

$client = $o_page->_user["FirmID"];

include TEMPLATE_DIR . "header.php";

// Page Template
$pTemplate = $o_page->get_pTemplate();
if($o_page->_page['SiteID'] != $o_site->_site['SitesID'] and in_array($o_page->_user["ID"], array(1424,1,1050,1419,67)) )
    include TEMPLATE_DIR."admin.php";	
elseif($o_page->_page['SiteID'] != $o_site->_site['SitesID'])
    include TEMPLATE_DIR."dashboard.php";
elseif($pTemplate['pt_url'])
    include $pTemplate['pt_url'];
elseif($o_page->_page['n'] == $o_site->_site['StartPage'] && strlen($search_tag)<3 && strlen($search)<3)
    if(is_logged()){
        include TEMPLATE_DIR."dashboard.php";
    } else {
        include TEMPLATE_DIR . "home.php";
    }
else
    include TEMPLATE_DIR . "main.php";
    
include TEMPLATE_DIR . "footer.php";
?>
