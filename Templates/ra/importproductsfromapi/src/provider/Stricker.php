<?php
namespace Maksoft\AFI\Provider;

use Maksoft\AFI\Settings\DefaultSettings as Settings;

use Maksoft\AFI\Libraries\StrickerPioneerSoapClient as WebService;


class Stricker implements Settings {

    protected $lang    = array(
        'PT' => 'por',
        'EN' => 'eng',
        'ES' => 'spa',
        'NL' => 'nld',
        'PL' => 'pol'
    );

    protected $token   = Settings::STRICKER_TOKEN;

    protected $sesName = Settings::STRICKER_SESSION_NAME;

    protected $valid   = false;

    protected $site    = 'stricker';

    protected $webservice;

    public static function initialize() {
        $webservice = new WebService();
        $cls = new Stricker($webservice);
        $cls->authenticate();
        return $cls;
    }

    public function __construct(WebService $webservice){
        $this->webservice = $webservice;
    }

    public function getLanguage() {
        // TODO dinamically change laguage by user`s geolocation
        return $this->lang['EN'];
    }

    public function authenticate() {
        // Ask for authentication
        try  {
            // if no session token exists, ask for a new token
            if(!isset($_SESSION[$this->sesName])) {

                // Authorize the client according to credentials and save token in session
                $_SESSION[$this->sesName] = $this->webservice->AuthorizeClient($this->token);

                // if session token is returned, session is valid
                if($_SESSION[$this->sesName] != "") {
                    $this->valid = true;
                    return true;
                }
            }
            else {
                // Validate authenticity of session 
                $this->valid = $this->webservice->ValidateSession($_SESSION[$this->sesName]);

                // if session is invalid, ask for a new session token
                if(!$this->valid) {
                    $_SESSION[$this->sesName] = $this->webservice->AuthorizeClient($this->token);

                    // if session token is returned, session is valid
                    if($_SESSION[$this->sesName] != "") {
                        $this->valid = true;
                        return true;
                    }
                }
            }
        } catch (Exception $ex) {

            // if a type 2 error occurs, the session is invalid
            if($ex->faultcode == 2) unset($_SESSION[$this->sesName]);

            // Show error returned by the webservice
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    public function get(){
        if(func_num_args() == 0){
            throw new \Exception("You must provide method name as argument 1" );
        }
        $args = func_get_args();
        $funcName= array_shift($args);
        if(!in_array($funcName, get_class_methods($this))){
            $this->throwException("Invalid method provided[".$funcName."]", __LINE__);
        }

        if(empty($args)){
            return call_user_func(array($this,$funcName));
        }
        return call_user_func_array(array($this,$funcName), $args);
    }

    public function throwException($msg, $line, $code=0){
        throw new \Exception(sprintf("%s [on line %s]", $msg, $line), $code);
    }

    public function catalogues() {
        $data = $this->webservice->get_Catalogues($_SESSION[$this->sesName],$this->site,$this->getLanguage());
        return json_decode($data);
    }

    public function catalogueSection($catalogueId) {
        $category = $this->webservice ->get_catalogueSections(
                             $_SESSION[$this->sesName],
                             $this->site,
                             $this->getLanguage(),
                             $catalogueId
                         );
        return json_decode($category);
    }

    public function catalogFamilies($catalogueId, $sectionId){
        $data = $this->webservice
                     ->get_catalogueFamilies(
                         $_SESSION[$this->sesName],
                         $this->site,
                         $this->getLanguage(),
                         $categoryId,
                         $sectionId
                );
        $data = json_decode($data);
    }

    public function products($catalogueId, $sectionId, $familyId){
        $data = $this->webservice->client_getProducts(
                $_SESSION[$this->sesName],
                $this->site,
                $this->getLanguage(),
                $catalogueId,
                $sectionId,
                $familyId,
                null,
                null
            );
        $data = json_decode($data);
    }

    public function image($ref){
        $img = $this->webservice->get_productPhoto($_SESSION[$this->sesName], $ref);
        return json_decode($img, true);
    }
}

