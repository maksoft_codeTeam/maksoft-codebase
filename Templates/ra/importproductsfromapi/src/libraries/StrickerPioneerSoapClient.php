<?php
namespace Maksoft\AFI\Libraries;
/**
 * WebService Client v1.34
 *
 * @service StrickerPioneerSoapClient
 */
class StrickerPioneerSoapClient{
	/**
	 * The WSDL URI
	 *
	 * @var string
	 */
	public static $_WsdlUri='http://wsdl.stricker.pt/wsdl_server.php?WSDL';
	/**
	 * The PHP SoapClient object
	 *
	 * @var object
	 */
	public static $_Server=null;

	/**
	 * Send a SOAP request to the server
	 *
	 * @param string $method The method name
	 * @param array $param The parameters
	 * @return mixed The server response
	 */
	public static function _Call($method,$param){
		if(is_null(self::$_Server))
			self::$_Server=new \SoapClient(self::$_WsdlUri, array("connection_timeout"=>60));
		return self::$_Server->__soapCall($method,$param);
	}

	/**
	 * Método de exemplo
	 *
	 * @param string $token application session token
	 * @param string $ref custom parameter
	 * @param string $name custom parameter
	 * @param array $filterParams listing parameters filter: array(page => requested page, limit => total per page)
	 * @return string object list in json
	 */
	public function NovoMetodo($token,$ref,$name,$filterParams){
		return self::_Call('NovoMetodo',Array(
			$token,
			$ref,
			$name,
			$filterParams
		));
	}

	/**
	 * Gets website catalogues by language
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @return string object catalogue list
	 */
	public function get_Catalogues($token,$site_name,$lang){
		return self::_Call('get_Catalogues',Array(
			$token,
			$site_name,
			$lang
		));
	}

	/**
	 * Gets product brands
	 *
	 * @param string $token application session token
	 * @return string object product_brand list
	 */
	public function get_productBrands($token){
		return self::_Call('get_productBrands',Array(
			$token
		));
	}

	/**
	 * Gets product colors
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @return string object product_brand list
	 */
	public function get_productColors($token,$site_name,$lang){
		return self::_Call('get_productColors',Array(
			$token,
			$site_name,
			$lang
		));
	}

	/**
	 * Gets product brands
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $brand_id id of brand
	 * @return string object product_brand list
	 */
	public function get_productColorsByBrand($token,$site_name,$lang,$brand_id){
		return self::_Call('get_productColorsByBrand',Array(
			$token,
			$site_name,
			$lang,
			$brand_id
		));
	}

	/**
	 * Gets product origins names
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @return string object product_brand list
	 */
	public function get_productOrigins($token,$site_name,$lang){
		return self::_Call('get_productOrigins',Array(
			$token,
			$site_name,
			$lang
		));
	}

	/**
	 * Gets website catalogue sections
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $catalogue_id id of catalogue
	 * @return string object catalogue_section list
	 */
	public function get_catalogueSections($token,$site_name,$lang,$catalogue_id){
		return self::_Call('get_catalogueSections',Array(
			$token,
			$site_name,
			$lang,
			$catalogue_id
		));
	}

	/**
	 * Gets website catalogue families by section
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $catalogue_id id of catalogue
	 * @param string $section_id id of section
	 * @return string object product_fam list
	 */
	public function get_catalogueFamilies($token,$site_name,$lang,$catalogue_id,$section_id){
		return self::_Call('get_catalogueFamilies',Array(
			$token,
			$site_name,
			$lang,
			$catalogue_id,
			$section_id
		));
	}

	/**
	 * Gets website catalogue families by section
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @return string object product_fam list
	 */
	public function get_usbFamilies($token,$site_name,$lang){
		return self::_Call('get_usbFamilies',Array(
			$token,
			$site_name,
			$lang
		));
	}

	/**
	 * Gets website catalogue products by family and section
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $catalogue_id id of catalogue
	 * @param string $section_id id of section
	 * @param string $fam_id id of family
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @param string $client_user_id id of client user for prices
	 * @param string $client_id id of client for prices
	 * @param int $margin margin for prices
	 * @param int $multiplier price multiplier
	 * @return string object product_fam list
	 */
	public function get_catalogueProducts($token,$site_name,$lang,$catalogue_id,$section_id,$fam_id,$page,$lines,$client_user_id,$client_id,$margin,$multiplier){
		return self::_Call('get_catalogueProducts',Array(
			$token,
			$site_name,
			$lang,
			$catalogue_id,
			$section_id,
			$fam_id,
			$page,
			$lines,
			$client_user_id,
			$client_id,
			$margin,
			$multiplier
		));
	}

	/**
	 * Gets website catalogue products by family and section
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $fam_id id of family
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @return string object product_fam list
	 */
	public function get_usbProducts($token,$site_name,$lang,$fam_id,$page,$lines){
		return self::_Call('get_usbProducts',Array(
			$token,
			$site_name,
			$lang,
			$fam_id,
			$page,
			$lines
		));
	}
	
	/**
	 * Gets website catalogue products by family and section
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $fam_id id of family
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @return string object product_fam list
	 */
	public function get_pwbProducts($token,$site_name,$lang,$fam_id,$page,$lines){
		return self::_Call('get_pwbProducts',Array(
			$token,
			$site_name,
			$lang,
			$fam_id,
			$page,
			$lines
		));
	}	

	/**
	 * Gets catalogue product by reference
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $product_ref id of product
	 * @param string $catalogue_id id of catalogue
	 * @param string $client_user_id id of client user for prices
	 * @param string $client_id id of client for prices
	 * @param boolean $showprice force show price for client
	 * @param int $seller_id id of seller
	 * @return string object product
	 */
	public function get_productByRef($token,$site_name,$lang,$product_ref,$catalogue_id,$client_user_id,$client_id,$showprice,$seller_id){
		return self::_Call('get_productByRef',Array(
			$token,
			$site_name,
			$lang,
			$product_ref,
			$catalogue_id,
			$client_user_id,
			$client_id,
			$showprice,
			$seller_id
		));
	}

	/**
	 * Gets catalogue product by reference
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $product_ref id of product
	 * @param string $client_user_id id of client user for prices
	 * @return string object product
	 */
	public function get_usbProductByRef($token,$site_name,$lang,$product_ref,$client_user_id){
		return self::_Call('get_usbProductByRef',Array(
			$token,
			$site_name,
			$lang,
			$product_ref,
			$client_user_id
		));
	}
	
	
	/**
	 * Gets catalogue product by reference
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $product_ref id of product
	 * @param string $client_user_id id of client user for prices
	 * @return string object product
	 */
	public function get_pwbProductByRef($token,$site_name,$lang,$product_ref,$client_user_id){
		return self::_Call('get_pwbProductByRef',Array(
			$token,
			$site_name,
			$lang,
			$product_ref,
			$client_user_id
		));
	}

	

	/**
	 * Gets catalogue product detail by id
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $product_detail_id id of product_detail
	 * @param string $client_user_id id of client user for prices
	 * @param string $client_id id of client user for prices
	 * @param boolean $showprice force show price for client
	 * @return string object product_detail
	 */
	public function get_product_detailById($token,$site_name,$lang,$product_detail_id,$client_user_id,$client_id,$showprice){
		return self::_Call('get_product_detailById',Array(
			$token,
			$site_name,
			$lang,
			$product_detail_id,
			$client_user_id,
			$client_id,
			$showprice
		));
	}

	/**
	 * Search website catalogue products by term
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $term search term
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @param string $catalogue_id catalogue id
	 * @param string $section_id section id
	 * @param string $family_id family id
	 * @param string $brand_id brand id
	 * @param array $colors colors id array
	 * @param float $price_from price min value
	 * @param float $price_to price max value
	 * @param int $stock_from stock min quantity
	 * @param string $stock_entry stock next entry date
	 * @param int $qty price quantity
	 * @param boolean $is_new product is new
	 * @param boolean $is_promo product is in promotion
	 * @param string $order order field and ordenation
	 * @param int $client_user_id id of client user for prices
	 * @param int $client_id id of client id for prices
	 * @param string $origin_id origin id of product
	 * @return string object product list
	 */
	public function searchProducts($token,$site_name,$lang,$term,$page,$lines,$catalogue_id,$section_id,$family_id,$brand_id,$colors,$price_from,$price_to,$stock_from,$stock_entry,$qty,$is_new,$is_promo,$order,$client_user_id,$client_id,$origin_id){
		return self::_Call('searchProducts',Array(
			$token,
			$site_name,
			$lang,
			$term,
			$page,
			$lines,
			$catalogue_id,
			$section_id,
			$family_id,
			$brand_id,
			$colors,
			$price_from,
			$price_to,
			$stock_from,
			$stock_entry,
			$qty,
			$is_new,
			$is_promo,
			$order,
			$client_user_id,
			$client_id,
			$origin_id
		));
	}
	
	
	/**
	 * Search website catalogue products by term
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $term search term
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @param string $catalogue_id catalogue id
	 * @param string $section_id section id
	 * @param string $family_id family id
	 * @param string $brand_id brand id
	 * @param array $colors colors id array
	 * @param float $price_from price min value
	 * @param float $price_to price max value
	 * @param int $stock_from stock min quantity
	 * @param string $stock_entry stock next entry date
	 * @param int $qty price quantity
	 * @param boolean $is_new product is new
	 * @param boolean $is_promo product is in promotion
	 * @param string $order order field and ordenation
	 * @param int $client_user_id id of client user for prices
	 * @param int $client_id id of client id for prices
	 * @param string $origin_id origin id of product
	 * @return string object product list
	 */
	public function searchProductsUsb($token,$site_name,$lang,$term,$page,$lines,$catalogue_id,$section_id,$family_id,$brand_id,$colors,$price_from,$price_to,$stock_from,$stock_entry,$qty,$is_new,$is_promo,$str123order,$client_user_id,$client_id,$origin_id){		

		/* Debug function arguments to file */
		/*$myDebugValues = '';
		$numargs = func_num_args();
		sleep(1);
		$myDebugValues.= "Call at: ".time()."\n";
		$myDebugValues.= "Number of arguments: $numargs\n";
		$arg_list = func_get_args();
		for ($i = 0; $i < $numargs; $i++) { $myDebugValues.= "Argument $i is: " . $arg_list[$i] . "\n"; }
		$myDebugFile = fopen("/var/www/wsdl/public_html/debug_soap.txt", "w") or die("Unable to open file!");
		fwrite($myDebugFile, $myDebugValues);
		fclose($myDebugFile);*/
		/* End of Debug */
		
		return self::_Call('searchProductsUsb',Array(
			$token,
			$site_name,
			$lang,
			$term,
			$page,
			$lines,
			$catalogue_id,
			$section_id,
			$family_id,
			$brand_id,
			$colors,
			$price_from,
			$price_to,
			$stock_from,
			$stock_entry,
			$qty,
			$is_new,
			$is_promo,
			$str123order,
			$client_user_id,
			$client_id,
			$origin_id
		));
	}



	/**
	 * Search website catalogue products by term
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $term search term
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @param string $catalogue_id catalogue id
	 * @param string $section_id section id
	 * @param string $family_id family id
	 * @param string $brand_id brand id
	 * @param array $colors colors id array
	 * @param float $price_from price min value
	 * @param float $price_to price max value
	 * @param int $stock_from stock min quantity
	 * @param string $stock_entry stock next entry date
	 * @param int $qty price quantity
	 * @param boolean $is_new product is new
	 * @param boolean $is_promo product is in promotion
	 * @param string $order order field and ordenation
	 * @param int $client_user_id id of client user for prices
	 * @param int $client_id id of client id for prices
	 * @param string $origin_id origin id of product
	 * @return string object product list
	 */
	public function searchProductsPwb($token,$site_name,$lang,$term,$page,$lines,$catalogue_id,$section_id,$family_id,$brand_id,$colors,$price_from,$price_to,$stock_from,$stock_entry,$qty,$is_new,$is_promo,$order,$client_user_id,$client_id,$origin_id){	
		
		return self::_Call('searchProductsPwb',Array(
			$token,
			$site_name,
			$lang,
			$term,
			$page,
			$lines,
			$catalogue_id,
			$section_id,
			$family_id,
			$brand_id,
			$colors,
			$price_from,
			$price_to,
			$stock_from,
			$stock_entry,
			$qty,
			$is_new,
			$is_promo,
			$order,
			$client_user_id,
			$client_id,
			$origin_id
		));
	}	

	/**
	 * Gets product costumization tables
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $product_ref product reference
	 * @param string $client_user_id id of client user for prices
	 * @param string $client_id id of client for prices
	 * @return string object customization_table list
	 */
	public function get_productCustomization($token,$site_name,$lang,$product_ref,$client_user_id,$client_id){
		return self::_Call('get_productCustomization',Array(
			$token,
			$site_name,
			$lang,
			$product_ref,
			$client_user_id,
			$client_id
		));
	}

	/**
	 * Gets product costumization View tables
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $product_ref product reference
	 * @param string $client_user_id id of client user for prices
	 * @param string $client_id id of client for prices
	 * @return string object customization_table list
	 */
	public function get_productCustomizationView($token,$site_name,$lang,$product_ref,$client_user_id,$client_id){
		return self::_Call('get_productCustomizationView',Array(
			$token,
			$site_name,
			$lang,
			$product_ref,
			$client_user_id,
			$client_id
		));
	}

	/**
	 * Gets products in promotion
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $client_id client_id
	 * @param string $lang website language
	 * @return string object product_group list
	 */
	public function get_productPromotions($token,$site_name,$client_id,$lang){
		return self::_Call('get_productPromotions',Array(
			$token,
			$site_name,
			$client_id,
			$lang
		));
	}

	/**
	 * Gets product photo url
	 *
	 * @param string $token application session token
	 * @param string $ref product reference
	 * @return string object product_group list
	 */
	public function get_productPhoto($token,$ref){
		return self::_Call('get_productPhoto',Array(
			$token,
			$ref
		));
	}

	/**
	 * Gets product cost
	 *
	 * @param string $token application session token
	 * @param string $ref product reference
	 * @return float product cost
	 */
	public function get_productCost($token,$ref){
		return self::_Call('get_productCost',Array(
			$token,
			$ref
		));
	}

	/**
	 * Gets customization table prices
	 *
	 * @param string $token application session token
	 * @param array $tables array with customization table codes
	 * @param string $client_user_id id of client user for prices
	 * @return string object customization_table list
	 */
	public function get_customizationPrices($token,$tables,$id_mercado,$client_user_id){
		return self::_Call('get_customizationPrices',Array(
			$token,
			$tables,
			$id_mercado,			
			$client_user_id
		));
	}

	/**
	 * Gets server mail queue
	 *
	 * @param string $token application session token
	 * @return string mail queue list
	 */
	public function mail_queue($token){
		return self::_Call('mail_queue',Array(
			$token
		));
	}

	/**
	 * Creates new order
	 *
	 * @param string $token application session token
	 * @param int $client_id id of client
	 * @param string $obs order observations
	 * @param array $destination order address destination (0 => address, 1 => zipcode, 2 => city, 3 => phone, 4 => country name, 5 => country iso)
	 * @param string $courier choosen courier
	 * @param string $shipping shipping cost
	 * @param array $order_items products to add to cart: array(0 => array(product_detail_id,quantity,price,status))
	 * @param int $source source of request (0: internal; 1:webservice)
	 * @param array $discount order discount
	 * @param string $seller_id seller phc id
	 * @param string $order_status status of order (0 - Aberto, 1 - Armazem, 2 - Em Falta, 3 - Pendente, 4 - Fechada)
	 * @param int $type order type (0 - normal; 1 - draft; 2: reservation; 3: sample)
	 * @param string $internal_ref internal order reference
	 * @param array $custom_fields customization fields
	 * @param bool $new_address new shipping address
	 * @param bool $free_shipping no shipping
	 * @return array order_id and order_line_id
	 */
	public function orderCreate($token,$client_id,$obs,$destination,$courier,$shipping,$dispatch,$stricker_transport_cost,$order_items,$source,$discount,$seller_id,$order_status,$type,$internal_ref,$custom_fields,$new_address,$free_shipping){
		return self::_Call('orderCreate',Array(
			$token,
			$client_id,
			$obs,
			$destination,
			$courier,
			$shipping,
			$dispatch,
			$stricker_transport_cost,
			$order_items,
			$source,
			$discount,
			$seller_id,
			$order_status,
			$type,
			$internal_ref,
			$custom_fields,
			$new_address,
			$free_shipping
		));
	}

	/**
	 * Updates order client
	 *
	 * @param string $token application session token
	 * @param int $client_user_id id of client user
	 * @param int $order_id id of order budget
	 * @param string $obs order observations
	 * @param array $order_items products to add to cart: array(0 => array(product_detail_id,quantity,price,margin))
	 * @param array $product_customization customization to add: array(0 => array(customization_id,n_colors,area,points,adds_ids,cost))
	 * @param int $client_id id of client
	 * @param array $discount order discount
	 * @param int $order_status order status
	 * @param string $internal_ref internal reference
	 * @param array $custom_fields customization fields : array(print_color,pantones,specification,repetition,proof,attachment,mail)
	 * @param string $created_by created name
	 * @param int $seller_id id of seller who modified the order
	 * @return array order_id and order_line_id
	 */
	public function order_clientUpdate($token,$client_user_id,$order_id,$obs,$order_items,$product_customization,$client_id,$discount,$order_status,$internal_ref,$custom_fields,$created_by,$seller_id){
		return self::_Call('order_clientUpdate',Array(
			$token,
			$client_user_id,
			$order_id,
			$obs,
			$order_items,
			$product_customization,
			$client_id,
			$discount,
			$order_status,
			$internal_ref,
			$custom_fields,
			$created_by,
			$seller_id
		));
	}

	/**
	 * Updates order client
	 *
	 * @param string $token application session token
	 * @param int $client_id id of client user
	 * @param int $order_id id of order client
	 * @param int $order_status order status
	 * @return array order_id and order_line_id
	 */
	public function order_clientConfirmDraft($token,$client_id,$order_id,$order_status){
		return self::_Call('order_clientConfirmDraft',Array(
			$token,
			$client_id,
			$order_id,
			$order_status
		));
	}

	/**
	 * Confirms order client reservation
	 *
	 * @param string $token application session token
	 * @param int $client_id id of client user
	 * @param int $order_id id of order client
	 * @return int order_id
	 */
	public function order_clientConfirmReservation($token,$client_id,$order_id){
		return self::_Call('order_clientConfirmReservation',Array(
			$token,
			$client_id,
			$order_id
		));
	}

	/**
	 * Updates address on current order client
	 *
	 * @param string $token application session token
	 * @param int $client_user_id id of client user
	 * @param int $order_id id of budget
	 * @param array $destination order address destination (0 => address, 1 => zipcode, 2 => city, 3 => phone, 4 => country name, 5 => country iso)
	 * @param string $courier choosen courier
	 * @param string $shipping shipping cost
	 * @param int $client_id id of client
	 * @param bool $new_address is a new address
	 * @param bool $free_shipping no shipping costs
	 * @return boolean result
	 */
	public function order_clientAddressUpdate($token,$client_user_id,$order_id,$destination,$courier,$shipping,$client_id,$new_address,$free_shipping){
		return self::_Call('order_clientAddressUpdate',Array(
			$token,
			$client_user_id,
			$order_id,
			$destination,
			$courier,
			$shipping,
			$client_id,
			$new_address,
			$free_shipping
		));
	}

	/**
	 * Cancels order client
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order budget
	 * @param string $obs order observations
	 * @return bool success or fail
	 */
	public function order_clientCancel($token,$order_id,$obs){
		return self::_Call('order_clientCancel',Array(
			$token,
			$order_id,
			$obs
		));
	}

	/**
	 * Prints client order
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order budget
	 * @param int $seller_id id of seller
	 * @return bool success or fail
	 */
	public function order_clientPrint($token,$order_id,$seller_id){
		return self::_Call('order_clientPrint',Array(
			$token,
			$order_id,
			$seller_id
		));
	}

	/**
	 * Authorize client order
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order budget
	 * @param string $order_status status of order
	 * @param int $seller_id id of seller
	 * @param bool $unathorize unathorized order
	 * @return bool success or fail
	 */
	public function order_clientAuthorize($token,$order_id,$order_status,$seller_id,$unathorize){
		return self::_Call('order_clientAuthorize',Array(
			$token,
			$order_id,
			$order_status,
			$seller_id,
			$unathorize
		));
	}

	/**
	 * Deletes order client draft
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order budget
	 * @return bool success or fail
	 */
	public function order_deleteDraft($token,$order_id){
		return self::_Call('order_deleteDraft',Array(
			$token,
			$order_id
		));
	}

	/**
	 * Deletes order budget draft
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order budget
	 * @return bool success or fail
	 */
	public function order_budgetDeleteDraft($token,$order_id){
		return self::_Call('order_budgetDeleteDraft',Array(
			$token,
			$order_id
		));
	}

	/**
	 * Adds complaint to client order
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order budget
	 * @param string $complaint order complaint
	 */
	public function order_clientComplaint($token,$order_id,$complaint){
		return self::_Call('order_clientComplaint',Array(
			$token,
			$order_id,
			$complaint
		));
	}

	/**
	 * Opens client order
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order budget
	 * @param string $obs order observations
	 */
	public function order_clientOpen($token,$order_id,$obs){
		return self::_Call('order_clientOpen',Array(
			$token,
			$order_id,
			$obs
		));
	}

	/**
	 * Creates new order budget
	 *
	 * @param string $token application session token
	 * @param int $client_id id of client
	 * @param string $obs order observations
	 * @param array $destination order address destination (0 => address, 1 => zipcode, 2 => city, 3 => phone, 4 => country name, 5 => country iso)
	 * @param string $courier choosen courier
	 * @param string $shipping shipping cost
	 * @param array $order_items products to add to cart: array(0 => array(product_detail_id,quantity,price,status))
	 * @param array $margin order margin
	 * @param string $seller_id seller phc id
	 * @param array $discount order discount
	 * @param bool $draft order is draft
	 * @param string $payment_conditions quote payment conditions
	 * @param string $validity quote validity date
	 * @return array order_id and order_line_id
	 */
	public function order_budgetCreate($token,$client_id,$obs,$destination,$courier,$shipping,$order_items,$margin,$seller_id,$discount,$draft,$payment_conditions,$validity){
		return self::_Call('order_budgetCreate',Array(
			$token,
			$client_id,
			$obs,
			$destination,
			$courier,
			$shipping,
			$order_items,
			$margin,
			$seller_id,
			$discount,
			$draft,
			$payment_conditions,
			$validity
		));
	}

	/**
	 * Updates order budget
	 *
	 * @param string $token application session token
	 * @param int $client_user_id id of client user
	 * @param int $budget_id id of order budget
	 * @param string $obs order observations
	 * @param array $order_items products to add to cart: array(0 => array(product_detail_id,quantity,price,margin))
	 * @param array $product_customization customization to add: array(0 => array(customization_id,n_colors,area,points,adds_ids,cost))
	 * @param int $client_id id of client
	 * @param array $discount order discount
	 * @param bool $draft budget is draft
	 * @return array order_id and order_line_id
	 */
	public function order_budgetUpdate($token,$client_user_id,$budget_id,$obs,$order_items,$product_customization,$client_id,$discount,$draft){
		return self::_Call('order_budgetUpdate',Array(
			$token,
			$client_user_id,
			$budget_id,
			$obs,
			$order_items,
			$product_customization,
			$client_id,
			$discount,
			$draft
		));
	}

	/**
	 * Updates address on current order budget
	 *
	 * @param string $token application session token
	 * @param int $client_user_id id of client user
	 * @param int $budget_id id of budget
	 * @param array $destination order address destination (0 => address, 1 => zipcode, 2 => city, 3 => phone, 4 => country name, 5 => country iso)
	 * @param string $courier choosen courier
	 * @param string $shipping shipping cost
	 * @param int $client_id id of client
	 * @return boolean result
	 */
	public function order_budgetAddressUpdate($token,$client_user_id,$budget_id,$destination,$courier,$shipping,$client_id){
		return self::_Call('order_budgetAddressUpdate',Array(
			$token,
			$client_user_id,
			$budget_id,
			$destination,
			$courier,
			$shipping,
			$client_id
		));
	}

	/**
	 * Process budget to order
	 *
	 * @param string $token application session token
	 * @param int $budget_id id of budget
	 * @param int $client_id id of client
	 * @param int $seller_id seller phc id
	 * @param array $discount order discount
	 * @return int id of order
	 */
	public function order_budgetProcess($token,$budget_id,$client_id,$seller_id,$discount){
		return self::_Call('order_budgetProcess',Array(
			$token,
			$budget_id,
			$client_id,
			$seller_id,
			$discount
		));
	}

	/**
	 * Closes order budget
	 *
	 * @param string $token application session token
	 * @param int $budget_id id of order budget
	 * @param string $obs choosen transporter
	 * @param string $cancel_reason reason for closing
	 * @param bool $open open budget
	 * @return bool true or false
	 */
	public function order_budgetClose($token,$budget_id,$obs,$cancel_reason,$open){
		return self::_Call('order_budgetClose',Array(
			$token,
			$budget_id,
			$obs,
			$cancel_reason,
			$open
		));
	}

	/**
	 * Adds product customization to order
	 *
	 * @param string $token application session token
	 * @param array $customization_items customization to add: array(0 => array(order_line_id,customization_id,n_colors,area,points,adds_ids,cost))
	 * @param int $type order type (0: order note; 1: order budget)
	 * @return boolean true if success; false if fail
	 */
	public function orderAddCustomization($token,$customization_items,$type){
		return self::_Call('orderAddCustomization',Array(
			$token,
			$customization_items,
			$type
		));
	}

	/**
	 * Concludes client order
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order client
	 * @param string $transporter choosen transporter
	 * @return bool true or false
	 */
	public function orderConclude($token,$order_id,$transporter){
		return self::_Call('orderConclude',Array(
			$token,
			$order_id,
			$transporter
		));
	}

	/**
	 * Concludes client order
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order client
	 * @param string $obs order observations
	 * @return bool true or false
	 */
	public function orderCancel($token,$order_id,$obs){
		return self::_Call('orderCancel',Array(
			$token,
			$order_id,
			$obs
		));
	}

	/**
	 * Adds products lines to order
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order_client
	 * @param array $order_items products to add: array(0 => array(product_detail_id,quantity,price,status))
	 * @return array $lines_ids order_line ids
	 */
	public function orderAddLine($token,$order_id,$order_items){
		return self::_Call('orderAddLine',Array(
			$token,
			$order_id,
			$order_items
		));
	}

	/**
	 * Updates products lines to order
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order_client
	 * @param array $order_items products to update: array(0 => array(product_detail_id,quantity,price,status))
	 * @return bool true or false
	 */
	public function orderUpdateLine($token,$order_id,$order_items){
		return self::_Call('orderUpdateLine',Array(
			$token,
			$order_id,
			$order_items
		));
	}

	/**
	 * Deletes product lines from order
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order_client
	 * @param array $order_items products to delete: array(0 => array(order_client_id,product_detail_id))
	 * @return bool true or false
	 */
	public function orderDelLine($token,$order_id,$order_items){
		return self::_Call('orderDelLine',Array(
			$token,
			$order_id,
			$order_items
		));
	}

	/**
	 * Get order lines
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order_client
	 * @return array order lines
	 */
	public function orderGetLines($token,$order_id){
		return self::_Call('orderGetLines',Array(
			$token,
			$order_id
		));
	}

	/**
	 * Gets order status
	 *
	 * @param string $token application session token
	 * @param int $order_type id of order_client
	 * @return array $status_list order_status_list
	 */
	public function orderStatusList($token,$order_type){
		return self::_Call('orderStatusList',Array(
			$token,
			$order_type
		));
	}

	/**
	 * Adds order status
	 *
	 * @param string $token application session token
	 * @param int $order_id id of order_client
	 * @param string $info information on status
	 * @param int $status order status id
	 * @return array $status_ids order_status ids
	 */
	public function orderUpdateStatus($token,$order_id,$info,$status){
		return self::_Call('orderUpdateStatus',Array(
			$token,
			$order_id,
			$info,
			$status
		));
	}

	/**
	 * Gets order created by name list
	 *
	 * @param string $token application session token
	 * @return array $created_list order_status_list
	 */
	public function orderCreatedList($token){
		return self::_Call('orderCreatedList',Array(
			$token
		));
	}

	/**
	 * Get list of orders
	 *
	 * @param string $token application session token
	 * @param int $client_id id of client_user
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @param string $order order field and ordenation (field;ord)
	 * @param int $year client order year
	 * @return object order lines
	 */
	public function orderGetList($token,$client_id,$page,$lines,$order,$year){
		return self::_Call('orderGetList',Array(
			$token,
			$client_id,
			$page,
			$lines,
			$order,
			$year
		));
	}

	/**
	 * Get list of orders
	 *
	 * @param string $token application session token
	 * @param int $client_id id of client_user
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @param string $order order field and ordenation (field;ord)
	 * @return object order lines
	 */
	public function order_budgetGetList($token,$client_id,$page,$lines,$order){
		return self::_Call('order_budgetGetList',Array(
			$token,
			$client_id,
			$page,
			$lines,
			$order
		));
	}

	/**
	 * Get order details by id
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param int $order_id id of order
	 * @param int $client_id id of client
	 * @param boolean $is_budget order is budget
	 * @param int $year client order year
	 * @return object order lines
	 */
	public function orderGetById($token,$site_name,$lang,$order_id,$client_id,$is_budget,$year){
		return self::_Call('orderGetById',Array(
			$token,
			$site_name,
			$lang,
			$order_id,
			$client_id,
			$is_budget,
			$year
		));
	}

	/**
	 * Get list of orders work sheets
	 *
	 * @param string $token application session token
	 * @param int $client_id id of client_user
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @return object order lines
	 */
	public function order_workList($token,$client_id,$page,$lines){
		return self::_Call('order_workList',Array(
			$token,
			$client_id,
			$page,
			$lines
		));
	}

	/**
	 * Get list of orders in progress
	 *
	 * @param string $token application session token
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @param string $search search expression
	 * @param string $order sort field (asc;desc)
	 * @param int $seller_id id of seller
	 * @param int $type order type
	 * @param int $status order status
	 * @param int $order_status order situation
	 * @param string $date_ini start date
	 * @param string $date_end end date
	 * @param string $ref order type
	 * @param int $source order source (0 - Web, 1 - External, 2 - PHC, 3 - Platform)
	 * @param string $created_by created by name
	 * @param string $country_iso order country_iso
	 * @param int $year client order year
	 * @return object order lines
	 */
	public function order_currentList($token,$page,$lines,$search,$order,$seller_id,$type,$status,$order_status,$date_ini,$date_end,$ref,$source,$created_by,$country_iso,$year){
		return self::_Call('order_currentList',Array(
			$token,
			$page,
			$lines,
			$search,
			$order,
			$seller_id,
			$type,
			$status,
			$order_status,
			$date_ini,
			$date_end,
			$ref,
			$source,
			$created_by,
			$country_iso,
			$year
		));
	}

	/**
	 * Get list of orders in progress
	 *
	 * @param string $token application session token
	 * @param int $client_id id of cliente
	 * @return float order values
	 */
	public function order_pendingTotal($token,$client_id){
		return self::_Call('order_pendingTotal',Array(
			$token,
			$client_id
		));
	}

	/**
	 * Get list of budgets
	 *
	 * @param string $token application session token
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @param string $search search expression
	 * @param string $order sort field (asc;desc)
	 * @param string $seller_id id of seller
	 * @return object order lines
	 */
	public function budget_currentList($token,$page,$lines,$search,$order,$seller_id){
		return self::_Call('budget_currentList',Array(
			$token,
			$page,
			$lines,
			$search,
			$order,
			$seller_id
		));
	}

	/**
	 * Get total of samples value
	 *
	 * @param string $token application session token
	 * @param int $client_id id of client_user
	 * @return float order total
	 */
	public function orderGetSamples($token,$client_id){
		return self::_Call('orderGetSamples',Array(
			$token,
			$client_id
		));
	}

	/**
	 * Get orders report
	 *
	 * @param string $token application session token
	 * @return string report
	 */
	public function order_Report($token){
		return self::_Call('order_Report',Array(
			$token
		));
	}

	/**
	 * Get list of orders year
	 *
	 * @param string $token application session token
	 * @return string year list
	 */
	public function order_historyYear($token){
		return self::_Call('order_historyYear',Array(
			$token
		));
	}

	/**
	 * Request client application authorization from server
	 *
	 * @param string $access_key client access key
	 * @return string authentication token
	 */
	public function AuthorizeApp($access_key){
		return self::_Call('AuthorizeApp',Array(
			$access_key
		));
	}

	/**
	 * Request client application authorization from server
	 *
	 * @param string $access_key client access key
	 * @return string authentication token
	 */
	public function AuthorizeClient($access_key){
		return self::_Call('AuthorizeClient',Array(
			$access_key
		));
	}

	/**
	 * Checks session token validity
	 *
	 * @param string $token application session token
	 * @return bool authentication token
	 */
	public function ValidateSession($token){
		return self::_Call('ValidateSession',Array(
			$token
		));
	}

	/**
	 * Closes webservice session
	 *
	 * @param string $token application session token
	 * @return bool authentication token
	 */
	public function CloseSession($token){
		return self::_Call('CloseSession',Array(
			$token
		));
	}

	/**
	 * Clears all webservice sessions
	 *
	 * @param string $token application session token
	 * @return bool authentication token
	 */
	public function ClearSessions($token){
		return self::_Call('ClearSessions',Array(
			$token
		));
	}

	/**
	 * Gets all availiable shipping countries
	 *
	 * @param string $token application session token
	 * @return string $shipment_countries array with availiable countries for shipping
	 */
	public function shipmentCountries($token){
		return self::_Call('shipmentCountries',Array(
			$token
		));
	}

	/**
	 * Calculates shipping costs for each courier
	 *
	 * @param string $token application session token
	 * @param array $product_data products reference and quantity: array(product reference => quantity in units)
	 * @param string $country country iso code or name
	 * @param string $order_total total cost for this order
	 * @param int $user_id id of client_user
	 * @param int $phc_id id of client_user
	 * @return string $shipment_values shipment costs for each courier and details
	 */
	public function shipmentCalculate($token,$product_data,$country,$order_total,$user_id,$phc_id){
		return self::_Call('shipmentCalculate',Array(
			$token,
			$product_data,
			$country,
			$order_total,
			$user_id,
			$phc_id
		));
	}

	/**
	 * Gets website text
	 *
	 * @param string $token application session token
	 * @param string $type type of text
	 * @param string $lang text language iso
	 * @return string site_txt content
	 */
	public function site_textGetByType($token,$type,$lang){
		return self::_Call('site_textGetByType',Array(
			$token,
			$type,
			$lang
		));
	}
	
	/**
	 * Gets website text by market
	 *
	 * @param string $token application session token
	 * @param string $type type of text
	 * @param string $lang text language iso
	 * @return string site_txt content
	 */
	public function site_textGetByMarket($token,$type, $market_id, $lang){
		return self::_Call('site_textGetByMarket',Array(
			$token,
			$type,
			$market_id,
			$lang
		));
	}

	/**
	 * Gets website emails
	 *
	 * @param string $token application session token
	 * @param string $type type of email
	 * @return object site_emails list
	 */
	public function site_emailsGetByType($token,$type){
		return self::_Call('site_emailsGetByType',Array(
			$token,
			$type
		));
	}

	/**
	 * Gets website translation constants
	 *
	 * @param string $token application session token
	 * @param string $lang language code
	 * @return object site_18n_const list
	 */
	public function site_translations($token,$lang){
		return self::_Call('site_translations',Array(
			$token,
			$lang
		));
	}

	/**
	 * Gets website translation constants
	 *
	 * @param string $token application session token
	 * @param string $site site name
	 * @return object site_conf_const list
	 */
	public function site_constants($token,$site){
		return self::_Call('site_constants',Array(
			$token,
			$site
		));
	}

	/**
	 * Gets website configuration settings
	 *
	 * @param string $token application session token
	 * @param string $site site name
	 * @return object site and site_conf
	 */
	public function site_settings($token,$site){
		return self::_Call('site_settings',Array(
			$token,
			$site
		));
	}

	/**
	 * Gets website text
	 *
	 * @param string $token application session token
	 * @return object list of countries
	 */
	public function getCountries($token){
		return self::_Call('getCountries',Array(
			$token
		));
	}

	/**
	 * Gets website text
	 *
	 * @param string $token application session token
	 * @param string $code country iso code
	 * @return object list of countries
	 */
	public function getCountry($token,$code){
		return self::_Call('getCountry',Array(
			$token,
			$code
		));
	}

	/**
	 * Register client contact form
	 *
	 * @param string $token application session token
	 * @param array $data client data
	 * @return boolean true: success, false: fail
	 */
	public function register_ClientContact($token,$data){
		return self::_Call('register_ClientContact',Array(
			$token,
			$data
		));
	}

	/**
	 * Get client contact
	 *
	 * @param string $token application session token
	 * @param string $email client email
	 * @return object site_client_contacts
	 */
	public function register_getClientContact($token,$email){
		return self::_Call('register_getClientContact',Array(
			$token,
			$email
		));
	}

	/**
	 * Gets website services
	 *
	 * @param string $token application session token
	 * @param string $lang text language iso
	 * @return string object site_services_products list
	 */
	public function get_siteServices($token,$lang){
		return self::_Call('get_siteServices',Array(
			$token,
			$lang
		));
	}

	/**
	 * Gets website service products
	 *
	 * @param string $token application session token
	 * @param string $type type of product
	 * @param string $lang text language iso
	 * @param string $group group of product
	 * @param int $market_id id of market
	 * @return string object site_services_products list
	 */
	public function get_siteServiceProducts($token,$type,$lang,$group,$market_id){
		return self::_Call('get_siteServiceProducts',Array(
			$token,
			$type,
			$lang,
			$group,
			$market_id
		));
	}

	/**
	 * Gets website service bags materials
	 *
	 * @param string $token application session token
	 * @param string $lang text language iso
	 * @return string object site_services_bags list
	 */
	public function get_siteServiceBags($token,$lang){
		return self::_Call('get_siteServiceBags',Array(
			$token,
			$lang
		));
	}

	/**
	 * Gets website service embroidery types
	 *
	 * @param string $token application session token
	 * @param string $lang text language iso
	 * @return string object site_services_embroidery list
	 */
	public function get_siteServiceEmbroidery($token,$lang){
		return self::_Call('get_siteServiceEmbroidery',Array(
			$token,
			$lang
		));
	}

	/**
	 * Gets website service paperin sizes
	 *
	 * @param string $token application session token
	 * @param string $lang text language iso
	 * @return string object site_services_embroidery list
	 */
	public function get_siteServicePaperin($token,$lang){
		return self::_Call('get_siteServicePaperin',Array(
			$token,
			$lang
		));
	}

	/**
	 * Gets website service paperin sizes
	 *
	 * @param string $token application session token
	 * @return string object site_services_embroidery list
	 */
	public function get_siteServiceBranding($token){
		return self::_Call('get_siteServiceBranding',Array(
			$token
		));
	}

	/**
	 * Gets website news
	 *
	 * @param string $token application session token
	 * @param string $lang text language iso
	 * @param string $limit limit news
	 * @return string object site_news list
	 */
	public function get_siteNews($token,$lang,$limit){
		return self::_Call('get_siteNews',Array(
			$token,
			$lang,
			$limit
		));
	}

	/**
	 * Gets website news by id
	 *
	 * @param string $token application session token
	 * @param string $lang text language iso
	 * @param string $id text site_news id
	 * @return string object site_news list
	 */
	public function get_siteNewsById($token,$lang,$id){
		return self::_Call('get_siteNewsById',Array(
			$token,
			$lang,
			$id
		));
	}

	/**
	 * Gets website banners by language
	 *
	 * @param string $token application session token
	 * @param string $lang text language iso
	 * @param int $market_id client market id
	 * @return string object site_banner list
	 */
	public function get_siteBanner($token,$lang,$market_id){
		return self::_Call('get_siteBanner',Array(
			$token,
			$lang,
			$market_id
		));
	}

	/**
	 * Gets website highlights by language and market
	 *
	 * @param string $token application session token
	 * @param string $lang text language iso
	 * @param int $market_id client market id
	 * @return string object site_banner list
	 */
	public function get_siteHighlights($token,$lang,$market_id){
		return self::_Call('get_siteHighlights',Array(
			$token,
			$lang,
			$market_id
		));
	}

	/**
	 * Gets website highlights by language and market
	 *
	 * @param string $token application session token
	 * @param string $lang text language iso
	 * @return string object site_history list
	 */
	public function get_siteHistory($token,$lang){
		return self::_Call('get_siteHistory',Array(
			$token,
			$lang
		));
	}

	/**
	 * Gets website team by language
	 *
	 * @param string $token application session token
	 * @param string $lang text language iso
	 * @return string object site_history list
	 */
	public function get_siteTeam($token,$lang){
		return self::_Call('get_siteTeam',Array(
			$token,
			$lang
		));
	}

	/**
	 * Gets website news
	 *
	 * @param string $token application session token
	 * @param string $lang website language
	 * @return string object site_news list
	 */
	public function get_siteContacts($token,$lang){
		return self::_Call('get_siteContacts',Array(
			$token,
			$lang
		));
	}

	/**
	 * Lists all products and details
	 *
	 * @param string $token application session token
	 * @param string $catalogue_id id of catalogue
	 * @param string $lang language code
	 * @param string $return_type type of return (json ou xml)
	 * @return string product list
	 */
	public function productGetAll($token,$catalogue_id,$lang,$return_type){
		return self::_Call('productGetAll',Array(
			$token,
			$catalogue_id,
			$lang,
			$return_type
		));
	}

	/**
	 * Lists all products and details
	 *
	 * @param string $token application session token
	 * @param string $catalogue_id id of catalogue
	 * @param string $lang language code
	 * @param int $margin margin price for products
	 * @param string $return_type type of return (json ou xml)
	 * @return string product list
	 */
	public function productPriceGetAll($token,$catalogue_id,$lang,$margin,$return_type){
		return self::_Call('productPriceGetAll',Array(
			$token,
			$catalogue_id,
			$lang,
			$margin,
			$return_type
		));
	}

	/**
	 * Lists all products and details
	 *
	 * @param string $token application session token
	 * @param string $catalogue_id id of catalogue
	 * @param string $lang language code
	 * @param int $margin margin price for products
	 * @param string $return_type type of return (json ou xml)
	 * @return string product list
	 */
	public function productPriceDoubleGetAll($token,$catalogue_id,$lang,$margin,$return_type){
		return self::_Call('productPriceDoubleGetAll',Array(
			$token,
			$catalogue_id,
			$lang,
			$margin,
			$return_type
		));
	}

	/**
	 * Lists all products technical info
	 *
	 * @param string $token application session token
	 * @param string $return_type type of return (json ou xml)
	 * @return string product list
	 */
	public function productGetTechnical($token,$return_type){
		return self::_Call('productGetTechnical',Array(
			$token,
			$return_type
		));
	}

	/**
	 * @param string $token application session token
	 * @param array $filterParams listing parameters filter: array(page => requested page, limit => total per page)
	 * @return string object list in json
	 */
	public function CustomProducts($token,$filterParams){
		return self::_Call('CustomProducts',Array(
			$token,
			$filterParams
		));
	}

	/**
	 * Authenticate and initiate client user website session
	 *
	 * @param string $token application session token
	 * @param string $username client username
	 * @param string $password client password
	 * @param string $session_id website session id
	 * @param string $session_name website session name
	 * @param boolean $auth_by_token authentication by token
	 * @return string serialized client object data
	 */
	public function client_userLogin($token,$username,$password,$session_id,$session_name,$auth_by_token){
		return self::_Call('client_userLogin',Array(
			$token,
			$username,
			$password,
			$session_id,
			$session_name,
			$auth_by_token
		));
	}

	/**
	 * Verify client user email and username
	 *
	 * @param string $token application session token
	 * @param string $username client username
	 * @param string $email client email
	 * @return string user salt or false
	 */
	public function client_verifyUsername($token,$username,$email){
		return self::_Call('client_verifyUsername',Array(
			$token,
			$username,
			$email
		));
	}

	/**
	 * Verify client email
	 *
	 * @param string $token application session token
	 * @param string $email client email
	 * @return string client number or name or false
	 */
	public function client_verifyEmail($token,$email){
		return self::_Call('client_verifyEmail',Array(
			$token,
			$email
		));
	}

	/**
	 * Verify client nif
	 *
	 * @param string $token application session token
	 * @param string $nif client email
	 * @return string client number or name or false
	 */
	public function client_verifyNif($token,$nif){
		return self::_Call('client_verifyNif',Array(
			$token,
			$nif
		));
	}

	/**
	 * Logsout client user website session
	 *
	 * @param string $token application session token
	 * @param int $user_id client user id
	 * @param string $session_id website session id
	 * @return boolen true if closed; false if not exists
	 */
	public function client_userLogout($token,$user_id,$session_id){
		return self::_Call('client_userLogout',Array(
			$token,
			$user_id,
			$session_id
		));
	}

	/**
	 * Changes client user password (recovery)
	 *
	 * @param string $token application session token
	 * @param string $password new client user password
	 * @param string $user_token recovery token
	 * @return boolen true if changed; false if invlaid
	 */
	public function client_changePassword($token,$password,$user_token){
		return self::_Call('client_changePassword',Array(
			$token,
			$password,
			$user_token
		));
	}

	/**
	 * Get client details information
	 *
	 * @param string $token session token
	 * @param int $user_id of client user
	 * @param string $session_id client session id
	 * @return string client user data
	 */
	public function client_getDetails($token,$user_id,$session_id){
		return self::_Call('client_getDetails',Array(
			$token,
			$user_id,
			$session_id
		));
	}

	/**
	 * Get client information
	 *
	 * @param string $token session token
	 * @param int $user_id of client user
	 * @param string $session_id client session id
	 * @return string client data
	 */
	public function client_getInfo($token,$user_id,$session_id){
		return self::_Call('client_getInfo',Array(
			$token,
			$user_id,
			$session_id
		));
	}

	/**
	 * Get client and user data
	 *
	 * @param string $token session token
	 * @param int $user_id of client user
	 * @return string client data
	 */
	public function client_getData($token,$user_id){
		return self::_Call('client_getData',Array(
			$token,
			$user_id
		));
	}

	/**
	 * Validates if client session is valid
	 *
	 * @param string $token session token
	 * @param int $user_id of client user
	 * @param string $session_id client session id
	 * @return string client data
	 */
	public function client_validateSession($token,$user_id,$session_id){
		return self::_Call('client_validateSession',Array(
			$token,
			$user_id,
			$session_id
		));
	}

	/**
	 * Changes client user password (account)
	 *
	 * @param string $token application session token
	 * @param int $user_id of client user
	 * @param string $password new client user password
	 * @param string $session_id client session id
	 * @return boolen true if changed; false if invlaid
	 */
	public function client_changePasswordAccount($token,$user_id,$password,$session_id){
		return self::_Call('client_changePasswordAccount',Array(
			$token,
			$user_id,
			$password,
			$session_id
		));
	}

	/**
	 * Get client invoice documents
	 *
	 * @param string $token session token
	 * @param int $user_id of client user
	 * @param string $session_id client session id
	 * @param bool $all_invoice gets all invoices
	 * @param array $filter_dates start and end dates
	 * @return string client user data
	 */
	public function client_getInvoice($token,$user_id,$session_id,$all_invoice,$filter_dates){
		return self::_Call('client_getInvoice',Array(
			$token,
			$user_id,
			$session_id,
			$all_invoice,
			$filter_dates
		));
	}

	/**
	 * Get client invoice documents
	 *
	 * @param string $token session token
	 * @param int $user_id of client user
	 * @param string $session_id client session id
	 * @param int $invoice_id id of invoice document
	 * @return string client user data
	 */
	public function client_getInvoiceById($token,$user_id,$session_id,$invoice_id){
		return self::_Call('client_getInvoiceById',Array(
			$token,
			$user_id,
			$session_id,
			$invoice_id
		));
	}

	/**
	 * Get client invoice documents
	 *
	 * @param string $token session token
	 * @param int $seller_id phc id of seller
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @param string $order field (asc;desc)
	 * @param string $search search term
	 * @param array $filter_dates start and end dates
	 * @param string $country client country iso
	 * @return string client user data
	 */
	public function client_InvoiceList($token,$seller_id,$page,$lines,$order,$search,$filter_dates,$country){
		return self::_Call('client_InvoiceList',Array(
			$token,
			$seller_id,
			$page,
			$lines,
			$order,
			$search,
			$filter_dates,
			$country
		));
	}

	/**
	 * Get client details information
	 *
	 * @param string $token session token
	 * @param int $user_id of client user
	 * @param string $session_id client session id
	 * @return string client user data
	 */
	public function client_getReceipt($token,$user_id,$session_id){
		return self::_Call('client_getReceipt',Array(
			$token,
			$user_id,
			$session_id
		));
	}

	/**
	 * Get client balance list
	 *
	 * @param string $token session token
	 * @param int $client_id of client user
	 * @param string $date_ini initial date
	 * @param string $date_end end date
	 * @return string client balance data
	 */
	public function client_getBalance($token,$client_id,$date_ini,$date_end){
		return self::_Call('client_getBalance',Array(
			$token,
			$client_id,
			$date_ini,
			$date_end
		));
	}

	/**
	 * Get client balance report
	 *
	 * @param string $token session token
	 * @param int $client_id of client user
	 * @param string $date_ini initial date
	 * @param string $date_end end date
	 * @param string $date_diff_ini initial date
	 * @param string $date_diff_end end date
	 * @return string client balance data
	 */
	public function client_getBalanceReport($token,$client_id,$date_ini,$date_end,$date_diff_ini,$date_diff_end){
		return self::_Call('client_getBalanceReport',Array(
			$token,
			$client_id,
			$date_ini,
			$date_end,
			$date_diff_ini,
			$date_diff_end
		));
	}

	/**
	 * Get client balance report
	 *
	 * @param string $token session token
	 * @param int $seller_id phc id of seller
	 * @param string $date_ini initial date
	 * @param string $date_end end date
	 * @param string $date_diff_ini initial date
	 * @param string $date_diff_end end date
	 * @param string $country clients country
	 * @param string $seller_phc_id phc id of seller
	 * @return string client balance data
	 */
	public function client_getBalanceAllReport($token,$seller_id,$date_ini,$date_end,$date_diff_ini,$date_diff_end,$country,$seller_phc_id){
		return self::_Call('client_getBalanceAllReport',Array(
			$token,
			$seller_id,
			$date_ini,
			$date_end,
			$date_diff_ini,
			$date_diff_end,
			$country,
			$seller_phc_id
		));
	}

	/**
	 * Get client balance form all months
	 *
	 * @param string $token session token
	 * @param int $seller_id phc id of seller
	 * @param int $client_id of client user
	 * @param string $country clients country
	 * @param string $seller_phc_id phc id of seller
	 * @return string client balance data
	 */
	public function client_getBalanceMonths($token,$seller_id,$client_id,$country,$seller_phc_id){
		return self::_Call('client_getBalanceMonths',Array(
			$token,
			$seller_id,
			$client_id,
			$country,
			$seller_phc_id
		));
	}

	/**
	 * Get client address book
	 *
	 * @param string $token session token
	 * @param int $user_id of client user
	 * @param string $session_id client session id
	 * @return string client user data
	 */
	public function client_getAddresses($token,$user_id,$session_id){
		return self::_Call('client_getAddresses',Array(
			$token,
			$user_id,
			$session_id
		));
	}

	/**
	 * Get client address book
	 *
	 * @param string $token session token
	 * @param int $client_id id of client
	 * @return string client user data
	 */
	public function client_addressList($token,$client_id){
		return self::_Call('client_addressList',Array(
			$token,
			$client_id
		));
	}

	/**
	 * Get client address book entry
	 *
	 * @param string $token session token
	 * @param int $user_id of client user
	 * @param string $session_id client session id
	 * @param int $address_id of client_address
	 * @return string client user data
	 */
	public function client_getAddressById($token,$user_id,$session_id,$address_id){
		return self::_Call('client_getAddressById',Array(
			$token,
			$user_id,
			$session_id,
			$address_id
		));
	}

	/**
	 * Updates client address book
	 *
	 * @param string $token session token
	 * @param int $user_id of client user
	 * @param string $session_id client session id
	 * @param array $data address book fields
	 * @param int $client_address_id client address id
	 * @return string client user data
	 */
	public function client_updateAddressBook($token,$user_id,$session_id,$data,$client_address_id){
		return self::_Call('client_updateAddressBook',Array(
			$token,
			$user_id,
			$session_id,
			$data,
			$client_address_id
		));
	}

	/**
	 * Sets client address as default
	 *
	 * @param string $token session token
	 * @param int $client_id client id
	 * @param int $client_address_id client address id
	 * @return bool true or false
	 */
	public function client_setAddressDefault($token,$client_id,$client_address_id){
		return self::_Call('client_setAddressDefault',Array(
			$token,
			$client_id,
			$client_address_id
		));
	}

	/**
	 * Adds or updates client address book
	 *
	 * @param string $token session token
	 * @param array $data address book fields
	 * @param int $client_address_id client address id
	 * @return int client_address id
	 */
	public function client_addAddress($token,$data,$client_address_id){
		return self::_Call('client_addAddress',Array(
			$token,
			$data,
			$client_address_id
		));
	}

	/**
	 * Get list of orders
	 *
	 * @param string $token application session token
	 * @return object order lines
	 */
	public function client_orderList($token){
		return self::_Call('client_orderList',Array(
			$token
		));
	}

	/**
	 * Get list of orders
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param int $order_id id of order
	 * @return object order lines
	 */
	public function client_orderDetails($token,$site_name,$lang,$order_id){
		return self::_Call('client_orderDetails',Array(
			$token,
			$site_name,
			$lang,
			$order_id
		));
	}

	/**
	 * Creates new order for client calls (orderCreate)
	 *
	 * @param string $token session token
	 * @param array $products products to be ordered: array("ref" => product reference, "qt" => quantity)
	 * @param array $destination order address destination (0 => address, 1 => zipcode, 2 => city, 3 => phone, 4 => country name, 5 => country iso)
	 * @param string $obs order observations
	 * @return int order_client id
	 */
	public function client_createOrder($token,$products,$destination,$obs){
		return self::_Call('client_createOrder',Array(
			$token,
			$products,
			$destination,
			$obs
		));
	}

	/**
	 * Gets catalogue product by reference (calls get_productByRef)
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $product_ref id of product
	 * @param string $catalogue_id id of catalogue
	 * @return string object product
	 */
	public function client_getProductByRef($token,$site_name,$lang,$product_ref,$catalogue_id){
		return self::_Call('client_getProductByRef',Array(
			$token,
			$site_name,
			$lang,
			$product_ref,
			$catalogue_id
		));
	}

	/**
	 * Gets product costumization tables (calls get_productCustomization)
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $product_ref id of product
	 * @return string object product
	 */
	public function client_getProductCustomization($token,$site_name,$lang,$product_ref){
		return self::_Call('client_getProductCustomization',Array(
			$token,
			$site_name,
			$lang,
			$product_ref
		));
	}

	/**
	 * Gets website catalogue products by family and section (calls get_catalogueProducts)
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $catalogue_id id of catalogue
	 * @param string $section_id id of section
	 * @param string $fam_id id of family
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @return string object product_fam list
	 */
	public function client_getProducts($token,$site_name,$lang,$catalogue_id,$section_id,$fam_id,$page,$lines){
		return self::_Call('client_getProducts',Array(
			$token,
			$site_name,
			$lang,
			$catalogue_id,
			$section_id,
			$fam_id,
			$page,
			$lines
		));
	}

	/**
	 * Gets website catalogue products by family and section (calls get_catalogueProducts)
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $catalogue_id id of catalogue
	 * @param string $section_id id of section
	 * @param int $margin margin for prices
	 * @param string $fam_id id of family
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @return string object product_fam list
	 */
	public function client_getProductsMargin($token,$site_name,$lang,$catalogue_id,$section_id,$margin,$fam_id,$page,$lines){
		return self::_Call('client_getProductsMargin',Array(
			$token,
			$site_name,
			$lang,
			$catalogue_id,
			$section_id,
			$margin,
			$fam_id,
			$page,
			$lines
		));
	}

	/**
	 * Gets website catalogue products by family and section (calls get_catalogueProducts)
	 *
	 * @param string $token application session token
	 * @param string $site_name website name
	 * @param string $lang website language
	 * @param string $catalogue_id id of catalogue
	 * @param string $section_id id of section
	 * @param int $margin margin for prices
	 * @param string $fam_id id of family
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @return string object product_fam list
	 */
	public function client_getProductsDouble($token,$site_name,$lang,$catalogue_id,$section_id,$margin,$fam_id,$page,$lines){
		return self::_Call('client_getProductsDouble',Array(
			$token,
			$site_name,
			$lang,
			$catalogue_id,
			$section_id,
			$margin,
			$fam_id,
			$page,
			$lines
		));
	}

	/**
	 * Gets all clients by country
	 *
	 * @param string $token session token
	 * @param string $country country iso
	 * @param int $seller_phc phc id of commercial
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @param string $order field (asc;desc)
	 * @param string $search search term
	 * @param array $advanced_search array fields and values to search
	 * @param bool $alloworder hide clients who are anot allowed to order
	 * @return string client list
	 */
	public function client_getByCountry($token,$country,$seller_phc,$page,$lines,$order,$search,$advanced_search,$alloworder){
		return self::_Call('client_getByCountry',Array(
			$token,
			$country,
			$seller_phc,
			$page,
			$lines,
			$order,
			$search,
			$advanced_search,
			$alloworder
		));
	}

	/**
	 * Gets all clients countries
	 *
	 * @param string $token session token
	 * @param int $seller_phc phc id of commercial
	 * @return string country list
	 */
	public function client_getCountries($token,$seller_phc){
		return self::_Call('client_getCountries',Array(
			$token,
			$seller_phc
		));
	}

	/**
	 * Gets all clients country zones
	 *
	 * @param string $token session token
	 * @return string country_zone list
	 */
	public function client_getCountryZones($token){
		return self::_Call('client_getCountryZones',Array(
			$token
		));
	}

	/**
	 * Register client for PHC
	 *
	 * @param string $token session token
	 * @param string $name client name
	 * @param string $country client country
	 * @param string $country_iso client country iso code
	 * @param string $zipcode client zipcode
	 * @param string $zipcodename client zipcode name
	 * @param string $billing_address billing address
	 * @param string $billing_zipcode billing zipcode
	 * @param string $billing_zipcodename billing zipcode name
	 * @param string $billing_country billing country
	 * @param string $nif client fiscal number
	 * @param string $phone client phone
	 * @param string $email client email
	 * @param string $website client website
	 * @param string $bank client bank
	 * @param string $agency client agency
	 * @param string $iban client iban
	 * @param string $info client info
	 * @param int $seller_phc_id phc id of commercial
	 * @param boolean $valid_vies vies validation is valid
	 * @param array $shipping_address array with client shipping addresses
	 * @param array $contacts array with client contacts
	 * @param bool $from_website client was registered in website
	 * @param bool $vies_error vies validation failed
	 * @param string $email_docs email for documents
	 * @param string $zone country zone
	 * @return string $seller_emails seller emails or false
	 */
	public function client_Register($token,$name,$country,$country_iso,$zipcode,$zipcodename,$billing_address,$billing_zipcode,$billing_zipcodename,$billing_country,$nif,$phone,$email,$website,$bank,$agency,$iban,$info,$seller_phc_id,$valid_vies,$shipping_address,$contacts,$from_website,$vies_error,$email_docs,$zone){
		return self::_Call('client_Register',Array(
			$token,
			$name,
			$country,
			$country_iso,
			$zipcode,
			$zipcodename,
			$billing_address,
			$billing_zipcode,
			$billing_zipcodename,
			$billing_country,
			$nif,
			$phone,
			$email,
			$website,
			$bank,
			$agency,
			$iban,
			$info,
			$seller_phc_id,
			$valid_vies,
			$shipping_address,
			$contacts,
			$from_website,
			$vies_error,
			$email_docs,
			$zone
		));
	}

	/**
	 * Get pending registered clients
	 *
	 * @param string $token session token
	 * @param int $seller_id seller id
	 * @return string client user list
	 */
	public function client_getPending($token,$seller_id){
		return self::_Call('client_getPending',Array(
			$token,
			$seller_id
		));
	}

	/**
	 * Gets all clients registration
	 *
	 * @param string $token session token
	 * @param int $page requested page
	 * @param int $lines lines per page
	 * @param string $order field (asc;desc)
	 * @param string $search search term
	 * @return string client list
	 */
	public function client_getRegister($token,$page,$lines,$order,$search){
		return self::_Call('client_getRegister',Array(
			$token,
			$page,
			$lines,
			$order,
			$search
		));
	}

	/**
	 * Aprove client registration
	 *
	 * @param string $token session token
	 * @param int $id registration id
	 * @param int $seller_id seller id
	 * @param array $data client data
	 * @param boolean $update only updates registration
	 * @return string client user list
	 */
	public function client_approveRegistration($token,$id,$seller_id,$data,$update){
		return self::_Call('client_approveRegistration',Array(
			$token,
			$id,
			$seller_id,
			$data,
			$update
		));
	}

	/**
	 * Delete client registration
	 *
	 * @param string $token session token
	 * @param int $id registration id
	 * @return string client user list
	 */
	public function client_deleteRegistration($token,$id){
		return self::_Call('client_deleteRegistration',Array(
			$token,
			$id
		));
	}

	/**
	 * Selects client registration
	 *
	 * @param string $token session token
	 * @param int $id registration id
	 * @return string client user list
	 */
	public function client_getRegistration($token,$id){
		return self::_Call('client_getRegistration',Array(
			$token,
			$id
		));
	}

	/**
	 * Get registered user clients in phc
	 *
	 * @param string $token session token
	 * @return string client user list
	 */
	public function client_getUserRegister($token){
		return self::_Call('client_getUserRegister',Array(
			$token
		));
	}

	/**
	 * Set registered user clients in phc
	 *
	 * @param string $token session token
	 * @param int $user_id client_register_user_phc id
	 * @return boolean true or false
	 */
	public function client_setUserRegister($token,$user_id){
		return self::_Call('client_setUserRegister',Array(
			$token,
			$user_id
		));
	}

	/**
	 * Gets all sellers
	 *
	 * @param string $token session token
	 * @return string client user list
	 */
	public function getSellers($token){
		return self::_Call('getSellers',Array(
			$token
		));
	}

	/**
	 * Gets all sellers
	 *
	 * @param string $token session token
	 * @param int $id seller phc _id
	 * @return string client user list
	 */
	public function get_SellerById($token,$id){
		return self::_Call('get_SellerById',Array(
			$token,
			$id
		));
	}

	/**
	 * Get client debt alerts to be sent
	 *
	 * @param string $token session token
	 * @param string $type type of alert
	 * @param int $seller_id id of seller
	 * @return string client user list
	 */
	public function client_getAlertDebt($token,$type,$seller_id){
		return self::_Call('client_getAlertDebt',Array(
			$token,
			$type,
			$seller_id
		));
	}

	/**
	 * Cancel send of alert debt
	 *
	 * @param string $token session token
	 * @param int $alert_id id of alert to be canceled
	 * @param string $obs observations
	 * @param int $user_id user who made the cancel
	 * @param boolean $send send alert
	 * @param int $type type of alert to be canceled
	 * @return bolean success true or false
	 */
	public function client_cancelAlertDebt($token,$alert_id,$obs,$user_id,$send,$type){
		return self::_Call('client_cancelAlertDebt',Array(
			$token,
			$alert_id,
			$obs,
			$user_id,
			$send,
			$type
		));
	}

	/**
	 * Adds or updates Subjects to Client Notes, by CP [07-08-2013]
	 *
	 * @param string $token session token
	 * @param array $data address book fields
	 * @param int $subject_id subject to note id
	 * @return int success true or false
	 */
	public function client_addSubjects($token,$data,$subject_id){
		return self::_Call('client_addSubjects',Array(
			$token,
			$data,
			$subject_id
		));
	}

	/**
	 * Delete Subject Configuration, by CP [08-08-2013]
	 *
	 * @param string $token session token
	 * @param int $subject_id subject id
	 * @return boolean sucess true, error false
	 */
	public function client_deleteSubject($token,$subject_id){
		return self::_Call('client_deleteSubject',Array(
			$token,
			$subject_id
		));
	}

	/**
	 * Gets all Subjects, by CP [07-08-2013]
	 *
	 * @param string $token session token
	 * @param string $page page position
	 * @param string $lines number lines
	 * @param string $order table ordering
	 * @param string $search quickly search
	 * @param string $advanced_search advance search
	 * @return array subject to notes list
	 */
	public function client_getSubjects($token,$page,$lines,$order,$search,$advanced_search){
		return self::_Call('client_getSubjects',Array(
			$token,
			$page,
			$lines,
			$order,
			$search,
			$advanced_search
		));
	}

	/**
	 * Get Subject data, by CP [07-08-2013]
	 *
	 * @param string $token session token
	 * @param int $subject_id of Subject
	 * @return array subject data
	 */
	public function client_getSubjectDetails($token,$subject_id){
		return self::_Call('client_getSubjectDetails',Array(
			$token,
			$subject_id
		));
	}

	/**
	 * Count how many times the Subjects is used into Clients Notes, by CP [07-08-2013]
	 *
	 * @param string $token session token
	 * @param int $subject_id of Subject
	 * @return int total uses
	 */
	public function client_countNotesBySubjectId($token,$subject_id){
		return self::_Call('client_countNotesBySubjectId',Array(
			$token,
			$subject_id
		));
	}

	/**
	 * Adds Notes to Client, by CP [07-08-2013]
	 *
	 * @param string $token session token
	 * @param array $data address book fields
	 * @return int success true or false
	 */
	public function client_addNotes($token,$data){
		return self::_Call('client_addNotes',Array(
			$token,
			$data
		));
	}

	/**
	 * Get NOTES per CLIENT, by CP [07-08-2013]
	 *
	 * @param string $token session token
	 * @param int $client_id Client ID
	 * @param array $advanced_search Advance Search data
	 * @return array subject data
	 */
	public function client_getNotesDetails($token,$client_id,$advanced_search){
		return self::_Call('client_getNotesDetails',Array(
			$token,
			$client_id,
			$advanced_search
		));
	}

	/**
	 * Get customization extra cost
	 *
	 * @param string $token session token
	 * @param int $client_id Client ID
	 * @param string $lang language code
	 * @return array customization extra
	 */
	public function client_customizationExtra($token,$client_id,$lang){
		return self::_Call('client_customizationExtra',Array(
			$token,
			$client_id,
			$lang
		));
	}

	/**
	 * Logs webservice access
	 *
	 * @param string $token application session token
	 * @param string $ip_address ip address of access
	 * @param string $username ip address of access
	 * @param string $method method requested
	 * @param string $log log action
	 * @return bool log insert
	 */
	public function LogAccess($token,$ip_address,$username,$method,$log){
		return self::_Call('LogAccess',Array(
			$token,
			$ip_address,
			$username,
			$method,
			$log
		));
	}

	/**
	 * Validates client by sub-domain
	 *
	 * @param string $token application session token
	 * @param string $subdomain client website subdomain
	 * @return int client id or 0 if not valid
	 */
	public function hidea_validateClientBySubdomain($token,$subdomain){
		return self::_Call('hidea_validateClientBySubdomain',Array(
			$token,
			$subdomain
		));
	}

	/**
	 * Get hidea client details information
	 *
	 * @param string $token session token
	 * @param int $client_id id of client
	 * @return string hidea client user data
	 */
	public function hidea_getClientDetails($token,$client_id){
		return self::_Call('hidea_getClientDetails',Array(
			$token,
			$client_id
		));
	}

	/**
	 * Get hidea client temprary saved details information
	 *
	 * @param string $token session token
	 * @param int $client_id id of client
	 * @return string hidea client user data
	 */
	public function hidea_getClientTempDetails($token,$client_id){
		return self::_Call('hidea_getClientTempDetails',Array(
			$token,
			$client_id
		));
	}

	/**
	 * Get hidea website contents for client
	 *
	 * @param string $token session token
	 * @param int $client_id id of client
	 * @param string $lang i18n language code
	 * @return string hidea contents data
	 */
	public function hidea_getClientContents($token,$client_id,$lang){
		return self::_Call('hidea_getClientContents',Array(
			$token,
			$client_id,
			$lang
		));
	}

	/**
	 * Get hidea website temporary saved contents for client
	 *
	 * @param string $token session token
	 * @param int $client_id id of client
	 * @param string $lang i18n language code
	 * @return string hidea contents data
	 */
	public function hidea_getClientTempContents($token,$client_id,$lang){
		return self::_Call('hidea_getClientTempContents',Array(
			$token,
			$client_id,
			$lang
		));
	}

	/**
	 * Check for pending content aprovals
	 *
	 * @param string $token session token
	 * @param int $client_id id of client
	 * @return string hidea contents data
	 */
	public function hidea_checkContents($token,$client_id){
		return self::_Call('hidea_checkContents',Array(
			$token,
			$client_id
		));
	}

	/**
	 * Validates if domain is not already in use
	 *
	 * @param string $token session token
	 * @param int $client_id id of client
	 * @param string $domain subdomain
	 * @return string hidea contents data
	 */
	public function hidea_validateDomain($token,$client_id,$domain){
		return self::_Call('hidea_validateDomain',Array(
			$token,
			$client_id,
			$domain
		));
	}

	/**
	 * Set hidea website contents for client
	 *
	 * @param string $token session token
	 * @param int $client_id id of client
	 * @param array $hidea_client hidea client data
	 * @param array $hidea_contents hidea contents data
	 * @param array $hidea_contact hidea contacts data
	 * @param array $hidea_products hidea promotion products data
	 * @return boolen true if success, false if fail
	 */
	public function hidea_setClientContents($token,$client_id,$hidea_client,$hidea_contents,$hidea_contact,$hidea_products){
		return self::_Call('hidea_setClientContents',Array(
			$token,
			$client_id,
			$hidea_client,
			$hidea_contents,
			$hidea_contact,
			$hidea_products
		));
	}

	/**
	 * Get hidea website promotion products for client
	 *
	 * @param string $token session token
	 * @param int $client_id id of client
	 * @return string hidea promotion data
	 */
	public function hidea_getClientPromotions($token,$client_id){
		return self::_Call('hidea_getClientPromotions',Array(
			$token,
			$client_id
		));
	}
}
