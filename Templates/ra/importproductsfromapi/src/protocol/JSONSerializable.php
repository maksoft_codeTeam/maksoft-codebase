<?php
namespace Maksoft\AFI\Protocol;


interface JSONSerializable{
    public function toJSON();
}
