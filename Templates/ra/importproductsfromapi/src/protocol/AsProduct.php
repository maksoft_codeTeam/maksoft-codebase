<?php
namespace Maksoft\AFI\Protocol;


interface AsProduct {

    public function getSku();
    public function getPrice();
    public function getName();
    public function getDescription();
    public function getImages();
    public function asJSON();
    public function asArray();
}
