<?php mb_internal_encoding ("cp1251"); 
// Switch off the cache
ini_set('soap.wsdl_cache',0);
ini_set('soap.wsdl_cache_limit',0);
ini_set('soap.wsdl_cache_ttl',0);
ini_set('soap.wsdl_cache_enabled',0);


// Include the client soap to communicate with the webservice methods
require_once("StrickerPioneerSoapClient.php");

// Start session
session_start();

/*--------------------------------------------------------------------------------------------------------------*/

// Create a new instance for the webservice client
$webservice = new StrickerPioneerSoapClient();

// Start validation false
$valid = false;
$token = 'JkNdxQFGvxWU3LZd';
// Start language arrays
$lang = array("PT" => "por", "EN" => "eng", "ES" => "spa", "NL" => "nld", "PL" => "pol");



// if session is valid, make the request to the webservice
if($valid == true) {
	$site = "stricker";
	$data = $webservice->get_Catalogues($_SESSION["token"],$site,$lang["EN"]);
	$data = json_decode($data);
	$data = (array) $data;
	$highIdea = (array) $data;
	$highIdea = array_shift($data);
	$category = $webservice->get_catalogueSections($_SESSION["token"],$site,$lang["EN"],$highIdea->id);
	$category = json_decode($category, true);
	$bags = $category[4];
	$data = $webservice->get_catalogueFamilies($_SESSION["token"],$site,$lang["EN"],$highIdea->id,$bags["id"]);
	$data = json_decode($data, true);
	$cat_f = $data[1];
	$data = $webservice->client_getProducts(
		$_SESSION["token"],
        $site,
        $lang["EN"],
		$highIdea->id,
		$bags["id"],
		$cat_f["id"],
		null,
		null
	);
    $products = json_decode($data);
    $count = 1;
    function get_image($webservice, $ref){
        $img = $webservice->get_productPhoto($_SESSION["token"], $ref);
        return json_decode($img, true);
    }
    if(isset($_GET["ref"])){
        $data = $webservice->client_getProductByRef($_SESSION["token"],$site,$lang["EN"],$_GET["ref"],null);
        //print_r(json_decode($data));
        $img = 
        $product = json_decode($data);
        $images = get_image($webservice, $product->ref);
        $img = array_shift($images);
        if(!$images){
            $img = "http://talajahelp.com/img/no_product.png";
        }
        ?>
        <div id="pageContent">
    <div class="page-content rounded">
        <a class="page-back rounded" href="/page.php?n=147853&amp;SiteID=2&#10;" title=
        "Биоразградими еко химикалки"></a>

        <div id="box-font-size">
            <a href="javascript:%20void(0)" id="inc-font" title="">A+</a> / <a href=
            "javascript:%20void(0)" id="dec-font" title="">A-</a>
        </div>


        <h1 class="title"><?php echo $product->description;?></h1>
        <img align="right" alt="еко химикалка бяло-синя" class="main_image align-right" src=
        "/img_preview.php?image_file=<?php echo $img;?>&amp;img_width=300&amp;ratio=strict">

        <table border="0" cellpadding="2" cellspacing="2" id="page_prices">
            <tbody>
                <tr>
                    <th colspan="7"><?php echo $product->description;?></th>
                </tr>

            <?php
           $price = number_format((float)$product->price_from * 1.96, 2, '.', '');
            foreach($product->product_detail as $p){ ?>
                <tr>
                    <?php if($p->stock <= 5){
                        continue;
                    }
                    ?>
                    <td align="center" class="" valign="top" width="70"><span>color: <?php echo $p->color_name;?></span></td>
                    <td align="center" class="cart_item_code" valign="top" width="70"><?php echo $p->id;?></td>
                    <td align="center" class="cart_item_availability" valign="top" width="60px">
                        <span class="product-on-stock" title="Product available"></span>
                    </td>
                    <?php if($p->min_sale_qt == 0){ $p->min_sale_qt = 1; } ?>
                    <td align="left" class="cart_item_pcs" valign="top">x <?php echo $p->min_sale_qt;?></td>

                    <td align="center" class="cart_item_qty" valign="top" width="30"><b><?php echo $p->min_sale_qt;?></b>x</td>

                    <td align="right" class="cart_item_price" valign="top" width="30"><b><?php echo $price;?></b>
                    </td>

                    <td align="center" class="cart" valign="top" width="50">
                        <a href=
                        "page.php?n=13202&amp;no=<?php echo $p->id."/".$p->product_ref;?>&amp;SiteID=2&amp;action=insert&amp;pid=<?=$price;?>">
                        <img alt="" border="0" hspace="0" src=
                        "http://www.maksoft.net/web/images/shoping_cart.gif" vspace="0"></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <br clear="all">
        <hr class="divider">
        <script language="JavaScript" type="text/JavaScript">
        <!--
        function check_form(form) {
        if(form.Firma.value=="") alert("Моля попълнете име на фирма! Компанията ни работи с големи клиенти и посредници и не сме в състояние да поемем частни поръчки!");
        if ((form.EMail.value!="") && (form.Tiraj.value!=""))  form.submit(); else alert("Моля попълнете задължителните полета отбелязани със * !");
        }
        //-->
        </script>
        <div class="alert alert-warning">
            <a href="http://www.maksoft.bg/catalog-reklama.html" target="_blank" title=
            "Свалете ценовата листа в Excel формат от ТУК!">Онлайн каталог и пълна ценова листа
            можете да свалите от тук!</a><br>
            Цените не включват ДДС. Компанията работи само с фирми!
        </div>
        <div class="alert alert-info">
            <i class="fa fa-refresh fa-spin"></i> <span class="sr-only">За определени продукти и
            количества се ползват</span> <b>отстъпки</b> за конкретна поръчка!<br>
            <br>
            За валидната в момента отстъпка, моля <b>попълнете запитването</b>!
        </div>
        Своето конкретно запитване можете да отправите чрез формата по-долу.
        <p>
        </p>
    </div>
    <p>
    </p>
    <p>
    </p>
    <div class="alert alert-success">
        <ul>
            <li>Компанията работи основно с рекламни агенции, търговци на едро и крайни клиенти-
            <strong>фирми</strong>.<br>
            За съжаление, не можем да се ангажираме с малки поръчки на частни лица!</li>
            <li>За спешни въпроси и консултации моля <strong>позвънете</strong> на <a alt=
            "телефони за контакт с Максофт и други рекламни агенции" href=
            "http://www.maksoft.bg/contact-reklamni-agencii.html">телефоните за контакт</a> от тук
            </li>
            <li>Плащането е 50% авансово, 50% при получаване на готовата продукция;</li>
            <li>Ползват се <strong>отстъпки</strong> за годишен оборот или отстъпки по договор от
            рекламни агенции и крайни клиенти- <strong>фирми</strong>;</li>
        </ul>
    </div>
    <div id="cms_admin_buttons">
        <a class="cms-add-page" href=
        "/page.php?n=111&amp;ParentPage=157455&amp;SiteID=2"><span></span>добави</a><a class=
        "cms-edit-page" href=
        "/page.php?n=132&amp;no=157455&amp;SiteID=2"><span></span>редактирай</a>
    </div>
    <div class="fb-like fb_iframe_widget" data-share="true" data-show-faces="false" data-width=
    "450" fb-iframe-plugin-query=
    "app_id=713405105407288&amp;container_width=708&amp;href=http%3A%2F%2Fwww.maksoft.bg%2Fpage.php%3Fn%3D157455%26SiteID%3D2&amp;locale=bg_BG&amp;sdk=joey&amp;share=true&amp;show_faces=false&amp;width=450"
    fb-xfbml-state="rendered">
        <span style="vertical-align: bottom; width: 450px; height: 20px;"><iframe allowfullscreen=
        "true" allowtransparency="true" class="" frameborder="0" height="1000px" id=
        "f1b79a103ae32c" name="f1b79a103ae32c" scrolling="no" src=
        "https://www.facebook.com/v2.1/plugins/like.php?app_id=713405105407288&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FP5DLcu0KGJB.js%3Fversion%3D42%23cb%3Df321993601658f%26domain%3Dwww.maksoft.bg%26origin%3Dhttp%253A%252F%252Fwww.maksoft.bg%252Ff30070cc2a0c3d%26relation%3Dparent.parent&amp;container_width=708&amp;href=http%3A%2F%2Fwww.maksoft.bg%2Fpage.php%3Fn%3D157455%26SiteID%3D2&amp;locale=bg_BG&amp;sdk=joey&amp;share=true&amp;show_faces=false&amp;width=450"
        style="border: none; visibility: visible; width: 450px; height: 20px;" title=
        "fb:like Facebook Social Plugin" width="450px"><span style=
        "vertical-align: bottom; width: 450px; height: 20px;"></span></iframe></span>
    </div>


    <div id="___plusone_0" style=
    "text-indent: 0px; margin: 0px; padding: 0px; background: transparent; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 106px; height: 24px;">
    <iframe data-gapiattached="true" frameborder="0" hspace="0" id="I0_1475243947652"
        marginheight="0" marginwidth="0" name="I0_1475243947652" scrolling="no" src=
        "https://apis.google.com/u/0/se/0/_/+1/fastbutton?usegapi=1&amp;hl=bg&amp;origin=http%3A%2F%2Fwww.maksoft.bg&amp;url=http%3A%2F%2Fwww.maksoft.bg%2Fpage.php%3Fn%3D147853%26SiteID%3D2&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.bg.IHKXHVeWUXc.O%2Fm%3D__features__%2Fam%3DAQ%2Frt%3Dj%2Fd%3D1%2Frs%3DAGLTcCPIX0hyg_GzfUGSc-26q8G_Tyfmgg#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I0_1475243947652&amp;parent=http%3A%2F%2Fwww.maksoft.bg&amp;pfname=&amp;rpctoken=14839910"
        style=
        "position: static; top: 0px; width: 106px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 24px;"
        tabindex="0" title="+1" vspace="0" width="100%"></iframe>
    </div>
    <br clear="all">
    <div id="navbar">
        <a class="nav_links" href="/page.php?n=2&amp;SiteID=2" title=
        "Максофт- рекламни сувенири и материали">Начало</a>&nbsp;/&nbsp;<a class="nav_links" href=
        "/page.php?n=1425&amp;SiteID=2" itemprop="url" title=
        "Рекламни химикалки от вносител"><span itemprop=
        "name">Химикалки</span></a>&nbsp;/&nbsp;<a class="nav_links" href=
        "/page.php?n=1426&amp;SiteID=2" itemprop="url" title=""><span itemprop="name">пластмасови
        химикалки</span></a>&nbsp;/&nbsp;<a class="nav_links" href=
        "/page.php?n=147853&amp;SiteID=2" itemprop="url" title=
        "Еко биоразградими химикалки за реклама"><span itemprop="name">Биоразградими еко
        химикалки</span></a>&nbsp;/&nbsp;еко химикалка бяло-синя&nbsp;/&nbsp;<a class=
        "navbar-admin-page" href="/page.php?n=11&amp;SiteID=2">Администрация</a> [ <a class=
        "navbar-add-page" href="/page.php?n=111&amp;ParentPage=157455&amp;SiteID=2">добави</a> |
        <a class="navbar-edit-page" href=
        "/page.php?n=132&amp;no=157455&amp;SiteID=2">редактирай</a> ] <b>Радослав Йорданов</b>,
        <a class="navbar-logout-page" href="/web/admin/logout.php">изход</a>
    </div>
    <br clear="all">
</div>

        <?php
    } else{
    ?>
    <div class="sDelimeter"></div>
    <?php
    foreach($products as $product){
        $img = $webservice->get_productPhoto($_SESSION["token"], $product->ref);
        $img = json_decode($img, true);
        if(!$img){
            continue;
        }
        $url = "http://www.maksoft.bg/page.php?";
        $url .= http_build_query(array('n'=>$o_page->_page['n'], 'SiteID' => $o_page->_page['SiteID'], 'ref'=>$product->ref));
        $img = array_shift($img);
        ?>
            <div class="sPage" style="float: left; width: 317px; margin: 6.6px;">
        <?php print_r(json_decode($img)); ?>
                <div class="sPage-content">
                    <a href="<?php echo $url;?>" title="<?=$product->description;?>" class="title">
                        <div class="bullet1" style="float:left;"></div>
                        <div class="text"><?php echo $product->description;?></div>
                    </a>
                    <br style="clear: both;" class="br-style">
                    <a href="<?php echo $url;?>" class="img align-center" style="" title="<?=$product->description;?>">
                        <img src="/img_preview.php?image_file=<?php echo $img;?>" align="" class="align-center" alt="<?=$product->description;?>" border="0">
                    </a>
                </div>
                <div class="page-cart-options">
                    <span class="product-on-stock" title="Product available"></span>
                    <?php
                       $price = number_format((float)$product->price_from * 1.96, 2, '.', '');
                    ?>
                    <span class="sPage-price"><?php echo $price;?></span>
                </div>
            </div>

        <?php
        if($count % 2 == 0){
         echo '<div style="clear: both" class="sPage-separator"></div>';   
        }
        $count +=1;
    }

    }
    }
