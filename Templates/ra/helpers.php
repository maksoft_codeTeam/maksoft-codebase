<?php

function format_invoice_number($number) {
    return str_pad($number, 10, "0", STR_PAD_LEFT);
}

function get_items($number){
    global $gate;
    return $gate->fak()->getDocumentItems($number);
}
