<?php
class RaMaksoftLoader implements \Pimple\ServiceProviderInterface
{
    public function register(\Pimple\Container $container)
    {
        $container['templ_dir'] = $_SERVER['DOCUMENT_ROOT'] . '/Templates/ra/templates/';
        $container['db'] = function($c){
            $env = array();
            $env['DATABASE']['DB_HOST']= 'localhost';
            $env['DATABASE']['DB_NAME']= 'maksoft';
            $env['DATABASE']['DB_USER']= 'maksoft';
            $env['DATABASE']['DB_PASS']= 'mak211';
            $db = new \Maksoft\Core\MysqlDb($env);
            $db->exec("SET NAMES cp1251");
            return $db;
        };

        $container['gate'] = $container->factory(function($c){
            return new \Maksoft\Gateway\Gateway($c['db']);
        });

        $container['twig_filesystem'] = $container->factory(function($c){
            return new \Twig_Loader_Filesystem($_SERVER['DOCUMENT_ROOT'] . '/Templates/ra/templates/');
        });
        $container['twig_debug'] = function($c){
            return new \Twig_Extension_Debug();
        };
        $container['twig'] = $container->factory(function($c){
            return new \Twig_Environment($c['twig_filesystem'], array(
                'cache' => $c['templ_dir'].'cache/',
                'auto_reload' => true,
                'debug' => false,
            ));
        });

        $container['twig_function_percentage'] = function($c){
            return new\Twig_SimpleFunction('percentage_filter', function($numb1, $numb2){
                    $biggest = max($numb1, $numb2);
                    $smallest = min($numb1, $numb2);
                    return abs(number_format((1-$smallest/$biggest)*100,0));
                    $diff = $biggest-$smallest;
                    $average = ($biggest+$smallest)/2;
                    return $diff/$average * 100;
            });
        };
        
        $container['this_url'] = function($c){
            return new \Twig_SimpleFunction('this_url', function(){
                return $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            });
        };


        $container['url'] = function($c){
            return new \Twig_SimpleFunction('url', function(){
                return $_SERVER['REQUEST_URI'];
            });
        };

        $conainer["Twig_Extensions_Text"] = function($c){
            return new \Twig_Extensions_Extension_Text();
        };

        $container['twig'] = $container->extend('twig', function($twig, $c){

            function format_invoice_number($number) {
                return str_pad($number, 10, "0", STR_PAD_LEFT);
            }

            $twig->addExtension(new Twig_Extensions_Extension_Text());
            $twig->addExtension($c['twig_debug']);
            $twig->addFunction($c['twig_function_percentage']);
            $twig->addFunction($c['this_url']);
            $twig->addFunction($c['url']);
            $twig->addFilter("invoice_number", new Twig_Filter_Function("format_invoice_number"));

            $twig->addFunction(new Twig_SimpleFunction("get_items", function($number){
                global $container;
                $gate = $container['gate'];
                return $gate->fak()->getDocumentItems($number);
            }));

            $twig->addFunction(new Twig_SimpleFunction("get_item", function($number){
                global $container;
                $gate = $container['gate'];
                return $gate->fak()->getProduct($number);
            }));
            return $twig;
        });



    }
}
