<?php
/*
#####################################################
#	module name: 	Tab Navigation
#	description:		show only pages defined with toplink = 1
#	directory:			web/admin/modules/navbar/
#	author:				M.Petrov, petrovm@abv.bg
#####################################################
*/
echo '<ul id=nav_bar>';
 
//reading top links menu		  
 $res = mysqli_query("SELECT * FROM pages WHERE SiteID = $SiteID AND toplink = 1 ORDER by sort_n ASC"); 
 $counter = 1;
 while ($top_links_row = mysqli_fetch_object($res)) 
	 {
			$class="";
			
			if($_GET['n'] == $top_links_row->n) $class = "class=cur";
			/*
			if($counter == 1) $class = "class=cur1";
			if($counter == 2) $class = "class=cur2";
			if($counter == 3) $class = "class=cur3";
			*/
			if($counter > 1) $add = "style=\"margin-left: 1px;\"";
			
			echo "<li $class $add><a href='page.php?n=".$top_links_row->n."&SiteID=".$SiteID."' onMouseOver=\"show_hide('submenu_$counter')\">".$top_links_row->Name."</a></li>";
			//read sublinks
			$sub_content="";
			$sub_res = mysqli_query("SELECT * FROM pages WHERE ParentPage = '$top_links_row->n' ORDER by sort_n ASC"); 
				while($sub_links_row = mysqli_fetch_object($sub_res))
					$sub_content.=  "<a href='page.php?n=".$sub_links_row->n."&SiteID=".$SiteID."'>".$sub_links_row->Name."</a>";
			echo "<div id=\"submenu_$counter\" class=\"submenu\" style=\"display: none;\" onMouseOut=\"show_hide('submenu_$counter')\">".$sub_content."</div>";	
	 		
			$counter ++;
	 }
echo "</ul>";
?>
<img src="Templates/ontario3/images/top_line.jpg" vspace="0">