<tr>
		<td height="150" align="left" valign="top" colspan=2 style="background-color: #d6d6d6" width="824" class="header">

    <!-- BANNER SLIDER //-->
    
    <?php
	$tmpl_config = array(
			"default_banner_width" => ($o_site->get_sConfigValue('CMS_MAX_WIDTH'))? $o_site->get_sConfigValue('CMS_MAX_WIDTH') : "520"
			);
	$pBanners = $o_page->get_pBanners();
	if(count($pBanners)>1 && $o_page->orig_n == $o_site->_site['StartPage'])
		{
			
			?>
      		<link rel="stylesheet" href="http://lib.maksoft.net/jquery/nivo-slider/themes/light/light.css" type="text/css" media="screen" />
            <link rel="stylesheet" href="http://lib.maksoft.net/jquery/nivo-slider/nivo-slider.css" type="text/css" media="screen" />
            <div class="slider-wrapper theme-default">
            <div id="banner-slider" class="nivoSlider banner">  
			<?php
                for($i=0; $i<count($pBanners); $i++)
                    print "<a href=\"".$o_page->get_pLink($pBanners[$i]['n'])."\" title=\"".htmlspecialchars($o_page->get_pInfo("title", $pBanners[$i]['n']))."\"><img src=\"http://".$o_site->get_sURL(0)."/img_preview.php?image_file=".$pBanners[$i]['image_src']."&amp;img_width=".$tmpl_config['default_banner_width']."&amp;ratio=strict\" $params alt=\"".htmlspecialchars($o_page->get_pInfo("title", $pBanners[$i]['n']))."\"></a>";
            ?>
            </div>
            </div>
              <script type="text/javascript" src="http://lib.maksoft.net/jquery/nivo-slider/jquery.nivo.slider.js"></script>
			  <script type="text/javascript">
              $j(window).load(function() {
                  $j('#banner-slider').nivoSlider({
						effect: 'sliceDown', // Specify sets like: 'fold,fade,sliceDown'
						slices: 15, // For slice animations
						boxCols: 8, // For box animations
						boxRows: 4, // For box animations
						animSpeed: 500, // Slide transition speed
						pauseTime: 5000, // How long each slide will show
						startSlide: 0, // Set starting Slide (0 index)
						directionNav: false, // Next & Prev navigation
						controlNav: false, // 1,2,3... navigation
						controlNavThumbs: false, // Use thumbnails for Control Nav
						pauseOnHover: false, // Stop animation while hovering
						manualAdvance: false, // Force manual transitions
						prevText: 'Prev', // Prev directionNav text
						nextText: 'Next', // Next directionNav text
						randomStart: false, // Start on a random slide
						beforeChange: function(){}, // Triggers before a slide transition
						afterChange: function(){}, // Triggers after a slide transition
						slideshowEnd: function(){}, // Triggers after all slides have been shown
						lastSlide: function(){}, // Triggers when last slide is shown
						afterLoad: function(){} // Triggers when slider has loaded
				    });
              });
              </script> 
            <?php
		}
		else
		{
	?>
    <div class="banner">
	<?=$o_page->print_pBanner($o_page->n, $tmpl_config['default_banner_width'], "class=\"banner-image\"", $tmpl_config['default_banner'])?>
    </div>
        <?php
		}
		?>

</tr>