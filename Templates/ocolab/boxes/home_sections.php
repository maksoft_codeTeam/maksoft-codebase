<div id="sections">
  <?php
	$home_pages = array(189853, 189854, 189858);
      for($i=0; $i<count($home_pages); $i++)
      {
  ?>
  <div class="box<?=($i==2) ? " last": ""?>">
      <h1 class="box-title"><?=$o_page->print_pName(true, $home_pages[$i])?></h1>
      <div class="box-content">
      <a href="<?=$o_page->get_pLink($home_pages[$i])?>" title="<?=$o_page->get_pTitle("strict", $home_pages[$i])?>"><?=$o_page->print_pImage(292, "", "", $home_pages[$i])?></a>
      <?php if($o_page->n == $o_site->StartPage) echo crop_text($o_page->get_pText($home_pages[$i])); ?>
      <?php if($i<3 || $o_page->n != $o_site->StartPage) { ?>
      <a href="<?=$o_page->get_pLink($home_pages[$i])?>" class="next_link"><?=$o_site->_site['MoreText']?></a>
      <?php } ?>
      </div>
  </div>
  <?php
      }
  ?>
</div>