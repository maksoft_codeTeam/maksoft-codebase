<script type="text/javascript" src="http://lib.maksoft.net/jquery/easyslider/easySlider1.7.js"></script>
<script type="text/javascript">
	$j(document).ready(function(){	
		$j("div.logos").easySlider({
			auto: true,
			controlsShow: false, 
			continuous: true
		});
	
	});	
</script>
<div id="partners">
<span id="prevBtn"><a href="javascript:void(0);" class="slide-arrow left"></a></span>
<div class="logos">
<?php
    $logos = $o_page->get_pSubpages(189852, "RAND()", "p.imageNo >0");
    echo "<ul><li>";
    for($i=0; $i<count($logos); $i++)
        {
            echo "<a href=\"".$o_page->get_pLink($logos[$i]['n'])."\" title=\"".$logos[$i]['title']."\">";
            $o_page->print_pImage(150, NULL, "", $logos[$i]['n']);
            echo "</a>";
            if(($i+1)%5 == 0)
                echo "</li><li>";
        }
	$add_counter = count($logos)%5;
	if($add_counter >0)
	for($i=0; $i<$add_counter; $i++)
		{
			echo "<a href=\"".$o_page->get_pLink($logos[$i+2]['n'])."\" title=\"".$logos[$i+2]['title']."\">";
            $o_page->print_pImage(150, NULL, "", $logos[$i+2]['n']);
            echo "</a>";
		}		
    echo "</li></ul>";
?>
</div>
<span id="nextBtn"><a href="javascript:void(0);" class="slide-arrow right"></a></a>
</div>