<div id="upcomming_events">
<h2 class="event-title"><?=$o_page->get_pTitle('strict', $n_events)?></h2>
<?php
    for($i=0; $i<count($events); $i++)
        {
        ?>
        <div class="event-content">
        <h3><?=$events[$i]['title']?></h3>
        <?=cut_text(strip_tags($events[$i]['textStr']))?>
        <a href="<?=$o_page->get_pLink($events[$i]['n'])?>" class="next_link"><?=$o_site->_site['MoreText']?></a>
        </div>
        <?
        }
?>
</div>