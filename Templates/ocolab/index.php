<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
	<title><?=$Title?></title>
    <?php
		//include template configurations
		include("Templates/ocolab/configure.php");		
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
    <link rel="stylesheet" href="http://lib.maksoft.net/jquery/nivo-slider/themes/light/light.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="http://lib.maksoft.net/jquery/nivo-slider/nivo-slider.css" type="text/css" media="screen" />
    <link href="http://www.maksoft.net/<?=TEMPLATE_DIR?>layout.css" id="layout-style" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/css/admin_classes.css" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/<?=TEMPLATE_DIR?>base_style.css" id="base-style" rel="stylesheet" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Comfortaa:400,300,700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
	<script language="javascript" type="text/javascript" src="<?=TEMPLATE_DIR?>effects.js"></script>
</head>
<?php
	//get site news
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 2");
	//$news = $o_page->get_pSubpages(196536, "p.date_added DESC LIMIT 5");
	
	//get most viewed pages
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5");

	//get last added pages
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5", "p.preview>30");
	
	//get top links
	$top_links = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 5", "p.toplink = 1");
	
	//upcomming events
	$n_events = 199768;
	$events = $o_page->get_pSubpages($n_events, "p.date_added DESC LIMIT 2");
	
	$home_news = $o_page->get_pGroupContent(558);
	if(count($home_news) == 0) $home_news = $news;
?>
<div id="fb-root"></div>
<script>
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/bg_BG/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<body>
<div id="site_container">
    <div id="header">
        	<?php include TEMPLATE_DIR."header.php"?>


    </div>
	<div id="page_container">
        <div class="main-content">
        <?php
			if($o_page->n == $o_site->StartPage)
				include TEMPLATE_DIR."home.php";
			elseif($o_page->_page['SiteID'] == $o_site->_site['SitesID'] )
				include TEMPLATE_DIR."main.php";
			else
				include TEMPLATE_DIR."admin.php";
		?>
		<?php include TEMPLATE_DIR . "boxes/box_partners.php"; ?>
        </div>
    </div>
    
    <div id="footer">
    	<div class="footer-content">
        	<br>
            <div id="navbar"><?=$o_page->print_pNavigation();?></div>
            <div class="copyrights"><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a></div>
        </div> 
    </div>
</div>
</body>
</html>