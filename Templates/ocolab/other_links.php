<?php
	$other_links = $o_page->get_pSubpages($o_page->_page['ParentPage']);
	if(count($other_links) == 0)
		$other_links = $o_page->get_pSubpages($o_site->_site['StartPage']);
?>
<div id="other_links">
<?php
for ($i=0; $i<count($other_links); $i++)
	{
		if($other_links[$i]['n'] != $o_page->n)
			$o_page->print_pName(true, $other_links[$i]['n']);	
	}
?>
</div>
