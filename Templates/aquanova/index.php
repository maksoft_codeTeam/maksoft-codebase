<html><head>
<title><?php echo("$Title");?></title>
<?php
include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/aquanova/");
define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/".DIR_TEMPLATE."images/");
define("DIR_MODULES","web/admin/modules/");

if(!isset($MAX_HEIGHT))
	$MAX_HEIGHT = "100%";
if(!isset($MAIN_WIDTH))
	$MAIN_WIDTH = "550";
if(!isset($MAX_WIDTH))
	$MAX_WIDTH = "950";
if(!isset($MENU_WIDTH))
	$MENU_WIDTH = "400";
if(!isset($PAGE_ALIGN))
	$PAGE_ALIGN = "center";

//load CSS style
if($Site->css!=0 && !empty($Site->cssDef))
	echo $Site->cssDef; 
else echo "<link href='http://www.maksoft.net/css/base_style.css' rel='stylesheet' type='text/css'>";

?>
<!--
THIS LIBRARY IS INCLUDED as global
<script language="JavaScript" type="text/JavaScript" src="Templates/aquanova/lightbox.js"></script>
//-->
<a name="top"></a>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>
<body onLoad="credits('web')">
<div align="center">
<table width="<?=$MAX_WIDTH?>" border="0" align="center" cellpadding="0" cellspacing="0">
<tr><td colspan="2" style="background-image: url(<?=DIR_TEMPLATE_IMAGES?>header_bg.jpg); background-repeat: repeat-x;">
<?php
	include "header.php";
?>
<tr>
<td width="<?=$MENU_WIDTH?>" valign="top">
	<?php include "column_left.php"; ?>
<td valign="top" width="<?=$MAIN_WIDTH?>" rowspan="2" style="background-image: url(<?=DIR_TEMPLATE_IMAGES?>AQN_content_bg.jpg); background-repeat: no-repeat;">
	<?php include "main.php"; ?>
<tr>
  <td valign="bottom">
<?php
	//show only if curent page has not subpages
	$res = mysqli_query("SELECT ParentPage FROM pages WHERE ParentPage = '$n'");
	//if(mysqli_num_rows($res) == 0)
		include "content.php";
?>
<div id="content_small" style="width:330px; height: 50px" align="center">
<?php	include "search.php"; ?>
</div>
<tr><td colspan="2">
	<?php 
		include "footer.php";
	?>
</table>
<div id="credits" style="display:">created by: Maksoft.Net</div>
</div>
</body>
</html>