<div id="home" class="rounded bottom">
    <div id="pageContent">
	<div class="box about">
    	<h2 class="title"><?=$o_page->get_pName($n_about)?></h2>
		<div class="content"><?=cut_text(strip_tags($o_page->get_pText($n_about)), 400)?><br clear="all"><br clear="all"><a href="<?=$o_page->get_pLink($n_about)?>" class="next_link"><?=$o_site->get_sMoreText()?></a></div>
    </div>
	<div class="box products">
    	<div class="titles"><?=$o_page->print_pName(true, $h_titles[0]['n'])?>

<?=$o_page->print_pName(true, $h_titles[1]['n'])?><?=$o_page->print_pName(true, $h_titles[2]['n'])?><?=$o_page->print_pName(true, $h_titles[3]['n'])?></div>
		<div class="photos">	
            <a href="<?=$h_titles[0]['page_link']?>"><img src="<?=DIR_TEMPLATE_IMAGES?>front_photo_01.jpg" width="110px" style="margin:0 0 0 0"></a>
            <a href="<?=$h_titles[1]['page_link']?>"><img src="<?=DIR_TEMPLATE_IMAGES?>front_photo_02.jpg" width="110px"></a>
            <a href="<?=$h_titles[2]['page_link']?>"><img src="<?=DIR_TEMPLATE_IMAGES?>front_photo_03.jpg" width="110px"></a>
            <a href="<?=$h_titles[3]['page_link']?>"><img src="<?=DIR_TEMPLATE_IMAGES?>front_photo_04.jpg" width="110px"></a>
        </div>
    </div>
    <br clear="all"><br clear="all">
    <div class="box banner"><a href="<?=$o_page->get_pLink(19326378)?>"><img src="<?=DIR_TEMPLATE_IMAGES?>banner-atrea.jpg"></a></div>
    <br clear="all">
    <div class="box promo">
    	<h2 class="title"><?=$o_page->get_pName($n_promotions)?></h2>
    	<div class="content">
        	<ul>
        	<?php
            	for($i=0; $i<count($promotions); $i++)
					echo "<li><a href=\"".$promotions[$i]['page_link']."\">".$promotions[$i]['Name']."</a></li>";
			?>
            </ul>
        </div>
    </div>
    <div class="box video">
    	<h2 class="title"><?=$o_page->get_pName($n_video)?></h2>
    	<div class="content">
        	<div class="preview">
            	<?php
					//print_pImage($width=NULL, $params="", $default_src=NULL, $n = NULL)
                	$o_page->print_pImage(200, "", "", $video_pages[0]['n']);
					echo strip_tags(cut_text($video_pages[0]['textStr']));
				?>
            </div>
            <ul>
        	<?php
            	for($i=0; $i<count($video_pages); $i++)
					echo "<li><a href=\"".$video_pages[$i]['page_link']."\">".$video_pages[$i]['Name']."</a></li>";
			?>
            </ul>
            <a href="<?=$o_page->get_pLink($n_video)?>" class="next_link"><?=$o_site->get_sMoreText()?></a>
        </div>
    </div>
  <div class="pcontent">
      &nbsp;
  	<?php
		$o_page->print_pContent();
	?>
  </div>
	<?php
		//$o_page->print_pContent();
		//$o_page->print_pSubContent();
		eval($o_page->get_pPHPcode());
		$o_page->print_pURL();
	?>
    </div>
</div>