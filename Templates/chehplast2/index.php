<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<?php
		if($o_page->_site['language_id'] == 1)  {
			include_once DIR_TEMPLATE."lang/bg.php";
			echo  '<html lang="bg">'; 
		}		
		if($o_page->_site['language_id'] == 2)  {
			include_once DIR_TEMPLATE."lang/en.php";
			echo  '<html lang="en">'; 
		}
		?>
		
<head>
<title><?php echo("$Title");?></title>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<?php
include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/chehplast2/");
define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/Templates/chehplast2/images/");
define("DIR_MODULES","web/admin/modules/");
	
//page about
$p_about = $o_page->get_pLink($n_about);

//news
$news = $o_page->get_pSubpages($o_site->get_sNewsPage());

//promo products
$promotions = $o_page->get_pSubpages($n_promotions);

//video
$video_pages = $o_page->get_pSubpages($n_video);

$bottom_menu = $o_page->get_pSubpages(0, "p.sort_n ASC", "p.toplink = 5");

//home page titles
$h_titles = $o_page->get_pSubpages($n_products, "p.sort_n ASC LIMIT 4");

//page newsletter
$p_newsletter = $o_page->get_page($n_newsletter);

$o_page->print_sConfigurations();

?>
<link href="http://www.maksoft.net/Templates/chehplast2/layout.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/chehplast2/base_style.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/chehplast2/easyslider.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="http://www.maksoft.net/lib/jquery/easyslider/easySlider1.7.js" type="text/javascript"></script>
<script language="JavaScript">
$j(document).ready(function(){
	//promo_box slider
	$j("#banner").easySlider({
		auto: true,
		continuous: true,
		speed: 		500,
		pause:		8000,
		numeric: true
	});
});
</script>
<img src="//as.adwise.bg/servlet/tp.gif?id=1405" width="1px" height="1px" style="display:none;">
</head>
<body>
	<div id="page_container" class="rounded">
		<div id="header" class="rounded_top">
        	<? include DIR_MODULES . "listing/list_languages.php";?>
			
            <div id="icons">
			<a href="<?=$o_page->get_pLink($o_site->get_sStartPage())?>" class="home"></a><a href="<?=$o_page->get_pLink(171435)?>" class="search"></a><a href="<?=$o_page->get_pLink(56868)?>" class="contacts"></a>
            </div>
        	<div id="home_label"><a href="http://<?=$o_site->get_sURL()?>

" target="_blank">www.chehplast.com</a></div>
            <a href="http://www.facebook.com/ChehPlast" target="_blank" id="social_label"></a>
			<?php
			if(($o_site->SiteID)=="859"){
			?>
			<a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>"><img src="<?=DIR_TEMPLATE_IMAGES?>chehplast_logo_en.png" class="logo"></a>
			<?php
		   }else{
		   ?>
		   <a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>"><img src="<?=DIR_TEMPLATE_IMAGES?>chehplast_logo.png" class="logo"></a>
		   <?php
			}
			?>
        </div>
        <div id="banner"><div class="line"></div><div class="line bottom"></div>
            <ul>
              <li><img src="<?=DIR_TEMPLATE_IMAGES?>banner_01.jpg" alt=""></li>
              <li><img src="<?=DIR_TEMPLATE_IMAGES?>banner_02.jpg" alt=""></li>
              <li><img src="<?=DIR_TEMPLATE_IMAGES?>banner_03.jpg" alt=""></li>
              <li><img src="<?=DIR_TEMPLATE_IMAGES?>banner_04.jpg" alt=""></li>
              <li><img src="<?=DIR_TEMPLATE_IMAGES?>banner_05.jpg" alt=""></li>
            </ul>
        </div>
        <div class="content_bg">
        <div id="column_left">
            <img src="<?=DIR_TEMPLATE_IMAGES?>menu_shadow.png" class="menu_shadow">
            <div id="box_search">
            <form action="page.php" method="get">
            	<input type="hidden" name="n" value="171435">
                <input type="hidden" name="SiteID" value="<?=$o_page->SiteID?>">
            	<span class="label">�������:</span>
                <input type="text" name="Search" value="<?=$Search?>">
            </form>
            </div>
            <div id="menu">
            	<?php 
			   		//show only main links
					$skip_menu_item = 1;
					include DIR_MODULES . "menu/menu_standart.php";
				?>
            </div>
            <div id="box_news">
            	<?php include "box_news.php"; ?>
            </div>
        </div>
        <?php
			if($o_page->n == $o_site->get_sStartPage() && strlen($_GET['search_tag'])<3)
				include "home.php";
			else include "main.php";
		?>
        </div>
        <div id="nav_links"><?=$o_page->print_pNavigation()?></div>
	</div>
    <div id="footer" class="rounded">
        <div class="content">
        	<a href="http://www.rehau.bg" target="_blank" title="������� �����"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_rehau.jpg" alt="������� Rehau"></a>
            <a href="http://www.solid55.com" target="_blank"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_solid.jpg" alt=""></a>
            <a href="http://www.etem.bg" target="_blank"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_etem.jpg" alt=""></a>
            <a href="http://www.schueco.com" target="_blank"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_schuco.jpg" alt=""></a>
            <a href="http://www.alumil.com" target="_blank"  title="������� Alumil"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_alumil.jpg" alt="������� ������"></a>
            <a href="http://www.geze.de" target="_blank"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_geze.jpg" alt=""></a>
            <a href="http://www.hormann.bg" target="_blank" title="������� ����� Hormann"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_horman.jpg" alt="�������"></a>
            <br clear="all">
        	<a href="http://www.dorma.com/bg" target="_blank"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_dorma.jpg" alt=""></a>
            <a href="http://www.reynaers.com" target="_blank"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_reynaers.jpg" alt=""></a>
            <a href="http://www.nevapv.cz" target="_blank"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_neva.jpg" alt=""></a>
            <a href="http://www.quanex.com/Products/3/ProductLine/Insulating-Glass-Systems/29/" target="_blank"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_truseal.jpg" alt=""></a>
            <a href="http://www.saav-bg.com" target="_blank" title="������� ����"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_CAAB.jpg" alt="������� ����" height="45"></a>
            <a href="http://www.atrea.cz" target="_blank"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_atrea.jpg" alt=""></a>
            <a href="http://www.bulwindoors.org" target="_blank" title="��������� ��������� ����� � ��������"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_sdrujenie.jpg" alt="��������� ��������� ����� � ��������" height="45"></a>
        </div>
    </div>
    <div class="bottom_menu">
        <div style="float: left; margin: 5px;">
        <script  type="text/javascript">
            <!-- NACHALO NA TYXO.BG BROYACH -->
            d=document;
            d.write('<a href="http://www.tyxo.bg/?86158" title="Tyxo.bg counter" target=" blank"><img width="65" border="0" alt="Tyxo.bg counter"');
            d.write(' src="http://cnt.tyxo.bg/86158?rnd='+Math.round(Math.random()*2147483647));
            d.write('&sp='+screen.width+'x'+screen.height+'&r='+escape(d.referrer)+'" /><\/a>');
            //-->
        </script>
        <noscript><a href="http://www.tyxo.bg/?86158" title="Tyxo.bg counter" target=" blank"><img src="http://cnt.tyxo.bg/86158" width="65" border="0" alt="Tyxo.bg counter" /></a></noscript>
        <!-- KRAI NA TYXO.BG BROYACH -->
        </div>
	<?=$o_site->get_sCopyright()?>, 
    <?php
		for($i=0; $i<count($bottom_menu); $i++)
			{	
				if($i>0) echo " / ";
				$o_page->print_pName(true, $bottom_menu[$i]['n']);
			}
	?>
    <a class="copyrights" href="http://maksoft.net/page.php?n=38146&SiteID=1" target="_blank"></a></div>
</body>
</html>
