<form>
	<div id="search">			
		<div id="search-box1" class="search-box">
			<span id="big">���������� </span>
			<span id="small"> ����������� ���-����� ����</span>
		</div>
		<div id="search-box2" class="search-box">
			<div class="search-box" id="date-box">
				<label>�� ����:</label>
				<input type="text" id="datepicker" size="30">
			</div>
			<div class="search-box" id="rooms-box">
				<label>����:</label>
				<div id="numRooms" class="num-rooms" onclick="$('#roomConfig').toggle(); return false;">
				������...
				</div>
				<div id="roomConfig" style="display:none;">
					<div class="roomsNum">
						<select name="ddRooms" id="ddRooms" class="num-rooms">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
						</select>
						<span style="font-size: 1.3em; font-weight: bold;">����</span>
					</div>
					<div id="rooms">
						<div class="room" id="room1" style="display: block;">
							<h4>� ���� 1</h4>
							<div class="people adults">
								<label>���������:</label>
								<select name="Adults1" id="Adults1" class="num-nights">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (7-12):</label>
								<select name="Child7_12_1" id="Child7_12_1" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (3-6):</label>
								<select name="Child3_6_1" id="Child3_6_1" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people infants">
								<label>������ (0-2):</label>
								<select name="Infants1" id="Infants1" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
						</div>
						<div class="room" id="room2">
							<h4>� ���� 2</h4>
							<div class="people adults">
								<label>���������:</label>
								<select name="Adults2" id="Adults2" class="num-nights">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">

								<label>���� (7-12):</label>
								<select name="Child7_12_2" id="Child7_12_2" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (3-6):</label>
								<select name="Child3_6_2" id="Child3_6_2" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people infants">
								<label>������ (0-2):</label>
								<select name="Infants2" id="Infants2" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
						</div>
						<div class="room" id="room3">
							<h4>� ���� 3</h4>
							<div class="people adults">
								<label>���������:</label>
								<select name="Adults3" id="Adults3" class="num-nights">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (7-12):</label>
								<select name="Child7_12_3" id="Child7_12_3" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (3-6):</label>
								<select name="Child3_6_3" id="Child3_6_3" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people infants">
								<label>������ (0-2):</label>
								<select name="Infants3" id="Infants3" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
						</div>
						<div class="room" id="room4">
							<h4>� ���� 4</h4>
							<div class="people adults">
								<label>���������:</label>
								<select name="Adults4" id="Adults4" class="num-nights">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (7-12):</label>
								<select name="Child7_12_4" id="Child7_12_4" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (3-6):</label>
								<select name="Child3_6_4" id="Child3_6_4" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people infants">
								<label>������ (0-2):</label>
								<select name="Infants4" id="Infants4" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
						</div>
						<div class="room" id="room5">
							<h4>� ���� 5</h4>
							<div class="people adults">
								<label>���������:</label>
								<select name="Adults5" id="Adults5" class="num-nights">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (7-12):</label>
								<select name="Child7_12_5" id="Child7_12_5" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (3-6):</label>
								<select name="Child3_6_5" id="Child3_6_5" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people infants">
								<label>������ (0-2):</label>
								<select name="Infants5" id="Infants5" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
						</div>
						<div class="room" id="room6">
							<h4>� ���� 6</h4>
							<div class="people adults">
								<label>���������:</label>
								<select name="Adults6" id="Adults6" class="num-nights">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (7-12):</label>
								<select name="Child7_12_6" id="Child7_12_6" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (3-6):</label>
								<select name="Child3_6_6" id="Child3_6_6" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people infants">
								<label>������ (0-2):</label>
								<select name="Infants6" id="Infants6" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
						</div>
						<div class="room" id="room7">
							<h4>� ���� 7</h4>
							<div class="people adults">
								<label>���������:</label>
								<select name="Adults7" id="Adults7" class="num-nights">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (7-12):</label>
								<select name="Child7_12_7" id="Child7_12_7" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (3-6):</label>
								<select name="Child3_6_7" id="Child3_6_7" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people infants">
								<label>������ (0-2):</label>
								<select name="Infants7" id="Infants7" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
						</div>
						<div class="room" id="room8">
							<h4>� ���� 8</h4>
							<div class="people adults">
								<label>���������:</label>
								<select name="Adults8" id="Adults8" class="num-nights">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (7-12):</label>
								<select name="Child7_12_8" id="Child7_12_8" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people children">
								<label>���� (3-6):</label>
								<select name="Child3_6_8" id="Child3_6_8" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="people infants">
								<label>������ (0-2):</label>
								<select name="Infants8" id="Infants8" class="num-nights">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
						</div>
					</div>
					<div id="roomsMessage" style="display: none;">
						<h4>�� �� ����������� ������ �� 8 ����, ���� ������� �� �� ������� + 359 2 8 107 800</h4>
					</div>
					<span class="close" title="Close" onclick="$('#roomConfig').hide();">Close</span>
					<span id="btnOK" class="btn" onclick="">
						<span>OK</span>
					</span>
				</div>
			</div>
			<div class="search-box" id="nights-box">
				<label>�������:</label>
				<input name="numNights" type="text" value="1" id="numNights" class="num-nights" />
			</div>
			<div class="search-box" id="promo-box">
				<label>����� ���:</label>
				<input name="promocode" type="text" id="promocode" class="promo" style="margin-right:0;" />
			</div>
			<a id="btnSearch" class="btn" onclick="searchButtonClicked()">�����</a>
		</div>
	</div>
</form>
<script type="text/javascript">

$("#ddRooms").change(function() {
	var roomsNum = $("#ddRooms").val();
	roomNumChanged(roomsNum);
	});

$("#btnOK").click(function(){
	var roomsNum = $("#ddRooms").val();
	roomNumChanged(roomsNum);
	$('#roomConfig').hide();
})

$(document).mouseup(function (e)
{
    var container = $("#roomConfig");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
});

</script>
