<div class="coll credits">
	<div class="coll-content">
    design by <b>pmd</b>studio
	</div>
</div>

<div class="coll links">
	<div class="coll-content h-menu">
    <?php
    	print_menu($bottom_menu);
	?>


	</div>
</div>
<div class="a2a_kit a2a_kit_size_32 a2a_floating_style a2a_default_style" style="bottom:0px; right:0px;">
    <a class="a2a_button_facebook"></a>
    <a class="a2a_button_twitter"></a>
    <a class="a2a_button_google_plus"></a>
    <a class="a2a_button_email"></a>
    <a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</div>

<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
<style type="text/css">
/* Hide AddToAny vertical share bar when screen is less than 980 pixels wide */
@media screen and (max-width: 980px) {
    .a2a_floating_style.a2a_vertical_style { display: none; }
}
</style>