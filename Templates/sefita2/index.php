<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
	<title><?=$Title?></title>
    <?php
		//include template configurations
		include("Templates/sefita2/configure.php");		
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
    <link href="<?=HTTP_SERVER."/".TEMPLATE_DIR?>layout.css" id="layout-style" rel="stylesheet" type="text/css" />
    <link href="http://maksoft.net/css/admin_classes.css" rel="stylesheet" type="text/css" />
    
    <link href="<?=HTTP_SERVER."/".TEMPLATE_DIR?>base_style.css" id="base-style" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="<?=HTTP_SERVER."/".TEMPLATE_DIR?>favicon.ico" type="image/x-icon">
    <script language="javascript" type="text/javascript" src="<?=TEMPLATE_DIR?>effects.js"></script>
    <script type='text/javascript' src='//cdn.jsdelivr.net/jquery.marquee/1.3.1/jquery.marquee.min.js'></script>
							 <script type="text/javascript">
 //proporcional speed counter (for responsive/fluid use)
var widths = $('.marquee').width()
var duration = widths * 7;

$('.marquee').marquee({
    //speed in milliseconds of the marquee
    duration: duration, // for responsive/fluid use
    //duration: 8000, // for fixed container
    //gap in pixels between the tickers
    gap: $('.marquee').width(),
    //time in milliseconds before the marquee will start animating
    delayBeforeStart: 0,
    //'left' or 'right'
    direction: 'left',
    //true or false - should the marquee be duplicated to show an effect of continues flow
    duplicated: true
});
						 </script>
	<?php
		
		if(defined("TMPL_BODY_BACKGROUND") || defined("TMPL_LOGO_POSITION"))
		{
			?>
			<style media="all">
			<?
			//CUSTOM TEMPLATE BACKGROUND
			if(defined("TMPL_BODY_BACKGROUND")) 
			{
			?>
				body{background: url(<?=TMPL_BODY_BACKGROUND?>) 50% 0% fixed !important;}
				#header, #footer {background: transparent !important; box-shadow: none !important;}
				#page_container {padding: 15px 0 !important;}
			<?
			}
			
			//LOGO POSITION
			if(defined("TMPL_LOGO_POSITION"))
			{
				$logo_margin = explode("x", TMPL_LOGO_POSITION);
			?>
				#header .logo {margin: <?=$logo_margin[1]?>px <?=$logo_margin[0]?>px}
			<?
			}
			
			if(defined("CSS_FONT_SIZE"))
			{
			?>
				#pageContent, #pageContent p {font-size: <?=CSS_FONT_SIZE?>em}
			<?
			}
			
			?>
            </style>
			<?
		}
	?>
</head>
<?php
	//get site news
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 6");
	
	//get most viewed pages
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5", "p.n != '".$o_site->_site['StartPage']."'");

	//get last added pages
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5", "p.preview>30");
	
	//main links
	$bottom_menu = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC", "p.toplink != 3");
	
	//home pages from group 557
	$home_pages = $o_page->get_pGroupContent(577);
	
	//main menu links
	$main_menu = $o_page->get_pSubpages(0, "p.sort_n ASC", "p.toplink = 3");
	
	//home news
	//$home_news = $o_page->get_pGroupContent(652, NULL, "ptg.sort_order DESC LIMIT 4");
	$home_news = $o_page->get_pGroupContent(652, NULL, "ptg.sort_order DESC");
?>
<body>
<?php
if(defined('FB_COMMENTS_APP_ID')) {
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/bg_BG/all.js#xfbml=1&appId=<?=FB_COMMENTS_APP_ID?>

";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php
} // end fb
?>

<div id="site_container">
    <div id="header">
        	<?php include TEMPLATE_DIR."header.php"?>
    </div>
    <br clear="all">
	<div id="page_container">

					<marquee>
				<?php
					$page = $o_page->get_page($o_site->get_sNewsPage());
					$string = crop_text(strip_tags($page['textStr']));
					//$string = substr($string,0, strpos($string, "</p>")+4);
						?>
						
								<a href="http://<?=$o_site->_site["primary_url"]."/?n=".$o_site->get_sNewsPage();?>"><?=$o_page->get_pTitle("strict", $o_site->_site['news'])?></a>
						</marquee>
						
			<br>
			<br>
        <div class="main-content">
        <?php
			if($o_page->_page['n'] == $o_site->_site['StartPage'])
				include TEMPLATE_DIR."home.php";
			elseif($o_page->_page['SiteID'] == $o_site->_site['SitesID'] )
				include TEMPLATE_DIR."main.php";
			else
				include TEMPLATE_DIR."admin.php";
		?>
        </div>
    </div>
    <div id="footer">
    	<div class="footer-content" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
		<?php
			include TEMPLATE_DIR . "footer.php";
		?>
        <br clear="all">
        <div class="copyrights"><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a></div>
        </div> 
    </div>
</div>
</body>
</html>