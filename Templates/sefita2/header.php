<div class="header-content" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
    <div class="banner">
    <div id="box_languages">
        <a href="<?=$o_page->get_pLink(179800)?>" title="<?=$o_page->get_pName(179800)?>" style="color: #FFF"><img src="web/images/flag_en.jpg"></a>
        <a href="<?=$o_page->get_pLink(179801)?>" title="<?=$o_page->get_pName(179801)?>" style="color: #FFF"><img src="web/images/flag_fr.jpg"></a>
        <a href="<?=$o_page->get_pLink(180324)?>" title="<?=$o_page->get_pName(180324)?>" style="color: #FFF"><img src="web/images/flag_de.jpg"></a>
    </div>
	<?=$o_page->print_pBanner($o_page->n, $tmpl_config['default_banner_width'], "class=\"banner-image\"", $tmpl_config['default_banner'])?>

</div>
    <div id="top-menu">
		<div class="top-menu-content h-menu">
        <a href="<?=$o_page->get_pLink($o_site->StartPage)?>" title="<?=$o_site->_site['Title']?>" class="home-button"></a>
		<?=print_menu($bottom_menu)?>
        </div>
        <div id="box_search"><div class="box-search-content">
        <form method="get">
            <input type="hidden" name="n" value="<?=$o_page->n?>">
            <input type="hidden" name="SiteID" value="<?=$o_site->SiteID?>">
            <input type="text" class="search_field" placeholder="<?=$o_site->get_sSearchText()?>" name="search">
        </form>
        </div></div>
    </div>
    <div class="h-shadow"></div>
</div>