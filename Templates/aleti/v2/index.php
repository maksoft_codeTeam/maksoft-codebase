<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
	<title><?=$Title?></title>
    <?php
		//include template configurations
		include("Templates/aleti/v2/configure.php");		
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css' />
    <link href="http://www.maksoft.net/Templates/aleti/v2/layout.css" id="layout-style" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/Templates/aleti/v2/base_style.css" id="base-style" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/Templates/aleti/v2/content_styles.css" id="content-style" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/css/admin_classes.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="lib/jquery-ui/themes/base/jquery.ui.all.css">
	<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.core.js"></script>
	<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.widget.js"></script>
	<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.tabs.js"></script>

</head>
<?php
	//get site news
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 5");
	
	//get most viewed pages
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5");

	//get last added pages
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5");
	
	//products
	$n_products = 188606;
	$products = $o_page->get_pSubpages($n_products);
	
	//main links
	$main_links = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 4", "p.toplink=4");
	
	//footer links
	$footer_links = array(188772, 188773, 188774, 188771);
?>
<body>
<div id="site_container">
    <div id="header">
        <div class="header-content" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
        	<?php include TEMPLATE_DIR."header.php"?>
        </div>
    </div>
	<div id="page_container">
        <div class="main-content">
        <?php
			if($o_page->_page['n'] == $o_site->_site['StartPage'])
				include TEMPLATE_DIR."home.php";
			elseif($o_page->_page['SiteID'] == $o_site->_site['SitesID'])
				include TEMPLATE_DIR."main.php";
			else
				include TEMPLATE_DIR."admin.php";
		?>
        </div>
    </div>
    <div id="footer">
    	<div class="footer-content" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
		<?php
			include TEMPLATE_DIR . "footer.php";
		?>
        </div>
        <div class="copyrights"><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a>, web design <a href="http://www.pmd-studio.com" target="_blank" title="��� ������">pmd-studio</a></div>
    </div>
</div>
</body>
</html>