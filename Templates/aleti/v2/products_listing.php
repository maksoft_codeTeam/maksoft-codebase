<?php
	//page template: Products Listing
	$products_all = $o_page->get_pSubpages();
	$count_products = count($products_all);
	$pagination = "";
	$p = $_GET['p'];
	if(!isset($p)) $p = 1;
	$products_per_page = 12;
	$count_pages = round($count_products / $products_per_page);
	//$count_pages++;
	$p_start = $products_per_page*($p-1);
	$p_end = $products_per_page;
	$products = $o_page->get_pSubpages(NULL, "p.sort_n ASC LIMIT $p_start, $p_end");
	for($i=0; $i<$count_pages; $i++)
		$pagination.= "<a href=\"".mk_href_link("p=".($i+1))."\" class=\"".((($i+1)==$p)? "page_selected":"page")."\">".($i+1)."</a>";
?>
<div class="box pagination" id="pagination"><?=$pagination?></div>
<div id="products-listing">
	<?php
		//print tabs
		echo "<ul>";
		for($i=0; $i<count($products); $i++)
			{
				echo "<li><div class=\"product-content\"><a href=\"".$o_page->get_pLink($products[$i]['n'])."\" class=\"fresco\">";
				echo $o_page->print_pImage(170, "", "", $products[$i]['n']);
				echo "<h3>".$o_page->get_pName($products[$i]['n'])."</h3></a><div class=\"info\">".strip_tags($products[$i]['textStr'], "<strong><b><br><p><span>")."</div>";
				echo "</div></li>";
				/*
				if($i%4 == 0 && $i>0)
					echo "<br clear=\"all\">";
				*/
			}
		echo "</ul>";
	?>
    <br clear="all">
</div>
<div class="box pagination" id="pagination"><?=$pagination?></div>
