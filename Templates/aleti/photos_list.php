<script language="JavaScript" type="text/JavaScript" src="Templates/aleti/lightbox.js"></script>
 <?PHP
 /* Shows a number of images sorted by pages */

//default domain
$default_domain_url = "http://www.aleti.eu";

//domain url
if(!isset($domain_url) || $domain_url =='')
	$domain_url = $default_domain_url;

//images directory
$img_dir = $photo_dir;
if(!isset($img_dir) || empty($img_dir) || $img_dir=='')
	$img_dir = 'img';

//change the current dir
//use $cuurent_dir if  use the gallery on a separate domain
$current_dir = getcwd();
	if(isset($domain_dir) && $domain_dir!='' && !empty($domain_dir))
		chdir($domain_dir);
	
//images per row
if(!isset($imgs_per_row) || empty($imgs_per_row) || $imgs_per_row=='') {
   if (!isset($table_cols)) {
	$imgs_per_row = 6;
	} else {
	$imgs_per_row = $table_cols;
	}
}

//images per page
if(!isset($imgs_per_page) || empty($imgs_per_page) || $imgs_per_page=='') {
   	if (!isset($table_rows))  $table_rows = 5;
	//default imgs_per_page = 30
	$imgs_per_page = $imgs_per_row*$table_rows;
	
}

// set to 1 if you want the image filename below the thumb, 0 otherwise
if(!isset($thumb_title) || empty($thumb_title) || $thumb_title=='')
	$thumb_title = 0;  
	
// set the thickness of the border around the thumb, 0 for no border
if(!isset($thumb_border) || empty($thumb_border) || $thumb_border=='')
	$thumb_border = 8; 

// thumbs border color
if(!isset($thumb_border_color) || empty($thumb_border_color) || $thumb_border_color=='')
	$thumb_border_color = '#EBEBEB'; 

if(!isset($read_subdirectories))
	$read_subdirectories = true;


//check for directory existance
if(!is_dir($img_dir))
        {
        	echo ('The image directory does not exist: <b>'.$domain_url.'/'.$img_dir.'</b>');
        }

		
$DIR = @opendir($img_dir) or die('can\'t open image directory: <b>'.$domain_url.'/'.$img_dir.'</b>');

//image thumbnails array
$thumbs = array('');

while(false !== ($thumb = readdir($DIR)))
        {
			if(is_dir($img_dir."/".$thumb) && $thumb !="." && $thumb !=".." && $read_subdirectories)	
				{
					$SUBDIR = @opendir($img_dir."/".$thumb);
					while(false !== ($subthumb = readdir($SUBDIR)))
						if(eregi('jpg|jpeg|gif|tif|bmp|png', $subthumb))
							array_push($thumbs,  $thumb."/".str_replace(' ','%20',$subthumb));
			
				}
				
			if(eregi('jpg|jpeg|gif|tif|bmp|png', $thumb))
					{
					 array_push($thumbs, str_replace(' ','%20',$thumb));
					 //array_push($thumbs, $thumb);
					}

        }

sort($thumbs);

	//page steps
	$step = $_POST['step'];
	if(!isset($step) || $step<0) $step = 0;
	//lock to the last step
	if($step>(count($thumbs)/$imgs_per_page)) $step = $step-1;

	$ai = $step*$imgs_per_page;			//start element
	$an = ($step+1)*$imgs_per_page;	//end element

?>
<TABLE align="center"  cellspacing=5 cellpadding=5 border=0 >
<TR>
		<td colspan="<?=$imgs_per_row?>">
		<TABLE align="center"  cellspacing=0 cellpadding=0 >
				<tr>
					<td width="10" align="center" valign="middle">
						<form method="post">
						<input type="hidden" name="step" value="<?=$step-1?>">
						<input type="submit" value="&laquo;">
						</form>
					<?
					for($i=0; $i<(count($thumbs)/$imgs_per_page); $i++)
						{
						if($step == $i) $value = "[ ".($i+1)." ]";
						else $value = ($i+1);
						echo '<td><form method="post"><input type="hidden" name="step" value="'.$i.'">';
						echo '<input type="submit" value="'.$value.'" class="button_submit"></form>';
						}
					?>
					
					<td>
						<form method="post">
						<input type="hidden" name="step" value="<?=$step+1?>">
						<input type="submit" value="&raquo;">
						</form>
				</tr>
		</table>
<TR>
<?
//imgs_per_row counter
$j=0;

for($i = 1; $i < sizeof($thumbs); $i++)
        {
			if($i>$ai && $i<=$an)
				{
					 if(($j >= $imgs_per_row))
					 		{
								print '<TR>';
								$j=1;					 		
							}
					 else $j++;
							
					print '<TD align="center" class="border_td" valign=middle>';
						
					$thumbs[$i]= htmlspecialchars($thumbs[$i]); 
					
				   print" <table border=0 cellspacing=0 cellpadding=0 height=100% width=100%><tr><td  height=\"155\" align=center><a href=\"".$domain_url."/".$img_dir."/".$thumbs[$i]."\" rel = \"lightbox\">
							<img  src='".$domain_url."/img_preview.php?pic_name=".$thumbs[$i]."&pic_dir=".$img_dir."&img_border=0' border=\"0\" ></a></div>";
							
					 /*
					 print" <table border=0 cellspacing=0 cellpadding=0 height=100% width=100%><tr><td  height=\"155\" align=center><a href=\"".$domain_url."/".$img_dir."/".$thumbs[$i]."\" rel = \"lightbox\">
							<img  src='".$domain_url."/".$img_dir."/".$thumbs[$i]."' border=\"0\" width=150></a></div>";		
					*/			
					if($thumb_title)
						{
							print "<tr><td height=10 align=center class=\"bgcolor_link\"><a href=\"".$domain_url."/".$img_dir."/".$thumbs[$i]."\" rel = \"lightbox\" class=\"bgcolor_link\">".$thumbs[$i]."</a>";
				
						}
					
					print '</table></TD>';
				}
        }
?>
<tr>
<TR>
		<td colspan="<?=$imgs_per_row?>">
		<TABLE align="center"  cellspacing=0 cellpadding=0 border="0">
				  <tr>
					  <td width="10" align="center" valign="middle">
						  <form method="post">
						  <input type="hidden" name="step" value="<?=$step-1?>">
						  <input type="submit" value="&laquo;">
						  </form>
					  <?
					for($i=0; $i<(count($thumbs)/$imgs_per_page); $i++)
						{
						if($step == $i) $value = "[ ".($i+1)." ]";
						else $value = ($i+1);
						echo '<td><form method="post"><input type="hidden" name="step" value="'.$i.'">';
						echo '<input type="submit" value="'.$value.'" class="button_submit"></form>';
						}
					?>
					  
					<td>
						  <form method="post">
						  <input type="hidden" name="step" value="<?=$step+1?>">
						  <input type="submit" value="&raquo;">
						  </form>
				  </tr>
		  </table>
</TABLE>
<?
chdir($current_dir);
?>
