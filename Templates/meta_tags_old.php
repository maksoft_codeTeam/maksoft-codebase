<?php
//including function files
include_once("lib/lib_functions.php"); 
//DB functions
include_once("web/admin/query_set.php");

//Input/Output functions
include_once("web/admin/io.php");

// shortcodes manipulation
include_once("shortcodes/main/shortcodes.php");
if(is_object($wwo)) {
    include_once("shortcodes/wwo/shortcodes.php");
}

if(defined("SHORTCODES_FILE") && (file_exists("SHORTCODES_FILE"))) {
    include_once("SHORTCODES_FILE");
}

$noindex=false; 

// description
$description= $Site->Description;

// keywords

$row=get_page($n);
if(strlen($row->tags)<10) {
	$row->tags=$row->tags.$row->title;
}
 if ($row->tags<>'') {
	$row->tags=str_replace(" \,", "\,",  $row->tags); 
	$keywords = $o_page->replace_shortcodes($row->tags);
} 
/*
else {
$keywords=str_replace(" ,", ",", $Site->keywords);   
} 
*/

// if(strlen($Site->keywords)<10) { $Site->keywords=$Site->title; }

if($Site->site_status>=0) {
	//if($n!=$Site->StartPage) {
		$description = $o_page->get_pText();
		$description = str_replace(array("\n", "\t", "\r"), '', $description);
        $description= htmlspecialchars(strip_tags($row->textStr));
    //}

	if(strlen($search_tag)>5)  {
	  $keywords = iconv("UTF-8","CP1251",$search_tag); 
	  $description = iconv("UTF-8","CP1251",$search_tag); 
	  $taged_pages = $o_site->do_search(iconv("UTF-8","CP1251",$_GET['search_tag']));
	  if(count($taged_pages) > 0) {
	  
		  foreach($taged_pages as $taged_page) {
			//$description .= $taged_page['Name'].",".$taged_page['title'].",";
			
			$keywords = $keywords ." ".$taged_page['Name'].",".$taged_page['title'].",";
		 }
		}
	   else {
	   	$noindex = true; 
	   }
	 
	$description=  strip_tags(str_replace("\"", "", $description)); 
	if(strlen($description)<10) {$description = rtrim("$description $row->tags $Site->Description")." ... "; }
}
	$description .= $o_page->_page['comment_subject']; 
	//if(strlen($description)>160) $description = rtrim(substr($description, 0, 160));  
	if(strlen($description)>180) $description = cut_text($description, 180, "...");

    $description = $o_page->replace_shortcodes($description);

}
$date_expires = new DateTime();
$date_expires->add(new DateInterval('P0DT2H30M5S'));  // cashe the CONTENT for 2 HOURS    date format must be RFC1123

// author
if($o_page->_page['author']>0) {
	$author_q = $o_page->db_query("SELECT * FROM user_authorship WHERE uID = ".$o_page->_page['author']." AND (user_authorship.SiteID = 0 OR  user_authorship.SiteID = '".$o_page->SiteID."') ORDER BY uaID DESC LIMIT 1"); 
	$row_author = mysqli_fetch_object($author_q); 
}
?>
<meta http-equiv="Content-Language" content="<?php echo("$Site->language_key"); ?>" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="refresh" content="10000" />
<meta http-equiv="Expire" content="now" />
<meta name="resource-type" content="document" />
<meta name="copyright" content="Copyright (C) 2004-<?php echo date("Y"); ?>" />
<meta name="webmaster" content="<?php echo("$Template->webmaster"); ?>" />
<meta name="Author" content="<?php echo("$Template->Author"); ?>" />
<meta name="keywords" content="<?php if(strlen($search_tag)>3) {echo iconv("UTF-8","CP1251",$_GET['search_tag']); } else { $Site->keywords=eregi_replace(" ,", ",", $Site->keywords);   $row=get_page($n); if(strlen($row->tags)<10) {$row->tags=$row->tags.$row->title;} if ($row->tags<>'') {$row->tags=eregi_replace(" \,", "\,",  $row->tags); echo("$row->tags"); } else {echo("$Site->Keywords"); }  }?>" />
<meta name="description" content="<?php echo $description ?>" />
<meta name="coverage" content="Worldwide" />
<meta name="language" content="<?php echo("$Site->language_key"); ?>" />
<meta name="geo.region" content="<?php echo("$Site->language_key"); ?>" />
<?php
//echo "<!--".$o_page->_page['page_link']."<>"."http://".$_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI']."//-->"; 
//echo "<!--".$o_page->_site['primary_url']."   ".$_SERVER['SERVER_NAME']."-->"; 
?>
<?php 
if(1==2) 
echo('
<meta name="geo.placename" content="Nioey" />
<meta name="geo.position" content="42.683; 23.317" />
<meta name="ICBM" content="42.683, 23.317" />
'); 
// http://en.wikipedia.org/wiki/Geotagging
?>
<?php if (($row->SecLevel>0) || ($row->SiteID != $SiteID) || !(eregi($o_page->_site['primary_url'],$_SERVER['SERVER_NAME']))   ) {?>
<meta name="distribution" content="iu" />
<meta name="robots" content="none" />
<meta name="robots" content="noindex,nofollow" />
<?php 
	if( !(eregi($o_page->_site['primary_url'],$_SERVER['SERVER_NAME'])) && ($o_page->_site['primary_url']<>'') ) {
		echo('<link rel="canonical" href="http://'.$o_page->_site['primary_url'].'" />'); 
	}
	//echo("<link rel=\"canonical\" href=\"$primary_uri\" >"); 
	$noindex=true;
} else {
	$noindex=false; 
?>
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="robots" content="index,follow,all,noarchive" />
<meta name="revisit-after" content="2 days" />
<meta name="GOOGLEBOT" content="revisit-after 1 day" />
<meta name="classification" content="Regional, Europe, Bulgaria, Business and Economy" />
<link rel="alternate" type="application/rss+xml" title="<?=$Site->title?>" href="http://<?=$o_page->_site['primary_url']?>/rss.php" />

<?php 
}

//if($o_page->get_pLink($o_page->n) <> $_SERVER['SERVER_NAME'].$_SERVER['SERVER_URI']) 
//	if(strlen($o_page->get_pLink($o_page->n))>12)
//	echo('<link rel="canonical" href="'.$o_page->get_pLink($o_page->n).'" />'); 
// ne raboti adekvatno s tag adresi 
?>
<?php 
	echo("<!-- $description ".$o_page->_site['primary_url'].",".$_SERVER['SERVER_NAME']."-->"); 
	if($row->tags<>'') echo("<!-- $row->tags -->"); 
	// <link rel="shortcut icon" href="http://127.0.0.1/stable/images/favicon.ico" />
?>
<script src="http://www.maksoft.net/lib/lib_functions.js" ></script>
<?php
//load ajax library
//AJAX library does not work with the OLD TEXT editor
//&& $row->n != 132
if($row->ParentPage != 11 && $row->n != 111 && $row->n != 132)
{
	
	?>
	<script language="javascript" src="http://www.maksoft.net/lib/lightbox/prototype.js" type="text/javascript"></script>
	<script language="javascript" src="http://www.maksoft.net/lib/lightbox/scriptaculous.js?load=effects" type="text/javascript"></script>
	<script language="javascript" src="http://www.maksoft.net/lib/lightbox/lightbox.js" type="text/javascript"></script>
	<link rel="stylesheet" href="http://www.maksoft.net/lib/lightbox/lightbox.css" type="text/css" media="screen" />
	<?php
	
}

?>
<?php
if($head_script<>"") {
 echo("$head_script"); 
}
?>


<!-- load FLASH library to prevent BORDER arround flash //-->
<script src="http://www.maksoft.net/lib/lib_flash.js" language="javascript" type="text/javascript"></script>
<!-- load jQuery library//-->
<!--<script  src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.0/jquery.js"></script>//-->
<!--<script type="text/javascript" language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>//-->
<script type="text/javascript" language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="http://www.maksoft.net/lib/jquery/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="http://www.maksoft.net/lib/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="lib/jquery/jquery.cookie.js"></script>
<script type="text/javascript" >
	//no conflict with Prototype Lib
	var $j = jQuery.noConflict();
</script>

<link href="http://www.maksoft.net/css/admin_classes.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="http://www.maksoft.net/lib/jquery/fancybox/jquery.fancybox-1.3.4.css" media="screen"/>
<?php if(defined('CMS_IMPORT_CSS')) { ?> <link href="<?=(defined("CMS_IMPORT_CSS"))?>" rel="stylesheet" type="text/css" /> <?php }?>
<!--
<script language="javascript" type="text/javascript">
	$j(document).ready(function(){
			$j('#page_stats').load("http://www.maksoft.net/web/admin/includes/page_stats.php");
		})
</script>
<div id="page_stats"></div>
//-->			


<?php
  //show admin navigation
  
	if($user->AccessLevel>=3 && ($o_page->_page['SiteID'] == $o_site->_site['SitesID'])) include "web/admin/admin_navigation.php";	
	//if(($user->ID==196 || $user->ID==1 || $user->ID==625 || $user->ID==67) && ($o_page->_page['SiteID'] == $o_site->_site['SitesID'])) include "web/admin/admin_navigation.php";		

$r = rand(1,10);   // 1-10 opcia za pokazvane	
if( (($user->ReadLevel==0)) && (($o_page->_site['netservice']<1) || ($o_page->_site['site_status']==1)  || ($r==5)  || ($noindex==true) ) ){
	include("fb/fb_like.php"); 
}
?>
