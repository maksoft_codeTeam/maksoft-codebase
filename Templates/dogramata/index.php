<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><?=$o_page->print_pTitle()?></title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<?php
	
	include("Templates/meta_tags.php"); 
	define ("DIR_TEMPLATE","Templates/dogramata/");
	define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/Templates/dogramata/images/");
	define("DIR_MODULES","web/admin/modules/");
	
	$o_site->print_sConfigurations();
	
	//define pages
	$page_about = $o_page->get_page(117849);
	$page_home = $o_page->get_page($o_site->StartPage);
	$page_contact = $o_page->get_page(117852);
	$page_login = $o_page->get_page(11);
	
	//dograma / distributors / contacts
	$page_01 = $o_page->get_page(118033);
	$page_02 = $o_page->get_page(117853);
	$page_03 = $o_page->get_page(117852);
	
	//top menu
	$top_menu = $o_page->get_pSubpages(0, "sort_n ASC", "p.toplink = 1");
	
?>
<link href="http://www.maksoft.net/Templates/dogramata/style.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/dogramata/contentslider.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/dogramata/jScrollPane.css" rel="stylesheet" type="text/css">

<link href="http://www.maksoft.net/Templates/dogramata/ddsmoothmenu.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/dogramata/ddsmoothmenu-v.css" rel="stylesheet" type="text/css">

<script language="JavaScript" src="lib/jquery/jScrollPane/jScrollPane.js" type="text/javascript"></script>
<script language="JavaScript" src="lib/contentslider/contentslider.js" type="text/javascript"></script>
<script language="JavaScript" src="Templates/dogramata/effects.js" type="text/javascript"></script>
<script src="http://www.maksoft.net/Templates/dogramata/ddsmoothmenu.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "menu2", //menu DIV id
	orientation: 'v', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu-v', //class added to menu's outer DIV
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-27872036-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" class="main_table">
	<tr valign="top">
		<td width="185px" height="122" align="center" valign="middle"><img src="<?=DIR_TEMPLATE_IMAGES?>logo.png" alt="Dogramata.bg" class="png">
		<td width="736px" valign="top"><img src="<?=DIR_TEMPLATE_IMAGES?>logo_text.png" alt="Dogramata.bg" style="float: left; margin: 20px 0 0 0;" class="png">
		<div id="icons">
			<a href="<?=$page_home['page_link']?>" class="icon home"></a>
			<a href="<?=$page_contact['page_link']?>" class="icon contact"></a>
			<a href="<?=$page_login['page_link']?>" class="icon login"></a>
		</div>
        <div id="site_search">
        <form name="search_form" method="get" action="page.php">
        <input name="n" type="hidden" value="141127">
        <input name="SiteID" type="hidden" value="<?=$o_site->SiteID?>">
        <input name="Search" type="text" value="" size="40" onChange="do_search(this.form)">
        <input type="button" value="&raquo;" onclick="do_search(this.form)" class="button_submit">
        </form>
        </div>
		</td>
		<td>
        <img src="<?=DIR_TEMPLATE_IMAGES?>iso_sign.jpg" alt="">
        </td>
	</tr>
	<tr>
		<td width="185px" height="65"></td>
		<td width="736px" class="top_menu" height="65px" valign="top">
		<?php
			for($i=0; $i<count($top_menu); $i++)
				{
					if($top_menu[$i]['n'] == 117854) $class = "special";
					else $class = "";
					echo "<a href=\"".$top_menu[$i]['page_link']."\" title=\"".$top_menu[$i]['Name']."\" class=\"$class\" title=\"".$top_menu[$i]['title']."\">".$top_menu[$i]['Name']."</a>";						
				}
		?>
		</td>
		<td></td>
	</tr>
	<tr>
		<td height="5"></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td width="185" valign="top">
		<?php
			include "column_left.php";
		?>
		</td>
		<td width="736" valign="top" class="pageContent">
		<?php
			if (strlen($search_tag)>2) {
		  	$o_page->print_search($o_site->do_search(iconv("UTF-8","CP1251",$search_tag),8)); 
			} else 
			if($o_page->n == $o_site->StartPage)
				include "home.php";
			else
				include "main.php";
				
/*			$url_etiketi="dogramata.bg";
			include("etiketi.php"); */
		?>
		</td>
		<td width="184" valign="top" rowspan="2">
		<?php
			include "column_right.php";
		?>
		</td>
	</tr>
	<tr>
		<td width="185"></td>
		<td width="736">
		<?php
			include "box_news.php";
		?>
        </td>
	</tr>
	<tr class="footer">
		<td></td>
		<td valign="top">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="250px" align="left" height="40" valign="top"><a href="http://www.dogramata.bg" class="title">www.dogramata.bg</a></td>
				<td width="250px" align="left" height="40" valign="top"><a href="http://www.dogramata.bg" class="title">������� ������</a></td>
				<td width="210px" align="right" height="40" valign="top"><a href="http://www.dogramata.bg" class="title">���������</a></td>
			</tr>
			<tr>
				<td valign="top"><br>
<!-- Web Counter code start -->
<script type="text/javascript" language="javascript">
<!--
	_d=document; _n=navigator; _t=new Date(); function t() { _d.write( "<img src=\"http://counter.search.bg/cgi-bin/c?_id=radj&_z=0&_r="+_r+"&_c="+_c+"&_j="+_j+"&_t="+(_t.getTimezoneOffset())+"&_k="+_k+"&_l"+escape(_d.referrer)+"\" width=70 height=15 "+"border=0>");} _c="0"; _r="0"; _j="U"; _k="U"; _d.cookie="_c=y";
	_d.cookie.length>0?_k="Y":_k="N";
//-->
</script>
<script type="text/javascript" language="javascript1.2">
<!--
	_b=screen; _r=_b.width; _n.appName!="Netscape"?_c=_b.colorDepth : _c=_b.pixelDepth;
	_n.javaEnabled()?_j="Y":_j="N";
//-->
</script>
<a href="http://counter.search.bg/cgi-bin/s?_id=radj" target="_top">
<script type="text/javascript" language="javascript">
<!--
	t();
//-->
</script>
</a>
<!-- Web Counter code end -->                
                
				<!--
					<li><a href="#">������� � Dogramata.bg</a>
					<li><a href="#">�����������</a>
					<li><a href="#">�� ���</a>
				//-->
				</td>
				<td valign="top"><br>
					<!--
					<li><a href="#">�������� ��������� �����������</a>
					<li><a href="#">������ ��������� �����������</a>
					<li><a href="#">JetCredit<sup>TM</sup></a>				
					//-->
				</td>
				<td align="right" valign="top"><br>
					<!--
					<a href="#">��� ������</a><br>
					<a href="#">������� & ��������</a><br>
					<br>
					//-->
					<div class="links"><a href="http://www.facebook.com/profile.php?id=100002386985291" target="blank" title="������� �� Dogramata.bg��� Facebook"><img src="Templates/dogramata/images/icon_facebook.jpg" alt="�������� �� ��� Facebook" border="0"></a><a href="skype:dogramata.bg?chat"><img src="Templates/dogramata/images/icon_skype.jpg" alt="Skype �������" border="0"></a><!--<a href="#"><img src="Templates/dogramata/images/icon_icq.jpg" alt="ICQ �������" border="0"></a>//--></div>				
				</td>
			</tr>			
		</table>
		</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td align="left" class="copyright"><hr>
		<?php
			if($user->AccessLevel > 0)
				echo $o_page->print_pNavigation();
			else echo "Dogramata.bg � ������ �� <a href=\"http://www.alsystems.bg\" title=\"���������\" target=\"_blank\">Alsystems</a> &copy; ".date("Y")." ������ ����� ��������."; 
		?><br><br>
		</td>
		<td></td>
	</tr>	
</table>
</div>
</body>
</html>
