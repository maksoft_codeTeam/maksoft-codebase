<div class="box news">
<div class="content">
	<br>
	<?php
		$news_categories = $o_page->get_pSubpages($o_site->get_sNewsPage());
		for($i=0; $i<count($news_categories); $i++)
			echo ($i==0 ? "":" / ") . "<a href=\"".$news_categories[$i]['page_link']."\" class=\"title".($i==0 ? " selected":"")."\">".$news_categories[$i]['Name']."</a>";
	?>
	<div id="news_scroll_content">
	<br>
	<?php
		$news = $o_page->get_pSubpages($news_categories[0]['n']);
		for($i=0; $i<count($news); $i++)
			echo "<span class=\"date\">".date("d.m.Y", strtotime($news[$i]['date_added']))."</span><br>".cut_text(strip_tags($news[$i]['textStr']), 200)."<br><br>";
	?>
	<br>
	</div>
</div>
</div>