<!-- MAIN-FOOTER-->
<footer class="main-footer">
  <div data-equalizer class="row">
    <div data-equalizer-watch class="small-12 large-3 columns show-for-large-up"> <a href="index.html" class="logo logo--footer vertical-center"> <img src="<?=DIR_TEMPLATE?>assets/img/logo_markela_symbol.svg" alt="Markela Ltd." width="120px">
      <div class="tagline"></div>
      </a> </div>
    <div data-equalizer-watch class="small-12 medium-12 large-9 columns">
      <div class="row">
        <div class="medium-5 columns">
          <div class="foot-widget mt-3">
            <li id="nav_menu-5" class="widget widget_nav_menu">
              <h2 class="widgettitle">������� ������</h2>
              <div class="menu-footer-navigation-container">
                <ul id="menu-footer-navigation" class="menu">

                    
<li>����� UniCredit Bulbank</li>
<li>������� ������ � <span style="color: #DDDDDD">EUR</span></li>
<li>IBAN: <span style="color: #DDDDDD">BG63 UNCR 7000 1519 8597 32</span></li>
<li>BIC: <span style="color: #DDDDDD">UNCRBGSF</span></li>
<li>������� ������ � <span style="color: #DDDDDD">BGN</span></li>
<li>IBAN: <span style="color: #DDDDDD">BG60 UNCR 7000 1519 8608 53</span></li>
<li>BIC: <span style="color: #DDDDDD">UNCRBGSF</span></li>
<li>�� "�������- ������ � ���"</li>
                    
                </ul>
              </div>
            </li>
          </div>
        </div>
        <div class="medium-4 columns">
          <div class="foot-widget mt-3">
            <div class="foot-widget__title">��������</div>
            <ul class="list-vertical navi-list--footer mt-3">
              <li>��. �����</li>
            </ul>
            <ul class="list-vertical navi-list--footer mt-3">
              <li>���: <span style="color: #DDDDDD">+359 2/ 981 64 21</span></li>
              <li>�������: <span style="color: #DDDDDD">+359 878 611 934</span></li>
              <li>Skype: <span style="color: #DDDDDD">markela.com</span></li>
              <li>Email: <a href="mailto:markela@abv.bg">markela@abv.bg</a></li>
            </ul>
          </div>
        </div>
        <div class="medium-3 columns">
          <div class="foot-widget mt-3">
            <div id="subscribe" class="foot-widget__title">��������� ��</div>
            <ul class="social-list list-horizontal mt-3">
              <li><a target="blank" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a target="blank" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
              <li><a target="blank" href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row" id='api'>
        <div class="small-12 columns">
          <div class="copyrights mt-4"> � 2002-<?php echo date("Y"); ?> Markela Ltd. - ������ � ���������: <a href="http://www.maksoft.net">Maksoft.net</a></div>
        </div>
      </div>
    </div>
  </div>
</footer>