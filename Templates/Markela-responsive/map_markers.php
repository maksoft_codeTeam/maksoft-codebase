<?php
$api_key = "AIzaSyCursBrXR7uH_kRJIHfdvTPdHGeGTL8HSg"
?>

<div id="googleMap" style="width:100vw;height:40vh;"></div>
<script type="text/javascript">

function initialize() {

    var markers = [];
    var apartments = [
        ['42.694647', '23.362049','asd','',1],
        ['42.694647', '23.362049','asd','link',2],
        ['42.694665', '23.313056','asd','link',3],
        ['42.699744', '23.336965','asd','link',4],
        ['42.703890', '23.325769','asd','link',5],
        ['42.689893', '23.309065','asd','link',6],
        ['42.687373', '23.326647','asd','link',7],
        ['42.684688', '23.28874','asd','link',8],
        ['42.704650', '23.300323','asd','link',9],
    ];

    var mapProp = {
        center:new google.maps.LatLng(42.690,23.320),
        zoom:13,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

    var infowindow = new google.maps.InfoWindow();

    function drop() {
      clearMarkers();
      for (var i = 0; i < apartments.length; i++) {
        addMarkerWithTimeout(apartments[i], i * 250);
      }
    }
    //var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
    var iconBase = 'http://markela.com/Templates/Markela-responsive/assets/img/';
    var image = { url: iconBase + 'map-pin.png', // image is 512 x 512
                       scaledSize : new google.maps.Size(30, 44)};


    function addMarkerWithTimeout(position, timeout) {
        window.setTimeout(function() {
            marker = new google.maps.Marker({
                  position: new google.maps.LatLng(position[0], position[1]),
                  map: map,
                  animation: google.maps.Animation.DROP,
                  icon: image
            });
            
            google.maps.event.addListener(marker, 'click', (function(marker) {
                return function() {
                  infowindow.setContent(position[2]);
                  infowindow.open(map, marker);
                }
              })(marker, position[3]));

            markers.push();
        }, timeout);
    }

    function clearMarkers() {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(null);
        }
        markers = [];
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    drop();
}


</script>
<script type='text/javascript' src="http://maps.googleapis.com/maps/api/js?key=<?php echo $api_key;?>&callback=initialize"/>


