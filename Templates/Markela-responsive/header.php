<header class="main-header"> 
  <!-- TOP-HEADER-->
  
  <div class="top-head">
    <div class="row">
      <div class="small-12 medium-12 large-3 columns"> <a href="index.html" class="logo vertical-center"> <img src="<?=DIR_TEMPLATE?>assets/img/logo_markela.svg" alt="Markela" width="100px" height="auto"> </a> <a id="js-trigger-navi" href="#" class="navi-open right vertical-center hide-for-large-up"><span
                        class="ic-hamburger"></span></a> </div>
      <div class="large-9 columns show-for-large-up">
        <div class="vertical-center">
          <nav class="navigation-wrapper clearfix">
            <div class="menu-secondary-menu-container">

              <ul id="menu-secondary-menu" class="navigation-secondary right clearfix">

                <?php
							if(!isset($top_links)) $top_links = 1;
	                        $top_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $top_links");                        
                            for($i=0; $i<count($top_links); $i++)
							{
								$subpages = $o_page->get_pSubpages($top_links[$i]['n']);
								?>
                <li class="menu-item"> <a href="<?=$o_page->get_pLink($top_links[$i]['n'])?>">
                  <?=$top_links[$i]['Name']?>
                  <?=((count($subpages) >0) ? "" : "")?>
                  </a></li>
                <? 	}

				?>
<li class="menu-item">
          
<form class="form-wrapper cf" method="get">
  	<input type="text" id="search" name="search" placeholder="������� � �����.." required>
	  <button type="submit"><div class="searchicon"><i class="fa fa-search" aria-hidden="true"></i></div></button>
      <input name="n" type="hidden" value="<?=$o_page->n?>">
		<input name="SiteID" type="hidden" value="<?=$o_site->SiteID?>">
</form>
</li>
  
              </ul>
            </div>
          </nav>
          <nav class="navigation-wrapper clearfix">
            <div class="menu-main-menu-container">
              <ul id="menu-main-menu" class="navigation-primary np-dropdown hoverline-effect right clearfix">
                <?php
							if(!isset($dd_links)) $dd_links = 2;
	                        $dd_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $dd_links");                        
                            for($i=0; $i<count($dd_links); $i++)
							{
								$subpages = $o_page->get_pSubpages($dd_links[$i]['n']);
								?>
                <li class="menu-item"> <a href="<?=$o_page->get_pLink($dd_links[$i]['n'])?>">
                  <?=$dd_links[$i]['Name']?>
                  <?=((count($subpages) >0) ? "" : "")?>
                  </a>
                  <?php	
										 	if(count($subpages) > 0)
												{
													?>
                  <ul class="sub-menu">
                    <?php
														for($j=0; $j<count($subpages); $j++)
															echo "<li class=\"menu-item\"><a href=\"".$o_page->get_pLink($subpages[$j]['n'])."\">".$subpages[$j]['Name']."</a></li>";	
													?>
                  </ul>
                </li>
                <?	
												}	
										 ?>
                <? 	}

				?>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <!-- TABLET & MOBILE Navigation-->
  <div class="side-navigation hide-for-large-up overflow-hidden">
    <ul class="lang lang--side clearfix">
      <li> <a href="http://markela.eu/">EN</a> </li>
      <li class="active"> <a href="#">BG</a> </li>
    </ul>
    <a id="js-close-navi" href="#" class="navi-close"><span class="ic-close"></span></a>
    <div class="menu-main-menu-container">
      <ul id="menu-main-menu-1" class="navigation-primary np-side clearfix">
        <?php
							if(!isset($resp_links)) $resp_links = 2;
	                        $resp_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $resp_links");                        
                            for($i=0; $i<count($resp_links); $i++)
							{
								$subpages = $o_page->get_pSubpages($resp_links[$i]['n']);
								?>
        <li class="menu-item menu-item-has-children"> <a href="<?=$o_page->get_pLink($resp_links[$i]['n'])?>">
          <?=$resp_links[$i]['Name']?>
          <?=((count($subpages) >0) ? "" : "")?>
          </a>
          <?php	
										 	if(count($subpages) > 0)
												{
													?>
          <ul class="sub-menu">
            <?php
														for($j=0; $j<count($subpages); $j++)
															echo "<li class=\"menu-item\"><a href=\"".$o_page->get_pLink($subpages[$j]['n'])."\">".$subpages[$j]['Name']."</a></li>";	
													?>
          </ul>
        </li>
        <?	
												}	
										 ?>
        <? 	}

				?>
      </ul>
    </div>
    <div class="menu-secondary-menu-container">
      <ul id="menu-secondary-menu-1" class="navigation-secondary ns-side clearfix">
                      <?php
							if(!isset($top_links2)) $top_links2 = 1;
	                        $top_links2 = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $top_links2");                        
                            for($i=0; $i<count($top_links2); $i++)
							{
								$subpages = $o_page->get_pSubpages($top_links2[$i]['n']);
								?>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-274"> <a href="<?=$o_page->get_pLink($top_links2[$i]['n'])?>">
                  <?=$top_links2[$i]['Name']?>
                  <?=((count($subpages) >0) ? "" : "")?>
                  </a></li>
                <? 	}

				?>
<!--        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-274"><a href="special-offers/index.html">�� ���</a></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-553"><a href="about-us/index.html">��������</a></li>-->
        <li>
        <form class="form-wrapper cf" method="get">
  	<input type="text" id="search" name="search" placeholder="������� � �����.." required>
	  <button type="submit"><div class="searchicon"><i class="fa fa-search" aria-hidden="true"></i></div></button>
      <input name="n" type="hidden" value="<?=$o_page->n?>">
		<input name="SiteID" type="hidden" value="<?=$o_site->SiteID?>">
</form>
        </li>
      </ul>
    </div>
  </div>
  
  <!-- TOP-BAR-->
  <div class="top-bar show-for-large-up">
    <div class="row">
      <div class="medium-12 columns clearfix">
        <ul class="list-horizontal vertical-center left">
          <?php $wwo->get_weather(); ?>
          <li><span style="opacity: 0.5;">��. �����,</span> ���: <?php echo date('H:m', time())?></li>
          <li>�������:
            <?=$wwo->weather->current_condition->temp_C?>
            C /
            <?=$wwo->weather->current_condition->temp_F?>
            F</li>
        </ul>
                                <ul class="lang vertical-center right clearfix">
          <li> <a href="http://markela.eu/">EN</a> </li>
          <li class="active"> <a href="#">BG</a> </li>
        </ul>
        <ul class="list-horizontal vertical-center right">
          <li>+359 878 611 934</li>
        </ul>
      </div>
    </div>
    </div>
</header>
