<!DOCTYPE html>
<html lang="bg-BG">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">

<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title><?php echo("$Title");?></title>

<?php
include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/Markela-responsive/");
define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/Templates/Markela-responsive/images/");
define("DIR_TEMPLATE","web/admin/modules/");

//set the language
switch($lng)
{
	case "EN":
		{
			include DIR_TEMPLATE."english.php";
		}
	case "BG":
	default:
		{
			include DIR_TEMPLATE."bulgarian.php";
		}
}

function head_title($title)
{
	$first_word= explode(" ", $title);
	$page_name = str_replace($first_word[0], "<b>".$first_word[0]."</b>", $title);
	return $page_name;
}

function change_title($title)
{
	$first_word= explode(" ", $title);
	if(strlen($first_word[0]) > 3)
		$page_name = str_replace($first_word[0], "<b>".$first_word[0]."</b><br>", $title);
	else $page_name = $title;
	return $page_name;
}

$api_key = "AIzaSyCursBrXR7uH_kRJIHfdvTPdHGeGTL8HSg"

?>

<link rel='stylesheet'  href='<?=DIR_TEMPLATE?>assets/css/style.css' type='text/css' media='all' />
<link rel='stylesheet'  href='<?=DIR_TEMPLATE?>assets/css/front_custom.css' type='text/css' media='all' />
<link rel='stylesheet'  href='<?=DIR_TEMPLATE?>assets/css/searchbar.css' type='text/css' media='all' />
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/jquery/jquery.js'></script>
<!--<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/jquery/jquery-migrate.min.js'></script>-->
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/vendor/datepicker/datepicker.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var calendar = {"January":"January","February":"February","March":"March","April":"April","May":"May","June":"June","July":"July","August":"August","September":"September","October":"October","November":"November","December":"December","Jan":"Jan","Feb":"Feb","Mar":"Mar","Apr":"Apr","Jun":"Jun","Jul":"Jul","Aug":"Aug","Sep":"Sep","Oct":"Oct","Nov":"Nov","Dec":"Dec","Person":"Person","Persons":"Persons"};
/* ]]> */
</script>
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/reservator.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var cssTarget = "img.";
/* ]]> */
</script>
    <script type="text/javascript">

        var font_families = ['Lora:400,400italic,700,700italic:latin,latin-ext,cyrillic','Playfair Display:400,400italic,700,700italic,900,900italic:latin,latin-ext,cyrillic']

    </script>
    <script type="text/javascript" src="<?=DIR_TEMPLATE?>assets/js/jssor.slider.mini.js"></script>
</head>
<body>

<?php
$n_ourrooms = 3275;
	include DIR_TEMPLATE . "header.php";
	
	        if(strlen($search_tag)>2)
			{
				$o_page->print_search($o_site->do_search(iconv("UTF-8","CP1251",$search_tag), 10)); 
				$keyword = $search_tag; 
				include("selected_sites.php"); 
			}
		//enable site search
		elseif(strlen($search)>2)
		  
			$o_page->print_search($o_site->do_search($search, 10)); 
			
		else
			{	
	if($o_page->_page['SiteID'] != $o_site->_site['SitesID'] )
		include DIR_TEMPLATE."admin.php";	
				elseif($o_page->n == $o_site->get_sStartPage())
					include_once "home.php";
				else
					include_once "main.php";
			}
   
 /*?>	// Page Template
	$pTemplate = $o_page->get_pTemplate();
	if($o_page->_page['SiteID'] != $o_site->_site['SitesID'] )
		include DIR_TEMPLATE."admin.php";	
	elseif($pTemplate['pt_url'])
		include $pTemplate['pt_url'];
	elseif($o_page->_page['n'] == $o_site->_site['StartPage'] && strlen($search_tag)<3 && strlen($search)<3)
		include DIR_TEMPLATE . "home.php";
	else
		include DIR_TEMPLATE . "main.php";<?php */
		
	include DIR_TEMPLATE . "footer.php";
	
?>

<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/vendor/font-loader/webfontloader.js'></script> 
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/vendor/bxslider/jquery.bxslider.min.js'></script> 
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/vendor/foundation/foundation.min.js'></script> 
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/vendor/countTo/jquery.countTo.js'></script> 
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/vendor/swipebox/jquery.swipebox.min.js'></script> 
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/vendor/waypoints/jquery.waypoints.min.js'></script> 
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/vendor/shares/shares.min.js'></script> 
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/vendor/gray/jquery.gray.min.js'></script> 
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/main.js'></script>
<script type='text/javascript' src='<?=DIR_TEMPLATE?>assets/js/map_markers.js'></script>
<script type='text/javascript' src="http://maps.googleapis.com/maps/api/js?key=<?php echo $api_key;?>&callback=initialize"/></script>
<script>
	//no conflict with Prototype Lib
	var $ = jQuery.noConflict();
	</script>

</body>
</html>
