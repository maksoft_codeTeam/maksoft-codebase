<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo("$Title"); ?></title>

    <?php include "config.php"; ?>

    <!-- Bootstrap Core CSS -->
    <link href="http://www.maksoft.net/<?=DIR_TEMPLATE?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="http://www.maksoft.net/<?=DIR_TEMPLATE?>assets/css/style.css" rel="stylesheet">
    <link href="http://www.maksoft.net/<?=DIR_TEMPLATE?>assets/css/blue.css" rel="stylesheet">

    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    
        <?php
			if(defined("TMPL_BODY_BACKGROUND")) 
			{
			if(filesize("TMPL_BODY_BACKGROUND")<5000)
             $b_params = " 50% 0% fixed !important ";
            else
			 $b_params = "  no-repeat center center fixed ";
			?>
            			<style>
            body{background: url(<?=TMPL_BODY_BACKGROUND?>) <?=$b_params?>;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                }
				</style>
			<?php
			}
			?>
    
</head>
<body>

    <?php include "inc/navbar.php"; ?>
    
    <!-- Page Content -->
    <div class="container border-bg">
      <div class="all-content">
        <div class="row">
            <div class="col-md-12">
            
            <?php include "header.php"; ?>
            
            </div>
            
            <div class="padding-bottom">
            <div class="col-md-3">
            
            <?php include "column_left.php"; ?>
                
            </div>

            <div class="col-md-9">
            
            <?php include "content.php"; ?>
                    
            </div>
                    
            </div>
            
            </div>
            
<div class="col-md-3">
</div>
<div class="col-md-9">
<ol class="breadcrumb">
  <?=$o_page->print_pNavigation()?>
</ol>
</div>

        </div>

    </div>
    <!-- /.container -->
        
    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p><?php include_once("Templates/copyrights_links.php"); ?></p>
                </div>
            </div>
        </footer>
            <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" 
  role="button" title="����� �� ������" data-toggle="tooltip" data-placement="top">
  <i class="fa fa-chevron-up" aria-hidden="true"></i>
</a>
    </div>
    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="http://www.maksoft.net/<?=DIR_TEMPLATE?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="http://www.maksoft.net/<?=DIR_TEMPLATE?>assets/js/bootstrap.min.js"></script>
    
    <!-- ������������ ��������� -->
        <script type="text/javascript">
    $(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');
});
</script>

<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/bg_BG/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php 
if(isset($tyxo) && $tyxo != ""){
	echo '<script  type="text/javascript">
<!--
d=document;
d.write(\'<a href="http://www.tyxo.bg/?'.$tyxo.'" title="'.$Title.'" target=" blank"><img width="1" height="1" border="0" alt="'.$Title.'"\');
d.write(\' src="http://cnt.tyxo.bg/'.$tyxo.'?rnd=\'+Math.round(Math.random()*2147483647));
d.write(\'&sp=\'+screen.width+\'x\'+screen.height+\'&r=\'+escape(d.referrer)+\'" /><\/a>\');
//-->
</script>
<noscript><a href="http://www.tyxo.bg/?'.$tyxo.'" title="'.$Title.'" target="_blank"><img src="http://cnt.tyxo.bg/'.$tyxo.'" width="1" height="1" border="0" alt="'.$Title.'"/></a></noscript>
	';
}

?>

<?php 
if (strlen($_uacct)>0) {
 echo("
	<script src=\"http://www.google-analytics.com/urchin.js\" type=\"text/javascript\">
	</script>
	<script type=\"text/javascript\">
	_uacct = \"$_uacct\";
	 urchinTracker();
	</script>
 "); 
}
?>

</body>
</html>