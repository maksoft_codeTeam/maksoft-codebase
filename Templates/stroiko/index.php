<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title><?php echo("$Title");?></title>
<?php
include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/stroiko/");
define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/Templates/stroiko/images/");
define("DIR_MODULES","web/admin/modules/");

//top menu buttons
$top_menu = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC", "p.toplink=1");

//bottom menu buttons
$bottom_menu = $o_page->get_pSubpages(0, "p.sort_n ASC", "p.toplink=5");

//catalog links - limit last 2 subpages in catalog

if(!isset($n_catalog))
	$n_catalog = 167812;
$catalog_links = $o_page->get_pSubpages($n_catalog, "p.sort_n DESC LIMIT 2");
$catalog_links_n = array($catalog_links[0]['show_group_id'], $catalog_links[1]['show_group_id']);

//get important news
$news_listing = $o_page->get_pSubpages($o_site->_site['News'], "p.sort_n ASC", "p.toplink=1");

//letter dictionary
$n_letter_dictionary = 172954;

$site_banner = "banner_bg.jpg";
if($o_site->SiteID == 462)
	$site_banner = "banner_en.jpg";
	
//define some texts
/*
define("TITLE_HOME_LABEL1", "�������� �� �� 1993 �.");
define("TITLE_HOME_LABEL2", "38-�� ���������<b>21-27 ����, 2012 �.</b>");
*/

?>
<link rel="shortcut icon" href="http://www.maksoft.net/Templates/stroiko/images/favicon.ico" type="image/x-icon">
<link href="http://www.maksoft.net/Templates/stroiko/layout.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/stroiko/base_style.css" rel="stylesheet" type="text/css">
<link href="http://www.maksoft.net/Templates/stroiko/content_styles.css" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="http://www.maksoft.net/Templates/stroiko/ie8-and-down.css">
<![endif]-->
<script>
$j(document).ready(function(){
	$j('#top_menu a').hover(
	function(){
		$j('#top_menu').css("border-top", "10px solid #00603a");
		},
	function(){
		$j('#top_menu').css("border-top", "10px solid #007446");
		})	
		
	//resize top_menu buttons 
	$j('#top_menu a:nth-child(1)').css("width", "89px");
	$j('#top_menu a:nth-child(2)').css("width", "142px");
	$j('#top_menu a:nth-child(3)').css("width", "90px");
	$j('#top_menu a:nth-child(4)').css("width", "98px");
	$j('#top_menu a:nth-child(5)').css("width", "195px");
	$j('#top_menu a:nth-child(6)').css("width", "110px");
	$j('#top_menu a:nth-child(7)').css("width", "90px");
	$j('#top_menu a:nth-child(8)').css("width", "102px");
	$j('#top_menu a:nth-child(9)').css("width", "68px");
	})
</script>
</head>
<body>
	<div id="page_container">
		<div id="header"><a href="<?=$o_page->get_pLink($o_site->get_sStartPage())?>"><img src="<?=DIR_TEMPLATE_IMAGES . $site_banner?>"></a></div>
        <div id="top_menu">
        	<?php
            	for($i=0; $i<count($top_menu); $i++)
					echo "<a href=\"".$top_menu[$i]['page_link']."\" title=\"".$top_menu[$i]['title']."\"><p>".$top_menu[$i]['Name']."</p></a>";

				$s_versions = $o_site->get_sVersions();
				for($i=0; $i<count($s_versions); $i++)
					//echo "<a href=\"".$s_versions[$i]['version_link']."\" class=\"language\"><img src=\"web/images/flag_big_".strtolower($s_versions[$i]['version_key'])."\" width=\"28px\"></a>";
					//echo "<a href=\"http://".$o_site->get_sInfo("primary_url", $s_versions[$i]['verSiteID'])."/".$o_page->get_pLink($s_versions[$i]['n'], $s_versions[$i]['verSiteID'])."\" class=\"language\"><img src=\"web/images/flag_big_".strtolower($s_versions[$i]['version_key'])."\" width=\"28px\"></a>";
					echo "<a href=\"".$o_page->get_pLink($s_versions[$i]['n'], $s_versions[$i]['verSiteID'])."\" class=\"language\"><img src=\"web/images/flag_big_".strtolower($s_versions[$i]['version_key'])."\" width=\"28px\"></a>";
			?>


        </div>
        <?php
        	if($o_page->_page['SiteID'] == 1)
				include "admin.php";
			else
			if($o_page->n == $o_site->get_sStartPage())
				include "home.php";
			else include "main.php";
		?>
        <br clear="all">
        <div id="nav_links"><?php if($user->AccessLevel > 0) $o_page->print_pNavigation(); ?></div>
	</div>
    <div id="footer">
    	<div class="content">
        	<div class="bottom_menu">
            	<?php
                	for($i=0; $i<count($bottom_menu); $i++)
						{
							if($i>0) echo " | ";
							$o_page->print_pName(true, $bottom_menu[$i]['n']); 
						}
				?>
            </div>
        </div>
        <!--<?=$o_site->get_sCopyright()?><a class="copyrights" href="http://maksoft.net/page.php?n=38146&SiteID=1" target="_blank"></a>//-->
       	<?=TEXT_FOOTER_ADDRESS?>
    </div>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-42091672-1', 'stroiko.eu');
  ga('send', 'pageview');
 </script>
</html>