<div id="image_gallery">
<?php
	//get subpages
	$gallery = $o_page->get_pSubpages($o_page->n, "p.sort_n ASC", "p.imageNo > 0 ");
	for($i=0; $i < count($gallery); $i++)
		{
		?>
			<div class="preview">
            <?php
            	$img_title = $gallery[$i]['title'];
				if(strlen($gallery[$i]['textStr']) > 3)
					$img_title.= "<p>".strip_tags(crop_text($gallery[$i]['textStr']))."</p>";
			?>
            <a href="<?=$gallery[$i]['image_src']?>" rel="lightbox[image_gallery]" title="<?=htmlspecialchars($img_title)?>"><img src="img_preview.php?image_file=<?=$gallery[$i]['image_src']?>&amp;img_width=160&amp;ratio=strict"></a>
            <?php
				if($user->WriteLevel >= $o_page->_page['SecLevel'])
					echo "<br clear=\"all\"><a href=\"".$o_page->get_pLink(132)."&no=".$gallery[$i]['n']."\">редактирай</a>"; 
			?>
            </div>
		<?php
			if(($i+1)%4 == 0)
				echo "<br clear=\"all\">";		
		}
	//print_r($gallery);
?>
<br clear="all">
</div>
