<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title><?php echo("$Title");?></title>
<?php
include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/bg-cleaning/");
define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/Templates/bg-cleaning/images/");
define("DIR_MODULES","web/admin/modules/");

if(!isset($MAX_HEIGHT))
	$MAX_HEIGHT = "100%";
if(!isset($MAIN_WIDTH))
	$MAIN_WIDTH = "750";
if(!isset($MAX_WIDTH))
	$MAX_WIDTH = "750";
if(!isset($MENU_WIDTH))
	$MENU_WIDTH = "0";
if(!isset($PAGE_ALIGN))
	$PAGE_ALIGN = "center";

//set the language
switch($lng)
{
	case "EN":
		{
			include DIR_TEMPLATE."english.php";
		}
	case "BG":
	default:
		{
			include DIR_TEMPLATE."bulgarian.php";
		}
}

//load CSS style
/*
if($Site->css!=0 && !empty($Site->cssDef))
	echo $Site->cssDef; 
else echo "<link href='http://www.maksoft.net/css/bg-cleaning/base_style.css' rel='stylesheet' type='text/css'>";
*/
echo "<link href='http://www.maksoft.net/css/bg-cleaning/base_style.css' rel='stylesheet' type='text/css'>";
?>
</head>
<body onLoad="credits('web')">
<div align="<?=$PAGE_ALIGN?>">
	<div id="header" style="width:<?=$MAIN_WIDTH?>" align="center">
		<div class="t1">&nbsp;</div>
		<div class="t2">&nbsp;</div>
		<div class="t3" align="right" style="padding: 2px; padding-right: 20px;">
		<?php
			
			$v_query_str = "SELECT * FROM Sites, versions WHERE Sites.SitesID = '$SiteID' AND Sites.SitesID = versions.SiteID"; 
	
			$versions_query = mysqli_query("$v_query_str"); 
			$i = 0;
			while ($ver = mysqli_fetch_object($versions_query)) 
			{
				if($i>0) echo " | ";
				$i++;
			 echo("<a href=\"/page.php?n=$ver->n&SiteID=$ver->verSiteID\">$ver->version</a>"); 
			
			}
			echo " | <a href=\"page.php?n=$Site->StartPage&SiteID=$SiteID\">$Site->Home</a>";
		?>


		</div>
	<?php
					// including header file
					include DIR_TEMPLATE."/header.php";
					?>
	</div>
	<div id="main" valign="top">
	<?php
		if($row->n == $Site->StartPage || $row->SiteID != $Site->SitesID)
			include "home.php";
		else include "main.php";
	?>
	</div>
	
	<div id=nav_bar>
			<?php
			if($user->AccessLevel >0)
				echo $nav_bar;
			?>
	</div>
	<div id="footer" align="center">
	<?
		include DIR_TEMPLATE."footer.php";
	?>
	</div>
</div>
</body>
</html>
