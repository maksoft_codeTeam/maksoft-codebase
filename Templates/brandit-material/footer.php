<!-- FOOTER -->
<footer id="footer">
	<div class="container">

		<div class="row">

			<div class="col-md-8">

				<!-- Footer Logo -->
				<img class="footer-logo footer-2" src="<?=TEMPLATE_DIR?>/assets/img/brandit_logo.png" width="210px" alt=""/>

				<!-- Small Description -->
				<p><?=$o_site->_site['Description']?></p>
				
<div class="row">

					<div class="col-md-12 col-sm-12 hidden-xs-down">

						<!-- Social Icons -->
						<div class="float-right clearfix">

							<p class="mb-10">Follow Us</p>
							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-facebook float-left" data-toggle="tooltip" data-placement="top" title="Facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>
						

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-twitter float-left" data-toggle="tooltip" data-placement="top" title="Twitter">
								<i class="icon-twitter"></i>
								<i class="icon-twitter"></i>
							</a>
						

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-gplus float-left" data-toggle="tooltip" data-placement="top" title="Google plus">
								<i class="icon-gplus"></i>
								<i class="icon-gplus"></i>
							</a>
						

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-linkedin float-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
								<i class="icon-linkedin"></i>
								<i class="icon-linkedin"></i>
							</a>
						

							<a href="/rss.php" class="social-icon social-icon-sm social-icon-transparent social-rss float-left" data-toggle="tooltip" data-placement="top" title="Rss">
								<i class="icon-rss"></i>
								<i class="icon-rss"></i>
							</a>
						

						</div>
						<!-- /Social Icons -->

					</div>

				</div>

				<hr/>

						<h5><i class="icon-envelope"></i> CONTACT INFO</h5>
						<ul class="list-unstyled">
							<div class="row">
								<div class="col-md-4">
								<li><b><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i></b> 1000 Sofia, Bulgaria, <br /> 150 Vassil Levski Blvd.</li>
								</div>
								<div class="col-md-4">
								<li><b><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i></b> +359 2 846 46 46, +359 888 846 006</li>
								</div>
								<div class="col-md-4">
								<li><b><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i></b> <a href="mailto:info@maksoft.net">info@maksoft.net</a></li>
								</div>
							</div>
						</ul>

			</div>

			<div class="col-md-4">

				<h4 class="letter-spacing-1">CONTACT US</h4>

				<!-- CONTACT MESSAGES -->
				<p id="alert_success" class="alert alert-success alert-mini">Message sent! Thank You!</p>
				<p id="alert_failed" class="alert alert-danger alert-mini">Message not sent!</p>
				<p id="alert_mandatory" class="alert alert-danger alert-mini">Please, complete all mandatory fields</p>

                            <?php if($sent == 1) { ?>
                            <div class="alert alert-success margin-bottom-30"><!-- SUCCESS -->
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">�</span><span class="sr-only">�������</span></button>
								<strong>���������� ��!</strong> ����������� �� � ���������!
							</div>
							<?php } ?>

							<form method="post" action="/form-cms.php?<?php echo("n=$n&SiteID=$SiteID"); ?>">				
												<input type="text" value="" name="Name" class="form-control required" id="Name" placeholder="Name*" required>
												<input type="text" value="" name="Phone" class="form-control required" id="Phone" placeholder="Phone*" required>
												<input type="text" value="" data-msg-email="Please enter a valid email address." name="EMail" class="form-control required" style="width: 100%;" id="EMail" placeholder="Email*" required>
												<textarea maxlength="5000" placeholder="Message*" rows="3" class="form-control required" name="Zapitvane" id="Zapitvane" required></textarea>
								<?php
                                include("web/forms/form_captcha.php");
                                ?>

                               <input type="submit" value="Submit message" class="btn btn-info">
        <input type="hidden" name="action" value="contact_send" />                        
        <input name="ConfText" type="hidden" id="ConfText" value="����������� �� � ����������!"> 
        <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
		<input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
        <input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$ToMail"); ?>"> 
        <input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$n&SiteID=$SiteID&sent=1"); ?>">
        <input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
		<input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">
        <div class="verify-hidden">
            <input name="verify" type="text">
        </div>
							</form>
<style>
.verify-hidden {
display: none;
}
</style>

			</div>

		</div>

	</div>

	<div class="copyright">
		<div class="container">
			<ul class="list-inline inline-links mobile-block float-right m-0">
			<?php
			if ( !isset( $footer_links ) )$footer_links = 5;
			$footer_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $footer_links" );

			for ( $i = 0; $i < count( $footer_links ); $i++ ) {
				$subpages = $o_page->get_pSubpages( $footer_links[ $i ][ 'n' ] );
				?>
			<li>
				<a href="<?php echo $o_page->get_pLink($footer_links[$i]['n']) ?>">
					<?php echo $o_page->get_pName($footer_links[$i]['n']) ?>
				</a>
			</li>

			<?php  
			}
			?>
			</ul>

			&copy; All Rights Reserved, BrandIt
		</div>
	</div>
</footer>
<!-- /FOOTER -->

</div>
<!-- /wrapper -->


<!-- SCROLL TO TOP -->
<a href = "#" id = "toTop"> </a>


<!-- PRELOADER -->
<div id="preloader">
	<div class="inner">
		<div class="spinner">
		  <div class="cube1"></div>
		  <div class="cube2"></div>
		</div>
	</div>
</div> <!-- /PRELOADER -->


<!-- JAVASCRIPT FILES -->
<script type="text/javascript">
	var plugin_path = '/global/smarty/assets/plugins/';
</script>
<script type="text/javascript" src="/global/smarty/assets/plugins/jquery/jquery-3.2.1.min.js"></script>

<script type="text/javascript" src="/global/smarty/assets/js/scripts.js"></script>

<?php include( "Templates/footer_inc.php" ); ?>
</body>
</html>