<!DOCTYPE html>
<html lang="<?php echo($Site->language_key); ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251"/>
<title><?=$Title?></title>

<!-- mobile settings -->
<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<?php
require_once "lib/lib_page.php";
require_once "lib/Database.class.php";
//include meta tags
include( "Templates/meta_tags.php" );
$o_site->print_sConfigurations();
$o_site = new site();
$db = new Database();
$o_page->setDatabase( $db );
$o_user = new user( $user->username, $user->pass );
?>

<!-- WEB FONTS : use %7C instead of | (pipe) -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

<!-- CORE CSS -->
<link href="/global/smarty/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

<!--MATERIAL DESIGN-->
<link href="/global/smarty/assets/plugins/mdl/material.indigo-blue.min.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!-- REVOLUTION SLIDER -->
<link href="/global/smarty/assets/plugins/slider.revolution.v5/css/pack.css" rel="stylesheet" type="text/css" />

<!-- THEME CSS -->
<link href="/global/smarty/assets/css/essentials.css" rel="stylesheet" type="text/css" />
<link href="/global/smarty/assets/css/layout.css" rel="stylesheet" type="text/css" />
<link href="<?=TEMPLATE_DIR?>assets/css/custom.css" rel="stylesheet" type="text/css" />

<!-- PAGE LEVEL SCRIPTS -->
<link href="/global/smarty/assets/css/header-1.css" rel="stylesheet" type="text/css" />
<link href="/global/smarty/assets/css/color_scheme/blue.css" rel="stylesheet" type="text/css" id="color_scheme" />
</head>
<body class="smoothscroll enable-animation enable-materialdesign">
		<!-- SLIDE TOP -->
		<div id="slidetop">

			<div class="container">
				
				<div class="row">

					<div class="col-md-4">
						<h6><i class="icon-heart"></i> WHY BRANDIT?</h6>
						<p><?=$o_site->_site['Description']?></p>
					</div>

					<div class="col-md-4">
						<h6><i class="fa-facheck"></i> RECENTLY VISITED</h6>
						<ul class="list-unstyled">
							<li><a href="#"><i class="fa fa-angle-right"></i> Consectetur adipiscing elit amet</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> This is a very long text, very very very very very very very very very very very very </a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Lorem ipsum dolor sit amet</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Dolor sit amet,consectetur adipiscing elit amet</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Consectetur adipiscing elit amet,consectetur adipiscing elit</a></li>
						</ul>
					</div>

					<div class="col-md-4">
						<h6><i class="icon-envelope"></i> CONTACT INFO</h6>
						<ul class="list-unstyled">
							<li><b>Address:</b> 1000 Sofia, Bulgaria, <br /> 150 Vassil Levski Blvd.</li>
							<li><b>Phone:</b> +359 2 846 46 46, +359 888 846 006</li>
							<li><b>Email:</b> <a href="mailto:info@maksoft.net">info@maksoft.net</a></li>
						</ul>
					</div>

				</div>

			</div>

			<a class="slidetop-toggle" href="#"><!-- toggle button --></a>

		</div>
		<!-- /SLIDE TOP -->

		<!-- wrapper -->
		<div id="wrapper">
			
		<div id="topBar">
				<div class="container">

					<!-- right -->
					<ul class="top-links list-inline float-right">
						<!--<li><a class="no-text-underline" href="#"><i class="fa fa-user hidden-xs-down"></i> Contacts</a></li>-->
						<?php
						if ( !isset( $topbar_links ) )$topbar_links = 5;
						$topbar_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $topbar_links" );

						for ( $i = 0; $i < count( $topbar_links ); $i++ ) {
							$subpages = $o_page->get_pSubpages( $topbar_links[ $i ][ 'n' ] );
							?>
						<li>
							<a href="<?php echo $o_page->get_pLink($topbar_links[$i]['n']) ?>" class="no-text-underline">
								<?php echo $o_page->get_pName($topbar_links[$i]['n']) ?>
							</a>
						</li>
						<?php  
						}
						?>
					</ul>

					<!-- left -->
					<ul class="top-links list-inline">
						<li>
							<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><img class="flag-lang" src="/global/smarty/assets/images/_smarty/flags/us.png" width="16" height="11" alt="lang"> ENGLISH</a>
							<ul class="dropdown-langs dropdown-menu">
								<li><a tabindex="-1" href="#"><img class="flag-lang" src="/global/smarty/assets/images/_smarty/flags/us.png" width="16" height="11" alt="lang"> ENGLISH</a></li>
								<li class="divider"></li>
								<li><a tabindex="-1" href="#"><img class="flag-lang" src="/global/smarty/assets/images/_smarty/flags/bg.png" width="16" height="11" alt="lang"> Български</a></li>
								<li><a tabindex="-1" href="#"><img class="flag-lang" src="/global/smarty/assets/images/_smarty/flags/de.png" width="16" height="11" alt="lang"> German</a></li>
							</ul>
						</li>
					</ul>

				</div>
			</div>

			<div id="header" class="navbar-toggleable-md clearfix sticky dark b-0">

				<!-- TOP NAV -->
				<header id="topNav">
					<div class="container">

						<!-- Mobile Menu Button -->
						<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
							<i class="fa fa-bars"></i>
						</button>


						<!-- Logo -->
						<a class="logo float-left" href="<?php echo $o_page->get_pLink($o_site->_site['StartPage']) ?>">
							<img src="<?=TEMPLATE_DIR?>assets/img/brandit_logo.png" alt="BrandIt" />
						</a>

						<div class="navbar-collapse collapse float-right nav-main-collapse submenu-color">
							<nav class="nav-main">

								<ul id="topMain" class="nav nav-pills nav-main">
									
									<?php
									if ( !isset( $menu_links ) )$menu_links = 2;
									$menu_links = $o_page->get_pSubpages( 0, "p.sort_n", "p.toplink = $menu_links" );

									for ( $i = 0; $i < count( $menu_links ); $i++ ) {
										$subpages = $o_page->get_pSubpages( $menu_links[ $i ][ 'n' ] );
										?>
									<li <?php echo ((count($subpages)> 0) ? "class=\"dropdown nav-animate-fadeIn nav-hover-animate hover-animate-bounceIn \"" : "") ?>>
									<a class="dropdown-toggle noicon" href="<?php echo $o_page->get_pLink($menu_links[$i]['n']) ?>" <?php echo ((count($subpages)> 0) ? "" : "") ?>>
										<?php echo $menu_links[$i]['Name'] ?></a>
									<?php echo ((count($subpages) > 0) ? "<ul class=\"dropdown-menu \">" : "</li>") ?>
									<?php
									if ( count( $subpages ) > 0 ) {


										for ( $j = 0; $j < count( $subpages ); $j++ )
											echo "<li><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\">" . $subpages[ $j ][ 'Name' ] . "</a></li>";
										?>
									</ul>
									</li>
										<?php
									}
									}
									?>
								</ul>

							</nav>
						</div>

					</div>
				</header>
				<!-- /Top Nav -->

			</div>