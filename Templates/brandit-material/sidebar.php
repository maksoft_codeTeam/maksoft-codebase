<div class="col-md-3 col-sm-3">
<div class="side-nav mdl-shadow--4dp mb-60">

	<div id="explore-slideto" class="side-nav-head">
		<button class="fa fa-bars"></button>
		<h4 class="mt-0">PRODUCTS</h4>
	</div>

	<ul class="list-group list-group-bordered list-group-noicon uppercase">
	<?php
	$products = $o_page->get_pSubpages( 169287, "p.sort_n" );

	for ( $i = 0; $i < count( $products ); $i++ ) {
		$subpages = $o_page->get_pSubpages( $products[ $i ][ 'n' ] );
		?>
	<li class="list-group-item">
		<a class="dropdown-toggle cats-title" href="<?php echo $o_page->get_pLink($products[$i]['n']) ?>">
			<?php echo $products[$i]['Name'] ?>
		</a>
		<?php echo ((count($subpages) > 0) ? "<ul>" : "</li>") ?>
		<?php
		if ( count( $subpages ) > 0 ) {
			for ( $j = 0; $j < count( $subpages ); $j++ )
				echo "<li><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\">" . $subpages[ $j ][ 'Name' ] . "</a></li>";
			?>
		</ul>
	</li>
	<?php
	}
	}
	?>
	</ul>
	
</div>

<!-- BANNER ROTATOR -->
<!--<div class="owl-carousel buttons-autohide controlls-over mb-60 text-center" data-plugin-options='{"singleItem": true, "autoPlay": 4000, "navigation": true, "pagination": false, "transitionStyle":"goDown"}'>
	<?php 
		$n_topproducts = "3255";
		$topproducts = $o_page->get_pGroupContent($n_topproducts);

			for($i=0; $i<count($topproducts); $i++)
				{
					?>
 
	<a href="<?=$o_page->get_pLink($topproducts[$i]['n'])?>">
		<img class="img-fluid" src="/img_preview.php?image_file=<?=$topproducts[$i]['image_src']?>&img_width=270px" width="270" height="350" alt="<?=$o_page->get_pName($topproducts[$i]['n'])?>" />
	</a>
	
	<?							
	}
	?>
</div>-->
<!-- /BANNER ROTATOR -->

<div class="side-nav mdl-shadow--4dp">
	<div id="explore-slideto" class="side-nav-head">
		<button class="fa fa-bars"></button>
		<h4 class="mt-0">CATALOGUES</h4>
	</div>

	<ul class="list-group list-unstyled">
	<?php
	$catalogues = $o_page->get_pSubpages( 161043, "p.sort_n" );

	for ( $i = 0; $i < count( $catalogues ); $i++ ) {
		$subpages = $o_page->get_pSubpages( $catalogues[ $i ][ 'n' ] );
		?>
	<li class="list-group-item">
		<a href="<?php echo $o_page->get_pLink($catalogues[$i]['n']) ?>">
			<?php echo $catalogues[$i]['Name'] ?>
		</a>
	</li>
	<?php
	}
	?>
	</ul>
</div>
</div>