			<!-- WELCOME -->
			<section class="home-page">
				<div class="container">

					<div class="row">

						<div class="col-sm-6">
							<header class="mb-40">
								<h1 class="fw-300">Welcome to BrandIt</h1>
								<h2 class="fw-300 letter-spacing-1 fs-13"><span>Your brand communication</span></h2>
								<p><?php $o_page->print_pContent(); ?></p>
							</header>

<div class="side-nav mdl-shadow--4dp">

	<div id="explore-slideto" class="side-nav-head">
		<button class="fa fa-bars"></button>
		<h4 class="mt-0">PRODUCTS</h4>
	</div>

	<ul class="list-group list-group-bordered list-group-noicon uppercase">
	<?php
	$products = $o_page->get_pSubpages( 169287, "p.sort_n" );

	for ( $i = 0; $i < count( $products ); $i++ ) {
		$subpages = $o_page->get_pSubpages( $products[ $i ][ 'n' ] );
		?>
	<li class="list-group-item">
		<a class="dropdown-toggle cats-title" href="<?php echo $o_page->get_pLink($products[$i]['n']) ?>">
			<?php echo $products[$i]['Name'] ?>
		</a>
		<?php echo ((count($subpages) > 0) ? "<ul>" : "</li>") ?>
		<?php
		if ( count( $subpages ) > 0 ) {
			for ( $j = 0; $j < count( $subpages ); $j++ )
				echo "<li><a href=\"" . $o_page->get_pLink( $subpages[ $j ][ 'n' ] ) . "\">" . $subpages[ $j ][ 'Name' ] . "</a></li>";
			?>
		</ul>
	</li>
	<?php
	}
	}
	?>
	</ul>
	
</div>

						</div>

						<div class="col-sm-6">

							<!-- 
								controlls-over		= navigation buttons over the image 
								buttons-autohide 	= navigation buttons visible on mouse hover only
								
								data-plugin-options:
									"singleItem": true
									"autoPlay": true (or ms. eg: 4000)
									"navigation": true
									"pagination": true
									"transitionStyle":"fadeUp" (fade,backSlide,goDown,fadeUp)
							-->
							<div class="owl-carousel buttons-autohide controlls-over" data-plugin-options='{"singleItem": true, "autoPlay": 3400, "navigation": true, "pagination": true, "transitionStyle":"fadeUp"}'>
									<?php 
										$n_topproducts = "273";
										$topproducts = $o_page->get_pGroupContent($n_topproducts);

											for($i=0; $i<count($topproducts); $i++)
												{
													?>

										<div><a href="<?=$o_page->get_pLink($topproducts[$i]['n'])?>">
											<img class="img-fluid" src="/img_preview.php?image_file=<?=$topproducts[$i]['image_src']?>&img_width=200px" alt="<?=$o_page->get_pName($topproducts[$i]['n'])?>" />
										</a></div>              

									<?							
									}
									?>
							</div>
							
							<hr />

							<!-- 
								controlls-over		= navigation buttons over the image 
								buttons-autohide 	= navigation buttons visible on mouse hover only
								
								data-plugin-options:
									"singleItem": true
									"autoPlay": true (or ms. eg: 4000)
									"navigation": true
									"pagination": true
							-->
							<div class="text-center mt-30">
								<div class="owl-carousel m-0" data-plugin-options='{"navigation": false, "pagination": false, "singleItem": false, "autoPlay": 3400, "items":"3"}'>
									<?php 
										$n_topproducts = "3255";
										$topproducts = $o_page->get_pGroupContent($n_topproducts);

											for($i=0; $i<count($topproducts); $i++)
												{
													?>

										<div style="padding:0 10px;"><a href="<?=$o_page->get_pLink($topproducts[$i]['n'])?>">
											<img class="img-fluid" src="/img_preview.php?image_file=<?=$topproducts[$i]['image_src']?>&img_width=200px" alt="<?=$o_page->get_pName($topproducts[$i]['n'])?>" />
										</a></div>              

									<?							
									}
									?>
									
								</div>
							</div>

						</div>

					</div>
				</div>
			</section>
			<!-- /WELCOME -->