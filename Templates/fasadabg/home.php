<?php
	$h_pages = array(162343, 162344, 162345, 162346);
?>
<img src="<?=TEMPLATE_IMAGES?>banner.jpg">
<div id="home">
	<div id="column_left">
    	<div class="info-box b1">
            <img src="<?=TEMPLATE_IMAGES?>label01.jpg">
            <h2><a href="<?=$o_page->get_pLink($h_pages[0])?>" title="<?=$o_page->get_pTitle("strict", $h_pages[0])?>"><?=$o_page->get_pTitle("strict", $h_pages[0])?></a></h2>
            <a class="link-more" href="<?=$o_page->get_pLink($h_pages[0])?>"><?=$o_site->get_sMoreText()?></a>
        </div>
        <div class="info-box b2">
        	<img src="<?=TEMPLATE_IMAGES?>

label02.jpg">
            <h2><a href="<?=$o_page->get_pLink($h_pages[1])?>" title="<?=$o_page->get_pTitle("strict", $h_pages[1])?>"><?=$o_page->get_pTitle("strict", $h_pages[1])?></a></h2>
            <a class="link-more" href="<?=$o_page->get_pLink($h_pages[1])?>"><?=$o_site->get_sMoreText()?></a>
        </div>
    </div>
    <div id="pageContent">
    	<div id="gallery">
        	<?php
            	//get random page

				$rc = rand(0, count($h_gallery)-1);
			?>
            <div class="numbering">
            <?php
				for($i=0; $i<count($h_gallery); $i++)
					echo "<a href=\"#\" title=\"".$h_gallery[$i]['title']."\" rel=\"".$h_gallery[$i]['image_src']."\"></a>";
			?>
            </div>
        	<div class="preview"><h3 class="title"><?=$h_gallery[$rc]['Name']?></h3><?=$o_page->print_pImage(450, "id=\"gallery_preview\"", "", $h_gallery[$rc]['n']);?></div>
            <div class="thumbs">
            <?php
				
            	for($i=0; $i<count($h_gallery); $i++)
					{
						echo "<a href=\"".$o_page->get_pLink($h_gallery[$i]['n'])."\" title=\"".$o_page->get_pTitle("strict", $h_gallery[$i]['n'])."\">";
						$o_page->print_pImage(110, "rel=\"".$h_gallery[$i]['image_src']."\"", "", $h_gallery[$i]['n'], "strict");
						echo "</a>";
					}
			?>
            </div>
        </div>
    </div>
	<div id="column_right">
    	<div class="info-box b3">
        	<img src="<?=TEMPLATE_IMAGES?>label03.jpg">
            <h2><a href="<?=$o_page->get_pLink($h_pages[2])?>" title="<?=$o_page->get_pTitle("strict", $h_pages[2])?>"><?=$o_page->get_pTitle("strict", $h_pages[2])?></a></h2>
        	<a class="link-more" href="<?=$o_page->get_pLink($h_pages[2])?>"><?=$o_site->get_sMoreText()?></a>
        </div>
        <div class="info-box b4">
        	<img src="<?=TEMPLATE_IMAGES?>label04.jpg">
            <h2><a href="<?=$o_page->get_pLink($h_pages[3])?>" title="<?=$o_page->get_pTitle("strict", $h_pages[3])?>"><?=$o_page->get_pTitle("strict", $h_pages[3])?></a></h2>
        	<a class="link-more" href="<?=$o_page->get_pLink($h_pages[3])?>"><?=$o_site->get_sMoreText()?></a>
        </div>
    </div>
    <br clear="all">
	<?php include TEMPLATE_DIR . "boxes/bottom_boxes.php"; ?>
</div>