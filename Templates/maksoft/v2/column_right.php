			<!-- Sidebar -->

				<div class="about">
					<h4 class="sidebar-heading">ABOUT THE BLOG</h4>
					<p>Lorem ipsum dolor sit amet, consectetr 
					adipiscing elit. Praesent vitae mi sem. In id justo eu lectus pharetra.Morbi mauris tortor, vulputate vel luctus nec, non urna.</p>
				</div>
				<hr>
				<div class="top-categories">
					<h4 class="sidebar-heading clearfix">TOP CATEGORIES</h4>
					<a href="#">
						<span class="icon-cat"><i class="icon-briefcase"></i></span>
						<span class="text">Neque ( 5 )</span>
						<i class="icon-right-open-mini right-open"></i>
					</a>
					<hr>
					<a href="#">
						<span class="icon-cat color2"><i class="icon-globe"></i></span>
						<span class="text">Vitae ( 11 )</span>
						<i class="icon-right-open-mini right-open"></i>
					</a>
					<hr>
					<a href="#">
						<span class="icon-cat color3"><i class="icon-brush"></i></span>
						<span class="text">Comodo ( 2 )</span>
						<i class="icon-right-open-mini right-open"></i>
					</a>
					<hr>
					<a href="#">
						<span class="icon-cat color4"><i class="icon-thumbs-up"></i></span>
						<span class="text">Egget ( 8 )</span>
						<i class="icon-right-open-mini right-open"></i>
					</a>
					<hr>
				</div>
				<form class="search" method="post" action="#" role="search">
			       	<input type="text" class="search-input" placeholder="Search on site...">
			      	<button type="submit">Search</button>
			    </form>
			    <hr>
			    <div class="tags">
			    	<h4 class="sidebar-heading">POPULAR TAGS</h4>
			    	<ul class="clearfix">
			    		<li><a href="#" class="tag">Web-Design</a></li>
			    		<li><a href="#" class="tag">Coding HTML</a></li>
			    		<li><a href="#" class="tag">Pretium</a></li>
			    		<li><a href="#" class="tag">Photography</a></li>
			    		<li><a href="#" class="tag">Scelerisque</a></li>
			    		<li><a href="#" class="tag">Timing</a></li>
			    	</ul>
			    </div>
			    <hr>
			    <div class="recent-post">
			    	<h4 class="sidebar-heading">RECENT POST</h4>
			    	<div class="media">
						<a href="#" class="pull-left">
							<img src="assets/img/blog/post-img-1.jpg" alt="img" class="media-object img-responsive">
						</a>
						<div class="media-body">
							<p>Sed ut perspicitias undeomnis iste doloream quiavoluptate magna metus. <a href="#" class="icon-right-open-big"></a></p>
						</div>
					</div>
					<hr>
					<div class="media">
						<a href="#" class="pull-left">
							<img src="assets/img/blog/post-img-2.jpg" alt="img" class="media-object img-responsive">
						</a>
						<div class="media-body">
							<p>Sed ut perspicitias undeomnis iste doloream quiavoluptate magna metus. <a href="#" class="icon-right-open-big"></a></p>
						</div>
					</div>
					<hr>
					<div class="media">
						<a href="#" class="pull-left">
							<img src="assets/img/blog/post-img-3.jpg" alt="img" class="media-object img-responsive">
						</a>
						<div class="media-body">
							<p>Sed ut perspicitias undeomnis iste doloream quiavoluptate magna metus. <a href="#" class="icon-right-open-big"></a></p>
						</div>
					</div>
					<hr>
					<div class="media">
						<a href="#" class="pull-left">
							<img src="assets/img/blog/post-img-4.jpg" alt="img" class="media-object img-responsive">
						</a>
						<div class="media-body">
							<p>Sed ut perspicitias undeomnis iste doloream quiavoluptate magna metus. <a href="#" class="icon-right-open-big"></a></p>
						</div>
					</div>
					<hr>
					<div class="media">
						<a href="#" class="pull-left">
							<img src="assets/img/blog/post-img-5.jpg" alt="img" class="media-object img-responsive">
						</a>
						<div class="media-body">
							<p>Sed ut perspicitias undeomnis iste doloream quiavoluptate magna metus. <a href="#" class="icon-right-open-big"></a></p>
						</div>
					</div>
					<hr>
			    </div>