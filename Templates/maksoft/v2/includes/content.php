<?php
	//initialize some site contents
	
	//top menu
	//$top_menu = $o_page->get_pSubpages($o_site->StartPage);
	//$top_menu = $o_page->get_pGroupContent(776);
    if(!isset($drop_links)) $drop_links = 2;
    $top_menu = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $drop_links");
	
	//home services
	$services = array(
		 $o_page->get_page(170211),
		 $o_page->get_page(38146),
		 $o_page->get_page(41315),
		 $o_page->get_page(173116)
	);
	
	//home news
	$home_news = $o_page->get_pSubpages($o_site->_site['news'], "p.sort_n DESC LIMIT 4");
	
	//home portfolio
	$portfolio =  array(
		 $o_page->get_page(267),
		 //$o_page->get_page(38146),
		 //$o_page->get_page(41315),
		 //$o_page->get_page(173116)
	);
	
	//admin top_menu
	$admin_top_menu = $o_page->get_pSubpages(0, "p.date_added DESC", "p.toplink = 1");
	
	//admin contents
	
	$admin_sections = array(
		array('title' => "Administration", 'pages' => array(111,131,145,146,154,177,185,213)),
		array('title' => "Management", 'pages' => array(304,305,312,683,934,1889,6031,144)),
		array('title' => "Configuration", 'pages' => array(22258,26225,40879,42560,63931,85869,127362,139268))
		
	);
?>
