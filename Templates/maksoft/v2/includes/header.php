<!DOCTYPE html>
<html  lang="<?php echo($Site->language_key); ?>">
<head>
  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async='async'></script>
  <script>
    var OneSignal = window.OneSignal || [];
      OneSignal.push(["init", {
      appId: "e4e8de5a-4255-454f-a1dc-a2924092a2ee",
      autoRegister: true, /* Set to true to automatically prompt visitors */
      subdomainName: 'https://maksoft.onesignal.com',   
      notifyButton: {
          enable: false /* Set to false to hide */
      }
    }]);
  </script>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title>
<?=$o_page->get_pTitle()?>
</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Cache-control" content="public">
<!-- FONTS -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel='shortcut icon' type='image/x-icon' href='<?=TEMPLATE_PATH?>assets/img/favicon.ico' />
<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,500&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

<!-- CSS -->
<link rel="stylesheet" href="<?=TEMPLATE_PATH?>assets/css/bootstrap.min.css" Cache-Control:"max-age=3153600">
<link rel="stylesheet" href="<?=TEMPLATE_PATH?>assets/css/style.css">
<link rel="stylesheet" href="<?=TEMPLATE_PATH?>assets/css/print.css">
<link rel="stylesheet" href="<?=TEMPLATE_PATH?>assets/css/style-red.css">
<link rel="stylesheet" href="<?=TEMPLATE_PATH?>assets/fullcalendar/fullcalendar.css">
<link rel="stylesheet" href="<?=TEMPLATE_PATH?>assets/fullcalendar/scheduler.css">
<link rel="stylesheet" href="<?=TEMPLATE_PATH?>assets/fullcalendar/fullcalendar.print.css" media="print">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_PATH?>assets/rs-plugin/css/settings.css" media="screen" />
<!-- jQuery -->

<script type="text/javascript" src="<?=TEMPLATE_PATH?>assets/js/jquery.js"></script>
<script async type="text/javascript" src="<?=TEMPLATE_PATH?>assets/js/modernizr.custom.js"></script>
<?php include "Templates/meta_tags.php";?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css">
<!--    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">-->

<!-- build:js modernizr.touch.js -->
<script async src="<?=TEMPLATE_PATH?>includes/floating/dist/lib/modernizr.touch.js"></script>
<!-- endbuild -->

<!-- build:css mfb.css -->
<link href="<?=TEMPLATE_PATH?>includes/floating/dist/mfb.css" rel="stylesheet">
<!-- endbuild -->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- Login form -->
<link rel="stylesheet" href="<?=TEMPLATE_PATH?>assets/login/css/style.css" />
<script async src="<?=TEMPLATE_PATH?>assets/login/js/login.js"></script>
</head>
<body>
<div class="container print">
  <div class="col-sm-6"> <img src="<?=TEMPLATE_IMAGES?>logo-standart.png" alt="Maksoft.net"> </div>
</div>
<?php
	if($user->AccessLevel > 0 )
	{
?>
<style>
.navbar-brand img {max-width: 45px !important;}
</style>
<!-- Navigation -->
<div id="header_nav" class="nav-container">
  <nav class="navbar  navbar-default container navigation" role="navigation"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="<?=$o_page->get_pLink(11);?>"><img src="<?=TEMPLATE_IMAGES?>logo-admin.png" width="40px" alt="Maksoft.net"></a> </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div  class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right list-menu dropdown">
        <?php 

									if($o_page->_page['SecLevel'] > 0)  {

																					for($i=0; $i<count($admin_top_menu); $i++) {
						echo "<li><a href=\"".$o_page->get_pLink($admin_top_menu[$i]['n'])."\" class=\"".(($o_page->n ==$admin_top_menu[$i]['n'] ) ? "selected":"")."\">".$o_page->get_pName($admin_top_menu[$i]['n'])."</a></li>";
	}
	
	}else{ 
				
											for($i=0; $i<count($top_menu); $i++)
							{
								$subpages = $o_page->get_pSubpages($top_menu[$i]['n']);
								$class = "";
								if($o_page->n == $top_menu[$i]['n'] || in_array($top_menu[$i]['n'], $o_page->get_pParentPages())) 
									$class = "selected";
								?>
        <li> <a class="<?=$class?>" href="<?=$o_page->get_pLink($top_menu[$i]['n'])?>">
          <?=$top_menu[$i]['Name']?>
          <?=((count($subpages) >0) ? "<i class=\"icon-down-open\"></i>" : "")?>
          </a>
          <?php	
										 	if(count($subpages) > 0)
												{
													?>
          <ul>
            <?php
														for($j=0; $j<count($subpages); $j++)
															echo "<li><a href=\"".$o_page->get_pLink($subpages[$j]['n'])."\"><i class=\"icon-plus\"></i> ".$subpages[$j]['Name']."</a></li>";	
													?>
          </ul>
          <?	
												}
										 ?>
        </li>
        <? 	}

	}
				?>
        <li>
          <?php include TEMPLATE_DIR."includes/boxes/log-in.php"; ?>
        </li>
      </ul>
    </div>
  </nav>
</div>
<?php } else { ?>
<!-- Info Bar -->
<!--<div class="info-bar">
  <div class="container">
    <?php /*?><?php include TEMPLATE_DIR."includes/boxes/login.php"; ?><?php */?>
  </div>
</div>-->
<!-- Navigation -->
<div id="header_nav" class="nav-container">
  <nav class="navbar navbar-default container navigation" role="navigation"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="<?=$o_page->get_pLink($o_site->StartPage)?>"><img src="<?php current_season() ?>" alt="Maksoft.net" /></a> <!--��������� � configure.php--> 
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div  class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right list-menu dropdown">
        <?php
						for($i=0; $i<count($top_menu); $i++)
							{
								$subpages = $o_page->get_pSubpages($top_menu[$i]['n']);
								$class = "";
								if($o_page->n == $top_menu[$i]['n'] || in_array($top_menu[$i]['n'], $o_page->get_pParentPages())) 
									$class = "selected";
								?>
        <li> <a class="<?=$class?>" href="<?=$o_page->get_pLink($top_menu[$i]['n'])?>">
          <?=$top_menu[$i]['Name']?>
          <?=((count($subpages) >0) ? "<i class=\"icon-down-open\"></i>" : "")?>
          </a>
          <?php
										 	if(count($subpages) > 0)
												{
													?>
          <ul>
            <?php
														for($j=0; $j<count($subpages); $j++)
															echo "<li><a href=\"".$o_page->get_pLink($subpages[$j]['n'])."\"><i class=\"icon-plus\"></i> ".$subpages[$j]['Name']."</a></li>";	
													?>
          </ul>
          <?	
												}
										 ?>
        </li>
        <?			
							}
					?>
        <li>
          <?php include TEMPLATE_DIR."includes/boxes/log-in.php"; ?>
        </li>
        <?
		//read google_translate_array
		if(isset($google_translate_languages) and is_array($google_translate_languages) && count($google_translate_languages)>0)
		{
			?>
        <li>
          <?
			for($i=0; $i<count($google_translate_languages); $i++)
			{
				if(isset($google_translate_languages[$i]['img']))
					echo "<a href=\"".$google_translate_languages[$i]['url']."\" target=\"_blank\"><img src=".$google_translate_languages[$i]['img']." border=\"0\" alt=\"".$google_translate_languages[$i]['title']."\"></a>";
				else
					echo "<a href=\"".$google_translate_languages[$i]['url']."\" target=\"_blank\" title=\"".$google_translate_languages[$i]['title']."\">".$google_translate_languages[$i]['key']."</a>";
			}
			?>
        </li>
        <?
		}
	else	
	if(count($o_site->get_sVersions()) > 0)
	{
	?>
        <li id="languages">
          <?=$o_site->print_sVersions()?>
        </li>
        <?
	}
	?>
      </ul>
    </div>
  </nav>
</div>

<?php } ?>


