	<!-- Portfolio -->
	<div class="portfolio-bg">
		<div class="container">
			<div class="row portfolio heading-category">
				<h2 class="fadeL-Big"><?=PORTFOLIO_PROJECTS?></h2>
				<p class="fadeR-Big"><?=PORTFOLIO_MAKSOFT?></p>
				<nav class="categories-portfolio  fadeInUp">
					<ul data-option-key="filter" class="option-set">
                    <?php
                    	for($i=0; $i<count($portfolio); $i++)
							{
								?>
                                    <li><a class="<?=($i==0) ? "selected":""?>" data-option-value=".<?=$portfolio[$i]['n']?>"><?=$portfolio[$i]['Name']?> <span class="arrow"></span></a></li>
                                <?	
							}
					?>
					</ul>
				</nav>
			</div>	
		</div>
		<nav id="container" class="posts-list container fadeInUp-Big">
			<ul>
				<?php
                    	for($i=0; $i<count($portfolio); $i++)
							{
								$projects = $o_page->get_pSubpages($portfolio[$i]['n'], "p.sort_n DESC LIMIT 12");								
								
								for($j=0; $j<count($projects); $j++)
								{
								?>
                                <li class="element <?=$portfolio[$i]['n']?> creative post-li col-xs-6 col-sm-3">
                                    <div class="view">
                                        <img class="img-responsive" src="/img_preview.php?image_file=<?=$projects[$j]['image_src']?>&img_width=300&ratio=exact" alt="post-img">
                                        <div class="mask"> 
                                            <a href="<?=$o_page->get_pLink($projects[$j]['n'])?>" class="link"><i class="icon-plus"></i></a>
                                        </div>  
                                    </div>
                                    <div class="info-block">
                                        <i class="icon-camera"></i>
                                        <h4><?=$projects[$j]['Name']?></h4>
                                        <p><?=$portfolio[$i]['Name']?></p>
                                    </div>
                                </li>
                                <?
								}
							}
				?>
			</ul>
		</nav>
		<div class="span7 text-center">	
		<a href="http://maksoft.net/267.html" class="btn btn-success"><i class="fa fa-eye"></i> <?=PORTFOLIO_MORE?></a>
	    </div>

	</div>
