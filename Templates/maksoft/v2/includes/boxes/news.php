<div class="testimonials">
<div class="container">
			<h2 class="fadeIn"><?=NEWS_ACTUAL?></h2>
			
            <?php
				for($i=0; $i<count($home_news); $i++)
					{
						$tags = $o_page->get_pTags($home_news[$i]['n']);
						?>
                            <div class="media col-sm-6 testimonial-1 scaleIn">
                                <div class="testimonial-box pull-left">
                                    <a href="<?=$o_page->get_pLink($home_news[$i]['n'])?>">
									<?php $o_page->print_pImage(100, "alt=\"".$home_news[$i]['Name']."\"", TEMPLATE_IMAGES."news/news.png", $home_news[$i]['n'], "exact"); ?>
									</a>
                                </div>
                                <div class="media-body">
                                    <h5 class="media-heading"><a href="<?=$o_page->get_pLink($home_news[$i]['n'])?>"><?=$home_news[$i]['Name']?></a></h5>		
                                    <p><?=cut_text($home_news[$i]['textStr'])?></p>
									<!--
									<div class="tags">
                                    		<i class="icon icon-tag"></i>
											<?php
												for($j=0; $j<count($tags); $j++)
													{
														if($j>0) echo ", ";
														echo "<a href=\"".$o_page->get_pLink($home_news[$i]['n'])."\">".$tags[$j]."</a>";
													}
											?>
                                    </div>
									//-->
                                </div>
                            </div>                        
                        <?
					}	
			?>
</div>
</div>    