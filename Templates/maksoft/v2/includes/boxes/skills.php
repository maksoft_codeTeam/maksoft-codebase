<?php

	$skills = array(
		array("" . SKILLS_REP . "", 2010, 173116),
		array("" . SKILLS_SEO . "", 2008, 41315),
		array("" . SKILLS_PRINT . "", 1997),
		array("" . SKILLS_SOUV . "", 1997),
		array("" . SKILLS_WEBDESIGN . "", 1999, 267),
		array("" . SKILLS_AD . "", 1992)
	);

	$skills_start = 1992;
	

?>

<div class="container progress-bars">
		<div class="elements-heading heading-category">
			<h2><?=SKILLS_SKILL?></h3>
			<hr>
		</div>
		<div class="row">
			<div class="col-sm-6">
			<?php
				
				for($i=0; $i<count($skills); $i++)
				{
					if((2*$i)%6 == 0 && $i>0) echo "</div><div class=\"col-sm-6\">";
					$year = date("Y");
					$difference = $year - $skills[$i][1];
					$percentage = $difference/($year-$skills_start)*100;
					$add_style = "";
					if($percentage > 75) $add_style = "blue ";
					?>
						<div class="progress-container">
							<div class="info clearfix">
								<span class="name">
									<?php
									if($skills[$i][2])
										echo "<a href=\"".$o_page->get_pLink($skills[$i][2])."\">".$skills[$i][0]."</a>";
									else echo $skills[$i][0];
									?>
								</span>
								<span class="percent"><?=$difference?> �.</span>
							</div>
							<div class="progress-bg round">
								<div class="progress-max" style="width: <?=$percentage?>%;">
									<div class="progress-inner round <?=$add_style?>progress-bar-filled" style="visibility: visible;"></div>
								</div>
							</div>
						</div>
					<?php

				}
			?>
	
			</div>
		</div>
	</div>