<?php

	$skills = array(
		array("". SKILLS_REP ."", 2010, 173116),
		array("". SKILLS_SEO ."", 2008, 41315),
		array("". SKILLS_PRINT ."", 1997, 229),
		array("". SKILLS_SOUV ."", 1997, 2),
		array("". SKILLS_WEBDESIGN ."", 1999, 267),
		array("". SKILLS_AD ."", 1992, 4836)
	);

	$skills_start = 1992;
	
?>

<div class="container progress-bars movedown" style="display:none;">
		<div class="elements-heading heading-category">
			<h2><?=SKILLS_SKILL?></h3>
			<hr>
		</div>
		<div class="row">
			<div class="col-sm-6">
			<?php
				
				for($i=0; $i<count($skills); $i++)
				{
					if((2*$i)%6 == 0 && $i>0) echo "</div><div class=\"col-sm-6\">";
					$year = date("Y");
					$difference = $year - $skills[$i][1];
					$percentage = $difference/($year-$skills_start)*100;
					$add_style = "";
					if($percentage > 75) $add_style = "blue ";
					?>
						<div class="progress-container">
							<div class="info clearfix">
								<span class="name">
									<?php
									if($skills[$i][2])
										echo "<a href=\"".$o_page->get_pLink($skills[$i][2])."\">".$skills[$i][0]."</a>";
									else echo $skills[$i][0];
									?>
								</span>
								<span class="percent"><?=$difference?> <?=SKILLS_YEARS?></span>
							</div>
							<div class="progress-bg round">
								<div class="progress-max" style="width: <?=$percentage?>%;">
									<div class="progress-inner round <?=$add_style?>progress-bar-filled" style="visibility: visible;"></div>
								</div>
							</div>
						</div>
					<?php

				}
			?>
	
			</div>
		</div>
	</div>
	<br /><br />

		<div class="container">
			<div class="row">
            <div class="col-xs-12">

                <?php
                    $o_page->print_pContent();
					$cms_args = array("CMS_MAIN_WIDTH"=>"100%", "CSS_FRAMEWORK"=>array('name'=>"bootstrap3", "max_cols"=>12));
                    $o_page->print_pSubContent(NULL, 1, true, $cms_args);
				?>
			</div>
	</div>
	</div></div>
										 	
<div class="spacing-block clearfix" style="height: 50px"></div>

<div class="product-presentation">
	<div class="container accordions">
		<div class="row fadeInUp fadeUp" style="visibility: visible;">
			<!-- Accordion -->
			<div class="panel-group col-sm-6" id="accordion-02" style="margin-top: 20px;">
			<div class="elements-heading">
				<h2><?=SKILLS_VIDEO?></h3>
				<hr>
			</div>

			<div id="simple-carousel" class="simple-carousel carousel slide fadeInUp fadeUp" style="visibility: visible;">
					<div class="carousel-inner">

							<iframe width="100%" height="315" src="https://www.youtube.com/embed/q6kpIJslAI4" frameborder="0" allowfullscreen></iframe>

					</div>
					<a class="left carousel-control" href="#simple-carousel" data-slide="prev">
						<span></span>
					</a>
					<a class="right carousel-control" href="#simple-carousel" data-slide="next">
						<span></span>
					</a>
					
			</div>

			</div>
			<!-- Skills -->
			<div class="panel-group col-sm-6" id="accordion-02" style="margin-top: 20px;" aria-multiselectable="true">
			<div class="elements-heading">
				<h2><?=SKILLS_GOOD?></h3>
				<hr>
			</div>			
			<?php
				for($i=0; $i<count($skills); $i++)
				{
					if($i==0) $colapse = "in";
					else $colapse = "";
					?>
				<div class="panel panel-default">
				    <div class="panel-heading">
				      	<h4 class="panel-title">
				        	<a class="accordion-toggle colapsed" data-toggle="collapse" aria-expanded="true" data-parent="#accordion-<?=$i?>" href="#collapse-<?=$i?>">
				        		<?=$skills[$i][0]?>
				        	</a>
				      	</h4>
				    </div>
				    <div id="collapse-<?=$i?>" class="panel-collapse collapse <?=$colapse?>">
				      	<div class="panel-body">
							<?php
								if($skills[$i][2])
									echo cut_text($o_page->get_pText($skills[$i][2]));
							?>
				      	</div>
				    </div>
				</div>
					<?php
				}
			?>					
			</div>
		</div>
	</div>
</div>
