	<div class="action-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
<?php

	$skills = array(
		array("". SKILLS_REP ."", 2010, 173116),
		array("". SKILLS_SEO ."", 2008, 41315),
		array("". SKILLS_PRINT ."", 1997, 229),
		array("". SKILLS_SOUV ."", 1997, 2),
		array("". SKILLS_WEBDESIGN ."", 1999, 267),
		array("". SKILLS_AD ."", 1992, 4836)
	);

	$skills_start = 1992;

?>

<div class="progress-container">
		<div class="elements-heading heading-category">
			<h2><?=SKILLS_SKILL?></h2>
		</div>

			<?php
				
				for($i=0; $i<count($skills); $i++)
				{
					if((2*$i)%6 == 0 && $i>0) echo "</div>";
					$year = date("Y");
					$difference = $year - $skills[$i][1];
					$percentage = $difference/($year-$skills_start)*100;
					$add_style = "";
					if($percentage > 75) $add_style = "blue ";
					?>
						<div class="progress-container">
							<div class="info clearfix">
								<span class="name">
									<?php
									if($skills[$i][2])
										echo "<a href=\"".$o_page->get_pLink($skills[$i][2])."\">".$skills[$i][0]."</a>";
									else echo $skills[$i][0];
									?>
								</span>
								<span class="percent"><?=$difference?> <?=SKILLS_Y?></span>
							</div>
							<div class="progress-bg round">
								<div class="progress-max" style="width: <?=$percentage?>%;">
									<div class="progress-inner round <?=$add_style?>progress-bar-filled" style="visibility: visible;"></div>
								</div>
							</div>
						</div>
					<?php

				}
			?>
				</div>
				<div class="col-md-4">
						<div class="elements-heading heading-category">
			<h2><?=SKILLS_FOLLOW?></h2>
		</div>
<div class="fb-page" data-href="https://www.facebook.com/Maksoft.net" data-width="300" data-height="350" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Maksoft.net"><a href="https://www.facebook.com/Maksoft.net"><?=SKILLS_MAKSOFTNET?></a></blockquote></div></div>
				</div>
								<div class="col-md-4">
						<div class="elements-heading heading-category">
			<h2><?=SKILLS_QUOTE?></h2>
		</div>
	
            <?php
				include TEMPLATE_DIR . "includes/boxes/zapitvane.php";
			?>
				</div>
			</div>
		</div>
	</div>	