	<!-- Services -->
	<div class="container services">
		<div class="row heading-category fadeInUp">
			<h2><?=SERVICES_WHAT?></h2>
			<p><?=SERVICES_MAKSOFT?></p>
		</div>
		<div class="row circles-services">
			<?php
				$icons = array("icon-design", "icon-support", "icon-seo", "icon-reputation");
				for($i=0; $i<count($services); $i++)
					{
						?>
                        <div class="col-sm-3 circle-container scaleIn">
                            <div class="circle">
                                <i class="<?=$icons[$i]?>"></i>
                            </div>
                            <div class="info">
                                <h4><?=$services[$i]['Name']?></h4>
                                <p><?=cut_text($services[$i]['textStr'])?></p>
                                <a href="<?=$o_page->get_pLink($services[$i]['n'])?>" class="read-more btn btn-primary btn-sm"><i class="icon-right-open-mini"></i><?=SERVICES_MORE?></a>
                            </div>
                        </div>                        
                        <?	
					}
			?>
		</div>
	</div>