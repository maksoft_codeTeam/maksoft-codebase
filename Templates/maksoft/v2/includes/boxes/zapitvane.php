<?php
$ToMail = '';
?>
<form name="form1" method="post" action="/form-cms.php?<?php echo("n=$n&SiteID=$SiteID"); ?>">
<div class="form-group">
<label for="Name"><?=ASK_NAME?></label>
	<div class="input-group">
	<span class="input-group-addon"><i class="fa fa-user"></i></span>
<input name="Name" class="form-control" type="text" id="Name" value="<?php echo("$user->Name"); ?>" required="">
</div>
</div>
<div class="form-group">
<label for="EMail">E-mail</label>
	<div class="input-group">
	<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
<input name="EMail" type="email" class="form-control" id="EMail" value="<?php echo("$user->EMail"); ?>" required="">
</div>
</div>
<div class="form-group">
<label for="Phone"><?=ASK_PHONE?></label>
	<div class="input-group">
	<span class="input-group-addon"><i class="fa fa-phone"></i></span>
<input name="Phone" class="form-control" type="text" id="Phone" required="">
</div>
</div>
<div class="form-group">
<label for="Zapitvane"><?=ASK_MSG?></label>
<textarea name="Zapitvane" class="form-control" rows="2" id="Zapitvane" required></textarea>
</div>
    <?php
    if(defined("reCAPTCHA_Site_key")) {
        echo ' <div class="g-recaptcha" data-sitekey="'.reCAPTCHA_Site_key.'"></div> ';
        echo "<script src='https://www.google.com/recaptcha/api.js?hl=".$o_site->get_sLanguage()."'></script>";
    }
    ?>

    <input name="Submit" type="submit" class="btn btn-default" value="<?=ASK_SEND?>">

        <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
		<input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
        <input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$ToMail"); ?>"> 
        <input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$Site->StartPage&SiteID=$SiteID&sent=1"); ?>">
        <input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
		<input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">

</form>
