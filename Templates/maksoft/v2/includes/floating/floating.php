<?php
	//$social_share_url = "http://".$o_site->_site['primary_url'].$_SERVER['REQUEST_URI'];
	$social_share_url  = $o_page->get_pLink($o_page->n, $o_page->SiteID, "", true); 
?>
   <ul id="menu" class="mfb-component--bl mfb-slidein-spring" data-mfb-toggle="hover">
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
          <i class="mfb-component__main-icon--resting ion-plus-round"></i>
          <i class="mfb-component__main-icon--active ion-close-round"></i>
        </a>
        <ul class="mfb-component__list">
          <li>
            <a href="http://www.facebook.com/sharer.php?u=<?=$social_share_url?>" target="_blank" data-mfb-label="������� � Facebook" class="mfb-component__button--child" rel="nofollow">
              <i class="mfb-component__child-icon ion-social-facebook"></i>
            </a>
          </li>
		  <li>
            <a href="http://maksoft.net/page.php?n=<?=$n?>&templ=28" target="_blank" data-mfb-label="��������� ����������" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-ios-printer"></i>
            </a>
          </li>
		  		  		  <li>
            <a href="http://maksoft.net/page.php?n=15462&SiteID=1" data-mfb-label="����� �������� �������" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-ios-help"></i>
            </a>
          </li>
		  		  <li>
            <a href="http://maksoft.net" data-mfb-label="������� ��������" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-ios-home"></i>
            </a>
          </li>
        </ul>
      </li>
    </ul>

  <!-- build:js mfb.js -->
  <script src="<?=TEMPLATE_PATH?>includes/floating/dist/mfb.js"></script>
  <!-- endbuild -->
  <script>

    var panel = document.getElementById('panel'),
        menu = document.getElementById('menu'),
        showcode = document.getElementById('showcode'),
        selectFx = document.getElementById('selections-fx'),
        selectPos = document.getElementById('selections-pos'),
        // demo defaults
        effect = 'mfb-slidein-spring',
        pos = 'mfb-component--bl';

    showcode.addEventListener('click', _toggleCode);
    selectFx.addEventListener('change', switchEffect);
    selectPos.addEventListener('change', switchPos);

    function _toggleCode() {
      panel.classList.toggle('viewCode');
    }

    function switchEffect(e){
      effect = this.options[this.selectedIndex].value;
      renderMenu();
    }

    function switchPos(e){
      pos = this.options[this.selectedIndex].value;
      renderMenu();
    }

    function renderMenu() {
      menu.style.display = 'none';
      // ?:-)
      setTimeout(function() {
        menu.style.display = 'block';
        menu.className = pos + effect;
      },1);
    }

  </script>