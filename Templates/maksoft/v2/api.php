<?php
require_once __DIR__.'/../../../loader.php';
$o_page = $services['o_page'];
$gate = $services['gate'];


header('Content-Type: application/json', 'charset=cp1251');
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

if(in_array($o_page->_user["ID"], array(1,67,1424,1419,1450, 271, 1564, 564)) and isset($_GET['cmd'])){
    $events = array();
    switch($_GET['cmd']){
        case 'tasks':
            $taskEvents = $gate->task()->getTasks($o_page->_user['ID']);
            $sh = 9;
            $eh = 10;
            foreach ($taskEvents as $task){
                $startDate = $task->start_date;
                while(date('Y-m-d', strtotime($startDate)) < date('Y-m-d', strtotime($task->end_date))){
                    $events[] = array(
                        "title" => iconv('cp1251', 'utf8', $task->subject),
                        "start"=>$startDate,
                        "end" => date('Y-m-d '.$eh.':00:00', strtotime($startDate)),
                        "url" => "/page.php?n=85549&SiteID=1&taskID=".$task->taskID,
                        "resource" => "tasks"
                    );

                    $startDate = date('Y-m-d '.$sh.':00:00', strtotime($startDate. ' + 1 days'));
                }
                if (date('Y-m-d '.$sh.':00:00', strtotime($startDate)) == date('Y-m-d H:i:s', strtotime($task->end_date))){
                
                } else if (date('Y-m-d', strtotime($startDate)) == date('Y-m-d', strtotime($task->end_date))) {
                    $events[] = array(
                        "title" => iconv('cp1251', 'utf8', $task->subject),
                        "start"=>$startDate,
                        "end" => date('Y-m-d '.$eh.':00:00', strtotime($task->end_date)),
                        "url" => "/page.php?n=85549&SiteID=1&taskID=".$task->taskID,
                        "color" => "#e53935",
                        "resource" => "tasks"
                    );
                }
                $sh+=2;
                $eh+=2;
            }
            break;
        case 'orders':
            $orders = $gate->fak()->ordersInProduction($o_page->_user["ID"]);    
            $sh = 9;
            $eh = 10;
            foreach($orders as $order){
                if($order->completed > 0 ){
                    continue;
                }
                $title = sprintf("%s | fID: %s | %s ��. | %s", $order->Name, $order->fID, $order->Total, $order->end_date);

                $startDate = $order->Data;
                while(date('Y-m-d', strtotime($startDate)) < date('Y-m-d', strtotime($order->end_date))){
                    $events[] = array(
                        "title" => iconv('cp1251', 'utf8', $title),
                        "start"=>$startDate,
                        "end" => date('Y-m-d '.$eh.':00:00', strtotime($startDate)),
                        "url" => "/page.php?n=319&SiteID=1&fID=".$order->fID,
                        "resource" => "tasks"
                    );

                    $startDate = date('Y-m-d '.$sh.':00:00', strtotime($startDate. ' + 1 days'));
                }
                if (date('Y-m-d '.$sh.':00:00', strtotime($startDate)) == date('Y-m-d H:i:s', strtotime($order->end_date))){
                } else if (date('Y-m-d', strtotime($startDate)) == date('Y-m-d', strtotime($order->end_date))) {
                    $events[] = array(
                        "title" => iconv('cp1251', 'utf8', $title),
                        "start" => date('Y-m-d '.$sh.':00:00', strtotime($order->end_date)),
                        "end" => date('Y-m-d '.$eh.':00:00', strtotime($order->end_date)),
                        "url" => "/page.php?n=319&SiteID=1&fID=".$order->fID,
                        "resource" => "tasks"
                    );
                }
                $sh+=2;
                $eh+=2;
            }
            break;
        case 'production_list':
            $events = (array) $gate->fak()->productionList($itGroup=5, $o_page->_user['ID']);    
            $events = array_map(function($r) {
                foreach($r as $k=>$v){
                  $r[$k] = iconv('cp1251', 'utf8', $r[$k]);
                }
                return $r;
            }, $events);
            break;
        case 'production_receiving':
            $events = (array) $gate->fak()->productionForReceiving($itGroup=5, $o_page->_user['ID']);    
            $events = array_map(function($r) {
                foreach($r as $k=>$v){
                  $r[$k] = iconv('cp1251', 'utf8', $r[$k]);
                }
                return $r;
            }, $events);
            break;
        default:
            $events = array();
    }
}
json_last_error(); 
echo json_encode($events);
