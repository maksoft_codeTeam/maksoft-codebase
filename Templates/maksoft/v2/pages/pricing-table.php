<script type="text/javascript" >
	//no conflict with Prototype Lib
	var $j = jQuery.noConflict();
</script>
<div id="main">
	<!-- Page Header -->
	<div class="page-header">
		<div class="container">
			<h1>
				<?=$o_page->print_pName()?>
			</h1>
			<ul class="breadcrumbs-custom">
				<li><?=$o_page->print_pNavigation("short");?>
			</ul>
		</div>
	</div>

	<?php
        include TEMPLATE_DIR . "includes/boxes/top-links.php";
    ?>

	<div class="spacing-block clearfix" style="height: 40px"></div>
	
<div class="container ">
		<div class="row heading-category fadeInUp fadeUp" style="visibility: visible;">
			<h2>NetService ������</h2>
			<p>���� � ������ ������ �� ��������� �� �������� ��� ���� � ��� ������. ���� �� ��������� �� ���� � ��������� ������� �� ���������� �� ������������ (CMS), ��������� ������ � �������� ���� ����������� �� Google (SEO), SEO ����������� ������, CRM �������</p>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row pricing-row">
					<div class="col-md-4 col-sm-4 col-xs-6 pricing-container fadeInDown fadeDown" style="visibility: visible;" id="pricing-info">
						<ul class="list-group">
						  	<li class="list-group-item heading">
								<div class="icon"><i class="icon-info"></i></div>
								<a href="#">NetService</a>
							</li>
							<li class="list-group-item pricing"><span class="value">����</span> <br>��. /  �����</li>
							<li class="list-group-item">CMS</li>
							<li class="list-group-item">������������ �� CMS</li>
							<li class="list-group-item">onSite �����������</li>
							<li class="list-group-item">SEO �����������</li>
							<li class="list-group-item">offPage �����������</li>
							<li class="list-group-item">������������ �� ������������</li>
							<li class="list-group-item">Google SiteMap</li>
							<li class="list-group-item">RSS �����</li>
							<li class="list-group-item">CRM �������</li>
							<li class="list-group-item">CMS ������� + ������</li>
							<li class="list-group-item">������� �� ������� ���� </li>
							<li class="list-group-item">Web mail</li>
							<li class="list-group-item">SMTP ������ �� ��������� �� ����</li>
							<li class="list-group-item">FTP ������</li>
							<li class="list-group-item">���������� ������� ������</li>
							<li class="list-group-item">���������� ������� / �������</li>
							<li class="list-group-item">�������� �� ������ ��� CMS</li>
							<li class="list-group-item">�������� ��� ���� ��� ������</li>
							<li class="list-group-item">&nbsp;<br><br><br></li>
						</ul>
					</div>				
					<div class="col-md-2 col-sm-4 col-xs-6 pricing-container pricing-coll fadeInDown fadeDown" style="visibility: visible;">
						<ul class="list-group">
							<li class="list-group-item heading">
								<div class="icon standard"><i class="icon-rocket"></i></div>
								�����
							</li>
							<li class="list-group-item pricing"><span class="value">39</span><br>��. /  �����</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item"><i class="fa fa-times text-danger"></i></li>
							<li class="list-group-item"><i class="fa fa-times text-danger"></i></li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">�������</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">�����������</li>
							<li class="list-group-item">99 � 50Mb</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item"><i class="fa fa-times text-danger text-danger"></i></li>
							<li class="list-group-item"><i class="fa fa-times text-danger"></i></li>
							<li class="list-group-item">1</li>
							<li class="list-group-item">1</li>
							<li class="list-group-item">&nbsp;</li>
							<li class="list-group-item">30%</li>
							<li class="list-group-item"><a href="#" class="btn btn-info">�������</a></li>							
						</ul>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-6 pricing-container pricing-coll fadeInUp-Big fadeUp-Big" style="visibility: visible;">
						<ul class="list-group">
						 	<li class="list-group-item heading">
								<div class="icon standard"><i class="icon-rocket"></i></div>
								Content
							</li>
							<li class="list-group-item pricing"><span class="value">49</span><br>��. /  �����</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item">1/5</li>
							<li class="list-group-item">1 ���./���.</li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">�������</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">�����������</li>
							<li class="list-group-item">99 � 50Mb</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item"><i class="fa fa-times text-danger"></i></li>
							<li class="list-group-item"><i class="fa fa-times text-danger"></i></li>
							<li class="list-group-item">1</li>
							<li class="list-group-item">1</li>
							<li class="list-group-item">&nbsp;</li>
							<li class="list-group-item">30%</li>
							<li class="list-group-item"><a href="#" class="btn btn-info">�������</a></li>								
						</ul>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-6 pricing-container pricing-coll fadeInDown fadeDown" style="visibility: visible;">
						<ul class="list-group">
						  	<li class="list-group-item heading">
								<div class="icon standard"><i class="icon-rocket"></i></div>
								Pro
							</li>
							<li class="list-group-item pricing"><span class="value">49</span><br>��. /  �����</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item"><i class="fa fa-times text-danger"></i></li>
							<li class="list-group-item"><i class="fa fa-times text-danger"></i></li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">���. + ��.</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">�����������</li>
							<li class="list-group-item">99 � 100Mb</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item"><i class="fa fa-times text-danger"></i></li>
							<li class="list-group-item"><i class="fa fa-times text-danger"></i></li>
							<li class="list-group-item">3</li>
							<li class="list-group-item">2</li>
							<li class="list-group-item">&nbsp;</li>
							<li class="list-group-item">30%</li>
							<li class="list-group-item"><a href="#" class="btn btn-info">�������</a></li>								
						</ul>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-6 pricing-container pricing-coll fadeInUp-Big fadeUp-Big" style="visibility: visible;">
						<ul class="list-group">
						  	<li class="list-group-item heading">
								<div class="icon standard"><i class="icon-rocket"></i></div>
								Pro+Content
							</li>
							<li class="list-group-item pricing"><span class="value">59</span><br>��. /  �����</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item">1/5</li>
							<li class="list-group-item">1</li>
							<li class="list-group-item">��������</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">���. + ��.</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item">�����������</li>
							<li class="list-group-item">99 � 100Mb</li>
							<li class="list-group-item"><i class="fa fa-check-square-o text-info"></i></li>
							<li class="list-group-item"><i class="fa fa-times text-danger"></i></li>
							<li class="list-group-item"><i class="fa fa-times text-danger"></i></li>
							<li class="list-group-item">3</li>
							<li class="list-group-item">2</li>
							<li class="list-group-item">&nbsp;</li>
							<li class="list-group-item">30%</li>
							<li class="list-group-item"><a href="#" class="btn btn-info">�������</a></li>								
						</ul>
					</div>					
					
				</div>
			</div>
		</div>
	</div>	
	
	<div class="container fadeIn" id="pageContent">
		<div class="row">
            
			<div class="col-xs-12">
                <?php
                    //$o_page->print_pContent();
					$cms_args = array("CMS_MAIN_WIDTH"=>"100%", "CSS_FRAMEWORK"=>array('name'=>"bootstrap3", "max_cols"=>12));
                    $o_page->print_pSubContent(NULL, 1, true, $cms_args);

					//print php code
					eval($o_page->get_pInfo("PHPcode"));

				if ($user->AccessLevel >= $row->SecLevel)
					include_once("$row->PageURL");
                ?>
            </div>
        </div>

		<div class="row">
            <div class="col-xs-12">
                <?php
					$o_page->print_pNavigation();
                ?>
            </div>
        </div>

        
	</div>
	<!-- Spacing Block -->
	<div class="spacing-block clearfix" style="height: 60px"></div>
    
  </div>