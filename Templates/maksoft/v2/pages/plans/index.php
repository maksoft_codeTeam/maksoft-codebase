<?php
include_once "params.php";

if($packid > 0) {
	include "calc/create-project.php";
}
else {
 ?>
    
    <script src="/Templates/maksoft/v2/pages/plans/js/prefixfree.min.js"></script>
    <link rel="stylesheet" href="/Templates/maksoft/v2/pages/plans/css/style.css">

<div id="main">
	<!-- Page Header -->
	<div class="row page-header">
		<div class="container">
				<div class="col-sm-10">
				
			<h1>
				<!-- <?=$o_page->print_pName()?>  -->
			</h1>
<!-- 			<ul class="breadcrumbs-custom">
				<li><?=$o_page->print_pNavigation("short");?></li>
			</ul> -->
		</div>
		</div>
	</div>
    
	<?php
        include TEMPLATE_DIR . "includes/boxes/top-links.php";
    ?>

		<div class="row heading-category fadeInUp fadeUp" style="visibility: visible; ">
			<h2>NetService ������</h2>
			<p><?php $o_page->print_pContent(); ?></p>
		</div>
</div>

<div class="netservice-packs">
<section id="pakete">

<div class="box"><!-- The surrounding box -->

  <!-- The front container -->
  <div class="front">
    <table border="0">
      <tr class="title">
        <th class="netservice"><?=$netservice?></th>
        <th><?=$limitpack?></th>
        <th><?=$basepack?></th>
        <th><?=$propack?></th>
        <th><?=$contentpack?></th>
        <th><?=$procontentpack?></th>
        <th><?=$joomlapack?></th>
        <th><?=$wordpresspack?></th>
      </tr>
      <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_maksoftcms?>"><?=$maksoftcms?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_maksoftcms_opt?>
        <?=$base_maksoftcms_opt?>
        <?=$pro_maksoftcms_opt?>
        <?=$content_maksoftcms_opt?>
        <?=$procontent_maksoftcms_opt?>
        <?=$joomla_maksoftcms_opt?>
        <?=$wordpress_maksoftcms_opt?>
      </tr>
      <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_updatecms?>"><?=$updatecms?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_updatecms_opt?>
        <?=$base_updatecms_opt?>
        <?=$pro_updatecms_opt?>
        <?=$content_updatecms_opt?>
        <?=$procontent_updatecms_opt?>
        <?=$joomla_updatecms_opt?>
        <?=$wordpress_updatecms_opt?>
      </tr>
      <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_onsite?>"><?=$onsite?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_onsite_opt?>
        <?=$base_onsite_opt?>
        <?=$pro_onsite_opt?>
        <?=$content_onsite_opt?>
        <?=$procontent_onsite_opt?>
        <?=$joomla_onsite_opt?>
        <?=$wordpress_onsite_opt?>
      </tr>
      <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_seocopy?>"><?=$seocopy?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_seocopy_opt?>
		<?=$base_seocopy_opt?>
        <?=$pro_seocopy_opt?>
        <?=$content_seocopy_opt?>
        <?=$procontent_seocopy_opt?>
        <?=$joomla_seocopy_opt?>
        <?=$wordpress_seocopy_opt?>
      </tr>
      <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_offpage?>"><?=$offpage?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_offpage_opt?>
        <?=$base_offpage_opt?>
        <?=$pro_offpage_opt?>
        <?=$content_offpage_opt?>
        <?=$procontent_offpage_opt?>
        <?=$joomla_offpage_opt?>
        <?=$wordpress_offpage_opt?>
      </tr>
      <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_relevant?>"><?=$relevant?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_relevant_opt?>
        <?=$base_relevant_opt?>
        <?=$pro_relevant_opt?>
        <?=$content_relevant_opt?>
        <?=$procontent_relevant_opt?>
        <?=$joomla_relevant_opt?>
        <?=$wordpress_relevant_opt?>
      </tr>
      <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_gsitemap?>"><?=$gsitemap?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_gsitemap_opt?>
        <?=$base_gsitemap_opt?>
        <?=$pro_gsitemap_opt?>
        <?=$content_gsitemap_opt?>
        <?=$procontent_gsitemap_opt?>
        <?=$joomla_gsitemap_opt?>
        <?=$wordpress_gsitemap_opt?>
      </tr>
      <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_rsschannel?>"><?=$rsschannel?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_rsschannel_opt?>        
		<?=$base_rsschannel_opt?>
        <?=$pro_rsschannel_opt?>
        <?=$content_rsschannel_opt?>
        <?=$procontent_rsschannel_opt?>
        <?=$joomla_rsschannel_opt?>
        <?=$wordpress_rsschannel_opt?>
      </tr>
       <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_crmfunc?>"><?=$crmfunc?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_crmfunc_opt?>
        <?=$base_crmfunc_opt?>
        <?=$pro_crmfunc_opt?>
        <?=$content_crmfunc_opt?>
        <?=$procontent_crmfunc_opt?>
        <?=$joomla_crmfunc_opt?>
        <?=$wordpress_crmfunc_opt?>
      </tr>
       <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_cmshosting?>"><?=$cmshosting?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_cmshosting_opt?>
        <?=$base_cmshosting_opt?>
        <?=$pro_cmshosting_opt?>
        <?=$content_cmshosting_opt?>
        <?=$procontent_cmshosting_opt?>
        <?=$joomla_cmshosting_opt?>
        <?=$wordpress_cmshosting_opt?>
      </tr>
            <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_webmail?>"><?=$webmail?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_webmail_opt?>
        <?=$base_webmail_opt?>
        <?=$pro_webmail_opt?>
        <?=$content_webmail_opt?>
        <?=$procontent_webmail_opt?>
        <?=$joomla_webmail_opt?>
        <?=$wordpress_webmail_opt?>
      </tr>
       <tr>
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_site_ver?>"><?=$site_ver?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <?=$limit_langvers_opt?>
        <?=$base_langvers_opt?>
        <?=$pro_langvers_opt?>
        <?=$content_langvers_opt?>
        <?=$procontent_langvers_opt?>
        <?=$joomla_langvers_opt?>
        <?=$wordpress_langvers_opt?>
      </tr>

      <tr class="percent">
        <th class="serviceth"><a href="#" data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_discount?>"><?=$discount?> <i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        <td><span class="percent-marker"><?=$limit_discount?></span></td>
        <td><span class="percent-marker"><?=$base_discount?></span></td>
        <td><span class="percent-marker"><?=$pro_discount?></span></td>
        <td><span class="percent-marker"><?=$content_discount?></span></td>
        <td><span class="percent-marker"><?=$procontent_discount?></span></td>
        <td><span class="percent-marker"><?=$joomla_discount?></span></td>
        <td><span class="percent-marker"><?=$wordpress_discount?></span></td>
      </tr>
      
      <tr class="price">
        <th class="serviceth"><?=$packageprice?></th>
        <td><span class="price"><?=$limit_packageprice?> ��.<br><span class="monthly">�������</span></span></td>
        <td><span class="price"><?=$base_packageprice?> ��.<br><span class="monthly">�������</span></span></td>
        <td><span class="price"><?=$pro_packageprice?> ��.<br><span class="monthly">�������</span></span></td>
        <td><span class="price"><?=$content_packageprice?> ��.<br><span class="monthly">�������</span></span></td>
        <td><span class="price"><?=$procontent_packageprice?> ��.<br><span class="monthly">�������</span></span></td>
        <td><span class="price"><?=$joomla_packageprice?> ��.<br><span class="monthly">�������</span></span></td>
        <td><span class="price"><?=$wordpress_packageprice?> ��.<br><span class="monthly">�������</span></span></td>
      </tr>      
      <tr>
        <th class="hiddenth"></th>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=1"><button>������ ������</button></a></td>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=2"><button>������ ������</button></a></td>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=3"><button>������ ������</button></a></td>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=4"><button>������ ������</button></a></td>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=5"><button>������ ������</button></a></td>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=6"><button>������ ������</button></a></td>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=7"><button>������ ������</button></a></td>
      </tr>
    </table>    
  </div>
<div class="responsive-tables">
  <div class="little1 active">
    <table>
      <tr class="title">
        <th class="netservice"><?=$netservice?></th>
        <th><?=$limitpack?></th>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_maksoftcms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$maksoftcms?></a></div></th>
        <?=$limit_maksoftcms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_updatecms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$updatecms?></a></div></th>
        <?=$limit_updatecms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_onsite?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$onsite?></a></div></th>
        <?=$limit_onsite_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_seocopy?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$seocopy?></a></div></th>
        <?=$limit_seocopy_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_offpage?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$offpage?></a></div></th>
        <?=$limit_offpage_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_relevant?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$relevant?></a></div></th>
        <?=$limit_relevant_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_gsitemap?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$gsitemap?></a></div></th>
        <?=$limit_gsitemap_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_rsschannel?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$rsschannel?></a></div></th>
        <?=$limit_rsschannel_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_crmfunc?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$crmfunc?></a></div></th>
        <?=$limit_crmfunc_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_cmshosting?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$cmshosting?></a></div></th>
        <?=$limit_cmshosting_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_webmail?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$webmail?></a></div></th>
        <?=$limit_webmail_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_langvers?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$langvers?></a></div></th>
        <?=$limit_langvers_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_discount?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$discount?></a></div></th>
        <?=$limit_discount_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><?=$packageprice?></th>
        <td><?=$limit_packageprice?> ��./�.</td>
      </tr>
      <tr>
        <th class="hiddenth2"></th>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=1"><button>������ ������</button></a></td>
      </tr>
    </table>
  </div>
  <div class="little2 activetwo">
      <table>
      <tr class="title">
        <th class="netservice"><?=$netservice?></th>
        <th><?=$basepack?></th>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_maksoftcms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$maksoftcms?></a></div></th>
        <?=$base_maksoftcms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_updatecms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$updatecms?></a></div></th>
        <?=$base_updatecms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_onsite?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$onsite?></a></div></th>
        <?=$base_onsite_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_seocopy?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$seocopy?></a></div></th>
        <?=$base_seocopy_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_offpage?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$offpage?></a></div></th>
        <?=$base_offpage_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_relevant?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$relevant?></a></div></th>
        <?=$base_relevant_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_gsitemap?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$gsitemap?></a></div></th>
        <?=$base_gsitemap_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_rsschannel?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$rsschannel?></a></div></th>
        <?=$base_rsschannel_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_crmfunc?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$crmfunc?></a></div></th>
        <?=$base_crmfunc_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_cmshosting?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$cmshosting?></a></div></th>
        <?=$base_cmshosting_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_webmail?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$webmail?></a></div></th>
        <?=$base_webmail_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_langvers?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$langvers?></a></div></th>
        <?=$base_langvers_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_discount?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$discount?></a></div></th>
        <?=$base_discount_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><?=$packageprice?></th>
        <td><?=$base_packageprice?> ��./�.</td>
      </tr>
      <tr>
        <th class="hiddenth2"></th>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=2"><button>������ ������</button></a></td>
      </tr>
    </table>
  </div>
  <div class="little3">
      <table>
      <tr class="title">
        <th class="netservice"><?=$netservice?></th>
        <th><?=$contentpack?></th>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_maksoftcms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$maksoftcms?></a></div></th>
        <?=$content_maksoftcms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_updatecms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$updatecms?></a></div></th>
        <?=$content_updatecms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_onsite?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$onsite?></a></div></th>
        <?=$content_onsite_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_seocopy?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$seocopy?></a></div></th>
        <?=$content_seocopy_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_offpage?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$offpage?></a></div></th>
        <?=$content_offpage_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_relevant?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$relevant?></a></div></th>
        <?=$content_relevant_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_gsitemap?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$gsitemap?></a></div></th>
        <?=$content_gsitemap_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_rsschannel?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$rsschannel?></a></div></th>
        <?=$content_rsschannel_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_crmfunc?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$crmfunc?></a></div></th>
        <?=$content_crmfunc_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_cmshosting?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$cmshosting?></a></div></th>
        <?=$content_cmshosting_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_webmail?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$webmail?></a></div></th>
        <?=$content_webmail_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_langvers?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$langvers?></a></div></th>
        <?=$content_langvers_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_discount?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$discount?></a></div></th>
        <?=$content_discount_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><?=$packageprice?></th>
        <td><?=$content_packageprice?> ��./�.</td>
      </tr>
      <tr>
        <th class="hiddenth2"></th>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=3"><button>������ ������</button></a></td>
      </tr>
    </table>
  </div>
  <div class="little4">
      <table>
      <tr class="title">
        <th class="netservice"><?=$netservice?></th>
        <th><?=$propack?></th>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_maksoftcms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$maksoftcms?></a></div></th>
        <?=$pro_maksoftcms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_updatecms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$updatecms?></a></div></th>
        <?=$pro_updatecms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_onsite?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$onsite?></a></div></th>
        <?=$pro_onsite_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_seocopy?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$seocopy?></a></div></th>
        <?=$pro_seocopy_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_offpage?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$offpage?></a></div></th>
        <?=$pro_offpage_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_relevant?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$relevant?></a></div></th>
        <?=$pro_relevant_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_gsitemap?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$gsitemap?></a></div></th>
        <?=$pro_gsitemap_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_rsschannel?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$rsschannel?></a></div></th>
        <?=$pro_rsschannel_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_crmfunc?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$crmfunc?></a></div></th>
        <?=$pro_crmfunc_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_cmshosting?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$cmshosting?></a></div></th>
        <?=$pro_cmshosting_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_webmail?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$webmail?></a></div></th>
        <?=$pro_webmail_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_langvers?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$langvers?></a></div></th>
        <?=$pro_langvers_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_discount?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$discount?></a></div></th>
        <?=$pro_discount_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><?=$packageprice?></th>
        <td><?=$pro_packageprice?> ��./�.</td>
      </tr>
      <tr>
        <th class="hiddenth2"></th>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=4"><button>������ ������</button></a></td>
      </tr>
    </table>
  </div>
  <div class="little5">
      <table>
      <tr class="title">
        <th class="netservice"><?=$netservice?></th>
        <th><?=$procontentpack?></th>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_maksoftcms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$maksoftcms?></a></div></th>
        <?=$procontent_maksoftcms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_updatecms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$updatecms?></a></div></th>
        <?=$procontent_updatecms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_onsite?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$onsite?></a></div></th>
        <?=$procontent_onsite_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_seocopy?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$seocopy?></a></div></th>
        <?=$procontent_seocopy_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_offpage?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$offpage?></a></div></th>
        <?=$procontent_offpage_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_relevant?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$relevant?></a></div></th>
        <?=$procontent_relevant_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_gsitemap?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$gsitemap?></a></div></th>
        <?=$procontent_gsitemap_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_rsschannel?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$rsschannel?></a></div></th>
        <?=$procontent_rsschannel_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_crmfunc?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$crmfunc?></a></div></th>
        <?=$procontent_crmfunc_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_cmshosting?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$cmshosting?></a></div></th>
        <?=$procontent_cmshosting_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_webmail?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$webmail?></a></div></th>
        <?=$procontent_webmail_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_langvers?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$langvers?></a></div></th>
        <?=$procontent_langvers_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_discount?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$discount?></a></div></th>
        <?=$procontent_discount_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><?=$packageprice?></th>
        <td><?=$procontent_packageprice?> ��./�.</td>
      </tr>
      <tr>
        <th class="hiddenth2"></th>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=5"><button>������ ������</button></a></td>
      </tr>
    </table>
  </div>
  <div class="little6">
      <table>
      <tr class="title">
        <th class="netservice"><?=$netservice?></th>
        <th><?=$joomlapack?></th>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_maksoftcms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$maksoftcms?></a></div></th>
        <?=$joomla_maksoftcms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_updatecms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$updatecms?></a></div></th>
        <?=$joomla_updatecms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_onsite?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$onsite?></a></div></th>
        <?=$joomla_onsite_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_seocopy?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$seocopy?></a></div></th>
        <?=$joomla_seocopy_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_offpage?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$offpage?></a></div></th>
        <?=$joomla_offpage_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_relevant?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$relevant?></a></div></th>
        <?=$joomla_relevant_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_gsitemap?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$gsitemap?></a></div></th>
        <?=$joomla_gsitemap_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_rsschannel?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$rsschannel?></a></div></th>
        <?=$joomla_rsschannel_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_crmfunc?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$crmfunc?></a></div></th>
        <?=$joomla_crmfunc_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_cmshosting?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$cmshosting?></a></div></th>
        <?=$joomla_cmshosting_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_webmail?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$webmail?></a></div></th>
        <?=$joomla_webmail_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_langvers?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$langvers?></a></div></th>
        <?=$joomla_langvers_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_discount?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$discount?></a></div></a></th>
        <?=$joomla_discount_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><?=$packageprice?></th>
        <td><?=$joomla_packageprice?> ��./�.</td>
      </tr>
      <tr>
        <th class="hiddenth2"></th>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=6"><button>������ ������</button></a></td>
      </tr>
    </table>

  </div>
    <div class="little7 last">
        <table>
      <tr class="title">
        <th class="netservice"><?=$netservice?></th>
        <th><?=$wordpresspack?></th>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_maksoftcms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$maksoftcms?></a></div></th>
        <?=$wordpress_maksoftcms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$updatecms?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$updatecms?></a></div></th>
        <?=$wordpress_updatecms_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_onsite?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$onsite?></a></div></th>
        <?=$wordpress_onsite_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_seocopy?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$seocopy?></a></div></th>
        <?=$wordpress_seocopy_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_offpage?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$offpage?></a></div></th>
        <?=$wordpress_offpage_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_relevant?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$relevant?></a></div></th>
        <?=$wordpress_relevant_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_gsitemap?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$gsitemap?></a></div></th>
        <?=$wordpress_gsitemap_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_rsschannel?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$rsschannel?></a></div></th>
        <?=$wordpress_rsschannel_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_crmfunc?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$crmfunc?></a></div></th>
        <?=$wordpress_crmfunc_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_cmshosting?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$cmshosting?></a></div></th>
        <?=$wordpress_cmshosting_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_webmail?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$webmail?></a></div></th>
        <?=$wordpress_webmail_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_langvers?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$langvers?></a></div></th>
        <?=$wordpress_langvers_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><div class="short-text"><a data-toggle="tooltip" data-placement="bottom" class="tooltip-a" title="<?=$info_discount?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=$discount?></a></div></th>
        <?=$wordpress_discount_opt?>
      </tr>
      <tr>
        <th class="serviceth2"><?=$packageprice?></th>
        <td><?=$wordpress_packageprice?> ��./�.</td>
      </tr>
      <tr>
        <th class="hiddenth2"></th>
        <td><a href="/page.php?n=<?=$n?>&SiteID=<?=$SiteID?>&packid=7"><button>������ ������</button></a></td>
      </tr>
    </table>
  </div>
  <button class="next-table entypo-right-dir"></button>
  <button class="prev-table entypo-left-dir"></button>

</div>
  
</section><!-- END of section-wrap -->
</div>

<?php
}
?>

<div class="spacing-block clearfix" style="height: 60px"></div>

	<div class="container fadeIn" id="pageContent">
		<div class="row">
            
			<div class="col-xs-12">
                <?php
                    //$o_page->print_pContent();
					$cms_args = array("CMS_MAIN_WIDTH"=>"100%", "CSS_FRAMEWORK"=>array('name'=>"bootstrap3", "max_cols"=>12));
                    $o_page->print_pSubContent(NULL, 1, true, $cms_args);

					//print php code
					eval($o_page->get_pInfo("PHPcode"));

				if ($user->AccessLevel >= $row->SecLevel)
					include_once("$row->PageURL");
                ?>
            </div>
        </div>

		<div class="row">
            <div class="col-xs-12">
                <?php
					$o_page->print_pNavigation();
                ?>
            </div>
        </div>

        
	</div>
	<!-- Spacing Block -->
	<div class="spacing-block clearfix" style="height: 60px"></div>
    
  </div>
  
  
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="/Templates/maksoft/v2/pages/plans/js/index.js"></script>
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
