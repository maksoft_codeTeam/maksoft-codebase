 <?php
   // ������
   $maksoftcms   = "����� ����� (CMS)";
   $updatecms    = "��������� CMS";
   $onsite       = "OnSite �����������";
   $seocopy      = "SEO �����������";
   $offpage      = "offPage �����������";
   $relevant     = "������������ �� ������������";
   $gsitemap     = "��������� Google SiteMap";
   $rsschannel   = "��������� RSS ������";
   $crmfunc      = "���������� ���������";
   $cmshosting   = "CMS ������� + ������";
   $webmail      = "WebMail ����  / ���� ����� / ���� ";
   $mail_func  = "POP3 / IMAP4 / SMTP";
   $mail_accounts  = "���� �������� �����";
   $site_ver     = "���� ������� / ������� ������";
   $discount     = "�������� �� ������ SEO �����������, ��� ������ � ��������� �� ������� ��������";
   $packageprice = "���� ��� ���";
   
   // ������
   $netservice         = "��������� �� ����";
   $limitpack          = "���������";
   $basepack           = "Base";
   $propack            = "PRO";
   $contentpack        = "Content";
   $procontentpack     = "PRO+Content";
   $joomlapack         = "Joomla";
   $wordpresspack      = "Wordpress";
   
   // LIMIT �����
   $limit_maksoftcms_opt = "<td class='entypo-check'></td>";  // Maksoft CMS
   $limit_updatecms_opt  = "<td class='entypo-check'></td>";  // ������������ �� CMS
   $limit_onsite_opt     = "<td class='entypo-cancel'></td>";  // OnSite �����������
   $limit_seocopy_opt    = "<td class='entypo-cancel'></td>"; // SEO �����������
   $limit_offpage_opt    = "<td class='entypo-cancel'></td>"; // offPage �����������
   $limit_relevant_opt   = "<td class='entypo-cancel'></td>";  // ������������ �� ������������
   $limit_gsitemap_opt   = "<td class='entypo-cancel'></td>";  // Google SiteMap
   $limit_rsschannel_opt = "<td class='entypo-cancel'></td>";  // RSS �����
   $limit_crmfunc_opt    = "<td class='entypo-cancel'></td>";  // CRM �������
   $limit_cmshosting_opt = "<td>���������</td>";            // CMS ������� + ������
   $limit_webmail_opt    = "<td class='entypo-check'> 3 / 1024Mb</td>";  // WebMail
   $limit_langvers_opt   = "<td>1</td>";                      // ������� ������
   $limit_discount       = "-";                             // ��������
   $limit_packageprice   = "9.<sup>90</sup>";                              // ����
   $limit_packageprice_calc   = "9.90"; // ���� �� �����������
   
   // BASE �����
   $base_maksoftcms_opt = "<td class='entypo-check'></td>";  // Maksoft CMS
   $base_updatecms_opt  = "<td class='entypo-check'></td>";  // ������������ �� CMS
   $base_onsite_opt     = "<td class='entypo-check'></td>";  // OnSite �����������
   $base_seocopy_opt    = "<td class='entypo-cancel'></td>"; // SEO �����������
   $base_offpage_opt    = "<td class='entypo-cancel'></td>"; // offPage �����������
   $base_relevant_opt   = "<td class='entypo-check'></td>";  // ������������ �� ������������
   $base_gsitemap_opt   = "<td class='entypo-check'></td>";  // Google SiteMap
   $base_rsschannel_opt = "<td class='entypo-check'></td>";  // RSS �����
   $base_crmfunc_opt    = "<td class='entypo-cancel'></td>";  // CRM �������
   $base_cmshosting_opt = "<td>�����������</td>";            // CMS ������� + ������
   $base_webmail_opt    = "<td class='entypo-check'> 10 /2048Mb</td>";  // WebMail
   $base_langvers_opt   = "<td>1 / +9<sup>90</sup></td>";                      // ������� ������
   $base_discount       = "-";                             // ��������
   $base_packageprice   = "39";                              // ����
   
 // PRO �����
   $pro_maksoftcms_opt = "<td class='entypo-check'></td>";  // Maksoft CMS
   $pro_updatecms_opt  = "<td class='entypo-check'></td>";  // ������������ �� CMS
   $pro_onsite_opt     = "<td class='entypo-check'></td>";  // OnSite �����������
   $pro_seocopy_opt    = "<td class='entypo-cancel'></td>"; // SEO �����������
   $pro_offpage_opt    = "<td class='entypo-cancel'></td>"; // offPage �����������
   $pro_relevant_opt   = "<td class='entypo-check'></td>";  // ������������ �� ������������
   $pro_gsitemap_opt   = "<td class='entypo-check'></td>";  // Google SiteMap
   $pro_rsschannel_opt = "<td class='entypo-check'></td>";  // RSS �����
   $pro_crmfunc_opt    = "<td class='entypo-check'></td>";  // CRM �������
   $pro_cmshosting_opt = "<td>�����������</td>";            // CMS ������� + ������
   $pro_webmail_opt    = "<td class='entypo-check'> 100 /5120 Mb</td>";  // WebMail
   $pro_langvers_opt   = "<td>3 / +9<sup>90</td>";                      // ������� ������
   $pro_discount       = "30%";                             // ��������
   $pro_packageprice   = "49";                              // ����
   
  // CONTENT �����
   $content_maksoftcms_opt = "<td class='entypo-check'></td>";  // Maksoft CMS
   $content_updatecms_opt  = "<td class='entypo-check'></td>";  // ������������ �� CMS
   $content_onsite_opt     = "<td class='entypo-check'></td>";  // OnSite �����������
   $content_seocopy_opt    = "<td class='entypo-check'></td>"; // SEO �����������
   $content_offpage_opt    = "<td class='entypo-check'></td>"; // offPage �����������
   $content_relevant_opt   = "<td class='entypo-check'></td>";  // ������������ �� ������������
   $content_gsitemap_opt   = "<td class='entypo-check'></td>";  // Google SiteMap
   $content_rsschannel_opt = "<td class='entypo-check'></td>";  // RSS �����
   $content_crmfunc_opt    = "<td class='entypo-cancel'></td>";  // CRM �������
   $content_cmshosting_opt = "<td>�����������</td>";            // CMS ������� + ������
   $content_webmail_opt    = "<td class='entypo-check'> 10 /2048Mb </td>";  // WebMail
   $content_langvers_opt   = "<td>1 / +19<sup>90</td>";                      // ������� ������
   $content_discount       = "-";                             // ��������
   $content_packageprice   = "59";                              // ����
   
      // PRO+CONTENT �����
   $procontent_maksoftcms_opt = "<td class='entypo-check'></td>";  // Maksoft CMS
   $procontent_updatecms_opt  = "<td class='entypo-check'></td>";  // ������������ �� CMS
   $procontent_onsite_opt     = "<td class='entypo-check'></td>";  // OnSite �����������
   $procontent_seocopy_opt    = "<td class='entypo-check'></td>"; // SEO �����������
   $procontent_offpage_opt    = "<td class='entypo-check'></td>"; // offPage �����������
   $procontent_relevant_opt   = "<td class='entypo-check'></td>";  // ������������ �� ������������
   $procontent_gsitemap_opt   = "<td class='entypo-check'></td>";  // Google SiteMap
   $procontent_rsschannel_opt = "<td class='entypo-check'></td>";  // RSS �����
   $procontent_crmfunc_opt    = "<td class='entypo-check'></td>";  // CRM �������
   $procontent_cmshosting_opt = "<td>�����������</td>";            // CMS ������� + ������
   $procontent_webmail_opt    = "<td class='entypo-check'>100 /5120 Mb</td>";  // WebMail
   $procontent_langvers_opt   = "<td>3 / +19<sup>90</td>";                      // ������� ������
   $procontent_discount       = "30%";                             // ��������
   $procontent_packageprice   = "69";                              // ����
   
   // JOOMLA �����
   $joomla_maksoftcms_opt = "<td class='entypo-check'></td>";  //  CMS
   $joomla_updatecms_opt  = "<td class='entypo-cancel'></td>";  // ������������ �� CMS
   $joomla_onsite_opt     = "<td class='entypo-check'></td>";  // OnSite �����������
   $joomla_seocopy_opt    = "<td class='entypo-cancel'></td>"; // SEO �����������
   $joomla_offpage_opt    = "<td class='entypo-cancel'></td>"; // offPage �����������
   $joomla_relevant_opt   = "<td class='entypo-cancel'></td>";  // ������������ �� ������������
   $joomla_gsitemap_opt   = "<td class='entypo-cancel'></td>";  // Google SiteMap
   $joomla_rsschannel_opt = "<td class='entypo-check'></td>";  // RSS �����
   $joomla_crmfunc_opt    = "<td class='entypo-check'></td>";  // CRM �������
   $joomla_cmshosting_opt = "<td>�����������</td>";            // CMS ������� + ������
   $joomla_webmail_opt    = "<td class='entypo-check'>100 /5120 Mb</td>";  // WebMail
   $joomla_langvers_opt   = "<td>1</td>";                      // ������� ������
   $joomla_discount       = "-";                             // ��������
   $joomla_packageprice   = "89";                              // ����
   
   // WORDPRESS �����
   $wordpress_maksoftcms_opt = "<td class='entypo-check'></td>";  // Maksoft CMS
   $wordpress_updatecms_opt  = "<td class='entypo-cancel'></td>";  // ������������ �� CMS
   $wordpress_onsite_opt     = "<td class='entypo-check'></td>";  // OnSite �����������
   $wordpress_seocopy_opt    = "<td class='entypo-cancel'></td>"; // SEO �����������
   $wordpress_offpage_opt    = "<td class='entypo-cancel'></td>"; // offPage �����������
   $wordpress_relevant_opt   = "<td class='entypo-check'></td>";  // ������������ �� ������������
   $wordpress_gsitemap_opt   = "<td class='entypo-check'></td>";  // Google SiteMap
   $wordpress_rsschannel_opt = "<td class='entypo-check'></td>";  // RSS �����
   $wordpress_crmfunc_opt    = "<td class='entypo-check'></td>";  // CRM �������
   $wordpress_cmshosting_opt = "<td>�����������</td>";            // CMS ������� + ������
   $wordpress_webmail_opt    = "<td class='entypo-check'>100 /5120 Mb</td>";  // WebMail
   $wordpress_langvers_opt   = "<td>1</td>";                      // ������� ������
   $wordpress_discount       = "-";                             // ��������
   $wordpress_packageprice   = "89";                              // ����
   
   // INFO BOXES
   $info_maksoftcms   = "����� ������� �� ��������� � �������� �� ���������� �� ���� �� ��������� ���� � ������� ������� ����.";
   $info_updatecms    = "������� �� ���������� �� ������������, ����o �������� ��� 500 �������� �� ��������� �������� �� �������, �������� � ������.";
   $info_onsite       = "������� � ��������� ��������� �� Google �������� �����������, ����� ��������� ����������� ��� �������.";
   $info_seocopy      = "���������� SEO ����������� ������� ����� ���������� �������� �� �������. �������� �� ������� ����������, �� �������� ���������� � Google, ������ ������.";
   $info_offpage      = "���������� �� ����������� ������ ��� ������ ������� � �������� �����, ���� ����������� �� �������� � Maksoft CMS. ������� ������� ������ (����) ��� ���������� �� �������, ���� ����� ������ �������������� ����� �� �������� ������ ����������.";
   $info_relevant     = "��������� �� ������������� ��������� �� �����, ����� �������� �� ��������� � Google � ����� ������������ � Google ��������� ������.";
   $info_gsitemap     = "��������� ������� � ������������ � ���������� �� ������������ � ����� �� ����, ���������, ������������ � �������� ��������, ����� ����������� ������������ �������� �� Google �������� ����� �� �����.";
   $info_rsschannel   = "�������� � ���� ������������� ����� ���-������� ���������� �� ����� ���� ���������� � ������� �� Maksoft CMS ���������. ����� ������ ����, �������� ��� ����� �� �������, �������� ������ �� �������������� ����� �� RSS �����, ��� ������� ������. ";
   $info_crmfunc      = "���������� ��������� �� ����������� �� ������� ����, ����� �� ����� ������������ �� �� ����������� �������� � ��������.";
   $info_cmshosting   = "������� ������� �� ���������� � ������� ����� � ������� ������ ������. ";
   $info_webmail      = "������� � �������� ���� �������� ����� � �������� ������������ �����, � ������ ���������� �� ���������� ��� ���� �������.";
   $info_mail_func    = "����������";
   $info_mail_accounts  = "����������";
   $info_site_ver     = "���� ������� ��� ���� ������� ������ �� ���� � ���� ����, ���������� �� ������.";
   $info_discount     = "��������� �� �������� �� ��������� �� ������������ ������ ��� ������� NetService ������� �����.";
   
   // ������������
   $backbutton = "������";         
 ?>