<div id="main" class="blog">
	<!-- Page Header -->
	<div class="page-header">
		<div class="container">
			<h1>
				<?=$o_page->print_pName()?>
			</h1>
			<ul class="breadcrumbs-custom">
				<li><?=$o_page->print_pNavigation("short");?>
			</ul>
		</div>
	</div>

	<?php
        include TEMPLATE_DIR . "includes/boxes/top-links.php";
    ?>

	<div class="spacing-block clearfix" style="height: 40px"></div>
	<div class="container fadeIn" id="pageContent">
		<div class="row">
            <div class="col-xs-9">
                <?php
				//print custom admin view
				
					
					$o_page->print_pContent();
					$cms_args = array("CMS_MAIN_WIDTH"=>"100%", "CSS_FRAMEWORK"=>array('name'=>"bootstrap3", "max_cols"=>9));
                    if($o_page->n != 11) 
						$o_page->print_pSubContent(NULL, 1, true, $cms_args);

					//print php code
					eval($o_page->get_pInfo("PHPcode"));

					if ($user->AccessLevel >= $row->SecLevel) include_once("$row->PageURL");
                ?>
            </div>
			<div class="col-xs-3 blog-sidebar">
			<?php
				//include TEMPLATE_DIR . "includes/boxes/admin-menu.php";
				include "web/forms/maksoft/faqs.php";
			?>
			</div>
			<div class="col-sm-3 blog-sidebar fadeR">
				<div class="top-categories">
					<h4 class="sidebar-heading clearfix">TOP CATEGORIES</h4>
					<?php
						$p_pages = $o_page->get_pSubpages($o_page->_page['ParentPage']);
						for($i=0; $i<count($p_pages); $i++)
							{
								?>
									
									<a href="#">
										<span class="icon-cat"><i class="icon-briefcase"></i></span>
										<span class="text"><?=$p_pages[$i]['Name']?></span>
										<i class="icon-right-open-mini right-open"></i>
									</a>
									<hr>								
								<?php
							}
					?>

					
				</div>
			    <div class="tags">
			    	<h4 class="sidebar-heading">POPULAR TAGS</h4>
			    	<ul class="clearfix">
			    		<?php
							$tags = $o_page->get_pTags();
							for($i=0; $i<count($tags); $i++)
								{
									?>
										<li><a href="#" class="tag"><?=$tags[$i]?></a></li>
									<?php
								}
						?>
			    	</ul>
			    </div>
			    <hr>
				
			</div>

			
        </div>
		<div class="clearfix"></div>
		<div class="row">
            <div class="col-xs-12">
                <?php
					$o_page->print_pNavigation();
                ?>
            </div>
        </div>

        
	</div>
	<!-- Spacing Block -->
	<div class="spacing-block clearfix" style="height: 60px"></div>
</div>
