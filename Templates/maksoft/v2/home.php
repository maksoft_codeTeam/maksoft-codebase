<div class="home">
	
	<?php include TEMPLATE_INCLUDES . "boxes/slider.php"; ?>
	
	<?php include TEMPLATE_DIR . "includes/boxes/top-links.php"; ?>

	<div class="spacing-block clearfix" style="height: 60px"></div>
	<?php include TEMPLATE_INCLUDES . "boxes/skills2.php"; ?>
	
	<?php include TEMPLATE_INCLUDES . "boxes/news.php"; ?>
	
	<?php include TEMPLATE_INCLUDES . "boxes/portfolio.php"; ?>
	
	<?php include TEMPLATE_INCLUDES . "boxes/action-box.php"; ?>
</div>	