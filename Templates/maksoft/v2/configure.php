<?php
	define("TEMPLATE_NAME", "maksoft/v2");
	define("TEMPLATE_DIR", "Templates/".TEMPLATE_NAME."/");
	#define("TEMPLATE_PATH", $o_page->scheme.$_SERVER['SERVER_NAME']."/Templates/".TEMPLATE_NAME."/");
	define("TEMPLATE_PATH", "https://maksoft.net/Templates/".TEMPLATE_NAME."/");
	define("TEMPLATE_IMAGES", $o_page->scheme."www.maksoft.net/".TEMPLATE_DIR."assets/img/");
	define("TEMPLATE_DEMO", "Templates/".TEMPLATE_NAME."/demo/");
	define("TEMPLATE_INCLUDES",  TEMPLATE_DIR . "includes/");
	define("TEMPLATE_IMAGES_DEMO", $o_page->scheme."www.maksoft.net/".TEMPLATE_DIR."demo/img/");
	if($user->AccessLevel > 0)
		define("DEMO_MODE", false);
	else
		define("DEMO_MODE", false);

	//define some default values
	$tmpl_config = array(
		"default_logo" => TEMPLATE_IMAGES."logo.png",
		"default_logo_width" =>($o_site->get_sConfigValue('TMPL_LOGO_SIZE'))? $o_site->get_sConfigValue('TMPL_LOGO_SIZE') : "300",
		"default_main_width" => "100%",
		"footer" => array(
								TEMPLATE_DIR . "boxes/news.php"
							),		
		"date_format"=> "d.m.Y"
	);
	
	if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->get_sImageDIr()."/".basename($o_site->_site['Logo']);

function current_season() {
       // Locate the icons
       $icons = array(
               "spring" => "". TEMPLATE_IMAGES ."logo-standart.png",
               "summer" => "". TEMPLATE_IMAGES ."logo-standart.png",
               "autumn" => "". TEMPLATE_IMAGES ."logo-christmas.png",
               "winter" => "". TEMPLATE_IMAGES ."logo-standart.png"
       );

       // What is today's date - number
       $day = date("z");

       //  Days of spring
       $spring_starts = date("z", strtotime("March 21"));
       $spring_ends   = date("z", strtotime("June 20"));

       //  Days of summer
       $summer_starts = date("z", strtotime("June 21"));
       $summer_ends   = date("z", strtotime("September 22"));

       //  Days of autumn
       $autumn_starts = date("z", strtotime("December 01"));
       $autumn_ends   = date("z", strtotime("December 31"));

       //  If $day is between the days of spring, summer, autumn, and winter
       if( $day >= $spring_starts && $day <= $spring_ends ) :
               $season = "spring";
       elseif( $day >= $summer_starts && $day <= $summer_ends ) :
               $season = "summer";
       elseif( $day >= $autumn_starts && $day <= $autumn_ends ) :
               $season = "autumn";
       else :
               $season = "winter";
       endif;

       $image_path = $icons[$season];

       echo $image_path;
}
?>
