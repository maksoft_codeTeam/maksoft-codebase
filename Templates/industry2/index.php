<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title><?php echo("$Title"); ?></title>
<?php

$o_site->print_sConfigurations();

include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/industry2/");
define("DIR_TEMPLATE_IMAGES", "http://maksoft.net/".DIR_TEMPLATE."images/");
define("DIR_MODULES","web/admin/modules/");

if(!isset($view_submenu)) $view_submenu = "open";

//load CSS style
if(!empty($Site->css_url))
	echo "<link href='".$Site->css_url."' rel='stylesheet' type='text/css'>";
else if($Site->css!=0 && !empty($Site->cssDef))
	echo $Site->cssDef; 
else
	echo "<link href='http://www.maksoft.net/css/industry/base_style.css' rel='stylesheet' type='text/css'>";

//top_menu
$top_menu = $o_page->get_pSubpages(0, "sort_n ASC", "p.toplink = 1");
//vip links
$vip_menu = $o_page->get_pSubpages(0, "RAND() LIMIT 3", "p.toplink = 4 AND im.image_src != ''");
?>
<body>
<?php 
if(isset($tyxo) && $tyxo != ""){
	echo '<script  type="text/javascript">
<!--
d=document;
d.write(\'<a href="http://www.tyxo.bg/?'.$tyxo.'" title="'.$Title.'" target=" blank"><img width="1" height="1" border="0" alt="'.$Title.'"\');
d.write(\' src="http://cnt.tyxo.bg/'.$tyxo.'?rnd=\'+Math.round(Math.random()*2147483647));
d.write(\'&sp=\'+screen.width+\'x\'+screen.height+\'&r=\'+escape(d.referrer)+\'" /><\/a>\');
//-->
</script>
<noscript><a href="http://www.tyxo.bg/?'.$tyxo.'" title="'.$Title.'" target="_blank"><img src="http://cnt.tyxo.bg/'.$tyxo.'" width="1" height="1" border="0" alt="'.$Title.'"/></a></noscript>
	';
}

?>


<?php 
if (strlen($_uacct)>0) {
 echo("
	<script src=\"http://www.google-analytics.com/urchin.js\" type=\"text/javascript\">
	</script>
	<script type=\"text/javascript\">
	_uacct = \"$_uacct\";
	 urchinTracker();
	</script>
 "); 
}
?>
<div align="center">
<table class="main_table" width="810" border="0" cellpadding="0" cellspacing="1" align="center">
	<tr>
		<td colspan="3">	
		<?php
			include "header.php";
		?>		
		</td>
	</tr>
	<tr><td colspan="3">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="top_line">
			<tr>
				<td width="203" rowspan="2" align="center" valign="top">
					<a href="http://<?=$o_site->get_sURL()?>/page.php?n=<?=$o_site->get_sStartPage()?>&amp;SiteID=<?=$o_site->SiteID?>" class="special_button" style="margin-left:25px;"><img src="<?=DIR_TEMPLATE_IMAGES?>icon_home.gif" border="0" alt=""></a>
					<a href="mailto:<?=$o_site->get_sEmail()?>" class="special_button"><img src="<?=DIR_TEMPLATE_IMAGES?>icon_contacts.gif" border="0" alt=""></a>
					<a href="page.php?n=145&amp;SiteID=<?=$o_site->SiteID?>" class="special_button"><img src="<?=DIR_TEMPLATE_IMAGES?>icon_sitemap.gif" border="0" alt=""></a>
					<a href="/admin" class="special_button login"><img src="<?=DIR_TEMPLATE_IMAGES?>icon_login.gif" border="0" alt=""></a>
				</td>
				<td width="32" height="34"></td>
				<td width="575" valign="middle"></td>
			</tr>
			<tr>
			  <td width="32" height="34"></td>
		      <td width="575" valign="bottom" align="right">
			  	<div id="top_menu">
					 <?php
						for($i=0; $i<count($top_menu); $i++)
							{
								if($i>0) echo " / ";
								echo "<a href=\"".$top_menu[$i]['page_link']."\">".$top_menu[$i]['Name']."</a>";
							}
					?>		
				</div>
			  </td>
		  </tr>
		</table>
	<tr>
		<td width="183"></td>
		<td width="32"></td>
		<td></td>
	</tr>
	<tr>
		<td width="203" valign="top" class="menu-bg">
		<table class="menu" width="202" height="100%">
		<tr><td valign="top">
          <?php include "column_left.php"; ?>


		</table>
	 </td>
	    <td width="32"><img src="http://www.maksoft.net/web/images/pixel.gif" width="32"></td>
	    <td width="575" height="100%" valign="top" class="industry-bg">
		<?php
		//enable tag search / tag addresses
        if(strlen($search_tag)>2)
			{
				$o_page->print_search($o_site->do_search(iconv("UTF-8","CP1251",$search_tag), 10)); 
				$keyword = $search_tag; 
				include("selected_sites.php"); 
			}
		//enable site search
		elseif(strlen($search)>2)
			$o_page->print_search($o_site->do_search($search, 10)); 
		else
			{	

				if($o_page->n == $o_site->get_sStartPage())
					include_once "home.php";
				else
					include_once "main.php";
			}
		?>
</td>
	</tr>
	<tr>
		<td>
		<td colspan="2" height="5">

		</td>
	</tr>
	<tr>
		<td colspan="3" class="footer" align="center"><?php echo "$Site->copyright&nbsp;-&nbsp;".date("Y").""; ?></td>
	</tr>
	<tr>
		<td colspan="3" align="center" id="nav_links"><?=$o_page->print_pNavigation()?></td>
	</tr>
</table>
</div>
</body>
</html>
