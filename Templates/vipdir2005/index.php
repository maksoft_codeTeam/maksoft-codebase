<!DOCTYPE HTML>
<html lang="<?php echo($Site->language_key); ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<title><?=$Title?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?=$Title?></title>
    <?php
		//include template configurations
		include("Templates/base/bootstrap_desktop/config.php");		
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<!-- Optional theme -->
	<?php
	//tmpl osnoven - black white
		$cssCode=$o_site->_site["css"];
		$cssquery = $o_page->db_query("SELECT css_url FROM CSSs where cssID=".$cssCode);
		$result = mysqli_fetch_array($cssquery);
		$cssHref = $result["css_url"];
	?>
	<link rel="stylesheet" id="stylechange" href="<?=$cssHref;?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="http://www.maksoft.net/<?=TEMPLATE_DIR;?>css/style.css" id="layout-style" rel="stylesheet" type="text/css" />
	<!-- Latest compiled and minified jQuery -->
	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="http://www.maksoft.net/<?=TEMPLATE_DIR;?>js/script.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,900,700italic&subset=cyrillic,cyrillic-ext,latin' rel='stylesheet' type='text/css'>
	<script src="http://www.maksoft.net/<?=TEMPLATE_DIR;?>fws-slider2/js/css3-mediaqueries.js"></script>
	<script src="http://www.maksoft.net/<?=TEMPLATE_DIR;?>fws-slider2/js/fwslider.js"></script>
	<link rel="stylesheet" href="http://www.maksoft.net/<?=TEMPLATE_DIR;?>fws-slider2/css/fwslider.css">
</head>
<?php

if(defined("TMPL_BODY_BACKGROUND")){
	?>
	<style media="all">
			<?
			//CUSTOM TEMPLATE BACKGROUND
			if(defined("TMPL_BODY_BACKGROUND")) 
			{
			?>
				body{
					background: url(<?=TMPL_BODY_BACKGROUND;?>);
					background-size: auto 100%;
					background-attachment:fixed;
				}
	</style>
	<?php
}
}
?>
<body>
<?php
if(($user->AccessLevel>0) && ($o_page->n == $o_page->_site['StartPage']) ) {
	?>
	<a id="fixed_css" href="#" class="btn btn-primary btn-lg fixed_css" role="button" title="��������� ����� �� �����" data-toggle="tooltip" data-placement="left"><span class="fa fa-bars"></span></a>
	
	<div id="hiddenadminbar">
		<div class="panel panel-default">
		  <div class="panel-heading">�����������</div>
		  <div class="panel-body">
		   <form enctype="multipart/form-data" method="post" action="">
			<div class="row marin0 margin-bottom-5 padding10">
				<span class="btn btn-primary hideopen" data-open="design"><i class="fa fa-paint-brush"></i> ������</span>
				<div id="design" class="hided well">
				<?php
				$i=0;
				$SiteTempl = $o_site->_site["Template"];
				$query = $o_site->db_query("SELECT * FROM CSSs WHERE css_group = ".$SiteTempl);
				while($row = mysqli_fetch_array($query)){
					$i++;
					?>
					<div class="radio">
						<label>
						  <input type="radio" name="optionsRadios" id="optionsRadios<?=$i;?>" value="<?=$row["css_url"];?>" <?php if($row['cssID']==$cssCode){ echo "checked";} ?>>
						  <?=$row["cssName"];?>
						  <input type="hidden" class="csside" value="<?=$row['cssID'];?>"/>
						  <?php
						  
							$colors = explode(";", $row["color_scheme"]);
						  foreach($colors as $color){
							  ?>
								<div class="label" style="background-color:<?=$color;?>; white-space: pre; border:1px solid black;"> </div>
							  <?php
						  }
						  ?>
						  
						</label>
					</div>
					<?php
				}
				?>
				</div>
			</div>
			<div class="row marin0 margin-bottom-5 padding10">
				<span class="btn btn-primary hideopen" data-open="layout"><i class="fa fa-columns"></i> ������</span>
				<div class="clearfix"></div>
				<div id="layout" class="hided well">
					<div class="col-xs-12 col-sm-6 layouts
					<?php
					if(!defined("TMPL_CSS_LAYOUT")){
						echo "selected";
					}
					if(defined("TMPL_CSS_LAYOUT") && TMPL_CSS_LAYOUT=="both-sidebars"){
						echo "selected";
					}
					?>
					">
						<img src="http://www.maksoft.net/<?=TEMPLATE_DIR;?>images/layout/both-sidebars.png"/>
						<input type="hidden" class="choise" value="both-sidebars"/>
					</div>
					<div class="col-xs-12 col-sm-6 layouts
					<?php
					if(defined("TMPL_CSS_LAYOUT") && TMPL_CSS_LAYOUT=="left-sidebar"){
						echo "selected";
					}
					?>
					">
						<img src="http://www.maksoft.net/<?=TEMPLATE_DIR;?>images/layout/left-sidebar.png"/>
						<input type="hidden" class="choise" value="left-sidebar"/>
					</div>
					<div class="col-xs-12 col-sm-6 layouts
					<?php
					if(defined("TMPL_CSS_LAYOUT") && TMPL_CSS_LAYOUT=="right-sidebar"){
						echo "selected";
					}
					?>
					">
						<img src="http://www.maksoft.net/<?=TEMPLATE_DIR;?>images/layout/right-sidebar.png"/>
						<input type="hidden" class="choise" value="right-sidebar"/>
					</div>
					<div class="col-xs-12 col-sm-6 layouts
					<?php
					if(defined("TMPL_CSS_LAYOUT") && TMPL_CSS_LAYOUT=="no-sidebar"){
						echo "selected";
					}
					?>
					">
						<img src="http://www.maksoft.net/<?=TEMPLATE_DIR;?>images/layout/no-sidebar.png"/>
						<input type="hidden" class="choise" value="no-sidebar"/>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="row marin0 margin-bottom-5 padding10">
				<span class="btn btn-primary hideopen" data-open="logomenu"><i class="fa fa-yelp"></i> ����</span>
				<div class="clearfix"></div>
				<div id="logomenu" class="hided well">
					<span class="label" rel="box_logo">����</span><br clear="all"> 
						<div id="box_logo">
							<img src="<?=$tmpl_config['default_logo']?>" width="180">
							<br clear="all">
							<span id="text_logo_position">0x0</span> / <span id="text_logo_size"></span>
							<input type="hidden" name="TMPL_LOGO_POSITION" id="TMPL_LOGO_POSITION" value="<?=$o_site->get_sConfigValue("TMPL_LOGO_POSITION")?>">
							<input type="hidden" name="TMPL_LOGO_SIZE" id="TMPL_LOGO_SIZE" value="">
							<input type="checkbox" name="remove_logo" value="1"> �������� ������
							<br clear="all">
							<input type="file" name="site_logo" style="width: 90%">
						</div>
					<div class="clearfix"></div>
				</div>
			</div>
		  </div>
		  <div class="panel-footer">
				<input type="hidden" name="new_css_id" id="new_css_id" value="<?=$cssCode;?>"/>
				<input type="hidden" name="TMPL_CSS_LAYOUT" id="TMPL_CSS_LAYOUT" value="<?php if(defined("TMPL_CSS_LAYOUT")){ echo TMPL_CSS_LAYOUT; }else{ echo "both-sidebars"; }?>"/>
				<button class="btn btn-primary" type="submit">
				  ������
				</button>
			</form>
		  </div>
		</div>
	</div>
	
	<script>
	$(document).ready(function(){
		$("#hiddenadminbar input[type='radio']").on("click", function(){
			if($(this).is(':checked')){
				var newstyle = $(this).val();
				$("#stylechange").attr("href", newstyle);
				var css_id = $(this).parent().find(".csside").val();
				$("#new_css_id").val(css_id);
			}
		})
		$(".hideopen").click(function(){
			var dataopen = $(this).attr("data-open");
			$("#"+dataopen).toggle();
		});
		$(".layouts").on("click", function(){
			$(".layouts.selected").removeClass("selected");
			$(this).addClass("selected");
			var choosen = $(this).find(".choise").val();
			$("input#TMPL_CSS_LAYOUT").val(choosen);
		})
	});
	</script>
	<?php
	if(isset($_POST["new_css_id"])){
		//update csss
		$new_css_id = $_POST["new_css_id"];
		$idto=$o_site->SiteID;
		$o_page->db_query("UPDATE Sites SET css = '".$new_css_id."' WHERE SitesID = '".$idto."'");
	}
	//set template layout
	if(isset($_POST['TMPL_CSS_LAYOUT']) && $_POST['TMPL_CSS_LAYOUT'])
	{
		//$css_content.= "body {background: url(\"".$body_background."\") 50% 0% no-repeat;}";
		$o_site->set_sConfiguration("TMPL_CSS_LAYOUT", $_POST['TMPL_CSS_LAYOUT'], $o_site->SiteID, $o_site->_site['Template']);
	}
	
	//upload logo
	if(strlen($_FILES['site_logo']['name'])>=5)
	{
		include "web/admin/upload_file.php";
		//print_r($_FILES['site_logo']);
		$imageNo = upload_file($imageID, "Logo", $Site, $_FILES['site_logo']['name'], $_FILES['site_logo']['tmp_name'], $_FILES['site_logo']['size'], $_FILES['site_logo']['type'], 2, $o_site->get_sImageDir('ID'));	
		$o_site->db_query("UPDATE Sites SET Logo = '".$_FILES['site_logo']['name']."' WHERE SitesID = '".$o_site->SiteID."' LIMIT 1");
	}
	//remove logo
	if($_POST['remove_logo']){
		$o_site->db_query("UPDATE Sites SET Logo = '' WHERE SitesID = '".$o_site->SiteID."' LIMIT 1");
	}
}
?>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">����</h4>
      </div>
      <div class="modal-body">
        <div class="form-login">
		<form name="login" method="post" id="logform" action="http://www.maksoft.net/web/admin/login.php">
			<input name="n" type="hidden" id="n" value="<?=$o_page->n?>">
			<input name="SiteID" type="hidden" id="SiteID" value="<?=$o_site->SiteID?>">
            <input type="text" id="username" name="username" class="form-control input-sm chat-input" placeholder="���" />
            </br>
            <input type="password" id="pass" name="pass" class="form-control input-sm chat-input" placeholder="������" />
            </br>
        </div>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">�����</button>
        <span class="group-btn">     
			<a onclick="document.getElementById('logform').submit();" class="btn btn-primary btn-md">���� <i class="glyphicon glyphicon-log-in"></i></a>
        </span>
		</form>
      </div>
    </div>
  </div>
</div>
<?php
include_once TEMPLATE_DIR."mainmenu.php";

		
if($o_page->_page['SiteID'] == $o_site->_site['SitesID'] ){
	$pBanners = $o_page->get_pBanners();
	if($o_page->_page['n'] == $o_site->_site['StartPage']){
		if(count($pBanners)>1){
			$i=0;
			?>
			<div id="fwslider">
				<div class="slider_container">
				<?php
				foreach($pBanners as $banner){
					$i++;
					$src = $o_site->primary_url.$banner['image_src'];
					$bannerN = $banner["n"];
					
					$padeSlide = $o_page->get_page($bannerN);
					$string = crop_text(strip_tags($padeSlide['textStr']));
					$string = strip_tags(substr($string, 0, 40));
					$title = $banner["imageName"];
					
					?>
					<div class="slide">
						<?=$o_page->print_timthumb($src, "", "1500", "200");?>
						<div class="slide_content">
							<div class="slide_content_wrap">
							<?php
							if($bannerN!=$o_site->_site["StartPage"]){
								?>
								<a href="http://<?=$o_site->_site["primary_url"]."/?n=".$bannerN;?>">
									<h4 class="title"><?=$title;?></h4>
								</a>
								<?php
								if($string!=""){
									?>
									<p class="description"><?=$string;?></p>
									<a class="readmore" href="http://<?=$o_site->_site["primary_url"]."/?n=".$bannerN;?>">
										<?=$o_site->_site["MoreText"];?>
									</a>
									<?php
								}
							}
							?>
							</div>
						</div>
					</div>
					<?php
				}
				?>
				</div>
				<div class="timers"></div>
				<div class="slidePrev"><span></span></div>
				<div class="slideNext"><span></span></div>
			</div>
			<?php
		}else{
			if($pBanners[0]['image_src']!=''){
				$src = $o_site->primary_url.$pBanners[0]['image_src'];
				?>
				<div class="row margin0">
					<div class="col-xs-12 padding0">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="item active">
									<?=$o_page->print_timthumb($src, "", $tmpl_config["default_banner_width"], "150");?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		} ?>
	<div class="container">
		<div id="navbar">
			<ol class="breadcrumb">
				<?php $o_page->print_pNavigation();?>
			</ol>
		</div>
		<?php
		include_once TEMPLATE_DIR."left_sidebar.php";
		include_once TEMPLATE_DIR."home.php";
		include_once TEMPLATE_DIR."right_sidebar.php";
	}else{
		// $src =($o_page->_page["image_src"]);
		$src = $o_site->primary_url.$pBanners[0]['image_src'];
		if(!$src==''){ ?>
		<div class="row margin0">
			<div class="col-xs-12 padding0">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="item active">
							<?php $o_page->print_timthumb($src, "", $tmpl_config["default_banner_width"], "150");?>
						</div>
					</div>
				</div>
			</div>
		</div>
			<?php
		}else{
			// $src=$tmpl_config['default_banner'];
			$src = $o_site->primary_url.$pBanners[0]['image_src'];
			?>
		<div class="row margin0">
			<div class="col-xs-12 padding0">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="item active">
							<?php $o_page->print_timthumb($src, "", $tmpl_config["default_banner_width"], "150");?>
						</div>
					</div>
				</div>
			</div>
		</div>
			<?php
		}
		?>
	<div class="container">
		<div id="navbar">
			<ol class="breadcrumb">
				<?php $o_page->print_pNavigation();?>
			</ol>
		</div>
		
		<?php
		include_once TEMPLATE_DIR."left_sidebar.php";
		include_once TEMPLATE_DIR."main.php";
		include_once TEMPLATE_DIR."right_sidebar.php";
	}
}else{
		//$src =($o_page->_page["image_src"]);
		$src = $o_site->primary_url.$pBanners[0]['image_src'];
		if(!$src==''){ ?>
		<div class="row margin0">
			<div class="col-xs-12 padding0">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="item active">
							<?php $o_page->print_timthumb($src, "", $tmpl_config["default_banner_width"], "150");?>
						</div>
					</div>
				</div>
			</div>
		</div>
			<?php
		}
		?>
	<div class="container">
		<div id="navbar">
			<ol class="breadcrumb">
				<?php $o_page->print_pNavigation();?>
			</ol>
		</div>
		<?php
		include_once TEMPLATE_DIR."admin.php";
}
?>
<div class="clearfix">
</div>
<?php
include_once TEMPLATE_DIR."footer.php";

if(defined('GOOGLE_TRACKING_CODE'))   { include_once("web/admin/modules/google/analitycs.php"); }
if(defined('FB_COMMENTS_APP_ID')) {
	?>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/bg_BG/all.js#xfbml=1&appId=<?=FB_COMMENTS_APP_ID?>";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
	<?php
}
?>
</div>
<?php include( "Templates/footer_inc.php" ); ?>
</body>
</html>