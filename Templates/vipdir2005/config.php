<?php
	//template base/tmpl_001 configurations
	
	define("TEMPLATE_NAME", "bootstrap_desktop");
	define("TEMPLATE_DIR", "Templates/base/".TEMPLATE_NAME."/");
	define("TEMPLATE_IMAGES", "http://www.maksoft.net/".TEMPLATE_DIR."images/");
	
	//show what options are available for config
	$tmpl_config_support = array(
		"change_theme"=>true,					// full theme support
		"change_layout"=>true,					// layout support
		"change_banner"=>true,				// banner support
		"change_css"=>true,						// css support
		"change_background"=>true,		// background support
		"change_fontsize"=>true,				// fontsize support
		"change_max_width"=>true,			// site width support
		"change_menu_width"=>false,		// site menu width support
		"change_main_width"=>true,		// page content width support
		"change_logo"=>true,						// logo support
		"column_left" => true, 						// column left support
		"column_right" => true,					// column right support
		"drop_down_menu"=> true			// drop-down menu support
	);
	//define some default values
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 5");
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5", "p.n != '".$o_site->_site['StartPage']."'");
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5", "p.preview>30");
	$last_visited = $o_page->get_pSubpages(0, "p.sort_n DESC LIMIT 5");
	
	$tmpl_config = array(
		"default_max_width"=> ($o_site->get_sConfigValue('CMS_MAX_WIDTH'))? $o_site->get_sConfigValue('CMS_MAX_WIDTH') : "960",
		"default_main_width"=>($o_site->get_sConfigValue('CMS_MAIN_WIDTH'))? $o_site->get_sConfigValue('CMS_МAIN_WIDTH') : "500",
		"default_menu_width"=>($o_site->get_sConfigValue('CMS_MENU_WIDTH'))? $o_site->get_sConfigValue('CMS_МENU_WIDTH') : "180",
		"default_banner_width" => ($o_site->get_sConfigValue('CMS_MAX_WIDTH'))? $o_site->get_sConfigValue('CMS_MAX_WIDTH') : "960",
		"default_banner" => TEMPLATE_IMAGES."banner.jpg",
		"default_logo" => TEMPLATE_DIR."images/logo.png",	
		"default_logo_width" =>($o_site->get_sConfigValue('TMPL_LOGO_SIZE'))? $o_site->get_sConfigValue('TMPL_LOGO_SIZE') : "180",
		"date_format"=> "d-m-Y",
		"column_left" => array(
								TEMPLATE_DIR . "menu.php", 
								TEMPLATE_DIR . "login.php", 
								TEMPLATE_DIR . "social_links.php"
								),
		"column_right" => array(
								TEMPLATE_DIR . "boxes/second_links.php",
								TEMPLATE_DIR . "boxes/news.php",
								TEMPLATE_DIR . "boxes/last_visited.php", 
								TEMPLATE_DIR . "boxes/shopping_cart.php", 
								TEMPLATE_DIR . "boxes/banners.php"
								),
		"footer" => array(
								TEMPLATE_DIR . "boxes/foot_first_col.php",
								TEMPLATE_DIR . "boxes/foot_second_col.php",
								TEMPLATE_DIR . "boxes/foot_third_col.php", 
								TEMPLATE_DIR . "boxes/foot_fourth_col.php"
								),
	);
	
	if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->get_sImageDIr()."/".basename($o_site->_site['Logo']);

?>