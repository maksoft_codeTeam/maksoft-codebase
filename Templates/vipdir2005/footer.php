<a id="back-to-top" href="#" class="btn btn-default btn-sm back-to-top" role="button"><span class="glyphicon glyphicon-chevron-up"></span></a>
<hr/>
<footer>
	<div class="panel panel-default">
		<div class="panel-heading">
			- <a href="http://<?=$o_site->_site['primary_url'];?>"><?=$o_site->_site['Name'];?></a> -
		</div>
		<div class="panel-body">
			<div class="row">
				<?php
				foreach($tmpl_config["footer"] as $block){
					?>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<?php include $block; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">
					<?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="" target="_blank">Netservice</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6">
					<div class="pull-right">��������� <a href="http://maksoft.bg/">�������</a></div>
				</div>
			</div>
		</div>
	</div>
</footer>