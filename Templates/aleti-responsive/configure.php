<?php
	//template /aleti/v2 configurations

	define("TEMPLATE_DIR", "Templates/aleti-responsive/");
	define("TEMPLATE_IMAGES", "http://www.maksoft.net/Templates/aleti-responsive/images/");
	/*
	//show what options are available for config
	$tmpl_config_support = array(
		"change_theme"=>true,					// full theme support
		"change_layout"=>true,					// layout support
		"change_banner"=>true,				// banner support
		"change_css"=>true,						// css support
		"change_background"=>true,		// background support
		"change_fontsize"=>true,				// fontsize support
		"change_max_width"=>true,			// site width support
		"change_menu_width"=>false,	// site menu width support
		"change_main_width"=>true,		// page content width support
		"change_logo"=>true,						// logo support
		"column_left" => true, 						// column left support
		"column_right" => true,					// column right support
		"drop_down_menu"=> true			// drop-down menu support
	);
	*/
	//define some default values
	$tmpl_config = array(
		"default_max_width"=> (defined('CMS_MAX_WIDTH'))? CMS_MAX_WIDTH : "950",
		"default_main_width"=>"750",
		"default_menu_width"=>"150",
		"default_banner_width" => (defined('CMS_MAX_WIDTH'))? CMS_MAX_WIDTH : "950",
		"default_banner" => TEMPLATE_IMAGES."banner.jpg",
		"default_logo" => TEMPLATE_IMAGES."logo.png",
		"default_logo_width" =>($o_site->get_sConfigValue('TMPL_LOGO_SIZE'))? $o_site->get_sConfigValue('TMPL_LOGO_SIZE') : "180",
		"column_right" => array(
								TEMPLATE_DIR . "boxes/main_links.php", 
								TEMPLATE_DIR . "menu.php"
								),
		"footer" => array(
								TEMPLATE_DIR . "boxes/news.php"
							),		
		"date_format"=> "d-m-Y"
	);
	
	if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->get_sImageDIr().$o_site->_site['Logo'];
?>