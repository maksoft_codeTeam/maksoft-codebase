<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<!--	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">-->
    <title><?=$Title?></title>
    <?php
		//include template configurations
		include("Templates/aleti-responsive/configure.php");		
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css' />
    <link href="http://www.maksoft.net/<?=TEMPLATE_DIR?>layout.css" id="layout-style" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/<?=TEMPLATE_DIR?>base_style.css" id="base-style" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/<?=TEMPLATE_DIR?>content_styles.css" id="content-style" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/css/admin_classes.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="lib/jquery-ui/themes/base/jquery.ui.all.css">
	<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.core.js"></script>
	<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.widget.js"></script>
	<script src="http://www.maksoft.net/lib/jquery-ui/ui/jquery.ui.tabs.js"></script>
    
    		<!-- Stylesheets -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/bootstrap/css/bootstrap.min.css">
<!--		<link rel="stylesheet" href="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/slidebars/slidebars.min.css">-->
		<link rel="stylesheet" href="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/css/style.css">
<!--        <link rel="stylesheet" href="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/css/responsive.css">-->
		
		<!-- Shims -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

</head>
<?php
	//get site news
	$news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 5");
	
	//get most viewed pages
	$most_viewed_pages = $o_page->get_pSubpages(0, "p.preview DESC LIMIT 5");

	//get last added pages
	$last_added_pages = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT 5");
	
	//products
	$n_products = 188606;
	$products = $o_page->get_pSubpages($n_products);
	
	//main links
	$main_links = $o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 4", "p.toplink=4");
	
	//footer links
	$footer_links = array(188772, 188773, 188774, 188771);
?>
<body>
<!--		
		<nav class="navbar navbar-default navbar-fixed-top sb-slide hidden-lg hidden-md hidden-sm" role="navigation">
		
			<div class="sb-toggle-left navbar-left">
				<div class="navicon-line"></div>
				<div class="navicon-line"></div>
				<div class="navicon-line"></div>
			</div>
			
	
			<div class="sb-toggle-right navbar-right">
				<i class="fa fa-search fa-2x icon-search"></i>
			</div>
			
			<div class="container">
	
				<div id="logo" class="navbar-left">
					<a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>"><img src="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/images/logo.png" alt="Aleti Furniture" width="66" height="auto"></a>
				</div>
				

			</div>
		</nav>-->

		<!-- Site -->
		<div id="sb-site">
<div id="site_container">
    <div id="header">
        <div class="header-content" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
        	<?php include TEMPLATE_DIR."header.php"?>
        </div>
    </div>
	<div id="page_container">
        <div class="main-content">
        <?php
            //enable tag search / tag addresses
            if(strlen($search_tag)>2)
                {
                    $o_page->print_search($o_site->do_search(iconv("UTF-8","CP1251",$search_tag), 10)); 
                    //$keyword = $search_tag; 
                    //include("selected_sites.php"); 
                }
            //enable site search
            elseif(strlen($search)>2) {
			    echo "<br /><br /><br />";
                $o_page->print_search($o_site->do_search($search, 10)); 
          }  else
                {	
			if($o_page->_page['n'] == $o_site->_site['StartPage'])
				include TEMPLATE_DIR."home.php";
			elseif($o_page->_page['SiteID'] == $o_site->_site['SitesID'])
				include TEMPLATE_DIR."main.php";
			else
				include TEMPLATE_DIR."admin.php";
				}
		?>
        </div>
    </div>
    <div id="footer">
    	<div class="footer-content" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
		<?php
			include TEMPLATE_DIR . "footer.php";
		?>
        </div>
        <div class="copyrights"><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a>, web design <a href="http://www.pmd-studio.com" target="_blank" title="��� ������">pmd-studio</a></div>
    </div>
</div>
      </div>  
		<!-- Slidebars -->
<!--		<div class="sb-slidebar sb-left">
	<nav>
		<ul class="sb-menu">
			<li><img src="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/images/logo.png" alt="Aleti Furniture" width="66" height="auto"></li>
          <li class="sb-close"><a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>"><i class="fa fa-home fa-2x"></i></a></li>
          
              <?php
		$top_menu_links = $products;
        for($i=0; $i<count($top_menu_links); $i++)
            echo "<li class=\"sb-close\"><a href=\"".$o_page->get_pLink($top_menu_links[$i]['n'])."\" title=\"".$top_menu_links[$i]['title']."\" class=\"".(($o_page->n == $top_menu_links[$i]['n'])? "selected":"")."\">".$top_menu_links[$i]['Name']."</a></li>";
	?>
          

		</ul>
	</nav>
</div><div class="sb-slidebar sb-right sb-style-overlay">
	<aside id="about-me">
    <div class="search-icons">
        <form action="" class="search-form">
		<input name="SiteID" type="hidden" value="<?=$SiteID?>">
		<input name="res_no" type="hidden" id="res_no" value="<?=$n?>">
        <input type="text" value="" placeholder="<?=$o_site->get_sSearchText()?>" name="search">
        <button type="submit" class="btn"><i class="fa fa-search fa-2x"></i></button>
        </form>
    </div>
	</aside>
</div>-->
		<!-- jQuery -->
<!--		<script src="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->

<!--		
		<script src="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/bootstrap/js/bootstrap.min.js"></script>
		
	
		<script src="http://www.maksoft.net/<?=TEMPLATE_DIR?>assets/slidebars/slidebars.min.js"></script>
		<script>
			(function($) {
				$(document).ready(function() {
					// Initiate Slidebars
					$.slidebars();
				});
			}) (jQuery);
		</script>-->
        <?php 
            $tyxo = $o_page->get_sConfigValue('tyxo_counter');
            if(!is_null($tyxo)){
            ?>
                <script>
                    (function(i,s,o,g,r,a,m){i['TyxoObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','//s.tyxo.com/c.js','tx');
                    tx('create', "<?php echo $tyxo; ?>");
                    tx('pageview');
                </script>
            <?php
                }
            ?>
</body>
</html>
