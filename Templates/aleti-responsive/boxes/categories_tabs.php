<div id="categories-tabs">
	<?php
		$categories = $o_page->get_pSubpages($o_page->get_pParent($o_page->_page['ParentPage']));
		//$categories = $products;
		$tab_content = "";
		
		//print tabs
		echo "<ul>";
		for($i=0; $i<count($categories); $i++)
			{
				echo "<li><a href=\"#category-".$categories[$i]['n']."\">";
				echo $o_page->get_pName($categories[$i]['n']);
				echo "</a></li>";
			}
		echo "</ul>";
		
		//print tabs content
		for($i=0; $i<count($categories); $i++)
			{
				echo "<div id=\"category-".$categories[$i]['n']."\">";
				$category_pages = $o_page->get_pSubpages($categories[$i]['n'], "RAND() LIMIT 4");
				for($j=0; $j<count($category_pages); $j++)
					{
						echo "<div class=\"category-preview\">";
						echo "<a href=\"".$o_page->get_pLink($category_pages[$j]['n'])."\">";
						$o_page->print_pImage(180, NULL, NULL, $category_pages[$j]['n']);
						echo "</a><br clear=\"all\"><br clear=\"all\">";
						$o_page->print_pName(true, $category_pages[$j]['n']);
						echo "<br clear=\"all\"><br clear=\"all\"></div>";
					}
				echo "<br clear=\"all\"></div>";
			}
	?>
</div>