<div class="banner">
	<div id="social-links">
                      <?php if(defined('CMS_FACEBOOK_PAGE_LINK') || $user->AccessLevel >0) { ?><a href="<?=(defined("CMS_FACEBOOK_PAGE_LINK")) ? CMS_FACEBOOK_PAGE_LINK:"#"?>"><div class="icon facebook"></div></a> <?php }?>
				<?php if(defined('CMS_TWITTER_PAGE_LINK') || $user->AccessLevel >0) { ?><a href="<?=(defined("CMS_TWITTER_PAGE_LINK")) ? CMS_TWITTER_PAGE_LINK:"#"?>"><div class="icon twitter"></div></a><?php }?>
				<?php if(defined('CMS_GPLUS_PAGE_LINK') || $user->AccessLevel >0) { ?> <a href="<?=(defined("CMS_GPLUS_PAGE_LINK")) ? CMS_GPLUS_PAGE_LINK:"#"?>" rel="publisher" ><div class="icon google"></div></a> <?php }?>
    </div>
	<div id="box-search">
    	<form action="" class="search-form">
		<input name="SiteID" type="hidden" value="<?=$SiteID?>">
		<input name="res_no" type="hidden" id="res_no" value="<?=$n?>">
        <input type="text" value="" placeholder="<?=$o_site->get_sSearchText()?>" name="search">
        </form>
    </div>
	<a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>" class="home" title="<?=$o_page->get_pTitle($o_site->_site['StartPage'])?>"><img src="<?=TEMPLATE_IMAGES?>logo_aleti.png" ></a>
</div>
<div id="top-menu">
	<div class="top-menu-content">
    <a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>" class="home" title="<?=$o_page->get_pTitle($o_site->_site['StartPage'])?>"></a>
    <?php
		$top_menu_links = $products;
        for($i=0; $i<count($top_menu_links); $i++)
            echo "<a href=\"".$o_page->get_pLink($top_menu_links[$i]['n'])."\" title=\"".$top_menu_links[$i]['title']."\" class=\"".(($o_page->n == $top_menu_links[$i]['n'])? "selected":"")."\">".$top_menu_links[$i]['Name']."</a>";
	?>
    </div>
</div>
<div id="navbar"><?=$o_page->print_pNavigation();?></div>
