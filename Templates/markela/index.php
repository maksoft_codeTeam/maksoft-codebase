<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title><?php echo("$Title");?></title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<?php
include("Templates/meta_tags.php"); 
define ("DIR_TEMPLATE","Templates/markela/");
define("DIR_TEMPLATE_IMAGES"," http://maksoft.net/Templates/markela/images/");
define("DIR_MODULES","web/admin/modules/");

//set the language
switch($lng)
{
	case "EN":
		{
			include DIR_TEMPLATE."english.php";
		}
	case "BG":
	default:
		{
			include DIR_TEMPLATE."bulgarian.php";
		}
}

function head_title($title)
{
	$first_word= explode(" ", $title);
	$page_name = str_replace($first_word[0], "<b>".$first_word[0]."</b>", $title);
	return $page_name;
}

function change_title($title)
{
	$first_word= explode(" ", $title);
	if(strlen($first_word[0]) > 3)
		$page_name = str_replace($first_word[0], "<b>".$first_word[0]."</b><br>", $title);
	else $page_name = $title;
	return $page_name;
}
?>
<link href="http://www.maksoft.net/css/markela/base_style.css" rel="stylesheet" type="text/css">
<style type="text/css">
body,td,th {
	font-family: "Trebuchet MS", Verdana, Geneva, Arial, Helvetica, sans-serif;
}
</style>
</head>
<body>
<div id="header">
	<?php
	// including header file
	include DIR_TEMPLATE."/header.php";
	?>
</div>
<table width="950px" border="0" cellpadding="0" cellspacing="0"  id="main_table">
  <tr>
    <td colspan="3" valign="middle" height="40px">
	<div id="languages">&nbsp;&nbsp;&nbsp;
	<?php
		$s_versions = $o_site->get_sVersions($SiteID);
		for($i=0; $i<count($s_versions); $i++)
			echo $s_versions[$i]['version_key']." <a href=\"".$s_versions[$i]['version_link']."\">".$s_versions[$i]['version']."</a>";
	?>


	&nbsp;&nbsp;&nbsp;
	</div>
	<div id="nav_links"><?=LABEL_NAVIGATION.$o_page->get_pNavigation("short")?></div>
	</td>
  </tr>
  <tr>
	<td valign="top" id="main_menu" align="left">
			<?php
					include "column_left.php"; 
			?>
    </td>
	  <td colspan="2" align="left" width="760px;" valign="top" style="background: url(http://www.maksoft.net/css/markela/content_bg.jpg)  100% 0% repeat-y;">
	  	<?php include "main.php"; ?>
	  </td>
  </tr>
  <tr>
    <td align="center" valign="middle"><a href="http://pochivka.bg/t-66-������-�-�����" title="������, ���� � ���� � ��������" target="_blank" rel="nofolow">�������.bg</a>
	<td colspan="2" valign="middle"><div id="nav_links"><?=LABEL_NAVIGATION.$o_page->get_pNavigation()?></div></div>
	
</table>
<div id="footer"><? include "footer.php";?></div>
</body>
</html>
