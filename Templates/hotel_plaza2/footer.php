<!--footer-->
<div id="footer">
    <div class="wrap">
        <div class="container">
            <div class="row">
                <?php if(!DEMO_MODE)
                {
                    $news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 2");
					
					?>
                <div class="span3">
                    <h2 class="title3"><span><?=$o_page->get_pName($o_site->_site['news'])?></span></h2>
                    <?php
						for($i=0; $i<count($news); $i++)
							{
								?>
                                    <div class="ftr_twit">
                                        <span class="date"><?=date($tmpl_config['date_format'], strtotime($news[$i]['date_modified']))?></span>
                                        <p><?=cut_text(strip_tags($news[$i]['textStr']))?></p>
                                        <a class="read_more" href="<?=$o_page->get_pLink($news[$i]['n'])?>"><?=$o_site->_site['MoreText']?></a>
                                    </div>
                                <?							
							}
					?>
                </div>
                <div class="span3">
                    <h2 class="title3"><span><?=TITLE_CONTACTS?></span></h2>
                    <form id="contact_form" method="post" action="form-cms.php">
                        <input class="span3" type="text" name="name" id="name" value="Name" onFocus="if (this.value == 'Name') this.value = '';" onBlur="if (this.value == '') this.value = 'Name';" />
                        <input class="span3" type="text" name="EMail" id="email" value="EMail" onFocus="if (this.value == 'EMail') this.value = '';" onBlur="if (this.value == '') this.value = 'EMail';" />
                        <textarea name="message" id="message" class="span3" onFocus="if (this.value == '<?=LABEL_MESSAGE?>...') this.value = '';" onBlur="if (this.value == '') this.value = '<?=LABEL_MESSAGE?>...';" ><?=LABEL_MESSAGE?>...</textarea>
                        <div class="clear"></div>
                        <input type="reset" class="btn dark_btn" value="<?=BUTTON_CLEAR?>" />
                        <input type="submit" class="btn btn-success" value="<?=BUTTON_SUBMIT?>" />
                        <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
                        <input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
                        <input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$Site->EMail"); ?>"> 
                        <input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$Site->StartPage&SiteID=$SiteID&sent=1"); ?>">
                        <input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
                        <input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">
                        <div class="clear"></div>
                    </form>
                </div>
                <div class="span3">
                <?php
					$categories = $o_page->get_pSubpages($n_gallery);
				?>
                    <h2 class="title3"><span><?=TITLE_GALLERY?></span></h2>
                    <div class="flickrs">
                        <div class="FlickrImages">
                            <ul>
                            <?
							$counter = 0;
							for($j=0; $j<count($categories); $j++)
							{
								
								$gallery = $o_page->get_pSubpages($categories[$j]['n'], "p.sort_n ASC", "im.image_src !=''");
								for($i=0; $i<count($gallery); $i++)
									{
										if($counter<12)
										{
										?>
                                			<li><a href="<?=HTTP_SERVER . "/".$gallery[$i]['image_src']?>" rel="prettyPhoto[smallGallery]"><img src="<?=HTTP_SERVER . "/".$gallery[$i]['image_src']?>" width="63px" alt="" /></a></li>        
                                        <?
										}
										$counter++;	
									}
							}
							?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="span3 newsletter">
                    <h2 class="title3"><span><?=TITLE_NEWSLETTER?></span></h2>
                    <?=TEXT_NEWSLETTER_CONTENT?>
                    <div class="clear"></div>
                    <form id="newsletter" method="post">
                        <input class="email" type="text" name="EMail" id="EMail" value="E-mail" onFocus="if (this.value == 'E-mail') this.value = '';" onBlur="if (this.value == '') this.value = 'E-mail';" />
                        <div class="clear"></div>
                        <p class="notification"><input type="checkbox" name="conditions" value="1" checked="checked"> <?=TEXT_NEWSLETTER_CONTENT2?></p>
                        <input type="submit" class="btn btn-success" value="<?=BUTTON_SUBMIT?>" />
                        <div class="clear"></div>
                    </form>
                </div>
                                    
                </div>
                </div>            
                </div>
    <div class="footer_bottom">
        <div class="wrap">
            <div class="container">
                <div class="row">
                    <div class="span2">
                            <div class="footer_logo">
                                <a href="<?=$o_page->get_pLink($o_site->get_sStartPage())?>"><img src="<?=TEMPLATE_IMAGES?>footer_logo.png" alt="<?=$o_site->_site['Title']?>"/></a>
                            </div>
                    </div>
                    <div class="span4">
                    	<div class="copyright">
                    		&copy; <?=date("Y")?> <?=$o_site->get_sCopyright()?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="foot_menu">
							<?php print_menu($o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 5"), array('menu_depth'=>0))?>
                        </div>
                    </div>
                </div>
                 <?php
                }
                else include TEMPLATE_PATH . "demo/footer.html";
                 ?>    	
            </div>
        </div>
    </div>
</div>
<!--//footer-->
<!--
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
//-->
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.easing.1.3.js"></script>
<script type="text/javascript" language="javascript">
	//no conflict with Prototype Lib
	var $ = jQuery.noConflict();
</script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.mobile.customized.min.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/camera.js"></script> 
<script src="<?=TEMPLATE_PATH?>js/bootstrap.js"></script>
<script src="<?=TEMPLATE_PATH?>js/superfish.js"></script>

<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.prettyPhoto.js"></script>
<script src="<?=TEMPLATE_PATH?>js/jquery.ui.totop.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.tweet.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/myscript.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	
	function check_reservation(step)
		{
			$('#date_from').removeClass("error");
			$('#date_to').removeClass("error");
			$('#name').removeClass("error");
			$('#phone').removeClass("error");
			$('#email').removeClass("error");
			if(step == 1)
				{
					if($('#date_from').val() != "" && $('#date_to').val() != "")
						return true;
					else 
					{
						if($('#date_from').val() == "") $('#date_from').addClass("error");
						if($('#date_to').val() == "") $('#date_to').addClass("error"); 
						return false;
					}
				}
			if(step == 2)
				{
					if($('#name').val() != "" && $('#phone').val() != "" && $('#email').val() != "")
						return true;
					else 
					{
						if($('#name').val() == "") $('#name').addClass("error");
						if($('#phone').val() == "") $('#phone').addClass("error"); 
						if($('#email').val() == "") $('#email').addClass("error"); 
						return false;
					}

				}
			//full reservation form
			if(step == 3)
				{
					if($('#date_from').val() != "" && $('#date_to').val() != "" && $('#name').val() != "" && $('#phone').val() != "" && $('#email').val() != "")
						return true;
					else 
					{
						if($('#date_from').val() == "") $('#date_from').addClass("error");
						if($('#date_to').val() == "") $('#date_to').addClass("error"); 
						if($('#name').val() == "") $('#name').addClass("error");
						if($('#phone').val() == "") $('#phone').addClass("error"); 
						if($('#email').val() == "") $('#email').addClass("error"); 
						return false;
					}

				}

		}
		
    $(document).ready(function(){	

	//build dropdown
		$("<select />").appendTo("nav#main_menu div");
		
		// Create default option "Go to..."
		$("<option />", {
		   "selected": "selected",
		   "value"   : "",
		   "text"    : "Please choose page"
		}).appendTo("nav#main_menu select");	
		
		// Populate dropdowns with the first menu items
		$("nav#main_menu li a").each(function() {
			var el = $(this);
			$("<option />", {
				"value"   : el.attr("href"),
				"text"    : el.text()
			}).appendTo("nav#main_menu select");
		});
		
		//make responsive dropdown menu actually work			
		$("nav#main_menu select").change(function() {
			window.location = $(this).find("option:selected").val();
		});
		
        //testimonials
        $('#testimonials_carousel').carousel({
            pause: 'hover'
        });	
        
        //Slider
        $('#camera_wrap_1').camera();		
        
        //Mycarousel
        $('#mycarousel, #mycarousel2, #mycarousel3').jcarousel({
            scroll:1
        });
        
        //Featured works & latest posts
        $('#mycarousel, #mycarousel2').jcarousel();
		
        $('#featured-works div.jcarousel-all').html("<a href=\"<?=$o_page->get_pLink($n_featured_works)?>\"><?=TEXT_VIEW_ALL?></a>");
		$('#latest-posts div.jcarousel-all').html("<a href=\"<?=$o_page->get_pLink($n_latest_posts)?>\"><?=TEXT_VIEW_ALL?></a>");
		
        //accordion
        $(".accordion h3").eq(1).addClass("active");
        $(".accordion .accord_cont").eq(1).show();
    
        $(".accordion h3").click(function(){
            $(this).next(".accord_cont").slideToggle("slow")
            .siblings(".accord_cont:visible").slideUp("slow");
            $(this).toggleClass("active");
            $(this).siblings("h3").removeClass("active");
        });
        
        //prettyPhoto
        $("a[rel^='prettyPhoto']").prettyPhoto();
        
        //Image hover
        $(".hover_img").live('mouseover',function(){
                var info=$(this).find("img");
                info.stop().animate({opacity:0.1},300);
                $(".preloader").css({'background':'none'});
            }
        );
        $(".hover_img").live('mouseout',function(){
                var info=$(this).find("img");
                info.stop().animate({opacity:1},300);
                $(".preloader").css({'background':'none'});
            }
        );	
        
        //Iframe transparent
        $("iframe").each(function(){
            var ifr_source = $(this).attr('src');
            var wmode = "wmode=transparent";
            if(ifr_source.indexOf('?') != -1) {
            var getQString = ifr_source.split('?');
            var oldString = getQString[1];
            var newString = getQString[0];
            $(this).attr('src',newString+'?'+wmode+'&'+oldString);
            }
            else $(this).attr('src',ifr_source+'?'+wmode);
        });
        
        
		//RESERVATION FORM
		$('#book-next').click(function(){
			if(check_reservation(1))
			{
				$('.section1').toggle();
				$('.section2').toggle();
			}
			else 
			{
				$('.alert').fadeIn();
			}
			})

		$('#book-now').click(function(){
			if(check_reservation(2))
				{
					$('#short-reservation-form').submit();
				}

			})

		$('.full #book-now').click(function(){
			if(check_reservation(3))
				{
					$('#short-reservation-form').submit();
				}
			else $('.alert').fadeIn();

			})
			
		$('#book-back').click(function(){
			$('.section1').toggle();
			$('.section2').toggle();
			})

		//RESREVATION FORM DATE PICKER

		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		 
		var checkin = $('#date_from').datepicker({
		  onRender: function(date) {
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  if (ev.date.valueOf() > checkout.date.valueOf()) {
			var newDate = new Date(ev.date)
			newDate.setDate(newDate.getDate() + 1);
			checkout.setValue(newDate);
		  }
		  checkin.hide();
		  $('#date_to')[0].focus();
		}).data('datepicker');
		var checkout = $('#date_to').datepicker({
		  onRender: function(date) {
			return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  checkout.hide();
		}).data('datepicker');
                    
    });
</script>
</body>
</html>