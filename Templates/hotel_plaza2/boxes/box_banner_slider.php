<?php
	//$banners = $o_page->get_pBanners();
	$banners = array(
		array("image_src"=>TEMPLATE_IMAGES . "banners/1.jpg"),
		array("image_src"=>TEMPLATE_IMAGES . "banners/2.jpg"),
		array("image_src"=>TEMPLATE_IMAGES . "banners/3.jpg")
	);
	if(count($banners)>0 && !DEMO_MODE)
		{
			?>
			  <div id="main_slider">
				  <div class="camera_wrap" id="camera_wrap_1">
					  <?php
							  for($i=0; $i<count($banners); $i++)
								  {
									  //$class = "";
									  //if($i==0) $class = "active ";
									  //echo "<div class=\"".$class."item\"><img src=\"".$banners[$i]['image_src']."\"></div>";
									  ?>
									  <div data-src="<?=$banners[$i]['image_src']?>">
										  <div class="camera_caption fadeIn">
											  <div class="wrap">
												  <div class="container">
													  <div class="row">
														  <div class="span6">
                                                          	  <img src="<?=TEMPLATE_IMAGES?>hotel_label.png" alt="">
															  <div class="temperature">
  															  <?php 
															    $wwo->get_weather(); 
															  	echo $wwo->city.',&nbsp'.$wwo->weather->current_condition->temp_C.' C&deg;';  
															  ?>
															  </div>
															  <div class="slide_descr"><?=cut_text(strip_tags($o_page->get_pText(38374)), 350)?></div>
														  </div>
													  </div>
												  </div>		
											  </div>			
										  </div>
									  </div>
									  
									  <?
								   }
					  ?>
					  
				  </div><!-- #camera_wrap_1 -->
				  <div class="clear"></div>
			  </div>
		
			<?
		}
	else include TEMPLATE_DIR . "demo/box_banner_slider.html";
?>