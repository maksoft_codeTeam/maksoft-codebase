<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="windows-1251">
<title><?=$Title?></title>
<?php
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<link href="<?=TEMPLATE_DIR?>css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" id="camera-css"  href="<?=TEMPLATE_DIR?>css/camera.css" type="text/css" media="all">
<link href="<?=TEMPLATE_DIR?>css/bootstrap.css" rel="stylesheet">
<link href="<?=TEMPLATE_DIR?>css/theme.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=TEMPLATE_DIR?>css/skins/tango/skin.css" />
<link href="<?=TEMPLATE_DIR?>css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?=TEMPLATE_DIR?>css/bootstrap-datepicker.css" rel="stylesheet">

<!--
<link href="Templates/_test/4978/css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" id="camera-css"  href="Templates/_test/4978/css/camera.css" type="text/css" media="all">
<link href="Templates/_test/4978/css/bootstrap.css" rel="stylesheet">
<link href="Templates/_test/4978/css/theme.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="Templates/_test/4978/css/skins/tango/skin.css" />
<link href="Templates/_test/4978/css/bootstrap-responsive.css" rel="stylesheet">
//-->
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->    
</head>
<body>
<?php $o_page->print_pNavigation()?>
	<!--header-->
    <div class="header" <?=($o_page->n != $o_site->get_sStartPage())?"style=\"position:static;\"":""?>>
    	<div class="wrap">
        	<div class="navbar navbar_ clearfix">
              <div class="logo"><a href="<?=$o_page->get_pLink($o_site->get_sStartPage())?>"><img src="<?=$tmpl_config['default_logo']?>" alt="<?=$o_site->_site['Title']?>" width="<?=$tmpl_config['default_logo_width']?>" /></a></div>
              <div class="menu_wrapper navbar navbar_ clearfix">
              		<!--<div id="languages"><? $o_page->print_sVersions();?></div>//-->
                    <!--<div class="social-links"></div>//-->
                  <nav id="main_menu">
                      <div class="menu_wrap">
                       <?php print_menu($o_page->get_pSubpages($o_site->_site['StartPage']), array('menu_depth'=>1, 'params'=>"class=\"nav sf-menu\""))?>
                      </div>
                  </nav>
              </div>
              <div class="clear"></div>
            </div>
        </div>    
    </div>
    <!--//header-->