<!--footer-->
<div id="footer">
    <div class="wrap">
        <div class="container">
            <div class="row">
                <?php if(!DEMO_MODE)
                {
                    $news = $o_page->get_pSubpages($o_site->_site['news'], "p.date_added DESC LIMIT 2");
					
					?>
                <div class="span3">
                    <h2 class="title3"><span><?=$o_page->get_pName($o_site->_site['news'])?></span></h2>
                    <?php
						for($i=0; $i<count($news); $i++)
							{
								?>
                                    <div class="ftr_twit">
                                        <span class="date"><?=date($tmpl_config['date_format'], strtotime($news[$i]['date_modified']))?></span>
                                        <p><?=cut_text(strip_tags($news[$i]['textStr']))?></p>
                                        <a class="read_more" href="<?=$o_page->get_pLink($news[$i]['n'])?>"><?=$o_site->_site['MoreText']?></a>
                                    </div>
                                <?							
							}
					?>
                </div>
                <div class="span3">
                    <h2 class="title3"><span>&#1055;&#1080;&#1096;&#1077;&#1090;&#1077; &#1085;&#1080;</span></h2>
                    <form id="contact_form" method="post" action="form-cms.php">
                        <input class="span3" type="text" name="name" id="name" value="Name" onFocus="if (this.value == 'Name') this.value = '';" onBlur="if (this.value == '') this.value = 'Name';" />
                        <input class="span3" type="text" name="email" id="email" value="Email" onFocus="if (this.value == 'Email') this.value = '';" onBlur="if (this.value == '') this.value = 'Email';" />
                        <textarea name="message" id="message" class="span3" onFocus="if (this.value == 'Message...') this.value = '';" onBlur="if (this.value == '') this.value = 'Message...';" >Message...</textarea>
                        <div class="clear"></div>
                        <input type="reset" class="btn dark_btn" value="Clear form" />
                        <input type="submit" class="btn btn-success" value="Submit" />
                        <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
                        <input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
                        <input name="ToMail" type="hidden" id="ToMail" value="<?php echo("$Site->EMail"); ?>"> 
                        <input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$Site->StartPage&SiteID=$SiteID&sent=1"); ?>">
                        <input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
                        <input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">
                        <div class="clear"></div>
                    </form>
                </div>
                <div class="span3">
                <?php
					$categories = $o_page->get_pSubpages(211831);
				?>
                    <h2 class="title3"><span><?=$o_page->get_pName(211831)?></span></h2>
                    <div class="flickrs">
                        <div class="FlickrImages">
                            <ul>
                            <?
							$counter = 0;
							for($j=0; $j<count($categories); $j++)
							{
								
								$gallery = $o_page->get_pSubpages($categories[$j]['n'], "p.sort_n ASC", "im.image_src !=''");
								for($i=0; $i<count($gallery); $i++)
									{
										if($counter<12)
										{
										?>
                                			<li><a href="<?=HTTP_SERVER . "/".$gallery[$i]['image_src']?>" rel="prettyPhoto[smallGallery]"><img src="<?=HTTP_SERVER . "/".$gallery[$i]['image_src']?>" width="63px" alt="" /></a></li>        
                                        <?
										}
										$counter++;	
									}
							}
							?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="span3 newsletter">
                    <h2 class="title3"><span>Newsletter</span></h2>
                    <p>&#1053;&#1072;&#1091;&#1095;&#1072;&#1074;&#1072;&#1081;&#1090;&#1077; &#1087;&#1098;&#1088;&#1074;&#1080; &#1079;&#1072; &#1085;&#1086;&#1074;&#1080;&#1090;&#1077; &#1085;&#1080; &#1091;&#1089;&#1083;&#1086;&#1074;&#1080;&#1103;, &#1087;&#1088;&#1086;&#1084;&#1086;&#1094;&#1080;&#1080; &#1080; &#1087;&#1088;&#1077;&#1076;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1103;. &#1047;&#1072;&#1087;&#1080;&#1096;&#1077;&#1090;&#1077; &#1089;&#1077; &#1079;&#1072; &#1085;&#1072;&#1096;&#1080;&#1103; &#1085;&#1102;&#1079;&#1083;&#1077;&#1090;&#1098;&#1088;!</p>
                    <div class="clear"></div>
                    <form id="newsletter" method="post">
                        <input class="email" type="text" name="EMail" id="EMail" value="E-mail" onFocus="if (this.value == 'E-mail') this.value = '';" onBlur="if (this.value == '') this.value = 'E-mail';" />
                        <div class="clear"></div>
                        <p class="notification"><input type="checkbox" name="conditions" value="1" checked="checked"> <em>&#1059;&#1074;&#1077;&#1076;&#1086;&#1084;&#1077;&#1085; &#1089;&#1098;&#1084;, &#1095;&#1077; &#1089;&#1077; &#1072;&#1073;&#1086;&#1085;&#1080;&#1088;&#1072;&#1084; &#1087;&#1086; &#1089;&#1074;&#1086;&#1077; &#1078;&#1077;&#1083;&#1072;&#1085;&#1080;&#1077; &#1076;&#1072; &#1087;&#1086;&#1083;&#1091;&#1095;&#1072;&#1074;&#1072;&#1084; &#1090;&#1098;&#1088;&#1075;&#1086;&#1074;&#1089;&#1082;&#1080; &#1089;&#1098;&#1086;&#1073;&#1097;&#1077;&#1085;&#1080;&#1103; &#1086;&#1090; <a href="#">&#1061;&#1086;&#1090;&#1077;&#1083; &#1055;&#1083;&#1072;&#1079;&#1072;</a></em></p>
                        <input type="submit" class="btn btn-success" value="Submit" />
                        <div class="clear"></div>
                    </form>
                </div>
                                    
                </div>
                </div>            
                </div>
    <div class="footer_bottom">
        <div class="wrap">
            <div class="container">
                <div class="row">
                    <div class="span2">
                            <div class="footer_logo">
                                <a href="<?=$o_page->get_pLink($o_site->get_sStartPage())?>"><img src="<?=TEMPLATE_IMAGES?>footer_logo.png" alt="<?=$o_site->_site['Title']?>"/></a>
                            </div>
                    </div>
                    <div class="span4">
                    	<div class="copyright">
                    		&copy; <?=date("Y")?> <?=$o_site->get_sCopyright()?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="foot_menu">
							<?php print_menu($o_page->get_pSubpages($o_site->_site['StartPage'], "p.sort_n ASC LIMIT 5"), array('menu_depth'=>0))?>
                        </div>
                    </div>
                </div>
                 <?php
                }
                else include TEMPLATE_DIR . "demo/footer.html";
                 ?>    	
            </div>
        </div>
    </div>
</div>
<!--//footer-->

<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/jquery.easing.1.3.js"></script>
<script type="text/javascript" language="javascript">
	//no conflict with Prototype Lib
	var $ = jQuery.noConflict();
</script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/jquery.mobile.customized.min.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/camera.js"></script> 
<script src="<?=TEMPLATE_DIR?>js/bootstrap.js"></script>
<script src="<?=TEMPLATE_DIR?>js/superfish.js"></script>

<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/jquery.prettyPhoto.js"></script>
<script src="<?=TEMPLATE_DIR?>js/jquery.ui.totop.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/jquery.tweet.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/myscript.js"></script>
<script type="text/javascript" src="<?=TEMPLATE_DIR?>js/bootstrap-datepicker.js"></script>


<!--
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript" src="Templates/_test/4978/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="Templates/_test/4978/js/jquery.mobile.customized.min.js"></script>
    <script type="text/javascript" src="Templates/_test/4978/js/camera.js"></script> 
    <script src="Templates/_test/4978/js/bootstrap.js"></script>
    <script src="Templates/_test/4978/js/superfish.js"></script>
    
	<script type="text/javascript" src="Templates/_test/4978/js/jquery.prettyPhoto.js"></script>
    <script src="Templates/_test/4978/js/jquery.ui.totop.js" type="text/javascript"></script>
    <script type="text/javascript" src="Templates/_test/4978/js/jquery.jcarousel.js"></script>
	<script type="text/javascript" src="Templates/_test/4978/js/jquery.tweet.js"></script>
	<script type="text/javascript" src="Templates/_test/4978/js/jquery.jcarousel.min.js"></script>
	<script type="text/javascript" src="Templates/_test/4978/js/myscript.js"></script>
//-->

<script type="text/javascript">
    $(document).ready(function(){	
	//build dropdown
		$("<select />").appendTo("nav#main_menu div");
		
		// Create default option "Go to..."
		$("<option />", {
		   "selected": "selected",
		   "value"   : "",
		   "text"    : "Please choose page"
		}).appendTo("nav#main_menu select");	
		
		// Populate dropdowns with the first menu items
		$("nav#main_menu li a").each(function() {
			var el = $(this);
			$("<option />", {
				"value"   : el.attr("href"),
				"text"    : el.text()
			}).appendTo("nav#main_menu select");
		});
		
		//make responsive dropdown menu actually work			
		$("nav#main_menu select").change(function() {
			window.location = $(this).find("option:selected").val();
		});

        //testimonials
        $('#testimonials_carousel').carousel({
            pause: 'hover'
        });	
        
        //Slider
        $('#camera_wrap_1').camera();		
        
        //Mycarousel
        $('#mycarousel, #mycarousel2, #mycarousel3').jcarousel({
            scroll:1
        });
        
        //Featured works & latest posts
        $('#mycarousel, #mycarousel2').jcarousel();
        
        //accordion
        $(".accordion h3").eq(1).addClass("active");
        $(".accordion .accord_cont").eq(1).show();
    
        $(".accordion h3").click(function(){
            $(this).next(".accord_cont").slideToggle("slow")
            .siblings(".accord_cont:visible").slideUp("slow");
            $(this).toggleClass("active");
            $(this).siblings("h3").removeClass("active");
        });
        
        //prettyPhoto
        $("a[rel^='prettyPhoto']").prettyPhoto();
        
        //Image hover
        $(".hover_img").live('mouseover',function(){
                var info=$(this).find("img");
                info.stop().animate({opacity:0.1},300);
                $(".preloader").css({'background':'none'});
            }
        );
        $(".hover_img").live('mouseout',function(){
                var info=$(this).find("img");
                info.stop().animate({opacity:1},300);
                $(".preloader").css({'background':'none'});
            }
        );	
        
        //Iframe transparent
        $("iframe").each(function(){
            var ifr_source = $(this).attr('src');
            var wmode = "wmode=transparent";
            if(ifr_source.indexOf('?') != -1) {
            var getQString = ifr_source.split('?');
            var oldString = getQString[1];
            var newString = getQString[0];
            $(this).attr('src',newString+'?'+wmode+'&'+oldString);
            }
            else $(this).attr('src',ifr_source+'?'+wmode);
        });
        
        //BOOK NOW
		$('#book-now').click(function(){
			$('#short-reservation-form').submit();
			})            
        
		//SHORT RESERVATION FORM
		$('.date_from').datepicker();
                    
    });
</script>

</body>
</html>