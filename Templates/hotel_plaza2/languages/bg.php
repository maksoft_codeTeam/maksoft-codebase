<?php
	//define some pages
	//room gallery
	$n_featured_works = 215191;
	
	//landmarks page
	$n_latest_posts = 215212;
	
	//group of 4 vip links under the banner
	$group_vip_links = 655;
	
	//about rooms text
	$n_about_rooms = 56070;

	//gallery
	$n_gallery = 56070;

	//about page
	$n_about = 56067;
		
	//reservation form
	define("TITLE_RESERVATION", "����������");
	define("LABEL_DATE_ARIVAL", "����������");
	define("LABEL_DATE_DEPARTURE", "����������");
	define("LABEL_ROOM_TYPE", "��� ����");
	define("LABEL_ADULTS", "���������");
	define("LABEL_KIDS1", "���� 0-6");
	define("LABEL_KIDS2", "���� 7-12");
	define("LABEL_COMMENTS", "���������");
	define("LABEL_NAME", "������ ���");
	define("LABEL_EMAIL", "����� e-mail");
	define("LABEL_PHONE", "�������");
	define("LABEL_CODE", "���");
	define("LABEL_ALL", "������");
	define("LABEL_SUBJECT", "����");
			
	define("MESSAGE_MANDANTORY_RESERVATION_FIELDS","<strong>��������!</strong> ������ ������ � * �� ������������!");
	
	define("BUTTON_NEXT", "������");
	define("BUTTON_BACK", "�����");
	define("BUTTON_BOOK_NOW", "����������");
	define("BUTTON_CLEAR", "�������");
	define("BUTTON_SUBMIT", "�������");
	define("BUTTON_BACK_TO", "����� ���");
	define("BUTTON_", "");
	
	$room_types = array(
	"vip" => "��� ����������", 
	"studio" => "������", 
	"standart-room" => "���������� ����", 
	"economy-room" => "���������� ����"
	);
	
	//contact form
	define("LABEL_MESSAGE", "���������");
	
	//footer content
	define("TITLE_NEWS", "������");
	define("TITLE_CONTACTS", "������ ��");
	define("TITLE_GALLERY", "�������");
	define("TITLE_NEWSLETTER", "��������");
	
	define("TEXT_NEWSLETTER_CONTENT", "<p>���������� ����� �� ������ �� �������, �������� � �����������. �������� �� �� ����� ��������!</p>");
	define("TEXT_NEWSLETTER_CONTENT2", "<em>�������� ���, �� �� �������� �� ���� ������� �� ��������� ��������� ��������� �� <a href=\"#\">����� �����</a></em>");
	define("TEXT_PAGE_VISITS","<b>%d</b> ������������");
	define("TEXT_VIEW_ALL", "��� ������");
	
	//home content
	define("HEADING_WELCOME", "���� ������ �� ������� ������ � �� ��������?");
	define("HEADING_WELCOME_TEXT", "*     *     *<br><span>����� ����� - �������������� ����� �� ������ � <br>�����������</span>");
	define("TITLE_COMMENTS", "���������");
	define("TITLE_FEATURED_WORKS", "������� ����");
?>