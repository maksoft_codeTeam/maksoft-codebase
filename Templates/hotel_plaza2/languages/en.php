<?php
	//define some pages
	//room gallery
	$n_featured_works = 38377;
	
	//landmarks page
	$n_latest_posts = 214310;
	
	//group of 4 vip links under the banner
	$group_vip_links = 649;
	
	//about rooms text
	$n_about_rooms = 38375;

	//gallery
	$n_gallery = 211831;
	
	//about page
	$n_about = 38374;
	
	//reservation form
	define("TITLE_RESERVATION", "Reservation");
	define("LABEL_DATE_ARIVAL", "arival date");
	define("LABEL_DATE_DEPARTURE", "daparture date");
	define("LABEL_ROOM_TYPE", "room type");
	define("LABEL_ADULTS", "adults");
	define("LABEL_KIDS1", "kids 0-6");
	define("LABEL_KIDS2", "kids 7-12");
	define("LABEL_COMMENTS", "comments");
	define("LABEL_NAME", "your name");
	define("LABEL_EMAIL", "your e-mail");
	define("LABEL_PHONE", "phone");
	define("LABEL_CODE", "security code");
	define("LABEL_ALL", "All");
	define("LABEL_SUBJECT", "subject");
	
	define("MESSAGE_MANDANTORY_RESERVATION_FIELDS","<strong>Warning!</strong> All fields marked with * are mandantory!");
	
	define("BUTTON_NEXT", "Next");
	define("BUTTON_BACK", "Go Back");
	define("BUTTON_BOOK_NOW", "Book now");
	define("BUTTON_CLEAR", "Clear");
	define("BUTTON_SUBMIT", "Submit");
	define("BUTTON_BACK_TO", "Back to");
	define("BUTTON_", "");
	
	$room_types = array(
	"vip" => "Vip appartment", 
	"studio" => "Studio", 
	"standart-room" => "Standart room", 
	"economy-room" => "Economy room"
	);
	
	//contact form
	define("LABEL_MESSAGE", "Message");
	
	//footer content
	define("TITLE_NEWS", "News");
	define("TITLE_CONTACTS", "Write to us");
	define("TITLE_GALLERY", "Gallery");
	define("TITLE_NEWSLETTER", "Newsletter");
	
	define("TEXT_NEWSLETTER_CONTENT", "<p>Learn first about our new terms, promotions and offers. Sign up for our newsletter!</p>");
	define("TEXT_NEWSLETTER_CONTENT2", "<em>I am informed that I subscribe voluntarily to receive commercial communications from <a href=\"#\">Hotel Plaza</a></em>");
	define("TEXT_PAGE_VISITS","<b>%d</b> visits");
	define("TEXT_VIEW_ALL","view all");
	
	//home content
	define("HEADING_WELCOME", "Where you can do business while relaxing?");
	define("HEADING_WELCOME_TEXT", "*     *     *<br><span>HOTEL PLAZA - PREFERRED PLACE FOR BUSINESS AND PLEASURE</span>");
	define("TITLE_COMMENTS", "Comments");
	define("TITLE_FEATURED_WORKS", "Gallery rooms");
?>