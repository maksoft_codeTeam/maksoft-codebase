    <!--page_container-->
    <div class="page_container">
    	<div class="breadcrumb">
        	<div class="wrap">
            	<div class="container">
                    <?=$o_page->print_pName("true", $o_page->_page['ParentPage'])?><span>/</span><?=$o_page->print_pName()?>
                </div>
            </div>
        </div>
    	<div class="wrap">
        	<div class="container" id="pageContent">
                <section>
                	<div class="row">
                    	<div class="span8">
                        	<h1 class="title"><?=$o_page->print_pTitle("strict")?></h1>
                        </div>
                    	<div class="span6">
                            <div id="map"><iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.bg/maps?ie=UTF8&amp;cid=7213512350450803085&amp;q=%D0%9F%D0%BB%D0%B0%D0%B7%D0%B0+%D0%A5%D0%BE%D1%82%D0%B5%D0%BB&amp;gl=BG&amp;hl=bg&amp;t=m&amp;iwloc=A&amp;ll=43.204593,27.921271&amp;spn=0.006295,0.006295&amp;output=embed"></iframe></div>
                        </div>
                    	<div class="span6">
                            <div class="contact_form">
                                                <?php
												if($sent)
												{
												?>
                                                <div class="alert">
                                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                  <strong>Message:</strong> Your message has been sent successfully!
                                                </div>
                                                <?php
												}
												?>
                                                <div id="fields">
                                                <form id="ajax-contact-form" action="form-cms.php" method="post">
                                                    <input class="span6" type="text" name="name" value="" placeholder="<?=LABEL_NAME?>" />
                                                    <input class="span6" type="text" name="EMail" value="" placeholder="<?=LABEL_EMAIL?>" />
                                                    <input class="span6" type="text" name="subject" value="" placeholder="<?=LABEL_SUBJECT?>" />
                                                    <textarea id="message2" name="message" class="span6" placeholder="<?=LABEL_MESSAGE?>"></textarea>
                                                    <div class="clear"></div>
                                                    <br>
                                                    <input type="reset" class="btn" value="<?=BUTTON_CLEAR?>" />
                                                    <input type="submit" class="btn dark_btn" value="<?=BUTTON_SUBMIT?>" />
                                                    <div class="clear"></div>
                                                    <br>
                                                        
                                                        <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
                                                        <input name="SiteID" type="hidden" id="SiteID" value="<?=$o_site->SiteID?>">
                                                        <input name="ToMail" type="hidden" id="ToMail" value="<?=$o_site->_site['EMail']?>"> 
                                                        <input name="conf_page" type="hidden" id="conf_page " value="<?=$o_page->get_pLink()?>&sent=1">
                                                </form>
                                 </div> 
                            </div>
                        </div>                	
                        <div class="clear"></div>
                        <div class="span3 contact_text" style="border-top: 1px solid #FFF"><?=$o_page->get_pText()?></div>
                        <div class="span3" style="border-top: 1px solid #FFF"></div>
                        <div class="span6" style="border-top: 1px solid #FFF"><br><?=$o_page->get_pText($n_about)?></div>
                        
            <!-- middle content -->
            <br clear="all"><br clear="all">
            <div class="row border">
                <div class="span6">
                <?php 
				//	include TEMPLATE_DIR . "boxes/box_reservation.php"; 
				?>
                </div>
                <div class="span6">
                <?php 
				// include TEMPLATE_DIR . "boxes/box_comments.php"; 
				?>
                </div>
            </div>                
            <!-- //middle content -->
                                    
                	</div>
                </section>
              <br clear="all">
              <?php $o_page->print_pNavigation()?>

            </div>
        </div>
    </div>
    <!--//page_container-->