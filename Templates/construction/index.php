<!DOCTYPE HTML>
<html lang="<?php echo($Site->language_key); ?>" class="js no-touch csstransforms csstransforms3d csstransitions svg js_active  vc_desktop  vc_transform  vc_transform">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<title><?=$Title?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="alternate" type="application/rss+xml" title="Construction Theme � Feed" href="http://demo.wpcharming.com/construction/feed/">
<link rel="alternate" type="application/rss+xml" title="Construction Theme � Comments Feed" href="http://demo.wpcharming.com/construction/comments/feed/">
<link rel="alternate" type="application/rss+xml" title="Construction Theme � Home Comments Feed" href="http://demo.wpcharming.com/construction/home/feed/">
<link rel="stylesheet" id="contact-form-7-css" href="/Templates/construction/files/styles.css" type="text/css" media="all">
<link rel="stylesheet" id="essential-grid-plugin-settings-css" href="/Templates/construction/files/settings.css" type="text/css" media="all">
<link rel="stylesheet" id="tp-montserrat-css" href="/Templates/construction/files/css" type="text/css" media="all">
<link rel="stylesheet" id="rs-plugin-settings-css" href="/Templates/construction/files/settings(1).css" type="text/css" media="all">
<style id="rs-plugin-settings-inline-css" type="text/css">
.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
</style>
<link rel="stylesheet" id="woocommerce-layout-css" href="/Templates/construction/files/woocommerce-layout.css" type="text/css" media="all">
<link rel="stylesheet" id="woocommerce-smallscreen-css" href="/Templates/construction/files/woocommerce-smallscreen.css" type="text/css" media="only screen and (max-width: 768px)">
<link rel="stylesheet" id="woocommerce-general-css" href="/Templates/construction/files/woocommerce.css" type="text/css" media="all">
<link rel="stylesheet" id="wpcharming-style-css" href="/Templates/construction/files/style.css" type="text/css" media="all">
<link rel="stylesheet" id="wpcharming-fontawesome-css" href="/Templates/construction/files/font-awesome.min.css" type="text/css" media="all">
<link rel="stylesheet" id="js_composer_front-css" href="/Templates/construction/files/js_composer.css" type="text/css" media="all">
<link rel="stylesheet" id="redux-google-fonts-wpc_options-css" href="/Templates/construction/files/css(1)" type="text/css" media="all">
<link rel="stylesheet" id="wpcharming-vccustom-css" href="/Templates/construction/files/vc_custom.css" type="text/css" media="all">
<div class="fit-vids-style">�<style>               .fluid-width-video-wrapper {                 width: 100%;                              position: relative;                       padding: 0;                            }                                                                                   .fluid-width-video-wrapper iframe,        .fluid-width-video-wrapper object,        .fluid-width-video-wrapper embed {           position: absolute;                       top: 0;                                   left: 0;                                  width: 100%;                              height: 100%;                          }                                       </style></div><script type="text/javascript">
/* <![CDATA[ */
var header_fixed_setting = {"fixed_header":"1"};
/* ]]> */
</script>
<script type="text/javascript" src="/Templates/construction/files/jquery.js"></script>
<script type="text/javascript" src="/Templates/construction/files/jquery-migrate.min.js"></script>
<script type="text/javascript" src="/Templates/construction/files/lightbox.js"></script>
<script type="text/javascript" src="/Templates/construction/files/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/Templates/construction/files/jquery.themepunch.essential.min.js"></script>
<script type="text/javascript" src="/Templates/construction/files/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="/Templates/construction/files/modernizr.min.js"></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://demo.wpcharming.com/construction/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://demo.wpcharming.com/construction/wp-includes/wlwmanifest.xml"> 
<meta name="generator" content="WordPress 4.1">
<meta name="generator" content="WooCommerce 2.3.5">
<link rel="canonical" href="/Templates/construction/files/Construction Theme   Just another WordPress site.html">
<link rel="shortlink" href="/Templates/construction/files/Construction Theme   Just another WordPress site.html">
		<script type="text/javascript">
			jQuery(document).ready(function() {
				// CUSTOM AJAX CONTENT LOADING FUNCTION
				var ajaxRevslider = function(obj) {
				
					// obj.type : Post Type
					// obj.id : ID of Content to Load
					// obj.aspectratio : The Aspect Ratio of the Container / Media
					// obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content
					
					var content = "";

					data = {};
					
					data.action = 'revslider_ajax_call_front';
					data.client_action = 'get_slider_html';
					data.token = '1bb4c4f55c';
					data.type = obj.type;
					data.id = obj.id;
					data.aspectratio = obj.aspectratio;
					
					// SYNC AJAX REQUEST
					jQuery.ajax({
						type:"post",
						url:"http://demo.wpcharming.com/construction/wp-admin/admin-ajax.php",
						dataType: 'json',
						data:data,
						async:false,
						success: function(ret, textStatus, XMLHttpRequest) {
							if(ret.success == true)
								content = ret.data;								
						},
						error: function(e) {
							console.log(e);
						}
					});
					
					 // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
					 return content;						 
				};
				
				// CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
				var ajaxRemoveRevslider = function(obj) {
					return jQuery(obj.selector+" .rev_slider").revkill();
				};

				// EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
				var extendessential = setInterval(function() {
					if (jQuery.fn.tpessential != undefined) {
						clearInterval(extendessential);
						if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
							jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
							// type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
							// func: the Function Name which is Called once the Item with the Post Type has been clicked
							// killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
							// openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
						}
					}
				},30);
			});
		</script>
		<!--[if lt IE 9]><script src="http://demo.wpcharming.com/construction/wp-content/themes/construction/assets/js/html5.min.js"></script><![endif]-->
<style id="theme_option_custom_css" type="text/css">
 
</style>
<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress.">
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="http://demo.wpcharming.com/construction/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]--><style type="text/css" title="dynamic-css" class="options-output">.site-header .site-branding{margin-top:20px;margin-right:0;margin-bottom:0;margin-left:0;}.page-title-wrap{background-color:#f8f9f9;}a, .primary-color, .wpc-menu a:hover, .wpc-menu > li.current-menu-item > a, .wpc-menu > li.current-menu-ancestor > a,
                                                       .entry-footer .post-categories li a:hover, .entry-footer .post-tags li a:hover,
                                                       .heading-404, .grid-item .grid-title a:hover, .widget a:hover, .widget #calendar_wrap a, .widget_recent_comments a,
                                                       #secondary .widget.widget_nav_menu ul li a:hover, #secondary .widget.widget_nav_menu ul li li a:hover, #secondary .widget.widget_nav_menu ul li li li a:hover,
                                                       #secondary .widget.widget_nav_menu ul li.current-menu-item a, .woocommerce ul.products li.product .price, .woocommerce .star-rating,
                                                       .iconbox-wrapper .iconbox-icon .primary, .iconbox-wrapper .iconbox-image .primary, .iconbox-wrapper a:hover,
                                                       .breadcrumbs a:hover, #comments .comment .comment-wrapper .comment-meta .comment-time:hover, #comments .comment .comment-wrapper .comment-meta .comment-reply-link:hover, #comments .comment .comment-wrapper .comment-meta .comment-edit-link:hover,
                                                       .nav-toggle-active i, .header-transparent .header-right-wrap .extract-element .phone-text, .site-header .header-right-wrap .extract-element .phone-text,
                                                       .wpb_wrapper .wpc-projects-light .esg-navigationbutton:hover, .wpb_wrapper .wpc-projects-light .esg-filterbutton:hover,.wpb_wrapper .wpc-projects-light .esg-sortbutton:hover,.wpb_wrapper .wpc-projects-light .esg-sortbutton-order:hover,.wpb_wrapper .wpc-projects-light .esg-cartbutton-order:hover,.wpb_wrapper .wpc-projects-light .esg-filterbutton.selected,
                                                       .wpb_wrapper .wpc-projects-dark .esg-navigationbutton:hover, .wpb_wrapper .wpc-projects-dark .esg-filterbutton:hover, .wpb_wrapper .wpc-projects-dark .esg-sortbutton:hover,.wpb_wrapper .wpc-projects-dark .esg-sortbutton-order:hover,.wpb_wrapper .wpc-projects-dark .esg-cartbutton-order:hover, .wpb_wrapper .wpc-projects-dark .esg-filterbutton.selected{color:#fab702;}input[type="reset"], input[type="submit"], input[type="submit"], .wpc-menu ul li a:hover,
                                                       .wpc-menu ul li.current-menu-item > a, .loop-pagination a:hover, .loop-pagination span:hover,
                                                       .loop-pagination a.current, .loop-pagination span.current, .footer-social, .tagcloud a:hover, woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,
                                                       .woocommerce #respond input#submit.alt:hover, .woocommerce #respond input#submit.alt:focus, .woocommerce #respond input#submit.alt:active, .woocommerce a.button.alt:hover, .woocommerce a.button.alt:focus, .woocommerce a.button.alt:active, .woocommerce button.button.alt:hover, .woocommerce button.button.alt:focus, .woocommerce button.button.alt:active, .woocommerce input.button.alt:hover, .woocommerce input.button.alt:focus, .woocommerce input.button.alt:active,
                                                       .woocommerce span.onsale, .entry-content .wpb_content_element .wpb_tour_tabs_wrapper .wpb_tabs_nav li.ui-tabs-active a, .entry-content .wpb_content_element .wpb_accordion_header li.ui-tabs-active a,
                                                       .entry-content .wpb_content_element .wpb_accordion_wrapper .wpb_accordion_header.ui-state-active a,
                                                       .btn, .btn:hover, .btn-primary, .custom-heading .heading-line, .custom-heading .heading-line.primary,
                                                       .wpb_wrapper .eg-wpc_projects-element-1{background-color:#fab702;}textarea:focus, input[type="date"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="email"]:focus, input[type="month"]:focus, input[type="number"]:focus, input[type="password"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="text"]:focus, input[type="time"]:focus, input[type="url"]:focus, input[type="week"]:focus,
                                                       .entry-content blockquote{border-color:#fab702;}#secondary .widget.widget_nav_menu ul li.current-menu-item a:before{border-left-color:#fab702;}.secondary-color, .iconbox-wrapper .iconbox-icon .secondary, .iconbox-wrapper .iconbox-image .secondary{color:#00aeef;}.btn-secondary, .custom-heading .heading-line.secondary{background-color:#00aeef;}.hentry.sticky, .entry-content blockquote, .entry-meta .sticky-label,
                                .entry-author, #comments .comment .comment-wrapper, .page-title-wrap, .widget_wpc_posts ul li,
                                .inverted-column > .wpb_wrapper, .inverted-row, div.wpcf7-response-output{background-color:#f8f9f9;}hr, abbr, acronym, dfn, table, table > thead > tr > th, table > tbody > tr > th, table > tfoot > tr > th, table > thead > tr > td, table > tbody > tr > td, table > tfoot > tr > td,
                                fieldset, select, textarea, input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="email"], input[type="month"], input[type="number"], input[type="password"], input[type="search"], input[type="tel"], input[type="text"], input[type="time"], input[type="url"], input[type="week"],
                                .left-sidebar .content-area, .left-sidebar .sidebar, .right-sidebar .content-area, .right-sidebar .sidebar,
                                .site-header, .wpc-menu.wpc-menu-mobile, .wpc-menu.wpc-menu-mobile li, .blog .hentry, .archive .hentry, .search .hentry,
                                .page-header .page-title, .archive-title, .client-logo img, #comments .comment-list .pingback, .page-title-wrap, .page-header-wrap,
                                .portfolio-prev i, .portfolio-next i, #secondary .widget.widget_nav_menu ul li.current-menu-item a, .icon-button,
                                .woocommerce nav.woocommerce-pagination ul, .woocommerce nav.woocommerce-pagination ul li,woocommerce div.product .woocommerce-tabs ul.tabs:before, .woocommerce #content div.product .woocommerce-tabs ul.tabs:before, .woocommerce-page div.product .woocommerce-tabs ul.tabs:before, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs:before,
                                .woocommerce div.product .woocommerce-tabs ul.tabs li:after, .woocommerce div.product .woocommerce-tabs ul.tabs li:before,
                                .woocommerce table.cart td.actions .coupon .input-text, .woocommerce #content table.cart td.actions .coupon .input-text, .woocommerce-page table.cart td.actions .coupon .input-text, .woocommerce-page #content table.cart td.actions .coupon .input-text,
                                .woocommerce form.checkout_coupon, .woocommerce form.login, .woocommerce form.register,.shop-elements i, .testimonial .testimonial-content, .breadcrumbs,
                                .woocommerce-cart .cart-collaterals .cart_totals table td, .woocommerce-cart .cart-collaterals .cart_totals table th,.carousel-prev, .carousel-next,.recent-news-meta{border-color:#e9e9e9;}.site{background-color:#ffffff;}.layout-boxed{background-color:#333333;}body{font-family:"PT Sans";color:#777777;font-size:14px;}h1,h2,h3,h4,h5,h6, .font-heading{font-family:Montserrat;font-weight:normal;color:#333333;}</style><style type="text/css">.esgbox-margin{margin-right:0px;}</style></head>

<body class="home page page-id-13 page-template-default header-fixed-on header-transparent chrome windows wpb-js-composer js-comp-ver-4.4.2 vc_responsive esg-blurlistenerexists">
<div id="page" class="hfeed site">

	<a class="skip-link screen-reader-text" href="http://demo.wpcharming.com/construction/#content">Skip to content</a>

	<header id="masthead" class="site-header fixed-on" role="banner">
		<div class="header-wrap">
			<div class="container">
				<div class="site-branding">
										<a href="/Templates/construction/files/Construction Theme   Just another WordPress site.html" title="Construction Theme" rel="home">
						<img src="/Templates/construction/files/logo_transparent.png" alt="">
					</a>
									</div><!-- /.site-branding -->

				<div class="header-right-wrap clearfix">

					<div class="header-widget">
						<div class="header-right-widgets clearfix">

							<div class="header-extract clearfix">

																<div class="extract-element">
									<div class="header-social">
										<a href="http://demo.wpcharming.com/construction/#" title="Twitter"><i class="fa fa-twitter"></i></a> 																				<a href="http://demo.wpcharming.com/construction/#" title="Linkedin"><i class="fa fa-linkedin-square"></i></a> 																				<a href="http://demo.wpcharming.com/construction/#" title="Pinterest"><i class="fa fa-pinterest"></i></a> 										<a href="http://demo.wpcharming.com/construction/#" title="Google Plus"><i class="fa fa-google-plus"></i></a> 																																								<a href="http://demo.wpcharming.com/construction/#" title="Email"><i class="fa fa-envelope"></i></a> 									</div>
								</div>
								
																<div class="extract-element">
									<span class="header-text">Toll Free</span> <span class="phone-text primary-color">1.800.123.4567</span>
								</div>
								
								
							</div>
						</div>
					</div>
				
					<nav id="site-navigation" class="main-navigation" role="navigation">
						<div id="nav-toggle"><i class="fa fa-bars"></i></div>
						<ul class="wpc-menu">	
					   	   <li id="menu-item-27" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-27"><div class="nav-toggle-subarrow"><i class="fa fa-angle-down"></i></div><a href="/Templates/construction/files/Construction Theme   Just another WordPress site.html">Home</a>
<ul class="sub-menu">
	<li id="menu-item-575" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-13 current_page_item menu-item-575"><a href="/Templates/construction/files/Construction Theme   Just another WordPress site.html">Home Default</a></li>
	<li id="menu-item-567" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-567"><a href="http://demo.wpcharming.com/construction/home-extended/">Home Extended</a></li>
	<li id="menu-item-651" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-651"><a href="http://demo.wpcharming.com/construction/home-video-header/">Home Video Header</a></li>
</ul>
</li>
<li id="menu-item-235" class="menu-features menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-235"><div class="nav-toggle-subarrow"><i class="fa fa-angle-down"></i></div><a href="http://demo.wpcharming.com/construction/#">Features</a>
<ul class="sub-menu">
	<li id="menu-item-583" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-583"><div class="nav-toggle-subarrow"><i class="fa fa-angle-down"></i></div><a href="http://demo.wpcharming.com/construction/#">Pages</a>
	<ul class="sub-menu">
		<li id="menu-item-588" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-588"><a href="http://demo.wpcharming.com/construction/who-we-are/">Who We Are</a></li>
		<li id="menu-item-596" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-596"><a href="http://demo.wpcharming.com/construction/our-team/">Our Team</a></li>
		<li id="menu-item-597" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-597"><a href="http://demo.wpcharming.com/construction/contact-us/">Contact Us</a></li>
		<li id="menu-item-634" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-634"><a href="http://demo.wpcharming.com/construction/testimonials/">Testimonials</a></li>
		<li id="menu-item-606" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-606"><a href="http://demo.wpcharming.com/construction/somethingwrong">404 Error Page</a></li>
	</ul>
</li>
	<li id="menu-item-584" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-584"><div class="nav-toggle-subarrow"><i class="fa fa-angle-down"></i></div><a href="http://demo.wpcharming.com/construction/#">Free Premium Plugins</a>
	<ul class="sub-menu">
		<li id="menu-item-585" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-585"><a href="http://demo.wpcharming.com/construction/#"># Free Visual Composer</a></li>
		<li id="menu-item-586" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-586"><a href="http://demo.wpcharming.com/construction/#"># Free Slider Revolution</a></li>
		<li id="menu-item-587" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-587"><a href="http://demo.wpcharming.com/construction/#"># Free Essential Grid</a></li>
	</ul>
</li>
</ul>
</li>
<li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="http://demo.wpcharming.com/construction/who-we-are/">Who We Are</a></li>
<li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-32"><div class="nav-toggle-subarrow"><i class="fa fa-angle-down"></i></div><a href="http://demo.wpcharming.com/construction/our-services/">Services</a>
<ul class="sub-menu">
	<li id="menu-item-225" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-225"><a href="http://demo.wpcharming.com/construction/our-services/construction-consultant/">Construction Consultant</a></li>
	<li id="menu-item-49" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49"><a href="http://demo.wpcharming.com/construction/our-services/metal-roofing/">Metal Roofing</a></li>
	<li id="menu-item-224" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-224"><a href="http://demo.wpcharming.com/construction/our-services/general-contracting/">General Contracting</a></li>
	<li id="menu-item-42" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-42"><a href="http://demo.wpcharming.com/construction/our-services/house-renovation/">House Renovation</a></li>
	<li id="menu-item-43" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-43"><a href="http://demo.wpcharming.com/construction/our-services/green-building/">Green Building</a></li>
	<li id="menu-item-226" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-226"><a href="http://demo.wpcharming.com/construction/our-services/laminate-flooring/">Laminate Flooring</a></li>
</ul>
</li>
<li id="menu-item-377" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-377"><div class="nav-toggle-subarrow"><i class="fa fa-angle-down"></i></div><a href="http://demo.wpcharming.com/construction/projects/">Our Work</a>
<ul class="sub-menu">
	<li id="menu-item-660" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-660"><div class="nav-toggle-subarrow"><i class="fa fa-angle-down"></i></div><a href="http://demo.wpcharming.com/construction/#">3 Columns</a>
	<ul class="sub-menu">
		<li id="menu-item-662" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-662"><a href="http://demo.wpcharming.com/construction/projects/">With Space</a></li>
		<li id="menu-item-667" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-667"><a href="http://demo.wpcharming.com/construction/projects-3-column-no-space/">Without Space</a></li>
	</ul>
</li>
	<li id="menu-item-661" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-661"><a href="http://demo.wpcharming.com/construction/project-full-width/">Project Full Width</a></li>
</ul>
</li>
<li id="menu-item-274" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-274"><a href="http://demo.wpcharming.com/construction/shop/">Shop</a></li>
<li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="http://demo.wpcharming.com/construction/news/">News</a></li>
<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="http://demo.wpcharming.com/construction/contact-us/">Contact Us</a></li>
					    </ul>
					</nav><!-- #site-navigation -->
				</div>
			</div>
			
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">		
				
		
		<div id="content-wrap" class=" full-screen">
			<div id="primary" class="content-area-full">
				<main id="main" class="site-main" role="main">

					
						<article id="post-13" class="post-13 page type-page status-publish hentry">

							<div class="entry-content">

								<div id="wpc_5506d5356301d" class="vc_row wpb_row vc_row-fluid  fit-screen">
		<div class="row_inner_wrapper  clearfix" style="background-position: left top;background-repeat: no-repeat;margin-top: -80px;float: left; width: 100%;padding-bottom: 0px;">
			<div class="row_inner row_fullwidth_content clearfix">
	<div class="vc_col-sm-12 wpb_column vc_column_container " style="padding-left: 0px; padding-right: 0px;">
		<div class="wpb_wrapper">
			<div class="wpb_revslider_element wpb_content_element">
<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin: 0px auto; padding: 0px; max-height: 650px; height: 650px; overflow: visible;">
<!-- START REVOLUTION SLIDER 4.6.5 fullwidth mode -->
	<div id="rev_slider_1_1" class="rev_slider fullwidthabanner revslider-initialised tp-simpleresponsive" style="max-height: 650px; height: 650px; background-image: url(http://demo.wpcharming.com/humanrights/wp-content/); background-position: 0% 100%; background-repeat: no-repeat;">
<ul class="tp-revslider-mainul" style="display: block; overflow: hidden; width: 100%; height: 100%; max-height: 650px;">	<!-- SLIDE  -->
	<li data-transition="fade,parallaxtotop" data-slotamount="7" data-masterspeed="300" data-thumb="http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider03-320x200.jpg" data-fstransition="fade" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="Welcome" class="tp-revslider-slidesli active-revslide" style="width: 100%; height: 100%; overflow: hidden; z-index: 18; visibility: hidden; opacity: 0;">
		<!-- MAIN IMAGE -->
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="center center" data-kenburns="undefined" data-easeme="undefined" data-bgfit="cover" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="undefined" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="http://www.maksoft.net/Templates/construction/slider/main_image_01.jpg" data-src="http://www.maksoft.net/Templates/construction/slider/main_image_01.jpg" style="width: 100%; height: 100%; opacity: 1; visibility: inherit; background-image: url(http://www.maksoft.net/Templates/construction/slider/main_image_01.jpg); background-color: rgba(0, 0, 0, 0); background-size: cover; background-position: 50% 50%; background-repeat: no-repeat;"></div></div>
		<!-- LAYERS -->

		<!-- LAYER NR. 1 -->
		<div class="tp-caption construction-large sfb tp-resizeme start" data-x="center" data-hoffset="0" data-y="center" data-voffset="-40" data-speed="1000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-captionhidden="on" style="z-index: 5; white-space: nowrap; -webkit-transition: all 0s ease 0s; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 40px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 13px; font-size: 70px; left: 408.5px; top: 265px; visibility: visible; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.0025, 50, 0, 0, 1);">WELCOME 
		</div>

		<!-- LAYER NR. 2 -->
		<div class="tp-caption construction-medium sfb tp-resizeme start" data-x="center" data-hoffset="0" data-y="center" data-voffset="20" data-speed="2000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-captionhidden="on" style="z-index: 6; white-space: nowrap; -webkit-transition: all 0s ease 0s; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 20px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 2px; font-size: 20px; left: 423.5px; top: 335px; visibility: visible; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.0025, 50, 0, 0, 1);">Creating a brighter future, together 
		</div>

		<!-- LAYER NR. 3 -->
		<div class="tp-caption black sfb tp-resizeme start" data-x="center" data-hoffset="0" data-y="center" data-voffset="80" data-speed="3000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7; white-space: nowrap; -webkit-transition: all 0s ease 0s; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 23px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 14px; left: 564.5px; top: 381.5px; visibility: visible; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00166, 0, 50, 0, 1);"><a class="btn btn-primary" href="http://demo.wpcharming.com/construction/#" style="-webkit-transition: all 0.2s ease-out 0s; transition: all 0.2s ease-out 0s; min-height: 0px; min-width: 0px; line-height: 13px; border-width: 0px; margin: 3px 5px 3px 0px; padding: 13px 18px 15px; letter-spacing: 1px; font-size: 13px;">WHO WE ARE �</a> 
		</div>
	</li>
	<!-- SLIDE  -->
	<li data-transition="fade,parallaxtotop" data-slotamount="7" data-masterspeed="300" data-thumb="http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider02-320x200.jpg" data-saveperformance="off" data-title="Dream House" class="tp-revslider-slidesli active-revslide current-sr-slide-visible" style="width: 100%; height: 100%; overflow: hidden; z-index: 20; visibility: inherit; opacity: 1;">
		<!-- MAIN IMAGE -->
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="center top" data-kenburns="undefined" data-easeme="undefined" data-bgfit="cover" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="undefined" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" data-lazydone="undefined" src="http://www.maksoft.net/Templates/" data-src="http://www.maksoft.net/Templates/construction/slider/main_image_01.jpg" style="width: 100%; height: 100%; opacity: 1; visibility: inherit; background-image: url(http://www.maksoft.net/Templates/construction/slider/main_image_01.jpg); background-color: rgba(0, 0, 0, 0); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;"></div></div>
		<!-- LAYERS -->

		<!-- LAYER NR. 1 -->
		<div class="tp-caption construction-slider-title sfb tp-resizeme start" data-x="50" data-y="center" data-voffset="-50" data-speed="1000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-captionhidden="on" style="z-index: 5; white-space: nowrap; -webkit-transition: all 0s ease 0s; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 40px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 40px; left: 78.5px; top: 255px; visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.0025, 0, 0, 0, 1);">
		</div>

		<!-- LAYER NR. 2 -->
		<div class="tp-caption construction-small-text sfb tp-resizeme start" data-x="50" data-y="center" data-voffset="20" data-speed="2000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-captionhidden="on" style="z-index: 6; white-space: nowrap; -webkit-transition: all 0s ease 0s; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 27px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 16px; left: 78.5px; top: 318px; visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.0025, 0, 0, 0, 1);">If you dream of designing a new home that takes full advantage<br style="-webkit-transition: all 0s ease 0s; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 27px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 16px;"> of the unique geography and views of land that you love 
		</div>

		<!-- LAYER NR. 3 -->
		<div class="tp-caption black sfb tp-resizeme start" data-x="220" data-y="center" data-voffset="90" data-speed="3000" data-start="450" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7; white-space: nowrap; -webkit-transition: all 0s ease 0s; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 23px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 14px; left: 248.5px; top: 391.5px; visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.0025, 0, 0, 0, 1);"><a class="btn btn-ghost" href="http://demo.wpcharming.com/construction/#" style="-webkit-transition: all 0.2s ease-out 0s; transition: all 0.2s ease-out 0s; min-height: 0px; min-width: 0px; line-height: 13px; border-width: 2px; margin: 3px 5px 3px 0px; padding: 11px 18px 13px; letter-spacing: 1px; font-size: 13px;">Get A Quote �</a> 
		</div>

		<!-- LAYER NR. 4 -->
		<div class="tp-caption black sfb tp-resizeme start" data-x="50" data-y="center" data-voffset="90" data-speed="3000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 8; white-space: nowrap; -webkit-transition: all 0s ease 0s; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 23px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 14px; left: 78.5px; top: 391.5px; visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.0025, 0, 0, 0, 1);"><a class="btn btn-primary" href="http://demo.wpcharming.com/construction/#" style="-webkit-transition: all 0.2s ease-out 0s; transition: all 0.2s ease-out 0s; min-height: 0px; min-width: 0px; line-height: 13px; border-width: 0px; margin: 3px 5px 3px 0px; padding: 13px 18px 15px; letter-spacing: 1px; font-size: 13px;">Our Services �</a> 
		</div>
	</li>
	<!-- SLIDE  -->
	<li data-transition="fade,parallaxtotop" data-slotamount="7" data-masterspeed="300" data-thumb="http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider04-320x200.jpg" data-saveperformance="off" data-title="Green House" class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden;">
		<!-- MAIN IMAGE -->
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="center bottom" data-kenburns="undefined" data-easeme="undefined" data-bgfit="cover" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="undefined" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" data-lazydone="undefined" src="http://www.maksoft.net/Templates/construction/slider/main_image_01.jpg" data-src="http://www.maksoft.net/Templates/construction/slider/main_image_01.jpg" style="background-color:rgba(0, 0, 0, 0);background-repeat:no-repeat;background-image:url(http://www.maksoft.net/Templates/construction/slider/main_image_01.jpg);background-size:cover;background-position:center bottom;width:100%;height:100%;"></div></div>
		<!-- LAYERS -->

		<!-- LAYER NR. 1 -->
		<div class="tp-caption construction-slider-title sfb tp-resizeme start" data-x="50" data-y="center" data-voffset="-50" data-speed="1000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-captionhidden="on" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Building a Green house 
		</div>

		<!-- LAYER NR. 2 -->
		<div class="tp-caption construction-small-text sfb tp-resizeme start" data-x="50" data-y="center" data-voffset="20" data-speed="2000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-captionhidden="on" style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">If you dream of designing a new home that takes full advantage<br> of the unique geography and views of land that you love 
		</div>

		<!-- LAYER NR. 3 -->
		<div class="tp-caption black sfb tp-resizeme start" data-x="220" data-y="center" data-voffset="90" data-speed="3000" data-start="450" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;"><a class="btn btn-ghost" href="http://demo.wpcharming.com/construction/#">Get A Quote �</a> 
		</div>

		<!-- LAYER NR. 4 -->
		<div class="tp-caption black sfb tp-resizeme start" data-x="50" data-y="center" data-voffset="90" data-speed="3000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><a class="btn btn-primary" href="http://demo.wpcharming.com/construction/#">Our Services �</a> 
		</div>
	</li>
	<!-- SLIDE  -->
	<li data-transition="fade,parallaxtotop" data-slotamount="7" data-masterspeed="300" data-thumb="http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider05-320x200.jpg" data-saveperformance="off" data-title="Swimming Pools" class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden; visibility: hidden; opacity: 0; z-index: 18;">
		<!-- MAIN IMAGE -->
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="center bottom" data-kenburns="undefined" data-easeme="undefined" data-bgfit="cover" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="undefined" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" data-lazydone="undefined" src="http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider05.jpg" data-src="http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider05.jpg" style="background-color:rgba(0, 0, 0, 0);background-repeat:no-repeat;background-image:url(http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider05.jpg);background-size:cover;background-position:center bottom;width:100%;height:100%;"></div></div>
		<!-- LAYERS -->

		<!-- LAYER NR. 1 -->
		<div class="tp-caption construction-slider-title sfb tp-resizeme start" data-x="50" data-y="center" data-voffset="-50" data-speed="1000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-captionhidden="on" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Swimming Pools Home 
		</div>

		<!-- LAYER NR. 2 -->
		<div class="tp-caption construction-small-text sfb tp-resizeme start" data-x="50" data-y="center" data-voffset="20" data-speed="2000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-captionhidden="on" style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">If you dream of designing a new home that takes full advantage<br> of the unique geography and views of land that you love 
		</div>

		<!-- LAYER NR. 3 -->
		<div class="tp-caption black sfb tp-resizeme start" data-x="220" data-y="center" data-voffset="90" data-speed="3000" data-start="450" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;"><a class="btn btn-ghost" href="http://demo.wpcharming.com/construction/#">Get A Quote �</a> 
		</div>

		<!-- LAYER NR. 4 -->
		<div class="tp-caption black sfb tp-resizeme start" data-x="50" data-y="center" data-voffset="90" data-speed="3000" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><a class="btn btn-primary" href="http://demo.wpcharming.com/construction/#">Our Services �</a> 
		</div>
	</li>
</ul>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important; width: 0%; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0, 1);"></div>	<div class="tp-loader spinner2" style="display: none;"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>
			

			<style scoped="">.tp-caption.black,.black{color:#000;text-shadow:none}.tp-caption.construction-large,.construction-large{font-size:70px;line-height:40px;font-weight:700;font-family:"Montserrat";color:#ffffff;text-decoration:none;background-color:transparent;margin:0px;white-space:nowrap;letter-spacing:13px;border-width:0px;border-color:rgb(255,255,255);border-style:none}.tp-caption.construction-medium,.construction-medium{color:#ffffff;font-weight:700;font-size:20px;line-height:20px;margin:0px;white-space:nowrap;background-color:transparent;text-decoration:none;text-transform:uppercase;letter-spacing:2px;border-width:0px;border-color:rgb(255,255,255);border-style:none}.tp-caption.construction-slider-title,.construction-slider-title{font-size:40px;line-height:40px;font-weight:700;font-family:"Montserrat";color:#ffffff;text-decoration:none;background-color:transparent;text-transform:uppercase;letter-spacing:-0.5px;margin:0px;white-space:nowrap;border-width:0px;border-color:rgb(255,255,255);border-style:none}.tp-caption.construction-small-text,.construction-small-text{color:#ffffff;font-weight:400;font-size:16px;line-height:27px;margin:0px;white-space:nowrap;background-color:transparent;text-decoration:none;border-width:0px;border-color:rgb(255,255,255);border-style:none}</style>

			<script type="text/javascript">

				/******************************************
					-	PREPARE PLACEHOLDER FOR SLIDER	-
				******************************************/
				

				var setREVStartSize = function() {
					var	tpopt = new Object();
						tpopt.startwidth = 1230;
						tpopt.startheight = 650;
						tpopt.container = jQuery('#rev_slider_1_1');
						tpopt.fullScreen = "off";
						tpopt.forceFullWidth="off";

					tpopt.container.closest(".rev_slider_wrapper").css({height:tpopt.container.height()});tpopt.width=parseInt(tpopt.container.width(),0);tpopt.height=parseInt(tpopt.container.height(),0);tpopt.bw=tpopt.width/tpopt.startwidth;tpopt.bh=tpopt.height/tpopt.startheight;if(tpopt.bh>tpopt.bw)tpopt.bh=tpopt.bw;if(tpopt.bh<tpopt.bw)tpopt.bw=tpopt.bh;if(tpopt.bw<tpopt.bh)tpopt.bh=tpopt.bw;if(tpopt.bh>1){tpopt.bw=1;tpopt.bh=1}if(tpopt.bw>1){tpopt.bw=1;tpopt.bh=1}tpopt.height=Math.round(tpopt.startheight*(tpopt.width/tpopt.startwidth));if(tpopt.height>tpopt.startheight&&tpopt.autoHeight!="on")tpopt.height=tpopt.startheight;if(tpopt.fullScreen=="on"){tpopt.height=tpopt.bw*tpopt.startheight;var cow=tpopt.container.parent().width();var coh=jQuery(window).height();if(tpopt.fullScreenOffsetContainer!=undefined){try{var offcontainers=tpopt.fullScreenOffsetContainer.split(",");jQuery.each(offcontainers,function(e,t){coh=coh-jQuery(t).outerHeight(true);if(coh<tpopt.minFullScreenHeight)coh=tpopt.minFullScreenHeight})}catch(e){}}tpopt.container.parent().height(coh);tpopt.container.height(coh);tpopt.container.closest(".rev_slider_wrapper").height(coh);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);tpopt.container.css({height:"100%"});tpopt.height=coh;}else{tpopt.container.height(tpopt.height);tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);}
				};

				/* CALL PLACEHOLDER */
				setREVStartSize();


				var tpj=jQuery;
				tpj.noConflict();
				var revapi1;

				tpj(document).ready(function() {

				if(tpj('#rev_slider_1_1').revolution == undefined){
					revslider_showDoubleJqueryError('#rev_slider_1_1');
				}else{
				   revapi1 = tpj('#rev_slider_1_1').show().revolution(
					{	
												dottedOverlay:"none",
						delay:9000,
						startwidth:1230,
						startheight:650,
						hideThumbs:10,

						thumbWidth:100,
						thumbHeight:50,
						thumbAmount:4,
						
												
						simplifyAll:"off",

						navigationType:"none",
						navigationArrows:"none",
						navigationStyle:"preview4",

						touchenabled:"on",
						onHoverStop:"on",
						nextSlideOnWindowFocus:"off",

						swipe_threshold: 75,
						swipe_min_touches: 1,
						drag_block_vertical: false,
						
												
												
						keyboardNavigation:"on",

						navigationHAlign:"center",
						navigationVAlign:"bottom",
						navigationHOffset:0,
						navigationVOffset:20,

						soloArrowLeftHalign:"left",
						soloArrowLeftValign:"center",
						soloArrowLeftHOffset:20,
						soloArrowLeftVOffset:0,

						soloArrowRightHalign:"right",
						soloArrowRightValign:"center",
						soloArrowRightHOffset:20,
						soloArrowRightVOffset:0,

						shadow:0,
						fullWidth:"on",
						fullScreen:"off",

												spinner:"spinner2",
												
						stopLoop:"on",
						stopAfterLoops:0,
						stopAtSlide:2,

						shuffle:"off",

						autoHeight:"off",
						forceFullWidth:"off",
						
						
						hideTimerBar:"on",
						hideThumbsOnMobile:"off",
						hideNavDelayOnMobile:1500,
						hideBulletsOnMobile:"off",
						hideArrowsOnMobile:"off",
						hideThumbsUnderResolution:0,

												hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:631,
						startWithSlide:0					});



									}
				});	/*ready*/

			</script>


			<style type="text/css">
	#rev_slider_1_1_wrapper .tp-loader.spinner2{ background-color: #ffffff !important; }
</style>
<div style="position: absolute; top: 325px; margin-top: -55px; left: 0px;" class="tp-leftarrow tparrows default preview4 hidearrows"><div class="tp-arr-allwrapper"><div class="tp-arr-iwrapper"><div class="tp-arr-imgholder" style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0, 1); background-image: url(http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider03-320x200.jpg);"></div><div class="tp-arr-imgholder2" style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0, 1); background-image: url(http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider03-320x200.jpg);"></div><div class="tp-arr-titleholder">Welcome</div><div class="tp-arr-subtitleholder"></div></div></div></div><div style="position: absolute; top: 325px; margin-top: -55px; right: 0px;" class="tp-rightarrow tparrows default preview4 hashoveralready hidearrows"><div class="tp-arr-allwrapper"><div class="tp-arr-iwrapper"><div class="tp-arr-imgholder" style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0, 1); background-image: url(http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider04-320x200.jpg);"></div><div class="tp-arr-imgholder2" style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0, 1); background-image: url(http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider04-320x200.jpg);"></div><div class="tp-arr-titleholder">Green House</div><div class="tp-arr-subtitleholder"></div></div></div></div></div><!-- END REVOLUTION SLIDER --></div>

		</div> 
	</div> 

			</div>
		</div>
</div>
<div id="wpc_5506d53567f03" class="vc_row wpb_row vc_row-fluid  mobile-no-margin-top inverted-row">
		<div class="row_inner_wrapper  clearfix" style="background-position: left top;background-repeat: no-repeat;margin-top: -119px;float: left; width: 100%;padding-bottom: 50px;">
			<div class="row_inner container clearfix">
				<div class="row_full_center_content clearfix">
	<div class="vc_col-sm-4 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="wpb_content_element featured-box ">
			<div class="featured-box-thumb">
					<a class="popup-video" href="https://www.youtube.com/watch?v=ZhCyoDlltbo">
						<img src="/Templates/construction/files/House-renovation-600x300.jpg">
						<span class="video_icon"><i class="fa fa-play"></i></span>
					</a>
			</div>
			<div class="featured-box-content"><h4>Best House Renovation</h4>
					<div class="featured-box-desc"><p>Constructor explains how you can enjoy high end flooring trends like textured wood and realistic stones with new laminate flooring.</p>
					</div>
					<div class="featured-box-button">
						<a href="http://demo.wpcharming.com/construction/#" class="">Read More</a>
					</div>
			</div>
	</div>
		</div> 
	</div> 

	<div class="vc_col-sm-4 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="wpb_content_element featured-box ">
			<div class="featured-box-thumb">
					<a class="popup-video" href="https://www.youtube.com/watch?v=ZhCyoDlltbo">
						<img src="/Templates/construction/files/teamwork-606818_1280-600x300.jpg">
						<span class="video_icon"><i class="fa fa-play"></i></span>
					</a>
			</div>
			<div class="featured-box-content"><h4>The Effective Teamwork</h4>
					<div class="featured-box-desc"><p>As the general contractor, we first create the highest level of trust and integrity with our clients. We value our role in the success of your project.</p>
					</div>
					<div class="featured-box-button">
						<a href="http://demo.wpcharming.com/construction/#" class="">The Benefits</a>
					</div>
			</div>
	</div>
		</div> 
	</div> 

	<div class="vc_col-sm-4 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="wpb_content_element featured-box ">
			<div class="featured-box-thumb"><a href="http://demo.wpcharming.com/construction/#"><img src="/Templates/construction/files/House2-600x300.jpg"></a>
			</div>
			<div class="featured-box-content"><h4>The Green Building</h4>
					<div class="featured-box-desc"><p>Green construction refers to a structure and using process that is environmentally responsible and resource-efficient throughout a building's life-cycle.</p>
					</div>
					<div class="featured-box-button">
						<a href="http://demo.wpcharming.com/construction/#" class="">The Progress</a>
					</div>
			</div>
	</div>
		</div> 
	</div> 

				</div>
			</div>
		</div>
</div>
<div id="wpc_5506d53569be8" class="vc_row wpb_row vc_row-fluid  ">
		<div class="row_inner_wrapper  clearfix" style="background-position: left top;background-repeat: no-repeat;padding-top: 50px;padding-bottom: 50px;">
			<div class="row_inner container clearfix">
				<div class="row_full_center_content clearfix">
	<div class="vc_col-sm-12 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="custom-heading wpb_content_element ">
		<h2 class="heading-title">U.S. Certified Contractors</h2>
		<span class="heading-line primary"></span>
	</div>
<div id="wpc_5506d5356a2bc" class="vc_row wpb_row vc_row-fluid  ">
		<div class="row_inner_wrapper  clearfix">
			<div class="row_inner row_center_content clearfix">
	<div class="vc_col-sm-4 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="iconbox-wrapper  text-center">
		<div class="iconbox-icon">
				<i class="fa fa-institution " style="color: #777777;font-size:70px;"></i>
		</div>
		<h4 class="iconbox-heading">
				<a href="http://demo.wpcharming.com/construction/#">Government Building</a>
		</h4>
			<div class="iconbox-desc">
				<p>We understand you need a building that works for<br>
you and your organization, and it must function well.</p>
			</div>
	</div>
		</div> 
	</div> 

	<div class="vc_col-sm-4 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="iconbox-wrapper  text-center">
		<div class="iconbox-icon">
				<i class="fa fa-hospital-o " style="color: #777777;font-size:70px;"></i>
		</div>
		<h4 class="iconbox-heading">Health Care Construction
		</h4>
			<div class="iconbox-desc">
				<p>We are very familiar with the challenges of creating high-quality, cost-effective health care environments.</p>
			</div>
	</div>
		</div> 
	</div> 

	<div class="vc_col-sm-4 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="iconbox-wrapper  text-center">
		<div class="iconbox-icon">
				<i class="fa fa-beer " style="color: #777777;font-size:70px;"></i>
		</div>
		<h4 class="iconbox-heading">Water Treatment
		</h4>
			<div class="iconbox-desc">
				<p>The most powerful things we do is help improve<br>
water quality for millions of people each year. </p>
			</div>
	</div>
		</div> 
	</div> 

			</div>
		</div>
</div>
		</div> 
	</div> 

				</div>
			</div>
		</div>
</div>
<div id="wpc_5506d5356b007" class="vc_row wpb_row vc_row-fluid  ">
		<div class="row_inner_wrapper wpc_row_parallax clearfix" style="padding-top: 150px;padding-bottom: 150px;color: #FFFFFF;" data-bg="http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider06.jpg" data-speed="0.5"><div class="wpc_video_color_overlay" style="background-color:"></div>
			<div class="wpc_parallax_bg not-mobile" style="background-image: url(http://demo.wpcharming.com/construction/wp-content/uploads/2015/02/new_slider06.jpg);"></div>
			<div class="row_inner container clearfix">
				<div class="row_full_center_content clearfix">
	<div class="vc_col-sm-8 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h5 class="medium-heading-inverted">Working with us<em><br>
</em></h5>
<h1 class="large-heading-inverted"><strong>Contractors &amp; Construction<br>
Managers Since 1989</strong></h1>

		</div> 
	</div> <a href="http://demo.wpcharming.com/construction/#" class="btn  btn-regular btn-ghost" style="margin-top: -20px;">Get A Quote</a>
		</div> 
	</div> 

	<div class="vc_col-sm-4 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
		</div> 
	</div> 

				</div>
			</div>
		</div>
</div>
<div id="wpc_5506d5356bf5f" class="vc_row wpb_row vc_row-fluid  ">
		<div class="row_inner_wrapper  clearfix" style="background-position: left top;background-repeat: no-repeat;padding-top: 60px;">
			<div class="row_inner container clearfix">
				<div class="row_full_center_content clearfix">
	<div class="vc_col-sm-12 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="custom-heading wpb_content_element ">
		<h2 class="heading-title">What We Do</h2>
		<span class="heading-line primary"></span>
	</div>
	<div class="child-page-wrapper">
			<script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery(".carousel-wrapper").slick({
						slidesToShow: 3,
						slidesToScroll: 1,
						draggable: false,
						prevArrow: "<span class='carousel-prev'><i class='fa fa-angle-left'></i></span>",
        				nextArrow: "<span class='carousel-next'><i class='fa fa-angle-right'></i></span>",
        				responsive: [{
						    breakpoint: 1024,
						    settings: {
						    slidesToShow: 3
						    }
						},
						{
						    breakpoint: 600,
						    settings: {
						    slidesToShow: 2
						    }
						},
						{
						    breakpoint: 480,
						    settings: {
						    slidesToShow: 1
						    }
						}]
					});
				});
			</script>
		<div class="grid-wrapper grid-3-columns grid-row carousel-wrapper slick-initialized slick-slider">
			<div class="slick-list" tabindex="0"><div class="slick-track" style="opacity: 1; width: 4680px; transform: translate3d(-1170px, 0px, 0px);"><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="-3" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/house-renovation/" title="House Renovation"><img width="600" height="300" src="/Templates/construction/files/House-renovation-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="House renovation"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/house-renovation/" rel="bookmark">House Renovation</a></h3>

				<p>With over thirty years of experience in residential renovation design and over 16,000 renovation plans delivered to this day.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/house-renovation/" title="House Renovation">Service Detail</a>

			</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="-2" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/green-building/" title="Green Building"><img width="600" height="300" src="/Templates/construction/files/green-building-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="green-building"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/green-building/" rel="bookmark">Green Building</a></h3>

				<p>Green construction refers to a structure and using process that is environmentally responsible and resource-efficient throughout a building's life-cycle.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/green-building/" title="Green Building">Service Detail</a>

			</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="-1" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/laminate-flooring/" title="Laminate Flooring"><img width="600" height="300" src="/Templates/construction/files/laminate-floor-covering-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="laminate floor covering"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/laminate-flooring/" rel="bookmark">Laminate Flooring</a></h3>

				<p>The flooring professionals at Construction offering laminate flooring and timber flooring with award winning flooring installation services.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/laminate-flooring/" title="Laminate Flooring">Service Detail</a>

			</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-active" data-slick-index="0" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/metal-roofing/" title="Metal Roofing"><img width="600" height="300" src="/Templates/construction/files/builder-roofer-painter-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="builder roofer painter"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/metal-roofing/" rel="bookmark">Metal Roofing</a></h3>

				<p>Homeowners can have confidence in their choice of a metal roofing contractor knowing they have met certain requirements in their services.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/metal-roofing/" title="Metal Roofing">Service Detail</a>

			</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-active" data-slick-index="1" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/general-contracting/" title="General Contracting"><img width="600" height="300" src="/Templates/construction/files/slider03-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="slider03"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/general-contracting/" rel="bookmark">General Contracting</a></h3>

				<p>As the general contractor, we first create the highest level of trust and integrity with our clients. We value our role in the success of your project.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/general-contracting/" title="General Contracting">Service Detail</a>

			</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-active" data-slick-index="2" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/construction-consultant/" title="Construction Consultant"><img width="600" height="300" src="/Templates/construction/files/slider04-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="slider04"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/construction-consultant/" rel="bookmark">Construction Consultant</a></h3>

				<p>Construction consultants provide expert proactive and forensic support for construction projects and construction claims.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/construction-consultant/" title="Construction Consultant">Service Detail</a>

			</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide" data-slick-index="3" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/house-renovation/" title="House Renovation"><img width="600" height="300" src="/Templates/construction/files/House-renovation-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="House renovation"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/house-renovation/" rel="bookmark">House Renovation</a></h3>

				<p>With over thirty years of experience in residential renovation design and over 16,000 renovation plans delivered to this day.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/house-renovation/" title="House Renovation">Service Detail</a>

			</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide" data-slick-index="4" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/green-building/" title="Green Building"><img width="600" height="300" src="/Templates/construction/files/green-building-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="green-building"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/green-building/" rel="bookmark">Green Building</a></h3>

				<p>Green construction refers to a structure and using process that is environmentally responsible and resource-efficient throughout a building's life-cycle.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/green-building/" title="Green Building">Service Detail</a>

			</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide" data-slick-index="5" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/laminate-flooring/" title="Laminate Flooring"><img width="600" height="300" src="/Templates/construction/files/laminate-floor-covering-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="laminate floor covering"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/laminate-flooring/" rel="bookmark">Laminate Flooring</a></h3>

				<p>The flooring professionals at Construction offering laminate flooring and timber flooring with award winning flooring installation services.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/laminate-flooring/" title="Laminate Flooring">Service Detail</a>

			</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="6" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/metal-roofing/" title="Metal Roofing"><img width="600" height="300" src="/Templates/construction/files/builder-roofer-painter-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="builder roofer painter"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/metal-roofing/" rel="bookmark">Metal Roofing</a></h3>

				<p>Homeowners can have confidence in their choice of a metal roofing contractor knowing they have met certain requirements in their services.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/metal-roofing/" title="Metal Roofing">Service Detail</a>

			</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="7" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/general-contracting/" title="General Contracting"><img width="600" height="300" src="/Templates/construction/files/slider03-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="slider03"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/general-contracting/" rel="bookmark">General Contracting</a></h3>

				<p>As the general contractor, we first create the highest level of trust and integrity with our clients. We value our role in the success of your project.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/general-contracting/" title="General Contracting">Service Detail</a>

			</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="8" style="width: 390px;">
				<div class="grid-thumbnail">
					<a href="http://demo.wpcharming.com/construction/our-services/construction-consultant/" title="Construction Consultant"><img width="600" height="300" src="/Templates/construction/files/slider04-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="slider04"></a>
				</div>
				<h3 class="grid-title"><a href="http://demo.wpcharming.com/construction/our-services/construction-consultant/" rel="bookmark">Construction Consultant</a></h3>

				<p>Construction consultants provide expert proactive and forensic support for construction projects and construction claims.</p>

				<a class="grid-more" href="http://demo.wpcharming.com/construction/our-services/construction-consultant/" title="Construction Consultant">Service Detail</a>

			</div></div></div>
			
			
			
			
			
			
			
			
			
			
			
		<span class="carousel-prev" style="display: block;"><i class="fa fa-angle-left"></i></span><span class="carousel-next" style="display: block;"><i class="fa fa-angle-right"></i></span></div>
	</div>
		</div> 
	</div> 

				</div>
			</div>
		</div>
</div>
<div id="wpc_5506d535705f2" class="vc_row wpb_row vc_row-fluid  inverted-row">
		<div class="row_inner_wrapper  clearfix" style="background-color: #222222;background-position: left top;background-repeat: no-repeat;padding-top: 70px;padding-bottom: 70px;">
			<div class="row_inner container clearfix">
				<div class="row_full_center_content clearfix">
	<div class="vc_col-sm-12 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="custom-heading wpb_content_element " style="margin-bottom: -40px;">
		<h2 class="heading-title" style="color: #ffffff;">Featured Works</h2>
		<span class="heading-line " style="background-color: #444444"></span>
	</div><style type="text/css">a.eg-henryharrison-element-1,a.eg-henryharrison-element-2{-webkit-transition:all .4s linear;   -moz-transition:all .4s linear;   -o-transition:all .4s linear;   -ms-transition:all .4s linear;   transition:all .4s linear}.eg-jimmy-carter-element-11 i:before{margin-left:0px; margin-right:0px}.eg-harding-element-17{letter-spacing:1px}.eg-harding-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-harding-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-ulysses-s-grant-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-ulysses-s-grant-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-richard-nixon-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-richard-nixon-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-herbert-hoover-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-herbert-hoover-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.eg-lyndon-johnson-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-lyndon-johnson-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.esg-overlay.eg-ronald-reagan-container{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.eg-georgebush-wrapper .esg-entry-cover{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.eg-jefferson-wrapper{-webkit-border-radius:5px !important; -moz-border-radius:5px !important; border-radius:5px !important; -webkit-mask-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC) !important}.eg-monroe-element-1{text-shadow:0px 1px 3px rgba(0,0,0,0.1)}.eg-lyndon-johnson-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0))); background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#59000000',endColorstr='#00131313',GradientType=1 )}.eg-wilbert-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0))); background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#59000000',endColorstr='#00131313',GradientType=1 )}.eg-wilbert-wrapper .esg-entry-media img{-webkit-transition:0.4s ease-in-out;  -moz-transition:0.4s ease-in-out;  -o-transition:0.4s ease-in-out;  transition:0.4s ease-in-out;  filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-wilbert-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.eg-phillie-element-3:after{content:" ";width:0px;height:0px;border-style:solid;border-width:5px 5px 0 5px;border-color:#000 transparent transparent transparent;left:50%;margin-left:-5px; bottom:-5px; position:absolute}.eg-howardtaft-wrapper .esg-entry-media img,.eg-howardtaft-wrapper .esg-media-poster{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.eg-howardtaft-wrapper:hover .esg-entry-media img,.eg-howardtaft-wrapper:hover .esg-media-poster{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.myportfolio-container .added_to_cart.wc-forward{font-family:"Open Sans"; font-size:13px; color:#fff; margin-top:10px}.esgbox-title.esgbox-title-outside-wrap{font-size:15px; font-weight:700; text-align:center}.esgbox-title.esgbox-title-inside-wrap{padding-bottom:10px; font-size:15px; font-weight:700; text-align:center}</style>
<!-- CACHE FOUND FOR: 2 --><style type="text/css">.wpc-projects-dark .navigationbuttons,.wpc-projects-dark .esg-pagination,.wpc-projects-dark .esg-filters{text-align:center}.wpc-projects-dark .esg-filterbutton,.wpc-projects-dark .esg-navigationbutton,.wpc-projects-dark .esg-sortbutton,.wpc-projects-dark .esg-cartbutton,.wpc-projects-dark .esg-selected-filterbutton{color:#999999;  margin-right:20px;  cursor:pointer;  position:relative;  z-index:2;  border:none;  font-size:14px;  font-weight:500;  display:inline-block;  background:transparent;  font-family:"Montserrat",Helvetica,Arial,sans-serif}.wpc-projects-dark .esg-navigationbutton{padding:2px 12px}.wpc-projects-dark .esg-navigationbutton *{color:#000}.wpc-projects-dark .esg-pagination-button:last-child{margin-right:0}.wpc-projects-dark .esg-sortbutton-wrapper,.wpc-projects-dark .esg-cartbutton-wrapper{display:inline-block}.wpc-projects-dark .esg-sortbutton-order,.wpc-projects-dark .esg-cartbutton-order{display:inline-block;  vertical-align:top;  border:none;  width:40px;  line-height:40px;  border-radius:5px;  -moz-border-radius:5px;  -webkit-border-radius:5px;  font-size:12px;  font-weight:700;  color:#999;  cursor:pointer;  background:transparent;  margin-left:5px}.wpc-projects-dark .esg-cartbutton{color:#fff;cursor:default !important}.wpc-projects-dark .esg-cartbutton .esgicon-basket{color:#fff;  font-size:15px;  line-height:15px;  margin-right:10px}.wpc-projects-dark .esg-cartbutton-wrapper{cursor:default !important}.wpc-projects-dark .esg-sortbutton,.wpc-projects-dark .esg-cartbutton{display:inline-block;  position:relative;  cursor:pointer;  margin-right:0px;  border-radius:5px;  -moz-border-radius:5px;  -webkit-border-radius:5px}.wpc-projects-dark .esg-navigationbutton:hover,.wpc-projects-dark .esg-filterbutton:hover,.wpc-projects-dark .esg-sortbutton:hover,.wpc-projects-dark .esg-sortbutton-order:hover,.wpc-projects-dark .esg-cartbutton-order:hover,.wpc-projects-dark .esg-filterbutton.selected{border-color:none;color:#fab702;  background:transparent}.wpc-projects-dark .esg-navigationbutton:hover *{color:#333}.wpc-projects-dark .esg-sortbutton-order.tp-desc:hover{color:#333}.wpc-projects-dark .esg-filter-checked{padding:1px 3px;  color:#cbcbcb;  background:#cbcbcb;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle}.wpc-projects-dark .esg-filterbutton.selected .esg-filter-checked,.wpc-projects-dark .esg-filterbutton:hover .esg-filter-checked{padding:1px 3px 1px 3px;  color:#fff;  background:#000;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle}.wpc-projects-dark .esg-navigationbutton{background:transparent;  border:2px solid rgba(255,255,255,0.2);  padding:3px 9px 5px;  border-radius:2px}.wpc-projects-dark .esg-navigationbutton *{color:rgba(255,255,255,0.2);font-size:11px}.wpc-projects-dark .esg-navigationbutton:hover{border-color:rgba(255,255,255,0.5);  background:transparent}.wpc-projects-dark .esg-navigationbutton:hover *{color:rgba(255,255,255,0.5)}</style>
<!-- ESSENTIAL GRID SKIN CSS -->
<style type="text/css">.eg-wpc_projects-element-7{font-size:15px !important; line-height:19px !important; color:#ffffff !important; font-weight:700 !important; padding:0px 0px 0px 0px !important; border-radius:0px 0px 0px 0px !important; background-color:rgba(255,255,255,0) !important; z-index:2 !important; display:block; font-family:"Montserrat" !important; text-transform:uppercase !important}.eg-wpc_projects-element-1{font-size:13px; line-height:30px; color:#ffffff; font-weight:800; padding:2px 15px 3px 15px ; border-radius:2px 2px 2px 2px ; background-color:rgba(250,183,2,1.00); z-index:2 !important; display:block; font-family:"Montserrat"; border-top-width:0px; border-right-width:0px; border-bottom-width:0px; border-left-width:0px; border-color:#617f52; border-style:solid}</style>
<style type="text/css"></style>
<style type="text/css">.eg-wpc_projects-element-7-a{display:block !important; text-align:left !important; clear:both !important; margin:10px 0px 20px 20px !important; position:relative !important}</style>
<style type="text/css">.eg-wpc_projects-element-1-a{display:inline-block; float:left; clear:both; margin:0px 0px 20px 20px ; position:relative}</style>
<style type="text/css">.eg-wpc_projects-container{background-color:rgba(0,0,0,0.70)}</style>
<style type="text/css">.eg-wpc_projects-content{background-color:#ffffff; padding:0px 0px 0px 0px; border-width:0px 0px 0px 0px; border-radius:0px 0px 0px 0px; border-color:transparent; border-style:none; text-align:left}</style>
<style type="text/css">.esg-grid .mainul li.eg-wpc_projects-wrapper{background-color:#ffffff; padding:0px 0px 0px 0px; border-width:0px 0px 0px 0px; border-radius:0px 0px 0px 0px; border-color:transparent; border-style:none}</style>
<!-- ESSENTIAL GRID END SKIN CSS -->

<!-- THE ESSENTIAL GRID 2.0.6 -->

<!-- GRID WRAPPER FOR CONTAINER SIZING - HERE YOU CAN SET THE CONTAINER SIZE AND CONTAINER SKIN -->
<article class="myportfolio-container wpc-projects-dark" id="wpc_projects_dark" style="position: relative; z-index: 0; min-height: 100px; height: auto;">

    <!-- THE GRID ITSELF WITH FILTERS, PAGINATION, SORTING ETC... -->
    <div class="esg-container-fullscreen-forcer" style="position: relative; left: 0px; top: 0px; width: auto; height: auto;"><div id="esg-grid-2-1" class="esg-grid esg-layout-even esg-container" style="background-color: transparent;padding: 0px 0px 0px 0px ; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;">
<article class="esg-filters esg-singlefilters" style="margin-bottom: 0px; text-align: right; "><div class="esg-navigationbutton esg-left  esg-fgc-2" style="display: inline-block; margin-left: 2.5px !important; margin-right: 2.5px !important; visibility: visible;"><i class="eg-icon-left-open"></i></div><div class="esg-navigationbutton esg-right  esg-fgc-2" style="display: inline-block; margin-left: 2.5px !important; margin-right: 2.5px !important; visibility: visible;"><i class="eg-icon-right-open"></i></div></article><div class="esg-clear-no-height"></div><!-- ############################ -->
<!-- THE GRID ITSELF WITH ENTRIES -->
<!-- ############################ -->
<div class="esg-overflowtrick" style="width: 100%; height: 268px;"><ul style="display: block; height: 268px;" class="mainul">
<!-- PORTFOLIO ITEM 45 -->
<li class="filterall filter-interior-design eg-wpc_projects-wrapper eg-post-id-373 tp-esg-item itemtoshow esg-hovered isvisiblenow" data-date="1423771025" style="opacity: 1; visibility: inherit; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0.1, 0.999916666666667); height: 268px; width: 357px; display: block; top: 0px; left: 0px; transform-origin: center center 0px;">
    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
    <div class="esg-media-cover-wrapper">
            <!-- THE MEDIA OF THE ENTRY -->
<div class="esg-entry-media-wrapper" style="width:100%;height:100%; overflow:hidden;position:relative;"><div class="esg-entry-media"><img src="/Templates/construction/files/slider01-600x300.jpg" alt="" style="top: 0%; left: -25%; width: auto; height: 101%; visibility: visible; display: block; position: absolute;"></div></div>

            <!-- THE CONTENT OF THE ENTRY -->
            <div class="esg-entry-cover" style="transform-style: flat; height: 268px;">

                <!-- THE COLORED OVERLAY -->
                <div class="esg-overlay esg-fade eg-wpc_projects-container" data-delay="0" style="visibility: hidden; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, -0.002, 1.000002);"></div>

				<div class="esg-bc eec"><div class="esg-bottom eg-post-373 eg-wpc_projects-element-7-a esg-slideup" data-delay="0" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-7 eg-post-373" href="http://demo.wpcharming.com/construction/project/the-langham-hotel/" target="_self">The Langham Hotel</a></div><div class="esg-bottom eg-post-373 eg-wpc_projects-element-1-a esg-slideup" data-delay="0.15" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-1 eg-post-373" href="http://demo.wpcharming.com/construction/project/the-langham-hotel/" target="_self">Read More</a></div><div></div></div>
				
           </div><!-- END OF THE CONTENT IN THE ENTRY -->
   </div><!-- END OF THE CONTAINER FOR THE MEDIA AND COVER/HOVER EFFECTS -->

</li><!-- END OF PORTFOLIO ITEM -->
<!-- PORTFOLIO ITEM 45 -->
<li class="filterall filter-interior-design filter-office eg-wpc_projects-wrapper eg-post-id-375 tp-esg-item itemtoshow esg-hovered isvisiblenow" data-date="1423771394" style="opacity: 1; visibility: inherit; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0.1, 0.999916666666667); height: 268px; width: 357px; display: block; top: 0px; left: 387px; transform-origin: center center 0px;">
    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
    <div class="esg-media-cover-wrapper">
            <!-- THE MEDIA OF THE ENTRY -->
<div class="esg-entry-media-wrapper" style="width:100%;height:100%; overflow:hidden;position:relative;"><div class="esg-entry-media"><img src="/Templates/construction/files/slider02-600x300.jpg" alt="" style="top: 0%; left: -25%; width: auto; height: 101%; visibility: visible; display: block; position: absolute;"></div></div>

            <!-- THE CONTENT OF THE ENTRY -->
            <div class="esg-entry-cover" style="transform-style: flat; height: 268px;">

                <!-- THE COLORED OVERLAY -->
                <div class="esg-overlay esg-fade eg-wpc_projects-container" data-delay="0" style="visibility: hidden; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, -0.002, 1.000002);"></div>

				<div class="esg-bc eec"><div class="esg-bottom eg-post-375 eg-wpc_projects-element-7-a esg-slideup" data-delay="0" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-7 eg-post-375" href="http://demo.wpcharming.com/construction/project/french-quarter-inn/" target="_self">French Quarter Inn</a></div><div class="esg-bottom eg-post-375 eg-wpc_projects-element-1-a esg-slideup" data-delay="0.15" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-1 eg-post-375" href="http://demo.wpcharming.com/construction/project/french-quarter-inn/" target="_self">Read More</a></div><div></div></div>
				
           </div><!-- END OF THE CONTENT IN THE ENTRY -->
   </div><!-- END OF THE CONTAINER FOR THE MEDIA AND COVER/HOVER EFFECTS -->

</li><!-- END OF PORTFOLIO ITEM -->
<!-- PORTFOLIO ITEM 45 -->
<li class="filterall filter-healthcare filter-office eg-wpc_projects-wrapper eg-post-id-421 tp-esg-item itemtoshow esg-hovered isvisiblenow" data-date="1423782243" style="opacity: 1; visibility: inherit; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0.1, 0.999916666666667); height: 268px; width: 357px; display: block; top: 0px; left: 774px; transform-origin: center center 0px;">
    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
    <div class="esg-media-cover-wrapper">
            <!-- THE MEDIA OF THE ENTRY -->
<div class="esg-entry-media-wrapper" style="width:100%;height:100%; overflow:hidden;position:relative;"><div class="esg-entry-media"><img src="/Templates/construction/files/hospital-600x300.jpg" alt="" style="top: 0%; left: -25%; width: auto; height: 101%; visibility: visible; display: block; position: absolute;"></div></div>

            <!-- THE CONTENT OF THE ENTRY -->
            <div class="esg-entry-cover" style="transform-style: flat; height: 268px;">

                <!-- THE COLORED OVERLAY -->
                <div class="esg-overlay esg-fade eg-wpc_projects-container" data-delay="0" style="visibility: hidden; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, -0.002, 1.000002);"></div>

				<div class="esg-bc eec"><div class="esg-bottom eg-post-421 eg-wpc_projects-element-7-a esg-slideup" data-delay="0" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-7 eg-post-421" href="http://demo.wpcharming.com/construction/project/guadalupe-clinic-renovation/" target="_self">Guadalupe Clinic Renovation</a></div><div class="esg-bottom eg-post-421 eg-wpc_projects-element-1-a esg-slideup" data-delay="0.15" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-1 eg-post-421" href="http://demo.wpcharming.com/construction/project/guadalupe-clinic-renovation/" target="_self">Read More</a></div><div></div></div>
				
           </div><!-- END OF THE CONTENT IN THE ENTRY -->
   </div><!-- END OF THE CONTAINER FOR THE MEDIA AND COVER/HOVER EFFECTS -->

</li><!-- END OF PORTFOLIO ITEM -->
<!-- PORTFOLIO ITEM 45 -->
<li class="filterall filter-education filter-green-building filter-healthcare eg-wpc_projects-wrapper eg-post-id-427 tp-esg-item itemonotherpage fitsinfilter esg-hovered" data-date="1423782709" style="opacity: 0; visibility: hidden; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0.01, 0.999991666666667);">
    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
    <div class="esg-media-cover-wrapper">
            <!-- THE MEDIA OF THE ENTRY -->
<div class="esg-entry-media-wrapper" style="width:100%;height:100%; overflow:hidden;position:relative;"><div class="esg-entry-media"><img src="/Templates/construction/files/architecture-426426_1280-600x300.jpg" alt="" style="top: 0px; left: 0px; width: 100%; height: auto; visibility: visible; display: block;"></div></div>

            <!-- THE CONTENT OF THE ENTRY -->
            <div class="esg-entry-cover" style="transform-style: flat;">

                <!-- THE COLORED OVERLAY -->
                <div class="esg-overlay esg-fade eg-wpc_projects-container" data-delay="0" style="visibility: hidden; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, -0.002, 1.000002);"></div>

				<div class="esg-bc eec"><div class="esg-bottom eg-post-427 eg-wpc_projects-element-7-a esg-slideup" data-delay="0" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-7 eg-post-427" href="http://demo.wpcharming.com/construction/project/clinical-research-center/" target="_self">Clinical Research Center</a></div><div class="esg-bottom eg-post-427 eg-wpc_projects-element-1-a esg-slideup" data-delay="0.15" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-1 eg-post-427" href="http://demo.wpcharming.com/construction/project/clinical-research-center/" target="_self">Read More</a></div><div></div></div>
				
           </div><!-- END OF THE CONTENT IN THE ENTRY -->
   </div><!-- END OF THE CONTAINER FOR THE MEDIA AND COVER/HOVER EFFECTS -->

</li><!-- END OF PORTFOLIO ITEM -->
<!-- PORTFOLIO ITEM 45 -->
<li class="filterall filter-interior-design filter-office eg-wpc_projects-wrapper eg-post-id-436 tp-esg-item itemonotherpage fitsinfilter esg-hovered" data-date="1423812806" style="opacity: 0; visibility: hidden; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0.01, 0.999991666666667);">
    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
    <div class="esg-media-cover-wrapper">
            <!-- THE MEDIA OF THE ENTRY -->
<div class="esg-entry-media-wrapper" style="width:100%;height:100%; overflow:hidden;position:relative;"><div class="esg-entry-media"><img src="/Templates/construction/files/interior5-600x300.jpg" alt="" style="top: 0px; left: 0px; width: 100%; height: auto; visibility: visible; display: block;"></div></div>

            <!-- THE CONTENT OF THE ENTRY -->
            <div class="esg-entry-cover" style="transform-style: flat;">

                <!-- THE COLORED OVERLAY -->
                <div class="esg-overlay esg-fade eg-wpc_projects-container" data-delay="0" style="visibility: hidden; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, -0.002, 1.000002);"></div>

				<div class="esg-bc eec"><div class="esg-bottom eg-post-436 eg-wpc_projects-element-7-a esg-slideup" data-delay="0" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-7 eg-post-436" href="http://demo.wpcharming.com/construction/project/inside-renovation/" target="_self">Inside Renovation</a></div><div class="esg-bottom eg-post-436 eg-wpc_projects-element-1-a esg-slideup" data-delay="0.15" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-1 eg-post-436" href="http://demo.wpcharming.com/construction/project/inside-renovation/" target="_self">Read More</a></div><div></div></div>
				
           </div><!-- END OF THE CONTENT IN THE ENTRY -->
   </div><!-- END OF THE CONTAINER FOR THE MEDIA AND COVER/HOVER EFFECTS -->

</li><!-- END OF PORTFOLIO ITEM -->
<!-- PORTFOLIO ITEM 45 -->
<li class="filterall filter-green-building eg-wpc_projects-wrapper eg-post-id-441 tp-esg-item itemonotherpage fitsinfilter esg-hovered" data-date="1423813145" style="opacity: 0; visibility: hidden; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0.01, 0.999991666666667);">
    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
    <div class="esg-media-cover-wrapper">
            <!-- THE MEDIA OF THE ENTRY -->
<div class="esg-entry-media-wrapper" style="width:100%;height:100%; overflow:hidden;position:relative;"><div class="esg-entry-media"><img src="/Templates/construction/files/green-building-600x300.jpg" alt="" style="top: 0px; left: 0px; width: 100%; height: auto; visibility: visible; display: block;"></div></div>

            <!-- THE CONTENT OF THE ENTRY -->
            <div class="esg-entry-cover" style="transform-style: flat;">

                <!-- THE COLORED OVERLAY -->
                <div class="esg-overlay esg-fade eg-wpc_projects-container" data-delay="0" style="visibility: hidden; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, -0.002, 1.000002);"></div>

				<div class="esg-bc eec"><div class="esg-bottom eg-post-441 eg-wpc_projects-element-7-a esg-slideup" data-delay="0" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-7 eg-post-441" href="http://demo.wpcharming.com/construction/project/riverview-green-building/" target="_self">Riverview Green Building</a></div><div class="esg-bottom eg-post-441 eg-wpc_projects-element-1-a esg-slideup" data-delay="0.15" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-1 eg-post-441" href="http://demo.wpcharming.com/construction/project/riverview-green-building/" target="_self">Read More</a></div><div></div></div>
				
           </div><!-- END OF THE CONTENT IN THE ENTRY -->
   </div><!-- END OF THE CONTAINER FOR THE MEDIA AND COVER/HOVER EFFECTS -->

</li><!-- END OF PORTFOLIO ITEM -->
<!-- PORTFOLIO ITEM 45 -->
<li class="filterall filter-education filter-green-building filter-healthcare eg-wpc_projects-wrapper eg-post-id-439 tp-esg-item itemonotherpage fitsinfilter esg-hovered" data-date="1423813913" style="opacity: 0; visibility: hidden; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0.01, 0.999991666666667);">
    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
    <div class="esg-media-cover-wrapper">
            <!-- THE MEDIA OF THE ENTRY -->
<div class="esg-entry-media-wrapper" style="width:100%;height:100%; overflow:hidden;position:relative;"><div class="esg-entry-media"><img src="/Templates/construction/files/house4-600x300.jpg" alt="" style="top: 0px; left: 0px; width: 100%; height: auto; visibility: visible; display: block;"></div></div>

            <!-- THE CONTENT OF THE ENTRY -->
            <div class="esg-entry-cover" style="transform-style: flat;">

                <!-- THE COLORED OVERLAY -->
                <div class="esg-overlay esg-fade eg-wpc_projects-container" data-delay="0" style="visibility: hidden; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, -0.002, 1.000002);"></div>

				<div class="esg-bc eec"><div class="esg-bottom eg-post-439 eg-wpc_projects-element-7-a esg-slideup" data-delay="0" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-7 eg-post-439" href="http://demo.wpcharming.com/construction/project/magdalen-catholic-school/" target="_self">Magdalen Catholic School</a></div><div class="esg-bottom eg-post-439 eg-wpc_projects-element-1-a esg-slideup" data-delay="0.15" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-1 eg-post-439" href="http://demo.wpcharming.com/construction/project/magdalen-catholic-school/" target="_self">Read More</a></div><div></div></div>
				
           </div><!-- END OF THE CONTENT IN THE ENTRY -->
   </div><!-- END OF THE CONTAINER FOR THE MEDIA AND COVER/HOVER EFFECTS -->

</li><!-- END OF PORTFOLIO ITEM -->
<!-- PORTFOLIO ITEM 45 -->
<li class="filterall filter-green-building eg-wpc_projects-wrapper eg-post-id-448 tp-esg-item itemonotherpage fitsinfilter esg-hovered" data-date="1423814035" style="opacity: 0; visibility: hidden; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0.01, 0.999991666666667);">
    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
    <div class="esg-media-cover-wrapper">
            <!-- THE MEDIA OF THE ENTRY -->
<div class="esg-entry-media-wrapper" style="width:100%;height:100%; overflow:hidden;position:relative;"><div class="esg-entry-media"><img src="/Templates/construction/files/House1-600x300.jpg" alt="" style="top: 0px; left: 0px; width: 100%; height: auto; visibility: visible; display: block;"></div></div>

            <!-- THE CONTENT OF THE ENTRY -->
            <div class="esg-entry-cover" style="transform-style: flat;">

                <!-- THE COLORED OVERLAY -->
                <div class="esg-overlay esg-fade eg-wpc_projects-container" data-delay="0" style="visibility: hidden; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, -0.002, 1.000002);"></div>

				<div class="esg-bc eec"><div class="esg-bottom eg-post-448 eg-wpc_projects-element-7-a esg-slideup" data-delay="0" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-7 eg-post-448" href="http://demo.wpcharming.com/construction/project/chicago-house-with-pool/" target="_self">Chicago House with Pool</a></div><div class="esg-bottom eg-post-448 eg-wpc_projects-element-1-a esg-slideup" data-delay="0.15" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-1 eg-post-448" href="http://demo.wpcharming.com/construction/project/chicago-house-with-pool/" target="_self">Read More</a></div><div></div></div>
				
           </div><!-- END OF THE CONTENT IN THE ENTRY -->
   </div><!-- END OF THE CONTAINER FOR THE MEDIA AND COVER/HOVER EFFECTS -->

</li><!-- END OF PORTFOLIO ITEM -->
<!-- PORTFOLIO ITEM 45 -->
<li class="filterall filter-education eg-wpc_projects-wrapper eg-post-id-450 tp-esg-item itemonotherpage fitsinfilter esg-hovered" data-date="1423814156" style="opacity: 0; visibility: hidden; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00083, 0, 0, 0.01, 0.999991666666667);">
    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
    <div class="esg-media-cover-wrapper">
            <!-- THE MEDIA OF THE ENTRY -->
<div class="esg-entry-media-wrapper" style="width:100%;height:100%; overflow:hidden;position:relative;"><div class="esg-entry-media"><img src="/Templates/construction/files/House2-600x300.jpg" alt="" style="top: 0px; left: 0px; width: 100%; height: auto; visibility: visible; display: block;"></div></div>

            <!-- THE CONTENT OF THE ENTRY -->
            <div class="esg-entry-cover" style="transform-style: flat;">

                <!-- THE COLORED OVERLAY -->
                <div class="esg-overlay esg-fade eg-wpc_projects-container" data-delay="0" style="visibility: hidden; opacity: 0; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, -0.002, 1.000002);"></div>

				<div class="esg-bc eec"><div class="esg-bottom eg-post-450 eg-wpc_projects-element-7-a esg-slideup" data-delay="0" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-7 eg-post-450" href="http://demo.wpcharming.com/construction/project/green-house-neighbourhood/" target="_self">Green House Neighbourhood</a></div><div class="esg-bottom eg-post-450 eg-wpc_projects-element-1-a esg-slideup" data-delay="0.15" style="visibility: hidden; opacity: 0; transform-origin: 50% 50% 0px; transform: translate(0%, 50%) matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.001, 0, 0, 0, 1);"><a class="eg-wpc_projects-element-1 eg-post-450" href="http://demo.wpcharming.com/construction/project/green-house-neighbourhood/" target="_self">Read More</a></div><div></div></div>
				
           </div><!-- END OF THE CONTENT IN THE ENTRY -->
   </div><!-- END OF THE CONTAINER FOR THE MEDIA AND COVER/HOVER EFFECTS -->

</li><!-- END OF PORTFOLIO ITEM -->
</ul></div>
<!-- ############################ -->
<!--      END OF THE GRID         -->
<!-- ############################ -->
    </div></div><!-- END OF THE GRID -->

<div class="esg-loader spinner2" style="visibility: hidden; opacity: 0; background-color: rgb(255, 255, 255);"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div><div class="esg-relative-placeholder" style="width:100%;height:auto"></div></article>
<!-- END OF THE GRID WRAPPER -->

<div class="clear"></div>
<script type="text/javascript">
function eggbfc(winw,resultoption) {
	var lasttop = winw,
	lastbottom = 0,
	smallest =9999,
	largest = 0,
	samount = 0,
	lamoung = 0,
	lastamount = 0,
	resultid = 0,
	resultidb = 0,
	responsiveEntries = [
						{ width:1400,amount:3},
						{ width:1170,amount:3},
						{ width:1024,amount:3},
						{ width:960,amount:3},
						{ width:778,amount:2},
						{ width:640,amount:2},
						{ width:480,amount:1}
						];
	if (responsiveEntries!=undefined && responsiveEntries.length>0)
		jQuery.each(responsiveEntries, function(index,obj) {
			var curw = obj.width != undefined ? obj.width : 0,
				cura = obj.amount != undefined ? obj.amount : 0;
			if (smallest>curw) {
				smallest = curw;
				samount = cura;
				resultidb = index;
			}
			if (largest<curw) {
				largest = curw;
				lamount = cura;
			}
			if (curw>lastbottom && curw<=lasttop) {
				lastbottom = curw;
				lastamount = cura;
				resultid = index;
			}
		})
		if (smallest>winw) {
			lastamount = samount;
			resultid = resultidb;
		}
		var obj = new Object;
		obj.index = resultid;
		obj.column = lastamount;
		if (resultoption=="id")
			return obj;
		else
			return lastamount;
	}
if ("even"=="even") {
	var coh=0,
		container = jQuery("#esg-grid-2-1");
	var	cwidth = container.width(),
		ar = "4:3",
		gbfc = eggbfc(jQuery(window).width(),"id"),
	row = 1;
ar = ar.split(":");
aratio=parseInt(ar[0],0) / parseInt(ar[1],0);
coh = cwidth / aratio;
coh = coh/gbfc.column*row;
	var ul = container.find("ul").first();
	ul.css({display:"block",height:coh+"px"});
}
var essapi_2;
jQuery(document).ready(function() {
	essapi_2 = jQuery("#esg-grid-2-1").tpessential({
        gridID:2,
        layout:"even",
        forceFullWidth:"off",
        lazyLoad:"off",
        row:1,
        loadMoreAjaxToken:"c3866cbbcc",
        loadMoreAjaxUrl:"http://demo.wpcharming.com/construction/wp-admin/admin-ajax.php",
        loadMoreAjaxAction:"Essential_Grid_Front_request_ajax",
        ajaxContentTarget:"ess-grid-ajax-container-",
        ajaxScrollToOffset:"0",
        ajaxCloseButton:"off",
        ajaxContentSliding:"on",
        ajaxScrollToOnLoad:"on",
        ajaxNavButton:"off",
        ajaxCloseType:"type1",
        ajaxCloseInner:"false",
        ajaxCloseStyle:"light",
        ajaxClosePosition:"tr",
        space:30,
        pageAnimation:"fade",
        paginationScrollToTop:"off",
        spinner:"spinner2",
        spinnerColor:"#FFFFFF",
        evenGridMasonrySkinPusher:"off",
        lightBoxMode:"single",
        animSpeed:300,
        delayBasic:1,
        mainhoverdelay:1,
        filterType:"single",
        showDropFilter:"hover",
        filterGroupClass:"esg-fgc-2",
        googleFonts:['Montserrat:400,700'],
        aspectratio:"4:3",
        responsiveEntries: [
						{ width:1400,amount:3},
						{ width:1170,amount:3},
						{ width:1024,amount:3},
						{ width:960,amount:3},
						{ width:778,amount:2},
						{ width:640,amount:2},
						{ width:480,amount:1}
						]
	});

});
</script>

		</div> 
	</div> 

				</div>
			</div>
		</div>
</div>
<div id="wpc_5506d5357ef02" class="vc_row wpb_row vc_row-fluid inverted-row ">
		<div class="row_inner_wrapper  clearfix" style="background-position: left top;background-repeat: no-repeat;padding-top: 50px;padding-bottom: 30px;">
			<div class="row_inner container clearfix">
				<div class="row_full_center_content clearfix">
	<div class="vc_col-sm-12 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="custom-heading wpb_content_element ">
		<h2 class="heading-title">Recent News</h2>
		<span class="heading-line primary"></span>
	</div>
	<div class="child-page-wrapper recent-news-wrapper">
					<script type="text/javascript">
						jQuery(document).ready(function(){
							"use strict";
							jQuery(".recent-news-carousel").slick({
								slidesToShow: 3,
								slidesToScroll: 1,
								draggable: false,
								prevArrow: "<span class='carousel-prev'><i class='fa fa-angle-left'></i></span>",
		        				nextArrow: "<span class='carousel-next'><i class='fa fa-angle-right'></i></span>",
		        				responsive: [{
								    breakpoint: 1024,
								    settings: {
								    slidesToShow: 3
								    }
								},
								{
								    breakpoint: 600,
								    settings: {
								    slidesToShow: 2
								    }
								},
								{
								    breakpoint: 480,
								    settings: {
								    slidesToShow: 1
								    }
								}]
							});
						});
					</script>
				<div class="grid-wrapper grid-3-columns grid-row recent-news-carousel slick-initialized slick-slider">
					<div class="slick-list" tabindex="0"><div class="slick-track" style="opacity: 1; width: 4290px; transform: translate3d(-1170px, 0px, 0px);"><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="-3" style="width: 390px;">
						<div class="grid-thumbnail">
							<a href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/" title="Construction Honored with AGC Builders"><img width="600" height="300" src="/Templates/construction/files/Awards2_tinyed-600x300.png" class="attachment-medium-thumb wp-post-image" alt="Awards2_tinyed"></a>
						</div>
						<h4 class="grid-title"><a href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/" rel="bookmark">Construction Honored with AGC Builders</a></h4>
						
						<div class="recent-news-meta">
							<span class="post-date"><i class="fa fa-file-text-o"></i> <time class="entry-date published updated" datetime="2015-02-12T09:47:09+00:00">February 12, 2015</time></span>
							<span class="comments-link"><i class="fa fa-comments-o"></i> <a href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/#comments">No Comments</a></span>
						</div>

						<p>Last night, Construction was honored to accept a Best Builders Award from the Associated General Contractors of Vermont for the construction ...</p>

						<a class="btn btn-light btn-small" href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/" title="Construction Honored with AGC Builders">Read More</a>

					</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="-2" style="width: 390px;">
						<div class="grid-thumbnail">
							<a href="http://demo.wpcharming.com/construction/construction-forklift-buyers-guide/" title="Construction Forklift Buyers Guide"><img width="600" height="300" src="/Templates/construction/files/slider04-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="slider04"></a>
						</div>
						<h4 class="grid-title"><a href="http://demo.wpcharming.com/construction/construction-forklift-buyers-guide/" rel="bookmark">Construction Forklift Buyers Guide</a></h4>
						
						<div class="recent-news-meta">
							<span class="post-date"><i class="fa fa-file-text-o"></i> <time class="entry-date published updated" datetime="2015-02-12T08:54:09+00:00">February 12, 2015</time></span>
							<span class="comments-link"><i class="fa fa-comments-o"></i> <a href="http://demo.wpcharming.com/construction/construction-forklift-buyers-guide/#comments">No Comments</a></span>
						</div>

						<p>A forklift used at one construction site might not be the best for&nbsp;another. Specific construction tasks vary, as do weight and ...</p>

						<a class="btn btn-light btn-small" href="http://demo.wpcharming.com/construction/construction-forklift-buyers-guide/" title="Construction Forklift Buyers Guide">Read More</a>

					</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="-1" style="width: 390px;">
						<div class="grid-thumbnail">
							<a href="http://demo.wpcharming.com/construction/build-a-wood-fired-clay-oven/" title="Build a Wood Fired Clay Oven"><img width="600" height="300" src="/Templates/construction/files/Wicker-Chair-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="Wicker-Chair"></a>
						</div>
						<h4 class="grid-title"><a href="http://demo.wpcharming.com/construction/build-a-wood-fired-clay-oven/" rel="bookmark">Build a Wood Fired Clay Oven</a></h4>
						
						<div class="recent-news-meta">
							<span class="post-date"><i class="fa fa-file-text-o"></i> <time class="entry-date published updated" datetime="2015-02-05T06:33:09+00:00">February 5, 2015</time></span>
							<span class="comments-link"><i class="fa fa-comments-o"></i> <a href="http://demo.wpcharming.com/construction/build-a-wood-fired-clay-oven/#comments">1 Comment</a></span>
						</div>

						<p>Building a wood fired clay oven is a fun project and when you get done you�ll have fun using it too. ...</p>

						<a class="btn btn-light btn-small" href="http://demo.wpcharming.com/construction/build-a-wood-fired-clay-oven/" title="Build a Wood Fired Clay Oven">Read More</a>

					</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-active" data-slick-index="0" style="width: 390px;">
						<div class="grid-thumbnail">
							<a href="http://demo.wpcharming.com/construction/finding-new-buildings-in-the-dust-of-the-old/" title="Finding New Buildings in the Dust of the Old"><img width="600" height="300" src="/Templates/construction/files/green-building-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="green-building"></a>
						</div>
						<h4 class="grid-title"><a href="http://demo.wpcharming.com/construction/finding-new-buildings-in-the-dust-of-the-old/" rel="bookmark">Finding New Buildings in the Dust of the Old</a></h4>
						
						<div class="recent-news-meta">
							<span class="post-date"><i class="fa fa-file-text-o"></i> <time class="entry-date published updated" datetime="2015-02-12T09:53:58+00:00">February 12, 2015</time></span>
							<span class="comments-link"><i class="fa fa-comments-o"></i> <a href="http://demo.wpcharming.com/construction/finding-new-buildings-in-the-dust-of-the-old/#comments">No Comments</a></span>
						</div>

						<p>With the continued and growing emphasis on sustainability in construction&nbsp;we could be on the verge of a radical shift in how ...</p>

						<a class="btn btn-light btn-small" href="http://demo.wpcharming.com/construction/finding-new-buildings-in-the-dust-of-the-old/" title="Finding New Buildings in the Dust of the Old">Read More</a>

					</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-active" data-slick-index="1" style="width: 390px;">
						<div class="grid-thumbnail">
							<a href="http://demo.wpcharming.com/construction/how-to-build-a-construction-marketing-plan/" title="How To Build A Construction Plan"><img width="600" height="300" src="/Templates/construction/files/teamwork-606818_1280-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="teamwork-606818_1280"></a>
						</div>
						<h4 class="grid-title"><a href="http://demo.wpcharming.com/construction/how-to-build-a-construction-marketing-plan/" rel="bookmark">How To Build A Construction Plan</a></h4>
						
						<div class="recent-news-meta">
							<span class="post-date"><i class="fa fa-file-text-o"></i> <time class="entry-date published updated" datetime="2015-02-12T09:50:24+00:00">February 12, 2015</time></span>
							<span class="comments-link"><i class="fa fa-comments-o"></i> <a href="http://demo.wpcharming.com/construction/how-to-build-a-construction-marketing-plan/#comments">No Comments</a></span>
						</div>

						<p>Learn how to market your contractor business professionally. In depth knowledge of attracting clients with online marketing strategies and deep thinking ...</p>

						<a class="btn btn-light btn-small" href="http://demo.wpcharming.com/construction/how-to-build-a-construction-marketing-plan/" title="How To Build A Construction Plan">Read More</a>

					</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-active" data-slick-index="2" style="width: 390px;">
						<div class="grid-thumbnail">
							<a href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/" title="Construction Honored with AGC Builders"><img width="600" height="300" src="/Templates/construction/files/Awards2_tinyed-600x300.png" class="attachment-medium-thumb wp-post-image" alt="Awards2_tinyed"></a>
						</div>
						<h4 class="grid-title"><a href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/" rel="bookmark">Construction Honored with AGC Builders</a></h4>
						
						<div class="recent-news-meta">
							<span class="post-date"><i class="fa fa-file-text-o"></i> <time class="entry-date published updated" datetime="2015-02-12T09:47:09+00:00">February 12, 2015</time></span>
							<span class="comments-link"><i class="fa fa-comments-o"></i> <a href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/#comments">No Comments</a></span>
						</div>

						<p>Last night, Construction was honored to accept a Best Builders Award from the Associated General Contractors of Vermont for the construction ...</p>

						<a class="btn btn-light btn-small" href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/" title="Construction Honored with AGC Builders">Read More</a>

					</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide" data-slick-index="3" style="width: 390px;">
						<div class="grid-thumbnail">
							<a href="http://demo.wpcharming.com/construction/construction-forklift-buyers-guide/" title="Construction Forklift Buyers Guide"><img width="600" height="300" src="/Templates/construction/files/slider04-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="slider04"></a>
						</div>
						<h4 class="grid-title"><a href="http://demo.wpcharming.com/construction/construction-forklift-buyers-guide/" rel="bookmark">Construction Forklift Buyers Guide</a></h4>
						
						<div class="recent-news-meta">
							<span class="post-date"><i class="fa fa-file-text-o"></i> <time class="entry-date published updated" datetime="2015-02-12T08:54:09+00:00">February 12, 2015</time></span>
							<span class="comments-link"><i class="fa fa-comments-o"></i> <a href="http://demo.wpcharming.com/construction/construction-forklift-buyers-guide/#comments">No Comments</a></span>
						</div>

						<p>A forklift used at one construction site might not be the best for&nbsp;another. Specific construction tasks vary, as do weight and ...</p>

						<a class="btn btn-light btn-small" href="http://demo.wpcharming.com/construction/construction-forklift-buyers-guide/" title="Construction Forklift Buyers Guide">Read More</a>

					</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide" data-slick-index="4" style="width: 390px;">
						<div class="grid-thumbnail">
							<a href="http://demo.wpcharming.com/construction/build-a-wood-fired-clay-oven/" title="Build a Wood Fired Clay Oven"><img width="600" height="300" src="/Templates/construction/files/Wicker-Chair-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="Wicker-Chair"></a>
						</div>
						<h4 class="grid-title"><a href="http://demo.wpcharming.com/construction/build-a-wood-fired-clay-oven/" rel="bookmark">Build a Wood Fired Clay Oven</a></h4>
						
						<div class="recent-news-meta">
							<span class="post-date"><i class="fa fa-file-text-o"></i> <time class="entry-date published updated" datetime="2015-02-05T06:33:09+00:00">February 5, 2015</time></span>
							<span class="comments-link"><i class="fa fa-comments-o"></i> <a href="http://demo.wpcharming.com/construction/build-a-wood-fired-clay-oven/#comments">1 Comment</a></span>
						</div>

						<p>Building a wood fired clay oven is a fun project and when you get done you�ll have fun using it too. ...</p>

						<a class="btn btn-light btn-small" href="http://demo.wpcharming.com/construction/build-a-wood-fired-clay-oven/" title="Build a Wood Fired Clay Oven">Read More</a>

					</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="5" style="width: 390px;">
						<div class="grid-thumbnail">
							<a href="http://demo.wpcharming.com/construction/finding-new-buildings-in-the-dust-of-the-old/" title="Finding New Buildings in the Dust of the Old"><img width="600" height="300" src="/Templates/construction/files/green-building-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="green-building"></a>
						</div>
						<h4 class="grid-title"><a href="http://demo.wpcharming.com/construction/finding-new-buildings-in-the-dust-of-the-old/" rel="bookmark">Finding New Buildings in the Dust of the Old</a></h4>
						
						<div class="recent-news-meta">
							<span class="post-date"><i class="fa fa-file-text-o"></i> <time class="entry-date published updated" datetime="2015-02-12T09:53:58+00:00">February 12, 2015</time></span>
							<span class="comments-link"><i class="fa fa-comments-o"></i> <a href="http://demo.wpcharming.com/construction/finding-new-buildings-in-the-dust-of-the-old/#comments">No Comments</a></span>
						</div>

						<p>With the continued and growing emphasis on sustainability in construction&nbsp;we could be on the verge of a radical shift in how ...</p>

						<a class="btn btn-light btn-small" href="http://demo.wpcharming.com/construction/finding-new-buildings-in-the-dust-of-the-old/" title="Finding New Buildings in the Dust of the Old">Read More</a>

					</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="6" style="width: 390px;">
						<div class="grid-thumbnail">
							<a href="http://demo.wpcharming.com/construction/how-to-build-a-construction-marketing-plan/" title="How To Build A Construction Plan"><img width="600" height="300" src="/Templates/construction/files/teamwork-606818_1280-600x300.jpg" class="attachment-medium-thumb wp-post-image" alt="teamwork-606818_1280"></a>
						</div>
						<h4 class="grid-title"><a href="http://demo.wpcharming.com/construction/how-to-build-a-construction-marketing-plan/" rel="bookmark">How To Build A Construction Plan</a></h4>
						
						<div class="recent-news-meta">
							<span class="post-date"><i class="fa fa-file-text-o"></i> <time class="entry-date published updated" datetime="2015-02-12T09:50:24+00:00">February 12, 2015</time></span>
							<span class="comments-link"><i class="fa fa-comments-o"></i> <a href="http://demo.wpcharming.com/construction/how-to-build-a-construction-marketing-plan/#comments">No Comments</a></span>
						</div>

						<p>Learn how to market your contractor business professionally. In depth knowledge of attracting clients with online marketing strategies and deep thinking ...</p>

						<a class="btn btn-light btn-small" href="http://demo.wpcharming.com/construction/how-to-build-a-construction-marketing-plan/" title="How To Build A Construction Plan">Read More</a>

					</div><div class="grid-item grid-sm-6 grid-md-4 slick-slide slick-cloned" data-slick-index="7" style="width: 390px;">
						<div class="grid-thumbnail">
							<a href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/" title="Construction Honored with AGC Builders"><img width="600" height="300" src="/Templates/construction/files/Awards2_tinyed-600x300.png" class="attachment-medium-thumb wp-post-image" alt="Awards2_tinyed"></a>
						</div>
						<h4 class="grid-title"><a href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/" rel="bookmark">Construction Honored with AGC Builders</a></h4>
						
						<div class="recent-news-meta">
							<span class="post-date"><i class="fa fa-file-text-o"></i> <time class="entry-date published updated" datetime="2015-02-12T09:47:09+00:00">February 12, 2015</time></span>
							<span class="comments-link"><i class="fa fa-comments-o"></i> <a href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/#comments">No Comments</a></span>
						</div>

						<p>Last night, Construction was honored to accept a Best Builders Award from the Associated General Contractors of Vermont for the construction ...</p>

						<a class="btn btn-light btn-small" href="http://demo.wpcharming.com/construction/construction-honored-with-agc-best-builders/" title="Construction Honored with AGC Builders">Read More</a>

					</div></div></div>
					
					
					
					
					
					
					
					
					
				<span class="carousel-prev" style="display: block;"><i class="fa fa-angle-left"></i></span><span class="carousel-next" style="display: block;"><i class="fa fa-angle-right"></i></span></div>
	</div>
		</div> 
	</div> 

				</div>
			</div>
		</div>
</div>
<div id="wpc_5506d53584c71" class="vc_row wpb_row vc_row-fluid  ">
		<div class="row_inner_wrapper  clearfix" style="background-position: left top;background-repeat: no-repeat;padding-top: 60px;padding-bottom: 50px;">
			<div class="row_inner container clearfix">
				<div class="row_full_center_content clearfix">
	<div class="vc_col-sm-6 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="custom-heading wpb_content_element ">
		<h2 class="heading-title">Our Clients</h2>
		<span class="heading-line primary"></span>
	</div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<table class="client-table">
<tbody>
<tr>
<td><a href="http://demo.wpcharming.com/construction/#"><img src="/Templates/construction/files/logo1.png" alt=""></a></td>
<td><a href="http://demo.wpcharming.com/construction/#"><img src="/Templates/construction/files/logo2.png" alt=""></a></td>
<td><a href="http://demo.wpcharming.com/construction/#"><img src="/Templates/construction/files/logo3.png" alt=""></a></td>
</tr>
<tr>
<td><a href="http://demo.wpcharming.com/construction/#"><img src="/Templates/construction/files/logo4.png" alt=""></a></td>
<td><a href="http://demo.wpcharming.com/construction/#"><img src="/Templates/construction/files/logo5.png" alt=""></a></td>
<td><a href="http://demo.wpcharming.com/construction/#"><img src="/Templates/construction/files/logo6.png" alt=""></a></td>
</tr>
</tbody>
</table>

		</div> 
	</div> 
		</div> 
	</div> 

	<div class="vc_col-sm-6 wpb_column vc_column_container ">
		<div class="wpb_wrapper">
			
	<div class="custom-heading wpb_content_element ">
		<h2 class="heading-title">Testimonials</h2>
		<span class="heading-line primary"></span>
	</div>
	<div class="testimonial">
		<div class="testimonial-content">
			<p>Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with �real� content. This is required when, for example, the final text is not yet available.</p>
		</div>
		<div class="testimonial-header clearfix">
			<div class="testimonial-avatar"><img src="/Templates/construction/files/3.jpg" alt="Howard K. Stern"></div>
			<div class="testimonial-name font-heading">Howard K. Stern</div>
		</div>
	</div>
		</div> 
	</div> 

				</div>
			</div>
		</div>
</div>
								
							</div><!-- .entry-content -->

						</article><!-- #post-## -->

					
				</main><!-- #main -->
			</div><!-- #primary -->
			
							
		</div> <!-- /#content-wrap -->


	</div><!-- #content -->
	
	<div class="clear"></div>

	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<div class="footer-connect">
			<div class="container">

								<div class="footer-social">
					 <label class="font-heading" for="">Follow us</label> 					<a href="http://demo.wpcharming.com/construction/#" title="Twitter"><i class="fa fa-twitter"></i></a> 					<a href="http://demo.wpcharming.com/construction/#" title="Facebook"><i class="fa fa-facebook"></i></a> 					<a href="http://demo.wpcharming.com/construction/#" title="Linkedin"><i class="fa fa-linkedin-square"></i></a> 															<a href="http://demo.wpcharming.com/construction/#" title="Google Plus"><i class="fa fa-google-plus"></i></a> 					<a href="http://demo.wpcharming.com/construction/#" title="Instagram"><i class="fa fa-instagram"></i></a> 																			</div>
							</div>
		</div>

		<div class="container">
			
						<div class="footer-widgets-area">
													<div class="sidebar-footer footer-columns footer-4-columns clearfix">
													<div id="footer-1" class="footer-1 footer-column widget-area" role="complementary">
								<aside id="text-4" class="widget widget_text"><h3 class="widget-title">�� �������</h3>			<div class="textwidget"><p><?=$o_site->_site['Description']?>
</p>
</div>
		</aside>							</div>
														<div id="footer-2" class="footer-2 footer-column widget-area" role="complementary">
								<aside id="nav_menu-3" class="widget widget_nav_menu"><h3 class="widget-title">Company</h3><div class="menu-footer-widget-menu-container"><ul id="menu-footer-widget-menu" class="menu"><li id="menu-item-492" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-13 current_page_item menu-item-492"><a href="/Templates/construction/files/Construction Theme   Just another WordPress site.html">Home</a></li>
<li id="menu-item-490" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-490"><a href="http://demo.wpcharming.com/construction/who-we-are/">Who We Are</a></li>
<li id="menu-item-486" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-486"><a href="http://demo.wpcharming.com/construction/news/">Latest News</a></li>
<li id="menu-item-487" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-487"><a href="http://demo.wpcharming.com/construction/our-services/">Our Services</a></li>
<li id="menu-item-489" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-489"><a href="http://demo.wpcharming.com/construction/projects/">Projects</a></li>
<li id="menu-item-491" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-491"><a href="http://demo.wpcharming.com/construction/shop/">Shop</a></li>
<li id="menu-item-494" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-494"><a href="http://demo.wpcharming.com/construction/careers/">Careers</a></li>
<li id="menu-item-485" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-485"><a href="http://demo.wpcharming.com/construction/contact-us/">Contact Us</a></li>
</ul></div></aside>							</div>
														<div id="footer-3" class="footer-3 footer-column widget-area" role="complementary">
								<aside id="text-5" class="widget widget_text"><h3 class="widget-title">��������</h3>			<div class="textwidget"><div class="contact-info-box">
	<div class="contact-info-item">
		<div class="contact-text">
		?>
	</div>
	</div>
	<div class="contact-info-item">
		<div class="contact-text"><i class="fa fa-phone"></i></div>
		<div class="contact-value">1.800.451.128</div>
	</div>
	<div class="contact-info-item">
		<div class="contact-text"><i class="fa fa-envelope"></i></div>
		<div class="contact-value"><a href=""><?=$�_page->_site['EMail']?></a></div>
	</div>
	<div class="contact-info-item">
		<div class="contact-text"><i class="fa fa-fax"></i></div>
		<div class="contact-value">FAX: </div>
	</div>
</div></div>
		</aside>							</div>
														<div id="footer-4" class="footer-4 footer-column widget-area" role="complementary">
								<aside id="text-7" class="widget widget_text"><h3 class="widget-title">Business Hours</h3>			<div class="textwidget"><div class="contact-info-box">
<p>Our support available to help you 24 hours a day, seven days a week.</p>
<ul class="hours">
<li>Monday-Friday: <span>9am to 5pm</span></li>
<li>Saturday: <span>10am to 2pm</span></li>
<li>Sunday: <span>Closed</span></li>
</ul>
</div></div>
		</aside>							</div>
												</div>
							</div>
					</div>
		<div class="site-info-wrapper">
			<div class="container">
				<div class="site-info clearfix">
					<div class="copy_text">
						<?=$o_page->_site['copyright']?><a href="<?=$o_page->_site['Name']?>" rel="designer"><?=$o_page->_site['Name']?></a>					</div>
					<div class="footer-menu">
						<div class="menu-footer-container"><ul id="menu-footer" class="menu"><li id="menu-item-501" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-501"><a href="http://demo.wpcharming.com/construction/our-services/">Our Services</a></li>
<li id="menu-item-502" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-502"><a href="http://demo.wpcharming.com/construction/projects/">Projects</a></li>
<li id="menu-item-500" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-500"><a href="http://demo.wpcharming.com/construction/contact-us/">Contact Us</a></li>
<li id="menu-item-498" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-498"><a href="http://demo.wpcharming.com/construction/#">Disclaimer</a></li>
<li id="menu-item-499" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-499"><a href="http://demo.wpcharming.com/construction/#">Privacy Policy</a></li>
</ul></div>					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->

</div><!-- #page -->

<div id="btt" style="display: none;"><i class="fa fa-angle-double-up"></i></div>

<script type="text/javascript" src="/Templates/construction/files/jquery.form.min.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/demo.wpcharming.com\/construction\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ...","cached":"1"};
/* ]]> */
</script>
<script type="text/javascript" src="/Templates/construction/files/scripts.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/construction\/wp-admin\/admin-ajax.php","i18n_view_cart":"View Cart","cart_url":"http:\/\/demo.wpcharming.com\/construction\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type="text/javascript" src="/Templates/construction/files/add-to-cart.min.js"></script>
<script type="text/javascript" src="/Templates/construction/files/jquery.blockUI.min.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/construction\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript" src="/Templates/construction/files/woocommerce.min.js"></script>
<script type="text/javascript" src="/Templates/construction/files/jquery.cookie.min.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/construction\/wp-admin\/admin-ajax.php","fragment_name":"wc_fragments"};
/* ]]> */
</script>
<script type="text/javascript" src="/Templates/construction/files/cart-fragments.min.js"></script>
<script type="text/javascript" src="/Templates/construction/files/libs.js"></script>
<script type="text/javascript" src="/Templates/construction/files/theme.js"></script>
<script type="text/javascript" src="/Templates/construction/files/comment-reply.min.js"></script>
<script type="text/javascript" src="/Templates/construction/files/js_composer_front.js"></script>


</body></html>