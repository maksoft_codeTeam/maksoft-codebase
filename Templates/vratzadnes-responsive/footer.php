<div id="footer_mailchimp_subscription" class="one withsmallpadding ppb_mailchimp_subscription withbg">
    <div class="standard_wrapper">
        <h2 class="ppb_title">Stay Updated</h2>
        <script type="text/javascript">(function() {
    if (!window.mc4wp) {
        window.mc4wp = {
            listeners: [],
            forms    : {
                on: function (event, callback) {
                    window.mc4wp.listeners.push({
                        event   : event,
                        callback: callback
                    });
                }
            }
        }
    }
})();
</script><!-- MailChimp for WordPress v3.1.10 - https://wordpress.org/plugins/mailchimp-for-wp/ --><form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-618" method="post" data-id="618" data-name="Subscribe"><div class="mc4wp-form-fields"><div class="subscribe_tagline">
Get the recent popular stories straight into your inbox
</div>

<div class="subscribe_form">
<p>
    <input type="email" name="EMAIL" placeholder="Email" required="">
</p>

<p>
    <input type="submit" value="Sign up">
</p>
</div><div style="display: none;"><input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off"></div><input type="hidden" name="_mc4wp_timestamp" value="1491811446"><input type="hidden" name="_mc4wp_form_id" value="618"><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1"></div><div class="mc4wp-response"></div></form><!-- / MailChimp for WordPress Plugin --> </div>
</div>
</div>
<br class="clear"/>
</div>
<div class="footer_bar">
    <div id="footer" class="">
        <ul class="sidebar_widget three">
            <li id="text-2" class="widget widget_text">
                <div class="textwidget">
                    <p><img src="/Templates/vratzadnes/images/logo.png" alt="" style="max-width: 230px;height: auto;"></p>
                    <p><?=$o_page->_site['Title'];?>
                    <div style="margin-top:15px;">
                        <div class="social_wrapper shortcode light large">
                            <ul>
                    <?php
                            $s_configurations = $o_page->get_sConfigurations();
                            foreach ($s_configurations as $conf) {
                                switch($conf['conf_key']){
                                case "CMS_FACEBOOK_PAGE_LINK":
                                    ?> 
                                        <li class="facebook"><a target="_blank" title="Facebook" href="<?=$conf['conf_value']?>"><i class="fa fa-facebook"></i></a></li>
                                    <?php break;
                                case "CMS_TWITTER_PAGE_LINK":
                                    ?> 
                                        <li class="twitter"><a target="_blank" title="Twitter" href="<?=$conf['conf_value']?>"><i class="fa fa-twitter"></i></a></li>
                                    <?php break;
                                case "CMS_LINKEDIN_PAGE_LINK":
                                    ?> 
                                        <li class="linkedin"><a target="_blank" title="LinkedIN" href="<?=$conf['conf_value']?>"><i class="fa fa-linkedin"></i></a></li>
                                    <?php break;
                                }
                            }
                    ?>
                                <li class="rss"><a target="_blank" title="RSS" href="/rss.php"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li id="grand_news_recent_posts-2" class="widget Grand_News_Recent_Posts">
                <h2 class="widgettitle"><span>Recent Posts</span></h2>
                <ul class="posts blog withthumb ">
                    <li>
                        <div class="post_circle_thumb">
                            <a href="3-delicious-post-holiday-detox-recipes-courtesy-of-marc-jacobs-personal-chef/index.html"><img src="Templates/vratzadnes-responsive/uploads/2016/03/photo-1448518184296-a22facb4446f-300x200.jpg" class="tg-lazy" alt=""></a>
                        </div><a href="3-delicious-post-holiday-detox-recipes-courtesy-of-marc-jacobs-personal-chef/index.html">3 Delicious Post-Holiday Detox Recipes, Courtesy of Personal Chef</a></li>
                    <li>
                        <div class="post_circle_thumb">
                            <a href="21-questions-with-fashion-photographer-emma-summerton/index.html"><img src="Templates/vratzadnes-responsive/uploads/2016/03/shutterstock_145254253-300x200.jpg" class="tg-lazy" alt=""></a>
                        </div><a href="21-questions-with-fashion-photographer-emma-summerton/index.html">21 Questions with� Fashion Photographer Emma Summerton</a></li>
                    <li>
                        <div class="post_circle_thumb">
                            <a href="runway-scorecard-the-10-most-in-demand-models-of-fashion-month-fall-2016/index.html"><img src="Templates/vratzadnes-responsive/uploads/2016/03/24-300x200.jpg" class="tg-lazy" alt=""></a>
                        </div><a href="runway-scorecard-the-10-most-in-demand-models-of-fashion-month-fall-2016/index.html">Runway Scorecard: The 10 Most In-Demand Models of Fashion Month Fall 2016</a></li>
                </ul>
            </li>
            <li id="grand_news_popular_posts-4" class="widget Grand_News_Popular_Posts">
                <h2 class="widgettitle"><span>Popular Posts</span></h2>
                <ul class="posts blog withthumb ">
                    <li>
                        <div class="post_circle_thumb">
                            <a href="magnificent-image-of-the-new-hoover-dam-bridge-taking-shape/index.html">
                                <div class="post_number">1</div><img src="Templates/vratzadnes-responsive/uploads/2015/07/4-300x200.jpg" class="tg-lazy" alt=""></a>
                        </div><a href="magnificent-image-of-the-new-hoover-dam-bridge-taking-shape/index.html">Magnificent Image Of The New Hoover Dam Bridge Taking Shape</a></li>
                    <li>
                        <div class="post_circle_thumb">
                            <a href="the-23-best-beauty-moments-from-the-fall-2016-runways/index.html">
                                <div class="post_number">2</div><img src="Templates/vratzadnes-responsive/uploads/2015/07/city-road-street-buildings-300x197.jpg" class="tg-lazy" alt=""></a>
                        </div><a href="the-23-best-beauty-moments-from-the-fall-2016-runways/index.html">The 23 Best Beauty Moments From the Fall 2016 Runways</a></li>
                    <li>
                        <div class="post_circle_thumb">
                            <a href="3-delicious-post-holiday-detox-recipes-courtesy-of-marc-jacobs-personal-chef/index.html">
                                <div class="post_number">3</div><img src="Templates/vratzadnes-responsive/uploads/2016/03/photo-1448518184296-a22facb4446f-300x200.jpg" class="tg-lazy" alt=""></a>
                        </div><a href="3-delicious-post-holiday-detox-recipes-courtesy-of-marc-jacobs-personal-chef/index.html">3 Delicious Post-Holiday Detox Recipes, Courtesy of Personal Chef</a></li>
                </ul>
            </li>
        </ul>
    <br class="clear">

    <div class="footer_bar_wrapper ">
        <div class="menu-top-menu-container">
            <ul id="footer_menu" class="footer_nav">
                <li id="menu-item-7" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7"><a href="#">Maksoft</a></li>

            </ul>
        </div>
        <div id="copyright">� ����� ����</div>
        <a id="toTop" style="opacity: 0.5; visibility: visible;"><i class="fa fa-angle-up"></i></a>

    </div>
</div>

<div id="overlay_background">
	<div id="fullscreen_share_wrapper">
		<div class="fullscreen_share_content">
		<div id="social_share_wrapper">
	<ul>
		<li><a class="tooltip" title="Share On Facebook" target="_blank" href="#"><i class="fa fa-facebook marginright"></i></a></li>
		<li><a class="tooltip" title="Share On Twitter" target="_blank" href="#"><i class="fa fa-twitter marginright"></i></a></li>
		<li><a class="tooltip" title="Share On Pinterest" target="_blank" href="#"><i class="fa fa-pinterest marginright"></i></a></li>
		<li><a class="tooltip" title="Share On Google+" target="_blank" href="#"><i class="fa fa-google-plus marginright"></i></a></li>
		<li><a class="tooltip" title="Share by Email" href="#"><i class="fa fa-envelope marginright"></i></a></li>
	</ul>
</div>		</div>
	</div>
</div>

<div id="overlay_background_search">
	<a id="search_close_button" class="tooltip" title="Close" href="javascript:;"><i class="fa fa-close"></i></a>
	<div class="search_wrapper">
		<div class="search_content">
		    <form role="search" method="get" name="searchform" id="searchform" action="#">
		        <div>
		        	<input type="text" value="" name="s" id="s" autocomplete="off" placeholder="Enter Keyword"/>
		        	<div class="search_tagline">Press enter/return to begin your search</div>
		        	<button>
		            	<i class="fa fa-search"></i>
		            </button>
		        </div>
		        <div id="autocomplete"></div>
		    </form>
		</div>
	</div>
</div>
<script id="article-template" type="text/x-handlebars-template">
<div class="post type-post">
    <div class="post_wrapper">
        <div class="post_content_wrapper">
            <div class="post_header search">
                <div class="post_img static one_third">
                    <a href="{{article_link}}" title="{{article_title}}">
                        <img src="{{article_image_src}}" alt="{{article_image_alt}}">
                    </a>
                </div>
                <div class="post_header_title two_third last">
                    <h5>
                        <a href="{{article_link}}" title="{{article_title}}">{{article_title}}</a>
                    </h5>
                    <p></p>
                    <p>
                    {{article_content}}
                    </p>
                    <p></p>
                    <div class="post_detail post_date">
                        <span class="post_info_author">
                            <a href="{{article_link}}"><i class="fa fa-eye" aria-hidden="true"></i> {{article_views}}</a>
                        </span>
                        <span class="post_info_date">
                            <a href="{{article_link}}"><i class="fa fa-calendar" aria-hidden="true"></i> {{article_published_date}}</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</script>



<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/woocommerce/assets/js/frontend/woocommerce.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/wp-review/assets/js/main.js'></script>
<!--<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/combined.js'></script>-->

<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.easing.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.gridrotator.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.isotope.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.lazy.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.mousewheel.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.sticky-kit.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.lazy.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/modernizr.custom.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/modernizr.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/waypoints.min.js'></script>

<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/ilightbox.packed.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.tooltipster.min.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.vide.js'></script>


<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/custom.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/custom_plugins.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/browser.js'></script>
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/hint.js'></script>

<!-- Newsticker -->
<script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/js/jquery.newsticker.min.js'></script>
<script type='text/javascript' src='http://themes.themegoods2.com/grandnews/demo2/wp-content/themes/grandnews/js/flexslider/jquery.flexslider-min.js'>

<script type="text/javascript">(function() {function addEventListener(element,event,handler) {
    if(element.addEventListener) {
        element.addEventListener(event,handler, false);
    } else if(element.attachEvent){
        element.attachEvent('on'+event,handler);
    }
    }})();
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.min.js" integrity="sha256-1O3BtOwnPyyRzOszK6P+gqaRoXHV6JXj8HkjZmPYhCI=" crossorigin="anonymous"></script>
<!--
<script src="https://rawgit.com/kimmobrunfeldt/progressbar.js/1.0.0/dist/progressbar.js"></script>
-->
<script type="text/javascript" src="/Templates/vratzadnes/autobahn-js/lib/autobahn.js"></script>

<!-- CUSTOM JS -->

<script src="<?=TEMPLATE_DIR?>assets/main.js"></script>
<script type="text/javascript" src="/modules/web/components/push.js/push.min.js"></script>
<script id="loader-cube" type="text/x-handlebars-template">
    <div class="loading_wrapper">
        <div class="sk-cube-grid">
            <div class="sk-cube sk-cube1"></div>
            <div class="sk-cube sk-cube2"></div>
            <div class="sk-cube sk-cube3"></div>
            <div class="sk-cube sk-cube4"></div>
            <div class="sk-cube sk-cube5"></div>
            <div class="sk-cube sk-cube6"></div>
            <div class="sk-cube sk-cube7"></div>
            <div class="sk-cube sk-cube8"></div>
            <div class="sk-cube sk-cube9"></div>
        </div>
    </div>
</script>
        <script>
            //post_filter_wrapper
            jQuery("#1487688597877917989_wrapper").hide();
            jQuery(window).load(function(){
                jQuery("#1487688597877917989_filter li a").click(function(){
                      jQuery("#1487688597877917989_filter li a").removeClass("selected");
                      jQuery(this).addClass("selected");
                      jQuery("#1487688597877917989_loading").addClass("active");

                      var url = "http://www.vratzadnes.com/api/?command="+jQuery(this).data("filter")+"&n=<?=$o_page->_page['n']?>&SiteID=<?=$o_page->_page['SiteID'];?>&limit=7";
                      get(url)
                       .then(JSON.parse)
                       .then(function(articles){
                           print_articles(articles);
                       })
                       .then(function(){
                           jQuery("#1487688597877917989_loading").removeClass("active");
                           jQuery("#1487688597877917989_wrapper").show();
                       });
                });

               live_news_container = [];
               var $ = jQuery;
               var conn = new ab.Session('ws://79.124.31.189:8802',
                    function() {
                           conn.subscribe('<?=$o_page->_site['primary_url'];?>-live', function(topic, article) {
                            //// This is where you would add the new article to the DOM (beyond the scope of this tutorial)
                            html = '<li><a title="'+article.Name+'" href="'+article.link+'">'+article.name+'</a></li>';
                            jQuery("#1487688597266321564").append(html);
                        });
                    },
                    function() {
                        console.warn('WebSocket connection closed');
                    },
                    {'skipSubprotocolCheck': true}
                );

               jQuery("#1487688597877917989_loading").addClass("active");
               get("<?=Settings::NEWEST_PAGES?>")
                .then(JSON.parse)
                .then(function(articles){
                    print_articles(articles);
                })
                .then(function(){
                    jQuery("#1487688597877917989_loading").hide();
                    jQuery("#1487688597877917989_wrapper").show();
                });
            });

            jQuery(document).ready(function(){

              jQuery("#1487688597266321564").newsTicker({
                  row_height: 36,
                  max_rows: 1,
                  duration: 6000,
                  autostart: 1,
                  prevButton: jQuery("#newsticker_prev1487688597266321564"),
                  nextButton: jQuery("#newsticker_next1487688597266321564"),
                  pauseOnHover: 1
              });
            });
        </script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/bg_BG/sdk.js#xfbml=1&version=v2.8&appId=1231753050227827";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
                
</body>
</html>
