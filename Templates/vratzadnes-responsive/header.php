<!DOCTYPE html>
<html lang="<?php echo($Site->language_key); ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title><?=$Title?></title>
    <?php
    require_once '/hosting/maksoft/maksoft/lib/lib_page.php';
    require_once '/hosting/maksoft/maksoft/lib/Database.class.php';
    //include meta tags
    include( __DIR__."/../meta_tags.php" );
    $o_site->print_sConfigurations();

    require_once __DIR__.'/settings.php';
    require_once __DIR__.'/helpers.php';
    ?>
    
    <!-- Custom Styles -->
    <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/plugins/post-views-counter/css/frontend.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/ilightbox/ilightbox.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/combined.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/menu/left-align.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/menu/left-align-grid.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/styles.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/css/layout.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/js/flexslider/flexslider.css' type='text/css' media='all' />
    
    <!-- Plugins -->
    
    <link rel='stylesheet'  href='<?=TEMPLATE_DIR?>assets/plugins/woocommerce/assets/css/woocommerce-layout.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/plugins/woocommerce/assets/css/woocommerce.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/plugins/zilla-likes/styles/zilla-likes.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?=TEMPLATE_DIR?>assets/plugins/wp-review/assets/css/wp-review.css' type='text/css' media='all' />

    <script type='text/javascript' src='<?=TEMPLATE_DIR?>assets/plugins/zilla-likes/scripts/zilla-likes.js'></script>
    
    <!-- Icons & Fonts -->
    <link rel='stylesheet'  href='<?=TEMPLATE_DIR?>assets/css/dashicons.min.css' type='text/css' media='all' />

    <link rel='stylesheet' id='grandnews-script-responsive-css-css' href='<?=TEMPLATE_DIR?>assets/css/custom.css' type='text/css' media='all' />
     
    <script src="https://use.typekit.net/uxf5hva.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>      

</head>

<body class="home page-template-default page page-id-3595 left_align">
<!-- Begin mobile menu -->
    <a id="close_mobile_menu" href="javascript:;"></a>
    <div class="mobile_menu_wrapper">
        <a id="close_mobile_menu_button" href="javascript:;"><i class="fa fa-close"></i></a>
        <div class="menu-main-menu-container">
            <ul id="mobile_main_menu" class="mobile_main_nav">
                <?php echo Helper::main_menu($menu_pages);?>
            </ul>
        </div>      
        <!-- Begin side menu sidebar -->
        <div class="page_content_wrapper">
            <div class="sidebar_wrapper">
                <div class="sidebar">
                    <div class="content">
                        <ul class="sidebar_widget"> </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End side menu sidebar -->
    </div>
    <!-- End mobile menu -->
    <!-- Begin template wrapper -->
    <div id="wrapper" class="ready">
        <div class="header_style_wrapper">
            <div class="top_bar">
                <div class="standard_wrapper">
                    <!-- Begin logo -->
                    <div id="logo_wrapper">
                        <div id="logo_normal" class="logo_container">
                            <div class="logo_align">
                                <a id="custom_logo" class="logo_wrapper default" href="/"><?=$o_page->_site['Name'];?></a>
                                <br />
                                <span class="slogan_logo"><?=$o_page->_site['Title'];?></span>
                            </div>
                        </div>
                        <!-- End logo -->
                        <div id="menu_wrapper">
                            <!-- Begin right corner buttons -->
                            <div id="logo_right_button">
                                <!-- Begin search icon -->
                                <a href="javascript:;" id="search_icon"><i class="fa fa-search"></i></a>
                                <!-- End side menu -->
                                <!-- Begin search icon -->
                                <a href="javascript:;" id="mobile_nav_icon"></a>
                                <!-- End side menu -->
                            </div>
                            <!-- End right corner buttons -->
                            <div id="nav_wrapper">
                                <div class="nav_wrapper_inner">
                                    <div id="menu_border_wrapper">
                                        <div class="menu-main-menu-container">
                                            <ul id="main_menu" class="nav">
                                                <?php echo Helper::main_menu($menu_pages);?>
                                            </ul>
                                        </div>              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
