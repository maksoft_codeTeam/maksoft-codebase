<?php
require_once __DIR__.'/settings.php';

class Helper implements Settings
{
    public static function main_menu($menu_pages){
        $html = "";
        $i = 0;
        foreach($menu_pages as $page){
            $class = "catmenu menu-item menu-item-type-taxonomy menu-item-object-category menu-item-714";
            $class = '';
            $html .= '<li class="'.$class.'">';
            $html .= '<a href="'.$page['page_link'].'" data-cat="11" style="padding-top: 8px; padding-bottom: 10px;">'.$page['Name'].'</a>';
            $html .= '<div class="catmenu_wrapper"></div>';
            $html .= '</li>';
            $i++;
            if($i >= Settings::MENU_MAX_ITEMS){
                break;
            } 
        }
        return $html;
    }

    public static function place_banner($br_id){
        echo '<script src="http://maksoft.net/banner.php?rid='.$br_id.'&SiteID=922" type=text/javascript></script>';
    }

    public static function place_small_banner($banners_container, $class){
        foreach($banners_container as $banner){
            $link = "/adclick.php?bid=".$banner->banner_id."&amp;SiteID=".$banner->banner_owner."&amp;url=".$banner->banner_url; 
            $img = $banner->banner_src;
            $title = $banner->banner_title;
            echo <<<HEREDOC
            <div class="$class" style="padding:30px 0 30px 0;">
                <div class="ads_label">- Advertisement -</div>
                    <a href="$link">
                        <img src="/$img" alt="$title">
                    </a>
            </div>
HEREDOC;
        }
    }
}
