<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<?php
define("LIMIT", Settings::PAGINATION_MAX_PAGES);
if(!isset($_GET['pag']) or !is_numeric($_GET['pag']) or $_GET['pag'] < 1){
    define("PAG", 1);
} else {
    define("PAG", $_GET['pag']);
}

$subpages = $o_page->get_pSubpages($o_page->_page['n'], null, 1, false, array(((PAG * LIMIT) - LIMIT), LIMIT));
$p_news = array_shift($subpages);

$right_coloumn_news_count = 4;

function build_url($n, $SiteID, $pagination){
    parse_str($_SERVER['QUERY_STRING'], $url);
    $url['n'] = $n;
    $url['SiteID'] = $SiteID;
    $url['pag'] = $pagination;
    return '/page.php?'.http_build_query($url);
}

?>
<div id="page_caption" class="  ">
    <div class="page_title_wrapper">
        <div class="page_title_inner">
            <h1><span><?=$o_page->_page['Name'];?></span></h1>
        </div>
        <div class="post_info_cat"> <?="".$nav_bar.""?> </div>
    </div>
</div>

<?php require_once Settings::TMPL_PATH.'includes/top_news_homepage2.php'; ?>
<div id="page_content_wrapper" class="">
    <div class="inner">
        <div class="one withsmallpadding ppb_ads" style="padding:30px 0 30px 0;">
            <div class="ads_label">- Advertisement -</div>
            <p><?=Helper::place_banner(Settings::BANNER_MIDDLE_SMALL); ?></p>
        </div>
        <div class="inner_wrapper" style="position: relative;">
            <div class="sidebar_content">
                <!-- Begin each blog post -->
            <?php foreach ($subpages as $article){ ?>
                <div id="post-79" class="post-79 post type-post status-publish format-standard has-post-thumbnail hentry category-business category-start-up tag-nature tag-photography tag-travel">
                    <div class="post_wrapper">
                        <div class="post_content_wrapper">
                            <div class="post_header search">
                                <div class="post_img static one_third">
                                <a href="<?=$article['page_link'];?>">
                                <img src="<?=$article['image_src'];?>" alt="<?=$article['Name'];?>" class="">
                                    </a>
                                </div>
                                <div class="post_header_title two_third last">
                                <h5><a href="<?=$article['page_link'];?>" title="<?=$article['Name'];?>"><?=$article['Name'];?></a></h5>
                                    <p></p>
                                    <p><?=cut_text($article['textStr'], Settings::MAXIMUM_CHARACTERS);?></p>
                                    <p></p>
                                    <div class="post_detail post_date">
                                        <span class="post_info_date">
                                        <a href="<?=$article['page_link'];?>"><?=$article['date_added'];?></a>
                        </span>
                        <a href="#" class="zilla-likes" id="zilla-likes-79" title="Like this"><span class="zilla-likes-count"><?=$article['preview'];?></span> <span class="zilla-likes-postfix"></span></a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br class="clear">
        <?php } ?>
                <div class="pagination">
                <ul class="pagination" role="menubar" aria-label="Pagination">
                    <?php $link = "/page.php?n=".$o_page->_page['n']; ?>
                    <?php $max_pages = round($subpages_length / LIMIT); ?>
                    <li><a href="<?=build_url($n, $SiteID, 1)?>"><span>First</span></a></li>
                    <?php if((PAG -1) > 1){ ?>
                        <li><a href="<?=build_url($n, $SiteID, PAG-1)?>"><span>Previous</span></a></li>
                    <?php }
                    $subpages_length =(int) round( $subpages_length / LIMIT);
                    for($i=1; $i< $subpages_length; $i++){
                       $cls = '';
                       if(PAG == $i) { 
                           $cls = 'class="current"';
                       }
                       echo "<li><a href=\"".build_url($n, $SiteID, $i)."\" $cls>$i</a></li>";
                    }
                    ?>
                    <li><a href="<?=build_url($n, $SiteID, PAG+1)?>"><span>Next</span></a></li>
                    <li><a href="<?=build_url($n, $SiteID, $max_pages)?>"><span>Last</span></a></li>
                  </ul>
                </div>
            </div>
            <div class="sidebar_wrapper">
                <div class="sidebar">
                    <div class="content">
                        <ul class="sidebar_widget">
                            <?php require_once Settings::TMPL_PATH.'includes/right_column_categories.php'; ?>
                            <?php require_once Settings::TMPL_PATH.'includes/right_column_poll.php'; ?> 
                            <li id="grand_news_popular_posts-3" class="widget Grand_News_Popular_Posts">
                                <h2 class="widgettitle"><span>Popular Posts</span></h2>
                                <ul id="popular-posts" class="posts blog withthumb "> </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <br class="clear">
            </div>
        </div>
        <!-- End main content -->
    </div>
</div>
<script id="single-post-popular" type="text/x-handlebars-template">
    {{# each articles }}
    <li>
        <div class="post_circle_thumb">
            <a href="/page.php?n={{n}}&SiteID={{SiteID}}">
                <div class="post_number">{{preview}}</div>
                <img src="/{{image_src}}" class="tg-lazy" alt="{{Name}}">
            </a>
        </div>
        <a href="/page.php?n={{n}}&SiteID={{SiteID}}">{{Name}}</a>
    </li>
    {{/each}}
</script>


<script>
jQuery(function() {
    var url ="/api/?command=most_visited&n=1&SiteID=922&limit=<?=LIMIT;?>";
    get(url)
    .then(JSON.parse)
    .then(function(articles){
       var source   = jQuery("#single-post-popular").html();
       var template = Handlebars.compile(source);
       var html = template({'articles':articles});
       jQuery("#popular-posts").html(html);
    });; 
});

</script>
