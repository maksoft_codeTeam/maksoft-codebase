<div id="page_content_wrapper" class="">
    <div class="inner">
        <!-- Begin main content -->
        <div class="inner_wrapper" style="position: relative;">

            <div class="sidebar_content">
                <!-- Begin each blog post -->
                <div id="post-79" class="post-79 post type-post status-publish format-standard has-post-thumbnail hentry category-style category-wellness tag-nature tag-photography tag-travel">
                    <div class="post_wrapper">
        
                        <div class="post_content_wrapper">
        
                    
                            <div class="post_header">
                                <div class="post_header_title">
                                    <div class="post_info_cat">
                                        <div class="breadcrumb"><?=$nav_bar?></div>
                                    </div>
                                    <h1><?=$o_page->_page['Name']?></h1>
                                    <div class="post_detail post_date">
                                        <span class="post_info_date"><?=$o_page->_page['date_added'];?></span>
                                        <div class="post_info_comment">
                                            <a href="http://themes.themegoods2.com/grandnews/demo1/magnificent-image-of-the-new-hoover-dam-bridge-taking-shape/#respond">
                                                <i class="fa fa-commenting"></i>0
                                            </a>
                                        </div>
                                        <div class="post_info_view">
                                            <i class="fa fa-eye"></i><?=$o_page->_page['preview'];?>&nbsp;Views
                                        </div>
                                        <a href="#" class="zilla-likes" id="zilla-likes-79" title="Like this">
                                            <span class="zilla-likes-count">28</span> <span class="zilla-likes-postfix"></span>
                                        </a>
                                   </div>
                               </div>
                           </div>
                           <hr class="post_divider"><br class="clear"> <br class="clear">      
                        <div class="post_img static">
                            <img src="/img_preview.php?image_file=<?=$o_page->_page['image_src'];?>&img_width=640&img_height=960" alt="" class="" style="width:960px;height:640px;">
                        </div>
                        <div class="post_share_center">
                            <div class="social_share_counter">
                                <div class="social_share_counter_number">
                                    <span class="post-views-icon dashicons dashicons-chart-bar"></span>
                                    <span class="post-views-count"><?=$o_page->_page['preview'];?></span>
                                </div>
                            </div>
                            <?php include __DIR__.'/templates/share-buttons.php'; ?>
                                <br class="clear">              
                            </div>
                            <div class="post_header single">
                                <?=$o_page->_page['textStr'];?>
                                <div class="post-views post-79 entry-meta"> </div>              
                                <hr>
                            </div>
                            <br class="clear"><br>
                            <?php $parent_title = $o_page->get_pName($o_page->_page['ParentPage']); ?>
                            <h5 class="single_subtitle">��� �� <?=$parent_title?></h5>
                            <div class="post_trending">
                            <?php 
                            $subpages = $o_page->get_pSubpages($o_page->_page['ParentPage'], null, 1, false, array(0, 9));
                            $i=0;
                            foreach ($subpages as $article){
                                $cls = '';
                                if($article['n'] == $o_page->_page['n']){ continue; }
                                if($i % 2 and $i != 0){
                                    $cls = 'last';
                                }
                            ?>
                                <div class="one_half <?=$cls?>">
                                    <div class="post_wrapper grid_layout">
                                        <div class="post_img small static">
                                            <a href="<?=$article['page_link']?>">
                                                <img src="<?=$article['image_src'];?>" alt="" class="" style="width:700px;height:466px;">
                                            </a>
                                        </div>
                                        <div class="blog_grid_content">
                                            <div class="post_header grid">
                                                <h6>
                                                <a href="<?=$article['page_link'];?>" title=""><?=$article['Name'];?></a>

                                                </h6>
                                                <div class="post_detail post_date">
                                                    <span class="post_info_date">
                                                    <?=$article['date_added'];?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php $i++; ?>
                            <?php } ?>
                            </div>

<h5 class="single_subtitle">��� �� <?=$parent_title?></h5>
<div class="post_related">
                        
        </div>
        
    </div>

</div>
<!-- End each blog post -->


<div class="fullwidth_comment_wrapper sidebar">
    <h5 class="single_subtitle">Leave A Reply</h5><br class="clear">
  




 

    <div id="respond" class="comment-respond">
        <h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" href="/grandnews/demo1/magnificent-image-of-the-new-hoover-dam-bridge-taking-shape/#respond" style="display:none;">Cancel reply</a></small></h3>         <form action="http://themes.themegoods2.com/grandnews/demo1/wp-comments-post.php" method="post" id="commentform" class="comment-form">
                <p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span></p><p class="comment-form-comment"><label for="comment">Comment</label> <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea></p><p class="comment-form-author"><label for="author">Name <span class="required">*</span></label> <input placeholder="Name*" id="author" name="author" type="text" value="" size="30" maxlength="245" aria-required="true" required="required"></p>
<p class="comment-form-email"><label for="email">Email <span class="required">*</span></label> <input type="email" placeholder="Email*" id="email" name="email" value="" size="30" maxlength="100" aria-describedby="email-notes" aria-required="true" required="required"></p>
<p class="comment-form-url"><label for="url">Website</label> <input placeholder="Website" id="url" name="url" type="url" value="" size="30" maxlength="200"></p>
<p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Post Comment"> <input type="hidden" name="comment_post_ID" value="79" id="comment_post_ID">
<input type="hidden" name="comment_parent" id="comment_parent" value="0">
</p>            </form>
            </div><!-- #respond -->
                
 </div>

                        
        </div>
        </div>

            <div class="sidebar_wrapper">
                <div class="sidebar_top"></div>
                <div class="sidebar">
                    <div class="content">
                        <ul class="sidebar_widget">
                            <li id="grand_news_custom_ads-3" class="widget Grand_News_Custom_Ads">
                                <div class="ads_label">- Advertisement -</div>
                                <?=Helper::place_banner(Settings::BANNER_RIGHT_BIG); ?>
                            </li>
                            <?php require_once Settings::TMPL_PATH.'includes/right_column_poll.php'; ?> 
                            <li id="grand_news_popular_posts-3" class="widget Grand_News_Popular_Posts">
                                <h2 class="widgettitle"><span>Popular Posts</span></h2>
                                <ul id="popular-posts" class="posts blog withthumb "> </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <br class="clear">
                <div class="sidebar_bottom"></div>
            </div>
    </div>
    <!-- End main content -->
   
</div>

</div>
<script id="single-post-popular" type="text/x-handlebars-template">
    {{# each articles }}
    <li>
        <div class="post_circle_thumb">
            <a href="/page.php?n={{n}}&SiteID={{SiteID}}">
                <div class="post_number">{{preview}}</div>
                <img src="/{{image_src}}" class="tg-lazy" alt="{{Name}}">
            </a>
        </div>
        <a href="/page.php?n={{n}}&SiteID={{SiteID}}">{{Name}}</a>
    </li>
    {{/each}}
</script>


<script>
jQuery(function() {
    var url ="/api/?command=most_visited&n=1&SiteID=922&limit=20";
    get(url)
    .then(JSON.parse)
    .then(function(articles){
       var source   = jQuery("#single-post-popular").html();
       var template = Handlebars.compile(source);
       var html = template({'articles':articles});
       jQuery("#popular-posts").html(html);
    });
});

</script>
