<div class="social_share_button_wrapper">
    <ul>
        <li><a class="tooltip facebook_share" title="Share On Facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=$o_page->get_pLink();?>"><i class="fa fa-facebook"></i>Share On Facebook</a></li>
        <li><a class="tooltip twitter_share" title="Share On Twitter" target="_blank" href="https://twitter.com/intent/tweet?original_referer=<?=$o_page->get_pLink();?>&amp;text=<?=iconv('cp1251', 'utf8', $o_page->_page['Name']);?>&amp;url=<?=$o_page->get_pLink();?>"><i class="fa fa-twitter"></i>Tweet It</a></li>
        <li><a class="tooltip google_share" title="Share On Google+" target="_blank" href="https://plus.google.com/share?url=<?=$o_page->get_pLink();?>"><i class="fa fa-google-plus"></i></a></li>
        <li><a class="tooltip email_share" title="Share by Email" href="mailto:?Subject=<?=$o_page->_page['Name'];?>&amp;Body=<?=$o_page->get_pLink();?>"><i class="fa fa-envelope"></i></a></li>
    </ul>
</div>
