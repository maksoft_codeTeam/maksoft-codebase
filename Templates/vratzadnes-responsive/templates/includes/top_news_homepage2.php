<div class="ppb_blog_grid_with_posts one nopadding " style="padding-top: 0 !important;padding:0px 0 0px 0;">

<?php
if (isset($title)){ ?> 
    <blockquote><p><?=$title?></p></blockquote> 
<?php } ?>
    <div class="standard_wrapper">
        <div class="two_third div_wrapper" style="background-image:url(<?=$p_news['image_src'];?>);">
            <div class="post_header">
                <div class="post_detail post_date">
                    <span class="post_info_date"> <a href="<?=$p_news['page_link'];?>"><?=$p_news['date_added'];?></a> </span>
                </div>
                <h2><a href="<?=$p_news['page_link'];?>" title="<?=$p_news['Name'];?>"><?=$p_news['Name'];?></a></h2>
            </div>
        </div>
        <div class="one_third last">
            <?php $i =0 ;?>
            <?php while($p_news = array_shift($subpages)) { ?>
            <?php if(isset($right_coloumn_news_count) and $right_coloumn_news_count == $i){ break; } ?>
            <?php $i++;?>
            <div class="post type-post">
                <div class="post_wrapper">
                    <div class="post_content_wrapper">
                        <div class="post_header search">
                            <div class="post_img static one_third">
                                <a href="<?=$p_news['page_link'];?>"> <img src="<?=$p_news['image_src'];?>" alt="<?=$p_news['Name'];?>" class=""> </a>
                            </div>
                            <div class="post_header_title two_third last">
                                <h5><a href="<?=$p_news['page_link'];?>" title="<?=$p_news['Name'];?>"><?=$p_news['Name'];?></a></h5>
                                <span class="post_info_date">
                                    <a href="<?=$p_news['page_link'];?>"><?=$p_news['date_added'];?></a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
</div>
</div>
