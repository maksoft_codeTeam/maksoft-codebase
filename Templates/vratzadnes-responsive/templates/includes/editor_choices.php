<script id="article-carousel" type="text/x-handlebars-template">
<div class="ppb_blog_carousel one nopadding" id="14913768121931109020{{n}}" style="padding:0px 0 0px 0;">
    <div class="standard_wrapper slider_wrapper">
        <a id="14915620891304612064_loadmore" href="{{link}}" class="single_subtitle pagination_load_more"><span>{{Name}}</span></a>
        <div class="flexslider" data-height="550"></div>
        <div class="flex-viewport" style="overflow: hidden; position: relative; height: auto;">
            <ul id="cat{{n}}" class="slides loader" style="width: 1000%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
            </ul>
        </div>
    </div>
</div>
</script>
<script id="article-other" type="text/x-handlebars-template">
<li style="width: 303.75px; margin-right: 30px; float: left; display: block;">
    <div class="one">
        <div class="post_img static small">
            <a href="{{link}}" title="{{Name}}">
                <img src="{{image_src}}" alt="{{Name}}" draggable="false">
            </a>
         </div>
        <br class="clear">
        <div class="post_header">
            <h5><a href="{{link}}" title="{{Name}}">{{Name}}</a></h5>
        </div>
    </div>
</li>
</script>
<script>
var flexslider;
 
function carouselGetGridSize() {
    return (window.innerWidth < 768) ? 1 :
           (window.innerWidth < 900) ? 3 : 4;
}

jQuery(window).load(function(){ 
    jQuery(".slider_wrapper").flexslider({
          animation: "slide",
          animationLoop: true,
          itemMargin: 30,
          itemWidth: 466,
          minItems: carouselGetGridSize(), // use function to pull in initial value
          maxItems: carouselGetGridSize(),
          directionNav: false,
          controlNav: true,
          smoothHeight: true,
          animationSpeed: 600,
          move: 1,
          start: function(slider){
            flexslider = slider;
          }
    });
    
    jQuery(window).resize(function() {
        var gridSize = carouselGetGridSize();
     
        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
    });
});

function children(page_id)
{
    var source   = jQuery("#article-other").html();
    var single_article = Handlebars.compile(source);
    var n = page_id;
    get("/api/?command=get_subpages&SiteID=922&ParentPage="+n+"&n=1&limit=6&order=date_desc")
        .then(JSON.parse)
        .then(function(articles){
            for(i=0; i< articles.length; i++){
                article = articles[i]
                html = single_article({
                    link: article.link,
                    Name: article.Name,
                    image_src: article.image_src,
                    date_added: article.date_added,
                    preview: article.preview
                });
                jQuery("#cat"+n).append(html);
            }
            return n;
        })
        .then(function(){
            jQuery("#cat" + n).removeClass("loader");
        });
}

jQuery(document).ready(function(){

    var news_group = jQuery("#article-carousel").html();
    var news_group = Handlebars.compile(news_group);

    get("/api/?command=get_group&n=<?=Settings::MAIN_MENU?>")
        .then(JSON.parse)
        .then(function(articles){
            for(i=0; i< articles.length; i++){
                article = articles[i]
                if (article.n == 201161) {
                    continue;
                }
                html = news_group({
                    n: article.n,
                    link: article.link,
                    Name: article.Name,
                    image_src: article.image_src,
                    date_added: article.date_added,
                    preview: article.preview
                });
                jQuery("#important-news").append(html);
                children(article.n);
            }
            return true;
        })
        .then(function(){
            jQuery("#important-news").show();
            jQuery("#1487688597877917989_loading2").hide();
        });
});
</script>

