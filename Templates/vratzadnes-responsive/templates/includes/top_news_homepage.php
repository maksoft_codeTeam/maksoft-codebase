<?php
$i = 0;
$styles = array(
    "one_half div_wrapper",
    "one div_small_wrapper",
    "one div_small_wrapper last"
);
$top_news = $o_page->get_pGroupContent(Settings::TOP_NEWS);
$style = array_shift($styles);
shuffle($top_news);
$p_news = array_shift($top_news);

?>
<div class="<?=$style?>" style="background-image:url(&quot;/<?=$p_news['image_src'];?>&quot;);">
    <div class="post_header">
        <div class="post_detail post_date">
            <span class="post_info_author">
            </span><span class="post_info_date">
            <a href="<?=$o_page->get_pLink($p_news['n']);?>"><?=$p_news['date_added'];?></a>
            </span>
        </div>
        <h2><a href="<?=$o_page->get_pLink($p_news['n']);?>" title="<?=$p_news['Name'];?>"><?=$p_news['Name'];?></a></h2>
    </div>
</div>

<div class="one_half div_wrapper last">
<?php foreach($styles as $style){ ?>
<?php
    shuffle($top_news);
    $style = array_shift($styles);
    $p_news = array_shift($top_news);
    if(!$p_news){
        break;
    }
?>
<div class="<?=$style?>" style="background-image:url(&quot;/<?=$p_news['image_src'];?>&quot;);">
    <div class="post_header">
        <div class="post_detail post_date">
            <span class="post_info_author">
            </span><span class="post_info_date">
            <a href="<?=$o_page->get_pLink($p_news['n']);?>"><?=$p_news['date_added'];?></a>
            </span>
        </div>
        <h2><a href="<?=$o_page->get_pLink($p_news['n']);?>" title="<?=$p_news['Name'];?>"><?=$p_news['Name'];?></a></h2>
    </div>
</div>
<?php } ?>
</div>
