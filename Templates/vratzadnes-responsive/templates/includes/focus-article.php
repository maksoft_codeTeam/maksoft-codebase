<div class="ppb_blog_fullwidth_slider one nopadding ">
    <div id="14913768121028307566" class="slider_wrapper">
        <div class="flexslider" data-height="550"></div>
        <div class="flex-viewport" style="overflow: hidden; position: relative; height: 480px;">
            <ul class="slides" style="width: 600%; transition-duration: 0s; transform: translate3d(-1265px, 0px, 0px);">
                <li class="clone" aria-hidden="true" style="width: 1265px; margin-right: 0px; float: left; display: block;">
                    <a href="http://themes.themegoods2.com/grandnews/demo2/forest-festivities-the-best-festival-outfits-from-secret-garden-australia/">
                        <div class="slider_image" style="background-image:url('http://themes.themegoods2.com/grandnews/demo2/wp-content/uploads/2015/07/startup-photos-1.jpg');">
                            <div class="slider_post_title">
                                <div class="standard_wrapper">
                                    <div class="post_category_tag"><span>Business</span></div>
                                    <div class="post_detail post_date">
                                        <span class="post_info_author">
                            <span class="gravatar"><img alt="" src="http://1.gravatar.com/avatar/db6f032dce962144efc9b625779461a1?s=60&amp;d=mm&amp;r=g" srcset="http://1.gravatar.com/avatar/db6f032dce962144efc9b625779461a1?s=120&amp;d=mm&amp;r=g 2x" class="avatar avatar-60 photo" height="60" width="60" draggable="false"></span>John Philippe
                                        </span>
                                        <span class="post_info_date">
                            July 21, 2015
                        </span>
                                    </div>
                                    <h2>Forest Festivities: The Best Festival Outfits From Secret Garden Australia</h2></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="flex-active-slide" style="width: 1265px; margin-right: 0px; float: left; display: block;">
                    <a href="http://themes.themegoods2.com/grandnews/demo2/forest-festivities-the-best-festival-outfits-from-secret-garden-australia/">
                        <div class="slider_image" style="background-image:url('http://themes.themegoods2.com/grandnews/demo2/wp-content/uploads/2015/07/startup-photos-1.jpg');">
                            <div class="slider_post_title">
                                <div class="standard_wrapper">
                                    <div class="post_category_tag"><span>Business</span></div>
                                    <div class="post_detail post_date">
                                        <span class="post_info_author">
                            <span class="gravatar"><img alt="" src="http://1.gravatar.com/avatar/db6f032dce962144efc9b625779461a1?s=60&amp;d=mm&amp;r=g" srcset="http://1.gravatar.com/avatar/db6f032dce962144efc9b625779461a1?s=120&amp;d=mm&amp;r=g 2x" class="avatar avatar-60 photo" height="60" width="60" draggable="false"></span>John Philippe
                                        </span>
                                        <span class="post_info_date">
                            July 21, 2015
                        </span>
                                    </div>
                                    <h2>Forest Festivities: The Best Festival Outfits From Secret Garden Australia</h2></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="clone" aria-hidden="true" style="width: 1265px; margin-right: 0px; float: left; display: block;">
                    <a href="http://themes.themegoods2.com/grandnews/demo2/forest-festivities-the-best-festival-outfits-from-secret-garden-australia/">
                        <div class="slider_image" style="background-image:url('http://themes.themegoods2.com/grandnews/demo2/wp-content/uploads/2015/07/startup-photos-1.jpg');">
                            <div class="slider_post_title">
                                <div class="standard_wrapper">
                                    <div class="post_category_tag"><span>Business</span></div>
                                    <div class="post_detail post_date">
                                        <span class="post_info_author">
                            <span class="gravatar"><img alt="" src="http://1.gravatar.com/avatar/db6f032dce962144efc9b625779461a1?s=60&amp;d=mm&amp;r=g" srcset="http://1.gravatar.com/avatar/db6f032dce962144efc9b625779461a1?s=120&amp;d=mm&amp;r=g 2x" class="avatar avatar-60 photo" height="60" width="60" draggable="false"></span>John Philippe
                                        </span>
                                        <span class="post_info_date">
                            July 21, 2015
                        </span>
                                    </div>
                                    <h2>Forest Festivities: The Best Festival Outfits From Secret Garden Australia</h2></div>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <ul class="flex-direction-nav">
            <li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li>
            <li class="flex-nav-next"><a class="flex-next flex-disabled" href="#" tabindex="-1">Next</a></li>
        </ul>
    </div>
    <script>
        jQuery(window).load(function() {
            jQuery("#14913768121028307566").flexslider({
                animation: "slide",
                animationLoop: true,
                itemMargin: 0,
                minItems: 1,
                maxItems: 1,
                slideshow: 0,
                controlNav: false,
                smoothHeight: true,
                slideshowSpeed: 5000,
                animationSpeed: 1600,
                move: 1
            });
        });
    </script>
</div>
