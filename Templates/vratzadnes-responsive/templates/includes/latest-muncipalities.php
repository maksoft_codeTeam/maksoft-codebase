<div class="ppb_blog_filterable_3cols one nopadding " style="padding-bottom: 0 !important;padding:30px 0 30px 0;">
    <div class="standard_wrapper three_cols">
        <div class="ppb_subtitle_left">
            <h5 class="single_subtitle">���������� ������</h5>
        </div>
        <ul id="14913768122119386420_filter" class="post_filter ">
            <?php
                $muncipalities = $o_page->get_pSubpages(190806);
                foreach($muncipalities as $muncipality){
                    echo '<li><a onclick="show(this)" href="javascript:;" class="" data-filter="'.$muncipality['n'].'" data-items="6">'.$muncipality['Name'].'</a></li>';
                }
            ?>
        </ul>
            <div id="1487688597877917989_loading3" class="post_filter_loading">
                <div class="loading_wrapper">
                    <div class="sk-cube-grid">
                        <div class="sk-cube sk-cube1"></div>
                        <div class="sk-cube sk-cube2"></div>
                        <div class="sk-cube sk-cube3"></div>
                        <div class="sk-cube sk-cube4"></div>
                        <div class="sk-cube sk-cube5"></div>
                        <div class="sk-cube sk-cube6"></div>
                        <div class="sk-cube sk-cube7"></div>
                        <div class="sk-cube sk-cube8"></div>
                        <div class="sk-cube sk-cube9"></div>
                    </div>
                </div>
            </div>
        <div id="muncipalities_articles" class="post_filter_wrapper">
         </div>
    </div>
</div>
<script id="article-muncipality" type="text/x-handlebars-template">
<div class="one_third {{last}}">
    <div class="post_img static small">
        <a href="{{link}}" title="{{name}}">
        <div class="post_icon_circle"><i class="fa fa-play"></i></div><img src="{{image_src}}"></a>
        </div>
        <br class="clear">
        <div class="post_header">
        <h5><a href="{{link}}" title="{{Name}}">{{Name}}></a></h5>
            <div class="post_detail post_date">
                <span class="post_info_author">
                <a href="/">Vratzadnes</a>
                </span><a href="#" class="zilla-likes" id="zilla-likes-56" title="{{Name}}"><span class="zilla-likes-count">{{preview}}</span>
                <span class="zilla-likes-postfix"></span></a><span class="post_info_date">
                <a href="{{link}}">{{date_added}}</a>
            </span>
            </div>
        </div>
    </div>
      {{#if last}}
        <br class="clear">
      {{/if}}
</script>
<script>
function hide_all(){
    jQuery.each(jQuery("#muncipalities_articles").children(), function(i, div){
        jQuery(div).hide();
    });
}


function show(el){
    el = jQuery(el);
    jQuery("#"+el.data('filter')+"14913768122119386420_wrapper").show();
    jQuery("#muncipalities_articles").html('');
    jQuery("#1487688597877917989_loading3").show();
    var source   = jQuery("#article-muncipality").html();
    var template = Handlebars.compile(source);
    get("/api/?command=get_subpages&SiteID=922&ParentPage="+el.data('filter')+"&n=1&limit=6&order=date_desc")
        .then(JSON.parse)
        .then(function(articles){
            jQuery("#1487688597877917989_loading3").show();
            return articles;
        })
        .then(function(articles){
            j = 1;
            for(i=0; i< articles.length; i++){
                article = articles[i]
                var last = '';
                if(j == 3) {
                    last = 'last'
                    j = 1;
                } else {
                    j += 1
                } 
                html = template({
                    last: last,
                    link: article.link,
                    Name: article.Name,
                    image_src: article.image_src,
                    date_added: article.date_added,
                    preview: article.preview
                });
                jQuery("#muncipalities_articles").append(html);

            }
            return true;
        })
       .then(function(){
            jQuery("#1487688597877917989_loading3").hide();
        });;
}

jQuery(document).ready(function(){
    show(document.querySelectorAll('[data-filter="193665"]'));

});
</script>

