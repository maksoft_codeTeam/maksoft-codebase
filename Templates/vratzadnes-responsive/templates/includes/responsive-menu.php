<a id="close_mobile_menu" href="javascript:;"></a>
<div class="mobile_menu_wrapper">
    <a id="close_mobile_menu_button" href="javascript:;"><i class="fa fa-close"></i></a>
    <div class="menu-main-menu-container">
        <ul id="mobile_main_menu" class="mobile_main_nav">
            <?php echo Helper::main_menu($o_page);?>
        </ul>
    </div>
    <!-- Begin side menu sidebar -->
    <div class="page_content_wrapper">
        <div class="sidebar_wrapper">
            <div class="sidebar">
                <div class="content">
                    <ul class="sidebar_widget">
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End side menu sidebar -->
</div>
