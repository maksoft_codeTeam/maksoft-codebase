<div class="header_style_wrapper">

    <div class="top_bar">
        <div class="standard_wrapper">
            <!-- Begin logo -->
            <div id="logo_wrapper">
                <div id="logo_normal" class="logo_container">
                    <div class="logo_align">
                        <a id="custom_logo" class="logo_wrapper default" href="/" style="margin-top: 0px;">
                        <img src="http://themes.themegoods2.com/grandnews/demo1/wp-content/uploads/2016/03/logo.png" alt="" width="267" height="33">
                        </a>
                    </div>
                </div>
                <!-- End logo -->
                <div id="menu_wrapper">
                    <!-- Begin right corner buttons -->
                    <div id="logo_right_button">
                        <!-- div class="post_share_wrapper">
                            <a id="page_share" href="javascript:;"><i class="fa fa-share-alt"></i></a>
                            </div -->
                        <!-- Begin search icon -->
                        <a href="javascript:;" id="search_icon"><i class="fa fa-search"></i></a>
                        <!-- End side menu -->
                        <!-- Begin search icon -->
                        <a href="javascript:;" id="mobile_nav_icon"></a>
                        <!-- End side menu -->
                    </div>
                    <!-- End right corner buttons -->
                    <div id="nav_wrapper">
                        <div class="nav_wrapper_inner">
                            <div id="menu_border_wrapper">
                                <div class="menu-main-menu-container">
                                    <ul id="main_menu" class="nav">
                                        <?php echo Helper::main_menu($o_page);?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- End main nav -->
                </div>
            </div>
        </div>
    </div>
</div>
