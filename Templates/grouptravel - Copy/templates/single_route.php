<link rel='stylesheet' href='<?=ASSETS_DIR?>assets/css/jquery.seat-charts.css' type='text/css' media='all' />
<?php
use Doctrine\Common\Collections\Criteria;


$from_name = $route->getStartStation()->getCountry(). ', ' .$route->getStartStation()->getName();
$to_name = $route->getEndStation()->getCountry(). ', ' .$route->getEndStation()->getName();

// �� ������������  � PHP ��������� ������� �� 0(������), �� �� ����� � ���������� :) � ��� ����� ����� :) 0_o
$dayOfWeek +=1; 

$dateOfWeek = date('w', strtotime($schedule->getStartDate()));
$schedule_time = $schedule->getStartDate()->format('H:i');
$direction = $schedule->getDirection();
$day = ucfirst($week_days['bg'][$dayOfWeek]);
$day = in_array(substr($day, -1), array('�', '�')) ? "����� $day" : "����� $day";
$ticketTypes = $em->getRepository('\Bus\models\TicketType')->findAll();

$vehicles = $schedule->getVehicles();
$tmp_map = array();
foreach(range(1,15) as $row) {
    $tmp_map[$row] = array();
    foreach(range(1,5) as $col){
        $tmp_map[$row][$col] = false;
        $i++;
    }
}
$seat_matrix = array();
$schedule_id = $schedule->getId();

foreach($vehicles as $vehicle) {
    $seat_matrix[$vehicle->getId()] = $tmp_map;
    foreach($vehicle->getSeats() as $seat) {
        $row = $seat->getRow();
        $col = $seat->getCol();
        $number = $seat->getNumber();
        $available = true;
        if($seat->isBlocked() or !is_free($seat, $schedule)) {
            $available = false;
        }

        $seat_matrix[$vehicle->getId()][$row][$col] = array(
            'id' => $seat->getId(),
            'number'=> $number,
            'num'=> $number,
            'available' => $available,
            'row' => $seat->getRow(),
            'col' => $seat->getCol()
        );
    }
}
$course_info = $get_course_info($direction, $route);
$time_to_course = $course_info['time']['to_course'];
$travel_time = $course_info['time']['duration'];
$deparature = $schedule->getStartDate()->modify("+$time_to_course minutes");
$arrival = clone $deparature;
$arrival->add(new DateInterval('PT' . $travel_time  . 'M'));
$arrival_date = $arrival->format('�������� � H:i �� d-m-Y');
?>
                        
<div class="destination-info">
    <div class="row">
        <div class="col-md-6 first-col no-padmob">
            <div class="col-md-3 famob">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
            </div>
            <div class="col-md-9 no-padmob">
                �������� ��:<br/>
                <span>
                    <?=$from_name?>
                </span><br/> ���������� �:<br/>
                <span>
                <?=$to_name?>
                </span><br/> ����������:
                <br/>
                <span>
                <?=$course_info['distance']?> ��.</span>
            </div>
        </div>
        <div class="col-md-6 no-padmob">
            <div class="col-md-3 famob">
                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
            </div>
            <div class="col-md-9 no-padmob">
                T�������:<br/>
                <span>
                    <?=$deparature->format('� H:i �. �� d-m-Y');?>
                </span><br/> ��������:<br/>
                <span>
                    <?=$arrival->format('� H:i �. �� d-m-Y');?>
                </span><br/> �����������:
                <br/>
                <span>
                    <?=iconv('utf8', 'cp1251', $format_time($travel_time))?>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <?php foreach($route->getPrices() as $price):
                  $pr = number_format($price->getPrice(), 2);
        ?>
        <div class="col-md-12 text-center">
            ���� �� <strong><?=$price->getType()->getDescription()?></strong>: <br/>
            <span class="price"> <?=$pr > 0 ? $pr . ' ��.' : '' ;?> </span>
        </div>
        <?php endforeach; ?>
    </div>
</div>

<div class="bs-title">
    <span>�������� �����</span>
</div>
<div class="row">
    <?php foreach($vehicles as $vehicle) : ?>
    <div class="col-md-6">
    <div class="seatCharts-container">
    <div class="front-indicator">������� <?=$vehicle->getMake()?></div>
            <?php foreach($seat_matrix[$vehicle->getId()] as $row) :?>
                <div class="seatCharts-row">
                    <?php foreach($row as $seat): ?>
                        <?php if(!$seat): ?>
                            <div class="seatCharts-cell seatCharts-space"></div>
                            <?php continue;?>
                        <?endif;?>
                    <div data-route="<?php echo $route->getId();?>"
                         data-seat="<?php echo $seat['id'];?>"
                         data-vehicle="<?php echo $vehicle->getId()?>"
                         data-client="1"
                         data-schedule="<?php echo $schedule->getId()?>"
                         role="checkbox" aria-checked="false" 
                         id="<?=$vehicle->getID()?><?=$seat['id'];?>"
                         focusable="true" class="seat seatCharts-seat seatCharts-cell economy-class <?=$seat['available'] ? 'available' : 'unavailable'?>"><?=$seat['number']?></div>
                    <?php endforeach;?>
                </div>
            <?php endforeach;?>
    </div>
    </div>
    <?php endforeach;?>
</div>
����: <strong><span id="total">0</span> ��.</strong>
<div class="bs-title">
    <h3>������:</h3>
    <?php foreach($ticketTypes as $type):?>
        <li><?=$type->getDescription();?></li>
    <?php endforeach;?>
    <hr>
    <h3>����� �����:</h3>
    <li>����� � ����� �� 20�� - ��������� </li>
    <li>2�� ����� - 10��. (5�) </li>
    <li>3�� ����� - 20��. (10�)</li>
    <hr>
    <span>��������� ����� �� ���������</span>
</div>

<form method="POST">
    <div id='passengers'>
    <?php $i=1;?>
    <?php foreach($get_reserved_tickets(array('schedule' => $schedule_id)) as $seat): ?>
        <?php include __DIR__.'/passenger_form.php';?>
        <?php $i++;?>
    <?php endforeach; ?>
    </div>
    <div class='checkout row'>
        <div class="col-md-12">
            <div class="well">
                <h4>������</h4>
                <fieldset id="private-fieldset">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="payer-firstname" class="control-label">���</label>
                                <input id="first_name" name="payer[firstName]" type="text" placeholder="" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="payer-lastname" class="control-label">�������</label>
                                <input type="text" maxlength="25" id="payer-lastname" name="payer[lastName]" value="">
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset id="common-fieldset">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="payer-phone" class="control-label" data-trigger="hover" data-toggle="popover" data-content="����� �� ������� �� ������ � ������� , ������������ �������, ��� �������� ����, �� ����� �� ������� ���� �� �� ������ ������ ����������, � ������ �� ������� � �� �  ������������ ������������� �������� �� �� ���������. " data-original-title="" title="">
                                    <span>�������</span>
                                    <i class="fa fa-info-circle"></i>
                                </label>
                                <div class="intl-tel-input allow-dropdown">
                                    <div class="flag-container">
                                        <div class="selected-flag" tabindex="0">
                                            <div class="iti-flag gb"></div>
                                            <div class="iti-arrow"></div>
                                        </div>
                                    </div>
                                    <input type="tel" maxlength="15" id="payer-phone" name="payer[phone]" value="" autocomplete="off">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="payer-email" class="control-label" data-trigger="hover" data-toggle="popover" data-content="���������� ���� �� �������. ���������� �� ��������� �� ���������� ������,�� ��������� �� ���������� �� ��������� � �� ���������� �� ������ ��� ������� � �� � ������������ �������������." data-original-title="" title="">
                                    <span>EMAIL</span>
                                    <i class="fa fa-info-circle"></i>
                                </label>
                                <input id="email" name="payer[email]" type="text" placeholder="example@example.com" required="">

                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12">
                            <?php require_once __DIR__ .'/../../../web/forms/form_captcha.php';?>
                          </div>
                        </div>
                    <input type='hidden' name='deparature_date' value="<?php #echo $deparature->format('d-m-Y H:i')?>">
                    <input type='hidden' name='total_sum'>
                    <input type='hidden' name='priceId'>
                    <input type='hidden' name='price'>
                    <input type='hidden' name='route_name' value="<?=$from_name . ' '. $to_name?>">
                    <input name="ConfText" type="hidden" id="ConfText" value="����������� �� � ����������!"> 
                    <input name="n" type="hidden" id="n" value="<?php echo("$n"); ?>"> 
                    <input name="SiteID" type="hidden" id="SiteID" value="<?php echo("$SiteID"); ?>">
                    <input name="ToMail" type="hidden" id="ToMail" value="<?=$o_site->_site['EMail']; ?>"> 
                    <input name="conf_page" type="hidden" id="conf_page " value="<?php echo("/page.php?n=$Site->StartPage&SiteID=$SiteID&sent=1"); ?>">
                    <input name="Form" type="hidden" id="Form" value="<?php $FormName = CyrLat("$Site->Name-$row->Name"); echo("$FormName"); ?>">
                    <input name="URL" type="hidden" id="URL" value="http://<?=$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']?>">
                    <div class="verify-hidden">
                        <input name="verify" type="text">
                    </div>
                        <div class="col-md-6">
                            <p></p>
                            <button type="submit" name="Submit" class="btn btn-primary btn-lg" > ����</button>
                        </div>
                    </div>
                
                </fieldset>
            </div>
        </div>
    </div>
</form>

<?php require_once __DIR__.'/../templates/js_templates.php'; ?>

<script>
    jQuery('#submit').on('click', function (e) {
        var button = jQuery(this) // Button that triggered the modal
        const data = button.data();
        jQuery("input[name=price]").val(data.price);
        jQuery("input[name=deparature_date]").val(data.deparature_date);
        jQuery("input[name=priceId]").val(data.priceId);
        jQuery("input[name=route_name]").val(data.route_name);
        jQuery("input[name=total_sum]").val(data.price);
        jQuery("#total").text(data.price + ' ��.');
        jQuery("#myModalLabel").html("���� ����� �� ����: <br>" + data.route_name);
        jQuery("input[name=qty]").on('change', function(el) {
            var input = jQuery(this);
            jQuery("input[name=total_sum]").val(data.price);
            const price = data.price * input.val();
            jQuery("#total").text(price + ' ��.');
        });
    });

var reserve_seat = function(seat) {
     payload = seat.data();
     payload['command'] = 'reserve_ticket';
     jQuery.ajax({
         url: '<?=$o_page->scheme?><?=$_SERVER['HTTP_HOST']?>/forms/gtravel/api.php',
         type: 'get',
         data: payload,
         success: function(resp) {
             if(resp.reserved) {
                 toastr.success('������������ ����o N:' + resp.number);
                 jQuery("#passengers").append(passenger_info(resp));
                 jQuery('[data-toggle="popover"]').popover(); 
                 jQuery('#selected-seats').append('<li id="s'+resp['number']+'"> # ' +resp['number'] + '</li>');
             } else {
                 toastr.success('����������� ����o N:'+resp.number);
                 jQuery('#passengers').children().last().remove();
                 jQuery('#s' + resp['number']).remove();
             }
             if(seat.hasClass('selected')) {
                 seat.removeClass('selected').addClass('available');
             } else {
                 seat.removeClass('available').addClass('selected');
             }
         }, 
         error: function(r) {
             toastr.error(r.responseText);
         }
     });
};

jQuery('.seat').on('click', function(el) {
     reserve_seat(jQuery(this));

     navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;

     if (navigator.vibrate) {
         navigator.vibrate([50]);
     }
});
jQuery(document).ready(function(){
    jQuery('[data-toggle="popover"]').popover(); 

});

function passenger_info(seat){
    const source = jQuery("#passenger-info").html(); 
    var template = Handlebars.compile(source);
    console.log(seat)
    context = {
        n: jQuery("#passengers").find('fieldset').length + 1,
        currency: 'bgn',
        seat_id: seat.id,
        schedule_id: <?=$schedule_id?>,
        number: seat.number,
    };
    return template(context);
}

function update_price(el) {
    var select = jQuery(el);
    
    var option= select.find(':selected');
    var data = option.data()
    if(data.price) {
        price = data.price;
    } else {
        price = '';
    }
    jQuery("#info-price" +select.data('num')).show().text(price);
    jQuery("#passengers" + data.num + "-" + "ticket_price").val(price);
    jQuery("#passengers" + data.num + "-" + "ticket_type").val(option.val());
    var total_price_element = jQuery("#total");
    total_sum = 0;
    jQuery('.pr').each(function(el, v) {
        var pr = jQuery(v).text();
        pr = parseFloat(pr.replace(/\s/g, ''));
        if (isNaN(pr)) {
            pr = parseFloat(0);
        }
        total_sum += pr;
    });
    total_price_element.html(total_sum);
}

function delete_seat_reservation(e, el) {
    e.preventDefault();
    reserve_seat(jQuery(el));
}
</script>

