<?php
use Doctrine\Common\Collections\Criteria;
require_once __DIR__ .'/../../../global/bus_tickets/forms/AddPayer.php';
$payer = new AddPayer($em, $request->request->get('payer', array()));
?>
<style>
a.list-group-item {
    height:auto;
    min-height:220px;
}
a.list-group-item.active small {
    color:#fff;
}
.stars {
    margin:20px auto 1px;    
}
</style>
<?php if(!isset($_GET['pickup1'], $_GET['dropoff1'], $_GET['deparature_date'])): ?>
    <?php return; ?>
<?php endif;?>
<?php
$msg = '';
try  {
    $user_date_obj = new DateTime($_GET['deparature_date']);
    $user_date = $user_date_obj->format('d.m.Y');
} catch (\Exception $e ) {
    $user_date_obj = new DateTime();
    $user_date = $user_date_obj->format('d.m.Y');
}
?>

<?php $route = get_object($get_course(COMPANY_ID, $_GET['pickup1'], $_GET['dropoff1'], $user_date)); ?>
    <div class="row">
        <div class="well">
        <div class="list-group">
<?php
$criteria = 'eq';
if($route instanceof EmptyObject) {
    $route = get_object($get_course_relative(COMPANY_ID, $_GET['pickup1'], $_GET['dropoff1'], $user_date));
    $criteria = 'gt';
    $msg = '�� ���� ���� ���� �������� �������. <br> ���������� ��������� �� �� ���������� ���-������ ����.';
    if($route instanceof EmptyObject or !$route instanceof \Bus\models\Route) {
        $msg = '�� ��������� �� ��� ���� �� ����� �� ����� �������� ������� �������. <br>';
    }
}

include __DIR__.'/../templates/single_route.php';
if($_SERVER['REQUEST_METHOD'] == "POST") {
    try {
        require_once __DIR__ .'/reserve_ticket.php';
    } catch( \Exception $e) {
        $add_js("toastr.error({$e->getMessage()});");
    }
}

?>

