    <div class="advanced-search color-home-search" id="booking">
        <div class="wrap">
            <form role="form" action="#" method="get">
                <!-- Row -->
                <input type='hidden' name='n' value='19373572'>
                <input type='hidden' name='SiteID' value='<?=$o_page->_page['SiteID']?>'>
                <div class="f-row">
                    <div class="form-group datepicker one-fifth">
                        <label for="departure-date">���� � ��� �� ��������</label>
                        <input type="text" id="deparature_date" name="deparature_date" value='<?=date('d.m.Y', time());?>'>
                    </div>
                    <div class="form-group select one-third">
                        <label>�������� ��</label>
						<?php
						$directions = array();
						$pickup1 = '';
						$selected = '';
						if(isset($_GET['pickup1']) && is_numeric($_GET['pickup1'])) {
							$pickup1 = $_GET['pickup1'];
						} 
						foreach($directions_from(COMPANY_ID) as $direction):
							if($direction->getStartStation()->getId() == $pickup1) {
								$selected = 'selected';
							}
							$option = sprintf("<option value=\"%s\" %s>%s</option>", $direction->getStartStation()->getId(), $selected, $direction->getStartStation()->getName());
							if(!isset($directions[$direction->getStartStation()->getCountry()])) {
								$directions[$direction->getStartStation()->getCountry()] = array();
							}
							$directions[$direction->getStartStation()->getCountry()][] = $option;
						endforeach; 

						?>
                            <select id="pickup1" name="pickup1">
                                <option value="">�������� ����� �� ��������</option>
                            <?php foreach($directions as $country=>$dir): ?>
                                <optgroup label="<?=$country?>">
                                <?=implode('', $directions[$country])?>
                                </optgroup>
                            <?php endforeach; ?>
                            </select>
                    </div>
                    <div class="form-group select one-third">
                        <label>���������� �</label>
                            <select id="dropoff1" name="dropoff1">
                                <option value="">�������� ����� �� ����������</option>
                            </select>
                    </div>
                    <div class="form-group search-button">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn large black search-home-button"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                </div>
                <!-- //Row -->
                <!-- Row -->
<!--                <div class="f-row" style="display: none;">
                    <div class="form-group datepicker one-third">
                        <label for="return-date">���� � ��� �� �������</label>
                        <input type="text" class="return-date hasDatepicker" id="return-date" disabled="">
                        <input type="hidden" name="ret" id="ret" disabled="" value="" style="cursor: pointer;">
                    </div>
                    <div class="form-group select one-third">
                        <label>�������� ��</label>
                        <div class="select-pickup">
                            <select id="pickup2" name="p2">
                                <option value="">�������� ����� �� ��������</option>
                                <optgroup label="������� 1">
                                    <option value="1">���� 1</option>
                                    <option value="2">���� 2</option>
                                    <option value="3">���� 3</option>
                                </optgroup>
                                <optgroup label="������� 2">
                                    <option value="4">���� 4</option>
                                    <option value="5">���� 5</option>
                                    <option value="6">���� 6</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group select one-third">
                        <label>���������� �</label>
                        <div class="select-dropoff">
                            <select id="dropoff2" name="d2">
                                <option value="">�������� ����� �� ����������</option>
                                <optgroup label="������� 1">
                                    <option value="1">���� 1</option>
                                    <option value="2">���� 2</option>
                                    <option value="3">���� 3</option>
                                </optgroup>
                                <optgroup label="������� 2">
                                    <option value="4">���� 4</option>
                                    <option value="5">���� 5</option>
                                    <option value="6">���� 6</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>-->
                <!-- Row -->
<!--                <div class="f-row">
                    <div class="form-group spinner">
                        <label for="people">���� ������� <small>(����������� ����)</small>?</label>
                        <input type="number" id="people" name="ppl" min="1" class="uniform-input number" value="">
                    </div>
                    <div class="form-group radios">
                        <div>
                            <div class="radio" id="uniform-return">
                                <span>
                                    <div class="radio" id="uniform-return"><span><input type="radio" name="trip" id="return" value="2"></span>
                                    </div>
                                </span>
                            </div>
                            <label for="return">����������</label>
                        </div>
                        <div>
                            <div class="radio" id="uniform-oneway">
                                <span class="checked">
                                    <div class="radio" id="uniform-oneway"><span class="checked"><input type="radio" name="trip" id="oneway" value="1" checked=""></span>
                                    </div>
                                </span>
                            </div>
                            <label for="oneway">�����������</label>
                        </div>
                    </div>
                    <div class="form-group right">
                        <button type="submit" class="btn large black">������� �� ����������</button>
                    </div>
                </div>-->
                <!--// Row -->
            </form>
        </div>
    </div>
<script>
const el = "#pickup1";

jQuery(document).ready(function() {
    populate_select();
    jQuery(el).on('change', populate_select);
});

function parse_option(el) {
    return '<option value="' +el[0]+ '">' + el[1] + '</option>';
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function populate_select() {
    city_id = jQuery( el + " option:selected" ).val();
    const url = '/forms/gtravel/api.php?command=travel_from_city_filtered&from_city=' + city_id; 
    jQuery.get(url, function(data) {
        var html = '';
        for(country in data) {
            var options = data[country].map(parse_option);
            var options_text = options.reduce(function(carry, item){ return carry+item; });
            html += '<optgroup label="'+ country +'">' + options_text  + '</optiongroup>';
        }


        jQuery("#dropoff1").html(html);
        jQuery('select[id="dropoff1"]').find('option[value="'+getParameterByName('dropoff1')+'"]').attr("selected",true);
    });
}
jQuery("#deparature_date").datepicker({
    minDate: 0, 
    defaultDate: new Date(),
    showButtonPanel : true,
    dateFormat      : "d.m.yy",
    changeMonth: true,
    firstDay: 1,
    changeYear: true,
});


</script>
