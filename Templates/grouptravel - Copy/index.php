<?php
date_default_timezone_set('Europe/Sofia');
setlocale(LC_ALL, 'bg_BG.utf8');
require_once __DIR__ . '/config.php';
use Symfony\Component\HttpFoundation\Request;
$request = Request::createFromGlobals();
$clean_tickets();


include TEMPLATE_DIR . "header.php";
$pTemplate = $o_page->get_pTemplate( $n, 299 );


switch(True){
    case $o_page->_page[ 'SiteID' ] != $o_site->_site[ 'SitesID' ]:
        include TEMPLATE_DIR . "admin.php";
        break; 
    case ($o_page->_page[ 'n' ] == $o_site->_site[ 'StartPage' ] && (!isset($_GET['search_tag']) and !isset($_GET['search']))):
        include TEMPLATE_DIR . "home.php";
        break;
    default:
        echo '<main class="main" role="main">';
        include TEMPLATE_DIR . "main-header.php";
        if ( $pTemplate[ 'pt_url' ] )
            include $pTemplate[ 'pt_url' ];
        else
            include TEMPLATE_DIR . "main.php";
        echo '</main>';
}

include TEMPLATE_DIR . "footer.php";

?>
