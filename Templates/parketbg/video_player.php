<?php
	require_once "lib/media/flowplayer/config.php";
	if(!isset($video_config))
		{
			$video_config = array("v_width"=>"500px", "v_height"=>"375px");
		}
?>
	<script type="text/javascript" src="<?=FLOWPLAYER_LIBRARY_PATH?>"></script>
	<script type="text/javascript" src="<?=FLOWPLAYER_DIR_PLUGINS?>flowplayer.playlist-3.0.8.js"></script>
<?php
if(isset($video_file))
	{
	?>
		<table align="center" id="video_preview" border="0">
		<td class="video_left">
		<td class="content" valign="middle">
			<!-- player container and a splash image (play button) -->
			<a class="player plain" href="<?=$video_file?>"  style="display:block;width:<?=$video_config['v_width']?>;height:<?=$video_config['v_height']?>; padding: 0; margin: 0"  id="player"></a> 
		<td class="video_right">
		</table>
		<script>
			$j(function() {
			
			// setup player without "internal" playlists
			$f("player", "<?=FLOWPLAYER_VERSION_PATH?>", {
		
				plugins:  {
					controls: {
						
						// tooltips configuration
						tooltips: {
							
							// enable english tooltips on all buttons
							buttons: true,
							
							// customized texts for buttons
							play: '�����',
							pause: '�����',
							fullscreen: '��� �����',
							mute: '���� �����'
						},
						
						// background color for all tooltips
						tooltipColor: '#3d4332',
						
						// text color
						tooltipTextColor: '#FFFFFF' 
					}
				}
			})
			
		});
		</script>		
	<?
	}
	else echo mk_output_message("warning", "���� �������� ����� ������� !");
?>