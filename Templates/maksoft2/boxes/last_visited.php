<div class="box" id="last_visited">
    <h3 class="box-title">Последно посетени</h3>
    <div class="box-content">
        <ol>
        <?php
            $last_visited = $o_page->get_pSubpages();
            for($i=0; $i<count($last_visited); $i++)
                {
                    echo "<li>";
                    $o_page->print_pName(true, $last_visited[$i]['n']);
                }
        ?>
        </ol>
    </div>
    <div class="box-footer"></div>
</div>