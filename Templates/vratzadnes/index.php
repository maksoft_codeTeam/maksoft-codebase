<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
        <meta property="fb:app_id" content="1231753050227827" />
        <title><?=$Title?></title>
        <?php
            include("Templates/vratzadnes/configure.php");		
            include("Templates/meta_tags.php");
            $o_site->print_sConfigurations();
        ?>
        <link href="/<?=TEMPLATE_DIR?>layout.css" id="layout-style" rel="stylesheet" type="text/css" />
        <link href="/<?=TEMPLATE_DIR?>assets/custom.css" id="layout-style" rel="stylesheet" type="text/css" />
        <link href="/css/admin_classes.css" rel="stylesheet" type="text/css" />
        <link href="/<?=TEMPLATE_DIR?>base_style.css" id="base-style" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="/<?=TEMPLATE_DIR?>favicon.ico" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    </head>
    <body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/bg_BG/sdk.js#xfbml=1&version=v2.10&appId=1231753050227827";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
        <?php
            $nc_ids_array = array_map(
                function($page){
                    return array_shift($page);
                },
                $o_page->get_pSubpages($o_site->_site['news'], "p.sort_n ASC")
            );
            $townships = $o_page->get_pGroupContent(3464); ?>
            <div id="site_container">
                <div id="page_container">
                    <div id="header"><?php include TEMPLATE_DIR."header.php"?></div>
                        <div class='live' style="position: relative; height: 40px; overflow: hidden;">
                            <ul  id="roadInformation"> </ul>
                        </div>
                    <?php
                        if($o_page->_page['n'] == $o_site->_site['StartPage'])
                            include TEMPLATE_DIR."home-js.php";
                        elseif($o_page->_page['SiteID'] == $o_site->_site['SitesID'] )
                            include TEMPLATE_DIR."main-new.php";
                        else
                            include TEMPLATE_DIR."admin.php"; 
                    ?>
                    <br clear="all">
                    <div id="navbar"><?=$o_page->print_pNavigation();?></div>
                    <div id="footer">
                        <div class="footer-content">
                            <?php include TEMPLATE_DIR . "footer.php"; ?>
                        </div> 
                        <div class="copyrights">
                        ���� <a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>">www.vratzadnes.com</a> � 2013 ������ ����� ��������. ������������ �� ��������� ����� ���� ������� ���������� �� ������������ ����, ���� ������������ �� <a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>">www.vratzadnes.com</a> � ������������. ������ �� ���� ����������� �� ������������� ���������� ���������. 
                        <hr><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="" title="��������, SEO �����������" target="_blank">Netservice</a></div>       
                    </div>
            </div>
        </div>
        <script
          src="https://code.jquery.com/jquery-1.12.4.min.js"
          integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
          crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.min.js" integrity="sha256-1O3BtOwnPyyRzOszK6P+gqaRoXHV6JXj8HkjZmPYhCI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="/Templates/busrent/assets/js/lib/masonry.pkgd.min.js"></script>
        <script type='text/javascript' src="<?=TEMPLATE_DIR?>assets/jquery.easy-ticker.min.js"></script>
        <script type='text/javascript' src="<?=TEMPLATE_DIR?>assets/jquery.easytabs.min.js"></script>
        <script type='text/javascript' src="<?=TEMPLATE_DIR?>assets/custom.js"></script>
        <!-- PUSH NOTIFICATIONS -->
        <script type="text/javascript" src="/modules/web/components/push.js/push.min.js"></script>
        <script type="text/javascript" src="/Templates/vratzadnes/autobahn-js/lib/autobahn.js"></script>
        <!-- // -->
        <script>
            $j(document).ready(function(){
                var url = '/api/?command=newest_pages&SiteID=922&n=13321&limit=10';
                var url2 = '/api/?command=most_visited&SiteID=922&n=13321&limit=10&parent_pages=<?php echo implode(', ', $nc_ids_array);?>';
                appendNews(url, ".myWrapper ul");
                appendNews(url2, ".mostVisited ul");
                $('.myWrapper').easyTicker({
                    visible:10,
                    speed: 3000,
                    interval: 7000
                });

                $('.mostVisited').easyTicker({
                    speed: 3000,
                    interval: 7000
                });
                $('#tab-container').easytabs();
                    $j("input.search_field").click(function(){
                        $j(this).val("");
                        //alert("Test");
                })
/*                var conn = new ab.Session('ws://79.124.31.189:8802',
                    function() {
                        conn.subscribe('<?=$o_page->_site['primary_url'];?>-live', function(topic, data) {
                            // This is where you would add the new article to the DOM (beyond the scope of this tutorial)
                            constructLiveNewsContainer(data);
                        });

                    },
                    function() {
                        console.warn('WebSocket connection closed');
                    },
                    {'skipSubprotocolCheck': true}
                );*/
            });
            var liveNewsStorage = {};
            function constructLiveNewsContainer(article) {
                liveNewsStorage[article.n] = article;
                var articles = liveNewsStorage;
                html = "";
                for (i in articles){
                    var article = articles[i];
                    html += articleJSONtoHTML(article);
                }
                $("#currentReaders").html(html);
            }

            function articleJSONtoHTML(data)
            {
                hot = '   |    <i class="fa fa-calendar" aria-hidden="true"></i>   ' + data.date_added;
                var html = '<div class="post-container">'+
                           '<h3 class="post-title"><a href="page.php?n='+data.n+'&SiteID='+data.SiteID+'">'+data.name+'</a></h3>';
                if(data.img && data.img != "web/images/upload/922/images.jpg"){
                    html+= '<div class="post-thumb">';
                    html+= '<img src="/'+data.img+'" alt="'+data.imageName+'"/></div>';
                }
                html += '<div class="post-content">'+
                           '<p><span><i class="fa fa-eye" aria-hidden="true"></i> ' + data.preview +' ' + hot +'</span></p>' +
                           '<p>'+ trimByWord(data.content) +'</p></div></div>';
                return html;
            }

            get("/api/?command=currentRoadInformation&n=1")
            .then(JSON.parse)
            .then(function(pages){
                for(i=0;i<pages.length;i++){
                    var page = pages[i];
                    $("#roadInformation").append('<li style="display: list-item;"><strong>'+page.date+': ' +page.title+'</strong></li>');
                }
            }).then(function(){
                $(".live").easyTicker({
                    visible: 1,
                    interval: 4500,
                })
            });;

        </script>	
    </body>
</html>

<?php /*?><?php
    require_once "/hosting/maksoft/maksoft/modules/vendor/autoload.php";
    // This is our new stuff
    if($o_page->_page['SecLevel'] == 0 and $o_page->_page['SiteID'] == $o_page->_site['SitesID']){
    $context = new ZMQContext();
    $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
    $socket->connect("tcp://localhost:5555");
    $socket->send(json_encode(array(
                        "preview" => $o_page->_page['preview'],
                        "n" => $o_page->_page['n'],
                        "SiteID" => $o_page->_page["SiteID"],
                        "category"=>$o_page->_site['primary_url']."-live", 
                        "date_added"=>$o_page->_page['date_added'],
                        "imageName" => iconv('cp1251', 'utf8', $o_page->_page['imageName']),
                        "image_name" => iconv('cp1251', 'utf8', $o_page->_page['imageName']),
                        "name"=>iconv('cp1251', 'utf8', $o_page->_page['Name']),
                        "Name"=>iconv('cp1251', 'utf8', $o_page->_page['Name']),
                        "link"=>$o_page->get_pLink(), 
                        "content" => iconv('cp1251', 'utf8', $o_page->_page['textStr']),
                        "image_src" => $o_page->_page['image_src'],
                        "img" => $o_page->_page['image_src']))
            );
    }
?><?php */?>
