<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
	<title><?=$Title?></title>
    <?php
		//include template configurations
		include("Templates/vratzadnes/configure.php");		
    	//include meta tags
		include("Templates/meta_tags.php");
		$o_site->print_sConfigurations();
	?>
    <link href="http://www.maksoft.net/<?=TEMPLATE_DIR?>layout.css" id="layout-style" rel="stylesheet" type="text/css" />
    <link href="http://maksoft.net/css/admin_classes.css" rel="stylesheet" type="text/css" />
    <link href="http://www.maksoft.net/<?=TEMPLATE_DIR?>base_style.css" id="base-style" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="http://www.maksoft.net/<?=TEMPLATE_DIR?>favicon.ico" />
    
    <link rel="stylesheet" href="http://lib.maksoft.net/jquery-ui/themes/base/jquery.ui.all.css">
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
	<script src="http://lib.maksoft.net/jquery-ui/ui/jquery.ui.core.js"></script>
    <script src="http://lib.maksoft.net/jquery-ui/ui/jquery.ui.widget.js"></script>
    <script src="http://lib.maksoft.net/jquery-ui/ui/jquery.ui.tabs.js"></script>
    <script>
		$j(document).ready(function(){
			$j("input.search_field").click(function(){
				$j(this).val("");
				//alert("Test");
				})
			});
	</script>	

</head>

<body>
<?php

	$news_limit = 20;
	
	if($o_page->cache_actual(3)) {
		$news_categories = $o_page->cache_get(3);
	} else {
		$news_categories = $o_page->get_pSubpages($o_site->_site['news'], "p.sort_n ASC");
		$o_page->cache_put($news_categories, 3); 
	}
	
	if($o_page->cache_actual(4)) {
		$news_categories2 = $o_page->cache_get(4);
	} else {
		$news_categories2 = $o_page->get_pSubpages(190806, "p.sort_n ASC");
		$o_page->cache_put($news_categories2, 4); 
	}
        /*
        �������� �� ���� �������������� 
        
        if($o_page->cache_actual(5)) {
		$recent_news = $o_page->cache_get(5);
	} else {
		$recent_news = $o_page->get_pSubpages(196709, "p.sort_n ASC");
		$o_page->cache_put($recent_news, 5); 
	}
      
         ���� �� �������� ���
         *          */

		$nc_ids = "";
		$nc_ids_array = array();
		for($i=0; $i<count($news_categories); $i++)
			{
				if($i>0)
					$nc_ids.=", ";
				$nc_ids.= $news_categories[$i]['n'];
				$nc_ids_array[] = $news_categories[$i]['n'];
			}
	
		for($i=0; $i<count($news_categories2); $i++)
			{
				$nc_ids.=", ";
				$nc_ids.= $news_categories2[$i]['n'];
				$nc_ids_array[] = $news_categories2[$i]['n'];
			}
                        /*
                        �������� �� ���� �������������� - �������� �� ��������� ������ 2015 � ���� ������ right_sidebar
                         *                      
                for ($i=0; $i<count($recent_news) ;$i++ )
                        {
                                $nc_ids.=", ";
                                $nc_ids.=$recent_news[$i]['n'];
                                $nc_ids_array[]=$recent_news[$i]['n'];
                        }
                         * 
                         */

//	if($o_page->cache_actual(5)) {
//		$recent_news = $o_page->cache_get(5);
//	} else {
			//get last added news
			$recent_news = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT ".$news_limit, "p.ParentPage IN (".$nc_ids.")  AND p.imageNo>0");
			//$recent_news = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT ".$news_limit, "p.ParentPage IN (".$nc_ids.") AND LENGTH(p.textStr)>0 AND p.imageNo>0");		
			$o_page->cache_put($recent_news, 5); 
//	}

	
	
	if($o_page->cache_actual(2)) {
		$popular_news = $o_page->cache_get(2); 	
	} else {
		$popular_news = $o_page->get_pSubpages(0, "p.preview DESC LIMIT ".$news_limit, "p.ParentPage IN (".$nc_ids.") AND p.imageNo>0");
		$o_page->cache_put($popular_news, 2); 
	}
	

	if($o_page->cache_actual(1)) {
		$commented_news = $o_page->cache_get(1); 
	} else {
		//get most commented news
		// tazi chast se izpulniava na survura celi 3 sec !!!
	$com_news = $o_page->get_pSubpages(0, "p.sort_n ASC", "p.ParentPage IN (".$nc_ids.")  AND p.imageNo>0");
	function compareComments($a, $b)
	{
	  return $b['page_comments'] - $a['page_comments'];
	}

	usort($com_news, 'compareComments');  // 0.3 sec
	for($i=0; $i<$news_limit; $i++)
		$commented_news[] = $com_news[$i];
	
	$o_page->cache_put($commented_news, 1); 	
	}
	
	if($o_page->cache_actual(8)) {
		$townships = $o_page->cache_get(8); 
	} else {
		//townships 1.1 s
		$townships = $o_page->get_pSubpages(190806, "p.sort_n ASC");
		$o_page->cache_put($townships, 8); 
	}
	

?>
<div id="site_container">
	<div id="page_container">
        <div id="header"><?php include TEMPLATE_DIR."header.php"?></div>
        <?php
			//if($o_page->_page['n'] == $o_site->_site['StartPage'] || $o_page->_page['n'] == $o_site->_site['news'])
			if($o_page->_page['n'] == $o_site->_site['StartPage'])
				include TEMPLATE_DIR."home.php";
			elseif($o_page->_page['SiteID'] == $o_site->_site['SitesID'] )
				include TEMPLATE_DIR."main.php";
			else
				include TEMPLATE_DIR."admin.php";
				
		?>
        <br clear="all">
        <div id="navbar"><?=$o_page->print_pNavigation();?></div>

    <div id="footer">
    	<div class="footer-content">
		<?php
			include TEMPLATE_DIR . "footer.php";
		?>
        </div> 
        <div class="copyrights">
		���� <a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>">www.vratzadnes.com</a> � 2013 ������ ����� ��������. ������������ �� ��������� ����� ���� ������� ���������� �� ������������ ����, ���� ������������ �� <a href="<?=$o_page->get_pLink($o_site->_site['StartPage'])?>">www.vratzadnes.com</a> � ������������. ������ �� ���� ����������� �� ������������� ���������� ���������. 
		<hr><?=$o_site->get_sCopyright() . ", " . date("Y", strtotime($o_page->get_pDate("date_added", $o_site->_site['StartPage'])))." - ".date("Y")?>, support <a href="http://www.maksoft.net" title="��������, SEO �����������" target="_blank">Netservice</a></div>       
    </div>
    </div>
</div>

<!--
<script type="text/javascript">

  var html = '<div style="display: block; margin: 0px; padding: 0px; width:100%;"><div class="sDelimeter"></div>';
  $(".page-content").append(html);

  var page;
  $.each(data.slice(0,50), function(a, page){
        var news ='<div class="sPage" style="float: left; width: 735px"><div class="sPage-content" style="margin: 7.5px; ">';
        news += '<a href="'+page.name.link+'" title="' + page.name.str + '" class="title">';
        news += '<div class="bullet1" style="float:left;"></div><div class="text">'+page.name.str+'</div></a><br style="clear: both;"></div></div><div style="clear: both" class="sPage-separator"></div>';
      $(".page-content").append(news);
   });
});
</script>
-->
</body>
</html>
