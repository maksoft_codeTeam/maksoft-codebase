<?php
    $dd_links = $o_page->get_pSubpages($o_site->_site['StartPage'], $o_page->get_pSubPagesOrder($o_site->_site['StartPage'])." LIMIT 10", "p.toplink = 0");
    if(count($dd_links) > 0)
        {
            ?>
            <link href="<?=ASSETS_DIR?>ddsmoothmenu.css" rel="stylesheet" type="text/css" />
            <script type="text/javascript" src="/lib/menu/ddsmoothmenu/ddsmoothmenu.js"></script>
            <script type="text/javascript">
            ddsmoothmenu.init({
                mainmenuid: "top_menu",
                orientation: 'h',
                classname: 'ddsmoothmenu',
                //customtheme: ["#1c5a80", "#18374a"],
                contentsource: "markup"
            })
            </script>
            <div  class="top-menu-container">
                <div id="top_menu"class="ddsmoothmenu" style="display: block; height: 30px; clear:both;" <?=(defined('CMS_MAX_WIDTH')) ? "style=\"width:".CMS_MAX_WIDTH."px\"" : ""?>>
                    <ul>
                        <?php 
                        $j = 0;
                        $max = count($dd_links);
                        while($dd_links){
                        $page = array_shift($dd_links);
                        ?>
                        <li>
                           <a href="<?=$o_page->get_pLink($page['n'])?>" title="<?=$page['Name'];?>" class="" <?php if($j + 1 != $max) { ?> style="padding-right: 23px;" <? } ?>><?=$page['Name'];?>
                                 <img src="/lib/menu/ddsmoothmenu/down.gif" class="downarrowclass" style="border:0;">
                          </a>

                          <?php if($submenu_items = $o_page->get_pSubpages($page['n'])){ ?>
                          <ul style="top: 25px; visibility: visible; left: 0px; width: 171px; display: none;">
                            <?php $i=0;?>
                            <?php foreach($submenu_items as $s_page){ ?>
                            <?php 
                                if($i===20){ break; }
                            ?>
                            <li>
                                <a href="<?=$o_page->get_pLink($s_page['n']);?>" title="<?=$s_page['Name'];?>" class=""><?=$s_page['Name'];?></a>
                            </li>
                            <?php $i++;?>
                            <?php } ?>
                          </ul>
                        <?php  } ?>
                      </li>
                        <?php $j++; ?>
                       <?php } ?>
                    </ul>
                </div>
            </div>
            <?
        }
?>
