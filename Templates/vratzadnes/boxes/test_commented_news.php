<?php
//top of script: 
ini_set('display_errors', 1); // set to 0 for production version 
error_reporting(E_ALL);
/*
  ����� �� ��������� �� ���������� 20 ������ �� ������� ���������.
 * � ��������� get_pSubpages() ������ ������ �� ������ �� �������� ��������, �� �� ������ �������� � �����������. 
 * ���� ���� � ���� for loop ��������� ������ ����������� � ���� ����������. � ���� ������ ���������� ��
 *  */

//$news55 = $o_page->get_pSubpages(196709, "p.sort_n desc limit 20"); //������
$news_category = $o_page->get_pSubpages(195636, "p.sort_n  desc limit 30"); //������ � ������ ������������
$municipality_category = $o_page->get_pSubpages(190806, "p.sort_n  desc limit 20"); //������
$tourism_category = $o_page->get_pSubpages(199538, "p.sort_n  desc limit 30"); //�������

$list_pages = "";
$topNewsNewest_array = array();

$election_pages = 196709; //������ 2015 � hardcode ������ $newestNews ������ ������������, � �� ���� ������
$list_pages.=$election_pages;

for ($i = 0; $i < count($news_category); $i++) {
    $list_pages.=", ";
    $list_pages.=$news_category[$i]['n'];
    
}
for ($i = 0; $i < count($municipality_category); $i++) {
    $list_pages.=", ";
    $list_pages.=$municipality_category[$i]['n'];
    
}

for ($i = 0; $i < count($tourism_category); $i++) {
    $list_pages.=", ";
    $list_pages.=$tourism_category[$i]['n'];
    
}
?>


<?php
$limit = 1560;
$sorted_news = $o_page->get_pSubpages(0, "p.date_added DESC LIMIT " . $limit, "p.ParentPage IN (" . $list_pages . ") AND p.imageNo>0 ");
$array = array();
array_multisort($array, SORT_DESC);
$array = $sorted_news;
usort($array, function ($a, $b) {
    if ($a['page_comments'] == $b['page_comments'])
        return 0;
    return ($a['page_comments'] > $b['page_comments']) ? -1 : 1;
});
$i=0;
foreach ($array as $value) {
    if ($value['page_comments']>0){
    ?>

    <div class="news_preview">
        <div class="image"><a href="<?= $value['page_link'] ?>"><?= $o_page->print_pImage(80, "", "", $value['n'], "strict") ?></a></div>
        <h2 class="title"><a href="<?= $value['page_link'] ?>"><?= $value['title'] ?></a></h2>
        <span class="label"><?= $o_page->get_pName($value['ParentPage']) ?></span>&nbsp;<span class="counter-visits"><?= $value['preview'] ?></span><?= ( $value['page_comments'] > 0) ? "<span class=\"counter-comments\">" . $value['page_comments'] . "</span>&nbsp;" : "" ?>
        <br clear="all">
        <p class="text"><?= cut_text(strip_tags($value['textStr']), 150); ?></p>
    </div>

    <?php
    }
    $i++;
if($i==20)    break;
}