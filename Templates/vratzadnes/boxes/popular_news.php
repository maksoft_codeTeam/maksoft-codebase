<table id="box-popular-news" cellpadding="0" cellspacing="11" border="0">
<caption><?=$news_caption?></caption>
<tr>
<?php
	//get the recent news list
	if(count($list_news)>5)
		$counter = 5;
	else $counter = count($list_news);
		
	for($i=0; $i<$counter; $i++)
	{
?>
	<td class="news_preview" valign="top">
        <div class="title"><a href="<?=$o_page->get_pLink($list_news[$i]['n'])?>"><?=cut_text($list_news[$i]['title'], 30)?></a></div>
        <span class="label"><?=$o_page->get_pName($list_news[$i]['ParentPage'])?></span>
        <div class="image"><a href="<?=$o_page->get_pLink($list_news[$i]['n'])?>"><?=$o_page->print_pImage(160, "", "", $list_news[$i]['n'], "strict")?></a></div>
        <p class="text"><a href="<?=$o_page->get_pLink($list_news[$i]['n'])?>"><?=cut_text(strip_tags($list_news[$i]['textStr']))?></a></p>
    </td>
    <?php
	}
?>
</table>