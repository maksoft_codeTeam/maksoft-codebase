<?php
	for($i=0; $i<count($list_news); $i++)
	{
?>
	<div class="news_preview">
        <div class="image"><a href="<?=$list_news[$i]['page_link']?>"><?=$o_page->print_pImage(80, "", "", $list_news[$i]['n'], "strict")?></a></div>
        <h2 class="title"><a href="<?=$list_news[$i]['page_link']?>"><?=$list_news[$i]['title']?></a></h2>
        <span class="label"><?=$o_page->get_pName($list_news[$i]['ParentPage'])?></span>&nbsp;<span class="counter-visits"><?=$list_news[$i]['preview']?></span><?=($list_news[$i]['page_comments']>0) ? "<span class=\"counter-comments\">".$list_news[$i]['page_comments']."</span>&nbsp;":""?>
        <br clear="all">
        <p class="text"><?=cut_text(strip_tags($list_news[$i]['textStr']), 150);?></p>
    </div>
    <?php
	}
?>