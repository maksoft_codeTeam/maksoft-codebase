<section id="content-section-1">
	<div class="gdlr-full-size-wrapper gdlr-show-all" style="padding-bottom: 0px;  background-color: #ffffff;">
		<div class="gdlr-master-slider-item gdlr-slider-item gdlr-item" style="margin-bottom: 0px;">
			<!-- MasterSlider -->
			<div id="P_MS5628cf5090bb7" class="master-slider-parent ms-parent-id-1">


				<!-- MasterSlider Main -->
				<div id="MS5628cf5090bb7" class="master-slider ms-skin-default">

					<div class="ms-slide" data-delay="7" data-fill-mode="fill">
						<img src="<?=TEMPLATE_PATH?>assets/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="<?=TEMPLATE_PATH?>img/slides/basein-slider.jpg"/>

						<div class="ms-layer  msp-cn-5-10" style="" data-duration="862" data-ease="easeOutQuint" data-offset-x="3" data-offset-y="-122" data-origin="mc" data-position="normal">
							&#1044;&#1086;&#1073;&#1088;&#1077; &#1076;&#1086;&#1096;&#1083;&#1080;</div>

						<div class="ms-layer  msp-cn-5-12" style="width:48px;height:2px;" data-duration="775" data-delay="525" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="-57" data-origin="mc" data-position="normal">
						</div>

						<div class="ms-layer  msp-cn-5-11" style="" data-effect="t(true,n,n,-500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="712" data-delay="975" data-ease="easeOutQuint" data-offset-x="4" data-offset-y="19" data-origin="mc" data-position="normal">
							EINSTEIN HOUSE &amp; SPA</div>

						<div class="ms-layer  msp-cn-1-13" style="" data-effect="t(true,n,n,500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="750" data-delay="1462" data-ease="easeOutQuint" data-offset-x="5" data-offset-y="113" data-origin="mc" data-position="normal">
							&#1055;&#1086;&#1078;&#1077;&#1083;&#1072;&#1074;&#1072;&#1084;&#1077; &#1042;&#1080; &#1087;&#1088;&#1080;&#1103;&#1090;&#1085;&#1072; &#1087;&#1086;&#1095;&#1080;&#1074;&#1082;&#1072;</div>


					</div>

					<div class="ms-slide" data-delay="7" data-fill-mode="fill">
						<img src="<?=TEMPLATE_PATH?>assets/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="<?=TEMPLATE_PATH?>img/slides/slider-summer.jpg"/>

						<div class="ms-layer  msp-cn-5-10" style="" data-duration="862" data-ease="easeOutQuint" data-offset-x="3" data-offset-y="-122" data-origin="mc" data-position="normal">
							&#1044;&#1086;&#1073;&#1088;&#1077; &#1076;&#1086;&#1096;&#1083;&#1080;</div>

						<div class="ms-layer  msp-cn-5-12" style="width:48px;height:2px;" data-duration="775" data-delay="525" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="-57" data-origin="mc" data-position="normal">
						</div>

						<div class="ms-layer  msp-cn-5-11" style="" data-effect="t(true,n,n,-500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="712" data-delay="975" data-ease="easeOutQuint" data-offset-x="4" data-offset-y="19" data-origin="mc" data-position="normal">
							EINSTEIN HOUSE &amp; SPA</div>

						<div class="ms-layer  msp-cn-1-13" style="" data-effect="t(true,n,n,500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="750" data-delay="1462" data-ease="easeOutQuint" data-offset-x="5" data-offset-y="113" data-origin="mc" data-position="normal">
							&#1055;&#1086;&#1078;&#1077;&#1083;&#1072;&#1074;&#1072;&#1084;&#1077; &#1042;&#1080; &#1087;&#1088;&#1080;&#1103;&#1090;&#1085;&#1072; &#1087;&#1086;&#1095;&#1080;&#1074;&#1082;&#1072;</div>


					</div>

					<div class="ms-slide" data-delay="7" data-fill-mode="fill">
						<img src="<?=TEMPLATE_PATH?>assets/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="<?=TEMPLATE_PATH?>img/slides/slider.jpg"/>

						<div class="ms-layer  msp-cn-5-10" style="" data-duration="862" data-ease="easeOutQuint" data-offset-x="3" data-offset-y="-122" data-origin="mc" data-position="normal">
							&#1044;&#1086;&#1073;&#1088;&#1077; &#1076;&#1086;&#1096;&#1083;&#1080;</div>

						<div class="ms-layer  msp-cn-5-12" style="width:48px;height:2px;" data-duration="775" data-delay="525" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="-57" data-origin="mc" data-position="normal">
						</div>

						<div class="ms-layer  msp-cn-5-11" style="" data-effect="t(true,n,n,-500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="712" data-delay="975" data-ease="easeOutQuint" data-offset-x="4" data-offset-y="19" data-origin="mc" data-position="normal">
							EINSTEIN HOUSE &amp; SPA</div>

						<div class="ms-layer  msp-cn-1-13" style="" data-effect="t(true,n,n,500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="750" data-delay="1462" data-ease="easeOutQuint" data-offset-x="5" data-offset-y="113" data-origin="mc" data-position="normal">
							&#1055;&#1086;&#1078;&#1077;&#1083;&#1072;&#1074;&#1072;&#1084;&#1077; &#1042;&#1080; &#1087;&#1088;&#1080;&#1103;&#1090;&#1085;&#1072; &#1087;&#1086;&#1095;&#1080;&#1074;&#1082;&#1072;</div>


					</div>
					<div class="ms-slide" data-delay="7" data-fill-mode="fill">
						<img src="<?=TEMPLATE_PATH?>assets/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="<?=TEMPLATE_PATH?>img/slides/slider-2.jpg"/>

						<div class="ms-layer  msp-cn-1-1" style="" data-duration="862" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="-117" data-origin="mc" data-position="normal">
							&#1044;&#1086;&#1073;&#1088;&#1077; &#1076;&#1086;&#1096;&#1083;&#1080;</div>

						<div class="ms-layer  msp-cn-1-3" style="width:48px;height:2px;" data-duration="775" data-delay="525" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="-61" data-origin="mc" data-position="normal">
						</div>

						<div class="ms-layer  msp-cn-1-2" style="" data-effect="t(true,n,n,-500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="712" data-delay="975" data-ease="easeOutQuint" data-offset-x="10" data-offset-y="7" data-origin="mc" data-position="normal">
							EINSTEIN HOUSE &amp; SPA</div>

						<div class="ms-layer  msp-cn-1-4" style="" data-effect="t(true,n,n,500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="750" data-delay="1462" data-ease="easeOutQuint" data-offset-x="1" data-offset-y="95" data-origin="mc" data-position="normal">
							&#1055;&#1086;&#1078;&#1077;&#1083;&#1072;&#1074;&#1072;&#1084;&#1077; &#1042;&#1080; &#1087;&#1088;&#1080;&#1103;&#1090;&#1085;&#1072; &#1087;&#1086;&#1095;&#1080;&#1074;&#1082;&#1072;</div>


					</div>

					<div class="ms-slide" data-delay="7" data-fill-mode="fill">
						<img src="<?=TEMPLATE_PATH?>assets/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="<?=TEMPLATE_PATH?>img/slides/slider-3.jpg"/>

						<div class="ms-layer  msp-cn-1-1" style="" data-duration="862" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="-117" data-origin="mc" data-position="normal">
							&#1044;&#1086;&#1073;&#1088;&#1077; &#1076;&#1086;&#1096;&#1083;&#1080;</div>

						<div class="ms-layer  msp-cn-1-3" style="width:48px;height:2px;" data-duration="775" data-delay="525" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="-61" data-origin="mc" data-position="normal">
						</div>

						<div class="ms-layer  msp-cn-1-2" style="" data-effect="t(true,n,n,-500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="712" data-delay="975" data-ease="easeOutQuint" data-offset-x="10" data-offset-y="7" data-origin="mc" data-position="normal">
							EINSTEIN HOUSE &amp; SPA</div>

						<div class="ms-layer  msp-cn-1-4" style="" data-effect="t(true,n,n,500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="750" data-delay="1462" data-ease="easeOutQuint" data-offset-x="1" data-offset-y="95" data-origin="mc" data-position="normal">
							&#1055;&#1086;&#1078;&#1077;&#1083;&#1072;&#1074;&#1072;&#1084;&#1077; &#1042;&#1080; &#1087;&#1088;&#1080;&#1103;&#1090;&#1085;&#1072; &#1087;&#1086;&#1095;&#1080;&#1074;&#1082;&#1072;</div>


					</div>

					<div class="ms-slide" data-delay="7" data-fill-mode="fill">
						<img src="<?=TEMPLATE_PATH?>assets/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="<?=TEMPLATE_PATH?>img/slides/slider-4.jpg"/>

						<div class="ms-layer  msp-cn-1-1" style="" data-duration="862" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="-117" data-origin="mc" data-position="normal">
							&#1044;&#1086;&#1073;&#1088;&#1077; &#1076;&#1086;&#1096;&#1083;&#1080;</div>

						<div class="ms-layer  msp-cn-1-3" style="width:48px;height:2px;" data-duration="775" data-delay="525" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="-61" data-origin="mc" data-position="normal">
						</div>

						<div class="ms-layer  msp-cn-1-2" style="" data-effect="t(true,n,n,-500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="712" data-delay="975" data-ease="easeOutQuint" data-offset-x="10" data-offset-y="7" data-origin="mc" data-position="normal">
							EINSTEIN HOUSE &amp; SPA</div>

						<div class="ms-layer  msp-cn-1-4" style="" data-effect="t(true,n,n,500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="750" data-delay="1462" data-ease="easeOutQuint" data-offset-x="1" data-offset-y="95" data-origin="mc" data-position="normal">
							&#1055;&#1086;&#1078;&#1077;&#1083;&#1072;&#1074;&#1072;&#1084;&#1077; &#1042;&#1080; &#1087;&#1088;&#1080;&#1103;&#1090;&#1085;&#1072; &#1087;&#1086;&#1095;&#1080;&#1074;&#1082;&#1072;</div>


					</div>

				</div>
				<!-- END MasterSlider Main -->


			</div>
			<!-- END MasterSlider -->

			<script>
				( function ( $ ) {
					"use strict";

					$( function () {
						var masterslider_0bb7 = new MasterSlider();

						// slider controls
						masterslider_0bb7.control( 'arrows', {
							autohide: true,
							overVideo: true
						} );
						masterslider_0bb7.control( 'bullets', {
							autohide: false,
							overVideo: true,
							dir: 'h',
							align: 'bottom',
							space: 8,
							margin: 25
						} );
						// slider setup
						masterslider_0bb7.setup( "MS5628cf5090bb7", {
							width: 1140,
							height: 580,
							minHeight: 0,
							space: 0,
							start: 1,
							grabCursor: true,
							swipe: false,
							mouse: true,
							keyboard: false,
							layout: "fullwidth",
							wheel: false,
							autoplay: true,
							instantStartLayers: false,
							loop: true,
							shuffle: false,
							preload: 0,
							heightLimit: true,
							autoHeight: false,
							smoothHeight: true,
							endPause: false,
							overPause: true,
							fillMode: "fill",
							centerControls: true,
							startOnAppear: false,
							layersMode: "center",
							autofillTarget: "",
							hideLayers: false,
							fullscreenMargin: 0,
							speed: 20,
							dir: "h",
							parallaxMode: 'swipe',
							view: "fadeFlow"
						} );


						MSScrollParallax.setup( masterslider_0bb7, 30, 50, true );
						$( "head" ).append( "<link rel='stylesheet' id='ms-fonts'  href='//fonts.googleapis.com/css?family=Open+Sans:300,700|Merriweather:regular' type='text/css' media='all' />" );

						window.masterslider_instances = window.masterslider_instances || [];
						window.masterslider_instances.push( masterslider_0bb7 );
					} );

				} )( jQuery );
			</script>

		</div>
		<div class="clear"></div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</section>