<?php
include TEMPLATE_DIR . "boxes/slider.php";
?>
<section id="content-section-2">
	<div class="gdlr-color-wrapper  gdlr-show-all no-skin" style="background-color: #ffffff; ">
		<div class="container">
			<?php
			if ( $user->AccessLevel > 0 ) {
				?>
			<h5>
				<?="".$nav_bar.""?>
			</h5>
			<?php
			}
			?>
			<div class="gdlr-item-title-wrapper gdlr-item pos-center gdlr-nav-container ">
				<div class="gdlr-item-title-head">
					<h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border"><?=$o_page->get_pName(19326054)?> � ��� ����� �������</h3>
					<div class="gdlr-item-title-carousel"><i class="icon-angle-left gdlr-flex-prev"></i><i class="icon-angle-right gdlr-flex-next"></i>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="room-item-wrapper type-classic">
				<div class="room-item-holder">
					<div class="gdlr-room-carousel-item gdlr-item">
						<div class="flexslider" data-type="carousel" data-nav-container="room-item-wrapper" data-columns="4">
							<ul class="slides">


								<?php 
                    $ourrooms = $o_page->get_pGroupContent($n_ourrooms);
					
						for($i=0; $i<count($ourrooms); $i++)
							{
								?>
								<li class="gdlr-item gdlr-classic-room">
									<div class="gdlr-room-thumbnail">
										<a href="<?=$o_page->get_pLink($ourrooms[$i]['n'])?>" data-rel="fancybox">
                                        <img src="/img_preview.php?image_file=<?=$ourrooms[$i]['image_src']?>&img_width=150&ratio=exact" alt="<?=$o_page->get_pName($ourrooms[$i]['n'])?>" width="150" height="150" >
                                        </a>
									</div>
									<h3 class="gdlr-room-title"><a href="<?=$o_page->get_pLink($ourrooms[$i]['n'])?>" alt="<?=$o_page->get_pName($ourrooms[$i]['n'])?>"><?=$o_page->get_pName($ourrooms[$i]['n'])?></a></h3>
									<a class="gdlr-button with-border" href="<?=$o_page->get_pLink($ourrooms[$i]['n'])?>">�������<i class="fa fa-long-arrow-right icon-long-arrow-right"></i></a>
								</li>

								<?							
							}
					?>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</section>

<section>
	<div class="container">


	<?php
	$o_page->print_pContent();
	eval( $o_page->get_pInfo( "PHPcode" ) );
	$cms_args = array( "CMS_MAIN_WIDTH" => "100%" );
	$o_page->print_pSubContent();

	//print php code
	if ( $user->AccessLevel >= $row->SecLevel && $row->PageURL ) {
		include_once( "$row->PageURL" );
	}
	?>

	
	</div>
	<div class="clear"></div>
</section>

<section id="content-section-3">
	<div class="gdlr-parallax-wrapper gdlr-background-image gdlr-show-all gdlr-skin-dark-skin" id="gdlr-parallax-wrapper-1" data-bgspeed="0.1" style="background-image: url('<?=TEMPLATE_PATH?>img/slides/slider-31.jpg'); padding-top: 135px; padding-bottom: 80px; ">
		<div class="container">
			<div class="six columns">
				<div class="gdlr-item gdlr-service-with-image-item gdlr-left">
					<div class="service-with-image-thumbnail gdlr-skin-box"><img src="<?=TEMPLATE_PATH?>img/slides/slider-1-400x400.jpg" alt="<?=$o_page->get_pName(19326054)?>" width="400" height="400"/>
					</div>
					<div class="service-with-image-content-wrapper">
						<h3 class="service-with-image-title"><a href="<?=$o_page->get_pLink(19326054)?>" alt="<?=$o_page->get_pName(19326054)?>"><?=$o_page->get_pName(19326054)?></a></h3>
						<div class="service-with-image-content">
							<div class="desc_wrapper">
								<div class="desc">
									<?=cut_text(strip_tags($o_page->get_pText(19326054)), 230)?><a href="<?=$o_page->get_pLink(19326054)?>" class="excerpt-read-more" alt="<?=$o_page->get_pName(19326054)?>">������� ���</a>
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="six columns">
				<div class="gdlr-item gdlr-service-with-image-item gdlr-left">
					<div class="service-with-image-thumbnail gdlr-skin-box"><img src="<?=TEMPLATE_PATH?>img/slides/slider-31-400x400.jpg" alt="<?=$o_page->get_pName(19326052)?>" width="400" height="400"/>
					</div>
					<div class="service-with-image-content-wrapper">
						<h3 class="service-with-image-title"><a href="<?=$o_page->get_pLink(19326052)?>" alt="<?=$o_page->get_pName(19326052)?>"><?=$o_page->get_pName(19326052)?></a></h3>
						<div class="service-with-image-content">
							<div class="desc_wrapper">
								<div class="desc">
									<?=cut_text(strip_tags($o_page->get_pText(19326052)), 230)?><a href="<?=$o_page->get_pLink(19326052)?>" class="excerpt-read-more" alt="<?=$o_page->get_pName(19326052)?>">������� ���</a>
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
</section>


<section id="content-section-4">
	<div class="gdlr-color-wrapper  gdlr-show-all no-skin" style="background-color: #ffffff; padding-top: 70px; ">
		<div class="container">
			<div class="eight columns">
				<div class="gdlr-item-title-wrapper gdlr-item pos-left pos-left-divider ">
					<div class="gdlr-item-title-head">
						<h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border"><?=$o_page->get_pName(19326472)?></h3>
						<div class="clear"></div>
					</div>
					<div class="gdlr-item-title-divider"></div>
				</div>
				<div class="blog-item-wrapper">
					<div class="blog-item-holder">

						<?php 
                    $promo = $o_page->get_pGroupContent($n_promogroup);
					
						for($i=0; $i<count($promo); $i++)
							{
								?>
						<div class="gdlr-item gdlr-blog-medium">
							<div class="gdlr-ux gdlr-blog-medium-ux">
								<article id="post-852" class="post-852 post type-post status-publish format-standard has-post-thumbnail hentry category-fit-row tag-blog tag-life-style">
									<div class="gdlr-standard-style">
										<div class="gdlr-blog-thumbnail">
											<a href="<?=$o_page->get_pLink($promo[$i]['n'])?>" alt="<?=$o_page->get_pName($promo[$i]['n'])?>">
                                        <img src="<?=HTTP_SERVER . "/".$promo[$i]['image_src']?>" alt="<?=$o_page->get_pName($promo[$i]['n'])?>" width="700" height="400" />
                                        </a>
										</div>

										<div class="gdlr-blog-content-wrapper">
											<header class="post-header">
												<h3 class="gdlr-blog-title"><a href="<?=$o_page->get_pLink($promo[$i]['n'])?>" alt="<?=$o_page->get_pName($promo[$i]['n'])?>"><?=$o_page->get_pName($promo[$i]['n'])?></a></h3>

												<div class="clear"></div>
											</header>

											<div class="gdlr-blog-content">
												<!--�����-->
												<div class="clear"></div><a href="<?=$o_page->get_pLink($promo[$i]['n'])?>" alt="<?=$o_page->get_pName($promo[$i]['n'])?>" class="excerpt-read-more">��� ������ ����������<i class="fa fa-long-arrow-right icon-long-arrow-right"></i></a>
											</div>
										</div>
										<div class="clear"></div>
									</div>
								</article>
							</div>
						</div>

						<?							
							}
					?>

					</div>
				</div>
			</div>
			<div class="four columns">

				<div class="gdlr-item-title-wrapper gdlr-item pos-left pos-left-divider ">
					<div class="gdlr-item-title-head">
						<h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border"><?=$o_page->get_pName(19326057)?></h3>
						<div class="clear"></div>
					</div>
					<div class="gdlr-item-title-divider"></div>
				</div>
				<div class="gdlr-gallery-item gdlr-item">

					<?php
					$gr_gallery = $o_page->get_pGroupContent( $n_gr_gallery );

					$counter = 0;


					//$gallery = $o_page->get_pSubpages($categories[$j]['n'], "p.sort_n ASC", "im.image_src !=''");
					$gr_gallery = $o_page->get_pGroupContent( $n_gr_gallery, '', 'RAND()' );
					for ( $i = 0; $i < count( $gr_gallery ); $i++ ) {
						if ( $counter < 1 ) {
							?>

					<div class="gallery-column eight columns">
						<div class="gallery-item"><a href="<?=HTTP_SERVER . "/ ".$gr_gallery[$i]['image_src']?>" data-fancybox-group="gdlr-gal-1" data-rel="fancybox" alt="<?=$o_page->get_pName($gr_gallery[$i]['n'])?>"><img src="/img_preview.php?image_file=<?=$gr_gallery[$i]['image_src']?>&img_width=400&ratio=exact" alt="<?=$o_page->get_pName($gr_gallery[$i]['n'])?>" width="400" height="400" /></a>
						</div>
					</div>
					<?
                                                }
                                                $counter++;	
                                            }

                                    ?>


					<?php
					$gallery = $o_page->get_pSubpages( $n_gallery );

					$counter = 0;


					//$gallery = $o_page->get_pSubpages($categories[$j]['n'], "p.sort_n ASC", "im.image_src !=''");
					$gallery = $o_page->get_pSubpages( $n_gallery, '', 'RAND()' );
					for ( $i = 0; $i < count( $gallery ); $i++ ) {
						if ( $counter < 8 ) {
							?>
					<div class="gallery-column four columns">
						<div class="gallery-item"><a href="<?=HTTP_SERVER . "/ ".$gallery[$i]['image_src']?>" data-fancybox-group="gdlr-gal-1" alt="<?=$o_page->get_pName($gallery[$i]['n'])?>" data-rel="fancybox"><img src="/img_preview.php?image_file=<?=$gallery[$i]['image_src']?>&img_width=400&ratio=exact" alt="<?=$o_page->get_pName($gallery[$i]['n'])?>" width="400" height="400" /></a>
						</div>
					</div>
					<?
                                                }
                                                $counter++;	
                                            }

                                    ?>

					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
</section>

