<!DOCTYPE html>
<html lang="bg-BG">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251"/>
	<meta name="viewport" content="initial-scale=1.0"/>
	<title>
		<?=$Title?>
	</title>
	<?php
	//include meta tags
	include( "Templates/meta_tags.php" );
	$o_site->print_sConfigurations();
	?>

	<link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/plugins/gdlr-hotel/gdlr-hoteld19b.css?ver=4.2.5' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/css/style.css' type='text/css' media='all'/>
	<link rel='stylesheet' id='Open-Sans-google-font-css' href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&amp;subset=greek%2Ccyrillic-ext%2Ccyrillic%2Clatin%2Clatin-ext%2Cvietnamese%2Cgreek-ext&amp;ver=4.2.5' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/plugins/superfish/css/superfishd19b.css?ver=4.2.5' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/plugins/dl-menu/componentd19b.css?ver=4.2.5' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/plugins/font-awesome-new/css/font-awesome.mind19b.css?ver=4.2.5' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/plugins/fancybox/jquery.fancyboxd19b.css?ver=4.2.5' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/plugins/flexslider/flexsliderd19b.css?ver=4.2.5' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/css/style-responsive.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/css/style-custom.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/plugins/masterslider/public/assets/css/masterslider.main9066.css?ver=2.14.2' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?=TEMPLATE_PATH?>assets/css/custom.css' type='text/css' media='all'/>
	<script type='text/javascript' src='https://www.google.com/jsapi?ver=4.2.5'></script>
	<script type='text/javascript' src='<?=TEMPLATE_PATH?>assets/js/jquery/jquery4a80.js?ver=1.11.2'></script>
	<script type='text/javascript' src='<?=TEMPLATE_PATH?>assets/js/jquery/jquery-migrate.min1576.js?ver=1.2.1'></script>

</head>

<body class="home page page-id-3720 page-template-default _masterslider _msp_version_2.14.2  header-style-1">
	<div class="body-wrapper  float-menu gdlr-icon-dark" data-home="http://einstein-house.bg">
		<header class="gdlr-header-wrapper">


			<!-- logo -->
			<div class="gdlr-header-inner">
				<div class="gdlr-header-container container">
					<!-- logo -->
					<div class="gdlr-logo">
						<div class="gdlr-logo-inner">
							<a href="<?=$o_page->get_pLink($o_site->StartPage)?>">
						<img src="<?=TEMPLATE_PATH?>img/logo_einstein_house.png" alt="" width="218" height="67" />						</a>
						
							<div class="gdlr-responsive-navigation dl-menuwrapper" id="gdlr-responsive-navigation"><button class="dl-trigger">Menu</button>

								
																			<ul id="menu-main-menu" class="dl-menu gdlr-main-mobile-menu">
											<li  class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-3720 menu-item-4230menu-item menu-item-type-post_type menu-item-object-page page_item page-item-3720 current_page_item menu-item-4230 gdlr-normal-menu">
				<a href="<?=$o_page->get_pLink($o_site->StartPage)?>">������</a></li>
				
												<?php

if (!isset($mobile_links)) $mobile_links = 2;
$mobile_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $mobile_links");

for ($i = 0; $i < count($mobile_links); $i++) {
    $subpages = $o_page->get_pSubpages($mobile_links[$i]['n']);
?>
<li <?php echo ((count($subpages) > 0) ? "class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4138menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4138 gdlr-normal-menu\"" : "") ?>>
<a href="<?php echo $o_page->get_pLink($mobile_links[$i]['n']) ?>" <?php echo ((count($subpages) > 0) ? "class=\"sf-with-ul-pre\"" : "") ?>>
            <?php echo $mobile_links[$i]['Name'] ?></a><?php echo ((count($subpages) > 0) ? "<ul class=\"dl-submenu\">" : "</li>") ?> <?php
    if (count($subpages) > 0) {


        for ($j = 0; $j < count($subpages); $j++)
            echo "<li class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-4251\"><a href=\"" . $o_page->get_pLink($subpages[$j]['n']) . "\">" . $subpages[$j]['Name'] . "</a></li>";
?>
          </ul></li>
<?php } ?>

<?php 
 }

?>
						</ul>
						
							</div>
						</div>
					</div>

					<!-- navigation -->
					<div class="gdlr-navigation-wrapper">
						<nav class="gdlr-navigation" id="gdlr-main-navigation" role="navigation">
											<ul id="menu-main-menu-1" class="sf-menu gdlr-main-menu">
											<li  class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-3720 menu-item-4230menu-item menu-item-type-post_type menu-item-object-page page_item page-item-3720 current_page_item menu-item-4230 gdlr-normal-menu">
				<a href="<?=$o_page->get_pLink($o_site->StartPage)?>">������</a></li>
				
												<?php

if (!isset($dd_links)) $dd_links = 2;
$dd_links = $o_page->get_pSubpages(0, "p.sort_n", "p.toplink = $dd_links");

for ($i = 0; $i < count($dd_links); $i++) {
    $subpages = $o_page->get_pSubpages($dd_links[$i]['n']);
?>
<li <?php echo ((count($subpages) > 0) ? "class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4138menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4138 gdlr-normal-menu\"" : "") ?>>
<a href="<?php echo $o_page->get_pLink($dd_links[$i]['n']) ?>" <?php echo ((count($subpages) > 0) ? "class=\"sf-with-ul-pre\"" : "") ?>>
            <?php echo $dd_links[$i]['Name'] ?></a><?php echo ((count($subpages) > 0) ? "<ul class=\"sub-menu\">" : "</li>") ?> <?php
    if (count($subpages) > 0) {


        for ($j = 0; $j < count($subpages); $j++)
            echo "<li class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-4251\"><a href=\"" . $o_page->get_pLink($subpages[$j]['n']) . "\">" . $subpages[$j]['Name'] . "</a></li>";
?>
          </ul></li>
<?php } ?>

<?php 
 }

?>
						</ul>
						
						</nav>

						<span class="gdlr-menu-search-button-sep">
<a href="http://translate.google.bg/translate?hl=bg&sl=bg&tl=en&u=http%3A%2F%2Fwww.einstein-house.bg&sandbox=1" alt="EN"><img src="<?=TEMPLATE_PATH?>img/flags/english.png" alt="EN" width="16" /></a>
<a href="http://translate.google.bg/translate?hl=bg&sl=bg&tl=el&u=http%3A%2F%2Fwww.einstein-house.bg&sandbox=1" alt="GR"><img src="<?=TEMPLATE_PATH?>img/flags/greece.png" alt="GR" width="16" /></a>
<a href="http://translate.google.bg/translate?hl=bg&sl=bg&tl=ru&u=http%3A%2F%2Fwww.einstein-house.bg&sandbox=1" alt="RU"><img src="<?=TEMPLATE_PATH?>img/flags/russian.png" alt="RU" width="16" /></a>
</span>
					
						<script>
							jQuery( document ).ready( function () {
								jQuery( '.sf-menu.gdlr-main-menu li' ).has( 'ul' ).addClass( 'menu-item menu-item-type-post_type menu-item-object-page page_item page-item-1122 menu-item-has-children menu-item-4138 gdlr-normal-menu' );
							} )
						</script>

						<div class="gdlr-navigation-gimmick" id="gdlr-navigation-gimmick"></div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</header>
		<div id="gdlr-header-substitute"></div>
		<!-- is search -->
		<div class="content-wrapper">
			<div class="gdlr-content">

				<!-- Above Sidebar Section-->

				<!-- Sidebar With Content Section-->
				<div class="with-sidebar-wrapper">