<?php
	define("TEMPLATE_NAME", "einstein-house");
	define("TEMPLATE_DIR", "Templates/".TEMPLATE_NAME."/");
	define("TEMPLATE_PATH", "/Templates/".TEMPLATE_NAME."/");
	define("TEMPLATE_IMAGES", "http://www.maksoft.net/".TEMPLATE_DIR."img/");

	//define some default values
	$tmpl_config = array(
		"default_logo" => TEMPLATE_IMAGES."logo.png",
		"default_logo_width" =>($o_site->get_sConfigValue('TMPL_LOGO_SIZE'))? $o_site->get_sConfigValue('TMPL_LOGO_SIZE') : "120",
		"default_main_width" => "100%",
		"footer" => array(
								TEMPLATE_DIR . "boxes/news.php"
							),		
		"date_format"=> "d.m.Y"
	);
	//$n_offers_group=230055;
	if(!empty($o_site->_site['Logo']))
		$tmpl_config['default_logo'] = $o_site->get_sImageDIr()."/".basename($o_site->_site['Logo']);
		
	// Our Rooms (Group)
	$n_ourrooms = 803;
	
	// Gallery - Home page
	$n_gallery = 19326057;
	
	// Gallery Group - Home page
	$n_gr_gallery = 804;
	
	// Promo packs
	$n_promo = 19326472;
	
		// Promo packs
	$n_promogroup = 821;
	
		
?>