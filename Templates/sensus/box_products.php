<div id="box_products" class="sliderwrapper">
	<div>
	<div class="contentdiv"><a href="#"><img src="Templates/sensus/photos/photo_001.png" class="png" border="0"></a></div>
	<div class="contentdiv"><a href="#"><img src="Templates/sensus/photos/photo_002.png" class="png" border="0"></a></div>
	<div class="contentdiv"><a href="#"><img src="Templates/sensus/photos/photo_003.png" class="png" border="0"></a></div>
	<div class="contentdiv"><a href="#"><img src="Templates/sensus/photos/photo_004.png" class="png" border="0"></a></div>
	<div class="contentdiv"><a href="#"><img src="Templates/sensus/photos/photo_005.png" class="png" border="0"></a></div>
	</div>
	<div id="paginate-box_products" class="pagination">
	<a href="#" class="toc">1</a> <a href="#" class="toc anotherclass">2</a> <a href="#" class="toc">3</a> <a href="#" class="toc">4</a> <a href="#" class="toc">5</a>
	</div>
</div>
<script type="text/javascript">

featuredcontentslider.init({
	id: "box_products",  //id of main slider DIV
	contentsource: ["inline", ""],  //Valid values: ["inline", ""] or ["ajax", "path_to_file"]
	toc: "markup",  //Valid values: "#increment", "markup", ["label1", "label2", etc]
	nextprev: ["Previous", "Next"],  //labels for "prev" and "next" links. Set to "" to hide.
	revealtype: "click", //Behavior of pagination links to reveal the slides: "click" or "mouseover"
	enablefade: [true, 0.1],  //[true/false, fadedegree]
	autorotate: [true, 3000],  //[true/false, pausetime]
	onChange: function(previndex, curindex){  //event handler fired whenever script changes slide
		//previndex holds index of last slide viewed b4 current (1=1st slide, 2nd=2nd etc)
		//curindex holds index of currently shown slide (1=1st slide, 2nd=2nd etc)
	}
})

</script>         