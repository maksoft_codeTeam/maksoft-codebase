<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

error_reporting(E_DEPRECATED | E_ERROR);
ini_set("display_errors", 1); 

require_once __DIR__.'/modules/vendor/autoload.php';
require_once __DIR__.'/lib/configure.php';
require_once __DIR__.'/lib/languages/bulgarian.php';
require_once __DIR__.'/lib/lib_functions.php'; 
require_once __DIR__.'/lib/i18n.php'; 
require_once __DIR__.'/web/admin/table_class.php';
require_once __DIR__.'/web/admin/db/logOn.inc.php';
require_once __DIR__.'/web/admin/query_set.php';
require_once __DIR__.'/web/admin/io.php';
require_once __DIR__.'/web/resize_image.php';
require_once __DIR__.'/lib/HttpResponseCode.php';


session_start();

$di_container = new \Pimple\Container();
$di_container->register(new \Maksoft\Containers\GlobalContainer());
$services = $di_container;
$response = new Response();
$request = Request::createFromGlobals();

$i18n = new i18n($services);
$i18n->compile();

$o_site = $services['o_site'];
$o_page = $services['o_page'];

$detect = new \Detection\MobileDetect;
