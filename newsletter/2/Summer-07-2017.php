﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="bg">
<head>
<title>Най-поръчваните рекламни артикули през Лято 2017</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Keep your wallet/purse safe with Toppoints Anti skimming card protectors!    </title>
    <style type="text/css" media="screen">
        /* Client-specific Styles */
        #outlook a { /* Force Outlook to provide a "view in browser" menu link. */
            padding:0;
        }
        body { /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
            width:100% !important;
            -webkit-text-size-adjust:100%;
            -ms-text-size-adjust:100%;
            margin:0;
            padding:0;
        }
        .ExternalClass { /* Force Hotmail to display emails at full width */
            width:100%;
            display: block !important; /*Hotmail bugfix*/
        }
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%; /* Force Hotmail to display normal line spacing.*/
        }
        .body {
            margin:0;
            padding:0;
            width:100% !important;
            line-height: 100% !important;
        }
        /* Some sensible defaults for images */
        img {
            outline:none;
            text-decoration:none;
            border:none;
            -ms-interpolation-mode: bicubic;
        }
        a img {
            border:none;
        }
        .image_fix {
            display:block;
        }
        p {
            margin: 0px 0px !important;
        }
        table td {
            border-collapse: collapse;
        }
        table {
            border-collapse:collapse;
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
        }
        a {
            color: #0a8cce;
            text-decoration: none;
        }
        /* Outlook 07, 10 Padding issue fix */
        table td {
            border-collapse: collapse;
        }

        /* Remove spacing around Outlook 07, 10 tables */
        table {
            border-collapse:collapse;
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
        }
        /* Hotmail header color reset */
        @import url(http://fonts.googleapis.com/css?family=Montserrat:700);
        h1, h1 a, h1 a:visited, h1 a:active {
            color: #0092CC !important;
            font-size: 30px !important;
            font-weight: normal !important;
            line-height: 1.4 !important;
            padding: 0 !important;
            margin: 0 !important;
            font-family: 'Montserrat', arial, helvetica, sans-serif !important;
            text-decoration: none !important;
        }
        h2, h2 a, h2 a:visited, h2 a:active {
            color: #0092CC !important;
            font-size: 26px !important;
            font-weight: normal !important;
            line-height: 1.4 !important;
            padding: 0 !important;
            margin: 0 !important;
            font-family: 'Montserrat', arial, helvetica, sans-serif !important;
            text-decoration: none !important;
        }
        h3, h3 a, h3 a:visited, h3 a:active {
            color: #0092CC !important;
            font-size: 20px !important;
            font-weight: normal !important;
            line-height: 1.4 !important;
            padding: 0 !important;
            margin: 0 !important;
            font-family: 'Montserrat', arial, helvetica, sans-serif !important;
            text-decoration: none !important;
        }
        h4, h4 a, h4 a:visited, h4 a:active,
        h5, h5 a, h5 a:visited, h5 a:active,
        .t_cht {
            color: #6A615B !important;
            font-family: arial, helvetica, sans-serif !important;
            text-decoration: none;
        }
        h1 {
            font-size: 16px !important;
            font-weight: normal !important;
            color: #202020 !important;
        }
        #footer h2 {
            font-size: 16px !important;
            color: #261300 !important;
        }
        #help-widget h2 {
            font-size: 20px !important;
            color: #FFF !important;
        }
    </style>
</head>
<body style="font: 14px arial, helvetica, sans-serif; color: #333333; line-height: 1.4; margin: 0; padding: 0; background: #F4F4F4; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
    <table width="100%" cellpadding="0" cellspacing="0" class="hidden-mobile" id="view-online" style="padding: 0; vertical-align: middle; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #CCCCCC; text-align: center;">
    <tbody>
    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
        <td style="font-family: arial, helvetica, sans-serif; color: #FFFFFF; line-height: 1.4; padding: 10px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; font-size: 12px;">
            Ako ne razchitate informaciata molia natisnete TUK? <a href="https://maksoft.net/newsletter/2/Summer-07-2017.php" style="color: #FFFFFF; text-decoration: underline;" title="Read the newsletter online">Прочетете онлайн!</a>
        </td>
    </tr>
    </tbody>
</table><!-- Header -->
<table id="header" class="" width="100%" cellpadding="0" cellspacing="0" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-bottom: 1px solid #E5E5E5; background-color: #FFF;">
    <tbody>
    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
            <table class="devicewidth" width="600" cellpadding="0" cellspacing="0" align="center" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                <tbody>
                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                    <td class="devicewidth-inner logo" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 35px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; width: 207px; padding-top: 38px;">
                        <a href="http://www.toppoint.com/en/home?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=home" title="Ga naar de homepage van Toppoint" style="color: #0a8cce; text-decoration: none;">
                            <img src="http://www.toppoint.com/mail/img/toppoint.jpg" width="207" height="36" alt="Toppoint" style="display: block; line-height: 0; border: none; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"></a>
                    </td>
                    <td class="user-info" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 35px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 36px; text-align: right;">
                        <strong class="user-title" style="font-size: 18px; color: #0092CC;">Maksoft</strong><br>
                        <a href="http://www.toppoint.com/en/extranet/users/account?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=account" title="My profile" style="color: #0a8cce; text-decoration: underline;">My profile</a>
                        <a href="http://www.toppoint.com/en/orders/index?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=orders" title="My orders" style="color: #0a8cce; text-decoration: underline;">My orders</a>
                        <a href="http://www.toppoint.com/en/webshop/offers/index?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=offers" title="My quotes" style="color: #0a8cce; text-decoration: underline;">My quotes</a>
                    </td>
                    <td class="user-icon" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 35px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-left: 10px; width: 50px;">
                        <img src="https://maksoft.net/img_preview.php?image_file=Templates/maksoft/v2/assets/img/logo-admin.png&img_width=25" width="50" height="50" alt="" style="display: block; line-height: 0; border: none; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table><!-- Visual -->
<table width="100%" cellpadding="0" cellspacing="0" class="body" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #F4F4F4; margin: 0; width: 100% !important; line-height: 100% !important;">
    <tbody>
    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
            <table width="600" cellpadding="0" cellspacing="0" align="center" class="devicewidth" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                <tbody>
                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                    <td class="devicewidth-inner" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                        <table width="100%" cellpadding="0" cellspacing="0" class="devicewidth-inner-table" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                            <tbody>
                            <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                <td class="intro-message" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-bottom: 30px; font-size: 16px;">
                                    <p><strong>Уважаеми клиенти и колеги, </strong></p>
                                    <p>&nbsp;</p>
                                    <p>ако смятате, че през това Лято най-поръчваните рекламни материали са добре познатите тениски и шапки- бъркате. Вижте информацията по-долу, която изпращаме на Вашият [EMail] </p>
                                    <p>&nbsp;</p>
                                    <p>Едни от най-продаваните, актуални рекламни артикули в Максофт през Лято 2017 рекламните спинери, силиконовите, all inclusive и tyvek гривни, рекламните джапанки, пластмасовите плажни пепелници, комплектите за плажен тенис, силиконови кутии за цигари и още много продукти налични в нашият сайт <a href="https://www.maksoft.bg">www.maksoft.bg</a> както и във Вашият портал за дистрибутиори и рекламни агенции на адрес <a href="https://ra.maksoft.bg">ra.maksoft.bg</a></p>
                                    <p><br />
                                    <br />Skimming is a common method for criminals to steal bank and identity card information, unfortunately this type of crime is becoming increasingly commonplace. When criminals skim with a simple device they read all of the information from your card just by walking past you! For example, they can easily drain your account or if you have a digital chip in your driving license they access your personal information without you knowing it. Now there's a very simple and easy way to secure your bank and identity cards with the anti-skimming card protectors. This protects your bank and identity cards thus becoming invisible to criminals. This way you do not have to fear unwanted problems and your cards are well protected. The anti-skimming cards have a large print surface and ensure safety and reliability!<br /><br />
                                    <strong>Benefits:</strong><br />- Protects your cards from criminals<br />- All cards fit easily<br />- Same format as bank card or credit card<br />
                                    </p></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- Visual -->
<table width="100%" cellpadding="0" cellspacing="0" class="body" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #F4F4F4; margin: 0; width: 100% !important; line-height: 100% !important;">
    <tbody>
    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
            <table width="600" cellpadding="0" cellspacing="0" align="center" class="devicewidth" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                <tbody>
                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                    <td class="visual" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 40px;">
                                                    <img width="600" alt="" src="https://www.maksoft.bg/img_preview.php?image_file=web/images/upload/maksoft/TheSpinner.jpg&img_width=600" class="visual-image" style="display: block; line-height: 0; border: none; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;">
                                                </td>
                </tr>
                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                    <td class="visual-footer" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                        <img src="http://www.toppoint.com/mail/img/visual-footer.jpg" width="600" height="20" alt="" style="display: block; line-height: 0; border: none; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- Newsletter message -->
<table id="news" class="" width="100%" cellpadding="0" cellspacing="0" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 1px solid #DADADA; border-bottom: 1px solid #DADADA; background-color: #E8E8E8;">
    <tbody>
    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 35px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
            <table class="devicewidth" width="600" cellpadding="0" cellspacing="0" align="center" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                <tbody>
                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                    <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                        <h2 style="color: #0092CC !important; font-size: 26px !important; font-weight: normal !important; line-height: 1.4 !important; padding: 0 !important; margin: 0 !important; font-family: 'Montserrat', arial, helvetica, sans-serif !important; text-decoration: none !important;">Themes and Multimedia</h2><table class="news-wrap" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
    <tbody>
    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 10px;">
            <table class="news-item panel" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: 1px solid #E0E0E0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFF;">
                <tbody>
                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                        <td class="thumbnail" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 20px; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-right: 0;">
                                                <a href="https://www.toppoint.com/en/catalog/product/index/LT91242/10/card-holder-anti-skimming/?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=Card Holder Anti Skimming - LT91242" title="Card Holder Anti Skimming - LT91242" style="color: #0a8cce; text-decoration: none;">
                                                    <img width="260" height="170" alt="" src="http://www.toppoint.com/content/newsletter_messages/495/10_LT91242.jpg" class="visual-image" style="display: block; line-height: 0; border: none; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;">
                                                </a>
                              </td>
                                        <td class="text" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 20px; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                        <h3 style="color: #0092CC !important; font-size: 20px !important; font-weight: normal !important; line-height: 1.4 !important; padding: 0 !important; margin: 0 !important; font-family: 'Montserrat', arial, helvetica, sans-serif !important; text-decoration: none !important;">Card Holder Anti Skimming - LT91242</h3>
                        <p style="margin: 0px 0px !important;">Soft case card holder with RFID protection to prevent skimming.&nbsp;</p>
                                                <table style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                            <tbody>
                                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                    <td class="pt-10" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 10px; width: 278px;">
                                        <div>
                                            <!--[if mso]>
                                            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.toppoint.com/en/catalog/product/index/LT91242/10/card-holder-anti-skimming/?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=Card Holder Anti Skimming - LT91242" style="height:30px;v-text-anchor:middle;width:200px;font-size:15px;" arcsize="6%" stroke="f" fillcolor="#1d8fd5">
                                                <w:anchorlock/>
                                                <center>
                                            <![endif]-->
                                                    <a href="https://www.toppoint.com/en/catalog/product/index/LT91242/10/card-holder-anti-skimming/?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=Card Holder Anti Skimming - LT91242" style="background-color: #1d8fd5; border-radius: 3px; color: #ffffff; display: inline-block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 30px; text-align: center; text-decoration: none; width: 120px; -webkit-text-size-adjust: none;">
                                                        More information                                                    </a>
                                            <!--[if mso]>
                                                </center>
                                            </v:roundrect>
                                            <![endif]-->
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                              </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table><table class="news-wrap" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
    <tbody>
    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 10px;">
            <table class="news-item panel" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: 1px solid #E0E0E0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFF;">
                <tbody>
                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                        <td class="thumbnail" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 20px; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-right: 0;">
                                                <a href="https://www.toppoint.com/en/catalog/product/index/LT91241/10/card-holder-anti-skimming-hard-case/?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=Card Holder Anti Skimming (hard case) - LT91241" title="Card Holder Anti Skimming (hard case) - LT91241" style="color: #0a8cce; text-decoration: none;">
                                                    <img width="260" height="170" alt="" src="http://www.toppoint.com/content/newsletter_messages/496/10_LT91241.jpg" class="visual-image" style="display: block; line-height: 0; border: none; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;">
                                                </a>
                              </td>
                                        <td class="text" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 20px; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                        <h3 style="color: #0092CC !important; font-size: 20px !important; font-weight: normal !important; line-height: 1.4 !important; padding: 0 !important; margin: 0 !important; font-family: 'Montserrat', arial, helvetica, sans-serif !important; text-decoration: none !important;">Card Holder Anti Skimming (hard case) - LT91241</h3>
                        <p style="margin: 0px 0px !important;">Hard case card holder for a debit card. Card holder includes RFID protection to prevent skimming.&nbsp;</p>
                                                <table style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                            <tbody>
                                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                    <td class="pt-10" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 10px; width: 278px;">
                                        <div>
                                            <!--[if mso]>
                                            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.toppoint.com/en/catalog/product/index/LT91241/10/card-holder-anti-skimming-hard-case/?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=Card Holder Anti Skimming (hard case) - LT91241" style="height:30px;v-text-anchor:middle;width:200px;font-size:15px;" arcsize="6%" stroke="f" fillcolor="#1d8fd5">
                                                <w:anchorlock/>
                                                <center>
                                            <![endif]-->
                                                    <a href="https://www.toppoint.com/en/catalog/product/index/LT91241/10/card-holder-anti-skimming-hard-case/?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=Card Holder Anti Skimming (hard case) - LT91241" style="background-color: #1d8fd5; border-radius: 3px; color: #ffffff; display: inline-block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 30px; text-align: center; text-decoration: none; width: 120px; -webkit-text-size-adjust: none;">
                                                        More information                                                    </a>
                                            <!--[if mso]>
                                                </center>
                                            </v:roundrect>
                                            <![endif]-->
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                              </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table><table class="news-wrap" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
    <tbody>
    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 10px;">
            <table class="news-item panel" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: 1px solid #E0E0E0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFF;">
                <tbody>
                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                        <td class="thumbnail" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 20px; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-right: 0;">
                                                <a href="https://www.toppoint.com/en/catalog/product/index/LT91191/10/rfid-card-holder/?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=RFID Card Holder - LT91191" title="RFID Card Holder - LT91191" style="color: #0a8cce; text-decoration: none;">
                                                    <img width="260" height="170" alt="" src="http://www.toppoint.com/content/newsletter_messages/497/10_LT91191.jpg" class="visual-image" style="display: block; line-height: 0; border: none; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;">
                                                </a>
                              </td>
                                        <td class="text" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 20px; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                        <h3 style="color: #0092CC !important; font-size: 20px !important; font-weight: normal !important; line-height: 1.4 !important; padding: 0 !important; margin: 0 !important; font-family: 'Montserrat', arial, helvetica, sans-serif !important; text-decoration: none !important;">RFID Card Holder - LT91191</h3>
                        <p style="margin: 0px 0px !important;">With the RFID technology you can make payments by holding your bank card next to a payment device designed for this purpose.&nbsp;</p>
                                                <table style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                            <tbody>
                                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                    <td class="pt-10" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 10px; width: 278px;">
                                        <div>
                                            <!--[if mso]>
                                            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.toppoint.com/en/catalog/product/index/LT91191/10/rfid-card-holder/?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=RFID Card Holder - LT91191" style="height:30px;v-text-anchor:middle;width:200px;font-size:15px;" arcsize="6%" stroke="f" fillcolor="#1d8fd5">
                                                <w:anchorlock/>
                                                <center>
                                            <![endif]-->
                                                    <a href="https://www.toppoint.com/en/catalog/product/index/LT91191/10/rfid-card-holder/?utm_source=Nieuwsbrief&utm_medium=e-mail&utm_campaign=Keep%2Byour%2Bwallet/purse%2Bsafe%2Bwith%2BToppoints%2BAnti%2Bskimming%2Bcard%2Bprotectors!&utm_content=RFID Card Holder - LT91191" style="background-color: #1d8fd5; border-radius: 3px; color: #ffffff; display: inline-block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 30px; text-align: center; text-decoration: none; width: 120px; -webkit-text-size-adjust: none;">
                                                        More information                                                    </a>
                                            <!--[if mso]>
                                                </center>
                                            </v:roundrect>
                                            <![endif]-->
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                              </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>                            </td>
                  </tr>
              </tbody>
          </table>
      </td>
      </tr>
    </tbody>
</table><!-- Content -->
<table width="100%" cellpadding="0" cellspacing="0" class="body" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #F4F4F4; margin: 0; width: 100% !important; line-height: 100% !important;">
    <tbody>
    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
            <table width="600" cellpadding="0" cellspacing="0" align="center" class="devicewidth" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                <tbody>
                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                    <td class="devicewidth-inner" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                        <table width="100%" cellpadding="0" cellspacing="0" class="devicewidth-inner-table" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                            <tbody>
                            <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                <td class="intro-message" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-bottom: 30px; font-size: 16px;">
                              </td>
                            </tr>
                            <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                <td class="intro-cta" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-bottom: 40px;">
                                    <div>
                                        <!--[if mso]>
                                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://www.toppoint.com" style="height:50px;v-text-anchor:middle;width:200px;font-size:15px;" arcsize="6%" stroke="f" fillcolor="#1d8fd5">
                                            <w:anchorlock/>
                                            <center>
                                        <![endif]-->
                                        <a href="http://www.toppoint.com" style="background-color: #1d8fd5; border-radius: 3px; color: #ffffff; display: inline-block; font-family: sans-serif; font-size: 15px; font-weight: bold; line-height: 50px; text-align: center; text-decoration: none; width: 200px; -webkit-text-size-adjust: none;">
                                            Visit the website                                        </a>
                                        <!--[if mso]>
                                        </center>
                                        </v:roundrect>
                                        <![endif]-->
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table><!-- Advice widget -->
<table id="help" class="" width="100%" cellpadding="0" cellspacing="0" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
    <tbody>
        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
            <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-bottom: 40px;">
                <table class="devicewidth" width="600" cellpadding="0" cellspacing="0" align="center" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                    <tbody>
                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                            <td id="help-widget" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; background-color: #0192CC; border-radius: 3px;">
                                <table width="100%" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                    <tbody>
                                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                            <td class="help-image" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; width: 100px;">
                                                                                                <img src="http://www.toppoint.com/content/employee/48/accountmanager/JuliaDeJong_wit.png" width="94" height="136" alt="" style="display: block; line-height: 0; border: none; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;">
                                          </td>
                                            <td class="help-text" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-left: 20px;">
                                                <table style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                                    <tbody>
                                                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                            <td class="pt-30" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 30px;">
                                                                <h2 style="color: #FFF !important; font-size: 20px !important; font-weight: normal !important; line-height: 1.4 !important; padding: 0 !important; margin: 0 !important; font-family: 'Montserrat', arial, helvetica, sans-serif !important; text-decoration: none !important;">Need help or advice?</h2>
                                                                <p style="margin: 0px 0px !important; color: #FFF; font-size: 14px;">Your back office contact is: Julia de Jong</p>
                                                                                                                                    <p class="phone" style="margin: 0px 0px !important; color: #FFF; font-size: 14px;"><strong>Tel:</strong> +49 (0)5921 8199357</p>
                                                          </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                                                                            <td class="help-cta" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-right: 20px; width: 180px;">
                                                    <table class="cta-table" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                                        <tbody>
                                                            <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                                <td class="pt-30" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 30px; padding-bottom: 5px;">
                                                                    <div>
                                                                        <!--[if mso]>
                                                                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="mailto:julia.de.jong@toppoint.com" style="height:35px;v-text-anchor:middle;width:180px;font-size:14px;" arcsize="6%" stroke="f" fillcolor="#FFF">
                                                                            <w:anchorlock/>
                                                                            <center>
                                                                                <![endif]-->
                                                                                <a href="mailto:julia.de.jong@toppoint.com" style="background-color: #FFF; border-radius: 3px; color: #1d8fd5; display: inline-block; font-family: sans-serif; font-size: 14px; font-weight: normal; line-height: 35px; text-align: center; text-decoration: none; width: 180px; -webkit-text-size-adjust: none;">
                                                                                    Send an email                                                                                </a>
                                                                                <!--[if mso]>
                                                                            </center>
                                                                        </v:roundrect>
                                                                        <![endif]-->
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                      </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<!-- Footer -->
<table id="footer" class="" width="100%" cellpadding="0" cellspacing="0" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
    <tbody>
    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
        <td class="footer-inner" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 35px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
            <table class="devicewidth" width="600" cellpadding="0" cellspacing="0" align="center" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                <tbody>
                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                    <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                        <table width="100%" class="products" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                                                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                        <td class="footer-title" colspan="2" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 0; padding-bottom: 20px;">
                                            <h2 style="color: #261300 !important; font-size: 16px !important; font-weight: normal !important; line-height: 1.4 !important; padding: 0 !important; margin: 0 !important; font-family: 'Montserrat', arial, helvetica, sans-serif !important; text-decoration: none !important;">Toppoint products</h2>
                                        </td>
                                    </tr>                                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                                                        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/51/bags" title="Bags" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Bags</a>
                                            </td>                                            <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/90/designed-by-toppoint" title="Designed by Toppoint" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Designed by Toppoint</a>
                                            </td>
                                        </tr>                                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                                                        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/25/diaries" title="Diaries" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Diaries</a>
                                            </td>                                            <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/18/doming" title="Doming" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Doming</a>
                                            </td>
                                        </tr>                                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                                                        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/52/drinking-eating" title="Drinking & Eating" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Drinking & Eating</a>
                                            </td>                                            <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/28/electronics-mobile-devices" title="Electronics & Mobile devices" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Electronics & Mobile devices</a>
                                            </td>
                                        </tr>                                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                                                        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/16/funny-holland-collection" title="Funny Holland Collection" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Funny Holland Collection</a>
                                            </td>                                            <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/53/give-aways" title="Give aways" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Give aways</a>
                                            </td>
                                        </tr>                                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                                                        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/62/health-lifestyle-events" title="Health, lifestyle & Events" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Health, lifestyle & Events</a>
                                            </td>                                            <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/05/lighters" title="Lighters" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Lighters</a>
                                            </td>
                                        </tr>                                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                                                        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/55/office-business" title="Office & Business" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Office & Business</a>
                                            </td>                                            <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/56/outlet" title="Outlet" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Outlet</a>
                                            </td>
                                        </tr>                                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                                                        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/58/produced-in-europe" title="Produced in Europe" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Produced in Europe</a>
                                            </td>                                            <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/99/selling-tools" title="Selling Tools" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Selling Tools</a>
                                            </td>
                                        </tr>                                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                                                        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/57/sustainability" title="Sustainability" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Sustainability</a>
                                            </td>                                            <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/70/trading" title="Trading" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Trading</a>
                                            </td>
                                        </tr>                                        <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                                                        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 5px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                <a target="_blank" href="http://www.toppoint.com/en/catalog/index/54/writing-instruments" title="Writing Instruments" style="color: #0a8cce; text-decoration: none; font-size: 13px;">Writing Instruments</a>
                                            </td>
                                        </tr>                        </table>
                    </td>
                    <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                        <table class="contact" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                            <tbody>
                                                                    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                        <td class="footer-title" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; padding-top: 0; padding-bottom: 20px;">
                                            <h2 style="color: #261300 !important; font-size: 16px !important; font-weight: normal !important; line-height: 1.4 !important; padding: 0 !important; margin: 0 !important; font-family: 'Montserrat', arial, helvetica, sans-serif !important; text-decoration: none !important;">Address</h2>
                                        </td>
                                    </tr>
                                    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                        <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                            Stationsweg 14a<br>7691 AR  Bergentheim<br>The Netherlands                                        </td>
                                    </tr>
                                                                                                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                    <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                        <table class="social" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                            <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                                                                    <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 20px 10px 0 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                        <a href="http://www.facebook.com/Lensen.Toppoint" target="_blank" title="Follow Toppoint on Facebook" style="color: #0a8cce; text-decoration: none;">
                                                            <img src="http://www.toppoint.com/mail/img/icon-facebook.jpg" width="32" height="32" alt="Follow Toppoint on Facenbool" style="display: block; line-height: 0; border: none; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;">
                                                        </a>
                                                    </td>
                                                                                                                                                    <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 20px 10px 0 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                        <a href="https://twitter.com/lensentoppoint" target="_blank" title="Follow Toppoint on Twitter" style="color: #0a8cce; text-decoration: none;">
                                                            <img src="http://www.toppoint.com/mail/img/icon-twitter.jpg" width="32" height="32" alt="Follow Toppoint on Twitter" style="display: block; line-height: 0; border: none; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;">
                                                        </a>
                                                    </td>
                                                                                                                                                    <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 20px 10px 0 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                                                        <a href="https://nl.linkedin.com/company/lensen-toppoint-bv" target="_blank" title="Follow Toppoint on LinkedIn" style="color: #0a8cce; text-decoration: none;">
                                                            <img src="http://www.toppoint.com/mail/img/icon-linkedin.jpg" width="32" height="32" alt="Follow Toppoint on LinkedIn" style="display: block; line-height: 0; border: none; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;">
                                                        </a>
                                                    </td>
                                                                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
        <td class="footnote" style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
            <table class="devicewidth centered" width="600" cellpadding="0" cellspacing="0" align="center" style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; mso-table-lspace: 0pt; mso-table-rspace: 0pt; text-align: center;">
                <tbody>
                <tr style="padding: 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none;">
                    <td style="font-family: arial, helvetica, sans-serif; color: #333333; line-height: 1.4; padding: 15px 0; vertical-align: top; border-collapse: collapse; border-spacing: 0; border-width: 0; border: none; border-top: 1px solid #DEDEDE; font-size: 12px;">
                        If you don’t want to receive our newsletter you can                                 <a href="http://www.toppoint.com/newsletter/signout'" title="Afmelden" style="color: #0a8cce; text-decoration: none;">sign out</a> here.                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- Footer end -->
</body>
</html>
