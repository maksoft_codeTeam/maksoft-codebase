<?php
require_once __DIR__.'/modules/vendor/autoload.php';
require_once __DIR__.'/modules/vendor/paragonie/random_compat/lib/random.php';
require_once __DIR__.'/lib/lib_site.php';
require_once __DIR__.'/lib/Database.class.php';

session_start();




$o_site = new site();

define('APP_ID', $o_site->get_sConfigValue("FACEBOOK_APP_ID"));

define('APP_SECRET', $o_site->get_sConfigValue("FACEBOOK_APP_SECRET"));


if(!APP_ID or !APP_SECRET){
    exit("you must define app id, app secret");
}

try{
    $db = new Database();
    $auth = new \Maksoft\Gateway\Gateway($db);
} catch (\Exception $e){
    die('database error');
}

if(isset($_GET['c']) and $_GET['c'] === "logout"){
    setcookie('fb', 'false', time()-300);   
    $auth->auth()->deleteAuthUser();
    unset($_SESSION['oauth']);
    unset($_COOKIE['fb']);
    header("Location: ". $_SERVER["HTTP_REFERER"]);
    exit();
}


function generate_salt(){
    try {
        $string = random_bytes(5);
    } catch (TypeError $e) {
        // Well, it's an integer, so this IS unexpected.
        die("An unexpected error has occurred"); 
    } catch (Error $e) {
        // This is also unexpected because 32 is a reasonable integer.
        die("An unexpected error has occurred");
    } catch (Exception $e) {
        // If you get this message, the CSPRNG failed hard.
        die("Could not generate a random string. Is our OS secure?");
    }
    return bin2hex($string);
}

if(!isset($_SESSION['oauth'])){
    $_SESSION['oauth'] = array();
}
$_SESSION['oauth']['logged'] = false;

/*
*   Check if isset facebook auth cookie
*/
if(isset($_COOKIE['fb'])) {
    if(get_me_fields($_COOKIE['fb'])){
        if(!isset($_SESSION['oauth'])){
            $_SESSION['oauth'] = array();
        }
        $_SESSION['oauth']['logged'] = true;
        header("Location: ". $_SERVER["HTTP_REFERER"]);
    }
}

function logged_with_facebook(){
    if(!isset($_COOKIE['fb'])){
        return false;
    }

}


$callback_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$login_dialog_url = "https://www.facebook.com/v2.8/dialog/oauth?client_id=".APP_ID."&display=popup&scope=public_profile,email&redirect_uri=".$callback_url;
if(!isset($_SESSION['referer'])){
    $_SESSION['referer'] = $_SERVER['HTTP_REFERER'];
}

if(!isset($_SESSION['step'])){
    $_SESSION['step'] = 1;
}


$parsed = parse_url($_SERVER['REQUEST_URI']);
parse_str($parsed['query'], $output_arr);


if (isset($output_arr['code'])) {
    $USER_CODE = $output_arr['code'];
    $token_url = "https://graph.facebook.com/v2.8/oauth/access_token?" .http_build_query(
                    array(
                        "client_id"     => APP_ID,
                        "redirect_uri"  => $callback_url,
                        "client_secret" => APP_SECRET,
                        "code"          => $USER_CODE
                    )
                );
    $token_data = file_get_contents($token_url);
    $_SESSION['oauth'] = array('fb' => json_decode($token_data));
    $_SESSION['oauth']['me'] = get_me_fields($_SESSION['oauth']['fb']->access_token);
    $_SESSION['oauth']['exchange'] = json_decode(exchange_token());
    $referer = $_SESSION['referer'];
    $token = $_SESSION['oauth']['exchange']->access_token;
    $salt = generate_salt();
    $hashed_token = hash('sha256', $token.$salt);
    setcookie('fb', $hashed_token , time() + 86000 * 30,'/', $_SERVER['HTTP_HOST'], false, true);
    try{
        $auth->auth()->registerOauthUser(
            $provider_id=1, //FACEBOOK
            $_SESSION['oauth']['me']->id,
            $token,
            iconv('utf8', 'cp1251', $_SESSION['oauth']['me']->name),
            $_SESSION['oauth']['me']->picture->data->url,
            $_SESSION['oauth']['me']->email,
            $salt,
            $hashed_token
        );
    } catch (\PDOException $e){
        $auth->auth()->updateOauthUser(
            $token,
            iconv('utf8', 'cp1251', $_SESSION['oauth']['me']->name),
            $_SESSION['oauth']['me']->picture->data->url,
            $_SESSION['oauth']['me']->email,
            $salt,
            $hashed_token,
            $_SESSION['oauth']['me']->id
        );
    }
    unset($_SESSION['referer']);
    header("Location: ". $referer);
} else {
    $_SESSION['fb_logged'] == false;
    header("Location: $login_dialog_url");
}


function exchange_token(){
    $url = "https://graph.facebook.com/v2.8/oauth/access_token?".  http_build_query(
             array(
                 "grant_type"        => "fb_exchange_token",
                 "client_id"         => APP_ID,
                 "client_secret"     => APP_SECRET,
                 "fb_exchange_token" => $_SESSION['oauth']['fb']->access_token 
             )
    );
    return file_get_contents($url);
}

function get_me_fields($token){
     $url = "https://graph.facebook.com/v2.8/me?fields=id%2Cname%2Cpicture%2Cemail&access_token=".$token;
     $response = file_get_contents($url);
     return json_decode($response);
}

