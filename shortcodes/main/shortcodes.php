<?php
require_once __DIR__.'/../../modules/shortcodes/shortcodes.php';
require_once __DIR__.'/../../modules/shortcodes/shortcodes-support.php';
require_once __DIR__.'/../../modules/shortcodes/wpautop.php';
require_once __DIR__.'/../../modules/vendor/autoload.php';
require_once __DIR__.'/bootstrap3.php';
require_once __DIR__.'/../../lib/lib_calendar.php';


use Event\Filter;
use Event\Type;
use Event\Month;
use Event\EasterCalendar;
use Event\EASTER_ORTHODOX;
use Event\EASTER_WESTERN;


if(!isset($di_container)) {
    $di_container = new \Pimple\Container();
    $di_container->register(new \Maksoft\Containers\GlobalContainer());
}

function current_year() {
    return date("Y");
}

function button_link($atts, $name=null) {
    extract(shortcode_atts( array(
        'href' => '#', 
        'class' => 'btn btn-info',
        'newTab' => True
    ), $atts));

    $name = do_shortcode($name);
    $newTab = $newTab ? "_target='blank'" : '';
    return "<a href='$href' class='$class' $newTab>$name</a>";
}

function next_year() {
    return date("Y")+1;
}

function country_codes()
{
    global $di_container;
    $cal = new Event\Calendar($di_container['gate']);
    return wordwrap(array_reduce($cal->getCountryCodes(), function($carry, $item) {
        $carry .= ' <b>' . trim($item->c_code) .'</b>';
        return $carry;
    }), 150, '<br>');
}

function print_calendar($atts)
{
    global $di_container;
    extract(shortcode_atts( array(
        'month' => date('n', time()),
        'shift' => 0,
    ), $atts));
    $year = date('Y', time());
    $year += $shift;
    $calendar = new Event\Calendar($di_container['gate']);
    $holidays = $calendar->month($month);
    $holidays->_(Filter::byTypes(array(Type::NATIONAL, Type::OFFICIAL)));
    $holidays = $holidays();
    $easter_orthodox = EasterCalendar::easter($year, new EASTER_ORTHODOX)->format(EasterCalendar::$format);
    $easter_western = EasterCalendar::easter($year, new EASTER_WESTERN)->format(EasterCalendar::$format);

    $easter_holidays = $calendar->floating(1, 6);
    $tmp = array();

    foreach($easter_holidays() as $celeb) {
        if($celeb->orthodox == 1) {
            $easter = new DateTime($easter_orthodox);
        } else {
            $easter = new DateTime($easter_western);
        }
        $sign = $celeb->emove > 0 ? '+' : '';
        if($celeb->emove == 0) {
            $c_date = $easter;
        } else {
            $c_date = $easter->modify("$sign {$celeb->emove} days");
        }
        $c_date = $c_date->format('d-m-Y');
        $day = date('j', strtotime($c_date));
        if(date('n', strtotime($c_date)) != $month) {
            continue;
        }
        /*if(in_array(date('w', strtotime($c_date)), array(0, 6))){
            $day = date('j', strtotime('next monday', strtotime($c_date)));
        }*/

        $celeb->mday = $day;
        $holidays[] = $celeb;
    }
    $calendar = array();
    foreach($holidays as $ev) {
        if(!isset($calendar[$ev->mday])) {
            $calendar[$ev->mday] = array();
        }

        $calendar[$ev->mday][] = $ev->c_code;

    }
    $calendar = array_map(function($d) {
        return array_unique($d);
    }, $calendar);
    ksort($calendar);
    $res = '<ul>';
    foreach($calendar as $day=>$val) {
        $txt = implode(" ", $val);
        $res.= "<li><b>$day</b> - $txt</li>";
    }
    $res .= '</ul>';
    return $res;
}

function fa($atts, $content = null) {
    $atts = shortcode_atts( array(
        "type" => '',
        "size" => false,
        "pull" => false,
        "border" => false,
        "spin" => false,
        "list_item" => false,
        "fixed_width" => false,
        "rotate" => false,
        "flip" => false,
        "stack_size" => false,
        "inverse" => false,
        "xclass" => false
    ), $atts );

    $class  = 'fa';
    $class .= ( $atts['type'] )         ? ' fa-' . $atts['type'] : '';
    $class .= ( $atts['size'] )         ? ' fa-' . $atts['size'] : '';
    $class .= ( $atts['pull'] )         ? ' pull-' . $atts['pull'] : '';
    $class .= ( $atts['border'] )       ? ' fa-border' : '';
    $class .= ( $atts['spin'] )         ? ' fa-spin' : '';
    $class .= ( $atts['list_item'] )    ? ' fa-li' : '';
    $class .= ( $atts['fixed_width'] )  ? ' fa-fw' : '';
    $class .= ( $atts['rotate'] )       ? ' fa-rotate-' . $atts['rotate'] : '';
    $class .= ( $atts['flip'] )         ? ' fa-flip-' . $atts['flip'] : '';
    $class .= ( $atts['stack_size'] )   ? ' fa-stack-' . $atts['stack_size'] : '';
    $class .= ( $atts['inverse'] )      ? ' fa-inverse' : '';
    $class .= ( $atts['xclass'] )   ? ' ' . $atts['xclass'] : '';
      
    return sprintf( 
      '<i class="%s"></i>',
       $class
    );
}


function get_easter($year) 
{
    return new DateTime(EasterCalendar::easter($year, new EASTER_ORTHODOX)->format(EasterCalendar::$format));
}

function print_easter($atts) {
    $array1=array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"); 
    $array2=array("����������", "�������", "�����", "���������", "�����", "������", "������"); 
    $array3=array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); 
    $array4=array("������", "��������", "����", "�����", "���", "���", "���", "������", "���������", "��������", "�������", "��������"); 

    setlocale(LC_TIME, 'bg.cp1251');
    extract(shortcode_atts( array(
        'shift' => 0,
        'format' => 'd F', 
    ), $atts));
    $year = date('Y', time()) + $shift;
    $easter = get_easter($year);
    $date=str_replace($array3,$array4, $easter->format($format)); 
    $higlight = '%s';
    if($year == date('Y', time())) { $higlight = "<b>%s</b>"; }
    return sprintf("�������� ���� $higlight � �� $higlight", $year, $date);
}


function get_empty_month()
{
    $holidays = array();

    for($i=1;$i < 32;$i++) {
        $holidays[$i] = array();
    }
    return $holidays;
}


function calendar($atts) {
    global $di_container;

    /*$apc_name = md5(json_encode(array('m'=>$month, 'c' => $country, 'y'=>$year, 'f' => __FUNCTION__)));
    if (apc_exists($apc_name)) {
        return apc_fetch($apc_name);
    }*/
    extract(shortcode_atts( array(
        'type' => Type::CHURCH,
        'month' => date('n', time()),
        'country' => 'bg',
        'year' => date('Y', time())
    ), $atts));

    $cal = new Event\Calendar($di_container['gate']);

    $easter = get_easter($year);

    $holidays = get_empty_month();

    $result = $cal->month($month);
    $result->_(Filter::byType($type))->_(Filter::byCountry($country));

    foreach ($result() as $ev) {
        $holidays[$ev->mday][] = trim($ev->eName);
    }

    $easter_holidays = $cal->floating($type);
    $easter_holidays->_(Filter::byCountry($country));
    $easter_orthodox = EasterCalendar::easter($year, new EASTER_ORTHODOX)->format(EasterCalendar::$format);

    foreach($easter_holidays() as $celeb) {
        $easter = new DateTime($easter_orthodox);
        $sign = $celeb->emove > 0 ? '+' : '';
        $c_date = $easter->modify("$sign {$celeb->emove} days");
        $c_date = $c_date->format('d-m-Y');
        $day = date('j', strtotime($c_date));
        if(date('n', strtotime($c_date)) != $month) {
            continue;
        }
        $t = $celeb->ev_type;

        if(!isset($holidays[$day])) {
            $holidays[$day] = array();
        }

        $holidays[$day][] = trim($celeb->eName);
    }

    if(date('n', strtotime($easter_orthodox)) == $month && (int) $type == Type::CHURCH) {
        $easter_orthodox = new \DateTime($easter_orthodox);
        $txt = '����������� ��������';
        $holidays[$easter_orthodox->format('j')][] = trim($txt);
    }
    /*apc_store($apc_name, $holidays);*/
    return $holidays;
}

function calendar_type($atts) {
    extract(shortcode_atts( array(
        'type' => Type::CHURCH,
        'month' => date('n', time()),
        'country' => 'bg',
        'year' => date('Y', time()),
        'shift' => 0,
    ), $atts));
    if($shift > 0) {
        $atts['year'] = $year +  $shift;
    }
    return format_calendar(calendar($atts));
}

function format_calendar(array $data) {
    ksort($data);
    $tmp = array();
    foreach($data as $day => $praznici) {
        $tmp[$day] = "<b>$day</b>";
        foreach($praznici as $c => $p_type) {
            if(empty($p_type) ) {
                continue;
            }
            $str = (is_array($p_type) ? implode(', ', array_unique($p_type)) : $p_type);
            if(!empty($str)) {
                $tmp[$day] .= ' - '. $str; 
            }
        }
    }

    return implode('; ', array_filter($tmp, function($e){
        return strlen(strip_tags($e)) > 3;
    }));
}

function calendar_combined($atts)
{
    global $di_container;
    extract(shortcode_atts( array(
        'month' => date('m', time()),
        'country' => 'bg',
        'year' => date('Y', time()),
        'shift' => 0,
    ), $atts));

    if($shift > 0) {
        $year += $shift;
        $atts['year'] = $year;
    }

    $apc_name = md5(json_encode(array('m'=>$month, 'c' => $country, 'y'=>$year, 'f' => __FUNCTION__)));
    /*if (apc_exists($apc_name)) {
        return apc_fetch($apc_name);
    }*/

    $cal = new Event\Calendar($di_container['gate']);

    $result = $cal->month($month);
    $holidays = array();
    for($i=1;$i < 32;$i++) {
        $holidays[$i] = array(
            Type::CHURCH => array(),
            Type::NAME_DAY => array(),
        );
    }

    $church_cal = calendar($atts);

    foreach($church_cal as $day => $ev) {
        $holidays[$day][Type::CHURCH] = $ev;
    }

    $easter_orthodox = EasterCalendar::easter($year, new EASTER_ORTHODOX)->format(EasterCalendar::$format);

    $easter_holidays = $cal->floating(Type::NAME_DAY);

    foreach($easter_holidays() as $celeb) {
        $easter = new DateTime($easter_orthodox);
        $sign = $celeb->emove > 0 ? '+' : '';
        $c_date = $easter->modify("$sign {$celeb->emove} days");
        $c_date = $c_date->format('d-m-Y');
        $day = date('j', strtotime($c_date));
        if(date('n', strtotime($c_date)) != $month) {
            continue;
        }
        $t = $celeb->ev_type;

        if(!isset($holidays[$day][$celeb->ev_type])) {
            $holidays[$day][$celeb->ev_type] = array();
        }

        $holidays[$day][$celeb->ev_type][] = trim($celeb->eName);
    }

    $atts['type'] = Type::NAME_DAY;
    $namedays_calendar = calendar($atts);

    foreach($namedays_calendar as $day => $names){
        $holidays[$day][Type::NAME_DAY] = array_merge($holidays[$day][Type::NAME_DAY], $names);
    }

    $result = format_calendar($holidays);
    /*apc_store($apc_name, $result);*/
    return $result;
}


function include_dop($atts){
    global $o_page;
    extract( shortcode_atts( array(
        'name' => '.'
    ), $atts) );

    return include_once $o_page->get_pageURL($name);
}

function get_picture($atts, $id){
    global $o_page;
    extract(shortcode_atts( array(
        'id' => '1', 
        "class" => "modalImg",
        'width' => null,
        "alt"   => null,
    ), $atts));

    return $o_page->get_image($id, $width, $class, $alt);
}

function popup_image(){
    return '
<link href=\'/Templates/base/bootstrap_desktop/css/modal_styles.css\' id=\'layout-style\' rel=\'stylesheet\' type=\'text/css\'>
<div id="myModal" class="modal">
  <span class="close" id="modalClose">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
<script>
var modal = document.getElementById("myModal");
modal.onclick = function(){
    modal.style.display= "none";
}

var images = document.getElementsByClassName("modalImg");
for(i=0;i<images.length;i++){
    img= images[i];
    img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }
}
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");

modalImg.onclick = function(){
    modal.style.display = "none";
}

var span = document.getElementById("modalClose");

span.onclick = function() { 
    modal.style.display = "none";
}
</script>
';
}

function autoescape($atts, $content){
    return $content;
}

function datetime($atts, $content){
    extract( shortcode_atts( array(
        'format' => 'd-m-Y'
    ), $atts) );
    return date($format, time());
}

function print_group($atts, $content){
    global $o_page;
    extract( shortcode_atts( array(
        'id' => null
    ), $atts) );
    ob_start();
    $o_page->print_subpages($o_page->get_pGroupContent($id));
    $page = ob_get_contents();
    ob_end_clean();
    return $page;
}

function sequence($atts, $context){
    extract(shortcode_atts( array(
        'times'     => 1,
        'separator' => '',
    ), $atts) );
    $context = do_shortcode($context);

    if($times < 1){
        return $context;
    }

    $html = array();
    $tmp = '';
    for($i=0; $i < $times; $i++){
        $tmp .=trim($context);
    }
    return $tmp;
    return implode($separator, $html);
}

//[hw name="RADOSLAV YORDANOV" ] CUSTOM CONTENT CUSTOM CONTENT CUSTOM CONTENT[hw]
function hw($atts, $content){
    extract( shortcode_atts( array(
        'name' => 'maksoft'
    ), $atts) );

    return "<br>Hello world<br> <b><h1>$name</h1></b><br><h1> CUSTOM $content CONTENT</h1> <hr>";
}

function row($atts, $content){
    extract( shortcode_atts( array(
        'class' => ''
    ), $atts) );
    $content = do_shortcode($content);
    return '<div class="row '.$class.'">'.$content.'</div>';
}

function col($atts, $content){
    extract( shortcode_atts( array(
        'md' => '12'
    ), $atts) );
    $content = do_shortcode($content);
    return '<div class="col-md-'.$md.'">'.$content.'</div>';
}

function code($content){
    return "<pre><code>$content</code></pre>";
}

function upper($atts, $content){
    return strtoupper(do_shortcode($content));
}

function lower($atts, $content){
    return strtolower(do_shortcode($content));
}


function dop() {
    global $o_site, $o_page;

    ob_start();
    eval($o_page->_page['PHPcode']);
    include_once($o_page->_page['PageURL']);
    $res = ob_get_contents();
    ob_end_clean();

    return $res;
}

function eval_php() {
    global $o_site, $o_page, $rss, $wwo;
    $res = "";
    if($o_page->_page['scope'] != "head") {
        ob_start();
        if(strlen($o_page->_page['PHPcode'])>12){
            eval($o_page->_page['PHPcode']);
        };
        $res .= ob_get_contents();
        $o_page->_page['PHPcode'] = '';
    }

    return $res;
}



function get_name($atts)
{
    global $o_page; 
    $data = $o_page->shortcode_atts(array(
        'first' => 'default',
        'last' => 'default'
        ), $atts);
    return implode(' ' ,$data);
}

function pull_quote_shortcode( $atts ) {
 
    $output = '';
    global $o_page;
 
    $pull_quote_atts = $o_page->shortcode_atts( array(
        'quote' => 'My Quote',
        'attribution' => 'Author',
    ), $atts );
 
    return implode(' ', $pull_quote_atts);
}





$functions = get_defined_functions();

foreach($functions['user'] as $instance => $func_name){
    $o_page->add_shortcode($func_name, $func_name);
}
