<?php
include '../lib/Database.class.php';
include '../lib/lib_page.php';



class ApiPage extends page
{
    protected $db;

    public function setDatabase($db)
    {
        $this->db = $db;
    }

    protected function getData()
    {
        $stmt = $this->db->prepare(
            "SELECT * FROM Sites
             WHERE url LIKE :url
             ORDER BY SitesID ASC LIMIT 1");
        $stmt->bindValue(":url", "%".$_SERVER['SERVER_NAME']."%");
        $stmt->execute();
        $site = $stmt->fetch(PDO::FETCH_ASSOC);
        return $site;
    }

    public function isPageFromSameSitePermission($n)
    {
        $page = (object) $this->get_page($n);
        $site = (object) $this->getData();
        if ($page->SiteID != $site->SitesID){
            throw new Exception(sprintf('Pages %s dont exist!', $page->n));
        }
        return True;
    }

    public function getParentPage($n,  $sort_order, $user_access_level, $limit=false)
    {

        if($limit && !empty($limit) && is_array($limit)){
            $limit = sprintf(" LIMIT %s, %s", $limit[0], $limit[1]);
        } else {
            $limit = '';
        }

        $query = "
            SELECT * FROM pages p 
            LEFT JOIN images im on p.imageNo = im.imageID
            WHERE p.ParentPage = :parent_page
            AND p.SecLevel <= :user_level
            ORDER by :sort_order 
            :limit ";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":parent_page", $n);
        $stmt->bindValue(":user_level", $user_access_level);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->bindValue(":limit", $limit);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $tmp[$row['n']] = $row;
        }
        return $tmp;
    }

    public function getSubContent($n,$link, $sort, $limit)
    {
        $sort_order = array(
            'ASC' => 'p.n ASC',
            'DESC' => 'p.n DESC',
            'p.Name DESC' => 'p.Name DESC',
            'p.Name ASC' => 'p.Name ASC',
            "p.date_added ASC" => "p.date_added ASC",
            "p.date_added DESC" => "p.date_added DESC",
            "p.toplink ASC" => "p.toplink ASC",
            "p.toplink DESC" => "p.toplink DESC",
            "RAND()" => "RAND()",
        );
        $subpages = $this->getParentPage(
                $n,
                $sort_order[$sort], 
                0,
                $limit);
        $show_link = array(
            0  => array(),
            1  => array('name'),
            2  => array('name','content','img'),
            3  => array('name','img'),
            4  => array('name','content2','img'),
            5  => array('content','img'),
            6  => array('content'),
            7  => array('img','content2'),
            8  => array('content2'),
            9  => array('img'),
            10 => array('name','content'),
            11 => array('name','content2'),
            13 => array('name','img','content2'),
            14 => array('content2', 'img'),
            15 => array('name', 'content2'),
        );

        $make_links = array(
            0 => array(),
            1 => array('name', 'content', 'img', 'content2'),
            2 => array('title'),
            3 => array('img'),
            4 => array('name', 'content', 'img', 'content2'),
        );
        $tmp = array();
        $data = array();
        $sub_content = array();
        $show_link = $show_link[$link];
        foreach($subpages as $page){
            $data['name'] = array('str'=>$page['Name'], 'link'=> '');
            $data['content'] = array($page['textStr'], 'link' => '');
            $data['img'] = array('str' => $this->get_pImage($page['n']), 'link' => '');
            $text = str_replace('</p>', '', $page['textStr']);
            $array = explode('<p>', $text);
            $data['content2'] = array('str'=>$array[1], 'link' => '');
            $link = $this->get_pLink($page['n']);
            foreach($make_links[(int) $page['make_links']] as $content){
                $data[$content]['link'] = $link;
            }
            foreach($show_link as $k){
                $tmp[$k] = $data[$k];
            }
            $sub_content[] = $tmp;
        }
        return $sub_content;
    }
}

$db = new Database();
$db->exec("SET NAMES utf8;");
$api = new ApiPage();
$api->setDatabase($db);
try{
    if(!isset($_GET['n']) or filter_var($_GET['n'], FILTER_VALIDATE_INT) == false){
        throw new Exception("Invalid value provided for page n!");
    }
    if(!isset($_GET['prev']) && $_GET['prev'] >= 0){
        throw new Exception("Invalid value provided for prev!");
    }
    if(!isset($_GET['next']) or filter_var($_GET['next'], FILTER_VALIDATE_INT) == false){
        throw new Exception("Invalid value provided for next!");
    }
    $n = $_GET['n'];
    $api->isPageFromSameSitePermission($n);
    $page = (object) $api->get_page($n);
    $response = $api->getSubContent($page->n, $page->show_link ,$page->show_link_order, array($_GET['prev'], $_GET['next']));
} catch ( Exception $e) {
    $response = array('error' => $e->getMessage());
}


header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json', 'charset="cp1251');
echo json_encode($response);
