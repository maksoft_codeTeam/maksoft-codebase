<?php
error_reporting(0);
ini_set("display_errors", 0); 
require_once __DIR__."/lib_json.php";
require_once __DIR__.'/../loader.php';


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


$api = new json_api($services);
$commands = require_once __DIR__.'/commands.php';

require_once __DIR__."/helpers.php";


$request = Request::createFromGlobals();
$response = new JsonResponse();
$response->headers->set('Access-Control-Allow-Origin', '*');
$response->headers->set('Access-Control-Allow-Methods', 'GET,POST,PUT');
$response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
$response->headers->set('Cache-Control', 'max-age='.$api->time);

$command = $request->query->get('command');
$page_id = $request->query->get('n');

if(isset($_SESSION['TICKSYS_SETTINGS']) and is_array($_SESSION['TICKSYS_SETTINGS'])) {
    foreach($_SESSION['TICKSYS_SETTINGS'] as $name => $value) {
        define(strtoupper($name), $value);
    }
}


try {
    if(!array_key_exists($command, $commands) or filter_var($page_id, FILTER_VALIDATE_INT) == false ) {
        throw new \Exception('Invalid parameter number provided');
    }

    $data = $commands[$command]($page_id);
    /*if($cache_exists($command) and $_SERVER['REMOTE_ADDR'] != '195.234.238.38' ) {
        $data = $get_cache($command);
    } else {
        $data = $commands[$command]($page_id);
        $store_result($command, $data);
    }
*/
    $response->setContent(iconv('cp1251', 'utf8', json_encode($data)));
} catch (Exception $e) {
    $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
    $response->setContent(json_encode($e->getMessage()));
}
$response->prepare($request);
$response->send();

