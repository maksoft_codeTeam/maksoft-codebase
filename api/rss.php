<?php
session_start();
include __DIR__."/../lib/Database.class.php";
include __DIR__."/lib_json.php";
$tsstring = gmdate('D, d M Y H:i:s ', $timestamp) . 'GMT';
$etag = $language . $timestamp;

$if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false;
$if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? $_SERVER['HTTP_IF_NONE_MATCH'] : false;
if ((($if_none_match && $if_none_match == $etag) || (!$if_none_match)) &&
    ($if_modified_since && $if_modified_since == $tsstring))
{
    header('HTTP/1.1 304 Not Modified');
    exit();
}
else
{
    header("Last-Modified: $tsstring");
    header("ETag: \"{$etag}\"");
}

$db = new Database($db);
$db->exec("set names cp1251");

$api = new json_api($db);

$commands = array(
    'rss_parse' => function($n) use ($api) { return $api->rss_parse($_GET['link']); },
);
$response = array('commands' => 'Available commands:\n'. implode('\n', array_keys($commands)));
try{
    $command = $_GET['command'];
    if(!array_key_exists($command, $commands)){
        throw new Exception(sprintf(
        "Invalid command [%s]! Available commands are %s",
                $command,
                implode(',', array_keys($commands))));
    }
    if(!isset($_GET['n']) or filter_var($_GET['n'], FILTER_VALIDATE_INT) == false){
        throw new Exception("Invalid value provided for page n!");
    }
    $n = $_GET['n'];
    $response = $commands[$command]($n);
    //$api->log_client($command, $_GET);
} catch (Exception $e){
    $response = array('error'=>$e->getMessage());
}

header('Content-Type: text/html', 'charset=cp1251');
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
header('Cache-Control: max-age='.$api->time);
header('Pragma: cache');
echo '<meta http-equiv="Content-Type" content="text/html;charset=windows-1251"/>';
echo iconv('utf8', 'cp1251', $response);
