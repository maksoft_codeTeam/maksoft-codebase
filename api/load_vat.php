<?php
session_start();
header('Content-Type: application/json');
include $_SERVER["DOCUMENT_ROOT"]."/lib/Database.class.php";
$token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.EkN-DOsnsuRjRO6BxXemmJDm3HbxrbRzXglbN2S4sOkopdU4IsDxTI8jO19W_A4K8ZPJijNLis4EZsHeY559a4DFOd50_OqgHGuERTqYZyuhtF39yxJPAjUESwxk2J5k_4zM3O-vtd1Ghyo4IbqKKSy6J9mTniYJPenn5-HIirE';
$db = new Database();

if(isset($_SESSION['j_token']) && $_SESSION['j_token'] == $token && is_numeric($_GET['term'])): 
    $sql = "SELECT Name, bulstat, dn FROM Firmi WHERE bulstat LIKE :search AND LENGTH(bulstat) > 3";
    $stmt = $db->prepare($sql);
    $stmt->bindValue(":search", $_GET['term']."%");
    $stmt->execute();
    $data = array();
    $i = 0;
    while ($firm = $stmt->fetch(PDO::FETCH_OBJ)) {
        $data[$i]['label'] = iconv('cp1251', 'utf8', $firm->Name);
        $data[$i]['value'] =  $firm->bulstat;
        $data[$i]['custom'] = array(iconv('cp1251', 'utf8',$firm->Name),
                                    $firm->dn);
        $i++;
    }
    echo json_encode($data);
elseif(isset($_SESSION['j_token']) && $_SESSION['j_token'] == $token):
    $sql = "SELECT Name, bulstat, dn FROM Firmi WHERE Name LIKE :search";
    $stmt = $db->prepare($sql);
    $stmt->bindValue(":search", iconv('utf8', 'cp1251', $_GET['term'])."%");
    $stmt->execute();
    $data = array();
    $i = 0;
    while ($firm = $stmt->fetch(PDO::FETCH_OBJ)) {
        $data[$i]['label'] = iconv('cp1251', 'utf8', $firm->Name);
        $data[$i]['value'] =  $firm->bulstat;
        $data[$i]['custom'] = array(iconv('cp1251', 'utf8',$firm->Name),
                                    $firm->dn);
        $i++;
    }
    echo json_encode($data, true);
else:
    echo json_encode(array('ERROR' => "Invalid Token"));
endif;
