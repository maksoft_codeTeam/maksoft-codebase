<?php

$buildKey = function($key) use ($request, $api){
    $parameters = $request->query->all();
    $SiteID = $api->_site['SitesID'];
    return hash('md5', json_encode(array($SiteID, $parameters))); 
};

$store_result = function($command, $result) use ($api, $buildKey) {
    apc_store($buildKey($command), $result, $api->time);
};


$cache_exists = function($command) use ($buildKey) {
    return apc_exists($buildKey($command));
};


$get_cache = function($command) use ($buildKey) {
    return apc_fetch($buildKey($command));
}; 
