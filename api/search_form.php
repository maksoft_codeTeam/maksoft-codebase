<?php
include __DIR__."/../lib/Database.class.php";
include __DIR__."/../lib/lib_page.php";
include __DIR__."/../lib/lib_functions.php";
$token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.EkN-DOsnsuRjRO6BxXemmJDm3HbxrbRzXglbN2S4sOkopdU4IsDxTI8jO19W_A4K8ZPJijNLis4EZsHeY559a4DFOd50_OqgHGuERTqYZyuhtF39yxJPAjUESwxk2J5k_4zM3O-vtd1Ghyo4IbqKKSy6J9mTniYJPenn5-HIirE';
$db = new Database();
$o_page = new page();
$result = $o_page->do_search(iconv('utf8', 'cp1251',$_GET['term']), 30);
$data = array();
$needle = iconv('utf8', 'cp1251', $_GET['term']);
for($i=0; $i < count($result); $i++){
    foreach($result[$i] as $k=>$v){
        $result[$i][$k] = iconv('cp1251', 'utf8', $v);
        $result[$i][$k] = str_replace($needle, '<b>'. $needle .'</b>', iconv('cp1251', 'utf8', $v));
    }
    $result[$i]['textStr'] = str_replace($needle, '<b>'. $needle .'</b>', $result[$i]['textStr']);
    $data[$i]['label'] =  $result[$i]['Name'];
    $data[$i]['link']  = $o_page->get_pLink($result[$i]['n']); 
    $data[$i]['value'] =  $result[$i];
    $data[$i]['value']['textStr']  = cut_text($result[$i]['textStr'], 300);
    #if(empty($data[$i]['value']['image_src'])){
    #    $data[$i]['value']['image_src'] = 'web/admin/images/no_image.jpg';
    #}
}
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
header('Content-Type: application/json');
header('Cache-Control: max-age=56000');
header('Pragma: cache');
echo json_encode($data);

