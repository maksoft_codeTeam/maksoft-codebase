<?php
use jokuf\ticksys\Config;

require_once __DIR__."/../modules/vendor/autoload.php";
include __DIR__."/../lib/lib_page.php";
include __DIR__."/../lib/lib_functions.php";
require_once __DIR__."/../global/comments/forms/NewComment.php";


class json_api extends page 

{
    protected $db;
    protected $agent='Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0';
    protected $services;

    public $time = 1800; 

    public function __construct($services)
    {
        parent::__construct();
        $this->services = $services;
        $this->db = $this->services['db'];
        $this->db->exec("SET NAMES utf8");
        $this->db->setAttribute( \PDO::ATTR_EMULATE_PREPARES, false );
    }

    public function getCities()
    {
        $config = new Config(
            $_SESSION['TICKSYS_SETTINGS']['TICKSYS_API_PASS'], 
            $_SESSION['TICKSYS_SETTINGS']['TICKSYS_COMPANY_ID']
        );

        $responsePromise = jokuf\ticksys\Api::L('get_cities');
        $result = $responsePromise->fetch();
        return $result['CITY'];
    }

    public function getTravelToCity($date, $city) 
    {
        $config = new Config(
            $_SESSION['TICKSYS_SETTINGS']['TICKSYS_API_PASS'], 
            $_SESSION['TICKSYS_SETTINGS']['TICKSYS_COMPANY_ID']
        );
        $date = date('d.m.Y', strtotime($date));
        $responsePromise = jokuf\ticksys\Api::L('get_travel_to_city', $date, $city);
        $result = $responsePromise->fetch();
        return $result['CITY'];
    }

    public function getMostVisitedPagesThisMonth()
    {
        $SiteID = $this->_site['SitesID'];
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $this->services['gate']->stats()->getMostVisitedPagesThisMonth($SiteID);
    }
    public function getSiteVisitsDaily()
    {
        $SiteID = $this->_site['SitesID'];
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $this->services['gate']->stats()->getSiteVisitsDaily($SiteID);
    }

    public function getSiteVisitsLastThreeMonths()
    {
        $SiteID = $this->_site['SitesID'];
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $this->services['gate']->stats()->getSiteVisitsLastThreeMonths($SiteID);
    }

    public function find_site_autocmpl($term) {
        if($this->_user['AccessLevel'] < 2) {
            return array();
        }
        $sql = "SELECT SitesID, primary_url from Sites where primary_url Is not null and primary_url LIKE :term";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":term", "%$term%");
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = array('label' => $res->primary_url, 'value'=>$res->SitesID);
        }
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $tmp;
    }

    public function autoSuggestPages($name) 
    {
        $accessLevel = isset($this->_user['AccessLevel']) ? $this->_user['AccessLevel'] : 0;

        if($accessLevel < 1) {
            return array();
        }
        $SiteID = $this->_site['SitesID'];
        $query = 'SELECT n as id, Name as value from pages WHERE Name LIKE :name and SiteID=:SiteID AND SecLevel <= :accessLevel and status = 1';
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':name', "%$name%");
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":accessLevel", $accessLevel);
        $stmt->execute();
        $pages = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $pages;
    }

    public function sortPage($n, $parent, $sort, $SiteID) {
        $sql = "UPDATE pages SET ParentPage=:parent, sort_n=:sort WHERE n=:n and SiteID = :SiteID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":parent", $parent);
        $stmt->bindValue(":sort", $sort);
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
    }

    public function swapPages($sourcePageId, $destinationPageId, $destinationRootId) 
    {
        if(!$destinationRootId) {
            $destinationRootId = $this->_site['StartPage'];
        }
        $sourcePage = $this->get_page($sourcePageId, $as='object');
        $destinationPage = $this->get_page($destinationPageId, $as='object');
        $destinationParent = $this->get_page($destinationRootId, $as='object');

        if($sourcePage->SiteID != $this->_site['SitesID'] or $destinationPage->SiteID != $this->_site['SitesID']) {
            throw new \Exception('Страницата която се опитвате да преместите не принадлежи на този сайт');
        }

        switch(True) {
            case $sourcePage->ParentPage == $destinationPage->ParentPage :
                // значи са на едно ниво и сменяме само сорт-а
                $this->sortPage($sourcePage->n, $sourcePage->ParentPage, $destinationPage->sort_n, $this->_site['SitesID']);
                $this->sortPage($destinationPage->n, $destinationPage->ParentPage, $sourcePage->sort_n, $this->_site['SitesID']);
                break;
            case $sourcePage->ParentPage != $destinationPage->ParentPage and $destinationPage->ParentPage == $destinationParent->n:
                $this->sortPage($sourcePage->n, $destinationParent->n, $sourcePage->sort_n, $this->_site['SitesID']);
                break;
            case $sourcePage->ParentPage != $destinationPage->ParentPage:
                $sourcePage->ParentPage == $destinationPage->n;
                $this->sortPage($sourcePage->n, $sourcePage->ParentPage, $sourcePage->sort_n, $this->_site['SitesID']);
                break;
            default:
                throw new \Exception('Страницата която се опитвате да преместите не принадлежи на този сайт');
        }
    }

    public function rss_parse($link)
    {
        $this->time = 60 * 60 * 24 ; // 1 day
        include_once __DIR__."/../lib/lib_rss.php"; 
        $rss=new rss(); 
        $rss->img_width = 280; 
        $rss->add_url($link);
        return $rss->parse();
    }

    public function log_client($command, $args){
        $insert_stmt = "
            INSERT INTO  `maksoft`.`api_history` (
            `SiteID` ,
            `command` ,
            `arguments`
            )
            VALUES (
                :siteID, :command, :args
            );
        ";

        $stmt = $this->db->prepare($insert_stmt);
        $stmt->bindValue(":siteID", $this->SiteID);
        $stmt->bindValue(":command", $command);
        $stmt->bindValue(":args", json_encode($args));
        $stmt->execute();
    }

    public function do_search($term, $max_results=20)
    {
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return parent::do_search();
    }

    public function dataScrapper($url)
    {
        $user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 20);
        $data = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        if ($curl_errno > 0) {
            throw new Exception("Error Processing Request cURL Error ($curl_errno):", 1);
        }
        curl_close($ch);
        return $data;
    }


    function currentTraficInformation(){
        $url = "http://www.api.bg/index.php/bg/promeni";
        $data = $this->dataScrapper($url);
        $dom = new DomDocument;
        $dom->loadHTML($data);
        $dom->preserveWhiteSpace = true;
        return $dom;
    }

    function getElementsByClassName($dom){
        $finder = new DomXPath($dom);
        $classname="news-item";
        $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");

        $news_container = array();
        foreach ($nodes as $node) {
            $title = $this->getValueByTag($node, 'h3', 'news-title');
            $date = $this->getValueByTag($node, 'div', 'date');
            $content = trim($node->textContent);

            $content = str_replace(array($date, $title, '.', ','), '', $content);

            $news_container[] = array(
                'title'   => $title,
                'date'    => $date,
                'content' => ucfirst(trim($content))
            );
        }

        return $news_container;
    }

    function getValueByTag($node, $tag_name, $class_name=false, $attribute='class'){
        $tags = $node->getElementsByTagName($tag_name);
        foreach($tags as $title){
            if($title->hasAttribute($attribute)){
                if($class_name == $title->getAttribute($attribute)){
                    return (string) $title->textContent;
                }
            }
            return (string) $title->textContent;
        }
    }


    function API_road_infrastructure_crawl()
    {
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $this->getElementsByClassName($this->currentTraficInformation());
    }

    public function deleteFbCookie(){
        if(isset($_COOKIE['fb'])){
            unset($_COOKIE['fb']);
        }

        return array("success");
    }

    public function publishComment(){
        $gate = new Maksoft\Gateway\Gateway($this->db); // Database abstraction layer
        $publish_comments = new NewComment($gate->page(), $this->_page['n'], $_POST);
        if($_SERVER['REQUEST_METHOD'] === "POST" && isset($_POST['action']) and $_POST['action'] == $publish_comments->action->value){
            try{
                $publish_comments->is_valid();
            } catch (\Exception $e){
                foreach($publish_comments as $field){
                    var_dump($field->get_errors());
                }
                return $e;
            }
            
            return $_POST;
        }

        return array("errors" => array("you cant!"));

    }

    public function getMyProfile(){
        return array(
            'name' => iconv('cp1251', 'utf8', $this->_user['Name']),
            'email'=> $this->_user['EMail'],
            'id'   => $this->_user['ID'],
        );
    }

    public function getUserIp(){
        return $_SERVER['REMOTE_ADDR'];
    }

    public function getStrickerImage($ref){
        $strickerCatalog = \Maksoft\AFI\Provider\Stricker::initialize();
        return $strickerCatalog->image($ref);
    }

    public function getComments($n){
        $sql = "SELECT comment_id, comment_subject, comment_author, comment_date, comment_parent, comment_user_id, comment_image, comment_status, comment_text
            FROM  `comments` 
            WHERE  `comment_n` = :n
            ORDER BY comment_date DESC
            ";
        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(":n", $n);
        $stmt->execute();
        $tmp = array();

        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }

        return $tmp;
    }

    public function getKeywordPositions($SiteID, $keyword)
    {
        $stmt = $this->db->prepare("
            SELECT keywords, g_pos, g_result, enter_time 
            FROM  `keywords` 
            WHERE  `keywords` LIKE :kw
            AND  `SiteID` = :sid
            AND g_result > 0
            AND g_pos > 0
            ORDER BY enter_time ASC
        ");
        $stmt->bindValue(":sid", $SiteID);
        $stmt->bindValue(":kw", $keyword);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch()){
            $tmp[] = $res;
        }
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $tmp;
    }

    public function getMostCommentedPages($site_id, $limit=5)
    {
        $sql = "
        SELECT * 
        FROM pages p
        JOIN images i ON i.imageID = p.imageNo
        JOIN comments c ON c.comment_n = p.n
        WHERE p.SiteID = :SiteID
        LIMIT :limit ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $site_id);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();

        while($res=$stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }

        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $tmp;
    }

    public function getMostVisitedPages($site_id, $limit=10)
    {
        $sql = "
        SELECT *  
        FROM pages p
        JOIN images i ON i.imageID = p.imageNo
        WHERE p.SiteID = :SiteID
        AND p.date_added > DATE_SUB(NOW(), INTERVAL 7 Day)
        AND LENGTH(p.textStr) > 30
        ORDER BY p.preview DESC
        LIMIT :limit";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $site_id);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();

        $parent_page = False;
        while($res=$stmt->fetch(\PDO::FETCH_OBJ)){
            $res->textStr = strip_tags($res->textStr);
            $res->page_link = $this->get_pLink($res->n);
            if($parent_page){
                $res->parent = $parent_page;
            } else {
               $parent_page = $this->getSinglePage($res->ParentPage);
               $res->parent = $parent_page;
            }
            $tmp[] = $res;
        }
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $tmp;
    }

    public function getMostVisitedPagesMonthly($limit=10)
    {
        $SiteID = $this->_site['SitesID'];
        $query = '
            SELECT COUNT( entry ) AS views, n
            FROM  `stats` 
            WHERE SiteID = :SiteID
            AND MONTH( viewTime ) = MONTH( CURRENT_DATE( ) ) 
            GROUP BY n
            ORDER BY views DESC'; 
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getPageVisitsForLastQuarter($n)
    {
        $SiteID = $this->_site['SitesID'];
        $query = '
            SELECT COUNT( entry ) AS views, n, viewTime
            FROM  `stats` 
            WHERE SiteID = :SiteID
            AND n = :n
            AND MONTH( viewTime ) = MONTH( CURRENT_DATE( ) ) 
            group by MONTH(viewTime)
            ORDER BY views DESC'; 
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        $tmp = array();
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getSubPages($site_id, $parent_page, $limit=5, $by="sort_n")
    {
        $order = array();
        $order['sort_n'] = "p.sort_n "; 
        $order['date_desc'] = "p.date_added DESC "; 
        $order['date_asc'] = "p.date_added ASC "; 
        if(array_key_exists($by, $order)){
            $order = $order[$by];
        } else {
            $order = "p.sort_n";
        }

        $sql = "
            SELECT SQL_CACHE  * 
            FROM pages p
            LEFT JOIN images i ON i.imageID = p.imageNo
            WHERE p.SiteID = :SiteID
            AND p.ParentPage = :parent_page
            ORDER BY $order
            LIMIT :limit";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $site_id);
        $stmt->bindValue(":parent_page", $parent_page);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array(); 
        $parent_page = false;

        while($res=$stmt->fetch(\PDO::FETCH_OBJ)){
            $res->textStr = strip_tags($res->textStr);
            if(strlen($res->textStr) < 10){
                continue;
            }

            if($parent_page){
                $res->parent = $parent_page;
            } else {
               $parent_page = $this->getSinglePage($res->ParentPage);
               $res->parent = $parent_page;
            }
            $res->link = $this->get_pLink($res->n);
            $str =  html_entity_decode($res->textStr);
            $res->textStr_part = cut_text(strip_tags($str), 160);
            $tmp[] = $res;
        }
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $tmp;
    }

    public function getSinglePage($n){
        $sql = "SELECT SQL_CACHE  * FROM pages left join images i on pages.imageNo = i.imageID where pages.n = :n LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":n", $n);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getNewestPages($site_id, $limit=10)
    {
        $sql = "
            SELECT SQL_CACHE  * 
            FROM pages p
            JOIN images i ON i.imageID = p.imageNo
            WHERE p.SiteID = :SiteID
            AND LENGTH(p.textStr) > 30
            ORDER BY p.date_added DESC 
            LIMIT :limit";
            // AND DATE(p.date_added ) = CURDATE( ) 
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $site_id);
        $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        $parent_page = false;

        while($res=$stmt->fetch(\PDO::FETCH_OBJ)){
            $res->textStr = strip_tags($res->textStr);
            $res->page_link = $this->get_pLink($res->n);
            if($parent_page){
                $res->parent = $parent_page;
            } else {
               $parent_page = $this->getSinglePage($res->ParentPage);
               $res->parent = $parent_page;
            }
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getSeoEtiketi()
    {
        $seo_query = "SELECT * FROM etiketi 
                      LEFT JOIN etiketi_groups on etiketi.egID=etiketi_groups.egID
                      WHERE (date_expire>=NOW() OR eg_date_expire>=NOW() )
                      AND LENGTH(Name)>=5 ORDER BY  date_checked ASC LIMIT 1";
        $stmt = $this->db->prepare($seo_query);
        $stmt->execute();
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function getSeoEtiketiPositions($egID, $keyword){
        $sql = "SELECT e.Name, e.description, ep.google_result, e.g_pos_org, ep.g_pos, ep.date_checked, e.url
                FROM etiketi e
                JOIN etiketi_pos ep ON ep.eID = e.eID
                WHERE Name LIKE :keyword
                AND e.egID = :egID
                ORDER BY ep.date_checked ASC
                ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":egID", $egID);
        $stmt->bindValue(":keyword", $keyword);
        $stmt->execute();
        $tmp = array();

        while($res=$stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $tmp;
    }

    public function getKeywords()
    {
        $this->db->exec("SET NAMES utf8");
        $sql = "SELECT DISTINCT(keywords.keywords), keywords.SiteID, Sites.primary_url, keywords.keysID 
                FROM keywords LEFT JOIN hostCMS on keywords.SiteID=hostCMS.SiteID 
                LEFT JOIN Sites on keywords.SiteID=Sites.SitesID 
                WHERE keywords.g_pos=-5 AND hostCMS.active=1
                AND hostCMS.toDate > DATE_SUB(CURDATE(), INTERVAL 90 DAY)
                AND primary_url<>'' 
                AND length(keywords.keywords)>5
                AND length(keywords.keywords)<36
                ORDER BY keysID 
                DESC LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }
    
    public function saveKeywords()
    {
        $data = $_GET;
        $sql = "UPDATE keywords 
                SET
                    g_pos= :g_pos,
                    g_result=:g_res, 
                    g_pos_org=:g_pos,
                    g_pos_adw=:g_pos_adw, 
                    g_pos_org=:g_pos,
                    api_sID    = :s_id
                WHERE keywords=:keyword
                AND SiteID=:site_id 
                AND keywords.g_pos=-5
                LIMIT 12 ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":g_pos", $data['org_position']);
        $stmt->bindValue(":g_pos_org", $data['org_position']);
        $stmt->bindValue(":g_res", $data['results']);
        $stmt->bindValue(":g_pos_adw", $data['adwords']);
        $stmt->bindValue(":keyword", $data['keywords']);
        $stmt->bindValue(":site_id", $data['SiteID']);
        $stmt->bindValue(":s_id", $data['server']);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function saveSeoEtiketi()
    {
        $data = $_GET;
        $this->db->exec("DELETE FROM etiketi_n WHERE bot_date<=DATE_SUB(CURDATE(), INTERVAL 15 DAY) ");
        $stmt = $this->db->prepare("SELECT DISTINCT(n) FROM etiketi_n WHERE eID=:k_id");
        $stmt->bindValue(":k_id", $data['eID']);
        $stmt->execute();
        $link_count_q = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $link_count = count($link_count_q);
        $upd_query = "UPDATE etiketi
                      SET
                        google_pos= :g_pos,
                        google_pr = :g_pr,
                        google_result= :g_res, 
                        active = :active, 
                        links    =:link_count,
                        date_checked=NOW(),
                        exact_url= :exact_url,
                        g_pos_adw= :g_pos_adw,
                        g_pos_org= :g_pos_adw_pos,
                        api_sID = 1    
                      WHERE eID=:eID
                      LIMIT 1";
        $stmt = $this->db->prepare($upd_query);
        $stmt->bindValue(":g_pos", $data['org_position']);
        $stmt->bindValue(":g_pr", $data['page_rank']);
        $stmt->bindValue(":g_res", $data['results']);
        $stmt->bindValue(":active", $data['active']);
        $stmt->bindValue(":link_count", $link_count);
        $stmt->bindValue(":exact_url", $data['exact_url']);
        $stmt->bindValue(":g_pos_adw", $data['adwords']);
        $stmt->bindValue(":g_pos_adw_pos", $data['adwords_position']);
        $stmt->bindValue(":eID", $data['eID']);
        $stmt->execute();
        return $this->db->lastInsertId(); 
    }

    public function  getPageGroups( $n, $sort_order = "rand()")
    {
        $stmt = $this->db->prepare("
            SELECT  SQL_CACHE * FROM pages_to_groups ptg
            LEFT JOIN groups g on ptg.group_id = g.group_id
            WHERE ptg.n = :n ORDER by :sort_order");
        $stmt->bindValue(":n", $n);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        $this->time = 60 * 60 * 24 * 5; // 5 days
        return $tmp;
    }


    protected function getData()
    {
        $stmt = $this->db->prepare(
            "SELECT * FROM Sites
             WHERE url LIKE :url
             ORDER BY SitesID ASC LIMIT 1");
        $stmt->bindValue(":url", "%".$_SERVER['SERVER_NAME']."%");
        $stmt->execute();
        $site = $stmt->fetch(PDO::FETCH_ASSOC);
        return $site;
    }

    public function isPageFromSameSitePermission($n)
    {
        $lang = null;
        if(isset($_GET['lang'])) {
            $lang = $_GET['lang'];
        }
        $page = $this->get_page($n, 'object', $lang);
        $site = (object) $this->getData();
        $page->textStr = iconv('cp1251', 'utf8', stripslashes($page->textStr));
        if ($page->SiteID != $site->SitesID){
            throw new Exception(sprintf('Pages %s dont exist!', $page->n));
        }
        return $page;
    }

    public function getParentPage($n,  $sort_order, $user_access_level)
    {
        $query = "
            SELECT * FROM pages p 
            LEFT JOIN images im ON p.imageNo = im.imageID
            WHERE p.ParentPage = :parent_page
            AND p.SecLevel <= :user_level
            ORDER by :sort_order";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":parent_page", $n);
        $stmt->bindValue(":user_level", $user_access_level);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $tmp[$row['n']] = $row;
        }
        return $tmp;
    }

    public function getSubContent($n,$link, $sort)
    {
        $sort_order = array(
            'ASC' => 'p.n ASC',
            'DESC' => 'p.n DESC',
            'p.Name DESC' => 'p.Name DESC',
            'p.Name ASC' => 'p.Name ASC',
            "p.date_added ASC" => "p.date_added ASC",
            "p.date_added DESC" => "p.date_added DESC",
            "p.toplink ASC" => "p.toplink ASC",
            "p.toplink DESC" => "p.toplink DESC",
            "RAND()" => "RAND()",
        );
        $subpages = $this->getParentPage(
                $n,
                $sort_order[$sort], 
                0);
        $show_link = array(
            0  => array(),
            1  => array('name'),
            2  => array('name','content','img'),
            3  => array('name','img'),
            4  => array('name','content2','img'),
            5  => array('content','img'),
            6  => array('content'),
            7  => array('img','content2'),
            8  => array('content2'),
            9  => array('img'),
            10 => array('name','content'),
            11 => array('name','content2'),
            13 => array('name','img','content2'),
            14 => array('content2', 'img'),
            15 => array('name', 'content2'),
        );

        $make_links = array(
            0 => array(),
            1 => array('name', 'content', 'img', 'content2'),
            2 => array('title'),
            3 => array('img'),
            4 => array('name', 'content', 'img', 'content2'),
        );
        $tmp = array();
        $data = array();
        $sub_content = array();
        $show_link = $show_link[$link];
        foreach($subpages as $page){
            $data['name'] = array('str'=>$page['Name'], 'link'=> '');
            $data['content'] = array($page['textStr'], 'link' => '');
            $data['img'] = array('str' => $this->get_pImage($page['n']), 'link' => '');
            $text = str_replace('</p>', '', $page['textStr']);
            $data['content2'] = array('str'=>$page['textStr'], 'link' => '');
            foreach($make_links[$page['make_links']] as $content){
                $data[$content]['link'] = $page['page_link'];
            }
            foreach($show_link as $k){
                $tmp[$k] = $data[$k];
            }
            $sub_content[] = $tmp;
        }
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $sub_content;
    }

    public function getPageJSON($n)
    {
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $this->isPageFromSameSitePermission($n);
    }

    public function getSubContentJSON($n)
    {
        $page = $this->isPageFromSameSitePermission($n);
        return $this->getSubContent($page->n, $page->show_link ,$page->show_link_order);
    }

    public function pagesList()
    {
        #$this->time = 60 * 60 * 24 * 5; //5 days
        #if(!isset($_SESSION['user']) or $_SESSION['user']->WriteLevel < 1) {
        #    return array();
        #}


        $this->lang_id =1;
        $sql = $this->pages_base_query(" AND p.SiteID=:SiteID");

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", 1);
        $stmt->execute();
        $tmp = array();
        while($page = $stmt->fetch(PDO::FETCH_OBJ)){
            $tmp[] = array('title' => $page->Name, 'value' => $this->get_pLink($page->n));
        }

        return $tmp;

    }
    
    public function getAllPages()
    {
        $site = $this->getData();
        $this->formatMenuData($site->SitesID);
        $this->time = 60 * 60 * 24 * 5; //2 weeks
    }

    public function formatMenuData($SiteID)
    {
        $pages = $this->pg->getAllPages($SiteID);
        // prepare special array with parent-child relations 
        $menuData = array( 
            'items' => array(), 
            'parents' => array() 
        ); 
        foreach($pages as $menuItem){ 
            $menuData['items'][$menuItem['n']] = $menuItem; 
            $menuData['parents'][$menuItem['ParentPage']][] = $menuItem['n']; 
        } 
        return $menuData;
    }

    #// 'get_group' => function($n) use ($api) { return $api->get_pGroupContent($n);},

    # public function get_pGroupContent($n){
    #     $tmp = array();
    #     $data = parent::get_pGroupContent($n);
    #     $i = 0;
    #     var_dump($data);
    #     foreach ($data as $page){
    #         foreach($page as $key => $val){
    #             $tmp[$i][$key] = iconv('cp1251', 'utf8', $val);
    #         }
    #         $i++;
    #     }
    #     return $tmp;
    # }

    public function get_pGroupContent($n, $limit=false, $lang=null)
    {
        $sql = "SELECT p.PHPcode, p.Name, p.Title, i.image_src, p.n, p.SiteID, p.TextStr, pr.price_description, pr.price_code, pr.price_pcs, pr.price_qty, pr.price_value
        FROM  `pages_to_groups` pg
        LEFT JOIN pages p ON pg.n = p.n
        LEFT JOIN images i ON i.imageID = p.imageNo
        LEFT JOIN prices pr ON pr.price_n = p.n
        WHERE pg.group_id =:group_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":group_id", $n);
        $stmt->execute();
        $tmp = array();
        $i = 0;
        $lang = isset($lang) ? $lang.'/' : 'page.php'; 
        $url = function($page) use ($lang) {
            return sprintf('%s?n=%s&SiteID=%s', $lang, $page['n'], $page['SiteID']);
        };
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            if($limit && $i == $limit) { break; }
            if(empty($row['image_src'])) {
                $row['image_src'] = '/web/admin/images/no_image.jpg';
            }
            $row['page_link'] = $url($row);
            $tmp[] = $row;
            $i++;
        }
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $tmp;
    }

    public function get_pGroupContentRandom($n, $limit=false)
    {
        $sql = "SELECT p.Name, p.Title, i.image_src, p.n, p.SiteID, p.TextStr, pr.price_description, pr.price_code, pr.price_pcs, pr.price_qty, pr.price_value
        FROM  `pages_to_groups` pg
        LEFT JOIN pages p ON pg.n = p.n
        LEFT JOIN images i ON i.imageID = p.imageNo
        LEFT JOIN prices pr ON pr.price_n = p.n
        WHERE pg.group_id =:group_id
        ORDER BY RAND()";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":group_id", $n);
        $stmt->execute();
        $tmp = array();
        $i = 0;
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            if($limit && $i == $limit) { break; }
            if(empty($row['image_src'])) {
                $row['image_src'] = '/web/admin/images/no_image.jpg';
            }
            $row['page_link'] = $this->get_pLink($row['n']);
            $tmp[] = $row;
            $i++;
        }
        return $tmp;
    }

    public function seo_etiketi_search($egID, $date_expire)
    {
        $sql = "SELECT * FROM etiketi WHERE  egID=:egID  AND `date_expire`  >= :date_expire ORDER BY Name ASC";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":egID", $egID);
        $stmt->bindValue(":date_expire", $date_expire);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $tmp;
    }

    public function etiketi_pos_monthly($eID)
    {
        $sql = "SELECT * FROM etiketi_pos  
                WHERE eID=:eID 
                AND date_checked > DATE_SUB( NOW( ) , INTERVAL 30 DAY)
                ORDER BY date_checked ASC 
                LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":eID", $eID);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $row;
    }

    public function etiketi_pos($eID, $limit=4)
    {
        $sql = "SELECT * FROM etiketi_pos
                WHERE eID=:eID 
                AND date_checked <> '0000-00-00 00:00:00'
                ORDER BY etiketi_pos.epID ASC
                LIMIT :limit";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":eID", $eID);
        $stmt->bindValue(":limit", (int) $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $tmp[] = $row;
        }
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $tmp;
    }

    public function newest_items($SiteID)
    {
        $sql = "
        SELECT pages.n, pages.SiteID, pages.Name, pages.TextStr, prices.price_pcs, prices.price_value, prices.price_qty, images.imageName, images.image_src, pages.preview, pages.date_added
        FROM pages
        JOIN prices ON price_n = pages.n
        JOIN images ON images.imageID = pages.ImageNo
        AND pages.SiteID = :SiteID
        AND pages.date_added > DATE_SUB( NOW( ) , INTERVAL 2 
        DAY ) 
        ORDER BY pages.date_added DESC"; 
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }
    
    public function top_items($SiteID){
        $sql = "SELECT pages.n, pages.SiteID, pages.Name, pages.TextStr, prices.price_pcs, prices.price_value, prices.price_qty, images.imageName, images.image_src, pages.preview 
                FROM pages
                JOIN prices ON price_n = pages.n
                JOIN images ON images.imageID = pages.ImageNo
                AND pages.SiteID = :SiteID
                GROUP BY pages.n
                ORDER BY pages.preview DESC 
                LIMIT 7";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $res->link = $this->get_pLink($res->n);
            $tmp[] = $res;
        }
        return $tmp;
    }

    # function dataScrapper()
    # {
    #     $ch = curl_init();
    #     curl_setopt($ch, CURLOPT_URL, $_GET['url']);
    #     curl_setopt($ch, CURLOPT_HEADER, 0);
    #     curl_setopt($ch, CURLOPT_ENCODING , "gzip");
    #     curl_setopt($ch, CURLOPT_VERBOSE,1);
    #     curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
    #     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    #     curl_setopt($ch, CURLOPT_POST,0);
    #     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 20);
    #     $data = curl_exec($ch);
    #     $curl_errno = curl_errno($ch);
    #     if ($curl_errno > 0) {
    #         throw new Exception("Error Processing Request cURL Error ($curl_errno):", 1);
    #     }
    #     curl_close($ch);
    #     return array("data"=>$data);
    # }

    public function getCartItems()
    {
        return ($_SESSION["cart"] ? count($_SESSION["cart"]) : 0 );
    }
    
    function tinyMceLinkList($dir){
        $tmp = array();
        $s_dir = '/hosting/maksoft/maksoft/web/pub/'.$dir.DIRECTORY_SEPARATOR;
        $pub_files = array();
        if(is_dir($s_dir)) {   
            if ($handle = opendir($s_dir)) 
                while (false !== ($file = readdir($handle))){
                    if ($file != "." && $file != ".." && $file!="index.php")  {
                       $file_info = pathinfo($file);
                       $file_type = $file_info['extension'];
                       array_push($pub_files, array('name'=>$file, 'ext'=>$type, 'size'=>filesize($fs_pub_dir."/".$file)/1024)); 
                    }
                }
                closedir($handle); 
                sort($pub_files);
        }
    
        for($i=1; $i<count($pub_files); $i++) { 
            // Use your correct (relative!) path here
            /*if($pub_files['name'] != "") $title = $pub_files['name'];
            else 
            */
            $title = basename($pub_files[$i]['name']); 
            
            //$abspath = preg_replace('~^/?(.*)/[^/]+$~', '/\\1', $_SERVER['SCRIPT_NAME']);
                        
            //$output .= $delimiter . "[\"".."\", \"". DIR_WS_PUB . $pub_dir . $pub_files[$i]['name']. "\"],";   
            $tmp[] = array("title" => $i . ". ". $title,
                           "value" => 'web/pub/'.$dir.DIRECTORY_SEPARATOR.$pub_files[$i]['name']);
         }
         return $tmp;
    }

    public function getQty($itype, $itype_tela, $cor=0)
    {
        $max_str = ""; 
        $doc_ID_val = 0; 
        $query_av_set = "SELECT sum(items.qty) as sold FROM `items` left join docs on items.fID=docs.fID WHERE ( items.itype=:itype OR items.itype=:tela) AND docs.docID>=:doc_id"; 
        $stmt = $this->db->prepare($query_av_set);
        $stmt->bindValue(":itype", $itype);
        $stmt->bindValue(":tela", $itype_tela);
        $stmt->bindValue(":doc_id", 0);
        $stmt->execute();
        $sold = array();
        $total_sold = 0;
        while( $res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $total_sold += $res->sold;
        }
        $stmt = $this->db->prepare("SELECT qty_on_stock FROM itemTypes WHERE  ( itID=:itype OR itID = :tela )");
        $stmt->bindValue(":itype", $itype);
        $stmt->bindValue(":tela", $itype_tela);
        $stmt->execute();
        $qty_on_stock = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $qty_on_stock[] = $res;
        }
        $qty_on_stock_tela = 0;

        if(count($qty_on_stock) > 1){
            $qty_on_stock_tela = $qty_on_stock[1];
            $qty_on_stock_tela = $qty_on_stock_tela->qty_on_stock;
        }
        $qty_on_stock = array_shift($qty_on_stock);
        $qty_on_stock = $qty_on_stock->qty_on_stock + $cor;
        
        $total_sold = round( $total_sold * 1.005, 0); // +0.5-1% lipsi
                 
        $qty_av = $qty_on_stock->qty_on_stock + $qty_on_stock_tela - $total_sold; // total available qty
        //$qty_av = $qty_av-$qty_on_stock*0.01-$qty_on_stock_tela*0.01;   // namaliavame realnite kolichestva s 1% 
         
        $qty_av_p = $qty_av/($qty_on_stock + $qty_on_stock_tela) * 100; 
        $qty_av_p = round($qty_av_p, 0); 

        $sold_p = 100-$qty_av_p; 
        $tmp = array();
        if ($qty_av<($qty_on_stock + $qty_on_stock_tela)*0.02) {
            $tmp['status'] = False;
        }
        else 
            $tmp['status'] = True;
            $qty_av_preview = round($qty_av/100, 0)*100-100; 
            $tmp['sold_perc'] = $sold_p;
            $tmp['qty_av_perc'] = $qty_av_p;
            $tmp['qty_pcs'] = $qty_av_preview;
        $this->time = 60 * 60 * 24 * 5; //2 weeks
        return $tmp;
    }

}
