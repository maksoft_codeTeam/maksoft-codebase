<html>
<head></head>

<body>
<div>
<h3> View live pages in maksoft.bg </h3>
<ul id="links">
    
</ul>
</div>
<script src="http://autobahn.s3.amazonaws.com/js/autobahn.min.js"></script>
<script type="text/javascript" src="/modules/web/components/push.js/push.min.js"></script>
<link rel="stylesheet" href="/Templates/vratzadnes/assets/alert/themes/alertify.core.css" />
<link rel="stylesheet" href="/Templates/vratzadnes/assets/alert/themes/alertify.default.css" id="toggleCSS" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="/Templates/vratzadnes/assets/alert/src/alertify.js"></script>
<script>
function trimByWord(sentence, wordCount) {
  var result = sentence;
  var resultArray = result.split(" ");
  if (resultArray.length > wordCount) {
    resultArray = resultArray.slice(0, wordCount);
    result = resultArray.join(" ") + "...";
  }
  return result.trim();
}

var visitedPages = [];
    var conn = new ab.Session('ws://79.124.31.189:8802',
        function() {
            conn.subscribe('livePages', function(topic, data) {
                // This is where you would add the new article to the DOM (beyond the scope of this tutorial)
                console.log('New article published to category "' + topic + '" : ' + data.title);
                console.log(data);
                $("#links").append("<li><a href=\""+data.link+"\"><h4>"+data.name+"</h4><img width=\"7%\" src=\"\\"+data.img+"\"></a>");
                // Push.create(data.name, {
                //     body: trimByWord(data.content, 15),
                //     icon: '/Templates/vratzadnes/favicon.ico',
                //     timeout: 5000,
                //     requireInteraction: true,
                //     onClick: function () {
                //         window.focus();
                //         this.close();
                //         location.href = data.link;
                //     }
                // });
            });
        },
        function() {
            console.warn('WebSocket connection closed');
        },
        {'skipSubprotocolCheck': true}
    );
</script>

</body>
</html>
