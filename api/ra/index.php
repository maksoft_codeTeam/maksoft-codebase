<?php
session_start();
require_once __DIR__."/../../modules/vendor/autoload.php";
require_once __DIR__."/../../lib/lib_page.php";
require_once __DIR__."/../../Templates/ra/container.php";
$o_page = new page();
$forms = array();
$container = new \Pimple\Container();
$container->register(new RaMaksoftLoader());
$gate = $container["gate"];
$client_ip = \Maksoft\Admin\FormCMS\Save::client_ip();
$cart = new \Maksoft\Cart\Cart("BG");
$cart->load($_SESSION[\Maksoft\Cart\Cart::getName()]);


$forms['add_product']        = new \Maksoft\Admin\Product\AddProduct($gate, $cart, $_POST); 
$forms['branding']           = new \Maksoft\Admin\Product\Brandit($gate, $cart, $_POST);
$forms['edit']               = new \Maksoft\Admin\Product\Edit($gate, $cart, $_POST);
$forms['remove']             = new \Maksoft\Admin\Product\Remove($gate, $cart, $_POST); 
$forms['create_calculation'] = new \Maksoft\Admin\Ra\Calculation($gate, $o_page, $cart, $_POST); 
$forms['delete_calculation'] = new \Maksoft\Admin\Ra\DeleteCalculation($gate, $o_page, $_POST); 
$forms['reset_calculation']  = new \Maksoft\Admin\Ra\ResetCalculation($_POST); 
$data = array();

if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["action"])){
    if(array_key_exists($_POST['action'], $forms)){
        $f = $forms[$_POST['action']];
        try{
            $f->is_valid();
            $data['message'] = $f->action();
            $data['cart'] = $cart->save();
        } catch (Exception $e ){
            $data['message'] = $e->getMessage();
        }
        $_SESSION[\Maksoft\Cart\Cart::getName()] = $data;
    }
} else {
    $data['cart'] = $cart->save();
}

echo json_encode($data);
