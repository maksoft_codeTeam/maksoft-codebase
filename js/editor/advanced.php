<!-- tinyMCE -->
<script language="javascript" type="text/javascript" src="js/editor/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "exact", // exact|: polzva tochno opredelen element
		elements : "text_editor", //ID-to na elementa, vurhu koito shte se izpulni redaktora
		theme : "advanced",  //  default, simple, advanced
		plugins : "table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,flash,searchreplace,print,contextmenu,paste,directionality,fullscreen",
		theme_advanced_buttons1_add_before : "save,newdocument,separator",
		theme_advanced_buttons1_add : "fontselect, fontsizeselect",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,separator,forecolor,backcolor",
		theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
		theme_advanced_buttons3_add_before : "tablecontrols,separator",
		theme_advanced_buttons3_add : "emotions,iespell,flash,advhr,separator,print,separator,ltr,rtl,separator,fullscreen",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_path_location : "bottom",
		content_css : "example_data/example_word.css",
	    plugin_insertdate_dateFormat : "%Y-%m-%d",
	    plugin_insertdate_timeFormat : "%H:%M:%S",
		extended_valid_elements : "a[name|href|target|title|onclick],img[usemap|class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style],map[name],area[shape|coords|href]",
		theme_advanced_styles : "  = ; ��� 1 [���-�����]=t1;��� 2 [�����]=t2;��� 3 [��������]=t3; ��� 4 [������]=t4;  = ; ����� [������]=big_text; ����� [��������]=small_text; = ; ������ [� �����]=border_link; ������ [������]=color_link; ������ [� ���]=bgcolor_link; ������ [������]=next_link; ������ [�����]=back_link; = ; �������� [�������]=main_image; �������� [� �����]=border_image; �������� [������]=link_image; = ;  ������� [�������] = main_table; ������� [� �����] =border_table; ��� [�������] =main_tr; ��� [� �����]=border_tr; ������ [�������]=main_td; ������ [� �����]=border_td; = ; ���� 1 = message_normal; ���� 2 = message_warning; ���� 3 = message_error;",
		external_link_list_url : "example_data/example_link_list.js",
		external_image_list_url : "example_data/example_image_list.js",
		flash_external_list_url : "example_data/example_flash_list.js",
		file_browser_callback : "mcFileManager.filebrowserCallBack",
		cleanup : false,
		paste_auto_cleanup_on_paste : true,
		paste_convert_headers_to_strong : true
	});

	function fileBrowserCallBack(field_name, url, type) {
		// This is where you insert your custom filebrowser logic
		alert("Filebrowser callback: " + field_name + "," + url + "," + type);
	}

	// Custom insert link callback, extends the link function
	function customInsertLink(href, target) {
		var result = new Array();

		alert("customInsertLink called href: " + href + " target: " + target);

		result['href'] = "http://www.sourceforge.net";
		result['target'] = '_blank';

		return result;
	}

	// Custom insert image callback, extends the image function
	function customInsertImage(src, alt, border, hspace, vspace, width, height, align) {
		var result = new Array();

		var debug = "CustomInsertImage called:\n"
		debug += "src: " + src + "\n";
		debug += "alt: " + alt + "\n";
		debug += "border: " + border + "\n";
		debug += "hspace: " + hspace + "\n";
		debug += "vspace: " + vspace + "\n";
		debug += "width: " + width + "\n";
		debug += "height: " + height + "\n";
		debug += "align: " + align + "\n";
		alert(debug);

		result['src'] = "logo.jpg";
		result['alt'] = "test description";
		result['border'] = "2";
		result['hspace'] = "5";
		result['vspace'] = "5";
		result['width'] = width;
		result['height'] = height;
		result['align'] = "right";

		return result;
	}

	// Custom save callback, gets called when the contents is to be submitted
	function customSave(id, content) {
		alert(id + "=" + content);
	}
</script>
<!-- /tinyMCE -->