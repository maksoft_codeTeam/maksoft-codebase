<?php
// sync command for cron 
//   rsync -zavuT --size-only sites/* maksoft@server.maksoft.net:~/public_html/render/
require_once "renderer.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/Database.class.php";

$select = "SELECT *  FROM `Sites` WHERE `url` LIKE :host";
$db = new Database();
$stmt = $db->prepare($select);
$stmt->bindValue(":host", "%".$_SERVER['SERVER_NAME']."%");
if($stmt->execute()){
    $res = $stmt->fetch(PDO::FETCH_OBJ);
    $SiteID = $res->SitesID;
    $b = new Renderer();
    $b->connect($SiteID);
    $b->download();
    $b->save();   
    return;
}

throw new Exception('OH NO! You broke THE INTERNET.');

