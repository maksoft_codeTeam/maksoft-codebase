<?php
require_once "settings.php";
require_once __DIR__.DIRECTORY_SEPARATOR.'Grabber.php';



class ConnectionError extends Exception {}


class Renderer
{
    private $db, $stmt, $dbh;
    public function __construct()
    {
        $this->db = new DataBase();
        $this->dbh = new DataBase();
        $this->dbh->beginTransaction();
    }

    public function connect($siteID)
    {
        $this->data = array("n"=>0, "siteID"=>$siteID, "date_modified"=>'1970');
        $last_updated = '1970';
        $max_updated_n=0;
        $stmt = $this->dbh->prepare(RENDER_SELECT_QUERY);
        $stmt->bindValue(":SiteID", $siteID);
        $stmt->execute();
        $site = $stmt->fetchAll();
        if(count($site) > 0):
            $last_updated = $site[0]["updated_at"];
            $max_updated_n = $site[0]["max_updated_n"];
        endif;
        $this->stmt = $this->db->prepare(PAGE_SELECT_QUERY);
        $this->stmt->bindValue(":SiteID", $siteID);
        $this->stmt->bindValue(":last_updated", $last_updated);
        $this->stmt->execute();
    }

    public function download()
    {
        $responce = $this->stmt->fetch(PDO::FETCH_OBJ);
        if($responce && $this->check_response($_SERVER['SERVER_NAME'])):
            while ($row=$this->stmt->fetch(PDO::FETCH_OBJ)) {
                $page_n = $row->n;
                $this->data["siteID"] = $row->SitesID;
                $this->data["n"] = $row->n;
                $this->data['date_modified'] = $row->date_modified;
                if(strlen($row->slug)>3)
                    $page_n = $row->slug;
                //$url = $row->primary_url.'/page.php?n='.$row->n.'&SiteID='.$row->SitesID;
                $url = $_SERVER['SERVER_NAME'].'/page.php?n='.$row->n.'&SiteID='.$row->SitesID;
                echo $url;
                if($page_n == $row->StartPage){
                    $page_n = "index";
                }
                $file_name = $page_n.'.html';
                $folder = $row->SitesID;
                $page_source = $this->page_source($url);
                $dom = new DomDocument;
                $dom->loadHTML(mb_convert_encoding($page_source, 'HTML-ENTITIES', 'cp1251'));
                $dom->preserveWhiteSpace = true;
                $image_grabber = new Grabber($dom, $row->SitesID);
                $image_grabber->grab();
                $image_grabber->grabCss();
                $image_grabber->grabJs();
                $dom = $this->editLinks($dom);
                $document->formatOutput=true;
                $path = DEFAULT_FOLDER.$folder.'/'.$file_name;
                $this->make_folder($folder);
                $this->save_file($path, html_entity_decode($dom->saveHTML($dom->getElement), ENT_QUOTES, "cp1251"));
                $this->insertRender();
                $img_preview_new_location = DEFAULT_FOLDER.$folder.'/img_preview.php';
                if(!file_exists($img_preview_new_location)){
                    copy('/hosting/maksoft/maksoft/img_preview.php', $img_preview_new_location); 
                }
            }
        endif;
    }

    public function editLinks($domDocumentInstance)
    {
        $xml = $domDocumentInstance;
        foreach($xml->getElementsByTagName('a') as $link) { 
               $oldLink = $link->getAttribute("href");
               $link->setAttribute('href', str_replace("http://".$_SERVER['SERVER_NAME'], "", $oldLink));
        }
        return $xml;
    }
    
    public function download_all($SiteID)
    {
        $select = "SELECT s.primary_url,s.url,p.date_modified, p.slug, p.ParentPage, p.n,s.SitesID
                        FROM `Sites` as s
                        JOIN pages as p ON p.SiteID = s.SitesID
                        WHERE s.url IS NOT NULL
                        AND s.SitesID=:SiteID
                        ORDER BY  `p`.`date_modified` ASC
                        LIMIT 500";
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $site_url = $_SERVER['SERVER_NAME'];
        while($row = $stmt->fetch(PDO::FETCH_OBJ)){
            $page_n=$row->n;
            if(strlen($row->slug)>3)
                $page_n = $row->slug;
            $url = $site_url.'/page.php?n='.$row->n.'&SiteID='.$row->SitesID;
            $page_source = $this->page_source($url);

            $path = DEFAULT_FOLDER.$row->SitesID.'/'.$page_n.'.html';
            $this->make_folder($folder);
            $this->save_file($path, $page_source);
        }
    }


    private function check_response($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERAGENT, MOZILLA_USER_AGENT);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, True);
        curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        if ($info['http_code']==200)
            return True;
        return False;
    }

    private function page_source($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_ENCODING , "gzip");
        curl_setopt($ch,CURLOPT_VERBOSE,1);
        curl_setopt($ch, CURLOPT_USERAGENT, MOZILLA_USER_AGENT);
        curl_setopt($ch, CURLOPT_REFERER,'http://www.google.com');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_POST,0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 20);
        return curl_exec($ch);
    }

    private function insertRender()
    {
        $insert = $this->dbh->prepare(RENDER_INSERT_QUERY);
        $insert->bindValue(":SiteID", $this->data['siteID']);
        $insert->bindValue(":max_updated_n", $this->data['n']);
        $insert->bindValue(":updated_at", $this->data['date_modified']);
        $insert->execute();
    }

    private function make_folder($folder_name)
    {
        if(!file_exists(DEFAULT_FOLDER.$folder_name.'/'))
            mkdir(DEFAULT_FOLDER.$folder_name.'/', 0755, true);
    }

    private function save_file($path, $content)
    {
        $h = fopen($path, 'w');
        fwrite($h, $content);
        fclose($h);
        echo 'successfully saved page in: <b>'.$path.'</b><br>';
    }

    public function save()
    {
        $this->dbh->commit();
    }
}


class Page
{
    public function __construct($page_source)
    {
        $this->source = $page_source;
    }
}
