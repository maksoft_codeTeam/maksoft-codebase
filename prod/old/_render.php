<?php

//page object classes
include_once($_SERVER['DOCUMENT_ROOT']."/lib/lib_page.php");


 $ob_file = fopen('test/test.html','w');

 function ob_file_callback($buffer)
 {
  global $ob_file;
  fwrite($ob_file,$buffer);
 }

 ob_start();

 include("page.php");
 ob_end_flush();

 fclose($ob_file);
// http://codeutopia.net/blog/2007/10/03/how-to-easily-redirect-php-output-to-a-file/
?>
