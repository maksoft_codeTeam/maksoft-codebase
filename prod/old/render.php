<?php

//page object classes
include_once('../lib/Database.class.php');
include_once('settings.php');
include_once($_SERVER['DOCUMENT_ROOT']."/lib/lib_page.php");
include_once($_SERVER['DOCUMENT_ROOT']."/lib/configure.php");
include_once($_SERVER['DOCUMENT_ROOT']."/lib/languages/bulgarian.php");
include_once($_SERVER['DOCUMENT_ROOT']."/lib/lib_functions.php");
include_once $_SERVER['DOCUMENT_ROOT']."/web/admin/table_class.php";
include_once($_SERVER['DOCUMENT_ROOT']."/web/admin/db/logOn.inc.php");
include_once($_SERVER['DOCUMENT_ROOT']."/web/admin/query_set.php");
include_once($_SERVER['DOCUMENT_ROOT']."/web/admin/io.php");
include_once($_SERVER['DOCUMENT_ROOT']."/web/resize_image.php");

function ob_file_callback($buffer)
{
    global $ob_file;
    fwrite($ob_file,$buffer);
}
$db = new Database();
$stmt = $db->prepare(PAGE_SELECT_QUERY);
$stmt->bindValue(":SiteID", 999);
$stmt->bindValue(":last_updated", '1980');
$stmt->execute();
$o_site = new site();
$o_page = new page();
$SiteID = $o_site->SiteID;
$site_pages = array();
while($row=$stmt->fetch(PDO::FETCH_OBJ)):
    if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/prod/sites/'.$row->SitesID.'/'))
       mkdir($_SERVER['DOCUMENT_ROOT'].'/prod/sites/'.$row->SitesID.'/', 0755, true);
    $path = $_SERVER['DOCUMENT_ROOT'].'/prod/sites/'.$row->SitesID.'/'.$row->n.'.html';
    echo $path."<br>";
    $ob_file = fopen($path,'w');
    $_GET['n']=$row->n;
    $o_site = new site();
    $o_page = new page($row->n);
    $SiteID = $o_site->SiteID;
    $n = $o_page->n;
    echo $o_page->get_pTitle();
    $_GET['SiteID']=$row->SitesID;
    ob_start('ob_file_callback');
    $h = fopen('page_render.php', 'rb');
    $code = fread($h, filesize('page_render.php'));
    fclose($h);
    #include ('page_render.php');
    eval($code);
    ob_end_flush();
    #fclose($ob_file);
endwhile;
// http://codeutopia.net/blog/2007/10/03/how-to-easily-redirect-php-output-to-a-file/
?>

