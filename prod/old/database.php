<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Radoslav Yordanov
 */


date_default_timezone_set('Europe/Sofia');
define('NOW', date("Y-m-d H:i:s"));

class Database extends PDO {

    private static $dsn;
    private static $username;
    private static $passwd;
    private static $db_name;
    private static $table_name;

    public function __construct($dsn = NULL, $username = NULL, $passwd = NULL, $db_name = NULL) {

        self::$dsn = ($dsn === NULL) ? 'localhost' : $dsn;
        self::$username = ($username === NULL) ? 'maksoft' : $username;
        self::$passwd = ($passwd === NULL) ? 'mak211' : $passwd;
        self::$db_name = ($db_name === NULL) ? 'maksoft' : $db_name;
        parent::__construct("mysql:host=" . self::$dsn . ";dbname=" . self::$db_name . "", self::$username, self::$passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        $this->exec("set names cp1251");
    }

    public function set_table_name($table_name) {
        self::$table_name = $table_name;
    }

    public function create_table($table_name){
        $sql = "CREATE TABLE IF NOT EXISTS ".$table_name." (
                p_id INT(11) NOT NULL AUTO_INCREMENT,
                created_at varchar(50) default '0000-00-00 00:00:00',
                updated_at timestamp default now() on update now(),
                PRIMARY KEY (p_id)
                ) ENGINE=InnoDB";
        try {
            $this->exec($sql);
        } catch (PDOException $e) {
            
        }
    }

    public function make_columns($table_name='', $data, $type = 'varchar(255)') {
        if(empty($table_name)):
            $table=  self::$table_name;
        else:
            $table=$table_name;
        endif;
        $i = 0;
        $this->create_table($table_name);
        $array = $data;
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if(is_array($value)):
                    $this->make_columns($table_name, $value);
                endif;
                try {
                    $sql = "ALTER TABLE $table ADD `$value` " . $type . " CHARACTER SET utf8 COLLATE utf8_general_ci";
                    $this->exec($sql);
                } catch (PDOException $e) {

                }
                $i++;
            }
        } else {
            try {
                $sql2 = "ALTER TABLE $table ADD $array $type CHARACTER SET utf8 COLLATE utf8_general_ci";
                $this->exec($sql2);
            } catch (PDOException $e) {
                
            }
        }
    }


    public function is_column_exists($column, $table) {
        try {
            $sql = "SELECT * FROM information_schema.COLUMNS 
                    WHERE TABLE_NAME    =  '" . $table . "' 
                    AND TABLE_SCHEMA     = '" . self::$db_name . "'
                    AND COLUMN_NAME     = '" . $column . "'";
            $result = $this->getQueryResult($sql);
            $t = count($result);
            } catch (Exception $e) {
        }
        if ($t > 0):
            return True;
        endif;
        return False;
    }

    private function getQueryResult($sql) {
        $sth = $this->prepare($sql);
        $sth->execute();
        $result = $sth->fetchAll();
        return $result;
    }

    public function create_and_insert($items, $table_name){
        $columns = array_keys($items);
        $this->create_table($table_name);
        $sql = "INSERT INTO ".$table_name." (created_at, ".implode(',', $columns).") VALUES (:created_at, :".implode(', :', $columns).")";
        $stmt = $this->prepare($sql);
        $this->make_columns($table_name, $columns);
        $stmt->bindValue(':created_at', NOW);
        foreach ($columns as $column):
            $stmt->bindValue(':'.$column, $items[$column]);
        endforeach;
        try {
            $stmt->execute();
        } catch (PDOException $e) {
            $this->exc_handler($e, $table_name);
        }
        $stmt->execute();
    }

    private function exc_handler($exception, $table_name){
        preg_match_all("/(column+[\s]+['][\w]+['])/", $exception, $output_array);
        $a = explode(' ', $output_array[0][0]);
        $a = str_replace("'", "", $a[1]);
        var_dump($a);
        $this->make_columns($table_name, $a);
        $this->exec("ALTER TABLE $table_name ADD $a TEXT CHARACTER SET utf8 COLLATE utf8_general_ci");
        return $a;
    }

    public function __destruct() {
        
    }

}
