<?php

// $str    pulniat text i snimki koiuto sledva da se vizualizirat
// $text_only    - samo teksta
//  $lnk[$i]  - masiv s vruzkite kum podstranicite
//      - $lnk[$i]->str

$page_extra_length = 2048;
// $str = "";                promenlivata $str moje da prieme stoinost kato PARAMETUR


//------------------------------------------------------------------------------------
// vzima sudurjanieto na stranica po parametri
// vrushta $str sudurjanie

global $lnk;


//checking IP address
if ($HTTP_X_FORWARDED_FOR)
    $ip = $HTTP_X_FORWARDED_FOR;
else
    $ip = $REMOTE_ADDR;


/* ############# NEW CMS CONTENT #######################*/

//$ip == "195.234.238.226" ; $ip =="89.190.198.100" ; $user->ID==196

/*
I.  Add links
1-  add links

II. Add content
2-  title + picture + text
5-  picture + text
6-  text
10- title+ text 

III.Photoalbum
3-  title + picture
9-  picture

IV.Announce
4-  title + picture + paragraph
7-  picture + paragraph
8-  paragraph
11- title+ paragraph 

V.  Subpage links
13- title + picture + subpage links
14- picture + subpage links
15-     title+ subpage links 


VI. No links
0- no links
*/

function get_content2($page, $ppage, $Site, $show_link, $show_link_cols, $wdir, $user)
{
    
    //$ppage is the parent page - the page that contents all the subpages - $page
    
    /*####################
    1. initialize temporary variables
    ####################*/
    
    $o_page = new page();
    
    //temporary subpage title
    $str_title         = "";
    //temporary subpage image
    $str_img           = "";
    //temporary subpage full text
    $str_text          = "";
    //page date_added
    $str_date          = "";
    //temporary subpage 1-st paragraph
    $str_paragraph     = "";
    //more link text
    $str_more          = "";
    //subpage item
    $str_subpage       = "";
    //subpage links
    $str_subpage_links = "";
    //alternative text
    $str_alt           = $page->Name;
    //link to the subpage
    $str_link          = "page.php?n=$page->n&amp;SiteID=$Site->SitesID";
    $str_link          = $o_page->get_pLink($page->n);
    //get_pInfo("page_link");
    
    
    //admin message
    $str_admin_message = "";
    //page price
    global $add_to_cart;
    $str_price = "<div style=\"display: block\" id=\"str_price\">" . $o_page->output_page_prices($page->n, $page->SiteID, "ALL", $add_to_cart) . "</div>";
    
    //page date
    if (($ppage->n == $Site->news || $page->ParentPage == $Site->news || $ppage->ParentPage == $Site->news) && $Site->news != $Site->StartPage)
        $str_date = "<div class=\"date\">" . date("d.m.Y", strtotime($page->date_added)) . "</div>";
    
    //get the page text
    $str_text = $page->textStr;
    if (strlen($str_text) <= 3 && $user->AccessLevel > 0)
        $str_text = "<br><em>page content empty</em>";
    
    //get the page 1-st paragraph
    $str_paragraph = crop_text($str_text);
    
    if ($ppage->make_links == 1) {
        //clean up the text from internal links
        $str_text      = strip_tags($str_text, "<hr><b><strong><br><p><ul><li><img><i><table><tr><td>");
        $str_text      = "<a href=" . $str_link . " title=\"" . $str_alt . "\">" . $str_text . "</a>";
        $str_paragraph = "<a href=" . $str_link . " title=\"" . $str_alt . "\">" . $str_paragraph . "</a>";
    }
    
    //get page more link
    $str_more = "<a href=\"" . $str_link . "\" class=\"next_link\"  rel=\"nofollow\">" . $Site->MoreText . "</a>";
    
    //get subpage title / title link
    $str_title = $page->Name;
    if ($ppage->make_links != 0) {
        //if(strlen($page->slug)>2) {
        //  $str_title = "<a href=\"http://$page->slug.$page->n.".$o_page->get_sURL()."\" class=\"main_link\" title=\"".$str_alt."\"><strong>".$str_title."</strong></a>".$str_date;
        //$str_title = "<a href=\"page.php?n=$page->n&amp;SiteID=$Site->SitesID\" class=\"main_link\" title=\"".$str_alt."\"><strong>".$str_title."</strong></a>".$str_date;
        //} else { // default
        // $str_title = "<a href=\"page.php?n=$page->n&amp;SiteID=$Site->SitesID\" class=\"main_link\" title=\"".$str_alt."\"><strong>".$str_title."</strong></a>".$str_date;
        $str_title = "<a href=\"$str_link\" class=\"main_link\" title=\"" . $str_alt . "\"><strong>" . $str_title . "</strong></a>" . $str_date;
        //}
    }
    
    //get the page image         
    if (!(empty($page->image_src)) && ($page->image_allign < 20) && file_exists($page->image_src)) {
        $query_img = mysql_query("SELECT * FROM images i, alligns a WHERE  i.imageID = '$page->imageNo' AND a.allignID = '$page->image_allign' ");
        $img       = mysql_fetch_object($query_img);
        
        //get the page name when image has none
        if (strlen($img->imageName) == 0)
            $img->imageName = $page->Name;
        
        //get image physical image size
        $size            = GetImageSize($page->image_src);
        $str_image_width = $size[0];
        $str_img_align   = $img->allignPos;
        
        //get MAIN_WIDTH variable from tmpl_vars, initPHP or row->PHP
        global $MAIN_WIDTH, $MENU_WIDTH, $HTTP_COOKIE_VARS;
        
        if (isset($MAIN_WIDTH)) {
            
            //MAIN_WIDTH = 100%
            if (eregi("%", $MAIN_WIDTH)) {
                //get screen resolution cookie
                $str_main_width = $HTTP_COOKIE_VARS["users_resolution_width"];
                
            } else
                $str_main_width = (int) $MAIN_WIDTH;
            
            //$str_main_width = (int)($MAIN_WIDTH);
        } else
            $str_main_width = 550;
        
        $ppage->show_link_cols = (int) $ppage->show_link_cols;
        
        if ($ppage->show_link_cols == 1 || $ppage->show_link_cols == 0) {
            /*
            if($size[0]<=$str_main_width)
            $str_img_width = $size[0];
            else $str_img_width = $str_main_width;
            */
            $str_col_width = $str_main_width;
        } else {
            $str_col_width = (int) ($str_main_width / $ppage->show_link_cols - 25);
            //$str_img_width = $str_col_width;
        }
        
        //if image is extended - get real size
        if ($str_col_width > $size[0])
            $str_img_width = $size[0];
        else
            $str_img_width = $str_col_width;
        
        //reduce img width and img align when we have picture + text, picture + paragraph, picture + subpage links
        if ($ppage->show_link != 3 && $ppage->show_link != 6 && $ppage->show_link != 9 && $ppage->show_link != 8 && $ppage->show_link != 0) {
            if ((strlen($page->textStr) > 0 || $ppage->show_link == 13 || $ppage->show_link == 14 || $ppage->show_link == 15) && $page->image_allign != 1) {
                if ($str_img_width >= (int) ($str_col_width / 2))
                    $str_img_width = (int) ($str_col_width / 2);
                //$str_img_align = "left";
            } else {
                $str_img_align = "default";
            }
        }
        
        $picture  = pathinfo($page->image_src);
        $pic_name = $picture["basename"];
        $pic_dir  = $picture["dirname"];
        $pic_file = $pic_dir . "/" . $pic_name;
        
        //$str_img = "<img src=\"http://maksoft.net/".$page->image_src."\" class=\"border_image\" border=0 align=".$str_img_align." alt=\"".$img->imageName."\" width=".$str_img_width.">";
        //use img_preview.php to generate image     
        
        //NOT WORKING WITH CYR FILENAMES
        //$str_img = "<img src=\"http://www.maksoft.net/img_preview.php?pic_name=".$pic_name."&amp;pic_dir=".$pic_dir."&amp;img_width=".$str_img_width."\" class=\"main_image\" border=\"0\" align=".$str_img_align." alt=\"".$img->imageName."\" >";
        
        //WORKING WITH CYR FILENAMES
        $str_img = "<img src=\"http://www.maksoft.net/img_preview.php?image_file=" . $pic_file . "&amp;img_width=" . $str_img_width . "\" class=\"main_image\" border=\"0\" align=" . $str_img_align . " alt=\"" . $img->imageName . "\" >";
        
        if ($ppage->make_links == 1)
            $str_img = "<a href=\"" . $str_link . "\" title=\"" . $str_alt . "\">" . $str_img . "</a>";
        
        if ($ppage->make_links == 3)
            $str_img = "<a href=\"javascript:void(0)\" onClick=\"popImage('" . $pic_dir . "/" . $pic_name . "','" . $img->imageName . "')\" title=\"" . $str_alt . "\">" . $str_img . "</a>";
        
        if ($ppage->make_links == 4)
            $str_img = "<a href=\"http://www.maksoft.net/" . $pic_dir . "/" . $pic_name . "\" rel=\"lightbox[gallery]\" title=\"$img->imageName\"><img src=\"http://www.maksoft.net/img_preview.php?pic_name=" . $pic_name . "&amp;pic_dir=" . $pic_dir . "&amp;img_width=" . $str_img_width . "\" class=\"border_image\" border=\"0\" align=\"" . $str_img_align . "\" alt=\"" . $img->imageName . "\" ></a>";
        
    }
    //user see empty image container
    else {
        //user is loged and have access to write to the page -> see an empty image message
        
        if (($user->WriteLevel >= $ppage->SecLevel) && ($user->AccessLevel > 0)) {
            $str_empty_img = "<img src=\"http://maksoft.net/web/admin/images/no_picture.jpg\" class=\"main_image\" border=\"0\" alt=\"no image. you won't see this when you logout\" width=\"75\" height=\"75\"><br>";
            if ($ppage->make_links == 1)
                $str_empty_img = "<a href=\"" . $str_link . "\" title=\"" . $str_alt . "\">" . $str_empty_img . "</a>";
        }
        
    }
    
    
    //get the admin message
    if ($user->AccessLevel > 0 && $ppage->n == 11) {
        //generate random row
        //mysql_query("SELECT COUNT(*) AS max FROM pages");
        //$limit = rand(1, $max);
        
        $admin_message_res = mysql_query("SELECT * FROM pages WHERE ParentPage = '12010' AND SecLevel = '0' ORDER by RAND()");
        
        $admin_message     = mysql_fetch_object($admin_message_res);
        $str_admin_message = "<div class=\"message_normal\" id=\"message_normal\"><a href=\"page.php?n=$admin_message->n&amp;SiteID=1\" target=_blank class=\"more_link\"><b>" . $admin_message->Name . "</b></a><br>
                " . crop_text($admin_message->textStr) . " <a href=\"page.php?n=$admin_message->n&amp;SiteID=1\" target=_blank class=\"more_link\"><b>" . $Site->MoreText . "</b></a></div>";
    }
    
    if ($user->AccessLevel > 0) {
        $str_admin_message = $o_page->user_message() . $str_admin_message;
        /*
        $tasks_query = mysql_query("SELECT * FROM tasks WHERE to_user='$user->ID' AND completed='0' AND priority='1'  AND start_date<NOW()  "); 
        $num_tasks = mysql_num_rows($tasks_query); 
        if ($num_tasks>0) {
        if ($num_tasks>1) {
        $num_tasks_str = " $num_tasks ñïåøíè çàäà÷è "; 
        } else {
        $num_tasks_str = " ñïåøíà çàäà÷à "; 
        }
        
        $str_admin_message  = "<div class=\"message_warning\" id=\"message_warning\"><a href=\"/page.php?n=85548&amp;SiteID=$SiteID\">$user->Name èìàòå $num_tasks_str ! </a></div>"; 
        
        }
        $user_sites_query = mysql_query("SELECT * FROM SiteAccess  left join Sites on SiteAccess.SiteID=Sites.SitesID WHERE userID='$user->ID'  AND notifications>0 "); 
        while($user_sites_row = mysql_fetch_object($user_sites_query)) {
        $forms_query = mysql_query("SELECT * FROM forms WHERE Data>DATE_SUB(CURDATE(),INTERVAL 12 HOUR) AND SiteID=$user_sites_row->SiteID AND ftype=0  "); 
        $num_forms = mysql_num_rows($forms_query); 
        if($num_forms>0) {
        $num_text="íåîáðàáîòåíè çàïèòàíèÿ"; 
        if($num_forms==1) $num_text="íåîáðàáîòåíî çàïèòâàíå"; 
        $str_admin_message  = " $str_admin_message<div class=\"message_warning\" id=\"message_warning\"><a href=\"/page.php?n=6031&amp;SiteID=$user_sites_row->SiteID\">èìà $num_forms $num_text â ñàéò $user_sites_row->Name</a></div>"; 
        } // num_forms>0
        }
        
        */
    }
    
    // get the page subpage links   
    if ($ppage->show_link == 13 || $ppage->show_link == 14 || $ppage->show_link == 15) {
        $sub_query = mysql_query("SELECT * FROM pages WHERE ParentPage='$page->n' ORDER by sort_n ASC");
        while ($sub_links = mysql_fetch_object($sub_query))
            $str_subpage_links .= "<br>&raquo; <a href=\"page.php?n=$sub_links->n&amp;SiteID=$Site->SitesID\" class=\"subpage_link\">" . $sub_links->Name . "</a>";
    }
    
    
    /*############################
    2. construct subpage item content
    ############################*/
    
    //1.    title
    if ($ppage->show_link == 1)
        $str_subpage .= "<li>" . $str_title . $str_price;
    
    //2.    title + picture + text
    if ($ppage->show_link == 2) {
        if (strlen($str_img) <= 0)
            $str_img = $str_empty_img;
        $str_subpage .= "<table width=\"100%\"><th align=\"left\">" . $str_title . "</th>";
        //image is background
        if ($page->image_allign == 22)
            $str_subpage .= "<tr><td valign=top  style=\"background: url(http://maksoft.net/" . $page->image_src . ")\">" . $str_img . $str_paragraph . "</td></tr>";
        else
            $str_subpage .= "<tr><td valign=top >" . $str_img . $str_text . "</td></tr>";
        $str_subpage .= "</table>" . $str_price;
    }
    
    //5.    picture + text
    if ($ppage->show_link == 5 && (strlen($str_text) > 0 || strlen($str_img) > 0)) {
        if (strlen($str_img) <= 0)
            $str_img = $str_empty_img;
        $str_subpage .= "<table class=\"main_table\"><tr><td>";
        $str_subpage .= $str_img . $str_text . "</td></tr></table>" . $str_price;
    }
    //6.    text
    if ($ppage->show_link == 6) {
        /*
        if($ppage->make_links == 1)
        $str_subpage.="<div onMouseOver=\"javascript: this.className='bgcolor_link';\" onMouseOut=\"this.className=''\" width=\"100%\">".$str_text."</div>";
        else
        $str_subpage.="<div class=\"main_table\" width=\"100%>".$str_text."</div>";
        */
        $str_subpage .= $str_text . $str_price;
    }
    
    //10.   title + text
    if ($ppage->show_link == 10) {
        if ($ppage->make_links == 1)
            $str_subpage .= "&raquo; " . $str_title . "<div width=\"100%\">" . $str_text . "</div>" . $str_price;
        else
            $str_subpage .= "&raquo; " . $str_title . "<div class=\"main_table\">" . $str_text . "</div>" . $str_price;
    }
    
    //3.    title + picture
    if ($ppage->show_link == 3) {
        if (strlen($str_img) <= 0)
            $str_img = $str_empty_img;
        $str_subpage .= "<table class=\"main_table\" width=\"100%\"><thead><th>" . $str_title . "</th></thead>";
        $str_subpage .= "<tbody><tr><td valign=top align=center>" . $str_img . "</td></tr></tbody></table>" . $str_price;
    }
    
    //9.    picture
    if ($ppage->show_link == 9) {
        if (strlen($str_img) <= 0)
            $str_img = $str_empty_img;
        $str_subpage .= "<table width=\"100%\">";
        $str_subpage .= "<tr><td valign=top align=center>" . $str_img . "</td></tr></table>" . $str_price;
    }
    
    //4.    title + picture + paragraph
    if ($ppage->show_link == 4) {
        if (strlen($str_img) <= 0)
            $str_img = $str_empty_img;
        $str_subpage .= "<table width=\"100%\"><th align=left>" . $str_title . "</th>";
        $str_subpage .= "<tr><td valign=top align=justify>" . $str_img . $str_paragraph . $str_more . "</td></tr></table>" . $str_price;
    }
    
    //7.    picture + paragraph
    if ($ppage->show_link == 7) {
        if (strlen($str_img) <= 0)
            $str_img = $str_empty_img;
        $str_subpage .= $str_img . $str_paragraph . $str_more . $str_price;
    }
    
    //8.    paragraph
    if ($ppage->show_link == 8) {
        if ($ppage->make_links == 1)
            $str_subpage .= "<div width=\"100%\">" . $str_paragraph . "</div>" . $str_more . $str_price;
        else
            $str_subpage .= "<div class=\"main_table\">" . $str_paragraph . "</div>" . $str_more . $str_price;
        
    }
    
    //11.   title + paragraph
    if ($ppage->show_link == 11) {
        if ($ppage->make_links == 1)
            $str_subpage .= "&raquo; " . $str_title . "<div width=\"100%\">" . $str_paragraph . "</div>" . $str_more . $str_price;
        else
            $str_subpage .= "&raquo; " . $str_title . "<div class=\"main_table\">" . $str_paragraph . "</div>" . $str_more . $str_price;
        
    }
    
    
    //13.   title + picture + subpage links
    if ($ppage->show_link == 13) {
        if (strlen($str_img) <= 0)
            $str_img = $str_empty_img;
        $str_subpage .= "<table width=\"100%\"><th align=left>" . $str_title . "</th>";
        $str_subpage .= "<tr><td valign=top align=justify>" . $str_img . $str_subpage_links . "</td></tr></table>";
    }
    
    //14.   picture + subpage links
    if ($ppage->show_link == 14) {
        if (strlen($str_img) <= 0)
            $str_img = $str_empty_img;
        $str_subpage .= $str_img . $str_subpage_links;
    }
    
    //15.   title + subpage links
    if ($ppage->show_link == 15) {
        $str_subpage .= "&raquo; " . $str_title . "<div class=\"main_table\">" . $str_subpage_links . "</div>";
    }
    
    //if page has to genrate its own conent: $ppage == $page
    
    if ($page->n == $ppage->n) {
        //get the current page text
        $str_text = $page->textStr;
        
        if (!(empty($page->image_src)) && ($page->image_allign < 20)) {
            if ($img->allignPos == "left")
                $margin = "margin-right: 25px;";
            if ($img->allignPos == "right")
                $margin = "margin-left: 25px;";
            if ($page->imageWidth == 0)
                $str_img = "";
            else if ($img->allignPos == "center")
                $str_img = "<img src=\"http://maksoft.net/img_preview.php?image_file=" . $img->image_src . "&amp;img_width=" . ($page->imageWidth - 3) . "\" class=\"main_image\" style=\"" . $margin . "\" id=\"main_image\" border=\"0\" alt=\"" . $img->imageName . "\" >";
            else
                $str_img = "<img src=\"http://maksoft.net/img_preview.php?image_file=" . $img->image_src . "&amp;img_width=" . ($page->imageWidth - 3) . "\" class=\"main_image\" style=\"" . $margin . "\" id=\"main_image\" border=\"0\" align=" . $img->allignPos . " alt=\"" . $img->imageName . "\" >";
            if ($ppage->make_links == 4 && $page->imageWidth > 0)
                $str_img = "<a href=\"http://www.maksoft.net/" . $img->image_src . "\" rel=\"lightbox[gallery]\" title=\"$img->imageName\" ><img src=\"http://maksoft.net/img_preview.php?image_file=" . $img->image_src . "&amp;img_width=" . ($page->imageWidth - 3) . "\" style=\"" . $margin . "\"  border=0 class=\"border_image\" id=\"main_image\" align=" . $img->allignPos . " alt=\"" . $img->imageName . "\" ></a>";
            
        } else
            $str_img = "";
        
        //default image align
        if ($img->allignPos == "default")
            $str_img .= "<br>";
        
        //center image align
        if ($img->allignPos == "center")
            $str_img = "<div align=\"center\">" . $str_img . "</div>";
        
        //if($img->valign == "top")
        $str_subpage = $str_admin_message . $str_img . $str_text;
        
        if ($img->valign == "bottom")
            $str_subpage = $str_admin_message . $str_text . "<div>" . $str_img . "</div>";
        
        //image is a background
        if ($page->image_allign == 22) {
            $bg_style = "";
            if ($page->imageWidth == 0)
                $bg_style = "background-image: url(http://maksoft.net/" . $page->image_src . ");";
            else
                $bg_style = "background-image: url(http://maksoft.net/" . $page->image_src . ");";
            $str_subpage = "<table width=\"100%\" class=\"main_table\" border=0 cellspacing=0 cellpadding=0 style=\"" . $bg_style . "\"><tr><td valign=top>" . $str_text . "</td></tr></table>";
        }
        
    }
    
    //return subpage item content
    return $str_subpage;
}

/*####################
3. display page content
####################*/

    
    if ($user->ReadLevel >= $row->SecLevel) {
        //position of the subpage links (default = top) 
        if (!isset($page_links_position))
            $page_links_position = "bottom";
        
        //read current page content
        $page_str .= get_content2($row, $row, $Site, 0, 1, $wdir, $user);
        
        //place page content above subpage links
        if ($page_links_position == "bottom")
            $str .= $page_str;
        
        if ($row->show_link_order == "ASC" || $row->show_link_order == "DESC")
            $sort_order = "p.sort_n " . $row->show_link_order;
        else
            $sort_order = $row->show_link_order;
        
        //output selected page subcontent
        if ($row->show_group_id == 0)
            $page_result = mysql_query("SELECT * FROM pages p left join images im on p.imageNo = im.imageID WHERE ParentPage = '$row->n' AND status>=0 ORDER BY $sort_order");
        
        //output selected group subcontent
        else
            $page_result = mysql_query("SELECT * FROM pages p left join images im on p.imageNo = im.imageID, pages_to_groups ptg WHERE ptg.group_id = '" . $row->show_group_id . "' AND ptg.n = p.n AND p.status >=0 ORDER BY ptg.sort_order ASC");
        
        $i = 1;
        //hide content if no subpages or hidelinks options selected
        if ($row->show_link != 0) {
            $str .= "<table cellspacing=0 cellpadding=5 class=\"main_table\" border=0><tr valign=top>";
            
            if (!isset($row->show_link_cols) || $row->show_link_cols <= 0)
                $row->show_link_cols = 1;
            $cell_width = ((int) (100 / $row->show_link_cols)) . "%";
            //$cell_width = "100%";
            while ($page = mysql_fetch_object($page_result)) {
                //check for user access
                if ($page->SecLevel <= $user->ReadLevel) {
                    if ($i > $row->show_link_cols) {
                        $str .= "<tr><td width=\"" . $cell_width . "\" valign=top>";
                        $i = 1;
                    } else
                        $str .= "<td width=\"" . $cell_width . "\" valign=top>";
                    
                    $str .= get_content2($page, $row, $Site, $row->show_link, $row->show_link_cols, $wdir, $user);
                    $i++;
                }
            }
            $str .= "</table>";
        }
        //get content of the current page
        //place page content above subpage links
        if ($page_links_position == "top")
            $str .= $page_str;
        //$str.="<hr>";
        if ($add_to_cart == true && $o_page->get_page_price($n, $SiteID, "", true))
            $str .= "<hr><div>" . $o_page->output_page_prices($n, $SiteID, "ALL", $add_to_cart) . "</div>";
    
    function make_links($links, $from_page, $user, $Site, $level)
    {
        if ($StartPage == 0) {
            $StartPage = $Site->StartPage;
        }
        
        if ($level <= 1) {
            $level       = $level + 1;
            $query_text  = "SELECT * FROM pages WHERE ParentPage = '$from_page' AND SecLevel <= '$user->ReadLevel' ORDER BY n ASC ";
            $links       = "$links";
            $query_pages = mysql_query(" $query_text ");
            
            //reading the links
            while ($row = mysql_fetch_object($query_pages)) {
                $indent = 0;
                for ($i = 0; $i <= $level; $i++) {
                    $indent = $indent + 10;
                }
                
                //$indent = "&raquo; "; 
                
                if ($level < 2) {
                    $row->Name = "<b>$row->Name</b>";
                }
                $links .= "<a href=\"page.php?n=$row->n&amp;SiteID=$Site->SitesID\" class=\"main_link\" style=\"display:block; text-indent: " . $indent . "px;\">&raquo; $row->Name</a>";
                //recursion
                $links = make_links($links, $row->n, $user, $Site, $level, 0);
            }
        }
        
        return $links;
    }
    
    // show standart related links as a main menu 
    $links = make_links($links, $Site->StartPage, $user, $Site, 0);
    
}
/* ################ END CMS CONTENT #######################*/

// show bottom navigation bar 
if ((strlen($str) > $page_extra_length)) {
    $botom_nav_bar = "";
}
if ($row->form > 0) {
    $str = "<form action=\"/form-cms.php?n=$row->form&amp;SiteID=$SiteID\" method=\"post\" enctype=\"multipart/form-data\" name=\"generated_from_page_$row->n\"> $str </form>";
}

/*mpetrov 03.07.2013*/
//test page template option
/*
if($user->ID == 196 )
{
$pt_sql = "SELECT * FROM page_templates pt WHERE n = '$n' LIMIT 1";
$pt_result = mysql_query($pt_sql);
$pt = mysql_fetch_array($pt_result);
echo $pt['page_template']; 
if($pt['page_template'])
include $pt['page_template'];
else
include("$Template->URL");
}
else
*/
echo $Template->URL;
include_once("$Template->URL");
?>

