<?php
require_once "settings.php";
require_once "database.php";
//directory names
require_once($_SERVER["DOCUMENT_ROOT"]."/lib/configure.php");

//CMS language package
require_once($_SERVER["DOCUMENT_ROOT"]."/lib/languages/bulgarian.php");

//page object classes


class ConnectionError extends Exception {}


class Renderer
{
    private $db, $stmt, $dbh;
    public function __construct()
    {
         $this->db = new DataBase();
         $this->dbh = new DataBase();
         $this->dbh->beginTransaction();
    }

     public function connect($siteID)
     {
         $this->data = array("n"=>0, "siteID"=>$siteID, "date_modified"=>'1970');
         $last_updated = '1970';
         $max_updated_n=0;
         $stmt = $this->dbh->prepare(RENDER_SELECT_QUERY);
         $stmt->bindValue(":SiteID", $siteID);
         $stmt->execute();
         $site = $stmt->fetchAll();
         if(count($site) > 0):
             $last_updated = $site[0]["updated_at"];
             $max_updated_n = $site[0]["max_updated_n"];
         endif;
         $this->stmt = $this->db->prepare(PAGE_SELECT_QUERY);
         $this->stmt->bindValue(":SiteID", $siteID);
         $this->stmt->bindValue(":last_updated", $last_updated);
         $this->stmt->execute();
     }

     public function download()
     {
         $responce = $this->stmt->fetch(PDO::FETCH_OBJ);
         if($responce && $this->check_response($responce->primary_url)):
             while ($row=$this->stmt->fetch(PDO::FETCH_OBJ)) {
                 var_dump($row);
                 $page_n = $row->n;
                 $this->data["siteID"] = $row->SitesID;
                 $this->data["n"] = $row->n;
                 $this->data['date_modified'] = $row->date_modified;
                 if(strlen($row->slug)>3)
                     $page_n = $row->slug;
                 $url = $row->primary_url.'/page.php?n='.$row->n.'&SiteID='.$row->SitesID;
                 $file_name = $page_n.'.html';
                 $folder = $row->SitesID;
                 $page_source = $this->page_source($url);
                 $this->path = DEFAULT_FOLDER.$folder.'/'.$file_name;
                 $this->make_folder($folder);
                 #$this->save_file($path, $page_source);
		ob_start(array($this, 'ob_file_callback'));
		$o_page = new page($this->data['n']);
		include_once '../renderController.php';
		ob_end_flush();
		break;
             }
             $this->insertRender();
         endif;
     }

    public function ob_file_callback($buffer)
    {
	$h = fopen($this->path, 'w');
	fwrite($h, $buffer);
	fclose($h);
    }   



    public function __download()
    {
	


	#$ob_file = fopen('sites/test.html','w');

#	$responce = $this->stmt->fetch(PDO::FETCH_OBJ);
         #if($responce && $this->check_response($responce->primary_url)):
             while ($row=$this->stmt->fetch(PDO::FETCH_OBJ)) {
                var_dump($row);
                $page_n = $row->n;
                $this->data["siteID"] = $row->SitesID;
                $this->data["n"] = $row->n;
                $this->data['date_modified'] = $row->date_modified;
                #if(strlen($row->slug)>3)
                #    $page_n = $row->slug;
                #$url = $row->primary_url.'/page.php?n='.$row->n.'&SiteID='.$row->SitesID;
                $file_name = $page_n.'.html';
                $folder = $row->SitesID;
                #$page_source = $this->page_source($url);
                $this->path = DEFAULT_FOLDER.$folder.'/'.$file_name;
                $this->make_folder($folder);
                #$this->save_file($this->path, $page_source);
		ob_start(array($this, 'ob_file_callback'));
		$o_page = new page($this->data['n']);
		include 'page2.php';
		ob_end_flush();
             }
             $this->insertRender();
         #endif;
    }

     private function check_response($url)
     {
         $ch = curl_init($url);
         curl_setopt($ch, CURLOPT_USERAGENT, MOZILLA_USER_AGENT);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, True);
         curl_exec($ch);
         $info = curl_getinfo($ch);
         curl_close($ch);
         if ($info['http_code']==200)
             return True;
         return False;
     }

     private function page_source($url)
     {
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_HEADER, 0);
         curl_setopt($ch,CURLOPT_VERBOSE,1);
         curl_setopt($ch, CURLOPT_USERAGENT, MOZILLA_USER_AGENT);
         curl_setopt($ch, CURLOPT_REFERER,'http://www.google.com');
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch,CURLOPT_POST,0);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 20);
         return curl_exec($ch);
     }

     private function insertRender()
     {
         $insert = $this->db->prepare(RENDER_INSERT_QUERY);
         $insert->bindValue(":SiteID", $this->data['siteID']);
         $insert->bindValue(":max_updated_n", $this->data['n']);
         $insert->bindValue(":updated_at", $this->data['date_modified']);
         $insert->execute();
     }

     private function make_folder($folder_name)
     {
         if(!file_exists(DEFAULT_FOLDER.$folder_name.'/'))
             mkdir(DEFAULT_FOLDER.$folder_name.'/', 0755, true);
     }

     private function save_file($path, $content)
     {
         $h = fopen($path, 'w');
         fwrite($h, $content);
         fclose($h);
     }

     public function save()
     {
         $this->dbh->commit();
     }
}


class Page
{
    public function __construct($page_source)
    {
        $this->source = $page_source;
    }
}
