<?php


class Grabber
{
    public function __construct($domDocument, $site_id)
    {
        $this->dom = $domDocument;
        $this->site_id = $site_id;
        $this->default_path = __DIR__.DIRECTORY_SEPARATOR.'sites'.DIRECTORY_SEPARATOR.$site_id.DIRECTORY_SEPARATOR;
    }
    
    public function grab()
    {
        $images = $this->dom->getElementsByTagName('img');
        foreach ($images as $image) {
            $link = $image->getAttribute('src'); 
            if($path = $this->create_path($link)){
                $this->create_dir($path); 
                $this->download($link, $path);
            }
        }
    }

    public function path_creator($link)
    {
        var_dump($link);
        if($path = parse_url($link)){ // On seriously malformed URLs, parse_url() may return FALSE.
            if(array_key_exists('host', $path)){
                $host = str_ireplace('www.', '', $path['host']);
                switch($host){
                    case $_SERVER['HTTP_HOST']:
                        return $path['path'];
                    case 'maksoft.net':
                        return $path['path'];
                    default:
                        return False;
                }
            }

            if(array_key_exists('path', $path)){
                return $path['path'];
            }
        } 
        return False;
    }

    public function grabCss()
    {
        $styles = $this->dom->getElementsByTagName('link');
        foreach ($styles as $css) {
            if($path = $this->path_creator($css->getAttribute('href'))){
                $this->create_dir($path); 
                $this->download($path, $path);
            }
        }
    }

    public function grabJs()
    {
        $javascripts = $this->dom->getElementsByTagName('script');
        foreach ($javascripts as $js) {
            if($path = $this->path_creator($js->getAttribute('src'))){
                $this->create_dir($path); 
                $this->download($path, $path);
            }
        }
    }

    protected function create_path($path)
    {
        if($path = $this->path_creator($path)){
            if(strpos($path, "img_preview.php") === false ){ return $path; }
            parse_str($path, $get_array);
            return array_shift($get_array);
        }
        return False;
    }

    protected function create_dir($path)
    {
        echo "<br>".$this->default_path.$path."<br>";
        if(!is_dir(dirname($this->default_path.$path))){
            mkdir(dirname($this->default_path.$path), 0755, true);
        }

    }

    protected function download($full_path, $path)
    {
        $content = file_get_contents('http://'.$_SERVER['HTTP_HOST'].'/'.$path);
        //Store in the filesystem.
        echo "<br><b>$full_path</b><br>";
        if(file_exists($path)){
            return;
        }
        $fp = fopen($this->default_path.$path, "w");
        fwrite($fp, $content);
        fclose($fp);
    }

    
}
