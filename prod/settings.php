<?php

const PAGE_SELECT_QUERY = "SELECT s.primary_url,s.url,p.date_modified, p.slug, p.ParentPage, p.n,s.SitesID, s.StartPage
                        FROM `Sites` as s
                        JOIN pages as p ON p.SiteID = s.SitesID
                        WHERE s.url IS NOT NULL
                        AND s.SitesID=:SiteID
                        AND p.date_modified > :last_updated
                        ORDER BY  `p`.`date_modified` ASC
                        LIMIT 500";


const MOZILLA_USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";

define('DEFAULT_FOLDER', '/hosting/maksoft/maksoft/prod/sites/');



const RENDER_INSERT_QUERY = "REPLACE INTO `SiteRender`(`SiteID`,
                                                  `max_updated_n`,
                                                  `updated_at`)
                             VALUES (:SiteID,
                                     :max_updated_n,
                                     :updated_at)";


const RENDER_SELECT_QUERY = "SELECT * FROM SiteRender WHERE SiteID=:SiteID";


const PAGE_SELECT = "
        SELECT s.primary_url,s.url,p.date_modified, p.slug, p.ParentPage, p.n,s.SitesID
        FROM `Sites` as s
        JOIN pages as p ON p.SiteID = s.SitesID
        WHERE s.url IS NOT NULL
        AND s.SitesID=:SiteID
        ORDER BY  `p`.`date_modified` ASC
        LIMIT 500";


