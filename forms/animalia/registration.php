<?php
require_once __DIR__.'/ClientRegister.php';


require_once __DIR__.'/../maksoft/inquire/index.php';



$form = new ClientRegister($_POST, $o_page, $mail);
$form->add_attr("class", "nomargin sky-form");
if(isset($_POST['action']) and $_SERVER['REQUEST_METHOD'] === "POST"){
    if($_POST['action'] == $form->action->value){
            try{
               $form->is_valid();
               $form_valid = $form->save();
                msg_notify("success", SUCCESS_MSG_REGISTRATION);
            } catch( Exception $e){
                msg_notify("error", ERROR_MSG_REGISTRATION);
            }
        }
    }
?>
<?=$form->start();?>
    <?=$form->action?>
    <fieldset class="nomargin">
        <label class="input margin-bottom-10">
            <i class="ico-append fa fa-user" aria-hidden="true"></i>
            <?=$form->username;?>
            <?=$form->username->print_errors();?>
        </label>
    
        <label class="input margin-bottom-10">
            <i class="ico-append fa fa-building" aria-hidden="true"></i>
            <?=$form->company_name;?>
            <?=$form->company_name->print_errors();?>
        </label>
        
        <label class="input margin-bottom-10">
            <i class="ico-append fa fa-map-marker" aria-hidden="true"></i>
            <?=$form->company_address;?>
            <?=$form->company_address->print_errors();?>
        </label>
                        
        <label class="input margin-bottom-10">
            <i class="ico-append fa fa-envelope"></i>
            <?=$form->company_email;?>
            <?=$form->company_email->print_errors();?>
        </label>
    
        <label class="input margin-bottom-10">
            <i class="ico-append fa fa-phone" aria-hidden="true"></i>
            <?=$form->phone;?>
            <?=$form->phone->print_errors();?>
        </label>

        <div class="row margin-bottom-10">
            <div class="col-md-6">
                <label class="input">
                    <?=$form->first_name;?>
                    <?=$form->first_name->print_errors();?>
                </label>
            </div>
            <div class="col col-md-6">
                <label class="input">
                    <?=$form->last_name;?>
                    <?=$form->last_name->print_errors();?>
                </label>
            </div>
        </div>

        <label class="select margin-bottom-10 margin-top-20">
            <?=$form->business_type;?>
            <?=$form->business_type->print_errors();?>
            <i></i>
        </label>
        
        
<!--									<div class="margin-top-30">
            <label class="checkbox nomargin"><input class="checked-agree" type="checkbox" name="checkbox"><i></i>I agree to the <a href="#" data-toggle="modal" data-target="#termsModal">Terms of Service</a></label>
            <label class="checkbox nomargin"><input type="checkbox" name="checkbox"><i></i>I want to receive news and  special offers</label>
        </div>-->
    </fieldset>

    <div class="row margin-bottom-20">
        <div class="col-md-12">
            <?=$form->recaptcha;?>
        </div>
        <div class="col-md-12">
            <br>
            <?=$form->submit;?>
        </div>
    </div>
<?=$form->end();?>
