<?php
use Maksoft\Amount\Money;
use \Maksoft\Cart\Cart;
use Jokuf\Form\Bootstrap;
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\Integerish;


class Add extends Bootstrap
{
    private $cart;

    public function __construct(Cart $cart, $post_data=null)
    {
        $this->cart = $cart;
        $this->sku = Hidden::init()
            ->add("name", "sku")
            ->add("hidden", True);

        $this->qty = Text::init()
            ->add("name","qty")
            ->add("label","�-��:")
            ->add("type","number")
            ->add("value",1)
            ->add("class","btn btn-default product-qty-dd noradius-right width-100 form-control")
            ->add("step",1);

        $this->qty->add_validator(Integerish::init());

        $this->name = Hidden::init();

        $this->action = Hidden::init()
            ->add("value","add_product")
            ->add("hidden",True);

        $this->price = Hidden::init();

        $this->submit = Submit::init()
            ->add("class", "btn btn-primary product-add-cart noradius-left")
            ->add("onclick","populate_inputs()")
            ->add("value","+ ������");
        $this->unit = Hidden::init();
        $this->link = Hidden::init();
        
        parent::__construct($post_data);
    }

    public function validate_price()
    {
        if(!is_float($this->price->value)){
            if(!is_integer($this->price->value)){
                return False;
            }
        }
        $price = number_format($this->price->value, 3, '.', '');
        if($price > 0){
            return True;
        }
        return False;
    }

    public function addToOrder($o_page)
    {
        $price = $this->format($this->price->value);
        $product  = new \Maksoft\Cart\Item($this->sku->value, $this->qty->value, $price, $this->name->value, $this->unit->value, $this->color->value);
        $product->image = $o_page->_page['image_src'];
        $product->link = $this->link->value;
        $product->color = $this->color->value;
        $this->cart->add($product);
        return $this->cart;
    }

    private function format($price)
    {
        return number_format($price, 2);
    }
}

