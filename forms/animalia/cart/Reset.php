<?php
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Fields\HiddenField;
use Maksoft\Form\Forms\DivForm;
use \Maksoft\Cart\Cart;


class Reset extends DivForm
{
    private $cart;

    public function __construct(Cart $cart, $post_data=null)
    {
        $this->cart = $cart;

        $this->submit = SubmitButton::init()
            ->add("class", "btn btn-danger btn-xs margin-top-20 pull-left")
            ->add("value", "X");

        $this->action = HiddenField::init()
            ->add("value", "reset")
            ->add("name", "action");
        parent::__construct($post_data);
    }

    public function reset()
    {
        $this->cart->reset();
        return $this->cart;
    }
}
