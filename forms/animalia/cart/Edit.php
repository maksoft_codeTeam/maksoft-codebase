<?php
use Maksoft\Amount\Money;
use \Maksoft\Cart\Cart;
use Jokuf\Form\Bootstrap;
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\Integerish;


class Edit extends Bootstrap
{
    private $cart;

    public function __construct(Cart $cart, $post_data=null)
    {
        $this->cart = $cart;

        $this->sku = Hidden::init()
            ->add("hidden", True);

        $this->qty = Text::init()
            ->add("label","�-��:")
            ->add("type","number")
            ->add("value",1)
            ->add("min", 1)
            ->add("maxlength", 3)
            ->add("max", 999)
            ->add("step",1);

        $this->qty->add_validator(Integerish::init());

        $this->name = Hidden::init();

        $this->action = Hidden::init()
            ->add("value","edit_product_qty")
            ->add("hidden",True);

        $this->submit = Submit::init()
            ->add("class", "btn btn-primary product-add-cart noradius-left")
            ->add("onclick","populate_inputs()")
            ->add("value","�������");
        
        parent::__construct($post_data);
    }

    private function format($price)
    {
        return number_format($price, 2);
    }

    public function save()
    {
        $item = $this->cart->get_by_sku($this->sku->value);
        $item->setQty($this->qty->value);
        $this->cart->edit($item);
        return $this->cart;
    }
}

