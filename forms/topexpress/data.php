<?php 


class Translation {
    public static $form = array(
        "registration" => array(
            "footer" => array(
                "bg" => "*������������ ����",
                "en" => "*Obligatory field",
                "ru" => "*������������ ����",
            ),
            "title" => array(
                "bg" => array(
                    "�� ����������� �� �������� ������ - ���� ������� � ������� ���������, ��������� ���������",
                    "�� �� �������� �������� ������, ���� ��������� ������� �� �������, �� �� �� �������� ������. �� �� ���� ���������� � ������� �� 24 ����."
                ),
                "en" => array(
                    "Registration for extended access � Newsletter and Market platform, Marketing desk",
                    "For an extended access please fill in the contact form so we can send you a mail containing your password. You will be contacted within 24 hours."
                ),
                "ru" => array(
                    "����������� ��� ������������ ������� - ���� ��������� � �������� ���������, ��������� ���������",
                    "��� ������������ �������, ������ ��������� ���������� ����� � �� ����������� ��� ������. ����� ������ ��������� � ������� �����."
                ),
            ),
            'email_text' => array(
                'bg' => array(
                    "title" => "������� �����������",
                    "content" => "���� � ������������, �� ������ ��������� � ���� �� �������� ������ ���� ����������� � �������� � �� ���� ���������� �������� ���-�����.
                    ���� ��������� �� ������� ���������, � ����� �� ������������ ������ �� ������."
                ),
                'en' => array(
                    "title" => "Succesfull registration",
                    "content" => "This is a confirmation that your mail request for extended access through <strong>registration</strong> has been received and will be processed at earliest convenience.
                Please check for an incoming mail providing you with an <strong>access password.</strong>"
                ),
                'ru' => array(
                    "title" => "�������� �����������",
                    "content" => "��������� ������������� � ���, ��� ��� ������ ������ �� ����������� ������ ����� ����������� ������� � ����� ��������� � ��������� �����.
                    ������ ���������� �� �������� ����������, ������� ������������� ��� ������ � �������."
                ),
            ),
            "name" => array(
                "bg" => array("label" => "���:*"),
                "en" => array("label" => "Name:*"),
                "ru" => array("label" => "���:*"),
            ),
            "address" => array(
                "bg" => array("label" => "�����:*"),
                "en" => array("label" => "Address:*"),
                "ru" => array("label" => "Address:*"),
            ),
            "username" => array(
                "bg" => array("label" => "������������� ���:*"),
                "en" => array("label" => "Username:*"),
                "ru" => array("label" => "��� ������������:*"),
            ),
            "company" => array(
                "bg" => array("label" => "�����/�����������:"),
                "en" => array("label" => "Company/Organization:"),
                "ru" => array("label" => "��������/�����������:"),
            ),
            "branch" => array(
                "bg" => array(
                    "label"=>"�����/�������:*",
                    "data" => array(
                        "�����",
                        "�������� ��������",
                        "�������� ����"
                    )
                ),
                "en" => array(
                    "label"=>"Industry/Activity:",
                    "data" => array(
                        "Warehouse",
                        "Door selling",
                        "Online Commerce"
                    )
                ),
                "ru" => array(
                    "label"=>"�������/������������:*",
                    "data"=> array(
                        "�����",
                        "�������� ��������",
                        "�������� ����"
                    )
                ),
            ),
            "email" => array(
                "bg" => array("label" => "����*"),
                "en" => array("label" => "E-mail:*"),
                "ru" => array("label" => "����������� �����:*"),
            ),
            "country" => array(
                "bg" => array("label" => "������:"),
                "en" => array("label" => "Country:"),
                "ru" => array("label" => "������:"),
            ),
            "phone" => array(
                "bg" => array("label" => "�������:"),
                "en" => array("label" => "Phone number:"),
                "ru" => array("label" => "�������:"),
            ),
            "first_name" => array(
                "bg" => array("label"=>"���:"),
                "en" => array("label"=>"Name:"),
                "ru" => array("label"=>"���:"),
            ),
            "last_name" => array(
                "bg" => array("label"=>"�������:"),
                "en" => array("label"=>"Last name:"),
                "ru" => array("label"=>"�������:"),
            ),
            "submit" => array(
                "bg" => array("label"=>"�������"),
                "en" => array("label"=>"Send"),
                "ru" => array("label"=>"���������"),
            ),
        ),
    );
}
