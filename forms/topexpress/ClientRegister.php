<?php
require_once __DIR__.'/../maksoft/inquire/BaseFormBootstrap.php';
require_once __DIR__.'/data.php';

use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;



class ClientRegister extends BaseFormBootstrap
{
    public $name = 'registration';

    public function __construct($post_data=null, $cms=null, $email=null)
    {
        $this->cms = $cms;
        $this->mailer = $email;

        $translations = Translation::$form[$this->name];

        $not_empty = NotEmpty::init(true)
                        ->err_msg("���� ���� �� ���� �� ���� ������!");

        $this->username     = Text::init()
                              ->add("placeholder", $translations['username'][LANG]['label'])
                              ->add("class", "form-control")
                              ->add("id", "Name")
                              ->add_validator($not_empty)
                              ->add("required", true);

        $this->company_name = Text::init()
                              ->add("placeholder", $translations['company'][LANG]['label'])
                              ->add("class", "form-control")
                              ->add_validator($not_empty)
                              ->add("required", true);

        $this->company_address = Text::init()
                              ->add("placeholder", $translations['address'][LANG]['label'])
                              ->add("class", "form-control")
                              ->add_validator($not_empty)
                              ->add("required", true);

        $this->company_email = Text::init()
                              ->add("type", "email")
                              ->add("placeholder", $translations['email'][LANG]['label'])
                              ->add("class", "form-control")
                              ->add_validator(EmailValidator::init()->err_msg("��������� ��������� ����� �����!"))
                              ->add_validator($not_empty)
                              ->add("required", true);
        $this->phone         = Phone::init()
                              ->add("placeholder", $translations['phone'][LANG]['label'])
                              ->add("class", "form-control")
                              ->add_validator($not_empty)
                              ->default_error_msg("������ ��� ��������� ��������� �����.")
                              ->add("required", true);

        $this->first_name    = Text::init()
                              ->add("placeholder", $translations['first_name'][LANG]['label'])
                              ->add("class", "form-control")
                              ->add_validator($not_empty)
                              ->add("required", true);
        $this->last_name     = Text::init()
                              ->add("placeholder", $translations['last_name'][LANG]['label'])
                              ->add("class", "form-control")
                              ->add_validator($not_empty)
                              ->add("required", true);

        $this->business_type = Select::init()
                              ->add("class", "form-control")
                              ->add("placeholder", $translations['branch'][LANG]['label'])
                              ->add("data", $translations['branch'][LANG]['data']);

        $this->action        = Hidden::init()
                              ->add("value", $this->name);

        $this->submit        = Submit::init()
                              ->add("value", $translations['submit'][LANG]['label'])
                              ->add("class", "btn btn-success");

        $this->recaptcha     = Recaptcha::init()
                              ->add("lang", LANG)
                              ->add("site_key", CAPTCHA_KEY)
                              ->add("secret", CAPTCHA_SECRET);
        parent::__construct($post_data, $this->cms, $this->mailer);
    }

    public function save()
    {
        $name = $this->first_name->value . ' ' . $this->last_name->value;
        $email = $this->company_email->value;
        return $this->before_save($name, $email);
    }
}
