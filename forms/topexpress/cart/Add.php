<?php
use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Fields\HiddenField;
use Maksoft\Form\Forms\DivForm;
use Maksoft\Amount\Money;
use \Maksoft\Cart\Cart;


class Add extends DivForm
{
    private $cart;

    public function __construct($cart, $product, $post_data=null)
    {
        $this->cart = $cart;
        $this->sku = new HiddenField(array(
            "name" => "sku",
            "hidden" => True,
            "value" => $product->p_Id,
        ));
        $this->sku->add_validators(new \Maksoft\Form\Validators\NotEmpty());

        $this->qty = new TextField(array(
            "name" => "qty",
            "label" => "�-��:",
            "type" => "number",
            "value" => 1,
            "class" => "btn btn-default product-qty-dd noradius width-100",
            "step" => 1,
        ));

        $this->qty->add_validators(new \Maksoft\Form\Validators\Integerish());

        $this->name = new HiddenField(array(
            "name" => "name",
        ));

        $this->action = new HiddenField(array(
            "name" => "action",
            "value" => "add_product",
            "hidden" => True,
        ));

        $this->price = new HiddenField(array(
            "name" => "price",
        ));  

        $this->submit = new SubmitButton(array(
            "class"=>"btn btn-primary pull-left product-add-cart noradius",
            "onclick" => "populate_inputs()",
            "value" => "+ ������"
        ));
        $this->unit = new HiddenField(array(
            "name" => "unit",
        ));
        $this->link = new HiddenField(array(
            "name" => "link",
        ));
        
        $this->color = new TextField(array(
            "name" => "color",
        ));
        parent::__construct($post_data);
    }

    public function validate_price()
    {
        if(!is_float($this->price->value)){
            if(!is_integer($this->price->value)){
                return False;
            }
        }
        $price = number_format($this->price->value, 3, '.', '');
        if($price > 0){
            return True;
        }
        return False;
    }

    public function addToOrder($o_page)
    {
        $price = $this->format($this->price->value);
        $product  = new \Maksoft\Cart\Item($this->sku->value, $this->qty->value, $price, $this->name->value, $this->unit->value, $this->color->value);
        $product->image = $o_page->_page['image_src'];
        $product->link = $this->link->value;
        $product->color = $this->color->value ? $this->color->value : '����';
        $this->cart->add($product);
        return $this->cart;
    }

    private function format($price)
    {
        return number_format($price, 2);
    }
}

