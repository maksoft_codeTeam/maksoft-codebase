<?php
use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Fields\PhoneField;
use Maksoft\Form\Fields\EmailField;
use Maksoft\Form\Fields\TextAreaField;
use Maksoft\Form\Fields\HiddenField;
use Maksoft\Form\Fields\SelectField;
use Maksoft\Form\Forms\DivForm;
use Maksoft\Form\Validators\NotEmpty;
use Maksoft\Amount\Money;
use \Maksoft\Cart\Cart;


class Purchase extends DivForm
{
    private $cart;
    private $page;
    private $twig;

    public function __construct(Cart $cart, $page, $twig, $post_data=null)
    {
        $this->twig = $twig;
        $this->cart = $cart;
        $this->page = $page;
        $class = "form-control margin-bottom-20";
        $this->name    = new TextField(array( "class"=>$class, "label" => "���", "value" => $this->page->_user['Name'], "required" => True));
        $this->phone   = new PhoneField(array( "class"=>$class, "label" => "�������", "value" => $this->page->_user['Phone'], "required" => True));
        $this->mail    = new EmailField(array( "class"=>$class, "label" => "�����", "value" => $this->page->_user['EMail'], "required" => True));
        $this->action  = new HiddenField(array("name"=>"action", "value" => "make_purchase"));
        $this->address = new TextField(array("class"=>$class, "label"=>"�����"));
        $this->notes   = new TextAreaField(array("class"=>$class, "label" => "���������"));
        $this->order   = new HiddenField(array("name"=>"order", "val" => ""));
        $this->delivery = new SelectField(array(
            "class" => $class,
            "label" => "��������",
            "options" => array(
                "�������� �� ������" => "�������� �� ������",
                "����� �������� � ������" => "����� �������� � ������",
                "������� �� �����" => "������� �� �����"
            )
        ));
        $this->mail->add_validators(new NotEmpty(True));
        $this->deliver = new TextAreaField();
        parent::__construct($post_data);
    }

    protected function construct_text()
    {
        $tmp = array();
        $tmp[] = $this->add_line("���:", $this->name->value);
        $tmp[] = $this->add_line("�������:", $this->phone->value);
        $tmp[] = $this->add_line("�����:", $this->mail->value);
        $tmp[] = $this->add_line("�����:", $this->address->value);
        $tmp[] = $this->add_line("���������:", $this->notes->value);
        $tmp[] = $this->order->value;
        return implode("<br>", $tmp);
    }

    protected function add_line($cat_name, $value){
        return sprintf("<b>[%s]</b> %s", $cat_name, $value);
    }

    public function save()
    {
        $msg = sprintf("���� ������� �� %s �� �������� %s ��. � ���", $this->name->value, $this->cart->sum_with_vat());
        $order_id = $this->page->insertIntoForms($this->name->value, $this->mail->value, $this->construct_text(), $msg);
        $this->cart->checkout();
        $this->send_email($order_id, $this->construct_text());
    }

    protected function construct_mail_body($order_id)
    {
        $template  = $this->twig->loadTemplate('mail_template.html');
        return $template->render(array(
            "name" => $this->name->value,
            "order_id" => $order_id,
            "email" => $this->mail->value,
            "order_info" => $this->order->value,
            "address" => $this->address->value,
            "notes" => $this->notes->value,
            "shipping" => $this->delivery->value,
            "client_name" => $this->page->_user["Name"],
        ));
    }

    protected function send_email($order_id, $body)
    {
        $mail = new \PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'server.maksoft.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->CharSet = "UTF-8";
        $mail->Username = 'cc@maksoft.bg';                 // SMTP username
        $mail->Password = 'maksoft@cc';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom($this->page->_site['EMail'], iconv('cp1251', 'utf8', $this->page->_site['Name']));
        $mail->addAddress($this->mail->value, iconv('cp1251', 'utf8', $this->name->value));     // Add a recipient
        $mail->addBcc($this->page->_site['EMail'], iconv('cp1251', 'utf8', '���� ������� �� �������� '.$this->cart->sum()));
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = iconv('cp1251', 'utf8', $this->page->_site['Name'].': ���� ������� #'.$order_id);
        $mail->Body    = iconv('cp1251', 'utf8', $this->construct_mail_body($order_id));

        if(!$mail->send()) {
            throw new $mail->ErrorInfo;
        }
        return True;
    }
}
