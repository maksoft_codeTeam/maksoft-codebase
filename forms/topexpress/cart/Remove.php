<?php
use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Fields\HiddenField;
use Maksoft\Form\Forms\DivForm;
use Maksoft\Amount\Money;
use \Maksoft\Cart\Cart;


class Remove extends DivForm
{
    private $cart;
    private $item;

    public function __construct(Cart $cart, $sku, $post_data=null)
    {
        $this->item = $item;
        $this->cart = $cart;

        $this->submit = new SubmitButton(array(
            "class"=>"btn btn-primary margin-top-30 pull-left product-add-cart noradius",
            "style" => "margin-top:3%;",
            "value" => "+ Добави"
        ));
        $this->action = new HiddenField(array(
            "name" => "action",
            "value" => "remove_product",
        ));
        $this->sku = new HiddenField(array(
            "name" => "sku",
            "value" => $sku,
        ));
        parent::__construct($post_data);
    }

    public function remove()
    {
        $item = $this->cart->get_by_sku($this->sku->value);
        $this->cart->remove($item);
        return $this->cart;
    }

}
