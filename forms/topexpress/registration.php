<?=$registration_form->start();?>
    <?=$registration_form->action?>
    <fieldset class="nomargin">
        <label class="input margin-bottom-10">
            <i class="ico-append fa fa-user" aria-hidden="true"></i>
            <?=$registration_form->username;?>
            <?=$registration_form->username->print_errors();?>
        </label>
    
        <label class="input margin-bottom-10">
            <i class="ico-append fa fa-building" aria-hidden="true"></i>
            <?=$registration_form->company_name;?>
            <?=$registration_form->company_name->print_errors();?>
        </label>
        
        <label class="input margin-bottom-10">
            <i class="ico-append fa fa-map-marker" aria-hidden="true"></i>
            <?=$registration_form->company_address;?>
            <?=$registration_form->company_address->print_errors();?>
        </label>
                        
        <label class="input margin-bottom-10">
            <i class="ico-append fa fa-envelope"></i>
            <?=$registration_form->company_email;?>
            <?=$registration_form->company_email->print_errors();?>
        </label>
    
        <label class="input margin-bottom-10">
            <i class="ico-append fa fa-phone" aria-hidden="true"></i>
            <?=$registration_form->phone;?>
            <?=$registration_form->phone->print_errors();?>
        </label>

        <div class="row margin-bottom-10">
            <div class="col-md-6">
                <label class="input">
                    <?=$registration_form->first_name;?>
                    <?=$registration_form->first_name->print_errors();?>
                </label>
            </div>
            <div class="col col-md-6">
                <label class="input">
                    <?=$registration_form->last_name;?>
                    <?=$registration_form->last_name->print_errors();?>
                </label>
            </div>
        </div>

        <label class="select margin-bottom-10 margin-top-20">
            <?=$registration_form->business_type;?>
            <?=$registration_form->business_type->print_errors();?>
            <i></i>
        </label>
        
        
<!--									<div class="margin-top-30">
            <label class="checkbox nomargin"><input class="checked-agree" type="checkbox" name="checkbox"><i></i>I agree to the <a href="#" data-toggle="modal" data-target="#termsModal">Terms of Service</a></label>
            <label class="checkbox nomargin"><input type="checkbox" name="checkbox"><i></i>I want to receive news and  special offers</label>
        </div>-->
    </fieldset>

    <div class="row margin-bottom-20">
        <div class="col-md-12">
            <?=$registration_form->recaptcha;?>
        </div>
        <div class="col-md-12">
            <br>
            <?=$registration_form->submit;?>
        </div>
    </div>
<?=$registration_form->end();?>
