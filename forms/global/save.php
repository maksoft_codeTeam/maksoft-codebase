<?php
require_once "/hosting/maksoft/maksoft/modules/vendor/autoload.php";
require_once __DIR__.'/../../lib/Database.class.php';
require_once __DIR__.'/../../global/admin/PageTranslationForm.php';

require_once __DIR__.'/../../lib/lib_page.php';
use \Maksoft\Gateway\Gateway;

$db = new Database();
$gate = new Gateway($db);
$o_page = new page();

if (isset($no) ) {
    $row_no = $o_page->get_page($no, 'object');
    $no_tags_selected = ""; 
    $form = new PageTranslationForm($gate, $row_no->n, $_POST, $o_page->_site['SitesID']);
    if($_SERVER['REQUEST_METHOD'] === 'POST' and isset($_POST['action']) and $_POST['action'] == $form->action->value) {
        try{
        $form->is_valid();
        $form->save();
        } catch (\Exception $e) {
            header("Location: ".$_SERVER['HTTP_REFERER']);
            #echo $e->getMessage();
            #foreach($form as $field){
            #    foreach($field->get_errors() as $err){
            #        var_dump($err);
            #    }
            #}
        }
    }
}

header("Location: ".$_SERVER['HTTP_REFERER']);

