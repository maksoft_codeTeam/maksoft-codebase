<?php
require_once __DIR__.'/../../../modules/bootstrap.php';


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Bus\forms\CompanyType;

$form = $formFactory->createBuilder('Bus\forms\CompanyType')
        ->getForm();

echo $twig->render('company.html.twig', array(
        'form' => $form->createView(),
    ));

