<?php
use Jokuf\Form\Bootstrap;


class HelpersTrait extends Bootstrap
{
    protected $helpers = array();


    public function __construct($post=array(), $files=array(), $initial=array())
    {
        $this->helpers['escape'] = function ($key, $default='') use ($clean) {
                                        if(!isset($clean[$key])) {
                                            return trim($default);
                                        }

                                        return $clean[$key];
                                    };
        return parent::__construct($post, $files, $initial);
    }

    public static function printErrors($field)
    {
        $template = '<ul class="alert alert-danger">%s</ul>';
        return sprintf($template, array_reduce($field->getErrors(), function($carry, $item) {
            if( !empty($item)) {
                $carry .= '<li>' . $item . '</li>';
            }
            return $carry;
        }));
    }
}
