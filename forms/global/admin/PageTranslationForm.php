<?php
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Field;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class PageTranslationForm extends Bootstrap
{
    private $gate;
    public function __construct($gate, $no, $post_data, $SiteID)
    {
        $this->SiteID = $SiteID;

        $this->n = $no;

        $this->gate = $gate;

        $this->NamePage = Text::init();

        $this->title = Text::init();

        $this->slug = Text::init();

        $this->textStr = Text::init();

        $this->tags = Text::init();

        $this->no = Text::init();

        $this->action = Hidden::init()
            ->add('value', 'insert_translation');
        $this->lang_id = Textarea::init();
        parent::__construct($post_data);
    }

    public function is_valid()
    {
        parent::is_valid();
    }

    public function validate_no($field)
    {
        return $this->no != $field->value;
    }

    public function save()
    {
        $this->gate->page()->insert_page_texts($this->n, $this->lang_id->value, rtrim($this->NamePage->value), $this->title->value, $this->slug->value, $this->textStr->value, $this->tags->value);
        $this->gate->page()->insert_slug($this->SiteID, $this->n, $this->slug->value);
        $pages_txt = $this->gate->page()->get_page_strings($this->n, rtrim($this->NamePage->value));
        $this->gate->page()->insert_page_version($this->n, $this->lang_id->value, $pages_txt->ptID);
    }
}
