<?php
use Jokuf\Form\Bootstrap;
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;



class CreateNewScheduledTaskForm extends Bootstrap 
{
    public $title = 'New scheduled task';
    protected $db;
    protected $tasks = array();
    protected $base_namespace = '\Maksoft\Task\Delayed\\';
    public function __construct($db, $post_data=array()) 
    {
        $this->db = $db;
        foreach (new DirectoryIterator('modules/tasks/Delayed') as $fileInfo) {
            if($fileInfo->isDot()) continue;
            if($this->isUpper($fileInfo->getFilename())) {
                $file = array_shift(explode('.', $fileInfo->getFilename()));
                $this->tasks[$file] = $file;
            }
        }

        $in_range = function($value) use ($a, $b) {
            return $value >= $a and $value <= $b; 
        };

        $this->task = Select::init()->add('data', $this->tasks);  
        $this->args = Text::init()
            ->add('label', '���������')
            ->add('class', 'form-control');
        $this->max_retries = Integer::init()
            ->add('value', 60)
            ->add('class', 'form-control')
            ->add('min', 1)
            ->add('step', 1)
            ->add('label', '���������� ���� �����');
        $this->max_execution_time = Integer::init()
            ->add('value', 60)
            ->add('class', 'form-control')
            ->add('label', '���������� ����� �� ����������');
        $minute_validator = function($value){
            var_dumP($value);
            if($value == '*') { return true; }
            var_dump(is_integer($value) and $value >= 0 and $value <= 23);
            if(is_integer($value) and $value >= 0 and $value <= 23) { return True; }
            throw new \Exception('minutes must be between 0 and 59');
        };
        $this->minute = Text::init()->add('value', '*')
            ->add_validator( $minute_validator)
            ->add('class', 'form-control')
            ->add('label', 'This controls what minute of the hour the command will run on, and is between \'0\' and \'59\'');
        $this->hour = Text::init()->add('value', '*')
            ->add_validator(function($value){
                if($value === '*') { return true; }
                if(is_integer($value) and $value >= 0 and $value <= 23) { return True; }
                throw new \Exception('hour must be between 0 and 23');
            })
            ->add('class', 'form-control')
            ->add('label', 'This controls what hour the command will run on, and is specified in
                    the 24 hour clock, values must be between \'0\' and \'23\'');
        $this->dom = Text::init()->add('value', '*')
            ->add_validator(function($value){
                if(is_integer($value) and $value >= 0 and $value <= 31 or $value === '*') { return True; }
                throw new \Exception('���� �� ������ ������ �� � ����� ��� �������� ����� 0 � 31');
            })
            ->add('class', 'form-control')
            ->add('label', 'This is the Day of Month, that you want the command run on, e.g. to
                 run a command on the 19th of each month, the dom would be 19.');
        $this->month = Text::init()->add('value', '*')
            ->add('class', 'form-control')
            ->add_validator(function($value){
                if(is_integer($value) and $value >= 0 and $value <= 12 or $value === '*') { return True; }
                throw new \Exception('Month must be between 0 and 12');
            })
            ->add('label', 'This is the month a specified command will run on, it may be specified
                 numerically (0-12), or as the name of the month (e.g. May)');
        $this->dow = Text::init()->add('value', '*')
            ->add_validator(function($value){
                return is_integer($value) and $value >= 0 and $value <= 7 or $value === '*';
            })
            ->add('class', 'form-control')
            ->add('label', 'This is the Day of Week that you want a command to be run on, it can
                 also be numeric (0-7) or as the name of the day (e.g. sun).');
        $this->execute_once = Radio::init()
            ->add('data', array(1=>'��', 0=>'��'))
            ->add('value', 1)
            ->add_validator(function($value){
                if(is_integer($value) and $value >= 0 and $value <= 1) { return True; }
                throw new \Exception ('�� ��� ������� ����� �� ����������');
            })
            ->add('class', 'radio')
            ->add('label', '������� ������');
        $this->state = false;
        $this->last_executed = false;
        $this->created_at = time();
        $this->updated_at = time();   
        $this->action = Hidden::init()->add('value', 'create_task'); 
        $this->submit = Submit::init()
            ->add('class', 'btn btn-success')
            ->add('value', 'Create');

        parent::__construct($post_data);
    }

    public function isUpper($str) {
        $chr = mb_substr ($str, 0, 1, "UTF-8");
        return mb_strtolower($chr, "UTF-8") != $chr;
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function validate_task($field) 
    {
        if(!array_key_exists($field->value, $this->tasks)) {
            throw new \Exception('Please choice valid task');
        }
        return true;
    }

    public function validate_args($field)
    {
        $args = explode(',', $field->value);
        $this->args->value = array_map(function($e) {
            return trim($e);
        }, $args);
    }
    
    public function validate_max_execution_time ($f)
    {
        if(!is_integer($f->value) or $f->value > 60) {
            $this->max_execution_time->value = 60;
        }
        return true;
    }

    public function save()
    {
        $cls = sprintf("%s%s", $this->base_namespace, $this->task->value);
        $task = call_user_func_array(array($cls, 'delay'), $this->args->value);
        $clean = $this->clean_data();
        $this->db->queue()->create_task(
            serialize($task),
            $clean['max_retries'], 
            $clean['max_execution_time'], 
            $clean['minute'], 
            $clean['hour'], 
            $clean['dom'], 
            $clean['month'], 
            $clean['dow'], 
            $this->state, 
            $clean['execute_once']
        );
    }
}
