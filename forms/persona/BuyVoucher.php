<?php
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\MinLength;
use Jokuf\Form\Validators\Integerish;
use Jokuf\Form\DivForm;
use Jokuf\Amount\Money;


class BuyVoucher extends DivForm
{
    public function __construct($post=null, $file=null){
        $class = "form-vaucher";
        $this->holder_name = Text::init()
            ->add("placeholder", "���")
            ->add("style", "margin-top:0;")
            ->add("class", $class)
            ->add("required", true)
            ->add_validator(
                MinLength::init(2)
                    ->err_msg("��������� ������� 2 �������")
            ); 

        $this->holder_lastname = Text::init()
            ->add("placeholder", "�������")
            ->add("class", $class)
            ->add("required", true)
            ->add("title", "�� ���� �� �������� ������ ���")
            ->add_validator(
                MinLength::init(2)
                    ->err_msg("��������� ������� 2 �������")
            );

        $this->holder_phone = Phone::init()
            ->add("placeholder", "�������")
            ->add("class", $class)
            ->add("required", true)
            ->add_validator(
                NotEmpty::init(true)
                    ->err_msg("���� ���� �� ���� �� ���� ������")
            ); 

        $this->holder_email = Email::init()
            ->add("placeholder", "Email")
            ->add("class", $class)
            ->add("required", true)
            ->add_validator(
                NotEmpty::init(true)
                    ->err_msg("���� ���� �� ���� �� ���� ������")
            ); 

        $this->voucher_price = Integer::init()
            ->add("placeholder", "���� �� �������")
            ->add("class", "form-vaucher suma")
			->add("pattern", "[0,9]")
            ->add("step", 50)
            ->add("min", 100)
            ->add("value", 100)
            ->add_validator(
                Integerish::init(true)
                    ->err_msg("���� ���� �� ���� �� ���� ������")
            ); 

        $this->street = Text::init()
            ->add("placeholder", "����� �� ��������")
            ->add("class", $class)
            ->add("required", true)
            ->add_validator(
                MinLength::init(2)
                    ->err_msg("��������� ������� 3 �������")
            ); 

        $this->city = Text::init()
            ->add("placeholder", "����")
            ->add("class", $class)
            ->add_validator(
                NotEmpty::init(true)
                    ->err_msg("���� ���� �� ���� �� ���� ������")
            ) 
            ->add("required", true);

        $this->postal_code = Text::init()
            ->add("class", $class)
            ->add("placeholder", "�������� ���");

        $this->action = Hidden::init()
            ->add("value", "buy");

        parent::__construct($post, $file);
    }

    public function save($o_page){
        $data = $this->clean_data();
        ob_start();
        $ip = $o_page->user_ip;
        require_once '/hosting/maksoft/maksoft/Templates/persona/templates/notification_email_self.php';
        $template = ob_get_contents();
        ob_end_clean();
        $name = $data['holder_name'] . "  " . $data['holder_last_name'];
        $email = $data['holder_email'];
        $subject = "���� ������� �� ������ �� �������� ".$data['voucher_price']. " ��.";
        $order_id = $o_page->insertIntoForms($name, $email, $template, $subject, $ftype="");
        ob_start();
        $ip = $o_page->user_ip;
        require_once '/hosting/maksoft/maksoft/Templates/persona/templates/notification_email_client.php';
        $template = ob_get_contents();
        ob_end_clean();
        $this->send_email($order_id, $template);
        return true;
    }

    protected function send_email($order_id, $body)
    {
        $mail = new \PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'server.maksoft.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->CharSet = "utf8";
        $mail->Username = 'cc@maksoft.bg';                 // SMTP username
        $mail->Password = 'maksoft@cc';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom('office@persona.bg', iconv('cp1251', 'utf8', '���������� ������� �������'));
        $mail->addAddress($this->holder_email->value, iconv('cp1251', 'utf8', $this->name->value));     // Add a recipient
        $mail->addBcc('office@persona.bg', iconv('cp1251', 'utf8', '���� ������� �� �������� '.$this->voucher_price->value));
        $mail->addBcc('cc@maksoft.bg', iconv('cp1251', 'utf8', '���� ������� �� �������� '.$this->voucher_price->value));
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = iconv('cp1251', 'utf8', '���������� ������� ������� - ������� #'.$order_id);
        $mail->Body    = iconv('cp1251', 'utf8', $body);

        if(!$mail->send()) {
            throw new $mail->ErrorInfo;
        }
        return True;
    }
}
