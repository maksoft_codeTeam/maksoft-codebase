<?php 
require_once __DIR__ .'/../../modules/bootstrap.php';

$company = $em->getRepository('Bus\models\Company')->findOneById(1);

echo $company->getName();
echo ' | ';
echo $company->getEmail();

$qb = $em->createQueryBuilder();

$directions = $em->createQueryBuilder()->select('d')
   ->from('Bus\models\Direction', 'd')
   ->join('Bus\models\Schedule', 'sh', 'WITH', 'sh.start_date <= :today')
   ->leftJoin('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
   ->join('d.tickets', 't')->addSelect('t')
   ->setParameter('today', new DateTime())
   ->setParameter('company_id', $company->getId())
   ->getQuery()
   ->getResult();

$tickets = $em->createQueryBuilder()->select('t')
   ->from('Bus\models\Ticket', 't')
   ->join('Bus\models\Station', 's', 'WITH', 's.company = :company_id')
   ->join('Bus\models\Schedule', 'sh', 'WITH', 'sh.start_date <= :today')
   ->where('sh.end_date > :today')
   ->groupBy('t.direction')
   ->addGroupBy('t.startStation')
   ->setParameter('company_id', $company->getId())
   ->setParameter('today', new DateTime())
   ->getQuery()
   ->getResult();

$bus_stations = $em->createQueryBuilder()->select('s')
   ->from('Bus\models\Station', 's')
   ->join('Bus\models\Schedule', 'sh', 'WITH', 'sh.start_date <= :today')
   ->where('s.company = :company_id')
   ->andWhere('sh.end_date > :today')
   ->groupBy('s.city')
   ->setParameter('company_id', $company->getId())
   ->setParameter('today', new DateTime())
   ->getQuery()
   ->getResult();

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<h1> �����������: </h1>
<table border="1|0">
    <thead>
        <th>N</th>
        <th>���</th>
        <th>��</th>
        <th>��</th>
        <th>��</th>
    </thead>
    <tbody>
<? foreach($directions as $direction): ?>
    <tr>
        <td><?=$direction->getId();?></td>
        <td><?=$direction->getName();?></td>
        <td><?=$direction->getStartStation()->getName()?></td>
        <td><?=$direction->getEndStation()->getName()?></td>
        <td>
            <ul>
                <? foreach($direction->getRoutes() as $route): ?>
                    <li><?=$route->getName();?>
                <? endforeach; ?>
            </ul>
        </td>
    </tr>
<? endforeach; ?>
    </tbody>
</table>
<H1>������:</H1>
<div id="find_ticket">
    <select name="from_city">
    <?php foreach($bus_stations as $station) : ?> 
        <option value='<?=$station->getId();?>'> <?=$station->getCity();?></option>
    <?php endforeach;?>
    </select>
    <select name="to_city" id="to_city"> 
    </select>
</div>

<script>
const el = "#find_ticket";
jQuery(el).on('change', function(e) {
    city_id = jQuery( el + " option:selected" ).val();
    const url = '/forms/gtravel/api.php?command=travel_from_city&from_city=' + city_id; 
    jQuery.get(url, function(data) {
          var data = data.map(parse_option)
          jQuery("#to_city").html(data.reduce(function(carry, item){
                return carry+item
          }));
    });
});

function parse_option(el) {
    return '<option value="' +el[0]+ '">' + el[1] + '</option>';
}

</script>
