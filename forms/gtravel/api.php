<?php
session_start();
require_once __DIR__ .'/../../modules/bootstrap.php';

require_once __DIR__ .'/../../global/bus_tickets/repository.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
$request = Request::createFromGlobals();
$response = new JsonResponse();
$response->headers->set('Access-Control-Allow-Origin', '*');
$response->headers->set('Access-Control-Allow-Methods', 'GET,POST,PUT');
$response->headers->set('Access-Control-Allow-Credentials', 'true');
$response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');

$qb = $em->createQueryBuilder();


function parse_ticket($ticket) {
    return array($ticket->getEndStation()->getId(), $ticket->getEndStation()->getName(), $ticket->getEndStation()->getCountry());
}

$get_travel_from = function ($kwargs) use ($em, $directions_to) {
    if(!isset($kwargs['from_city'])) { return array(); }
    return array_map(parse_ticket, $directions_to(1, $kwargs['from_city']));
};


$get_travel_from_filtered = function($kwargs) use ($em, $directions_to) {
    if(!isset($kwargs['from_city'])) { return array(); }
    $directions_from = array_map(parse_ticket, $directions_to(1, $kwargs['from_city']));

    $directions = array();
    foreach($directions_from as $direction):
        $option = array($direction[0], $direction[1]);
        if(!isset($directions[$direction[2]])) {
            $directions[$direction[2]] = array();
        }
        $directions[$direction[2]][] = $option;
    endforeach; 
    return $directions;
};


$commands = array(
    'travel_from_city' => $get_travel_from,
    'travel_from_city_filtered' => $get_travel_from_filtered,
    'reserve_ticket' => $reserve_ticket
);

if(!isset($_GET['command']) or !array_key_exists($_GET['command'], $commands)) {
    $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
    $response->prepare($request);
    $response->send();
    return;
}

$cmd = $_GET['command'];

try {
    $data = my_json_encode($commands[$cmd]($_GET));
    $response->setContent(iconv('cp1251', 'utf8', $data));
    $response->prepare($request);
} catch (\Exception $e) {
    $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
    $response->setContent($e->getMessage());
}

$response->send();
