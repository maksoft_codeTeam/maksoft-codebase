<?php
namespace Maksoft\Authentication;
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Password;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;


class Login extends Bootstrap
{  
    protected $db;

    public function __construct($post_data, $db) 
    {
        $this->username      = Text::init()
                                    ->add('placeholder', '������������� ���')
                                    ->add('required', true);
        $this->SiteID        = Hidden::init()
                                    ->add('value', \site::$sID);
        $this->pass          = Password::init()
                                    ->add('placeholder', '������')
                                    ->add('required', true);
        $this->op            = Submit::init()
                                    ->add("value", '����')
                                    ->add("class", "btn btn-success");

        $this->attributes['action'] = '/web/admin/login.php';
        $this->db = $db;

        parent::__construct($post_data);
    }
}
