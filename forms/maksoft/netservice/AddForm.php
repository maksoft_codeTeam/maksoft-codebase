<?php

use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Field;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Date;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class AddForm extends Bootstrap 
{
    protected $gate;

    public function __construct($post_data, $initial, $gate)
    {
        $this->gate = $gate;
        $bool_field = array(0=>"��", 1=>"��");
        $services = array(
            '��� ������ �����' => '��� ������ �����',
            '��� ������ Pro' => '��� ������ Pro',
            '��� ������ Pro+Content' => '��� ������ Pro+Content',
            '��� ������ Pro+Content 2' => '��� ������ Pro+Content 2',
            'O�������� ����� ������� ��� CMS' => 'O�������� ����� ������� ��� CMS',
        );

        $base_cls = 'form-control';

        $this->SiteID = Text::init()
            ->add('class', $base_cls)
            ->add_validator(NotEmpty::init(true)->err_msg('�� ������ �� �������� ��� ����� ��� �� ��� ������ SiteID'))
            ->add('label', 'ID �� �����');

        $this->fID = Text::init()
            ->add('class', $base_cls)
            ->add_validator(NotEmpty::init(true)->err_msg('�� ������ �� �������� ��� ����� ��� �� ��� ������ ������� � �������� ����� �� ��������'))
            ->add('label', 'fID �� ���������');

        $this->Visits = Text::init()
            ->add('class', $base_cls)
            ->add('value', 0)
            ->add('label', '���������');

        $this->maxVisits = Text::init()
            ->add('class', $base_cls)
            ->add('value', 100000)
            ->add('label', '���������� ���� ���������');

        $this->packname = Select::init()
            ->add('class', $base_cls)
            ->add('value', '��� ������ Pro+Content')
            ->add_validator(NotEmpty::init(true)->err_msg('����, �������� ������ �� �������� ����.'))
            ->add('data', $services)
            ->add('label', '������:');

        $this->Date = Text::init()
            ->add('type', 'date')
            ->add('class', $base_cls.' hasDatepicker')
            ->add_validator(NotEmpty::init(true)->err_msg('������ � ���������.'))
            ->add('label', '������� ����');

        $this->toDate = Text::init()
            ->add('type', 'date')
            ->add('id', 'toDate')
            ->add('class', $base_cls.' hasDatepicker')
            ->add_validator(NotEmpty::init(true)->err_msg('������ � ���������'))
            ->add('label', '������ ����');

        $this->active = Radio::init()
            ->add('data', array("��", "��"))
            ->add_validator(NotEmpty::init(true)->err_msg('����, �������� ���� � ������� ��� ��'))
            ->add('label', '�������');

        $this->netservice = Radio::init()
            ->add('data', array("��", "��"))
            ->add_validator(NotEmpty::init(true)->err_msg('����, �������� ���� � ��������� ���������'))
            ->add('label', '���������');

        $this->action = Hidden::init()->add('value', 'start_netservice');
        $this->submit = Submit::init()->add('value', '������')->add('class', 'btn btn-success');

        parent::__construct($post_data, null, $initial);
    }

    public function validate_fID($field)
    {
        if($this->gate->site()->host_cms_fID_is_unique($field->value, $this->SiteID->value)){
            return true;
        }

        throw new \Exception("�� ���� ���� ���� ��� ����� � ������� hostCMS, ���� ������������ ��. ");
    }

    public function save()
    {
        $Id = $this->gate
                   ->site()
                   ->insertHostCms($this->SiteID->value, $this->fID->value, $this->Visits->value, $this->maxVisits->value, 0,
                                   $this->packname->value, $this->Date->value, $this->toDate->value, $this->active->value, $this->netservice->value);
        return $Id;
    }
}

