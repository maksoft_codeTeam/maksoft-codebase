<?php
$document_root = realpath(__DIR__.'/../../../');
require_once __DIR__.'/BaseFormBootstrap.php';

if(!isset($o_page)){
    require_once $document_root.'/lib/lib_page.php';
    $o_page = new page();
}

$languages = array(
    1 => "bg",
    2 => "en",
    3 => "ru"
);

$lang = $languages[$o_page->_site['language_id']];

define("LANG",  $lang);


if(!defined("CAPTCHA_KEY")){
    define("CAPTCHA_KEY", $o_page->get_sConfigValue("reCAPTCHA_Site_key"));
}

if(!defined("CAPTCHA_SECRET")){
    define("CAPTCHA_SECRET", $o_page->get_sConfigValue("reCAPTCHA_Secret_key"));
}

