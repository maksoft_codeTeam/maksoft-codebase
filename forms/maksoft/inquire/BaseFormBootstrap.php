<?php
$document_root = realpath(__DIR__.'/../../../');
require_once $document_root.'/modules/vendor/autoload.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Field;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


abstract class BaseFormBootstrap extends Bootstrap
{
    protected $mailer;

    protected $cms;


    public function __construct($post_data=null, $cms=null, $email=null, $files=null, $initial=null)
    {
        $this->cms = $cms;

        $this->mailer = $email;

        parent::__construct($post_data, $files, $initial);
    }

    protected function to_utf8($string)
    {
        return iconv('cp1251', 'utf8', $string);
    }

    public function build_email_content()
    {
        $txt = '';
        foreach($_POST as $field_name => $value){
            $field = $this->$field_name;
            if(!$field instanceof Field){
                continue;
            }
            switch (true){
                case $field instanceof Checkbox:
                    $label = $this->get_label($field);
                    $txt .= '<br><b>'.$label.'</b>';
                    $txt .= '<ul><li>';
                    $attr = $field->as_array();
                    if(is_array($value)){
                        $txt .= implode('</li><li>', array_filter(array_map(function($choice) use ($value){
                            if(in_array($choice['value'], $value)){
                                return $choice['name'];
                            }
                        }, $attr)));
                    } else {
                        $txt .= implode('</li><li>', array_filter(array_map(function($choice) use ($value){
                            if($choice['value'] == $value){
                                return $choice['name'];
                            }
                        }, $attr)));
                    }

                    $txt .= '</li></ul>';
                    break;
                case $field instanceof Select:
                    $label = $this->get_label($field);
                    $txt .= '<br><b>'.$field->label.'</b>';
                    foreach($field->data as $val => $name){
                        if($val == $value){
                            $txt .= ' '. $name;
                            break;
                        }
                    }
                    break;
                case $field instanceof Hidden:
                    break;
                case $field instanceof Recaptcha:
                    break;
                case $field instanceof Submit:
                    break;
                default:
                    if($label = $this->get_label($field)){
                        $txt .= '<br><b>'.$label.'</b>';
                        $txt .= ' '. $value;
                    }

            }
        }
        $txt .= '<br>';
        return $txt;
    }

    protected function get_label(Field $field)
    {
        if($field->label) { return $field->label; }
        if($field->placeholder) { return $field->placeholder; }
        if($field->name) { return $field->name; }

        return false;
    }

    public function before_save($name, $email)
    {
        $translations = Translation::$form[$this->selected];
        $content = $this->build_email_content();
        if($action != $this->action->value){
            $action = '';
        }
        $subject = $this->cms->_site['Title'] . ' - '. $this->cms->_page['Name'] .' - '. $action;
        $order_id = $this->cms->insertIntoForms($name, $email, $content, $subject, $ftype="");
        if($this->mailer){
            $this->send_email($name, $email, $this->to_utf8($subject), $this->to_utf8($content));
        }
        return true;
    }

    private function send_email($name, $email, $subject, $content)
    {
        $from_email = (defined('CUSTOM_EMAIL') ? CUSTOM_EMAIL : $this->cms->_site['EMail']);
        $this->mailer->setFrom($from_email, $subject);
        $this->mailer->addAddress($email, $this->to_utf8($name));     // Add a recipient
        $this->mailer->addBcc($from_email, $subject);
        $this->mailer->addBcc('cc@maksoft.bg', $subject);
        $this->mailer->Subject = $subject;
        $this->mailer->Body    = $subject."<br>".$content;
        if(!$this->mailer->send()) {
            throw new \Exception($this->mailer->ErrorInfo);
        }
        return True;
    }
}
