<?php
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\MinLength;
use Jokuf\Form\Validators\MaxLength;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class InsertItem extends Bootstrap
{

    protected $gate;

    public function __construct($gate, $data, $files, $populate)
    {
        $this->gate = $gate;

        $groups = $this->gate->fak()->getGroups();
        $tmp = array();
        foreach($groups as $group){
            $tmp[$group->ID] = $group->itGName;
        }

        $this->itID = Integer::init()
                        ->add("class", "form-control")
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "Уникален номер на продукта SKU*:")
                        ->add_validator(NotEmpty::init(true)->err_msg("Полето продуктов код, трябва да бъде уникално и да не е празно!"));

        $this->itParentID = Text::init()
                        ->add("class", "form-control")
                        ->add("label", "Сходен продукт, който ще бъде предложен на потребителя при липса:")
                        ->add("value", 0);

        $this->itGroup = Select::init()
                        ->add("class", "form-control")
                        ->add("label", "Група на продукта*:")
                        ->add("groups",  $tmp);
    
        $this->itName = Text::init()
                        ->add_validator(MaxLength::init(19)->err_msg("Максимално допустимата дължина е 20 символа!"))
                        ->add_validator(MinLength::init(4)->err_msg("Минимално допустимата дължина е 5 символа!"))
                        ->add_validator(NotEmpty::init(true)->err_msg("Полето itName, трябва да бъде уникално и да не е празно!"))
                        ->add("class", "form-control")
                        ->add("label", "Кратко име на продукта*:"); 

        $this->itStr = Text::init()
                        ->add_validator(NotEmpty::init(true)->err_msg("Полето itStr, трябва да бъде уникално и да не е празно!"))
                        ->add_validator(MaxLength::init(95)->err_msg("Максимално допустимата дължина е 96 символа!"))
                        ->add_validator(MinLength::init(4)->err_msg("Минимално допустимата дължина е 5 символа!"))
                        ->add("class", "form-control")
                        ->add("label", "Име на продукта [описание]*:"); 

        $this->it_weight = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 0)
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "Тегло на продукта в грамове*:");

        $this->dost_price = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 0)
                        ->add('step', '0.01')
                        ->add("label", "Доставна цена*:"); 

        $this->qty_on_stock = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 0)
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "К-во на склад");

        $this->price = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 0)
                        ->add('step', '0.01')
                        ->add("label", "продажна цена*:");

        $this->itPeriod = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 3)
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "Период:");

        $this->commision = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 10)
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "Комисионна:"); 
        $this->discount = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 10)
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "Отстъпка:");

        $this->m = Radio::init()
                        ->add("label", "Мерна единица:")
                        ->add("data", $this->gate->fak()->getMeasurementUnits());


        $this->it_docID = Radio::init()
                        ->add("label", "Тип договор:")
                        ->add('value', 0)
                        ->add("data", array(0  => 'Договор стандартен', 
                                            50 => 'Договор интернет', 
                                            51 => 'Договор СЕО',
                                            52 => 'Договор ORM',
                                            11 => 'Договор стоки',
                                            53 => 'Договор СЕО Одит'));
        $this->prior = Radio::init()
                        ->add('value', 0)
                        ->add("data", range(0,10))
                        ->add("label", "Приоритет:");

        $this->slType = Hidden::init()
                        ->add("value", 0);

        $this->submit = Submit::init()->add("class", "btn btn-primary");

        parent::__construct($data, $files, $populate);
    }

    public function validate_itID($field)
    {
        $response = $this->gate->fak()->getProduct($field->value)->itID;
        if($response or $response == $field->value){
            throw new \Exception("Въведохте вече съществуващ продуктов код. Към този момент няма функционалност за промяна на продукт. Моля, опитайте отново!");
        }
        return true;
    }

    public function save()
    {
        $clean_data= $this->clean_data();
        $tmp = array();
        foreach($clean_data as $key=>$value){
            if($key == 'submit'){
                continue;
            }
            $tmp[':'.$key] = $value;
        }
        return $this->gate->fak()->createItem($tmp);
    }
}
