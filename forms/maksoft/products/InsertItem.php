<?php
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\MinLength;
use Jokuf\Form\Validators\MaxLength;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\Bootstrap;


class InsertItem extends Bootstrap
{

    protected $gate;

    public function __construct($gate, $data, $files, $populate)
    {
        $this->gate = $gate;

        $groups = $this->gate->fak()->getGroups();
        $tmp = array();
        foreach($groups as $group){
            $tmp[$group->ID] = $group->itGName;
        }

        $this->itID = Integer::init()
                        ->add("class", "form-control")
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "�������� ����� �� �������� SKU*:")
                        ->add_validator(NotEmpty::init(true)->err_msg("������ ��������� ���, ������ �� ���� �������� � �� �� � ������!"));

        $this->itParentID = Text::init()
                        ->add("class", "form-control")
                        ->add("label", "������ �������, ����� �� ���� ��������� �� ����������� ��� �����:")
                        ->add("value", 0);

        $this->itGroup = Select::init()
                        ->add("class", "form-control")
                        ->add("label", "����� �� ��������*:")
                        ->add("groups",  $tmp);
    
        $this->itName = Text::init()
                        ->add_validator(MaxLength::init(25)->err_msg("���������� ����������� ������� � 20 �������!"))
                        ->add_validator(MinLength::init(4)->err_msg("��������� ����������� ������� � 5 �������!"))
                        ->add_validator(NotEmpty::init(true)->err_msg("������ itName, ������ �� ���� �������� � �� �� � ������!"))
                        ->add("class", "form-control")
                        ->add("label", "������ ��� �� ��������*:"); 

        $this->itStr = Text::init()
                        ->add_validator(NotEmpty::init(true)->err_msg("������ itStr, ������ �� ���� �������� � �� �� � ������!"))
                        ->add_validator(MaxLength::init(95)->err_msg("���������� ����������� ������� � 96 �������!"))
                        ->add_validator(MinLength::init(4)->err_msg("��������� ����������� ������� � 5 �������!"))
                        ->add("class", "form-control")
                        ->add("label", "��� �� �������� [��������]*:"); 

        $this->it_weight = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 0)
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "����� �� �������� � �������*:");

        $this->dost_price = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 0)
                        ->add('step', '0.01')
                        ->add("label", "�������� ����*:"); 

        $this->qty_on_stock = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 0)
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "�-�� �� �����");

        $this->price = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 0)
                        ->add('step', '0.01')
                        ->add("label", "�������� ����*:");

        $this->itPeriod = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 3)
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "������:");

        $this->commision = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 10)
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "����������:"); 
        $this->discount = Integer::init()
                        ->add("class", "form-control")
                        ->add("value", 10)
                        ->add("step", 1)
                        ->add("min", 0)
                        ->add("label", "��������:");

        $this->m = Radio::init()
                        ->add("label", "����� �������:")
                        ->add("data", $this->gate->fak()->getMeasurementUnits());


        $this->it_docID = Radio::init()
                        ->add("label", "��� �������:")
                        ->add('value', 0)
                        ->add("data", array(0  => '������� ����������', 
                                            50 => '������� ��������', 
                                            51 => '������� ���',
                                            52 => '������� ORM',
                                            11 => '������� �����',
                                            53 => '������� ��� ����'));
        $this->prior = Radio::init()
                        ->add('value', 0)
                        ->add("data", range(0,10))
                        ->add("label", "���������:");

        $this->slType = Hidden::init()
                        ->add("value", user::$uID);

        $this->submit = Submit::init()->add("class", "btn btn-primary");

        parent::__construct($data, $files, $populate);
    }

    public function validate_itID($field)
    {
        $response = $this->gate->fak()->getProduct($field->value)->itID;
        if($response or $response == $field->value){
            throw new \Exception("��������� ���� ����������� ��������� ���. ��� ���� ������ ���� �������������� �� ������� �� �������. ����, �������� ������!");
        }
        return true;
    }

    public function save()
    {
        $clean_data= $this->clean_data();
        $tmp = array();
        foreach($clean_data as $key=>$value){
            if($key == 'submit'){
                continue;
            }
            $tmp[':'.$key] = $value;
        }
        return $this->gate->fak()->createItem($tmp);
    }
}
