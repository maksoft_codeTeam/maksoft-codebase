<?php

require_once __DIR__.'/../../../modules/vendor/autoload.php';
require_once __DIR__.'/InsertItem.php';
if(!isset($o_page)){
    return;
}
if($o_page->_user['AccessLevel'] < 2){
    return;
}


$db = new PDO('mysql:host=127.0.0.1;dbname=maksoft', 'maksoft', 'mak211');
$gate = new \Maksoft\Gateway\Gateway($db);
$product = array();
if(isset($_GET['itID']) and is_numeric($_GET['itID'])){
    $product = $gate->fak()->getProduct($_GET['itID']);
}
$item = new InsertItem($gate,$_POST, $_FILES, (array) $product);


if($_SERVER['REQUEST_METHOD'] === 'POST'){
    try{
        $item->is_valid();
        $itID = $item->save();
        $item = sprintf('������� ��������� %s � ��������� ��� "%s"</h2>', $item->itStr->value, $itID);
        $item .= sprintf('<hr><a class="btn btn-primary" href="'.$o_page->get_pLink().'">������ ���</a><p></p>');
    } catch(\Exception $e){
        throw new \Exception($e);
        echo $e->getMessage();
        foreach($item as $field){
            echo '<ul><li>'.implode('</li><li>', $field->get_errors()).'</li></ul>';
        }
    }
}
?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <?=$item?>
    </div>
</div>
