<?php
$document_root = realpath(__DIR__.'/../../../');
require_once $document_root.'/modules/vendor/autoload.php';
require_once $document_root.'/lib/Database.class.php';
require_once __DIR__.'/InsertItem.php';


$db = new Database();

$gate = new \Maksoft\Gateway\Gateway($db);


$f = new InsertItem($_POST, $gate);
$f->form_class("form-horizontal");
$f->div_class("col-md-6");
if( $_SERVER['REQUEST_METHOD'] === "POST"){
    try {
        $f->is_valid();
    } catch(Exception $e){
        echo $e->getMessage();
    }
}
echo $f;
