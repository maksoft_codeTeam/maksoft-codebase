<?php
use Jokuf\Form\DivForm;
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\TextArea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;

use \Maksoft\Gateway\Domain\Page\PageGateway;


class InsertNewComment extends DivForm
{
    protected $page;

    protected $n;

    public function __construct(PageGateway $page, $n, $post_data){

        $this->page = $page;

        $this->n = $n;

        $this->author = Text::init()
            ->add("label", "�����")
            ->add("required", true)
            ->add_validator(
                NotEmpty::init(true)
                    ->err_msg("���� ���� �� ���� �� ���� ������")
            );

        $this->email = Text::init()
            ->add("label", "����� �����:")
            ->add("required", true)
            ->add_validator(
                NotEmpty::init(true)
                    ->err_msg("���� ���� �� ���� �� ���� ������")
            );

        $this->subject = Text::init()
            ->add("label", "��������: ")
            ->add("required", true);

        $this->text = Text::init()
            ->add("label", "������ ��������: ")
            ->add("required", true)
            ->add_validator(
                NotEmpty::init(true)
                    ->err_msg("���� ���� �� ���� �� ���� ������")
            );

        $this->parent = Hidden::init()
            ->add("value", "0")
            ->add_validator(
                NotEmpty::init(true)
                    ->err_msg("���� ���� �� ���� �� ���� ������")
            );

        $this->user_id = Hidden::init()
            ->add("value", 0)
            ->add_validator(
                NotEmpty::init(true)
                    ->err_msg("���� ���� �� ���� �� ���� ������")
            );
        $this->image  = Hidden::init();

        $this->action = Hidden::init()
            ->add("value", "insert_comment")
            ->add_validator(
                NotEmpty::init(true)
                    ->err_msg("���� ���� �� ���� �� ���� ������")
            );

        $this->status = Hidden::init()
            ->add("value", 0)
            ->add_validator(
                NotEmpty::init(true)
                    ->err_msg("���� ���� �� ���� �� ���� ������")
            );

        parent::__construct($post_data);
    }

    public function validate_image($field){
        if(empty($field->value)){
            $field->value = "http://maksoft.net/web/forms/comments/wmd/images/avatar.jpg";
        }
        return true;
    }
    public function validate_subject($field){
        $this->subject->value = strip_tags($field->value);
        return true;
    }

    public function validate_author($field){
        $this->subject->value = strip_tags($field->value);
        return true;
    }

    public function validate_text($field){
        $this->subject->value = strip_tags($field->value, '<ul><li><div><ol><h1><h2><h3><h4><pre><code><p><em><strong><blockquote>');
        return true;
    }
    public function save(){
        $this->page->insertComment(
            $this->n,
            $this->subject->value,
            $this->author->value,
            $this->email->value,
            $this->text->value,
            $this->image->value,
            "",
            $this->user_id->value,
            $this->parent->value,
            $this->status->value
        );
    }
}
