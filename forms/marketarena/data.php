<?php 


class Translation {
    public static $form = array(
        "registration" => array(
            "footer" => array(
                "bg" => "*������������ ����",
                "en" => "*Obligatory field",
                "ru" => "*������������ ����",
            ),
            "title" => array(
                "bg" => array(
                    "�� ����������� �� �������� ������ - ���� ������� � ������� ���������, ��������� ���������",
                    "�� �� �������� �������� ������, ���� ��������� ������� �� �������, �� �� �� �������� ������. �� �� ���� ���������� � ������� �� 24 ����."
                ),
                "en" => array(
                    "Registration for extended access � Newsletter and Market platform, Marketing desk",
                    "For an extended access please fill in the contact form so we can send you a mail containing your password. You will be contacted within 24 hours."
                ),
                "ru" => array(
                    "����������� ��� ������������ ������� - ���� ��������� � �������� ���������, ��������� ���������",
                    "��� ������������ �������, ������ ��������� ���������� ����� � �� ����������� ��� ������. ����� ������ ��������� � ������� �����."
                ),
            ),
            'email_text' => array(
                'bg' => array(
                    "title" => "������� �����������",
                    "content" => "���� � ������������, �� ������ ��������� � ���� �� �������� ������ ���� ����������� � �������� � �� ���� ���������� �������� ���-�����.
                    ���� ��������� �� ������� ���������, � ����� �� ������������ ������ �� ������."
                ),
                'en' => array(
                    "title" => "Succesfull registration",
                    "content" => "This is a confirmation that your mail request for extended access through <strong>registration</strong> has been received and will be processed at earliest convenience.
                Please check for an incoming mail providing you with an <strong>access password.</strong>"
                ),
                'ru' => array(
                    "title" => "�������� �����������",
                    "content" => "��������� ������������� � ���, ��� ��� ������ ������ �� ����������� ������ ����� ����������� ������� � ����� ��������� � ��������� �����.
                    ������ ���������� �� �������� ����������, ������� ������������� ��� ������ � �������."
                ),
            ),
            "name" => array(
                "bg" => array("label" => "���:*"),
                "en" => array("label" => "Name:*"),
                "ru" => array("label" => "���:*"),
            ),
            "company" => array(
                "bg" => array("label" => "�����/�����������:"),
                "en" => array("label" => "Company/Organization:"),
                "ru" => array("label" => "��������/�����������:"),
            ),
            "terms" => array(
                "bg" => array(
                    "label" => "���� �������:",
                    "data"  => array("������� ������ �������"),
                    "error" => "������ �� ��� �������� � ������ �������, �� �� ��������� �����������" 
                ),
                "en" => array(
                    "label" => "Terms of use:",
                    "data" => array("I accept website terms of use"),
                    "error" => "You must have accepted the general terms and conditions to submit this inquiry"
                ),
                "ru" => array(
                    "label" => "������� ����������� �������� ������:", 
                    "data" => array("� �������� ������� ����������� �������� ������"),
                    "error" => "�� ������ ���� �������� ����� ������� ��� �������� �������"
                ),
            ),
            "branch" => array(
                "bg" => array(
                    "label"=>"�����/�������:*",
                    "data" => array(
                        "�����-�������",
                        "����������",
                        "���������",
                        "������������",
                        "������",
                        "������",
                        "������ � ����������",
                        "������� �������� ��������",
                        "����",
                        "������������ �����",
                        "������ ����������",
                        "�����",
                        "���������",
                        "��������",
                        "��������� � �������",
                        "�����������",
                        "��������������",
                        "�������� �������������",
                        "������",
                        "�����",
                    )
                ),
                "en" => array(
                    "label"=>"Industry/Activity:",
                    "data" => array(
                        "Mining",
                        "Energy",
                        "Chemical",
                        "Construction",
                        "Metals",
                        "Rubber",
                        "Industrial equipment",
                        "Motor vehicles",
                        "Tyres",
                        "Lubricants",
                        "Agriculture",
                        "Food",
                        "Textile",
                        "Trading",
                        "Logistics and Freight",
                        "Education",
                        "Health services",
                        "Public administration",
                        "Services",
                        "Others",
                    )
                ),
                "ru" => array(
                    "label"=>"�������/������������:*",
                    "data"=> array(
                        "���������������",
                        "����������",
                        "����������",
                        "�������������",
                        "�������",
                        "������",
                        "������ � ������������",
                        "����������",
                        "����",
                        "�������������� �����",
                        "�������� ���������",
                        "�������",
                        "�����������",
                        "��������",
                        "��������� � ���������",
                        "�����������",
                        "���������������",
                        "��������� �������������",
                        "������",
                        "������",
                    )
                ),
            ),
            "email" => array(
                "bg" => array("label" => "����*"),
                "en" => array("label" => "E-mail:*"),
                "ru" => array("label" => "����������� �����:*"),
            ),
            "country" => array(
                "bg" => array("label" => "������:"),
                "en" => array("label" => "Country:"),
                "ru" => array("label" => "������:"),
            ),
            "phone" => array(
                "bg" => array("label" => "�������:"),
                "en" => array("label" => "Phone number:"),
                "ru" => array("label" => "�������:"),
            ),
            "access" => array(
                "bg" => array(
                    "label" => "����� �������� ������ ��:*", 
                    "data" => array("��", "��")
                ),
                "en" => array(
                    "label" => "Extended access to:*",
                    "data" => array("Yes", "No")
                ),
                "ru" => array(
                    "label" => "����� ������������ ������� �:*",
                    "data" => array("��", "���")
                ),
            ),
            "bulletin" => array(
                "bg" => array(
                    "label"=>"�������:", 
                    "data" => array(
                        "�������",
                        "������",
                        "������ ���������� - ��������",
                        "����� ���������� - ����������, ���.�����",
                        "����� � �������� ���������",
                        "����",
                    )
                ),
                "en" => array(
                    "label"=> "Newsletter:",
                    "data" => array(
                        "Steel",
                        "Rubber",
                        "Agriculture - raw materials",
                        "Mining equipment - components, spares",
                        "Lubricants",
                        "Tyres",
                    )
                ),
                "ru" => array(
                    "label"=>"���������:",
                    "data" => array(
                        "�����",
                        "������",
                        "�������� ��������� - �����",
                        "������ ������������ - �������������, ��������",
                        "����� � ��������� ���������",
                        "����",
                    )
                ),
            ),
            "trade_platform" => array(
                "bg" => array(
                    "label" => "������� ���������:",
                    "data" => array(
                        "���������� �����",
                        "������� �����"
                    )
                ),
                "en" => array(
                    "label" => "Market platform:",
                    "data" => array(
                        "Goods offered",
                        "Goods demanded",
                    )
                ),
                "ru" => array(
                    "label" => "�������� ���������:", 
                    "data" => array(
                        "������������ ������",
                        "������������� ������",
                    )
                ),
            ),
            "marketing_platform" => array(
                "bg" => array(
                    "label" => "��������� ���������:",
                    "data" => array(
                        "�������, �������, ������ ������, ���������",
                        "�������"
                    )
                ),
                "en" => array(
                    "label" => "Marketing desk:",
                    "data" => array(
                        "Exhibitions, fairs, business forums, interviews",
                        "Advertisment",
                    )
                ),
                "ru" => array(
                    "label" => "��������� ���������:", 
                    "data" => array(
                        "��������, �������, ������ ������, ��������",
                        "�������",
                    )
                ),
            ),
            "details" => array(
                "bg" => array("label"=>"���������:"),
                "en" => array("label"=>"Message:"),
                "ru" => array("label"=>"���������:"),
            ),
            "submit" => array(
                "bg" => array("label"=>"�������"),
                "en" => array("label"=>"Send"),
                "ru" => array("label"=>"���������"),
            ),
        ),
        "subscription" => array(
            "submit" => array(
                "bg" => array("label"=>"�������"),
                "en" => array("label"=>"Send"),
                "ru" => array("label"=>"���������"),
            ),
            "footer" => array(
                "bg" => "*������������ ����",
                "en" => "*Obligatory field",
                "ru" => "*������������ ����",
            ),

            "terms" => array(
                "bg" => array(
                    "label" => "���� �������:",
                    "data"  => array("������� ������ �������"),
                    "error" => "������ �� ��� �������� � ������ �������, �� �� ��������� �����������" 
                ),
                "en" => array(
                    "label" => "Terms of use:",
                    "data" => array("I accept website terms of use"),
                    "error" => "You must have accepted the general terms and conditions to submit this inquiry"
                ),
                "ru" => array(
                    "label" => "������� ����������� �������� ������:", 
                    "data" => array("� �������� ������� ����������� �������� ������"),
                    "error" => "�� ������ ���� �������� ����� ������� ��� �������� �������"
                ),
            ),
            "title" => array(
                "bg" => array(
                    "�� ����������� �� ����� ������ ���� ���������",
                    "�� �� �������� ����� ������, ���� ��������� ������� �� �������, �� �� �� ��������� � ����������� ������ ������ ��������� �������������. �� �� ���� ���������� � ������� �� 24 ����."
                ),
                "en" => array(
                    "Registration for extended access � Newsletter and Market platform, Marketing desk",
                    "For an extended access please fill in the contact form so we can send you a mail containing your password. You will be contacted within 24 hours."
                ),
                "ru" => array(
                    "����������� ��� ������� ������� ����� ��������",
                    "��� ������� �������, ������ ��������� ���������� ����� � �� ������� ��� ������������ �������� ����� ���������� ��������������. ����� ������ ��������� � ������� �����."
                ),
            ),
            'email_text' => array(
                'bg' => array(
                    "title" => "������� ����������� �� ���������",
                    "content" => "���� � ������������, �� ������ ��������� � ���� �� ����� ������ ���� <strong>���������</strong> � ��������, �� ���� ���������� � �������� ���-����� �� �� ����������� ��������� �� ���������.
                    ���� ��������� �� ������� ���������, � ����� �� ������������ <strong>������ �� ������.</strong>"
                ),
                'en' => array(
                    "title" => "Successfull registration for subscription access",
                    "content" => "This is a confirmation that your mail request for full access through <strong>subscription<strong> has been received, will be processed and at earliest convenience you will be informed of the subscription terms. Please check for an incoming mail providing you with an <strong>access password.</strong>"
                ),
                'ru' => array(
                    "title" => "�������� ����������� ��� ����������",
                    "content" => "��������� ������������� � ���, ��� ��� ������ ������ �� ������ ������ ����� ���������� �������, ����� ��������� � � ��������� ����� ��� ����� ������������� ������� ��� ����������. ������ ���������� �� �������� ����������, ������� ������������� ��� <strong>������ � �������.</strong>"
                ),
            ),
            "name" => array(
                "bg" => array("label" => "���:*"),
                "en" => array("label" => "Name:*"),
                "ru" => array("label" => "���:*"),
            ),
            "company" => array(
                "bg" => array("label" => "�����/�����������:"),
                "en" => array("label" => "Company/Organization:"),
                "ru" => array("label" => "��������/�����������:"),
            ),
            "branch" => array(
                "bg" => array(
                    "label"=>"�����/�������:*",
                    "data" => array(
                        "�����-�������",
                        "����������",
                        "���������",
                        "������������",
                        "������",
                        "������",
                        "������ � ����������",
                        "������� �������� ��������",
                        "����",
                        "������������ �����",
                        "������ ����������",
                        "�����",
                        "���������",
                        "��������",
                        "��������� � �������",
                        "�����������",
                        "��������������",
                        "�������� �������������",
                        "������",
                        "�����",
                    )
                ),
                "en" => array(
                    "label"=>"Industry/Activity:",
                    "data" => array(
                        "Mining",
                        "Energy",
                        "Chemical",
                        "Construction",
                        "Metals",
                        "Rubber",
                        "Industrial equipment",
                        "Motor vehicles",
                        "Tyres",
                        "Lubricants",
                        "Agriculture",
                        "Food",
                        "Textile",
                        "Trading",
                        "Logistics and Freight",
                        "Education",
                        "Health services",
                        "Public administration",
                        "Services",
                        "Others",
                    )
                ),
                "ru" => array(
                    "label"=>"�������/������������:*",
                    "data"=> array(
                        "���������������",
                        "����������",
                        "����������",
                        "�������������",
                        "�������",
                        "������",
                        "������ � ������������",
                        "����������",
                        "����",
                        "�������������� �����",
                        "�������� ���������",
                        "�������",
                        "�����������",
                        "��������",
                        "��������� � ���������",
                        "�����������",
                        "���������������",
                        "��������� �������������",
                        "������",
                        "������",
                    )
                ),
            ),
            "email" => array(
                "bg" => array("label" => "����*"),
                "en" => array("label" => "E-mail:*"),
                "ru" => array("label" => "����������� �����:*"),
            ),
            "country" => array(
                "bg" => array("label" => "������:"),
                "en" => array("label" => "Country:"),
                "ru" => array("label" => "������:"),
            ),
            "phone" => array(
                "bg" => array("label" => "�������:"),
                "en" => array("label" => "Phone number:"),
                "ru" => array("label" => "�������:"),
            ),
            "access" => array(
                "bg" => array(
                    "label" => "����� �������� ������ ��:*", 
                    "data" => array("��", "��")
                ),
                "en" => array(
                    "label" => "Extended access to:*",
                    "data" => array("Yes", "No")
                ),
                "ru" => array(
                    "label" => "����� ������������ ������� �:*",
                    "data" => array("��", "���")
                ),
            ),
            "bulletin" => array(
                "bg" => array(
                    "label"=>"�������:", 
                    "data" => array(
                        "�������",
                        "������",
                        "������ ���������� - ��������",
                        "����� ���������� - ����������, ���.�����",
                        "����� � �������� ���������",
                        "����",
                    )
                ),
                "en" => array(
                    "label"=> "Newsletter:",
                    "data" => array(
                        "Steel",
                        "Rubber",
                        "Agriculture - raw materials",
                        "Mining equipment - components, spares",
                        "Lubricants",
                        "Tyres",
                    )
                ),
                "ru" => array(
                    "label"=>"���������:",
                    "data" => array(
                        "�����",
                        "������",
                        "�������� ��������� - �����",
                        "������ ������������ - �������������, ��������",
                        "����� � ��������� ���������",
                        "����",
                    )
                ),
            ),
            "trade_platform" => array(
                "bg" => array(
                    "label" => "������� ���������:",
                    "data" => array(
                        "���������� �����",
                        "������� �����"
                    )
                ),
                "en" => array(
                    "label" => "Market platform:",
                    "data" => array(
                        "Goods offered",
                        "Goods demanded",
                    )
                ),
                "ru" => array(
                    "label" => "�������� ���������:", 
                    "data" => array(
                        "������������ ������",
                        "������������� ������",
                    )
                ),
            ),
            "marketing_platform" => array(
                "bg" => array(
                    "label" => "��������� ���������:",
                    "data" => array(
                        "�������, �������, ������ ������, ���������",
                        "�������"
                    )
                ),
                "en" => array(
                    "label" => "Marketing desk:",
                    "data" => array(
                        "Exhibitions, fairs, business forums, interviews",
                        "Advertisment",
                    )
                ),
                "ru" => array(
                    "label" => "��������� ���������:", 
                    "data" => array(
                        "��������, �������, ������ ������, ��������",
                        "�������",
                    )
                ),
            ),
            "details" => array(
                "bg" => array("label"=>"���������:"),
                "en" => array("label"=>"Message:"),
                "ru" => array("label"=>"���������:"),
            ),
        ),
    );
}
