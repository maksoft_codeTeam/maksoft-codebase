<?php 
$document_root = realpath(__DIR__.'/../../');
require_once $document_root.'/modules/vendor/autoload.php';
require_once __DIR__.'/Registration.php';
require_once __DIR__.'/Subscription.php';
require_once __DIR__.'/Bootstrap.php';
require_once __DIR__.'/data.php';

$lang = $o_page->_site['language_id'];
$secret = "6LdpARkUAAAAAFBMH3IEQxV7OK0OSSI5KPQavXBq";

$copyright_link = $o_page->get_pLink($o_page->_site['copyright_n']);
define("TERMS_LINK", $copyright_link);

$recaptcha = new \ReCaptcha\ReCaptcha($secret);

