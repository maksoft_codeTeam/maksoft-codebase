<?php 
require_once __DIR__.'/index.php';

$languages = array(
    1 => "bg",
    2 => "en",
    3 => "ru"
);

$lang = $languages[$lang];


$form = new Subscription($_POST, $lang);

$translations = Translation::$form['subscription'];
# foreach($translations['title'][$lang] as $title){
#     echo "$title<hr>";
# }

$form->form_class("form-horizontal");

if($_SERVER['REQUEST_METHOD'] === "POST"){
    try{
        $form->is_valid();
        if($form->save($o_page)){
            echo "<p class=\"bg-success\">".$translations['email_text'][$lang]['title']."</p>";
            $form = '';
        }
    } catch (\Jokuf\Form\Exceptions\ValidationError $e){
    }
}

echo $form;
echo "<h4>".$translations['footer'][$lang]."<h4>";
