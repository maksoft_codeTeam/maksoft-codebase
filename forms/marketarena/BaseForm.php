<?php
require_once __DIR__.'/Bootstrap.php';
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Field;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Exceptions\ValidationError;
use Jokuf\Form\DivForm;


abstract class BaseForm extends Bootstrap
{
    private $selected = 'registration';

    private $lang;

    public function __construct($post_data=null, $lang)
    {
        $translations = Translation::$form[$this->selected];
        $this->lang = $lang;
        $not_empty = NotEmpty::init(true)
                        ->err_msg("���� ���� �� ���� �� ���� ������!");
        $this->name               = Text::init()
                                      ->add("label", $translations['name'][$lang]['label'])
                                      ->add("class", "form-control")
                                      ->add_validator($not_empty)
                                      ->add("required", true);

        $this->company_name       = Text::init()
                                      ->add("class", "form-control")
                                      ->add("label", $translations['company'][$lang]['label']);

        $this->branch             = Select::init()
                                      ->add("class", "form-control")
                                      ->add("label", $translations['branch'][$lang]['label'])
                                      ->add("data", $translations['branch'][$lang]['data']);

        $this->email              = Email::init()
                                      ->add("class", "form-control")
                                      ->add("label", $translations['email'][$lang]['label'])
                                      ->add_validator($not_empty)
                                      ->add("required", true);

        $this->country            = Text::init()
                                      ->add("class", "form-control")
                                      ->add("label", $translations['country'][$lang]['label']);

        $this->phone              = Phone::init()
                                      ->add("class", "form-control")
                                      ->add("label", $translations['phone'][$lang]['label']);

        # $this->access             = Radio::init()
        #                               ->add("label", $translations['access'][$lang]['label'])
        #                               ->add("data", $translations['access'][$lang]['data']);

        $this->bulletin           = Checkbox::init()
                                      ->add("name", "bulletin[]")
                                      ->add("label", $translations['bulletin'][$lang]['label'])
                                      ->add("data", $translations['bulletin'][$lang]['data']);

        $this->trade_platform     = Radio::init()
                                      ->add("label", $translations['trade_platform'][$lang]['label'])
                                      ->add("data", $translations['trade_platform'][$lang]['data']);

        $this->marketing_platform = Checkbox::init()
                                      ->add("label", $translations['marketing_platform'][$lang]['label'])
                                      ->add("data", $translations['marketing_platform'][$lang]['data']);

        $this->details            = Textarea::init()
                                      ->add("class", "form-control")
                                      ->add("rows", 5)
                                      ->add("label", $translations['details'][$lang]['label']);

        $this->action             = Hidden::init()
                                      ->add("value", "registration");

        $terms_link = "<a target=\"_blank\" href=\"".TERMS_LINK."\">%s</a>";
        $terms_error = sprintf($terms_link, $translations['terms'][$lang]['error']);

        $question_icon = '<i class="fa fa-file-text-o" aria-hidden="true"></i>';

        $this->terms              = Checkbox::init()
                                      ->add("required", true)
                                      ->add_validator(NotEmpty::init(true)
                                                                ->err_msg($terms_error)
                                                        )
                                      ->add("label", sprintf($terms_link, $question_icon . ' ' .$translations['terms'][$lang]['label']))
                                      ->add("data", $translations['terms'][$lang]['data']);

        $this->recaptcha          = Recaptcha::init()
                                      ->add("lang", $lang)
                                      ->add("site_key", "6LdpARkUAAAAAG9qgwPWFtMzBCSsQqXkbNI_NHaZ")
                                      ->add("secret", "6LdpARkUAAAAAFBMH3IEQxV7OK0OSSI5KPQavXBq");

        $this->submit             = Submit::init()
                                      ->add("value", $translations['submit'][$lang]['label'])
                                      ->add("class", "btn btn-success");

        parent::__construct($post_data);
    }

    protected function to_utf8($string)
    {
        return iconv('cp1251', 'utf8', $string);
    }

    public function build_email_content()
    {
        $txt = '';
        $translations = Translation::$form[$this->selected];
        foreach($_POST as $field_name => $value){
            switch (true){
                case $this->$field_name instanceof Checkbox:
                    $txt .= '<br><b>'.$this->$field_name->label.'</b>';
                    $txt .= '<ul><li>';
                    $attr = $this->$field_name->as_array();
                    if(is_array($value)){
                        $txt .= implode('</li><li>', array_filter(array_map(function($choice) use ($value){
                            if(in_array($choice['value'], $value)){
                                return $choice['name'];
                            }
                        }, $attr)));
                    } else {
                        $txt .= implode('</li><li>', array_filter(array_map(function($choice) use ($value){
                            if($choice['value'] == $value){
                                return $choice['name'];
                            }
                        }, $attr)));
                    }

                    $txt .= '</li></ul>';
                    break;
                case $this->$field_name instanceof Select:
                    $txt .= '<br><b>'.$this->$field_name->label.'</b>';
                    foreach($this->$field_name->data as $val => $name){
                        if($val == $value){
                            $txt .= ' '. $name;
                            break;
                        }
                    }
                    break;
                case $this->$field_name instanceof Hidden:
                    break;
                case $this->$field_name instanceof Recaptcha:
                    break;
                case $this->$field_name instanceof Submit:
                    break;
                default:
                    $txt .= '<br><b>'.$this->$field_name->label.'</b>';
                    $txt .= ' '. $value;
            }
        }
        $txt .= '<br>';
        return $txt;
    }

    public function save($o_page)
    {
        $translations = Translation::$form[$this->selected];
        $content = $translations['email_text'][$this->lang]['content']."<br>".$this->build_email_content();
        $subject = $translations['email_text'][$this->lang]['title'];
        $order_id = $o_page->insertIntoForms($this->name->value, $this->email->value, $content, $subject, $ftype="");
        $this->send_email($o_page->_site['Title']);
        return true;
    }

    private function send_email($site_title)
    {
        $translations = Translation::$form[$this->selected];
        $mail = new \PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'server.maksoft.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->CharSet = "utf8";
        $mail->Username = 'sales@marketarena.eu';                 // SMTP username
        $mail->Password = 'ag5tpTaq5kKu';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
        $mail->setFrom('sales@marketarena.eu', $this->to_utf8($site_title));
        $mail->addAddress($this->email->value, $this->to_utf8( $this->name->value));     // Add a recipient
        $mail->addBcc('sales@marketarena.eu', $this->to_utf8($translations['email_text'][$this->lang]['title']));
        $mail->addBcc('cc@maksoft.bg', $this->to_utf8($translations['email_text'][$this->lang]['title']));
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $this->to_utf8($translations['email_text'][$this->lang]['title']);
        $mail->Body    = $this->to_utf8($translations['email_text'][$this->lang]['content']."<br>".$this->build_email_content());
        if(!$mail->send()) {
            throw new \Exception($mail->ErrorInfo);
        }
        return True;
    }
}
