<?php
namespace Casting\Authentication;
use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Password;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;


class Login extends Bootstrap
{  
    protected $db;

    public function __construct($post_data, $db) 
    {
        $this->db = $db;
        $this->email = Email::init()
            ->add('placeholder', '����� �����')
            ->add('required', true);
        $this->password = Password::init()
            ->add('placeholder', '������')
            ->add('required', true);
        $this->action = Hidden::init()->add('value', 'login_casting');

        $this->capcha     = Recaptcha::init()
                              ->add("lang", LANG)
                              ->add("site_key", CAPTCHA_KEY)
                              ->add("secret", CAPTCHA_SECRET);

        $this->submit        = Submit::init()
                              ->add("value", '����')
                              ->add("class", "btn btn-success");

        parent::__construct($post_data);

    }

    public function save($o_page)
    {
        $user = $this->db->get_statist_by('email', $this->email->value);
        if (!$user) {
            throw new \Exception('������������� ����������');
        }
        if(!\user::check($this->password->value, $user['password'])) {
            throw new \Exception('������ ������');
        }

        if(!$m_user = $o_page->get_user_by($this->db->db, 'username', $this->email->value)){
            throw new \Exception('������������� ����������');
        }
        $_SESSION['user'] = $m_user;

        $this->authenticate($user);
        echo '<script>location="/page.php?n='.$m_user->init_page.'&SiteID='.$m_user->SiteID.'";</script>';
    }

    protected function authenticate($user) {
        $_SESSION['abstract_user'] = (object) $user;
    }
}
