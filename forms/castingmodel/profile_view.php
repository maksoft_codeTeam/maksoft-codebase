<?php

require_once __DIR__.'/base.php';
require_once __DIR__.'/AgencyModelProfileEdit.php';

$uid = $_GET['appl'];
$applicant = $model->get_single_applicant($uid);
$images = $model->get_images_list($uid);
$no_image = "/web/admin/images/no_image.jpg";
$profile_image = array_shift($images);
$profile_image = $profile_image ? $profile_image->path : $no_image;
$country = $model->get_country($applicant->country_id);
$experiences = $model->get_experience($uid);
$languages = $model->get_languages($uid);
$models = $model->get_model_types($uid);
$participations = $model->get_participations($uid);
$skills = $model->get_skills($uid);
?>
  <div class="row">
    <div class="col-md-12">
     <?php
parse_str($_SERVER['QUERY_STRING'], $url);
if(isset($url['appl'])) { unset($url['appl']); }
$url = http_build_query($url);
    ?>
    <a href="/page.php?<?=$url?>" class="btn">�����</a>
    
      <div class="panel panel-default">
            <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8">
                            <h2><?=$m_user->first_name;?> <?=$m_user->middle_name;?> <?=$m_user->last_name;?></h2>
                            <p><strong>���: </strong> <?=$m_user->gender;?> </p>
                            <p><strong>�������:</strong> <?=$show($applicant->phone)?></p>
                            <p><strong>E-mail:</strong> <?=$show($applicant->email)?></p>
                            <p><strong>�����:</strong> <?=$country->name. ', '. $applicant->city .', '. $applicant->address?></p>
        <div class='row'>
            <?=$parser($experiences, '������');?>
            <?=$parser($languages, '�����');?>
            <?=$parser($models, '���� ����� ��:');?>
            <?=$parser($participations, '�e �������� ����:');?>
            <?=$parser($skills, '������');?>
        </div>
            <div class="row">
    <?php foreach($images as $img): ?>
                <div class="col-md-4">
            <a href="<?=$img->path;?>" data-fresco-caption="�� ���" data-fresco-group="gallery" data-fresco-group-options="overflow: true, thumbnails: 'vertical', onClick: 'close', ui: 'outside'" title="dsasda / �� ���" class="fresco" style="">
                <img src="<?=$img->path?>" alt="<?=$img->name?>">
            </a>
                    <hr>
                </div>
    <?php endforeach;?>
            </div>
                        </div><!--/col-->          
                        <div class="col-xs-12 col-sm-4 text-center">
                                <img src="<?=$profile_image?>" alt="" class="center-block img-circle img-responsive">
                                <hr>
                                <ul class="list-inline ratings text-left" title="Ratings">
                                    <li>���: <?=$applicant->gender?></li>
                                    <li>������ �� �������: <?=$applicant->year_of_birth?></li>
                                    <li>�����������: <?=$applicant->obrazovanie?></li> 
                                    <li>������������: <?=$applicant->fizika?></li>
                                    <li>�����: <?=$applicant->weight?></li>
                                    <li>��������: <?=$applicant->height?></li>
                                    <li>����: <?=$applicant->waist?></li>
                                    <li>�����: <?=$applicant->hips?></li>
                                    <li>���� �� �����: <?=$applicant->eyes?></li>
                                    <li>������� �� ������: <?=$applicant->hair_length?></li>
                                    <li>��� ����: <?=$applicant->hair_type?></li>
                                    <li>���� �� ������: <?=$applicant->hair_color?></li>
                                    <li>������ �����: <?=$applicant->clothes_size?></li>
                                    <li>������ ������: <?=$applicant->shoes_size?></li>
                                </ul>
                        </div><!--/col-->
                    </div><!--/row-->
              </div><!--/panel-body-->
          </div><!--/panel-->
    </div>
  </div>
<script>
jQuery(document).ready(function($) {
    
    ///HOME PAGE - image resizing
        function imageLoaded() {
           var w = $(this).width();
           var h = $(this).height();
           var parentW = $(this).parent().width();
           var parentH = $(this).parent().height();
           
           //console.log(w + '-' + h + '-' + parentW + '-' + parentH);
           
           //if (w >= parentW){ //always true because of CSS
               if (h > parentH){
                   $(this).css('top', -(h-parentH)/2);
               } else if (h < parentH){
                   $(this).css('height', parentH).css('width', 'auto');
                   $(this).css('left', -($(this).width()-parentW)/2);
               }
           //}
        }
        $('#topGallery img').each(function() {
            if( this.complete ) {
                imageLoaded.call( this );
            } else {
                $(this).one('load', imageLoaded);
            }
        });
});
</script>
