<?php
require_once __DIR__.'/../maksoft/inquire/BaseFormBootstrap.php';

use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;



class ActivateApplicant extends BaseFormBootstrap
{

    protected $db, $mailer, $cms;

    public function __construct($db, $post_data=array(), $files_data=array(), $cms, $email)
    {
        $this->cms = $cms;
        $this->mailer = $email;
        $this->db = $db;
        $this->status = Select::init()->add('data', array(1=>'�������', 0 => '���������'));
        $this->action = Hidden::init()->add('value', 'set_status');
        $this->submit        = Submit::init()
                              ->add("value", '������')
                              ->add("class", "btn btn-success");
        parent::__construct($post_data, $this->cms, $this->mailer, $files_data);
    }

    public function save($uid)
    {
        $this->db->set_status($uid, $this->status->value);
    }
}
