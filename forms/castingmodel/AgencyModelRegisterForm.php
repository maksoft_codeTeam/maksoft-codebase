<?php
require_once __DIR__.'/../maksoft/inquire/BaseFormBootstrap.php';

use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Password;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;



class AgencyModelRegisterForm extends BaseFormBootstrap
{
    const MODEL_MIN_AGE = 4;
    const MODEL_MAX_AGE = 60;
    const MODEL_MIN_WEIGHT = 40;
    const MODEL_MAX_WEIGHT = 130;
    const MODEL_MIN_HEIGHT = 130;
    const MODEL_MAX_HEIGHT = 210;
    const MODEL_MIN_CHEST = 60;
    const MODEL_MAX_CHEST = 150;
    const MODEL_MIN_WAIST = 50;
    const MODEL_MAX_WAIST = 150;
    const MODEL_MIN_HIPS = 50;
    const MODEL_MAX_HIPS = 150;

    private $db;

    public $img_path;

    public $title = '����������� �� ������ � ��������:';

    public $default_error = '������� ������� ������';

    public function __construct($db, $post_data=array(), $files_data=array(), $cms, $email)
    {
        $path = realpath(__DIR__.'/../../web/images/upload/'.$cms->_site['SitesID'].'/models/');
        $this->img_path = $path;
        $this->cms = $cms;
        $this->mailer = $email;
        $this->db = $db;
        $choices = array("��"=>"��", "��"=>"��");

        $data = function($from, $to) { 
            $tmp = array();
            foreach(range($from, $to) as $data) {
                $tmp[$data] = $data;
            }
            return $tmp;
        };
        $not_empty = NotEmpty::init(true)->err_msg("���� ���� �� ���� �� ���� ������!");
        $this->email = Email::init()->add("label", "���������� ����: *")->add('class', 'form-control')->add('required', True);
        $this->first_name = Text::init()
                                ->add("class", "form-control")
                                ->add("label", "���:")
                                ->add("required", true)
                                ->add_validator($not_empty);

        $this->family_name = Text::init()
                                ->add("class", "form-control")
                                ->add("label", "�������:")
                                ->add("required", true)
                                ->add_validator($not_empty);

        $this->gender = Radio::init()
                                ->add_validator($not_empty)
                                ->add("label", '���')
                                ->add("required", true)
                                ->add('value', '�����')
                                ->add("data", array("�����" => "�����" , "������" => "������")); 

        $this->password = Password::init()
            ->add("label", "������: *")
            ->add('class', 'form-control')
            ->add_validator(Jokuf\Form\Validators\MaxLength::init(14)->err_msg('������������� ������� �� �������� � 15 �������'))
            ->add_validator(Jokuf\Form\Validators\MinLength::init(4)->err_msg('����������� ������� �� �������� � 5 �������'))
            ->add('required', True);
        $this->password2 = Password::init()->add("label", "������� ��������: *")->add('class', 'form-control')->add('required', True);
        $terms_link = "<a target=\"_blank\" href=\"".TERMS_LINK."\">%s</a>";
        $terms_error = sprintf($terms_link, "������ �� ��� �������� � ������ �������, �� �� ��������� �����������");
        $question_icon = '<i class="fa fa-file-text-o" aria-hidden="true"></i>';
        $this->terms  = Checkbox::init()
          ->add("required", true)
          ->add_validator(NotEmpty::init(true)
                                    ->err_msg($terms_error)
                            )
          ->add("label", sprintf($terms_link, $question_icon . ' ' ."���� �������"))
          ->add("data", array('yes' => "�������� �������� ������� � ��� �������� � ���."));

        $this->action        = Hidden::init()
                              ->add("value", 'model_register');

        
        $this->capcha     = Recaptcha::init()
                              ->add("lang", LANG)
                              ->add("site_key", CAPTCHA_KEY)
                              ->add("secret", CAPTCHA_SECRET);
        

        $this->submit        = Submit::init()
                              ->add("value", '�����������')
                              ->add("class", "btn btn-success");
        parent::__construct($post_data, $this->cms, $this->mailer, $files_data);
    }

    public function validate_password($field)
    {
        if($field->value != $this->password2->value) {
            throw new \Exception( "���������� ������ �� ��������" );
        }

        return true;
    }

    public function validate_terms($field) 
    {
        if($field->value == 'yes') { 
            return true;
        }
        throw new \Exception("������ �� �������� ������ ������� �� �� ������ �� �� ������������");
    }

    public function save()
    {
        $o_page = new \page();
        $this->db->db->beginTransaction();
        try {
            $this->db->db->exec("use maksoft;");
            $db = $this->db->db;
            if($user = $o_page->get_user_by($db, 'username', $this->email->value)){
                throw new \Exception('���� ���������� ����� ����������');
            }
            $data = $this->clean_data();
            $values = array();
            $keys = array();
            $forbidden_fields = array(
                'capcha', 
                'foreign_languages',
                'skills', 'experience',
                'participate',
                'as_model',
                'pictures',
                'action',
                'password2',
                'terms',
                'submit'
            );
            $data[$this->password->name] = \user::hash($data[$this->password->name]);
            foreach($data as $k=>$v) {
                if(in_array($k, $forbidden_fields)) {
                    continue;
                }
                $keys[] = ":$k";
                $values[] = $v;
            } 
            $user_id = $o_page->register($db, array(
                ':FirmID' => '0',
                ':ParentUserID' => 1424,
                ':Name'=> $this->first_name->value. ' ' .$this->last_name->value,
                ':EMail'=> trim($this->email->value),
                ':Phone'=> '',
                ':username' =>  trim($this->email->value),
                ':pass' =>  \user::hash($data[$this->password->name]),
                ':InitPage' =>  19372958,
                ':InitSiteID' => 1047,
                ':AccessLevel' => 1,
            ));
            $o_page->set_access_level($db, 19372958, 1047, 1,0,0, $user_id);
            $this->db->db->exec("use castingmodel;");
            $id = $this->db->register_statist($keys, $values);

            $this->db->db->commit();
        } catch (\Exception $e) {
            $this->db->db->rollBack();
            throw new \Exception($e->getMessage());
        }
        $name = $this->first_name->value . ' ' . $this->last_name->value;
        $email = $this->email->value;
        return $this->before_save($name, $email);
    }
}
