<?php
$document_root = realpath(__DIR__.'/../../../');
require_once $document_root.'/modules/vendor/autoload.php';
require_once __DIR__.'/../CastingModel.php';
require_once __DIR__.'/../data.php';

$di_container = new \Pimple\Container();
$di_container->register(new \Maksoft\Containers\GlobalContainer());
$model = new CastingModel($di_container['db']);
$applicants = $model->get_applicants_list($random=True, $limit=10);
echo '<div class="sDelimeter"></div>';
$max_columns = $o_page->_page['show_link_cols'];
$bootstrap_column = ceil(12 / $max_columns);
echo $boostrap_column;

$i = 0;
?>
    <h3> �������� ������������: </h3>
    <div class="row">
    <br>
    <?php foreach($applicants as $applicant): ?>
        <?php
        $image = $model->get_first_image($applicant->id)->path;
        if(!$image) { 
            $image = '/web/admin/images/no_image.jpg';
        }
        ?>
            <div class="col-md-<?=$bootstrap_column;?>">
            <div class="well well-sm">
                <div class="row">
                  <div class="col-sm-6 col-md-5">
                  <img src="<?=$image?>" alt="" class="img-circle img-responsive" />
                  </div>
                  <div class="col-sm-6 col-md-7">
                  <h4><i class="glyphicon glyphicon-user"></i> <strong><?=$applicant->first_name .' '. $applicant->last_name?></strong></h4>
                  <p>N: <?=$applicant->id?></p>
                    <small>
                    <?php
                        $u_membership = $model->get_user_membership($applicant);
                        $type = $model->get_membership_type($u_membership->type_id);
                        echo str_repeat('&#9733;', $type->id);
                    ?>
                      <cite title="<?=$model->get_country($applicant->country_id)->name.', '.$applicant->city?>">
                      <i class="glyphicon glyphicon-map-marker"></i> <?=$model->get_country($applicant->country_id)->name.', '.$applicant->city?>
                      </cite>
                        - 
                    <?php if ($active($applicant)): ?> 
                    <i class="glyphicon glyphicon-star"></i>�������<br />
                    <?php else: ?>
                    <i class="glyphicon glyphicon-star"></i>���������<br />
                    <?php endif; ?>
                    </small>

                    <?php if ($_SESSION['user']->ReadLevel >= 1 and $active($applicant) and !isset($_SESSION['abstract_user'])) { ?> 
                    <i class="glyphicon glyphicon-envelope"></i> <a href="#"><?=$applicant->email;?></a><br />
                    <i class="glyphicon glyphicon-phone"></i> <a href="#"><?=$applicant->phone?></a>
                    <?php } ?>
                            <br />
                    <i class="glyphicon glyphicon-gift"></i> �������: <?=$applicant->year_of_birth == 0 ? '--' : date('Y', time()) - $applicant->year_of_birth .'�.'?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php
            if($max_columns % $i):
                $i = 0;
                echo '</div><div class="row">';
            else:
                $i++;
            endif;
        endforeach;
        ?>
    </div>
