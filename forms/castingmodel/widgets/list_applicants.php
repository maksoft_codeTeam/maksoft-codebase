<?php
require_once __DIR__.'/../base.php';
require_once __DIR__ .'/../DeleteUserProfileForm.php';
if($user->ReadLevel == 1 and !$active($applicant)) {
    echo '��������� ������';
    return;
}

$delete_form = new DeleteUserProfileForm($model, $_POST);
if($delete_form->is_valid()) {
    $delete_form->save();
}


if(isset($_GET['uid']) and is_numeric($_GET['uid']) and $_GET['uid'] > 0) {
    $uid = $_GET['uid'];
    if(isset($_GET['action']) and $_GET['action'] == 'membership') {
        include __DIR__.'/membership_detail.php';
    } else {
        include __DIR__.'/applicant_detail.php';
    }
    return;
}


$applicants = $model->get_applicants_list();
$n = $o_page->_page['n'];
$SiteID = $o_page->_page['SiteID'];
?>
<div class="container">
    <div class="row">
    <h2> ���� ������������: <?=count($applicants)?></h2>
    <table class='table table-responsive' >
        <thead>
            <th> ID </th>
            <th> ��� </th>
            <th> ������� </th>
            <th> E-mail </th>
            <th> ������� </th>
            <th> �������� </th>
        </thead>
        <tbody>
            <?php 
            foreach($applicants as $applicant) {
                if($user->ReadLevel == 1 and !$active($applicant)) {
                    continue;
                }
                $link = '/page.php?'.http_build_query(array('n' => $n, 'SiteID' => $SiteID, 'uid'=>$applicant->id));
            ?>
            <tr>
                <td> <?=$applicant->id?></td>
                <td> <?=$applicant->first_name . ' ' .$applicant->last_name?> </td>
                <td> <?=$show('<a href="tel:'.$applicant->phone.'">'.$applicant->phone.'</a>', '--');?></td>
                <td> <?=$show('<a href="mailto:'.$applicant->email.'">'.$applicant->email.'</a>', '--');?></td>
                <td> <?php echo  !$active($applicant) ? '���������' : '�������' ?></td>
                <td> 
                <?php if ($_SESSION['user']->AccessLevel > 1) { ?>
                    <a class='submit' href="<?=$link?>">�������</a>
                    <hr>
                <?php } ?>
                    <form method='get'>
                    <input type='hidden' name='n' value='<?=$o_page->_page['n']?>'>
                    <input type='hidden' name='SiteID' value='<?=$o_page->_page['SiteID']?>'>
                    <input type='hidden' name='uid' value='<?=$applicant->id?>'>
                    <input type='hidden' name='action' value='membership'>
                    <input type='submit' value='���������/���� ���������'>
                    </form>
                    <?php
                    $delete_form->userId->value = $applicant->id; 
                    echo $delete_form;
                    ?>
                    
                </td>
            </tr>
            <?php } ?>
        </tbody>
        </table>
    </div>
</div>
