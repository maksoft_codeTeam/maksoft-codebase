<?php
$document_root = realpath(__DIR__.'/../../../');
require_once $document_root.'/modules/vendor/autoload.php';
require_once __DIR__.'/../CastingModel.php';
require_once __DIR__.'/../data.php';
require_once __DIR__.'/../base.php';

$only_producers = !isset($_SESSION['abstract_user']);

$di_container = new \Pimple\Container();
$di_container->register(new \Maksoft\Containers\GlobalContainer());
$model = new CastingModel($di_container['db']);
$applicants = $model->get_applicants_list($random=false);
echo '<div class="sDelimeter"></div>';
$max_columns = $o_page->_page['show_link_cols'];
$bootstrap_column = ceil(12 / $max_columns);
echo $boostrap_column;
$i = 0;
?>
    <div class="row">
    <br>
<?php
if (isset($_GET['appl'])) {
    require_once __DIR__ . '/../profile_view.php';
    return;
} 
?>
    <?php foreach($applicants as $applicant): ?>
        <?php
        $image = $model->get_first_image($applicant->id)->path;
        if(!$image) { 
            $image = '/web/admin/images/no_image.jpg';
        }
        ?>
            <div class="col-xs-12 col-sm-6 col-md-<?=$bootstrap_column;?>">
            <div class="well well-sm">
                <div class="row">
                  <div class="col-sm-6 col-md-5">
                  <img src="<?=$image?>" alt="" class="img-circle img-responsive" />
                  </div>
                  <div class="col-sm-6 col-md-7">
                  <h4><i class="glyphicon glyphicon-user"></i> <strong><?=$applicant->first_name .' '. $applicant->last_name?></strong></h4>
                  <p>N: <?=$applicant->id?></p>
                    <small>
                      <cite title="<?=$model->get_country($applicant->country_id)->name.', '.$applicant->city?>">
                      <i class="glyphicon glyphicon-map-marker"></i> <?=$model->get_country($applicant->country_id)->name.', '.$applicant->city?>
                      </cite>
                        - 
                    <?php if ($active($applicant)): ?> 
                    <i class="glyphicon glyphicon-star"></i>�������<br />
                    <?php else: ?>
                    <i class="glyphicon glyphicon-star"></i>���������<br />
                    <?php endif; ?>
                    </small>

                    <?php if ($_SESSION['user']->ReadLevel >=1 and $active($applicant) /* and $only_producers */ ) { ?> 
                <?php if($authorized) {  ?>
                    <i class="glyphicon glyphicon-envelope"></i> <?=$show('<a href="#">'.$applicant->email.'</a>', '');?><br />
                    <i class="glyphicon glyphicon-phone"></i> <?=$show('<a href="#">'.$applicant->phone.'</a>', '');?>
                    <form method="GET">
                        <input type="hidden" name="n" value="<?=$o_page->_page['n']?>">
                        <input type="hidden" name="SiteID" value="<?=$o_page->_page['SiteID']?>">
                        <input type="hidden" name="appl" value="<?=$applicant->id?>">
                        <input type="submit" value="�������" class="btn btn-info">
                    </form>
                <?php } ?>
                    <?php } ?>
                            <br />
                    <i class="glyphicon glyphicon-gift"></i> �������: <?=$applicant->year_of_birth == 0 ? '--' : date('Y', time()) - $applicant->year_of_birth .'�.'?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php
            if($max_columns % $i):
                $i = 0;
                echo '</div><div class="row">';
            else:
                $i++;
            endif;
        endforeach;
        ?>
    </div>
