<?php
require_once __DIR__.'/../ActivateApplicant.php';
$applicant = $model->get_single_applicant($uid);

$images = $model->get_images_list($uid);
$country = $model->get_country($applicant->country_id);
$experiences = $model->get_experience($uid);
$languages = $model->get_languages($uid);
$models = $model->get_model_types($uid);
$participations = $model->get_participations($uid);
$skills = $model->get_skills($uid);


$status_form = new ActivateApplicant($model, $_POST, $_FILES, $o_page, $mail);
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST[$status_form->action->name]) and $_POST[$status_form->action->name] == $status_form->action->value) {
    try {
        $status_form->is_valid();
        $status_form->save($uid);
        Message\success('������� � �������� �������!');
        echo '<script> location="'.$_SERVER['REQUEST_URI'].'";</sciprt>';
    } catch(Exception $e) {
        Message\error('������� ������� ������, ���� ������� ������.');
    }
}

?>

<div class="container">    
      <div class="row">
        <?php foreach($images as $image) { ?>
          <div class="col-md-6 col-xs-12 col-sm-6 col-lg-4">
              <img src="<?=$image->path?>" alt="stack photo" class="img">
          </div>
        <?php  } ?>
          <div class="col-md-8 col-xs-12 col-sm-6 col-lg-8">
              <div class="container" style="border-bottom:1px solid black">
                <h2><?=$applicant->first_name .' '. $applicant->middle_name. ' ' .$applicant->last_name?></h2>
              </div>
                <hr>
              <ul class="list-group">
                <li class='list-group-item'>�������: <?=$applicant->phone?></li>
                <li class='list-group-item'>E-mail: <?=$applicant->email?></li>
                <li class='list-group-item'>�����: <?=$country->name. ', '. $applicant->city .', '. $applicant->address?></li>
                <?php if(user::$uLevel > 2) { ?>
                <li class='list-group-item'><span>������: 
                <?php 
                    $status_form->status->value = $applicant->status;
                    echo $status_form;
                ?>
                </li>
                <?php } ?>
                <li class='list-group-item'>���: <?=$applicant->gender?></li>
                <li class='list-group-item'>������ �� �������: <?=$applicant->year_of_birth?></li>
                <li class='list-group-item'>�����������: <?=$applicant->obrazovanie?></li> 
                <li class='list-group-item'>������������: <?=$applicant->fizika?></li>
                <li class='list-group-item'>�����: <?=$applicant->weight?></li>
                <li class='list-group-item'>��������: <?=$applicant->height?></li>
                <li class='list-group-item'>����: <?=$applicant->waist?></li>
                <li class='list-group-item'>�����: <?=$applicant->hips?></li>
                <li class='list-group-item'>���� �� �����: <?=$applicant->eyes?></li>
                <li class='list-group-item'>������� �� ������: <?=$applicant->hair_length?></li>
                <li class='list-group-item'>��� ����: <?=$applicant->hair_type?></li>
                <li class='list-group-item'>���� �� ������: <?=$applicant->hair_color?></li>
                <li class='list-group-item'>������ �����: <?=$applicant->clothes_size?></li>
                <li class='list-group-item'>������ ������: <?=$applicant->shoes_size?></li>
              </ul>
          </div>
        </div>
        <div class='row'>
            <?=$parser($experiences, '������');?>
            <?=$parser($languages, '�����');?>
            <?=$parser($models, '���� ����� ��:');?>
            <?=$parser($participations, '�e �������� ����:');?>
            <?=$parser($skills, '������');?>
        </div>
</div>
