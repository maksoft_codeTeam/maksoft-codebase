<?php
require_once __DIR__.'/../CreateMembershipForm.php';
require_once __DIR__.'/../EditMembershipForm.php';
$m_user = $model->get_single_applicant($uid);
$cr_form = new CreateMembershipForm($model, $m_user, $_POST);
if($_SERVER['REQUEST_METHOD'] === 'POST' and isset($_POST['action']) and $_POST['action'] == $cr_form->action->value) {
    try {
        $cr_form->is_valid();
        $cr_form->save();
        $custom_js[] = 'toastr.success("������� ����������� ��������� �� ���������� '.$m_user->first_name. ' '. $m_user->last_name.'");';
    } catch (\Exception $e) {
        $custom_js[] = 'toastr.error("'.$e->getMessage().'");';
    }
}

$membership = $model->get_user_membership($m_user);
$ed_form = new EditMembershipForm($model, $m_user, $membership, $_POST);
$membership = $model->get_user_membership($m_user);
$ed_form = new EditMembershipForm($model, $m_user, $membership, $_POST);
if($_SERVER['REQUEST_METHOD'] === 'POST' and isset($_POST['action']) and $_POST['action'] == $ed_form->action->value) {
    try {
        $ed_form->is_valid();
        $ed_form->save();
        $custom_js[] = 'toastr.success("������� �������");';
        $membership = $model->get_user_membership($m_user);
    } catch (\Exception $e) {
        $custom_js[] = 'toastr.error("'.$e->getMessage().'");';
    }
}
if($_SERVER['REQUEST_METHOD'] === 'POST' and isset($_POST['action']) and $_POST['action'] == 'delete') {
    
    try {
        $model->delete_membership($m_user);
        $custom_js[] = 'toastr.success("������� � ������ �������");';
        $membership = $model->get_user_membership($m_user);
    } catch (\Exception $e) {
        $custom_js[] = 'toastr.error("'.$e->getMessage().'");';
    }
}
$membership_type = $model->get_membership_type($membership->type_id);
$payments = $model->get_user_payments($uid);
?>
<div class="row">
<div class='col-md-12'>
<h2>���������� �� ����������: <?=$m_user->first_name?> <?=$m_user->last_name?></h2>
<hr>
<?php if($membership) { ?>
<table class='table'>
    <tr>
        <td><?=$membership_type->description?></td>
        <td><?=$membership->from_date?></td>
        <td><?=$membership->to_date?></td>
        <td>
            <?php if(strtotime($membership->to_date) < time()):?>
                �������
            <?php else:?>
                �������
            <?php endif;?>
        </td>
        <td> <input type="submit" value='����������' class="btn btn-primary" data-toggle="modal" data-target="#myModal"> </td>
        <td>
            <form method='POST'>
                <input type="hidden" name="action" value="delete">
                <input type="submit" value='������' onclick="return confirm('������� �� ���, �� ������� �� �������� ���� ���������?')">
            </form>
        </td>
    </tr>
</table>
<?php } ?>
<h2>����� ����������� �� ����������</h2>
<?=$cr_form?>
</div>
<div class='col-md-12'>
    <h3>������� �� ����������</h3>
    <table class='table  table-condensed'>
    <th>ID</th>
    <th>���</th>
    <th>����</th>
    <th>������</th>
    <th>���������</th>
    <th>����</th>
    <?php foreach($payments as $payment): ?>
        <tr class="success" style='background-color: #25A25A'>
            <td> <?=$payment->id?> </td>
            <td> <?=$model->get_membership_type($payment->membership_type)->description?> </td>
            <td> <?=$payment->price?> </td>
            <td> <?=$payment->status?> </td>
            <td>
            <?php if($payment->message == 'success' AND $payment->membership == 0) {
                echo '��������� � ����������';
            } else {
                echo $payment->message;
            } ?>
            </td>
            <td> <?=$payment->created?> </td>
        </tr>
    <?php endforeach;?>
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
            <?=$ed_form->start()?>
            <div class='form-control'>
            <label for="<?=$ed_form->type->name?>"> <?=$ed_form->type->label?> </label>
            <?=$ed_form->type?>
            </div>
            <hr>
            <div class='form-control'>
            <label for="<?=$ed_form->from_date->name?>"> <?=$ed_form->from_date->label?> </label>
            <?=$ed_form->from_date?>
            </div>
            <hr>
            <div class='form-control'>
            <label for="<?=$ed_form->to_date->name?>"> <?=$ed_form->to_date->label?> </label>
            <?=$ed_form->to_date?>
            </div>
            <?=$ed_form->action?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
            <?=$ed_form->submit ;?>
            <?=$ed_form->end()?>
      </div>
    </div>
  </div>
</div>
<?php $custom_js[] = <<<HEREDOC
var picker = new Pikaday({
    field: document.getElementById('from_date'),
    format: 'DD/MM/YYYY', 
    theme: 'triangle-theme',
    defaultDate: new Date('{$membership->from_date}'),
    toString(date, format) {
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        return day + '.' + month + '.' + year;
    },
});

var picker2 = new Pikaday({
    field: document.getElementById('to_date'),
    format: 'DD/MM/YYYY', 
    defaultDate: new Date('{$membership->to_date}'),
    theme: 'triangle-theme',
    toString(date, format) {
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        return day + '.' + month + '.' + year;
    },
});
HEREDOC;
?>
