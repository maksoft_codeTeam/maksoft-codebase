<style>
.table_title h1, .table_title p {
    text-align: center;
}
.table_title > p {
    font-size: 21px;
}
.table_title > hr {
    border-color: #e3e3e3;
    border-width: 3px;
    max-width: 3%;
    width: 100%;
}
.table_title h1, .table_title p {
    text-align: center;
}
.table_title > h1 {
    font-size: 33px !important;
}
.center {
    text-align: center;
}
.panel-success {
    border: 1px solid #ffd3b4;
}
.panel-success > .panel-heading {
    background: <?=TMPL_CSS_NAME_COLOR?> none repeat scroll 0 0;
    color: #fff;
    padding: 10px 15px;
}
.panel-body {
    padding: 15px;
}
.panel-body.text-center > p {
    margin-bottom: 0;
}
.panel-body.text-center strong {
    font-size: 24px;
}
.panel > .list-group, .panel > .panel-collapse > .list-group {
    margin-bottom: 0;
}
.list-group-item {
    font-size: 20px;
    padding-bottom: 18px;
}
.list-group + .panel-footer {
    border-top-width: 0;
}
.panel-footer {
    background-color: #f5f5f5;
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    border-top: 1px solid #ddd;
    padding: 10px 15px;
}
.btn.btn-lg.btn-block.btn-success {
    background: #f05f40 none repeat scroll 0 0;
    color: #fff !important;
}
.btn {
    border: 0 none;
    border-radius: 300px;
    font-family: "Raleway",sans-serif;
    font-weight: 700;
    text-transform: uppercase;
}
.table_title > h1 {
    font-size: 33px !important;
    padding-bottom:20px;
}


/*form*/
.order input, .order select {
    border: 2px solid #e5e5e5 !important;
}
.order input, select {
    -moz-appearance: none;
    border: 1px solid #e5e5e5;
    border-radius: 2px;
    box-sizing: border-box;
    color: #666;
    font-size: 14px;
    font-weight: 300;
    margin-bottom: 20px;
    padding: 14px 15px;
    transition: border-color 0.25s ease 0s;
    width: 100%;
}
.cn_btn {
    background: #f05f40 none repeat scroll 0 0;
    color: #fff !important;
    margin: 0 0 0 15px !important;
    max-width: 241px;
    padding: 14px 0 !important;
    width: 100%;
}
.order input:focus, .order select:focus {
    border: 2px solid #f05f40 !important;
}
.r_pay img {
    margin-top: -16px;
    max-width: 225px;
    width: 100%;
}
.r_pay > button {
    background: #f05f40 none repeat scroll 0 0;
    border: medium none;
    border-radius: 3px;
    color: #fff;
    margin: 3px 0;
    padding: 8.5px 0;
    width: 100%;
}
p{margin-bottom"10px;}
</style>

<br>
<div class="row" style="margin-top: 5%;">
<div class="col-md-12" style="margin-bottom: 2%;">
    <h1 style="text-align:center;">��������� <strong></strong></h1>
</div>
<?php $membership_types = $model->get_membership_types();
    foreach($membership_types as $type): ?>
    <div class="col-sm-3 col-md-3 col-xs-12">
        <div class="box-1 center">
            <div class="panel panel-success panel-pricing">
                <div class="panel-heading">
                    <h3><?=$type->name?></h3>
                </div>
                <div class="panel-body text-center">
                    <p><strong><?=$type->price?> BGN</strong></p>
                </div>
                <ul class="list-group text-center">
                        <strong> <?=$type->description?> </strong>
                </ul>
                <div class="panel-footer">
                    <?php 
                        $pay_form->total->value = $type->price;
                        $pay_form->package_type->value = $type->id;
                        $pay_form->descr->value = $type->description;
                        $pay_form->total->value = $type->price;
                    ?>
                    <?=$pay_form?>
                 </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
</div>
<br>
<div class="row">
    <div class="col-sm-12 col-md-12 col-xs-12">
        ��������� �� ����������� ���� <a class="epay" href="https://www.epay.bg/">ePay.bg</a> - �������� ��������� �� ������� � ������� ����� � �����������
    </div>
</div>
