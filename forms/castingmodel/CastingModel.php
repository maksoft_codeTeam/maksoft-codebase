<?php

class CastingModel
{
    private static $table_name;
    public $db;
    const SALT = 'YYLmfY6IehjZMQ';

    public function __construct($db) 
    {
        $this->db = $db;
        $this->db->exec('use castingmodel');
    }

    public function get_experience($uid)
    {
        $sql = "
            SELECT * FROM  `applicant_experience` ap
            JOIN experience_type et ON ap.exp_id = et.id
            WHERE ap.appl_id = :uid
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function insert_payment($user, $type, $status, $msg, $session, $token)
    {
        $sql = '
            INSERT INTO
                `payments`(`applicant_id`, `membership_type`, `price`, `status`, `message`, session, token) 
            VALUES (
                :user_id, :membership_type, :price, :status, :message, :session, :token
            )
        ';

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":user_id", $user->id, \PDO::PARAM_INT);
        $stmt->bindParam(":membership_type", $type->id, \PDO::PARAM_INT);
        $stmt->bindParam(":price", $type->price, \PDO::PARAM_INT);
        $stmt->bindParam(":status", $status, \PDO::PARAM_INT);
        $stmt->bindValue(":message", $msg);
        $stmt->bindValue(":session", $session);
        $stmt->bindValue(":token", $token);
        $stmt->execute();
        return $this->get_payment($this->db->lastInsertId(), $user, $session);
    }

    public function update_payment($payment, $status, $message) {
        $sql = "UPDATE payments SET status=:status, message=:message WHERE id=:id";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $payment->id);
        $stmt->bindValue(":message", $message);
        $stmt->bindParam(":status", $status, \PDO::PARAM_INT);
        $stmt->execute();
    }

    public function set_payment_succeed($id, $membership) 
    {
        $stmt = $this->db->prepare("
            UPDATE payments SET status=1, membership=:membership WHERE id=:id
        ");
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":membership", $membership->id);
        $stmt->execute();
        return $payment;
    }

    public function get_payment($id, $user, $session) {
        $sql = 'SELECT * FROM payments WHERE applicant_id=:user_id AND id=:id AND session=:session AND status=0';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":user_id", $user->id);
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":session", $session);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function get_applicant_languages($user_id)
    {
        $sql = "SELECT * 
        FROM languages l
        LEFT JOIN applicant_language ll ON l.id = ll.lang_id
        WHERE ll.appl_id = :user_id
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":user_id", $user_id,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function get_languages($uid)  {
        $sql = "SELECT * FROM `applicant_language` al
            JOIN languages  l on al.lang_id = l.id
            WHERE al.appl_id = :uid
            ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);

    }

    public function get_model_types($uid) {
        $sql = "SELECT * FROM `applicant_model` am
            JOIN model_type  m on am.model_id = m.id
            WHERE am.appl_id = :uid
            ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function get_skill_types($uid) {
        $sql = "Select * from skill_type st 
            join applicant_skills s ON s.skill_id = st.id
            WHERE s.appl_id = :uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function get_experience_types($uid) {
        $sql = "SELECT * 
            FROM experience_type st
            JOIN applicant_experience s ON s.exp_id = st.id
            WHERE s.appl_id =:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function get_appl_choicen_types($uid) {
        $sql = "Select * from model_type st 
            join applicant_model s ON s.model_id = st.id
            WHERE s.appl_id = :uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }
    
    public function get_participation_types($uid) {
        $sql = "Select * from participation_type st 
            join applicant_participate s ON s.part_id = st.id
            WHERE s.appl_id = :uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }


    public function get_participations($uid) {
        $sql  =" 
        SELECT * FROM `applicant_participate` ap
            JOIN participation_type pt on ap.part_id = pt.id
            WHERE ap.appl_id = :uid
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }
    
    public function get_skills($uid) {
        $sql = "SELECT * 
            FROM  `applicant_skills` a
            JOIN skill_type st ON a.skill_id = st.id
            WHERE a.appl_id = :uid
            ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function get_applicants_list($random=false, $limit=false)
    {
        $order = '';
        if($random) {
            $order = ' order by rand()';
        }
        if($limit and is_numeric($limit) and $limit > 1) {
            $limit = " LIMIT $limit";
        }
        $sql = "SELECT * FROM applicants $order $limit";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function set_status($uid, $status) 
    {
        $sql = 'UPDATE applicants SET status=:status where id = :uid';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->bindParam(":status", $status,\PDO::PARAM_INT);
        $stmt->execute();
    }

    public function get_single_applicant($uid) 
    {
        $sql = "SELECT a.*, pt.name as fizika, et.name as obrazovanie FROM applicants a  
                LEFT JOIN phisique_type pt on pt.id = a.physique
                LEFT JOIN education_type et on et.id = a.education
                WHERE a.id=:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function get_first_image($uid){
        $sql = "SELECT * FROM images WHERE applicant=:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function get_images_list($uid){
        $sql = "SELECT * FROM images WHERE applicant=:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function get_user_payments($uid) {
        $sql = "SELECT * FROM payments WHERE applicant_id = :uid ORDER BY created DESC";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function get_country($uid){
        $sql = "SELECT * FROM country WHERE id=:uid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":uid", $uid,\PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function field_data($table, $type=\PDO::FETCH_OBJ, $fields='*')
    {
        $sql = "SELECT $fields FROM $table ORDER BY id";
        $tables = array(
            'appicants',
            'languages',
            'experience_type',
            'education_type',
            'model_type',
            'participation_type',
            'phisique_type',
            'skill_type',
            'country'
        );
        if(!in_array($table, $tables)){
            return array();
        }

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $tmp=array();
        while($res = $stmt->fetch($type)){
            $tmp[$res->id] = $res->name;
        }
        return $tmp;
    }

    public function register_applicant($keys, $values)
    {
        $sql = "
            INSERT INTO `applicants`(
                     `first_name`,
                     `middle_name`,
                     `last_name`,
                     `gender`,
                     `year_of_birth`,
                     `education`,
                     `physique`,
                     `weight`,
                     `height`,
                     `chest_size`,
                     `waist`,
                     `hips`,
                     `eyes`,
                     `hair_length`,
                     `hair_type`,
                     `hair_color`,
                     `clothes_size`,
                     `shoes_size`,
                     country_id,
                     city,
                     address,
                     phone,
                     email
             ) VALUES (
                    :first_name,
                    :last_name,
                    :family_name,
                    :gender,
                    :year_of_birth,
                    :education,
                    :physique,
                    :weight,
                    :height,
                    :chest_size,
                    :waist,
                    :hips,
                    :eyes,
                    :hair_length,
                    :hair_type,
                    :hair_color,
                    :size_clothes,
                    :size_shoes,
                    :country_id,
                    :city,
                    :address,
                    :phone,
                    :email
                )";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array_combine($keys, $values));
        return $this->db->lastInsertId();
    }

    public function update_applicant($default)
    {
        $sql = "
            UPDATE  `applicants` SET
                    year_of_birth = :year_of_birth,
                    education = :education,
                    physique = :physique,
                    weight = :weight,
                    height = :height,
                    chest_size = :chest_size,
                    waist = :waist,
                    hips = :hips,
                    eyes = :eyes,
                    hair_length = :hair_length,
                    hair_type = :hair_type,
                    hair_color = :hair_color,
                    clothes_size = :size_clothes,
                    shoes_size = :size_shoes,
                    country_id = :country_id,
                    city = :city,
                    address = :address,
                    phone = :phone
             WHERE id = :id ";
        $stmt = $this->db->prepare($sql);
        $stmt->execute($default);
        return $this->db->lastInsertId();
    }

    public function get_countries()
    {
        $sql = "SELECT * FROM country";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }
    
    public function upload_applicant_picture($applicant_id, $image_name, $extension, $size, $path) 
    {
        $sql = "INSERT INTO `images` (
                   `applicant`, 
                   `name`, 
                   `extension`, 
                   `size`, 
                   `path`
               ) VALUES (
                   :applicant_id, 
                   :image_name, 
                   :extension, 
                   :size, 
                   :path
               ) ";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array(
            ":applicant_id" => $applicant_id,
            ":image_name" => $image_name,
            ":extension" => $extension,
            ":size" => $size,
            ":path" => $path
        ));
    }
    public function add_experience($user_id, $exp_id) {
        $table = 'applicant_experience';
        $column = 'exp_id';
        return $this->insert_m2m($table, $column, $user_id, $exp_id);
    }

    public function add_language($user_id, $lang_id) {
        $table = 'applicant_language';
        $column = 'lang_id';
        var_dump($user_id);
        return $this->insert_m2m($table, $column, $user_id, $lang_id);
    }

    public function add_model($user_id, $model_id) {
        $table = 'applicant_model';
        $column = 'model_id';
        return $this->insert_m2m($table, $column, $user_id, $model_id);
    }

    public function add_participate($user_id, $participate_id) {
        $table = 'applicant_participate';
        $column = 'part_id';
        return $this->insert_m2m($table, $column, $user_id, $participate_id);
    }

    public function add_skill($user_id, $skill_id) {
        $table = 'applicant_skills';
        $column = 'skill_id';
        return $this->insert_m2m($table, $column, $user_id, $skill_id);
    }

    public function insert_m2m($table, $column, $id1, $id2)
    {
        $sql = "INSERT IGNORE INTO $table (`appl_id`, $column) VALUES (:val1, :val2)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":val1", $id1, \PDO::PARAM_INT);
        $stmt->bindParam(":val2", $id2, \PDO::PARAM_INT);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function get_statist_by($field, $value)
    {
        $fields = array('email', 'id', 'password');
        if(!in_array($field)) {
            $field = 'email';
        }
        $sql = "SELECT * 
            FROM  `applicants` 
            WHERE  $field = :value LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":value", $value);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function register_statist($keys, $values)
    {
        $sql = "
            INSERT INTO 
            applicants 
            (email, first_name, last_name, gender, password)
            VALUES
            (:email, :first_name, :family_name, :gender, :password)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array_combine($keys, $values));
        return $this->db->lastInsertId();
    }

    public function get_membership_types()
    {
        $stmt = $this->db->prepare("SELECT * FROM  `membership_types` ");
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function get_membership_type($id) {
        $stmt = $this->db->prepare("SELECT * FROM membership_types WHERE id=:id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }



    public function get_membership($id) {
        $stmt = $this->db->prepare("SELECT * FROM membership WHERE id=:id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }
    public function get_last_membership($user, $from)
    {
        $stmt = $this->db->prepare("SELECT * FROM membership WHERE to_date >= :date and user_id=:uid");
        $stmt->bindValue(":date", $from);        
        $stmt->bindValue(":uid", $user->id);        
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    protected function membership_insert_query()
    {
        return "
           INSERT INTO `membership` 
            (`type_id`, `user_id`, `from_date`, `to_date`) 
            VALUES      
            (:type, :user, :from, :to ) ";
    }

    protected function membership_update_query()
    {
        return "
            UPDATE `membership`
            SET 
                `type_id`= :type,
                `user_id`= :user,
                `from_date`= :from,
                `to_date`= :to,
                updated_at = NOW()
            WHERE 1
        ";            
    }

    public function update_membership($type, $user, $from_date, $to_date) 
    {
        $sql = "
           INSERT INTO `membership` 
            (`type_id`, `user_id`, `from_date`, `to_date`, updated_at) 
            VALUES      
            (:type, :user, :from, :to, :updated_at ) 
            ON DUPLICATE KEY UPDATE type_id =:type, from_date = :from, to_date=:to, updated_at = :updated_at
            ";
        $from = date('Y-m-d H:i:s', strtotime($from_date));
        $to = date('Y-m-d H:i:s', strtotime($to_date));

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":type", $type->id);        
        $stmt->bindValue(":user", $user->id);        
        $stmt->bindValue(":from", $from);        
        $stmt->bindValue(":to", $to);        
        $stmt->bindValue(":updated_at", date('Y-m-d H:i:s', time()));        
        $stmt->execute();
        return $this->get_membership($this->db->lastInsertId());
    }

    public function delete_membership($user) {
        $stmt = $this->db->prepare("DELETE FROM membership where user_id=:user_id");
        $stmt->bindValue(":user_id", $user->id);
        $stmt->execute();
    }


    public function deleteProfile($uid) {
        $stmt = $this->db->prepare("DELETE FROM applicants where id = :uid");
        $stmt->bindValue(":uid", $uid);
        $stmt->execute();
    }

    public function deleteFromMaksoftProfile($id) {
        $this->db->exec('use maksoft');
        $stmt = $this->db->prepare("DELETE FROM users WHERE ID=:id");
        $stmt->bindValue(":id", $id);
        var_dump($id);
        $stmt->execute();
        $this->db->exec('use castingmodel');
    }

    /*
     * realpath — Returns canonicalized absolute pathname
     * is_writable — Tells whether the filename is writable
     * unlink — Deletes a file
     */

    public function deleteImage ($img_path) {
        $document_root = '/hosting/maksoft/maksoft';
        $path = realpath($document_root.$img_path);
        if(is_writable($path)) {
            unlink($path);
        }
    }

    public function deleteFromSiteAccess($makUID) {
        $this->db->exec('use maksoft;');
        $stmt = $this->db->prepare("DELETE FROM SiteAccess WHERE userID = :maksoft_user_id");
        $stmt->bindValue(":maksoft_user_id", $makUID);
        $stmt->execute();
        $this->db->exec('use castingmodel;');
    }

    public function getSingleMaksoftUserProfile($email) {
        $this->db->exec('use maksoft;');
        $sql = "SELECT * FROM users u  where u.username=:email";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":email", $email);
        $stmt->execute();
        $result =  $stmt->fetchAll(\PDO::FETCH_OBJ);
        if(count($results) > 1) {
            throw new \Exception("Not a single result, must be deleted manually");
        }

        $this->db->exec('use castingmodel;');
        return array_shift($result);
    }

    public function create_membership($type, $user) {
        $day_in_seconds = 86400;
        $sql = "
           INSERT INTO `membership` 
            (`type_id`, `user_id`, `from_date`, `to_date`) 
            VALUES      
            (:type, :user, :from, :to ) 
            ON DUPLICATE KEY UPDATE type_id =:type, from_date = :from, to_date=:to
            ";
        $now = time();
        $from = date('Y-m-d H:i:s', $now);
        $membership = $this->get_last_membership($user, $from);
        $to = date('Y-m-d H:i:s', strtotime($from) + $day_in_seconds * $type->days);
        if ($membership) {
            $to = date('Y-m-d H:i:s', strtotime($membership->to_date) + $day_in_seconds * $type->days);
        }

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":type", $type->id);        
        $stmt->bindValue(":user", $user->id);        
        $stmt->bindValue(":from", $from);        
        $stmt->bindValue(":to", $to);        
        $stmt->execute();
        return $this->get_membership($this->db->lastInsertId());
    }

    public function get_user_membership($user) {
        $stmt = $this->db->prepare("SELECT * FROM  `membership` where user_id = :user ");
        $stmt->bindValue(":user", $user->id);        
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }
}
