<?php
require_once __DIR__.'/base.php';

$actions = array('thanks', 'cancel');
$types = array();
global $custom_js;



foreach($model->get_membership_types() as $type) {
    $types[$type->id] = $type; 
}


if(!isset($_GET['action']) or !in_array($_GET['action'], $actions)) {
    return;
}

if(!isset($_GET['type']) and !array_key_exists($_GET['type'], $types)) {
    $custom_js[] = 'toastr.error("type error");';
    return;
}
if(!isset($_GET['uid'])) {
        $custom_js[] = 'toastr.error("uid error");';
    return;
}


$type = $model->get_membership_type($_GET['type']);

switch($_GET['action']) {
    case 'thanks':
        $m_user = isset($_SESSION['abstract_user']) ? $_SESSION['abstract_user'] : $model->get_single_applicant($_GET['uid']);
        $payment = $model->get_payment($_GET['pid'], $m_user, session_id());
        if (!$payment) {
            $custom_js[] = 'toastr.error("payment error");';
            return;
        }
        if($payment->token != $_SESSION['token']) {
            $custom_js[] = 'toastr.error("��������� �����");';
            $model->update_payment($payment, false, 'Token compare invalid');
            return;
        }
        if(!$m_user) {
            $model->update_payment($payment, false, '�� � ������� $m_user');
            $custom_js[] = 'toastr.error("$m_user �� � ���������");';
            return;
        }

        $membership = $model->create_membership($type, $m_user);
        if(!$membership) {
            $model->update_payment($payment, false, '��������� ���� �� ��������� �� ����� � membership');
            $custom_js[] = 'toastr.error("��������� ���� �� ��������� �� ����� � membership");';
            return;
        }
        $payment = $model->set_payment_succeed($payment->id, $membership);
        $custom_js[] = 'toastr.success("������� �������");';
        break;
    default:
        $custom_js[] = 'toastr.error("��������� �������");';
        break;
}

