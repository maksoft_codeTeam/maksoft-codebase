<?php 
session_start();
require_once __DIR__.'/base.php';
require_once __DIR__.'/../../modules/vendor/autoload.php';
require_once __DIR__.'/../../lib/messages.php';
require_once __DIR__.'/AgencyModelRegisterForm.php';
    $f = new AgencyModelRegisterForm($model, $_POST, $_FILES, $o_page, $email);
    $f->add_attr('enctype', "multipart/form-data");
    $f->add_attr('id', 'registrationForm');
    $f->form_class("form-horizontal");
    echo '<h2>'.$f->title.'</h2>';
    $msg = '';
    if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST[$f->action->name]) and $_POST[$f->action->name] == $f->action->value) {
        try {
            $f->is_valid();
            $f->save();
            $f = '<div class="alert alert-success" role="alert">
                  </div>';
            $f = '<div class="alert dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                  ������������� � �������, ���������� ��! ������ � ������� �� �� <a href="/page.php?n=19372946&SiteID=1047" class="alert-link">���</a>
                </div>';
        } catch(Exception $e) {
            $msg = '<div class="alert dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">x</span>
                                                                            </button>'.$e->getMessage().'</div>';
        }
    }
echo $msg;
echo $f;


?>

<!-- Terms and conditions modal -->
<div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="Terms and conditions" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="overflow-y: scroll; max-height:85%;  margin-top: 50px; margin-bottom:50px;">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><?=$o_page->print_pTitle(19372614)?></h3>
            </div>

            <div class="modal-body">
                <?=$o_page->print_pText(19372614);?>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="agreeButton" data-dismiss="modal">�������</button>
                <button type="button" class="btn btn-default" id="disagreeButton" data-dismiss="modal">��������</button>
            </div>
        </div>
    </div>
</div>

<script src= "https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"> </script>

<script>
jQuery(document).ready(function(){
jQuery('#agreeButton, #disagreeButton').on('click', function() {
    var whichButton = jQuery(this).attr('id');

    jQuery('#registrationForm')
        .find('[name="terms"]')
            .val(whichButton === 'agreeButton' ? 'yes' : 'no')
            .end();
        // Revalidate the field manually
});
});
</script>
