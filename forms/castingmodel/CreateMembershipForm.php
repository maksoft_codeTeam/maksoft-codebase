<?php

use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;



class CreateMembershipForm extends Bootstrap
{

    protected $db, $mailer, $cms;

    public function __construct($db, $user, $post_data=array())
    {
        $this->db = $db;
        $this->user = $user;
        $types_select = array();

        foreach($this->db->get_membership_types() as $type) {
            $types_select[$type->id] = $type->description;
        }

        $this->type = Select::init()->add('data', $types_select);
        $this->action = Hidden::init()->add('value', 'create_membership');
        $this->submit        = Submit::init()
                              ->add("value", '������')
                              ->add("class", "btn btn-success");

        parent::__construct($post_data);
    }

    public function save($uid)
    {
        $type = $this->db->get_membership_type($this->type->value);
        if(!$type) {
            throw new \Exception('������� ��� ������������� ����������� ����');
        }

        $this->db->create_membership($type, $this->user);
    }
}
