<?php

use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Radio;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;



class EditMembershipForm extends Bootstrap
{

    protected $db, $mailer, $cms;

    public function __construct($db, $user,$membership,  $post_data=array())
    {
        $this->db = $db;
        $this->user = $user;
        $types_select = array();

        foreach($this->db->get_membership_types() as $type) {
            $types_select[$type->id] = $type->description;
        }

        $this->type          = Select::init()->add('data', $types_select)->add('label', '��� ���������')->add('value', $membership->type_id);
        $this->from_date     = Text::init()->add('id', 'from_date')->add('label', '�� ����')->add('value', $membership->from_date);
        $this->to_date       = Text::init()->add('id', 'to_date')->add('label', '�� ����')->add('value', $membership->to_date);
        $this->action        = Hidden::init()->add('value', 'edit_membership');
        $this->submit        = Submit::init()->add("value", '������')
                                             ->add("class", "btn btn-success");

        parent::__construct($post_data);
    }

    public function save()
    {
        $type = $this->db->get_membership_type($this->type->value);
        if(!$type) {
            throw new \Exception('������� ��� ������������� ����������� ����');
        }

        $this->db->update_membership($type, $this->user, $this->from_date->value, $this->to_date->value);
    }
}
