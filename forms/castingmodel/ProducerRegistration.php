<?php
require_once __DIR__.'/../maksoft/inquire/BaseFormBootstrap.php';

use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Password;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;



class ProducerRegistration extends BaseFormBootstrap
{

    protected $db, $mailer, $cms;

    public $title = '����������� �� ����������:';

    public $default_error = '������� ������� ������';

    public function __construct($db, $post_data=array(), $files_data=array(), $cms, $email)
    {
        $this->cms = $cms;
        $this->mailer = $email;
        $this->db = $db;


        $this->first_name = Text::init()
                                ->add("class", "form-control")
                                ->add("label", "���:")
                                ->add("required", true)
                                ->add_validator(NotEmpty::init(true)->err_msg("������������ ����"));

        $this->last_name = Text::init()
                                ->add("class", "form-control")
                                ->add("label", "������:")
                                ->add("required", true)
                                ->add_validator(NotEmpty::init(true)->err_msg("������������ ����"));

        $this->email = Email::init()
                                ->add("label", "���������� ����:")
                                ->add('class', 'form-control')
                                ->add_validator(NotEmpty::init(true)->err_msg("������������ ����"));

        $this->phone = Phone::init()
            ->add("label", "�������: *")
            ->add('class', 'form-control')
            ->add_validator(NotEmpty::init(true)->err_msg("������������ ����"));
        $this->password = Password::init()
            ->add("label", "������: *")
            ->add('class', 'form-control')
            ->add_validator(NotEmpty::init(true)->err_msg("������������ ����"));
        $this->password2 = Password::init()
            ->add("label", "������� ��������: *")
            ->add('class', 'form-control')
            ->add_validator(NotEmpty::init(true)->err_msg("������������ ����"));

        $this->action = Hidden::init()->add('value', 'register_producer');

        $terms_link = "<a target=\"_blank\" href=\"".TERMS_LINK."\">%s</a>";
        $terms_error = sprintf($terms_link, "������ �� ��� �������� � ������ �������, �� �� ��������� �����������");
        $question_icon = '<i class="fa fa-file-text-o" aria-hidden="true"></i>';
        $this->terms  = Checkbox::init()
          ->add("required", true)
          ->add_validator(NotEmpty::init(true)
                                    ->err_msg($terms_error)
                            )
          ->add("label", sprintf($terms_link, $question_icon . ' ' ."���� �������"))
          ->add("data", array(true=>"�������� �������� ������� � ��� �������� � ���."));

        $this->capcha     = Recaptcha::init()
                              ->add("lang", LANG)
                              ->add("site_key", CAPTCHA_KEY)
                              ->add("secret", CAPTCHA_SECRET);

        $this->submit        = Submit::init()
                              ->add("value", '������')
                              ->add("class", "btn btn-success");
        parent::__construct($post_data, $this->cms, $this->mailer, $files_data);
    }

    public function validate_password($f) {
        return $f->value == $this->password2->value;
    }

    public function save($uid)
    {
        $name = $this->first_name->value . ' ' . $this->last_name->value;
        $email = $this->email->value;
        return $this->before_save($name, $email);
    }
}

