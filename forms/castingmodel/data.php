<?php 

class Translation {
    public static $form = array(
        "registration" => array(
            "footer" => array(
                "bg" => "*Задължително поле",
                "en" => "*Obligatory field",
                "ru" => "*Обязательное поле",
            ),
            "title" => array(
                "bg" => array(
                    "За регистрация за разширен достъп - Инфо Бюлетин и Пазарна платформа, Маркетинг платформа",
                    "За да получите разширен достъп, моля попълнете формата за контакт, за да Ви изпратим парола. Ще Ви бъде отговорено в рамките на 24 часа."
                ),
                "en" => array(
                    "Registration for extended access – Newsletter and Market platform, Marketing desk",
                    "For an extended access please fill in the contact form so we can send you a mail containing your password. You will be contacted within 24 hours."
                ),
                "ru" => array(
                    "Регистрация для расширенного доступа - Инфо Бюллетень и Рыночная платформа, Маркетинг платформа",
                    "Для расширенного доступа, просим заполнить контактную форму и мы предоставим Вам пароль. Ответ мейлом последует в течении сутки."
                ),
            ),
            'email_text' => array(
                'bg' => array(
                    "title" => "Успешна регистрация",
                    "content" => "Това е потвърждение, че Вашето запитване с мейл за разширен достъп чрез регистрация е получено и ще бъде обработено възможно най-скоро.
                    Моля проверете за входящо съобщение, с което Ви предоставяме парола за достъп."
                ),
                'en' => array(
                    "title" => "Succesfull registration",
                    "content" => "This is a confirmation that your mail request for extended access through <strong>registration</strong> has been received and will be processed at earliest convenience.
                Please check for an incoming mail providing you with an <strong>access password.</strong>"
                ),
                'ru' => array(
                    "title" => "Успешной регистрации",
                    "content" => "Настоящее подтверждение о том, что Ваш запрос мейлом на расширенный доступ путем регистрации получен и будет обработан в ближайшем сроке.
                    Просим проследить за входящим сообщением, которым предоставляем Вам пароль к доступу."
                ),
            ),
            "name" => array(
                "bg" => array("label" => "Име:*"),
                "en" => array("label" => "Name:*"),
                "ru" => array("label" => "Имя:*"),
            ),
            "company" => array(
                "bg" => array("label" => "Фирма/Организация:"),
                "en" => array("label" => "Company/Organization:"),
                "ru" => array("label" => "Компания/Организация:"),
            ),
            "terms" => array(
                "bg" => array(
                    "label" => "Общи условия:",
                    "data"  => array("Приемам общите условия"),
                    "error" => "Трябва да сте съгласни с общите условия, за да изпратите запитването" 
                ),
                "en" => array(
                    "label" => "Terms of use:",
                    "data" => array("I accept website terms of use"),
                    "error" => "You must have accepted the general terms and conditions to submit this inquiry"
                ),
                "ru" => array(
                    "label" => "Условия пользования интернет сайтом:", 
                    "data" => array("Я принимаю условия пользования интернет сайтом"),
                    "error" => "Вы должны быть получены общие условия для отправки запроса"
                ),
            ),
            "branch" => array(
                "bg" => array(
                    "label"=>"Бранш/Дейност:*",
                    "data" => array(
                        "Минно-добивен",
                        "Енергетика",
                        "Химически",
                        "Строителство",
                        "Метали",
                        "Каучук",
                        "Машини и оборудване",
                        "Моторни превозни средства",
                        "Гуми",
                        "Индустриални масла",
                        "Селско стопанство",
                        "Храни",
                        "Текстилен",
                        "Търговия",
                        "Логистика и превози",
                        "Образование",
                        "Здравеопазване",
                        "Държавна администрация",
                        "Услуги",
                        "Други",
                    )
                ),
                "en" => array(
                    "label"=>"Industry/Activity:",
                    "data" => array(
                        "Mining",
                        "Energy",
                        "Chemical",
                        "Construction",
                        "Metals",
                        "Rubber",
                        "Industrial equipment",
                        "Motor vehicles",
                        "Tyres",
                        "Lubricants",
                        "Agriculture",
                        "Food",
                        "Textile",
                        "Trading",
                        "Logistics and Freight",
                        "Education",
                        "Health services",
                        "Public administration",
                        "Services",
                        "Others",
                    )
                ),
                "ru" => array(
                    "label"=>"Отрасль/Деятельность:*",
                    "data"=> array(
                        "Горнодобывающая",
                        "Енергетика",
                        "Химическая",
                        "Строительство",
                        "Металлы",
                        "Резина",
                        "Машины и оборудование",
                        "Автомобили",
                        "Шины",
                        "Индустриальные масла",
                        "Сельское хозяйство",
                        "Пищевая",
                        "Текстильная",
                        "Торговля",
                        "Логистика и перевозки",
                        "Образование",
                        "Здравеохранение",
                        "Публичная администрация",
                        "Услуги",
                        "Другая",
                    )
                ),
            ),
            "email" => array(
                "bg" => array("label" => "Мейл*"),
                "en" => array("label" => "E-mail:*"),
                "ru" => array("label" => "Электронная почта:*"),
            ),
            "country" => array(
                "bg" => array("label" => "Страна:"),
                "en" => array("label" => "Country:"),
                "ru" => array("label" => "Страна:"),
            ),
            "phone" => array(
                "bg" => array("label" => "Телефон:"),
                "en" => array("label" => "Phone number:"),
                "ru" => array("label" => "Телефон:"),
            ),
            "access" => array(
                "bg" => array(
                    "label" => "Желая разширен достъп до:*", 
                    "data" => array("Да", "Не")
                ),
                "en" => array(
                    "label" => "Extended access to:*",
                    "data" => array("Yes", "No")
                ),
                "ru" => array(
                    "label" => "Желаю расширенного доступа к:*",
                    "data" => array("Да", "Нет")
                ),
            ),
            "bulletin" => array(
                "bg" => array(
                    "label"=>"Бюлетин:", 
                    "data" => array(
                        "Стомана",
                        "Каучук",
                        "Селско стопанство - суровини",
                        "Минно оборудване - компоненти, рез.части",
                        "Масла и смазочни материали",
                        "Гуми",
                    )
                ),
                "en" => array(
                    "label"=> "Newsletter:",
                    "data" => array(
                        "Steel",
                        "Rubber",
                        "Agriculture - raw materials",
                        "Mining equipment - components, spares",
                        "Lubricants",
                        "Tyres",
                    )
                ),
                "ru" => array(
                    "label"=>"Бюллетень:",
                    "data" => array(
                        "Сталь",
                        "Резина",
                        "Сельское хозяйство - сырье",
                        "Горное оборудование - комплектующие, запчасти",
                        "Масла и Смазочные материалы",
                        "Шины",
                    )
                ),
            ),
            "trade_platform" => array(
                "bg" => array(
                    "label" => "Пазарна платформа:",
                    "data" => array(
                        "Предлагани стоки",
                        "Търсени стоки"
                    )
                ),
                "en" => array(
                    "label" => "Market platform:",
                    "data" => array(
                        "Goods offered",
                        "Goods demanded",
                    )
                ),
                "ru" => array(
                    "label" => "Рыночная платформа:", 
                    "data" => array(
                        "Предлагаемые товары",
                        "Запрашиваемые товары",
                    )
                ),
            ),
            "marketing_platform" => array(
                "bg" => array(
                    "label" => "Маркетинг платформа:",
                    "data" => array(
                        "Изложби, панаири, бизнес форуми, интервюта",
                        "Реклама"
                    )
                ),
                "en" => array(
                    "label" => "Marketing desk:",
                    "data" => array(
                        "Exhibitions, fairs, business forums, interviews",
                        "Advertisment",
                    )
                ),
                "ru" => array(
                    "label" => "Маркетинг платформа:", 
                    "data" => array(
                        "Выставки, ярмарки, бизнес форумы, интервью",
                        "Реклама",
                    )
                ),
            ),
            "details" => array(
                "bg" => array("label"=>"Съобщение:"),
                "en" => array("label"=>"Message:"),
                "ru" => array("label"=>"Сообщение:"),
            ),
            "submit" => array(
                "bg" => array("label"=>"Изпрати"),
                "en" => array("label"=>"Send"),
                "ru" => array("label"=>"Отправить"),
            ),
        ),
        "subscription" => array(
            "submit" => array(
                "bg" => array("label"=>"Изпрати"),
                "en" => array("label"=>"Send"),
                "ru" => array("label"=>"Отправить"),
            ),
            "footer" => array(
                "bg" => "*Задължително поле",
                "en" => "*Obligatory field",
                "ru" => "*Обязательное поле",
            ),

            "terms" => array(
                "bg" => array(
                    "label" => "Общи условия:",
                    "data"  => array("Приемам общите условия"),
                    "error" => "Трябва да сте съгласни с общите условия, за да изпратите запитването" 
                ),
                "en" => array(
                    "label" => "Terms of use:",
                    "data" => array("I accept website terms of use"),
                    "error" => "You must have accepted the general terms and conditions to submit this inquiry"
                ),
                "ru" => array(
                    "label" => "Условия пользования интернет сайтом:", 
                    "data" => array("Я принимаю условия пользования интернет сайтом"),
                    "error" => "Вы должны быть получены общие условия для отправки запроса"
                ),
            ),
            "title" => array(
                "bg" => array(
                    "За регистрация за пълен достъп чрез абонамент",
                    "За да получите пълен достъп, моля попълнете формата за контакт, за да Ви отговорим с предложение според Вашите конкретни предпочитания. Ще Ви бъде отговорено в рамките на 24 часа."
                ),
                "en" => array(
                    "Registration for extended access – Newsletter and Market platform, Marketing desk",
                    "For an extended access please fill in the contact form so we can send you a mail containing your password. You will be contacted within 24 hours."
                ),
                "ru" => array(
                    "Регистрация для полного доступа через подписку",
                    "Для полного доступа, просим заполнить контактную форму и мы ответим Вам предложением согласно Вашим конкретним предпочитаниям. Ответ мейлом последует в течении сутки."
                ),
            ),
            'email_text' => array(
                'bg' => array(
                    "title" => "Успешна регистрация за абонамент",
                    "content" => "Това е потвърждение, че Вашето запитване с мейл за пълен достъп чрез <strong>абонамент</strong> е получено, ще бъде обработено и възможно най-скоро ще Ви предоставим условията за абонамент.
                    Моля проверете за входящо съобщение, с което Ви предоставяме <strong>парола за достъп.</strong>"
                ),
                'en' => array(
                    "title" => "Successfull registration for subscription access",
                    "content" => "This is a confirmation that your mail request for full access through <strong>subscription<strong> has been received, will be processed and at earliest convenience you will be informed of the subscription terms. Please check for an incoming mail providing you with an <strong>access password.</strong>"
                ),
                'ru' => array(
                    "title" => "Успешной регистрации для абонамента",
                    "content" => "Настоящее подтверждение о том, что Ваш запрос мейлом на полный доступ путем абонемента получен, будет обработан и в ближайшем сроке Вам будут предоставлены условия для абонемента. Просим проследить за входящим сообщением, которым предоставляем Вам <strong>пароль к доступу.</strong>"
                ),
            ),
            "name" => array(
                "bg" => array("label" => "Име:*"),
                "en" => array("label" => "Name:*"),
                "ru" => array("label" => "Имя:*"),
            ),
            "company" => array(
                "bg" => array("label" => "Фирма/Организация:"),
                "en" => array("label" => "Company/Organization:"),
                "ru" => array("label" => "Компания/Организация:"),
            ),
            "branch" => array(
                "bg" => array(
                    "label"=>"Бранш/Дейност:*",
                    "data" => array(
                        "Минно-добивен",
                        "Енергетика",
                        "Химически",
                        "Строителство",
                        "Метали",
                        "Каучук",
                        "Машини и оборудване",
                        "Моторни превозни средства",
                        "Гуми",
                        "Индустриални масла",
                        "Селско стопанство",
                        "Храни",
                        "Текстилен",
                        "Търговия",
                        "Логистика и превози",
                        "Образование",
                        "Здравеопазване",
                        "Държавна администрация",
                        "Услуги",
                        "Други",
                    )
                ),
                "en" => array(
                    "label"=>"Industry/Activity:",
                    "data" => array(
                        "Mining",
                        "Energy",
                        "Chemical",
                        "Construction",
                        "Metals",
                        "Rubber",
                        "Industrial equipment",
                        "Motor vehicles",
                        "Tyres",
                        "Lubricants",
                        "Agriculture",
                        "Food",
                        "Textile",
                        "Trading",
                        "Logistics and Freight",
                        "Education",
                        "Health services",
                        "Public administration",
                        "Services",
                        "Others",
                    )
                ),
                "ru" => array(
                    "label"=>"Отрасль/Деятельность:*",
                    "data"=> array(
                        "Горнодобывающая",
                        "Енергетика",
                        "Химическая",
                        "Строительство",
                        "Металлы",
                        "Резина",
                        "Машины и оборудование",
                        "Автомобили",
                        "Шины",
                        "Индустриальные масла",
                        "Сельское хозяйство",
                        "Пищевая",
                        "Текстильная",
                        "Торговля",
                        "Логистика и перевозки",
                        "Образование",
                        "Здравеохранение",
                        "Публичная администрация",
                        "Услуги",
                        "Другая",
                    )
                ),
            ),
            "email" => array(
                "bg" => array("label" => "Мейл*"),
                "en" => array("label" => "E-mail:*"),
                "ru" => array("label" => "Электронная почта:*"),
            ),
            "country" => array(
                "bg" => array("label" => "Страна:"),
                "en" => array("label" => "Country:"),
                "ru" => array("label" => "Страна:"),
            ),
            "phone" => array(
                "bg" => array("label" => "Телефон:"),
                "en" => array("label" => "Phone number:"),
                "ru" => array("label" => "Телефон:"),
            ),
            "access" => array(
                "bg" => array(
                    "label" => "Желая разширен достъп до:*", 
                    "data" => array("Да", "Не")
                ),
                "en" => array(
                    "label" => "Extended access to:*",
                    "data" => array("Yes", "No")
                ),
                "ru" => array(
                    "label" => "Желаю расширенного доступа к:*",
                    "data" => array("Да", "Нет")
                ),
            ),
            "bulletin" => array(
                "bg" => array(
                    "label"=>"Бюлетин:", 
                    "data" => array(
                        "Стомана",
                        "Каучук",
                        "Селско стопанство - суровини",
                        "Минно оборудване - компоненти, рез.части",
                        "Масла и смазочни материали",
                        "Гуми",
                    )
                ),
                "en" => array(
                    "label"=> "Newsletter:",
                    "data" => array(
                        "Steel",
                        "Rubber",
                        "Agriculture - raw materials",
                        "Mining equipment - components, spares",
                        "Lubricants",
                        "Tyres",
                    )
                ),
                "ru" => array(
                    "label"=>"Бюллетень:",
                    "data" => array(
                        "Сталь",
                        "Резина",
                        "Сельское хозяйство - сырье",
                        "Горное оборудование - комплектующие, запчасти",
                        "Масла и Смазочные материалы",
                        "Шины",
                    )
                ),
            ),
            "trade_platform" => array(
                "bg" => array(
                    "label" => "Пазарна платформа:",
                    "data" => array(
                        "Предлагани стоки",
                        "Търсени стоки"
                    )
                ),
                "en" => array(
                    "label" => "Market platform:",
                    "data" => array(
                        "Goods offered",
                        "Goods demanded",
                    )
                ),
                "ru" => array(
                    "label" => "Рыночная платформа:", 
                    "data" => array(
                        "Предлагаемые товары",
                        "Запрашиваемые товары",
                    )
                ),
            ),
            "marketing_platform" => array(
                "bg" => array(
                    "label" => "Маркетинг платформа:",
                    "data" => array(
                        "Изложби, панаири, бизнес форуми, интервюта",
                        "Реклама"
                    )
                ),
                "en" => array(
                    "label" => "Marketing desk:",
                    "data" => array(
                        "Exhibitions, fairs, business forums, interviews",
                        "Advertisment",
                    )
                ),
                "ru" => array(
                    "label" => "Маркетинг платформа:", 
                    "data" => array(
                        "Выставки, ярмарки, бизнес форумы, интервью",
                        "Реклама",
                    )
                ),
            ),
            "details" => array(
                "bg" => array("label"=>"Съобщение:"),
                "en" => array("label"=>"Message:"),
                "ru" => array("label"=>"Сообщение:"),
            ),
        ),
    );
}
