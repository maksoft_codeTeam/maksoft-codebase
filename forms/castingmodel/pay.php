<?php
session_start();


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

require_once __DIR__.'/base.php';
require_once __DIR__ .'/../../modules/bootstrap.php';
require_once __DIR__.'/EpayForm.php';

$request = Request::createFromGlobals();

$response = new JsonResponse();
$response->headers->set('Access-Control-Allow-Origin', '*');
$response->headers->set('Access-Control-Allow-Methods', 'GET,POST,PUT');
$response->headers->set('Access-Control-Allow-Credentials', 'true');
$response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');


$actions = array('thanks', 'cancel');
$types = array();

if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
    $response->prepare($request);
    $response->send();
    return;
}

/*
 * Проверявам дали всички пост полета са налични и валидни
 */
try {
    $form = new EpayForm($_POST);
    $form->is_valid();
    define('URL_OK' , 'https://castingmodel.eu/?action=thanks&type='.$form->type->value.'&uid='.$_SESSION['abstract_user']->id);
    define('URL_CANCEL' , 'https://castingmodel.eu/?action=cancel&type='.$form->type->value.'&uid='.$_SESSION['abstract_user']->id);
} catch (\Exception $e) {
    $errors = array();
    foreach($form as $field){
        if(!$field->getErrors()){
            continue;
        }
        $errors[$field->name] = $field->getErrors();
    }
    $response->setContent(json_encode($errors));
    $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
    $response->prepare($request);
    $response->send();
    return;
}


