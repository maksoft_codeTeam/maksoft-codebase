<?php
require_once __DIR__.'/../maksoft/inquire/BaseFormBootstrap.php';

use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;



class AgencyModelProfileEdit extends Bootstrap
{
    const MODEL_MIN_AGE = 4;
    const MODEL_MAX_AGE = 60;
    const MODEL_MIN_WEIGHT = 40;
    const MODEL_MAX_WEIGHT = 130;
    const MODEL_MIN_HEIGHT = 130;
    const MODEL_MAX_HEIGHT = 210;
    const MODEL_MIN_CHEST = 60;
    const MODEL_MAX_CHEST = 150;
    const MODEL_MIN_WAIST = 50;
    const MODEL_MAX_WAIST = 150;
    const MODEL_MIN_HIPS = 50;
    const MODEL_MAX_HIPS = 150;

    protected $db;

    public $img_path;

    public $default_error = '������� ������� ������';

    public function __construct($db, $post_data=array(), $files_data=array(), $cms, $email, $user)
    {
        $path = realpath(__DIR__.'/../../web/images/upload/'.$cms->_site['SitesID'].'/models/');
        $this->img_path = $path;
        $this->cms = $cms;
        $this->mailer = $email;
        $this->db = $db;
        $choices = array("��"=>"��", "��"=>"��");

        $data = function($from, $to) { 
            $tmp = array();
            foreach(range($from, $to) as $data) {
                $tmp[$data] = $data;
            }
            return $tmp;
        };
        $default_year = date("Y", time()) - 21;

        $default_value = function($val1, $val2) {
            return $val1 > 0 ? $val1 : $val2;
        };
        $this->year_of_birth = Select::init()
                                ->add("class", "form-control")
                                ->add("label", "������ �� �������")
                                ->add("value", $user->year_of_birth > 0 ? $user->year_of_birth : $default_year)
                                ->add("data", $data(date("Y", time()) - self::MODEL_MAX_AGE, date("Y", time()) - self::MODEL_MIN_AGE));

        $this->education = Select::init()
                                ->add("class", "form-check")
                                ->add("label", "�����������:")
                                ->add('value', $default_value($user->education, 6))
                                ->add('data', $db->field_data('education_type'));

        $this->foreign_languages = Checkbox::init()
                                ->add("class", "form-check")
                                ->add('name', 'foreign_languages[]')
                                ->add("label", "����� �����:")
                                ->add("value", "����������")
                                ->add("data", $db->field_data('languages'));
        $physique = $db->field_data('phisique_type');

        $this->physique = Select::init()
                                ->add("class", "form-check")
                                ->add("label", "�����������e")
                                ->add("value", 3)
                                ->add("data", $physique);
        $this->weight = Select::init()
                                ->add("class", "form-control")
                                ->add("label", "�����:")
                                ->add("value", $default_value($user->weight, self::MODEL_MIN_WEIGHT))
                                ->add("data", $data(self::MODEL_MIN_WEIGHT, self::MODEL_MAX_WEIGHT)); 
        $this->height = Select::init()
                                ->add("class", "form-control")
                                ->add("label", "��������:")
                                ->add("value", $default_value($user->height, self::MODEL_MIN_HEIGHT))
                                ->add("data", $data(self::MODEL_MIN_HEIGHT, self::MODEL_MAX_HEIGHT)); 

        $this->chest_size = Integer::init()
                                ->add("class", "form-control")
                                ->add("label", "������ ��������:")
                                ->add("value", $default_value($user->chest_size, self::MODEL_MIN_CHEST));
        $this->waist = Integer::init()
                                ->add("class", "form-control")
                                ->add("label", "�����:")
                                ->add("value", $default_value($user->waist, self::MODEL_MIN_WAIST));
        $this->hips = Integer::init()
                                ->add("class", "form-control")
                                ->add("label", "����:")
                                ->add('value', $default_value($user->hips, self::MODEL_MIN_HIPS));

        $this->eyes = Select::init()
                                ->add("label", "���:")
                                ->add("value", $user->eyes ? $user->eyes : "�����")
                                ->add("data", array(
                                        "�����" => "�����",
                                        "������" => "������",
                                        "������" => "������",
                                        "����" => "����",
                                        "������" => "������",
                                    )
                                );
        $this->hair_length = Select::init()
                                ->add("label", "������� �� ����:")
                                ->add("value", "����")
                                ->add("data", array(
                                    "����� ����"  => "����� ����",
                                    "����"        => "����",
                                    "������ �����"=> "������ �����",
                                    "�����"       => "�����",
                                    "����� �����" => "����� �����",
                                ));
        $this->hair_type = Select::init()
                                ->add("value", "�����")
                                ->add("label", "��� ����:")
                                ->add("data", array(
                                    "�����"    => "�����",
                                    "��������" => "��������",
                                    "�������"  => "�������"
                                ));
        $this->hair_color = Select::init()
                                ->add("class", "form-control")
                                ->add("label", "���� �� ����:")
                                ->add("value", $user->hair_color)
                                ->add("data", array(
                                    "�����"            => "�����",
                                    "����"             => "����",
                                    "��������"         => "��������",
                                    "����"             => "����",
                                    "�������"          => "�������",
                                    "������"           => "������",
                                    "�������"          => "�������",
                                    "������ ���������" => "������ ���������",
                                    "����� ���������"  => "����� ���������",
                                    "����"             => "����",
                                ));

        $this->size_clothes = Select::init()
                                ->add("value", $user->clothes_size ? $user->clothes_size : "S")
                                ->add("label", "������ �����")
                                ->add("data", array(
                                    "XS"  => "XS",
                                    "S"   => "S",
                                    "M"   => "M",
                                    "L"   => "L",
                                    "XL"  => "XL",
                                    "XXL" => "XXL"
                                ));
        $this->size_shoes = Select::init()
                                ->add("class", "form-control")
                                ->add("value", $default_value($user->shoes_size, 40))
                                ->add("label", "������ ������")
                                ->add("data", $data(25, 48));
        $this->skills = Checkbox::init()
                                ->add('name', 'skills[]')
                                ->add("label", '������:')
                                ->add("data", $db->field_data('skill_type'));
        $this->experience = Checkbox::init()
                                ->add('name', 'experience[]')
                                ->add("label", '����:')
                                ->add("data", $db->field_data('experience_type'));

        $this->participate = Checkbox::init()
                                ->add('name', 'participate[]')
                                ->add("label", '�������:')
                                ->add("data", $db->field_data('participation_type'));

        $this->as_model = Checkbox::init()
                                ->add('name', 'as_model[]')
                                ->add("label", '���� ����� ��:')
                                ->add('value', 1)
                                ->add("data", $db->field_data('model_type'));

        $this->country_id = Select::init()
            ->add("label", "������:")
            ->add("value", $user->country_id ? $user->country_id : 38)
            ->add('data', $db->field_data('country'))
            ->add('class', 'form-control');
        $this->city = Text::init()->add("label", "����: *")->add('class', 'form-control')->add('value', $user->city);
        $this->address = Text::init()->add("label", "�����:")->add('class', 'form-control')->add('value', $user->address);
        $this->phone = Text::init()->add("label", "�������: *")->add('class', 'form-control')->add('value', $user->phone);
        $this->pictures = Files::init()->add('name', 'pictures[]')->add("multiple", true)->add('class', 'form-control')->add('label', '����� ! 
�� �������� ������ �� ���:
            <ul>
            <li>���� ���</li>
            <li>��� ����� �����!</li>
            <li>������ �� �������� ������ �� 1 ��������</li>
            </ul>
            �������� ������ �� ����� � jpg ������ � ������� �� �� ���������� 
            10 MB')->add('id', 'p_pic');
        $this->action        = Hidden::init()
                              ->add("value", 'model_register');

        $this->capcha     = Recaptcha::init()
                              ->add("lang", LANG)
                              ->add("site_key", CAPTCHA_KEY)
                              ->add("secret", CAPTCHA_SECRET);

        $this->submit        = Submit::init()
                              ->add("value", $translations['submit'][LANG]['label'])
                              ->add("class", "btn btn-success");
        parent::__construct($post_data, $files_data);
    }

    public function save()
    {
        $this->db->db->beginTransaction();
        $default =  array(
            ":year_of_birth" => null,
            ":education" => null,
            ":physique" => null,
            ":weight" => null,
            ":height" => null,
            ":chest_size" => null,
            ":waist" => null,
            ":hips" => null,
            ":eyes" => null,
            ":hair_length" => null,
            ":hair_type" => null,
            ":hair_color" => null,
            ":size_clothes" => null,
            ":size_shoes" => null,
            ":country_id" => null,
            ":city" => null,
            ":address" => null,
            ":phone" => null
        );
        try {
            #$data = $this->clean_data();
            $data =$_POST;
            $values = array();
            $keys = array();
            foreach($data as $k=>$v) {
                if(in_array($k, array('capcha', 'foreign_languages', 'skills', 'experience', 'participate', 'as_model', 'pictures', 'action', 'submit'))) {
                    continue;
                }
                $default[":$k"] = $v;
            } 
            $default[":id"] = $_SESSION['abstract_user']->id; 
            $this->db->update_applicant($default);
            $id = $_SESSION['abstract_user']->id;
            $this->db->db->commit();
        } catch (\Exception $e) {
            $this->db->db->rollBack();
            throw new \Exception($e->getMessage());
        }

        if($images = $this->pictures->files){
            $images = $this->reArrayFiles($images);
            foreach($images as $image) {
                $this->saveImage($image, $id);
            }
        }

        $db = $this->db;
        $m2m = function($data=null, $func_name) use($id, $db){
            var_dumP($data);
            if(!is_array($data)) {
                return;
            }

            foreach($data as $value) {
                $db->$func_name($id, $value);
            }
        };
        $m2m($data['skills'], 'add_skill');
        $m2m($data['experience'], 'add_experience');
        $m2m($data['as_model'], 'add_model');
        $m2m($data['foreign_languages'], 'add_language');
        $m2m($data['participate'], 'add_participate');
        $name = $this->first_name->value . ' ' . $this->last_name->value;
        $email = $this->email->value;
        #return $this->before_save($name, $email);
    }

    public function reArrayFiles(&$file_post)
    {
        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }
        return $file_ary;
    }

    public function saveImage($image, $id)
    {
        $status = false;
        $ds = DIRECTORY_SEPARATOR;
        $image_name = urlencode($image['name']);
        $full_path = $this->img_path.$ds.$image_name;
        if(file_exists($full_path)) {
            return $status;
        }

        $status = move_uploaded_file($image['tmp_name'], $full_path);
        $web_path = str_replace($_SERVER['DOCUMENT_ROOT'], '', $full_path);
        
        if(!$status){
            return $status;
        }

        return $this->db->upload_applicant_picture(
                    $id,
                    $image_name,
                    $image['type'],
                    $image['size'],
                    $web_path
                );
    }

}
