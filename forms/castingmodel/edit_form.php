<form method="post" enctype="multipart/form-data">
<?php
$data = function($from, $to) { 
    $tmp = array();
    foreach(range($from, $to) as $data) {
        $tmp[$data] = $data;
    }
    return $tmp;
};
?>
        <div class="form-group">
        <label for="<?=$edit_form->year_of_birth->name?>" class=""><?=$edit_form->year_of_birth->label?></label>
            <div class="selector" >
                <?=$edit_form->year_of_birth?>
            </div>
        </div>
        <div class="form-group">
        <label for="<?=$edit_form->education->name?>" class=""><?=$edit_form->education->label?></label>
            <div class="selector" >
                <?=$edit_form->education?>
            </div>
        </div>
        <div class="form-group">
            <label for="country_id" class=""><?=$edit_form->country_id->label?></label>
            <?=$edit_form->country_id?>
        </div>
        <div class="form-group">
            <label for="<?=$edit_form->city->name?>" class=""><?=$edit_form->city->label?></label>
                <?=$edit_form->city;?>
        </div>
        <div class="form-group">
            <label for="<?=$edit_form->address->name?>" class=""><?=$edit_form->address->label?></label>
                <?=$edit_form->address;?>
        </div>
        <div class="form-group">
            <label for="<?=$edit_form->phone->name?>" class=""><?=$edit_form->phone->label?></label>
                <?=$edit_form->phone;?>
        </div>
        <div class="form-group">
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>���:</h4>
                <p></p>
            </div>
            <?php
            $eyes = array( "�����" => "�����", "������" => "������", "������" => "������", "����" => "����", "������" => "������",);
            foreach($eyes as $k=>$v) { ?>
            <?php 
                $selected = '';
                if($applicant->eyes == $k) {
                    $selected = 'checked';
                }
            ?>
            <label class="radio-inline">
            <div class="radio"><span><input type="radio" name="eyes" value="<?=$k?>" <?=$selected?>></span></div><?=$v?>
            </label>
            <?php } ?>
        </div>
        <div class="form-group">
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>������� �� ����:</h4>
                <p></p>
            </div>
            <?php 
            $hair_lengths = array( "����� ����"  => "����� ����", "����" => "����", "������ �����"=> "������ �����", "�����"       => "�����", "����� �����" => "����� �����",);
            foreach($hair_lengths as $k=>$v): ?>
            <?php 
                $selected = '';
                if($applicant->hair_length == $k) {
                    $selected = 'checked';
                }
            ?>
                <label class="radio-inline">
                <div class="radio"><span><input type="radio" name="hair_length" value="<?=$k?>" <?=$selected?>></span></div><?=$v?>
                </label>
            <?php endforeach;?>
        </div>
        <div class="form-group">
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>��� ����:</h4>
                <p></p>
            </div>
            <?php
                $hair_type = array( "�����"    => "�����", "��������" => "��������", "�������"  => "�������");
            foreach($hair_type as $k=>$v): ?>
            <?php 
                $selected = '';
                if($applicant->hair_type == $k) {
                    $selected = 'checked';
                }
            ?>
                <label class="radio-inline">
                <div class="radio"><span><input type="radio" name="hair_type" value="<?=$k?>" <?=$selected?>></span></div><?=$v?>
                </label>
            <?php endforeach;?>
        </div>
        <div class="form-group">
        <label for="hair_color" class=""><?=$edit_form->hair_color->label?></label>
            <?=$edit_form->hair_color?>
        </div>
        <div class="form-group col-md-4">
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>������:</h4>
                <p></p>
            </div>
            <?php
            $skills = $model->field_data('skill_type');
            $skill_types = array() ;
            foreach( $model->get_skill_types($uid) as $skill) { 
                $skill_types[] = $skill->skill_id; 
            }
                foreach($skills as $k=>$v): ?>
            <?php
                $selected = '';
                if(in_array($k, $skill_types)) {
                    $selected = 'checked';
                }
            ?>
                    <div class="checkbox">
                        <label>
                        <div class="checker"><span><input type="checkbox" name="skills[]" value="<?=$k?>" <?=$selected?>></span></div>
                            <?=$v?>
                        </label>
                    </div>
            <?php endforeach;?>
        </div>
        <div class="form-group col-md-4">
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>����:</h4>
                <p></p>
            </div>
            <?php
            $experiences = $model->field_data('experience_type');
            $experience_type = array() ;
            foreach( $model->get_experience_types($uid) as $exp) { 
                $experience_type[] = $exp->exp_id; 
            }
                foreach($experiences as $k=>$v): ?>
            <?php
                $selected = '';
                if(in_array($k, $experience_type)) {
                    $selected = 'checked';
                }
            ?>
                    <div class="checkbox">
                        <label>
                        <div class="checker"><span><input type="checkbox" name="experience[]" value="<?=$k?>" <?=$selected?>></span></div>
                        <?=$v?>
                        </label>
                    </div>
            <?php endforeach;?>
        </div>
        <div class="form-group">
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>�����������e</h4>
                <p></p>
            </div>
            <?php
            $physique = $model->field_data('phisique_type'); 
            foreach($physique as $k=>$v): ?>
            <?php 
                $selected = '';
                if($applicant->physique == $k) {
                    $selected = 'checked';
                }
            ?>
                <label class="radio-inline">
                <div class="radio"><span><input type="radio" class="form-check" name="physique" value="<?=$k?>" <?=$selected?>></span></div><?=$v?>
                </label>
            <?php endforeach; ?>
        </div>
        <div class="form-group">
            <label for="weight" class="">�����:</label>
            <div class="selector" style="width: 75px;">
                <select class="form-control" label="�����:" name="weight">
                <?php foreach($data(AgencyModelProfileEdit::MODEL_MIN_WEIGHT, AgencyModelProfileEdit::MODEL_MAX_WEIGHT) as $k=>$v): ?>
                <?php 
                    $selected = '';
                    if($applicant->weight == $k) {
                        $selected = 'selected';
                    }
                ?>
                                <option value="<?=$k?>" <?=$selected?>><?=$v?></option>
                <?php endforeach;?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="height" class="">��������:</label>
            <div class="selector" style="width: 106px;">
                <select class="form-control" label="��������:" value="185" name="height">
                <?php foreach($data(AgencyModelProfileEdit::MODEL_MIN_HEIGHT, AgencyModelProfileEdit::MODEL_MAX_HEIGHT) as $k=>$v): ?>
                <?php 
                    $selected = '';
                    if($applicant->height == $k) {
                        $selected = 'selected';
                    }
                ?>
                    <option value="<?=$k?>" <?=$selected?>><?=$v?></option>
                <?php endforeach;?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="size_shoes" class=""><?=$edit_form->size_shoes->label?></label>
            <div class="selector" style="width: 137px;">
            <?=$edit_form->size_shoes;?>
        </div>
        <div class="form-group">
            <label for="chest_size" class="">������ ��������:</label>
            <input type="number" class="form-control uniform-input number" value="<?=$applicant->chest_size;?>" name="chest_size">
        </div>
        <div class="form-group">
            <label for="waist" class="">�����:</label>
            <input type="number" class="form-control uniform-input number" value="<?=$applicant->waist?>" name="waist">
        </div>
        <div class="form-group">
            <label for="hips" class="">����:</label>
            <input type="number" class="form-control uniform-input number" value="<?=$applicant->hips?>" name="hips">
        </div>
        <div class="form-group">
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>����� �����:</h4>
                <p></p>
            </div>
    <?php $model_lang = array() ;
    foreach($model->get_applicant_languages($uid) as $lang) {
        $model_lang[] = $lang->lang_id;
    }
    ?>
            <?php foreach($model->field_data('languages') as $k=> $lang): ?>
            <?php 
                    $selected = '';
                    if(in_array($k, $model_lang)) {
                        $selected = 'checked';
                    }
            ?>
                <div class="checkbox">
                    <label>
                    <div class="checker"><span><input type="checkbox" class="form-check" name="foreign_languages[]" value="<?=$k?>" <?=$selected?>></span></div>
                    <?=$lang?>
                    </label>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="form-group">
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>���� ����� ��:</h4>
                <p></p>
            </div>
    <?php
    $experiences = $model->field_data('model_type');
    $experience_type = array() ;
    foreach( $model->get_appl_choicen_types($uid) as $exp) { 
        $experience_type[] = $exp->model_id; 
    }
        foreach($experiences as $k=>$v): ?>
    <?php
        $selected = '';
        if(in_array($k, $experience_type)) {
            $selected = 'checked';
        }
    ?>
            <div class="checkbox">
                <label>
                <div class="checker"><span><input type="checkbox" name="as_model[]" value="<?=$k?>" <?=$selected?>></span></div>
                <?=$v?>
                </label>
            </div>
    <?php endforeach;?>
        </div>
        <div class="form-group">
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>����� �� �������� �:</h4>
                <p></p>
            </div>
            <?php
            $experiences = $model->field_data('participation_type');
            $experience_type = array() ;
            foreach( $model->get_participation_types($uid) as $exp) { 
                $experience_type[] = $exp->part_id; 
            }
                foreach($experiences as $k=>$v): ?>
            <?php
                $selected = '';
                if(in_array($k, $experience_type)) {
                    $selected = 'checked';
                }
            ?>
            <div class="checkbox">
                <label>
                <div class="checker"><span><input type="checkbox" name="participate[]" value="<?=$k?>" <?=$selected?>></span></div>
                <?=$v?>
                </label>
            </div>
    <?php endforeach;?>
        </div>
        <div class="form-group">
        <label for="<?=$edit_form->pictures->name?>" class=""><?=$edit_form->pictures->label?></label>
            <?=$edit_form->pictures?>
        </div>
    </div>
    <div class="form-group">
        <?=$edit_form->action?>
    </div>
