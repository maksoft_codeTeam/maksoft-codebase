<?php
require_once __DIR__.'/../maksoft/inquire/BaseFormBootstrap.php';

use Jokuf\Form\Field\Text;
use Jokuf\Form\Field\Email;
use Jokuf\Form\Field\Select;
use Jokuf\Form\Field\Phone;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Checkbox;
use Jokuf\Form\Field\Files;
use Jokuf\Form\Field\Textarea;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Validators\EmailValidator;
use Jokuf\Form\Field\Recaptcha;
use Jokuf\Form\Bootstrap;



class DeleteUserProfileForm extends Bootstrap
{
    protected $db;

    public function __construct($db, $post_data=array())
    {
        $post_data = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $this->db = $db;
        $this->userId = Hidden::init()->add_validator(NotEmpty::init(true)->err_msg('not empty!'));
        $this->action = Hidden::init()->add('value', 'delete_account');
        $this->submit = Submit::init()
                            ->add('value', '������')
                            ->add("onclick", "return confirm('������� �� ���, �� ������ �� �������� ���� ������? ���������� � ����������.')");
        parent::__construct($post_data);
    }

    public function is_valid()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST[$this->action->name]) and $_POST[$this->action->name] == $this->action->value) {
            try {
                return parent::is_valid(); 
            } catch (\Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }
        return false;
    }


    public function save()
    {
        //GET PROFILE
        $profile = $this->db->get_single_applicant($this->userId->value);
        $mak_user = $this->db->getSingleMaksoftUserProfile($profile->email);
        $images = $this->db->get_images_list($profile->id);

        if(!$profile or !$mak_user) {
            throw new \Exception('Unable to delete user');
        }

        foreach($images as $image) {
            $this->db->deleteImage($image->path);
        }

        $this->db->delete_membership($profile);
        $this->db->deleteProfile($profile->id);
        $this->db->deleteFromMaksoftProfile($mak_user->ID);
        $this->db->deleteFromSiteAccess($mak_user->ID);
    }
}
