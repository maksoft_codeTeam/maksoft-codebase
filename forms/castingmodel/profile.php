<?php if(!isset($_SESSION['abstract_user'])) { return; }?>
<?php $m_user = $_SESSION['abstract_user'];?>
<?php 
require_once __DIR__.'/base.php';
require_once __DIR__.'/AgencyModelProfileEdit.php';
require_once __DIR__.'/EpayForm.php';
$pay_form = new EpayForm($_POST); $uid = $_SESSION['abstract_user']->id;
$applicant = $model->get_single_applicant($uid);
$images = $model->get_images_list($uid);
$edit_form = new AgencyModelProfileEdit($model, $_POST, $_FILES, $o_page, $mail, $applicant);  
$no_image = "/web/admin/images/no_image.jpg";
$profile_image = array_shift($images);
$profile_image = $profile_image ? $profile_image->path : $no_image;
$country = $model->get_country($applicant->country_id);
$experiences = $model->get_experience($uid);
$languages = $model->get_languages($uid);
$models = $model->get_model_types($uid);
$participations = $model->get_participations($uid);
$skills = $model->get_skills($uid);
$u_membership = $model->get_user_membership($applicant);
if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action']) && $_POST['action'] == $edit_form->action->value) {
    try {
        $edit_form->save($o_page);
        $o_page->login_as_guest();
        echo '<script>location = \'/page.php?n=19372958&SiteID=1047\';</script>';
    } catch (\Exception $e) {
    }
}
$payment_step = 1;
?>
<?php
if($_SERVER['REQUEST_METHOD'] === 'POST' and isset($_POST['action']) and $_POST['action'] == 'user_canceled' && is_numeric($_POST['pid'])) {
    $payment = $model->get_payment($_POST['pid'], $m_user, session_id());
    $model->update_payment($payment, false, '��������� � �������� �� ����������� �� ������ 2');
}
if($_SERVER['REQUEST_METHOD'] === 'POST' and isset($_POST[$pay_form->action->name]) and $_POST[$pay_form->action->name] == $pay_form->action->value) {
    /*
     * ���������� ���� ������ ���� ������ �� ������� � �������
     */
    $actions = array('thanks', 'cancel');
    $types = array();
    $token = uniqid(session_id().'_', true);
    $_SESSION['token'] = $token;
    try {
        $pay_form->is_valid(); 
        $type = $model->get_membership_type($pay_form->package_type->value);
        $payment = $model->insert_payment($m_user, $type, false, 'success', session_id(), $token);
        if(!$payment) {
            throw new \Exception('insert payment doesnt make it');
        }
        define('URL_OK' , 'https://castingmodel.eu/?action=thanks&type='.$pay_form->package_type->value.'&uid='.$_SESSION['abstract_user']->id.'&pid='.$payment->id);
        define('URL_CANCEL' , 'https://castingmodel.eu/?action=cancel&type='.$pay_form->package_type->value.'&uid='.$_SESSION['abstract_user']->id);
        $payment_step=2;
    } catch (\Exception $e) {
        $errors = array();
        foreach($pay_form as $field){
            if(!$field->getErrors()){
                continue;
            }
            $errors[$field->name] = $field->getErrors();
        }

        $model->insert_payment($m_user, $type, false, array_to_txt($fields) . ' |||||||| '.$e->getMessage(), session_id(), $token);
    }
}
?>
  <div class="row">
    <div class="col-md-12">
    
      <div class="panel panel-default">
            <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8">
                            <h2><?=$m_user->first_name;?> <?=$m_user->middle_name;?> <?=$m_user->last_name;?></h2>
                            <p><strong>���: </strong> <?=$m_user->gender;?> </p>
                            <p><strong>�������:</strong> <?=$applicant->phone?></p>
                            <p><strong>E-mail:</strong> <?=$applicant->email?></p>
                            <p><strong>�����:</strong> <?=$country->name. ', '. $applicant->city .', '. $applicant->address?></p>
        <div class='row'>
            <?=$parser($experiences, '������');?>
            <?=$parser($languages, '�����');?>
            <?=$parser($models, '���� ����� ��:');?>
            <?=$parser($participations, '�e �������� ����:');?>
            <?=$parser($skills, '������');?>
        </div>
        <div class="row">
<?php foreach($images as $img): ?>
            <div class="col-md-4">
            <img src="<?=$img->path?>" alt="<?=$img->name?>">
                <hr>
            </div>
<?php endforeach;?>
        </div>
                        </div><!--/col-->          
                        <div class="col-xs-12 col-sm-4 text-center">
                                <img src="<?=$profile_image?>" alt="" class="center-block img-circle img-responsive">
                                <h5>ID <?=$applicant->id?></h5>
                                <hr>
                                <ul class="list-inline ratings text-left" title="Ratings">
                                    <li>���: <?=$applicant->gender?></li>
                                    <li>������ �� �������: <?=$applicant->year_of_birth?></li>
                                    <li>�����������: <?=$applicant->obrazovanie?></li> 
                                    <li>������������: <?=$applicant->fizika?></li>
                                    <li>�����: <?=$applicant->weight?></li>
                                    <li>��������: <?=$applicant->height?></li>
                                    <li>����: <?=$applicant->waist?></li>
                                    <li>�����: <?=$applicant->hips?></li>
                                    <li>���� �� �����: <?=$applicant->eyes?></li>
                                    <li>������� �� ������: <?=$applicant->hair_length?></li>
                                    <li>��� ����: <?=$applicant->hair_type?></li>
                                    <li>���� �� ������: <?=$applicant->hair_color?></li>
                                    <li>������ �����: <?=$applicant->clothes_size?></li>
                                    <li>������ ������: <?=$applicant->shoes_size?></li>
                                </ul>
                        </div><!--/col-->

                        <div class="col-xs-12 col-sm-4">
                            <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-block"><span class="fa fa-gear"></span> �������� </button>  
                        </div><!--/col-->
                    </div><!--/row-->
              </div><!--/panel-body-->
          </div><!--/panel-->
    </div>
  </div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">�������� �� ������</h4>
      </div>
      <div class="modal-body">
        <?php require_once __DIR__ .'/edit_form.php'; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
        <button type="submit" class="btn btn-primary">������</button>
      </div>
        </form>
    </div>
  </div>
</div>
<style type="text/css">
#bg, #search-bg {
  background-repeat: no-repeat;
  background-size: 1080px auto;
}

#bg {
  background-position: center top;
  padding: 70px 90px 120px 90px;
}

#search-container {
  position: relative;
}

#search-bg {
  /* Absolutely position it, but stretch it to all four corners, then put it just behind #search's z-index */
  position: absolute;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
  z-index: 99;

  /* Pull the background 70px higher to the same place as #bg's */
  background-position: center -70px;

  -webkit-filter: blur(10px);
  filter: url('/media/blur.svg#blur');
  filter: blur(10px);
}

#search {
  /* Put this on top of the blurred layer */
  position: relative;
  z-index: 100;
  padding: 20px;
  background: rgb(34,34,34); /* for IE */
  background: rgba(34,34,34,0.75);
}

@media (max-width: 600px ) {
  #bg { padding: 10px; }
  #search-bg { background-position: center -10px; }
}

#search h2, #search h5, #search h5 a { text-align: center; color: #fefefe; font-weight: normal; }
#search h2 { margin-bottom: 50px }
#search h5 { margin-top: 70px }

.close_box {
    position: absolute;
    top: 5%;
    left: 95%;
    background:none!important;
    color: inherit!important;
    border: none;
    cursor: pointer;
}
</style>
<?php if ($payment_step === 1): ?>
<?php require_once __DIR__.'/widgets/membership_types.php';?>
<?php else: ?>
 <div id='bg' class=' transparent text-center' style="
        position: absolute;
        top: 5%;
        left: 5%;
        width: 90%;
        height:30%;
">
      <div id="search-container">
        <div id="search-bg"></div>
        <div id="search">
        <div class="close_box">
            <form method="POST">
                <input type='hidden' name='action' value='user_canceled'>
                <input type='hidden' name='pid' value='<?=$payment->id?>'>
                <button type='submit' name='submit'>x</button>
            </form>
        </div>
          <h2>�������� ��� ePay.bg</h2>
            <form action="https://www.epay.bg/v3main/paylogin" method="POST">
                <input type="hidden" name="TOTAL" value="<?=$pay_form->total->value?>">
                <input type="hidden" name="MIN" value="6254594350">
                <input type="hidden" name="DESCR" value="<?=iconv('cp1251', 'utf8', $pay_form->descr->value)?>">
                <input type="hidden" name="URL_OK" value="<?=URL_OK?>">
                <input type="hidden" name="URL_CANCEL" value="<?=URL_CANCEL?>">
                <input type="hidden" name="ENCODING" value="utf8">
                <input type="hidden" name="CHECKSUM" value="1f005c6b23efbf9ea8c127d0370cb0f08b6f230d">
                <input type="submit" name="BUTTON:EPAYNOW" value="������� on-line">
                </form>
            </div>
          </div>
 </div>
<?php endif;?>
