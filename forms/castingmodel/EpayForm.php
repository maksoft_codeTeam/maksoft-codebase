<?php
use Jokuf\Form\Validators\NotEmpty;
use Jokuf\Form\Field\Hidden;
use Jokuf\Form\Field\Submit;
use Jokuf\Form\Field\Integer;
use Jokuf\Form\Field\Text;
use Jokuf\Form\Bootstrap;


class EpayForm extends Bootstrap
{
    public function __construct($post_data)
    {
        $this->total = Hidden::init()
            ->add_validator(NotEmpty::init(true)->err_msg('form is modified'));
        $this->descr = Hidden::init();
        $this->package_type = Hidden::init()
            ->add_validator(function($value) {
                return $value > 0;
            });
        $this->submit = Submit::init()
            ->add('value', '����');
        $this->action = Hidden::init()
            ->add('value', 'subscribe');

        parent::__construct($post_data);
    }

    public function validate_total($field) {
        return $field->value > 0;
    }
}
