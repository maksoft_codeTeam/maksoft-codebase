<?php
ini_set('display_errors',1);
ini_set('display_startup_errors', 1);
$document_root = realpath(__DIR__.'/../../');
require_once $document_root.'/modules/bootstrap.php';
require_once $document_root.'/lib/lib_functions.php';
require_once __DIR__.'/CastingModel.php';
require_once __DIR__.'/data.php';

require_once $document_root.'/lib/lib_page.php';

if(!isset($di_container) or $di_container) {
    $di_container = new \Pimple\Container();
    $di_container->register(new \Maksoft\Containers\GlobalContainer());
}

$email = $di_container['email'];

if(!isset($o_page)) {
    $o_page = new site();
}
$model = new CastingModel($di_container['db']);
$db = $di_container['db'];
$parser = function($data, $title) {
    return "<div class='col-md-4'><h3>$title</h3><ul class='container details'>".array_reduce($data, function($carry, $item) {
            $carry .= sprintf("<li>%s</li>", $item->name);
            return $carry;
    })."</ul></div>";
};
define("CAPTCHA_KEY", $o_page->get_sConfigValue("reCAPTCHA_Site_key"));
define("CAPTCHA_SECRET", $o_page->get_sConfigValue("reCAPTCHA_Secret_key"));

$only_producers = array(!isset($_SESSION['abstract_user']), $user->AccessLevel > 0);
$authorized = count(array_filter($only_producers)) == 2;

$show = function($d, $text='������ �����') use ($user) {
    if(!isset($_SESSION['abstract_user']) and $user->AccessLevel > 0) {
        return $d;
    }
    return $text;
};

$active = function($applicant) use ($model) {
    $memb = $model->get_user_membership($applicant);

    if(!$memb) {
        return false;
    }

    return strtotime($memb->to_date) > time();
};
