<?php
require_once __DIR__.'/base.php';
require_once __DIR__.'/Login.php';
require_once __DIR__.'/../maksoft/authentication/Login.php';
$login_form = new \Casting\Authentication\Login($_POST, $model);
$login_form_producer = new Maksoft\Authentication\Login($_POST, $model);
$logged = false;
$logged_abstract = false;
if($_SESSION['user']->ID > 0) :
    $logged = true;
endif;
if($_SESSION['abstract_user']) {
    $logged_abstract = true;
}


if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action']) && $_POST['action'] == $login_form->action->value) {
    try { 
        $login_form->is_valid();
        $login_form->save($o_page);
        $o_page->login_as_guest();
    } catch (\Exception $e) {
        echo $e->getMessage();
    }
}

?>
<div class="row justify-content-around">
    <div class="col-md-<?=$logged ? 10 : 4?>">
        <h2>���� �� ������ � ��������</h2>
        <hr>
        <?=$login_form?>
    </div>
    <?php if(!$logged): ?>
    <div class="col-md-4">
        <h2>���� �� ����������</h2>
        <hr>
        <div id="emp-login" class="wrap text-center">
            <?=$login_form_producer?>
        </div>
    </div>
    <?php endif;?>
</div>
