	<!-- Meditsinski-izdelia.eu individual TEXT EDITOR BG version //-->

	<script type="text/javascript" src="text_editor/jscripts/tiny_mce/tiny_mce.js"></script>
	
	<script language="javascript" type="text/javascript">
		tinyMCE.init({
		language : 'bg', 
		mode : "exact", // exact|: polzva tochno opredelen element
		elements : "text_editor", //ID-to na elementa, vurhu koito shte se izpulni redaktora
		theme : "advanced",  //  default, simple, advanced
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		button_tile_map : true,
		entity_encoding : "raw",
		verify_html : false,
		apply_source_formatting : true,
		fix_nesting : true,
		remove_redundant_brs : true,
		cleanup_on_startup : true,
		element_format : "html",
		width: "100%",
		
		// Theme options
		//theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_blockformats : "p,address,h1,h2,h3,h4", 
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect, styleselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/aikhorn/content_styles.css",

		// Drop lists for link/image/media/template dialogs
		<?php
			if(file_exists("web/links/templates/".$SiteID."_template_list.js"))
				echo 'template_external_list_url : "web/links/templates/'.$SiteID.'_template_list.js",'; 
			else
				echo 'template_external_list_url : "text_editor/examples/lists/template_list.js",';
		?>
		//template_external_list_url : "text_editor/examples/lists/template_list.js",
		external_link_list_url : "web/links/lists/<?=$SiteID?>_links_list.js",
		external_image_list_url : "web/images/lists/<?=$SiteID?>_image_list.js",
		//external_image_list_url : "text_editor/examples/lists/image_list.js",
		//media_external_list_url : "web/links/<?=$SiteID?>_links_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
	</script>