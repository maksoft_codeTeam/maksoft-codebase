<?php
session_start();
require_once __DIR__.'/../lib/lib_page.php';


if(!isset($o_page)) {
    $o_page = new page();
}


if(!isset($_SESSION['user']) or $_SESSION['user']->WriteLevel < 1) {
    echo '[]';
    return;
}


$sql = $o_page->pages_base_query("AND p.SiteID='{$o_page->_site['SitesID']}' AND p.SecLevel=0 AND LENGTH(tr.Name) >3");

$o_page->db_query("SET NAMES UTF8");
$pages_q = $o_page->db_query($sql); 
$links = array();
$pages = $o_page->fetch('object', $pages_q);
foreach($pages as $page){
    $links[] = array('title' => $page->Name, 'value' => $o_page->get_pLink($page->n));
}
header('Content-Type: application/json', 'charset=utf8');
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
header('Cache-Control: max-age='.$api->time);
header('Pragma: cache');


echo json_encode($links);
