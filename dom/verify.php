<!--
	The script tries to catch the confirmation id from the url.
	It either uses the GET variable key, the last GET variable or the string after the last slash (/)

	Please remove the test service URL below for production use.
-->
<html>
<head>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<style>
		body {
			font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
			font-size:14px;
			line-height:1.42857143;
			color:#333;
			background-color:#fff
		}
		input,button,select,textarea{
			font-family:inherit;
			font-size:inherit;
			line-height:inherit
		}
		.box {
			border: 1px solid #428BCA;
			border-radius: 5px;
			width: 90%;
			max-width: 800px;
			min-width:320px;
			margin-left: auto;
			margin-right: auto;
		}
		.box .header {
			background-image: linear-gradient(to bottom, #428BCA 0px, #357EBD 100%);
			background-repeat: repeat-x;
			border-color: #428BCA;
			border-top-left-radius: 3px;
		    border-top-right-radius: 3px;
			overflow: hidden;
			padding: 10px;
			color: #fff;
		}
		.box .header h1,
		.box .header h2,
		.box .header h3,
		.box .header h4,
		.box .header h5,
		.box .header h6
		{
			margin: 0;
			font-family:inherit;
			font-weight:500;
			line-height:1.1;
			color:inherit
		}
		.box .body {
			padding:10px;
		}
		.box.error {
			border-color: #EBCCD1;
		}
		.box.error .header {
			background-image: linear-gradient(to bottom, #F2DEDE 0px, #EBCCCC 100%);
			color: #A94442;
		}

		.box.success {
			border-color: #D6E9C6;
		}
		.box.success .header {
			background-image: linear-gradient(to bottom, #DFF0D8 0px, #D0E9C6 100%);
			color:#3C763D;
		}

		.box.info {
			border-color: #BCE8F1;
		}
		.box.info .header {
			background-image: linear-gradient(to bottom, #D9EDF7 0px, #C4E3F3 100%);
			color:#31708F;
		}

		.btn {
			background-image: linear-gradient(to bottom, #FFFFFF 0px, #E0E0E0 100%);
		    background-repeat: repeat-x;
		    color: #333333;
		    border: 1px solid #CCCCCC;
			border-radius: 4px;
		    cursor: pointer;
		    display: inline-block;
		    font-size: 14px;
		    font-weight: 400;
		    margin-bottom: 0;
		    padding: 6px 12px;
		    text-align: center;
		    vertical-align: middle;
		    white-space: nowrap;
		}
		.btn:focus,.btn:hover {
			background-color: #E0E0E0;
			background-position: 0 -15px;
			border-color: #ADADAD;
		}
		.form-radio {
			margin-top: 0;
		}
		.form-element {
			margin-top: 10px;
		}
		#confirmation_form, #confirmation_error, #confirmation_ack, #confirmation_nack, #confirmation_load {
			display: none;
		}
	</style>
</head>
<body>
<script type="text/javascript">

// var service_url = "http://csl-registrar.com/service.joker";

// Test URL 
// - every id ending with 0 will be directly rejected
// - every id ending with 1 will be rejected on submit
// - every other id will be accepted
// service_url = "http://csl-registrar.com/service_test.joker";

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function getKey(){
    var key = getUrlVars()['key'];
    if (key) return key;
    var keyRE = window.location.href.match("[/=]([^&/\.]+)$");
    if (keyRE) return(keyRE[1]);
    return false;
}

function send_request(request,key,cb_success,cb_error) {
	$.ajax({
        url: service_url,
        dataType: 'jsonp',
        timeout: 10000,
        data: { data: JSON.stringify({
        	request: request,
        	key: key
        }) },
        success: function (data) {
			if (data.error) {
				cb_error();
			} else {
				cb_success(data);
			}
        },
        error: function() {
        	cb_error();
        }
	});	
}


function get_confirmation_details(key) {
	send_request('confirmation_details',key,
		function(data) {
			$('#c_email').append(data.email);
			$.each(data.domains,function(i,domain) {
				$('#c_domains').append('<li>'+domain+'</li>');
			});
			show('form');
		},
		function() {
			show('error');
		}
	);
 }

function send_confirmation(key,vote) {
	var response = vote?'confirmation_acknowledge':'confirmation_decline';
	send_request(response,key,
		function(data) {
			show(vote?'ack':'nack');
		},
		function() {
			show('error');
		}
	);
}

function show(section) {
	hide_all();
	$("#confirmation_"+section).show();
}

function hide_all() {
	$("#confirmation_form").hide();
	$("#confirmation_error").hide();
	$("#confirmation_ack").hide();
	$("#confirmation_nack").hide();
	$("#confirmation_load").hide();
}

$(function(){

	var confirmation_key = getKey();
	hide_all();
	show("load");
	if (confirmation_key) {
		get_confirmation_details(confirmation_key);
		$("#c_send_confirm").click(function() {
			 var vote = ($("input:radio[name ='c_vote']:checked").val()=="yes");
			 send_confirmation(confirmation_key,vote);
		});
	} else {
		show('error');
	}

	
});


</script>

<noscript>
<div class="box error">
<!-- Content on load -->
	<div class="header"><h3>Error</h3></div>
	<div class="body">
		Please enable Javascript to complete your confirmation!
	</div>
</div>
</noscript>

<div id="confirmation_form" class="box">
	<!-- Content on page load  -->
	<div class="header"><h3>Please confirm your request</h3></div>
	<div class="body">
		The owner email address <strong id="c_email"></strong> of the following domains needs to be confirmed:
		<ul id="c_domains">
		</ul>
		<div class="form-radio">
			<input type="radio" name="c_vote" id="c_vote_yes" value="yes" checked>
			<label for="c_vote_yes"> Yes, I confirm this owner email address.</label>
		</div>
		<div class="form-radio">
			<input type="radio" name="c_vote" id="c_vote_no" value="no">
			<label for="c_vote_no"> No, I do not confirm this owner email address.</label>
		</div>
		<div class="form-element">
			<input class="btn" type="button" name="c_send" id="c_send_confirm" value="Send">
		</div>
	</div>
</div>

<div id="confirmation_load" class="box info">
<!-- Content on load -->
	<div class="header"><h3>Please wait...</h3></div>
	<div class="body">
		Retrieving confirmation data ...
	</div>
</div>

<div id="confirmation_error" class="box error">
<!-- Content on error -->
	<div class="header"><h3>Request could not be completed!</h3></div>
	<div class="body">
		Confirmation link has expired or has already been used!
	</div>
</div>

<div id="confirmation_ack" class="box success">
<!-- Content on success and answer yes -->
	<div class="header"><h3>Request completed!</h3></div>
	<div class="body">
		The email address is successfully confirmed.
	</div>
</div>

<div id="confirmation_nack" class="box success">
<!-- Content on success and answer no -->
	<div class="header"><h3>Request completed!</h3></div>
	<div class="body">
	The email address is now declared as invalid. The affected domains will be suspended.
	</div>
</div>

</body>
</html>