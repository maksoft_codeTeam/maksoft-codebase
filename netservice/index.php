<?php
define(DS, DIRECTORY_SEPARATOR);
require_once __DIR__.DS.'config.php';
require_once __DIR__.DS.'../modules/vendor/autoload.php'; 
require_once __DIR__.DS.'../lib/config.php'; 
require_once __DIR__.DS.'../lib/event_handler.php'; 
require_once __DIR__.DS.'../lib/lib_page.php'; 
$site = new site();


function bd_nice_number($n) {
    // first strip any formatting;
    $n = (0+str_replace(",","",$n));
    
    // is this a number?
    if(!is_numeric($n)) return false;
    
    // now filter it;
    if($n>1000000000000) return round(($n/1000000000000),2).' trillion';
    else if($n>1000000000) return round(($n/1000000000),2).' милиарда резултата';
    else if($n>1000000) return round(($n/1000000),2).' милионa резултата';
    else if($n>1000) return round(($n/1000),2).' хиляди резултата';
    
    return number_format($n);
}

if(!isset($_GET['service'])){
exit('invalid request');
}
$service = $_GET['service'];

$url = "https://maksoft.net/api/?";
$keyword = $_GET['keyword'];
$siteID = $_GET['SiteID'];
$urlData = array(
    "keyword" => $keyword,
    "n" => 123,
    "SiteID" => $siteID,
);
switch ($service){
    case "netservice":
        $urlData["command"] = "kPosition";
        break;
    case "offsite":
        $urlData["command"] = "ePosition";
        $urlData["egID"] = $_GET['egID'];
        break;
    default:
        exit("unknown service!");
}

$url .= http_build_query($urlData);
$keywords = file_get_contents($url);
$keywords = json_decode($keywords);
$googleLink = "https://www.google.bg/search?".http_build_query(array("num" => 100, "q" => $keyword));
?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <script
          src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
          integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="
          crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
         <!--Load the AJAX API-->
        <script type="text/javascript" src="assets/chart.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js" integrity="sha256-RASNMNlmRtIreeznffYMDUxBXcMRjijEaaGF/gxT6vw=" crossorigin="anonymous"></script>
        <style>
            .chart {
              width: 100%; 
              min-height: 450px;
            }
        </style>
    </head>
    <body>

        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
 <nav class="nav-extended grey lighten-5">
    <div class="nav-wrapper"  style="margin-right: 5%;margin-left: 5%;">
      <a href="/" class="brand-logo"><img src="/Templates/maksoft/v2/assets/img/logo-standart.png"></a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
      <li ><span  class="blue-text text-darken-2"><?=$site->_site['Name'];?></span></li>
      </ul>
    </div>

  </nav>

        <div class="row">
            <div class="col s12">
                <h4> <a target="_blank" href="http://<?=$site->_site['primary_url']?>"><?=$site->_site['primary_url']?></a></h4>
                <p class="flow-text">Резултати за дума: <?=$keyword?></p>
              <div class="chip">
                <img src="https://www.greenmedinfo.com/sites/all/themes/wilderness/images-upgrade/Google_icon_2015.png" alt="Contact Person">
                <a target="_blank" href="<?=$googleLink?>">Виж в Гугъл</a>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m8">
                <canvas id="myChart" ></canvas>
                <div class="chart" id="chart_div"></div>
                <div class="chart" id="chart2"></div>
                <div class="chart" id="chart3"></div>
            </div>
            <div class="col s12 m4">
                <table>
                    <thead>
                      <tr>
                          <th data-field="id">Позиция</th>
                          <th data-field="name">Общо резултати</th>
                          <th data-field="price">Към дата</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($keywords){
                        $keyword = array_pop($keywords);
                        if($service==='netservice'){
                        ?>
                      <tr>
                        <td><?=$keyword->g_pos == 0 ? '> 100' : $keyword->g_pos?></td>
                        <td><?=bd_nice_number($keyword->g_result)?></td>
                        <td><?=$keyword->enter_time?></td> 
                      </tr>
                        <? } else { ?>
                      <tr>
                        <td><?=$keyword->g_pos == 0 ? '> 100' : $keyword->g_pos?></td>
                        <td><?=bd_nice_number($keyword->google_result)?></td>
                        <td><?=$keyword->date_checked?></td> 
                      </tr>

                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
        <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script>
        <script src="https://www.google-analytics.com/analytics.js" async defer></script>
        <script>

var url = "<?php echo $url;?>";
$.ajax({
    url: url,
    dataType: "json"
  }).done(function(response) {
        var adsa = [];
        var positions = [];
        var results = [];
        var scatter = [];
        for(i=0; i<response.length;i++){
      <?php if($service==='netservice'){ ?>
            dateObj = toDate(response[i].enter_time);
            var google_position = toInt(response[i].g_pos); 
            var google_result = toInt(response[i].g_result); 
            var month = dateObj.getUTCMonth() + 1; //months from 1-12
            var day = dateObj.getUTCDate();
            var year = dateObj.getUTCFullYear();
            newdate = day + "/" + month + "/" + year;
            scatter.push({x: google_position, y: google_result});
            results.push(google_result);
            positions.push(google_position);
            adsa.push(newdate);
      <?php } else { ?>
            dateObj = toDate(response[i].date_checked);
            var google_position = toInt(response[i].g_pos); 
            var google_result = toInt(response[i].google_result); 
            var month = dateObj.getUTCMonth() + 1; //months from 1-12
            var day = dateObj.getUTCDate();
            var year = dateObj.getUTCFullYear();
            newdate = day + "/" + month + "/" + year;
            scatter.push({x: google_position, y: google_result});
            results.push(google_result);
            positions.push(google_position);
            adsa.push(newdate);
      <?php }?>
        }
        var data = {
            labels: adsa,
            datasets: [
                {
                    label: "Позиция при търсене",
                    fill: true,
                    lineTension: 0.1,
                    fillColor: "rgba(151,187,205,0.2)",
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: positions,
                    scale: 1000,
                    spanGaps: false,
                },
                {
                    label: "Резултати от търсене",
                    lineTension: 0.1,
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151, 187, 205, 1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointColor: "rgba(151, 187, 205, 1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: results,
                    spanGaps: false,
                }
            ]
        };
        var maxValue = Math.max.apply(null, results);
        var ctx = document.getElementById("myChart");
        var myLineChart = new Chart(ctx, {
            type: 'line',
            responsive: true,
            animation: true,
            data: data,
            multiTooltipTemplate: "<%=datasetLabel%> : <%= value %>" ,
            tooltipTemplate: "<%if (label){%><%=label.tooltip%><%}%>",
            options: {

                hover: {
                    mode: "point",
                },
                scales: {
                    yAxes:[{
                        gridLines: {
                            display: false
                        },
                        type: "logarithmic",
                        ticks: {
                            min: 1,
                            max: maxValue * 95,
                        }
                    }],
                    xAxes:[{
                        gridLines:{
                            display: true
                        }
                    }],
                }
            },
        });
  });

        </script>
    </body>
</html>
