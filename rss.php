﻿<?php
header("Content-Type: text/xml; charset=UTF-8");
// UTF-8 
// http://www.xul.fr/en-xml-rss.html#structure
//login functions
/*iconv_set_encoding("internal_encoding","UTF-8");
iconv_set_encoding("output_encoding","UTF-8");
header("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
header("<rss version=\"2.0\">");*/

include_once("lib/lib_adbg.php"); 

include_once("web/admin/db/logOn.inc.php");

include_once("lib/lib_functions.php"); 

//DB functions
include_once("web/admin/query_set.php");

//Input/Output functions
include_once("web/admin/io.php");

include_once("lib/lib_rss.php");
$o_page = new rss();

include_once("lib/lib_polls.php");
$poll = new poll();

include_once("lib/lib_wwo.php");
$wwo = new wwo();

include_once("shortcodes/main/shortcodes.php");
include_once("shortcodes/poll/shortcodes.php");
include_once("shortcodes/wwo/shortcodes.php");



//page object classes
//include_once("lib/lib_page.php");

if($interval<1) {$interval=15;}
if($limit<1) {$limit=15;}
if($rss_n==0) {$rss_n=$Site->StartPage; }
if($preview < 1) $preview = 25;

function list_rss_items($Site, $url, $Rub, $PRub, $keyword, $interval, $limit, $rss_n, $rss_url, $ppage) {

 //$o_page = new page(); 
 global $Vid;
 global $preview;
 global $o_page;
 
 $maksoft = new page(); 
 $today = date("YYYY-MM-DD");
 if($ppage>0) {$ppage_str = " AND ParentPage='$ppage' "; } else {$ppage_str=""; }
 $keywQ = ""; 
 //if(strlen($keyword)>2) {
 	//$keyword = htmlspecialchars(iconv('CP1251','UTF-8',$keyword));
 	// $keywQ = " AND textStr LIKE '%$keyword%' ";
 //}
 $SiteIDstr = "SiteID='".$Site->SitesID."' ";
 if(strlen($Rub)>2) {
     $SiteIDstr = " Rub='".$Rub."'  ";
	 if(strlen($PRub)>2) {
	     $SiteIDstr = $SiteIDstr." AND PRub='".$PRub."'  ";
	 }
     $query_str_et = "SELECT * FROM etiketi WHERE $SiteIDstr AND date_expire > NOW() AND date_start <= NOW() AND date_modified >= DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND LENGTH(description)>20 ORDER BY date_modified  DESC  LIMIT $limit";
     // echo $query_str_et;
     $etiketi_q = $maksoft->db_query($query_str_et);
     $SiteIDstr = " Rub='".$Rub."' AND imageNo > 0  ";
	 if(strlen($PRub)>2) {
		 $SiteIDstr = $SiteIDstr." AND PRub='".$PRub."'  ";
	 }
     while ($row = mysqli_fetch_object($etiketi_q)) {
         //$date = $page->date_modified;
         $item_title = htmlspecialchars(iconv('CP1251', 'UTF-8', rtrim(substr(strip_tags($row->Name), 0, 80))));
         $item_title = $o_page->replace_shortcodes($item_title);
         $img_src = "";
         if(strlen($row->img_src)>12) {
             $img_src = '<enclosure url="'.$row->img_src.'" type="image/jpeg>"';
         }
         $item_description = htmlspecialchars(iconv('CP1251', 'UTF-8', cut_text($row->description, 255, "...")));
         $item_description = $o_page->replace_shortcodes($item_description);
         echo("
        <item>
         <title>$item_title</title>
         <link>".htmlspecialchars($row->url)."</link>
        <description>$item_description</description>
        <pubDate>".date(DATE_RFC2822, strtotime($row->date_modified))."</pubDate>
         <guid>".htmlspecialchars($row->url)."</guid>
         $img_src
         </item>");
     }
 }


 $query_str = "SELECT pages.*, images.image_src, Sites.Rub FROM pages left join images on pages.imageNo = images.imageID left join Sites on pages.SiteID = Sites.SitesID WHERE (SELECT MAX(toDate) 
 FROM `hostCMS` WHERE hostCMS.SiteID = pages.SiteID AND hostCMS.netservice>0)>NOW() AND $SiteIDstr  $ppage_str $keywQ AND SecLevel=0 AND (LENGTH(textStr)>150) AND ((LENGTH(pages.Name)>12) OR (LENGTH(pages.Title)>12))  AND (pages.preview>$preview) AND date_modified >= DATE_SUB(CURDATE(), INTERVAL $interval DAY)
 ORDER BY date_modified DESC LIMIT $limit";
 //echo $query_str;
 //if(strlen($Rub)<5) {

 $pages_q = $maksoft->db_query($query_str);
 while ($row = mysqli_fetch_object($pages_q)) {
     //$date = $page->date_modified;
     $item_title = $row->Name;
     if(strlen($row->title)>12) {$item_title = $row->title;}
     $item_title=htmlspecialchars(iconv('CP1251','UTF-8', rtrim(substr(strip_tags($item_title),0,80))));
     $item_title = $o_page->replace_shortcodes($item_title);
     $item_description = htmlspecialchars(iconv('CP1251','UTF-8', cut_text($row->textStr,255,"...")));
     $item_description = $o_page->replace_shortcodes($item_description);
     $enc_image == "";
     if($row->imageNo>0) {
         $enclosure_url = 'http://'.$url.'/'.$row->image_src;

         $enc_image='<enclosure url="'.$enclosure_url.'" type="image/jpeg" length = "'.filesize($row->image_src).'"/>';
     }

     if( (strlen($keyword)<5) || (strpos($item_title, $keyword)>0) || (strpos($item_desrciption, $keyword)>0) )  {
     echo("
	<item>
	 <title>$item_title</title>
	 <link>".$maksoft->get_pLink($row->n)."</link>
	<description>$item_description</description>
	<pubDate>".date(DATE_RFC2822, strtotime($row->date_modified))."</pubDate>
	 <guid>".$maksoft->get_pLink($row->n)."</guid>
	 $enc_image
	</item>");
         }

	  } // while content pages

/*	else {
 	 if(strlen($rss_url)<5)
	 {
	 	$rss_url = "$url/page.php"; 
	 }
	 
	 $PrubQ=""; 
	 if (strlen($Rub)>2) {$PrubQ = " AND ads.Rub='$Rub' "; }	 
	 if (strlen($PRub)>2) {$PrubQ = " AND ads.PRub='$PRub' "; }
	 if (strlen($keyword)>2) {$PrubQ = "$PrubQ AND ads.textAd LIKE '%$keyword%' "; }
	 
	 $VidQ = "";
	 if(isset($Vid)) $VidQ = "AND ads.Vid = '".$Vid."'";
	 
	 $query_str = "SELECT * FROM (ads, Contact, cities) left join imoti on ads.ID = imoti.ADBG WHERE Contact.SiteID='$Site->SitesID' AND ads.Mail=Contact.EMail AND ads.Vis>=0 AND ads.Mail = Contact.EMail AND Contact.Region = cities.Code $PrubQ $VidQ AND ads.Data >= DATE_SUB(CURDATE(), INTERVAL $interval DAY) ORDER BY ads.preview DESC LIMIT $limit";   	
	 //echo("$query_str"); 
	 $adbg = new adbg(); 
 	 $ads_q = $adbg->db_query("$query_str"); 
	 while ($row = mysqli_fetch_object($ads_q)) {
		 //$date = $page->date_modified; 
		 $item_title=" $row->RegionImot $row->Constr $row->Title";  
		 $item_title=htmlspecialchars(iconv('CP1251','UTF-8', substr(strip_tags($item_title),0,150))); 
		 if($row->Price>0) $item_title="$row->Price $row->Currency $item_title"; 
		 //$item_title=" $row->RegionImot $item_title";  
		 $item_desrciption = htmlspecialchars(iconv('CP1251','UTF-8',substr(strip_tags($row->textAd),0,250)));
		 echo("
		 <item>
			 <title>$item_title</title>
			 <link>http://$rss_url?n=$rss_n&amp;SiteID=$row->SiteID&amp;id=$row->ID</link> 
			<description>$item_desrciption</description>
		  </item>
		 "); 
	  } // while content ads
	 } //Rub is set*/
 
  //} // content ad-bg if Rub is set
}
/*
	if(!isset($SiteID))
		{
			$hostname = $_SERVER['HTTP_HOST'];
			$hostname = str_replace("www.", "", $hostname);
			$hostname = str_replace("ww.", "", $hostname);
			$s_query = mysqli_query("SELECT * FROM Sites WHERE url LIKE CONVERT( _utf8 '%".$hostname."%' USING cp1251)");
			$result = mysqli_fetch_array($s_query);
			$SiteID = $result['SitesID'];
		}
*/
	$o_site = new site();
	$SiteID = $o_site->get_sId();
    $lang = $o_page->get_sLanguage();
	
 if($SiteID>0) {
	$site_query="SELECT * FROM Sites WHERE SitesID='$SiteID'"; 
	$site_q = mysqli_query(" $site_query "); 
	$Site = mysqli_fetch_object($site_q); 
	$urls = explode(" ", $Site->url); 
	$url = $Site->primary_url; 
	if(strlen($url)<1) $url=$urls[0]; 
	$SiteTitle = htmlspecialchars(iconv('CP1251','UTF-8',substr(strip_tags($Site->Title),0,250)));
	$SiteDescription = htmlspecialchars(iconv('CP1251','UTF-8',substr(strip_tags($Site->Description),0,250)));
	echo("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">
<channel>
<atom:link href=\"http://".$o_site->_site['primary_url']."/rss.php\"  rel=\"self\" type=\"application/rss+xml\" />
	<title>$SiteTitle</title>
	<link>http://$url</link> 
	<description>$SiteDescription</description>");
    echo " <language>$lang</language>";
	list_rss_items($Site, $url, $Rub, $PRub, $keyword, $interval, $limit, $rss_n, $rss_url, $ppage);
	echo("
</channel>
</rss>
");
 }
?>
