<?php
//return the file type
function FileBaseName($address,$ret)
{
    $path_parts = pathinfo($address);
    $fdir = $path_parts["dirname"];
    $fext = $path_parts["extension"];
    $fname = str_replace(".".$fext,"",$path_parts["basename"]);
    switch ($ret)
        {
            case 'dir': return $fdir; 
                      break;
            case 'name': return $fname; 
                      break;
            case 'ext': return $fext; 
                      break;
            default: return $address;
        }
}

//default domain
$default_domain_url = "http://www.maksoft.net";

//default empty image
global $nopicture;

if(!isset($nopicture) || !(file_exists($nopicture)))
    $nopicture = "web/images/nopicture.jpg";

//get the variables picture name and directory
@$pic_name = $_GET['pic_name'];
$pic_name = htmlentities($pic_name);

if(!isset($domain_url) || $domain_url =='')
    @$domain_url = $default_domain_url;
    
@$pic_dir = $domain_url."/".$HTTP_GET_VARS['pic_dir'];

if(!isset($img_width) || empty($img_width))
    $img_width = 150;

if(!isset($image_file) || empty($image_file))
    $image_file = $pic_dir."/".$pic_name;

//if(!file_exists($image_file))
    //$image_file = $nopicture;

//ratio = strict > image width / height will be no reduced
//ratio = normal > image width / height will be reduced 
if(!isset($ratio) || empty($ratio))
    $ratio = "normal";
    
/*
if(!file_exists($domain_url."/".$image_file))
    $image_file = $nopicture;
*/  
if(!isset($img_border))
    $img_border = 0;

//botom offset
if(!isset($border_bottom))
    $border_bottom = 0; 
    
$image_extension = FileBaseName($image_file,'ext');
//die($image_extension);
//detect the image type
switch(strtolower($image_extension))
{
case 'jpg':
        $img = imagecreatefromjpeg($image_file);
        break;
case 'jpeg':
        $img = imagecreatefromjpeg($image_file);
        break;
case 'gif':
        if (imagetypes() & IMG_GIF)
            $img = imagecreatefromgif($image_file);
        else $img = imagecreatefromjpeg($image_file);
        break;

case 'png':
        if (imagetypes() & IMG_PNG)
            $img = imagecreatefrompng($image_file);
        else $img = imagecreatefromjpeg($image_file); 
        break;
}

$image_width=imagesx($img); 
$image_height=imagesy($img);

$new_image_max_size = @$img_width+2*$img_border; // max image width

if($image_width > $image_height)
{
    $relation = $image_width/$new_image_max_size;
    $new_image_width = $new_image_max_size;
    $new_image_height = $image_height/$relation;
}
else
{
    $relation = $image_height/$new_image_max_size;
    $new_image_height = $new_image_max_size;
    $new_image_width = $image_width/$relation;
    
}

//reduce by image width // height
if($ratio == "strict")
    {
        $relation = $image_width/$image_height;
        $new_image_width = $new_image_max_size;
        $new_image_height = $new_image_width/$relation;
        
        if(isset($img_height) && !empty($img_height))
            {
                $new_image_height = $img_height;
                $new_image_width = $new_image_height*$relation;
            }
    }

if($ratio == "exact")
    {
        $relation = $image_width/$image_height;
        if($relation > 1)
            {
                $new_image_width = $img_width*$relation;
                $new_image_height = $img_width;
            }
        else
            {
                $new_image_width = $img_width;
                $new_image_height = $img_width/$relation;
            }
    }
    
$new_image_width = $new_image_width+$img_border;
$new_image_height = $new_image_height+$img_border;

$new_image = imagecreatetruecolor($new_image_width, $new_image_height+$border_bottom);
#$img_border_color = imagecolorallocate($new_image, 115, 115, 115);

$trans_background = imagecolorallocatealpha($new_image, 0, 0, 0, 127);
imagefill($new_image, 0, 0, $trans_background); 
$dist_X = $img_border;
$dist_Y = $img_border;
imagecopyresampled($new_image, $img, $dist_X, $dist_Y, 0, 0, $new_image_width-(2*$img_border), $new_image_height-(2*$img_border), $image_width, $image_height);

#imagecopyresized($new_image, $img, $dist_X, $dist_Y, 0, 0, $new_image_width-(2*$img_border), $new_image_height-(2*$img_border), $image_width, $image_height);


//detect the image type
switch(strtolower($image_extension)) {
case 'jpg':
case 'jpeg':
        {
        header('Content-Type: image/jpeg');
        imagejpeg($new_image, '', $new_image_max_size);
        break;
        }
case 'gif':
        {
        if (imagetypes() & IMG_GIF)
            {
                header('Content-Type: image/gif');
                $transparent = imagecolorexact($new_image, 115, 115, 115); 
                imagecolortransparent($new_image, $transparent); 
                imagegif($new_image, '', $new_image_max_size);
            }           
        else 
            {
                header('Content-Type: image/jpeg');
                imagejpeg($new_image, '', $new_image_max_size);         
            }
        break;
        }
case 'png':
        {
        
        if (imagetypes() & IMG_PNG)
            {
            
                header('Content-Type: image/png');
                //imagejpeg($new_image, '', $new_image_max_size);
                //$transparent = imagecolorexact($new_image, 115, 115, 115); 
                //imagecolortransparent($new_image, $transparent); 
                //imagepng($new_image);
                $dest_image = imagecreatetruecolor($new_image_width, $new_image_height);
                imagesavealpha($dest_image, true);
                #$trans_background = imagecolorallocatealpha($dest_image, 0, 0, 0, 127);
                imagefill($dest_image, 0, 0, $trans_background);
                $a = $img;
                imagecopy($dest_image, $new_image, 0, 0, 0, 0, $new_image_width, $new_image_height);
                imagepng($dest_image);

                imagedestroy($a);
                imagedestroy($dest_image);

            
            }           
        else 
            {
                header('Content type: image/jpeg');
                imagejpeg($new_image, '', $new_image_max_size);         
            }
                
        break;
        }
}

?>
