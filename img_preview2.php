<?php
//return the file type
function base_name($address,$ret)
{
    $path_parts = pathinfo($address);
    $fdir = $path_parts["dirname"];
    $fext = $path_parts["extension"];
    $fname = str_replace(".".$fext,"",$path_parts["basename"]);
    switch ($ret)
        {
            case 'dir': return $fdir; 
                      break;
            case 'name': return $fname; 
                      break;
            case 'ext': return $fext; 
                      break;
            default: return $address;
        }
}

define('DEFAULT_IMAGE', "web/images/nopicture.jpg");
$img_border = 0;
$border_bottom = 0; 
$image_file = isset($_GET['image_file']) ? $_GET['image_file'] : DEFAULT_IMAGE;
$image_extension = strtolower(base_name($image_file,'ext'));
$thumb_size = $_GET['img_width'];

switch($image_extension) {
    case 'jpg':
        $img = imagecreatefromjpeg($image_file);
        break;
    case 'jpeg':
        $img = imagecreatefromjpeg($image_file);
        break;
    case 'gif':
        if (imagetypes() & IMG_GIF) {
            $img = imagecreatefromgif($image_file);
        }
        $img = imagecreatefromjpeg($image_file);
        break;
    case 'png':
        if (imagetypes() & IMG_PNG) {
            $img = imagecreatefrompng($image_file);
            break;
        }
        $img = imagecreatefromjpeg($image_file); 
        break;
    default:
        $image_file = DEFAULT_IMAGE;
        $img = imagecreatefromjpeg(DEFAULT_IMAGE);
}

$image_width  = imagesx($img); 
$image_height = imagesy($img);

$new_image_max_size = @$img_width+2*$img_border; // max image width

if($image_width > $image_height)
{
    $relation = $image_width/$new_image_max_size;
    $new_image_width = $new_image_max_size;
    $new_image_height = $image_height/$relation;
}
else
{
    $relation = $image_height/$new_image_max_size;
    $new_image_height = $new_image_max_size;
    $new_image_width = $image_width/$relation;
    
}

switch($ratio) {
    case 'strict':
        $relation = $image_width/$image_height;
        $new_image_height = $img_height;
        $new_image_width = $new_image_height*$relation;
        break;
    case 'exact':
        $relation = $image_width/$image_height;
        if($relation > 1) {
                $new_image_width = $img_width*$relation;
                $new_image_height = $img_width;
                break;
        }
        $new_image_width = $img_width;
        $new_image_height = $img_width/$relation;
        break;
    case 'square':
        header('Content type: image/jpeg');
        makeThumb($image_file);
           
}
$max_width = 500;
$max_height = 500;

function makeThumb( $filename , $thumbSize=100 ){
  global $max_width, $max_height;
 /* Set Filenames */
  $srcFile = $filename;
  $thumbFile = '';
 /* Determine the File Type */
    $type = strtolower(base_name($filename,'ext'));
 /* Create the Source Image */
  switch( $type ){
    case 'jpg' : case 'jpeg' :
      $src = imagecreatefromjpeg( $srcFile ); break;
    case 'png' :
      $src = imagecreatefrompng( $srcFile ); break;
    case 'gif' :
      $src = imagecreatefromgif( $srcFile ); break;
  }
 /* Determine the Image Dimensions */
  $oldW = imagesx( $src );
  $oldH = imagesy( $src );
 /* Calculate the New Image Dimensions */
  if( $oldH > $oldW ){
   /* Portrait */
    $newW = $thumbSize;
    $newH = $oldH * ( $thumbSize / $newW );
  }else{
   /* Landscape */
    $newH = $thumbSize;
    $newW = $oldW * ( $thumbSize / $newH );
  }
 /* Create the New Image */
  $new = imagecreatetruecolor( $thumbSize , $thumbSize );
 /* Transcribe the Source Image into the New (Square) Image */
  imagecopyresampled( $new , $src , 0 , 0 , ( $newW-$thumbSize )/2 , ( $newH-$thumbSize )/2 , $thumbSize , $thumbSize , $oldW , $oldH );
  switch( $type ){
    case 'jpg' : case 'jpeg' :
      imagejpeg($new, '', $max_height); break;         
    case 'png' :
      imagepng( $new , $thumbFile ); break;
    case 'gif' :
      imagegif( $new , $thumbFile ); break;
  }
  imagedestroy( $new );
  imagedestroy( $src );
}
?>
