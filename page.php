<?php 
require_once __DIR__.'/loader.php';
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

$di_container = $services;
ob_start();

$n = $o_page->n; 
$SiteID = $o_site->SiteID;


if($o_page->_page['url_rules'] == 3 ) {
    $redirect_url = $o_page->scheme.$o_page->_site['primary_url'];
    $response = new RedirectResponse(!empty($o_page->_site['primary_url']) ? $redirect_url : '/' , Response::HTTP_SWITCHING_PROTOCOLS);
    $response->send();
}
$Rub = '';
if(isset($_GET['Rub'])){
    $Rub = $_GET['Rub'];
}

$protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';

if($o_page->_page['url_rules'] == 1 and $protocol != $o_page->scheme) {
    $response = new RedirectResponse($o_page->get_pLink($n), Response::HTTP_FOUND);
    $response->send();
}
    
if(stristr("iframe.", $_SERVER["HTTP_HOST"]) ) {
    $templ=1; 
}
    
$url=  $_SERVER['SERVER_NAME']; 

// write STATISTICS in table stats

if (is_object($user) and !$user->WriteLevel>0)
{
    $today = date("YmdHi")."00";
    $ip = GetHostByName($REMOTE_ADDR); 
    $sesID = session_id(); 
}

    
$row = $o_page->get_page($n, $as='object');
    
if ($n>0) {
   $o_page->db_query("UPDATE pages SET preview=preview+1 WHERE n='$n' "); 
}

    
//check if mirror page does exist
$m_page = $o_page->db_query("SELECT n, an FROM pages WHERE n='$n' AND an='$row->an'");
$an = $row->an;

//save original $row variable for future use
$row_original = $row;

if ($row->an>0 && $o_page->db_count($m_page)>0) {
        eval("$row->PHPvars");  // purvo se izpulniavat promenlivite na tekushtata stranica, sled koeto i promenlivite na ogledalnata stranica
        $row = get_page($row->an);
}
    

// CHECK SiteID
if ($row->SiteID <> $SiteID) 
{
     $SiteShare_query = $o_page->db_query("SELECT * FROM SiteShare WHERE  (SiteOriginal='$row->SiteID') AND (SiteShare='$SiteID' OR SiteShare='0') "); 
    if ($o_page->db_count($SiteShare_query) > 0) {
    // DOSTUPUT do informaciata v Site SiteOriginal e razreshen na SiteShare ------------- zadava se v tablica SiteShare !!!
 } 
 else
    {
         $SiteID = $row->SiteID; 
    }
}


// USER permisions SET
// $user sledva da e setnat v login.php kato $SESSION[$user]
if ($user->ID)  {
        $user_query="SELECT * FROM  users, SiteAccess WHERE  users.ID='$user->ID' AND SiteAccess.SiteID = '$SiteID' AND SiteAccess.userID = '$user->ID' ";
        $user_result = $o_page->db_query("$user_query");
        $user=$o_page->fetch("object", $user_result);
        // CLEAN UP STATS
        // SET Start Page
        if ($n == 11) {
            //$o_page->db_query("DELETE FROM stats WHERE  logTime<=DATE_SUB(CURDATE(),INTERVAL 366 DAY);");
            $init_page_query = $o_page->db_query("SELECT * FROM SiteAccess WHERE userID='$user->userID' AND SiteID='$SiteID' ");
            $init_page_row = $o_page->fetch("object", $init_page_query);
            $n= $init_page_row->init_page;
            $row = get_page($n);
        }

/* ====== ????????? ============= */

        if ($user->WriteLevel < 1) {
            if ($n==11)  {
                $n=$Site->StartPage;
                $row = get_page($n);
            }
        }

/* ========================================= */

    }
else
    {
        $user->AccessLevel = 0;

    }

if (!(isset($user->AccessLevel)))
    $user->AccessLevel = 0;
if (!(isset($user->ReadLevel)))
    $user->ReadLevel = 0;
if ($user->WriteLevel > $user->ReadLevel)
    $user->ReadLevel = $user->WriteLevel;

// GET SITE
    /*
    if($user->ID == 196 )
    $query = $o_page->db_query("SELECT * FROM  CSSs, Sites left join image_dirs on Sites.imagesdir = image_dirs.ID WHERE Sites.SitesID = '$SiteID' AND Sites.css = CSSs.cssID");
    else
    */
    $query = $o_page->db_query("SELECT * FROM Sites, CSSs, languages WHERE Sites.SitesID = '$SiteID' AND Sites.css = CSSs.cssID AND Sites.language_id=languages.language_id");
    $Site = $o_page->fetch("object", $query);


############################################################

//MANAGE SITE VISITS
//********************************************************************************************

if ($n == $Site->StartPage)
{
    //checking IP address that opens the site
    if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    else
        $ip=$_SERVER['REMOTE_ADDR'];

    //seting cookie if still not visited
    if(!isset($_COOKIE['visit_SiteID_cookie']) || $Site->SitesID!=$_COOKIE['visit_SiteID_cookie'] || $ip==$HOST_IP)
        {
            $visited = false;
            //seting cookie for 24 hours (86400 seconds)
            setcookie ("visit_SiteID_cookie", $Site->SitesID, time()+86400);

        }
    // if visited already
    else $visited = true;

    if ($hostCMSquery=$o_page->db_query("SELECT * FROM hostCMS WHERE SiteID='$SiteID' AND active='1' ORDER BY Date DESC ")) {
        $hostCMSrow=$o_page->fetch("object", $hostCMSquery);
        $hostVisits =  $hostCMSrow->Visits;
    } else {
        $hostVisits=0;
    }

    $Visits = $Site->Visits;

    if ( $ip != $Site->last_visit  && !$visited)
        {
            $Visits++;
            $hostVisits++;
            $query = "UPDATE Sites SET Visits = '$Visits',  last_visit ='$ip'  WHERE SitesID = '$Site->SitesID'  ";
            $o_page->db_query("$query");

            if ($hostVisits>0) {
             $o_page->db_query("UPDATE hostCMS  SET Visits = '$hostVisits'  WHERE hostCMSid = '$hostCMSrow->hostCMSid' ");
            }
        }


}
        
$tablet_browser = (int) $detect->isTablet();
$mobile_browser = (int) ($detect->isMobile() && !$detect->isTablet());

// Site is EXPIRED
if( ($o_page->_site['site_status']<1) ) {
    if (($user->AccessLevel>0) && ($o_page->_site['language_id']<2)) {
         if($o_page->_site['days_left']<-30) $templ = 244; // expired
    }
}



//INIT GLOBAL VARIABLES

        //init template vars
        if(!empty($Template->tmpl_vars))
            eval($Template->tmpl_vars);
        //if($user->ID == 196) print_r ("<div class=\"message_normal\">tmpl vars: <br>".$Template->tmpl_vars."</div>");

        //init site vars
        if(!empty($Site->initPHP))
            eval("$Site->initPHP");


        //init page vars
        //eval("$row->PHPvars");
        eval($o_page->_page['PHPvars']);

        if($Rub<>'')  {
            $o_page->Rub = $Rub;
        } else {
            $o_page->Rub =  $o_page->_site['Rub'];
        }


        //$o_page->print_pPHPvars();

        //if($user->ID == 196) echo ("<div class=\"message_normal\">page vars: <br>".$row->PHPvars."</div>");


    $o_page->url_etiketi = $url_etiketi;


//GET TEMPLATE  VAR $templ
    if (isset($templ)  )
    {
        //check for existing template
        $tmpl_query = $o_page->db_query("SELECT * FROM Templates t left join CSSs c on t.default_css = c.cssID WHERE t.ID='$templ' ");
        $preview_templ = $o_page->fetch("array", $tmpl_query);
        if($o_page->db_count($tmpl_query) ==0)
            $templ=1;
        $Site->Template = "$templ";
        $TemplateStr = "&templ=$templ";
        $Site->cssDef = "";
        $Site->css_url = $preview_templ['css_url'];
     }
     else $TemplateStr="";

//READ SITE TEMPLATE
    $template_query = $o_page->db_query("SELECT * FROM Templates t left join CSSs c on t.default_css = c.cssID WHERE t.ID = '$Site->Template' ");
    $Template = $o_page->fetch("object", $template_query);

    if (($mobile_browser > 0) && ($Template->mobID > 0)) {
        $mob_template_query = $o_page->db_query("SELECT * FROM Templates t left join CSSs c on t.default_css = c.cssID WHERE t.ID = '$Template->mobID' ");
        $Template = $o_page->fetch("object", $mob_template_query);
    }



// ***
//PAGE TITLE
$Title_arr = str_split(" ", $Site->Title);
if ($row_original->ParentPage<>$Site->StartPage) {
 $row_original_parent = $o_page->get_page($row_original->ParentPage, 'object');
 if (strlen($row_original_parent->Name)<16) {
  $Title_parent = "$row_original_parent->Name&raquo; ";
 }
}
$Title = "$Site->Title &raquo; $Title_parent $row_original->Name";
if (strlen($Title)>140) {
 $Title="$row_original->Name &raquo; $Title_arr[0]&nbsp$Title_arr[1]&nbsp$Title_arr[2] &raquo; $Title_parent &nbsp ";
}
if ($row_original->title<>"") {
    $Title = $row_original->title;
}

$Title=$o_page->get_pTitle("full");


//PAGE NAME
$Name = $row_original->Name;

//PARENT PAGE
$Parent = $row_original->ParentPage;

//NAVIGATION BAR
if($Site->Home != $Name)
    $nav_bar = "<a href=\"#top\" class='nav_links' title='".$Name."'>$Name</a>";
if (strlen($user->ID)>0)
     $nav_bar .= " | <b>$user->Name </b> | <a href=\"/web/admin/logout.php\" class='nav_links'>ecoia</a>";
    // $nav_bar .= " | <b>$user->Name </b> | <a href=\"http://www.maksoft.net/web/admin/logout.php\" class='nav_links'>ecoia</a>";
    // $nav_bar .= " | <b>$user->Name </b> | <a href=\"".$o_page->USER_LOGOUT_LINK."\" class='nav_links'>ecoia</a>";

$navigation_links = "";

if ($user->ID == 1) { $userSiteAcces_str = "SELECT * FROM SiteAccess WHERE SiteID = '$SiteID' AND userID = '$user->ID' ";   }

if ( ($user->WriteLevel >= $row_original->SecLevel)  && ($row_original->SiteID == $SiteID)  && $user->WriteLevel >0 )
{
    $edit_page_str  = "  | <a href=\"/page.php?n=11&SiteID=$SiteID\" class='nav_links'>???????</a> [ <i> <a href=\"/page.php?n=111&amp;ParentPage=$row_original->n&SiteID=$Site->SitesID$TemplateStr \" class='nav_links'><img src='http://maksoft.net/web/images/add_icon.gif' border=0 align=middle> aiaaae</a>  |  <a href=\"/page.php?n=132&amp;no=$row_original->n&amp;SiteID=$Site->SitesID$TemplateStr \" class='nav_links'> <img src='http://maksoft.net/web/images/edit_icon.gif' border=0 align=middle> ?aaaeoe?ae</a> </i>]";
    } else {
        $edit_page_str  = "";
    }

if ($row_original->n != $Site->StartPage)
    $back = "<a href=\"javascript: history.go(-1)\" class=\"next_link\">$Site->Back </a>";
else $back = "";


$top = "<a href=\"#top\" class=\"next_link\">$Site->top</a>";
$forward = "<a href=\"javascript: history.go(1)\" class=\"next_link\">$Site->forward </a>";
//back-top-next navigation
define("GOTO_NAVIGATION" ,"$back | $top | $forward ");
define("LINKS_NAVIGATION" ,"<div id=\"nav_links\"> $navigation_links </div>");
// $nav_bar = "<a href=\"/page.php?n=$Site->StartPage&SiteID=$SiteID\" target=\"_parent\">$Site->Home /</a> $nav_bar  $edit_page_str <a href=\"#links\"> >>>   </a>";
$nav_bar='';
$nav_bar = "<div id=\"nav_links\"><a href=\"/page.php?n=$Site->StartPage&amp;SiteID=$SiteID\" class=\"nav_links\" title=\"".$Site->Home."\">$Site->Home &raquo;</a> $nav_bar  $edit_page_str</div>";
$nav_bar_cms = $nav_bar; //copy in case $nav_bar is user for something else

$nav_bar = $o_page->get_pNavigation();
    $url=  $_SERVER['SERVER_NAME']; 
    $exist = site_ok($url, $row, $SiteID); 
    if(($exist>0) || ($user->AccessLevel>0) ) 
        {   
            // SHOW DATABASE PAGE CONTENT
            $o_page->add_keys();
            if(($o_site->_site['netservice']>0)  &&  ($user->AccessLevel==0)) {
                $o_page->add_stats($o_page->n, $o_page->ParentPage);
            }
            include("$Template->URL");

        }
    //redirect if site is missing
    else  {
        if($o_page->n > 0) include("no_site.html");
    //header("Location: /no_site.html");
    }

    $o_page->sl_work_add();

    if(!isset($o_page->_user['ID'])){
        $user_id = 0;
    } else {
        $user_id = $o_page->_user['ID'];
    }
    $httpReferer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
    $page = ob_get_contents();
    ob_end_clean();
// cache for 3600 seconds
if(!$response instanceof Response) {
    $response = new Response();
}

$response->setSharedMaxAge(3600);

// (optional) set a custom Cache-Control directive
$response->headers->addCacheControlDirective('must-revalidate', true);
$response->setStatusCode(Response::HTTP_OK);
$response->setCharset('cp1251');
$response->setContent($o_page->replace_shortcodes($page));
$response->prepare($request);
$response->send();
session_write_close();
?>
