<?php
//return a drop down menu with offer types
function list_offer_type($name = "Vid", $selected = 0, $params = "")
	{

		//array of types
		$offer_types = array(array("Sell", "�������"), array("Offer", "���� ��� ����"), array("Buy", "������"), array("Seek", "����� ��� ����"));
		$content = "<select name=\"".$name."\" $params>"; 			
		for($i=0; $i<count($offer_types); $i++)
			{
				if ($offer_types[$i][0] == $selected)
					$content.= "<option value=\"".$offer_types[$i][0]."\" selected>".$offer_types[$i][1]."</option>";
				else 
					$content.= "<option value=\"".$offer_types[$i][0]."\">".$offer_types[$i][1]."</option>";
			}
		$content.= "</select>"; 
		
		return $content;	
	}

//return a drop down menu with imot types, PRub
function list_imot_type($name = "PRub", $selected = 0, $params = "")
	{	
		
		$query = "SELECT * FROM Imoti"; 
		$result = mysqli_query($query); 
		$content = "<select name=\"".$name."\" $params>"; 			
		while ($PRub_row = mysqli_fetch_array($result)) 
			{
				if ($PRub_row['PRubV'] == $selected)
					$content.= "<option value=\"".$PRub_row['PRubV']."\" selected>".$PRub_row['PRubText']."</option>";
				else 
					$content.= "<option value=\"".$PRub_row['PRubV']."\">".$PRub_row['PRubText']."</option>";
			}
		$content.= "</select>"; 
	return $content;
	}

//return a drop down menu with imot cities
function list_cities($name = "Region", $selected = 0, $params = "")	
	{
		?>
          <select name="<?=$name?>">
            <option value="1000 �����" selected>�����</option>
			<option value="����� ������">����� ������</option>
            <option value="4000 �������">�������</option>
            <option value="9000 �����">�����</option>
            <option value="8000 ������">������</option>
            <option value="7000 ����">����</option>
            <option value="6000 ����� ������">����� ������</option>
            <option value="5000">�. �������</option>
            <option value="5800 ������">������</option>
            <optgroup label="---------------------"></optgroup>
            <option value="4230 ���������� ������">����������</option>
            <option value="2700">�����������</option>
            <option value="4600 ���������� ������">���������</option>
            <option value="3700 �����">�����</option>
            <option value="3000 �����">�����</option>
            <option value="5100 ������ ������� � ��������">�. ���������</option>
            <option value="5300">�������</option>
            <option value="6400">������������</option>
            <option value="9300 ������">������</option>
            <option value="2600 �������">�������</option>
            <option value="6100 ��������">��������</option>
            <option value="4700 ���������� ������">�������</option>
            <option value="6600 ��������">��������</option>
            <option value="2500 �������� ������">���������</option>
            <option value="5500 �����">�����</option>
            <option value="3400 �������">�������</option>
            <option value="4400 ���������">���������</option>
            <option value="2100 �������� ������">������</option>
            <option value="2600 ������">������</option>
            <option value="7200 �������">�������</option>
            <option value="2000 �������� ������">�������</option>
            <option value="5250 ������ �.�������">������</option>
            <option value="5400 ������ �.�������">��������</option>
            <option value="7500 ��������">��������</option>
            <option value="8800 �������� ������">������</option>
            <option value="4700 ���������� ������">������</option>
            <option value="7700 ���������">���������</option>
            <option value="6300 �������">�������</option>
            <option value="9700 �����">�����</option>
            <option value="8600 �����">�����</option>
          </select>
		<?php	
	}

//return a drop down menu with imot regions
function list_regions($name = "RegionImot", $selected = 0, $params = "")	
	{
			$content = "<select name=\"".$name."\">";
			$query = mysqli_query("SELECT * FROM parts ");
			while ($part = mysqli_fetch_object($query)) 
			{
			  	$content.="<optgroup label=\"".$part->Name."\">";
			  	$result = mysqli_query("SELECT * FROM  RegCodes WHERE part='".$part->partID."' ORDER BY Region ASC ");	
			  	while ($reg_row = mysqli_fetch_object($result)) 
				{
					if ($reg_row->Code == $selected)
						$content.="<option value=\"".$reg_row->Region."\" selected>".$reg_row->Region."</option>";
					else
						$content.="<option value=\"".$reg_row->Region."\">".$reg_row->Region."</option>";
				}
			}
			$content.="</select>";
			
			return $content;	
	}
	
function etaj_show($var_name, $default, $user)
	{
	  if ($user->AccessLevel > 0)
		{
			echo "<select class=dropdown name=\"$var_name\">"; 
			for ($i=0; $i<=22; $i++)
				{
					if ($i == $default)
						$selected = "selected";
					else $selected = "";
				echo "<option value=\"$i\" $selected>$i </option>"; 
				}
			echo "</select>"; 
		  
		  }
	}

function dir_empty($directory)
{
	$number_of_files = 0;
	if ($handle = opendir($directory)) 
	{ 
		while (false !== ($file = readdir($handle))) 
			if ($file != "." && $file != "..") $number_of_files++; 
		
		closedir($handle); 
	} 
	
	if($number_of_files == 0) return true;
	else return false;
}	
?>