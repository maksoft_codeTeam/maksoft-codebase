<?php
if( class_exists('wwo') && ((strlen($wwo->city)>=3) || (strlen($country)>3) || (strlen($areaName)>3) || (strlen($region)>3)) ) {  //
?>
<link rel="stylesheet" href="/wwo/webfont/climacons-font.css" />
<style>
	.climacon {font-size: 30px; }
</style>
<?php
include_once("lib/lib_wwo.php"); 

if(!(is_object($wwo))) {
	$wwo = new wwo();
}

$xml = $wwo->get_weather($wwo_city); 

$lat = $xml->latitude;   // North
$long = $xml->longitude;    // East
$offset = $xml->utcOffset;    // difference between GMT and local time in hours

$today = getdate(); 
$sun_info = date_sun_info(time(), $wwo->latitude, $wwo->longitude);
$sunrise = getdate($sun_info['sunrise']);
$sunset = getdate($sun_info['sunset']);
//echo "---".$sunrise['hours']."---".$sunset['hours']."---".$today['hours']; 

$day_night = "day_sm"; 
//if($today['hours']<$sunrise['hours']) $day_night = "night_sm"; 
//if($today['hours']>$sunset['hours']) $day_night = "night_sm"; 

$w_days = array("������","����������","�������","�����","���������", "�����", "������"); 

?>
<div itemprop="location">  <span itemscope itemtype="http://schema.org/Place">
    <div itemprop="geo">
      <span itemscope itemtype="http://schema.org/GeoCoordinates">
        <span property="latitude" content="<?php echo $lat ?>"></span>
        <span property="longitude" content="<?php echo $long ?>"></span>
      </span>
    </div>
  </span>
</div>
<?php
  }   //  strlen($wwo_city)>3) 
?>
<table width="100%" border="0">
  <tr>
 <?php
  	for($i=1;$i<=$wwo->num_of_days-1;$i++) {
 ?>
    <td><div align="center"><img src="http://cdn.worldweatheronline.net/images/weather/small/<?php echo $xml->weather[$i]->hourly[3]->weatherCode."_$day_night"; ?>.png"></div>
	<?php
	printf("%s <span class=\"climacon thermometer medium-high\" aria-hidden=\"true\"></span> %s | %s �C ",date(date(), "D"),$xml->weather[$i]->mintempC,$xml->weather[$i]->maxtempC);
	?>
	</td>
 <?php
  	} // for end
 ?>
  </tr>
</table>
