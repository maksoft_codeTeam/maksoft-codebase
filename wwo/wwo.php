<?php
if( ($wwo->active) && (strlen($wwo->city)>=3) || (strlen($country)>3) || (strlen($areaName)>3) || (strlen($region)>3) ) {  //
$color="".WWO_BACKGROUNDCOLOR."";
?>
<link rel="stylesheet" href="/wwo/webfont/climacons-font.css" />
<style>
	.climacon {font-size: 30px; }
	.background-color {background-color: <?=$color?>;}
</style>
<div class="background-color">
<?php
<?php
include_once("lib/lib_wwo.php");

if(!(is_object($wwo))) {
	$wwo = new wwo();
}

$xml = $wwo->get_weather($wwo_city ? $wwo_city : $wwo->city); 

$lat = $xml->latitude;   // North
$long = $xml->longitude;    // East
$offset = $xml->utcOffset;    // difference between GMT and local time in hours

$today = getdate(); 
$sun_info = date_sun_info(time(), $wwo->latitude, $wwo->longitude);
$sunrise = getdate($sun_info['sunrise']);
$sunset = getdate($sun_info['sunset']);
//echo "---".$sunrise['hours']."---".$sunset['hours']."---".$today['hours']; 

$day_night = "day_sm"; 
if($today['hours']<$sunrise['hours']) $day_night = "night_sm"; 
if($today['hours']>$sunset['hours']) $day_night = "night_sm"; 

$w_days = array("������","����������","�������","�����","���������", "�����", "������"); 

?>
<div itemprop="location">
  <span itemscope itemtype="http://schema.org/Place">
    <div itemprop="geo">
      <span itemscope itemtype="http://schema.org/GeoCoordinates">
        <span property="latitude" content="<?php echo $lat ?>"></span>
        <span property="longitude" content="<?php echo $long ?>"></span>
      </span>
    </div>
   </span>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><div align="center">
      <h2>������� <br>
        <?php if($xml->population>0) {echo " � "; } else { echo(" �� "); }?><br>
        <span itemprop="name"><?php echo $wwo->city; ?></span></h2>
    </div></td>
  </tr>
  <tr>
    <td><div align="center"><img src="http://cdn.worldweatheronline.net/images/weather/small/<?php echo $xml->current_condition->weatherCode."_$day_night"; ?>.png"></div></td>
  </tr>
  <tr>
    <td><div align="center">
      <?php
	printf("<h2><span class=\"climacon thermometer medium-high\" aria-hidden=\"true\"></span> %s  �C </h2>",$xml->current_condition->temp_C);
	?>
    </div></td>
  </tr>
  <tr>
    <td><div align="center"><?php echo $wwo->get_condition($xml->current_condition->weatherCode);  ?>
		</div></td>
  </tr>
  <tr>
    <td><div align="center">
      <?php
			printf("<p><span class=\"climacon wind cloud\" aria-hidden=\"true\"></span>����� %s km/�</p>", 
			$xml->current_condition->windspeedKmph);
		?><p></p>
	</div></td>
  </tr>
	<?php
	for($i=1;$i<$wwo->num_of_days;$i++) {
		echo('
		   <tr>
		<td><div align="center">
		'); 
		printf("%s <span class=\"climacon thermometer medium-high\" aria-hidden=\"true\"></span> %s | %s �C ",$xml->weather[$i]->date,$xml->weather[$i]->mintempC,$xml->weather[$i]->maxtempC);
        //if($o_page->n == 19326052) {echo print_r($xml->weather[$i]);echo("<br><br><br><br><br>");}
		echo('
		</div></td>
		  </tr>
		'); 
	} // for 
	
	if($wwo->marine->weather->hourly->waterTemp_C > 0) {   // waterTemp_C > 0
	?>
	  <tr>
		<td><div align="center">
		  <h3>����������� �� ������ </h3>
		</div>
	   </td>
	  </tr>
	  <tr>
		<td><div align="center">
		   <img src="/wwo/images/water_temperature.png" width="25" ><?php
			printf("<h2></span> %s  �C </h2>",$wwo->marine->weather->hourly->waterTemp_C);
		?>
	 </div></td>
	  </tr>
	 <?php
	} //waterTemp_C > 0
?>
</table>
</div>
</div>
<?php
  }   //  wwo->active     strlen($wwo_city)>3) 
?>
