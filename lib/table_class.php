<?php
/*
  $Id: boxes.php,v 1.33 2003/06/09 22:22:50 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
 
   class tableBox {
    var $table_border = '0';
    var $table_width = '300';
    var $table_cellspacing = '0';
    var $table_cellpadding = '0';
    var $table_parameters = '';
    var $table_row_parameters = '';
    var $table_data_parameters = '';

// class constructor
    function __construct($contents, $direct_output = false) {
      $tableBox_string = '<table border="' . ($this->table_border) . '" width="' . ($this->table_width) . '" cellspacing="' . ($this->table_cellspacing) . '" cellpadding="' . ($this->table_cellpadding) . '"';
      if (!empty($this->table_parameters)) $tableBox_string .= ' ' . $this->table_parameters;
      $tableBox_string .= '>' . "\n";

      for ($i=0, $n=sizeof($contents); $i<$n; $i++) {
        if (isset($contents[$i]['form']) && !empty($contents[$i]['form'])) $tableBox_string .= $contents[$i]['form'] . "\n";
        $tableBox_string .= '  <tr';
        if (!empty($this->table_row_parameters)) $tableBox_string .= ' ' . $this->table_row_parameters;
        if (isset($contents[$i]['params']) && !empty($contents[$i]['params'])) $tableBox_string .= ' ' . $contents[$i]['params'];
        $tableBox_string .= '>' . "\n";

        if (isset($contents[$i][0]) && is_array($contents[$i][0])) {
          for ($x=0, $n2=sizeof($contents[$i]); $x<$n2; $x++) {
            if (isset($contents[$i][$x]['text']) && !empty($contents[$i][$x]['text'])) {
              $tableBox_string .= '    <td';
              if (isset($contents[$i][$x]['align']) && !empty($contents[$i][$x]['align'])) $tableBox_string .= ' align="' . ($contents[$i][$x]['align']) . '"';
              if (isset($contents[$i][$x]['params']) && !empty($contents[$i][$x]['params'])) {
                $tableBox_string .= ' ' . $contents[$i][$x]['params'];
              } elseif (!empty($this->table_data_parameters)) {
                $tableBox_string .= ' ' . $this->table_data_parameters;
              }
              $tableBox_string .= '>';
              if (isset($contents[$i][$x]['form']) && !empty($contents[$i][$x]['form'])) $tableBox_string .= $contents[$i][$x]['form'];
              $tableBox_string .= $contents[$i][$x]['text'];
              if (isset($contents[$i][$x]['form']) && !empty($contents[$i][$x]['form'])) $tableBox_string .= '</form>';
              $tableBox_string .= '</td>' . "\n";
            }
          }
        } else {
          $tableBox_string .= '    <td';
          if (isset($contents[$i]['align']) && !empty($contents[$i]['align'])) $tableBox_string .= ' align="' . ($contents[$i]['align']) . '"';
          if (isset($contents[$i]['params']) && !empty($contents[$i]['params'])) {
            $tableBox_string .= ' ' . $contents[$i]['params'];
          } elseif (!empty($this->table_data_parameters)) {
            $tableBox_string .= ' ' . $this->table_data_parameters;
          }
          $tableBox_string .= '>' . $contents[$i]['text'] . '</td>' . "\n";
        }

        $tableBox_string .= '  </tr>' . "\n";
        if (isset($contents[$i]['form']) && !empty($contents[$i]['form'])) $tableBox_string .= '</form>' . "\n";
      }

      $tableBox_string .= '</table>' . "\n";

      if ($direct_output == true) echo $tableBox_string;

      return $tableBox_string;
    }
  }

  class infoBox extends tableBox {
    function __construct($contents) {
      $info_box_contents = array();
      $info_box_contents[] = array('text' => $this->infoBoxContents($contents));
      $this->table_cellpadding = '1';
      $this->table_parameters = 'class="infoBox"';
      $this->tableBox($info_box_contents, true);
    }

    function infoBoxContents($contents) {
      $this->table_cellpadding = '3';
      $this->table_parameters = 'class="infoBoxContents"';
      $info_box_contents = array();
      $info_box_contents[] = array(array('text' => ''));
      for ($i=0, $n=sizeof($contents); $i<$n; $i++) {
        $info_box_contents[] = array(array('align' => (isset($contents[$i]['align']) ? $contents[$i]['align'] : ''),
                                           'form' => (isset($contents[$i]['form']) ? $contents[$i]['form'] : ''),
                                           'params' => 'class="boxText"',
                                           'text' => (isset($contents[$i]['text']) ? $contents[$i]['text'] : '')));
      }
      $info_box_contents[] = array(array('text' => ''));
      return $this->tableBox($info_box_contents);
    }
  }

  class infoBoxHeading extends tableBox {
    function __construct($contents, $left_corner = true, $right_corner = true, $right_arrow = false) {
      $this->table_cellpadding = '0';

      if ($left_corner == true) {
        $left_corner = "<img src='corner_left.gif'>";
      } else {
        $left_corner = "";
      }
      if ($right_arrow == true) {
        $right_arrow = '<a href="#"><img src="arrow_right.gif" border=0></a>';
      } else {
        $right_arrow = '';
      }
      if ($right_corner == true) {
        $right_corner = $right_arrow . "<img src='corner_right.gif'>";
      } else {
        $right_corner = $right_arrow . "";
      }

      $info_box_contents = array();
      $info_box_contents[] = array(array('params' => 'height="14" class="infoBoxHeading"',
                                         'text' => $left_corner),
                                   array('params' => 'width="100%" height="14" class="infoBoxHeading"',
                                         'text' => $contents[0]['text']),
                                   array('params' => 'height="14" class="infoBoxHeading" nowrap',
                                         'text' => $right_corner));

      $this->tableBox($info_box_contents, true);
    }
  }

  class contentBox extends tableBox {
    function __construct($contents) {
      $info_box_contents = array();
      $info_box_contents[] = array('text' => $this->contentBoxContents($contents));
      $this->table_cellpadding = '1';
      $this->table_parameters = 'class="infoBox"';
      $this->tableBox($info_box_contents, true);
    }

    function contentBoxContents($contents) {
      $this->table_cellpadding = '4';
      $this->table_parameters = 'class="infoBoxContents"';
      return $this->tableBox($contents);
    }
  }

  class contentBoxHeading extends tableBox {
    function __construct($contents) {
      $this->table_width = '100%';
      $this->table_cellpadding = '0';

      $info_box_contents = array();
      $info_box_contents[] = array(array('params' => 'height="14" class="infoBoxHeading"',
                                         'text' => 'X'),
                                   array('params' => 'height="14" class="infoBoxHeading" width="100%"',
                                         'text' => $contents[0]['text']),
                                   array('params' => 'height="14" class="infoBoxHeading"',
                                         'text' => 'X'));

      $this->tableBox($info_box_contents, true);
    }
  }

  class errorBox extends tableBox {
    function __construct($contents) {
      $this->table_data_parameters = 'class="errorBox"';
      $this->tableBox($contents, true);
    }
  }

  class productListingBox extends tableBox {
    function __construct($contents) {
      $this->table_parameters = 'class="productListing"';
      $this->tableBox($contents, true);
    }
  }

 
 
 
 
 
 
 
 class innerTable
{ 

  var $t_title;
  var $t_children;
  var $t_childlist;
  var $t_depth;

  function treenode($t_title)
  { 
 
    $this->t_title = $title;
    $this->t_children =$children;
    $this->t_childlist = array();
    $this->t_depth = $depth;

    // we only care what is below this node if it 
    // has children and is marked to be expanded
    // sublists are always expanded
	$view_sub_tables = true;
	
    if($view_sub_tables)
    {


      for ($count=0; $row = 5; $count++)
      {
        if($sublist||$expanded[ $row['postid'] ] == true)
          $expand = true;
        else
          $expand = false;
        $this->m_childlist[$count]= new treenode($row['postid'],$row['title'],
                                      $row['poster'],$row['posted'],
                                      $row['children'], $expand,
                                      $depth+1, $expanded, $sublist);
      }
    }
  }  


  function display_table_table($row, $sublist = false)
  {
    // as this is an object, it is responsible for display_tableing itself

    // $row tells us what row of the display_table we are up to 
    // so we know what color it should be

    // $sublist tells us whether we are on the main page
    // or the message page.  Message pages should have 
    // $sublist = true.  
    // On a sublist, all messages are expanded and there are

    // no "+" or "-" symbols.

    // if this is the empty root node skip display_tableing
    if($this->m_depth>-1)  
    {
      //color alternate rows
      echo '<tr><td bgcolor = ';
      if ($row%2) 
        echo "'#cccccc'>";
      else
        echo "'#ffffff'>";

      // indent replies to the depth of nesting
      for($i = 0; $i<$this->m_depth; $i++)
      {
        echo "<img src = 'images/spacer.gif' height = 22
                          width = 22 alt = '' valign = bottom>";
      }
  
      // display_table + or - or a spacer 
      if ( !$sublist && $this->m_children && sizeof($this->m_childlist))
      // we're on the main page, have some children, and they're expanded 
      {
        // we are expanded - offer button to collapse
        echo "<a href = 'index.php?collapse=".
                         $this->t_postid."#$this->t_postid'
             ><img src = 'images/minus.gif' valign = bottom 
             height = 22 width = 22 alt = 'Collapse Thread' border = 0></a>";
      }
      else if(!$sublist && $this->m_children)
      {
        // we are collapsed - offer button to expand
        echo "<a href = 'index.php?expand=".
             $this->t_postid."#$this->t_postid'><img src = 'images/plus.gif'  
             height = 22 width = 22 alt = 'Expand Thread' border = 0></a>";
      }
      else
      {
        // we have no children, or are in a sublist, do not give button
        echo "<img src = 'images/spacer.gif' height = 22 width = 22 
                   alt = ''valign = bottom>";
      }


      echo " <a name = $this->t_postid ><a href = 
             'view_post.php?postid=$this->t_postid'>$this->t_title - 
              $this->t_poster - ".reformat_date($this->m_posted).'</a>';
      echo '</td></tr>';

      // increment row counter to alternate colors
      $row++;
    }
    // call display_table on each of this node's children
    // note a node will only have children in its list if expanded
    $num_children = sizeof($this->m_childlist);
    for($i = 0; $i<$num_children; $i++)
    {
      $row = $this->m_childlist[$i]->display_table($row, $sublist);
    }
    return $row;
  }

};
 
?>
