<?php
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_seo >> lib_site >> lib_page >> lib_cms
 require_once('lib_seo.php');

// constructor
class site extends seo
{
    static $sHome;
    static $sID;
    static $sStartPage;
    static $uWriteLevel;
    static $uReadLevel;
    static $sMAIN_WIDTH;
    public static $INPUT_ENCODING='cp1251'; 
    public static $OUTPUT_ENCODING='cp1251'; 
    public $SiteID;
    public $StartPage;
    public $referer; // the referer string
    public $search_engine; // the name of  a refering the search engine
    public $keys; // keywords the site is reached
    public $sep;  // Holds the query & keyword seperator
    public $time_of_entry;
    public $user_ip; 
    public $url;  // The host url address
    public $uri; //  The filename of the currently executing script, relative to the document root   $_SERVER['PHP_SELF']
    public $q_string;  // ���������� ���, ��� ��� �����, � ����� � ��� �������� ������ �� ����������. $_SERVER['QUERY_STRING']
    public $primary_url = '';

    public $_site; // get_site array 
    public $_templ; // template array 

    protected static $ssl_cert;

    protected static $pages = array();
 
  
   // site( [int site identification number; int site number; ] )
    // constructor 
    function __construct($SiteID=null)
    {
        parent::__construct(); 
        
        if ($_SERVER['HTTP_REFERER'] OR $_ENV['HTTP_REFERER']) {
            $this->referer = urldecode(($_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $_ENV['HTTP_REFERER']));
            $this->sep = (preg_replace('#(\?q=|\?qt=|\?p=)#i', $this->referer)) ? '\?' : '\&';
        }

        $this->time_of_entry = date("YmdHis");
        $this->user_ip =  ip2long(GetHostByName($_SERVER['REMOTE_ADDR']));
        $this->url = $_SERVER['SERVER_NAME'];    //HTTP_HOST
        $this->uri = $_SERVER['PHP_SELF'];
        $this->q_string = $_SERVER['QUERY_STRING']; 
          

        if (is_numeric($SiteID)) {
            $this->SiteID = $SiteID; 
        } else {
            $this->SiteID = $this->getSiteIdFromEnvironment();
        }
         
        $this->_site = $this->get_site($this->SiteID); 
        
        $this->StartPage = $this->_site['StartPage'];
        
        site::$sHome = $this->_site['Home'];
        site::$sID = $this->_site['SitesID'];
        site::$sStartPage = $this->_site['StartPage'];
        site::$uWriteLevel = (int)$this->get_site_permissions("WriteLevel");
        site::$uReadLevel = (int)$this->get_site_permissions("ReadLevel");
        site::$sMAIN_WIDTH = $this->get_sConfigValue("CMS_MAIN_WIDTH");

        if(site::$sMAIN_WIDTH == '100%') {
           site::$sMAIN_WIDTH = 950;
        }
           
        if(site::$sMAIN_WIDTH == 0) {
            site::$sMAIN_WIDTH = NULL;
        }
    }  

    public function getSiteIdFromEnvironment()
    {
        if(isset($_GET['SiteID'])) {
            return $_GET['SiteID'];
        }
        
        if(isset($_POST['SiteID'])) {
            return $_POST['SiteID'];
        }

        $url = $_SERVER['SERVER_NAME']; 
        $site_url_q = $this->db_query("SELECT SitesID FROM Sites WHERE `url` LIKE '%$url%' ORDER BY SitesID ASC LIMIT 1"); 

        if ($this->count($site_url_q)>0) {
            $site = $this->fetch("object", $site_url_q); 
            return $site->SitesID; 
        }

        return null;
    }

    public function get_sLanguageByCode($code)
    {
        $res = $this->db_query("SELECT * FROM languages where language_key='$code'");
        $language = $this->fetch('array', $res);
        return $language;
    }

    public static function ssl_active($ssl)
    {
        if(!$ssl){
            return false;
        }

        $now = time();

        $start_date = strtotime($ssl->start_date);
        $end_date = strtotime($ssl->expire_date);

        return $start_date > $now or $end_date < $now ? false : true; 
    }
    
    //search for user permisions on that site
    function get_site_permissions($return)
    {
        $query = $this->db_query("SELECT SQL_CACHE * FROM SiteAccess WHERE userID= '".user::$uID."' AND SiteID = '".$this->SiteID."' LIMIT 1"); 
        $permissions = $this->fetch("array", $query); 
        return $permissions[$return];   
    }

    function get_ssl()
    {
        if(self::$ssl_cert) {
            return self::$ssl_cert;
        }

        $domain =  $this->get_domain();
        $subdomain = sprintf('%s%s', $this->get_subdomain(), $domain);
        $sql = "SELECT * FROM `ssl_certs` WHERE (domain = '$domain' OR domain ='$subdomain')";
        self::$ssl_cert = $this->fetch("object", $this->db_query($sql)); 
        return self::$ssl_cert;
    }

    function get_domain()
    {
        return preg_replace('#^(?:.+?\\.)+(.+?\\.(?:co\\.uk|com|net|biz|eu|bg|gr|de))#', '$1', $_SERVER['HTTP_HOST']);
    }

    function get_subdomain()
    {
        return str_replace($this->get_domain(),'' ,$_SERVER['HTTP_HOST']);
    }

    public function debug_cli()
    {

        echo '<pre><code>';
        print_r(var_export(func_get_args()));
        echo '</code></pre>';
    }

    public function is_admin()
    {
        return in_array($this->_user['ID'], array(1,1419,1424));
    }

    public function debug()
    {
        if(!$this->is_admin() or self::$sID==1047){
            return;
        }
        echo '<script>';
        echo 'console.log('. json_encode(func_get_args()) .')';
        echo '</script>';
    }
    
        
    //return site array
    function get_site($SiteID = NULL)
    {
        if(!$SiteID) {
            $SiteID = $this->SiteID;
        }

        $query = $this->db_query("
            SELECT * FROM Sites
            LEFT JOIN hostCMS ON Sites.SitesID = hostCMS.SiteID
            WHERE SitesID = '$SiteID' 
            AND hostCMS.active>0
            ORDER BY hostCMS.hostCMSid DESC  LIMIT 1"); 
        if($this->count($query)==0) {
            $query = $this->db_query("SELECT * FROM Sites  WHERE SitesID = '$SiteID'  LIMIT 1"); 
        }

        $row = $this->fetch("array", $query); 
        
        $row['days_left'] = 0; 

        return $row; 
    }
    
    //return site configurations
    function get_sConfigurations($SiteID = NULL)
    {
        if(!$SiteID) $SiteID = $this->SiteID;
        $c_query = $this->db_query("SELECT * FROM configuration WHERE SiteID = '".$SiteID."'");
        $i=0;
        while($c_array = $this->fetch("array", $c_query))
            $conf[$i++] = $c_array;
        return $conf;
    }
    
    //return the site configuration value; 
    //get_sConfigValue("MAIN_WIDTH") returns the site main width (if defined)
    function get_sConfigValue($return, $SiteID=NULL)
    {
        $value = NULL;
        $config_values = $this->get_sConfigurations($SiteID);
        for($i=0; $i<count($config_values); $i++)
            if($config_values[$i]['conf_key'] == $return)  
                $value = $config_values[$i]['conf_value'];
        return $value;
    }
    
    //print site configurations
    function print_sConfigurations($SiteID = NULL)
        {   
            $conf_str = "";
            $c_array = $this->get_sConfigurations($SiteID);
            for($i=0; $i<count($c_array); $i++)
                if(!defined($c_array[$i]['conf_key'])) {
                    define($c_array[$i]['conf_key'], $c_array[$i]['conf_value']);
                }
        }
            
    //return site All info
    function get_sInfo($return, $SiteID = NULL)
    {
        if(!$SiteID) $SiteID = $this->SiteID;
        $sInfo = $this->get_site($SiteID);
        if($return == 'SiteID') { $return = 'SitesID'; }
        return $sInfo[$return]; 
    }
    
    //get and print SiteID
    function get_sId()
    {
        $sId = $this->get_sInfo("SitesID");
        return $sId; 
    }

    function print_sId()
    {
        print $this->get_sId();
    }
        
    //get and print site administration Name
    function get_sName()
    {
        $sName = $this->get_sInfo("Name");
        return $sName; 
    }

    function print_sName()
    {
        print $this->get_sName();
    }
    
    //get and print site Title      
    function get_sTitle()
    {
        $sTitle = $this->get_sInfo("Title");
        return $sTitle; 
    }

    function print_sTitle()
    {
        print $this->get_sTitle();
    }

    //get and print site URL
    function get_sURL($SiteID=NULL) 
    {
        $sURL = $this->_site['primary_url']; 
        
        if(user::$uLevel > 0) {
            return $_SERVER['SERVER_NAME'];
        }
        
        if(is_numeric($SiteID)) {
            $primary_url = $this->get_sInfo("primary_url", $SiteID);
            return   $primary_url ? $primary_url : $sURL; 
        }
            
        return $sURL;
    }
        
    function print_sURL()
    {
        print ("<a href=\"#\">".$this->get_sURL()."</a>");
    }

    //get and print site contact EMail  
    function get_sEmail()
    {
        return $this->get_sInfo("EMail");
    }

    function print_sEMail()
    {
        print $this->get_sEMail();
    }

    //get and print site StartPage number   
    function get_sStartPage()
    {
        $sStartPage = $this->get_sInfo("StartPage");
        return $sStartPage; 
    }
    function print_sStartPage()
    {
        print $this->get_sStartPage();
    }
        
    //get and print site Description    
    function get_sDescription()
    {
        $sDescription = $this->get_sInfo("Description");
        return $sDescription; 
    }   
    function print_sDescription()
    {
        print $this->get_sDescription();
    }
    
    //get and print site Keywords           
    function get_sKeywords()
    {
        $sKeywords = $this->get_sInfo("Keywords");
        return $sKeywords; 
    }
    function print_sKeywords()
    {
        print $this->get_sKeywords();
    }
    
    //get and initialize site Initial PHP code  
    function get_sInitPHP()
    {
        $sInitPHP = $this->get_sInfo("initPHP");
        return $sInitPHP; 
    }
        
    function print_sInitPHP()
    {
        eval($this->get_sInitPHP());
    }       

    //get and print site homepage Title 
    function get_sHome()
    {
        $sHome = $this->get_sInfo("Home");
        return $sHome; 
    }
    function print_sHome()
    {
        print $this->get_sHome();
    }
    
    function get_sMoreText()
    {
        return $this->get_sInfo("MoreText");
    }

    function get_sBack()
    {
        return $this->get_sInfo("Back");
    }       

    function get_sForward()
    {
        return $this->get_sInfo("forward");
    }
    
    function get_sTop()
    {
        return $this->get_sInfo("top");
    }
            
    function get_sSearchText()
    {
        return $this->get_sInfo("SearchText");
    }
        
    function get_sCopyright()
    {
        return $this->get_sInfo("copyright");
    }
    function print_sCopyright()
    {
        print $this->get_sCopyright();
    }
        
    function get_sNewsPage()
    {
        return $this->get_sInfo("news");
    }
    
    function get_sTemplate()
    {
        return $this->get_sInfo("Template");
    }
        
    function get_sCSS()
    {
        $cssID = $this->get_sInfo("css");
        $result = $this->db_query("SELECT * FROM CSSs WHERE cssID = '".$cssID."'");
        $css = $this->fetch("array", $result);
        return $css['css_url'];
    }   
    //return site image directory path
    function get_sImageDir($return = "image_dir", $id=NULL)
    {
        //$dirs = array();
        if(!$id) $id = $this->get_sInfo("imagesdir");
        $result = $this->db_query("SELECT * FROM image_dirs WHERE ID = '".$id."'");
        $dir = $this->fetch("array", $result);
        return $dir[$return];
    }

    //return array of all image directories accessed by the selected site or a single dir
    function get_sImageDirectories($SiteID = NULL, $dir_id = NULL)
    {
        if(!$SiteID)
            $SiteID = $this->SiteID;
        $result = $this->db_query("SELECT * FROM ImageAccess ia, image_dirs id WHERE ia.SiteID=".$SiteID." AND ia.imagesdir=id.ID ORDER by id.Name ASC");
        if($dir_id > 0)
            $result = $this->db_query("SELECT * FROM ImageAccess ia, image_dirs id WHERE ia.SiteID=".$SiteID." AND id.ID = ".$dir_id." AND ia.imagesdir=id.ID ORDER by id.Name ASC");
        
        while ($dir = $this->fetch("array", $result))
            $s_dirs[] = $dir;
        return $s_dirs;
    }

    //return directory info
    function get_sDirectoryInfo($id)
    {
        $result = $this->db_query("SELECT * FROM image_dirs WHERE ID=".$id." LIMIT 1");
        $dInfo = $this->fetch("array", $result);
        return $dInfo;
    }
        
    //return number of files in selected directory
    function count_directory_files($image_dir)
    {
        $result = $this->db_query("SELECT * FROM images im WHERE im.image_dir='".$image_dir."'");
        $count = $this->count($result);
        return $count;
    }
    
    //return all files in selected directory
    // $filter_usage = true : returns only non used images
    function get_sImages($image_dir, $filter = 1, $filter_usage = false)
    {
        global $user;
        $query = ("SELECT * FROM images im WHERE im.image_dir='".$image_dir."' AND $filter");
        $result = $this->db_query($query);
        while($file = $this->fetch("array", $result))
        {
            //check for image usage in any pages
            //$search_result = $this->db_query("SELECT * FROM pages p WHERE (p.imageNo='".$file['imageID']."' OR p.textStr LIKE CONVERT( _utf8 '%".$file['image_src']."%' USING cp1251 ) ) AND p.SiteID = '".$this->SiteID."' ");
            //$search_result = $this->db_query("SELECT * FROM pages p WHERE p.imageNo='".$file['imageID']."' AND p.SiteID = '".$this->SiteID."' LIMIT 1");
            
            //�������� ���������� �� ���������� �������� � ������
            $query = "SELECT * FROM pages p WHERE p.imageNo='".$file['imageID']."' LIMIT 1";
            $search_result = $this->db_query($query);
            $img_usage = $this->count($search_result);
            $file['usage'] = $img_usage;
            
            //filter only images not used anywhere in pages
            if($filter_usage)
                { 
                    if($img_usage == 0) $images[] = $file;
                }
            
            else 
                $images[] = $file;
        }
        return $images;
    }
        
        
    //return image info 
    function get_sImageInfo($imageID)
    {
        $result = $this->db_query("SELECT * FROM images im left join image_dirs imd on im.image_dir = imd.ID WHERE im.imageID='".$imageID."'");
        $image = $this->fetch("array", $result);
        return $image;
    }
            
    //search in the site
    function do_search($Search, $max_num_of_res=15){
     $num_of_res = 0; 

     if($Search=="") {
      $Search = $_POST['Search']; 
     }
     if($Search=="") {
      $Search = $_POST['search']; 
     } 
     if($Search=="") {
      $Search = $_GET['Search']; 
       $Search = iconv("UTF-8","CP1251",$Search);
     } 
     if($Search=="") {
       $Search = $_GET['search_tag']; 
       $Search = iconv("UTF-8","CP1251",$Search);
     } 
     if(strlen($Search)>2) {
         $search_level = 1;     // search in page title, Name, tags exact phrase
         $add_query=""; 
            while(($num_of_res<=$max_num_of_res) && ($search_level<=4)) { 
                 if($search_level==2) {$add_query= "$add_query  OR ((tags LIKE '%$Search%')  OR (textStr LIKE '%$Search%')) ";}  // search in page text  exact phrase
                 if($search_level>=3) { // search in page Name, title, tags diferent words
                    $search_cleaned = str_replace(",", "", $Search); 
                    $search_words = split(" ", $search_cleaned);    // max 10 words to search for
                    foreach($search_words as $s_word) {
                        if(strlen($s_word)>3) {
                            if ($search_level==3) {$add_query= "$add_query  OR  (Name LIKE '%$s_word%' ) OR (title LIKE '%$s_word%') OR (tags LIKE '%$s_word%') "; }; 
                            if ($search_level==4) {$add_query= "$add_query  OR  (textStr LIKE '%$s_word%' )  "; }; 
                            }
                    }
                 }
                 $query_str = "SELECT p.n, p.ParentPage, p.Name, p.title, p.SiteID, p.textStr, im.image_src FROM pages p  left join images im on p.imageNo = im.imageID  WHERE (SiteID='$this->SiteID' )  AND (p.SecLevel<='user::$uLevel') AND ( (Name LIKE '%$Search%') OR  (title LIKE '%$Search%')  $add_query ) ORDER BY p.preview DESC, p.date_modified  DESC  "; 
                 //echo("$search_level . $query_str <br>"); 
                 //echo("$add_query   <br>$search_level $query_str<br>");
                 $s_query = $this->db_query("$query_str"); 
                 while(($num_of_res<$max_num_of_res) && ($s_row=$this->fetch("array", $s_query))) {
                      $already_found = false; 
                      foreach($s_results as $row) {
                           if($row['n']==$s_row['n']) {
                            $already_found=true; 
                           } 
                      }               
                      if (!($already_found)) {
                          $s_row['textStr'] = strip_tags($s_row['textStr']); 
                          $pos = strripos($s_row['textStr'],$Search); 
                          $s_row['textStr'] = substr($s_row['textStr'], $pos, 1000); 
                          $s_row['textStr'] = str_replace(strtolower($Search), "<u><b>".strtolower($Search)."</b></u>", strtolower($s_row['textStr']));
                          $s_row['Name'] = str_replace(strtolower($Search), "<u><b>".strtolower($Search)."</b></u>", strtolower($s_row['Name']));
                          $s_results[] = $s_row;                     
                          $num_of_res++; 
                          }
                 }
                 $search_level++; 
            } // max_num of results reached
     } // there is no Search var defined
     return $s_results; 
    }

    //get tags from all site pages
    function get_sTags($limit = 100, $SiteID=0)
    {
        if($SiteID == 0) $SiteID = $this->SiteID; 
        $tags_array = array();
        $tags_query = $this->db_query("SELECT * FROM pages WHERE SiteID = '".$SiteID."' AND tags !='' ORDER by RAND()");
                
        while($page = $this->fetch("array", $tags_query) && ($limit>0) )
            {
                $page['tags'] = str_replace(" ,",",", $page['tags']);
                $page['tags'] = str_replace(", ",",", $page['tags']);
                $split_tags = explode(",", $page['tags']);
                
                foreach($split_tags as $tag) {
                    $tags_array[] = $tag; 
                }
                
                $limit--; 
                //for($i=0; $i<count($split_tags); $i++)
                //  if(strlen($split_tags[$i]) > 4)
                        //$tags_array[] = array($page['n'], $split_tags[$i], $page['Name']);

            }
            shuffle($tags_array);
            if($limit > 0 && $limit <=count($tags_array))
                $tags_array = array_slice($tags_array, 0, $limit);
            return $tags_array;
    }
    
    
    //output login form
    function print_login_form()
    {
        print "<div class=\"message_warning\">Please login <a href=\"/admin\">here</a></div>";
    }

     function get_keys()  {
      if (!empty($this->referer))
      {
          $this->referer = strtolower($this->referer);
       if (preg_match('#www\.google#', $this->referer))
       {
        // Google
        preg_replace("#{$this->sep}q=(.*?)\&#si", $this->referer, $this->keys);
        $this->search_engine = 'Google';
       }
       else if (preg_match('#(yahoo\.com|search\.yahoo)#', $this->referer))
       {
        // Yahoo
        preg_replace("#{$this->sep}p=(.*?)\&#si", $this->referer, $this->keys);
        $this->search_engine = 'Yahoo';
       }
       else if (preg_match('#search\.msn#', $this->referer))
       {
        // MSN
        preg_replace("#{$this->sep}q=(.*?)\&#si", $this->referer, $this->keys);
        $this->search_engine = 'MSN';
       }
       else if (preg_match('#www\.alltheweb#', $this->referer))
       {
        // AllTheWeb
        preg_replace("#{$this->sep}q=(.*?)\&#si", $this->referer, $this->keys);
        $this->search_engine = 'AllTheWeb';
       }
       else if (preg_match('#(looksmart\.com|search\.looksmart)#i', $this->referer))
       {
        // Looksmart
        preg_replace("#{$this->sep}qt=(.*?)\&#si", $this->referer, $this->keys);
        $this->search_engine = 'Looksmart';
       }
       else if (preg_match('#(askjeeves\.com|ask\.com)#', $this->referer))
       {
        // AskJeeves
        preg_replace("#{$this->sep}q=(.*?)\&#si", $this->referer, $this->keys);
        $this->search_engine = 'AskJeeves';
       }
       else if (preg_match('#yandex\.ru#', $this->referer )) {
         // Yandex.ru
        preg_replace("#{$this->sep}text=(.*?)\&#si", $this->referer, $this->keys);
         $this->search_engine = 'Yandex'; 
       }
       else
       {
        $this->keys = 'Not available';
        $this->search_engine = 'Unknown';
       }
       return array(
        $this->referer,
        (!is_array($this->keys) ? $this->keys : $this->keys[1]),
        $this->search_engine
       );
      }
      return array();
  }
  
    function clear_url($url)
    {  // get a domain Name from a full url 
        $nowww = preg_replace('#www\.#i','',$url); 
        $domain = parse_url($nowww); 
        if(!empty($domain["host"]))     {      return $domain["host"];      } else      {      return $domain["path"];      } 

      }
            
    function add_stats($n, $ParentPage)
    {
        $sesID = session_id(); 
        $referer_short = str_ireplace("http://", "", $this->referer); 
        $referer_short = substr($referer_short, 0, 10); 
        $url_short = substr($this->url, 0, 10); 
        
        $entry=1;
        if($referer_short==$url_short) $entry=0;
        
        //$stat_inserted= $this->db_query("SELECT * FROM stats WHERE n='$n' AND viewTime='$this->time_of_entry' "); 
        //if($this->count($stat_inserted)==0) {
            $this->db_query("INSERT INTO stats SET sesID='$sesID', n='$n',  SiteID='$this->SiteID', ip='$this->user_ip', entry='$entry'  "); 
        //}
        
        if($entry==1) {
          $stID = $this->last_insert_id(); 
          $referer_name = $this->clear_url("$this->referer"); 
          $url_name = $this->clear_url("$this->url"); 
            if($referer_name<>'') 
                $this->db_query("INSERT INTO referers SET  stID='$stID' , referer='$referer_name', url='$url_name', SiteID='".$this->SiteID."'  "); 
            
        }

    }
    
    function add_keys() {
        $keys = $this->get_keys();
        //if($this->n == 205952) { echo $_SERVER['HTTP_REFERER'];  echo("***"); for($i=0; $i<=10; ++$i) {echo($i); echo $keys[$i]; }}
        if (count($keys) && ($this->keys != 'Not available' || $this->search_engine != 'Unknown')) 
        {
          //$keys[1] = html_entity_decode($keys[1], ENT_COMPAT, 'CP1251'); 
          if(strlen($keys[1])>3) {
              $keys[1] = urldecode($keys[1]); 
              $keys[1] = iconv("UTF-8", "CP1251//TRANSLIT", $keys[1]); 
              $key_inserted = $this->db_query("SELECT * FROM keywords WHERE n='$this->n' AND enter_time='$this->time_of_entry'   "); 
              if($this->count($key_inserted)==0) {
                $search_engine_id=0; 
                if($keys[2]=="Google")      $search_engine_id=1; 
                if($keys[2]=="Yahoo")   $search_engine_id=2; 
                if($keys[2]=="Yandex")      $search_engine_id=3; 
                if($keys[2]=="AskJeeves")   $search_engine_id=4; 
                if($this->_site['netservice']>0)  {
                    //$g_pos=$this->google_pos($keys[1], $_SERVER["SERVER_NAME"]); 
                    $g_pos = 0; 
                    if( (strlen($keys[1])>=6) AND (strlen($keys[1])<=16) ) $g_pos = -5; 
                    //$keys[1] = $_SERVER["SERVER_NAME"]; 
                     // $this->db_query("INSERT INTO keywords SET  keywords='$keys[1]', search_engine='$keys[2]',  n='$this->n', SiteID='$this->SiteID', ip='$this->user_ip',  enter_time='$this->time_of_entry', referer='$this->referer' "); 
                    $this->db_query("INSERT INTO keywords SET  keywords='$keys[1]', search_engine='$keys[2]',  search_engine_id='$search_engine_id', g_pos='$g_pos', n='$this->n', SiteID='$this->SiteID', ip='$this->user_ip',  enter_time='$this->time_of_entry'  "); 
                    if ((strlen($keys[1])<=36) && (strlen($keys[1])>=6) ) {
                        $this->db_query("UPDATE keywords SET repeated='1' WHERE keywords='$keys[1]' AND ip<>'$this->user_ip'  LIMIT 5"); 
                        $key_count_q = $this->db_query("SELECT * FROM keywords WHERE keywords='$keys[1]'  AND search_engine_id>0 "); 
                        $key_count = $this->count($key_count_q);
                        if($key_count>1) {
                            $this->db_query("UPDATE keywords SET visits='$key_count ' WHERE SiteID='$this->SiteID' AND keywords='$keys[1]'  ORDER BY keysID DESC LIMIT 2  ");   // SiteID e za uskoriavane na zaiavkata
                        }
                    } // $this->_site['netservice']>0 
                }
             }
            } // strlen $keys[1]
        }
    } // end function add_keys
    
    
    //return array of all site requests
    function get_sRequests($SiteID, $r_status = 0, $filter = "1")
        {
            // $filter = "f.Data>DATE_SUB(CURDATE(),INTERVAL 12 HOUR)";
            if(!isset($SiteID)) $SiteID = $this->SiteID;

            if($r_status < 0)
                $results =  $this->db_query("SELECT * FROM forms f left join pages p on f.n=p.n WHERE f.SiteID = '$SiteID' AND $filter ORDER by f.Data DESC LIMIT 0, 100");
            else
                $results =  $this->db_query("SELECT * FROM forms f left join pages p on f.n=p.n WHERE f.SiteID = '$SiteID' AND f.ftype = '$r_status' AND $filter ORDER by f.Data DESC LIMIT 0, 100");   

            $sRequests = array();
            
            while($request = $this->fetch("array", $results))
                $sRequests[] = $request;
            return $sRequests;
        }
    
    //return site cms hosting info array
    function get_sCMSHosting($SiteID, $return = "last", $filter=1)
        {
            $query = "SELECT * FROM hostCMS WHERE SiteID = ".$SiteID." AND $filter ORDER by Date DESC";
            $result = $this->db_query($query);
            $sCMSHosting = array();
            
            if($return == "last")
                $sCMSHosting = $this->fetch("array", $result);
            else
            while($row = $this->fetch("array", $result))
                $sCMSHosting[] = $row;
            
            return $sCMSHosting;
        }
    
    //return site versions
    function get_sVersions($SiteID=NULL)
        {
            if(!$SiteID) $SiteID = $this->SiteID;
            //$v_query = $this->db_query("SELECT * FROM Sites s, versions v WHERE s.SitesID = '".$SiteID."' AND s.SitesID = v.SiteID"); 
            $v_query = $this->db_query("SELECT * FROM versions v left join languages l on l.language_key = v.version_key WHERE v.SiteID = '".$SiteID."'"); 
            $sVersions = array();
            while($version = $this->fetch("array", $v_query))
                {
                    
                    $primary_url = $this->get_sInfo("primary_url", $version['verSiteID']);
                    if( (strlen($primary_url) > 7) && ($primary_url <> $this->primary_url) && (user::$uLevel == 0) )
                        $version['version_link'] = "http://".$primary_url;
                    else
                        $version['version_link'] = "page.php?n=".$version['n']."&amp;SiteID=".$version['verSiteID'];
                    
                    $sVersions[] = $version;
                }
            return $sVersions;
        }

    //output site languages (text / image / select) 
    function print_sVersions($output = "text", $SiteID = NULL)
        {
            $versions = $this->get_sVersions($SiteID);
            $content = "";
            for($i=0; $i<count($versions); $i++)
                {
                    
                    if($versions[$i]['verSiteID'] == $this->SiteID)
                        $class = "selected";
                    else
                        $class = "";
                    $content.= "<a href=\"".$versions[$i]['version_link']."\" title=\"".$versions[$i]['version_key']."\" class=\"$class\">".$versions[$i]['version']."</a>";
                    /*
                    switch($output)
                        {
                            //output text links
                            case "text" :
                                {
                                    
                                } 
                        }
                    */
                }
            echo $content;
        }   
    //return site language: BG=1, EN=2, RU=3, FR=4
    function get_sLanguage($SiteID=NULL, $full=false)
        {
            if(!$SiteID) $SiteID = $this->SiteID;
            $sLanguage = $this->get_sInfo("language_id", $SiteID);
            $l_query = $this->db_query("SELECT * FROM languages l WHERE l.language_id = '".$sLanguage."' LIMIT 1"); 
            $language = $this->fetch("array", $l_query);
            if($full){
                return $language;
            }
            return strtolower($language['language_key']);
        }
    
    //return aaray of all site groups
    function get_sGroups($SiteID=NULL, $filter = "1")
        {
            if(!$SiteID) $SiteID = $this->SiteID;
            $res = $this->db_query("SELECT * FROM groups WHERE SiteID = $SiteID AND $filter");
            if($this->count($res) == 0)
                return NULL;
            $groups = array();
            while ($group = $this->fetch("array", $res))
                {
                    $pages_in_group = 0;
                    $res2 = $this->db_query("SELECT * FROM pages_to_groups ptg WHERE ptg.group_id = '".$group['group_id']."'");
                    $pages_in_group = $this->count($res2);
                    $group['pages_in_group'] = $pages_in_group;
                    $groups[] = $group;
                    
                }
            return $groups;
        }
        
    //return site options
    function get_sOptions()
        {
            $sOptions = array();
            $SiteID = $this->SiteID;
            $res = $this->db_query("SELECT * FROM options o WHERE o.SiteID = $SiteID");
            while ($option = $this->fetch("array", $res))
                $sOptions[] = $option;
            return $sOptions;
                
        }
        
    function get_Host_order($type=NULL,$ok=NULL)
        {
            $q = "SELECT * FROM items WHERE itype='".$type."' and ok='".$ok."'";
                while($row = $this->db_query($q)){
                }
    
        }

    //return all comments in site
    function get_sComments($SiteID=NULL)
        {
            if(!$SiteID) $SiteID = $this->SiteID;
            $res = $this->db_query("SELECT * FROM pages p RIGHT JOIN comments c on c.comment_n = p.n WHERE p.SiteID = '".$SiteID."' AND c.comment_status=1 ORDER by c.comment_date DESC");
            while($comment = $this->fetch("array", $res))
                $comments[] = $comment;
            return $comments;
        }

/* ###################### ADMINISTRATIVE FUNCTIONS: add / update / delete etc. ###################### */

//add or update confuguration value
function set_sConfiguration($conf_key, $conf_value, $SiteID=NULL, $templ_id=0)
    {
        //response codes
        // 0 - error, conf_key not defined
        // 1 - success, configuration INSERTED
        // 2 - success, configuration UPDATED
        
        if(!$SiteID) $SiteID = $this->SiteID;
        
        //find and extract selcted configuration default values
        //If teh configuration key does not exist - break operation  
        $search_query = "SELECT * FROM configuration_types WHERE conf_type_key = '".$conf_key."' LIMIT 1";
        $configuration = $this->fetch("array", $this->db_query($search_query));
        
        if($this->count($this->db_query($search_query)) > 0)
            {
                if($this->get_sConfigValue($conf_key, $SiteID))
                    {
                        //update configuration  
                        $query = "UPDATE configuration SET conf_value = '".$conf_value."', last_modified = now(), templ_id = ".$templ_id." WHERE conf_key = '".$conf_key."' AND SiteID = ".$SiteID." AND templ_id= ".$templ_id."";
                        //$search_query = "SELECT * FROM configuration WHERE conf_key = '".$conf_key."' AND SiteID = ".$SiteID." AND templ_id = ".$templ_id." LIMIT 1";
                        $this->db_query($query);
                        //echo $query;
                        return 2;
                    }
                else
                    {
                        //add new key + value
                        $query = "INSERT INTO configuration SET conf_title = '', conf_key = '".$conf_key."', conf_value = '".$conf_value."', conf_description = '".$configuration['conf_type_description']."', conf_group_id = '".$configuration['conf_group_id']."', sort_order = 0, last_modified = 'now()', date_added = 'now()', SiteID = '".$SiteID."', templ_id = '".$templ_id."'";
                        $this->db_query($query);
                        //echo $query;  
                        return 1;           
                    }
            }
        else echo "Configuration key <b>".$conf_key."</b> not found in DB !";
        //else return 0;
    }
//delete all configurations filtered by templ_id and SiteID
function reset_sConfigurations($templ_id, $SiteID)
    {
        if(!isset($templ_id)) $templ_id = $this->_site["Template"];
        if(!isset($SiteID)) $SiteID = $this->_site["SitesID"];
            
        $query = "DELETE FROM configuration WHERE SiteID=".$SiteID." AND templ_id = ".$templ_id."";
        $this->db_query($query);
    }
    
    
    function site_create($NewSiteName, $userID, $language_id = 1) {
    
         // insert start page
         $query="INSERT INTO pages SET ParentPage='0', Name='������' ";    
         //echo("$query");
         $this->db_query("$query");
         $StartPage = $this->last_insert_id();
         
         //insert image directory
         $query="INSERT INTO image_dirs SET Name='$NewSiteName' ";
         $this->db_query("$query");  
         $imagesdir = $this->last_insert_id();
         
         //insert new site   UPDATED 22.04.2014 Martin Kamenov
         $query="INSERT INTO Sites SET Name='$NewSiteName', StartPage='$StartPage', imagesdir='$imagesdir', Rub = '".$this->_site['Rub']."', Template = ".$this->_site['Template'].",  css = ".$this->_site['css'].", language_id = '".$language_id."' ";
         echo("$query");
         $this->db_query("$query");
         $SiteID = $this->last_insert_id();
         
         //update image directory
         $image_dir_path = "web/images/upload/".$SiteID;
         $query="UPDATE image_dirs SET image_dir ='$image_dir_path' WHERE ID='$imagesdir' "; 
         //echo("$query");
         $this->db_query("$query");
         
         //update start page
         $query="UPDATE pages SET SiteID='$SiteID' WHERE n='$StartPage' "; 
         //echo("$query");
         $this->db_query("$query");
         
         //site access
         $query="INSERT INTO SiteAccess SET SiteID='$SiteID', userID='$userID'";
         //echo("$query");
         $this->db_query("$query");
    
         //image_dir access
         $query="INSERT INTO ImageAccess SET SiteID='$SiteID', imagesdir='$imagesdir' ";
         //echo("$query");
         $this->db_query("$query");
         
         //update users 
         $query="UPDATE users SET InitPage = '154', InitSiteID = '$SiteID' WHERE ID = '$userID' ";
         //echo("$query");
         $this->db_query("$query");
      
         //create directory on the server
         mkdir ("/hosting/maksoft/maksoft/".$image_dir_path, 0755);
         
     return  $SiteID; 
    
    }
    
    function cache_get($cvID) {
        $res = $this->db_query("SELECT * FROM cache_vars WHERE cvID='$cvID' LIMIT 1 "); 
        $row = $this->fetch("object", $res);        
        $val = unserialize($row->val); 
        return $val; 
    }
    
    function cache_put($val, $cvID){
        $gen_time = microtime(true); 
        $this->db_query("UPDATE cache_vars SET val = '".serialize($val)."', gen_time = '".$gen_time."' WHERE cvID = '$cvID'"); 
    } 
    
    function cache_actual($cvID) {
        $res = $this->db_query("SELECT * FROM cache_vars WHERE cvID='$cvID' LIMIT 1 "); 
        $row = $this->fetch("object", $res); 
        $cache_live = microtime(true) - $row->gen_time ; 
        //echo $cache_live; 
        if($cache_live < $row->ttl) {
            return true; 
        } else {
            return false; 
        }
    }

    function isActive()
    {
        $res = $this->db_query("
                    SELECT DISTINCT(SiteID) FROM  `hostCMS` 
                    WHERE NOW() < DATE_ADD(toDate, INTERVAL 6 MONTH) 
                    AND SiteID = ".$this->SiteID);
        if($this->count($res)>0){
             return True;
        }
        return True;
    }

     function __site()
    {
      $this->childObject = null;
    }
            
} // end class site
?>
