<?php
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_site >> lib_page >> lib_anketi
require_once('lib_mysql.php');

class hotels extends mysql_db {

var $db_host_upl = 'localhost';
var $db_user_upl = 'maksoft';
var $db_pass_upl = 'mak211';
var $main_db_upl = 'hotels';

// constructor 
function __construct()
	{	
		//parent::mysql_db($db_host_upl, $db_user_upl, $db_pass_upl, $main_db_upl);
		//$this->mysql_db($db_host_upl, $db_user_upl, $db_pass_upl, $main_db_upl);
		$this->__construct();
		$this->db_select("$main_db_upl");
		//$link_hotels = new mysql_db(); 
	}
			
// destructor
function __destruct()
	{	
		//parent::mysql_db($db_host_upl, $db_user_upl, $db_pass_upl, $main_db_upl);
		//$this->mysql_db($db_host_upl, $db_user_upl, $db_pass_upl, $main_db_upl);
		$this->mysql_db();
		$this->db_select("maksoft");	
		//$link_hotels = new mysql_db(); 
	}
#######################
# FRONT-END functions #
#######################

//get all hotel info
function get_hotel($id, $language_id, $return=NULL)
	{     
		//global $language_id;
		//return array, all hotel info
		$sql = "SELECT * FROM ".TABLE_HOTELS." h left join ".TABLE_HOTELS_DESCRIPTION." hd on h.hotel_id = hd.hotel_id, ".TABLE_HOTELS_TO_TYPES." ht, ".TABLE_TOUR_TYPES." tt, ".TABLE_RESORTS_DESCRIPTION." rd WHERE h.hotel_id='".$id."' AND h.hotel_id = ht.hotel_id AND ht.tour_type_id = tt.tour_type_id AND h.resort_id = rd.resort_id AND hd.language_id = '".$language_id."' AND rd.language_id = '".$language_id."' LIMIT 1";
		$result = $this->db_query($sql);
		$hotel_array = mysqli_fetch_array($result);
		//$hotel_array['status_text'] = $this->get_hotel_status($id); 
		if($return)
			return $sql;
		else
		return $hotel_array;
	}

//get all resort info
function get_resort($id, $language_id)
	{     
		//return array, all hotel info
		$result = $this->db_query("SELECT * FROM ".TABLE_RESORTS." r left join ".TABLE_RESORTS_DESCRIPTION." rd on r.resort_id = rd.resort_id WHERE r.resort_id='".$id."' AND rd.language_id = '".$language_id."' LIMIT 1"); 
		$resort_array = mysqli_fetch_array($result);
		return $resort_array;
	}
	
//return list of hotels for selcted SiteID
function get_hotels($SiteID, $filter = "1")
	{
		global $language_id;
		//return array, all hotels for selected SiteID
		$sql = "SELECT DISTINCT hd.hotel_title, h.hotel_id, h.hotel_SiteID, h.hotel_category, h.resort_id, h.hotel_infant_age, h.hotel_child_age, h.hotel_image, h.hotel_status, hd.hotel_title, hd.hotel_description, hd.hotel_description_extra, r.resort_name, r.resort_id, rd.resort_title, rd.resort_description FROM (".TABLE_HOTELS." h left join ".TABLE_HOTELS_DESCRIPTION." hd on h.hotel_id = hd.hotel_id) left join (".TABLE_RESORTS." r left join ".TABLE_RESORTS_DESCRIPTION." rd on r.resort_id = rd.resort_id) on h.resort_id = r.resort_id, ".TABLE_HOTELS_TO_TYPES." ht left join ".TABLE_TOUR_TYPES." tt on ht.tour_type_id = tt.tour_type_id WHERE h.hotel_SiteID='$SiteID' AND ht.hotel_id = h.hotel_id AND hd.language_id = '".$language_id."' AND rd.language_id = '".$language_id."' AND $filter";
		$result = $this->db_query($sql);
		while($hotel = mysqli_fetch_array($result))
			{
				$hotel['status_text'] = $this->get_hotel_status($hotel['hotel_id']);
				//$hotel['status_text'] = "test";
				$hotels_array[] = $hotel;
			}
		
		//$hotels_array['sql'] = $sql;
		return $hotels_array;	
	}

//return list of all resorts
function get_resorts($filter = "1")
	{
		global $language_id;
		$result = $this->db_query("SELECT * FROM ".TABLE_RESORTS." r left join ".TABLE_RESORTS_DESCRIPTION." rd on r.resort_id = rd.resort_id WHERE rd.language_id = '".$language_id."' AND $filter");
		while($resort = mysqli_fetch_array($result))
			$resorts_array[] = $resort;
		
		return $resorts_array;		
	}
/*
//return list of all periods
function get_periods($hotel_id, $filter = "1")
	{
		global $hotel_SiteID, $language_id;
		
		$result = $this->db_query("SELECT * FROM periods p, periods_type pt left join periods_type_description ptd on pt.period_type_id = ptd.period_type_id WHERE ptd.language_id = '".$language_id."' AND p.period_type_id = pt.period_type_id AND pt.hotel_id = '".$hotel_id."' AND $filter ORDER by pt.period_order ASC, p.date_from ASC"); 
		while($period = mysqli_fetch_array($result))
			{
				$key = $period['period_type_key'];
				$periods_types_array[$key][] = array("period_type_id" =>$period['period_type_id'], "date_from"=>$period['date_from'], "date_to"=>$period['date_to'], "min_days"=>$period['min_days'], "period_type_key"=>$period['period_type_key'], "period_type_title"=>$period['period_type_title']);
			}
		//return $periods_array;
		return $periods_types_array;		

	}
*/
//return hotel rooms
function get_rooms($hotel_id)
	{
		$sql = "SELECT * FROM ".TABLE_ROOMS." r WHERE r.hotel_id = '".$hotel_id."' ORDER by r.room_id ASC";
		$result = $this->db_query($sql);
		while($room = mysqli_fetch_array($result))
			$rooms[] = $room;
		return $rooms;
	}

//return hotel types
function get_types($hotel_id=NULL)
	{
		global $language_id;
		if($hotel_id)
			$sql = "SELECT * FROM ".TABLE_HOTELS_TO_TYPES." htt, ".TABLE_TOUR_TYPES." tt left join ".TABLE_TOUR_TYPES_DESCRIPTION." ttd on tt.tour_type_id = ttd.tour_type_id WHERE htt.hotel_id = '".$hotel_id."' AND htt.tour_type_id = tt.tour_type_id AND ttd.language_id = '".$language_id."' ORDER by ttd.tour_type_title ASC";
		else
			$sql = "SELECT * FROM ".TABLE_TOUR_TYPES." tt left join ".TABLE_TOUR_TYPES_DESCRIPTION." ttd on tt.tour_type_id = ttd.tour_type_id WHERE ttd.language_id = '".$language_id."' ORDER by ttd.tour_type_title ASC";		
		$result = $this->db_query($sql);
		while($type = mysqli_fetch_array($result))
			$types[] = $type;
		return $types;
	}
					
//get selected hotel info
function get_hInfo($id, $return, $language_id)
	{
		//return selected info by $return
		$hInfo = $this->get_hotel($id, $language_id);
		return $hInfo[$return];
	}

//return period types, $active=true (select only periods with dates defined)
function get_period_types($hotel_id, $active = false, $language_id = 1, $expire_date = NULL)
	{
		global $hotel_SiteID, $language_id;
		
		$sql = "SELECT * FROM ".TABLE_PERIODS_TYPE." pt left join ".TABLE_PERIODS_TYPE_DESCRIPTION." ptd on pt.period_type_id = ptd.period_type_id WHERE pt.SiteID='".$hotel_SiteID."' AND pt.hotel_id = '".$hotel_id."' AND ptd.language_id = '".$language_id."' ORDER by pt.period_order ASC";
		$result = $this->db_query($sql);
		while($period_type = mysqli_fetch_array($result))
			{
				if($active)
					{
						//expire date
						if($expire_date)
							$add_sql = "AND date_to >= '".date("Y-m-d", strtotime($expire_date))."'"; 
						else $add_sql = "AND 1";
						$sql2 = "SELECT * FROM ".TABLE_PERIODS." WHERE period_type_id = '".$period_type['period_type_id']."' $add_sql";
						$result2 = $this->db_query($sql2);
						if(mysqli_num_rows($result2) > 0)
							$periods_type[] = $period_type;
					}
				else $periods_type[] = $period_type;
			}
		return $periods_type;
	}

//return period type info
function get_period_type_info($period_type_id, $language_id)
	{
		global $hotel_id;
		$period_types = $this->get_period_types($hotel_id, false, $language_id);
		for($i=0; $i<count($period_types); $i++)
			if($period_types[$i]['period_type_id'] == $period_type_id)
				return $period_types[$i];
	}

//return period dates
function get_period_dates($period_type_id)
	{
	
		$result = $this->db_query("SELECT * FROM ".TABLE_PERIODS." WHERE period_type_id = '".$period_type_id."' ORDER by date_from ASC"); 
		while($dates = mysqli_fetch_array($result))
			$period_dates[] = $dates; 
		return $period_dates;		
	}
	
//get selected resort info
function get_rInfo($id, $return, $language_id)
	{
		$rInfo = $this->get_resort($id, $language_id);
		return $rInfo[$return];
	}
	
//return hotel Name
function get_hName($id, $language_id)
	{
		$hName = $this->get_hInfo($id, "hotel_title", $language_id);
		return $hName;
	}
	
function print_hName($id, $language_id)
	{
		print $this->get_hName($id);
	}
	
//return hotel description
function get_hDescription()
	{
		//return hotel description fo selected language_id
	}

function print_hDescription()
	{
		print $this->get_hDescription();
	}		

//return hotel resort_id
function get_hResort()
	{
		//return hotel resort id
	}

function get_hConditions($hotel_id, $language_id)
	{
		$hConditions = $this->get_hInfo($hotel_id, "hotel_conditions", $language_id);
		return $hConditions;
	}


function singleHotelInfo($singlehotel=0, $language_id){
	if(!$singlehotel){
		exit;
	}
	$hotelID=$singlehotel;
	$query = $this->db_query("
	SELECT hotels_description.hotel_title as Title,  hotels.hotel_category as numberOfStars, hotels.hotel_id as mainID, hotels.resort_id as resortID, resorts_description.resort_title as resortTitle, hotels_description.hotel_description as hDescr, hotels.hotel_image as image
	FROM hotels_description 
	LEFT JOIN hotels ON hotels_description.hotel_id = hotels.hotel_id
	LEFT JOIN  resorts_description ON hotels.resort_id = resorts_description.resort_id
	WHERE hotels_description.hotel_id =".(int)$hotelID." 
	AND hotels_description.language_id=".$language_id."
	AND resorts_description.language_id=".$language_id."
	");
	$query = mysqli_fetch_array($query);
	return $query;
	
}

function hotel_tour_icons($hotelID=0){
	if(!$hotelID){
		exit;
	}
	$query = $this->db_query("
	SELECT tour_types.tour_type_key as ICON
	FROM hotels_to_types
	LEFT JOIN tour_types ON hotels_to_types.tour_type_id = tour_types.tour_type_id
	WHERE hotels_to_types.hotel_id = ".$hotelID."
	");
	$iconset=array();
	while($data = mysqli_fetch_array($query)) {
		$icon = $data['ICON'];
		array_push($iconset, $icon);
	}
	return $iconset;
}

function search_hotels($resort_id, $tour_type_id, $hotel_category, $hotelname, $hotels_per_page, $page){

				$offset =  $page * $hotels_per_page;
				global $language_id;
				if(($resort_id!=0) && ($tour_type_id!=0) && ($hotel_category!=0)){
					$allCount = $this->db_query("
						SELECT COUNT(*)  FROM ".TABLE_HOTELS_TO_TYPES." tht
						LEFT JOIN ".TABLE_HOTELS." th ON tht.hotel_id = th.hotel_id 
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON tht.hotel_id = thd.hotel_id 
						WHERE tht.tour_type_id= ".$tour_type_id." 
						AND th.resort_id = ".$resort_id."
						AND th.hotel_category = ".$hotel_category."
						AND thd.hotel_title LIKE %".$hotelname."%");
					$allCount=mysqli_fetch_assoc($allCount);
					$result = $this->db_query("
						SELECT th.hotel_id as hotelID FROM ".TABLE_HOTELS_TO_TYPES." tht
						LEFT JOIN ".TABLE_HOTELS." th ON tht.hotel_id = th.hotel_id 
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON tht.hotel_id = thd.hotel_id 
						WHERE tht.tour_type_id= ".$tour_type_id." 
						AND th.resort_id = ".$resort_id."
						AND th.hotel_category = ".$hotel_category."
						AND thd.hotel_title LIKE %".$hotelname."%
						ORDER BY th.hotel_category DESC LIMIT ".$offset.", 10");
					$hotelsIDS=array();
					while($data = mysqli_fetch_array($result)) {
					  $currentID = $data['hotelID'];
					  array_push($hotelsIDS,  $currentID);
					}
					return array(
						'hotelsIDS' => $hotelsIDS,
						'allCount' => $allCount["COUNT(*)"],
					);
				}else if(($resort_id!=0) && ($tour_type_id!=0) && ($hotel_category==0)){
					$allCount = $this->db_query("
						SELECT COUNT(*) FROM ".TABLE_HOTELS_TO_TYPES." tht
						LEFT JOIN ".TABLE_HOTELS." th ON tht.hotel_id = th.hotel_id 
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND tht.tour_type_id= ".$tour_type_id." 
						AND th.resort_id = ".$resort_id);
					$allCount=mysqli_fetch_assoc($allCount);
					$result = $this->db_query("
						SELECT th.hotel_id as hotelID FROM ".TABLE_HOTELS_TO_TYPES." tht
						LEFT JOIN ".TABLE_HOTELS." th ON tht.hotel_id = th.hotel_id 
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND tht.tour_type_id= ".$tour_type_id." 
						AND th.resort_id = ".$resort_id." 
						ORDER BY th.hotel_category DESC LIMIT ".$offset.", 10");
					$hotelsIDS=array();
					while($data = mysqli_fetch_array($result)) {
					  $currentID = $data['hotelID'];
					  array_push($hotelsIDS,  $currentID);
					}
					return array(
						'hotelsIDS' => $hotelsIDS,
						'allCount' => $allCount["COUNT(*)"],
					);
				
				}else if(($resort_id==0) && ($tour_type_id!=0) && ($hotel_category==0)){
					$allCount = $this->db_query("
						SELECT COUNT(*) FROM ".TABLE_HOTELS_TO_TYPES." tht
						LEFT JOIN ".TABLE_HOTELS." th ON tht.hotel_id = th.hotel_id 
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND tht.tour_type_id= ".$tour_type_id);
					$allCount=mysqli_fetch_assoc($allCount);
					$result = $this->db_query("
						SELECT th.hotel_id as hotelID FROM ".TABLE_HOTELS_TO_TYPES." tht
						LEFT JOIN ".TABLE_HOTELS." th ON tht.hotel_id = th.hotel_id 
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND tht.tour_type_id= ".$tour_type_id."
						ORDER BY th.hotel_category DESC LIMIT ".$offset.", 10");
					$hotelsIDS=array();
					while($data = mysqli_fetch_array($result)) {
					  $currentID = $data['hotelID'];
					  array_push($hotelsIDS,  $currentID);
					}
					return array(
						'hotelsIDS' => $hotelsIDS,
						'allCount' => $allCount["COUNT(*)"],
					);
				
				}else if(($resort_id==0) && ($tour_type_id!=0) && ($hotel_category!=0)){
					$allCount = $this->db_query("
						SELECT COUNT(*) FROM ".TABLE_HOTELS_TO_TYPES." tht
						LEFT JOIN ".TABLE_HOTELS." th ON tht.hotel_id = th.hotel_id 
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND tht.tour_type_id= ".$tour_type_id."
						AND th.hotel_category = ".$hotel_category);
					$allCount=mysqli_fetch_assoc($allCount);
					$result = $this->db_query("
						SELECT th.hotel_id as hotelID FROM ".TABLE_HOTELS_TO_TYPES." tht
						LEFT JOIN ".TABLE_HOTELS." th ON tht.hotel_id = th.hotel_id 
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND tht.tour_type_id= ".$tour_type_id."
						AND th.hotel_category = ".$hotel_category."
						ORDER BY th.hotel_category DESC LIMIT ".$offset.", 10");
					$hotelsIDS=array();
					while($data = mysqli_fetch_array($result)) {
					  $currentID = $data['hotelID'];
					  array_push($hotelsIDS,  $currentID);
					}
					return array(
						'hotelsIDS' => $hotelsIDS,
						'allCount' => $allCount["COUNT(*)"],
					);
				
				}else if(($resort_id!=0) && ($tour_type_id==0) && ($hotel_category!=0)){
					$allCount = $this->db_query("
						SELECT COUNT(*) FROM ".TABLE_HOTELS." th
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND th.resort_id= ".$resort_id."
						AND th.hotel_category = ".$hotel_category);
					$allCount=mysqli_fetch_assoc($allCount);
					$result = $this->db_query("
						SELECT th.hotel_id as hotelID FROM ".TABLE_HOTELS." th
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND th.resort_id= ".$resort_id."
						AND th.hotel_category = ".$hotel_category."
						ORDER BY th.hotel_category DESC LIMIT ".$offset.", 10");
					$hotelsIDS=array();
					while($data = mysqli_fetch_array($result)) {
					  $currentID = $data['hotelID'];
					  array_push($hotelsIDS,  $currentID);
					}
					return array(
						'hotelsIDS' => $hotelsIDS,
						'allCount' => $allCount["COUNT(*)"],
					);
				
				}else if(($resort_id!=0) && ($tour_type_id==0) && ($hotel_category==0)){
					$allCount = $this->db_query("
						SELECT COUNT(*) FROM ".TABLE_HOTELS." th
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND th.resort_id= ".$resort_id);
					$allCount=mysqli_fetch_assoc($allCount);
					$result = $this->db_query("
						SELECT th.hotel_id as hotelID FROM ".TABLE_HOTELS." th
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND th.resort_id= ".$resort_id." 
						ORDER BY th.hotel_category DESC LIMIT  ".$offset.", 10");
					$hotelsIDS=array();
					while($data = mysqli_fetch_array($result)) {
					  $currentID = $data['hotelID'];
					  array_push($hotelsIDS,  $currentID);
					}
					return array(
						'hotelsIDS' => $hotelsIDS,
						'allCount' => $allCount["COUNT(*)"],
					);
				
				}else if(($resort_id==0) && ($tour_type_id==0) && ($hotel_category!=0)){
					$allCount = $this->db_query("
						SELECT COUNT(*) FROM ".TABLE_HOTELS." th
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND th.hotel_category= ".$hotel_category);
					$allCount=mysqli_fetch_assoc($allCount);
					$result = $this->db_query("
						SELECT th.hotel_id as hotelID FROM ".TABLE_HOTELS." th
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						AND th.hotel_category= ".$hotel_category." 
						ORDER BY th.hotel_category DESC LIMIT  ".$offset.", 10");
					$hotelsIDS=array();
					while($data = mysqli_fetch_array($result)) {
					  $currentID = $data['hotelID'];
					  array_push($hotelsIDS,  $currentID);
					}
					return array(
						'hotelsIDS' => $hotelsIDS,
						'allCount' => $allCount["COUNT(*)"],
					);
				
				}else if(($resort_id==0) && ($tour_type_id==0) && ($hotel_category==0)){
					$allCount = $this->db_query("
						SELECT COUNT(*) FROM ".TABLE_HOTELS." th
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id);
					$allCount=mysqli_fetch_assoc($allCount);
					$result = $this->db_query("
						SELECT th.hotel_id as hotelID
						FROM ".TABLE_HOTELS." th 
						LEFT JOIN ".TABLE_HOTELS_DESCRIPTION." thd ON th.hotel_id = thd.hotel_id 
						WHERE thd.hotel_title LIKE '%".$hotelname."%'
						AND thd.language_id = ".$language_id."
						ORDER BY th.hotel_category DESC LIMIT ".$offset.", 10");
					$hotelsIDS=array();
					while($data = mysqli_fetch_array($result)) {
					  $currentID = $data['hotelID'];
					  array_push($hotelsIDS,  $currentID);
					}
					return array(
						'hotelsIDS' => $hotelsIDS,
						'allCount' => $allCount["COUNT(*)"],
					);
				}
}

//return array of tour types (sea / spa / mountine etc.)
function get_tour_types()
	{
		global $language_id;
		
		$result = $this->db_query("SELECT * FROM ".TABLE_TOUR_TYPES." tt left join ".TABLE_TOUR_TYPES_DESCRIPTION." ttd on tt.tour_type_id = ttd.tour_type_id WHERE ttd.language_id = '".$language_id."'"); 
		while($type = mysqli_fetch_array($result))
			$tour_types_array[] = $type;
		return $tour_types_array;	
	}

//return room prices, descriptions
function get_room_descriptions($room_id)
	{
		global $language_id;
		$result = $this->db_query("SELECT DISTINCT rd.room_description, rdt.room_description_title, rd.room_description_id, rdt.room_description_id, rd.room_id, rd.room_standart_beds, rd.room_extra_beds, rd.accommodation_type, rd.room_ep_type, at.accommodation_type_name, at.accommodation_type_string FROM ".TABLE_ROOMS_DESCRIPTION." rd left join ".TABLE_ROOMS_DESCRIPTION_TITLES." rdt on rd.room_description_id = rdt.room_description_id, ".TABLE_ACCOMMODATION_TYPES." at, rooms r WHERE rd.room_id = r.room_id AND rd.accommodation_type = at.accommodation_type_id AND r.room_id = '".$room_id."' AND rdt.language_id = '".$language_id."'");
		while($room_description = mysqli_fetch_array($result))
			$room_descriptions[] = $room_description;
		return $room_descriptions;	
	}

//return price info
function get_room_description_info($room_description_id)
	{
		//$result = $this->db_query("SELECT DISTINCT rdt.room_description_title, rd.room_description_id, rd.room_id, rd.room_standart_beds, rd.room_extra_beds, rd.accommodation_type, rd.room_ep_type, at.accommodation_type_name, at.accommodation_type_string FROM ".TABLE_ROOMS_DESCRIPTION." rd left join ".TABLE_ROOMS_DESCRIPTION_TITLES." rdt on rd.room_description_id = rdt.room_description_id, ".TABLE_ACCOMMODATION_TYPES." at WHERE rd.room_description_id = '".$room_description_id."' AND rd.accommodation_type = at.accommodation_type_id");
		$result = $this->db_query("SELECT * FROM ".TABLE_ROOMS_DESCRIPTION." rd left join ".TABLE_ROOMS_DESCRIPTION_TITLES." rdt on rd.room_description_id = rdt.room_description_id, ".TABLE_ACCOMMODATION_TYPES." at WHERE rd.room_description_id = '".$room_description_id."' AND rd.accommodation_type = at.accommodation_type_id");
		$room_description = array();
		while($descriptions = mysqli_fetch_array($result))
			$room_description[] = $descriptions;
		return $room_description;		
	}
	
	
//return array of accommodation types
function get_accommodation_types()
	{
		$result = $this->db_query("SELECT * FROM ".TABLE_ACCOMMODATION_TYPES." ORDER by accommodation_type_name ASC");
		while($accommodation = mysqli_fetch_array($result))
			$accommodation_types[] = $accommodation;
		return $accommodation_types;		
	}

//return array of all curencies
function get_currencies()
	{
		$result = $this->db_query("SELECT * FROM ".TABLE_CURRENCIES." ORDER by currency_order ASC");
		while($currency = mysqli_fetch_array($result))
			$currencies[] = $currency;
		return $currencies;
	}


//mail functions
function send_request($mail_to)
	{
		//global $_POST;
		if(mail($mail_to, "TEST subject", "TEST message"))
			return 1;
		else return 0;
	}


//return all extra payments for selected hotel
function get_extra_payments($hotel_id)
	{
		global $language_id;
		$sql = "SELECT * FROM ".TABLE_EXTRA_PAYMENTS." ep left join ".TABLE_EP_TYPES." ept on ep.ep_type_id = ept.ep_type_id, ".TABLE_ACCOMMODATION_TYPES." at, ".TABLE_PERIODS_TYPE." pt left join ".TABLE_PERIODS_TYPE_DESCRIPTION." ptd on pt.period_type_id = ptd.period_type_id, ".TABLE_ROOMS_DESCRIPTION." rd WHERE ep.period_type_id = pt.period_type_id AND ep.room_description_id = rd.room_description_id AND ep.accommodation_type_id = at.accommodation_type_id AND ptd.language_id = '".$language_id."' AND ep.hotel_id = '".$hotel_id."'";
		$result = $this->db_query($sql);	
		while($payment = mysqli_fetch_array($result))
			$extra_payments[] = $payment;
		return $extra_payments;
		//return $sql;
	}

//return all available promotions
function get_hotel_promotions($hotel_id = 0, $date_end = NULL)
	{
		global $language_id;
		if(!$date_end)
			{
				$today = date("Y-m-d");
				$add_sql = "p.promo_date_to >= '".$today."'";
			}
		else $add_sql = "p.promo_date_to >= '".$date_end."'";
		
		if(isset($hotel_id) && $hotel_id != 0 )
			$sql = "SELECT * FROM ".TABLE_PROMOTIONS." p left join ".TABLE_PROMOTIONS_DESCRIPTION." pd on p.promo_id = pd.promo_id WHERE pd.language_id = '".$language_id."' AND p.hotel_id = '".$hotel_id."' AND $add_sql ORDER by p.sort_n ASC";
		else
			$sql = "SELECT * FROM ".TABLE_PROMOTIONS." p left join ".TABLE_PROMOTIONS_DESCRIPTION." pd on p.promo_id = pd.promo_id WHERE pd.language_id = '".$language_id."' ORDER by p.promo_id DESC";
		$result = $this->db_query($sql);	
		while($promotion = mysqli_fetch_array($result))
			$promotions[] = $promotion;
		return $promotions;	
	}

function get_promotion($promo_id, $language_id)
	{
		//global $language_id;
		$sql = "SELECT * FROM ".TABLE_PROMOTIONS." p left join ".TABLE_PROMOTIONS_DESCRIPTION." pd on p.promo_id = pd.promo_id WHERE pd.language_id = '".$language_id."' AND p.promo_id = '".$promo_id."'";
		$result = $this->db_query($sql);	
		$promotion = mysqli_fetch_array($result);
		return $promotion;	
	}

//count hotels
function count_hotels($SiteID, $filter = "1")
	{
		$hotels = $this->get_hotels($SiteID, $filter);
		return count($hotels);
	}
	
//count resorts
function count_resorts($filter = "1")
	{
		$resorts = $this->get_resorts($filter);
		return count($resorts);
	}

//get hotel status, return status label
function get_hotel_status($hotel_id)
	{
		global $language_id;
		$h_status = $this->get_hInfo($hotel_id, "hotel_status", $language_id);
		$status_array = array(
				//BG
			"1" => array("���������", "�������", "��� �����", "last minute", "��������", "stop sale"),
				//EN
			"2" =>array("���������", "active", "top offer", "last minute", "promotion", "stop sale"),
				//RU
			"3" =>array("���������", "�������", "��� �����", "last minute", "��������", "stop sale"),
				//FR
			"4" =>array("���������", "active", "top offer", "last minute", "promotion", "stop sale")
		);

		return $status_array[$language_id][$h_status];
	}
	
#######################
# ADMIN functions     #
#######################

//upload files - max 524288 b = 512KB
function file_upload($files_array, $dir, $new_name = NULL)
{
	global $max_upload_size;
	
	$file_name = $files_array['name'];
	$file_tmp_name = $files_array['tmp_name'];
	$file_type = $files_array['type'];
	$file_size = $files_array['size'];
	
	//valid filetypes
	$valid_types = array('image/pjpeg', 'image/jpeg', 'image/x-png', 'image/gif', 'image/tiff','text/plain', 'application/x-shockwave-flash', 'audio/x-ms-wma', 'audio/mpeg', 'application/pdf', 'application/x-zip-compressed', 'application/x-gzip', 'video/x-ms-asf');
	
	//check directory / create directory
	if(!is_dir($dir) && !file_exists($dir)) mkdir($dir);
	
	if(!in_array($file_type, $valid_types))
		return 301;						//invalid filetype
	if($file_size > $max_upload_size)
		return 302;						//invalid filesize
	else
	if(!empty($file_name))
		if (is_uploaded_file($file_tmp_name)) 
			{
				$file_info = pathinfo($file_name);
				$f_extension = $file_info['extension'];
				$f_name = basename($file_info['basename'], ".".$f_extension);
				$f_name = str_replace(" ", "_", $f_name);
								
				$new_file_name = $f_name . "." . $f_extension;	
				$i=1;
				while(file_exists($dir.$new_file_name))
					{
						$new_file_name = $f_name."[$i].".$f_extension;
						$i++;
					}
					
				if(move_uploaded_file($file_tmp_name, $dir.$new_file_name))
					return $new_file_name;
											// success, return uploaded file name
				else return 102; 		//no file moved	
			}
		else return 103;				//no file uploaded
	else return 201;					//no file selected	
}
	
	
//add a hotel with POSTED vars
//if $id is set function will UPADATE hotel
function add_hotel()
	{
		//get hotel_id if defined
		if(func_num_args() == 1)
			$hotel_id =  func_get_arg(0);
		
		//insert all posted vars in DB
		global $_POST, $_FILES, $SiteID, $languages;
		
		$hotel_SiteID = $_POST["hotel_SiteID"];
		$hotel_category = $_POST["hotel_category"];
		$resort_id = $_POST["resort_id"];
		$hotel_infant_age = $_POST["hotel_infant_age"];
		$hotel_child_age = $_POST["hotel_child_age"];
		$currency_id = $_POST["currency_id"];
		//array
		$tour_type_id = $_POST['tour_type_id'];
		$hotel_image_old = $_POST['hotel_image_old'];
		$hotel_status = $_POST['hotel_status'];
		
		if(isset($_FILES["hotel_image"]['name']) && !empty($_FILES["hotel_image"]['name']))
			{
				//delete old image
				if(isset($hotel_id))
					$del_old_image = $this->get_hInfo($hotel_id, "hotel_image", 1);
				unlink(DIR_WS_HOTELS_UPLOAD . $del_old_image);
			
				$upload_result = $this->file_upload($_FILES["hotel_image"], DIR_WS_HOTELS_UPLOAD);
				if(is_string($upload_result))
					$hotel_image = $upload_result;
			}
		else $hotel_image = $hotel_image_old;
		
			if(isset($hotel_id))
				{
					//UPDATE
					$this->db_query("UPDATE hotels SET hotel_SiteID='".$hotel_SiteID."', hotel_category = '".$hotel_category."', resort_id = '".$resort_id."', hotel_infant_age = '".$hotel_infant_age."',  hotel_child_age = '".$hotel_child_age."', hotel_image = '".$hotel_image."', hotel_status = '".$hotel_status."', currency_id = '".$currency_id."' WHERE hotel_id = '".$hotel_id."' LIMIT 1");
					
					//set hotel types
					$this->db_query("DELETE FROM ".TABLE_HOTELS_TO_TYPES." WHERE hotel_id = '".$hotel_id."'");
					for($i=0; $i<count($tour_type_id);$i++)
						{
							$tt_id = $tour_type_id[$i];
							if($tt_id <= 0) $tt_id = 1;
							$this->db_query("INSERT INTO ".TABLE_HOTELS_TO_TYPES." SET tour_type_id = '".$tt_id."', hotel_id = '".$hotel_id."'");
						}
						
					for($i=0; $i<count($languages); $i++)
						{
							$language_id = $languages[$i]['language_id'];
							$language_key = $languages[$i]['language_key'];
							
							$hotel_title = $_POST["hotel_title"][$language_id];
							
							if($language_key == "FR")
								$hotel_title = htmlentities($_POST['hotel_title'][$language_id]);
							else 
								$hotel_title = html_entity_decode($_POST['hotel_title'][$language_id]);
									
							if(empty($hotel_title)) $hotel_title = "No title";
							$hotel_description = $_POST["hotel_description"][$language_id];
							$hotel_description_extra = $_POST["hotel_description_extra"][$language_id];
							
							//serach for new language input
							$res = mysqli_query("SELECT * FROM ".TABLE_HOTELS_DESCRIPTION." WHERE hotel_id = '".$hotel_id."' AND language_id  = '".$language_id."' LIMIT 1");
							if(mysqli_num_rows($res) > 0)
								$this->db_query("UPDATE ".TABLE_HOTELS_DESCRIPTION." SET hotel_title = '".$hotel_title."', hotel_description = '".$hotel_description."', hotel_description_extra = '".$hotel_description_extra."' WHERE hotel_id = '".$hotel_id."' AND language_id = '".$language_id."' LIMIT 1");
							else
								$this->db_query("INSERT INTO ".TABLE_HOTELS_DESCRIPTION." SET hotel_id = '".$hotel_id."', hotel_title = '".$hotel_title."', hotel_description = '".$hotel_description."', hotel_description_extra = '".$hotel_description_extra."', language_id = '".$language_id."' ");
			
						}					
				}
			else
				{
					//INSERT
					$this->db_query("INSERT INTO hotels SET hotel_SiteID='".$hotel_SiteID."', hotel_category = '".$hotel_category."', resort_id = '".$resort_id."', hotel_infant_age = '".$hotel_infant_age."',  hotel_child_age = '".$hotel_child_age."', hotel_image = '".$hotel_image."', hotel_status = '".$hotel_status."', currency_id = '".$currency_id."' ");	
					$inserted_id = mysql_insert_id();
					//create hotel directory
					$hotel_dir = DIR_WS_HOTELS_UPLOAD . $inserted_id;
					mkdir($hotel_dir, 0777);
					//change dir permissions using system SHELL command
					system("chmod 775 ".$hotel_dir);
					for($i=0; $i<count($languages); $i++)
						{
							$language_id = $languages[$i]['language_id'];
							$hotel_title = $_POST["hotel_title"][$language_id];
							if(empty($hotel_title)) $hotel_title = "No title";
							$hotel_description = $_POST["hotel_description"][$language_id];
							$hotel_description_extra = $_POST["hotel_description_extra"][$language_id];
							$this->db_query("INSERT INTO ".TABLE_HOTELS_DESCRIPTION." SET hotel_id = '".$inserted_id."', hotel_title = '".$hotel_title."', hotel_description = '".$hotel_description."', hotel_description_extra = '".$hotel_description_extra."', language_id = '".$language_id."' ");
						}

					//set hotel types
					for($i=0; $i<count($tour_type_id);$i++)
						if($tour_type_id[$i] > 0)
						{
							$tt_id = $tour_type_id[$i];
							if($tt_id <= 0) $tt_id = 1;
							$this->db_query("INSERT INTO ".TABLE_HOTELS_TO_TYPES." SET tour_type_id = '".$tt_id."', hotel_id = '".$inserted_id."'");
						}
					
				}

			//update hotel rooms
			$rti = $_POST['room_type_id'];
			
			$rtk = $_POST['room_type_key'];
			$rtt = $_POST['room_type_title'];
			$rmsb = $_POST['room_max_standart_beds'];
			$rmeb = $_POST['room_max_extra_beds'];
			$rq = $_POST['room_qty'];
			if(!isset($hotel_id) || $hotel_id == 0)
				$hotel_id = $inserted_id;
			
			for($i=0; $i<count($rtk); $i++)
				{
					if($rtk[$i] !="")
					{
						$query = $this->db_query("SELECT * FROM rooms WHERE hotel_id = '".$hotel_id."' AND room_type_key = '".$rtk[$i]."'");
						$rtk[$i] = strtoupper(str_replace(" ", "", $rtk[$i]));
						
						if(mysqli_num_rows($query) > 0)
							{ 
								if($rq[$i] == 0)
									$this->db_query("DELETE FROM rooms WHERE room_type_key = '".$rtk[$i]."' AND hotel_id = '".$hotel_id."'");
								else
									$this->db_query("UPDATE rooms SET room_max_standart_beds = '".$rmsb[$i]."', room_max_extra_beds = '".$rmeb[$i]."', room_qty = '".$rq[$i]."', hotel_id = '".$hotel_id."' WHERE room_type_key = '".$rtk[$i]."' AND hotel_id = '".$hotel_id."'");
							}
						else 
							{
								//echo "<br> INSERT INTO rooms SET room_type_id = '".$rti[$i]."', room_max_standart_beds = '".$rmsb[$i]."', room_max_extra_beds = '".$rmeb[$i]."', room_qty = '".$rq[$i]."', hotel_id = '".$hotel_id."'";
								$this->db_query("INSERT INTO rooms SET room_type_key = '".$rtk[$i]."', room_type_title = '".$rtt[$i]."', room_max_standart_beds = '".$rmsb[$i]."', room_max_extra_beds = '".$rmeb[$i]."', room_qty = '".$rq[$i]."', hotel_id = '".$hotel_id."'");
							}
					}
				}
	}

//delete selected hotel
function delete_hotel($id)	
	{
		//delete hotel from DB, delete all rooms, accomodations, galleries, photos ...
		global $_POST, $SiteID, $language_id;
		$message = "";
		//delete hotel images
		$hotel_image = $this->get_hInfo($id, "hotel_image", $language_id);
		if(file_exists(DIR_WS_HOTELS_UPLOAD . $hotel_image) && unlink(DIR_WS_HOTELS_UPLOAD . $hotel_image))
			$message.= "<li>Image <b>$hotel_image</b> deleted !";
	
		//delete hotel DB record
		$hotel_name = $this->get_hName($id, $language_id);
		$this->db_query("DELETE FROM hotels WHERE hotel_id = '".$id."' AND hotel_SiteID = '".$SiteID."' LIMIT 1");
		if(mysqli_affected_rows() > 0)
			$message.= "<li>Hotel <b>$hotel_name</b> removed from DB !";
		else
			{ 
				$message.= "Hotel <b>$hotel_name</b> not found !";
				print("<div class=\"message_error\">".$message."</div>");
				return;
			}

		$this->db_query("DELETE FROM ".TABLE_HOTELS_DESCRIPTION." WHERE hotel_id = '".$id."'");
		$this->db_query("DELETE FROM ".TABLE_HOTELS_TO_TYPES." WHERE hotel_id = '".$id."'");
			
		//delete hotel gallery
		$hotel_dir = DIR_WS_HOTELS_UPLOAD . $id;	
		if(rmdir($hotel_dir))
			$message.= "<li>Gallery deleted !";
		else $message.= "<li>Gallery NOT deleted ! Possibly not empty: ".$hotel_dir;
		
		//delete hotel periods
		$res = $this->db_query("SELECT * FROM ".TABLE_PERIODS_TYPE." WHERE hotel_id = '".$id."'");
		while($del_period = mysqli_fetch_array($res))
			$this->db_query("DELETE FROM ".TABLE_PERIODS." WHERE period_type_id = '".$del_prediod['period_type_id']."'");

		//delete hotel periods_type
		$this->db_query("DELETE FROM ".TABLE_PERIODS_TYPE." WHERE hotel_id = '".$id."'");
		
		if(mysqli_affected_rows() > 0)
			$message.= "<li>".mysqli_affected_rows()." hotel period(s) deleted !";
		 
		$res1 = $this->db_query("SELECT * FROM rooms WHERE hotel_id = '".$id."'");
		while($del_room = mysqli_fetch_array($res1))
			{
				$res2 = $this->db_query("SELECT * FROM ".TABLE_ROOMS_DESCRIPTION." WHERE room_id = '".$del_room['room_id']."'");
				//delete all prices
				while($del_description = mysqli_fetch_array($res2))
					$this->db_query("DELETE FROM ".TABLE_ROOMS_PRICES." WHERE room_description_id = '".$del_description['room_description_id']."'");
						
				//delete all descriptions
				$this->db_query("DELETE FROM ".TABLE_ROOMS_DESCRIPTION." WHERE room_id = '".$del_room['room_id']."'");
				if(mysqli_affected_rows() > 0)
					$message.= "<li>".mysqli_affected_rows()." room price(s) deleted !";
			}
		
		//delete hotel rooms	
		$this->db_query("DELETE FROM rooms WHERE hotel_id = '".$id."'");
		if(mysqli_affected_rows() > 0)
			$message.= "<li>".mysqli_affected_rows()." hotel room(s) deleted !";
			
		print("<div class=\"message_normal\">".$message."</div>");
	}
	

//add resort	
function add_resort()
	{
		//get resort_id if defined
		if(func_num_args() == 1)
			$resort_id =  func_get_arg(0);
			
		//insert all posted vars in DB
		global $_POST, $languages;
		
		$resort_name = strip_tags($_POST['resort_name']);
		$resort_description = $_POST['resort_description'];
		$tour_type_id = $_POST['tour_type_id'];
		
		if(isset($resort_id))
			{
				//UPDATE
				$this->db_query("UPDATE resorts SET resort_name = '".$resort_name."' WHERE resort_id = '".$resort_id."'");			
				for($i=0; $i<count($languages); $i++)
					{		
						$language_id = $languages[$i]['language_id'];
						$language_key = $languages[$i]['language_key'];
						
						$resort_title = strip_tags($_POST['resort_title'][$language_id]);
						
						//if($language_key == "FR")
							//$resort_title = htmlentities($_POST['resort_title'][$language_id]);
						
						//serach for new language input
						$res = mysqli_query("SELECT * FROM ".TABLE_RESORTS_DESCRIPTION." WHERE resort_id = '".$resort_id."' AND language_id  = '".$language_id."' LIMIT 1");
						if(mysqli_num_rows($res) > 0)
							$this->db_query("UPDATE ".TABLE_RESORTS_DESCRIPTION." SET resort_title = '".$resort_title."', resort_description = '".$resort_description."' WHERE resort_id = '".$resort_id."' AND  language_id = '".$language_id."'");
						else
							$this->db_query("INSERT INTO ".TABLE_RESORTS_DESCRIPTION." SET resort_id = '".$resort_id."', resort_title = '".$resort_title."', resort_description = '".$resort_description."', language_id = '".$language_id."'");
					}
			}
		else
			{ 
				//INSERT
				$this->db_query("INSERT INTO resorts SET resort_name = '".$resort_name."'");
				$inserted_id = mysql_insert_id();
				for($i=0; $i<count($languages); $i++)
					{		
						$language_id = $languages[$i]['language_id'];
						$resort_title = strip_tags($_POST['resort_title'][$language_id]);
						$this->db_query("INSERT INTO ".TABLE_RESORTS_DESCRIPTION." SET resort_id = '".$inserted_id."', resort_title = '".$resort_title."', resort_description = '".$resort_description."', language_id = '".$language_id."'");
					}
			}
			
	}

//delete selected resort
function delete_resort($id)
	{
		//delete selected resort
	}

function add_period()
	{
		//insert all posted vars in DB
		global $_POST, $hotel_SiteID, $languages;
		
		$period_type_id = (int)$_POST['period_type_id'];
		$period_type_key = $_POST['period_type_key'];
		$hotel_id = $_POST['hotel_id'];
		$date_from = date("Y-m-d", strtotime($_POST['date_from']));
		$date_to = date("Y-m-d", strtotime($_POST['date_to']));
		$min_days = $_POST['min_days'];
		$period_order = $_POST['period_order'];
		
		if($period_type_id == 0)
			{
				//check for at least 1 inserted period type title
				$valid_name = false;
				for($i=0; $i<count($languages); $i++)
					if($_POST["period_type_title"][$languages[$i]['language_id']] != "") $valid_name = true;
					
				if($valid_name)
				{	
					$this->db_query("INSERT INTO ".TABLE_PERIODS_TYPE." SET period_type_key = '".$period_type_key."', SiteID = '".$hotel_SiteID."', hotel_id = '".$hotel_id."', period_order = '".$period_order."'");		
					$inserted_id = mysql_insert_id();
					for($i=0; $i<count($languages); $i++)
						{
							$language_id = $languages[$i]['language_id'];
							$period_type_title = $_POST["period_type_title"][$language_id];
							if($period_type_title == "") $period_type_title = "no title";
							$this->db_query("INSERT INTO ".TABLE_PERIODS_TYPE_DESCRIPTION." SET period_type_id = '".$inserted_id."', period_type_title = '".$period_type_title."', language_id = '".$language_id."'");
						}
					
					$this->db_query("INSERT INTO ".TABLE_PERIODS." SET period_type_id = '".$inserted_id."', date_from = '".$date_from."', date_to = '".$date_to."', min_days = '".$min_days."'");
					//$this->db_query("UPDATE periods_type SET period_order = '".$inserted_id."' WHERE period_type_id = '".$inserted_id."'");
				}
			}
		else
			{
				$this->db_query("INSERT INTO ".TABLE_PERIODS." SET period_type_id = '".$period_type_id."', date_from = '".$date_from."', date_to = '".$date_to."', min_days = '".$min_days."'");
	
			}
		
	}

//delete selected period_type
function delete_period_type($period_type_id)
	{
		global $_POST, $SiteID;
		if(!isset($period_type_id)) return;
		$message = "<ol>";

		$sql = "DELETE FROM ".TABLE_PERIODS_TYPE_DESCRIPTION." WHERE period_type_id = '".(int)$period_type_id."'";
		$res1 = $this->db_query($sql1);
		$message.= "<li>".$sql;
				
		$sql1 = "DELETE FROM ".TABLE_PERIODS_TYPE." WHERE period_type_id = '".(int)$period_type_id."' AND SiteID = '".$SiteID."'";
		$res1 = $this->db_query($sql1);
		$message.= "<li>".$sql1;
		//if(mysqli_affected_rows($res1) > 0)
			//{
				//delete periods
				$sql2 = "DELETE FROM ".TABLE_PERIODS." WHERE period_type_id = '".(int)$period_type_id."'";
				//$this->db_query($sql2);
				$message.= "<li>".$sql2;
				
				//delete prices
				$sql3 = "SELECT * FROM ".TABLE_ROOMS_PRICES." WHERE period_type_id = '".(int)$period_type_id."'";
				$res3 = $this->db_query($sql3);
				$message.= "<li>".$sql3;
				$message.= "<hr><ul>";
				while($price = mysqli_fetch_array($res3))
					{
						//check if this price is in onother period
						$check_sql = "SELECT * FROM ".TABLE_ROOMS_PRICES." WHERE room_description_id = '".$price['room_description_id']."'";
						$check_res = $this->db_query($check_sql);
						$message.="<li>[".mysqli_num_rows($check_res)."]";
						
						if(mysqli_num_rows($check_res)==1)
							{
								$sql4 = "DELETE FROM ".TABLE_ROOMS_DESCRIPTION." WHERE room_description_id = '".$price['room_description_id']."'";
								$this->db_query($sql4);
								$message.= "<li>".$sql4;
							}
						
					}
				$message.= "</ul>";	
				$sql5 = "DELETE FROM ".TABLE_ROOMS_PRICES." WHERE period_type_id = '".(int)$period_type_id."'";
				$this->db_query($sql5);
				$message.= "<li>".$sql5."</ol>";
			//}
		
		mk_output_message("warning", $message);
	}
	
//delete selected period
function delete_period($period_id)
	{
		global $_POST;
		if(!isset($period_id)) return;
		$sql = "SELECT * FROM ".TABLE_PERIODS." p, ".TABLE_PERIODS_TYPE." pt WHERE p.period_type_id = pt.period_type_id AND p.period_id = '".$period_id."'";
		$result = $this->db_query($sql);
		$period = mysqli_fetch_array($result);
		
		$sql2 = "SELECT * FROM ".TABLE_PERIODS." WHERE period_type_id = '".$period['period_type_id']."'";
		$result2 = $this->db_query($sql2);
		
		if(mysqli_num_rows($result2) > 0)
			{
				$this->db_query("DELETE FROM ".TABLE_PERIODS." WHERE period_id = '".$period_id."' LIMIT 1");
				$this->db_query("DELETE FROM periods_description WHERE period_id = '".$period_id."'");
				mk_output_message("normal", "Period deleted !");
			}
		else mk_output_message("warning", "Nothing deleted !");
		
		//mk_output_message("warning", "Could not delete last date! Nothing deleted !<hr>".$sql2);
		
	}


//add prices for selectet rooms in hotel form selected periods
function add_prices($hotel_id)
	{
		global $_POST, $SiteID, $languages;
		if(!isset($hotel_id)) return;
		
		$room_id = $_POST['room_id'];
		$room_description_titles = $_POST['room_description_titles'];
		$accommodation_type = $_POST['accommodation_type'];
		$room_standart_beds = $_POST['room_standart_beds'];
		$room_extra_beds = $_POST['room_extra_beds'];
		$price_value = $_POST['price_value'];
		$room_ep_type = $_POST['room_ep_type'];
		
		for($i=0; $i<count($room_id); $i++)
			{
				
				$check_price = false;
				for($j=0; $j<count($accommodation_type[$i]); $j++)
				{
					$keys = array_keys($price_value[$i]);

					//insert room descriptions
					//$sql = "INSERT INTO ".TABLE_ROOMS_DESCRIPTION." SET room_id = '".$room_id[$i]."', room_standart_beds = '".$room_standart_beds[$i][$j]."', room_extra_beds = '".$room_extra_beds[$i][$j]."', accommodation_type = '".$accommodation_type[$i][$j]."', room_ep_type = '".$room_ep_type[$i][$j]."'";
					$this->db_query("INSERT INTO ".TABLE_ROOMS_DESCRIPTION." SET room_id = '".$room_id[$i]."', room_standart_beds = '".$room_standart_beds[$i][$j]."', room_extra_beds = '".$room_extra_beds[$i][$j]."', accommodation_type = '".$accommodation_type[$i][$j]."', room_ep_type = '".$room_ep_type[$i][$j]."'");
					//echo "<hr>".$sql;
					
					$inserted_description_id = mysql_insert_id();	
					
					//insert room titles
					for($l=0; $l<count($languages); $l++)
						{
							//$titles_sql = "INSERT INTO ".TABLE_ROOMS_DESCRIPTION_TITLES." SET room_description_id = '".$inserted_description_id."', language_id = '".$languages[$l]['language_id']."', room_description_title = '".$room_description_titles[$i][$l][$j]."'";
							$this->db_query("INSERT INTO ".TABLE_ROOMS_DESCRIPTION_TITLES." SET room_description_id = '".$inserted_description_id."', language_id = '".$languages[$l]['language_id']."', room_description_title = '".$room_description_titles[$i][$l][$j]."'");
							//echo "<li>".$titles_sql;
						}
						
					for($k=0; $k<count($price_value[$i]); $k++)
						{
								$this->db_query("INSERT INTO ".TABLE_ROOMS_PRICES." SET room_description_id = '".$inserted_description_id."', period_type_id='".$keys[$k]."', price_value = '".$price_value[$i][$keys[$k]][$j]."'");
						}
					
				}
			}
			
	}
	
//edit selected price	
function edit_price($room_description_id)
	{
		global $_POST, $languages;

		$room_description_titles = $_POST['room_description_titles'];
		$accommodation_type = $_POST['accommodation_type'];
		$room_standart_beds = $_POST['room_standart_beds'];
		$room_extra_beds = $_POST['room_extra_beds'];
		$price_value = $_POST['price_value'];
		$room_ep_type = $_POST['room_ep_type'];
		$keys = array_keys($price_value);
		
		//update room descriptions
		$this->db_query("UPDATE ".TABLE_ROOMS_DESCRIPTION." SET room_standart_beds = '".$room_standart_beds."', room_extra_beds = '".$room_extra_beds."', accommodation_type = '".$accommodation_type."', room_ep_type = '".$room_ep_type."' WHERE room_description_id = '".$room_description_id."'");		
		
		//update description titles
		for($i=0; $i<count($languages); $i++)
			{
				if(!empty($room_description_titles[$i]))
					$this->db_query("UPDATE ".TABLE_ROOMS_DESCRIPTION_TITLES." SET room_description_title = '".$room_description_titles[$i]."' WHERE room_description_id = '".$room_description_id."' AND language_id = '".$languages[$i]['language_id']."'");
			}
		
		//update room prices
		for($i=0; $i<count($keys); $i++)
			{
				$period_type_id = $keys[$i];
				$s_query = "SELECT * FROM ".TABLE_ROOMS_PRICES." WHERE room_description_id = '".$room_description_id."' AND period_type_id='".$period_type_id."'";
				$s_result = $this->db_query($s_query);
				if(mysqli_num_rows($s_result) > 0)
					$query = "UPDATE ".TABLE_ROOMS_PRICES." SET price_value = '".$price_value[$period_type_id]."' WHERE room_description_id = '".$room_description_id."' AND period_type_id='".$period_type_id."'";
				else
					$query = "INSERT INTO ".TABLE_ROOMS_PRICES." SET price_value = '".$price_value[$period_type_id]."', room_description_id = '".$room_description_id."', period_type_id='".$period_type_id."'";
					
				$this->db_query($query);
			}		
		if(mysqli_affected_rows())
			mk_output_message("normal", "Price edited !");
	}
	
//delete selected price
function delete_prices($room_description_id)
	{
		if(isset($room_description_id) && $room_description_id > 0 )
			{
				$this->db_query("DELETE FROM ".TABLE_ROOMS_DESCRIPTION_TITLES." WHERE room_description_id = '".$room_description_id."'");
				$this->db_query("DELETE FROM ".TABLE_ROOMS_DESCRIPTION." WHERE room_description_id = '".$room_description_id."'");
				$deleted_rows = mysqli_affected_rows();
				$this->db_query("DELETE FROM ".TABLE_ROOMS_PRICES." WHERE room_description_id = '".$room_description_id."'");
			}
		if($deleted_rows > 0)
			mk_output_message("normal", "Deleted ".$deleted_rows." record(s)");
	}
//add extra payment
function add_extra_payment()
	{
		global $_POST;
		
		$ep_type_id = $_POST['ep_type_id'];
		$period_type_id = $_POST['period_type_id'];
		$room_description_id = $_POST['room_description_id'];
		$accommodation_type_id = $_POST['accommodation_type_id'];
		$hotel_id = $_POST['hotel_id'];
		$ep_price = $_POST['ep_price'];
		$sql = "INSERT INTO ".TABLE_EXTRA_PAYMENTS." SET period_type_id = '".$period_type_id."', room_description_id = '".$room_description_id."', accommodation_type_id = '".$accommodation_type_id."', ep_type_id = '".$ep_type_id."', hotel_id = '".$hotel_id."', ep_price = '".$ep_price."'";
		if($ep_price > 0)
			{
				$this->db_query($sql);
				return true;
			}
		else return false;
	}

//delete extra payment
function delete_extra_payment($ep_id)
	{
		if(isset($ep_id))
			{
				$sql = "DELETE FROM ".TABLE_EXTRA_PAYMENTS." WHERE ep_id = '".$ep_id."' LIMIT 1";
				$this->db_query($sql);
				return true;
			}
		else return false;
	}
	

//add hotel promotion
function add_promotion($hotel_id, $edit_id = 0)
	{
		global $_POST, $languages;
		
		$date_from = $_POST['date_from'];
		$date_to = $_POST['date_to'];
		$sort_n = $_POST['sort_n'];
		$promo_status = $_POST['promo_status'];
		$message = "";
		//count hotel promotions
		$h_promotions = $this->get_hotel_promotions($hotel_id);
		$count_promotions = count($h_promotions)+1;
		
		if($edit_id >0)
			//update promotion
			$sql = "UPDATE ".TABLE_PROMOTIONS." SET hotel_id = '".$hotel_id."', promo_date_from = '".$date_from."', promo_date_to = '".$date_to."', sort_n = '".$sort_n."', promo_status = '".$promo_status."' WHERE promo_id = '".$edit_id."'";
		else
			//insert into promotions
			$sql = "INSERT INTO ".TABLE_PROMOTIONS." SET hotel_id = '".$hotel_id."', promo_date_from = '".$date_from."', promo_date_to = '".$date_to."', sort_n = '".$count_promotions."', promo_status = '".$promo_status."'";
		
		$this->db_query($sql);
		
		if($edit_id >0)
			{
				for($i=0; $i<count($languages); $i++)
					{
						$language_id = $languages[$i]['language_id'];
						$promo_title =  $_POST['promo_title'][$language_id];
						$promo_description =  $_POST['promo_description'][$language_id];	
						
						$sql2 = "UPDATE ".TABLE_PROMOTIONS_DESCRIPTION." SET language_id = '".$language_id."', promo_title = '".$promo_title."', promo_description = '".$promo_description."' WHERE promo_id = '".$edit_id."' AND language_id = '".$language_id."'";
						if(!empty($promo_title))
							$this->db_query($sql2);
					}
				$message = "Promo #".$edit_id." edited !";
			}
		else
			{
				$promo_id = mysql_insert_id();
				//insert promo descriptions
				for($i=0; $i<count($languages); $i++)
					{
						$language_id = $languages[$i]['language_id'];
						$promo_title =  $_POST['promo_title'][$language_id];
						$promo_description =  $_POST['promo_description'][$language_id];	
						
						$sql2 = "INSERT INTO ".TABLE_PROMOTIONS_DESCRIPTION." SET promo_id = '".$promo_id."', language_id = '".$language_id."', promo_title = '".$promo_title."', promo_description = '".$promo_description."'";
						if(!empty($promo_title))
							$this->db_query($sql2);
					}
				$message = "New promo added !";
			}
			
		mk_output_message("normal", $message);
		
	}
	
//delete promotion
function delete_promotion($del_id)
	{
		$sql1 = "DELETE FROM ".TABLE_PROMOTIONS." WHERE promo_id = '".$del_id."'";
		$sql2 = "DELETE FROM ".TABLE_PROMOTIONS_DESCRIPTION." WHERE promo_id = '".$del_id."'";
		if($this->db_query($sql1))
			{
				if($this->db_query($sql2)) return true;
				else return false;
			}
		else return false;
	} 	
	
//add / edit hotel conditions
function edit_hotel_conditions($hotel_id)
	{
		global $_POST, $languages;
		$hotel_conditions = $_POST['hotel_conditions'];
		for($i=0; $i<count($languages); $i++)
			{
				$lang_id = $languages[$i]['language_id'];
				$sql = "UPDATE ".TABLE_HOTELS_DESCRIPTION." SET hotel_conditions = '".$hotel_conditions[$lang_id]."' WHERE hotel_id = '".$hotel_id."' AND language_id = '".$lang_id."'";
				$this->db_query($sql);
			}
		
		mk_output_message("normal", "Hotel conditions edited !");

	}	
	
	
		
}
// END CLASS hotels
	
	
#######################
# TESTING #
#######################

 //$hotels = new hotels(); 
 //$row = $hotels->get_hotel(1); 


?>
