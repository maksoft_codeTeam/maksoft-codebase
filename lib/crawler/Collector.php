<?php 
namespace Maksoft\Crawler;


class Collector implements \SplSubject 
{ 
    protected $crawlers = array();

    private $storage;

    private $status=array();

    protected $agent='Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0';

    protected $results=100;

    protected $db;


    public function __construct()
    {
        $this->storage = new \SplObjectStorage();
    }

    public function init($kwargs)
    {
        $this->kwargs = $kwargs;
    }

    public function notify()
    {
        $crawlers = $this->storage->count(); //4
        $total_words = count($this->kwargs); //50
        $keywords_per_proxy = floor($total_words/$crawlers); //12
        echo 'crawlers: '.$crawlers . '<br> words: '.$total_words.'<br> keywords per crawler: '.$keywords_per_proxy.'<br>';
        $left = $keywords_per_proxy * $valid_proxies - $total_words;
        $i = 0;
        $data = array();
        $kwargs = array_chunk($this->kwargs, $keywords_per_proxy);
        foreach($this->storage as $observer){
            $this->setStatus($kwargs[$i]);
            $data[] = $observer->update($this);
            $i++;
        }
        return $data;
    }

    protected function check_url($url)
    {
        
        $get_url_headers = get_headers($url);
        
        if ($get_url_headers[3] == "Location: http://www.maksoft.net/error_404.php") {
            $status = 0; // nevaliden url 
        } else {
            $status = 1; // validen url 
        }
        return $status;
    }

    public function setDatabase($db)
    {
        $this->db = $db;
    }

    public function update_etiketi($keyword)
    {
        $position = (object) $keyword;
        $stmt = $this->db->prepare("SELECT DISTINCT(n) FROM etiketi_n WHERE eID=:eID");
        $stmt->bindValue(":eID", $position->results['keyword']['eID']);
        $stmt->execute();
        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $link_count = count($res);
        $sql = "UPDATE etiketi 
                SET google_pos=:position,
                google_pr = :rank,
                google_result= :all_results, 
                active=:active,
                links=:links, 
                date_checked=NOW(), 
                exact_url=:url
                WHERE eID=:eID LIMIT 1 ";
        $stmt = $this->db->prepare($sql); 
        $stmt->execute(array(
            ':position'=>$position->results->position , 
            ':rank'=>$position->results->position, 
            ':all_results'=>$position->results->position, 
            ':active'=>$this->check_url($position->results->exact_url), 
            ':links'=>$link_count, 
            ':url'=>$position->results->exact_url, 
            ':eID'=>$position->results['keyword']['eID'], 
        ));
        echo '<br>Succesfull update -> '.
            $stmt->rowCount().' eID '.$position->results['keyword']['eID'] .'<br>';
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($keywords)
    {
        $this->status = array($keywords, $this->results);
    }

    public function attach( \SplObserver $observer )
    {
        $this->storage->attach( $observer );
    }
 
    public function detach( \SplObserver $observer )
    {
        $this->storage->detach( $observer );
    }

    public function setAgent($agent)
    {
        $this->agent = $agent;
    }

    public function setResults($results)
    {
        $this->results = $results;
    }
}

?>
