<?php
namespace Maksoft\Crawler;


class Spider implements \SplObserver
{
    protected $agent='Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0';
    protected $kwargs;
    protected $dom;
    protected $results=200;
    protected $matches=array();
    protected $proxy='';
    protected $agents;

    /**
     * Constructor
     *
     * take assoc array keyword => domain
     *
     * @param array -> assoc array [
     *                         0 => ['keyowrd'  => $keyword,
     *                               'domain'   => $domain],
     *                         1 => ['keyowrd1' => $keyword1,
     *                               'domain1'  => $domain1],
     *                      ];
     * @return (null) null
    */
    public function __construct($proxy, $search_url)
    {
        $this->proxy = $proxy;
        $this->url = $search_url;
        $this->dom = new \DOMDocument;
        $this->agents = $this->loadAgents();
    }

    protected function loadAgents()
    {
        $fp = fopen(__DIR__.'/agents.json', 'r');
        $jsonAgents = fread($fp, filesize(__DIR__.'/agents.json'));
        fclose($fp);
        return json_decode($jsonAgents);
    }

    public function update( \SplSubject $SplSubject )
    {
        $status = $SplSubject->getStatus();
        $this->kwargs = $status[0];
        $this->results = $status[1];
        return $this->parse();
    }

    public function setProxy($proxy)
    {
        $this->proxy = $proxy;
    }

    public function getProxy()
    {
        return $this->proxy;
    }

    public function setResults($google_results)
    {
        if (!filter_var($google_results, FILTER_VALIDATE_INT) === false and $google_results < 50) {
            return;
        }
        $this->results = $google_results;
    }

    protected function build_url($keyword)
    {
        $get_data = array('q'=>urlencode($keyword), 'num'=>$this->results);
        if($this->proxy == ''){
            return $this->url.http_build_query($get_data);
        }
        return $this->proxy.$this->url.http_build_query($get_data);
    }

    protected function page_source($url)
    {
        $k = array_rand($this->agents);
        $this->agent = $this->agents[$k];
        echo '<br> used agent: '. $this->agent . '<br>';
        echo '<br> url: '. $url.'<br>';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_ENCODING , "gzip");
        curl_setopt($ch, CURLOPT_VERBOSE,1);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
        curl_setopt($ch, CURLOPT_REFERER,$this->getDomainFromUrl($url));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_POST,0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 20);
        return curl_exec($ch);
    }

    protected function update_etiketi()
    {

    }

    /**
     * Spider:parse
     *
     * parse keywords from constuctor
     *
     * @param array -> assoc array [
     *                         0 => ['keyowrd'  => $keyword,
     *                               'domain'   => $domain],
     *                         1 => ['keyowrd1' => $keyword1,
     *                               'domain1'  => $domain1],
     *                      ];
     * @return (array) array(4) {
     *                               ["adwords"]=>
     *                               int(0), int(1) True/False
     *                               ["results"]=>
     *                               array(2) {
     *                                 [0]=>
     *                                 string(11) "1 250 000" all results
     *                                 [1]=>
     *                                 string(1) "1" // coef
     *                               }
     *                               ["position"]=>
     *                               int(38) first position
     *                               ["exact_url"]=>
     *                               int(0) exact url or other
     *                           }
    */
    public function parse()
    {
        $data = array();
        foreach($this->kwargs as $query){
            $gResult = $this->gResult($query);
            $tmp['results']['keyword'] = $query;
            if($gResult !== 0){
              $tmp['results']['position'] = $gResult;
              continue;
            }
            $url = $this->build_url($query['Name']);
            $p_src = $this->page_source($url);
            $this->dom->loadHTML($p_src);
            printf('<pre>%s</pre>', $p_src);
            $pos = 0;
            $tmp = array();
            $domain = @$this->getDomainFromUrl($query['url']);
            $tmp['getDomainFromUrl'] = $domain;
            $tmp['results'] = $this->getAllResults($this->dom);
            $tmp['results']['adwords'] = $this->getAdwordsResults($this->dom, $domain);

            $tmp['results']['searched_url'] = $query['url'];

            if($this->dom->getElementsByTagName('cite')->length < 1){
              $tmp['results']['position'] = -4;
              continue;
            }
            $tmp['results']['position'] = $pos;
            $tmp['results']['page_rank'] = $this->page_rank($query['url']);
            foreach($this->dom->getElementsByTagName('cite') as $link) {
                $tmp['links'][] = $link->nodeValue;
                if (stristr($link->nodeValue, $domain)) {
                    $tmp['results']['matched_url'] = $link->nodeValue;
                    $tmp['results']['position'] = $pos + 1;
                    $tmp['results']['exact_url'] = ($link->nodValue == 'http://'.$query['url'] ? 1 : 0);
                    break;
                }
                $pos++;
            }
            $data[] = $tmp;
            $sec = mt_rand(2, 6);
            echo '<br> sleep for: '.$sec.' seconds.<br>';
            sleep($sec);
        }
        return $data;
    }

    protected function gResult($keyword)
    {
        if(strlen($keyword['Name']) < 4 or strlen($keyword['Name']) > 70){
            return -1;
        }
        if(count(explode('.', $keyword['url'])) <= 1){
            return -2;
        }
        return 0;
    }
    /**
     *
     * @param DOMDocument object
     * @param string url
     * @return 0 no adwords // 1 adwords
    */
    protected function getAdwordsResults($dom, $url)
    {
        $xpath = new \DOMXpath($dom);
        $adwords = @$xpath->query('//*[@class="_WGk"]');
        foreach($adwords as $ad){
            if(stripos($ad->nodeValue, $url)){
                return 1;
            }
        }
        return 0;
    }

    protected function str_to_int($str)
    {
        $tmp = '';
        $string = str_split($str);
        foreach($string as $str){
            if(is_integer($str)){
                $tmp .=$str;
            }
        }
        return $tmp;
    }


    protected function getAllResults($dom)
    {
        $all_res = $dom->getElementById("resultStats");
        $results = $all_res->nodeValue;
        preg_match_all("/(\d+)/", $results, $output_array);
        $ret['all_results'] = implode('', $output_array[0]);

        $coefficient = "1";

        if (($gResultOut >= 500000) && ($gResultOut <= 1000000)) {
            $coefficient = "2";
        }
        if ($gResultOut >= 1000000) {
            $coefficient = "3";
        }
        $ret['coeff'] = $coefficient;
        return $ret;
    }

    public function getDomainFromUrl($url_addr)
    {
        $url = str_ireplace('www.', '', parse_url($url_addr, PHP_URL_HOST));
        if(!$url){
            $url = 'http://'.$url_addr;
            return str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
        }
        return $url;
    }

    protected function fch($csm)
    {
        if($csm < 0)
            $csm += 4294967296.0;

        $a = (int)fmod($csm, 10);
        $t = 1;
        $b = (int)($csm / 10);
        while($b) {
            $c = $b % 10;
            if($t)
                $c = (int)($c / 5) + ($c * 2) % 10;
            $b = (int)($b / 10);
            $a += $c;
            $t ^= 1;
        }

        $a = 10 - $a % 10;
        if($a == 10)
            return ord('0');

        if($t)
            return ord('0') + (int)(($a & 1 ? $a + 9 : $a) / 2);

        return ord('0') + $a;
    }

    protected function checksum($str)
    {
        if(strlen($str) == 0)
            return 0x1000;

        /* the floating point hacks are due to PHP's bugs when handling integers */

        $a = 5381.0;
        for($i = 0; $i < strlen($str); $i++)
            $a = fmod($a + ($a * 32) + ord($str[$i]), 4294967296.0);
        if($a > 2147483647.0)
            $a -= 4294967296.0;
        $a = (int)$a;

        $b = 0.0;
        for($i = 0; $i < strlen($str); $i++)
            $b = fmod(($b * 64) + ($b * 65536) - $b + ord($str[$i]), 4294967296.0);
        if($b > 2147483647.0)
            $b -= 4294967296.0;
        $b = (int)$b;

        $a = (($a >> 6) & 0x3ffffc0) | (($a >> 2) & 0x3f);
        $c = (($a >> 4) & 0x3ffc00) | ($a & 0x3ff);
        $d = (($c >> 4) & 0x3c000) | ($c & 0x3fff);
        $c = ((($d & 0x3c0) << 4) | ($d & 0x3c)) << 2;
        $a = $b & 0x0f0f;
        $e = $b & 0x0f0f0000;
        $b = (($d & 0xffffc000) << 4) | ($d & 0x3c00);

        return ($b << 10) | $c | $a | $e;
    }

    protected function page_rank($page)
    {
        $PR_SERVER = "http://toolbarqueries.google.com/tbr?client=navclient-auto&features=Rank&q=info:";
        $csm = $this->checksum($page);
        $url = sprintf($PR_SERVER . "%s&ch=7%c%u\n", $page, $this->fch($csm), $csm);
        $data = $this->page_source($url);
        $pos   = strpos($data, "Rank_");
        if ($pos === false) {
            return 0;
        } else {
            $pagerank = $data;
            return $pagerank;
        }
    }
}


?>
