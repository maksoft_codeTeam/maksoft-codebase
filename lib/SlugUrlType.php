<?php
require_once __DIR__.'/AbstractUrlResolver.php';


class SlugUrlType extends AbstractUrlResolver
{
    public function generate()
    {
        $slug = $this->buildSlug()->getSlug();

        if ($lang = $this->request->query->get('lang')) {
            return sprintf("/%s/%s%s", urlencode($lang), $slug, $this->getPrefix()); 
        }

        return sprintf("/%s%s", $slug, $this->getPrefix()); 
    }

    public function buildSlug()
    {
        if($this->isHomepage()) {
            $this->prefix = '';
            $this->slug = '';
            return $this;
        }

        $slug = empty($this->page->slug) ? $this->page->n : $this->page->slug; 

        $this->slug = $slug;
        return $this;
    }
}
