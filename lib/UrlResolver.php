<?php
use Symfony\Component\HttpFoundation\Request;


require_once __DIR__.'/lib_page.php';


class UrlResolver
{
    protected $data = array();
    protected $request, $page;

    public function __construct(\AbstractUrlResolver $url)
    {
        $this->url = $url;
    }

    public function __toString()
    {
        return $this->url;
    }


    public function generate()
    {
        return $this->url->generate();
    }

}
