<?php
define("FLOWPLAYER_DIR_ROOT", "lib/media/flowplayer/");
//define the flowplayer library
define("FLOWPLAYER_LIBRARY_PATH", FLOWPLAYER_DIR_ROOT."flowplayer-3.2.4.js");
//define the flowplayer version
define("FLOWPLAYER_VERSION_PATH", FLOWPLAYER_DIR_ROOT."flowplayer-3.2.5.swf");
//define flowplayer plugins path
define("FLOWPLAYER_DIR_PLUGINS", "lib/media/flowplayer/plugins/");

//define latest flowplayer plugins
define("FLOWPLAYER_PLUGIN_AUDIO", FLOWPLAYER_DIR_PLUGINS."flowplayer.audio-3.2.1.swf");
?>