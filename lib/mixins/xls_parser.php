<?php
require_once '/hosting/maksoft/maksoft/lib/mixins/Excell/reader.php';
require_once "/hosting/maksoft/maksoft/lib/Database.class.php";

if(!isset($__template_tags))
    $__template_tags = array('sku', 'content', 'title', 'textStr');
$update_query = "UPDATE pages SET textStr=:content WHERE Name=:sku";
$db = new Database();
$excel = new Spreadsheet_Excel_Reader();
$stmt = $db->prepare($update_query);
$excel->setOutputEncoding('cp1251');
$excel->read('ofis.xls');
if(count(array_diff($excel->sheets[0]['cells'][1], $__template_tags)) == 0){
    $x = 2; //from second row to end
    $i = 1; //counter for columns
    while ($x <= $excel->sheets[0]['numRows']) {
	for ($i = 1; $i < count($excel->sheets[0]['cells'][1]) + 1; $i++):
	    $stmt->bindValue(':'.$__template_tags[$i-1],  $excel->sheets[0]['cells'][$x][$i]);
	endfor;
	$x++;
	$stmt->execute();
	sleep(2);
    }
    try {
    } catch (PDOException $e) {
	echo $e->getMessage();
    }
}
