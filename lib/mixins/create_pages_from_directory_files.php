<?php
include '../Database.class.php';

$db = new Database();

function _join($path, $file){
    return $path.'/'.$file;
}

function insert_page($pPage, $name, $title, $image_id)
{
    global $db;
    $page_insert_query = "INSERT INTO `pages`
                                       (ParentPage, Name, title,
                                        url_rules, toplink, show_link,
                                        image_allign, SiteID, imageNo)
                          VALUES (:Ppage, :name, :title, 0, 0,0, 2, 1007, :image_id)";

    $stmt = $db->prepare($page_insert_query);
    $stmt->bindValue(":Ppage", $pPage);
    $stmt->bindValue(":name", $name);
    $stmt->bindValue(":title", $title);
    $stmt->bindValue(":image_id", $image_id);
    $stmt->execute();
    return $db->lastInsertId();
}

function insert_image($img_name, $path, $siteID=1007)
{
    global $db;
    $img_insert_query = "INSERT INTO `images` (imageName, image_src, image_dir, image_date_added)
                         VALUES (:img_name,:img_src, :path, :date)";
    $stmt = $db->prepare($img_insert_query);
    $stmt->bindValue(":img_name", $img_name);
    $stmt->bindValue(":img_src", $path);
    $stmt->bindValue(":path", $siteID);
    $stmt->bindValue(":date", date("Y-m-d"));
    $stmt->execute();
    return $db->lastInsertId();
}

class DirCrawlerException extends Exception{}

$img_id = 0;
$data = array();
$img_path = '/web/images/1007/products';

function img_path($path){
    $path = explode('/', $path['dirname']);
    return implode('/', array_slice($path, 4));
}   
    
function dir_parser($p_Page=999, $path, &$data)
{
    global $img_id, $img_path;
    $file = pathinfo($path);
    if(substr($file['basename'],0,1) != '.')
        if(is_dir($path)){
            $l_id = insert_page($p_Page, $file['basename'], $file['basename'], 1007);
            $data[$file['basename']]['id'] = $l_id;
            $files = array_slice(scandir($path, 1), 2);
            foreach($files as $k){
                dir_parser($l_id, _join($path, $k), $data);
            }
        }else{
            if(stristr($file['basename'], 'jpg')){
                if(strstr($file['filename'], '.')){
                    $file_name = explode('.', $file['filename']);
		    $file_name = $file_name[0];
                    $img_id = insert_image($file['basename'],_join(img_path($file), $file['basename']), 1007);
                    if(array_key_exists($file_name, $data)){
                        $page_id = insert_page($data[$file_name]['id'], $file['filename'], $file['filename'], $img_id);
                        $data[$file['filename']]['id'] = $page_id;
                    }else{
			$img_id = insert_image($file_name, _join(img_path($file), $file_name.'.jpg'), 1007);
                        $page_id = insert_page($p_Page, $file_name, $file_name, $img_id);
                        $data[$file_name]['id'] = $page_id;
                        $page_id = insert_page($data[$file_name]['id'], $file['filename'], $file['filename'], $img_id);
                        $data[$file['filename']]['id'] = $page_id;
                    }
                }else{
                    if(!array_key_exists($file['filename'], $data))
                        echo $file['filename'].'<br>';
	    
			$img_id = insert_image($file['basename'], _join(img_path($file), $file['basename']), 1007);
                        $page_id = insert_page($p_Page, $file['filename'], $file['filename'], $img_id);
                        $data[$file['filename']]['id'] = $page_id;
                }
                return $data;
            }
        }
        return $data;
}

# TODO slug == $file['basename']
$path = '/hosting/maksoft/maksoft/web/images/upload/1007/products/7';
$path = '/hosting/maksoft/maksoft/web/images/upload/1007/products/7/7_1/1.1_Mom4eta_Bluzi_rizi_potnici/BS270045_BLUZA_BEBE/';
$parent_page = 19333932;
var_dump(img_path(pathinfo($path)));
$files = scandir($path, 1);
var_dump($files);
foreach($files as $folder){
    if(substr($folder, 0, 1) == '.')
	continue;
    if(!is_dir(_join($path, $folder))) continue;
    dir_parser($parent_page, _join($path, $folder), $data);
}

