<?php
include '../Database.class.php';

$db = new Database();

function _join($path, $file){
    return $path.'/'.$file;
}

function insert_page($pPage, $name, $title, $image_id, $siteID)
{
    global $db;
    $page_insert_query = "INSERT INTO `pages`
                                       (ParentPage, Name, title,
                                        url_rules, toplink, show_link,
                                        image_allign, SiteID, imageNo)
                          VALUES (:Ppage, :name, :title, 0, 0,0, 2, :siteID, :image_id)";

    $stmt = $db->prepare($page_insert_query);
    $stmt->bindValue(":Ppage", $pPage);
    $stmt->bindValue(":name", $name);
    $stmt->bindValue(":title", $title);
    $stmt->bindValue(":image_id", $image_id);
    $stmt->bindValue(":siteID", $siteID);
    $stmt->execute();
    $page_id = $db->lastInsertId();
    updatePageSort($page_id, $db);
    return $page_id;
}

function updatePageSort($page_n, $db)
{
    $update_query = "UPDATE pages SET sort_n=:n WHERE n=:n";
    $stmt = $db->prepare($update_query);
    $stmt->execute(array(":n"=>$page_n));
}

function insert_image($img_name, $path, $siteID=1007)
{
    global $db;
    $img_insert_query = "INSERT INTO `images` (imageName, image_src, image_dir, image_date_added)
                         VALUES (:img_name,:img_src, :path, :date)";
    $stmt = $db->prepare($img_insert_query);
    $stmt->bindValue(":img_name", $img_name);
    $stmt->bindValue(":img_src", $path);
    $stmt->bindValue(":path", $siteID);
    $stmt->bindValue(":date", date("Y-m-d"));
    $stmt->execute();
    return $db->lastInsertId();
}

class DirCrawlerException extends Exception{}

$img_id = 0;
$data = array();

function img_path($path){
    $path = explode('/', $path['dirname']);
    return implode('/', array_slice($path, 4));
}   
    
function dir_parser($p_Page=999, $path, &$data)
{
    $file = pathinfo($path);
    if(substr($file['basename'],0,1) != '.'){
	if(stristr($file['basename'], 'JPG')){
	    $img_id = insert_image($file['basename'], _join(img_path($file), $file['basename']), 1007);
	    $page_id = insert_page($p_Page, $file['filename'], $file['filename'], $img_id);
	    echo 'inserted page -> '.$page_id. ' ' . 'inserted_image '. $img_id.PHP_EOL;
	    $data[$file['filename']]['id'] = $page_id;
	    return $data;
	}
    }
    return $data;
}

# TODO slug == $file['basename']
$path = '/hosting/maksoft/maksoft/web/images/upload/1012/man-fashion/';
$img_path = $path;
$parent_page = 19336496;
$files = scandir($path, 1);
foreach($files as $folder){
    if(substr($folder, 0, 1) === '.')
	continue;
    if(is_dir(_join($path, $folder))) continue;
    dir_parser($parent_page, _join($path, $folder), $data);
}

