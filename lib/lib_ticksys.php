<?php
// http://www.unrealsoft.net/products/ticksys/
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_site >> lib_page >> xml2array >> lib_rss

require_once('lib_page.php');
include $_SERVER['DOCUMENT_ROOT']."/lib/reservation_system/fields.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/lib_rss.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation_system/default_messages.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/prettytable.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/xml2array.php";

class ticksys extends rss {

var $company=1;
var $ticksys_url;
var $ticksys_pass;
var $cities;
var $admin_mail;
var $online_discount;
var $ticksys_hoursBefore;
var $active;
var $dev_debug;
var $min_free_places;
var $cities_cache = 10;
var $biletni_kasi;
private $_prices = array();
private $_xml;
private $counter = 0;
public $template_data;
private $ticksys_data;
public $template='modal';

    function __construct($company=1) { // constructor
        parent::__construct();
        $this->_xml = new DomDocument();
        $this->company = $company;
        //$this->ticksys_url = "http://78.128.1.70:2008/?command=";
        $this->ticksys_url = "http://78.128.1.75:2008/?command=";
        $this->ticksys_pass = "12TfrS";
        $this->ticksys_hoursBefore = 6; //�����(� ������) �� �������� �� ����� '���� �����'
        //$this->admin_mail = 'mk@maksoft.net'; // bcc all notifications to this email- used in ajax_handlers
        $this->online_discount = 10;
        $this->min_free_places = 10; // min place to activate online sale

        $cities_arr = $this->get_cities();

        foreach($cities_arr as $city) {
            $this->cities[$city['ID']] = $city['NAME'];
        }


        //$this->dev_debug = false;
    }

    function unserializeForm($strfromPOST) {
        $array = "";
        $returndata = "";
        $strfromPOST=urldecode($strfromPOST);
        $strArray = explode("&", $strfromPOST);
        $i = 0;
        foreach ($strArray as $str)
        {
            $array = explode("=", $str);
            $returndata[(string)$array[0]] = (string)$array[1];
            $i++;
        }
        return($returndata);
    }

    function GET_SCHEDULES($first_city, $last_city, $data=null, $days = 7) {

        if($data == null) $data = time();   // timestamp

        echo('
        <h2>���������� '.$this->cities[$first_city].' - '.$this->cities[$last_city].'</h2>
        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="schedule">
          <thead>
          <tr>
            <th>����</th>
            <th>�����������</th>
            <th>T������� �� '.$this->cities[$first_city].'</th>
            <th>������</th>
            <th>������</th>
           </tr>
          </thead>
          <tbody>
        ');

        for($i = 0; $i<=$days; $i++) {

            $curent_data = $data+$i*60*60*24;

            $todayIs  = date("d.m.Y", time());
            $data_str = date("d.m.Y", $curent_data);


            $url = $this->ticksys_url."GET_SCHEDULES&PASS=".$this->ticksys_pass."&FOR_DATE=".$data_str."&FIRST_CITY=".$first_city."&LAST_CITY=".$last_city."&company=".$this->company."&MIN_FREE_PLACES=0&JSON=T";
            if($this->_user["ID"] ==1424){
                echo $url;
            }
            #echo '<br><b>'.$url.'</b><br>';
            if(($this->_user['AccessLevel'] >= 4)  && ($this->dev_debug)) echo $url."<br>";
            $xml = $this->parse_json($url);

                foreach($xml->RESULT->COMPANY->SCHEDULE as $row) {    // Company  za staroto IP

                        if ( ($this->active) || ($this->_user['AccessLevel'] > 1)  ){   // $_SESSION["user"]
                            $course_id = $row->KURS_ID;
                            $urlto=$this->_site["primary_url"];

                            $date1 = new DateTime();
                            $date2 = new DateTime($data_str.'T'.$row->START_TIME);
                            $diff = $date2->diff($date1);
                            $hours = $diff->h;
                            $hours = $hours + ($diff->days*24);

                            if($course_id == 0) {
                                $sch_id = $row->ID;
                                $price_id = $row->PRICES->PRICE_ID;
                                $price_type_id = $row->PRICES->PRICE_TYPE;

                                $course_id = $this->init_kurs($data_str, $sch_id, $price_id, $price_type_id);
                            }

                            if( ($course_id > 0 or $this->dev_debug) && ($hours>=$this->ticksys_hoursBefore) && ($row->FREE_PLACES >= $this->min_free_places) ) {
                                $info_str = '<a class="btn btn-success greenbutton" href="/razpisanie-avtobusi.html?KURS_ID=' . $course_id . '&FIRST_CITY=' . $first_city . '&LAST_CITY=' . $last_city . '&DATA_STR=' . $data_str . '" alt="���� ��������� ������ ������">���� �����' . $dev_str . '</a>';
                            }
                            else {
                                $info_str='<a href="'.$this->get_pLink($this->biletni_kasi).'" class="btn btn-primary bluebutton" alt="���� ��������� ����� �� ����">���� �� ����</a>';
                            }
/*                            else {
                                $sch_id = $row->ID;
                                $price_id = $row->PRICES->PRICE_ID;
                                $price_type_id = $row->PRICES->PRICE_TYPE;

                                $course_id = $this->init_kurs($data_str, $sch_id, $price_id, $price_type_id);
                                if($course_id > 0){
                                    if(($data_str === $todayIs)){


                                        if($hours<=$this->ticksys_hoursBefore){
                                            $info_str='<a href="http://www.trans5.bg/biletni-kasi.html" class="btn btn-primary" alt="���� ����� �� ����">���� �� ����</a>';
                                        }
                                        else{
                                            $info_str = '<a class="btn btn-primary" href="http://'.$urlto.'/razpisanie-avtobusi.html?KURS_ID='.$course_id.'&FIRST_CITY='.$first_city.'&LAST_CITY='.$last_city.'&DATA_STR='.$data_str.'">���� �����</a>';
                                        }
                                    }else{
                                        $info_str = '<a class="btn btn-primary" href="http://'.$urlto.'/razpisanie-avtobusi.html?KURS_ID='.$course_id.'&FIRST_CITY='.$first_city.'&LAST_CITY='.$last_city.'&DATA_STR='.$data_str.'">���� �����</a>';
                                    }
                                }

                            }*/
                        }

                    if($row->SEKTOR == 0) {
                        $sektor_str = "-";
                    }else{
                        $sektor_str = $row->SEKTOR;
                    }

                    echo("<tr>");
                    echo  '<td>'.$data_str.'</td><b>'.iconv('UTF-8','CP1251', '</b><td>'.$row->NAME).'</td><td>'.$row->START_TIME.'�.</td><td>'.$info_str.'</td><td>'.$sektor_str.'</td>';
                    //if($_SESSION["user"]){echo "<td>".$row->PRICES->PRICE_NAME."</td>";}
                    echo("</tr>");
                    $info_str = "";
                } // foreach

        } // for

        echo('
          </tbody>
        </table>
        ');

    }

    function GET_SCHEDULES_TWOWAY($first_city, $last_city, $data=null, $days = 7) {

        if($data == null) $data = time();   // timestamp

        echo('
        <h2>���������� '.$this->cities[$first_city].' - '.$this->cities[$last_city].'</h2>
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <thead>
          <tr>
            <th>����</th>
            <th>�����������</th>
            <th>T������� �� '.$this->cities[$first_city].'</th>
            <th>������</th>
           </tr>
          </thead>
          <tbody>
        ');
        $j=0;
        for($i = 0; $i<=$days; $i++) {

            $curent_data = $data+$i*60*60*24;

            $todayIs  = date("d.m.Y", time());
            $data_str = date("d.m.Y", $curent_data);


            $url = $this->ticksys_url."GET_SCHEDULES&PASS=".$this->ticksys_pass."&FOR_DATE=".$data_str."&FIRST_CITY=".$first_city."&LAST_CITY=".$last_city."&company=".$this->company."&MIN_FREE_PLACES=0&JSON=T";
            if(($this->_user['AccessLevel'] >= 4)  && ($this->dev_debug)) echo $url."<br>";
            $xml = $this->parse_json($url);

                foreach($xml->RESULT->COMPANY->SCHEDULE as $row) {    // Company  za staroto IP
                    $j++;
                        if( ($this->active) || ($this->_user['AccessLevel'] > 1) ){
                            $course_id = $row->KURS_ID;
                            $urlto=$this->_site["primary_url"];

                            if($course_id > 0 or $this->dev_debug) {


                                    $date1 = new DateTime();
                                    $date2 = new DateTime($data_str.'T'.$row->START_TIME);
                                    $diff = $date2->diff($date1);
                                    $hours = $diff->h;
                                    $hours = $hours + ($diff->days*24);
                                    if($hours<=$this->ticksys_hoursBefore){
                                        $info_str='<a href="http://www.trans5.bg/biletni-kasi.html" class="btn btn-primary" alt="���� ����� �� ����">���� �� ����</a>';
                                    }
                                    else{
                                        $info_str = '
                                        <div class="funkyradio-success">
                                            <input type="radio" name="radio'.$first_city.'" id="radio'.$first_city.''.$j.'" KURS_ID="'.$course_id.'" FIRST_CITY="'.$first_city.'" LAST_CITY="'.$last_city.'" DATA_STR="'.$data_str.'">
                                            <label for="radio'.$first_city.''.$j.'">������</label>
                                        </div>';
                                        //$info_str = '<a class="btn btn-primary" href="http://'.$urlto.'/razpisanie-avtobusi.html?KURS_ID='.$course_id.'&FIRST_CITY='.$first_city.'&LAST_CITY='.$last_city.'&DATA_STR='.$data_str.'">���� �����</a>';
                                    }
                            } else {
                                $sch_id = $row->ID;
                                $price_id = $row->PRICES[0]->PRICE_ID;
                                $price_type_id = $row->PRICES[0]->PRICE_TYPE;

                                $course_id = $this->init_kurs($data_str, $sch_id, $price_id, $price_type_id);
                                if($course_id > 0 or $this->dev_debug){
                                    if(($data_str === $todayIs)){

                                        $takeofftime = strtotime($todayIs.$row->START_TIME);
                                        $takeofftimeMinusOneHour = $takeofftime+60*60*$this->ticksys_hoursBefore;
                                        $difference = $takeofftimeMinusOneHour-$takeofftime;
                                        if($difference<=3600*$this->ticksys_hoursBefore){
                                            $info_str='<a href="http://www.trans5.bg/biletni-kasi.html" class="btn btn-primary" alt="���� ����� �� ����">���� �� ����</a>';
                                        }
                                        else{
                                        $info_str = '
                                        <div class="funkyradio-success">
                                            <input type="radio" name="radio'.$first_city.'" id="radio'.$first_city.''.$j.'" KURS_ID="'.$course_id.'" FIRST_CITY="'.$first_city.'" LAST_CITY="'.$last_city.'" DATA_STR="'.$data_str.'">
                                            <label for="radio'.$first_city.''.$j.'">������</label>
                                        </div>';
                                            //$info_str = '<a class="btn btn-primary" href="http://'.$urlto.'/razpisanie-avtobusi.html?KURS_ID='.$course_id.'&FIRST_CITY='.$first_city.'&LAST_CITY='.$last_city.'&DATA_STR='.$data_str.'">���� �����</a>';
                                        }
                                    }else{
                                        $info_str = '
                                        <div class="funkyradio-success">
                                            <input type="radio" name="radio'.$first_city.'" id="radio'.$first_city.''.$j.'" KURS_ID="'.$course_id.'" FIRST_CITY="'.$first_city.'" LAST_CITY="'.$last_city.'" DATA_STR="'.$data_str.'">
                                            <label for="radio'.$first_city.''.$j.'">������</label>
                                        </div>';
                                        //$info_str = '<a class="btn btn-primary" href="http://'.$urlto.'/razpisanie-avtobusi.html?KURS_ID='.$course_id.'&FIRST_CITY='.$first_city.'&LAST_CITY='.$last_city.'&DATA_STR='.$data_str.'">���� �����</a>';
                                    }
                                }else{
                                    var_dump($course_id);
                                }

                            }
                        }
                        else{
                            if($row->SEKTOR == 0) {
                                $info_str = "-";
                            }else{
                                $info_str = '������ '.$row->SEKTOR;
                            }
                        }

                    echo("<tr>");
                    echo  '<td>'.$data_str.'</td><b>'.iconv('UTF-8','CP1251', '</b><td>'.$row->NAME).'</td><td>'.$row->START_TIME.'�.</td>
                    <td>
                    <div class="funkyradio">
                        '.$info_str.'
                    </div>
                    </td>';
                    //if($_SESSION["user"]){echo "<td>".$row->PRICES->PRICE_NAME."</td>";}
                    echo("</tr>");
                    $info_str = "";
                } // foreach

        } // for

        echo('
          </tbody>
        </table>
        ');

    }  // end GET_SCHEDULES

    /*
        TODO: new method set_cache where must be inserted SiteID
        convert cache get to make select with SiteID
    
    */
    function get_cities() {   // get cities in array

    # if($this->cache_actual($this->cities_cache)) {
    #    $cities = $this->cache_get($this->cities_cache);
    # } else {
       $url = $this->ticksys_url."GET_CITIES&PASS=".$this->ticksys_pass."&company=".$this->company.'&JSON=T';
       $xml = $this->parse_json($url);
       foreach($xml->RESULT->COMPANY->CITY as $city) {   // Company v starata versia
           $cities[] = array("ID"=>(int)$city->ID, "NAME"=>iconv("UTF-8","CP1251",(string)$city->NAME));
       }
       #$this->cache_put($cities, $this->cities_cache);
    # }

        foreach($cities_arr as $city) {
            $this->cities[$city['ID']] = $city['NAME'];
        }

        return $cities;

    }// end get_cities


    function get_destinations($from_city_id, $for_date = null) {
        if($for_date == null)  { $for_date = date("d.m.Y"); }
        $url = $this->ticksys_url."GET_TRAVEL_TO_CITY&PASS=".$this->ticksys_pass."&FOR_DATE=".$for_date."&CITY_ID=".$from_city_id."&company=".$this->company."&JSON=T";
       $xml = $this->parse_json($url);
       foreach($xml->RESULT->COMPANY->CITY as $city) {   // Company v starata versia
           $cities[] = array("ID"=>(int)$city->ID, "NAME"=> $city->NAME);
       }

        return $cities;
    }

    public function parse_json($url, $assoc_array=False)
    {
       $xml = file_get_contents($url);
       return json_decode($xml, $assoc_array);
    }

    function  get_travel_to_city($from_city_id) {
    }

    function reserve($for_date, $kurs_id, $schedule_id, $price_id, $price_type_id, $for_person, $phone, $places_num, $additional="") {
        $url = $this->ticksys_url."RESERVE&PASS=".$this->ticksys_pass."&COMPANY=".$this->company."&FOR_DATE=".$for_date."&KURS_ID=".$kurs_id."&SCHEDULE_ID=".$schedule_id."&PRICE_ID=".$price_id."&PRICE_TYPE_ID=".$price_type_id."&FOR_PERSON=".$for_person."&PHONE=".$phone."&PLACES_NUM=".$places_num.$additional;
        //if(($this->_user['AccessLevel'] >= 4)  && ($this->dev_debug));
        return $url;
    }

    function delete_reserve($reserve_id){
        $url = $this->ticksys_url."DEL_RESERVE&PASS=".$this->ticksys_pass."&COMPANY=".$this->company."&RESERVE_ID=".$reserve_id;
        return $url;
    }

    function sale_reserve($reserve_id, $discount=0){
        if($this->active) {
            $disc_str = "";
            if($discount == 0) $disc_str = "&DISCOUNT_ID=1"; //  - ���� � 10% online
            if($discount == 1) $disc_str = "&DISCOUNT_ID=2"; // - ���� � 20% � ����� ��������� ���� �� ������� ����� � ���� �� ����������� ������
            $url = $this->ticksys_url."SALE_RESERVE&PASS=".$this->ticksys_pass."&COMPANY=".$this->company."&RESERVE_ID=".$reserve_id.$disc_str;
            return $url;
        }
    }

    function init_kurs($for_date, $schedule_id, $price_id, $price_type_id){
        $url = $this->ticksys_url."RESERVE&PASS=".$this->ticksys_pass."&COMPANY=".$this->company."&FOR_DATE=".$for_date."&KURS_ID=0&SCHEDULE_ID=".$schedule_id."&PRICE_ID=".$price_id."&PRICE_TYPE_ID=".$price_type_id."&FOR_PERSON=TEST%20TEST&PHONE=0123456789&PLACES_NUM=1";
        $xml = $this->parse_json($url."&JSON=T");
        $reserveId = $xml->RESULT->COMPANY->RESERVE->ID;
        $kursid = $xml->RESULT->COMPANY->RESERVE->KURS_ID;
        $urlDelete = $this->delete_reserve($reserveId);
        $this->parse_json($urlDelete."&JSON=T");
        return $kursid;
    }


    /**
     * Check if price has more than one Price types if yes, iterate over them and
     * pass to price_mixin
     *
     * @param array $route
     * @return string
     */
    public function get_prices(array $route)
    {
        $t = '';
        if(array_key_exists("PRICE_NAME", $route))
            return $this->price_mixin($route);
        foreach($route as $price){
            $t .= $this->price_mixin($price);
        }
        return $t;
    }

    /**
     * Return price table for price modal
     *
     * @param array $price
     * @return string
     * @throws Exception
     */
    private function price_mixin(array $price)
    {
        $table = '';
        $price_name = iconv('utf8', 'cp1251', $price["PRICE_NAME"]);
        $table .= "<tr><td>".$price_name."</td>";
        $_p = $prie["PRICE"];
        if($_p =='0.00'):
            $table .= "<td><strong> ��������� </strong></td></tr>";
        else:
            $_p = $price['PRICE'];
            if($price_name == '���������� 24 ����' or $price_name == '���������� 168 ����'){
                $_p = $price["PRICE2"];
            }
            $table .= "<td><strong>".$_p." ��.</strong></td></tr>";
        endif;
        return $table;
    }


    /**
     * return cities as pair id => name
     *
     * @param
     * @return array
     * @throws Exception
     */
    public function return_cities()
    {
        $cities = $this->get_cities();
        foreach ($cities as $city):
            $this->template_data["cities"][$city["ID"]] = $city["NAME"];
        endforeach;
        return $this->template_data["cities"];
    }

    /**
     * Loops over fefined days on second iteration change dates to tommorrow
     * check if schedule have more than one route and if there is more than one loops over them
     * On every iteration add row to table
     *
     *
     * @param integer $first_city, integer $last_city, string $date, integer $days
     * @return string
     * @throws ValidationError
     */
    /*
     * 1. Set table field names
     * 2. get cities array
     * 3. loop over days
     * 3.1 check if days are bigger than 1
     * 3.2. get schedule_data
     * 3.3 check if key exist
     * 3.4 iterate over schedule and call schedule mixin
     * 3.5 check if table rows > 1
     * return array
     *
     * */
    public function __GET_SCHEDULES($first_city, $last_city, $date, $days = 7)
    {
        $table = new PrettyTable("����","�����������", "��� �� ��������",
                                 "��� �� ����������", "������","������");
        $table->set_class('table');
        $table->add_sheet('direct_road');
        $table->add_sheet('all');
        $table->add_sheet('with_attachment');
        #$button = '<a href="'.$this->get_pLink($this->biletni_kasi).'" class="btn btn-primary bluebutton" alt="'.Messages::BUTTON_ALT_MESSAGE.'">'.Messages::BUTTON_MESSAGE.'</a>';
        $_date = new DateTimeField($date);
        $cities = $this->return_cities();
        $this->template_data = $cities;
        for ($day=0; $day < $days; $day++):
            $date = $_date();
            if($day > 0){
                $date = $_date->tomorrow();
            }
            $routes = (array) $this->schedule_raw_data($first_city, $last_city, $date);
            $_routes = $routes["RESULT"]["COMPANY"]["SCHEDULE"];
            if(!array_key_exists('SCHEDULE', $routes["RESULT"]["COMPANY"])){
                continue;
            }
            if(!array_key_exists("KURS_ID", $_routes)){
                $_routes = $routes["RESULT"]["COMPANY"]["SCHEDULE"];
                foreach($_routes as $route):
                    $route_name = iconv('utf8', 'cp1251', $route["NAME"]);
                    if(strpos($route_name, '������ �������')){continue;} // Skip Travel Express
                    $route_data = $this->schedule_mixin($route, $cities, $first_city, $last_city);
                    $table->insert_in_sheet('all', $route_data);
                    if($cities[$first_city]. ' - '.$cities[$last_city] == $route_name){
                        $table->insert_in_sheet('direct_road', $route_data);
                        $this->counter++;
                        continue;
                    }
                    $table->insert_in_sheet('with_attachment', $route_data);
                    $this->counter++;
                endforeach;
            }else{
                $route_data = $this->schedule_mixin($_routes, $cities, $first_city, $last_city);
                $table->insert_in_sheet('direct_road', $route_data);
                $table->insert_in_sheet('all', $route_data);
                $this->counter++;
            }
        endfor;
        return $table;
    }

    /**
     * Simple search and replace in predefined template
     *
     *
     * @param array $route, array $cities, integer(incremental) $j, integer $first_city,
     *        integer $last_city
     * @return array
     * @throws Exception
     */
    private function schedule_mixin($route, $cities, $first_city, $last_city){
        $file_path = $_SERVER['DOCUMENT_ROOT']."/web/forms/mbus/tickets/modal_prices.html";
        $search = array("{{id}}", "{{alt_message}}", "{{first_city}}", "{{last_city}}",
                        "{{table}}", "{{button}}");
        $route_name = iconv('utf8', 'cp1251', $route["NAME"]);
        $h = fopen($file_path, 'rb');
        $template = fread($h, filesize($file_path));
        fclose($h);
        $price_modal = $this->get_prices($route["PRICES"]);
        $replace = array($this->counter, Messages::BUTTON_ALT_MESSAGE, $cities[$first_city],
                         $cities[$last_city], $price_modal, $button);
        $time = explode(' ', $route["SCHEDULE_DATETIME"]);
        $_modal = str_ireplace($search, $replace, $template);
        return array($time[0],$route_name, $route["START_TIME"],
                     $route["END_TIME"], $_modal, $route["SEKTOR"]);
    }




    /**
     * Return bus stops between two cities as tags
     *
     * @param integer $from_city, string/DateTime $for_date, string $link
     * @return string
     * @throws Exception
     */
    public function print_two_way($first_city, $last_city, $date, $days = 1, $template=False)
    {
        if($template==True){
            return $this->print_direct_course($first_city, $last_city, $date, $days);
        }

        $first_course = $this->__GET_SCHEDULES($first_city, $last_city, $date, $days);
        $second_course = $this->__GET_SCHEDULES($last_city, $first_city, $date, $days);
        $search = array("{{first_course}}", "{{second_course}}");
        $replace = array($first_course->print_sheet('all'), $second_course->print_sheet('all'));
        $file_path = $_SERVER['DOCUMENT_ROOT']."/web/forms/mbus/tickets/print_courses.html";
        $h = fopen($file_path, 'rb');
        $template = fread($h, filesize($file_path));
        fclose($h);
        return str_replace($search, $replace, $template);
    }


    public function print_one_way($first_city, $last_city, $date, $days=1)
    {
        $one_way = $this->__GET_SCHEDULES($first_city, $last_city, $date, $days);
        $search = array("{{one_way}}");
        $replace = array($one_way->print_sheet('all'));
        $file_path = $_SERVER['DOCUMENT_ROOT']."/web/forms/mbus/tickets/one_way_template.html";
        $h = fopen($file_path, 'rb');
        $template = fread($h, filesize($file_path));
        fclose($h);
        return str_replace($search, $replace, $template);
    }


    public function print_direct_course($first_city, $last_city, $date, $days=1)
    {
        $first = $this->__GET_SCHEDULES($first_city, $last_city, $date, $days);
        $second = $this->__GET_SCHEDULES($last_city, $first_city, $date, $days);
        $search = array("{{first_course_direct}}", "{{first_course_attach}}",
                        "{{second_course_direct}}", "{{second_course_attach}}");
        $replace = array($first->print_sheet('direct_road'), $first->print_sheet('with_attachment'),
                         $second->print_sheet('direct_road'), $second->print_sheet('with_attachment'));
        $file_path = $_SERVER['DOCUMENT_ROOT']."/web/forms/mbus/tickets/print_direct.html";
        $h = fopen($file_path, 'rb');
        $template = fread($h, filesize($file_path));
        fclose($h);
        return str_replace($search, $replace, $template);
    }

    /**
     * Make call to api convert xml to json and return array
     *
     * @param integer $from_city, integer $last_city, string/DateTime $date
     * @return array
     * @throws ValidationError
     */
    public function schedule_raw_data($first_city, $last_city, $date)
    {
        list($first_city, $last_city, $date) = $this->validate_input_fields($first_city, $last_city, $date);
        $url = $this->ticksys_url."GET_SCHEDULES&PASS=".$this->ticksys_pass.
                "&FOR_DATE=".$date."&FIRST_CITY=".$first_city.
                "&LAST_CITY=".$last_city."&company=".$this->company.
                "&MIN_FREE_PLACES=".$this->min_free_places."&JSON=T";
        return $this->parse_json($url, true);
    }

    /**
     * Validate fields for schedule_raw_data -> see /lib/reservation_system/fields.php
     *
     * @param integer $from_city, integer $last_city, string/DateTime $date
     * @return array
     * @throws ValidationError
     */
    private function validate_input_fields($first_city, $last_city, $date)
    {
        $first_city = new PositiveIntegerField($first_city);
        $last_city = new PositiveIntegerField($last_city);
        $date = new DateTimeField($date);
        return array($first_city(), $last_city(), $date());
    }


    /**
     * Return bus stops between two cities as tags
     *
     * @param integer $from_city, string/DateTime $for_date, string $link
     * @return string
     * @throws Exception
     */
     public function print_to_destination($from_city_id, $for_date, $link)
    {
        $cities = $this->get_destinations($from_city_id, $for_date);
        $text  = '';
        $to_city = $this->return_cities();
        foreach($cities as $city){
            $from_city = iconv('utf8', 'cp1251', $city["NAME"]);
            $text .= '  <a href="'.$link.'?first_city='.$city["ID"].
                        '&last_city='.$from_city_id.'" >'.$from_city.' - '. $to_city[$from_city_id].'</a> , ';
        }
        return $text;
    }


} // end class TickSys UnReal Soft ticket system



/*
primer
http://78.128.1.70:2008/?command=GET_TRAVEL_TO_CITY&PASS=12TfrS&CITY_ID=1&FOR_DATE=19.01.2014&company=3

GET_CITIES&PASS=.....
����� ������ �������
<Result>
<data>
<transaction>Ok</transaction>
</data>
<Company id="%d">
<CITY>
<ID>%d</ID>
<NAME>%s</NAME>
</CITY>
</Company
</Result>

GET_TRAVEL_TO_CITY&PASS=.....&FOR_DATE=dd.mm.yyyy&CITY_ID=x&company=3
����� ��������� �� ����� ������ � ���� ���
<Result>
<data><transaction>Ok</transaction></data>
<Company id="%d">
<CITY>
<ID>%d</ID>
<NAME>%s</NAME>
</CITY>
</Company>
</Result>


GET_BUS_TYPE&PASS=.....
����� �������� ��������
<Result>
<data><transaction>Ok</transaction></data>
<Company id="%d">
<BUS_TYPE>
<ID>%d</ID>
<NAME>%s</NAME>
<PLACES>%d</PLACES>
</BUS_TYPE>
</Company>
</Result>
GET_BUS_PLACES&PASS=.....&BUS_TYPE_ID=xx&COMPANY=3
����� �������������� � ������� � �������
<Result>
<data><transaction>Ok</transaction></data>
<ROW_COUNT>%d</ROW_COUNT>
<POS_COUNT>%d</POS_COUNT>
<BUS_PLACE>
<ROW>%d</ROW>
<POS>%d</POS>
<NUMBER>%s</NUMBER>
<SALE_LAST>%s</SALE_LAST>
</BUS_PLACE>
</Result>
GET_FREE_PLACES&PASS=.....&KURS_ID=xx&FIRST_CITY=c1&LAST_CITY=c2&COMPANY=3
����� ����� �� ����� ���� � ������ �� �������������, ������� ����� � �������� �����
<Result>
<data><transaction>Ok</transaction></data>
<ROW_COUNT>%d</ROW_COUNT>
<POS_COUNT>%d</POS_COUNT>
<BUS_PLACE>
<ROW>%d</ROW>
 <POS>%d</POS>
 <NUMBER>%s</NUMBER>
 <SALE_LAST>%s</SALE_LAST>
<TAKEN>%d</TAKEN>
</BUS_PLACE>
</Result>

GET_SCHEDULES&PASS=.....&FOR_DATE=dd.mm.yyyy&FIRST_CITY=x1&LAST_CITY=y1&company=3&MIN_FREE_PLACES=xx
����� ������������ �� ������ ���� � �������, � ������
<Result>
<data>
<transaction>Ok</transaction>
</data>
<Company id="3">
<SCHEDULE>
<ID>35</ID>
<KURS_ID>42394</KURS_ID>
<FIRST_CITY_ID>1</FIRST_CITY_ID>
<LAST_CITY_ID>12</LAST_CITY_ID>
<NAME>����� - ������ ������ �� 14:00</NAME>
<START_TIME>14:00</START_TIME>
<END_TIME>18:20</END_TIME>
<SCHEDULE_DATETIME>10.05.2014 14:00</SCHEDULE_DATETIME>
<SEKTOR>28</SEKTOR>
<BUS_TYPE>1</BUS_TYPE>
<FREE_PLACES>48</FREE_PLACES>
<PRICES>
<PRICE_TYPE>1</PRICE_TYPE>
<PRICE>22.00</PRICE>
<PRICE_CHILD>0.00</PRICE_CHILD>
<PRICE_ID>2508</PRICE_ID>
</PRICES>
<PRICES>
<PRICE_TYPE>5</PRICE_TYPE>
<PRICE>20.00</PRICE>
<PRICE_CHILD>0.00</PRICE_CHILD>
<PRICE_ID>2509</PRICE_ID>
</PRICES>
<PRICES>
<PRICE_TYPE>7</PRICE_TYPE>
<PRICE>11.00</PRICE>
<PRICE_CHILD>0.00</PRICE_CHILD>
<PRICE_ID>6795</PRICE_ID>
</PRICES>
</SCHEDULE>
</Company>
</Result>

GET_PRICE_TYPE&PASS=.....&company=3
����� �������� ����
<Result>
<data><transaction>Ok</transaction></data>
<Company id="%d">
<PRICE_TYPE>
<ID>%d</ID>
<NAME>%s</NAME>
</PRICE_TYPE>
</Company>
</Result>


http://78.128.1.70:2008/?command=GET_CITIES&PASS=.....

http://78.128.1.70:2008/?command=GET_CITIES&PASS=12TfrS

������ = 12TfrS
*/

?>
