<?php
use Symfony\Component\HttpFoundation\Request;


require_once __DIR__.'/lib_page.php';
require_once __DIR__.'/AdminUrlType.php';
require_once __DIR__.'/BaseUrlType.php';
require_once __DIR__.'/FullUrlType.php';
require_once __DIR__.'/SlugUrlType.php';
require_once __DIR__.'/SlugParentUrlType.php';


class UrlFactory
{
    protected $request, $page, $urlType;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function makeUrl(\stdClass $page, array $site, $strict=true) 
    {
        $urlType = $site['url_type'];

        if(page::$uID > 0 and $strict) {
            return new AdminUrlType($this->request, $page, $site);
        }

        switch($urlType) {
        case 0:
            return new BaseUrlType($this->request, $page, $site);
            break;
        case 1:
            return new FullUrlType($this->request, $page, $site);
            break;
        case 2:
            return new SlugUrlType($this->request, $page, $site);
            break;
        case 3:
            return new SlugParentUrlType($this->request, $page, $site);
            break;
        }
    }
}
