<?php
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_site >> lib_page >> lib_rss
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_site >> lib_page >> xml2array >> lib_rss
require_once('xml2array.php');
require_once __DIR__.'/lib_functions.php';
// require_once('xml2array.php');
// include_once('./rss/rss_fetch.inc');

class rss extends xml2array {

    var $count = 10;
    var $description_length = 500;
    var $filer_title = "";
    var $urls=array();
    var $link=""; //default links refer to original
    var $img_width;


    //cosntructor
    function __construct() {
        parent::__construct();
        $this->img_width = 200;

    }

    function get_rss() {
        foreach($this->urls as $url) {
            if($url) $rss = fetch_rss($url);
        }
        return $rss;
    }

    function parse($rss=NULL) {
        $i = 1;
        while($url = array_pop($this->urls)) {
            //if($url) $rss = fetch_rss($url);
            //echo $url; echo("<br>");
            $result_rss = $this->db_query("SELECT * FROM cache_rss WHERE rss_time > DATE_SUB(CURDATE(), INTERVAL 30 MINUTE) AND rss_url = '$url'");


            if(($url) && ($rss==NULL)) {
               //if ($this->n == 147850) echo $rss_row->rss;
               if($rss_row = mysqli_fetch_object($result_rss)) {
                   $rss_str = $rss_row->rss;
                } else {
                   $rss_str = file_get_contents($url);
                   if(strlen($rss_str)>20)
                       $rss_str_escaped = mysqli_real_escape_string($rss_str);
                       if($this->n == 147850) echo $rss_str_escaped;
                        $this->db_query("INSERT INTO cache_rss SET n = '".$this->n."', SiteID = '".$this->SiteID."', rss_url = '$url', rss = '$rss_str_escaped', rss_time = NOW()  ");
                }
               // echo $rss_str;
                $rss = simplexml_load_string($rss_str);

            }
            if(is_object($rss)) $namespaces = $rss->getNamespaces(true); // get namespaces

            if(($this->n)=="205851"){

                //$rss = preg_replace("/<\!\[CDATA\[/i", "", $rss);
                /* $rssPattern ="/^<\!\[CDATA\[(.*?)\]\]>$/";
                preg_match($rssPattern,$rss,$rssMatch);
                var_dump($rssMatch);
                $rss = $rssMatch[1]; */
                //$rss = preg_replace("/<\!\[CDATA\[/i", " ", $rss);
                //$rss = preg_replace("/\]\]>/i", " ", $rss);


            }
            //$rss = preg_replace("/^<\!\[CDATA\[*?.\]\]>$/",  "/^<\!\[CDATA\[*?.\]\]>$/", $rss);

            //foreach ($rss->items as $item)
            foreach ($rss->channel->item as $item)
            {
                $news_title = iconv('UTF-8','CP1251', $item->title);
                $news_description = iconv('UTF-8','CP1251', $item->description);
                $news_description = strip_tags($news_description, '<img>');

                //print_r($item);





                if(($i<=$this->count) && ((stristr($news_title, $this->filter) || (stristr($news_description, $this->filter)) || ($this->filter==""))) )
                {
                    $news_link = $item->link;
                    if(strlen($this->link)>3)  $news_link = $this->link;
                    if (isset($this->link)>6) {$news_link=$this->link;}
                    $image_url = $item->enclosure['url'];

                    //$image_url = $item->media['content']['url'];
                    if(strlen($image_url)==0) {
                        $image_url = trim((string)
                        $item->children($namespaces['media'])->content->attributes()->url);

                    }



                    ?>

                    <div class="sPage" style = "margin: 5px;" >
                        <div class="sPage-content">
                            <div class="bullet1" style="float:left;"></div><a href="<?=$news_link?>" title="<?=$news_title?>" class="title"><?=$news_title?></a>
                            <br style="clear: both;">
                            <div class="text" style = "margin:3px;">
                                <p>
                                    <?php
                                    //echo "<div class=\"sPage\" style=\"float: left; \"><div class=\"sPage-content\" ><div class=\"text\" >".cut_text($news_description, $this->description_length, "...", "<img>") ."</div>". $str_img ."</div>".$str_cart_options."</div>";
                                    echo(cut_text($news_description, $this->description_length, "...", "<img>"));
                                    if($image_url) echo '<a href="'.$news_link.'" title="'.$news_title.'"><img src="'.$image_url.'" width='.$this->img_width.'px" align="right" class="align-left">'.cut_text($news_description, $this->description_length, "...", "<img></a>");

                                    if( ($this->_site['site_status']>0) && ($this->_page['SecLevel']==0) ) {
                                        echo($this->get_seo_etiketi_link($this->get_top_word($this->page['title'].' '.$this->page['Name'].' '.$this->_page['textStr'].' '.$news_title.' '.$news_description))); ;
                                    }

                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <?php



                    $i++;
                }
            } // foreach
            return $rss;
        }  // parse

    }

    function add_url($url) {
        $this->urls[]=$url;
    }

    function clear_urls() {
        $this->urls[]=null;
    }


    function parse_lazy()
    {
        $i = 0;
echo <<<HEREDOC
<script>
function get(url) {
  // Return a new promise.
  return new Promise(function(resolve, reject) {
    // Do the usual XHR stuff
    var req = new XMLHttpRequest();
    req.open('GET', url);
    req.setRequestHeader("Accept-Language", "bg, en");
    req.setRequestHeader("Accept-Charset", "windows-1251"); 
    req.overrideMimeType('text/plain;charset=windows-1251');

    req.onload = function() {
      // This is called even on 404 etc
      // so check the status
      if (req.status == 200) {
        // Resolve the promise with the response text
        resolve(req.response);
      }
      else {
        // Otherwise reject with the status text
        // which will hopefully be a meaningful error
        reject(Error(req.statusText));
      }
    };

    // Handle network errors
    req.onerror = function() {
      reject(Error("Network Error"));
    };

    // Make the request
    req.send();
  });
}
rss_feed = jQuery("#pageContent");
jQuery('<div>', { 
    id: 'rss_feed',
}).appendTo('#pageContent');
jQuery('<div>', { 
    id: 'loader',
    class: 'loader',
}).appendTo('#pageContent');
rss_container = jQuery("#rss_feed");
rss_container.hide();
HEREDOC;
        while($url = array_pop($this->urls)) {
            $url = "/api/rss.php?command=rss_parse&n=1&link=$url";
        echo <<<HEREDOC
            jQuery("#loader").show(); 
            get("$url").then(function(response){
                rss_container.append(response);
            }).then(function(){
                jQuery("#loader").hide(); 
                rss_container.show();
            });

HEREDOC;
            $i++;
        }
        echo '  </script>';
    }
} // end class rss()

?>
