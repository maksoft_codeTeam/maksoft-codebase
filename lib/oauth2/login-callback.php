<?php
require_once __DIR__ . '/config.php';
# login-callback.php
$helper = $fb->getRedirectLoginHelper();
try {
  $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
}

if (isset($accessToken)) {
  // Logged in!
    $_SESSION['facebook_access_token'] = (string) $accessToken;
    $oAuth2Client = $fb->getOAuth2Client();
        // Exchanges a short-lived access token for a long-lived one
    $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
    $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
        // setting default access token to be used in script
    $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
  // Now you can redirect to another page and use the
  // access token from $_SESSION['facebook_access_token']

    try {
        $img = $fb->get('/me/?fields=picture');
        $img = $img->getDecodedBody();
        echo '<img src="'.$img['picture']['data']['url'].'">';
        $profile_request = $fb->get('/me?fields=name,first_name,last_name,email');
        $profile = $profile_request->getGraphNode()->asArray();
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        echo 'Graph returned an error: ' . $e->getMessage();
        session_destroy();
        // redirecting user back to app login page
        header("Location: ./");
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
    
    try{
        $likes = $fb->get('/me/likes/'.getenv('FACEBOOK_APP_ID'));
        $likes = $likes->getDecodedBody();
        print_r($likes);
    } catch(Facebook\Exceptions\FacebookSDKException $e){
        echo 'Facebook SDK returned an error: '.$e->getMessage();
    }
    // printing $profile array on the screen which holds the basic info about user
    print_r($profile);

}else{
    login();
}


function login()
{  
    global $helper, $fb, $permissions;
    $loginUrl = $helper->getLoginUrl('http://localhost/auth0/login-callback.php', $permissions);
    header('Location: '.$loginUrl);
}
?>

<li><a href="?action=logout">Logout</a></li>

<?php

if(isset($_GET['action']) && $_GET['action'] === 'logout'){
        session_destroy();
        header("Location: http://localhost/auth0/login.php");
        $facebook->destroySession();
    }
