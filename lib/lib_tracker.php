<?php
use \PhpAmqpLib\Connection\AMQPStreamConnection;
use \PhpAmqpLib\Message\AMQPMessage;


class Tracker
{
    protected $username='guest';

    protected $password='mnk@22MNK';

    protected $port=5672;

    protected $host='localhost';

    protected $durable = True;

    protected $queue_name='track_log';

    protected $connection;

    protected $channel;

    protected $message;

    private $data = array();

    public function __construct()
    {
        $this->connection = new AMQPStreamConnection($this->host, $this->port, $this->username, $this->password);
        $this->channel = $this->connection->channel();
        $this->channel->queue_declare($this->queue_name, false, $this->durable, false, false);
    }
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }

        return null;
    }

    /**  As of PHP 5.1.0  */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function __unset($name)
    {
        unset($this->data[$name]);
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setPort($port)
    {
        $this->port = $port;
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function setQueue($name)
    {
        $this->queue_name = $name;
    }

    public function constructPersistentMessage(){
        $this->message = new AMQPMessage(json_encode($this->data),  array("delivery_mode" => 2));
        return $this;
    }

    public function publish()
    {
        $this->channel->basic_publish($this->message, '', $this->queue_name);
    }

    #public function __destruct()
    #{
    #    $this->channel->close();
    #    $this->connection->close();
    #}
}
