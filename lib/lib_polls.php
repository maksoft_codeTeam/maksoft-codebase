<?php
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_site >> lib_polls
 require_once('lib_mysql.php');

class poll extends mysql_db {
  var $post_vars; 
  var $get_vars;
  var $title;   	
  public $ankID=false;
	// page( [int site identification number; int page number; ] )
	// constructor 
	function __construct()
	{	
		parent::__construct(); 

		$num_args = func_num_args(); 
		
		//no arguments
		if (($num_args == 0)  )
			{
			 	if(isset($_GET['ankID']))
			 		$this->ankID =  $_GET['ankID']; 
			} 		

		if ($num_args == 1)
			{
				$this->ankID = func_get_arg(0);
			}
			
	    if ($num_args > 1) 
			{
				$this->error_handler('Class page not defined properly!  page( [int site identification number; [int page number]] ',C_USER_ERROR,__FILE__,__LINE__,__FUNCTION__,__CLASS__,microtime());
				//echo "Class page not defined properly!  page( [int site identification number; [int page number]]";
			}
		$poll = $this->get_poll($this->ankID);
		$this->title = $poll['ankText'];
		$this->n = $poll['n'];
	}

    function get_page_polls($n, $SiteID)
    {
        $select_qry = "SELECT ankID, ankText from anketi WHERE n = $n and SiteID=$SiteID";
        return $this->fetch("object", $this->db_query($select_qry));
    }
	
	//output formated poll title
	function print_title()
		{
			echo "<h4 class=\"poll-title\">".$this->title."</h4>";	
		}
	
	//return poll
	function get_poll($poll_id)
		{
			$poll_query = $this->db_query("SELECT *FROM anketi WHERE ankID = '".$poll_id."' LIMIT 1");
			$poll = $this->fetch("array", $poll_query);
			return $poll;
		}
    function get_poll_total_votes($ankID)
    {
        $select_qry = "SELECT SUM(votes) as qty  FROM `ank_vuprosi` WHERE `ankID` = $ankID";
        return $this->fetch("object", $this->db_query($select_qry));
    }
	
	//return poll options
	function get_poll_options($poll_id)
		{
			if(!$poll_id) $poll_id = $this->ankID;
			if($poll_id)
				{
					$poll_options = array();
					$poll_query = $this->db_query("SELECT * FROM ank_vuprosi WHERE ankID = '".$poll_id."' ORDER by qID ASC");
					while($option = $this->fetch("array", $poll_query))
						$poll_options[] = $option;
					return $poll_options;
				}
			else return NULL;
		}
		
	//otput all poll options
	function print_options()
		{
			$poll_id = $this->ankID;
			$poll_options = $this->get_poll_options($poll_id);
			echo "<ul class=\"poll-box\">";
			for($i=0; $i<count($poll_options); $i++)
				echo "<li class=\"poll-option\"><input type=\"radio\" class=\"select\" name=\"voted\" value=\"".$poll_options[$i]['qID']."\">".$poll_options[$i]['quest']."</li>";			
			echo "</ul>";
		}
	
	//output poll
	function view($poll_id)
		{
			if(!$poll_id) $poll_id = $this->ankID;
			if($poll_id)
				{
					$poll = $this->get_poll($poll_id);
					$poll_options = $this->get_poll_options($poll_id);
					echo "<h4 class=\"poll-title\">".$poll['ankText']."</h4><ul class=\"poll-box\">";
					for($i=0; $i<count($poll_options); $i++)
						echo "<li class=\"poll-option\"><input type=\"radio\" class=\"select\" name=\"voted\" value=\"".$poll_options[$i]['qID']."\">".$poll_options[$i]['quest']."</li>";
					echo "<button>vote</button>";
					echo "</ul>";
				}
			else return NULL;
		}
			
	//vrushta qID na vuprosa subral nai-mnogo glasove ot dadena anketa s nomer ankID
	function chosen($ankID)
		{ 	
			 $query = $this->db_query("SELECT DISTINCT( votes.qID ) FROM votes WHERE qID IN (SELECT ank_vuprosi.qID FROM ank_vuprosi WHERE ankID='$ankID') "); 
			 $i=0; 
			 $qID=0; 
			 $max_votes=0; 
			 while($row = $this->fetch("object", $query)) {
			    $query_max_str = "SELECT * FROM votes WHERE  qID='$row->qID'  "; 
			    $query_max = $this->db_query("$query_max_str "); 
				//echo("$max_votes $qID $query_max_str \r\n"); 
				$num_votes = $this->count($query_max); 
				if($num_votes > $max_votes) {
				  $max_votes = $num_votes; 
				  $num_votes_row = $this->fetch("object", $query_max); 		  
				  $qID =  $num_votes_row->qID; 
				}
			  
			 } // while
			 return $qID; 
		}
	
	//vrushta texta na vuprosa po zadaden nomer qID
	function quest_text($qID) {
	 $query = $this->db_query("SELECT quest, url FROM ank_vuprosi WHERE qID='$qID'   "); 
	 $row = $this->fetch("object", $query); 
	 $str=$row->quest; 
	 if(strlen($row->url)>5) {
		 $str = "<a href=\"$row->url\">$row->quest</a>";
		 }
	 return $str; 
	}
	
	// ankID  ankText  SiteID  qID  quest  ankID  
	function last_voted($SiteID) {
	  $query_text = "
		 SELECT *
			FROM anketi a
			JOIN ank_vuprosi av ON av.ankID = a.ankID
			WHERE a.SiteID='$SiteID' 
			AND av.qID = (
			SELECT qID
			FROM votes
			ORDER BY vID DESC
			LIMIT 1 )
	 	"; 
	 $query = $this->db_query("$query_text"); 
	 $row = $this->fetch("object", $query); 
	 return $row; 
	}


 } // end class poll
?>
