<?php
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_site >> lib_wwo

// This class is PROVIDING Weather Info 
// DIR    /wwo
// file /wwo//wwo.php

// Powered by <a href="http://www.worldweatheronline.com/" 
//title="Free local weather content provider" 
// target="_blank">World Weather Online</a>

 require_once('lib_site.php');
 
  class wwo extends site {

	 var $num_of_days=5;
	 var $wwo_api_key; 
 	 var $wwo_api_prem_key; 
	 var $city = ""; 
	 var $loc_array = array(0,0); 
	 var $latitude;
	 var $longitude;
	 var $active = true; 
	 
 
	 //cosntructor
	 function __construct() {
		parent::__construct(); 
		$this->wwo_api_key = $this->get_sConfigValue("WWO_API_KEY");
		//$this->active = false;
         $this->wwo_api_key = "77a1b1a5fa374f31a0635738170207";
		$this->wwo_api_prem_key = "77a1b1a5fa374f31a0635738170207";   // ekskurzii
        $this->ver_dir = "premium/v1";
         // old api fd7u5fm6jf2t2u48c2pgfca5
	 }
	 
	 function wwo_search($wwo_city="") {
		 if(empty($wwo->city)) $wwo_city = $this->city; 
		 
		 //To add more conditions to the query, just lengthen the url string
		$basicurl=sprintf('http://api.worldweatheronline.com/'.$this->ver_dir.'/search.ashx?key=%s&q=%s&format=xml',
				$this->wwo_api_key, urlencode($wwo_city));
		
		//echo $basicurl;
		$xml_response = file_get_contents($basicurl);
		
		$xml = simplexml_load_string($xml_response);
		foreach ($xml as $name=>$data) {
				if($data->longitude > 0)
					$this->db_query("INSERT INTO wwo_loc SET wwo_city = '$wwo_city', areaName = '$data->areaName', country = '$data->country', region = '$data->region', 
					latitude = '$data->latitude', 	longitude = '$data->longitude',	population = '$data->population',  weatherUrl = '$data->weather_url'  "); 
			}	
			return $xml; 
			
			$this->latitude = '$data->latitude'; 
			$this->longitude = '$data->longitude';

	 }
	 
	 function get_marine($loc_array) {
		 //$loc_array= Array("45","-2");		//data validated in foreach. 
		 if(empty($loc_array)) $loc_array = $this->loc_array; 
		 $loc_safe=Array();
			foreach($loc_array as $loc){
				$loc_safe[]= urlencode($loc);
			}
			$loc_string=implode(",", $loc_safe);
		$basicurl=sprintf('http://api.worldweatheronline.com/'.$this->ver_dir.'/marine.ashx?key=%s&q=%s&format=xml',
			$this->wwo_api_key, $loc_string);
		//echo $basicurl; 
		$xml_response = file_get_contents($basicurl);
		$xml = simplexml_load_string($xml_response);
		
		if(strlen($xml_response) > 100) 
			$this->db_query("UPDATE wwo_loc SET marine_string='$xml_response'  WHERE ABS(latitude - $loc_array[0])<0.005 AND ABS(longitude - $loc_array[1])<0.005 "); 
		
		return $xml; 
	 } // end get_marine
	 
	 function get_weather($wwo_city="") {
		 if(empty($wwo->city)) $wwo_city = $this->city; 
		  
		 $wwo_loc_q = $this->db_query("SELECT * FROM wwo_loc WHERE (wwo_city LIKE '$wwo_city' OR  areaName LIKE '$wwo_city' )  "); 

		 if($this->count($wwo_loc_q) > 0) {

		   $wwo_loc_row = $this->fetch("object", $wwo_loc_q); 
		   
		   $this->latitude = $wwo_loc_row->latitude;
		   $this->longitude = $wwo_loc_row->longitude;

			$xml_response = $wwo_loc_row->weather_string;
			//$xml_marine = $wwo_loc_row->marine_string;

			$xml_marine =  simplexml_load_string($wwo_loc_row->marine_string);
			
			/*			   
		   if( (strlen($wwo_loc_row ->weather_string)>10) && (strtotime($wwo_loc_row->data_taken) > time()-60*60) && (strlen($this->wwo_api_key)>10) ) {  // cashe ot predi 1 chas
		   		$xml_response = $wwo_loc_row->weather_string;
				$xml_marine =  simplexml_load_string($wwo_loc_row->marine_string);
				
				//echo("<!-- marine <pre>"); print_r($xml_marine);  echo("</pre>-->"); 
		   } 
		   */
		 
			 if(  ( ($this->active) && strlen($this->wwo_api_key)>10)  && ((strtotime($wwo_loc_row->data_taken) < time()-1*60*60)  ||  (strlen($wwo_loc_row ->weather_string)<10)  ) ) {  // cashe ot predi 1 chas

 		        $loc_str = $wwo_loc_row->latitude.'%2C'.$wwo_loc_row->longitude;
		   		$basicurl=sprintf('http://api.worldweatheronline.com/'.$this->ver_dir.'/weather.ashx?key=%s&q=%s&num_of_days=%s',
					$this->wwo_api_key, $loc_str, intval($this->num_of_days));

				$xml_response = file_get_contents($basicurl);
					
				//	echo $xml_response."***"; 
														
				if( (strlen($xml_response)>200)  )  {
				
						$this->db_query("UPDATE wwo_loc SET weather_string='$xml_response'  WHERE ABS(latitude - $wwo_loc_row->latitude)<0.005 AND ABS(longitude - $wwo_loc_row->longitude)<0.005 "); 

						//echo("UPDATE wwo_loc SET weather_string='$xml_response'  WHERE ABS(latitude - $wwo_loc_row->latitude)<0.005 AND ABS(longitude - $wwo_loc_row->longitude)<0.005 "); 

					
						if($wwo_loc_row->marine>0) {
							if( (strtotime($wwo_loc_row->data_taken) < time()-24*60*60) && ($xml_marine<>'') ) {
							 $xml_marine  = $this->get_marine(array($wwo_loc_row->latitude, $wwo_loc_row->longitude));
							}
						}
						
						$xml = simplexml_load_string($xml_response);
						
						 $wwo_data = getdate(); 
						$this->db_query("INSERT INTO wwo_data SET latitude = '$wwo_loc_row->latitude', longitude = '$wwo_loc_row->longitude', temp_F = '".$xml->current_condition->temp_F."', waterTemp_F = '".$xml_marine->weather->hourly->waterTemp_F."', weatherCode = '".$xml->current_condition->weatherCode."', hours = ".$wwo_data['hours'].", mday = '".$wwo_data['mday']."', mon = '".$wwo_data['mon']."', yday = '".$wwo_data['yday']."', year = '".$wwo_data['year']."', SiteID = '".$this->SiteID."' "); 
						
					} // $xml response is 	empty     - data limit reached 
					else {
						//echo $basicurl; 
						$this->db_query("INSERT INTO  wwo_error_log SET basicurl = '$basicurl' ,  errstr = '$xml_response' , n = '".$this->n."', SiteID = '".$this->SiteID."', page_url = '".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."', errtime = NOW()   ");
                        $xml_response = $wwo_loc_row->weather_string;
                        $xml_marine =  simplexml_load_string($wwo_loc_row->marine_string);

                    }

			} // get current weather  end
			$xml = simplexml_load_string($xml_response);
	   
		 } else {  // city location is not present
		 	$xml_search = $this->wwo_search($wwo_city); 

			$basicurl=sprintf('http://api.worldweatheronline.com/'.$this->ver_dir.'/weather.ashx?key=%s&q=%s&num_of_days=%s',
				$this->wwo_api_key, $wwo_city, intval($this->num_of_days));
			$xml_response = file_get_contents($basicurl);
			$xml = simplexml_load_string($xml_response);
			//echo("INSERT INTO wwo_loc SET wwo_city = ' $wwo_city', latitude = $xml_search->latitude, 	longitude = '$xml_search->longitude',	population = $xml_search->population,  weatherUrl = '$xml_search->weather_url'  "); 
		 }


		$this->marine = $xml_marine;
		$this->weather = $xml;
		//print_r($xml_marine); 
        $xml->marine = $xml_marine;
        $xml->weather = $xml;
		$xml->latitude = $wwo_loc_row->latitude;
		$xml->longitude = $wwo_loc_row->longitude; 
		$xml->utcOffset = $wwo_loc_row->utcOffset;
		$xml->Condition = $wwo_loc_row->Condition;
		$xml->population = $wwo_loc_row->population;
		
		
		return $xml; 
	 }
	 
 
	 function parse($wwo_city="") {
	 
	 if($this->active) {
		if(empty($wwo->city)) $wwo_city = $this->city; 
	 	$xml = $this->get_weather($wwo_city); 
		
		if($xml->population>0) {$pref = " � "; } else { $pref = " �� "; }
		
		echo('
		<div itemprop="location">
		  <span itemscope itemtype="http://schema.org/Place">
			<div itemprop="geo">
			  <span itemscope itemtype="http://schema.org/GeoCoordinates">
				<span property="latitude" content="'.$this->latitude.'"></span>
				<span property="longitude" content="'.$this->longitude.'"></span>
			  </span>
			</div>
		   </span>
		');
		
		printf("<p>������������� � �������  %s <b><span itemprop=\"name\">%s</span></b>  e <span class=\"climacon thermometer medium-high\" aria-hidden=\"true\"></span> %s  �C </p>",$pref, $wwo_city, $xml->current_condition->temp_C);

		if($xml->marine->weather->hourly->waterTemp_C > 0) {   // waterTemp_C > 0
			printf("������������� �� <b>�������� ����</b>%s %s � ������� e %s �C<br>", $pref, $this->city, $xml->marine->weather->hourly->waterTemp_C);
		}		

		printf(" ������������ ����������� �� ���� � %s �C. <br><b>���� %s %s </b> �� ������ ���������� ����������� ��  %s �C ",
		$xml->weather[0]->maxtempC, $pref, $wwo_city, $xml->weather[1]->maxtempC);
		
		//echo "***".$xml->weather[4]->tempMaxC; 
		if(abs($xml->weather[0]->maxtempC - $xml->weather[$this->num_of_days]->maxtempC) > 7) {
			printf(" ���������� ������� ��� �� ������ ������� � ������������� ���� ���� %s ��� �� ������ ����������� ����������� �� ��������� %s �C, � ������������  %s �C ",
			$this->num_of_days, $xml->weather[$this->num_of_days-1]->mintempC, $xml->weather[$this->num_of_days-1]->maxtempC);
		}
		
		
		if($xml->current_condition->windspeedKmph > 1) { // WIND 
			switch($xml->current_condition->winddir16Point) {
				case "N": 
				$wind_str = "������� ����� ��� ������� ".$xml->current_condition->windspeedKmph."  km/� "; 
				break; 
				case "S": 
				$wind_str = "���� ����� ��� ������� ".$xml->current_condition->windspeedKmph."  km/� ";
				break; 			
				case "W": 
				$wind_str = "������� ����� ��� ������� ".$xml->current_condition->windspeedKmph."  km/� "; 
				break; 			
				case "E": 
				$wind_str = "������� ����� ��� ������� ".$xml->current_condition->windspeedKmph."  km/� "; 
				break; 			
			
			}
					
			if(strlen($wind_str)>0) {
				if($xml->current_condition->windspeedKmph < 10)  $wind_str = "���� ".$wind_str; 
				if($xml->current_condition->windspeedKmph > 20)  $wind_str = "���� ����� ".$wind_str; 
				echo "<p><span class=\"climacon wind cloud\" aria-hidden=\"true\"></span> $wind_str</p>"; 
			} else
			printf("<p><span class=\"climacon wind cloud\" aria-hidden=\"true\"></span> ���� �� ������ %s km/�  %s</p>", 
				$xml->current_condition->windspeedKmph, $xml->current_condition->winddir16Point );
		
		} // WIND parse end
		
		//print "<pre>";
		//if($user->AccessLevel>4) print_r($xml);
		//print "</pre>";
		
		echo("</div>");  // itemscope Place end
		
		} // active 		
	 
	 } // wwo->parse end
	 
	 function get_condition($WeatherCode) {
	 	$q = $this->db_query("SELECT * FROM wwo_codes WHERE WeatherCode = '$WeatherCode' AND language_key =  1 AND  SiteID = 0 ORDER BY wwo_codes_id DESC");
		$row = $this->fetch("object", $q); 
		return $row->Condition; 
	 }

      // current class shortcodes





 
 } // end class wwo()
 
?>
