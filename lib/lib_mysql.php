<?php
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_site >> lib_page >> lib_cms

require_once("event_handler.php");

class mysql_db extends event_handler {

    public $db_host; 
    public $db_user;
    public $db_pass; 
    public $db_name; // current database name
    public $link; //current database link   if set there is a current connection 
    protected $default_encoding = 'cp1251';
    public $lang_id = 1;


    function __construct($db_host=null, $db_user=null, $db_pass=null, $db_name=null)
    {
        #if(!DEBUG) {
            #parent::__construct();  
        #}

        $this->db_host = ( empty($db_host) ? $this->db_host_upl : $db_host); 
        $this->db_user = ( empty($db_user) ? $this->db_user_upl : $db_user); 
        $this->db_pass = ( empty($db_pass) ? $this->db_pass_upl : $db_pass); 
        $this->db_name = ( empty($db_name) ? $this->main_db_upl : $db_name); 

        
        $this->link = $this->db_connect(); 

        $this->db_query("SET NAMES ".$this->default_encoding);
        
        self::$instance = $this->link;
     }
 
    function db_connect() 
    {  
          
          $this->link = mysqli_connect($this->db_host, $this->db_user, $this->db_pass, $this->db_name) 
                or 
                 $this->common_handler('�� ���� �� �� �������� ',C_USER_ERROR,__FILE__,__LINE__,__FUNCTION__,__CLASS__,microtime());
                 
          return $this->link; 
    }

    public function db_affected_rows()
    {
        return mysqli_affected_rows();
    }
    

    public function db_num_fields($result){
        return mysqli_num_fields($result);
    }

    public function db_fetch_field($result, $i){
        if(!$i or $i < 2){
            return mysqli_fetch_field($result);
        }

        $tmp = array();
        for($x=0; $x<$i; $x++) {
              $tmp[] = mysqli_fetch_field( $result );
        }
        return $tmp;
    }



//db_select(string database_name )        
// sets the current database name 
 function db_select($db_name) {
  $res = mysqli_select_db($this->link, $db_name); 
   return $res; 
 }

    function result($result, $offset=0) {
        return $this->db_result($result, $offset);
    }
    function db_result($result, $offset=0){
        $data = $this->fetch('array', $result);
        return $data[$offset];
    }
    function count($db_query){
        return mysqli_num_rows($db_query);
    }
    function db_count($db_query){
        return mysqli_num_rows($db_query);
    }

    function last_insert_id()
    {
        return mysqli_insert_id($this->link);
    }

    function fetch($type, $db_query)
    {
        if(!$db_query instanceof mysqli_result ) {
            return false;
        }
        return $this->db_fetch($type, $db_query);
    }

    public function db_fetch($type,  mysqli_result $db_query)
    {
        switch($type){
            case "object":
            return mysqli_fetch_object($db_query);
        default:
            return mysqli_fetch_array($db_query);
        }
    }


    function db_free($result){
        mysqli_free_result($result);
    }

    public function db_query($query) 
    {  
        if(empty($query)) {
            return false;
        }
        return mysqli_query($this->link, $query);
    }


    function pages_base_query($join_clause='', $and_clause='', $lang_id='')
    {
        if(!$lang_id or !is_numeric($lang_id)) {
            $lang_id = $this->lang_id;
        }
        $sql = "
            SELECT 
                p.n, p.an, p.ParentPage, tr.Name, tr.slug,
                tr.title, tr.tags, p.url_rules, p.PHPvars, tr.lang_id,
                tr.textStr, p.PHPcode, p.toplink, p.show_link,
                p.show_group_id, p.show_link_order, p.show_link_cols,
                p.make_links, p.form, p.PageURL, p.imageNo, p.imageWidth,
                p.image_allign, p.ankID, p.PrintVer, p.SiteID, p.date_added,
                p.author, p.date_modified, p.preview, p.addtype, p.SecLevel,
                p.status, p.sort_n, im.ImageID, im.imageName, im.image_src
            FROM pages p
            LEFT JOIN pages_text tr ON tr.n = p.n
            LEFT JOIN images im ON p.imageNo = im.imageID
            $join_clause
            WHERE tr.lang_id = $lang_id
            $and_clause
            ";
        return $sql;
    }
 
 // destructor
    public function _mysql_db() {
       mysqli_close($this->link);
     }
} // end class


?>
