<?php
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_site >> lib_page >> lib_cms

require_once __DIR__ .'/config.php';
// config >> event_handler >> lib_mysql >> lib_user >> lib_page

/*
func_get_arg() may be used in conjunction with func_num_args() and func_get_args() to allow user-defined functions to accept variable-length argument lists. 
*/

class event_handler  extends app_config
{
    const C_PHP_WARNING = 1;
    const C_PHP_NOTICE = 2;
    const C_BEGIN_FUNCTION = 4;
    const C_END_FUNCTION = 8;
    const C_DUMP_VAR = 16;
    const C_USER_EVENT = 32;
    const C_USER_ERROR = 64;
    const C_USER_WARNING = 128;
    const C_USER_NOTICE = 256;
    const C_KERNEL_ERROR =  3; //Use of undefined constant C_KERNEL_ERROR - assumed 'C_KERNEL_ERROR'

    public $events = array();
    public $events_rep = array();
    public $events_log = array();
    public $func_deep=0;
    public $back_trace_arr = array();
    public $event_times = array();
    public $old_error_handler ;


    //constructor
    function __construct()
    {
        // set to the user defined error handler
        $this->old_error_handler = set_error_handler(array(&$this,'error_handler'));

    }


    function error_handler($errno, $errstr, $errfile, $errline)
    {
        if (!isset($this->event_print)) {
            return;
        }
        echo "<!-- DEBUG INFO ";
        $this->print_events(); 
        echo "-->"; 


        $err_start = "<!--"; 
        $err_end = "-->"; 
            
        switch ($errno) {
            case E_USER_ERROR:
                echo "<b>My ERROR</b> [$errno] $errstr   $errfile  $errline <br />\n";
                echo "  Fatal error in line $errline of file $errfile";
                echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
                echo "Aborting...<br />\n";
                exit(1);
                break;
            case E_USER_WARNING:
                echo "<b>My WARNING</b> [$errno] $errstr   $errfile  $errline <br />\n";
                break;
            case E_USER_NOTICE:
                echo "<b>My NOTICE</b> [$errno] $errstr       $errfile  $errline <br />\n";
                break;
            default:
                echo "$err_start Unkown error type: [$errno] $errstr        $errfile  $errline <br />\n  $err_end";
        }
    }




    function common_handler($event_vars, $event_id, $file, $line, $function, $class, $time)
    {
        switch ($event_id)
            {
            case(1)://C_PHP_WARNING
                //��� event_vars � ������� �� ������ - ������� �� �� php � �� ���� �� ���� ��������
                $this->php_error($event_id,$event_vars,$file,$line);
                break;
            case(2)://C_PHP_NOTICE
                $this->php_error($event_id,$event_vars,$file,$line);
                break;
            case(4)://C_BEGIN_FUNCTION
                $this->begin_function($event_id,$event_vars,$file,$line,$function,$class,$time);
                break;
            case(8)://C_END_FUNCTION
                $this->end_function($event_id,$event_vars,$file,$line,$function,$class,$time);
                break;
            case(16)://C_DUMP_VAR
                $this->dump_var($event_id,$event_vars,$file,$line,$function,$class,$time);
                break;
            case(32)://C_USER_EVENT
                $this->user_event($event_id,$event_vars,$file,$line,$function,$class,$time);
                break;
            case(64)://C_USER_ERROR
                //�� ��� � ��� ������, �� � event_vars ����� �� �� ������� ���������� ����������� serialize
                $this->user_error($event_id,$event_vars,$file,$line);
                break;
            case(128)://C_USER_WARNING
                $this->user_error($event_id,$event_vars,$file,$line);
                break;
            case(256)://C_USER_NOTICE
                $this->user_error($event_id,$event_vars,$file,$line);
                break;
            case(512)://C_KERNEL_ERROR
                break;
            default:
                $this->common_handler('common_handler-a � ������� ������� � ����� '.$event_id.', ����� �� ���� �� ���� ����������.',self::C_KERNEL_ERROR,$file,$line,NULL,NULL,NULL);
            }//end switch
    }   
        
    function user_error($event_id,$err_string,$file,$line)
    {
        return 1;
    }


    function process_function($args_array)
    {
        $string='';
        foreach($args_array as $key=>$value) {
            $string .= $this->process_var($value).',';
        }

        $string = substr($string,0,strlen($string)-1);
        return $string;
    }
        
    function process_var($var)
    {
        //print gettype($var);
        $ser = serialize($var);
        //$ser = htmlentities($ser);
        $ser = htmlspecialchars($ser);
        $string ='<a target="_blank" href="/lib/debug.php?dump='.$ser.'">';
        if(is_int($var)||is_float($var))
            $string .= $var;
        if(is_null($var))
            $string .= 'NULL';
        if(is_string($var))
            $string .= '\''.$var.'\'';
        if(is_bool($var))
            {
            if($var)
                $string .= 'true';
                else
                    $string .= 'false';
            }
        if(is_resource($var))
            {
            $string .= get_resource_type($var);
            }
        if(is_array($var))
            {
            $string .= 'array'.$this->array_dim($var);
            $this->array_dim();//aeea na iae (aa?a aac a?aoiaioe) , ca aa na eco?ea noaoe?iaoa i?iiaieeaa
            }
        if(is_object($var))
            {
            $string .= 'object '.get_parent_class($var).'->'.get_class($var);
            //$string = 'object '.get_class($var); //aac aa oeacaa eie a ?iaeoaeneey eean
            }
        $string .= '</a>';
        return $string;
        }   
        

    function begin_function($event_id,$args,$file,$line,$function,$class,$time)
    {
        $this->events[] = "<div>$file,  line: $line  <br>$event_id:$class -> $function(".$this->process_function($args).")  deep: $this->func_deep </div>";

        if($this->func_deep>50) { 
            $this->events_rep[] = '�������� ������ - ���������� �� '.$this->func_deep.' �������� ���� � ����� �������.';
            $this->events_log[] = '�������� ������ - ���������� �� '.$this->func_deep.' �������� ���� � ����� �������.';
            $this->func_deep = 0;
        }
            
        $this->event_times[$this->func_deep] = $time;
        $this->back_trace_arr[$this->func_deep] = $file.';'.$line.';'.$function;
        
        $this->func_deep++;
    }
        
    function end_function($event_id,$ret,$file,$line,$function,$class,$time)
    {
        $this->func_deep--;
        
        $delta_time = $time - $this->event_times[$this->func_deep];
        
        unset($this->back_trace_arr[$this->func_deep]);

        $this->events[] = "<div>$file,  line: $line  <br>$event_id: end_function     time: $delta_time</div>";

        //unset($this->event_times[$this->func_deep]);//���� �� � ��-������� �� �� ����
        
    }
        
    function print_events()
        {
        foreach ($this->events as $event)
        {
         if($this->event_print) echo("$event\r\n"); 
        }
        
        }
        
        
    function mysql_status(){
        #Return status load averages in %
        $CMD = "mysqladmin -uroot -pmnk2MNK stat";
        $CMD = shell_exec($CMD);
        
        $RegEx = "|Queries per second avg: [0-9]+|";
        preg_match_all($RegEx,$CMD,$get_sql_query_num);
        
        
        $setQuery =  substr($get_sql_query_num[0][0],24,26)/10;
        
        return $setQuery;
    }   
    
    
}//end class
    

?>
