<?php
// app_config >> event_handler >> lib_mysql >> cron
include "lib_mysql.php";

class cron extends mysql_db{

 // cosntructora na clasa sledva da proveri natovarvaneto na sistemata i udachnostta za izpulnenie na sistemni zadachi v izbrania moment
 function __construct() { // constructor 
 	parent::__construct();  
 } // cron constructor end
 
 function load_average() {
 }
 
 function optimize_tables() {
  $this->db_query("OPTIMIZE TABLE pages");
  $this->db_query("OPTIMIZE TABLE keywords");
  $this->db_query("OPTIMIZE TABLE storage");
  //$this->db_query("OPTIMIZE TABLE banners_imp");
 } 
  
 
 function delete_expired() {
  $this->db_query("DELETE FROM stats WHERE  logTime<=DATE_SUB(CURDATE(),INTERVAL 366 DAY) "); 
 } 
 
 function _cron() { // destructor 
 } // cron destructor end 
 
} // end class cron

//$sql_cron = "SELECT * FROM etiketi WHERE date_expire = DATE_ADD(CURDATE(), INTERVAL 100 DAY)"; 
//$o_cron = new cron();
//$s_nr = mysqli_num_rows($o_cron->db_query($sql_cron)); 

?>
