<?php
namespace Message;
define('MSG_WRAPPER' , "<div class='%s'>%s</div>\n"); 
define('MSG_BEFORE' , '');  
define('MSG_AFTER' , '');  
define('STICKY_CSS_CLASS', 'sticky');
define('MSG_CSS_CLASS', 'alert dismissable');
define('CLOSE_BUTTON', '<button type="button" class="close" 
                                data-dismiss="alert" 
                                aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>');

define("INFO"   , 'i');
define("SUCCESS", 's');
define("WARNING", 'w');
define("ERROR", 'e');
define("DEFAULT_TYPE", INFO);

define("SESSION_PREFIX", "flash_messages");

function construct_types($types)
{
    if (is_null($types) || !$types || (is_array($types) && empty($types)) ) {
        return array_keys(message_types());
    }

    if (is_array($types) && !empty($types)) {
        return array_map(function($type) {
            return lower($type);
        }, $types);
    }

    return array(lower($types));
}

function lower($type){
    return strtolower($type[0]);
}

function check($type)
{
    if(!in_array($type, array_keys(message_types()))) {
        return DEFAULT_TYPE;   
    }
    
    return $type;
}

function message_types()
{
    return array(
        ERROR   => 'error',
        WARNING => 'warning', 
        SUCCESS => 'success', 
        INFO    => 'info', 
    );
}

function add($message, $type, $redirectUrl=null, $sticky=false)
{
    $type = check(trim($type));
    
    // Add the message to the session data
    if (!array_key_exists( $type, $_SESSION[SESSION_PREFIX])) $_SESSION[SESSION_PREFIX][$type] = array();
    $_SESSION[SESSION_PREFIX][$type][] = array('sticky' => $sticky, 'message' => $message);
    session_write_close();

    // Handle the redirect if needed
    return do_redirect($redirectUrl);
}

function do_redirect($toUrl=null)
{
    if(empty($toUrl)){
        return true;
    }
    
    header('Location: ' . $toUrl);
}

function info($message, $redirectUrl=null, $sticky=false) {
    add($message, INFO, $redirectUrl, $sticky);
}
function success($message, $redirectUrl=null, $sticky=false) {
    add($message, SUCCESS, $redirectUrl, $sticky);
}
function warning($message, $redirectUrl=null, $sticky=false) {
    add($message, WARNING, $redirectUrl, $sticky);
}
function error($message, $redirectUrl=null, $sticky=false) {
    add($message, ERROR, $redirectUrl, $sticky);
}

function formatMessage($msgDataArray, $type)
{
    $cssClassMap = array( 
        INFO    => 'alert-info',
        SUCCESS => 'alert-success',
        WARNING => 'alert-warning',
        ERROR   => 'alert-danger',
    );

    $msgType = check($type);
    $cssClass = MSG_CSS_CLASS . ' ' . $cssClassMap[$type];
    $msgBefore = MSG_BEFORE;
    // If sticky then append the sticky CSS class
    if ($msgDataArray['sticky']) {
        $cssClass .= ' ' . STICKY_CSS_CLASS;
    // If it's not sticky then add the close button
    } else {
        $msgBefore = CLOSE_BUTTON . $msgBefore;
    }
    // Wrap the message if necessary
    $formattedMessage = $msgBefore . $msgDataArray['message'] . MSG_AFTER; 
    return sprintf(
        MSG_WRAPPER, 
        $cssClass, 
        $formattedMessage
    );
}


function display($types=null, $print=true) 
{
    if (!isset($_SESSION[SESSION_PREFIX])) return false;
    // Retrieve and format the messages, then remove them from session data
    $output = retrieve_and_format_messages(construct_types($types));
    // Print everything to the screen (or return the data)
    if ($print) { 
        echo $output; 
        return;
    }

    return $output; 
}

function retrieve_and_format_messages($message_types)
{
    $output = '';
    foreach ($message_types as $type) {
        $messages = get_messages_by_type($_SESSION, $type);
        $output .= build_messages($messages, $type);
        clear($type);            
    }
    return $output;
}

function build_messages($messages, $type) {
    $output = '';
    foreach ($messages as $msgData) {
        $output .= formatMessage($msgData, $type);
    }

    return $output;
}

function get_messages_by_type($storage, $type) {
    if (!isset($storage[SESSION_PREFIX][$type]) || empty($storage[SESSION_PREFIX][$type]) )
        return array();

    return $storage[SESSION_PREFIX][$type];
}


function clear($types=array()) { 
    if ((is_array($types) && empty($types)) || is_null($types) || !$types) {
        unset($_SESSION[SESSION_PREFIX]);
    } elseif (!is_array($types)) {
        $types = array($types);
    }
    foreach ($types as $type) {
        unset($_SESSION[SESSION_PREFIX][$type]);
    }
}

