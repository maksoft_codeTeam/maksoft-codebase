<?php
use Symfony\Component\HttpFoundation\Request;


require_once __DIR__.'/lib_page.php';


abstract class AbstractUrlResolver 
{
    protected $page;
    protected $request;

    protected $data = array();
    protected $prefix = '.html';

    public function __construct(Request $request, $page, $site)
    {
        $this->page = $page;
        $this->request = $request;
        $this->site = $site;
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        return isset($this->data[$name]) ? $this->data[$name] : null;
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function __unset($name)
    {
        unset($this->data[$name]);
    }

    public final function __toString()
    {
        return $this->generate();
    }

    public abstract function generate();


    protected function getSlug()
    {
        return $this->slug;
    }

    public function getPrefix()
    {
        return empty($this->slug) ? '' : $this->prefix; 
    }

    public function isHomepage()
    {
        return $this->page->n == $this->site['StartPage'];
    }

    public function getLanguage()
    {
        return $this->request->query->get('lang', null);
    }

    protected function getQueryString()
    {
        \parse_str($_SERVER['QUERY_STRING'], $url_arr);
        return $url_arr ? $url_arr : array(); 
    }
}
