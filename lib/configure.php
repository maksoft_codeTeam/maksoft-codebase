<?php

// Define the webserver and path parameters
// * DIR_FS_* = Filesystem directories (local/physical)
// * DIR_WS_* = Webserver directories (virtual/URL)

  define('HTTP_SERVER', 'http://www.maksoft.net'); // eg, http://localhost - should not be empty for productive servers
  define('LIB_SERVER', 'http://lib.maksoft.net'); // library server
  define('HTTPS_SERVER', ''); // eg, https://localhost - should not be empty for productive servers
  define('HOST_IP', '79.124.31.189'); // hosting survur IP address
  define('ENABLE_SSL', false); // secure webserver for checkout procedure?
  define('DIR_WS_ROOT', '/');

  define('DIR_FS_ROOT', '/hosting/maksoft/maksoft/');
  define('DIR_FS_PUB', DIR_FS_ROOT . 'web/pub/');
  define('DIR_WS_PUB', 'web/pub/');
  define('DIR_WS_MODULES', 'web/admin/modules/');
  define('DEFAULT_IMAGE', 'http://www.animalia.bg/web/admin/images/no_image.jpg');
?>
