<?php
use rcastera\Browser\Session\Session as RcasteraSession;


class Session extends RcasteraSession
{
    protected $db;
    protected $two_weeks=1209600;

    public function __construct($db)
    {
        session_set_save_handler(
          array($this, "__open"),
          array($this, "__close"),
          array($this, "__read"),
          array($this, "__write"),
          array($this, "__destroy"),
          array($this, "__clean")
        );
        $this->db = $db;
        // The following prevents unexpected effects when using objects as save handlers.
        #register_shutdown_function('session_write_close');
        session_start();
    }

    public function __open()
    {
        if($this->db){
            return true;
        }
        return false;
    }

    public function __close()
    {
        $this->db = null;
    }

    public function __read($id)
    {
        $stmt =  $this->db->prepare("SELECT * FROM session WHERE id=:id LIMIT 1");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $res['data'];
    }

    public function __write($id, $data)
    {
        $stmt = $this->db->prepare("REPLACE INTO session VALUES  (:id, :name, :modified, :lifetime, :data)");
        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":name", $_SERVER['HTTP_HOST']);
        $stmt->bindValue(":lifetime", 84000 * 14); //30 days
        $stmt->bindValue(":data", $data);
        $stmt->execute();
    }

    public function __clean($max)
    {
        $old = time() - $max;
        $stmt = $this->db->prepare("DELETE FROM session WHERE modified < :old");
        $stmt->bindValue(":old", $old);
        $stmt->execute();
    }

    public function __destroy($session_id)
    {
        #if($this->is_valid($session_id)){
        #    return true;
        #}

        $stmt = $this->db->prepare("DELETE FROM session WHERE id=:id");
        $stmt->bindValue(":id", $session_id);
        $stmt->execute();
    }

    public function isValid($id)
    {
        $stmt = $this->db->prepare("
            SELECT * 
            FROM  `session` 
            WHERE modified + lifetime < NOW( ) and id = : id");
        $stmt->execute();
        $stmt->bindValue(":id", $id);
        $res = $stmt->fetchAll();
        return $res;
    }

    public function getOnline()
    {
        $stmt = $this->db->prepare("
            SELECT * 
            FROM  `session` 
            WHERE modified + lifetime < NOW( )");
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }
}

