<?php
require_once __DIR__.'/AbstractUrlResolver.php';


class FullUrlType extends AbstractUrlResolver
{
    public function generate()
    {
        $url_arr = array();
        $url_arr['n'] = $this->page->n;
        $url_arr['SiteID'] = $this->page->SiteID;
        if($lang = $this->request->query->get('lang')) {
            return sprintf("/%s/?%s", $lang, http_build_query($url_arr)); 
        }

        return sprintf("/page.php?%s", http_build_query($url_arr)); 
    }
}
