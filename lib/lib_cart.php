<?php
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_seo >> lib_site >> lib_cart
 require_once('lib_site.php');
 
 class cart extends site {

	 var $count = 10;
	 var $description_length = 500; 
	 var $urls=array(); 
	 var $link=""; //default links refer to original 
	 
 
     //cosntructor
     function __construct() {
        if(!defined('SHOPPING_CART_VIEW_CURRENCY')) {
            define('SHOPPING_CART_VIEW_CURRENCY', true);
        }
        parent::__construct(); 
            
     }
     
     function item_info($itID) {
        $query = $this->db_query("SELECT * FROM itemTypes left join itGroups on itemTypes.itGroup=itGroups.ID WHERE itID='$itID' "); 
        $item_info_row = $this->fetch("array", $query); 
        return $item_info_row;
     }

	 function info($itID) {
		$query = $this->db_query("SELECT * FROM itemTypes left join itGroups on itemTypes.itGroup=itGroups.ID WHERE itemTypes.itID='$itID' "); 
		$item_info_row = $this->fetch("object", $query); 
		return $item_info_row;
	 }

     function item_sold($itype) {
         $res = $this->db_query("SELECT sum(items.qty) FROM items LEFT JOIN docs ON items.fID = docs.fID WHERE itype = '$itype' AND docs.docID>=0 ");
         $qty = $this->db_result($res,0);
         return $qty;
     }
     
    protected function getAvailability($qty, $page_prices, $item){

        if($this->_page['SiteID'] != 2){
            return "";
        }
        $pcs = $page_prices['pcs'];
        $str_availability = "";
        if($qty > $pcs){
            $str_availability = "<span class=\"on-stock circle-status\" title=\"� ��������� \"><i class=\"fa fa-check\" aria-hidden=\"true\"></i></span>";
        } elseif ($qty < $pcs * 1.5 and $item->itGroup != 5){
            $str_availability = "<span class=\"out-of-stock circle-status\" title=\"�������� \"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>";
        } elseif ($qty >= 0 and $qty < (($pcs * 2) +5 ) ) {
            $str_availability = "<span class=\"call-to\" title=\"����� ��\"><i class=\"fa fa-phone\" aria-hidden=\"true\"></i></span>";
        } else {
            if($item->itGroup == 5 ){
                $str_availability = "<span class=\"produce circle-status\" title=\"� ������������\"><i class=\"fa fa-spinner\" aria-hidden=\"true\"></i></span>";
            } else {
                $str_availability = "<span class=\"call-to circle-status\" title=\"����� ��\"><i class=\"fa fa-phone\" aria-hidden=\"true\"></i></span>";
            }
        }
        return $str_availability;
    }

    public function get_newest_products($days, $SiteID, $limit=12)
    {
        
        $sql = "SELECT  p.n, p.Name, p.textStr, p.date_added, p.date_added, i.image_src, i.imageName, pr.* 
                FROM pages p
                INNER JOIN prices pr ON pr.price_n = p.n
                INNER JOIN images i ON i.imageID = p.imageNo
                WHERE p.SiteID = $SiteID
                AND p.date_added BETWEEN DATE_SUB(NOW(), INTERVAL $days DAY) AND NOW()
                GROUP BY n
                ORDER BY p.date_added DESC LIMIT $limit
                ";         
        $query = $this->db_query($sql);
        $tmp = array();
        while($row = $this->fetch("array", $query)) {
            $tmp[$row['n']] = $row;
        }
        return $tmp;
     }
	 
	 function qty_calc($itID, $stID=0, $include_sold=0, $st_date=null) {  // returns qty of item itID in storage stID	 
	 	if($stID>0) {
		   if($st_date<>null) {
			$st_date_str = " AND st_date<='$st_date'   "; 
			$p_date_str = " AND p_date<='$st_date'   "; 		
		  }
		  //$itID = int($itID); 
           if(is_object($itID) ) {
               $itID = $itID->itID;
           }
		  $qty=0; 
		  $query_add =""; 
		  $query=$this->db_query("SELECT sum(st_qty) FROM storage WHERE to_stID='$stID' AND itID='$itID'  $st_date_str"); 
		  $qty_store_in=$this->db_result($query,0); 
		  $query=$this->db_query("SELECT sum(st_qty) FROM storage WHERE from_stID='$stID' AND itID='$itID'  $st_date_str "); 
		  $qty_store_out=$this->db_result($query,0); 
		  if($include_sold==1) { $query_add = " AND items.ok= 1  "; 	  }
		  $query=$this->db_query("SELECT sum( items.qty )  FROM items LEFT JOIN docs ON docs.fID = items.fID WHERE itype='$itID' AND docs.docID>=0  AND docs.storage='$stID'  $query_add  ");  // st_date ne uchastva
		  $qty_sold=$this->db_result($query,0); 
		  $query=$this->db_query("  SELECT sum( p_qty )  FROM production  WHERE itID = '$itID' AND p_stID='$stID'  $p_date_str "); 	  
		  $qty_products = $this->db_result($query,0); 
		  $query=$this->db_query("SELECT sum( production.p_qty )  FROM  production, recipes  WHERE materialID='$itID' AND productID=itID AND  production.p_stID='$stID'   "); 	  // st_date ne uchastva
		  $qty_materials = $this->db_result($query,0); 
		  $qty = $qty_store_in - $qty_sold - $qty_store_out + $qty_products - $qty_materials;  
		  
		//  if(($include_sold==0) && ($st_date==null)) {
		  $this->db_query("DELETE FROM storage_qty WHERE itID='$itID'  AND stID='$stID' "); 
		   $this->db_query("INSERT storage_qty SET qty='$qty' , itID='$itID' , stID='$stID' "); 
		//  }
		   
		 } else {
		 	$storages_q = $this->db_query("SELECT * FROM storages WHERE 1"); 
			while($stID_row = $this->fetch("object", $storages_q)) {
				$qty = $qty + $this->qty_calc($itID, $stID_row->stID); 
			}
		 }
		  return $qty;
	 }
	 
	 function storage_qty($itID, $stID, $include_sold, $st_date=null) {  // returns qty of item itID in storage stID
	 
	//  if(($include_sold==0) && ($st_date==null)) {
	  	 $storage_query = $this->db_query("SELECT * FROM storage_qty WHERE itID='$itID' AND stID='$stID'  "); 
	//   }
	 if($this->count($storage_query)>0) {
		 $st_qty_row = $this->fetch("object", $storage_query); 
		 $qty = $st_qty_row->qty; 
	 } 
//	 else {
	  	$qty = $this->qty_calc($itID, $stID, $include_sold, $st_date=null);
//	 }
	  return $qty; 
	  
	 }
	 
	 function storage_query($itID, $stID, $limit_from=0, $res_per_page=500) {
		 $query_add_str =""; 
		 if($itID<>0) {$query_add_str = " AND itID='$itID' "; }
		 if($stID<>0) {$query_add_str = "$query_add_str  AND (from_stID='$stID' OR to_stID='$stID' ) "; }
		 $query_set= " SELECT * FROM storage  WHERE 1 $query_add_str ORDER BY st_date DESC LIMIT $limit_from, $res_per_page "; 
		 //echo $query_set;
		  $query=$this->db_query("$query_set "); 
		  return $query; 
	 }

	 function storage_name($stID) {
	  $query=$this->db_query("SELECT * FROM storages WHERE stID='$stID' "); 
	  $row=$this->fetch("object", $query); 
	  return $row->stName; 
	 }	 
	 
	 function item_name($itID) {
	  $query=$this->db_query("SELECT * FROM itemTypes WHERE itID='$itID' "); 
	  $row=$this->fetch("object", $query); 
	  return $row->itName; 
	 }	
    
    public function top_items($db, $SiteID){
        $sql = "SELECT pages.n, pages.SiteID, pages.Name, pages.TextStr, prices.price_pcs, prices.price_value, prices.price_qty, images.imageName, images.image_src, pages.preview 
                FROM pages
                JOIN prices ON price_n = pages.n
                JOIN images ON images.imageID = pages.ImageNo
                AND pages.SiteID = :SiteID
                ORDER BY pages.preview DESC 
                LIMIT 5";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(\PDO::FETCH_OBJ)){
            $tmp[] = $res;
        }
        return $tmp;
    }
	
	 	 
	 function item_full_name($itID) {
	  $query=$this->db_query("SELECT * FROM itemTypes WHERE itID='$itID' "); 
	  $row=$this->fetch("object", $query); 
	  return $row->itStr; 
	 }
	 
	 //check for site storages and return all
	 function site_storages($SiteID)
	 	{
		  $storages = array();
		  $query = "SELECT * FROM storages WHERE SiteID='".$SiteID."' ";
		  $result = $this->db_query($query); 
		  while($storage = $this->fetch("array", $result))
		  	$storages[] = $storage; 
		  return $storages; 			
		}
	 
	 //check if current product is includeed in any storage. If no - tne product is not real item  
	 function is_item($itID)
	 	{
			$query = "SELECT * FROM itemTypes WHERE itID='".$itID."' LIMIT 1";
			$result = $this->db_query($query); 
			return $this->count($result);
		}

     function is_product($itID)
     {
         if($itID > 1000) {
             return true; // product
         }
         else {
             return false; // service
         }
     }
     //return page prices as array

	function get_page_price($page, $SiteID, $condition="ASC")
	{
	
		$page_prices = array();
		$sub_prices = array();
			
		
		$page_prices_sql = "SELECT * FROM prices WHERE price_n = '$page' AND price_SiteID = '$SiteID' ORDER by price_value $condition";
		$page_prices_res = $this->db_query($page_prices_sql);
		while($row = $this->fetch("object", $page_prices_res))
			{
				$sub_prices['id'] = $row->price_id;
				$sub_prices['n'] = $row->price_n;
				$sub_prices['SiteID'] = $row->price_SiteID;
				$sub_prices['description'] = $row->price_description;
				$sub_prices['code'] = $row->price_code;
				$sub_prices['qty'] = $row->price_qty;
				$sub_prices['price_value'] = number_format($row->price_value, 2, '.', '');
	
				array_push($page_prices, $sub_prices);
				//reset sub_prices array
				$sub_prices = array();
			}
		if($this->count($page_prices_res) > 0)	
			return $page_prices;
		else return NULL;
	}
	

	 
	 function output_page_prices($page, $SiteID, $return_value="ALL", $add_to_cart = false)
	{
		/*
		return_value: 
							MIN	- return the minimal price  
							MAX	- return the highest price
							ITEM- return the average item price
							ALL	- return all prices
		desrciption: output page prices + add_to_cart button (optional) or returns an array of prices
		*/ 
		
		
	
		switch($return_value)
			{
				case "MIN":
							{
								$page_prices = $this->get_page_price($page, $SiteID, "ASC",$add_to_cart);
								//$add_to_cart = false;
								$stop = true;
								//return $page_prices[0]['description'] . " | ".$page_prices[0]['price_value'];
								break;						
							}
				case "MAX":
							{
								$page_prices = $this->get_page_price($page, $SiteID, "DESC",$add_to_cart);
								$stop = true;
								//$add_to_cart = false;
								//return $page_prices[0]['description'] . " | ".$page_prices[0]['price_value'];
								break;						
							}
				case "ALL":
				default:
							{ 
								$stop = false;
								$page_prices = $this->get_page_price($page, $SiteID, "ASC",$add_to_cart);
								break; 
							}		
			}
		
		$add_to_cart_str = "";
		$content = "<table border=0 cellspacing=0 cellpadding=2>";
		for($i=0;$i<count($page_prices);$i++)
				{
					if($add_to_cart) 
					{
						if(count($page_prices)>1 && $stop)
							$add_to_cart_str = "<td width=50 valign=top align=center class=t1><a href=\"page.php?n=$page&amp;SiteID=$SiteID\"><img src=\"http://www.maksoft.net/web/images/shoping_cart.gif\" border=0 vspace=0 hspace=0 alt=\"���� ������� ��� ������������ �����\"></a>";
						else
							$add_to_cart_str = "<td width=50 valign=top align=center  class=t1><a href=\"page.php?n=13202&amp;no=$page&amp;SiteID=$SiteID&amp;action=insert&amp;pid=".$page_prices[$i]['id']."\"><img src=\"http://www.maksoft.net/web/images/shoping_cart.gif\" border=0 vspace=0 hspace=0 alt=\"������ � ���������\"></a>";
						$content.="<tr><td align=justify valign=top>". $page_prices[$i]['description'] ." ". $page_prices[$i]['code']."<td width=30 align=center valign=top><b>". $page_prices[$i]['qty']."</b>-<td align=right valign=top width=30><b>".$page_prices[$i]['price_value']."</b></td>".$add_to_cart_str ."</tr>";
						if($stop) break;
					}
				}
				
		$content.="</table>";
		
		return $content;
	}  // end page prices  generate

	  
 } // end class cart()
 
?>
