<?php
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_site >> lib_page >> lib_cms

 require_once('lib_mysql.php');

 class user extends mysql_db {

  //var $AccessLevel=0; 
  //var $user;
  static $uName;
  static $uLevel;
  static $uEmail;
  static $uID;
  const DEFAULT_WORK_FACTOR = 8;
  
  // mail var
    public $mail_parts;
    public $mail_to;
    public $mail_from;
    public $mail_headers;
    public $mail_subject;
    public $mail_body;
    public $u;
    public $_user;
    
   
    // user ([string user, string username])
    // konstructor 
    function __construct($username=null, $pass=null) 
    {
        parent::__construct();  

        $this->_user = (array) $this->authenticate_guest();

        if(empty($username) && empty($pass)) {
            if(isset($_SESSION['user'])) {
                $session_user = $_SESSION['user'];
                $username = $session_user->username;
                $pass = $session_user->pass;
            }
        }

        if(!empty($username) && !empty($pass)){
            $this->login($username, $pass); 
        }

        self::$uName = $this->get_uInfo("Name");
        self::$uLevel = (int)$this->get_uLevel();
        self::$uEmail = $this->get_uInfo("EMail");
        self::$uID = (int)$this->get_uInfo("ID");
    
    }

    public function authenticate_guest()
    {
         return (object) array(
        'ID' => null,
        'Name' => null, 
        'EMail' => null, 
        'username' => null,
        'pass' => null,
        'InitPage' => null,
        'InitSiteID' => null,
        'AccessLevel' => 0,
        'WriteLevel' => 0,
        'ReadLevel' => 0,
        'FName' => null,
        'sl' => null,
        'RA' => null, 
        'Phone' => null, 
        'Fax' => null, 
        'Address' => null,
        'MOL' => null,
        'dn' => null,
        'bulstat' => null
    );
    }
    
    function login($username, $pass)
    {
       $result = $this->db_query("SELECT 
           users.ID, users.Name, users.EMail, users.username, users.pass, users.InitPage, users.InitSiteID, users.AccessLevel, 
            Firmi.Name as FName, Firmi.sl, Firmi.RA, Firmi.Phone, Firmi.Fax, Firmi.Address, Firmi.MOL, Firmi.dn, Firmi.bulstat,
            sl.*
            FROM users
            LEFT JOIN Firmi ON users.FirmID = Firmi.ID
            LEFT JOIN sl on sl.userID  = users.ID
            WHERE users.username= BINARY('$username') AND pass= BINARY('$pass')");
        if ( $this->count($result) > 0  )
            { 
                //registered user
                $user = $this->fetch("array", $result); 
                $this->_user = $user;
                return true; 
             }
            else return false; 
    }

    function authorize($SiteID)
    {
        
    }
        
         
        
    function get_uInfo($return)
    {
        return isset($this->_user[$return]) ? $this->_user[$return] : null;
    }

    function insert_user_logs($ip, $log_time, $n, $SiteID)
    {
         $this->db_query(
             "INSERT INTO user_logs 
              SET 
                user='$this->ID',
                IP='$ip',
                logTime='$today',
                n='$n',
                SiteID='$SiteID' "
        );

        return $this->last_insert_id();
    }

    function clean_logs() {
        $this->db_query("DELETE FROM user_logs WHERE  logTime<=DATE_SUB(CURDATE(),INTERVAL 366 DAY);"); 
    }

    function extract_uInfo($uID, $return="")
        {
            $user_q = $this->db_query("SELECT * FROM users WHERE ID='$uID'  LIMIT 1 "); 
            $user = $this->fetch("array", $user_q); 
            if($return == "")
                return $user;       
            else  return $user[$return];
        }
            
    function get_uName() {   return $this->_user['Name']; }
        
    function print_uName()
        {
            echo "<a href=\"#\">".$this->get_uName()."</a>";        
        }

    function get_uEmail() {   return $this->_user['EMail']; }
    
        public static function hash($password, $work_factor = 0)
        {
            if (version_compare(PHP_VERSION, '5.3') < 0) throw new \Exception('Bcrypt s PHP 5.3 or above');
            if (! function_exists('openssl_random_pseudo_bytes')) {
                throw new \Exception('Bcrypt s openssl PHP extension');
            }
            if ($work_factor < 4 || $work_factor > 31) $work_factor = self::DEFAULT_WORK_FACTOR;
            $salt = 
                '$2a$' . str_pad($work_factor, 2, '0', STR_PAD_LEFT) . '$' .
                substr(
                    strtr(base64_encode(openssl_random_pseudo_bytes(16)), '+', '.'), 
                    0, 22
                )
            ;
            return crypt($password, $salt);
        }
        public static function check($password, $stored_hash, $legacy_handler = NULL)
        {
            if (version_compare(PHP_VERSION, '5.3') < 0) throw new \Exception('Bcrypt s PHP 5.3 or above');
            if (self::is_legacy_hash($stored_hash)) {
                if ($legacy_handler) return call_user_func($legacy_handler, $password, $stored_hash);
                else throw new \Exception('Unsupported hash format');
            }
            return crypt($password, $stored_hash) == $stored_hash;
        }
        public static function is_legacy_hash($hash) { return substr($hash, 0, 4) != '$2a$'; }
        
    function print_uEmail()
        {
            echo "<a href=\"mailto:".$this->get_uEmail()."\">".$this->get_uEmail()."</a>";      
        }

    function get_uID()
    {   
        return $this->get_uInfo("ID");      
    }

    function get_user_by($db, $field, $value) 
    {
        $fields = array('ID', 'username', 'EMail');
        if(!in_array($field, $fields)) {
            $field = 'EMail';
        }
        $sql = "
            SELECT * FROM maksoft.users u 
            LEFT JOIN maksoft.SiteAccess s on s.userID = u.ID
            WHERE u.username = :val
            AND s.SiteID = :SiteID
            LIMIT 1";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(":val", $value);
        $stmt->bindValue(":SiteID", \site::$sID);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }


    function register($db, $data)
    {
        $sql = "
            INSERT INTO `users`
            (`FirmID`, `ParentUserID`, `Name`, `EMail`, `Phone`, `username`, `pass`, `InitPage`, `InitSiteID`, `AccessLevel`)
            VALUES
            (:FirmID,:ParentUserID,:Name,:EMail,:Phone,:username, :pass, :InitPage, :InitSiteID, :AccessLevel)";
        $stmt = $db->prepare($sql);
        $stmt->execute($data);
        return $db->lastInsertId();
    }

    public function set_access_level($db, $init_page, $SiteID, $ReadLevel, $WriteLevel, $notification=0, $userID)
    {
        $sql = "INSERT INTO
                SiteAccess 
                    (SiteID, init_page, ReadLevel, WriteLevel, notifications, userID)
                VALUES 
                    (:SiteID, :init_page, :ReadLevel, :WriteLevel, :notifications, :userID)";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(":init_page", $init_page);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":ReadLevel", $ReadLevel);
        $stmt->bindValue(":WriteLevel", $WriteLevel);
        $stmt->bindValue(":notifications", $notification);
        $stmt->bindValue(":userID", $userID);
        $stmt->execute();
        return $db->lastInsertId();
    }

    public function get_access_level($db)
    {
        $sql = "SELECT * FROM SiteAccess WHERE SiteID = :SiteID and userID = :userID";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(":SiteID", self::$sID);
        $stmt->bindValue(":userID", self::$uID);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }
    
    //return slID of the user
    function get_uslID($userID=0)
        {
            if($userID == 0) $userID = $this->get_uID(); 
            $slID = 0;
            $result = $this->db_query("SELECT * FROM sl WHERE userID = '".$userID."' LIMIT 1");
            $sl = $this->fetch("array", $result);
            $slID = $sl['slID'];
            return $slID;
        }
        
    // return responsible emplyee ReplyTo address from an EMail address
    function get_reply_to($EMail) {
        $result = $this->db_query("SELECT sl.EMail, sl.FullName FROM Firmi, sl WHERE Firmi.EMail LIKE '$EMail' AND sl<255 AND sl.slID=Firmi.sl LIMIT 1 "); 
        $reply_to = $this->fetch("object", $result); 
        $reply_to_str = $this->CyrLat($reply_to->FullName)."<".$reply_to->EMail.">"; 
        return $reply_to_str;       
    }   
        
    function CyrLat ($s) {
       $cyr = array ("�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�", "�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","� ","�", "�", "��", "�","�");
       $lat = array  ("a","b","v","g","d","e","g","z","i","i","k","l","m","n","o","p","r","s","t","u","f","h","c","ch","sh","sht","u", "i","iu","ia","A","B","V","G","D","E","G","Z","I","I","K","L","M","N","O","P","R","S","T","U","F","H","C","Ch","Sh","Sht","U", "IO", "Ju","Q");
       for ($i=0; $i<count($cyr); $i++) {
        $s = str_replace($cyr[$i], $lat[$i], $s);
       }
       return $s;
     }  
        
     function mime_mail()
         {
              $this->mail_parts = array();
              $this->mail_to =  "";
              $this->mail_from =  "";
              $this->mail_subject =  "";
              $this->mail_body =  "";
              $this->mail_headers =  "";
         }

   /*
    *     void add_attachment(string message, [string name], [string ctype])
    *     Add an attachment to the mail object
    */

    function add_attachment($message, $name =  "", $ctype = "application/octet-stream")
        {
          $this->mail_parts[] = array (
                "ctype" => $ctype,
                "message" => $message,
                "encode" => $encode,
                "name" => $name
                );
       }

   /*
    *      void build_message(array part=
    *      Build message parts of an multipart mail
    */

   function build_message($part) {
      $message = $part[ "message"];
      $message = chunk_split(base64_encode($message));
      $encoding =  "base64";
      return  "Content-Type: ".$part[ "ctype"].
         ($part[ "name"]? "; name = \"".$part[ "name"].
         "\"" :  "").

         "\nContent-Transfer-Encoding: $encoding\n\n$message\n";
   }

   /*
    *      void build_multipart()
    *      Build a multipart mail
    */

   function build_multipart() {
      $boundary =  "b".md5(uniqid(time()));
      if($this->mail_ctype==0) {$multipart = "Content-Type: multipart/related; charset=\"windows-1251\"; boundary = $boundary\n\nThis is a MIME encoded message.\n\n--$boundary";}
      //$multipart = "Content-Type: multipart/mixed; charset=\"windows-1251\"; boundary = $boundary\n\nThis is a MIME encoded message.\n\n--$boundary";
     // $multipart = "Content-Type: multipart/related; type=\"text/html\"; charset=\"windows-1251\";  boundary = $boundary\n\nThis is a MIME encoded message.\n\n--$boundary";
      if($this->mail_ctype==1) {$multipart = "Content-Type: multipart/related; type=\"text/html\"; charset=\"utf-8\";  boundary = $boundary\n\nThis is a MIME encoded message.\n\n--$boundary";}

         for($i = sizeof($this->mail_parts)-1; $i >= 0; $i--)
      {
         $multipart .=  "\n".$this->build_message($this->mail_parts[$i]).
            "--$boundary";
      }
      return $multipart.=  "--\n";
   }

   /*
    *      string get_mail()
    *      returns the constructed mail
    */

   function get_mail($complete = true) {
      $mime =  "";
      if (!empty($this->mail_from))
         $mime .=  $this->CyrLat("From: ".$this->mail_from. "\n");
      if (!empty($this->mail_headers))
         $mime .= $this->mail_headers. "\n";

      if ($complete) {
         if (!empty($this->mail_to)) {
            $mime .= "To: $this->mail_to\n";
         }
         if (!empty($this->mail_subject)) {
            $mime .= "Subject: $this->mail_subject\n";
         }
      }

      if (!empty($this->mail_body))
         $this->add_attachment($this->mail_body,  "",  "text/html");   // plain
      $mime .=  "MIME-Version: 1.0\n".$this->build_multipart();

      return $mime;
   }

   /*
    *      void send()
    *      Send the mail (last class-function to be called)
    */

   function mail_send() {
      //$mime = $this->get_mail(false);   
    //  $mime = $this->get_mail(true);
    // mail($this->mail_to, $this->mail_subject,  "", $mime);

       $headers = "MIME-Version: 1.0" . "\r\n";
       $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

       // More headers
       $headers .= 'From: <'.$this->mail_from.'>' . "\r\n";

       //mail($to,$subject,$message,$headers);
       mail($this->mail_to, $this->mail_subject,$this->mail_body,$headers);

   }
   
   function mail_queue() {
    $this->db_query(" INSERT INTO mails_outbox SET mail_to='$this->mail_to' , mail_subject = ' $this->mail_subject', mail_ctype = '$this->mail_ctype', mail_reply_to = '$this->mail_from', mail_text = '$this->mail_text' "); 
   }

     function add_date($givendate,$day=0,$mth=0,$yr=0) {
         $cd = strtotime($givendate);
         $newdate = date('Y-m-d h:i:s', mktime(date('h',$cd),
             date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
             date('d',$cd)+$day, date('Y',$cd)+$yr));
         return $newdate;
     }


     //return all user tasks, selected by $filter
    function get_tasks($to_user, $start_date="", $filter = 1)
        {
            if($start_date == "")
                $row=$this->db_query("SELECT * FROM tasks WHERE to_user='$to_user' AND completed='0' AND start_date<NOW() AND $filter ORDER BY priority ASC,  start_date ASC  ");
            else 
                $row=$this->db_query("SELECT * FROM tasks WHERE to_user='$to_user' AND completed='0' AND start_date>='".$start_date."' AND $filter ORDER BY priority ASC,  start_date ASC ");       
            return $row; 
        }

    //return e specific user task 
    function get_task($taskID) 
        {
            $query=$this->db_query("SELECT * FROM tasks WHERE taskID='$taskID' "); 
            $row=$this->fetch("object", $query); 

            return $row;            
        }

     function complete_task($taskID) {
         $row = $this->get_task($taskID);
         if($row->repeat_in>0) {
             $start_date = $this->add_date($row->start_date, $row->repeat_in, 0, 0);
             $this->db_query("UPDATE tasks SET start_date='$start_date' WHERE taskID='$taskID' ");
         } else {
             $this->db_query("UPDATE tasks SET completed ='1' WHERE taskID='$taskID' ");
         }
     }
        
    //return all user tasks assigned to other users
    function assigned_tasks($from_user)
        {
            $row=$this->db_query("SELECT * FROM tasks WHERE from_user='$from_user' AND from_user<>to_user AND completed='0' AND start_date<NOW() ORDER BY start_date DESC, priority ASC "); 
            return $row; 
        }
        
    //return user tasks count, selcted by $filter
    function count_user_tasks($to_user, $start_date="", $filter=1)
        {
            $result = $this->get_tasks($to_user, $start_date="", $filter);
            $u_tasks = array();
            while($tasks = $this->fetch("array", $result))
                $u_tasks[] = $tasks;                
            return count($u_tasks);
        }
        
    //insert a new task assigned by the selected user
    function write_task($taskID, $task_set)
        { 
            if ($taskID>0) {
            //update_task
                $date_modified = date_time("Y-m-d H:i:s");
                $this->db_query("UPDATE tasks SET $task_set , date_modified='$date_modified' WHERE taskID='$taskID' "); 
            } else { //new task
                $date_added = date_time("Y-m-d H:i:s");
                $this->db_query("INSERT INTO tasks SET $task_set , date_added='$date_added' "); 
            }
        }
        
    //return user access level
    function get_uLevel()
        {   
            return (int)$this->get_uInfo("AccessLevel");        
        }
            
    //return all sites, that user has access
    function get_uSites($user_id, $filter=1)
        {
            $result = $this->db_query("SELECT * FROM SiteAccess sa right join Sites s on sa.SiteID = s.SitesID WHERE sa.userID = ".$user_id." AND $filter ORDER by s.Name ASC");
            $uSites = array();
            while($row = $this->fetch("array", $result))
                $uSites[] = $row;
            return $uSites;
        }

    function sl_work_add() {
       $slID = $this->get_uslID();

        if($slID>0) {

            $userID = $this->_user['ID'];

            $res = $this->db_query("SELECT * FROM sl_work WHERE userID = '$userID'  ORDER BY start_time DESC LIMIT 1");
            
            if($row = $this->fetch("object", $res)) {
                //$hours_at_work =  round((strtotime(date()) - strtotime($row->start_time))/60/60, 0);
                $date1 = new DateTime();

                $date2 = new DateTime($row->start_time);
                $diff = $date2->diff($date1);


                $hours = $diff->h;
                $hours = $hours + ($diff->days*24);

                $this->db_query("UPDATE sl_work SET end_time = NOW(), hours_at_work = '$hours' WHERE sl_work_id = '".$row->sl_work_id."' LIMIT 1 ");

            }
            else {
              //  $this->db_query("INSERT INTO sl_work SET userID = '$uID',  start_time ='$today' ");
            }
        }
    }
    
    // user device
    function device_detect() {
    
        $tablet_browser = 0;
        $mobile_browser = 0;
         
        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
        }
         
        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
        }
         
        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
        }
         
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
            'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
            'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
            'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
            'newt','noki','palm','pana','pant','phil','play','port','prox',
            'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
            'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
            'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
            'wapr','webc','winw','winw','xda ','xda-');
         
        if (in_array($mobile_ua,$mobile_agents)) {
            $mobile_browser++;
        }
         
        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
              $tablet_browser++;
            }
        }
         
        if ($tablet_browser > 0) {
           $device_res = "tablet"; 
        }
        else if ($mobile_browser > 0) {
           $device_res = "mobile"; 
        }
        else {
           $device_res = "desktop";
        }  
        
        return $device_res; 
        
    }
    
    //geo ip
     function user_data(){
    
        include "geoip/geoip.inc";
        
        $geoip_open = geoip_open("/data/hosting/maksoft/maksoft/lib/geoip/GeoIP.dat", GEOIP_STANDARD);
        
        $UserData['ip'] = $_SERVER['REMOTE_ADDR'];
        $UserData['country_code'] = geoip_country_code_by_addr($geoip_open, $UserData['ip']);
        $UserData['country_name'] = geoip_country_name_by_addr($geoip_open, $UserData['ip']);
        $UserData['browser']  = $_SERVER['HTTP_USER_AGENT'];
        geoip_close($geoip_open);

        return $UserData;
        
    }
    /*
        check if there is a valid email, then make second check for vali MX record
        @param return bool
    */
    function mail_validator($mail){
    preg_match("/[\w_.-]{2,30}+[@]+([\w-]{2,15}+[.][a-z]{2,10})/", $mail, $output);
    if (count($output) > 0) :
        $arr= dns_get_record($output[1], DNS_MX);
        if($arr[0]['host']==$output[1]&&!empty($arr[0]['target'])):
                #  return $arr[0]['target'];  #for testing 
                return true;
        endif;
    endif;
    return false;
    }

    public function domain_exists($email, $record = 'MX')
    {
        if(!$this->emailValidator($email)){
            return false;
        }
        list($user, $domain) = explode('@', $email);
        return checkdnsrr($domain, $record);
    }

    protected function emailValidator($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

     public function emailSubscribed($email) {

         $email = trim($email);

         $res = $this->db_query("SELECT * FROM IgnoreList WHERE EMail LIKE '%$email%'");
         if($this->count($res)>0) {
             return false;
         } else {
             return $this->emailValidator($email);
         }


    }

     function logout() {
         $this->_user();
     }

     // destructor
     function _user() {
         session_unset();
     }
    
 } // end class user
 

?>
