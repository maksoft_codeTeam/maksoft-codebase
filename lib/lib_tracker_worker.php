<?php
use \PhpAmqpLib\Connection\AMQPStreamConnection;
require_once __DIR__.'/lib_tracker.php';


class Worker extends Tracker
{
    protected $acknowledgment=false;

    protected $db;

    protected $records = array();

    public function __construct($pdo_database)
    {
        $this->db = $pdo_database;
        parent::__construct();

        $this->channel->basic_qos(null, 1, null);

        $this->channel->basic_consume($this->queue_name, '', false, $this->acknowledgment, false, false, array($this, 'callback'));
    }

    public function callback($msg)
    {
        $data = json_decode($msg->body);
        $this->records[] = $data;
        $records_count = count($this->records);
        if($records_count > 200){
            $this->db->beginTransaction();
            try{
                $this->save();
                $this->db->commit();
            } catch(Exception $e) {
                $this->db->rollback();
                echo PHP_EOL.'error occurs: msg['.$e->getMessage().']'.PHP_EOL;
                #$this->db->rollBack();
            }
        }

        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }

    protected function save(){
        #$this->db->beginTransaction();
        $sql = "
            INSERT INTO `track_log`(`site_id`, `page_n`, `action_id`, `user_id`, `ip`, `session_id`, `referer`)
            VALUES (:site_id, :page_n, :action_id, :user_id, :ip, :session_id, :referer)
        ";
        $stmt = $this->db->prepare($sql);
        while($this->records){
            $record = array_shift($this->records);
                $stmt->bindValue(":site_id", $record->site_id);
                $stmt->bindValue(":page_n", $record->page_n);
                $stmt->bindValue(":action_id", $record->action_id);
                $stmt->bindValue(":user_id", $record->user_id);
                $stmt->bindValue(":ip", $record->ip);
                $stmt->bindValue(":referer", $record->referer);
                $stmt->bindValue(":session_id", $record->session_id);
                echo '.';
                $stmt->execute();
        }
    }

    public function listen()
    {
        while(count($this->channel->callbacks)) {
            $this->channel->wait();
        };
    }
}
