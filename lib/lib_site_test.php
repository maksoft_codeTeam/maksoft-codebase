<?php
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_site >> lib_page >> lib_cms
 require_once('lib_user.php');

class site extends user
{
  var $SiteID;
  var $StartPage;
  
  static $sHome;
  static $sID;
  static $sStartPage;
  static $uWriteLevel;
  static $uReadLevel;
  static $sMAIN_WIDTH;
  
   // site( [int site identification number; int site number; ] )
	// constructor 
	function site()
	{
		parent::user(); 
		$num_args = func_num_args(); 
		
		if (($num_args == 0) )
			{
				if(isset($_GET['SiteID']))
			  		$this->SiteID =  $_GET['SiteID'];
				else if(isset($_POST['SiteID']))
					$this->SiteID =  $_POST['SiteID'];
				else $this->SiteID = $this->get_sInfo("SiteID");
				
				
				if(!isset($this->SiteID))
				{
					//global $_SERVER['SERVER_NAME'];
					
					$url = $_SERVER['SERVER_NAME']; 
					$site_url_q = mysqli_query("SELECT * FROM Sites WHERE `url` LIKE '%$url%' "); 
					if (mysqli_num_rows($site_url_q)>0)
						{
							$site = mysqli_fetch_object($site_url_q); 
							$this->SiteID = $site->SitesID; 
							$this->n = $site->StartPage;
						}
				}
			
			}

		if ($num_args == 1)
		  $this->SiteID = func_get_arg(0); 

	    if ($num_args > 1)
		 $this->error_handler('Class site not defined properly!  site( [int site identification number; [int site number]] ',C_USER_ERROR,__FILE__,__LINE__,__FUNCTION__,__CLASS__,microtime());
		 
		 $this->StartPage = $this->get_sInfo("StartPage");
		 
		 site::$sHome = $this->get_sInfo("Home");
		 site::$sID = $this->get_sInfo("SitesID");
		 site::$sStartPage = $this->get_sInfo("StartPage");
		 site::$uWriteLevel = (int)$this->get_site_permissions("WriteLevel");
		 //if(!isset(site::$uWriteLevel)) site::$uWriteLevel = 0;
	 
		 site::$uReadLevel = (int)$this->get_site_permissions("ReadLevel");
		 //if(!isset(site::$uReadLevel)) site::$uReadLevel = 0;
		 
		 site::$sMAIN_WIDTH = $this->get_sConfigValue("CMS_MAIN_WIDTH");
		 if(site::$sMAIN_WIDTH == 0) site::$sMAIN_WIDTH = 450;
	}
	
	//search for user permisions on that site
	function get_site_permissions($return)
		{
			$query = $this->db_query("SELECT * FROM SiteAccess WHERE userID= '".user::$uID."' AND SiteID = '".$this->SiteID."' LIMIT 1"); 
			$permissions = mysqli_fetch_array($query); 
			return $permissions[$return]; 	
		}
	
		
	//return site array
	function get_site()
		{
			 $query = $this->db_query("SELECT * FROM Sites WHERE SitesID = '$this->SiteID' LIMIT 1"); 
			 $this->row = mysqli_fetch_array($query); 
			 return $this->row; 
		}
	
	//return site configurations
	function get_sConfigurations()
		{
			$c_query = $this->db_query("SELECT * FROM configuration WHERE SiteID = '$this->SiteID' ");
			$i=0;
			while($c_array = mysqli_fetch_array($c_query))
				$conf[$i++] = $c_array;
			return $conf;
		}
	
	//return the site configuration value; 
	//get_sConfigValue("MAIN_WIDTH") returns the site main width (if defined)
	function get_sConfigValue($return)
	{
		$value = NULL;
		$config_values = $this->get_sConfigurations();
		for($i=0; $i<count($config_values); $i++)
			if($config_values[$i]['conf_key'] == $return)  
				$value = $config_values[$i]['conf_value'];
		return $value;
	}
	
	//print site configurations
	function print_sConfigurations()
		{	
			$conf_str = "";
			$c_array = $this->get_sConfigurations();
			for($i=0; $i<count($c_array); $i++)
				$conf_str.= "define(\"".$c_array[$i]['conf_key'] . "\",  \"" . $c_array[$i]['conf_value'] . "\"); ";
			eval($conf_str);
			//print_r($conf_str);
		}
			
	//return site All info
	function get_sInfo($return)
		{
			$sInfo = $this->get_site();
			return $sInfo[$return]; 
		}
	
	//get and print SiteID
	function get_sId()
		{
			$sId = $this->get_sInfo("SitesID");
			return $sId; 
		}
	function print_sId()
		{
			print $this->get_sId();
		}
		
	//get and print site administration Name
	function get_sName()
		{
			$sName = $this->get_sInfo("Name");
			return $sName; 
		}
	function print_sName()
		{
			print $this->get_sName();
		}
	
	//get and print site Title		
	function get_sTitle()
		{
			$sTitle = $this->get_sInfo("Title");
			return $sTitle; 
		}
	function print_sTitle()
		{
			print $this->get_sTitle();
		}

	//get and print site URL
	function get_sURL()
		{
			$sURL = $this->get_sInfo("url");
			return $sURL; 
		}
	function print_sURL()
		{
			print ("<a href=\"#\">".$this->get_sURL()."</a>");
		}

	//get and print site contact EMail 	
	function get_sEmail()
		{
			$sEmail = $this->get_sInfo("EMail");
			return $sEmail; 
		}
	function print_sEMail()
		{
			print $this->get_sEMail();
		}
	
	//get and print site StartPage number 	
	function get_sStartPage()
		{
			$sStartPage = $this->get_sInfo("StartPage");
			return $sStartPage; 
		}
	function print_sStartPage()
		{
			print $this->get_sStartPage();
		}
			
	//get and print site Description	
	function get_sDescription()
		{
			$sDescription = $this->get_sInfo("Description");
			return $sDescription; 
		}	
	function print_sDescription()
		{
			print $this->get_sDescription();
		}
		
	//get and print site Keywords			
	function get_sKeywords()
		{
			$sKeywords = $this->get_sInfo("Keywords");
			return $sKeywords; 
		}
	function print_sKeywords()
		{
			print $this->get_sKeywords();
		}
		
	//get and initialize site Initial PHP code	
	function get_sInitPHP()
		{
			$sInitPHP = $this->get_sInfo("initPHP");
			return $sInitPHP; 
		}
	function print_sInitPHP()
		{
			eval($this->get_sInitPHP());
		}		

	//get and print site homepage Title	
	function get_sHome()
		{
			$sHome = $this->get_sInfo("Home");
			return $sHome; 
		}
	function print_sHome()
		{
			print $this->get_sHome();
		}
		
	function get_sMoreText()
		{
			return $this->get_sInfo("MoreText");
		}
		
	
	function get_sCopyright()
		{
			return $this->get_sInfo("copyright");
		}
	function print_sCopyright()
		{
			print $this->get_sCopyright();
		}
		
	function get_sNewsPage()
		{
			return $this->get_sInfo("news");
		}
	
	//return site image directory
	function get_sImageDir($return = "image_dir")
		{
			//$dirs = array();
			$result = $this->db_query("SELECT * FROM image_dirs WHERE ID = '".$this->get_sInfo("imagesdir")."'");
			$dir = mysqli_fetch_array($result);
			return $dir[$return];
		}
	
	function do_search($Search, $max_num_of_res=15) {
	 $num_of_res = 0; 
	 if($Search=="") {
	  $Search = $_POST['Search']; 
	 }
	 if($Search=="") {
	  $Search = $_POST['search']; 
	 } 
	 if(strlen($Search)>2) {
	 	 $search_level = 1; 	// search in page title, Name, tags exact phrase
		 $add_query=""; 
			while(($num_of_res<=$max_num_of_res) && ($search_level<=4)) { 
				 if($search_level==2) {$add_query= "$add_query  OR (textStr LIKE '%$Search%') ";}  // search in page text  exact phrase
				 if($search_level>=3) { // search in page Name, title, tags diferent words
				 	$search_cleaned = eregi_replace(",", "", $Search); 
				 	$search_words = split(" ", $search_cleaned);    // max 10 words to search for
					foreach($search_words as $s_word) {
						if(strlen($s_word)>2) {
							if ($search_level==3) {$add_query= "$add_query  OR  (Name LIKE '%$s_word%' ) OR (tags LIKE '%$s_word%') "; }; 
							if ($search_level==4) {$add_query= "$add_query  OR  (textStr LIKE '%$s_word%' )  "; }; 
							}
					}
				 }
				 $query_str = "SELECT p.n, p.Name, p.title, p.SiteID, p.textStr FROM pages p WHERE (SiteID='$this->SiteID' ) AND ( (Name LIKE '%$Search%') OR  (title LIKE '%$Search%') OR  (tags LIKE '%$Search%')  $add_query ) "; 
				 //if (user::$uID==1) {echo("$query_str"); }
				 //echo("$add_query   <br>$search_level $query_str<br>");
				 $s_query = $this->db_query("$query_str"); 
				 while(($num_of_res<$max_num_of_res) && ($s_row=mysqli_fetch_array($s_query))) {
				 	  $already_found = false; 
					  foreach($s_results as $row) {
						   if($row['n']==$s_row['n']) {
						   	$already_found=true; 
						   } 
					  }				  
					  if (!($already_found)) {
					  	  $s_row['textStr'] = strip_tags($s_row['textStr']); 
						  $pos = strripos($s_row['textStr'],$Search); 
						  $s_row['textStr'] = substr($s_row['textStr'], $pos, 250); 
						  $s_results[] = $s_row; 	 				 
						  $num_of_res++; 
						  }
				 }
				 $search_level++; 
			} // max_num of results reached
	 } // there is no Search var defined
	 return $s_results; 
	}
	
	function print_search($s_results){
	 foreach($s_results as $res) {
	  //$res['textStr'] = substr($s_results['textStr'], 0, 100); 
	  echo "<li><a href=page.php?n=".$res['n']." title=".$res['title'].">".$res['Name']."</a><br>".$res['textStr']."...<br><br>"; 
	 }
	}
}
?>