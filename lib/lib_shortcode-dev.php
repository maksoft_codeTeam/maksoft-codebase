<?php



class ShortCode
{
    private $regex;
    private $spaces;
    private $merged_filters;
    private $wp_current_filter;
    private $wp_filter=array();
    private $shortcode_tags=array();



    /**
     * Separate HTML elements and comments from the text.
     *
     * @since 4.2.4
     *
     * @param string $input The text which has to be formatted.
     * @return array The formatted text.
     */
    function wp_html_split( $input ) {
        return preg_split( $this->get_html_split_regex(), $input, -1, PREG_SPLIT_DELIM_CAPTURE );
    }
    /**
     * Retrieve the regular expression for an HTML element.
     *
     * @since 4.4.0
     *
     * @return string The regular expression
     */
    function get_html_split_regex() {
        if ( !$this->regex ) {
            $comments =
                  '!'           // Start of comment, after the <.
                . '(?:'         // Unroll the loop: Consume everything until --> is found.
                .     '-(?!->)' // Dash not followed by end of comment.
                .     '[^\-]*+' // Consume non-dashes.
                . ')*+'         // Loop possessively.
                . '(?:-->)?';   // End of comment. If not found, match all input.
            $cdata =
                  '!\[CDATA\['  // Start of comment, after the <.
                . '[^\]]*+'     // Consume non-].
                . '(?:'         // Unroll the loop: Consume everything until ]]> is found.
                .     '](?!]>)' // One ] not followed by end of comment.
                .     '[^\]]*+' // Consume non-].
                . ')*+'         // Loop possessively.
                . '(?:]]>)?';   // End of comment. If not found, match all input.
            $escaped =
                  '(?='           // Is the element escaped?
                .    '!--'
                . '|'
                .    '!\[CDATA\['
                . ')'
                . '(?(?=!-)'      // If yes, which type?
                .     $comments
                . '|'
                .     $cdata
                . ')';
            $this->regex =
                  '/('              // Capture the entire match.
                .     '<'           // Find start of element.
                .     '(?'          // Conditional expression follows.
                .         $escaped  // Find end of escaped element.
                .     '|'           // ... else ...
                .         '[^>]*>?' // Find end of normal element.
                .     ')'
                . ')/';
        }
        return $this->regex;
    }

    function shortcode_atts( $pairs, $atts, $shortcode = '' ) {
        $atts = (array)$atts;
        $out = array();
        foreach ($pairs as $name => $default) {
            if ( array_key_exists($name, $atts) )
                $out[$name] = $atts[$name];
            else
                $out[$name] = $default;
        }
        /**
         * Filter a shortcode's default attributes.
         *
         * If the third parameter of the shortcode_atts() function is present then this filter is available.
         * The third parameter, $shortcode, is the name of the shortcode.
         *
         * @since 3.6.0
         * @since 4.4.0 Added the `$shortcode` parameter.
         *
         * @param array  $out       The output array of shortcode attributes.
         * @param array  $pairs     The supported attributes and their defaults.
         * @param array  $atts      The user defined shortcode attributes.
         * @param string $shortcode The shortcode name.
         */
        if ( $shortcode ) {
            $out = $this->apply_filters( "shortcode_atts_{$shortcode}", $out, $pairs, $atts, $shortcode );
        }
        return $out;
    }

    function unescape_invalid_shortcodes( $content ) {
        // Clean up entire string, avoids re-parsing HTML.
        $trans = array( '&#91;' => '[', '&#93;' => ']' );
        $content = strtr( $content, $trans );
        return $content;
    }

    function strip_shortcodes( $content ) {
        if ( false === strpos( $content, '[' ) ) {
            return $content;
        }
        if (empty($this->shortcode_tags) || !is_array($this->shortcode_tags))
            return $content;
        // Find all registered tag names in $content.
        preg_match_all( '@\[([^<>&/\[\]\x00-\x20=]++)@', $content, $matches );
        $tagnames = array_intersect( array_keys( $this->shortcode_tags ), $matches[1] );
        if ( empty( $tagnames ) ) {
            return $content;
        }
        $content = $this->do_shortcodes_in_html_tags( $content, true, $tagnames );
        $pattern = $this->get_shortcode_regex( $tagnames );
        $content = preg_replace_callback( "/$pattern/", array($this, 'strip_shortcode_tag'), $content );
        // Always restore square braces so we don't break things like <!--[if IE ]>
        $content = $this->unescape_invalid_shortcodes( $content );
        return $content;
    }

    function replace_shortcodes($text){
        return $this->do_shortcode($text);
    }
    
    /**
     * Don't auto-p wrap shortcodes that stand alone
     *
     * Ensures that shortcodes are not wrapped in `<p>...</p>`.
     *
     * @since 2.9.0
     *
     * @global array $shortcode_tags
     *
     * @param string $pee The content.
     * @return string The filtered content.
     */
    function shortcode_unautop( $pee ) {
        if ( empty( $this->shortcode_tags ) || !is_array( $this->shortcode_tags ) ) {
            return $pee;
        }
        $tagregexp = join( '|', array_map( 'preg_quote', array_keys( $this->shortcode_tags ) ) );
        $spaces = $this->wp_spaces_regexp();
        $pattern =
              '/'
            . '<p>'                              // Opening paragraph
            . '(?:' . $spaces . ')*+'            // Optional leading whitespace
            . '('                                // 1: The shortcode
            .     '\\['                          // Opening bracket
            .     "($tagregexp)"                 // 2: Shortcode name
            .     '(?![\\w-])'                   // Not followed by word character or hyphen
                                                 // Unroll the loop: Inside the opening shortcode tag
            .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
            .     '(?:'
            .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
            .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
            .     ')*?'
            .     '(?:'
            .         '\\/\\]'                   // Self closing tag and closing bracket
            .     '|'
            .         '\\]'                      // Closing bracket
            .         '(?:'                      // Unroll the loop: Optionally, anything between the opening and closing shortcode tags
            .             '[^\\[]*+'             // Not an opening bracket
            .             '(?:'
            .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
            .                 '[^\\[]*+'         // Not an opening bracket
            .             ')*+'
            .             '\\[\\/\\2\\]'         // Closing shortcode tag
            .         ')?'
            .     ')'
            . ')'
            . '(?:' . $spaces . ')*+'            // optional trailing whitespace
            . '<\\/p>'                           // closing paragraph
            . '/';
        return preg_replace( $pattern, '$1', $pee );
    }
    /**
     * Returns the regexp for common whitespace characters.
     *
     * By default, spaces include new lines, tabs, nbsp entities, and the UTF-8 nbsp.
     * This is designed to replace the PCRE \s sequence.  In ticket #22692, that
     * sequence was found to be unreliable due to random inclusion of the A0 byte.
     *
     * @since 4.0.0
     *
     * @staticvar string $spaces
     *
     * @return string The spaces regexp.
     */
    function wp_spaces_regexp() {
        $this->spaces = '';
        if ( empty( $this->spaces ) ) {
            /**
             * Filter the regexp for common whitespace characters.
             *
             * This string is substituted for the \s sequence as needed in regular
             * expressions. For websites not written in English, different characters
             * may represent whitespace. For websites not encoded in UTF-8, the 0xC2 0xA0
             * sequence may not be in use.
             *
             * @since 4.0.0
             *
             * @param string $spaces Regexp pattern for matching common whitespace characters.
             */
            $this->spaces = $this->apply_filters( array($this, 'wp_spaces_regexp'), '[\r\n\t ]|\xC2\xA0|&nbsp;' );
        }
        return $this->spaces;
    }

    function do_shortcodes_in_html_tags( $content, $ignore_html, $tagnames ) {
        // Normalize entities in unfiltered HTML before adding placeholders.
        $trans = array( '&#91;' => '&#091;', '&#93;' => '&#093;' );
        $content = strtr( $content, $trans );
        $trans = array( '[' => '&#91;', ']' => '&#93;' );
        $pattern = $this->get_shortcode_regex( $tagnames );
        $textarr = $this->wp_html_split( $content );
        foreach ( $textarr as &$element ) {
            if ( '' == $element || '<' !== $element[0] ) {
                continue;
            }
            $noopen = false === strpos( $element, '[' );
            $noclose = false === strpos( $element, ']' );
            if ( $noopen || $noclose ) {
                // This element does not contain shortcodes.
                if ( $noopen xor $noclose ) {
                    // Need to encode stray [ or ] chars.
                    $element = strtr( $element, $trans );
                }
                continue;
            }
            if ( $ignore_html || '<!--' === substr( $element, 0, 4 ) || '<![CDATA[' === substr( $element, 0, 9 ) ) {
                // Encode all [ and ] chars.
                $element = strtr( $element, $trans );
                continue;
            }
            $attributes = $this->wp_kses_attr_parse( $element );
            if ( false === $attributes ) {
                // Some plugins are doing things like [name] <[email]>.
                if ( 1 === preg_match( '%^<\s*\[\[?[^\[\]]+\]%', $element ) ) {
                    $element = $this->preg_replace_callback( "/$pattern/", array($this, 'do_shortcode_tag'), $element );
                }
                // Looks like we found some crazy unfiltered HTML.  Skipping it for sanity.
                $element = strtr( $element, $trans );
                continue;
            }
            // Get element name
            $front = array_shift( $attributes );
            $back = array_pop( $attributes );
            $matches = array();
            preg_match('%[a-zA-Z0-9]+%', $front, $matches);
            $elname = $matches[0];
            // Look for shortcodes in each attribute separately.
            foreach ( $attributes as &$attr ) {
                $open = strpos( $attr, '[' );
                $close = strpos( $attr, ']' );
                if ( false === $open || false === $close ) {
                    continue; // Go to next attribute.  Square braces will be escaped at end of loop.
                }
                $double = strpos( $attr, '"' );
                $single = strpos( $attr, "'" );
                if ( ( false === $single || $open < $single ) && ( false === $double || $open < $double ) ) {
                    // $attr like '[shortcode]' or 'name = [shortcode]' implies unfiltered_html.
                    // In this specific situation we assume KSES did not run because the input
                    // was written by an administrator, so we should avoid changing the output
                    // and we do not need to run KSES here.
                    $attr = preg_replace_callback( "/$pattern/", array($this, 'do_shortcode_tag'), $attr );
                } else {
                    // $attr like 'name = "[shortcode]"' or "name = '[shortcode]'"
                    // We do not know if $content was unfiltered. Assume KSES ran before shortcodes.
                    $count = 0;
                    $new_attr = preg_replace_callback( "/$pattern/", array($this, 'do_shortcode_tag'), $attr, -1, $count );
                    if ( $count > 0 ) {
                        // Sanitize the shortcode output using KSES.
                        $new_attr = $this->wp_kses_one_attr( $new_attr, $elname );
                        if ( '' !== trim( $new_attr ) ) {
                            // The shortcode is safe to use now.
                            $attr = $new_attr;
                        }
                    }
                }
            }
            $element = $front . implode( '', $attributes ) . $back;
            // Now encode any remaining [ or ] chars.
            $element = strtr( $element, $trans );
        }
        $content = implode( '', $textarr );
        return $content;
    }

    function wp_kses_hair_parse( $attr ) {
        if ( '' === $attr ) {
            return array();
        }
        $regex =
          '(?:'
        .     '[-a-zA-Z:]+'   // Attribute name.
        . '|'
        .     '\[\[?[^\[\]]+\]\]?' // Shortcode in the name position implies unfiltered_html.
        . ')'
        . '(?:'               // Attribute value.
        .     '\s*=\s*'       // All values begin with '='
        .     '(?:'
        .         '"[^"]*"'   // Double-quoted
        .     '|'
        .         "'[^']*'"   // Single-quoted
        .     '|'
        .         '[^\s"\']+' // Non-quoted
        .         '(?:\s|$)'  // Must have a space
        .     ')'
        . '|'
        .     '(?:\s|$)'      // If attribute has no value, space is required.
        . ')'
        . '\s*';              // Trailing space is optional except as mentioned above.
        // Although it is possible to reduce this procedure to a single regexp,
        // we must run that regexp twice to get exactly the expected result.
        $validation = "%^($regex)+$%";
        $extraction = "%$regex%";
        if ( 1 === preg_match( $validation, $attr ) ) {
            preg_match_all( $extraction, $attr, $attrarr );
            return $attrarr[0];
        } else {
            return false;
        }
    }

    function wp_kses_attr_parse( $element ) {
        $valid = preg_match('%^(<\s*)(/\s*)?([a-zA-Z0-9]+\s*)([^>]*)(>?)$%', $element, $matches);
        if ( 1 !== $valid ) {
            return false;
        }
        $begin =  $matches[1];
        $slash =  $matches[2];
        $elname = $matches[3];
        $attr =   $matches[4];
        $end =    $matches[5];
        if ( '' !== $slash ) {
            // Closing elements do not get parsed.
            return false;
        }
        // Is there a closing XHTML slash at the end of the attributes?
        if ( 1 === preg_match( '%\s*/\s*$%', $attr, $matches ) ) {
            $xhtml_slash = $matches[0];
            $attr = substr( $attr, 0, -strlen( $xhtml_slash ) );
        } else {
            $xhtml_slash = '';
        }
        
        // Split it
        $attrarr = wp_kses_hair_parse( $attr );
        if ( false === $attrarr ) {
            return false;
        }
        // Make sure all input is returned by adding front and back matter.
        array_unshift( $attrarr, $begin . $slash . $elname );
        array_push( $attrarr, $xhtml_slash . $end );
        
        return $attrarr;
    }

    function get_shortcode_atts_regex() {
        return '/([\w-]+)\s*=\s*"([^"]*)"(?:\s|$)|([\w-]+)\s*=\s*\'([^\']*)\'(?:\s|$)|([\w-]+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/';
    }

    function shortcode_parse_atts($text) {
        $atts = array();
        $pattern = $this->get_shortcode_atts_regex();
        $text = preg_replace("/[\x{00a0}\x{200b}]+/u", " ", $text);
        if ( preg_match_all($pattern, $text, $match, PREG_SET_ORDER) ) {
            foreach ($match as $m) {
                if (!empty($m[1]))
                    $atts[strtolower($m[1])] = stripcslashes($m[2]);
                elseif (!empty($m[3]))
                    $atts[strtolower($m[3])] = stripcslashes($m[4]);
                elseif (!empty($m[5]))
                    $atts[strtolower($m[5])] = stripcslashes($m[6]);
                elseif (isset($m[7]) && strlen($m[7]))
                    $atts[] = stripcslashes($m[7]);
                elseif (isset($m[8]))
                    $atts[] = stripcslashes($m[8]);
            }
            // Reject any unclosed HTML elements
            foreach( $atts as &$value ) {
                if ( false !== strpos( $value, '<' ) ) {
                    if ( 1 !== preg_match( '/^[^<]*+(?:<[^>]*+>[^<]*+)*+$/', $value ) ) {
                        $value = '';
                    }
                }
            }
        } else {
            $atts = ltrim($text);
        }
        return $atts;
    }


    function do_shortcode_tag( $m ) {
        // allow [[foo]] syntax for escaping a tag
        if ( $m[1] == '[' && $m[6] == ']' ) {
            return substr($m[0], 1, -1);
        }
        $tag = $m[2];
        $attr = $this->shortcode_parse_atts( $m[3] );
        if ( ! is_callable( $this->shortcode_tags[ $tag ] ) ) {
            /* translators: %s: shortcode tag */
            $message = sprintf( __( 'Attempting to parse a shortcode without a valid callback: %s' ), $tag );
            return $m[0];
        }
        if ( isset( $m[5] ) ) {
            // enclosing tag - extra parameter
            return $m[1] . call_user_func( $this->shortcode_tags[$tag], array($this, $attr), $m[5], $tag ) . $m[6];
        } else {
            // self-closing tag
            return $m[1] . call_user_func( $this->shortcode_tags[$tag], array($this, $attr), null,  $tag ) . $m[6];
        }
    }


    function _wp_call_all_hook($args) {
        $this->wp_filter['all']->do_all_hook( $args );
    }

    /**
     * Call the functions added to a filter hook.
     *
     * The callback functions attached to filter hook $tag are invoked by calling
     * this function. This function can be used to create a new filter hook by
     * simply calling this function with the name of the new hook specified using
     * the $tag parameter.
     *
     * The function allows for additional arguments to be added and passed to hooks.
     *
     *     // Our filter callback function
     *     function example_callback( $string, $arg1, $arg2 ) {
     *         // (maybe) modify $string
     *         return $string;
     *     }
     *     add_filter( 'example_filter', 'example_callback', 10, 3 );
     *
     *     /*
     *      * Apply the filters by calling the 'example_callback' function we
     *      * "hooked" to 'example_filter' using the add_filter() function above.
     *      * - 'example_filter' is the filter hook $tag
     *      * - 'filter me' is the value being filtered
     *      * - $arg1 and $arg2 are the additional arguments passed to the callback.
     *     $value = apply_filters( 'example_filter', 'filter me', $arg1, $arg2 );
     *
     * @since 0.71
     *
     * @global array $wp_filter         Stores all of the filters.
     * @global array $merged_filters    Merges the filter hooks using this function.
     * @global array $wp_current_filter Stores the list of current filters with the current one last.
     *
     * @param string $tag     The name of the filter hook.
     * @param mixed  $value   The value on which the filters hooked to `$tag` are applied on.
     * @param mixed  $var,... Additional variables passed to the functions hooked to `$tag`.
     * @return mixed The filtered value after all hooked functions are applied to it.
     */
    function apply_filters( $tag, $value ) {
        $args = array();
        // Do 'all' actions first.
        if ( isset($this->wp_filter['all']) ) {
            $this->wp_current_filter[] = $tag;
            $args = func_get_args();
            $this->_wp_call_all_hook($args);
        }
        if ( !isset($wp_filter[$tag]) ) {
            if ( isset($this->wp_filter['all']) )
                array_pop($this->wp_current_filter);
            return $value;
        }
        if ( !isset($this->wp_filter['all']) )
            $this->wp_current_filter[] = $tag;
        // Sort.
        if ( !isset( $this->merged_filters[ $tag ] ) ) {
            ksort($this->wp_filter[$tag]);
            $this->merged_filters[ $tag ] = true;
        }
        reset( $this->wp_filter[ $tag ] );
        if ( empty($args) )
            $args = func_get_args();
        do {
            foreach ( (array) current($this->wp_filter[$tag]) as $the_ )
                if ( !is_null($the_['function']) ){
                    $args[1] = $value;
                    $value = call_user_func_array(array($this, $the_['function']), array_slice($args, 1, (int) $the_['accepted_args']));
                }
        } while ( next($this->wp_filter[$tag]) !== false );
        array_pop( $this->wp_current_filter );
        return $value;
    }
    /**
     * Execute functions hooked on a specific filter hook, specifying arguments in an array.
     *
     * @since 3.0.0
     *
     * @see apply_filters() This function is identical, but the arguments passed to the
     * functions hooked to `$tag` are supplied using an array.
     *
     * @global array $wp_filter         Stores all of the filters
     * @global array $merged_filters    Merges the filter hooks using this function.
     * @global array $wp_current_filter Stores the list of current filters with the current one last
     *
     * @param string $tag  The name of the filter hook.
     * @param array  $args The arguments supplied to the functions hooked to $tag.
     * @return mixed The filtered value after all hooked functions are applied to it.
     */
    function apply_filters_ref_array($tag, $args) {
        // Do 'all' actions first
        if ( isset($this->wp_filter['all']) ) {
            $this->wp_current_filter[] = $tag;
            $all_args = func_get_args();
            $this->_wp_call_all_hook($all_args);
        }
        if ( !isset($this->wp_filter[$tag]) ) {
            if ( isset($this->wp_filter['all']) )
                array_pop($this->wp_current_filter);
            return $args[0];
        }
        if ( !isset($this->wp_filter['all']) )
            $this->wp_current_filter[] = $tag;
        // Sort
        if ( !isset( $this->merged_filters[ $tag ] ) ) {
            ksort($this->wp_filter[$tag]);
            $this->merged_filters[ $tag ] = true;
        }
        reset( $this->wp_filter[ $tag ] );
        do {
            foreach ( (array) current($this->wp_filter[$tag]) as $the_ )
                if ( !is_null($the_['function']) )
                    $args[0] = call_user_func_array($the_['function'], array_slice($args, 0, (int) $the_['accepted_args']));
        } while ( next($this->wp_filter[$tag]) !== false );
        array_pop( $this->wp_current_filter );
        return $args[0];
    }

    function remove_shortcode($tag) {
        unset($this->shortcode_tags[$tag]);
    }

    function remove_all_shortcodes() {
        $this->shortcode_tags = array();
    } 

    function shortcode_exists( $tag ) {
        return array_key_exists( $tag, $this->shortcode_tags );
    }

    function has_shortcode( $content, $tag ) {
        if ( false === strpos( $content, '[' ) ) {
            return false;
        }
        if ( $this->shortcode_exists( $tag ) ) {
            preg_match_all( '/' . $thos->get_shortcode_regex() . '/', $content, $matches, PREG_SET_ORDER );
            if ( empty( $matches ) )
                return false;
            foreach ( $matches as $shortcode ) {
                if ( $tag === $shortcode[2] ) {
                    return true;
                } elseif ( ! empty( $shortcode[5] ) && $this->has_shortcode( $shortcode[5], $tag ) ) {
                    return true;
                }
            }
        }
        return false;
    }


    function add_shortcode($tag, $func) {
        if ( '' == trim( $tag ) ) {
            $message = __( 'Invalid shortcode name: Empty name given.' );
            echo ( __FUNCTION__. ' '. $message . ' ' .'4.4.0' );
            return;
        }
        if ( 0 !== preg_match( '@[<>&/\[\]\x00-\x20=]@', $tag ) ) {
            /* translators: 1: shortcode name, 2: space separated list of reserved characters */
            $message = sprintf( __( 'Invalid shortcode name: %1$s. Do not use spaces or reserved characters: %2$s' ), $tag, '& / < > [ ] =' );
            echo $message;
            return;
        }
        $this->shortcode_tags[ $tag ] = $func;
    }

    function do_shortcode( $content, $ignore_html = false ) {
        if ( false === strpos( $content, '[' ) ) {
            return $content;
        }
        if (empty($this->shortcode_tags) || !is_array($this->shortcode_tags))
            return $content;
        // Find all registered tag names in $content.
        preg_match_all( '@\[([^<>&/\[\]\x00-\x20=]++)@', $content, $matches );
        $tagnames = array_intersect( array_keys( $this->shortcode_tags ), $matches[1] );
        if ( empty( $tagnames ) ) {
            return $content;
        }
        $content = $this->do_shortcodes_in_html_tags( $content, $ignore_html, $tagnames );
        $pattern = $this->get_shortcode_regex( $tagnames );
        $content = preg_replace_callback( "/$pattern/", array($this, 'do_shortcode_tag'), $content );
        // Always restore square braces so we don't break things like <!--[if IE ]>
        $content = $this->unescape_invalid_shortcodes( $content );
        return $content;
    }

    function strip_shortcode_tag( $m ) {
        // allow [[foo]] syntax for escaping a tag
        if ( $m[1] == '[' && $m[6] == ']' ) {
            return substr($m[0], 1, -1);
        }
        return $m[1] . $m[6];
    }


    function get_shortcode_regex( $tagnames = null ) {
        if ( empty( $tagnames ) ) {
            $tagnames = array_keys( $this->shortcode_tags );
        }
        $tagregexp = join( '|', array_map('preg_quote', $tagnames) );
        // WARNING! Do not change this regex without changing do_shortcode_tag() and strip_shortcode_tag()
        // Also, see shortcode_unautop() and shortcode.js.
        return
              '\\['                              // Opening bracket
            . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
            . "($tagregexp)"                     // 2: Shortcode name
            . '(?![\\w-])'                       // Not followed by word character or hyphen
            . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
            .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
            .     '(?:'
            .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
            .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
            .     ')*?'
            . ')'
            . '(?:'
            .     '(\\/)'                        // 4: Self closing tag ...
            .     '\\]'                          // ... and closing bracket
            . '|'
            .     '\\]'                          // Closing bracket
            .     '(?:'
            .         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
            .             '[^\\[]*+'             // Not an opening bracket
            .             '(?:'
            .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
            .                 '[^\\[]*+'         // Not an opening bracket
            .             ')*+'
            .         ')'
            .         '\\[\\/\\2\\]'             // Closing shortcode tag
            .     ')?'
            . ')'
            . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
    }

    function wpautop( $pee, $br = true ) {
        $pre_tags = array();
        if ( trim($pee) === '' )
            return '';
        // Just to make things a little easier, pad the end.
        $pee = $pee . "\n";
        /*
         * Pre tags shouldn't be touched by autop.
         * Replace pre tags with placeholders and bring them back after autop.
         */
        if ( strpos($pee, '<pre') !== false ) {
            $pee_parts = explode( '</pre>', $pee );
            $last_pee = array_pop($pee_parts);
            $pee = '';
            $i = 0;
            foreach ( $pee_parts as $pee_part ) {
                $start = strpos($pee_part, '<pre');
                // Malformed html?
                if ( $start === false ) {
                    $pee .= $pee_part;
                    continue;
                }
                $name = "<pre wp-pre-tag-$i></pre>";
                $pre_tags[$name] = substr( $pee_part, $start ) . '</pre>';
                $pee .= substr( $pee_part, 0, $start ) . $name;
                $i++;
            }
            $pee .= $last_pee;
        }
        // Change multiple <br>s into two line breaks, which will turn into paragraphs.
        $pee = preg_replace('|<br\s*/?>\s*<br\s*/?>|', "\n\n", $pee);
        $allblocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|form|map|area|blockquote|address|math|style|p|h[1-6]|hr|fieldset|legend|section|article|aside|hgroup|header|footer|nav|figure|figcaption|details|menu|summary)';
        // Add a single line break above block-level opening tags.
        $pee = preg_replace('!(<' . $allblocks . '[\s/>])!', "\n$1", $pee);
        // Add a double line break below block-level closing tags.
        $pee = preg_replace('!(</' . $allblocks . '>)!', "$1\n\n", $pee);
        // Standardize newline characters to "\n".
        $pee = str_replace(array("\r\n", "\r"), "\n", $pee);
        // Find newlines in all elements and add placeholders.
        $pee = $this->wp_replace_in_html_tags( $pee, array( "\n" => " <!-- wpnl --> " ) );
        // Collapse line breaks before and after <option> elements so they don't get autop'd.
        if ( strpos( $pee, '<option' ) !== false ) {
            $pee = preg_replace( '|\s*<option|', '<option', $pee );
            $pee = preg_replace( '|</option>\s*|', '</option>', $pee );
        }
        /*
         * Collapse line breaks inside <object> elements, before <param> and <embed> elements
         * so they don't get autop'd.
         */
        if ( strpos( $pee, '</object>' ) !== false ) {
            $pee = preg_replace( '|(<object[^>]*>)\s*|', '$1', $pee );
            $pee = preg_replace( '|\s*</object>|', '</object>', $pee );
            $pee = preg_replace( '%\s*(</?(?:param|embed)[^>]*>)\s*%', '$1', $pee );
        }
        /*
         * Collapse line breaks inside <audio> and <video> elements,
         * before and after <source> and <track> elements.
         */
        if ( strpos( $pee, '<source' ) !== false || strpos( $pee, '<track' ) !== false ) {
            $pee = preg_replace( '%([<\[](?:audio|video)[^>\]]*[>\]])\s*%', '$1', $pee );
            $pee = preg_replace( '%\s*([<\[]/(?:audio|video)[>\]])%', '$1', $pee );
            $pee = preg_replace( '%\s*(<(?:source|track)[^>]*>)\s*%', '$1', $pee );
        }
        // Remove more than two contiguous line breaks.
        $pee = preg_replace("/\n\n+/", "\n\n", $pee);
        // Split up the contents into an array of strings, separated by double line breaks.
        $pees = preg_split('/\n\s*\n/', $pee, -1, PREG_SPLIT_NO_EMPTY);
        // Reset $pee prior to rebuilding.
        $pee = '';
        // Rebuild the content as a string, wrapping every bit with a <p>.
        foreach ( $pees as $tinkle ) {
            $pee .= '<p>' . trim($tinkle, "\n") . "</p>\n";
        }
        // Under certain strange conditions it could create a P of entirely whitespace.
        $pee = preg_replace('|<p>\s*</p>|', '', $pee);
        // Add a closing <p> inside <div>, <address>, or <form> tag if missing.
        $pee = preg_replace('!<p>([^<]+)</(div|address|form)>!', "<p>$1</p></$2>", $pee);
        // If an opening or closing block element tag is wrapped in a <p>, unwrap it.
        $pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee);
        // In some cases <li> may get wrapped in <p>, fix them.
        $pee = preg_replace("|<p>(<li.+?)</p>|", "$1", $pee);
        // If a <blockquote> is wrapped with a <p>, move it inside the <blockquote>.
        $pee = preg_replace('|<p><blockquote([^>]*)>|i', "<blockquote$1><p>", $pee);
        $pee = str_replace('</blockquote></p>', '</p></blockquote>', $pee);
        // If an opening or closing block element tag is preceded by an opening <p> tag, remove it.
        $pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)!', "$1", $pee);
        // If an opening or closing block element tag is followed by a closing <p> tag, remove it.
        $pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee);
        // Optionally insert line breaks.
        if ( $br ) {
            // Replace newlines that shouldn't be touched with a placeholder.
            $pee = preg_replace_callback('/<(script|style).*?<\/\\1>/s', array($this, '_autop_newline_preservation_helper'), $pee);
            // Normalize <br>
            $pee = str_replace( array( '<br>', '<br/>' ), '<br />', $pee );
            // Replace any new line characters that aren't preceded by a <br /> with a <br />.
            $pee = preg_replace('|(?<!<br />)\s*\n|', "<br />\n", $pee);
            // Replace newline placeholders with newlines.
            $pee = str_replace('<WPPreserveNewline />', "\n", $pee);
        }
        // If a <br /> tag is after an opening or closing block tag, remove it.
        $pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*<br />!', "$1", $pee);
        // If a <br /> tag is before a subset of opening or closing block tags, remove it.
        $pee = preg_replace('!<br />(\s*</?(?:p|li|div|dl|dd|dt|th|pre|td|ul|ol)[^>]*>)!', '$1', $pee);
        $pee = preg_replace( "|\n</p>$|", '</p>', $pee );
        // Replace placeholder <pre> tags with their original content.
        if ( !empty($pre_tags) )
            $pee = str_replace(array_keys($pre_tags), array_values($pre_tags), $pee);
        // Restore newlines in all elements.
        if ( false !== strpos( $pee, '<!-- wpnl -->' ) ) {
            $pee = str_replace( array( ' <!-- wpnl --> ', '<!-- wpnl -->' ), "\n", $pee );
        }
        return $pee;
    }
        function wp_replace_in_html_tags( $haystack, $replace_pairs ) {
        // Find all elements.
        $textarr = $this->wp_html_split( $haystack );
        $changed = false;
        // Optimize when searching for one item.
        if ( 1 === count( $replace_pairs ) ) {
            // Extract $needle and $replace.
            foreach ( $replace_pairs as $needle => $replace );
            // Loop through delimiters (elements) only.
            for ( $i = 1, $c = count( $textarr ); $i < $c; $i += 2 ) {
                if ( false !== strpos( $textarr[$i], $needle ) ) {
                    $textarr[$i] = str_replace( $needle, $replace, $textarr[$i] );
                    $changed = true;
                }
            }
        } else {
            // Extract all $needles.
            $needles = array_keys( $replace_pairs );
            // Loop through delimiters (elements) only.
            for ( $i = 1, $c = count( $textarr ); $i < $c; $i += 2 ) {
                foreach ( $needles as $needle ) {
                    if ( false !== strpos( $textarr[$i], $needle ) ) {
                        $textarr[$i] = strtr( $textarr[$i], $replace_pairs );
                        $changed = true;
                        // After one strtr() break out of the foreach loop and look at next element.
                        break;
                    }
                }
            }
        }
        if ( $changed ) {
            $haystack = implode( $textarr );
        }
        return $haystack;
    }
    
    function _autop_newline_preservation_helper( $matches ) {
        return str_replace( "\n", "<WPPreserveNewline />", $matches[0] );
    }
}
