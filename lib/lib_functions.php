<?php 
require_once __DIR__.'/lib_mysql.php';

function memoize($function) {
    return function() use ($function) {
        static $results = array();

        $args = func_get_args();
        $key = serialize($args);
        if(empty($results[$key])) {
            $results[$key] = call_user_func_array($function, $args);
        }
        return $results[$key];
    };
}

function partial() {
    $args = func_get_args();
    $func = array_shift($args);
    return function() use($func, $args) {
        $full_args = array_merge($args, func_get_args());
        return call_user_func_array($func, array_filter($full_args)); 
    };
}


function date_time($format = "F j, Y, g:i a")
{
    $today = date($format); 
    return $today;
}

function download($file)
{
  //header("Content-Type: " . mime_content_type($FileName));
  // if you are not allowed to use mime_content_type, then hardcode MIME type
  // use application/octet-stream for any binary file
  // use application/x-executable-file for executables
  // use application/x-zip-compressed for zip files
  header("Content-Type: application/octet-stream");
  header("Content-Length: " . filesize($file));
  header("Content-Disposition: attachment; filename=".$file." ");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  $fp = fopen($file,"rb");
  fpassthru($fp);
  fclose($fp);
}


function file_download($filename)
{
// required for IE, otherwise Content-disposition is ignored
        if(ini_get('zlib.output_compression'))
          ini_set('zlib.output_compression', 'Off');
        
        // addition by Jorg Weske
        $file_extension = strtolower(substr(strrchr($filename,"."),1));
        
        if($filename == "" || !file_exists($filename)) 
          exit;
        
        switch( $file_extension )
        {
          /*
          case "pdf": $ctype="application/pdf"; break;
          case "exe": $ctype="application/octet-stream"; break;
          case "zip": $ctype="application/zip"; break;
          case "doc": $ctype="application/msword"; break;
          case "xls": $ctype="application/vnd.ms-excel"; break;
          case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
          case "gif": $ctype="image/gif"; break;
          case "png": $ctype="image/png"; break;
          case "jpeg":
          case "jpg": $ctype="image/jpg"; break;
          */
          default: $ctype="application/force-download";
        }
    
        header("Pragma: public"); // required
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false); // required for certain browsers 
        header("Content-Type: $ctype");
        // change, added quotes to allow spaces in filenames, by Rajkumar Singh
        header("Content-Disposition: attachment; filename=\"".basename($filename)."\";" );
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".filesize($filename));
        readfile($filename);
        //chmod($filename, 0);
        exit();
} 

//COMENTED FUNCTIONS ARE DECLARED SOMEWHERE ELSE :(


 /*
// convert Cyrilic to Latin symbols
function CyrLat ($s) {
       $cyr = array ("�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�" ,"�" ,"�", "�", "�", "�" ,"�" ,"�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�" ,"� ","�"  ,"�"   ,"�");
       $lat = array  ("a","b","v","g","d","e","g","z", "i", "i", "k",  "l", "m","n","o","p","r","s",   "t","u","f","h","c", "ch","sh","sht", "u", "", "iu","ia","A","B","V","G","D","E","G","Z","I",  "I",  "K",  "L","M","N","O","P","R","S","T","U","F","H","C","Ch","Sh","Sht","Ju",  "J");
       for ($i=0; $i<count($cyr); $i++) {
        $s = str_replace($cyr[$i], $lat[$i], $s);
       }
       return $s;
     }


 function TextFormF($vars) {
   $i=1;
   while (list($var, $value) = each ($vars)){
     $TextForm = "$TextForm    $var:    '$value' \n  ";
     }
   return $TextForm;
 }

function  unhtmlspecialchars( $string )
   {
       $string = str_replace ( '&amp;', '&', $string );
       $string = str_replace ( '&#039;', '\'', $string );
       $string = str_replace ( '&quot;', '\"', $string );
       $string = str_replace ( '&lt;', '<', $string );
       $string = str_replace ( '&gt;', '>', $string );
       
       return $string;
   }
*/

// DBASE functions
/*
 function  row_exist($table_name, $field_name, $value) {   // proveriava za sushtestvuvaneto na POLE $field_name sus STOINOST $value v TABLICA $table_name
  $res = false;
  $result = $db->db_query("SELECT * FROM $table_name WHERE $field_name = '$value'  ");
  if  ( $db->db_count(($result) > 0 )  {
   $res = true;
   }   
   return $res; 
 }

 function field_exist($field_name, $table_name) {
 //  - ����� ������ ��� ������ field_name ���������� � ������� table_name
 $res = false;
 $result = $db->db_query("SELECT * FROM $table_name");
 $i = 0;
  while ($i < mysql_num_fields($result)) {
   $meta = mysql_fetch_field($result, $i);
   if ($meta->name == $field_name) {
    $res = true;
   }
   $i++;
    }
  return $res;
 }   // END function field_exist
 
// $table name, $vars must be 2 dimension array i.e. $_POST 
function query_set($table_name, $vars) {  
   while ( list($var, $value) = each ($vars) ) {
    if ( !(empty($value)) && (field_exist($var, $table_name)) ) {
        if ($i > 0) {$query_set = "$query_set, ";} // slaga zapetaika sled vsiako pole s izkluchenie na purvoto
        $i++; //
        // echo("$var -> $value <br> \n\r");
        $query_set = "$query_set $var='$value'";
        }
   }
   return $query_set;
}

function get_page($n) {
$query_set = "SELECT * FROM pages left join images on pages.imageNo = images.imageID WHERE n='$n'  "; 
 $query = $db->db_query("$query_set"); 
 echo("<!-- get_page($n) : $query_set //--> "); 
 $row = $db->fetch("object", $query); 
     if ($row->imageWidth >= 500)  {$row->image_allign = 6; } // GOLEMI SNIMKI se SLAGAT DOLU za da NE ZAKRIVAT TEXTA
 return $row; 
}
*/
 
 /*
function resize_image($src_file, $dst_file, $max_width, $max_height) {  // RESIZE JPG images - 
// AKO FILE NE E JPG funkciata samo kopira sudurjanieto 

// $image_type = image_type_to_mime_type ($src_file); 

// if ($image_type == "image/jpeg") {    // IMAGE is  "image/jpeg"

if ($image = imagecreatefromjpeg("$src_file") ) {

if (!$max_width)
  $max_width = 400;
if (!$max_height)
  $max_height = 300;

$size = GetImageSize($src_file);
$width = $size[0];
$height = $size[1];

$x_ratio = $max_width / $width;
$y_ratio = $max_height / $height;

if ( ($width <= $max_width) && ($height <= $max_height) ) {
  $tn_width = $width;
  $tn_height = $height;
}
else if (($x_ratio * $height) < $max_height) {
  $tn_height = ceil($x_ratio * $height);
  $tn_width = $max_width;
}
else {
  $tn_width = ceil($y_ratio * $width);
  $tn_height = $max_height;
}

//$src = ImageCreateFromJpeg($image);
$src = $image; 
$dst = ImageCreate($tn_width,$tn_height);
ImageCopyResized($dst, $src, 0, 0, 0, 0,
    $tn_width,$tn_height,$width,$height);
// header('Content-type: image/jpeg');
// ImageJpeg($dst, null, -1);     flow TO BROWSER DIRECTLY
// ImageJpeg($dst, $filename, -1); 
ImageDestroy($src);

//Grab new image
ob_start();
ImageJPEG($dst);
$dst_image = ob_get_contents();
ob_end_clean();
ImageDestroy($dst);
//write $dst_image to dst_file
$fp = fopen("$dst_file", "w"); 
 fwrite($fp, $dst_image);
fclose($fp); 

 }  // IMAGE is  "image/jpeg")
else {  // JUST COPY     image_src -> image_dst      AKO FILE NE E JPG funkciata samo kopira sudurjanieto 
  $fp = fopen("$src_file", "r"); 
  $dst_image = fread($fp, filesize("$src_file")); 
  fclose($fp); 
}

$fp = fopen("$dst_file", "w"); 
 fwrite($fp, $dst_image);
fclose($fp); 

} // END function resize_image
*/

//check if the directory is empty
function is_dir_empty($dir)
    {
    $count = 0;
    if ($handle = opendir($dir)) 
        { 
            while (false !== ($file = readdir($handle))) 
                if ($file != "." && $file != "..") 
                    $count++; 
                    
        } 
    
    closedir($handle); 
    if($count==0) return true;
    else return false;
    } 

//read directory content and return array of contents
function read_dir($dir, $filter="jpg|jpeg|gif|tif|bmp|png|avi|doc|pdf|xls")
    {
        $files = array();
        if(is_dir($dir))
            {
                $handle = opendir($dir);
                while($file = readdir($handle))
                    if(eregi($filter, $file))
                        array_push($files, str_replace(' ','%20',$file));
                sort($files);
            }
        return $files;      
    }

//split the text by paragraphs and return the first paragraph longer than 5 symbols
//example: <p>par1</p><p>paragraph5</p> will return <p>paragraph5</p>  
//$return_paragraph - which paragraph to return
function crop_text($text_str='', $return_paragraph = 0)
{
    //try to format text with commas (add &nbsp after each comma)
    $text_str = str_replace(",", ", ", $text_str);
    $text_str = str_replace(";", "; ", $text_str);
    
    //$split_text = explode("</p>", $text_str);
    $split_text = preg_split("#</p>#i", $text_str);
    $i=0;
    foreach($split_text as $paragraph) {
        if(strlen($paragraph) > 5 and $return_paragraph == $i) {
            $j = $i;
            break;
        }
        $i ++;
    }
    if(!isset($j)) {
        $j = 0;
    }
    
    return $split_text[$j]."</p>";      
}

// returns first 100 symbols
function cut_text($text='', $symbols=100, $close_str = "...", $strip_tags = NULL)
{
    if($strip_tags)
        $text = strip_tags($text, $strip_tags);
    else
        $text = strip_tags($text);
        
    $text = str_replace(",", ", ", $text);
    $text = str_replace(";", "; ", $text);
    $text = preg_replace("/\s{2}/", " ", $text);
    $text = rtrim($text);
    if(strlen($text) <= $symbols) return $text;
    if(!isset($symbols) || $symbols==0) return $text;
    else {
        $text = substr($text, 0, $symbols); 
         
        $text = explode(' ', $text);
        array_pop($text);
        $text = implode(' ', $text);

        $text = $text . $close_str; 
        return $text;
    }
}


//upload files 
function file_upload($file_name, $file_tmp_name, $dir, $file_type, $file_size, $valid_type=true, $max_upload_size = 262144)
{

//invalid filetypes
//$invalid_types = array('application/octet-stream', 'video/x-ms-asf');
$invalid_types = array('');

//if $valid_type=true upload only theese $valid_types
$valid_types = array('image/pjpeg', 'image/jpeg', 'image/x-png','image/x-icon', 'image/png', 'image/gif', 'image/tiff','text/plain', 'application/x-shockwave-flash', 'image/vnd.microsoft.icon');

//extra valid types, when $valid_type=false
$extra_valid_types = array('audio/x-ms-wma', 'audio/mpeg', 'video/mpeg', 'video/webm', 'video/mp4', 'application/pdf', 'application/x-pdf', 'application/x-zip-compressed', 'application/x-gzip', 'application/vnd.sun.xml.calc', 'application/msword', 'application/octet-stream', 'video/x-ms-asf', 'application/vnd.ms-powerpoint', 'application/rar', 'application/vnd.ms-excel', 'video/x-flv', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');

if(!$valid_type)
    $valid_types = array_merge($valid_types, $extra_valid_types);

    if(in_array($file_type, $invalid_types) || !in_array($file_type, $valid_types))
        return 301;

    if($file_size > $max_upload_size)
        return 302;
    else
    if(!empty($file_name))
        if (is_uploaded_file($file_tmp_name)) 
            if(move_uploaded_file($file_tmp_name, $dir."/".$file_name))
                return 101;
            else return 102;
        else return 103;
    else return 201;
    
}


// banner_count_down impressions 
function banner_count_down($id, $SiteID) {
 //$query=$db->db_query("SELECT * FROM banners_access WHERE banner_id='banner_id' AND site_id='$SiteID' "); 
 //$row=mysqli_fetch_row($query); 
 //$row->banner_impressions=$row->banner_impressions-1; 
 //$db->db_query("UPDATE  banners_access SET banner_impressions='$row->banner_impressions' WHERE  banner_id='$id' "); 
 }
//display banner
function display_banner($SiteID, $banner_group)
{
 $list_banners_query = "SELECT * FROM banners_access, banners WHERE banners_access.banner_id = banners.banner_id AND banners_access.banner_impressions>0 AND banners_access.site_id = '$SiteID'  AND banners.banner_group = '$banner_group' ORDER BY RAND()";
 //echo("<-- SELECT * FROM banners_access, banners WHERE banners_access.banner_id = banners.banner_id AND banners_access.banner_impressions>0 AND banners_access.site_id = '$SiteID'  AND banners.banner_group = '$banner_group' ORDER BY RAND() --> "); 
 $db = mysql_db::getInstance();
 $list_banners_result = $db->db_query($list_banners_query); 
 $banners_list = $db->fetch('object', $list_banners_result);
 return $banners_list;
}


//output help icon with link
function help_link($url, $target="_blank", $popupWin=false, $alt="������� ���������� �� ���� �����")
{
if(empty($url) || $url=="" || !isset($url)) $url = "page.php?n=213&SiteID=1";
$help_url =" <a href=\"http://maksoft.net/".$url."&templ=2\" target=".$target."><img src=\"http://www.maksoft.net/web/images/icon_info.gif\" border=\"0\" vspace=\"0\" hspace=\"0\" align=\"middle\" alt=\"".$alt."\"></a>";
return $help_url;
} 



//return page name
function get_page_title($n, $SiteID)
{
     $db = mysql_db::getInstance();
    $page_name_sql = "SELECT Name FROM pages WHERE n = '$n' AND SiteID = '$SiteID' ";
    $page_name_res = $db->query($page_name_sql);
    $page = $db->fetch('object', $page_name_res);
    
    return $page->Name;
}


// Output a message (normal, warning, error)
function mk_output_message($type='normal', $message = '', $parameters = 'align=center')
    {
        
        $content = "<div class=\"message_".$type."\" ".$parameters." id=\"message_".$type."\">";
        $content.= $message;
        $content.= "</div>";
        
        echo $content;  
    }  

// Output an ADMI message (nornal, error) - use jQuery theme
function mk_output_admin_message($type='normal', $message = '', $parameters = 'align=center')
  {
    switch($type)
        {
            case "error": { $type="error"; $label = "��������:"; break;}
            case "normal":
            default: {$type = "highlight"; $label = "���������:";}
            
            }
    $content = "<div class=\"ui-widget\" style=\"margin: 20px;\">";
    $content.= "<div class=\"ui-state-".$type." ui-corner-all\" style=\"padding: 0 .7em;\">";
    $content.= "<p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>"; 
    $content.= "<strong>".$label."</strong> ".$message."</p>";
    $content.= "</div></div>";
    echo $content;  
  }  


// generate RANDOM password
  function randomkeys($length)
  {
   if(defined(RANDOM_KEYS_PATTERN)) {
       $pattern = RANDOM_KEYS_PATTERN;
   }  else {
       $pattern = "123456789abcdefghijklmnpqrstuvwxyzABCDEFGHKLMNOPQRSTUVWXYZ";
   }
   for($i=0;$i<$length;$i++)
   {
     $key .= $pattern{rand(1,58)};
   }
   return $key;
  }
 
  
function access_code_create($Rub, $days) {
   $today = date("YmdHi")."00";
   $expire = time() + ($days*24*60*60); 
   $expire = date("YmdHi", $expire)."00";    
   $code = randomkeys(4); 
     $db = mysql_db::getInstance();
  $db->db_query("INSERT INTO access_codes  SET code='$code',  Rub='$Rub', expire = '$expire'  "); 
  return $code; 
}

function code_str_create($code) {
     $db = mysql_db::getInstance();

   $db->db_query("DELETE FROM tmpcodes  WHERE  gen_date <=DATE_SUB(NOW(),INTERVAL 4 MINUTE) ");
   $codestr = randomkeys(5); 
  $db->db_query("INSERT INTO tmpcodes SET code='$code',  codestr='$codestr'  "); 
  return $codestr; 
}


function check_code_str($code, $codestr, $withRecaptcha=false)
{
  if($withRecaptcha===true){
      return true;
  }
  $db = mysql_db::getInstance();
  $res = false; 
  if (($code>0) && ($codestr<>"")) {
   $rowc_query = $db->db_query("SELECT * FROM  tmpcodes  WHERE code='$code' ");
   $rowc = $db->fetch("object", $rowc_query); 

   if (($code>0)  && ($rowc->codestr == "$codestr")) {
    $res = true; 
   } 

   $db->db_query("DELETE FROM  tmpcodes WHERE code='$code' ");  
   $db->db_query("DELETE FROM tmpcodes  WHERE  gen_date <=DATE_SUB(NOW(),INTERVAL 5 MINUTE) " ); 
  }
  return $res; 
 }

function return_code_str($code)
{
  if ($code>0)
  {
   $db = mysql_db::getInstance();
   $rowc_query = $db->db_query("SELECT * FROM  tmpcodes  WHERE code='$code' ");
   $rowc = $db->fetch('object', $rowc_query); 
  }
  return $rowc->codestr; 
 }

//return page level
function get_page_level($n)
{
    $p_level = 0; //home page
    $c_page = get_page($n);
    $parent_page = $c_page->ParentPage;
     
    while($parent_page > 0)
        {
            $c_page = get_page($parent_page);
            $parent_page = $c_page->ParentPage;
            $p_level++;
        }
    return $p_level;
}

//return array of page groups
function get_page_groups($n)
    {
        global $SiteID;
        $groups = array();
        $db = mysql_db::getInstance();
        $sql = "SELECT  * FROM pages_to_groups pg left join groups g on pg.group_id = g.group_id WHERE pg.n = '".$n."' AND g.SiteID = '".$SiteID."' ORDER by g.group_title ASC";
        $group_query = $db->db_query($sql);
        if($db->count($group_query) > 0)
            while($group = $db->fetch('array', $group_query))
                $groups[] = $group;
        return $groups;
        //echo $sql;
    }

//return site groups
function get_site_groups($SiteID)
    {
       $db = mysql_db::getInstance();
        $groups = array();
        $sql = "SELECT  * FROM groups g WHERE g.SiteID = '".$SiteID."' ORDER by g.group_title ASC";
        $group_query = $db->db_query($sql);
        if($db->db_count($group_query) > 0)
            while($group = $db->fetch("array", $group_query))
                $groups[] = $group;
        return $groups;
    }

//return drop-down menu of gropus
function list_page_groups($name="group_id", $selected = 0, $params="")
    {
        global $SiteID;
        
        $groups = get_site_groups($SiteID);
        $content = "<select name=\"".$name."\" $params><option value=0> - select group - </option>";
        for($i=0; $i<count($groups); $i++)
            {
                if($selected == $groups[$i]['group_id'])
                    $selected_str = "selected";
                else $selected_str = "";
                
                $content.= "<option value=\"".$groups[$i]['group_id']."\" $selected_str>".$groups[$i]['group_title']."</option>";
            }
        $content.="</select>";
        
        echo $content;
    }

//return group info
function get_group_info($group_id, $return = NULL)
    {
       $db = mysql_db::getInstance();
        $group = $db->fetch("array", $db->db_query("SELECT  * FROM groups g WHERE g.group_id = '".$group_id."'"));
        if($return)
            return $group[$return];
        else
            return $group; 
    }
    
//format a number into a string + prefix
function format_number($number, $digits = 2, $prefix="")
    {
        $content = "";
        $digits--;
        $leading_zeros = "";
        if($number>=10)
            $digits--;
        
        if($number>=100)
            $digits--;
                
        for($j=0; $j<$digits; $j++)
            $leading_zeros.= "0";
        $prefix_number = $prefix . $leading_zeros;
        
        $content.= $prefix_number . $number;
        return $content;
    }
    
    
function site_ok($url, $row, $SiteID) 
    {
        $exist=0; 
            // get host name from URL
        preg_match('@^(?:http://)?([^/]+)@i', "$url", $matches);
        $host = $matches[1];
        // get last two segments of host name
        preg_match('/[^.]+\.[^.]+$/', $host, $matches);
        $url=array_shift($matches);
    
       $db = mysql_db::getInstance();
        $exist_query = "SELECT * FROM Sites WHERE SitesID='$SiteID' ";
        $exist = $db->db_count($db->db_query("SELECT SitesID FROM Sites WHERE (SitesID='$SiteID') AND ((url LIKE '%$url%' ) OR (primary_url LIKE '%$url%'))  "));
        
        //if ($exist==0) {
            //$exist = $db->db_count(($db->db_query("SELECT * FROM versions WHERE verSiteID='$SiteID' AND verSiteID='$row->SiteID'  "));
        //}
        return $exist; 
    }

function url_available($url, $timeout = 100) { 
        $ch = curl_init(); // get cURL handle 
 
        // set cURL options 
        $opts = array(CURLOPT_RETURNTRANSFER => true, // do not output to browser 
                                  CURLOPT_URL => $url,            // set URL 
                                  CURLOPT_NOBODY => true,                 // do a HEAD request only 
                                  CURLOPT_TIMEOUT => $timeout);   // set timeout 
        curl_setopt_array($ch, $opts);  
 
        curl_exec($ch); // do it! 
 
        $retval = curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200; // check if HTTP OK 
 
        curl_close($ch); // close handle 
 
        return $retval; 
}

function add_date($givendate,$day=0,$mth=0,$yr=0) {
      $cd = strtotime($givendate);
      $newdate = date('Y-m-d h:i:s', mktime(date('h',$cd),
    date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
    date('d',$cd)+$day, date('Y',$cd)+$yr));
      return $newdate;
}
              
//generate list of all site pages             
function list_site_pages($SiteID, $start_page=NULL, $n, $indent="&nbsp;", $params="name=\"site_pages\"", $limit_level = 5)
    {
        global $user, $o_page, $o_site;
        if(!$start_page) $start_page = $o_site->StartPage;
        
        //get current page by n
        $c_page = $o_page->get_page($n);
        
        //$result = $db->db_query("SELECT * FROM pages WHERE ParentPage = '".$start_page."' ORDER BY sort_n ASC");
        $pages = $o_page->get_pSubpages($start_page);

        $limit_level--;
        
        if($limit_level > 0)
        {
        if($c_page['n'] == $o_site->StartPage && $SiteID == $o_site->SiteID)
            $content.= "<option value=\"0\" selected>- no parent page - </option>";     
        else
        for($i=0; $i<count($pages); $i++)
        {
            if($user->WriteLevel >= $pages[$i]['SecLevel'])
            {
            if ($pages[$i]['n'] == $c_page['ParentPage']) {$str_selected = "selected";} else {$str_selected = "";}   
            
            $lenght = strlen($pages[$i]['Name']);
            if($lenght > 40 )
                $pages[$i]['Name'] = substr($pages[$i]['Name'], 0, 40)." ..."; 
        
            if ($pages[$i]['n'] != $c_page['n'])
                $content.= "<option value=\"".$pages[$i]['n']."\" $str_selected>$indent|&nbsp;&nbsp;".$pages[$i]['Name']." </option>";
            
            if ($pages[$i]['ParentPage'] != 0 && $pages[$i]['n'] != $c_page['n'])
                $content.= list_site_pages($SiteID, $pages[$i]['n'], $n, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$indent, $params, $limit_level);
                
            }
        } 
    
        if($start_page == $o_site->StartPage)
            if($c_page['n'] != $o_site->StartPage)
                return "<select id=\"site_pages\" $params><option value=\"".$o_site->StartPage."\">".$o_site->get_sHome()."</option>".$content."</select>";
            else
                return "<select id=\"site_pages\" $params>".$content."</select>";
        else return $content;
        
        }
        else $limit_level++;        
    }   

//construct link depending on the local variables and the given parameters
//for ADMIN use only
//type = default - add parameters into the link
//type = strict - use only selected parameters

function mk_href_link($parameters = '', $type = "default")
  {
    $link = "";
    $page = $_SERVER['SCRIPT_NAME'];

    //get all parameters from URL
    $params_old = explode("&", str_replace("?", "", strstr($_SERVER["REQUEST_URI"], "?")));
    
    //insert all old variables in array
    $params_old_array = array();
    for($i=0; $i<count($params_old); $i++)
        {
            if(!empty($params_old[$i]))
            {
                $parts = explode("=", $params_old[$i]);
                $key = $parts[0];
                $value = $parts[1];
                $params_old_array[$key] = $value;
            }
        }
    
    //check new variables
    $params_new = explode("&", $parameters);

    //insert all new variables in array
    $params_new_array = array();
    for($i=0; $i<count($params_new); $i++)
        {
            $parts = explode("=", $params_new[$i]);
            $key = $parts[0];
            $value = $parts[1];
            $params_new_array[$key] = $value;
        }
    

    //add old and new params
    $keys = array_keys($params_new_array);
    for($i=0; $i<count($keys); $i++)
        {
            //if(array_key_exists($keys[$i], $params_old_array))
                $params_old_array[$keys[$i]] = $params_new_array[$keys[$i]];
            //else array_push($params_old_array, $params_new_array[$keys[$i]]);
        }

    //construct final link 
    if(isset($parameters))
        $link .= $page . '?';
    else
        $link .= $page;
    
    $keys_old = array_keys($params_old_array);
    for($i=0; $i<count($keys_old); $i++)
        if($i>0) $link .= "&".$keys_old[$i]."=".$params_old_array[$keys_old[$i]];
        else $link .= $keys_old[$i]."=".$params_old_array[$keys_old[$i]];
    
    //return constructed address only with the given parameters
    if($type == "strict")
        {
            if($parameters != "")
                $link = $page . '?' . $parameters; 
            else $link = $page;
        }
    return $link;
  }

/*
//generate drop_down menu of all site images
function site_list_images($SiteID, $n)
    {
        global $o_site, $o_page;
        //get current page
        $c_page = $o_page->get_page($n);
        
        $content = "<select name=\"imageNo\" id=\"imageNo\" onChange=\"set_image()\">";
        $content.= "<option value=\"".$c_page['imageNo'] ."\">- �������� ������ -</option>";

        if ($c_page['imageNo'] > 0)
            $content.= "<option value=\"\"> - �������� �������� ! - </option>"); 
            
            //get all site image directories
            $s_image_dirs = $o_site->get_sImageDirectories($o_site->SiteID);
            
            for($i=0; $i<count($s_image_dirs); $i++)
                {
                    echo "<optgroup label=\"".$s_image_dirs[$i]['Name']."\"></optgroup>";
                    
                    //read all image in selected directory / directory ID, SQL filter, $filter_usage = false
                    $images_res = $o_page->db_query("SELECT * FROM (SELECT * FROM images WHERE image_dir=".$s_image_dirs[$i]['ID']." ORDER BY imageID DESC LIMIT 999) as tbl ORDER BY imageName, image_src ASC");
        
                    while($images = $db->fetch("array", $images_res))
                        $dir_images[] = $images;
                    
                    for($j=0; $j<count($dir_images); $j++)
                        {
                            $selected = "";
                            if($dir_images[$j]['imageID'] == $row_no->imageNo)
                                $selected = "selected";
                            $img_title = $dir_images[$j]['imageName'];
                            if (strlen($img_title)<=2)
                                $img_title = $dir_images[$j]['image_src'];
                    
                            $img_title = eregi_replace("^.+/", "", $img_title); 
                            if(strlen($img_title) > 48)
                                {
                                    $part1 = substr($img_title, 0, 24);
                                    $part2 = substr($img_title, -24);
                                    
                                    $img_title =  $part1 . " [...] " . $part2;
                                }
                            $content.= "<option value=\"".$dir_images[$j]['imageID']."\" $selected rel=\"".$dir_images[$j]['image_src']."\">&nbsp;&nbsp;&nbsp;".format_number(($j+1), 3).". ".$img_title."</option>";
                        }
                    
                }
        $conten.= "</select>";
      
    }       
*/ 
//output dropdown menu with all currencies
function list_price_currencies($name="currency", $selected = 0, $params="")
    {
        $content = "<select name=\"".$name."\" $params><option value=\"0\"> - select currency - </option>";
           $db = mysql_db::getInstance();
        $c_query = $db->db_query("SELECT * FROM currencies");
        while($currency = $db->fetch("array", $c_query))
            {
                if($selected == $currency['currency_id'])
                    $selected_str = "selected";
                else $selected_str = "";
                
                $content.= "<option value=\"".$currency['currency_id']."\" $selected_str>".$currency['currency_key'].", ".$currency['currency_string']."</option>";

            }
        $content.= "</select>";
        echo $content;
    } 
    
//$O_PAGE FUNCTIONS 
//get menu
function get_menu($menu_array, $params_array=NULL)
    {
        global $o_page, $o_site, $o_user, $user;

        if(!isset($params_array['menu_depth']))
                $params_array['menu_depth'] = 0;
        
        if(!isset($params_array['view_mirror_pages']))
                $params_array['view_mirror_pages'] = false;
                
    $params = isset($params_array['params']) ? $params_array['params'] : '';
        $content = "<ul ".$params.">";
        
        //show home button whithout dropdown
        if(isset($params_array['home']))
            {
                $content.= "<li><a href=\"".$o_page->get_pLink($o_site->StartPage)."\" title=\"".htmlspecialchars($o_page->_page['Name'], ENT_QUOTES)."\">".$o_page->get_pName($o_site->StartPage)."</a></li>";
                $params_array['home'] = false;
            }
        
        for($i=0; $i<count($menu_array); $i++)
            {
                $content.= "<li><a href=\"".$menu_array[$i]['page_link']."\" title=\"".htmlspecialchars($menu_array[$i]['Name'])."\" class=\"".(($o_page->n == $menu_array[$i]['n'] || in_array($menu_array[$i]['n'], $o_page->get_pParentPages($o_page->n))) ? "selected":"")."\">".$menu_array[$i]['Name']."</a>"; 
                $subpages = $o_page->get_pSubpages($menu_array[$i]['n']);
/*              $content.= "<li><a href=\"".$menu_array[$i]['page_link']."\" title=\"".htmlspecialchars($menu_array[$i]['Name']." ".$menu_array[$i]['title'])."\" class=\"".(($o_page->n == $menu_array[$i]['n'] || in_array($menu_array[$i]['n'], $o_page->get_pParentPages($o_page->n))) ? "selected":"")."\">".$menu_array[$i]['Name']."</a>"; 
                $subpages = $o_page->get_pSubpages($menu_array[$i]['n']);*/
                
                //limit subpages
                if(isset($params_array['submenu_limit']) and $params_array['submenu_limit'] >0)
                    if($params_array['view_mirror_pages'] && $menu_array[$i]['an'] > 0)
                        $subpages = $o_page->get_pSubpages($menu_array[$i]['an'], $o_page->get_pSubPagesOrder($menu_array[$i]['an'])." LIMIT ".$params_array['submenu_limit']);
                    else
                        $subpages = $o_page->get_pSubpages($menu_array[$i]['n'], $o_page->get_pSubPagesOrder($menu_array[$i]['n'])." LIMIT ".$params_array['submenu_limit']);
                //get group content
                $group_id = $menu_array[$i]['show_group_id'];
                if(isset($group_id) && !empty($group_id))
                    {
                        $s_pages = $o_page->get_pSubpages($menu_array[$i]['n']);
                        $g_pages = $o_page->get_pGroupContent($group_id, date("Y-m-d"));
                        if(count($subpages) == 0)
                            $subpages = $g_pages;
                        else
                            $subpages = array_merge($s_pages, $g_pages);

                    }

                if($params_array['menu_depth'] >0 && count($subpages) > 0)
                    {
                        $params_array['menu_depth']--;
                        $params_array['params'] = "";
                        $content.= get_menu($subpages, $params_array);
                        $params_array['menu_depth']++;
                    }   
                $content.="</li>";
            }
        $content.= "</ul>";
        return $content;
    }
    
//output menu
function print_menu($menu_array, $params_array=NULL)
    {
        global $o_page, $o_site, $o_user;
        if(count($menu_array) >0)
            echo get_menu($menu_array, $params_array);
    }


function is_logged($level=0)
{
    if(!isset($_SESSION["user"]) or !is_object($_SESSION["user"]) or !isset($_SESSION['user']->ID) or !isset($_SESSION['user']->ParentUserID)){
        return False;
    }

    if($_SESSION['user']->AccessLevel > $level){
        return True;
    }

    return True;
}


/**
 * Check the syntax of some PHP code.
 * @param string $code PHP code to check.
 * @return boolean|array If false, then check was successful, otherwise an array(message,line) of errors is returned.
 */
function php_syntax_error($code){
    if(!defined("CR"))
        define("CR","\r");
    if(!defined("LF"))
        define("LF","\n") ;
    if(!defined("CRLF"))
        define("CRLF","\r\n") ;
    $braces=0;
    $inString=0;
    foreach (token_get_all('<?php ' . $code) as $token) {
        if (is_array($token)) {
            switch ($token[0]) {
                case T_CURLY_OPEN:
                case T_DOLLAR_OPEN_CURLY_BRACES:
                case T_START_HEREDOC: ++$inString; break;
                case T_END_HEREDOC:   --$inString; break;
            }
        } else if ($inString & 1) {
            switch ($token) {
                case '`': case '\'':
                case '"': --$inString; break;
            }
        } else {
            switch ($token) {
                case '`': case '\'':
                case '"': ++$inString; break;
                case '{': ++$braces; break;
                case '}':
                    if ($inString) {
                        --$inString;
                    } else {
                        --$braces;
                        if ($braces < 0) break 2;
                    }
                    break;
            }
        }
    }
    $inString = @ini_set('log_errors', false);
    $token = @ini_set('display_errors', true);
    ob_start();
    $code = substr($code, strlen('<?php '));
    $braces || $code = "if(0){{$code}\n}";
    if (eval($code) === false) {
        if ($braces) {
            $braces = PHP_INT_MAX;
        } else {
            false !== strpos($code,CR) && $code = strtr(str_replace(CRLF,LF,$code),CR,LF);
            $braces = substr_count($code,LF);
        }
        $code = ob_get_clean();
        $code = strip_tags($code);
        if (preg_match("'syntax error, (.+) in .+ on line (\d+)$'s", $code, $code)) {
            $code[2] = (int) $code[2];
            $code = $code[2] <= $braces
                ? array($code[1], $code[2])
                : array('unexpected $end' . substr($code[1], 14), $braces);
        } else $code = array('syntax error', 0);
    } else {
        ob_end_clean();
        $code = false;
    }
    @ini_set('display_errors', $token);
    @ini_set('log_errors', $inString);
    return $code;
}

function json_encode_n($in) { 
  $_escape = function ($str) { 
    return addcslashes($str, "\v\t\n\r\f\"\\/"); 
  }; 
  $out = ""; 
  if (is_object($in)) { 
    $class_vars = get_object_vars(($in)); 
    $arr = array(); 
    foreach ((array) $class_vars as $key => $val) { 
      $arr[$key] = "\"{$_escape($key)}\":\"{$val}\""; 
    } 
    $val = implode(',', $arr); 
    $out .= "{{$val}}"; 
  }elseif (is_array($in)) { 
    $obj = false; 
    $arr = array(); 
    foreach($in AS $key => $val) { 
      if(!is_numeric($key)) { 
        $obj = true; 
      } 
      $arr[$key] = json_encode_n($val); 
    } 
    if($obj) { 
      foreach($arr AS $key => $val) { 
        $arr[$key] = "\"{$_escape($key)}\":{$val}"; 
      } 
      $val = implode(',', $arr); 
      $out .= "{{$val}}"; 
    }else { 
      $val = implode(',', $arr); 
      $out .= "[{$val}]"; 
    } 
  }elseif (is_bool($in)) { 
    $out .= $in ? 'true' : 'false'; 
  }elseif (is_null($in)) { 
    $out .= 'null'; 
  }elseif (is_string($in)) { 
    $out .= "\"{$_escape($in)}\""; 
  }else { 
    $out .= $in; 
  } 
  return "{$out}"; 
} 

function array_to_txt($array, $pad=0) {
    if(!is_array($array)) {
        return $array;
    }
    $res = '';
    $repeater = str_repeat('&nbsp', $pad);
    $tmpl = "<br>$repeater<b>[%s]</b>: %s<br>";
    foreach ($array as $key=>$value) {
        if(is_array($value)) {
            $res.= sprintf($tmpl, $key, array_to_txt($value, $pad+4));
            continue;
        }
        $res.= sprintf($tmpl, $key, $value);
    }
    
    return $res;
}


?>
