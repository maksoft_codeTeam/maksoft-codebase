<?php
// app_config >> event_handler >> lib_mysql >> lib_user >> lib_seo >> lib_site >> lib_cart >>  lib_page >> lib_cms
//.
 require_once('lib_cart.php');
 require_once('kses.php');

require_once __DIR__.'/../modules/shortcodes/shortcodes.php';
require_once __DIR__.'/../modules/shortcodes/shortcodes-support.php';
require_once __DIR__.'/../modules/shortcodes/wpautop.php';
require_once __DIR__.'/UrlFactory.php';
require_once __DIR__.'/../modules/vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;


class page extends cart {
  public  $post_vars; 
  public  $get_vars; 
  public  $str; 
  public  $row; 
  public  $n;
  public  $orig_n;
  public  $np;
  public  $pr; 
  public  $Rub; 
  public  $_page; 
  public  $_ppage; 
  public  $shortcodes; 
  public  $sc;
  public $lang_id = 1;
  public $scheme='http://';
  protected $shortcode_tags=array();
  protected $default_return_type = 'array';
  public static $request;
    
    // page( [int site identification number; int page number; ] )
    // constructor 
    function __construct($n=null, $SiteID=null)
    {   
        parent::__construct($SiteID); 

        if(!self::$request) { 
            self::$request = Request::createFromGlobals();
        }

        $this->request = self::$request;
        $this->urlFactory = new UrlFactory($this->request);
        $this->lang_id = $this->getLangId();
        
        $this->scheme = $this->get_protocol();

        $this->n = $n ? $n : $this->getPageId();

        $this->_page = $this->get_page($this->n); 
        
        $this->orig_n = $this->n;

        if($this->get_pMirror() > 0) {
            $this->n = $this->get_pMirror();
        }
            
        $this->np =(integer)  $this->request->query->get('np', 0);

        $this->show_group_id = $this->get_pInfo("show_group_id");
        
        define("EDIT_LINK", "/page.php?n=".$this->PAGE_EDIT_N."&amp;no=".$this->orig_n."&amp;SiteID=".site::$sID);
        define("ADD_LINK", "/page.php?n=".$this->PAGE_ADD_N."&amp;ParentPage=".$this->orig_n."&amp;SiteID=".site::$sID);

        if($this->SiteID == $this->get_pSiteID()) {
            define("PAGE_EDIT_LINK","<a href=\"".EDIT_LINK."\" class=\"navbar-edit-page\">".CMS_LINK_EDIT_PAGE."</a>");
        } else {
            define("PAGE_EDIT_LINK","X");
        }
            
        if($this->show_group_id == 0) {
            if($this->SiteID == $this->get_pSiteID()) {
                define("PAGE_ADD_LINK","<a href=\"".ADD_LINK."\" class=\"navbar-add-page\">".CMS_LINK_ADD_PAGE."</a>");
            } else {
                define("PAGE_ADD_LINK","X");
            }
        }

        define("PAGE_ADD_LINK","<a href=\"/page.php?n=".$this->PAGE_ADD_N."&amp;ParentPage=".$this->orig_n."&amp;SiteID=".site::$sID."&amp;group_id=".$this->show_group_id."\" class=\"navbar-add-page\">".CMS_LINK_ADD_PAGE."</a>");
        define("USER_LOGOUT_LINK","<a href=\"/web/admin/logout.php\" class=\"navbar-logout-page\">".CMS_LINK_LOGOUT."</a>");            
        define("PAGE_ADMIN_LINK", "<a href=\"/page.php?n=".$this->PAGE_ADMIN_N."&amp;SiteID=".site::$sID."\" class=\"navbar-admin-page\">".CMS_LINK_ADMINISTRATION."</a>");         
         
        if($this->_page['ParentPage']>0) {
            $this->_ppage = $this->get_page($this->_page['ParentPage']); 
        }

        $this->pr = 0;

        if($this->uri == "/index.php") {
            $this->pr=2; 
        }

        if($this->_page['ParentPage'] == $this->_site['StartPage']) {
            $this->pr=1; 
        }

        if(($this->_page['preview'] > 15000) && (strlen($_GET['search_tag']<3)) ) {
            $this->pr++; 
        }
        
        $current_n = $this->n; 
            
        if(($this->_site['inherit_vars']>0) && ($this->n <> $this->_site['StartPage']) ) {
            $i=4; 
            $rowp['ParentPage'] = $this->_page['ParentPage']; 

            while( ($current_n <> $this->_site['StartPage']) &&  ($this->_page['PHPvars']=="") && ($i>0) )  {
                $rowp_query = $this->db_query("SELECT * FROM pages WHERE n = '".$rowp['ParentPage']."' "); 
                $rowp = $this->fetch("array", $rowp_query); 
                $this->_page['PHPvars'] = $rowp['PHPvars']; 
                $current_n = $rowp['n'];
                $i--; 
            }

            eval($this->_page['PHPvars']); 
        } 
    }  

    public function getLangId()
    {
        if(!$this->request->query->get('lang')) {
            return $this->_site['language_id'];
        }
        $lang_code = $this->request->query->get('lang'); 
        $lang = $this->get_sLanguageByCode($lang_code);
        return $lang ? $lang['language_id']  : $this->_site['language_id'];
    }

    public function count_subpages($n)
    {
        $sql = "SELECT COUNT(n) as qty from pages where ParentPage=$n ;";
        return $this->fetch("object", $this->db_query($sql))->qty;
    }

    public function get_protocol()
    {
        $ssl_cert = $this->get_ssl();
        return $this->ssl_active($ssl_cert) ? 'https://' : 'http://';
    }

    public function getPageSlugByN($n) {
        $sql = "SELECT * FROM page_slug WHERE n=$n LIMIT 1";
        $pres = $this->db_query($sql);
        $res =  $this->fetch("object", $pres);
        return $res ? $res->slug : false;
    }

    public function get_PageUrl($name)
    {
        $SiteID = $this->_page['SiteID'];
        $pres = $this->db_query("SELECT filename from PageURLs where SiteID=$SiteID LIMIT 1");
        if($prow = $this->fetch("object", $pres)) {
            return $prow->filename;
        }

        return '';

    }
    
    public function getPageId() {
        if(isset($_GET['n'])) { 
            return $_GET['n'];
        }
        $purl = iconv("UTF-8", "CP1251", $_GET['purl']); 
        $ppurl =  iconv("UTF-8", "CP1251", $_GET['ppurl']); 
        if(strlen($purl)>1) {
            if(is_numeric($purl)) {
                $n= intval($purl); 
            } else {
                $page = $this->getPageIdBySlug($purl);
                $n = $page->n;
            }
        }

        if ((empty($n)) || ($n == 0) or !$n) {
            $n = $this->getPageIdByDomain($_SERVER['SERVER_NAME']);
        }

        return is_numeric($n) ? $n : $this->_site['StartPage']; 
    }


    protected function getPageIdByDomain($domain) 
    {
        $urls = explode(".", $domain, 7); 
        return is_numeric($urls[1]) ? $urls[1] : false;
    }

    protected function getPageIdBySlug($slug)
    {
        $result = $this->db_query("SELECT * FROM page_slug WHERE SiteID='$this->SiteID' AND slug='$slug' LIMIT 1"); 
        return $this->fetch("object", $result);
    }
    //return page array
    public function get_page($n = NULL, $type='array', $lang='')
    {
        $n = $this->validatePageId($n);
        
        return $this->pageIsCached($n) ? $this->getCachedPage($n, $type) : $this->insertPageToCache($n, $type, $lang); 
    }

    protected function insertPageToCache($n, $type, $lang) 
    {
        self::$pages[$n] = $this->getPageFromDatabase($n, $lang);
        return $this->getCachedPage($n, $type);
    }

    protected function getPageFromDatabase($n, $lang)
    {
        if(in_array($this->_site['SitesID'], array(1007, 1036, 1038, 1042))){
            $sql = $this->pages_base_query('', " AND p.n = '$n' LIMIT 1", $lang ? $lang : $this->lang_id);
        } else {
            $sql = "SELECT * FROM pages p left join images im on p.imageNo = im.imageID WHERE p.n= '$n' LIMIT 1";
        }

        $query = $this->db_query($sql);

        return  $this->fetch($this->getDefaultReturnType(), $query);
    }

    protected function pageIsCached($n)
    {
        return array_key_exists($n, self::$pages);
    }

    protected function getCachedPage($n, $type) 
    {
        $types = $this->getAllowedReturnTypes();
        $type = $this->isTypeAllowed($type) ? $type : $this->getDefaultReturnType();
        return $types[$type](self::$pages[$n]);
    }

    protected function getDefaultReturnType() 
    {
        return $this->default_return_type;
    }

    protected function getAllowedReturnTypes()
    {
        return array(
            'array' => function($page) {
                return is_array($page) ? $page : (array) $page; 
            },
            'object' => function($page) {
                return is_object($page) ? $page : (object) $page; 
            }
        );
    }

    protected function isTypeAllowed($type) {
        return array_key_exists($type, $this->getAllowedReturnTypes());
    }
    
    //return page Info by selected argument
    function get_pInfo($return, $n=NULL)
    {
        $n = $this->validatePageId($n);
        $pInfo = $this->get_page($n);
        //set default values for some cells
        switch($return)
            {
                case "imageName" : { if($pInfo['imageName'] == "") $pInfo['imageName'] = $this->get_sName() . ", " .$this->get_pName(); break;}
                
            }
            
        return $pInfo[$return]; 
    }
    
    //return page Name
    function get_pName($n=NULL)
    {
        $pName = $this->get_pInfo("Name", $n);
        return stripslashes($pName);
    }
    
    function print_pName($link=false, $n=NULL, $params = NULL)
        {
            $n = $this->validatePageId($n);
            //clean extra variables passed in the URL
            $this->q_string = "";
            if($link)
                echo "<a href=\"".$this->get_pLink($n)."\" title=\"".$this->get_pInfo("title", $n)."\" $params>".stripslashes($this->get_pName($n))."</a>";
            else
                echo $this->get_pName($n);
        }
    
    //return page Title
    //full - return site name + page title/name, strict - returns only page title/name
    function get_pTitle($output = "full", $n=NULL) 
    {
        $n = $this->validatePageId($n);
            
            if($n<>$this->n) {
                $row = $this->get_page($n); 
            } else {
                 $row = $this->_page; 
            }
            
            $title_array = explode(" ", str_replace(",", " ", $this->get_sTitle()));
            //$indent = " &raquo; ";
            $indent = " ";
            //$indent = " ";
            $sTitle = $this->get_sTitle() ;
            $pTitle = $sTitle;
            $ppTitle = '';

            
                    if(($this->_page['ParentPage']>0) && ($output == "full") )  {
                        $ppTitle = $this->get_pTitle("strict", $this->_page['ParentPage']);
                    }
                    $pTitle = $row['Name'];
                    
                    if(strlen($ppTitle) > 20) $ppTitle = "";                

                    if(strlen($row['title']) >1) {
                        $pTitle = $row['title'];
                        } else {
                        $pTitle = $row['Name'] . $indent . $ppTitle . $indent. $sTitle ;
                        if(strlen($pTitle)>85)  $pTitle = $row['Name'] . $indent . $sTitle ;
                        if(strlen($pTitle)>85)  $pTitle = $row['Name'];

                        //$pTitle = $title_array[0] . $title_array[1] . $ppTitle . $indent . $pTitle;
                        $i = count($title_array) - 1; 
                        while(($i>=0) && (strlen($pTitle)<50) ) {
                            $pTitle  = $title_array[$i] . $indent . $pTitle; 
                            $i--; 
                        }
                    }   
                    
                                
            //if(strlen($pTitle) < 8) {
            //   $pTitle = $row['Name'] . $indent . $title_array[0] . $indent . $title_array[1] . $indent . $title_array[2];
            //   if($n==$this->_site['StartPage']) $pTitle = $sTitle;
            // }
             
            
            if($output == "strict")
                {
                    $pTitle = $this->get_pInfo("title", $n);
                    if($pTitle == "") $pTitle = $this->get_pName($n);
                }
            
            if(isset($_GET['search_tag']) and strlen($_GET['search_tag'])>=3) {
                $pTitle = iconv("UTF-8","CP1251",$_GET['search_tag']);
            }
            
            $comment_id = isset($_GET['comment_id']) ? $_GET['comment_id'] : 0; 
            if($comment_id>0) {
                $q_comment = $this->db_query("SELECT * FROM comments WHERE comment_id = '$comment_id'  AND comment_parent=0 LIMIT 1 "); 
                $row_comment = $this->fetch("object", $q_comment); 
                $this->_page['comment_subject'] = $row_comment->comment_subject;            
                $pTitle = $this->_page['comment_subject']." | ".$pTitle; 
            }
                
            return stripslashes($pTitle);     
        }
        
        
    //full - return page Title + Site Title for browser output
    //strict - return only page Title/Name  
    function print_pTitle($output = "full", $link=false, $n=NULL)
    {
        $pTitle = $this->get_pTitle($output, $n);           
        if($link) $pTitle = "<a href=\"".$this->get_pLink($n)."\" title=\"".$this->get_pTitle($output, $n)."\">".$pTitle."</a>";
        print $pTitle; 
    }

    //return page Parent, int
    function get_pParent($n=NULL)
    {
        $n = $this->validatePageId($n);
        $pParent = $this->get_pInfo("ParentPage", $n);

        if($pParent == 0) $pParent = $this->n;
        return $pParent;
    }
        
    //return an array of ALL parent pages from current page
    function get_pParentPages($n=NULL)
        {
            $n = $this->validatePageId($n);
            $pParent = $this->get_pInfo("ParentPage", $n);
            if($pParent == 0) $pParent = $this->n;
            $pParentPages = array();
            
            //start cycle to collect ALL upper pages parent page
            while($pParent != 0)
                {
                    $pParentPages[] = $pParent;
                    $pParent = $this->get_pInfo("ParentPage", $pParent);
                }

            return $pParentPages;
        }
    
    //return page Mirror, int
    function get_pMirror()
    {
        $pMirror = $this->get_pInfo("an");
        return $pMirror;
    }
    
    //return page PHP code, string
    function get_pPHPcode($n=NULL)
    {
        $n = $this->validatePageId($n);
        $pPHPcode = $this->get_pInfo("PHPcode", $n);
        return $pPHPcode;
    }
    
    function print_pPHPcode()
    {
        $pPHPcode = $this->get_pPHPcode();
        return eval($pPHPcode);
    }   
    
    //return page PHP variables, string
    function get_pPHPvars()
    {
        $pPHPvars = $this->get_pInfo("PHPvars");
        return $pPHPvars;
    }
    
    function print_pPHPvars()
    {
        eval($this->get_pPHPvars());
    }
    //return page Text, string
    /*
    function get_pText()
        {
            $pText = $this->get_pInfo("textStr");
            return $pText;
        }
    */
    function search_pText()
        {
            global $search;
            $search = urldecode($search);
            //echo $search;
            echo str_replace($search, "<a href=\"".$this->scheme."bg.wikipedia.org/wiki/".$search."\" target=\"_blank\">".$search."</a>", $this->get_pText());
        }
    
    
    function get_pSiteID()
    {
        return $this->get_pInfo("SiteID");
    }
        
    function get_pText($n = NULL)
        {
            
            global $search;
            $search = urldecode($search);
            
            $split_tag = "<!-- pagebreak -->";
            $text = explode($split_tag, $this->get_pInfo("textStr", $n));
            #$text = $this->replace_shortcodes($text);
            //          $shortcodes[] = array($shortcode => $shortcode_code);
            $np = $this->np;
            if(!isset($np)) $np = 0;
            if($np >= count($text)) $np = count($text) - 1;
            if($np < 0) $np = 0;
            
            $pagination = "";
            for($i=0; $i<count($text); $i++)
                {
                    if($np == $i) $class = "page_selected";
                    else $class = "page";
                    $pagination.= "<a href=\"page.php?n=".$this->n."&amp;SiteID=".$this->SiteID."&amp;np=".$i."\" class=\"".$class."\">".($i+1)."</a>";
                }
            $pagination = "<div id=\"pagination\">".$pagination."</div>";
            if(count($text) <= 1)
                $pagination = "";
            //print($this->get_pInfo("textStr"));       
            if(isset($search))
                return stripslashes(str_replace($search, "<a href=\"".$this->scheme."bg.wikipedia.org/wiki/".$search."\" target=\"_blank\">".$search."</a>", $text[$np]) . $pagination);
            else
                return stripslashes($text[$np] . $pagination);
        }

    function get_pParagraph($n=NULL, $paragraph = 0, $split_tag = "<!-- pagebreak -->")
        {
            //$split_tag = "<!-- pagebreak -->";
            $text = explode($split_tag, $this->get_pInfo("textStr", $n));

            return $text[$paragraph];
            return $this->replace_shortcodes($text[$paragraph]);
        }
        
    function print_pText($n=NULL)
        {
            $n = $this->validatePageId($n);
            print $this->get_pText($n);
        }
    
    //return page Link position, int
    function get_pLinkStatus()
    {
        $pLinkStatus = $this->get_pInfo("toplink");
        return $pLinkStatus;
    }
    
    //return page Subcontent structure
    function get_pShowLink($n=NULL)
    {
        $n = $this->validatePageId($n);
        $pShowLink = $this->get_pInfo("show_link", $n);
        return $pShowLink;
    }
    
    //return page Subpages order
    function get_pSubPagesOrder($n=NULL)
    {
        $n = $this->validatePageId($n);
        $pSubPagesOrder = $this->get_pInfo("show_link_order", $n);
        if($pSubPagesOrder == "ASC" || $pSubPagesOrder == "DESC")
        $pSubPagesOrder = "p.sort_n ".$pSubPagesOrder;
        return $pSubPagesOrder;
    }
    
    //return page Subcontent colls
    function get_pSubPagesColls()
    {
        $pSubPagesColls = $this->get_pInfo("show_link_cols");
        return $pSubPagesColls;
    }
    
    // get the include file set ot PageURL field in table pages
    function get_pURL()
    {
        $pURL = $this->get_pInfo("PageURL");
        if(site::$uReadLevel >= $this->get_pInfo("SecLevel"))
            return $pURL;
        else return NULL;
    }
    
    //THIS FUNCTION DO NOT WORK CORRECT. LOOSE SOME GLOBAL VARIABLES.
    function print_pURL()
    {
        global $n, $no, $SiteID, $o_page, $o_site, $o_user;

        //break when user has no access
        //if(site::$uReadLevel > $this->get_pInfo("SecLevel")) 
            //return NULL;
        //else
            if(file_exists($this->get_pURL()))
                include_once $this->get_pURL();
        
    }

    protected function validatePageId($n) {
        return is_numeric($n) ? $n : $this->n;
    }
    
    function get_pLink($n=null, $SiteID=null)
    {
        $n = $this->validatePageId($n);
        $page = $this->get_page($n, 'object');
        $page->SiteID = $SiteID ? $SiteID : $this->_site['SitesID'];

        if($this->_site['url_type'] == 3 ) {
            $ppage = $this->get_page($page->ParentPage, 'object');
            $page->pslug = $ppage->slug;
        }
        $relative_link = (string) $this->urlFactory->makeUrl($page, $this->_site);
        return $this->getAbsoluteLink($relative_link, $page->SiteID);
    }
    
    protected function getAbsoluteLink($relative_link, $SiteID) {
        $protocol = $this->scheme;
        $domain = $this->get_sURL($SiteID);

        if($domain !== $this->get_sURL()) {
            $protocol = 'http://';
        }

        return sprintf("%s%s%s", $protocol, $domain, $relative_link);
    }
        
    function print_pLink($n=NULL, $params = "")
    {
        $n = $this->validatePageId($n);
        //clean extra variables passed in the URL
        $this->q_string = "";
        $pLink = "<a href=\"".$this->get_pLink($n)."\" title=\"".$this->get_pInfo("title", $n)."\" $params>".$this->get_pName($n)."</a>";
        print $pLink;
    }   
    
    //returns page date: date_added or date_modified
    function get_pDate($return="date_added", $n = NULL)
    {
        $pDate = $this->get_pInfo($return, $n);
        return $pDate;
    }
    
    function print_pDate($return="date_added", $format="d.m.Y")
    {
        $pDate = $this->get_pDate($return);
        print(date($format, strtotime($pDate)));
    }
    
    function get_pImage($n = NULL)
    {
        $pImage = $this->get_pInfo("image_src", $n);
        $pImage = str_replace("//", "/", $pImage);  // bug fix in image_dir declared with / at the end
        if(empty($pImage))
            return DEFAULT_IMAGE;

        return $pImage;
    }
    
    //output page image 
    function print_pImage($width=NULL, $params="", $default_src=NULL, $n = NULL, $ratio=NULL)
    {
        if(!$ratio) $ratio_str = "";
        else $ratio_str =  "&amp;ratio=".$ratio;
        
        if(!$width)
            $width = $this->get_pInfo("imageWidth", $n);
        //$align = $this->get_pInfo("image_allign", $n);
        
        if($this->get_pImage($n))
            $img = "<img src=\""."/img_preview.php?image_file=".$this->get_pImage($n)."&amp;img_width=".$width.$ratio_str."\" alt=\"".$this->get_pInfo("imageName",$n) ."\" $params>";
        else if(isset($default_src) && !empty($default_src))
            $img = "<img src=\""."/img_preview.php?image_file=".$default_src."&amp;img_width=".$width.$ratio_str."\" alt=\"".$this->get_pInfo("imageName",$n)."\" $params>";
        else
            $img = "<div style=\"display: block; width:".$width.$MAIN_WIDTH_DIMMENSION."; height:".$width.$MAIN_WIDTH_DIMMENSION."; background-color: #FFFFFF;  background-image: url(/web/admin/images/no_image.jpg); background-repeat: no-repeat; background-position: 50% 50%;\"></div>";
        print $img; 
    }
    
    //return page visits
    function get_pVisits($n)
    {
        $visits = $this->get_pInfo("preview", $n);
        return $visits;
    }
    
    function print_pVisits()
    {
        print $this->get_pVisits();
    }
    
    //return page status(0: locked page; 1: visible page; -1: page in trash, not visible)
    function get_pStatus($n=NULL)
        {
            $n = $this->validatePageId($n);
            $pStatus = $this->get_pInfo("status", $n);
            return $pStatus;
        }
    
    //return page subpages array
    //$sort_order="p.sort_n ASC"
    function get_pSubpages($parent_page=null, $sort_order= NULL, $filter=1, $filter_by_tag = false, $limit=false)
    {
        global $tag;
        

        
        if($this->n <> $parent_page) {
                $pSubPagesOrder = $this->get_pSubPagesOrder($parent_page, $limit);
        } else {
                $pSubPagesOrder = $this->_page['show_link_order'];
                if($pSubPagesOrder == "ASC" || $pSubPagesOrder == "DESC")
                    $pSubPagesOrder = "p.sort_n ".$pSubPagesOrder;
        }
        
        
        $add_condition = "";
        //works when no filter is defined, $filter = 1 (by default)
        if(!empty($tag) && isset($tag) && $filter == 1 && $filter_by_tag)
            $add_condition.= " AND p.tags LIKE '%$tag%' ";
        
        if($filter == 1)
            $add_condition.= "AND p.status >= 0 ";
                                            
        if(!isset($parent_page)) $parent_page = $this->n;   
        if(!$sort_order) $sort_order = $pSubPagesOrder;
        
        //$subpages[] = array(); 

        if(is_array($limit)){
            $limit = " LIMIT ". $limit[0].','.$limit[1];
        } else {
            $limit = "";
        }

        //$apc_cache_name = sprintf("%s%s%s%s%s%s", $parent_page, $sort_order, $filter, $filter_by_tag, $limit, $this->_site['SitesID']);
        //if (apc_exists($apc_cache_name)) {
            //return apc_fetch($apc_cache_name);
        //}

        if(in_array($this->_site['SitesID'], array(1007, 1036, 1038, 1042))){
            if($parent_page == 0)
                $res = $this->db_query($this->pages_base_query('', "AND p.SiteID = '".$this->SiteID."' $add_condition AND $filter ORDER by ".$sort_order." ".$limit));
            else    
                $res = $this->db_query($this->pages_base_query('', "AND p.ParentPage = '$parent_page' $add_condition AND $filter ORDER by ".$sort_order." ".$limit)); 
        } else {
            if($parent_page == 0)
                $res = $this->db_query("SELECT * FROM pages p left join images im on p.imageNo = im.imageID  WHERE p.SiteID = '".$this->SiteID."' $add_condition AND $filter ORDER by ".$sort_order." ".$limit);
            else    
                $res = $this->db_query("SELECT * FROM pages p left join images im on p.imageNo = im.imageID  WHERE p.ParentPage = '$parent_page' $add_condition AND $filter ORDER by ".$sort_order." ".$limit); 
        }

        if($this->count($res) == 0)
            return NULL;
        
        $i = 0;
        while ($row = $this->fetch("array", $res)) {
                //break when user has no access
                if(site::$uReadLevel >= (int)$row["SecLevel"])
                    {
                        if($row['title'] == "") $row['title'] = $row['Name'];
                        //$row['page_link'] = "page.php?n=".$row['n']."&amp;SiteID=".$row['SiteID'];
                        
                        if(user::$uLevel > 0)
                            $row['SiteID'] = $this->get_sId();
                            
                        //$row['page_link'] = "http://".$this->get_sURL()."/page.php?n=".$row['n']."&amp;SiteID=".$row['SiteID'];
                        // if((strlen($row['slug'])>2) ) {
                        //   $row['page_link'] = "http://".$row['slug'].".".$row['n'].".".$this->get_sURL(); 
                        // }

                        $row['page_link'] = $this->get_pLink($row['n']); 
                        $row['page_comments'] = count($this->get_pComments($row['n'])); 
                        $subpages[$i++] = $row; 
                    }
            }

        return $subpages; 
    }   
    //return array of all page groups
    function get_pGroups($n=NULL, $sort_order = "g.group_id ASC")
        {
            $n = $this->validatePageId($n);

            //$apc_cache_name = sprintf("%s%s%s", $n, $sort_order, $this->_site['SitesID']);
            //if (apc_exists($apc_cache_name)) {
                //return apc_fetch($apc_cache_name);
            //}

            $res = $this->db_query("SELECT * FROM pages_to_groups ptg left join groups g on ptg.group_id = g.group_id WHERE ptg.n = ".$n." ORDER by ".$sort_order);

            if($this->count($res) == 0)
                return NULL;
            $groups = array();
            while ($group = $this->fetch("array", $res))
                {
                    $groups[] = $group;
                }
            //apc_store($apc_cache_name, $groups);
            return $groups;
        }
    
    //return selected group info
    function get_pGroupInfo($group_id, $return = NULL)
        {
            $res = $this->db_query("SELECT * FROM groups WHERE group_id = $group_id LIMIT 1");
            $gInfo = $this->fetch("array", $res);
            if($return)
                return $gInfo[$return];
            else return $gInfo;
        }
            
    //return all pages in selected group    
    function get_pGroupContent($group_id=NULL, $date_expire="0000-00-00", $sort_order= NULL, $filter=1)
        {

            if(!$group_id)
                $group_id = $this->get_pInfo("show_group_id");
            $subpages[] = array();
            
            /*
            if(!$sort_order)
                { 
                    if($this->get_pSubPagesOrder() == "ASC" || $this->get_pSubPagesOrder() == "DESC")
                        $sort_order = "sort_n ". $this->get_pSubPagesOrder();
                    else
                        $sort_order = $this->get_pSubPagesOrder();
                }
            */
            if(!$sort_order)
                $sort_order = "ptg.sort_order, p.Name ASC";
                        
            //$filter = "1";
            #$sql = $this->pages_base_query($join_clause, $and_clause);
            if($date_expire != "0000-00-00")
                $filter.= " AND ((ptg.end_date > '".$date_expire."') || ptg.end_date = '0000-00-00')";
            $sql = "SELECT * FROM pages p left join images im on p.imageNo = im.imageID, pages_to_groups ptg left join groups g on ptg.group_id = g.group_id WHERE p.n = ptg.n  AND g.group_id = '".$group_id."' AND g.SiteID = '".site::$sID."' AND p.SecLevel <= '".user::$uLevel."' AND ".$filter." ORDER by ".$sort_order."";
            if(in_array($this->_site['SitesID'], array(1007, 1036, 1038, 1042))){
                $join_clause = ', pages_to_groups ptg left join groups g on ptg.group_id = g.group_id ';
                $and_clause = " AND p.n = ptg.n  AND g.group_id = '".$group_id."' AND g.SiteID = '".site::$sID."' AND p.SecLevel <= '".user::$uLevel."' AND ".$filter." ORDER by ".$sort_order."";
                $sql = $this->pages_base_query($join_clause, $and_clause);
            }

            
            $res = $this->db_query($sql); 
            if($this->count($res) == 0)
                return NULL;
            $i = 0;
            while ($row = $this->fetch("array", $res))
                {
                    if($row['title'] == "") $row['title'] = $row['Name'];
                    //$row['page_link'] = "page.php?n=".$row['n']."&amp;SiteID=".$row['SiteID'];
                    $row['page_link'] = $this->get_pLink($row['n']);
                    $subpages[$i++] = $row; 
                }
            return $subpages; 
        }   
    
    function  get_pSubpagesCount($n=NULL, $limit=false)
    {
        $n = $this->validatePageId($n);
        $sCount = count($this->get_pSubpages($n,null,1,false,$limit));
        return (int)$sCount;
    }

    //get and print page Nvaigation
    function get_pNavigation($type = "standart", $n = NULL, $nav_bullet = "&nbsp;/&nbsp;")
        {
            if(!isset($n)) {
                $n=$this->n; 
                $p_parent = $this->_page['ParentPage'];
             } else  {
              $row = $this->get_page($n); 
              $p_parent = $row['ParentPage'];
             }
            $navigation = "";
            $navigation_start = "<a href=\"".$this->get_pLink($this->_site['StartPage'])."\" class='nav_links".(($this->n == $this->_site['StartPage']) ? " selected":"")."' title=\"".$this->get_pInfo("title", site::$sStartPage)."\">".site::$sHome."</a>";
            
            $i=0; // itterations count
            $query = "SELECT * FROM pages WHERE n = '%d' LIMIT 1"; 
            if(in_array($this->_site['SitesID'], array(1007, 1036, 1038, 1042))){
                $query = $this->pages_base_query('', " and n='%d'");
            }
            while (($p_parent > 1) && ($p_parent != site::$sStartPage) && ($i<1000))
                {
                    $result= $this->db_query(sprintf($query, $p_parent));
                    $row_parent =  $this->fetch("array", $result);
                    $name = $row_parent['Name'];
                    $title = $this->get_pInfo("title", $row_parent['n']);
                    //skip Administration page and page Internet
                    if($row_parent['n'] != 11 && $row_parent['n'] != 223)
                        {
                            if(site::$uReadLevel >= $row_parent['SecLevel'])
                                {
                                        $navigation = $nav_bullet."<a itemprop=\"url\" href=\"".$this->get_pLink($row_parent['n'])."\"  class=\"nav_links\" title=\"".$title."\"><span itemprop=\"name\">$name</span></a>".$navigation;
                                }
                            else
                                $navigation = $nav_bullet.$name.$navigation;
                        }
                    $p_parent = $row_parent['ParentPage'];
                    $i++; 
                };
            
        if(site::$sHome != $this->get_pName() && $this->_page['n'] != 11)
            $navigation.= $nav_bullet.$this->get_pName();
            
        if(user::$uID>0  and $_SESSION['user']->AccessLevel > 1 && $type == "standart") {
             //$navigation.= site::$uWriteLevel . site::$uReadLevel;
             $navigation.= $nav_bullet . PAGE_ADMIN_LINK . " [ ".PAGE_ADD_LINK . " | " . PAGE_EDIT_LINK." ] <b>".user::$uName ."</b>, ". USER_LOGOUT_LINK;
        }
            
        //$navigation =  strip_tags($navigation, "<a>"); 

            // schema
            //return '<nav itemscope="itemscope" itemtype="http://www.schema.org/SiteNavigationElement">' . $navigation_start . $navigation . '</nav>';
            return $navigation_start . $navigation;
        }

    public function get_site_version_href($url, $version)
    {
        $class = '';
        $logged = $this->_user['ID'] > 0;
        $lang_code = strtolower($version['language_key']);



        if($version['verSiteID'] == $this->SiteID) { 
            $class = "selected";
        }

        $link_build = function($code, $rest='') {
            return  sprintf("/%s/%s", $code, $rest);
        };

        switch(true) {
            case $logged:
                $url['lang'] = $lang_code;
                $slug =  sprintf('/page.php?%s', http_build_query($url));
                break;
            case $this->_page['n'] == $this->_site['StartPage']:
                $slug = $link_build($lang_code);
                break;
            default:
                $slug = $link_build($lang_code, $this->_page['n'].'.html');
        }

        return sprintf("<a href='%s' title='%s' class='%s'>%s</a>", $slug, $version['version_key'], $class, $version['version']);

    }

    function print_sVersions_dev($output = "text", $SiteID = NULL)
    {
        $versions = $this->get_sVersions($SiteID);

        $url = array(); 
        $url['n'] = $this->_page['n'];
        $url['SiteID'] = $this->SiteID;

        $lang_code = $this->get_sLanguageByCode($this->_site['language_id']);
        
        $lang_versions = array();

        foreach($versions as $version) {
            $lang_versions[$version['language_id']] = $this->get_site_version_href($url, $version);
        }

        if($this->_site['language_id'] != $this->_page['lang_id']){
            $s_lang = $this->get_sLanguage();
            $lang_versions[$this->_site['language_id']] = $this->get_site_version_href($url, array('language_key'=>$s_lang, 'version_key' => $s_lang, 'version'=>$s_lang));
        }

        unset($lang_versions[$this->_page['lang_id']]);
        echo implode('', $lang_versions);

           #for($i=0; $i<count($versions); $i++) {
           #    if($versions[$i]['verSiteID'] == $this->SiteID)
           #        $class = "selected";
           #    else
           #        $class = "";
           #    $content.= "<a href=\"/".strtolower($versions[$i]['language_key'])."/".$slug."\" title=\"".$versions[$i]['version_key']."\" class=\"$class\">".$versions[$i]['version']."</a>";
           #}
        echo $content;
    }
    
    function print_pNavigation($type="standart", $n=null, $nav_bullet = "&nbsp;/&nbsp;")
        {
            $pNavigation = $this->get_pNavigation($type, $n, $nav_bullet);
            print $pNavigation;
        }

    function get_image_size($img_src) 
    {
        if ($imgInfo = @GetImageSize($img_src)) {
            return $imgInfo;
        }
        return DEFAULT_IMAGE;
    }
    
    //read curent page info and construct the content (title, text, image)
    function print_pContent($n = NULL, $custom_prices=null)
    {
            ob_start();
            $n = $this->validatePageId($n);
            
            $align = $this->get_pInfo("image_allign", $n);
            $query = $this->db_query("SELECT * FROM alligns WHERE allignID = '".$align."' LIMIT 1");
            $img_align = $this->fetch("array", $query); $align_class = "";
            //image align top-left
            if($align == 2 || $align == 10)
                $align_class = "align-left";
            
            if($align == 3 || $align == 11)
                $align_class = "align-right";               
            
            $width = $this->get_pInfo("imageWidth", $n);

            $content = "";
            if(isset($_GET['str']) && strlen($_GET['str'])>16) {
                $content = '<br>'.$_GET['str'].'<hr>';
            }
            $img="";
            $imgInfo = $this->get_image_size($this->get_pImage($n));
            
            if($width > 0 && $align != 21 && $align != 22 && is_file($this->get_pImage($n)))
                {
                    
                    if($this->get_pInfo("make_links", $n) == 4)
                        $img = "<a href=\"".$this->get_pImage($n)."\" 
                                    class=\"fresco\" 
                                    data-fresco-caption=\"".$this->get_pInfo("imageName", $n)."\" 
                                    data-fresco-group=\"gallery\" 
                                    title=\"".$this->get_pInfo("imageName", $n)."\">
                                <img src=\"/img_preview.php?image_file=".$this->get_pImage($n)."&amp;img_width=".$width."&amp;ratio=strict\" alt=\"".$this->get_pInfo("imageName", $n)."\" align=\"".$img_align["allignPos"]."\" class=\"border_image $align_class\"></a>";
                    /*
                    else if($imgInfo[0] > $width)
                            $img = "<a href=\"".$this->get_pImage($n)."\" rel=\"lightbox\" title=\"".$this->get_pInfo("imageName", $n)."\"><img src=\"img_preview.php?image_file=".$this->get_pImage($n)."&amp;img_width=".$width."\" alt=\" + ".$this->get_pInfo("imageName", $n)."\" align=\"".$img_align["allignPos"]."\" class=\"main_image zoom\"></a>";   
                    */
                        else
                            $img = "<img src=\"/img_preview.php?image_file=".$this->get_pImage($n)."&amp;img_width=".$width."&amp;ratio=strict\" alt=\"".$this->get_pInfo("imageName", $n)."\" align=\"".$img_align["allignPos"]."\" class=\"main_image $align_class\">";
                }
            
            if($align == 9 || $align == 12)
                $img = "<center>".$img."</center>";
                
            if($this->get_pInfo("SiteID", $n) == 1 && $n != 132)
                $content.= $this->user_message();

            switch($align)
                {
                    case 1: 
                    case 2:
                    case 3:
                    case 9: { $content.=  $img . $this->get_pText($n);  break; }
                    case 10: 
                    case 11:
                    case 12: { $content.= $this->get_pText($n) . $img; break; }
                    default: $content.= $img . $this->get_pText($n);
                }
    
            if(site::$uReadLevel >= $this->get_pInfo("SecLevel", $n))
                {
                    print $content;
                    /*
                    if(!defined("SHOPPING_CART_ACTIVATE"))
                        define('SHOPPING_CART_ACTIVATE', false);
                        */
                    if($this->get_sConfigValue("SHOPPING_CART_ACTIVATE"))
                        $this->print_pPrice($this->n, "ALL", $this->get_sConfigValue("SHOPPING_CART_ACTIVATE"), $custom_prices);    
                }
            else include "web/admin/login_form.php";
            //else $this->print_login_form();
            //CMS ADMIN BUTTONS
            //move to each Template
            /*
            if($this->get_pInfo("show_link") == 0 || count($this->get_pSubpages()) == 0)
                $this->admin_buttons();
            */
            $content = ob_get_contents();

            ob_end_clean();
            echo stripslashes($content);
    }

    
    
    function get_top_word($str="", $min_strlen=5) {
    
        if(strlen($str)<5) {
            $str = $this->_page['Name']; 
            $str = $str.",".$this->_page['title']; 
            $str = $str.",".$this->_page['keywords']; 
            $str = $str.",".$this->_page['textStr']; 
        }
        $str = strip_tags($str); 
    /*
        $str = preg_replace('/ [0-9A-?]{1,2}\./', " ", $str); 
        $str = preg_replace('/\ [0-9A-?]{1,4}\ /', " ", $str); 
    */
        //$str = preg_replace('/\b[?-??-?0-9]{1,5}\b\s?/i', '', $str);
        $str = preg_replace("/\s{2,}/", " ", $str); 
        
        $str = str_replace(',', " ", $str); 
        $str = str_replace('-', " ", $str); 
        $str = str_replace('.', " ", $str); 
        //$str = str_replace("  ", " ", $str);      
        $str_arr = explode(' ', $str);
        
//      $str = preg_replace('/ [0-9A-?]{1,2}\./', " ", $str); 
//      $str = preg_replace('/\ [0-9A-?]{1,4}\ /', " ", $str); 
        
        $result = array_count_values($str_arr);
        asort($result);
        end($result);
        
        $i=0; 
        
        //echo $str; 
        //print_r($result); 
        
        while((strlen(key($result))<$min_strlen) && (count($result)>0) )  {
            array_pop($result); 
            end($result);
        }
        $top_word = key($result);
        $word_repeats = current($result); 
        
        while( (count($result)>0)  && (current($result)>=$word_repeats) )  {
            array_pop($result); 
            end($result);
            if((strlen(key($result))>=$min_strlen)) $top_word = $top_word.",".key($result);
        }
        

        
        return $top_word;
    }
	
    function print_pSubContentBS($parent_page=NULL, $filter=1, $filter_by_tag = true, $cms_args = array(), $limit=false)
    {
        //array of global parameters overwriting some of the variables
        global $cms_args;
        
        if($parent_page==NULL) $parent_page = $this->n;
        
        $colls = $this->get_pSubPagesColls();
        if($cms_args['CMS_COLS'])
            $colls =    $cms_args['CMS_COLS'];
        
        $order = $this->get_pSubPagesOrder($parent_page);
        //output group content
        $group_id = $this->get_pInfo("show_group_id", $parent_page);
        //get_pGroupContent($group_id=NULL, $date_expire="0000-00-00", $sort_order= NULL, $filter=1)
        
        /*
        if(isset($group_id) && !empty($group_id))
            $subpages = $this->get_pGroupContent($group_id, date("Y-m-d"), $order, $filter);
        else
            $subpages = $this->get_pSubpages($parent_page, $order, $filter, $filter_by_tag);
        */
		
            if($colls == 1);
            {
                $colls = 12;
            }
            if($colls == 2)
            {
                $colls = 6;
            }
        $subpages = $this->get_pSubpages($parent_page, $order, $filter, $filter_by_tag, $limit);
        //add group content to subpages
        
        if(isset($group_id) && !empty($group_id))
            {
                $g_pages = array();
                //$s_pages = $this->get_pSubpages($parent_page, $order, $filter, $filter_by_tag);
                $g_order = $order;
                if($order == "p.sort_n ASC" || $order == "ASC" || $order == "p.sort_n DESC" || $order == "DESC")
                    $g_order = NULL;
                $g_pages = $this->get_pGroupContent($group_id, date("Y-m-d"), $g_order, $filter);

                global $user;
                if(count($subpages) == 0)
                    $subpages = $g_pages;
                else
                    if(count($g_pages) > 0)
                    //$subpages = array_merge($s_pages, $g_pages);
                    if($this->n == $this->_site['StartPage']) {
                        $subpages = $g_pages;
                    } else {
                        $subpages = array_merge($subpages, $g_pages);
                    }                        
                    //$subpages = array_merge($subpages, $g_pages);

/*                if(count($g_pages) > 0) {
                    if($this->n == $this->_site['StartPage']) {
                     $subpages = $g_pages;
                    } else {
                     $subpages = array_merge($subpages, $g_pages);
                    }

             }*/


                //place code here for sorting the entire array of pages + group content
                //By now the group pages are added after the subpages
            }
        
        if(isset($cms_args['subpages']))
            $subpages = $cms_args['subpages'];
        
        $MAIN_WIDTH_DIMMENSION = "px";  
        
        if(isset($cms_args['MAIN_WIDTH']))
            $MAIN_WIDTH = $cms_args['MAIN_WIDTH'];
            
    
        if(site::$sMAIN_WIDTH)
            $MAIN_WIDTH = site::$sMAIN_WIDTH;
        
        /* mpetrov correction: 3.05.2014 */
        if(isset($cms_args['CMS_MAIN_WIDTH']))
            $MAIN_WIDTH = $cms_args['CMS_MAIN_WIDTH'];

        if(!isset($MAIN_WIDTH))
            $MAIN_WIDTH = 450;
                    
        //looking for percentages
        if(strpos($MAIN_WIDTH, "%") !== false )
            {
                $MAIN_WIDTH_DIMMENSION = "%";
                $MAIN_WIDTH = str_replace("%", "", $MAIN_WIDTH);
            }
        
        //100% == 950px, 50% == 950*50/100 = 475px
        if($MAIN_WIDTH_DIMMENSION == "%")
            {
                //$MAIN_WIDTH = 950*$MAIN_WIDTH/100;
                //$MAIN_WIDTH_DIMMENSION = "px";
            }
            
        //standart colls margin is 2% peprcents from the computed width
        $width_margin = 1*$MAIN_WIDTH/100;
        // -25 M.Kamenov correction 15.06.2014
        $width = round($MAIN_WIDTH/$colls - 6*$width_margin, 0, PHP_ROUND_HALF_DOWN);  //2*width
        
        if($colls == 1)
            {
                //$width = 100; 
                //$MAIN_WIDTH_DIMMENSION = "%";
                //$width_margin = 0;
            }
            
        $width_percents = (100*$width/$MAIN_WIDTH);
        //if($this->n == 11 || $this->get_pParent() == 11) $width = (int)(450/$colls - 4);
        $image_width = $width/2;
        
        $colls_counter = 1;
        $content = "<div class=\"sDelimeter\"></div>";

        //temporary subpage title
        $str_title = "";
        //temporary subpage image
        $str_img = "";
        //temporary subpage full text
        $str_text = "";
        //temporary subpage 1-st paragraph
        $str_paragraph = "";
        //more link text
        $str_more = "";
        //subpage item
        $str_subpage = "";
        //subpage links
        $str_subpage_links = "";
        //alternative text
        $str_alt = $page->Name;
        //link to the subpage
        $str_link = "";
        //get the previous page image width in px
        $str_previous_img_info = array("width"=>"180", "height"=>"180", "align"=>"");
        //page price content
        $str_price = "";
        
        //read and analyze subpages content
        //collect information about subpages images, subpages text
        //cycle works only in edit mode
        if(is_array($subpages))
            {                   
            
                for($i=0; $i<count($subpages); $i++)
                    {
                        if($subpages[$i]["image_allign"] == 2 || $subpages[$i]["image_allign"] == 10)
                            {
                                $str_img_align = "left";
                                $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                                $str_previous_img_info["align"] = $str_img_align;
                            }
                        
                        if($subpages[$i]["image_allign"] == 3 || $subpages[$i]["image_allign"] == 11)
                            {
                                $str_img_align = "right";
                                $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                                $str_previous_img_info["align"] = $str_img_align;
                            }
                            
                        if(file_exists($subpages[$i]["image_src"]) && is_file($subpages[$i]["image_src"]) && user::$uID>0)
                            {
                                $str_previous_img_info["width"] = $image_width;
                                $str_previous_img_info["height"] = $image_width;
                            }
                        
                    }
            }

        //read subapages
        if(is_array($subpages))
            {                   
            
            for($i=0; $i<count($subpages); $i++)
                {
                        
                    //break when no links shown
                    if($this->get_pInfo("show_link") == 0) break;       
                    
                    //page date
                    if(($this->n == $this->get_sInfo("news") || $this->get_pParent() == $this->get_sInfo("news"))  && $this->get_sInfo("news") != site::$sStartPage)
                        $str_date = "<div class=\"date\">".date("d.m.Y", strtotime($subpages[$i]['date_added']))."</div>";
                    else $str_date ="";
                    
                    $hide_news_dates = $this->get_sConfigValue("CMS_HIDE_NEWS_DATES");
                    if($hide_news_dates) $str_date = "";
                    
                    //default variables
                    //$str_link = "page.php?n=".$subpages[$i]["n"]."&amp;SiteID=$this->SiteID";
                    $str_link = $subpages[$i]['page_link']; 
                    //$str_link = $this->get_pLink($subpages[$i]['n']); 
                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$width."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\">";

                    $str_img_align = "default";
                    
                    //label from pages_to_groups table
                    $str_label = "<span class=\"label\">".$subpages[$i]["label"]."</span>";
                    
                    $str_title = $subpages[$i]["Name"] . $str_date;
                    //$str_txt = "<div class=\"text\">".$subpages[$i]["textStr"]."</div>";
                    $str_txt = $str_label.$subpages[$i]["textStr"]; 
                    //$str_paragraph = "<div class=\"text\">".strip_tags(crop_text($subpages[$i]["textStr"]), "<P>, <p>, <B>, <b>, <U>, <u>, <LI>, <li>, <strong>, <STRONG>, <span>, <i>, <em>, <br>, <BR>")."</div>";
                    $str_paragraph = strip_tags(crop_text($subpages[$i]["textStr"]), "<P>, <p>, <B>, <b>, <U>, <u>, <LI>, <li>, <ul>, <UL>, <ol>, <OL><strong>, <STRONG>, <span>, <i>, <em>, <br>, <BR>");
                    //page prices, product availability etc.
                    $p_prices = $this->get_pPrice($subpages[$i]["n"], "ASC");
                    $str_price = "";
                    $str_availability = "";
                    $str_cart_options = "";
                    if(!empty($p_prices[0]['code']) && $this->get_sConfigValue("SHOPPING_CART_ACTIVATE"))
                        {
                            $str_price = "<span class=\"sPage-price\">".$p_prices[0]['price_value']."</span>";
                            $qty = $this->qty_calc($p_prices[0]['code']);
                            $item = $this->info($p_prices[0]['code']);
                            if($item->itGroup == 5 and $qty > 0 and $this->_site["SitesID"] == 2){
                                $qty = $this->qty_calc($this->info($item->itParentID));
                            };

                            $str_availability = $this->getAvailability($qty, $p_prices[0], $item);

                            if(!($this->is_product($p_prices[0]['code'])))   // service not product
                                $str_availability = "<span class=\"product-call\" title=\"Call to order\"></span>";
                            $price_pcs_str = "";
                            if($p_prices[0]['pcs']>1)  {
                                $price_pcs_str = $p_prices[0]['pcs'].' x ';
                                $unit_price = round($p_prices[0]['price_value']/$p_prices[0]['pcs'],3); 
                                $str_price = "<span class=\"sPage-price\">".$unit_price."</span>";
                            }
                            //page options for shopping
                            $str_cart_options = "<div class=\"page-cart-options2\">".$str_availability.$price_pcs_str.$str_price.((SHOPPING_CART_VIEW_CURRENCY) ? $p_prices[$i]['currency_string'] : "")."</div>";
                        }
                    
                    //read subpages of the current subpage
                    $str_subpage_links = "";
                    $ss_array = $this->get_pSubpages($subpages[$i]["n"], null, 1, false, $limit);
                    if(count($ss_array[0]) > 0 )
                    {
                        $str_subpage_links = "<div class=\"ssLinks\">";
                        for($j = 0; $j<count($ss_array); $j++)
                            {
                                //$str_subpage_links.= "<a href=\"page.php?n=".$ss_array[$j]["n"]."&amp;SiteID=".$this->SiteID."\" title=\"".$ss_array[$j]['title']."\">".$ss_array[$j]["Name"]."</a>"; !!!                     
                                $str_subpage_links.= "<a href=\"".$ss_array[$j]["page_link"]."\" title=\"".$ss_array[$j]['title']."\">".$ss_array[$j]["Name"]."</a>";                       
                            }
                        $str_subpage_links.= "</div>";
                    }
                    else $str_subpage_links = "";
                    //get page more link
                    $str_more = "<a href=\"".$str_link."\" class=\"next_link\" title=\"".$this->get_pInfo("title", $subpages[$i]["n"])."\"  rel=\"nofollow\">".$this->get_sMoreText()."</a>";                   
                    
                    //deafult TEXT and PARAGRAPH content                
                    if(strlen(strip_tags($subpages[$i]["textStr"])) < 5 && user::$uID>0)
                        { 
                            $str_txt = "<div style=\"width: 100%; display: block; background-color: Red; color: #FFFFFF; text-align: center;\"><br><i>page content empty</i><br><br></div>";
                            #$str_paragraph = $this->replace_shortcodes($str_txt);
                        }
                                    
                    //reduce image width depending on page width: default image_width = 1/3*page_width 
                    if(!defined("CMS_IMG_WIDTH_REDUCE"))
                        define("CMS_IMG_WIDTH_REDUCE", 3);

                    //define image crop ratio
                    if(!defined("CMS_IMAGE_CROP_RATIO"))
                        define("CMS_IMAGE_CROP_RATIO", 4/3);    
                    
                    //define automatic image crop
                    if(!defined("CMS_IMAGE_CROP"))
                        define("CMS_IMAGE_CROP", false);
                                            
                    //get subpage image alignment
                    $sp_img_align = $subpages[$i]["image_allign"];                      
                    
                    if($sp_img_align == 2 || $sp_img_align == 10)
                        {
                            $str_img_align = "left";
                            $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                            //$str_previous_img_info["align"] = $str_img_align;
                        }
                    else
                    if($sp_img_align == 3 || $sp_img_align == 11)
                        {
                            $str_img_align = "right";
                            $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                            //$str_previous_img_info["align"] = $str_img_align;
                        }
                    else
                        {
                            $image_width = $width-2*$width_margin;
                            //$str_img_align = "default";
                        }
                        
                    
                    
                    //do not reduce image variables if shown: only image, image + title or suppage text is empty (and subpage links are hidden)
                    if($this->get_pInfo("show_link") == 3 ||  $this->get_pInfo("show_link") == 9 || (strlen($subpages[$i]["textStr"]) <= 3 && $this->_page["show_link"] != 13)) 
                        $image_width = $width-2*$width_margin;
                    
                    //get real img width and reduce if necessary
                    $imgInfo = GetImageSize($subpages[$i]["image_src"]); 
                    //if($imgInfo[0] < $image_width) $image_width = $image_width/2;
                    if($imgInfo[0] < $image_width) $image_width = $imgInfo[0];

                    
                    if($subpages[$i]["image_allign"] == 2 || $subpages[$i]["image_allign"] == 10)
                        $align_class = "align-left";
            
                    if($subpages[$i]["image_allign"] == 3 || $subpages[$i]["image_allign"] == 11)
                        $align_class = "align-right";       
                    
                    if($this->_page['show_link'] == 3)
                        {
                            $align_class = "align-center";  
                            $str_img_align  = "";
                        }
                                        
                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                    
                    //mpetrov correction: 22.10.2014
                    if($MAIN_WIDTH_DIMMENSION == "%")
                        {
                            
                            $image_width = $MAIN_WIDTH;
                            $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/".$subpages[$i]["image_src"]."\" width=\"".$image_width.$MAIN_WIDTH_DIMMENSION."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                        }
                        
                    if($sp_img_align == 1)
                        $str_img.= "";
                        
                    if($sp_img_align == 9 || $sp_img_align == 12)
                        $str_img = "<center>".$str_img."</center>";
                        
                    if($this->get_pInfo("make_links") != 0)
                        {
                            //$str_title = "<a href=\"".$str_link."\" title=\"".$subpages[$i]['title']."\" class=\"title\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; margin: ".$width_margin."px; \"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\">";
                            $str_title = "<a href=\"".$str_link."\" title=\"".$subpages[$i]['title']."\" class=\"title\"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\" class=\"br-style\">";
                        }
                    else    
                            //$str_title = "<a href=\"#\" title=\"".$subpages[$i]['title']."\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; margin: ".$width_margin."px; \"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\">";
                            $str_title = "<a href=\"#\" title=\"".$subpages[$i]['title']."\" class=\"title\"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\" class=\"br-style\">";
                    
                    if($this->get_pInfo("make_links") == 1)
                        { 
                            
                            
                            $style = "";
                            if(CMS_IMAGE_CROP && $this->_page['SiteID'] == $subpages[$i]["SiteID"])
                                {
                                    $style = "display: block; width: ".$image_width."px; float: ".$str_img_align."; height: ".($image_width/CMS_IMAGE_CROP_RATIO)."px; overflow: hidden;";
                                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."&amp;ratio=exact\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                                }
                            
                            $str_img = "<a href=\"".$str_link."\" class=\"img ".$align_class."\" style=\"$style\" title=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\">".$str_img."</a>";
                            
                            $str_txt = "<a href=\"".$str_link."\" title=\"".$subpages[$i]["title"]."\">".$str_txt."</a>";
                            $str_paragraph = "<a href=\"".$str_link."\" >".$str_paragraph."</a>";
                        }
                        
                    //photo gallery
                    if($this->get_pInfo("make_links") == 4)
                        {
                                                        
                            $style = "";
                            if(CMS_IMAGE_CROP && $this->_page['SiteID'] == $subpages[$i]["SiteID"])
                                {
                                    $style = "display: inline-block; width: ".$image_width."px; height: ".($image_width/CMS_IMAGE_CROP_RATIO)."px; overflow: hidden;";
                                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."&amp;ratio=exact\" align=\"".$str_img_align."\"  class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"],ENT_QUOTES)."\" border=\"0\">";
                                }
                            $str_img = "<a href=\"".$subpages[$i]["image_src"]."\" 
                                            data-fresco-caption=\"".$this->get_pInfo("imageName", $subpages[$i]['n'])."\" 
                                            data-fresco-group=\"gallery\" 
                                            data-fresco-group-options=\"overflow: true, thumbnails: 'vertical', onClick: 'close', ui: 'outside'\"
                                            title=\"".$subpages[$i]["Name"] . " / ".$subpages[$i]["imageName"]."\" 
                                            class=\"fresco\" style=\"$style\">".$str_img.
                                        "</a>";

                            //if($this->get_pSubpagesCount($subpages[$i]['n'])==0)
                                                
                            //else
                                //$str_img = "<a href=\"".$this->get_pLink($subpages[$i]["n"])."\" title=\"".$subpages[$i]["Name"] . " / ".$subpages[$i]["imageName"]."\">".$str_img."</a>";                
                        }
                    //no photo file
                    if(!file_exists($subpages[$i]["image_src"]) && !is_file($subpages[$i]["image_src"]))
                        if(user::$uID>0)
                        {
                            //$str_img = "<table style=\"display: block; width: ".$str_previous_img_info["width"]."px; height: ".$str_previous_img_info["height"]."px; border: 1px solid #e7e7e7; text-align: center\" align=\"".$str_previous_img_info["align"]."\"><tr><td valign=\"middle\">no picture</table>";
                            $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=web/admin/images/no_image.jpg&amp;img_width=".$width."\" align=\"".$str_img_align."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\"  style=\"border: 1px solid #ebebeb\">";

                            //mpetrov correction: 22.10.2014
                            if($MAIN_WIDTH_DIMMENSION == "%")
                                $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/web/admin/images/no_image.jpg\" width=\"".$image_width.$MAIN_WIDTH_DIMMENSION."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" style=\"border: 1px solid #ebebeb\">";
                        
                        }
                        else 
                            $str_img = "";              
                    else 
                        {
                            
                            //$str_previous_img_info["width"] = $image_width;
                            //$str_previous_img_info["height"] = $image_width;

                        }
                    
                    if( ($this->_site['site_status']>0) && ($this->_page['SecLevel']==0) ) {
                        //$str_paragraph .= $this->get_top_word($str_paragraph); 
    
                    //if($this->n == 100306)  $str_paragraph = $str_paragraph."<p>***".$this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph)."***</p>"; 
                        $str_txt = $str_txt."&nbsp;".$this->get_seo_etiketi_link($this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph.' '.$str_txt.' '.$subpages[$i]['tags']))."&nbsp"; 
                        $str_paragraph = $str_paragraph."&nbsp;".$this->get_seo_etiketi_link($this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph))."&nbsp";
                        // $str_paragraph .= ' ***  '; 
                    }
                    
                    // MPETROV correction - 22.10.2014
                    
                    $col_class = "";
                    if(is_array($cms_args['CSS_FRAMEWORK']))
                        {
                            $framework = $cms_args['CSS_FRAMEWORK'];
                            if($framework['name'] == "bootstrap3")
                                {
                                    $col_class = " col-xs-".(int)($framework['max_cols']/$colls);   
                                }
                            if($framework['name'] == "bootstrap2")
                                {
                                    $col_class = " col-".(int)($framework['max_cols']/$colls);  
                                }                           
                        }   
                        
                    /*
                    switch($align)
                        {
                            case 1: 
                            case 2:
                            case 3:
                            case 9: { $content.=  $img . $this->get_pText($n);  break; }
                            case 10: 
                            case 11:
                            case 12: { $content.= $this->get_pText($n) . $img; break; }
                            default: $content.= $img . $this->get_pText($n);
                        }
                    */
                    //CONSTRUCT SUBPAGE CONTENT
                    
                    // 1- add links
                    //if($this->get_pInfo("show_link") == 1)
                    if($this->_page['show_link'] == 1)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title ."</div></div>";
                    
                    
                    // 2-   title + picture + text
                    if($this->_page['show_link'] == 2)
                        if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                            $content.= "<div class=\"sPage$col_class\"><div class=\"sPage-content\">". $str_title . "<div class=\"text\">".$str_txt ."</div>". $str_img ."</div>".$str_cart_options."</div>";
                        else
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">". $str_title . "<div class=\"text\">". $str_img . $str_txt ."</div></div>".$str_cart_options."</div>";
                    
                    // 3-   title + picture 
                    if($this->_page['show_link'] == 3)
                        $content.= "<div class=\"col-md-".$colls."\">" . $str_title . $str_img . "".$str_cart_options."</div>";
                    
                    // 4-   title + picture + paragraph
                    if($this->_page['show_link'] == 4)
                        if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_paragraph ."</div>". $str_img . $str_cart_options . $str_more ."</div></div>";
                        else
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_img . $str_cart_options . $str_paragraph ."</div>". $str_more ."</div></div>";
                        
                    // 5-   picture + text
                    if($this->_page['show_link'] == 5)
                        if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_txt .  $str_img . "</div>".$str_cart_options."</div>";
                        else
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_img . $str_txt . "</div>".$str_cart_options."</div>";
                        
                    // 6-   text
                    if($this->_page['show_link'] == 6)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_txt . "</div></div>"; 
                    

                    // 7-   picture + paragraph
                    if($this->_page['show_link'] == 7)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">". $str_img . $str_cart_options . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more ."</div></div>";
                    
                    // 8-   paragraph
                    if($this->_page['show_link'] == 8)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more."</div></div>";
                    
                    // 9-   picture
                    if($this->_page['show_link'] == 9)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content border_image\" style=\"margin: ".$width_margin."px; \">" . $str_img . "</div>".$str_cart_options."</div>";
                        
                    // 10-      title+text
                    if($this->_page['show_link'] == 10)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">".$str_txt . "</div></div></div>";

                    // correction M Kamenov 2.06.2014 padding-bottom   MUST be set as $MAIN_PADDING_BOTTOM
                    //11-   title+ paragraph 
                    if($this->_page['show_link']  == 11)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."; padding-bottom:20px \"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more ."</div></div>";


                    //13-   title + picture + subpage links 
                    if($this->_page['show_link'] == 13)
                        $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_title . $str_img . $str_subpage_links . "</div>";

                    //14-   picture + subpage links 
                    if($this->_page['show_link']  == 14)
                        $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_img . $str_subpage_links . "</div>";

                    //15-   title+ subpage links 
                    if($this->_page['show_link']  == 15)
                        $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_title . $str_subpage_links . "</div>";


                    //end of the current row, new row
                    if(  ($colls_counter >= $colls) || ($i==count($subpages)-1)  )
                        {
                            $content.= "<div style=\"clear: both\" class=\"sPage-separator\"></div>";
                            //$content.= "<div class=\"sPage-separator\"></div>";
                            // Martin Kamenov 06.08.2014
                            $colls_counter = 0;
                        }
                    $colls_counter++;
                    
                }
                
                $content.= "<div class=\"sDelimeter\"></div>";          
                if(site::$uReadLevel >= $this->get_pInfo("SecLevel"))
                        echo "<div class=\"row\">".$content."</div>";
                
                //CMS ADMIN BUTTONS
                //if($this->get_pInfo("show_link") != 0 && count($this->get_pSubpages()) > 0)
                    //$this->admin_buttons();
        }
        
    }
    //read curent page and construct the subcontent
    function print_pSubContent($parent_page=NULL, $filter=1, $filter_by_tag = true, $cms_args = array(), $limit=false)
    {
        //array of global parameters overwriting some of the variables
        global $cms_args;
        
        if($parent_page==NULL) $parent_page = $this->n;
        
        $colls = $this->get_pSubPagesColls();
        if(isset($cms_args['CMS_COLS']))
            $colls =    $cms_args['CMS_COLS'];
        
        $order = $this->get_pSubPagesOrder($parent_page);
        //output group content
        $group_id = $this->get_pInfo("show_group_id", $parent_page);
        //get_pGroupContent($group_id=NULL, $date_expire="0000-00-00", $sort_order= NULL, $filter=1)
        
        /*
        if(isset($group_id) && !empty($group_id))
            $subpages = $this->get_pGroupContent($group_id, date("Y-m-d"), $order, $filter);
        else
            $subpages = $this->get_pSubpages($parent_page, $order, $filter, $filter_by_tag);
        */
        
        $subpages = $this->get_pSubpages($parent_page, $order, $filter, $filter_by_tag, $limit);
        //add group content to subpages
        
        if(isset($group_id) && !empty($group_id))
            {
                $g_pages = array();
                //$s_pages = $this->get_pSubpages($parent_page, $order, $filter, $filter_by_tag);
                $g_order = $order;
                if($order == "p.sort_n ASC" || $order == "ASC" || $order == "p.sort_n DESC" || $order == "DESC")
                    $g_order = NULL;
                $g_pages = $this->get_pGroupContent($group_id, date("Y-m-d"), $g_order, $filter);

                global $user;
                if(count($subpages) == 0)
                    $subpages = $g_pages;
                else
                    if(count($g_pages) > 0)
                    //$subpages = array_merge($s_pages, $g_pages);
                    if($this->n == $this->_site['StartPage']) {
                        $subpages = $g_pages;
                    } else {
                        $subpages = array_merge($subpages, $g_pages);
                    }                        
                    //$subpages = array_merge($subpages, $g_pages);

/*                if(count($g_pages) > 0) {
                    if($this->n == $this->_site['StartPage']) {
                     $subpages = $g_pages;
                    } else {
                     $subpages = array_merge($subpages, $g_pages);
                    }

             }*/


                //place code here for sorting the entire array of pages + group content
                //By now the group pages are added after the subpages
            }
        
        if(isset($cms_args['subpages']))
            $subpages = $cms_args['subpages'];
        
        $MAIN_WIDTH_DIMMENSION = "px";  
        
        if(isset($cms_args['MAIN_WIDTH']))
            $MAIN_WIDTH = $cms_args['MAIN_WIDTH'];
            
    
        if(site::$sMAIN_WIDTH)
            $MAIN_WIDTH = site::$sMAIN_WIDTH;
        
        /* mpetrov correction: 3.05.2014 */
        if(isset($cms_args['CMS_MAIN_WIDTH']))
            $MAIN_WIDTH = $cms_args['CMS_MAIN_WIDTH'];

        if(!isset($MAIN_WIDTH))
            $MAIN_WIDTH = 450;
                    
        //looking for percentages
        if(strpos($MAIN_WIDTH, "%") !== false )
            {
                $MAIN_WIDTH_DIMMENSION = "%";
                $MAIN_WIDTH = str_replace("%", "", $MAIN_WIDTH);
            }
        
        //100% == 950px, 50% == 950*50/100 = 475px
        if($MAIN_WIDTH_DIMMENSION == "%")
            {
                //$MAIN_WIDTH = 950*$MAIN_WIDTH/100;
                //$MAIN_WIDTH_DIMMENSION = "px";
            }
            
        //standart colls margin is 2% peprcents from the computed width
        $width_margin = 1*$MAIN_WIDTH/100;
        // -25 M.Kamenov correction 15.06.2014
        $width = round($MAIN_WIDTH/$colls - 6*$width_margin, 0, PHP_ROUND_HALF_DOWN);  //2*width
        
        if($colls == 1)
            {
                //$width = 100; 
                //$MAIN_WIDTH_DIMMENSION = "%";
                //$width_margin = 0;
            }
            
        $width_percents = (100*$width/$MAIN_WIDTH);
        //if($this->n == 11 || $this->get_pParent() == 11) $width = (int)(450/$colls - 4);
        $image_width = $width/2;
        
        $colls_counter = 1;
        $content = "<div class=\"sDelimeter\"></div>";

        //temporary subpage title
        $str_title = "";
        //temporary subpage image
        $str_img = "";
        //temporary subpage full text
        $str_text = "";
        //temporary subpage 1-st paragraph
        $str_paragraph = "";
        //more link text
        $str_more = "";
        //subpage item
        $str_subpage = "";
        //subpage links
        $str_subpage_links = "";
        //alternative text
        $str_alt = $this->_page['Name'];
        //link to the subpage
        $str_link = "";
        //get the previous page image width in px
        $str_previous_img_info = array("width"=>"180", "height"=>"180", "align"=>"");
        //page price content
        $str_price = "";
        if(!defined('CMS_IMG_WIDTH_REDUCE')) {
            define ('CMS_IMG_WIDTH_REDUCE', 1);
        }
        
        //read and analyze subpages content
        //collect information about subpages images, subpages text
        //cycle works only in edit mode
        if(is_array($subpages))
            {                   
            
                for($i=0; $i<count($subpages); $i++)
                    {
                        if($subpages[$i]["image_allign"] == 2 || $subpages[$i]["image_allign"] == 10)
                            {
                                $str_img_align = "left";
                                $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                                $str_previous_img_info["align"] = $str_img_align;
                            }
                        
                        if($subpages[$i]["image_allign"] == 3 || $subpages[$i]["image_allign"] == 11)
                            {
                                $str_img_align = "right";
                                $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                                $str_previous_img_info["align"] = $str_img_align;
                            }
                            
                        if(file_exists($subpages[$i]["image_src"]) && is_file($subpages[$i]["image_src"]) && user::$uID>0)
                            {
                                $str_previous_img_info["width"] = $image_width;
                                $str_previous_img_info["height"] = $image_width;
                            }
                        
                    }
            }

        //read subapages
        if(is_array($subpages))
            {                   
            
            for($i=0; $i<count($subpages); $i++)
                {
                        
                    //break when no links shown
                    if($this->get_pInfo("show_link") == 0) break;       
                    
                    //page date
                    if(($this->n == $this->get_sInfo("news") || $this->get_pParent() == $this->get_sInfo("news"))  && $this->get_sInfo("news") != site::$sStartPage)
                        $str_date = "<div class=\"date\">".date("d.m.Y", strtotime($subpages[$i]['date_added']))."</div>";
                    else $str_date ="";
                    
                    $hide_news_dates = $this->get_sConfigValue("CMS_HIDE_NEWS_DATES");
                    if($hide_news_dates) $str_date = "";
                    
                    //default variables
                    //$str_link = "page.php?n=".$subpages[$i]["n"]."&amp;SiteID=$this->SiteID";
                    $str_link = $subpages[$i]['page_link']; 
                    //$str_link = $this->get_pLink($subpages[$i]['n']); 
                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$width."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\">";

                    $str_img_align = "default";
                    
                    //label from pages_to_groups table
                    if(isset($subpages[$i]['label'])) {
                        $str_label = "<span class=\"label\">".$subpages[$i]["label"]."</span>";
                    } else {
                        $str_label = '';
                    }
                    
                    $str_title = $subpages[$i]["Name"] . $str_date;
                    //$str_txt = "<div class=\"text\">".$subpages[$i]["textStr"]."</div>";
                    $str_txt = $str_label.$subpages[$i]["textStr"]; 
                    //$str_paragraph = "<div class=\"text\">".strip_tags(crop_text($subpages[$i]["textStr"]), "<P>, <p>, <B>, <b>, <U>, <u>, <LI>, <li>, <strong>, <STRONG>, <span>, <i>, <em>, <br>, <BR>")."</div>";
                    $str_paragraph = strip_tags(crop_text($subpages[$i]["textStr"]), "<P>, <p>, <B>, <b>, <U>, <u>, <LI>, <li>, <ul>, <UL>, <ol>, <OL><strong>, <STRONG>, <span>, <i>, <em>, <br>, <BR>");
                    //page prices, product availability etc.
                    $p_prices = $this->get_pPrice($subpages[$i]["n"], "ASC");
                    $str_price = "";
                    $str_availability = "";
                    $str_cart_options = "";
                    if(!empty($p_prices[0]['code']) && $this->get_sConfigValue("SHOPPING_CART_ACTIVATE"))
                        {
                            $str_price = "<span class=\"sPage-price\">".$p_prices[0]['price_value']."</span>";
                            $qty = $this->qty_calc($p_prices[0]['code']);
                            $item = $this->info($p_prices[0]['code']);
                            if($item->itGroup == 5 and $qty > 0 and $this->_site["SitesID"] == 2){
                                $qty = $this->qty_calc($this->info($item->itParentID));
                            };

                            $str_availability = $this->getAvailability($qty, $p_prices[0], $item);

                            if(!($this->is_product($p_prices[0]['code'])))   // service not product
                                $str_availability = "<span class=\"product-call\" title=\"Call to order\"></span>";
                            $price_pcs_str = "";
                            if($p_prices[0]['pcs']>1)  {
                                $price_pcs_str = $p_prices[0]['pcs'].' x ';
                                $unit_price = round($p_prices[0]['price_value']/$p_prices[0]['pcs'],3); 
                                $str_price = "<span class=\"sPage-price\">".$unit_price."</span>";
                            }
                            //page options for shopping
                            $str_cart_options = "<div class=\"page-cart-options2\">".$str_availability.$price_pcs_str.$str_price.((SHOPPING_CART_VIEW_CURRENCY) ? $p_prices[$i]['currency_string'] : "")."</div>";
                        }
                    
                    //read subpages of the current subpage
                    $str_subpage_links = "";
                    $ss_array = $this->get_pSubpages($subpages[$i]["n"], null, 1, false, $limit);
                    if(count($ss_array[0]) > 0 )
                    {
                        $str_subpage_links = "<div class=\"ssLinks\">";
                        for($j = 0; $j<count($ss_array); $j++)
                            {
                                //$str_subpage_links.= "<a href=\"page.php?n=".$ss_array[$j]["n"]."&amp;SiteID=".$this->SiteID."\" title=\"".$ss_array[$j]['title']."\">".$ss_array[$j]["Name"]."</a>"; !!!                     
                                $str_subpage_links.= "<a href=\"".$ss_array[$j]["page_link"]."\" title=\"".$ss_array[$j]['title']."\">".$ss_array[$j]["Name"]."</a>";                       
                            }
                        $str_subpage_links.= "</div>";
                    }
                    else $str_subpage_links = "";
                    //get page more link
                    $str_more = "<a href=\"".$str_link."\" class=\"next_link\" title=\"".$this->get_pInfo("title", $subpages[$i]["n"])."\"  rel=\"nofollow\">".$this->get_sMoreText()."</a>";                   
                    
                    //deafult TEXT and PARAGRAPH content                
                    if(strlen(strip_tags($subpages[$i]["textStr"])) < 5 && user::$uID>0)
                        { 
                            $str_txt = "<div style=\"width: 100%; display: block; background-color: Red; color: #FFFFFF; text-align: center;\"><br><i>page content empty</i><br><br></div>";
                            #$str_paragraph = $this->replace_shortcodes($str_txt);
                        }
                                    
                    //reduce image width depending on page width: default image_width = 1/3*page_width 
                    if(!defined("CMS_IMG_WIDTH_REDUCE"))
                        define("CMS_IMG_WIDTH_REDUCE", 3);

                    //define image crop ratio
                    if(!defined("CMS_IMAGE_CROP_RATIO"))
                        define("CMS_IMAGE_CROP_RATIO", 4/3);    
                    
                    //define automatic image crop
                    if(!defined("CMS_IMAGE_CROP"))
                        define("CMS_IMAGE_CROP", false);
                                            
                    //get subpage image alignment
                    $sp_img_align = $subpages[$i]["image_allign"];                      
                    
                    if($sp_img_align == 2 || $sp_img_align == 10)
                        {
                            $str_img_align = "left";
                            $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                            //$str_previous_img_info["align"] = $str_img_align;
                        }
                    else
                    if($sp_img_align == 3 || $sp_img_align == 11)
                        {
                            $str_img_align = "right";
                            $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                            //$str_previous_img_info["align"] = $str_img_align;
                        }
                    else
                        {
                            $image_width = $width-2*$width_margin;
                            //$str_img_align = "default";
                        }
                        
                    
                    
                    //do not reduce image variables if shown: only image, image + title or suppage text is empty (and subpage links are hidden)
                    if($this->get_pInfo("show_link") == 3 ||  $this->get_pInfo("show_link") == 9 || (strlen($subpages[$i]["textStr"]) <= 3 && $this->_page["show_link"] != 13)) 
                        $image_width = $width-2*$width_margin;
                    
                    //get real img width and reduce if necessary
                    $img = $subpages[$i]['image_src'];
                    if(empty($img)) {
                        $img = DEFAULT_IMAGE;
                    }
                    $imgInfo = $this->get_image_size($img); 
                    //if($imgInfo[0] < $image_width) $image_width = $image_width/2;
                    if($imgInfo[0] < $image_width) $image_width = $imgInfo[0];

                    $align_class = '';

                    
                    if($subpages[$i]["image_allign"] == 2 || $subpages[$i]["image_allign"] == 10)
                        $align_class = "align-left";
            
                    if($subpages[$i]["image_allign"] == 3 || $subpages[$i]["image_allign"] == 11)
                        $align_class = "align-right";       
                    
                    if($this->_page['show_link'] == 3)
                        {
                            $align_class = "align-center";  
                            $str_img_align  = "";
                        }
                                        
                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                    
                    //mpetrov correction: 22.10.2014
                    if($MAIN_WIDTH_DIMMENSION == "%")
                        {
                            
                            $image_width = $MAIN_WIDTH;
                            $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/".$subpages[$i]["image_src"]."\" width=\"".$image_width.$MAIN_WIDTH_DIMMENSION."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                        }
                        
                    if($sp_img_align == 1)
                        $str_img.= "";
                        
                    if($sp_img_align == 9 || $sp_img_align == 12)
                        $str_img = "<center>".$str_img."</center>";
                        
                    if($this->get_pInfo("make_links") != 0)
                        {
                            //$str_title = "<a href=\"".$str_link."\" title=\"".$subpages[$i]['title']."\" class=\"title\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; margin: ".$width_margin."px; \"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\">";
                            $str_title = "<a href=\"".$str_link."\" title=\"".$subpages[$i]['title']."\" class=\"title\"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\" class=\"br-style\">";
                        }
                    else    
                            //$str_title = "<a href=\"#\" title=\"".$subpages[$i]['title']."\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; margin: ".$width_margin."px; \"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\">";
                            $str_title = "<a href=\"#\" title=\"".$subpages[$i]['title']."\" class=\"title\"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\" class=\"br-style\">";
                    
                    if($this->get_pInfo("make_links") == 1)
                        { 
                            
                            
                            $style = "";
                            if(CMS_IMAGE_CROP && $this->_page['SiteID'] == $subpages[$i]["SiteID"])
                                {
                                    $style = "display: block; width: ".$image_width."px; float: ".$str_img_align."; height: ".($image_width/CMS_IMAGE_CROP_RATIO)."px; overflow: hidden;";
                                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."&amp;ratio=exact\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                                }
                            
                            $str_img = "<a href=\"".$str_link."\" class=\"img ".$align_class."\" style=\"$style\" title=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\">".$str_img."</a>";
                            
                            $str_txt = "<a href=\"".$str_link."\" title=\"".$subpages[$i]["title"]."\">".$str_txt."</a>";
                            $str_paragraph = "<a href=\"".$str_link."\" >".$str_paragraph."</a>";
                        }
                        
                    //photo gallery
                    if($this->get_pInfo("make_links") == 4)
                        {
                                                        
                            $style = "";
                            if(CMS_IMAGE_CROP && $this->_page['SiteID'] == $subpages[$i]["SiteID"])
                                {
                                    $style = "display: inline-block; width: ".$image_width."px; height: ".($image_width/CMS_IMAGE_CROP_RATIO)."px; overflow: hidden;";
                                    $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."&amp;ratio=exact\" align=\"".$str_img_align."\"  class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"],ENT_QUOTES)."\" border=\"0\">";
                                }
                            $str_img = "<a href=\"".$subpages[$i]["image_src"]."\" 
                                            data-fresco-caption=\"".$this->get_pInfo("imageName", $subpages[$i]['n'])."\" 
                                            data-fresco-group=\"gallery\" 
                                            data-fresco-group-options=\"overflow: true, thumbnails: 'vertical', onClick: 'close', ui: 'outside'\"
                                            title=\"".$subpages[$i]["Name"] . " / ".$subpages[$i]["imageName"]."\" 
                                            class=\"fresco\" style=\"$style\">".$str_img.
                                        "</a>";

                            //if($this->get_pSubpagesCount($subpages[$i]['n'])==0)
                                                
                            //else
                                //$str_img = "<a href=\"".$this->get_pLink($subpages[$i]["n"])."\" title=\"".$subpages[$i]["Name"] . " / ".$subpages[$i]["imageName"]."\">".$str_img."</a>";                
                        }
                    //no photo file
                    if(!file_exists($subpages[$i]["image_src"]) && !is_file($subpages[$i]["image_src"]))
                        if(user::$uID>0)
                        {
                            //$str_img = "<table style=\"display: block; width: ".$str_previous_img_info["width"]."px; height: ".$str_previous_img_info["height"]."px; border: 1px solid #e7e7e7; text-align: center\" align=\"".$str_previous_img_info["align"]."\"><tr><td valign=\"middle\">no picture</table>";
                            $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=web/admin/images/no_image.jpg&amp;img_width=".$width."\" align=\"".$str_img_align."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\"  style=\"border: 1px solid #ebebeb\">";

                            //mpetrov correction: 22.10.2014
                            if($MAIN_WIDTH_DIMMENSION == "%")
                                $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/web/admin/images/no_image.jpg\" width=\"".$image_width.$MAIN_WIDTH_DIMMENSION."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" style=\"border: 1px solid #ebebeb\">";
                        
                        }
                        else 
                            $str_img = "";              
                    else 
                        {
                            
                            //$str_previous_img_info["width"] = $image_width;
                            //$str_previous_img_info["height"] = $image_width;

                        }
                    
                    if( ($this->_site['site_status']>0) && ($this->_page['SecLevel']==0) ) {
                        //$str_paragraph .= $this->get_top_word($str_paragraph); 
    
                    //if($this->n == 100306)  $str_paragraph = $str_paragraph."<p>***".$this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph)."***</p>"; 
                        $str_txt = $str_txt."&nbsp;".$this->get_seo_etiketi_link($this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph.' '.$str_txt.' '.$subpages[$i]['tags']))."&nbsp"; 
                        $str_paragraph = $str_paragraph."&nbsp;".$this->get_seo_etiketi_link($this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph))."&nbsp";
                        // $str_paragraph .= ' ***  '; 
                    }
                    
                    // MPETROV correction - 22.10.2014
                    
                    $col_class = "";
                    if(isset($cms_args['CSS_FRAMEWORK']) and is_array($cms_args['CSS_FRAMEWORK']))
                        {
                            $framework = $cms_args['CSS_FRAMEWORK'];
                            if($framework['name'] == "bootstrap3")
                                {
                                    $col_class = " col-xs-".(int)($framework['max_cols']/$colls);   
                                }
                            if($framework['name'] == "bootstrap2")
                                {
                                    $col_class = " col-".(int)($framework['max_cols']/$colls);  
                                }                           
                        }   
                        
                    /*
                    switch($align)
                        {
                            case 1: 
                            case 2:
                            case 3:
                            case 9: { $content.=  $img . $this->get_pText($n);  break; }
                            case 10: 
                            case 11:
                            case 12: { $content.= $this->get_pText($n) . $img; break; }
                            default: $content.= $img . $this->get_pText($n);
                        }
                    */
                    //CONSTRUCT SUBPAGE CONTENT
                    
                    // 1- add links
                    //if($this->get_pInfo("show_link") == 1)
                    if($this->_page['show_link'] == 1)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title ."</div></div>";
                    
                    
                    // 2-   title + picture + text
                    if($this->_page['show_link'] == 2)
                        if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">". $str_title . "<div class=\"text\">".$str_txt ."</div>". $str_img ."</div>".$str_cart_options."</div>";
                        else
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">". $str_title . "<div class=\"text\">". $str_img . $str_txt ."</div></div>".$str_cart_options."</div>";
                    
                    // 3-   title + picture 
                    if($this->_page['show_link'] == 3)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."; margin: ".$width_margin."px;\"><div class=\"sPage-content\">" . $str_title . $str_img . "</div>".$str_cart_options."</div>";
                    
                    // 4-   title + picture + paragraph
                    if($this->_page['show_link'] == 4)
                        if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_paragraph ."</div>". $str_img . $str_cart_options . $str_more ."</div></div>";
                        else
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_img . $str_cart_options . $str_paragraph ."</div>". $str_more ."</div></div>";
                        
                    // 5-   picture + text
                    if($this->_page['show_link'] == 5)
                        if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_txt .  $str_img . "</div>".$str_cart_options."</div>";
                        else
                            $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_img . $str_txt . "</div>".$str_cart_options."</div>";
                        
                    // 6-   text
                    if($this->_page['show_link'] == 6)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_txt . "</div></div>"; 
                    

                    // 7-   picture + paragraph
                    if($this->_page['show_link'] == 7)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">". $str_img . $str_cart_options . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more ."</div></div>";
                    
                    // 8-   paragraph
                    if($this->_page['show_link'] == 8)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more."</div></div>";
                    
                    // 9-   picture
                    if($this->_page['show_link'] == 9)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content border_image\" style=\"margin: ".$width_margin."px; \">" . $str_img . "</div>".$str_cart_options."</div>";
                        
                    // 10-      title+text
                    if($this->_page['show_link'] == 10)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">".$str_txt . "</div></div></div>";

                    // correction M Kamenov 2.06.2014 padding-bottom   MUST be set as $MAIN_PADDING_BOTTOM
                    //11-   title+ paragraph 
                    if($this->_page['show_link']  == 11)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."; padding-bottom:20px \"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more ."</div></div>";


                    //13-   title + picture + subpage links 
                    if($this->_page['show_link'] == 13)
                        $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_title . $str_img . $str_subpage_links . "</div>";

                    //14-   picture + subpage links 
                    if($this->_page['show_link']  == 14)
                        $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_img . $str_subpage_links . "</div>";

                    //15-   title+ subpage links 
                    if($this->_page['show_link']  == 15)
                        $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_title . $str_subpage_links . "</div>";


                    //end of the current row, new row
                    if(  ($colls_counter >= $colls) || ($i==count($subpages)-1)  )
                        {
                            $content.= "<div style=\"clear: both\" class=\"sPage-separator\"></div>";
                            //$content.= "<div class=\"sPage-separator\"></div>";
                            // Martin Kamenov 06.08.2014
                            $colls_counter = 0;
                        }
                    $colls_counter++;
                    
                }
                
                $content.= "<div class=\"sDelimeter\"></div>";          
                if(site::$uReadLevel >= $this->get_pInfo("SecLevel"))
                    if(isset($cms_args['CSS_FRAMEWORK']) and $cms_args['CSS_FRAMEWORK'] == "bootstrap3" || $cms_args['CSS_FRAMEWORK'] == "bootstrap2")
                        echo "<div class=\"row\" id=\"pageSubcontent\">".$content."</div>";
                    else
                        echo "<div class=\"subPages-columns\" style=\"display: block; margin: 0px; padding: 0px; width:100%;\">".$content."</div>";
                
                //CMS ADMIN BUTTONS
                //if($this->get_pInfo("show_link") != 0 && count($this->get_pSubpages()) > 0)
                    //$this->admin_buttons();
        }
        
    }

    protected function as_subpage($page)
    {
        if($subpages[$i]["image_allign"] == 2 || $subpages[$i]["image_allign"] == 10)
            {
                $str_img_align = "left";
                $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                $str_previous_img_info["align"] = $str_img_align;
            }
        
        if($subpages[$i]["image_allign"] == 3 || $subpages[$i]["image_allign"] == 11)
            {
                $str_img_align = "right";
                $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                $str_previous_img_info["align"] = $str_img_align;
            }
            
        if(file_exists($subpages[$i]["image_src"]) && is_file($subpages[$i]["image_src"]) && user::$uID>0)
            {
                $str_previous_img_info["width"] = $image_width;
                $str_previous_img_info["height"] = $image_width;
            }
        
    }


    public function print_subpages($subpages)
    {
        $colls_counter =1;
        $content = '';
        if(!is_array($subpages)){
            return $content;
        }

        $colls = $this->get_pSubPagesColls();
        global $cms_args;
        if($cms_args['CMS_COLS'])
            $colls =    $cms_args['CMS_COLS'];
        $str_previous_img_info = array("width"=>"180", "height"=>"180", "align"=>"");


        $MAIN_WIDTH_DIMMENSION = "px";  
        
        if(isset($cms_args['MAIN_WIDTH']))
            $MAIN_WIDTH = $cms_args['MAIN_WIDTH'];
            
    
        if(site::$sMAIN_WIDTH)
            $MAIN_WIDTH = site::$sMAIN_WIDTH;
        
        /* mpetrov correction: 3.05.2014 */
        if(isset($cms_args['CMS_MAIN_WIDTH']))
            $MAIN_WIDTH = $cms_args['CMS_MAIN_WIDTH'];

        if(!isset($MAIN_WIDTH))
            $MAIN_WIDTH = 450;
                    
        //looking for percentages
        if(strpos($MAIN_WIDTH, "%") !== false )
            {
                $MAIN_WIDTH_DIMMENSION = "%";
                $MAIN_WIDTH = str_replace("%", "", $MAIN_WIDTH);
            }
        
        //standart colls margin is 2% peprcents from the computed width
        $width_margin = 1*$MAIN_WIDTH/100;
        // -25 M.Kamenov correction 15.06.2014

        $width = round($MAIN_WIDTH/$colls - 2*$width_margin, 0, PHP_ROUND_HALF_DOWN);
            for($i=0; $i<count($subpages); $i++) {
                if($subpages[$i]["image_allign"] == 2 || $subpages[$i]["image_allign"] == 10)
                    {
                        $str_img_align = "left";
                        $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                        $str_previous_img_info["align"] = $str_img_align;
                    }
                
                if($subpages[$i]["image_allign"] == 3 || $subpages[$i]["image_allign"] == 11)
                    {
                        $str_img_align = "right";
                        $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                        $str_previous_img_info["align"] = $str_img_align;
                    }
                    
                if(file_exists($subpages[$i]["image_src"]) && is_file($subpages[$i]["image_src"]) && user::$uID>0)
                    {
                        $str_previous_img_info["width"] = $image_width;
                        $str_previous_img_info["height"] = $image_width;
                    }

                //break when no links shown
                if($this->get_pInfo("show_link") == 0) break;       
                
                //page date
                if(($this->n == $this->get_sInfo("news") || $this->get_pParent() == $this->get_sInfo("news"))  && $this->get_sInfo("news") != site::$sStartPage)
                    $str_date = "<div class=\"date\">".date("d.m.Y", strtotime($subpages[$i]['date_added']))."</div>";
                else $str_date ="";
                
                $hide_news_dates = $this->get_sConfigValue("CMS_HIDE_NEWS_DATES");
                if($hide_news_dates) $str_date = "";
                
                //default variables
                //$str_link = "page.php?n=".$subpages[$i]["n"]."&amp;SiteID=$this->SiteID";
                $str_link = $subpages[$i]['page_link']; 
                //$str_link = $this->get_pLink($subpages[$i]['n']); 
                $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$width."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\">";

                $str_img_align = "default";
                
                //label from pages_to_groups table
                $str_label = "<span class=\"label\">".$subpages[$i]["label"]."</span>";
                
                $str_title = $subpages[$i]["Name"] . $str_date;
                //$str_txt = "<div class=\"text\">".$subpages[$i]["textStr"]."</div>";
                $str_txt = $str_label.$subpages[$i]["textStr"]; 
                //$str_paragraph = "<div class=\"text\">".strip_tags(crop_text($subpages[$i]["textStr"]), "<P>, <p>, <B>, <b>, <U>, <u>, <LI>, <li>, <strong>, <STRONG>, <span>, <i>, <em>, <br>, <BR>")."</div>";
                $str_paragraph = strip_tags(crop_text($subpages[$i]["textStr"]), "<P>, <p>, <B>, <b>, <U>, <u>, <LI>, <li>, <ul>, <UL>, <ol>, <OL><strong>, <STRONG>, <span>, <i>, <em>, <br>, <BR>");
                //page prices, product availability etc.
                $p_prices = $this->get_pPrice($subpages[$i]["n"], "ASC");
                $str_price = "";
                $str_availability = "";
                $str_cart_options = "";
                if($p_prices[0]['code'] > 0 && $this->get_sConfigValue("SHOPPING_CART_ACTIVATE"))
                    {
                        $str_price = "<span class=\"sPage-price\">".$p_prices[0]['price_value']."</span>";
                        $qty = $this->qty_calc($p_prices[0]['code']);
                        $item = $this->info($p_prices[0]['code']);
                        if($item->itGroup == 5 and $qty > 0 and $this->_site["SitesID"] == 2){
                            $qty = $this->qty_calc($this->info($item->itParentID));
                        };

                        $str_availability = $this->getAvailability($qty, $p_prices[0], $item);

                        if(!($this->is_product($p_prices[0]['code'])))   // service not product
                            $str_availability = "<span class=\"product-call\" title=\"Call to order\"></span>";
                        $price_pcs_str = "";
                        if($p_prices[0]['pcs']>1)  {
                            $price_pcs_str = $p_prices[0]['pcs'].' x ';
                            $unit_price = round($p_prices[0]['price_value']/$p_prices[0]['pcs'],3); 
                            $str_price = "<span class=\"sPage-price\">".$unit_price."</span>";
                        }
                        //page options for shopping
                        $str_cart_options = "<div class=\"page-cart-options2\">".$str_availability.$price_pcs_str.$str_price.((SHOPPING_CART_VIEW_CURRENCY) ? $p_prices[$i]['currency_string'] : "")."</div>";
                    }
                
                //read subpages of the current subpage
                $str_subpage_links = "";
                $ss_array = $this->get_pSubpages($subpages[$i]["n"], null, 1, false, $limit);
                if(count($ss_array[0]) > 0 )
                {
                    $str_subpage_links = "<div class=\"ssLinks\">";
                    for($j = 0; $j<count($ss_array); $j++)
                        {
                            //$str_subpage_links.= "<a href=\"page.php?n=".$ss_array[$j]["n"]."&amp;SiteID=".$this->SiteID."\" title=\"".$ss_array[$j]['title']."\">".$ss_array[$j]["Name"]."</a>"; !!!                     
                            $str_subpage_links.= "<a href=\"".$ss_array[$j]["page_link"]."\" title=\"".$ss_array[$j]['title']."\">".$ss_array[$j]["Name"]."</a>";                       
                        }
                    $str_subpage_links.= "</div>";
                }
                else $str_subpage_links = "";
                //get page more link
                $str_more = "<a href=\"".$str_link."\" class=\"next_link\" title=\"".$this->get_pInfo("title", $subpages[$i]["n"])."\"  rel=\"nofollow\">".$this->get_sMoreText()."</a>";                   
                
                //deafult TEXT and PARAGRAPH content                
                if(strlen(strip_tags($subpages[$i]["textStr"])) < 5 && user::$uID>0)
                    { 
                        $str_txt = "<div style=\"width: 100%; display: block; background-color: Red; color: #FFFFFF; text-align: center;\"><br><i>page content empty</i><br><br></div>";
                        #$str_paragraph = $this->replace_shortcodes($str_txt);
                    }
                                
                //reduce image width depending on page width: default image_width = 1/3*page_width 
                if(!defined("CMS_IMG_WIDTH_REDUCE"))
                    define("CMS_IMG_WIDTH_REDUCE", 3);

                //define image crop ratio
                if(!defined("CMS_IMAGE_CROP_RATIO"))
                    define("CMS_IMAGE_CROP_RATIO", 4/3);    
                
                //define automatic image crop
                if(!defined("CMS_IMAGE_CROP"))
                    define("CMS_IMAGE_CROP", false);
                                        
                //get subpage image alignment
                $sp_img_align = $subpages[$i]["image_allign"];                      
                
                if($sp_img_align == 2 || $sp_img_align == 10)
                    {
                        $str_img_align = "left";
                        $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                        //$str_previous_img_info["align"] = $str_img_align;
                    }
                else
                if($sp_img_align == 3 || $sp_img_align == 11)
                    {
                        $str_img_align = "right";
                        $image_width = $width/CMS_IMG_WIDTH_REDUCE;
                        //$str_previous_img_info["align"] = $str_img_align;
                    }
                else
                    {
                        $image_width = $width-2*$width_margin;
                        //$str_img_align = "default";
                    }
                    
                
                
                //do not reduce image variables if shown: only image, image + title or suppage text is empty (and subpage links are hidden)
                if($this->get_pInfo("show_link") == 3 ||  $this->get_pInfo("show_link") == 9 || (strlen($subpages[$i]["textStr"]) <= 3 && $this->_page["show_link"] != 13)) 
                    $image_width = $width-2*$width_margin;
                
                //get real img width and reduce if necessary
                $imgInfo = $this->get_image_size($subpages[$i]["image_src"]); 
                //if($imgInfo[0] < $image_width) $image_width = $image_width/2;
                if($imgInfo[0] < $image_width) $image_width = $imgInfo[0];

                
                if($subpages[$i]["image_allign"] == 2 || $subpages[$i]["image_allign"] == 10)
                    $align_class = "align-left";
        
                if($subpages[$i]["image_allign"] == 3 || $subpages[$i]["image_allign"] == 11)
                    $align_class = "align-right";       
                
                if($this->_page['show_link'] == 3)
                    {
                        $align_class = "align-center";  
                        $str_img_align  = "";
                    }
                                    
                $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                
                //mpetrov correction: 22.10.2014
                if($MAIN_WIDTH_DIMMENSION == "%")
                    {
                        
                        $image_width = $MAIN_WIDTH;
                        $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/".$subpages[$i]["image_src"]."\" width=\"".$image_width.$MAIN_WIDTH_DIMMENSION."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                    }
                    
                if($sp_img_align == 1)
                    $str_img.= "";
                    
                if($sp_img_align == 9 || $sp_img_align == 12)
                    $str_img = "<center>".$str_img."</center>";
                    
                if($this->get_pInfo("make_links") != 0)
                    {
                        //$str_title = "<a href=\"".$str_link."\" title=\"".$subpages[$i]['title']."\" class=\"title\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; margin: ".$width_margin."px; \"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\">";
                        $str_title = "<a href=\"".$str_link."\" title=\"".$subpages[$i]['title']."\" class=\"title\"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\" class=\"br-style\">";
                    }
                else    
                        //$str_title = "<a href=\"#\" title=\"".$subpages[$i]['title']."\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; margin: ".$width_margin."px; \"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\">";
                        $str_title = "<a href=\"#\" title=\"".$subpages[$i]['title']."\" class=\"title\"><div class=\"bullet1\" style=\"float:left;\"></div><div class=\"text\">".$str_title."</div></a><br style=\"clear: both;\" class=\"br-style\">";
                
                if($this->get_pInfo("make_links") == 1)
                    { 
                        
                        
                        $style = "";
                        if(CMS_IMAGE_CROP && $this->_page['SiteID'] == $subpages[$i]["SiteID"])
                            {
                                $style = "display: block; width: ".$image_width."px; float: ".$str_img_align."; height: ".($image_width/CMS_IMAGE_CROP_RATIO)."px; overflow: hidden;";
                                $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."&amp;ratio=exact\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" border=\"0\">";
                            }
                        
                        $str_img = "<a href=\"".$str_link."\" class=\"img ".$align_class."\" style=\"$style\" title=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\">".$str_img."</a>";
                        
                        $str_txt = "<a href=\"".$str_link."\" title=\"".$subpages[$i]["title"]."\">".$str_txt."</a>";
                        $str_paragraph = "<a href=\"".$str_link."\" >".$str_paragraph."</a>";
                    }
                    
                //photo gallery
                if($this->get_pInfo("make_links") == 4)
                    {
                                                    
                        $style = "";
                        if(CMS_IMAGE_CROP && $this->_page['SiteID'] == $subpages[$i]["SiteID"])
                            {
                                $style = "display: inline-block; width: ".$image_width."px; height: ".($image_width/CMS_IMAGE_CROP_RATIO)."px; overflow: hidden;";
                                $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$subpages[$i]["image_src"]."&amp;img_width=".$image_width."&amp;ratio=exact\" align=\"".$str_img_align."\"  class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"],ENT_QUOTES)."\" border=\"0\">";
                            }
                        $str_img = "<a href=\"".$subpages[$i]["image_src"]."\" 
                                        data-fresco-caption=\"".$this->get_pInfo("imageName", $subpages[$i]['n'])."\" 
                                        data-fresco-group=\"gallery\" 
                                        data-fresco-group-options=\"overflow: true, thumbnails: 'vertical', onClick: 'close', ui: 'outside'\"
                                        title=\"".$subpages[$i]["Name"] . " / ".$subpages[$i]["imageName"]."\" 
                                        class=\"fresco\" style=\"$style\">".$str_img.
                                    "</a>";

                        //if($this->get_pSubpagesCount($subpages[$i]['n'])==0)
                                            
                        //else
                            //$str_img = "<a href=\"".$this->get_pLink($subpages[$i]["n"])."\" title=\"".$subpages[$i]["Name"] . " / ".$subpages[$i]["imageName"]."\">".$str_img."</a>";                
                    }
                //no photo file
                if(!file_exists($subpages[$i]["image_src"]) && !is_file($subpages[$i]["image_src"]))
                    if(user::$uID>0)
                    {
                        //$str_img = "<table style=\"display: block; width: ".$str_previous_img_info["width"]."px; height: ".$str_previous_img_info["height"]."px; border: 1px solid #e7e7e7; text-align: center\" align=\"".$str_previous_img_info["align"]."\"><tr><td valign=\"middle\">no picture</table>";
                        $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=web/admin/images/no_image.jpg&amp;img_width=".$width."\" align=\"".$str_img_align."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\"  style=\"border: 1px solid #ebebeb\">";

                        //mpetrov correction: 22.10.2014
                        if($MAIN_WIDTH_DIMMENSION == "%")
                            $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/web/admin/images/no_image.jpg\" width=\"".$image_width.$MAIN_WIDTH_DIMMENSION."\" align=\"".$str_img_align."\" class=\"".$align_class."\" alt=\"".htmlspecialchars($subpages[$i]["imageName"], ENT_QUOTES)."\" style=\"border: 1px solid #ebebeb\">";
                    
                    }
                    else 
                        $str_img = "";              
                else 
                    {
                        
                        //$str_previous_img_info["width"] = $image_width;
                        //$str_previous_img_info["height"] = $image_width;

                    }
                
                if( ($this->_site['site_status']>0) && ($this->_page['SecLevel']==0) ) {
                    //$str_paragraph .= $this->get_top_word($str_paragraph); 

                //if($this->n == 100306)  $str_paragraph = $str_paragraph."<p>***".$this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph)."***</p>"; 
                    $str_txt = $str_txt."&nbsp;".$this->get_seo_etiketi_link($this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph.' '.$str_txt.' '.$subpages[$i]['tags']))."&nbsp"; 
                    $str_paragraph = $str_paragraph."&nbsp;".$this->get_seo_etiketi_link($this->get_top_word($subpages[$i]['title'].' '.$subpages[$i]['name'].' '.$str_paragraph))."&nbsp";
                    // $str_paragraph .= ' ***  '; 
                }
                
                // MPETROV correction - 22.10.2014
                
                $col_class = "";
                if(is_array($cms_args['CSS_FRAMEWORK']))
                    {
                        $framework = $cms_args['CSS_FRAMEWORK'];
                        if($framework['name'] == "bootstrap3")
                            {
                                $col_class = " col-xs-".(int)($framework['max_cols']/$colls);   
                            }
                        if($framework['name'] == "bootstrap2")
                            {
                                $col_class = " col-".(int)($framework['max_cols']/$colls);  
                            }                           
                    }   
                    
                /*
                switch($align)
                    {
                        case 1: 
                        case 2:
                        case 3:
                        case 9: { $content.=  $img . $this->get_pText($n);  break; }
                        case 10: 
                        case 11:
                        case 12: { $content.= $this->get_pText($n) . $img; break; }
                        default: $content.= $img . $this->get_pText($n);
                    }
                */
                //CONSTRUCT SUBPAGE CONTENT
                
                // 1- add links
                //if($this->get_pInfo("show_link") == 1)
                if($this->_page['show_link'] == 1)
                    $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title ."</div></div>";
                
                
                // 2-   title + picture + text
                if($this->_page['show_link'] == 2)
                    if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">". $str_title . "<div class=\"text\">".$str_txt ."</div>". $str_img ."</div>".$str_cart_options."</div>";
                    else
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">". $str_title . "<div class=\"text\">". $str_img . $str_txt ."</div></div>".$str_cart_options."</div>";
                
                // 3-   title + picture 
                if($this->_page['show_link'] == 3)
                    $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."; margin: ".$width_margin."px;\"><div class=\"sPage-content\">" . $str_title . $str_img . "</div>".$str_cart_options."</div>";
                
                // 4-   title + picture + paragraph
                if($this->_page['show_link'] == 4)
                    if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_paragraph ."</div>". $str_img . $str_cart_options . $str_more ."</div></div>";
                    else
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_img . $str_cart_options . $str_paragraph ."</div>". $str_more ."</div></div>";
                    
                // 5-   picture + text
                if($this->_page['show_link'] == 5)
                    if($sp_img_align == 10 || $sp_img_align == 11 || $sp_img_align == 12)
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_txt .  $str_img . "</div>".$str_cart_options."</div>";
                    else
                        $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_img . $str_txt . "</div>".$str_cart_options."</div>";
                    
                // 6-   text
                if($this->_page['show_link'] == 6)
                    $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"text sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_txt . "</div></div>"; 
                

                // 7-   picture + paragraph
                if($this->_page['show_link'] == 7)
                    $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">". $str_img . $str_cart_options . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more ."</div></div>";
                
                // 8-   paragraph
                if($this->_page['show_link'] == 8)
                    $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more."</div></div>";
                
                // 9-   picture
                if($this->_page['show_link'] == 9)
                    $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content border_image\" style=\"margin: ".$width_margin."px; \">" . $str_img . "</div>".$str_cart_options."</div>";
                    
                // 10-      title+text
                if($this->_page['show_link'] == 10)
                    $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."\"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">".$str_txt . "</div></div></div>";

                // correction M Kamenov 2.06.2014 padding-bottom   MUST be set as $MAIN_PADDING_BOTTOM
                //11-   title+ paragraph 
                if($this->_page['show_link']  == 11)
                    $content.= "<div class=\"sPage$col_class\" style=\"float: left; width: ".$width.$MAIN_WIDTH_DIMMENSION."; padding-bottom:20px \"><div class=\"sPage-content\" style=\"margin: ".$width_margin."px; \">" . $str_title . "<div class=\"text\">". $str_paragraph ."</div>" . $str_more ."</div></div>";


                //13-   title + picture + subpage links 
                if($this->_page['show_link'] == 13)
                    $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_title . $str_img . $str_subpage_links . "</div>";

                //14-   picture + subpage links 
                if($this->_page['show_link']  == 14)
                    $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_img . $str_subpage_links . "</div>";

                //15-   title+ subpage links 
                if($this->_page['show_link']  == 15)
                    $content.= "<div class=\"sPage$col_class\" style=\"width: ".$width.$MAIN_WIDTH_DIMMENSION."; float: left;\">" . $str_title . $str_subpage_links . "</div>";


                //end of the current row, new row
                if(  ($colls_counter >= $colls) || ($i==count($subpages)-1)  )
                    {
                        $content.= "<div style=\"clear: both\" class=\"sPage-separator\"></div>";
                        //$content.= "<div class=\"sPage-separator\"></div>";
                        // Martin Kamenov 06.08.2014
                        $colls_counter = 0;
                    }
                $colls_counter++;
                
            }
                
            $content.= "<div class=\"sDelimeter\"></div>";          
            if(site::$uReadLevel >= $this->get_pInfo("SecLevel"))
                if($cms_args['CSS_FRAMEWORK'] == "bootstrap3" || $cms_args['CSS_FRAMEWORK'] == "bootstrap2")
                    echo "<div class=\"row\" id=\"pageSubcontent\">".$content."</div>";
                else
                    echo "<div style=\"display: block; margin: 0px; padding: 0px; width:100%;\">".$content."</div>";
                
                //CMS ADMIN BUTTONS
                //if($this->get_pInfo("show_link") != 0 && count($this->get_pSubpages()) > 0)
                    //$this->admin_buttons();
        return $content;
    }
            
    //search for page prices
    //function could return MIN, MAX or ALL prices
    function get_pPrice($page=false, $condition="ASC")
        {
            if(!$page) {
                $page = $this->n;
            }
            $page_prices = array();
            $sub_prices = array();
            
            //$page_prices_sql = "SELECT * FROM prices p left join currencies c on p.price_currency = c.currency_id WHERE price_n = '".$page."' AND price_SiteID = '".$this->SiteID."' ORDER by price_value $condition";
            $page_prices_sql = "SELECT * FROM prices p left join currencies c on p.price_currency = c.currency_id WHERE price_n = '".$page."' ORDER by price_value $condition";
            $page_prices_res = $this->db_query($page_prices_sql);
            while($row = $this->fetch("object", $page_prices_res))
                {
                    $sub_prices['id'] = $row->price_id;
                    $sub_prices['n'] = $row->price_n;
                    $sub_prices['SiteID'] = $row->price_SiteID;
                    if(empty($row->price_description) || $row->price_description == "")
                        $row->price_description = $this->get_pName($page);
                    $sub_prices['description'] = $row->price_description;
                    $sub_prices['code'] = $row->price_code;
                    $sub_prices['pcs'] = $row->price_pcs;
                    $sub_prices['qty'] = $row->price_qty;
                    $sub_prices['price_value'] = number_format($row->price_value, 2, '.', '');
                    $sub_prices['price_value_single'] = $sub_prices['price_value']/$sub_prices['pcs'];
                    $sub_prices['currency_string'] = $row->currency_string;
                    $sub_prices['currency_id'] = $row->currency_id;
                    $sub_prices['currency_key'] = $row->currency_key;
                    
                    array_push($page_prices, $sub_prices);
                    //reset sub_prices array
                    $sub_prices = array();
                }
            if($this->count($page_prices_res) > 0)    
                return $page_prices;
            else return NULL;
        }   
        
    function print_pPrice($page, $return_value="ALL", $add_to_cart = false, $prices_table=false)
        {
            $content = '';
            if($prices_table){
                print $prices_table;
                return;
            }
            if(!isset($page)) $page = $this->n;


            /*
            return_value: 
                                MIN - return the minimal price  
                                MAX - return the highest price
                                ALL - return all prices
            desrciption: output page prices + add_to_cart button (optional) or returns an array of prices
            */ 
        


            switch($return_value)
                {
                    case "MIN":
                                {
                                    $page_prices = $this->get_pPrice($page, "ASC");
                                    $stop = true;
                                    break;                      
                                }
                    case "MAX":
                                {
                                    $page_prices = $this->get_pPrice($page, "DESC");
                                    $stop = true;
                                    break;                      
                                }
                    case "ALL":
                    default:
                                { 
                                    $stop = false;
                                    $page_prices = $this->get_pPrice($page, "ASC");
                                    break; 
                                }       
                }
            
            $add_to_cart_str = "";
            $currency_str = "";
            
            
            if(count($page_prices) > 0)
            {
                $content = "<table border=\"0\" cellspacing=\"2\" cellpadding=\"2\" id=\"page_prices\"><tr><th colspan=\"7\">".$this->get_pName()."</th>";
                for($i=0;$i<count($page_prices);$i++)
                        {
                            $product_availability = "";
                            if(SHOPPING_CART_VIEW_CURRENCY == 1)
                                $currency_str = "&nbsp;".$page_prices[$i]['currency_string'];
                                
                            if($add_to_cart) 
                            {
                                //if product is not stored in any storage do not show product availability                  
                                if($this->is_item($page_prices[$i]['code']))
                                    {

                                        $qty = $this->qty_calc($page_prices[$i]['code']);

                                        $item = $this->info($page_prices[$i]['code']);

                                        if($item->itGroup == 5 and $qty < 0){
                                            $qty = $this->qty_calc($this->info($item->itParentID));
                                        };

                                        $str_availability = $this->getAvailability($qty, $page_prices[$i], $item);

                                        $product_availability = "<td align=\"center\" valign=\"top\" class=\"cart_item_availability\" width=\"auto\">".$str_availability;
                                        
                                    }
                                //var_dump($page_prices);
                                if(count($page_prices)>1 && $stop)
                                    $add_to_cart_str = "<td width=50 valign=\"top\" align=\"center\" class=\"cart\"><a href=\"page.php?n=".$page."&amp;SiteID=".$this->SiteID."\"><img src=\"/web/images/shoping_cart.gif\" border=0 vspace=0 hspace=0 alt=\"Add to cart\"></a>"; 

                                else
                                    $add_to_cart_str = "<td width=50 valign=\"top\" align=\"center\"  class=\"cart\"><a href=\"page.php?n=13202&amp;no=".$page."&amp;SiteID=".$this->SiteID."&amp;action=insert&amp;pid=".$page_prices[$i]['id']."&amp;moq=".$page_prices[$i]["pcs"]."\"><img src=\"/web/images/shoping_cart.gif\" border=0 vspace=0 hspace=0 alt=\"\"></a>";
                                //calculate the single price of an item
                                if($page_prices[$i]['pcs'] > 1)
                                    $page_prices[$i]['price_value'] = "<span title=\"1x ".$page_prices[$i]['price_value_single'].$currency_str."\">".$page_prices[$i]['price_value']."</span>";
                                
                                $content .= $this->single_price_manager(
                                                $page_prices[$i]['description'], 
                                                $product_availability, 
                                                $page_prices[$i]['code'],
                                                $page_prices[$i]['pcs'],
                                                $page_prices[$i]['qty'], 
                                                $page_prices[$i]['price_value'],
                                                $currencystr,
                                                $add_to_cart_str
                                            );

                                //$content .="<tr><td align=justify valign=\"top\" class=\"cart_item_description\">". $page_prices[$i]['description'] .$product_availability . "<td width=70 align=\"center\" valign=\"top\" class=\"cart_item_code\">".$page_prices[$i]['code']."<td align=\"left\" valign=\"top\" class=\"cart_item_pcs\">x ". $page_prices[$i]['pcs']."<td width=30 align=\"center\" valign=\"top\" class=\"cart_item_qty\"><b>". $page_prices[$i]['qty']."</b>x<td align=\"right\" valign=\"top\" width=30 class=\"cart_item_price\"><b>".$page_prices[$i]['price_value'].$currency_str."</b></td>".$add_to_cart_str ."</tr>";
                                if($stop) break;
                            }
                        }
                        
                $content.="</table>";
            }
                        
            print $content; 
        }


    function single_price_manager($description, $product_availability, $code, $pcs, $qty, $price_value, $currencystr, $add_to_cart_str){
        $prices_data = array();
        $prices_data[] = "<td align=\"justify\" valign=\"top\" class=\"cart_item_description\"> $description $product_availability </td>";

        if($qty < 1){
            $qty = 1;
        }

        if($pcs > 1){
            $prices_data[] = "<td width=\"70\" align=\"center\" valign=\"top\" class=\"cart_item_code\">$code </td>";
            $prices_data[] = "<td align=\"left\" valign=\"top\" class=\"cart_item_pcs\">x $pcs</td> ";
        } else {
            $prices_data[] = "<td colspan=\"2\" width=\"70\" align=\"center\" valign=\"top\" class=\"cart_item_code\">$code </td>";
        }

        $prices_data[] = "<td width=\"30\" align=\"center\" valign=\"top\" class=\"cart_item_qty\"><b>$qty </b></td>";
        $prices_data[] = "<td align=\"right\" valign=\"top\" width=\"30\" class=\"cart_item_price\"><b>$price_value$currencyestr</b> </td>";
        $prices_data[] = "<td>$add_to_cart_str</td>";
        return sprintf("<tr>%s</tr>", implode("", $prices_data));
    }


    //return array of page keywords
    function get_pTags($n=NULL)
    {
        if($n) 
            { $tags = $this->get_pInfo("tags", $n); }
        else $tags = $this->get_pInfo("tags");
        $tags = str_replace(", ", ",", $tags);
        
        $split_tags = explode(",", $tags);
        $split_tags = array_unique($split_tags);
        return $split_tags;
    }

    function print_pTags($link = NULL, $n=NULL)
    {
        $tags_array = $this->get_pTags($n);
        for($i=0; $i<count($tags_array); $i++)
            {
                if($i>0) print ",&nbsp;";
                if($link) print "&nbsp;<a href=\"".$link."&amp;tag=".$tags_array[$i]."\">".$tags_array[$i]."</a>";
                else print $tags_array[$i];
            }
    }

    function get_related_links($n=null)
    {
        $result = array();
        $pLinks = $this->get_pTags($n); 
        foreach($pLinks as $pLink) {
          if(strlen($pLink)>4) {
              $s_result  = $this->do_search($pLink,5); 
              $font_size = rand(9,16); 
              $link_num = rand(0,4); 
              $page_link=$this->get_page($s_result[$link_num]['n']); 
              //echo("<li>&nbsp<a href=".$page_link['page_link']." >".$page_link['Name']."</a>&nbsp"); 
              $result[] = array('link' => $this->get_pLink($s_result[$link_num]['n']), 'name' => $s_result[$link_num]['Name']);  
          }
        } // foreach   
        return $result;

    }
    
    function print_pLinks($n=NULL) {
       $pLinks = $this->get_pTags($n); 
       echo "<ul id=\"page_keywords\">";
       foreach($pLinks as $pLink) {
         if(strlen($pLink)>4) {
             $s_result  = $this->do_search($pLink,5); 
             $font_size = rand(9,16); 
             $link_num = rand(0,4); 
             $page_link=$this->get_page($s_result[$link_num]['n']); 
             //echo("<li>&nbsp<a href=".$page_link['page_link']." >".$page_link['Name']."</a>&nbsp"); 
             echo("<li>&nbsp<a href=".$this->get_pLink($s_result[$link_num]['n'])." >".$s_result[$link_num]['Name']."</a>&nbsp");
         }
       } // foreach   
        echo "</ul>";
     }

//return prev / next page from the same branch
function get_pPrevPage($limit=false)
    {
        $p_parent = $this->get_pParent();
        $pages = $this->get_pSubpages($p_parent, "p.sort_n ", 1, false, $limit);
        $prev = $this->n;
        for($i=0; $i<count($pages); $i++)
            {
                if($pages[$i]['n'] == $this->n && $i>0) 
                    $prev = $pages[$i-1]['n'];
            }
            
        return $prev;
    }   
 
function get_pNextPage($limit=false)
    {
        $p_parent = $this->get_pParent();
        $pages = $this->get_pSubpages($p_parent, "p.sort_n ", 1, false, $limit);
        $next = $this->n;
        for($i=0; $i<count($pages); $i++)
            {
                if($pages[$i]['n'] == $this->n && $i<(count($pages)-1)) 
                    $next = $pages[$i+1]['n'];
            }
            
        return $next;
    }   

//return current page serial number in the branch
function get_pSerialNumber($n, $limit=false)
    {
        $page = $this->get_page($n);
        $p_parent = $page['ParentPage'];
        $pages = $this->get_pSubpages($p_parent, "p.sort_n", $limit=false);
        $sn = 0;
        for($i=0; $i<count($pages); $i++)
            if($n == $pages[$i]['n']) $sn = $i;
        return $sn;
    }

//return current page banner image
function get_pBanner($n=NULL, $default_src = NULL)
    {
        
        $n = $this->validatePageId($n);
        if(site::$sID != $this->get_pInfo("SiteID")) $n = site::$sStartPage;
        
        $pPage = $this->get_pInfo("ParentPage", $n);
        $img_align = $this->get_pInfo("image_allign", $n);
        $p_image = $this->get_pInfo("image_src", $n);
        $banner_src = '';
        
        if($img_align == 21 && $p_image != ""):
            $banner_src = $p_image;
        else:
            while($pPage != 0)
                {
                    $banner_src = $this->get_pBanner($pPage, $default_src);
                    $pPage = $this->get_pInfo("ParentPage", $pPage);
                    if(!empty($banner_src)) break;
                }
        endif;
        //return default banner
        if($banner_src == "") {
            $banner_src = $default_src;
        }
        
        return $banner_src;
    }

//output page banner
function print_pBanner($n=NULL, $width=NULL, $params="", $default_src='')
    {
        if(!$width) $width = $this->get_pInfo("imageWidth");
        $pBanner = $this->get_pBanner($n, $default_src);    
        print "<img src=\"".$this->scheme.$this->get_sURL(0)."/img_preview.php?image_file=".$pBanner."&amp;img_width=".$width."&amp;ratio=strict\" $params alt=\"".$this->get_pInfo("title", $n)."\">";
    }
	
function print_pSlider($n=NULL, $width=NULL, $params="", $default_src)
    {
        if(!$width) $width = $this->get_pInfo("imageWidth");
        $pSlider = $this->get_pBanner($n, $default_src);    
        print "".$pSlider."";
    }

//return all banners from selected site
function get_pBanners($SiteID=NULL, $limit=false)
    {
        if(!$SiteID) $SiteID = $this->SiteID;
        $query = "SELECT p.n,  im.imageName, im.image_src, p.Name, p.title FROM pages p left join images im on p.imageNo = im.imageID WHERE p.image_allign = 21 AND im.image_src != '' AND p.SiteID = '".$SiteID."' ORDER by p.preview DESC". ($limit > 0 ? " LIMIT ".$limit : "");
        $result = $this->db_query($query);
        $pBanners = array();
        while($pBanner = $this->fetch("array", $result))
            $pBanners[] = $pBanner;
        return $pBanners;
    }

/*
function print_pBanners($width, $params="")
    {
        $pBanners = $this->get_pBanners();
        for($i=0; $i<count($pBanners); $i++)
            print "<img src=\"$this->scheme://".$this->get_sURL(0)."/img_preview.php?image_file=".$pBanners[$i]['image_src']."&amp;img_width=".$width."&amp;ratio=strict\" $params alt=\"".$this->get_pInfo("title", $n)."\">";
    }
*/

//return selected comment
function get_comment($comment_id, $filter = 1)
    {
        $result = $this->db_query("SELECT * FROM comments WHERE comment_id = '".$comment_id."' AND $filter");
        $comment = $this->fetch("array", $result);
        return $comment;
    }
    
//return page comments
function get_pComments($n=NULL, $filter="1", $sort_order="comment_id DESC")
    {
        $n = $this->validatePageId($n);
        $result = $this->db_query("SELECT * FROM comments WHERE comment_n = '".$n."' AND comment_status>0 AND $filter ORDER by $sort_order");
        $comments = array();
        while($comment = $this->fetch("array", $result))
            $comments[] = $comment;
        return $comments;
    }   

//return page options
function get_pOptions($n=NULL)
    {
        $pOptions = array();
        $n = $this->validatePageId($n);
        $res = $this->db_query("SELECT * FROM page_options po left join options o on po.option_id = o.option_id WHERE po.n = $n");
        while ($pOption = $this->fetch("array", $res))
            $pOptions[] = $pOption;
        return $pOptions;
            
    }

//return all page templates for current site
function get_pTemplates()
    {
        $templ = $this->_site['Template'];
        $pt_sql = "SELECT * FROM pt_descriptions ptd WHERE ptd.SiteID = '".$this->SiteID."' AND ptd.template_id = '$templ'";
        $pt_result = $this->db_query($pt_sql);
        while($pt = $this->fetch("array", $pt_result))
            $pTemplates[] = $pt;
        return $pTemplates;     
    }
    
    
//return page template
function get_pTemplate($n=NULL, $templ = NULL)
    {
        $n = $this->validatePageId($n);
        if(!$templ) $templ = $this->_site['Template'];
        $pt_sql = "SELECT * FROM page_templates pt left join pt_descriptions ptd on pt.pt_id = ptd.pt_id WHERE pt.n = '$n' AND ptd.template_id = '$templ' LIMIT 1";
        $pt_result = $this->db_query($pt_sql);
        $pt = $this->fetch("array", $pt_result);
        return $pt;
    }

function get_image($im_no, $width=null, $class="modalImg", $ratio="strict", $alt=false){
    $sql = "SELECT * FROM images WHERE imageID = '$im_no'";
    $pt_result = $this->db_query($sql);
    $pt = $this->fetch("object", $pt_result);
    if(!$pt){
        return null;
    }
    if(!$alt){
        $alt = $pt->imageName;
    }

    if(!$width){
        return "<img class=\"$class\" id=\"myImg\" src=\"{$pt->image_src}\" alt=\"{$alt}\">";
    }

    return "<img class=\"$class\" id=\"myImg\" src=\"/img_preview.php?image_file={$pt->image_src}&img_width={$width}&ratio={$ratio}\" alt=\"{$alt}\">";
}

    
//===================
//  ADMIN FUNCTIONS
//===================


//print admin buttons
function admin_buttons()
    {
        if(user::$uID > 0 && $this->SiteID == $this->get_pSiteID() && site::$uWriteLevel >= $this->_page['Level'])
            echo "<div id=\"cms_admin_buttons\"><a href=\"".ADD_LINK."\" class=\"cms-add-page\"><span></span>".CMS_LINK_ADD_PAGE."</a><a href=\"".EDIT_LINK."\" class=\"cms-edit-page\"><span></span>".CMS_LINK_EDIT_PAGE."</a></div>";
    }

//add page
function add($pParent, $pName)   // praveno e zaradi EmoAuto - ne se polzva 
    {       
        global $pText, $pImage, $pSubPagesColls, $pShowLinks, $pMakeLinks, $pImageWidth, $pImageAlign, $pTags;
        
        if($pImage['name'] != "")
            $imageNo = $this->insert_file($pImage, "", $pName);
        
        $add_sql = "INSERT INTO pages SET ParentPage = '".$pParent."', Name = '".$pName."', SiteID = '".site::$sID."', date_added = now(), date_modified = now(), textStr = '".$pText."', show_link = '".$pShowLinks."', show_link_cols = '".$pSubPagesColls."', make_links = '".$pMakeLinks."', imageNo = '".$imageNo."', imageWidth = '".$pImageWidth."', image_allign = '".$pImageAlign."', tags = '".$pTags."'";
        // author is not added - da se dobavi 
    
        
        if(isset($pParent) && isset($pName))
            {
                if($this->db_query($add_sql))
                    return $this->last_insert_id();
                else return false;
            }
        else return false;
        
        //return $add_sql;
        //return $pName . "<br>" . $pText;
    }

//delete page and all subpages
// del=0 simulate only; del=1 delete pages; del=-1 move to trash; del=2 empty pages (text, pics, php code etc.)
function delete_page($n, $del=0)
    {  
        if($n == 0) return false; 
        //$max_pages_del=9; 
        $result = $this->db_query("SELECT * FROM pages WHERE ParentPage='$n' AND status != 0"); 
        $content.="<ul>";
        $page_row = $this->get_page($n);
        $content.= "<li>".$page_row['Name']."</li>";
        //if(!isset($pages_affected)) $pages_affected = 0;
        if($this->count($result)>0)
            {
                while($row=$this->fetch("object", $result))
                    {
                        $this->delete_page($row->n, $del);
                        $pages_affected++;
                    }
            }
                
        //could not delete locked pages
        if($this->get_pStatus($n)==0)
            $del = 0;
        
        $result2 = $this->db_query("SELECT * FROM pages WHERE ParentPage = '$n' AND status = 0");           
        
        if($del==-1)
            {   
                //if no locked subpages, move to trash current page
                if($this->count($result2)==0) 
                {
                    $this->db_query("UPDATE pages SET status = -1 WHERE n = '$n' LIMIT 1 "); 
                    $pages_affected++;
                }
            }
        else        
        if($del==1)
            {   
              //if no locked subpages, delete current page
              if($this->count($result2)==0)
                {   
                    $lang_id = $this->lang_id;
                    $this->db_query("DELETE FROM pages WHERE n = '$n' LIMIT 1 "); 
                    $this->db_query("DELETE FROM pages_text WHERE n = '$n' and lang_id=$lang_id LIMIT 1 "); 
                    $this->db_query("UPDATE pages SET an='0' WHERE pages.an='$n'  ");
                    //remove page from all groups
                    $this->db_query("DELETE FROM pages_to_groups WHERE n='$n'");
                    $pages_affected++;
                }
            }
        else    
        if($del==2)
            {
                $this->db_query("UPDATE pages SET an = 0, tags = '', PHPvars = '',  textStr = '', PHPcode = '', toplink = 0, show_link = 1, show_group_id = 0, show_link_order = 'sort_n ASC', show_link_cols = 1, make_links = 1, PageURL = '',  imageNo = 0, imageWidth = '200', image_allign = 1, date_modified = 'now()' WHERE n = '$n' LIMIT 1 "); 
                $pages_affected++;
            }

        $content.="</ul>";
        return $pages_affected; 
    }

//restore pages from trash
function restore_page($n)
    {
        if(!isset($n)) return 0;
        else
            {
                if($this->db_query("UPDATE pages SET status = 1 WHERE n = '".$n."' LIMIT 1"))
                    return 1;
                else return 0;
            }
    }

//same is defined in lib_site   
function print_search($s_results){
if (count($s_results)>0) {
     echo "<ul class=\"search_results\">";
     foreach($s_results as $res) {
      $str_img = ""; 
     $width = 150; 
      //$res['textStr'] = substr($s_results['textStr'], 0, 100); 
     // echo "<li><b><a href=\"http://".$this->get_sURL()."/".$this->get_pLink($res['n'])."\" title=".$res['title'].">".$res['Name']."</a></b><p>".$res['textStr']."<br>";
    if(strlen($res['image_src'])>5)  $str_img = "<img src=\"".$this->scheme.$this->get_sURL()."/img_preview.php?image_file=".$res["image_src"]."&amp;img_width=".$width."\" align=\"right\" alt=\"".htmlspecialchars($res["imageName"], ENT_QUOTES)."\">";
      echo "<li>$str_img<p>".$this->get_pText($res['n'])."...</p><b><a href=\"".$this->get_pLink($res['n'])."\" title=".$res['title'].">".$res['Name']."</a></b><br>"; 
      echo $this->print_pNavigation("short", $res['n']); 
      echo "...</p><br>";   
        if($this->_site['site_status']>0) {
          echo "<p>".$this->get_seo_etiketi_link($this->get_top_word($res['title'].",".$res['Name'].",".$res['textStr']))."</p><br>"; 
          }   
     }
     echo "</ul>";
  } else {
  }
}
    

//upload selected file
function upload_file($file, $file_name = "", $valid_type=true, $max_upload_size = 1000000)
    {
        if($file_name == "")
            $file_name = $file['name'];
        $file_tmp_name = $file['tmp_name'];
        $file_type = $file['type'];
        $file_size = $file['size'];
        $dir = $this->get_sImageDir()."/";
        
        //invalid filetypes
        //$invalid_types = array('application/octet-stream', 'video/x-ms-asf');
        $invalid_types = array('');
        
        //if $valid_type=true upload only theese $valid_types
        $valid_types = array('image/pjpeg', 'image/jpeg', 'image/x-png', 'image/gif', 'image/tiff','text/plain', 'application/x-shockwave-flash');
        
        //extra valid types, when $valid_type=false
        $extra_valid_types = array('audio/x-ms-wma', 'audio/mpeg', 'application/pdf', 'application/x-zip-compressed', 'application/x-gzip', 'application/vnd.sun.xml.calc', 'application/msword', 'application/octet-stream', 'video/x-ms-asf');
                
        if(in_array($file_type, $invalid_types))
            return 301;
        else
        if($valid_type && !in_array($file_type, $valid_types))
            return 301;
        else
        if(!$valid_type && !(in_array($file_type, $valid_types) || in_array($file_type, $extra_valid_types)))
            return 301;
        if($file_size > $max_upload_size)
            return 302;
        else
        if(!empty($file_name))
            if (is_uploaded_file($file_tmp_name)) 
                    if(move_uploaded_file($file_tmp_name, $dir.$file_name))
                        return 101;
                    else return 102;
            else return 103;
        else return 201;

    }   

//uploaded file and insert into DB
function insert_file($file, $file_name="", $file_title="")
    {
        if($file_name == "")
            $file_name = $file['name'];
        $dir = $this->get_sImageDir()."/";
        $dir_id = $this->get_sImageDir("ID");
        
        //search for file with the same name
        list($filename, $extension) = preg_split("\.", $file_name, 2); 
        $filename_const=$filename; 
        $i=1;
        while(file_exists("$dir$filename.$extension"))
            {
                $filename = "$filename_const($i)";
                $i++;  
            }
        
        $fname = "$filename.$extension"; 
        $fname = str_replace(" ", "_", $fname);
        //return $fname;
        $status = $this->upload_file($file, $fname);
        /*
        switch($status)
            {
                case 101: { echo "File uploaded...[ ".$dir." ]"; break;}
                case 102: { echo "Error coping...".$file_name; break;}
                case 102: { echo "Error uploading...".$file_tmp_name; break;}
                case 201: { echo "Select files to upload..."; break;}
                case 301: { echo "Invalid filetype..."; break;}
                case 302: { echo "Invalid filesize..."; break;}
                default  : { echo "unknown error"; break;}
            }
        */
        $imageID = 0;
        if($status == 101)
            {
                 $sql = "INSERT INTO images SET imageName = '".$file_title."', image_src =  '".$dir.$fname."', image_dir = '".$dir_id."'";
                 $this->db_query($sql);
                 $imageID = $this->last_insert_id();
                 //echo "<b> File saved to database <b>$imageID</b>! </b>";
                 //echo $sql;
            }
        
        return $imageID;        
    }

    function utf8_to_cp1251($s)
    {
        if($this->char_set == 1) {
            $out = $s;
        }
        if($this->char_set == 2) {
            for ($c=0;$c<strlen($s);$c++)
            {
                $i=ord($s[$c]);
                if ($i<=127) $out.=$s[$c];
                if ($byte2){
                    $new_c2=($c1&3)*64+($i&63);
                    $new_c1=($c1>>2)&5;
                    $new_i=$new_c1*256+$new_c2;
                    if ($new_i==1025){
                        $out_i=168;
                    } else {
                        if ($new_i==1105){
                            $out_i=184;
                        } else {
                            $out_i=$new_i-848;
                        }
                    }
                    $out.=chr($out_i);
                    $byte2=false;
                }
                if (($i>>5)==6) {
                    $c1=$i;
                    $byte2=true;
                }
            }
        }
        return $out;
    }

    
    //return user access statistic or message
    function user_message($limit=false)
        {
            global $templ;
            
            $uName          = $this->get_uName();
            $uLevel         = $this->get_uLevel();
            $uEmail         = $this->get_uEmail();
            $uReadLevel     = site::$uReadLevel;
            $uWriteLevel    = site::$uWriteLevel;
            $uslID          = $this->get_uslID();
            $options = "";
            
            //count pages in trash
            $pages_in_trash = count($this->get_pSubpages(0, "p.sort_n ASC", "p.status = -1", false, $limit));
            if($pages_in_trash >0){
                $options.= "<a class=\"option border\" href=\"".$this->get_pLink(144)."\" title=\"".CMS_TITLE_PAGES_IN_TRASH."\"><i class=\"number\">$pages_in_trash</i> ".CMS_TITLE_TRASH."</a>";
            }
            
            //count user tasks (1: only priority 1)
            $user_tasks = $this->count_user_tasks($this->get_uID(), "", "(priority=1 OR (completed = '0' AND NOW() > (start_date + INTERVAL period DAY) ))");
            if($user_tasks >0){
                $options.= "<a class=\"option border\" href=\"page.php?n=85548&SiteID=1\" title=\"user tasks\"><i class=\"number\">$user_tasks</i>".CMS_TITLE_TASKS."</a>";
            }

            //count only new site requests from the last 48 hours for all user sites, with active notification
            $all_site_requests = 0;
            $uSites = $this->get_uSites($this->get_uID(), "sa.notifications>0");
            for($i=0; $i<count($uSites); $i++) {
                $site_requests = count($this->get_sRequests($uSites[$i]['SitesID'],0, "f.Data>DATE_SUB(CURDATE(),INTERVAL 2 DAY)"));
                if($site_requests >0) {
                    $all_site_requests++;
                    $options.= "<a class=\"option border\" href=\"page.php?n=6031&SiteID=".$uSites[$i]['SitesID']."\" title=\"Site request for th? last 12 hours\"><i class=\"number\">$site_requests</i>".$uSites[$i]['Name']."</a>";          
                }   
            }
            //count user reprint docs
            // UPDATE docs SET reData='0000-00-00' WHERE reData!='0000-00-00' AND reData<'2011-01-01'  
            $reprint_query = "SELECT * FROM docs d, docTypes dt, Firmi f WHERE  d.ClientID=f.ID AND d.reData != '0000-00-00' AND (d.reData <= DATE_ADD(CURDATE(), INTERVAL 30 DAY)) AND d.docID=dt.docTypeID  AND dt.docTypeID>=0 AND f.sl = '$uslID' ORDER BY d.reData ASC";
            $result = $this->db_query($reprint_query);          
            $reprint_docs = $this->count($result);
            if($reprint_docs >0){
                $options.= "<a class=\"option border\" href=\"".$this->get_pLink(11427)."&slID=".$uslID."\" title=\"repritn documents\"><i class=\"number\" >$reprint_docs</i>".CMS_TITLE_REPRINT_DOCS."</a>";          
            }
            
            
            /*count expiring SEO groups*/
            $seo_groups = $this->get_seo_groups($this->get_uID(), "eg_date_expire <= DATE_ADD(CURDATE(), INTERVAL 30 DAY) AND eg_date_expire > DATE_SUB(CURDATE(),INTERVAL 30 DAY) ORDER by eg_date_expire DESC");
            if(count($seo_groups) >0){
                $options.= "<a class=\"option border\" href=\"".$this->get_pLink(99173)."&amp;groups=menu\" title=\"Expiring SEO groups\"><i class=\"number\">".count($seo_groups)."</i>".CMS_TITLE_EXPIRING_SEO."</a>";
            }
            
            /*count site comments*/
            $comments_query = "SELECT * FROM comments c, pages p WHERE c.comment_status=0 AND p.SiteID = '".$this->_site['SitesID']."' AND p.n = c.comment_n";
            $result = $this->db_query($comments_query);
            $count_site_comments = $this->count($result);
            if($count_site_comments > 0){
                $options.= "<a class=\"option border\" href=\"".$this->get_pLink(127362)."&amp;comment_status=0\" title=\"Waiting comments\"><i class=\"number\">".$count_site_comments."</i>".CMS_TITLE_WAITING_COMMENTS."</a>";
            }
            
            $debug='';
            $text='';
            $notifications='';
            $test=array();
            /* if($_SESSION["user"]->ID=="1373"){ */
                $loggedUser = $_SESSION["user"];
            if(isset($loggedUser->notifications) and $notifications = $loggedUser->notifications) {
                $notifications = $loggedUser->notifications;
            }
                /* if($notifications>0){ */
                    $query="SELECT * FROM chats LEFT JOIN forms ON chats.formID=forms.ID WHERE forms.SiteID = '".$this->_site['SitesID']."' AND ftype = 0";
                    $result = $this->db_query($query);
                    $rows=array();
                    while($row = $this->fetch("array", $result)){
                        $rows[] = $row;
                    }
                    $j=0;
                    foreach($rows as $row){
                        $debug = stripslashes($row["chatlog"]);
                        $j++;
                        $Rposition=$j*8;
                        $siteid = $row["SiteID"];
                        $text[] = "
                        <div class=\"chatlog\" style=\"right:".$Rposition."%;\">
                            <div class=\"chathead\">
                                <div class=\"chattitle\">
                                    ��� id->".$siteid."
                                </div>
                                <div class=\"maxmimizer".$j."\">
                                &uarr;
                                </div>
                                <div class=\"clearboth\"></div>
                            </div>
                            <div class=\"chatpanel".$j."\">
                                <div class=\"chatmsgs".$j."\">".$debug."</div>
                                <hr/>
                                <div class=\"chatform\">
                                    <input type=\"text\" name=\"adminmessage\" id=\"adminmessage".$j."\"/>
                                    <input type=\"hidden\" name=\"formID\" id=\"formID".$j."\" value=\"".$row['formID']."\"/>
                                    <input type=\"hidden\" name=\"EMail\" id=\"EMail".$j."\" value=\"".$_SESSION["user"]->EMail."\"/>
                                    <input type=\"hidden\" name=\"Name\" id=\"Name".$j."\" value=\"".$_SESSION["user"]->Name."\"/>
                                    <button class=\"btn\" id=\"sendadmmsg".$j."\">&raquo;</button>
                                </div>
                                <div class=\"clearboth\"></div>
                            </div>
                        </div>
                        <style>
                        .chatlog{
                            position:fixed;
                            bottom:0px;
                            right:10%;
                            width:200px;
                        }
                        .chathead{
                            padding:5px;
                            background:royalblue;
                            color:white;
                            font-weight:bold;
                            border-top-left-radius:5px;
                            border-top-right-radius:5px;
                        }
                        .row{
                            margin:0px;
                            
                        }
                        .chatpanel".$j."{
                            padding:5px;
                            background:white;
                            border-left: 1px solid #ededed;
                            border-right: 1px solid #ededed;
                            display:none;
                        }
                        #adminmessage".$j."{
                            width:70%;
                            float:left;
                        }
                        #sendadmmsg".$j."{
                            width:20%;
                            float:left;
                        }
                        .clearboth{
                            clear:both;
                        }
                        .chattitle{
                            float:left;
                        }
                        .minimizer".$j.", .maxmimizer".$j."{
                            float:right;
                            font-weight:bold;
                            cursor:pointer;
                            font-size:24px;
                        }
                        .chatmsgs".$j."{
                            max-height:350px;
                            overflow-y:scroll;
                        }
                        </style>
                        <script tpe=\"text/JavaScript\">
                            jQuery(document).ready(function(){
                                function getem(){
                                    var getthem=jQuery('#formID".$j."').val();
                                    jQuery.ajax({
                                      url:'".$this->scheme.$this->_site['primary_url']."/web/forms/chat/lib_admin_message.php',
                                      type:'post',
                                      data:{getthem:getthem},
                                      contentType:'application/x-www-form-urlencoded; charset=Windows-1251',
                                      dataType: 'html',
                                      success: function(result){
                                        jQuery('.chatmsgs".$j."').html(result);
                                      },
                                      error: function (error) {
                                          alert('error; ' + eval(error));
                                      }
                                    })
                                }
                                
                                window.setInterval(function(){
                                    getem();
                                }, 2000);
                                
                                jQuery('#sendadmmsg".$j."').on('click', function(){
                                    var message = jQuery('#adminmessage".$j."').val();
                                    var formID = jQuery('#formID".$j."').val();
                                    var EMail = jQuery('#EMail".$j."').val();
                                    var Name = jQuery('#Name".$j."').val();
                                    jQuery.ajax({
                                        url:'".$this->scheme.$this->_site['primary_url']."/web/forms/chat/lib_admin_message.php',
                                        type:'post',
                                        contentType:'application/x-www-form-urlencoded; charset=Windows-1251',
                                        datatype: 'html',
                                        data:{message:message, formID:formID, EMail:EMail, Name:Name},
                                        success: function(result){
                                            jQuery('.chatmsgs".$j."').html(result);
                                            jQuery('#adminmessage".$j."').attr('value', '');
                                        }
                                        
                                    })
                                    
                                });
                                jQuery(document).on('click', '.minimizer".$j."', function(){
                                    jQuery('.chatpanel".$j."').hide('fast');
                                    jQuery('.minimizer".$j."').removeClass('minimizer".$j."').addClass('maxmimizer".$j."').html('&uarr;');
                                });
                                jQuery(document).on('click', '.maxmimizer".$j."', function(){
                                    jQuery('.chatpanel".$j."').show('fast');
                                    jQuery('.maxmimizer".$j."').removeClass('maxmimizer".$j."').addClass('minimizer".$j."').html('&minus;');
                                });
                            });
                        </script>
                        ";
                        $j++;
                    }
                /* } */
            /* } */
            $text = serialize($text);
            $user_message = null;
            if($uLevel > 0 && $templ!=28 && ($pages_in_trash > 0 || $user_tasks > 0 || $all_site_requests > 0 || $reprint_docs > 0 || $site_hostings >0 || $calculation_docs >0 || $count_site_comments > 0)){
                return "<table id=\"cms_user_message\" class=\"border\"><tr><td><a href=\"#\" onClick=\"javascript: show_hide('cms_user_message')\" class=\"number\">X</a>".$user_message.$options."<tr><td>"."</td></tr></table>";
            }
            else {
                return NULL;
            }
        }
        
    function add_shortcode($shortcode, $func, $shortcode_attr = null) {
        add_shortcode($shortcode, $func);
        if(is_callable($func))
            $this->shortcodes[] = array($shortcode => $func);
    }

    /**
     * @param $text
     * @return mixed
     */

    // MAIN shortcode replacement function = do_shortcode

    function replace_shortcodes($text){
        if($this->_page['n'] == 132) {
            return $text;
        }
        return do_shortcode($text);
    }

    protected function shortcode_exists( $tag ) {
        return array_key_exists( $tag, $this->shortcodes );
    }

    protected function has_shortcode($content, $tag)
    {
        if ( false === strpos( $content, '[' ) ) {
            return false;
        }
        if ( $this->shortcode_exists( $tag ) ) {
            preg_match_all( '/' . $this->get_shortcode_regex() . '/', $content, $matches, PREG_SET_ORDER );
            if ( empty( $matches ) )
                return false;
            foreach ( $matches as $shortcode ) {
                if ( $tag === $shortcode[2] ) {
                    return true;
                } elseif ( ! empty( $shortcode[5] ) && $this->has_shortcode( $shortcode[5], $tag ) ) {
                    return true;
                }
            }
        }
        return false;

    }


    function get_shortcode_regex() {
        $tagnames = array_keys($this->shortcodes);
        $tagregexp = join( '|', array_map('preg_quote', $tagnames) );
        // WARNING! Do not change this regex without changing do_shortcode_tag() and strip_shortcode_tag()
        // Also, see shortcode_unautop() and shortcode.js.
        return
            '\\['                              // Opening bracket
            . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
            . "($tagregexp)"                     // 2: Shortcode name
            . '(?![\\w-])'                       // Not followed by word character or hyphen
            . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
            .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
            .     '(?:'
            .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
            .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
            .     ')*?'
            . ')'
            . '(?:'
            .     '(\\/)'                        // 4: Self closing tag ...
            .     '\\]'                          // ... and closing bracket
            . '|'
            .     '\\]'                          // Closing bracket
            .     '(?:'
            .         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
            .             '[^\\[]*+'             // Not an opening bracket
            .             '(?:'
            .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
            .                 '[^\\[]*+'         // Not an opening bracket
            .             ')*+'
            .         ')'
            .         '\\[\\/\\2\\]'             // Closing shortcode tag
            .     ')?'
            . ')'
            . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
    }

    function do_shortcode_tag( $m ) {

        // allow [[foo]] syntax for escaping a tag
        if ( $m[1] == '[' && $m[6] == ']' ) {
            return substr($m[0], 1, -1);
        }
        $tag = $m[2];
        $attr = $this->shortcode_parse_atts( $m[3] );
        if ( isset( $m[5] ) ) {
            // enclosing tag - extra parameter
            return $m[1] . call_user_func( $this->shortcodes[$tag], $attr, $m[5], $tag ) . $m[6];
        } else {
            // self-closing tag
            return $m[1] . call_user_func( $this->shortcodes[$tag], $attr, null,  $tag ) . $m[6];
        }
    }

    function shortcode_atts( $pairs, $atts, $shortcode = '' ) {
        $atts = (array)$atts;
        $out = array();
        foreach($pairs as $name => $default) {
            if ( array_key_exists($name, $atts) )
                $out[$name] = $atts[$name];
            else
                $out[$name] = $default;
        }
        /**
         * Filter a shortcode's default attributes.
         *
         * If the third parameter of the shortcode_atts() function is present then this filter is available.
         * The third parameter, $shortcode, is the name of the shortcode.
         *
         * @since 3.6.0
         *
         * @param array $out The output array of shortcode attributes.
         * @param array $pairs The supported attributes and their defaults.
         * @param array $atts The user defined shortcode attributes.
         */
        //if ( $shortcode )
        //    $out = apply_filters( "shortcode_atts_{$shortcode}", $out, $pairs, $atts );
        return $out;
    }


    function shortcode_parse_atts($text) {

        $pattern = '/(\w+)\s*=\s*"([^"]*)"(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/';
        $text = preg_replace("/[\x{00a0}\x{200b}]+/u", " ", $text);
        if ( preg_match_all($pattern, $text, $match, PREG_SET_ORDER) ) {
            foreach ($match as $m) {
                if (!empty($m[1]))
                    $atts[strtolower($m[1])] = stripcslashes($m[2]);
                elseif (!empty($m[3]))
                    $atts[strtolower($m[3])] = stripcslashes($m[4]);
                elseif (!empty($m[5]))
                    $atts[strtolower($m[5])] = stripcslashes($m[6]);
                elseif (isset($m[7]) && strlen($m[7]))
                    $atts[] = stripcslashes($m[7]);
                elseif (isset($m[8]))
                    $atts[] = stripcslashes($m[8]);
            }
        } else {
            $atts = ltrim($text);
        }
        return $atts;
    }



    // end shortodes


    function print_timthumb($src="/web/images/nopicture.jpg", $class="", $w="100", $h="100", $q="100", $a="c", $zc="1", $f='0', $cc="ffffff", $ct="0"){

        //src   source          url to image

        //w     width           the width to resize to

        //h     height          the height to resize to

        //q     quality         0 - 100

        //a     alignment       c, t, l, r, b, tl, tr, bl, br
        // c : position in the center (this is the default)
        // t : align top
        // tr : align top right
        // tl : align top left
        // b : align bottom
        // br : align bottom right
        // bl : align bottom left
        // l : align left
        // r : align right



        //zc    zoom/crop       0, 1, 2, 3
        // 0    Resize to Fit specified dimensions (no cropping)
        // 1    Crop and resize to best fit the dimensions (default)
        // 2    Resize proportionally to fit entire image into specified dimensions, and add borders if required
        // 3    Resize proportionally adjusting size of scaled image so there are no borders gaps

        //f     filters         too many to mention
        // 1 = Negate - Invert colours
        // 2 = Grayscale - turn the image into shades of grey
        // 3 = Brightness - Adjust brightness of image. Requires 1 argument to specify the amount of brightness to add. Values can be negative to make the image darker.
        // 4 = Contrast - Adjust contrast of image. Requires 1 argument to specify the amount of contrast to apply. Values greater than 0 will reduce the contrast and less than 0 will increase the contrast.
        // 5 = Colorize/ Tint - Apply a colour wash to the image. Requires the most parameters of all filters. The arguments are RGBA
        // 6 = Edge Detect - Detect the edges on an image
        // 7 = Emboss - Emboss the image (give it a kind of depth), can look nice when combined with the colorize filter above.
        // 8 = Gaussian Blur - blur the image, unfortunately you can't specify the amount, but you can apply the same filter multiple times (as shown in the demos)
        // 9 = Selective Blur - a different type of blur. Not sure what the difference is, but this blur is less strong than the Gaussian blur.
        // 10 = Mean Removal - Uses mean removal to create a "sketchy" effect.
        // 11 = Smooth - Makes the image smoother.

        //example -> "f=1" as well as "f=1|2" (for multiple filters) or look at this url $this->scheme://www.binarymoon.co.uk/demo/timthumb-filters/
        //cc    canvas          hexadecimal colour value (#ffffff)
        //      colour

        //ct    canvas          true (1)
        //      transparency
        $siteurl=$this->_site['primary_url'];
        echo "<img src='/web/timthumb/timthumb.php?src=".$src."&w=".$w."&h=".$h."&q=".$q."&a=".$a."&zc=".$zc."&f=".$f."&cc=".$cc."&ct=".$ct."' class='".$class."'/>";

    }

    public function setDatabase($db)
    {
        $this->db = $db;
    }

    public function insertIntoForms($Name, $EMail, $TextForm, $Zapitvane, $ftype="")
    {
        if(!$this->db){
            require_once __DIR__.'/Database.class.php';
            try{
                $this->db = new Database();
            } catch (Exception $e){
                return false;
            } catch (PDOException $e){
                return false;
            }
        }
        $user_id =0;
        if($this->_user['ID']){
            $user_id = $this->_user['ID'];
        }

        $today = date("Y-m-d H:i:")."00";
        $stmt = $this->db->prepare("
            INSERT INTO forms
             ( ftype, userID, Name, EMail, n, TextForm, SiteID, Zapitvane, ip, Data)
            VALUES
             (:ftype, :userID, :Name, :EMail, :n, :TextForm, :SiteID,:Zapitvane, :ip, :Data)");
        $stmt->bindValue(":ftype", $ftype);
        $stmt->bindValue(":Name", $Name);
        $stmt->bindValue(":EMail", $EMail);
        $stmt->bindValue(":userID", $user_id);
        $stmt->bindValue(":n", $this->_page['n']);
        $stmt->bindValue(":TextForm", $TextForm);
        $stmt->bindValue(":SiteID", $this->_site['SitesID']);
        $stmt->bindValue(":Zapitvane", $Zapitvane);
        $stmt->bindValue(":ip", $this->user_ip);
        $stmt->bindValue(":Data", $today);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    protected function getAllPages($SiteID)
    {
        $select = "
            SELECT 
            n, ParentPage, Name,title 
            FROM 
            pages 
            WHERE SiteID=:SiteID
            AND SecLevel <= :AccessLevel
            ORDER BY 
            sort_n, Name ";
        $access_level = (int) $this->_user['AccessLevel'];
        if(!isset($this->db)) {
            return array();
        }
        $stmt = $this->db->prepare($select);
        $stmt->bindValue(":SiteID", $SiteID);
        $stmt->bindValue(":AccessLevel", $access_level);
        $stmt->execute();
        $pages =array();
        while($row=$stmt->fetch(\PDO::FETCH_ASSOC)){
            $pages[] = $row;
        }
        return $pages;
    }

    public function formatMenuData($SiteID)
    {
        $pages = $this->getAllPages($SiteID);
        // prepare special array with parent-child relations 
        $menuData = array( 
            'items' => array(), 
            'parents' => array() 
        ); 
        foreach($pages as $menuItem){ 
            $menuData['items'][$menuItem['n']] = $menuItem; 
            $menuData['parents'][$menuItem['ParentPage']][] = $menuItem['n']; 
        } 
        return $menuData;
    }

    public function customFormat($array_of_pages)
    {
        foreach($array_of_pages as $menuItem){ 
            $menuData['items'][$menuItem['n']] = $menuItem; 
            $menuData['parents'][$menuItem['ParentPage']][] = $menuItem['n']; 
        } 
        return $menuData;
    }
    /*
    
        $params = array( "ul" => class na ul,
                         "li" => class i dr neshta,
                         "a" => class i dr neshta
                         )
    */

    public function buildMenu($parentId, $menuData, $dept, $params=array(), $children=array(), $counter=0)
    {
        $html = '';
        if($counter>0 && is_array($children)){
            $params = $children;
        }
        if (isset($menuData['parents'][$parentId])) {
            $html = '<ul '.$params['ul'][0]. ' ' . $params['ul'][1].'>';
            foreach($menuData['parents'][$parentId] as $itemId) {
                $a_class = '';
                if(count($this->get_pSubPages($itemId))>0){
                    $a_class= $params['a'][0].' data-activates="'. $itemId.'"';
                }
                $html.= '<li '.$params['li'][0]. ' ' .$params['li'][1].' >';
                $html.= '<a '.$a_class.' href="'.$this->get_pLink($itemId).'">';
                $html.= $params['a'][1] .' '. $menuData['items'][$itemId]['Name'].'</a>';
                if ($dept > 1) {
                    $html.= $this->buildMenu($itemId, $menuData, $dept - 1, $params, $children, $counter + 1);
                }
                $html.= '</li>';
            }

            $html.= '</ul>';
        }
        return $html;
    }

    public function get_subGroups($group_id)
    {
        $sql = 'SELECT *
                FROM `groups`
                WHERE `parent_id` = :group_id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":group_id", $group_id);
        $stmt->execute();
        $tmp = array();
        while($res = $stmt->fetch(PDO::FETCH_ASSOC)){
            $tmp[] = $res;
        }
        return $tmp;
    }

    public function getParentPage($n,  $sort_order, $user_access_level)
    {
        $query = "
            SELECT * FROM pages p 
            LEFT JOIN images im ON p.imageNo = im.imageID
            WHERE p.ParentPage = :parent_page
            AND p.SecLevel <= :user_level
            ORDER by :sort_order";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":parent_page", $n);
        $stmt->bindValue(":user_level", $user_access_level);
        $stmt->bindValue(":sort_order", $sort_order);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $tmp[$row['n']] = $row;
        }
        return $tmp;
    }

    public function getRotatorBanners($rotator_id){
        $sql = "
            SELECT * 
            FROM banner_rotators br
            LEFT JOIN banrot b ON br.rid = b.rid
            JOIN banners bs ON b.bid = bs.banner_id
            WHERE br.rid =:rid
            AND br.rotator_status >0
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":rid", $rotator_id);
        $stmt->execute();
        $tmp = array();
        while($row = $stmt->fetch(PDO::FETCH_OBJ)){
            $tmp[] = $row;
        }
        return $tmp;
    }


    public function getSubContent($n,$link, $sort)
    {
        $sort_order = array(
            'ASC' => 'p.n ASC',
            'DESC' => 'p.n DESC',
            'p.Name DESC' => 'p.Name DESC',
            'p.Name ASC' => 'p.Name ASC',
            "p.date_added ASC" => "p.date_added ASC",
            "p.date_added DESC" => "p.date_added DESC",
            "p.toplink ASC" => "p.toplink ASC",
            "p.toplink DESC" => "p.toplink DESC",
            "RAND()" => "RAND()",
        );
        $subpages = $this->getParentPage(
                $n,
                $sort_order[$sort], 
                0);
        $show_link = array(
            0  => array(),
            1  => array('name'),
            2  => array('name','content','img'),
            3  => array('name','img'),
            4  => array('name','content2','img'),
            5  => array('content','img'),
            6  => array('content'),
            7  => array('img','content2'),
            8  => array('content2'),
            9  => array('img'),
            10 => array('name','content'),
            11 => array('name','content2'),
            13 => array('name','img','content2'),
            14 => array('content2', 'img'),
            15 => array('name', 'content2'),
        );

        $make_links = array(
            0 => array(),
            1 => array('name', 'content', 'img', 'content2'),
            2 => array('title'),
            3 => array('img'),
            4 => array('name', 'content', 'img', 'content2'),
        );
        $tmp = array();
        $data = array();
        $sub_content = array();
        $show_link = $show_link[$link];
        foreach($subpages as $page){
            $link = $this->get_pLink($page['n']);
            $data['name'] = array('str'=>$page['Name'], 'link'=> '');
            $data['content'] = array('str' => $page['textStr'], 'link' => '');
            $data['img'] = array('str' => $this->get_pImage($page['n']), 'link' => '');
            $text = str_replace('</p>', '', $page['textStr']);
            $data['content2'] = array('str'=>$page['textStr'], 'link' => '');

            foreach($make_links[$page['make_links']] as $content){
                $data[$content]['link'] = $link;
            }
            foreach($show_link as $k){
                $tmp[$k] = $data[$k];
            }
            $tmp['p_link'] = $link;
            $sub_content[] = $tmp;
        }
        return $sub_content;
    }

    public function __destruct(){
        $this->cleanup();
    }

    public function cleanup() {
        //cleanup everything from attributes
        foreach (get_class_vars(__CLASS__) as $clsVar => $_) {
            $prop = new ReflectionProperty(__CLASS__, $clsVar);
            if(!$prop->isStatic()) {
                unset($this->$clsVar);
            }
        }
    }

        

    
} // end class page
?>
