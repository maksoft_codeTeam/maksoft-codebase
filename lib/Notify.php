<?php


class Notify
{
    protected $app_id;
    protected $rest_api_key; # NzAzZDU3MTgtOTcwZS00OTMzLWIxZGUtNzY1NzExMjBmMmY0
    public function __construct($app_id, $rest_api_key){
        $this->appId = $app_id;
        $this->rest_api_key = $rest_api_key;
        $this->baseUrl = 'https://onesignal.com/api/v1/';
    }

    protected function get($url, $method="GET", $fields=array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_ENCODING , "gzip");
        curl_setopt($ch, CURLOPT_VERBOSE,1);
        if($method === "POST"){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields); 
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST,0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 20);
        $data = curl_exec($ch);
        $error = error($ch);
        if ($error > 0) {
            throw new CurlException("Error Processing Request cURL Error No: ($error):", 1);
        }
        curl_close($ch);
        return json_decode($data);
    }

    protected function post($url, $fields)
    {
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
        curl_setopt($ch, CURLOPT_HEADER, FALSE); 
        curl_setopt($ch, CURLOPT_POST, TRUE); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
        $response = curl_exec($ch); 
        $error = error($ch);
        if ($error > 0) {
            throw new CurlException("Error Processing Request cURL Error No: ($error):", 1);
        }
        curl_close($ch); 
        return $response;
    }

    public function getAppId()
    {
        return $this->get($this->baseUrl.'apps/'.$this->appId);
    }

    public function getDevices()
    {
        return $this->get($this->baseUrl.'players?app_id='.$this->appId);
    }

    public function getDevice($id)
    {
        return $this->get($this->baseUrl.'players/'.$id.'?app_id='.$this->appId);
    }

    /*
        $fields = array( 
        'app_id' => "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx", 
        'identifier' => "ce777617da7f548fe7a9ab6febb56cf39fba6d382000c0395666288d961ee566", 
        'language' => "en", 
        'timezone' => "-28800", 
        'game_version' => "1.0", 
        'device_os' => "9.1.3", 
        'device_type' => "0", 
        'device_model' => "iPhone 8,2", 
        'tags' => array("foo" => "bar") 
        ); 
    */
    public function registerDevice($fields)
    {
        $fields['app_id'] = $this->appId;
        $fields = json_encode($fields);
        $url = 'https://onesignal.com/api/v1/players'; 


    }

    public function sendMessage($msg, $segments=array('All'), $url=false, $filters=false, $lang='en')
    {
        $content = array(
          $lang => iconv('cp1251', 'utf8', $msg)
          );

        $fields = array(
          'app_id' => $this->appId,
          'included_segments' => $segments,
          'data' => array("1097" => "1"),
          'contents' => $content
        );
        if($filters){
            $fields['filters'] = $filters;
        }
        if($url){
            $fields['url'] = $url;
        }

        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                               'Authorization: Basic '.$this->rest_api_key));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;

        # $response = sendMessage();
        # $return["allresponses"] = $response;
        # $return = json_encode( $return);
    }
}
#$api_key =  "e4e8de5a-4255-454f-a1dc-a2924092a2ee";
#$rest_api_key = "NzAzZDU3MTgtOTcwZS00OTMzLWIxZGUtNzY1NzExMjBmMmY0";
#$msg_service = new Notify($api_key, $rest_api_key);
#$resp = $msg_service->sendMessage('*', array('employees'));
#var_dump($resp);
