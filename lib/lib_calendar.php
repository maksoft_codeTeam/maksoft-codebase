<?php
namespace Event;
$countries = array( 
  'BG', 'GR', 'RO', 'CY', 'DE',
  'FR', 'UK', 'AD', 'SL', 'MT',
  'VA', 'RS', 'LT', 'RU', 'EE',
  'EL', 'BA', 'NL', 'PL', 'NO',
  'GE', 'AM', 'IT', 'DK', 'SE',
  'PT', 'IS', 'LU', 'HR', 'BY',
  'ME', 'BE', 'CH', 'LI', 'HU',
  'UA', 'MD', 'SK', 'SM', 'KZ',
  'AT', 'TR', 'LV', 'MC', 'AL',
  'FL', 'CS', 'ЕЕ', 'FI', 'MK',
  'ES'); 

interface Type
{
    const NATIONAL=1;
    const CHURCH=2;
    const NAME_DAY=3;
    const PROFFESIONAL=4;
    const TAX=5;
    const OFFICIAL=6;
}

interface Month
{
    const JANUARY = 1;
    const FEBRUARY = 2;
    const MARCH = 3;
    const APRIL = 4;
    const MAY = 5;
    const JUNE = 6;
    const JULY = 7;
    const AUGUST = 8;
    const SEPTEMBER = 9;
    const OCTOBER = 10;
    const NOVEMBER = 11;
    const DECEMBER  = 12;
}


class Order
{
    
}


class Filter
{
    public static function byName($name) 
    {
        return function($ev) use ($name) {
            return $ev->eName == $name;
        };
    }   

    public static function byMDay($day)
    {
        return function ($ev) use ($day) {
            return $ev->mday == $day;
        };
    }

    public static function byMonth($month) 
    {
        return function ($ev) use ($month) {
            return $ev->mon == $month;
        };
    }

    public static function byType($type)
    {
        return function ($ev) use ($type) {
            return $ev->ev_type == $type;
        };
    }

    public static function byTypes(array $types)
    {
        return function($ev) use ($types) {
            return in_array($ev->ev_type, $types);
        };
    }

    public static function byCountry($country) 
    {
        return function ($ev) use ($country) {
            return strtolower($ev->c_code) == strtolower($country);
        };
    }

    public static function byCountries(array $countries) 
    {
        return function ($ev) use ($countries) {
            return in_array(strtolower($ev->c_code), array_map(function($country) {
                return strtolower($country);
            }.$countries));
        };
    }
}


class ResultSet implements \ArrayAccess, \Countable
{
    protected $data;
    protected $filters = array();
    public function __construct($data) {
        $this->data = $data;
    }
    public function _(\Closure $filter) {
        $this->filters[] = $filter;
        return $this;
    }
    public function count()
    {
        return $this->count($data);
    }
    
    public function __invoke()
    {
        $temp_result = $this->data;
        while($filter = array_shift($this->filters)) {
            $temp_result = array_filter($temp_result, $filter);
        }
        return $temp_result;
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }    
}

class Calendar 
{
    protected $gate;

    public function __construct($gate) 
    {
        $this->gate = $gate;
    }

    public function getCountryCodes()
    {
        return $this->gate->event()->getCountryCodes();
    }

    public function month($month)
    {
        return new ResultSet($this->gate->event()->getByMonth($month));
    }

    public function floating()
    {
        $args = func_get_args();
        return new ResultSet(call_user_func_array(array($this->gate->event(), 'getFloatingBy'), $args));
    }
    
    public function all()
    {
        return new ResultSet($this->gate->event()->getAll());
    }
}

class EASTER_WESTERN{
}

class EASTER_ORTHODOX{
}

class EASTER_JULIAN{
}

class EasterCalendar
{
    public static $year = 2015;
    public static $format = 'd-m-Y';
    public static function easter($year, $type)
    {
        $century = $year / 100;   
        $gregorian_shift = $century - $century / 4 -2;
        $x = 15; $y = 6;
        if($type instanceof EASTER_WESTERN){
            $coef = -1;
        } else {
            $coef = 0;
        }
        $x += (2 - (13+8 * $century) / 25 + $gregorian_shift) & ($type instanceof EASTER_WESTERN ? -1 : 0);
        $y += $gregorian_shift & ($type instanceof EASTER_WESTERN ? -1 : 0);

        $g = $year % 19;
        $d = ($g * 19 + $x) % 30;

        $e = (2 * ($year % 4) + 4 * $year - $d + $y) % 7;

        $day = $d + $e;
        $day -= ($e == 6) & (($d==29) || (($d==28) && ($g > 10))) ? 7: 0;
        // Convert Orthodox Easter to Grigorian calendar
        //  day += gregorian_shift & (type == EASTER_ORTHODOX ? -1 : 0);
        $day += $gregorian_shift & ($type instanceof EASTER_ORTHODOX ? -1 : 0);

        $is_may = ($day >= 40);
        $is_not_march = ($day >= 10);

        $month = 3 + $is_not_march + $is_may;
        $day += 22 - ($is_not_march ? 31: 0) - ($is_may ? 30 : 0);

        return new \DateTime(sprintf("%s-%s-%s", $day, $month, $year));
    }

    public static function zagovezni()
    {
        return self::sub("P49D");
    }

    public static function lazarov_den()
    {
        return self::sub("P8D");
    }
    public static function spasovden()
    {
        return self::sub("P40D");
    }
    public static function petdesetnica()
    {
        return self::add("P50D");
    }

    protected static function sub($interval)
    {
        $date = self::easter(self::$year, new EASTER_ORTHODOX);
        $date->sub(new DateInterval($interval));
        return $date->format(self::$format);
    }

    protected static function add($interval)
    {
        $date = self::easter(self::$year, new EASTER_ORTHODOX);
        $date->add(new DateInterval($interval));
        return $date->format(self::$format);
    }

    public static function cvetnica()
    {
        $date = new \DateTime(self::lazarov_den());
        $date->add(new DateInterval("P1D"));
        return $date->format(self::$format);
    }
}


class CalendarRender
{  
     
    /**
     * Constructor
     */
    private $cal;
    public function __construct($events)
    {     
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $calendar = array();
        foreach($events as $ev) {
            if(!isset($calendar[$ev->mday])) {
                $calendar[$ev->mday] = array();
            }

            $calendar[$ev->mday][] = $ev->c_code;

        }
        $this->cal = array_map(function($d) {
            return array_unique($d);
        }, $calendar);
    }

    
     
    /********************* PROPERTY ********************/  
    private $dayLabels = array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
     
    private $currentYear=0;
     
    private $currentMonth=0;
     
    private $currentDay=0;
     
    private $currentDate=null;
     
    private $daysInMonth=0;
     
    private $naviHref= null;
     
    /********************* PUBLIC **********************/  
        
    /**
    * print out the calendar
    */
    public function show() {
        $year  == null;
         
        $month == null;
         
        if(null==$year&&isset($_GET['year'])){
 
            $year = $_GET['year'];
         
        }else if(null==$year){
 
            $year = date("Y",time());  
         
        }          
         
        if(null==$month&&isset($_GET['month'])){
 
            $month = $_GET['month'];
         
        }else if(null==$month){
 
            $month = date("m",time());
         
        }                  
         
        $this->currentYear=$year;
         
        $this->currentMonth=$month;
         
        $this->daysInMonth=$this->_daysInMonth($month,$year);  
         
        $content='<div id="calendar">'.
                        '<div class="box">'.
                        $this->_createNavi().
                        '</div>'.
                        '<div class="box-content">'.
                                '<ul class="label">'.$this->_createLabels().'</ul>';   
                                $content.='<div class="clear"></div>';     
                                $content.='<ul class="dates">';    
                                 
                                $weeksInMonth = $this->_weeksInMonth($month,$year);
                                // Create weeks in a month
                                for( $i=0; $i<$weeksInMonth; $i++ ){
                                     
                                    //Create days in a week
                                    for($j=1;$j<=7;$j++){
                                        $content.=$this->_showDay($i*7+$j);
                                    }
                                }
                                 
                                $content.='</ul>';
                                 
                                $content.='<div class="clear"></div>';     
             
                        $content.='</div>';
                 
        $content.='</div>';
        return $content;   
    }
     
    /********************* PRIVATE **********************/ 
    /**
    * create the li element for ul
    */
    private function _showDay($cellNumber){
         
        if($this->currentDay==0){
             
            $firstDayOfTheWeek = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));
                     
            if(intval($cellNumber) == intval($firstDayOfTheWeek)){
                 
                $this->currentDay=1;
                 
            }
        }
         
        if( ($this->currentDay!=0)&&($this->currentDay<=$this->daysInMonth) ){
             
            $this->currentDate = date('Y-m-d',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.($this->currentDay)));
             
            $cellContent = $this->currentDay;
             
            $this->currentDay++;   
             
        }else{
             
            $this->currentDate =null;
 
            $cellContent=null;
        }
            
        $extra = '';
        if(array_key_exists($this->currentDay, $this->cal)) {
            $extra = '<table class="table"><tr>';
            foreach($this->cal[$this->currentDay] as $day) {
                $extra .= '<td>'.$day.'</td>';
            }
            $extra .= '</tr></table>';
        }
         
        return '<li id="li-'.$this->currentDate.'" class="'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
                ($cellContent==null?'mask':'').'">'.$cellContent.$extra.'</li>';
    }
     
    /**
    * create navigation
    */
    private function _createNavi(){
         
        $nextMonth = $this->currentMonth==12?1:intval($this->currentMonth)+1;
         
        $nextYear = $this->currentMonth==12?intval($this->currentYear)+1:$this->currentYear;
         
        $preMonth = $this->currentMonth==1?12:intval($this->currentMonth)-1;
         
        $preYear = $this->currentMonth==1?intval($this->currentYear)-1:$this->currentYear;
         
        return
            '<div class="header">'.
                '<a class="prev" href="'.$this->naviHref.'?month='.sprintf('%02d',$preMonth).'&year='.$preYear.'">Prev</a>'.
                    '<span class="title">'.date('Y M',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
                '<a class="next" href="'.$this->naviHref.'?month='.sprintf("%02d", $nextMonth).'&year='.$nextYear.'">Next</a>'.
            '</div>';
    }
         
    /**
    * create calendar week labels
    */
    private function _createLabels(){  
                 
        $content='';
         
        foreach($this->dayLabels as $index=>$label){
             
            $content.='<li class="'.($label==6?'end title':'start title').' title">'.$label.'</li>';
 
        }
         
        return $content;
    }
     
     
     
    /**
    * calculate number of weeks in a particular month
    */
    private function _weeksInMonth($month=null,$year=null){
         
        if( null==($year) ) {
            $year =  date("Y",time()); 
        }
         
        if(null==($month)) {
            $month = date("m",time());
        }
         
        // find number of days in this month
        $daysInMonths = $this->_daysInMonth($month,$year);
         
        $numOfweeks = ($daysInMonths%7==0?0:1) + intval($daysInMonths/7);
         
        $monthEndingDay= date('N',strtotime($year.'-'.$month.'-'.$daysInMonths));
         
        $monthStartDay = date('N',strtotime($year.'-'.$month.'-01'));
         
        if($monthEndingDay<$monthStartDay){
             
            $numOfweeks++;
         
        }
         
        return $numOfweeks;
    }
 
    /**
    * calculate number of days in a particular month
    */
    private function _daysInMonth($month=null,$year=null){
         
        if(null==($year))
            $year =  date("Y",time()); 
 
        if(null==($month))
            $month = date("m",time());
             
        return date('t',strtotime($year.'-'.$month.'-01'));
    }
     
}

