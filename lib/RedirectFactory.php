<?php


class RedirectFactory
{
    protected $request, $page;
    const REDIRECT_PAGE_TO_HTTPS = 1;
    const REDIRECT_PAGE_TO_PRIMARY_URL = 3;
    const REDIRECT_SITE_TO_PRIMARY_URL = 2;
    public function __construct(Request $request, page $page) 
    {
        $this->request = $request;
        $this->page = $page;
    }

    public function doRedirect()
    {
        switch($this->page->_page['url_rules']) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
        }
    }
}
