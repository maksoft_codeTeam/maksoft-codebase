<?php 


class i18nResult
{
    protected $text, $fallback;

    public function __construct($text, $fallback=false) 
    {
        $this->text = $text;
        $this->fallback = $fallback;
    }

    public function __toString()
    {
        return $this->text;
    }

    public function isTranslated()
    {
        return $this->fallback;
    }
}
