<?php
//
// Scriptol Link Checker 1.0
// (c) 2008 Scriptol.com
// Free under the GNU GPL 2 License.
// Requires the PHP interpreter.
// Sources are compiled with the Scriptol PHP compiler 7.0
// www.scriptol.com
//
// The program checks the page of a website for broken links.
// Read the manual for details of use at:
//   http://www.scriptol.com/scripts/link-checker.php.
//
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
include_once("path.php");
include_once("dom.php");
include_once("url.php");
$RECURSE=false;
$PROCESSDEFAULT=true;
$counter=0;
$brocount=0;
$redcount=0;
$params=array();
$checked=array();
$scanned=array();
function usage()
{
   echo "\n";
   echo "Link Checher - (c) 2008 Scriptol.com - Freeware.", "\n";
   echo "-----------------------------------------------", "\n";
   echo "Syntax:", "\n";
   echo "  solp linche [options] url", "\n";
   echo "Options:", "\n";
   echo "  -r follow links (default the page only).", "\n";
   echo "  -v verbose, display more infos.", "\n";
   echo "  -q quiet, display nothing.", "\n";
   echo "Arguments:", "\n";
   echo "  url: http address of a page, usually the home page.", "\n";
   echo "Logs stored into links.log.", "\n";
   echo "More info at: http://www.scriptol.com/scripts/", "\n";
   exit(0);
   return;
}

function splitSite($url)
{
   $pos=strpos($url,'/',8);
   if($pos===false)
   {
      $ext=Path::getExtension($url);
      global $extensions;
      if(!in_array($ext,$extensions))
      {
         return array($url,"");
      }
      die("$url not a valid url");
   }
   $site=substr($url,0,$pos);
   $filename=substr($url,$pos+1);
   return array($site,$filename);
}

function isInternal($url)
{
   global $website;
   $l=strlen($website);
   $url=strtolower($url);

   if($website===substr($url,0,$l))
   {
      return true;
   }
   return false;
}

function checkLink($url)
{
   global $counter;
   $counter+=1;
   global $DEBUG;
   if($DEBUG)
   {
      echo "Checking $url", "\n";
   }
   $status=sockAccess($url);
   
   if($status===404)
   {
      display($status,$url,true);
      global $brocount;
      $brocount+=1;
   }
   else
   {
      if($status===301)
      {
         display($status,$url,true);
         global $redcount;
         $redcount+=1;
      }
   else
   {
      if($status===200)
      {
         display($status,$url,false);
      }
   else
   {
      if($status===0)
      {
         display($status,$url,true);
      }
   else
   {
      display($status,$url,true);
   }
   }}}
   global $checked;
   $checked[$url]=$status;
   return $status;
}

function pageScan($fname,$caller)
{
   $current=null;
   $elem=null;
   $xres=0;
   $links=array();
   $d=new DOMDocument();

   
    $xres = @$d->loadHTMLFile($fname);
  
   if($xres===false)
   {
      echo "Error \"$fname\" not found in $caller", "\n";
      global $brocount;
      $brocount+=1;
      return array();
   }
   $dnl=$d->getElementsByTagName("a");
   if($dnl->length===0)
   {
      return array();
   }
   for($i=0;$i<=$dnl->length;$i++)
   {
      $current=$dnl->item($i);
      if($current===null)
      {
         continue;
      }
      $elem=$current;
      if($elem->hasAttribute("href"))
      {
         array_push($links,$elem->getAttribute("href"));
      }
   }
   return $links;
}

function httpCheck($page,$caller)
{
   $links=array();
   $todo=array();
   $reldir="";
   $src="";
   $ext="";
   if(trim($page) ==false)
   {
      return;
   }
   if($page{0}===".")
   {
      return;
   }
   global $scanned;
   if(@array_key_exists($page,$scanned))
   {
      return;
   }
   $scanned[$page]=200;
   global $checked;
   $checked[$page]=200;

   global $DEBUG;
   if($DEBUG)
   {
      echo "Entering $page ", "\n";
   }
   global $differed;
   $differed="\n$page\n".str_repeat("-",strlen($page));
   global $DIFFEREDFLAG;
   $DIFFEREDFLAG=true;

   $infos=pathinfo($page);
   $reldir=@strtolower($infos{'dirname'});
   $src=@strtolower($infos{'filename'});
   $ext=@strtolower($infos{'extension'});

   if(substr($page,-1,1)==="/")
   {
      global $website;
      $l=intVal(strlen($website));
      $reldir=$page;
      $src="";

   }
   else
   {
      $infos=pathinfo($page);
      $reldir=@strtolower($infos{'dirname'});
      $src=@strtolower($infos{'filename'});
      $ext=@strtolower($infos{'extension'});

      if($ext!=false)
      {
         $ext=".".$ext;
         global $extensions;
         if(!in_array($ext,$extensions))
         {
            return;
         }
         $src.=$ext;
      }
   }
   if($DEBUG)
   {
      echo "Processing  $reldir/$src", "\n";
   }
   $links=pageScan($page,$caller);

   if(count($links)===0)
   {
      return;
   }
   $l=count($links);
   for($i=0;$i<$l;$i++)
   {
      $link=$links[$i];
      if($link{0}==="#")
      {
         continue;
      }
      $p=strpos($link,"#",0);
      if($p!=0)
      {
         $link=substr($link,0,$p);
      }
      if(!hasProtocol($link))
      {
         if(strlen($link)>7)
         {
            if(substr($link,0,7)==="mailto:")
            {
               if($DEBUG)
               {
                  echo "Skipped mailto.", "\n";
               }
               continue;
            }
         }
         $link=Path::merge($reldir,$link);
      }
      if(trim($link) ==false)
      {
         continue;
      }
      if(@array_key_exists($link,$checked))
      {
         display($checked[$link],$link,false);
         continue;
      }
      if(isInternal($link))
      {
         global $PROCESSDEFAULT;
         if($PROCESSDEFAULT)
         {
            if(substr($link,-1)==="/")
            {
               $home=findDefault($link);
               if(@array_key_exists($home,$checked)===false)
               {
                  $checked[$home]=200;
               }
            }
         }
         array_push($todo,$link);
      }
      $st=checkLink($link);
      $checked[$link]=$st;
   }
   reset($todo);
   do
   {
      $link= current($todo);
      if(@array_key_exists($link,$scanned))
      {
         continue;
      }
      if(@array_key_exists($link,$checked)===false)
      {
         continue;
      }
      if($checked[$link]===200)
      {
         httpCheck($link,$page);
      }
   }
   while(!(next($todo) === false));

   return;
}

function httpProcess($page)
{
   if(substr($page,-1)==="/")
   {
      $page=findDefault($page);
   }
   httpCheck($page,"command line");
   return;
}

function processCommand($argnum,$arguments)
{
   $opt="";
   if($argnum<2)
   {
      usage();
   }
   reset($arguments);
   do
   {
      $param= current($arguments);
      if(strlen($param)>1)
      {
         $opt=substr($param,0,2);
      }
      else
      {
         usage();
      }
      
      if($opt==="-v")
      {
         global $VERBOSE;
         $VERBOSE=true;
         continue;
      }
      else
      {
         if($opt==="-q")
         {
            global $QUIET;
            $QUIET=true;
            continue;
         }
      else
      {
         if($opt==="-u")
         {
            global $DEBUG;
            $DEBUG=true;
            continue;
         }
      else
      {
         if($opt==="-r")
         {
            global $RECURSE;
            $RECURSE=true;
            continue;
         }
      }}}
      if(substr($param,0,5)==="http:")
      {
         global $server;
         $server=$param;
         continue;
      }
      if($param{0}==="-")
      {
         echo "Unknown command $param", "\n";
         usage();
      }
      global $server;
      if($server ==false)
      {
         $server=$param;
         continue;
      }
      echo "Unknown command $param", "\n";
      usage();
   }
   while(!(next($arguments) === false));

   global $server;
   if($server ==false)
   {
      die("You must provide a URL.");
   }
   global $params;
   $params["server"]=$server;

   return;
}

function main($argc,$argv)
{
   global $brocount;   
   global $redcount;   
   global $counter;   
   $filename="";
   $x=array_slice($argv,1);
   processCommand($argc,$x);
   global $server;
   global $params;
   $server=$params["server"];

   if(!hasProtocol($server))
   {
      $server="http://".$server;
   }
   global $website;
   $_I1=splitSite($server);
   $website=reset($_I1);
   $filename=next($_I1);
   $website=strtolower($website);

   global $domain;
   $domain=substr($website,7);
   if(substr($domain,-1,1)==="/")
   {
      $domain=substr($domain,0);
   }
   global $baseLength;
   $baseLength=intVal(strlen($domain)+7);

   global $QUIET;
   if(!$QUIET)
   {
      global $VERBOSE;
      if($VERBOSE===true)
      {
         echo "Verbose mode enabled", "\n";
      }
      global $DEBUG;
      if($DEBUG===true)
      {
         echo "Debug mode enabled", "\n";
      }
      echo "Website is", " ", $website, "\n";
      echo "Domain is", " ", $domain, "\n";
      echo "Starting from $server", "\n";
   }
   global $log;
   $log=fopen("links.log","w");
   httpProcess($server);
   global $scanned;
   $sp=count($scanned);
   fwrite($log,"$brocount broken links, ");
   fwrite($log,"$redcount redirects, in ");
   fwrite($log,"$counter links checked in $sp pages.");
   fclose($log);
   if($QUIET)
   {
      return 0;
   }
   global $brocount;
   global $redcount;
   global $counter;
   echo $brocount, " ", " broken links,", " ", $redcount, " ", "redirect, in", " ", $counter, " ", "links checked in $sp pages.", "\n";
   return 0;
}

//main(intVal($argc),$argv);

//main func
//print_r(checkLink($url));  
?>
