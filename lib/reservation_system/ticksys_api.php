<?php
require_once $_SERVER['DOCUMENT_ROOT']."/lib/lib_rss.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation_system/fields.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation_system/default_messages.php";


class TicksysApi extends rss
{
    private $_settings=array();
    private $_xml;
    public $first_city;
    public $last_city;

    public function __construct()
    {
        $this->_xml = new DomDocument();
    }

    public function __get($index)
    {
        $restricted = array('pass', 'company', 'min_free_places');
        if (!in_array($index, $restricted)):
            return $this->_settings[$index];
        endif;
        throw new ValidationError(sprintf("Private value %s", $index), 1);
        
    }

    public function __set($index, $value)
    {
        $this->_settings[$index] = $value;
    }

    public function get_schedule()
    {
        $first_city = new PositiveIntegerField($this->first_city);
        $last_city = new PositiveIntegerField($this->last_city);
        $date = new DateTimeField($this->_settings['date']);
        $url = Messages::DEFAULT_URL."GET_SCHEDULES&PASS=".$this->_settings['pass'].
                "&FOR_DATE=".$date()."&FIRST_CITY=".$first_city().
                "&LAST_CITY=".$last_city()."&company=".$this->_settings['company'].
                "&MIN_FREE_PLACES=".$this->_settings['min_free_places'];
        $this->_xml->load($url);
        $x = $this->_xml->saveXML();
        return $this->createArray($x);
    }


    public function get_price_type()
    {
        $url = Messages::DEFAULT_URL."GET_PRICE_TYPE&PASS=".$this->_settings['pass']."&company=".$this->_settings['company'];
        $this->_xml->load($url);
        $x = $this->_xml->saveXML();
        return $this->createArray($x);
    }


    public function get_cities()
    {
        $url = Messages::DEFAULT_URL."GET_CITIES&PASS=".$this->_settings['pass']."&company=".$this->_settings['company'];
        $this->_xml->load($url);
        $x = $this->_xml->saveXML();
        return $this->createArray($x);
    }


    public function get_travel_to()
    {
        $url = Messages::DEFAULT_URL."GET_TRAVEL_TO_CITY&PASS=".$this->_settings['pass']."FOR_DATE=".$this->_settings['date']."&CITY_ID=".$city_id."&company=".$this->_settings['company'];
        $x = $this->_xml->saveXML();
        return $this->createArray($x);
    }


    public function get_bus_types()
    {
        $url = Messages::DEFAULT_URL."GET_BUS_TYPE&PASS=".$this->_settings['pass'];
        $this->_xml->load($url);
        $x = $this->_xml->saveXML();
        return $this->createArray($x);
    }


    public function get_bus_places($bus_type_id)
    {
        $bus_type_id = new PositiveIntegerField($bus_type_id);
        $url = Messages::DEFAULT_URL."GET_BUS_PLACES&PASS=".$this->_settings['pass']."&BUS_TYPE_ID=".$bus_type_id()."&COMPANY=".$this->_settings['company'];
        $this->_xml->load($url);
        $x = $this->_xml->saveXML();
        return $this->createArray($x);
    }


    public function get_free_places($course_id, $f_city, $l_city)
    {
        $course_id = new PositiveIntegerField($course_id);
        $f_city    = new PositiveIntegerField($f_city);
        $l_city    = new PositiveIntegerField($l_city);
        $url       = Messages::DEFAULT_URL.
                    "GET_FREE_PLACES&PASS=".$this->_settings['pass'].
                    "&KURS_ID=".$course_id().
                    "&FIRST_CITY=".$f_city().
                    "&LAST_CITY=".$l_city()."&COMPANY=".$this->_settings['company'];
        $this->_xml->load($url);
        $x = $this->_xml->saveXML();
        return $this->createArray($x);
    }

}
