<?php

class DateTimeField
{
    private $date;
    public function __construct($date='')
    {
	$this->date = $date;
    }

    public function __invoke()
    {
	if(strtotime($this->date)){
	    return date('d.m.Y', strtotime($this->date));
	}
	return date('d.m.Y', time());
    }

    public function tomorrow()
    {
        $datetime = new DateTime($this->__invoke());
        $datetime->modify('+1 day');
        $this->date = $datetime->format('d.m.Y');
        return $this->date;
    }
}


class IntegerField
{


    public function __construct($int)
    {
        $this->int = $int;
    }

    public function __invoke($args='')
    {
        if(is_numeric($this->int)):
            return (int) $this->int;
        endif;
        throw new ValidationError(sprintf("Inavalid integer %s", $this->int), 1);
    }
}


class PositiveIntegerField extends IntegerField
{
	public function __construct($int)
	{
		parent::__construct($int);
	}

	public function __invoke()
	{
		if(parent::__invoke() > 0)
		{
			return $this->int;
		}
		throw new ValidationError(sprintf("%s is not >= 0", $this->int), 1);
	}
}
