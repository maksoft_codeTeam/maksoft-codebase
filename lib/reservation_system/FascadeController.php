<?php


/**
* 
*/
abstract class ArrayXML
{
    public $url;

    public function Describe()
    {
        return $this->name . ", " . $this->age . " years old";    
    }
    
    abstract public function Greet();
}




/**
* 
*/
class TwoWayScheduleFascade
{
    private $date;

    private $first_city;

    private $last_city;

    public function __construct($date, $first_city, $last_city)
    {
        $this->date       = $date;
        $this->first_city = $first_city;
        $this->last_city  = $last_city;
    }

    public function __toString()
    {
        $fist_way = new OneWayScheduleFascade($date, $first_city, $last_city);
    }
}


require_once $_SERVER['DOCUMENT_ROOT']."/lib/lib_rss.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation_system/fields.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation_system/default_messages.php";

class TicksysApi
{
    public function get_schedule()
    {
        $url = Messages::DEFAULT_URL."GET_SCHEDULES&PASS=".$this->_settings['pass'].
                "&FOR_DATE=".$date()."&FIRST_CITY=".$first_city().
                "&LAST_CITY=".$last_city()."&company=".$this->_settings['company'].
                "&MIN_FREE_PLACES=".$this->_settings['min_free_places'];
        return $url;
    }


    public function get_price_type()
    {
        $url = Messages::gh."GET_PRICE_TYPE&PASS=".$this->_settings['pass']."&company=".$this->_settings['company'];
        return $url;
    }


    public function get_cities()
    {
        $url = Messages::DEFAULT_URL."GET_CITIES&PASS=".$this->_settings['pass']."&company=".$this->_settings['company'];
        return $url;
    }


    public function get_travel_to()
    {
        $url = Messages::DEFAULT_URL."GET_TRAVEL_TO_CITY&PASS=".$this->_settings['pass']."FOR_DATE=".$this->_settings['date']."&CITY_ID=".$city_id."&company=".$this->_settings['company'];
        return $url;
    }


    public function get_bus_types()
    {
        $url = Messages::DEFAULT_URL."GET_BUS_TYPE&PASS=".$this->_settings['pass'];
        return $url;
    }


    public function get_bus_places($bus_type_id)
    {
        $bus_type_id = new PositiveIntegerField($bus_type_id);
        $url = Messages::DEFAULT_URL."GET_BUS_PLACES&PASS=".$this->_settings['pass']."&BUS_TYPE_ID=".$bus_type_id()."&COMPANY=".$this->_settings['company'];
        return $url;
    }


    public function get_free_places($course_id, $f_city, $l_city)
    {
        $url       = Messages::DEFAULT_URL.
                    "GET_FREE_PLACES&PASS=".$this->_settings['pass'].
                    "&KURS_ID=".$course_id().
                    "&FIRST_CITY=".$f_city().
                    "&LAST_CITY=".$l_city()."&COMPANY=".$this->_settings['company'];
        return $url;
    }

}





interface UrlAdapter
{
    public function get_schedule($date, $first_city, $last_city, $pass, $company_id);


    public function get_price_type($pass, $company_id);


    public function get_cities($pass, $company_id);


    public function get_travel_to($date, $city_id, $pass, $company_id);


    public function get_bus_types($pass, $company_id);


    public function get_bus_places($bus_type_id, $pass, $company_id);


    public function get_free_places($course_id, $f_city, $l_city, $pass, $company_id);
}

/**
* 
*/
class TicksysAdapter implements UrlAdapter
{
    private $ticksys;
    private $xml;
    private $data;

    function __construct(TicksysApi $ticksys)
    {
        $this->ticksys = $ticksys;
        $this->xml     = new DomDocument();
        $this->$data   = new XML2Array();
    }

    private function return_array($url)
    {
        return $this->$data->createArray($this->xml->load($url));
    }

    public function get_schedule($date, $first_city, $last_city, $pass, $company_id)
    {
        $url = $this->ticksys->get_schedule($date, $first_city, $last_city, $pass, $company_id);
        return $this->return_array($url)
    }


    public function get_price_type($pass, $company_id)
    {
        $url = $this->ticksys->get_price_type($pass, $company_id);
        return $this->return_array($url)
    }


    public function get_cities($pass, $company_id)
    {
        $url = $this->ticksys->get_cities($pass, $company_id);
        return $this->return_array($url)
    }


    public function get_travel_to($date, $city_id, $pass, $company_id)
    {
        $url = $this->ticksys->get_travel_to($date, $city_id, $pass, $company_id);
        return $this->return_array($url)
    }


    public function get_bus_types($pass, $company_id)
    {
        $url = $this->ticksys->get_bus_types($pass, $company_id);
        return $this->return_array($url)
    }


    public function get_bus_places($bus_type_id, $pass, $company_id)
    {
        $url = $this->ticksys->get_bus_places($bus_type_id, $pass, $company_id);
        return $this->return_array($url)
    }


    public function get_free_places($course_id, $f_city, $l_city, $pass, $company_id)
    {
        $url = $this->ticksys->get_free_places($course_id, $f_city, $l_city, $pass, $company_id);
        return $this->return_array($url)
    }
}


/**
* 
*/
class OneWayScheduleFascade extends TicksysAdapter
{
    private $date;

    private $first_city;

    private $last_city;

    private $ticksys;

    private $table;

    public function __construct($date, $first_city, $last_city)
    {
        $this->date       = new DateTimeField($date);
        $this->first_city = new PositiveIntegerField($first_city);
        $this->last_city  = new PositiveIntegerField($last_city);
        $this->table      = new PrettyTable()
    }

    public function __toString()
    {
        return $this->generate_table($this->getScheduleData());
    }

    public function getScheduleData()
    {
        $data = 
    }

    private function generate_table()
    {
        $this->table = new PrettyTable();
        $data        = $this->get_schedule($this->date(), $this->first_city(),
                                           $this->last_city());
        $button      = '<a href="'.$this->get_pLink($this->_settings['biletni_kasi']).'" class="btn btn-primary bluebutton" alt="'.Messages::BUTTON_ALT_MESSAGE.'">'.Messages::BUTTON_MESSAGE.'</a>';
        $this->table->set_field_names(array("Äàòà","Íàïðàâëåíèå", "×àñ íà òðúãâàíå",
                                      "×àñ íà ïðèñòèãàíå", "Áèëåòè","Ñåêòîð"));
        foreach($routes["RESULT"]["COMPANY"]["SCHEDULE"] as $route):
            $this->prices[] = $route["PRICES"];
            $time = explode(' ', $route["SCHEDULE_DATETIME"]);
            $this->table->add_row(array($time[0], iconv('utf8', 'cp1251', $route["NAME"]), 
                                  $route["START_TIME"], $route["END_TIME"], $button, $route["SEKTOR"]));
        endforeach;
        return (string) $this->table;
    }
}
