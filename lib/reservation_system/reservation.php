<?php
require_once $_SERVER['DOCUMENT_ROOT']."/lib/reservation_system/ticksys_api.php";
require_once $_SERVER['DOCUMENT_ROOT']."/lib/prettytable.php";


class Helpers extends TicksysApi
{

    private $prices = array();

    public function __construct()
    {
        parent::__construct();
    }

    private function gen_schedule_table()
    {
        $table = new PrettyTable();
        $button = '<a href="'.$this->get_pLink($this->biletni_kasi).'" class="btn btn-primary bluebutton" alt="'.Messages::BUTTON_ALT_MESSAGE.'">'.Messages::BUTTON_MESSAGE.'</a>';
        $table->set_field_names(array("����","�����������", "��� �� ��������",
                                      "��� �� ����������", "������","������"));
        $routes = $this->get_schedule();
        foreach($routes["RESULT"]["COMPANY"]["SCHEDULE"] as $route):
            $this->prices[] = $route["PRICES"];
            $time = explode(' ', $route["SCHEDULE_DATETIME"]);
            $table->add_row(array($time[0],iconv('utf8', 'cp1251', $route["NAME"]),
                                        $route["START_TIME"], $route["END_TIME"],
                                        $button, $route["SEKTOR"]));
        endforeach;
        return (string) $table;
    }

    private function gen_weather_texts($city)
    {
        global $wwo;
        $city = trim($city);
        ob_start();
        if (mb_strtolower($city[0]) == '�'):
            echo sprintf(Messages::WEATHER_TEXT_2, $city);
        else:
            echo sprintf(Messages::WEATHER_TEXT_1, $city);
        endif;
        $wwo->city = $city;
        include($_SERVER['DOCUMENT_ROOT']."/wwo/wwo_forecast.php");
        $currentoutput = ob_get_clean();
        return $currentoutput;
    }


    private function get_prices()
    {
        $table = "";
        foreach (array_unique($this->prices) as $price_type):
            foreach ($price_type as $price):
                $table .= "<tr>";
                $table .= "<td>".iconv('utf8', 'cp1251', $price["PRICE_NAME"])."</td>";
                $table .= "<td><strong>".iconv('utf8', 'cp1251', $price["PRICE"])."</strong></td>";
                $table .= "</tr>";
            endforeach;
        endforeach;
        $file_path = $_SERVER['DOCUMENT_ROOT']."/lib/reservation_system/views/print_prices.html";
        $h = fopen($file_path, 'rb');
        $template = fread($h, filesize($file_path));
        fclose($h);
        $cities = $this->get_cities();
        $cities = $cities['RESULT']["COMPANY"]["CITY"];
        $search = array("{first_city}", "{last_city}", "{table}");
        $replace = array($this->data_search($this->first_city, $cities),
                         $this->data_search($this->last_city, $cities), $table);
        return str_replace($search, $replace, $template);
    }

    private function data_search($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            if ($value["ID"] == (string) $needle):
                return iconv('utf8', 'cp1251', (string)$value["NAME"]);
            endif;
        }
    }

    public function print_schedule()
    {
        return $this->gen_schedule_table();
    }

    public function print_weather($city)
    {
        return $this->gen_weather_texts($city);
    }

    public function print_prices()
    {
        return $this->get_prices();
    }

    public function return_cities()
    {
        $cities = $this->get_cities();
        $tmp = array();
        foreach ($cities['RESULT']["COMPANY"]["CITY"] as $city):
            $tmp[$city["ID"]] = $city["NAME"];
        endforeach;
        return $tmp;
    }
}



class Ticksys_test extends Helpers
{
    public function __construct()
    {
        parent::__construct();
    }
}
