<?php


class Facebook
{
    protected $url = array(
        "GRAPH" => "https://graph.facebook.com/v2.8/",
        "BASE"  => "https://www.facebook.com/v2.8/"
    );

    protected $scope = array(
        "public_profile", 
        "email"
    );

    protected $APP_ID;
    protected $APP_SECRET;

    protected $FB;

    protected $redirect_url;
    
    public function __construct($api_key, $api_secret)
    {
        $this->APP_ID = $api_key;

        $this->APP_SECRET = $api_secret;

    }

    public function add_permission($permission){
        $this->scope[] = $permission;
    }

    public function set_redirect_url($url){
        $this->redirect_url = $url;
    }


    private function call($url){
        return json_decode(file_get_contents($url));
    }


    public function log_user(){
        $callback_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $login_dialog_url = $this->url['BASE'] . "dialog/oauth?client_id=".self::APP_ID."&display=popup&scope=".implode(array_unique($this->scope))."&redirect_uri=".$callback_url;
        header("Location: $login_dialog_url");
    }

    public function get_access_token(){
        $this->FB = $this->call(
            $this->get_url_for_token(
                $this->get_user_code_from_uri()
            )
        ); 
        return $this;
    }

    public function me(){
        return $this->FB;
    }

    public function get_me_fields(){
         $url = $this->url['GRAPH'] . "me?fields=id%2Cname%2Cpicture%2Cemail&access_token=".$this->FB->access_token;
         $this->FB->me = $this->call($url);
         return $this->FB->me;
    }

    private function get_user_code_from_uri(){
        $parsed = parse_url($_SERVER['REQUEST_URI']);
        parse_str($parsed['query'], $output_arr);
        return $output_arr['code'];
    }

    private function get_url_for_token($USER_CODE){
        return $this->url['GRAPH']."oauth/access_token?" .http_build_query(
                    array(
                        "client_id"     => self::APP_ID,
                        "redirect_uri"  => $this->redirect_url,
                        "client_secret" => self::APP_SECRET,
                        "code"          => $USER_CODE
                    )
                );
    }
}
