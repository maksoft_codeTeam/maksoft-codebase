<?php


class i18n implements ArrayAccess, IteratorAggregate, Countable
{
    public $services;
    public $cachePath;
    
    public $prefix = 'Q';
    public $cacheTime = 592200; // 7 days
    public $language;
    public $appliedLang;
    public static $data = array();

    public function __construct(\Pimple\Container $services) 
    {
        $this->services = $services;   
        $this->cachePath = realpath(__DIR__.'/../localizations/');
        $this->language = $this->services['gate']->site()->getSiteLanguage($this->getLanguageId());
        $this->appliedLang = strtolower($this->language->language_key);
    }

    public function getTranslations()
    {
        self::$data =  $this->services['gate']->lang()->getTranslations($this->getSiteID(), $this->getLanguageId());
        return self::$data;
    }

    public function getLanguageId()
    {
        return 2; 
        return $this->services['o_page']->lang_id;
    }

    

    public function getSiteID()
    {
        return $this->services['o_page']->_site['SitesID'];
    }

    public function __set($offset, $value)
    {
        self::$data[$offset] = $value;
    }

    public function __get($offset)
    {
        return $this->getTranslation($text);
    }

    public function __isset($offset)
    {
        return $this->offsetExists($offset);
    }

    public function __unset($offset)
    {
        unset(self::$data[$offset]);
    }

    public final function __toString()
    {
        return $this->generate();
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            self::$data[] = $value;
        } else {
            self::$data[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset(self::$data[$offset]);
    }

    public function offsetUnset($offset) {
        unset(self::$data[$offset]);
    }

    public function offsetGet($offset) {
        return $this->getTranslation($text);
    }

    public function getTranslation($text) 
    {
        return $this->offsetExists($text) ? self::$data[$text] : $text;
    }

    public function getIterator() {
        return new \ArrayIterator(self::$data);
    }

    public function count()
    {
        return count(self::$data);
    }

    public function compile()
    {
        if($this->isCacheOutdated()) {
            $this->getTranslations();
            $this->createCacheDirIfNotExists();
            $this->writeCacheFile();
        }

        require_once $this->getCachedFilePath();
        return $this;
    }

    public function writeCacheFile()
    {
        if (file_put_contents($this->getCachedFilePath(), $this->buildCls()) === FALSE) {
            throw new Exception("Could not write cache file to path '" . $this->getCachedFilePath() . "'. Is it writable?");
        }

        chmod($this->getCachedFilePath(), 0755);
    }

    public function buildCls()
    {
        return
            '<?php'.PHP_EOL 
            .'    class '.$this->prefix. ' { '.PHP_EOL
            .'    protected static $data = '.var_export(self::$data, true).'; '.PHP_EOL
            .'    public static function __() { '.PHP_EOL
            .'         $args  = func_get_args(); '.PHP_EOL
            .'         $text = array_shift($args); '.PHP_EOL 
            .'         $t = md5(trim(mb_strtolower($text)));'.PHP_EOL
            .'         $text = isset(self::$data[$t]) ? self::$data[$t] : $text; '.PHP_EOL
            .'         $args = $args ? $args : array("%s");'. PHP_EOL
            .'         return call_user_func_array("vsprintf", array_merge(array($text), $args)); '.PHP_EOL
            .'    }'.PHP_EOL
            .'}';  
    }

    public function getCacheFileName()
    {
        return 'php_i18n_siteID'.$this->getSiteID() . '_' . $this->prefix . '_' . $this->appliedLang . '.cache.php';
    }

    public function getCachedFilePath()
    {
        return $this->cachePath . '/'.$this->getCacheFileName();
    }

    public function isCachedFileExists()
    {
        return file_exists($this->getCachedFilePath());
    }
    
    public function isCacheOutdated()
    {
        if(!$this->isCachedFileExists()) {
            return true;
        }

        return filemtime($this->getCachedFilePath()) + $this->cacheTime < time();
    }

    public function createCacheDirIfNotExists()
    {
        if( ! is_dir($this->cachePath)) {
            mkdir($this->cachePath, 0755, true);
        }

        return $this;
    }
}
