<?php
// http://code.google.com/apis/ajaxlanguage/documentation/#HelloWorld
?>
<html> 
  <head> 
    <script type="text/javascript" src="http://www.google.com/jsapi"> 
    </script> 
    <script type="text/javascript"> 
 
    google.load("language", "1"); 
 
    function initialize() { 
      var text = document.getElementById("text").innerHTML; 
      google.language.detect(text, function(result) { 
        if (!result.error && result.language) { 
          google.language.translate(text, result.language, "en", 
                                    function(result) { 
            var translated = document.getElementById("translation"); 
            if (result.translation) { 
              translated.innerHTML = result.translation; 
            } 
          }); 
        } 
      }); 
    } 
    google.setOnLoadCallback(initialize); 
 
    </script> 
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1251"></head> 
  <body> 
    <div id="text">�� ��� ������</div> 
    <div id="translation"></div> 
  </body> 
</html>
