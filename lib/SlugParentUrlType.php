<?php
require_once __DIR__.'/AbstractUrlResolver.php';


class SlugParentUrlType extends AbstractUrlResolver
{
    public function generate()
    {

        $slug = $this->buildSlug()->getSlug();

        if ($lang = $this->request->query->get('lang')) {
            return sprintf("/%s/%s%s", urlencode($lang), $slug, $this->prefix); 
        }

        return sprintf("/%s%s", $slug, $this->prefix); 
    }

    public function buildSlug()
    {
        if($this->isHomepage()) {
            $this->prefix = '';
            $this->slug = '';
            return $this;
        }

        $slug = empty($this->page->slug) ? $this->page->n : $this->page->slug; 

        if($this->page->pslug) {
            $slug = $this->page->pslug . '/'.$slug;
        }
        $this->slug = $slug;
        return $this;
    }
}
