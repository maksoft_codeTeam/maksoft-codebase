<?php
require_once __DIR__.'/AbstractUrlResolver.php';


class AdminUrlType extends AbstractUrlResolver
{
    public function generate()
    {
        $url_arr = array();
        if($templ = $this->request->query->get('templ')) {
            $url_arr['templ'] = $templ;
        }

        if($this->request->query->get('lang')) {
            $url_arr['lang'] = $this->request->query->get('lang');
        }

        $url_arr['n'] = $this->page->n;
        $url_arr['SiteID'] = $this->page->SiteID;

        if (isset($_GET['templ'])) {
            $url_arr['templ'] = $_GET['templ'];
        }

        return sprintf("/page.php?%s", http_build_query($url_arr)); 
    }
}
