<?php
require_once __DIR__."/lib_page.php";


class page_bs extends page {
    function print_pSubContentBS($parent_page=NULL, $filter=1, $filter_by_tag = true, $cms_args = array(), $limit=false)
    {
        if(!$parent_page) {
            $parent_page = $this->n;
        }

        $max_columns = $this->_page['show_link_cols'];

        $order = $this->get_pSubPagesOrder($parent_page);

        $subpages = $this->get_pSubpages($parent_page, $order);


        $g_order = $order;

        if($order == "p.sort_n ASC" || $order == "ASC" || $order == "p.sort_n DESC" || $order == "DESC") {
            $g_order = NULL;
        }

        $g_pages = $this->get_pGroupContent($this->_page['show_group_id'], date("Y-m-d"), $g_order, $filter);
        $subpages = array_merge($subpages, $g_pages ? $g_pages : array());

        $bootstrap_column = ceil(12 / $max_columns);
        $txt = '<div class="row">';

        foreach($subpages as $page) {
            $link = $page['page_link'];
            $img = (!empty($page['image_src']) ? $page['image_src'] : '/web/admin/images/no_image.jpg') ;
            $img_alt = (!empty($page['imageName']) ? $page['imageName'] : $page['Name']);
            $full_title = $page['Name'];
            $title = cut_text($full_title, MAX_TITLE_LENGTH,'');
            $txt .= <<<HEREDOC
            <div class="col-xs-12 col-sm-6 col-md-$bootstrap_column">
                <div class="sPage-content">
                    <a href="$link" title="$full_title" class="title">
                        <div class="bullet1" style="float:left;"></div>
                        <div class="text title-trunc">$title</div>
                    </a><br style="clear: both;" class="br-style">
                    <a href="$link" class="img align-center" style="" title="8582.jpg">
                        <img src="$img" width="100%" align="" class="align-center" alt="$img_alt" border="0">
                    </a>
                    $modal_button
                </div>
            </div>
HEREDOC;

            if($max_columns % $i){
                $i = 0;
                $txt .= '</div><div class="row">';
            } else {
                $i++;
            }
        }
        $txt .= '</div>';
        $txt .= '<div class="sDelimeter"></div>';
        echo $txt;
    }
}
